.class public Lcom/intsig/document/widget/PagesView;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/document/widget/PagesView$k;,
        Lcom/intsig/document/widget/PagesView$AnnotFocusState;,
        Lcom/intsig/document/widget/PagesView$z;,
        Lcom/intsig/document/widget/PagesView$x;,
        Lcom/intsig/document/widget/PagesView$q;,
        Lcom/intsig/document/widget/PagesView$a0;,
        Lcom/intsig/document/widget/PagesView$y;,
        Lcom/intsig/document/widget/PagesView$s;,
        Lcom/intsig/document/widget/PagesView$l;,
        Lcom/intsig/document/widget/PagesView$t;,
        Lcom/intsig/document/widget/PagesView$u;,
        Lcom/intsig/document/widget/PagesView$n;,
        Lcom/intsig/document/widget/PagesView$RenderArgs;,
        Lcom/intsig/document/widget/PagesView$b0;,
        Lcom/intsig/document/widget/PagesView$w;,
        Lcom/intsig/document/widget/PagesView$r;,
        Lcom/intsig/document/widget/PagesView$p;,
        Lcom/intsig/document/widget/PagesView$o;,
        Lcom/intsig/document/widget/PagesView$v;,
        Lcom/intsig/document/widget/PagesView$m;,
        Lcom/intsig/document/widget/PagesView$InkArgs;,
        Lcom/intsig/document/widget/PagesView$WatermarkArgs;,
        Lcom/intsig/document/widget/PagesView$ImageArgs;,
        Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;,
        Lcom/intsig/document/widget/PagesView$ElementData;,
        Lcom/intsig/document/widget/PagesView$BaseArgs;,
        Lcom/intsig/document/widget/PagesView$AnnotBox;,
        Lcom/intsig/document/widget/PagesView$ShapeArgs;,
        Lcom/intsig/document/widget/PagesView$StampArgs;,
        Lcom/intsig/document/widget/PagesView$TextArgs;
    }
.end annotation


# instance fields
.field public A:Z

.field public A0:F

.field public B:Z

.field public B0:I

.field public C:Z

.field public C0:[F

.field public D:Z

.field public E:Ljava/lang/String;

.field public F:Z

.field public G:F

.field public H:F

.field public I:Landroid/graphics/drawable/Drawable;

.field public J:Landroid/graphics/drawable/Drawable;

.field public K:Landroid/graphics/drawable/Drawable;

.field public L:I

.field public M:I

.field public N:I

.field public O:I

.field public P:I

.field public Q:I

.field public R:I

.field public S:I

.field public T:Z

.field public U:Z

.field public V:Z

.field public W:Z

.field public a:Z

.field public a0:Z

.field public b:La/b;

.field public b0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/document/widget/PagesView$ElementData;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroidx/core/view/GestureDetectorCompat;

.field public c0:Lcom/intsig/document/widget/PagesView$ElementData;

.field public d:Landroid/view/ScaleGestureDetector;

.field public d0:I

.field public e:Lcom/intsig/document/widget/PagesView$v;

.field public e0:Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;

.field public f:Landroid/os/Handler;

.field public f0:Lcom/intsig/document/widget/PagesView$InkArgs;

.field public g:Landroid/os/Handler;

.field public g0:Lcom/intsig/document/widget/PagesView$p;

.field public h:Landroid/os/HandlerThread;

.field public h0:Landroid/graphics/Path;

.field public i:F

.field public i0:F

.field public j:F

.field public j0:F

.field public k:F

.field public k0:F

.field public l:I

.field public l0:F

.field public m:I

.field public m0:Lcom/intsig/document/widget/PagesView$r;

.field public n:I

.field public n0:Lcom/intsig/document/widget/a;

.field public o:I

.field public o0:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/intsig/document/widget/PagesView$RenderArgs;",
            ">;"
        }
    .end annotation
.end field

.field public p:Landroid/graphics/RectF;

.field public p0:Landroid/graphics/Paint;

.field public q:Z

.field public q0:I

.field public r:Lb/a;

.field public r0:Lcom/intsig/document/widget/PagesView$a0;

.field public s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/document/widget/PagesView$s;",
            ">;"
        }
    .end annotation
.end field

.field public s0:Ljava/util/Timer;

.field public t:Z

.field public t0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/document/DocumentLoader$d;",
            ">;"
        }
    .end annotation
.end field

.field public u:F

.field public u0:Lcom/intsig/document/DocumentLoader$d;

.field public v:F

.field public v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

.field public w:F

.field public w0:Lcom/intsig/document/widget/PagesView$z;

.field public x:F

.field public x0:Lcom/intsig/document/widget/PagesView$k;

.field public y:F

.field public y0:Z

.field public z:F

.field public z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->a:Z

    const/high16 v1, 0x42000000    # 32.0f

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->i:F

    const/high16 v1, 0x41800000    # 16.0f

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->j:F

    const v1, 0x3dcccccd    # 0.1f

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->k:F

    const/4 v1, 0x0

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->l:I

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->m:I

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->n:I

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->o:I

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/intsig/document/widget/PagesView;->p:Landroid/graphics/RectF;

    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->q:Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->t:Z

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->u:F

    const/4 v3, 0x0

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->v:F

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->w:F

    const/high16 v4, -0x40800000    # -1.0f

    iput v4, p0, Lcom/intsig/document/widget/PagesView;->x:F

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->z:F

    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->A:Z

    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->B:Z

    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->C:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->D:Z

    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->G:F

    const/high16 v3, 0x41700000    # 15.0f

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->H:F

    const/high16 v3, -0x1000000

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->L:I

    const/4 v3, -0x1

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->M:I

    const v3, -0x80809

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->N:I

    const v3, -0x278ae3

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->O:I

    const v3, 0x66aaaaaa

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->P:I

    const/16 v3, 0x14

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->Q:I

    const/4 v4, 0x2

    iput v4, p0, Lcom/intsig/document/widget/PagesView;->R:I

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->S:I

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->T:Z

    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->U:Z

    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->V:Z

    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->W:Z

    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->d0:I

    new-instance v4, Lcom/intsig/document/widget/PagesView$InkArgs;

    const/high16 v5, -0x10000

    invoke-direct {v4, v5, v2}, Lcom/intsig/document/widget/PagesView$InkArgs;-><init>(IF)V

    iput-object v4, p0, Lcom/intsig/document/widget/PagesView;->f0:Lcom/intsig/document/widget/PagesView$InkArgs;

    new-instance v2, Lcom/intsig/document/widget/PagesView$p;

    invoke-direct {v2}, Lcom/intsig/document/widget/PagesView$p;-><init>()V

    iput-object v2, p0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/intsig/document/widget/PagesView;->o0:Ljava/util/LinkedList;

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    new-instance v0, Lcom/intsig/document/widget/PagesView$a0;

    invoke-direct {v0}, Lcom/intsig/document/widget/PagesView$a0;-><init>()V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    sget-object v0, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->None:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->y0:Z

    iput-object v3, p0, Lcom/intsig/document/widget/PagesView;->z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    const/high16 v0, 0x40c00000    # 6.0f

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->A0:F

    const v0, -0x7703b700

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->B0:I

    const/4 v0, 0x4

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->C0:[F

    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/PagesView;->a(Landroid/content/Context;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x41400000    # 12.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 2
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x1

    iput-boolean p2, p0, Lcom/intsig/document/widget/PagesView;->a:Z

    const/high16 v0, 0x42000000    # 32.0f

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->i:F

    const/high16 v0, 0x41800000    # 16.0f

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->j:F

    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->k:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->l:I

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->m:I

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->n:I

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->o:I

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/intsig/document/widget/PagesView;->p:Landroid/graphics/RectF;

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->q:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->t:Z

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->u:F

    const/4 v2, 0x0

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->v:F

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->w:F

    const/high16 v3, -0x40800000    # -1.0f

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->x:F

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->z:F

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->A:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->B:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->C:Z

    iput-boolean p2, p0, Lcom/intsig/document/widget/PagesView;->D:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->G:F

    const/high16 v2, 0x41700000    # 15.0f

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->H:F

    const/high16 v2, -0x1000000

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->L:I

    const/4 v2, -0x1

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->M:I

    const v2, -0x80809

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->N:I

    const v2, -0x278ae3

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->O:I

    const v2, 0x66aaaaaa

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->P:I

    const/16 v2, 0x14

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->Q:I

    const/4 v3, 0x2

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->R:I

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->S:I

    iput-boolean p2, p0, Lcom/intsig/document/widget/PagesView;->T:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->U:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->V:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->W:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->d0:I

    new-instance v3, Lcom/intsig/document/widget/PagesView$InkArgs;

    const/high16 v4, -0x10000

    invoke-direct {v3, v4, v1}, Lcom/intsig/document/widget/PagesView$InkArgs;-><init>(IF)V

    iput-object v3, p0, Lcom/intsig/document/widget/PagesView;->f0:Lcom/intsig/document/widget/PagesView$InkArgs;

    new-instance v1, Lcom/intsig/document/widget/PagesView$p;

    invoke-direct {v1}, Lcom/intsig/document/widget/PagesView$p;-><init>()V

    iput-object v1, p0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/intsig/document/widget/PagesView;->o0:Ljava/util/LinkedList;

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    new-instance p2, Lcom/intsig/document/widget/PagesView$a0;

    invoke-direct {p2}, Lcom/intsig/document/widget/PagesView$a0;-><init>()V

    iput-object p2, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    sget-object p2, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->None:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    iput-object p2, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->y0:Z

    iput-object v2, p0, Lcom/intsig/document/widget/PagesView;->z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    const/high16 p2, 0x40c00000    # 6.0f

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->A0:F

    const p2, -0x7703b700

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->B0:I

    const/4 p2, 0x4

    new-array p2, p2, [F

    fill-array-data p2, :array_0

    iput-object p2, p0, Lcom/intsig/document/widget/PagesView;->C0:[F

    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/PagesView;->a(Landroid/content/Context;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x41400000    # 12.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x1

    iput-boolean p2, p0, Lcom/intsig/document/widget/PagesView;->a:Z

    const/high16 p3, 0x42000000    # 32.0f

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->i:F

    const/high16 p3, 0x41800000    # 16.0f

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->j:F

    const p3, 0x3dcccccd    # 0.1f

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->k:F

    const/4 p3, 0x0

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->l:I

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->m:I

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->n:I

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->o:I

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->p:Landroid/graphics/RectF;

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->q:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->t:Z

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->u:F

    const/4 v1, 0x0

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->v:F

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->w:F

    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->x:F

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->z:F

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->A:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->B:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->C:Z

    iput-boolean p2, p0, Lcom/intsig/document/widget/PagesView;->D:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->G:F

    const/high16 v1, 0x41700000    # 15.0f

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->H:F

    const/high16 v1, -0x1000000

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->L:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->M:I

    const v1, -0x80809

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->N:I

    const v1, -0x278ae3

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->O:I

    const v1, 0x66aaaaaa

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->P:I

    const/16 v1, 0x14

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->Q:I

    const/4 v2, 0x2

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->R:I

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->S:I

    iput-boolean p2, p0, Lcom/intsig/document/widget/PagesView;->T:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->U:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->V:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->W:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->d0:I

    new-instance v2, Lcom/intsig/document/widget/PagesView$InkArgs;

    const/high16 v3, -0x10000

    invoke-direct {v2, v3, v0}, Lcom/intsig/document/widget/PagesView$InkArgs;-><init>(IF)V

    iput-object v2, p0, Lcom/intsig/document/widget/PagesView;->f0:Lcom/intsig/document/widget/PagesView$InkArgs;

    new-instance v0, Lcom/intsig/document/widget/PagesView$p;

    invoke-direct {v0}, Lcom/intsig/document/widget/PagesView$p;-><init>()V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->o0:Ljava/util/LinkedList;

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    new-instance p2, Lcom/intsig/document/widget/PagesView$a0;

    invoke-direct {p2}, Lcom/intsig/document/widget/PagesView$a0;-><init>()V

    iput-object p2, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    sget-object p2, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->None:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    iput-object p2, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->y0:Z

    iput-object v1, p0, Lcom/intsig/document/widget/PagesView;->z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    const/high16 p2, 0x40c00000    # 6.0f

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->A0:F

    const p2, -0x7703b700

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->B0:I

    const/4 p2, 0x4

    new-array p2, p2, [F

    fill-array-data p2, :array_0

    iput-object p2, p0, Lcom/intsig/document/widget/PagesView;->C0:[F

    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/PagesView;->a(Landroid/content/Context;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x41400000    # 12.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 4
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p2, 0x1

    iput-boolean p2, p0, Lcom/intsig/document/widget/PagesView;->a:Z

    const/high16 p3, 0x42000000    # 32.0f

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->i:F

    const/high16 p3, 0x41800000    # 16.0f

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->j:F

    const p3, 0x3dcccccd    # 0.1f

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->k:F

    const/4 p3, 0x0

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->l:I

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->m:I

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->n:I

    iput p3, p0, Lcom/intsig/document/widget/PagesView;->o:I

    new-instance p4, Landroid/graphics/RectF;

    invoke-direct {p4}, Landroid/graphics/RectF;-><init>()V

    iput-object p4, p0, Lcom/intsig/document/widget/PagesView;->p:Landroid/graphics/RectF;

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->q:Z

    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    iput-object p4, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->t:Z

    const/high16 p4, 0x3f800000    # 1.0f

    iput p4, p0, Lcom/intsig/document/widget/PagesView;->u:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->v:F

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->w:F

    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->x:F

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->z:F

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->A:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->B:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->C:Z

    iput-boolean p2, p0, Lcom/intsig/document/widget/PagesView;->D:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    iput p4, p0, Lcom/intsig/document/widget/PagesView;->G:F

    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->H:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->L:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->M:I

    const v0, -0x80809

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->N:I

    const v0, -0x278ae3

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->O:I

    const v0, 0x66aaaaaa

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->P:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->Q:I

    const/4 v1, 0x2

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->R:I

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->S:I

    iput-boolean p2, p0, Lcom/intsig/document/widget/PagesView;->T:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->U:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->V:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->W:Z

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->d0:I

    new-instance v1, Lcom/intsig/document/widget/PagesView$InkArgs;

    const/high16 v2, -0x10000

    invoke-direct {v1, v2, p4}, Lcom/intsig/document/widget/PagesView$InkArgs;-><init>(IF)V

    iput-object v1, p0, Lcom/intsig/document/widget/PagesView;->f0:Lcom/intsig/document/widget/PagesView$InkArgs;

    new-instance p4, Lcom/intsig/document/widget/PagesView$p;

    invoke-direct {p4}, Lcom/intsig/document/widget/PagesView$p;-><init>()V

    iput-object p4, p0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    new-instance p4, Ljava/util/LinkedList;

    invoke-direct {p4}, Ljava/util/LinkedList;-><init>()V

    iput-object p4, p0, Lcom/intsig/document/widget/PagesView;->o0:Ljava/util/LinkedList;

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    new-instance p2, Lcom/intsig/document/widget/PagesView$a0;

    invoke-direct {p2}, Lcom/intsig/document/widget/PagesView$a0;-><init>()V

    iput-object p2, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    sget-object p2, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->None:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    iput-object p2, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView;->y0:Z

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    const/high16 p2, 0x40c00000    # 6.0f

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->A0:F

    const p2, -0x7703b700

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->B0:I

    const/4 p2, 0x4

    new-array p2, p2, [F

    fill-array-data p2, :array_0

    iput-object p2, p0, Lcom/intsig/document/widget/PagesView;->C0:[F

    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/PagesView;->a(Landroid/content/Context;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x41400000    # 12.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
        0x40800000    # 4.0f
    .end array-data
.end method


# virtual methods
.method public Close()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->t:Z

    .line 6
    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->s0:Ljava/util/Timer;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 14
    .line 15
    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    iput v0, p0, Lcom/intsig/document/widget/PagesView;->l:I

    .line 18
    .line 19
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->t:Z

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$v;->〇080()V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    .line 32
    .line 33
    new-instance v1, Lcom/intsig/document/widget/PagesView$h;

    .line 34
    .line 35
    invoke-direct {v1, p0}, Lcom/intsig/document/widget/PagesView$h;-><init>(Lcom/intsig/document/widget/PagesView;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v1}, Lcom/intsig/document/widget/PagesView$v;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 39
    .line 40
    .line 41
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->n0:Lcom/intsig/document/widget/a;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/document/widget/a;->〇080()V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public CloseSearch()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->t0:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lez v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->t0:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->u0:Lcom/intsig/document/DocumentLoader$d;

    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->t0:Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public CreateAnnotation(I)I
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$a0;->〇o〇:Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    mul-int/lit8 v1, v0, 0x8

    .line 10
    .line 11
    new-array v5, v1, [F

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    :goto_0
    if-ge v1, v0, :cond_0

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 17
    .line 18
    iget-object v2, v2, Lcom/intsig/document/widget/PagesView$a0;->〇o〇:Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    check-cast v2, Landroid/graphics/RectF;

    .line 25
    .line 26
    mul-int/lit8 v3, v1, 0x8

    .line 27
    .line 28
    add-int/lit8 v4, v3, 0x0

    .line 29
    .line 30
    iget v6, v2, Landroid/graphics/RectF;->left:F

    .line 31
    .line 32
    aput v6, v5, v4

    .line 33
    .line 34
    add-int/lit8 v4, v3, 0x1

    .line 35
    .line 36
    iget v7, v2, Landroid/graphics/RectF;->top:F

    .line 37
    .line 38
    aput v7, v5, v4

    .line 39
    .line 40
    add-int/lit8 v4, v3, 0x2

    .line 41
    .line 42
    iget v8, v2, Landroid/graphics/RectF;->right:F

    .line 43
    .line 44
    aput v8, v5, v4

    .line 45
    .line 46
    add-int/lit8 v4, v3, 0x3

    .line 47
    .line 48
    aput v7, v5, v4

    .line 49
    .line 50
    add-int/lit8 v4, v3, 0x6

    .line 51
    .line 52
    aput v8, v5, v4

    .line 53
    .line 54
    add-int/lit8 v4, v3, 0x7

    .line 55
    .line 56
    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    .line 57
    .line 58
    aput v2, v5, v4

    .line 59
    .line 60
    add-int/lit8 v4, v3, 0x4

    .line 61
    .line 62
    aput v6, v5, v4

    .line 63
    .line 64
    add-int/lit8 v3, v3, 0x5

    .line 65
    .line 66
    aput v2, v5, v3

    .line 67
    .line 68
    add-int/lit8 v1, v1, 0x1

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_0
    const/16 v0, 0x9

    .line 72
    .line 73
    const/16 v1, -0x100

    .line 74
    .line 75
    if-ne p1, v0, :cond_2

    .line 76
    .line 77
    :cond_1
    const/16 v7, -0x100

    .line 78
    .line 79
    goto :goto_2

    .line 80
    :cond_2
    const/16 v0, 0xa

    .line 81
    .line 82
    if-ne p1, v0, :cond_3

    .line 83
    .line 84
    const/high16 v0, -0x1000000

    .line 85
    .line 86
    const/high16 v7, -0x1000000

    .line 87
    .line 88
    goto :goto_2

    .line 89
    :cond_3
    const/16 v0, 0xc

    .line 90
    .line 91
    if-ne p1, v0, :cond_4

    .line 92
    .line 93
    goto :goto_1

    .line 94
    :cond_4
    const/16 v0, 0xb

    .line 95
    .line 96
    if-ne p1, v0, :cond_1

    .line 97
    .line 98
    :goto_1
    const/high16 v0, -0x10000

    .line 99
    .line 100
    const/high16 v7, -0x10000

    .line 101
    .line 102
    :goto_2
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 103
    .line 104
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 105
    .line 106
    iget v3, v0, Lcom/intsig/document/widget/PagesView$a0;->〇o00〇〇Oo:I

    .line 107
    .line 108
    const/4 v6, 0x0

    .line 109
    move v4, p1

    .line 110
    invoke-virtual/range {v2 .. v7}, Lb/a;->AddAnnotation(II[FFI)I

    .line 111
    .line 112
    .line 113
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 114
    .line 115
    iget p1, p1, Lcom/intsig/document/widget/PagesView$a0;->〇o00〇〇Oo:I

    .line 116
    .line 117
    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/PagesView;->b(I)V

    .line 118
    .line 119
    .line 120
    const/4 p1, 0x1

    .line 121
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 122
    .line 123
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 124
    .line 125
    iget p1, p1, Lcom/intsig/document/widget/PagesView$a0;->〇o00〇〇Oo:I

    .line 126
    .line 127
    return p1
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public CreateInkAnnotation()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public DeleteAnnot(La/b;)V
    .locals 5

    .line 1
    if-eqz p1, :cond_3

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 4
    .line 5
    iget-wide v0, v0, Lb/a;->〇080:J

    .line 6
    .line 7
    iget v2, p1, La/b;->pageNum:I

    .line 8
    .line 9
    iget v3, p1, La/b;->id:I

    .line 10
    .line 11
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/document/nativelib/PDFium;->DeleteAnnot(JII)I

    .line 12
    .line 13
    .line 14
    iget v0, p1, La/b;->pageNum:I

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Lcom/intsig/document/widget/PagesView$s;

    .line 23
    .line 24
    iget-object v1, v0, Lcom/intsig/document/widget/PagesView$s;->Oo08:Ljava/util/ArrayList;

    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    if-eqz v2, :cond_1

    .line 35
    .line 36
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    check-cast v2, La/b;

    .line 41
    .line 42
    iget v3, v2, La/b;->id:I

    .line 43
    .line 44
    iget v4, p1, La/b;->id:I

    .line 45
    .line 46
    if-le v3, v4, :cond_0

    .line 47
    .line 48
    add-int/lit8 v3, v3, -0x1

    .line 49
    .line 50
    iput v3, v2, La/b;->id:I

    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_1
    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$s;->Oo08:Ljava/util/ArrayList;

    .line 54
    .line 55
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 56
    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->b:La/b;

    .line 59
    .line 60
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    if-eqz v0, :cond_2

    .line 65
    .line 66
    const/4 v0, 0x0

    .line 67
    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->b:La/b;

    .line 68
    .line 69
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 70
    .line 71
    .line 72
    :cond_2
    const/4 v0, 0x1

    .line 73
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 74
    .line 75
    iget p1, p1, La/b;->pageNum:I

    .line 76
    .line 77
    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/PagesView;->b(I)V

    .line 78
    .line 79
    .line 80
    :cond_3
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public Destroy()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/document/widget/PagesView;->Close()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->h:Landroid/os/HandlerThread;

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$v;->〇o〇()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public GetPageCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/document/widget/PagesView;->l:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public GetThumbnails(ILcom/intsig/document/widget/PagesView$b0;)I
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public InsertFreeTextAnnot(Lcom/intsig/document/widget/PagesView$TextArgs;)V
    .locals 9

    .line 1
    iget-object v0, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->rect:Landroid/graphics/RectF;

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 4
    .line 5
    iget v0, v0, Landroid/graphics/RectF;->top:F

    .line 6
    .line 7
    invoke-virtual {p0, v0}, Lcom/intsig/document/widget/PagesView;->a(F)Lcom/intsig/document/widget/PagesView$m;

    .line 8
    .line 9
    .line 10
    new-instance v0, Lcom/intsig/document/DocumentLoader$c;

    .line 11
    .line 12
    iget-object v3, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->rect:Landroid/graphics/RectF;

    .line 13
    .line 14
    iget-object v4, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->text:Ljava/lang/String;

    .line 15
    .line 16
    iget-object v5, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->font:Ljava/lang/String;

    .line 17
    .line 18
    iget v6, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->fontSize:F

    .line 19
    .line 20
    iget v7, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->color:I

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    const/4 v8, 0x0

    .line 24
    move-object v1, v0

    .line 25
    invoke-direct/range {v1 .. v8}, Lcom/intsig/document/DocumentLoader$c;-><init>(ILandroid/graphics/RectF;Ljava/lang/String;Ljava/lang/String;FII)V

    .line 26
    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 29
    .line 30
    invoke-virtual {p1, v0}, Lb/a;->AddTextAnnotation(Lcom/intsig/document/DocumentLoader$c;)I

    .line 31
    .line 32
    .line 33
    const/4 p1, 0x1

    .line 34
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public InsertTextAnnot(Ljava/lang/String;Landroid/graphics/Rect;FI)V
    .locals 8

    .line 1
    iget v0, p2, Landroid/graphics/Rect;->left:I

    .line 2
    .line 3
    iget v0, p2, Landroid/graphics/Rect;->top:I

    .line 4
    .line 5
    int-to-float v0, v0

    .line 6
    invoke-virtual {p0, v0}, Lcom/intsig/document/widget/PagesView;->a(F)Lcom/intsig/document/widget/PagesView$m;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 11
    .line 12
    iget v2, v0, Lcom/intsig/document/widget/PagesView$m;->〇o〇:I

    .line 13
    .line 14
    const/4 v7, 0x1

    .line 15
    move-object v3, p2

    .line 16
    move-object v4, p1

    .line 17
    move v5, p3

    .line 18
    move v6, p4

    .line 19
    invoke-virtual/range {v1 .. v7}, Lb/a;->AddTextAnnotation(ILandroid/graphics/Rect;Ljava/lang/String;FIZ)I

    .line 20
    .line 21
    .line 22
    const/4 p1, 0x1

    .line 23
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public Save(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/document/widget/PagesView$w;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "Save to "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    .line 15
    .line 16
    new-instance v1, Lcom/intsig/document/widget/PagesView$i;

    .line 17
    .line 18
    invoke-direct {v1, p0, p1, p3, p2}, Lcom/intsig/document/widget/PagesView$i;-><init>(Lcom/intsig/document/widget/PagesView;Ljava/lang/String;Lcom/intsig/document/widget/PagesView$w;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/document/widget/PagesView$v;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public SaveToStream(Ljava/io/OutputStream;Ljava/lang/String;Lcom/intsig/document/widget/PagesView$w;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/document/widget/PagesView$j;

    .line 4
    .line 5
    invoke-direct {v1, p0, p1, p2, p3}, Lcom/intsig/document/widget/PagesView$j;-><init>(Lcom/intsig/document/widget/PagesView;Ljava/io/OutputStream;Ljava/lang/String;Lcom/intsig/document/widget/PagesView$w;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/document/widget/PagesView$v;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public ScrollTo(ILandroid/graphics/RectF;Z)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    iget-object v3, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/document/widget/PagesView$s;

    iget-object v3, v3, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    invoke-virtual {v3}, Landroid/util/Size;->getHeight()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    iget p2, p2, Landroid/graphics/RectF;->top:F

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    iget v1, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v2, v2, v1

    add-int/lit8 p1, p1, 0x1

    int-to-float p1, p1

    iget v3, p0, Lcom/intsig/document/widget/PagesView;->i:F

    mul-float p1, p1, v3

    add-float/2addr p1, v2

    mul-float p2, p2, v1

    add-float/2addr p2, p1

    iget p1, p0, Lcom/intsig/document/widget/PagesView;->w:F

    sub-float/2addr p2, p1

    if-nez p3, :cond_2

    invoke-virtual {p0, v0, p2}, Lcom/intsig/document/widget/PagesView;->a(FF)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto :goto_2

    :cond_2
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    new-instance v1, Lcom/intsig/document/widget/PagesView$e;

    invoke-direct {v1, p0, p2, v0}, Lcom/intsig/document/widget/PagesView$e;-><init>(Lcom/intsig/document/widget/PagesView;FLjava/util/Timer;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1e

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    :cond_3
    :goto_2
    return-void
.end method

.method public ScrollTo(IZ)V
    .locals 2

    .line 2
    new-instance v0, Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0, p1, v0, p2}, Lcom/intsig/document/widget/PagesView;->ScrollTo(ILandroid/graphics/RectF;Z)V

    return-void
.end method

.method public Search(Ljava/lang/String;Lcom/intsig/document/widget/PagesView$x;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/document/widget/PagesView$b;

    .line 4
    .line 5
    invoke-direct {v1, p0, p1, p2}, Lcom/intsig/document/widget/PagesView$b;-><init>(Lcom/intsig/document/widget/PagesView;Ljava/lang/String;Lcom/intsig/document/widget/PagesView$x;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/document/widget/PagesView$v;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public final a(Landroid/graphics/PointF;)I
    .locals 8

    .line 1
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lcom/intsig/document/widget/PagesView;->w:F

    neg-float v2, v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    iget v5, p0, Lcom/intsig/document/widget/PagesView;->l:I

    if-ge v4, v5, :cond_1

    iget-object v5, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/intsig/document/widget/PagesView$s;

    iget-object v5, v5, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    iget v6, p0, Lcom/intsig/document/widget/PagesView;->i:F

    add-float/2addr v2, v6

    cmpl-float v6, v1, v2

    if-lez v6, :cond_0

    invoke-virtual {v5}, Landroid/util/Size;->getHeight()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v6, v6, v7

    add-float/2addr v6, v2

    cmpg-float v6, v1, v6

    if-gtz v6, :cond_0

    iget v3, p0, Lcom/intsig/document/widget/PagesView;->x:F

    invoke-virtual {v5}, Landroid/util/Size;->getWidth()I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v5, v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float/2addr v3, v5

    sub-float/2addr v0, v3

    sub-float/2addr v1, v2

    move v3, v4

    goto :goto_1

    :cond_0
    invoke-virtual {v5}, Landroid/util/Size;->getHeight()I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v5, v5, v6

    add-float/2addr v2, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    iget v2, p0, Lcom/intsig/document/widget/PagesView;->u:F

    div-float/2addr v0, v2

    iput v0, p1, Landroid/graphics/PointF;->x:F

    div-float/2addr v1, v2

    iput v1, p1, Landroid/graphics/PointF;->y:F

    return v3
.end method

.method public final a(Lcom/intsig/document/widget/PagesView$AnnotBox;FFZ)I
    .locals 11

    .line 2
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p2, p3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/PointF;)I

    iget p2, p0, Lcom/intsig/document/widget/PagesView;->G:F

    const/high16 p3, 0x41200000    # 10.0f

    mul-float p2, p2, p3

    const/4 v1, 0x6

    new-array v1, v1, [F

    iget-object v2, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->right:F

    const/4 v4, 0x0

    aput v3, v1, v4

    iget v3, v2, Landroid/graphics/RectF;->bottom:F

    const/4 v5, 0x1

    aput v3, v1, v5

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->width()F

    move-result v3

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v3, v6

    add-float/2addr v3, v2

    const/4 v2, 0x2

    aput v3, v1, v2

    iget-object v3, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    iget v6, v3, Landroid/graphics/RectF;->top:F

    iget v7, p0, Lcom/intsig/document/widget/PagesView;->H:F

    sub-float v7, v6, v7

    sub-float/2addr v7, p3

    const/4 p3, 0x3

    aput v7, v1, p3

    iget v3, v3, Landroid/graphics/RectF;->left:F

    const/4 v7, 0x4

    aput v3, v1, v7

    const/4 v3, 0x5

    aput v6, v1, v3

    invoke-virtual {p1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->isRotateable()Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    iget v8, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    int-to-float v8, v8

    iget-object v9, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    invoke-virtual {v9}, Landroid/graphics/RectF;->centerX()F

    move-result v9

    iget-object v10, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    invoke-virtual {v10}, Landroid/graphics/RectF;->centerY()F

    move-result v10

    invoke-virtual {v6, v8, v9, v10}, Landroid/graphics/Matrix;->setRotate(FFF)V

    invoke-virtual {v6, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    :cond_0
    aget v4, v1, v4

    aget v6, v1, v5

    invoke-virtual {p0, v0, v4, v6, p2}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/PointF;FFF)Z

    move-result v4

    if-eqz v4, :cond_1

    return v7

    :cond_1
    if-eqz p4, :cond_2

    aget p4, v1, v7

    aget v4, v1, v3

    invoke-virtual {p0, v0, p4, v4, p2}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/PointF;FFF)Z

    move-result p4

    if-eqz p4, :cond_2

    return p3

    :cond_2
    iget-object p4, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget v6, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p4, v4, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result p4

    if-eqz p4, :cond_3

    return v2

    :cond_3
    iget-boolean p1, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    if-eqz p1, :cond_4

    aget p1, v1, v2

    aget p3, v1, p3

    invoke-virtual {p0, v0, p1, p3, p2}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/PointF;FFF)Z

    move-result p1

    if-eqz p1, :cond_4

    return v3

    :cond_4
    return v5
.end method

.method public final a(Lcom/intsig/document/widget/PagesView$ElementData;Z)I
    .locals 2

    .line 3
    iget-object v0, p1, Lcom/intsig/document/widget/PagesView$ElementData;->args:Lcom/intsig/document/widget/PagesView$BaseArgs;

    instance-of v1, v0, Lcom/intsig/document/widget/PagesView$ImageArgs;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/intsig/document/widget/PagesView$ImageArgs;

    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    invoke-virtual {p0, v0, p1, p2}, Lcom/intsig/document/widget/PagesView;->doneInsertImage(Lcom/intsig/document/widget/PagesView$ImageArgs;Lcom/intsig/document/widget/PagesView$AnnotBox;Z)I

    move-result p1

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lcom/intsig/document/widget/PagesView$StampArgs;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/intsig/document/widget/PagesView$StampArgs;

    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    invoke-virtual {p0, v0, p1, p2}, Lcom/intsig/document/widget/PagesView;->doneStampAnnot(Lcom/intsig/document/widget/PagesView$StampArgs;Lcom/intsig/document/widget/PagesView$AnnotBox;Z)I

    move-result p1

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lcom/intsig/document/widget/PagesView$TextArgs;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/intsig/document/widget/PagesView$TextArgs;

    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    invoke-virtual {p0, v0, p1, p2}, Lcom/intsig/document/widget/PagesView;->doneTextStampAnnot(Lcom/intsig/document/widget/PagesView$TextArgs;Lcom/intsig/document/widget/PagesView$AnnotBox;Z)I

    move-result p1

    goto :goto_0

    :cond_2
    instance-of v1, v0, Lcom/intsig/document/widget/PagesView$ShapeArgs;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/intsig/document/widget/PagesView$ShapeArgs;

    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    invoke-virtual {p0, v0, p1, p2}, Lcom/intsig/document/widget/PagesView;->doneShapeAnnot(Lcom/intsig/document/widget/PagesView$ShapeArgs;Lcom/intsig/document/widget/PagesView$AnnotBox;Z)I

    move-result p1

    goto :goto_0

    :cond_3
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public final a(ILandroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 5

    .line 4
    iget v0, p2, Landroid/graphics/PointF;->x:F

    iget v1, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v0, v0, v1

    iget p2, p2, Landroid/graphics/PointF;->y:F

    mul-float p2, p2, v1

    iget v1, p0, Lcom/intsig/document/widget/PagesView;->i:F

    iget v2, p0, Lcom/intsig/document/widget/PagesView;->w:F

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    iget-object v3, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/document/widget/PagesView$s;

    iget-object v3, v3, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    iget v4, p0, Lcom/intsig/document/widget/PagesView;->i:F

    add-float/2addr v1, v4

    invoke-virtual {v3}, Landroid/util/Size;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v3, v3, v4

    add-float/2addr v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    add-float/2addr p2, v1

    iget p1, p0, Lcom/intsig/document/widget/PagesView;->z:F

    iget v1, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float p1, p1, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr p1, v1

    iget v1, p0, Lcom/intsig/document/widget/PagesView;->x:F

    sub-float/2addr p1, v1

    sub-float/2addr v0, p1

    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1, v0, p2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object p1
.end method

.method public final a(Lcom/intsig/document/widget/PagesView$TextArgs;F)Landroid/util/Size;
    .locals 3

    if-eqz p1, :cond_0

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iget v1, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->fontSize:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    new-instance v1, Lcom/intsig/document/widget/PagesView$y;

    invoke-direct {v1}, Lcom/intsig/document/widget/PagesView$y;-><init>()V

    iget-object v2, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->text:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, p2}, Lcom/intsig/document/widget/PagesView$y;->〇080(Ljava/lang/String;Landroid/graphics/Paint;F)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/PagesView$TextArgs;->setLayoutText(Ljava/util/List;)V

    .line 21
    iget p1, v1, Lcom/intsig/document/widget/PagesView$y;->〇080:F

    .line 22
    new-instance v0, Landroid/util/Size;

    float-to-int p2, p2

    float-to-int p1, p1

    invoke-direct {v0, p2, p1}, Landroid/util/Size;-><init>(II)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final a(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Lcom/intsig/document/widget/PagesView$r;)Lcom/intsig/document/DocumentLoader;
    .locals 10

    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->E:Ljava/lang/String;

    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    .line 15
    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$v;->o0:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->clear()V

    .line 16
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/4 v7, 0x1

    invoke-direct {v0, v7}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iget-object v8, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    new-instance v9, Lcom/intsig/document/widget/PagesView$d;

    move-object v1, v9

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lcom/intsig/document/widget/PagesView$d;-><init>(Lcom/intsig/document/widget/PagesView;Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Ljava/util/concurrent/BlockingQueue;)V

    invoke-virtual {v8, v9}, Lcom/intsig/document/widget/PagesView$v;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    const/4 p1, 0x0

    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/ArrayBlockingQueue;->take()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_0

    invoke-interface {p4, p2, p1}, Lcom/intsig/document/widget/PagesView$r;->a(II)V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p2

    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 17
    iget-wide v0, v0, Lb/a;->〇080:J

    invoke-static {v0, v1}, Lcom/intsig/document/nativelib/PDFium;->GetPageCount(J)I

    move-result v0

    .line 18
    iput v0, p0, Lcom/intsig/document/widget/PagesView;->l:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->y:F

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->z:F

    const/4 v1, 0x0

    :goto_1
    iget v2, p0, Lcom/intsig/document/widget/PagesView;->l:I

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    invoke-virtual {v2, v1}, Lb/a;->GetPageSize(I)Landroid/util/Size;

    move-result-object v2

    iget v3, p0, Lcom/intsig/document/widget/PagesView;->y:F

    invoke-virtual {v2}, Landroid/util/Size;->getHeight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/intsig/document/widget/PagesView;->y:F

    iget-object v3, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    new-instance v4, Lcom/intsig/document/widget/PagesView$s;

    iget-object v5, p0, Lcom/intsig/document/widget/PagesView;->n0:Lcom/intsig/document/widget/a;

    invoke-direct {v4, p0, v1, v2, v5}, Lcom/intsig/document/widget/PagesView$s;-><init>(Lcom/intsig/document/widget/PagesView;ILandroid/util/Size;Lcom/intsig/document/widget/a;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Landroid/util/Size;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v0, v3

    if-gez v3, :cond_1

    invoke-virtual {v2}, Landroid/util/Size;->getWidth()I

    move-result v0

    int-to-float v0, v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iput v0, p0, Lcom/intsig/document/widget/PagesView;->z:F

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "GetPageSize  "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p3, "ms "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p3, p0, Lcom/intsig/document/widget/PagesView;->l:I

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iput-boolean v7, p0, Lcom/intsig/document/widget/PagesView;->t:Z

    iget-object p2, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    if-lez p2, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p2

    if-lez p2, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p2

    div-int/lit8 p2, p2, 0x2

    int-to-float p2, p2

    iput p2, p0, Lcom/intsig/document/widget/PagesView;->x:F

    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p2

    if-lez p2, :cond_4

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    invoke-virtual {p0, p2}, Lcom/intsig/document/widget/PagesView;->a(I)V

    :cond_4
    iget p2, p0, Lcom/intsig/document/widget/PagesView;->l:I

    invoke-interface {p4, p1, p2}, Lcom/intsig/document/widget/PagesView$r;->a(II)V

    iput-object p4, p0, Lcom/intsig/document/widget/PagesView;->m0:Lcom/intsig/document/widget/PagesView$r;

    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    return-object p1
.end method

.method public final a(F)Lcom/intsig/document/widget/PagesView$m;
    .locals 6

    .line 5
    iget v0, p0, Lcom/intsig/document/widget/PagesView;->w:F

    neg-float v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    iget v3, p0, Lcom/intsig/document/widget/PagesView;->l:I

    if-ge v2, v3, :cond_1

    iget-object v3, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/document/widget/PagesView$s;

    iget-object v3, v3, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    iget v4, p0, Lcom/intsig/document/widget/PagesView;->i:F

    add-float/2addr v0, v4

    cmpl-float v4, p1, v0

    if-lez v4, :cond_0

    invoke-virtual {v3}, Landroid/util/Size;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v4, v4, v5

    add-float/2addr v4, v0

    cmpg-float v4, p1, v4

    if-gtz v4, :cond_0

    iget p1, p0, Lcom/intsig/document/widget/PagesView;->x:F

    invoke-virtual {v3}, Landroid/util/Size;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v3, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v1, v1, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    sub-float/2addr p1, v1

    move v1, v2

    goto :goto_1

    :cond_0
    invoke-virtual {v3}, Landroid/util/Size;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v3, v3, v4

    add-float/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    const/4 p1, 0x0

    :goto_1
    new-instance v2, Lcom/intsig/document/widget/PagesView$m;

    invoke-direct {v2, p1, v0, v1}, Lcom/intsig/document/widget/PagesView$m;-><init>(FFI)V

    return-object v2
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 13
    iget-object v1, v0, Lcom/intsig/document/widget/PagesView$a0;->〇080:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$a0;->〇o〇:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x1

    .line 14
    iput v0, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final a(I)V
    .locals 6

    iget v0, p0, Lcom/intsig/document/widget/PagesView;->S:I

    sub-int v0, p1, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/intsig/document/widget/PagesView;->z:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->v:F

    iget v1, p0, Lcom/intsig/document/widget/PagesView;->u:F

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-double v1, v1

    const-wide v3, 0x3fb999999999999aL    # 0.1

    cmpl-double v5, v1, v3

    if-lez v5, :cond_0

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->u:F

    :cond_0
    iget v0, p0, Lcom/intsig/document/widget/PagesView;->k:F

    iget v1, p0, Lcom/intsig/document/widget/PagesView;->u:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->k:F

    :cond_1
    const-string v0, "calcuteInitState "

    .line 19
    invoke-static {v0}, La/a;->〇080(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 20
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    div-int/lit8 p1, p1, 0x2

    int-to-float p1, p1

    iput p1, p0, Lcom/intsig/document/widget/PagesView;->x:F

    const/4 p1, 0x0

    iput p1, p0, Lcom/intsig/document/widget/PagesView;->w:F

    return-void
.end method

.method public final a(IFLandroid/graphics/RectF;Z)V
    .locals 8

    .line 6
    if-ltz p1, :cond_2

    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/document/widget/PagesView$s;

    iget-boolean v0, v0, Lcom/intsig/document/widget/PagesView$s;->o〇0:Z

    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestRenderPage "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " rect "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    new-instance v7, Lcom/intsig/document/widget/PagesView$u;

    move-object v1, v7

    move-object v2, p0

    move v3, p1

    move v4, p4

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/intsig/document/widget/PagesView$u;-><init>(Lcom/intsig/document/widget/PagesView;IZFLandroid/graphics/RectF;)V

    invoke-virtual {v0, v7}, Lcom/intsig/document/widget/PagesView$v;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 5

    .line 7
    new-instance v0, Landroidx/core/view/GestureDetectorCompat;

    invoke-direct {v0, p1, p0}, Landroidx/core/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->c:Landroidx/core/view/GestureDetectorCompat;

    invoke-virtual {v0, p0}, Landroidx/core/view/GestureDetectorCompat;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->d:Landroid/view/ScaleGestureDetector;

    new-instance v0, Lcom/intsig/document/widget/PagesView$v;

    invoke-direct {v0, p0}, Lcom/intsig/document/widget/PagesView$v;-><init>(Lcom/intsig/document/widget/PagesView;)V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->f:Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/intsig/document/R$drawable;->ic_rotate_bar:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->J:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/intsig/document/R$drawable;->ic_delete_bar:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->K:Landroid/graphics/drawable/Drawable;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "event"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->h:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->g:Landroid/os/Handler;

    invoke-virtual {p0, p0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    new-instance v0, Lb/a;

    invoke-direct {v0, p1}, Lb/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/View;->setVerticalScrollBarEnabled(Z)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setHorizontalScrollBarEnabled(Z)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/intsig/document/R$dimen;->one_dp:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->G:F

    const/high16 v2, 0x41200000    # 10.0f

    mul-float v2, v2, v1

    iput v2, p0, Lcom/intsig/document/widget/PagesView;->i:F

    const/high16 v2, 0x41a00000    # 20.0f

    mul-float v1, v1, v2

    iput v1, p0, Lcom/intsig/document/widget/PagesView;->H:F

    const-string v1, "activity"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/ActivityManager;

    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    invoke-virtual {p1, v1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    iget-wide v1, v1, Landroid/app/ActivityManager$MemoryInfo;->totalMem:J

    const-wide/32 v3, 0x100000

    div-long/2addr v1, v3

    long-to-int p1, v1

    div-int/lit8 p1, p1, 0xa

    const/16 v1, 0x64

    if-ge p1, v1, :cond_0

    const/16 p1, 0x64

    :cond_0
    new-instance v1, Lcom/intsig/document/widget/a;

    invoke-direct {v1, p1}, Lcom/intsig/document/widget/a;-><init>(I)V

    iput-object v1, p0, Lcom/intsig/document/widget/PagesView;->n0:Lcom/intsig/document/widget/a;

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method public final a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFZ)V
    .locals 16

    .line 8
    move-object/from16 v0, p2

    move/from16 v9, p3

    move/from16 v1, p4

    new-instance v10, Landroid/graphics/Path;

    invoke-direct {v10}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v11, p0

    iget v2, v11, Lcom/intsig/document/widget/PagesView;->u:F

    const/high16 v3, 0x3f800000    # 1.0f

    div-float v12, v3, v2

    const/high16 v2, 0x41a00000    # 20.0f

    mul-float v2, v2, v12

    const/high16 v3, 0x41f00000    # 30.0f

    mul-float v3, v3, v12

    const/high16 v13, 0x40000000    # 2.0f

    mul-float v4, v3, v13

    invoke-virtual {v10, v9, v1}, Landroid/graphics/Path;->moveTo(FF)V

    add-float v14, v1, v2

    add-float/2addr v3, v14

    invoke-virtual {v10, v9, v3}, Landroid/graphics/Path;->lineTo(FF)V

    if-eqz p5, :cond_0

    sub-float v2, v9, v4

    add-float v5, v14, v4

    const/4 v6, 0x0

    const/high16 v7, 0x43870000    # 270.0f

    const/4 v8, 0x0

    move-object v1, v10

    move v3, v14

    move/from16 v4, p3

    goto :goto_0

    :cond_0
    add-float v5, v9, v4

    add-float v6, v14, v4

    const/high16 v7, -0x3ccc0000    # -180.0f

    const/high16 v8, -0x3c790000    # -270.0f

    const/4 v15, 0x0

    move-object v1, v10

    move/from16 v2, p3

    move v3, v14

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v15

    :goto_0
    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Path;->arcTo(FFFFFFZ)V

    invoke-virtual {v10, v9, v14}, Landroid/graphics/Path;->lineTo(FF)V

    mul-float v12, v12, v13

    invoke-virtual {v0, v12}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const v1, -0x33119e00

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v1, p1

    invoke-virtual {v1, v10, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/graphics/PointF;)V
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->w0:Lcom/intsig/document/widget/PagesView$z;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/intsig/document/widget/PagesView$z;->onTextSelected(Ljava/lang/String;Landroid/graphics/PointF;)V

    :cond_0
    return-void
.end method

.method public final a(FF)Z
    .locals 13

    .line 10
    iget v0, p0, Lcom/intsig/document/widget/PagesView;->w:F

    add-float/2addr v0, p2

    const/4 p2, 0x1

    const/4 v1, 0x0

    cmpg-float v2, v0, v1

    if-gez v2, :cond_0

    goto :goto_0

    :cond_0
    iget v2, p0, Lcom/intsig/document/widget/PagesView;->y:F

    iget v3, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v2, v2, v3

    iget v3, p0, Lcom/intsig/document/widget/PagesView;->l:I

    add-int/2addr v3, p2

    int-to-float v3, v3

    iget v4, p0, Lcom/intsig/document/widget/PagesView;->i:F

    mul-float v3, v3, v4

    add-float/2addr v3, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v4, v3, v2

    if-gez v4, :cond_1

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    add-float v4, v0, v2

    cmpl-float v4, v4, v3

    if-ltz v4, :cond_2

    sub-float v0, v3, v2

    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/intsig/document/widget/PagesView;->z:F

    iget v4, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v3, v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    cmpg-float v5, v3, v2

    if-gez v5, :cond_3

    div-float/2addr v2, v4

    goto :goto_2

    :cond_3
    iget v2, p0, Lcom/intsig/document/widget/PagesView;->x:F

    add-float/2addr v2, p1

    div-float/2addr v3, v4

    sub-float p1, v2, v3

    iget v5, p0, Lcom/intsig/document/widget/PagesView;->i:F

    cmpl-float p1, p1, v5

    if-lez p1, :cond_4

    add-float/2addr v5, v3

    move v2, v5

    :cond_4
    add-float p1, v2, v3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lcom/intsig/document/widget/PagesView;->i:F

    sub-float/2addr v5, v6

    cmpg-float p1, p1, v5

    if-gez p1, :cond_5

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    int-to-float p1, p1

    sub-float/2addr p1, v3

    iget v2, p0, Lcom/intsig/document/widget/PagesView;->i:F

    sub-float v2, p1, v2

    :cond_5
    :goto_2
    iget p1, p0, Lcom/intsig/document/widget/PagesView;->w:F

    const/4 v3, 0x0

    cmpl-float p1, v0, p1

    if-nez p1, :cond_7

    iget p1, p0, Lcom/intsig/document/widget/PagesView;->x:F

    cmpl-float p1, v2, p1

    if-eqz p1, :cond_6

    goto :goto_3

    :cond_6
    return v3

    :cond_7
    :goto_3
    iput v2, p0, Lcom/intsig/document/widget/PagesView;->x:F

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->w:F

    iget p1, p0, Lcom/intsig/document/widget/PagesView;->m:I

    iget v0, p0, Lcom/intsig/document/widget/PagesView;->n:I

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget v5, p0, Lcom/intsig/document/widget/PagesView;->w:F

    neg-float v5, v5

    const/4 v6, -0x1

    const/4 v7, 0x0

    const/4 v8, -0x1

    :goto_4
    iget v9, p0, Lcom/intsig/document/widget/PagesView;->l:I

    if-ge v7, v9, :cond_d

    iget-object v9, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/intsig/document/widget/PagesView$s;

    iget-object v9, v9, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    invoke-virtual {v9}, Landroid/util/Size;->getWidth()I

    invoke-virtual {v9}, Landroid/util/Size;->getHeight()I

    move-result v9

    int-to-float v9, v9

    iget v10, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v9, v9, v10

    iget v10, p0, Lcom/intsig/document/widget/PagesView;->i:F

    add-float/2addr v5, v10

    cmpg-float v10, v5, v2

    if-gtz v10, :cond_8

    add-float v10, v5, v9

    cmpl-float v10, v10, v1

    if-ltz v10, :cond_8

    const/4 v10, 0x1

    goto :goto_5

    :cond_8
    const/4 v10, 0x0

    :goto_5
    div-float v11, v2, v4

    cmpl-float v12, v11, v5

    if-lez v12, :cond_9

    add-float v12, v5, v9

    cmpg-float v11, v11, v12

    if-gez v11, :cond_9

    iput v7, p0, Lcom/intsig/document/widget/PagesView;->o:I

    :cond_9
    if-eqz v10, :cond_b

    if-ne v8, v6, :cond_a

    move p1, v7

    move v8, p1

    :cond_a
    move v0, v7

    goto :goto_6

    :cond_b
    if-ltz v8, :cond_c

    if-le v7, p1, :cond_c

    goto :goto_7

    :cond_c
    :goto_6
    add-float/2addr v5, v9

    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_d
    :goto_7
    iget v1, p0, Lcom/intsig/document/widget/PagesView;->m:I

    if-ne p1, v1, :cond_e

    iget v1, p0, Lcom/intsig/document/widget/PagesView;->n:I

    if-eq v0, v1, :cond_f

    :cond_e
    iput p1, p0, Lcom/intsig/document/widget/PagesView;->m:I

    iput v0, p0, Lcom/intsig/document/widget/PagesView;->n:I

    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->m0:Lcom/intsig/document/widget/PagesView$r;

    invoke-interface {v1, p1, v0}, Lcom/intsig/document/widget/PagesView$r;->onPageScroll(II)V

    :cond_f
    return p2
.end method

.method public final a(FFF)Z
    .locals 4

    iget v0, p0, Lcom/intsig/document/widget/PagesView;->u:F

    iget v1, p0, Lcom/intsig/document/widget/PagesView;->z:F

    mul-float v0, v0, v1

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p2

    div-int/lit8 p2, p2, 0x2

    int-to-float p2, p2

    :cond_0
    iget v0, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float p1, p1, v0

    iget v1, p0, Lcom/intsig/document/widget/PagesView;->j:F

    cmpl-float v2, p1, v1

    if-lez v2, :cond_1

    :goto_0
    move p1, v1

    goto :goto_1

    :cond_1
    iget v1, p0, Lcom/intsig/document/widget/PagesView;->k:F

    cmpg-float v2, p1, v1

    if-gez v2, :cond_2

    goto :goto_0

    :cond_2
    :goto_1
    cmpl-float v0, p1, v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p2, p3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/PointF;)I

    move-result v0

    .line 23
    iget v1, p0, Lcom/intsig/document/widget/PagesView;->w:F

    add-float/2addr p3, v1

    add-int/lit8 v1, v0, 0x1

    int-to-float v1, v1

    .line 24
    iget v2, p0, Lcom/intsig/document/widget/PagesView;->i:F

    mul-float v1, v1, v2

    sub-float v2, p3, v1

    mul-float v2, v2, p1

    iget v3, p0, Lcom/intsig/document/widget/PagesView;->u:F

    div-float/2addr v2, v3

    add-float/2addr v2, v1

    sub-float/2addr v2, p3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Damn onScale "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v3, "   "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " py:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p3, "  "

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p3, p0, Lcom/intsig/document/widget/PagesView;->u:F

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    iget p3, p0, Lcom/intsig/document/widget/PagesView;->x:F

    sub-float/2addr p3, p2

    iget p2, p0, Lcom/intsig/document/widget/PagesView;->u:F

    sub-float v0, p1, p2

    mul-float v0, v0, p3

    div-float/2addr v0, p2

    invoke-virtual {p0, v0, v2}, Lcom/intsig/document/widget/PagesView;->a(FF)Z

    iput p1, p0, Lcom/intsig/document/widget/PagesView;->u:F

    const/4 p1, 0x1

    return p1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method public final a(Landroid/graphics/PointF;FFF)Z
    .locals 1

    .line 11
    iget v0, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, p2

    mul-float v0, v0, v0

    iget p1, p1, Landroid/graphics/PointF;->y:F

    sub-float/2addr p1, p3

    mul-float p1, p1, p1

    add-float/2addr p1, v0

    float-to-double p1, p1

    invoke-static {p1, p2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p1

    float-to-double p3, p4

    cmpg-double v0, p1, p3

    if-gez v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final a(Lcom/intsig/document/widget/PagesView$s;)Z
    .locals 2

    .line 12
    iget-object v0, p1, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    invoke-virtual {v0}, Landroid/util/Size;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    invoke-virtual {p1}, Landroid/util/Size;->getHeight()I

    move-result p1

    int-to-float p1, p1

    iget v1, p0, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v0, v0, v1

    mul-float p1, p1, v1

    mul-float p1, p1, v0

    const/high16 v0, 0x4ba00000    # 2.097152E7f

    cmpl-float p1, p1, v0

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final b(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/document/widget/PagesView$s;

    .line 8
    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/document/widget/PagesView;->a(Lcom/intsig/document/widget/PagesView$s;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    iget v1, p0, Lcom/intsig/document/widget/PagesView;->u:F

    .line 17
    .line 18
    iget-object v3, v0, Lcom/intsig/document/widget/PagesView$s;->〇〇888:Landroid/graphics/RectF;

    .line 19
    .line 20
    invoke-virtual {p0, p1, v1, v3, v2}, Lcom/intsig/document/widget/PagesView;->a(IFLandroid/graphics/RectF;Z)V

    .line 21
    .line 22
    .line 23
    const/4 p1, 0x0

    .line 24
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView$s;->〇o00〇〇Oo(Z)V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    .line 29
    .line 30
    const/4 v3, 0x0

    .line 31
    invoke-virtual {p0, p1, v1, v3, v2}, Lcom/intsig/document/widget/PagesView;->a(IFLandroid/graphics/RectF;Z)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v2}, Lcom/intsig/document/widget/PagesView$s;->〇o00〇〇Oo(Z)V

    .line 35
    .line 36
    .line 37
    :goto_0
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public beginImageWatermark(Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->e0:Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;

    .line 2
    .line 3
    const/4 p1, 0x1

    .line 4
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->y0:Z

    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public beginInkAnnot(Lcom/intsig/document/widget/PagesView$InkArgs;)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->V:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->W:Z

    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->f0:Lcom/intsig/document/widget/PagesView$InkArgs;

    .line 7
    .line 8
    const/4 p1, 0x1

    .line 9
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->V:Z

    .line 10
    .line 11
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->U:Z

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public beginInsertImage(Lcom/intsig/document/widget/PagesView$ImageArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 10

    .line 1
    iget-object v0, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->bmp:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_5

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-float v0, v0

    .line 10
    iget-object v1, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->bmp:Landroid/graphics/Bitmap;

    .line 11
    .line 12
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    int-to-float v1, v1

    .line 17
    iget v2, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->initialSize:F

    .line 18
    .line 19
    const/4 v3, 0x0

    .line 20
    cmpl-float v4, v2, v3

    .line 21
    .line 22
    if-lez v4, :cond_1

    .line 23
    .line 24
    float-to-double v4, v2

    .line 25
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 26
    .line 27
    cmpg-double v8, v4, v6

    .line 28
    .line 29
    if-gez v8, :cond_0

    .line 30
    .line 31
    iget-object v4, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 32
    .line 33
    iget v5, p0, Lcom/intsig/document/widget/PagesView;->o:I

    .line 34
    .line 35
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    check-cast v4, Lcom/intsig/document/widget/PagesView$s;

    .line 40
    .line 41
    iget-object v4, v4, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    .line 42
    .line 43
    invoke-virtual {v4}, Landroid/util/Size;->getWidth()I

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    int-to-float v4, v4

    .line 48
    mul-float v2, v2, v4

    .line 49
    .line 50
    cmpl-float v4, v0, v2

    .line 51
    .line 52
    if-lez v4, :cond_1

    .line 53
    .line 54
    :cond_0
    iget-object v0, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->bmp:Landroid/graphics/Bitmap;

    .line 55
    .line 56
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    int-to-float v0, v0

    .line 61
    mul-float v0, v0, v2

    .line 62
    .line 63
    iget-object v1, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->bmp:Landroid/graphics/Bitmap;

    .line 64
    .line 65
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    int-to-float v1, v1

    .line 70
    div-float v1, v0, v1

    .line 71
    .line 72
    move v0, v2

    .line 73
    :cond_1
    new-instance v2, Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 74
    .line 75
    invoke-direct {v2}, Lcom/intsig/document/widget/PagesView$AnnotBox;-><init>()V

    .line 76
    .line 77
    .line 78
    iget-boolean v4, p1, Lcom/intsig/document/widget/PagesView$BaseArgs;->fixedRatio:Z

    .line 79
    .line 80
    invoke-virtual {v2, v0, v1, v4}, Lcom/intsig/document/widget/PagesView$AnnotBox;->reset(FFZ)V

    .line 81
    .line 82
    .line 83
    iget-boolean v0, p1, Lcom/intsig/document/widget/PagesView$BaseArgs;->fixedSize:Z

    .line 84
    .line 85
    iput-boolean v0, v2, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixSize:Z

    .line 86
    .line 87
    iget-boolean v0, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->rotatable:Z

    .line 88
    .line 89
    iput-boolean v0, v2, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    .line 90
    .line 91
    new-instance v0, Lcom/intsig/document/widget/PagesView$ElementData;

    .line 92
    .line 93
    const/4 v1, 0x1

    .line 94
    invoke-direct {v0, v1, v2, p1}, Lcom/intsig/document/widget/PagesView$ElementData;-><init>(ZLcom/intsig/document/widget/PagesView$AnnotBox;Lcom/intsig/document/widget/PagesView$BaseArgs;)V

    .line 95
    .line 96
    .line 97
    iget-object v4, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 98
    .line 99
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    .line 101
    .line 102
    if-eqz p2, :cond_4

    .line 103
    .line 104
    iget p2, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->left:I

    .line 105
    .line 106
    int-to-float p2, p2

    .line 107
    iget p1, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->top:I

    .line 108
    .line 109
    int-to-float p1, p1

    .line 110
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 111
    .line 112
    iget v4, p0, Lcom/intsig/document/widget/PagesView;->o:I

    .line 113
    .line 114
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 115
    .line 116
    .line 117
    move-result-object v1

    .line 118
    check-cast v1, Lcom/intsig/document/widget/PagesView$s;

    .line 119
    .line 120
    iget-object v1, v1, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    .line 121
    .line 122
    cmpl-float v4, p2, v3

    .line 123
    .line 124
    if-ltz v4, :cond_2

    .line 125
    .line 126
    invoke-virtual {v1}, Landroid/util/Size;->getWidth()I

    .line 127
    .line 128
    .line 129
    move-result v4

    .line 130
    int-to-float v4, v4

    .line 131
    cmpg-float v4, p2, v4

    .line 132
    .line 133
    if-gez v4, :cond_2

    .line 134
    .line 135
    cmpl-float v3, p1, v3

    .line 136
    .line 137
    if-ltz v3, :cond_2

    .line 138
    .line 139
    invoke-virtual {v1}, Landroid/util/Size;->getHeight()I

    .line 140
    .line 141
    .line 142
    move-result v3

    .line 143
    int-to-float v3, v3

    .line 144
    cmpg-float v3, p1, v3

    .line 145
    .line 146
    if-ltz v3, :cond_3

    .line 147
    .line 148
    :cond_2
    invoke-virtual {v1}, Landroid/util/Size;->getWidth()I

    .line 149
    .line 150
    .line 151
    move-result p1

    .line 152
    int-to-float p1, p1

    .line 153
    invoke-virtual {v2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->width()F

    .line 154
    .line 155
    .line 156
    move-result p2

    .line 157
    sub-float/2addr p1, p2

    .line 158
    const/high16 p2, 0x40000000    # 2.0f

    .line 159
    .line 160
    div-float/2addr p1, p2

    .line 161
    invoke-virtual {v1}, Landroid/util/Size;->getHeight()I

    .line 162
    .line 163
    .line 164
    move-result v1

    .line 165
    int-to-float v1, v1

    .line 166
    invoke-virtual {v2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->height()F

    .line 167
    .line 168
    .line 169
    move-result v3

    .line 170
    sub-float/2addr v1, v3

    .line 171
    div-float p2, v1, p2

    .line 172
    .line 173
    move v9, p2

    .line 174
    move p2, p1

    .line 175
    move p1, v9

    .line 176
    :cond_3
    iget v1, p0, Lcom/intsig/document/widget/PagesView;->o:I

    .line 177
    .line 178
    invoke-virtual {v2, v1, p2, p1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->place(IFF)V

    .line 179
    .line 180
    .line 181
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 182
    .line 183
    .line 184
    goto :goto_0

    .line 185
    :cond_4
    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    .line 186
    .line 187
    :goto_0
    return-object v0

    .line 188
    :cond_5
    new-instance p1, Ljava/lang/RuntimeException;

    .line 189
    .line 190
    const-string p2, "Bitmap in ImageArgs should not be null"

    .line 191
    .line 192
    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    throw p1
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public beginShapeAnnot(Lcom/intsig/document/widget/PagesView$ShapeArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/document/widget/PagesView$AnnotBox;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-boolean v1, p1, Lcom/intsig/document/widget/PagesView$BaseArgs;->fixedRatio:Z

    .line 7
    .line 8
    const/high16 v2, 0x42c80000    # 100.0f

    .line 9
    .line 10
    invoke-virtual {v0, v2, v2, v1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->reset(FFZ)V

    .line 11
    .line 12
    .line 13
    new-instance v1, Lcom/intsig/document/widget/PagesView$ElementData;

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-direct {v1, v2, v0, p1}, Lcom/intsig/document/widget/PagesView$ElementData;-><init>(ZLcom/intsig/document/widget/PagesView$AnnotBox;Lcom/intsig/document/widget/PagesView$BaseArgs;)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    if-eqz p2, :cond_0

    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 27
    .line 28
    iget p2, p0, Lcom/intsig/document/widget/PagesView;->o:I

    .line 29
    .line 30
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    check-cast p1, Lcom/intsig/document/widget/PagesView$s;

    .line 35
    .line 36
    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/util/Size;->getWidth()I

    .line 39
    .line 40
    .line 41
    move-result p2

    .line 42
    int-to-float p2, p2

    .line 43
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$AnnotBox;->width()F

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    sub-float/2addr p2, v2

    .line 48
    const/high16 v2, 0x40000000    # 2.0f

    .line 49
    .line 50
    div-float/2addr p2, v2

    .line 51
    invoke-virtual {p1}, Landroid/util/Size;->getHeight()I

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    int-to-float p1, p1

    .line 56
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$AnnotBox;->height()F

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    sub-float/2addr p1, v3

    .line 61
    div-float/2addr p1, v2

    .line 62
    iget v2, p0, Lcom/intsig/document/widget/PagesView;->o:I

    .line 63
    .line 64
    invoke-virtual {v0, v2, p2, p1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->place(IFF)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 68
    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_0
    iput-boolean v2, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    .line 72
    .line 73
    :goto_0
    return-object v1
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public beginStampAnnot(Lcom/intsig/document/widget/PagesView$StampArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 8

    .line 1
    iget-object v0, p1, Lcom/intsig/document/widget/PagesView$StampArgs;->bmp:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    const/high16 v1, 0x42f00000    # 120.0f

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget v2, p1, Lcom/intsig/document/widget/PagesView$StampArgs;->initialSize:F

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    cmpl-float v3, v2, v3

    .line 11
    .line 12
    if-lez v3, :cond_1

    .line 13
    .line 14
    float-to-double v3, v2

    .line 15
    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    .line 16
    .line 17
    cmpg-double v7, v3, v5

    .line 18
    .line 19
    if-gez v7, :cond_0

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 22
    .line 23
    iget v3, p0, Lcom/intsig/document/widget/PagesView;->o:I

    .line 24
    .line 25
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    check-cast v0, Lcom/intsig/document/widget/PagesView$s;

    .line 30
    .line 31
    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    .line 32
    .line 33
    invoke-virtual {v0}, Landroid/util/Size;->getWidth()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    int-to-float v0, v0

    .line 38
    mul-float v2, v2, v0

    .line 39
    .line 40
    cmpl-float v0, v1, v2

    .line 41
    .line 42
    if-lez v0, :cond_1

    .line 43
    .line 44
    iget-object v0, p1, Lcom/intsig/document/widget/PagesView$StampArgs;->bmp:Landroid/graphics/Bitmap;

    .line 45
    .line 46
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    int-to-float v0, v0

    .line 51
    mul-float v0, v0, v2

    .line 52
    .line 53
    iget-object v1, p1, Lcom/intsig/document/widget/PagesView$StampArgs;->bmp:Landroid/graphics/Bitmap;

    .line 54
    .line 55
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    int-to-float v1, v1

    .line 60
    div-float v1, v0, v1

    .line 61
    .line 62
    move v0, v1

    .line 63
    move v1, v2

    .line 64
    goto :goto_0

    .line 65
    :cond_1
    const/high16 v0, 0x42f00000    # 120.0f

    .line 66
    .line 67
    :goto_0
    new-instance v2, Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 68
    .line 69
    invoke-direct {v2}, Lcom/intsig/document/widget/PagesView$AnnotBox;-><init>()V

    .line 70
    .line 71
    .line 72
    iget-boolean v3, p1, Lcom/intsig/document/widget/PagesView$BaseArgs;->fixedRatio:Z

    .line 73
    .line 74
    invoke-virtual {v2, v1, v0, v3}, Lcom/intsig/document/widget/PagesView$AnnotBox;->reset(FFZ)V

    .line 75
    .line 76
    .line 77
    iget-boolean v0, p1, Lcom/intsig/document/widget/PagesView$BaseArgs;->fixedSize:Z

    .line 78
    .line 79
    iput-boolean v0, v2, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixSize:Z

    .line 80
    .line 81
    iget-boolean v0, p1, Lcom/intsig/document/widget/PagesView$BaseArgs;->fixedRatio:Z

    .line 82
    .line 83
    iput-boolean v0, v2, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixRatio:Z

    .line 84
    .line 85
    iget-boolean v0, p1, Lcom/intsig/document/widget/PagesView$StampArgs;->rotatable:Z

    .line 86
    .line 87
    iput-boolean v0, v2, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    .line 88
    .line 89
    new-instance v0, Lcom/intsig/document/widget/PagesView$ElementData;

    .line 90
    .line 91
    const/4 v1, 0x1

    .line 92
    invoke-direct {v0, v1, v2, p1}, Lcom/intsig/document/widget/PagesView$ElementData;-><init>(ZLcom/intsig/document/widget/PagesView$AnnotBox;Lcom/intsig/document/widget/PagesView$BaseArgs;)V

    .line 93
    .line 94
    .line 95
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 96
    .line 97
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    .line 99
    .line 100
    if-eqz p2, :cond_2

    .line 101
    .line 102
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 103
    .line 104
    iget p2, p0, Lcom/intsig/document/widget/PagesView;->o:I

    .line 105
    .line 106
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 107
    .line 108
    .line 109
    move-result-object p1

    .line 110
    check-cast p1, Lcom/intsig/document/widget/PagesView$s;

    .line 111
    .line 112
    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    .line 113
    .line 114
    invoke-virtual {p1}, Landroid/util/Size;->getWidth()I

    .line 115
    .line 116
    .line 117
    move-result p2

    .line 118
    int-to-float p2, p2

    .line 119
    invoke-virtual {v2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->width()F

    .line 120
    .line 121
    .line 122
    move-result v1

    .line 123
    sub-float/2addr p2, v1

    .line 124
    const/high16 v1, 0x40000000    # 2.0f

    .line 125
    .line 126
    div-float/2addr p2, v1

    .line 127
    invoke-virtual {p1}, Landroid/util/Size;->getHeight()I

    .line 128
    .line 129
    .line 130
    move-result p1

    .line 131
    int-to-float p1, p1

    .line 132
    invoke-virtual {v2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->height()F

    .line 133
    .line 134
    .line 135
    move-result v3

    .line 136
    sub-float/2addr p1, v3

    .line 137
    div-float/2addr p1, v1

    .line 138
    iget v1, p0, Lcom/intsig/document/widget/PagesView;->o:I

    .line 139
    .line 140
    invoke-virtual {v2, v1, p2, p1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->place(IFF)V

    .line 141
    .line 142
    .line 143
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 144
    .line 145
    .line 146
    goto :goto_1

    .line 147
    :cond_2
    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    .line 148
    .line 149
    :goto_1
    return-object v0
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public beginTextStampAnnot(Lcom/intsig/document/widget/PagesView$TextArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;
    .locals 4

    .line 1
    const/high16 v0, 0x43480000    # 200.0f

    .line 2
    .line 3
    invoke-virtual {p0, p1, v0}, Lcom/intsig/document/widget/PagesView;->a(Lcom/intsig/document/widget/PagesView$TextArgs;F)Landroid/util/Size;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 8
    .line 9
    invoke-direct {v1}, Lcom/intsig/document/widget/PagesView$AnnotBox;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/util/Size;->getWidth()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    int-to-float v2, v2

    .line 17
    invoke-virtual {v0}, Landroid/util/Size;->getHeight()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    int-to-float v0, v0

    .line 22
    const/4 v3, 0x0

    .line 23
    invoke-virtual {v1, v2, v0, v3}, Lcom/intsig/document/widget/PagesView$AnnotBox;->reset(FFZ)V

    .line 24
    .line 25
    .line 26
    iget-boolean v0, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->rotatable:Z

    .line 27
    .line 28
    invoke-virtual {v1, v0}, Lcom/intsig/document/widget/PagesView$AnnotBox;->setRotatable(Z)V

    .line 29
    .line 30
    .line 31
    new-instance v0, Lcom/intsig/document/widget/PagesView$ElementData;

    .line 32
    .line 33
    const/4 v2, 0x1

    .line 34
    invoke-direct {v0, v2, v1, p1}, Lcom/intsig/document/widget/PagesView$ElementData;-><init>(ZLcom/intsig/document/widget/PagesView$AnnotBox;Lcom/intsig/document/widget/PagesView$BaseArgs;)V

    .line 35
    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 38
    .line 39
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    if-eqz p2, :cond_0

    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 45
    .line 46
    iget p2, p0, Lcom/intsig/document/widget/PagesView;->o:I

    .line 47
    .line 48
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    check-cast p1, Lcom/intsig/document/widget/PagesView$s;

    .line 53
    .line 54
    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    .line 55
    .line 56
    invoke-virtual {p1}, Landroid/util/Size;->getWidth()I

    .line 57
    .line 58
    .line 59
    move-result p2

    .line 60
    int-to-float p2, p2

    .line 61
    invoke-virtual {v1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->width()F

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    sub-float/2addr p2, v2

    .line 66
    const/high16 v2, 0x40000000    # 2.0f

    .line 67
    .line 68
    div-float/2addr p2, v2

    .line 69
    invoke-virtual {p1}, Landroid/util/Size;->getHeight()I

    .line 70
    .line 71
    .line 72
    move-result p1

    .line 73
    int-to-float p1, p1

    .line 74
    invoke-virtual {v1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->height()F

    .line 75
    .line 76
    .line 77
    move-result v3

    .line 78
    sub-float/2addr p1, v3

    .line 79
    div-float/2addr p1, v2

    .line 80
    iget v2, p0, Lcom/intsig/document/widget/PagesView;->o:I

    .line 81
    .line 82
    invoke-virtual {v1, v2, p2, p1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->place(IFF)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 86
    .line 87
    .line 88
    goto :goto_0

    .line 89
    :cond_0
    iput-boolean v2, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    .line 90
    .line 91
    :goto_0
    return-object v0
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public beginWatermark(Lcom/intsig/document/widget/PagesView$WatermarkArgs;)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->y0:Z

    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final c(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/document/widget/PagesView$f;

    .line 4
    .line 5
    invoke-direct {v1, p0, p1}, Lcom/intsig/document/widget/PagesView$f;-><init>(Lcom/intsig/document/widget/PagesView;I)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/document/widget/PagesView$v;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public doneAllElements(Z)I
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    .line 3
    .line 4
    iget-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->V:Z

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/PagesView;->doneInkAnnot(Z)I

    .line 9
    .line 10
    .line 11
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    if-eqz v3, :cond_1

    .line 27
    .line 28
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    check-cast v3, Lcom/intsig/document/widget/PagesView$ElementData;

    .line 33
    .line 34
    invoke-virtual {p0, v3, p1}, Lcom/intsig/document/widget/PagesView;->a(Lcom/intsig/document/widget/PagesView$ElementData;Z)I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    add-int/lit8 v0, v0, 0x1

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 49
    .line 50
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 51
    .line 52
    .line 53
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    const/4 v1, -0x1

    .line 61
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    if-eqz v2, :cond_3

    .line 66
    .line 67
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    check-cast v2, Ljava/lang/Integer;

    .line 72
    .line 73
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 74
    .line 75
    .line 76
    move-result v2

    .line 77
    if-ne v2, v1, :cond_2

    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_2
    invoke-virtual {p0, v2}, Lcom/intsig/document/widget/PagesView;->b(I)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {p0, v2}, Lcom/intsig/document/widget/PagesView;->c(I)V

    .line 84
    .line 85
    .line 86
    move v1, v2

    .line 87
    goto :goto_1

    .line 88
    :cond_3
    return v0
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public doneElement(Lcom/intsig/document/widget/PagesView$ElementData;Z)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, p1, p2}, Lcom/intsig/document/widget/PagesView;->a(Lcom/intsig/document/widget/PagesView$ElementData;Z)I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    if-ltz p1, :cond_0

    .line 11
    .line 12
    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/PagesView;->b(I)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/PagesView;->c(I)V

    .line 16
    .line 17
    .line 18
    :cond_0
    return p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public doneImageWatermark(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->e0:Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    .line 8
    .line 9
    new-instance v2, Lcom/intsig/document/widget/PagesView$c;

    .line 10
    .line 11
    invoke-direct {v2, p0, v0}, Lcom/intsig/document/widget/PagesView$c;-><init>(Lcom/intsig/document/widget/PagesView;Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v1, v2}, Lcom/intsig/document/widget/PagesView$v;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 19
    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->y0:Z

    .line 22
    .line 23
    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->e0:Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;

    .line 25
    .line 26
    if-nez p1, :cond_1

    .line 27
    .line 28
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 29
    .line 30
    .line 31
    :cond_1
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public doneInkAnnot(Z)I
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, -0x1

    .line 5
    if-eqz p1, :cond_6

    .line 6
    .line 7
    iget-object v3, v0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    .line 8
    .line 9
    invoke-virtual {v3}, Lcom/intsig/document/widget/PagesView$p;->〇080()Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    check-cast v3, Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 16
    .line 17
    .line 18
    move-result v4

    .line 19
    if-lez v4, :cond_6

    .line 20
    .line 21
    new-instance v4, Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 24
    .line 25
    .line 26
    new-instance v5, Landroid/graphics/PathMeasure;

    .line 27
    .line 28
    invoke-direct {v5}, Landroid/graphics/PathMeasure;-><init>()V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    const/4 v6, 0x0

    .line 36
    const/4 v7, 0x1

    .line 37
    const/4 v8, 0x1

    .line 38
    const/4 v9, 0x0

    .line 39
    const/4 v10, 0x0

    .line 40
    const/4 v14, 0x0

    .line 41
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 42
    .line 43
    .line 44
    move-result v11

    .line 45
    if-eqz v11, :cond_4

    .line 46
    .line 47
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object v11

    .line 51
    check-cast v11, Landroid/graphics/Path;

    .line 52
    .line 53
    invoke-virtual {v5, v11, v1}, Landroid/graphics/PathMeasure;->setPath(Landroid/graphics/Path;Z)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v5}, Landroid/graphics/PathMeasure;->getLength()F

    .line 57
    .line 58
    .line 59
    move-result v11

    .line 60
    cmpl-float v12, v11, v6

    .line 61
    .line 62
    if-lez v12, :cond_0

    .line 63
    .line 64
    const/4 v12, 0x2

    .line 65
    new-array v12, v12, [F

    .line 66
    .line 67
    fill-array-data v12, :array_0

    .line 68
    .line 69
    .line 70
    const/high16 v13, 0x41000000    # 8.0f

    .line 71
    .line 72
    cmpg-float v13, v11, v13

    .line 73
    .line 74
    if-gez v13, :cond_1

    .line 75
    .line 76
    const/high16 v13, 0x3f000000    # 0.5f

    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_1
    const/high16 v13, 0x40800000    # 4.0f

    .line 80
    .line 81
    :goto_1
    const/4 v15, 0x0

    .line 82
    :goto_2
    cmpg-float v16, v15, v11

    .line 83
    .line 84
    if-gez v16, :cond_3

    .line 85
    .line 86
    const/4 v6, 0x0

    .line 87
    invoke-virtual {v5, v15, v12, v6}, Landroid/graphics/PathMeasure;->getPosTan(F[F[F)Z

    .line 88
    .line 89
    .line 90
    if-eqz v8, :cond_2

    .line 91
    .line 92
    aget v6, v12, v7

    .line 93
    .line 94
    invoke-virtual {v0, v6}, Lcom/intsig/document/widget/PagesView;->a(F)Lcom/intsig/document/widget/PagesView$m;

    .line 95
    .line 96
    .line 97
    move-result-object v6

    .line 98
    iget v9, v6, Lcom/intsig/document/widget/PagesView$m;->〇080:F

    .line 99
    .line 100
    iget v10, v6, Lcom/intsig/document/widget/PagesView$m;->〇o00〇〇Oo:F

    .line 101
    .line 102
    iget v14, v6, Lcom/intsig/document/widget/PagesView$m;->〇o〇:I

    .line 103
    .line 104
    const/4 v8, 0x0

    .line 105
    :cond_2
    aget v6, v12, v1

    .line 106
    .line 107
    sub-float/2addr v6, v9

    .line 108
    iget v1, v0, Lcom/intsig/document/widget/PagesView;->u:F

    .line 109
    .line 110
    div-float/2addr v6, v1

    .line 111
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    .line 117
    .line 118
    aget v1, v12, v7

    .line 119
    .line 120
    sub-float/2addr v1, v10

    .line 121
    iget v6, v0, Lcom/intsig/document/widget/PagesView;->u:F

    .line 122
    .line 123
    div-float/2addr v1, v6

    .line 124
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 125
    .line 126
    .line 127
    move-result-object v1

    .line 128
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    add-float/2addr v15, v13

    .line 132
    const/4 v1, 0x0

    .line 133
    const/4 v6, 0x0

    .line 134
    goto :goto_2

    .line 135
    :cond_3
    const/high16 v1, -0x40800000    # -1.0f

    .line 136
    .line 137
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 138
    .line 139
    .line 140
    move-result-object v6

    .line 141
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    .line 143
    .line 144
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 145
    .line 146
    .line 147
    move-result-object v1

    .line 148
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    .line 150
    .line 151
    const/4 v1, 0x0

    .line 152
    const/4 v6, 0x0

    .line 153
    goto :goto_0

    .line 154
    :cond_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 155
    .line 156
    .line 157
    move-result v1

    .line 158
    new-array v11, v1, [F

    .line 159
    .line 160
    const/4 v1, 0x0

    .line 161
    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 162
    .line 163
    .line 164
    move-result v3

    .line 165
    if-ge v1, v3, :cond_5

    .line 166
    .line 167
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 168
    .line 169
    .line 170
    move-result-object v3

    .line 171
    check-cast v3, Ljava/lang/Float;

    .line 172
    .line 173
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 174
    .line 175
    .line 176
    move-result v3

    .line 177
    aput v3, v11, v1

    .line 178
    .line 179
    add-int/lit8 v1, v1, 0x1

    .line 180
    .line 181
    goto :goto_3

    .line 182
    :cond_5
    iget-object v1, v0, Lcom/intsig/document/widget/PagesView;->f0:Lcom/intsig/document/widget/PagesView$InkArgs;

    .line 183
    .line 184
    iget v12, v1, Lcom/intsig/document/widget/PagesView$InkArgs;->strokeWidth:F

    .line 185
    .line 186
    iget v13, v1, Lcom/intsig/document/widget/PagesView$InkArgs;->color:I

    .line 187
    .line 188
    iget-object v8, v0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 189
    .line 190
    const/16 v10, 0xf

    .line 191
    .line 192
    move v9, v14

    .line 193
    invoke-virtual/range {v8 .. v13}, Lb/a;->AddAnnotation(II[FFI)I

    .line 194
    .line 195
    .line 196
    invoke-virtual {v0, v14}, Lcom/intsig/document/widget/PagesView;->c(I)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {v0, v14}, Lcom/intsig/document/widget/PagesView;->b(I)V

    .line 200
    .line 201
    .line 202
    iput-boolean v7, v0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 203
    .line 204
    goto :goto_4

    .line 205
    :cond_6
    const/4 v14, -0x1

    .line 206
    :goto_4
    iget-object v1, v0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    .line 207
    .line 208
    iget-object v3, v1, Lcom/intsig/document/widget/PagesView$p;->〇080:Ljava/util/ArrayList;

    .line 209
    .line 210
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 211
    .line 212
    .line 213
    iput v2, v1, Lcom/intsig/document/widget/PagesView$p;->〇o00〇〇Oo:I

    .line 214
    .line 215
    const/4 v1, 0x0

    .line 216
    iput-boolean v1, v0, Lcom/intsig/document/widget/PagesView;->V:Z

    .line 217
    .line 218
    iput-boolean v1, v0, Lcom/intsig/document/widget/PagesView;->U:Z

    .line 219
    .line 220
    if-nez p1, :cond_7

    .line 221
    .line 222
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->postInvalidate()V

    .line 223
    .line 224
    .line 225
    :cond_7
    return v14

    .line 226
    nop

    .line 227
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public doneInsertImage(Lcom/intsig/document/widget/PagesView$ImageArgs;Lcom/intsig/document/widget/PagesView$AnnotBox;Z)I
    .locals 4

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->empty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    if-eqz p3, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 12
    .line 13
    iget v1, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 14
    .line 15
    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->bmp:Landroid/graphics/Bitmap;

    .line 16
    .line 17
    iget-object v2, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 18
    .line 19
    iget v3, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    .line 20
    .line 21
    invoke-virtual {v0, v1, p1, v2, v3}, Lb/a;->InsertImage(ILandroid/graphics/Bitmap;Landroid/graphics/RectF;I)I

    .line 22
    .line 23
    .line 24
    const/4 p1, 0x1

    .line 25
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 26
    .line 27
    iget p1, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const/4 p1, -0x1

    .line 31
    :goto_0
    const/4 p2, 0x0

    .line 32
    iput-boolean p2, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    .line 33
    .line 34
    if-nez p3, :cond_1

    .line 35
    .line 36
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 37
    .line 38
    .line 39
    :cond_1
    return p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public doneShapeAnnot(Lcom/intsig/document/widget/PagesView$ShapeArgs;Lcom/intsig/document/widget/PagesView$AnnotBox;Z)I
    .locals 7

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->empty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    if-eqz p3, :cond_0

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/document/DocumentLoader$a;

    .line 12
    .line 13
    iget v2, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 14
    .line 15
    iget v3, p1, Lcom/intsig/document/widget/PagesView$ShapeArgs;->type:I

    .line 16
    .line 17
    iget-object v4, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 18
    .line 19
    iget v5, p1, Lcom/intsig/document/widget/PagesView$ShapeArgs;->color:I

    .line 20
    .line 21
    iget v6, p1, Lcom/intsig/document/widget/PagesView$ShapeArgs;->strokeWidth:F

    .line 22
    .line 23
    move-object v1, v0

    .line 24
    invoke-direct/range {v1 .. v6}, Lcom/intsig/document/DocumentLoader$a;-><init>(IILandroid/graphics/RectF;IF)V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Lb/a;->AddShapeAnnotation(Lcom/intsig/document/DocumentLoader$a;)I

    .line 30
    .line 31
    .line 32
    const/4 p1, 0x1

    .line 33
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 34
    .line 35
    iget p1, v0, La/b;->pageNum:I

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    const/4 p1, -0x1

    .line 39
    :goto_0
    const/4 v0, 0x0

    .line 40
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    .line 41
    .line 42
    invoke-virtual {p2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->clear()V

    .line 43
    .line 44
    .line 45
    if-nez p3, :cond_1

    .line 46
    .line 47
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 48
    .line 49
    .line 50
    :cond_1
    return p1
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public doneStampAnnot(Lcom/intsig/document/widget/PagesView$StampArgs;Lcom/intsig/document/widget/PagesView$AnnotBox;Z)I
    .locals 4

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->empty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    if-eqz p3, :cond_1

    .line 10
    .line 11
    iget-object v0, p1, Lcom/intsig/document/widget/PagesView$StampArgs;->bmp:Landroid/graphics/Bitmap;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    new-instance p1, Lcom/intsig/document/DocumentLoader$b;

    .line 16
    .line 17
    iget v1, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 18
    .line 19
    iget-object v2, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 20
    .line 21
    iget v3, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    .line 22
    .line 23
    invoke-direct {p1, v1, v2, v0, v3}, Lcom/intsig/document/DocumentLoader$b;-><init>(ILandroid/graphics/RectF;Landroid/graphics/Bitmap;I)V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    new-instance v0, Lcom/intsig/document/DocumentLoader$b;

    .line 28
    .line 29
    iget v1, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 30
    .line 31
    iget-object v2, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 32
    .line 33
    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$StampArgs;->text:Ljava/lang/String;

    .line 34
    .line 35
    invoke-direct {v0, v1, v2, p1}, Lcom/intsig/document/DocumentLoader$b;-><init>(ILandroid/graphics/RectF;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    move-object p1, v0

    .line 39
    :goto_0
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 40
    .line 41
    invoke-virtual {v0, p1}, Lb/a;->AddStampAnnotation(Lcom/intsig/document/DocumentLoader$b;)I

    .line 42
    .line 43
    .line 44
    const/4 v0, 0x1

    .line 45
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 46
    .line 47
    iget p1, p1, La/b;->pageNum:I

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_1
    const/4 p1, -0x1

    .line 51
    :goto_1
    const/4 v0, 0x0

    .line 52
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    .line 53
    .line 54
    invoke-virtual {p2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->clear()V

    .line 55
    .line 56
    .line 57
    if-nez p3, :cond_2

    .line 58
    .line 59
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 60
    .line 61
    .line 62
    :cond_2
    return p1
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public doneTextStampAnnot(Lcom/intsig/document/widget/PagesView$TextArgs;Lcom/intsig/document/widget/PagesView$AnnotBox;Z)I
    .locals 9

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->empty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    if-eqz p3, :cond_0

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/document/DocumentLoader$c;

    .line 12
    .line 13
    iget v2, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 14
    .line 15
    iget-object v3, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/document/widget/PagesView$TextArgs;->getText()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v4

    .line 21
    iget-object v5, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->font:Ljava/lang/String;

    .line 22
    .line 23
    iget v6, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->fontSize:F

    .line 24
    .line 25
    iget v7, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->color:I

    .line 26
    .line 27
    iget v8, p2, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    .line 28
    .line 29
    move-object v1, v0

    .line 30
    invoke-direct/range {v1 .. v8}, Lcom/intsig/document/DocumentLoader$c;-><init>(ILandroid/graphics/RectF;Ljava/lang/String;Ljava/lang/String;FII)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Lb/a;->AddTextAnnotation(Lcom/intsig/document/DocumentLoader$c;)I

    .line 36
    .line 37
    .line 38
    const/4 p1, 0x1

    .line 39
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 40
    .line 41
    iget p1, v0, La/b;->pageNum:I

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    const/4 p1, -0x1

    .line 45
    :goto_0
    const/4 v0, 0x0

    .line 46
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    .line 47
    .line 48
    invoke-virtual {p2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->clear()V

    .line 49
    .line 50
    .line 51
    if-nez p3, :cond_1

    .line 52
    .line 53
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 54
    .line 55
    .line 56
    :cond_1
    return p1
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public doneWatermark(Z)V
    .locals 7

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    iget-object v2, p1, Lcom/intsig/document/widget/PagesView$WatermarkArgs;->text:Ljava/lang/String;

    .line 8
    .line 9
    iget-object v3, p1, Lcom/intsig/document/widget/PagesView$WatermarkArgs;->font:Ljava/lang/String;

    .line 10
    .line 11
    iget v4, p1, Lcom/intsig/document/widget/PagesView$WatermarkArgs;->color:I

    .line 12
    .line 13
    iget v5, p1, Lcom/intsig/document/widget/PagesView$WatermarkArgs;->level:I

    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    .line 16
    .line 17
    new-instance v6, Lcom/intsig/document/widget/e;

    .line 18
    .line 19
    move-object v0, v6

    .line 20
    move-object v1, p0

    .line 21
    invoke-direct/range {v0 .. v5}, Lcom/intsig/document/widget/e;-><init>(Lcom/intsig/document/widget/PagesView;Ljava/lang/String;Ljava/lang/String;II)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, v6}, Lcom/intsig/document/widget/PagesView$v;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 25
    .line 26
    .line 27
    const/4 p1, 0x1

    .line 28
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 32
    .line 33
    .line 34
    :goto_0
    const/4 p1, 0x0

    .line 35
    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 36
    .line 37
    const/4 p1, 0x0

    .line 38
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->y0:Z

    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public enableAnnotOperate(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->a:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public enableTextSelection(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->D:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final finalize()V
    .locals 0

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAnnotBox()Lcom/intsig/document/widget/PagesView$AnnotBox;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 6
    .line 7
    iget v1, v0, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 8
    .line 9
    if-ltz v1, :cond_0

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 12
    .line 13
    invoke-direct {v1}, Lcom/intsig/document/widget/PagesView$AnnotBox;-><init>()V

    .line 14
    .line 15
    .line 16
    new-instance v2, Landroid/graphics/RectF;

    .line 17
    .line 18
    iget-object v3, v0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 19
    .line 20
    invoke-direct {v2, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 21
    .line 22
    .line 23
    iput-object v2, v1, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 24
    .line 25
    iget v2, v0, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 26
    .line 27
    iput v2, v1, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 28
    .line 29
    iget v0, v0, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    .line 30
    .line 31
    iput v0, v1, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    .line 32
    .line 33
    return-object v1

    .line 34
    :cond_0
    const/4 v0, 0x0

    .line 35
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public getInkCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$p;->〇080:Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2
    .param p1    # Landroid/os/Message;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget v0, p1, Landroid/os/Message;->what:I

    .line 2
    .line 3
    const/16 v1, 0x64

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->C:Z

    .line 9
    .line 10
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/16 v1, 0x65

    .line 15
    .line 16
    if-ne v0, v1, :cond_1

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->o0:Ljava/util/LinkedList;

    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->o0:Ljava/util/LinkedList;

    .line 24
    .line 25
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 26
    .line 27
    check-cast p1, Lcom/intsig/document/widget/PagesView$RenderArgs;

    .line 28
    .line 29
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 33
    return p1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public isChanged()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->F:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 7

    .line 1
    iget-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->q:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->q:Z

    .line 7
    .line 8
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 17
    .line 18
    invoke-virtual {v2}, Lcom/intsig/document/widget/PagesView$a0;->O8()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    const/4 v3, 0x1

    .line 23
    if-eqz v2, :cond_4

    .line 24
    .line 25
    iget v2, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 26
    .line 27
    if-ne v2, v3, :cond_4

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/document/widget/PagesView$a0;->〇080()Lcom/intsig/document/DocumentLoader$d;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 36
    .line 37
    invoke-virtual {v2}, Lcom/intsig/document/widget/PagesView$a0;->Oo08()Lcom/intsig/document/DocumentLoader$d;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    iget v4, v1, Lcom/intsig/document/DocumentLoader$d;->〇o〇:I

    .line 42
    .line 43
    new-instance v5, Landroid/graphics/PointF;

    .line 44
    .line 45
    iget-object v1, v1, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    .line 46
    .line 47
    iget v6, v1, Landroid/graphics/RectF;->left:F

    .line 48
    .line 49
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    .line 50
    .line 51
    invoke-direct {v5, v6, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0, v4, v5}, Lcom/intsig/document/widget/PagesView;->a(ILandroid/graphics/PointF;)Landroid/graphics/PointF;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    iget v4, v2, Lcom/intsig/document/DocumentLoader$d;->〇o〇:I

    .line 59
    .line 60
    new-instance v5, Landroid/graphics/PointF;

    .line 61
    .line 62
    iget-object v2, v2, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    .line 63
    .line 64
    iget v6, v2, Landroid/graphics/RectF;->right:F

    .line 65
    .line 66
    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    .line 67
    .line 68
    invoke-direct {v5, v6, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {p0, v4, v5}, Lcom/intsig/document/widget/PagesView;->a(ILandroid/graphics/PointF;)Landroid/graphics/PointF;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    iget v4, v1, Landroid/graphics/PointF;->x:F

    .line 76
    .line 77
    const/high16 v5, 0x41f00000    # 30.0f

    .line 78
    .line 79
    sub-float/2addr v4, v5

    .line 80
    iput v4, v1, Landroid/graphics/PointF;->x:F

    .line 81
    .line 82
    iget v4, v1, Landroid/graphics/PointF;->y:F

    .line 83
    .line 84
    const/high16 v6, 0x42480000    # 50.0f

    .line 85
    .line 86
    add-float/2addr v4, v6

    .line 87
    iput v4, v1, Landroid/graphics/PointF;->y:F

    .line 88
    .line 89
    iget v4, v2, Landroid/graphics/PointF;->x:F

    .line 90
    .line 91
    add-float/2addr v4, v5

    .line 92
    iput v4, v2, Landroid/graphics/PointF;->x:F

    .line 93
    .line 94
    iget v4, v2, Landroid/graphics/PointF;->y:F

    .line 95
    .line 96
    add-float/2addr v4, v6

    .line 97
    iput v4, v2, Landroid/graphics/PointF;->y:F

    .line 98
    .line 99
    const/high16 v4, 0x42700000    # 60.0f

    .line 100
    .line 101
    invoke-virtual {p0, v1, v0, p1, v4}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/PointF;FFF)Z

    .line 102
    .line 103
    .line 104
    move-result v1

    .line 105
    if-eqz v1, :cond_1

    .line 106
    .line 107
    const/4 p1, 0x2

    .line 108
    :goto_0
    iput p1, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 109
    .line 110
    goto :goto_1

    .line 111
    :cond_1
    invoke-virtual {p0, v2, v0, p1, v4}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/PointF;FFF)Z

    .line 112
    .line 113
    .line 114
    move-result p1

    .line 115
    if-eqz p1, :cond_2

    .line 116
    .line 117
    const/4 p1, 0x3

    .line 118
    goto :goto_0

    .line 119
    :cond_2
    :goto_1
    iget p1, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 120
    .line 121
    if-eq p1, v3, :cond_3

    .line 122
    .line 123
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->w0:Lcom/intsig/document/widget/PagesView$z;

    .line 124
    .line 125
    if-eqz p1, :cond_3

    .line 126
    .line 127
    invoke-interface {p1}, Lcom/intsig/document/widget/PagesView$z;->onSelectionClose()V

    .line 128
    .line 129
    .line 130
    :cond_3
    return v3

    .line 131
    :cond_4
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 132
    .line 133
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 134
    .line 135
    .line 136
    move-result-object v2

    .line 137
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 138
    .line 139
    .line 140
    move-result v4

    .line 141
    if-eqz v4, :cond_7

    .line 142
    .line 143
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 144
    .line 145
    .line 146
    move-result-object v4

    .line 147
    check-cast v4, Lcom/intsig/document/widget/PagesView$ElementData;

    .line 148
    .line 149
    iget-object v5, v4, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 150
    .line 151
    iget-object v6, v4, Lcom/intsig/document/widget/PagesView$ElementData;->args:Lcom/intsig/document/widget/PagesView$BaseArgs;

    .line 152
    .line 153
    iget-boolean v6, v6, Lcom/intsig/document/widget/PagesView$BaseArgs;->deleteable:Z

    .line 154
    .line 155
    invoke-virtual {p0, v5, v0, p1, v6}, Lcom/intsig/document/widget/PagesView;->a(Lcom/intsig/document/widget/PagesView$AnnotBox;FFZ)I

    .line 156
    .line 157
    .line 158
    move-result v5

    .line 159
    iput v5, p0, Lcom/intsig/document/widget/PagesView;->d0:I

    .line 160
    .line 161
    if-eq v5, v3, :cond_5

    .line 162
    .line 163
    iput-object v4, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 164
    .line 165
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 166
    .line 167
    if-eqz v1, :cond_6

    .line 168
    .line 169
    sget-object v2, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Down:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 170
    .line 171
    iput-object v2, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 172
    .line 173
    invoke-interface {v1, v4, v2}, Lcom/intsig/document/widget/PagesView$k;->onElementDataFocus(Lcom/intsig/document/widget/PagesView$ElementData;Lcom/intsig/document/widget/PagesView$AnnotFocusState;)V

    .line 174
    .line 175
    .line 176
    :cond_6
    const/4 v1, 0x1

    .line 177
    :cond_7
    if-eqz v1, :cond_8

    .line 178
    .line 179
    return v3

    .line 180
    :cond_8
    const/4 v1, 0x0

    .line 181
    iput-object v1, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 182
    .line 183
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->p:Landroid/graphics/RectF;

    .line 184
    .line 185
    invoke-virtual {v1, v0, p1}, Landroid/graphics/RectF;->contains(FF)Z

    .line 186
    .line 187
    .line 188
    move-result p1

    .line 189
    if-eqz p1, :cond_9

    .line 190
    .line 191
    iput-boolean v3, p0, Lcom/intsig/document/widget/PagesView;->q:Z

    .line 192
    .line 193
    iput-boolean v3, p0, Lcom/intsig/document/widget/PagesView;->C:Z

    .line 194
    .line 195
    :cond_9
    return v3
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 30

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, v6, Lcom/intsig/document/widget/PagesView;->t:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v9

    iget v0, v6, Lcom/intsig/document/widget/PagesView;->N:I

    invoke-virtual {v7, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    if-eqz v0, :cond_4a

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_2a

    :cond_1
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget v0, v6, Lcom/intsig/document/widget/PagesView;->w:F

    neg-float v0, v0

    const/4 v11, 0x0

    :goto_0
    iget v1, v6, Lcom/intsig/document/widget/PagesView;->l:I

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v12, 0x0

    if-ge v11, v1, :cond_3f

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/document/widget/PagesView$s;

    iget-object v2, v1, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    invoke-virtual {v2}, Landroid/util/Size;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2}, Landroid/util/Size;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget v5, v6, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v13, v4, v5

    mul-float v5, v5, v2

    iget v15, v6, Lcom/intsig/document/widget/PagesView;->x:F

    div-float v19, v13, v3

    sub-float v15, v15, v19

    iget v3, v6, Lcom/intsig/document/widget/PagesView;->i:F

    add-float/2addr v3, v0

    int-to-float v0, v9

    cmpg-float v20, v3, v0

    if-gtz v20, :cond_2

    add-float v20, v3, v5

    cmpl-float v20, v20, v12

    if-ltz v20, :cond_2

    const/16 v20, 0x1

    goto :goto_1

    :cond_2
    const/16 v20, 0x0

    :goto_1
    if-eqz v20, :cond_3d

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v10

    invoke-virtual {v7, v15, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget v14, v6, Lcom/intsig/document/widget/PagesView;->R:I

    move/from16 v21, v4

    if-eqz v14, :cond_3

    iget-object v14, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v14, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v4, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const v14, -0x777778

    invoke-virtual {v4, v14}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v4, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v14, v6, Lcom/intsig/document/widget/PagesView;->R:I

    int-to-float v14, v14

    invoke-virtual {v4, v14}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v12, v12, v13, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v14, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v4, v14}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v14, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/4 v4, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v25, v1

    move v1, v4

    move v4, v2

    move/from16 v2, v22

    move/from16 v26, v3

    move v3, v13

    move/from16 v28, v4

    move/from16 v27, v21

    const/high16 v12, 0x40800000    # 4.0f

    move/from16 v4, v23

    move/from16 v17, v5

    move/from16 v18, v9

    const/high16 v9, 0x41000000    # 8.0f

    move-object v5, v14

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v5, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move/from16 v4, v17

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v9, v12, v12, v1}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    iget-object v5, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/4 v1, 0x0

    move-object/from16 v0, p1

    move/from16 v2, v17

    move v3, v13

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v5, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/4 v4, 0x0

    move v1, v13

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->clearShadowLayer()V

    goto :goto_2

    :cond_3
    move/from16 v24, v0

    move-object/from16 v25, v1

    move/from16 v28, v2

    move/from16 v26, v3

    move/from16 v17, v5

    move/from16 v18, v9

    move/from16 v27, v21

    const/high16 v9, 0x41000000    # 8.0f

    const/high16 v12, 0x40800000    # 4.0f

    :goto_2
    iget v0, v6, Lcom/intsig/document/widget/PagesView;->u:F

    invoke-virtual {v7, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    move/from16 v14, v27

    move/from16 v0, v28

    const/4 v1, 0x0

    invoke-virtual {v7, v1, v1, v14, v0}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    const/4 v1, -0x1

    invoke-virtual {v7, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    iget v1, v6, Lcom/intsig/document/widget/PagesView;->u:F

    move-object/from16 v5, v25

    invoke-virtual {v5, v1}, Lcom/intsig/document/widget/PagesView$s;->〇080(F)Lcom/intsig/document/widget/PagesView$l;

    move-result-object v1

    iget-object v2, v1, Lcom/intsig/document/widget/PagesView$l;->〇080:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, v1, Lcom/intsig/document/widget/PagesView$l;->〇080:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v9, v1, Lcom/intsig/document/widget/PagesView$l;->〇080:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    const/4 v12, 0x0

    invoke-direct {v3, v12, v12, v4, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v4, Landroid/graphics/RectF;

    const/4 v9, 0x0

    invoke-direct {v4, v9, v9, v14, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v9, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v2, v3, v4, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    :cond_4
    mul-float v2, v13, v17

    const/high16 v3, 0x4ba00000    # 2.097152E7f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_5

    const/4 v2, 0x1

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_11

    const/16 v16, 0x0

    cmpg-float v2, v15, v16

    if-gez v2, :cond_6

    neg-float v2, v15

    iget v3, v6, Lcom/intsig/document/widget/PagesView;->u:F

    div-float/2addr v2, v3

    move/from16 v3, v26

    goto :goto_4

    :cond_6
    move/from16 v3, v26

    const/4 v2, 0x0

    :goto_4
    cmpg-float v21, v3, v16

    if-gez v21, :cond_7

    neg-float v9, v3

    iget v12, v6, Lcom/intsig/document/widget/PagesView;->u:F

    div-float/2addr v9, v12

    goto :goto_5

    :cond_7
    const/4 v9, 0x0

    :goto_5
    add-float/2addr v15, v13

    int-to-float v12, v8

    cmpl-float v13, v15, v12

    if-lez v13, :cond_8

    sub-float/2addr v15, v12

    iget v12, v6, Lcom/intsig/document/widget/PagesView;->u:F

    div-float/2addr v15, v12

    sub-float v12, v14, v15

    goto :goto_6

    :cond_8
    move v12, v14

    :goto_6
    add-float v13, v3, v17

    cmpl-float v15, v13, v24

    if-lez v15, :cond_9

    sub-float v13, v13, v24

    iget v15, v6, Lcom/intsig/document/widget/PagesView;->u:F

    div-float/2addr v13, v15

    sub-float/2addr v0, v13

    :cond_9
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Page "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v15, " visiable Region "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v15, ","

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v4, "  "

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v4, " pp "

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/intsig/document/widget/PagesView$l;->〇o00〇〇Oo:Lcom/intsig/document/widget/PagesView$t;

    if-eqz v4, :cond_a

    iget-object v4, v4, Lcom/intsig/document/widget/PagesView$t;->〇080:Landroid/graphics/Bitmap;

    goto :goto_7

    :cond_a
    const/4 v4, 0x0

    :goto_7
    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/intsig/document/widget/PagesView$l;->〇o00〇〇Oo:Lcom/intsig/document/widget/PagesView$t;

    if-eqz v4, :cond_b

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    iget-object v13, v1, Lcom/intsig/document/widget/PagesView$l;->〇o00〇〇Oo:Lcom/intsig/document/widget/PagesView$t;

    iget-object v13, v13, Lcom/intsig/document/widget/PagesView$t;->〇o00〇〇Oo:Landroid/graphics/RectF;

    invoke-virtual {v7, v13}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    iget-object v13, v1, Lcom/intsig/document/widget/PagesView$l;->〇o00〇〇Oo:Lcom/intsig/document/widget/PagesView$t;

    iget-object v15, v13, Lcom/intsig/document/widget/PagesView$t;->〇080:Landroid/graphics/Bitmap;

    iget-object v13, v13, Lcom/intsig/document/widget/PagesView$t;->〇o00〇〇Oo:Landroid/graphics/RectF;

    move/from16 v26, v3

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    move-object/from16 v25, v5

    const/4 v5, 0x0

    invoke-virtual {v7, v15, v5, v13, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Page draw ragion..."

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/intsig/document/widget/PagesView$l;->〇o00〇〇Oo:Lcom/intsig/document/widget/PagesView$t;

    iget-object v5, v5, Lcom/intsig/document/widget/PagesView$t;->〇o00〇〇Oo:Landroid/graphics/RectF;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_8

    :cond_b
    move/from16 v26, v3

    move-object/from16 v25, v5

    :goto_8
    iget-object v3, v1, Lcom/intsig/document/widget/PagesView$l;->〇080:Landroid/graphics/Bitmap;

    if-nez v3, :cond_c

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v6, v11, v3, v5, v4}, Lcom/intsig/document/widget/PagesView;->a(IFLandroid/graphics/RectF;Z)V

    :cond_c
    iget-object v3, v1, Lcom/intsig/document/widget/PagesView$l;->〇o00〇〇Oo:Lcom/intsig/document/widget/PagesView$t;

    if-nez v3, :cond_d

    const/4 v3, 0x1

    goto :goto_9

    :cond_d
    const/4 v3, 0x0

    :goto_9
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v2, v9, v12, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v4, v1, Lcom/intsig/document/widget/PagesView$l;->〇o00〇〇Oo:Lcom/intsig/document/widget/PagesView$t;

    if-eqz v4, :cond_10

    iget-object v3, v4, Lcom/intsig/document/widget/PagesView$t;->〇o00〇〇Oo:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_f

    iget-object v3, v1, Lcom/intsig/document/widget/PagesView$l;->〇o00〇〇Oo:Lcom/intsig/document/widget/PagesView$t;

    iget-object v3, v3, Lcom/intsig/document/widget/PagesView$t;->〇o00〇〇Oo:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v9

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_f

    iget-object v3, v1, Lcom/intsig/document/widget/PagesView$l;->〇o00〇〇Oo:Lcom/intsig/document/widget/PagesView$t;

    iget-object v3, v3, Lcom/intsig/document/widget/PagesView$t;->〇o00〇〇Oo:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_f

    iget-object v1, v1, Lcom/intsig/document/widget/PagesView$l;->〇o00〇〇Oo:Lcom/intsig/document/widget/PagesView$t;

    iget-object v1, v1, Lcom/intsig/document/widget/PagesView$t;->〇o00〇〇Oo:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, v12

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v4

    if-lez v1, :cond_e

    goto :goto_a

    :cond_e
    const/4 v3, 0x0

    goto :goto_b

    :cond_f
    :goto_a
    const/4 v3, 0x1

    :cond_10
    :goto_b
    if-eqz v3, :cond_13

    iget v1, v6, Lcom/intsig/document/widget/PagesView;->u:F

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v2, v9, v12, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v0, 0x0

    invoke-virtual {v6, v11, v1, v3, v0}, Lcom/intsig/document/widget/PagesView;->a(IFLandroid/graphics/RectF;Z)V

    goto :goto_c

    :cond_11
    move-object/from16 v25, v5

    iget-object v0, v1, Lcom/intsig/document/widget/PagesView$l;->〇080:Landroid/graphics/Bitmap;

    if-nez v0, :cond_12

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const-string v2, "Loading..."

    const/high16 v3, 0x42000000    # 32.0f

    invoke-virtual {v7, v2, v3, v3, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_12
    iget-boolean v0, v1, Lcom/intsig/document/widget/PagesView$l;->〇o〇:Z

    if-nez v0, :cond_13

    iget-boolean v0, v6, Lcom/intsig/document/widget/PagesView;->A:Z

    if-nez v0, :cond_13

    iget v0, v6, Lcom/intsig/document/widget/PagesView;->u:F

    const/4 v1, 0x0

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v0, v1, v12}, Lcom/intsig/document/widget/PagesView;->a(IFLandroid/graphics/RectF;Z)V

    goto :goto_d

    :cond_13
    :goto_c
    const/4 v12, 0x0

    :goto_d
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->b:La/b;

    if-eqz v0, :cond_14

    iget v0, v0, La/b;->pageNum:I

    if-ne v11, v0, :cond_14

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/high16 v9, 0x40000000    # 2.0f

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/high16 v1, -0x55010000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    const/4 v13, 0x2

    new-array v2, v13, [F

    fill-array-data v2, :array_0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->b:La/b;

    iget-object v1, v1, La/b;->rect:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    const/high16 v1, -0x3f800000    # -4.0f

    invoke-virtual {v0, v1, v1}, Landroid/graphics/RectF;->inset(FF)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    goto :goto_e

    :cond_14
    const/4 v4, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v13, 0x2

    :goto_e
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->t0:Ljava/util/ArrayList;

    if-eqz v0, :cond_17

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_15
    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/document/DocumentLoader$d;

    iget v2, v1, Lcom/intsig/document/DocumentLoader$d;->〇o〇:I

    if-ne v2, v11, :cond_15

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const v3, -0x5503447a

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->u0:Lcom/intsig/document/DocumentLoader$d;

    if-ne v2, v1, :cond_16

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const v3, -0x55119e00

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    :cond_16
    iget-object v1, v1, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_f

    :cond_17
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$a0;->O8()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$a0;->〇080()Lcom/intsig/document/DocumentLoader$d;

    move-result-object v0

    iget v0, v0, Lcom/intsig/document/DocumentLoader$d;->〇o〇:I

    if-ne v0, v11, :cond_1c

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const v1, 0x66ee6200

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$a0;->〇o〇:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_10

    :cond_18
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$a0;->〇080()Lcom/intsig/document/DocumentLoader$d;

    move-result-object v15

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$a0;->Oo08()Lcom/intsig/document/DocumentLoader$d;

    move-result-object v5

    iget-object v0, v15, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v22, v1

    move-object/from16 v1, p1

    move/from16 v23, v2

    move-object/from16 v2, v22

    move/from16 v22, v26

    move/from16 v26, v3

    move-object v12, v4

    move/from16 v4, v23

    move-object v9, v5

    move-object/from16 v12, v25

    move/from16 v5, v20

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFZ)V

    iget-object v0, v9, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    iget v5, v0, Landroid/graphics/RectF;->right:F

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move v3, v5

    move/from16 v28, v4

    move/from16 v29, v5

    move/from16 v5, v20

    invoke-virtual/range {v0 .. v5}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFZ)V

    iget v0, v6, Lcom/intsig/document/widget/PagesView;->u:F

    float-to-double v1, v0

    const-wide/high16 v3, 0x4010000000000000L    # 4.0

    cmpg-double v5, v1, v3

    if-gez v5, :cond_1b

    iget v1, v6, Lcom/intsig/document/widget/PagesView;->q0:I

    const/4 v5, 0x1

    if-eq v1, v5, :cond_1b

    iget v2, v6, Lcom/intsig/document/widget/PagesView;->G:F

    const/high16 v3, 0x42000000    # 32.0f

    mul-float v2, v2, v3

    div-float/2addr v2, v0

    if-ne v1, v13, :cond_19

    iget-object v0, v15, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    move/from16 v28, v23

    move/from16 v3, v26

    goto :goto_11

    :cond_19
    iget-object v0, v9, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    move/from16 v3, v29

    :goto_11
    const/high16 v1, 0x40000000    # 2.0f

    mul-float v4, v2, v1

    sub-float v4, v28, v4

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/high16 v9, -0x1000000

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v9, v6, Lcom/intsig/document/widget/PagesView;->u:F

    const/high16 v15, 0x40800000    # 4.0f

    div-float v9, v15, v9

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v9, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v3, v4, v2, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    sget-object v9, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v3, v4, v2, v9}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    invoke-virtual {v7, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    const/4 v1, -0x1

    invoke-virtual {v7, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    iget v1, v6, Lcom/intsig/document/widget/PagesView;->u:F

    invoke-virtual {v12, v1}, Lcom/intsig/document/widget/PagesView$s;->〇080(F)Lcom/intsig/document/widget/PagesView$l;

    move-result-object v1

    iget-object v9, v1, Lcom/intsig/document/widget/PagesView$l;->〇080:Landroid/graphics/Bitmap;

    if-eqz v9, :cond_1a

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v9, v14

    new-instance v14, Landroid/graphics/Rect;

    const/high16 v15, 0x41a00000    # 20.0f

    sub-float v20, v3, v15

    mul-float v20, v20, v9

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v5

    sub-float v20, v28, v15

    mul-float v20, v20, v9

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v13

    add-float v20, v3, v15

    mul-float v20, v20, v9

    move/from16 v23, v8

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->round(F)I

    move-result v8

    add-float v28, v28, v15

    mul-float v28, v28, v9

    invoke-static/range {v28 .. v28}, Ljava/lang/Math;->round(F)I

    move-result v9

    invoke-direct {v14, v5, v13, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v5, Landroid/graphics/RectF;

    sub-float v8, v3, v2

    sub-float v9, v4, v2

    add-float v13, v3, v2

    add-float/2addr v2, v4

    invoke-direct {v5, v8, v9, v13, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    iget-object v1, v1, Lcom/intsig/document/widget/PagesView$l;->〇080:Landroid/graphics/Bitmap;

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v14, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const v2, -0x119e00

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v2, v6, Lcom/intsig/document/widget/PagesView;->u:F

    const/high16 v5, 0x40800000    # 4.0f

    div-float v2, v5, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget v1, v6, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v0, v0, v1

    sub-float v5, v4, v0

    iget-object v8, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move v1, v3

    move v2, v4

    move v4, v5

    const/4 v9, 0x1

    move-object v5, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_12

    :cond_1a
    move/from16 v23, v8

    const/4 v9, 0x1

    :goto_12
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_14

    :cond_1b
    move/from16 v23, v8

    goto :goto_13

    :cond_1c
    move/from16 v23, v8

    move-object/from16 v12, v25

    move/from16 v22, v26

    :goto_13
    const/4 v9, 0x1

    :goto_14
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1d
    :goto_15
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/intsig/document/widget/PagesView$ElementData;

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    if-ne v0, v13, :cond_1e

    const/4 v2, 0x1

    goto :goto_16

    :cond_1e
    const/4 v2, 0x0

    :goto_16
    iget-object v14, v13, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    invoke-virtual {v14}, Lcom/intsig/document/widget/PagesView$AnnotBox;->empty()Z

    move-result v0

    if-nez v0, :cond_1d

    iget v0, v14, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    if-ne v11, v0, :cond_1d

    iget v0, v6, Lcom/intsig/document/widget/PagesView;->A0:F

    iget v1, v6, Lcom/intsig/document/widget/PagesView;->u:F

    div-float/2addr v0, v1

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v3, v6, Lcom/intsig/document/widget/PagesView;->B0:I

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/RectF;

    iget-object v3, v14, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    invoke-direct {v1, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    const/high16 v3, -0x3ee00000    # -10.0f

    invoke-virtual {v1, v3, v3}, Landroid/graphics/RectF;->inset(FF)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {v14}, Lcom/intsig/document/widget/PagesView$AnnotBox;->isRotateable()Z

    move-result v3

    if-eqz v3, :cond_1f

    iget v3, v14, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-virtual {v7, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    :cond_1f
    iget-boolean v3, v14, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixSize:Z

    const/high16 v15, 0x40e00000    # 7.0f

    if-nez v3, :cond_21

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    if-eqz v2, :cond_20

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v3, v6, Lcom/intsig/document/widget/PagesView;->B0:I

    const/high16 v20, -0x1000000

    or-int v3, v3, v20

    goto :goto_17

    :cond_20
    const/high16 v20, -0x1000000

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v3, v6, Lcom/intsig/document/widget/PagesView;->B0:I

    :goto_17
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    iget v2, v1, Landroid/graphics/RectF;->right:F

    iget v3, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v4, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v2, v3, v15, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iget v3, v1, Landroid/graphics/RectF;->right:F

    iget v4, v1, Landroid/graphics/RectF;->bottom:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v0, v5

    add-float/2addr v0, v15

    sget-object v5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    sget-object v0, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {v7, v2, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    goto :goto_18

    :cond_21
    const/high16 v20, -0x1000000

    :goto_18
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->C0:[F

    if-eqz v0, :cond_22

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/DashPathEffect;

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->C0:[F

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    :cond_22
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-boolean v0, v14, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixSize:Z

    if-nez v0, :cond_23

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_23
    iget-object v0, v13, Lcom/intsig/document/widget/PagesView$ElementData;->args:Lcom/intsig/document/widget/PagesView$BaseArgs;

    iget-boolean v0, v0, Lcom/intsig/document/widget/PagesView$BaseArgs;->deleteable:Z

    if-eqz v0, :cond_24

    iget v0, v1, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    add-int/lit8 v0, v0, -0x10

    iget v2, v1, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    add-int/lit8 v2, v2, -0x10

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->K:Landroid/graphics/drawable/Drawable;

    add-int/lit8 v4, v0, 0x10

    add-int/lit8 v4, v4, 0x10

    add-int/lit8 v5, v2, 0x10

    add-int/lit8 v5, v5, 0x10

    invoke-virtual {v3, v0, v2, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->K:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v7}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_24
    iget-boolean v0, v14, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    if-eqz v0, :cond_27

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    iget v2, v1, Landroid/graphics/RectF;->left:F

    add-float v5, v0, v2

    iget v2, v1, Landroid/graphics/RectF;->top:F

    iget v0, v6, Lcom/intsig/document/widget/PagesView;->H:F

    sub-float v4, v2, v0

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->C0:[F

    if-eqz v0, :cond_25

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->C0:[F

    const/4 v9, 0x0

    invoke-direct {v1, v3, v9}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    :cond_25
    iget-object v9, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move v1, v5

    move v3, v5

    move/from16 v21, v4

    move v15, v5

    move-object v5, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->J:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_26

    float-to-int v1, v15

    add-int/lit8 v2, v1, -0x14

    move/from16 v3, v21

    float-to-int v3, v3

    add-int/lit8 v4, v3, -0x28

    add-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->J:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v7}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_19

    :cond_26
    move/from16 v3, v21

    const/high16 v0, 0x40e00000    # 7.0f

    sub-float v4, v3, v0

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v15, v4, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_27
    :goto_19
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    iget-object v0, v13, Lcom/intsig/document/widget/PagesView$ElementData;->args:Lcom/intsig/document/widget/PagesView$BaseArgs;

    instance-of v2, v0, Lcom/intsig/document/widget/PagesView$StampArgs;

    if-eqz v2, :cond_29

    check-cast v0, Lcom/intsig/document/widget/PagesView$StampArgs;

    iget-object v2, v0, Lcom/intsig/document/widget/PagesView$StampArgs;->bmp:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_28

    iget-object v0, v14, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    invoke-virtual {v7, v2, v1, v0, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_1b

    :cond_28
    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$StampArgs;->text:Ljava/lang/String;

    if-eqz v0, :cond_2d

    iget-object v1, v14, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1b

    :cond_29
    instance-of v1, v0, Lcom/intsig/document/widget/PagesView$ImageArgs;

    if-eqz v1, :cond_2a

    check-cast v0, Lcom/intsig/document/widget/PagesView$ImageArgs;

    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$ImageArgs;->bmp:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2d

    iget-object v1, v14, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    const/4 v2, 0x0

    invoke-virtual {v7, v0, v2, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_1b

    :cond_2a
    instance-of v1, v0, Lcom/intsig/document/widget/PagesView$ShapeArgs;

    if-eqz v1, :cond_2c

    check-cast v0, Lcom/intsig/document/widget/PagesView$ShapeArgs;

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v2, v0, Lcom/intsig/document/widget/PagesView$ShapeArgs;->color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v2, v0, Lcom/intsig/document/widget/PagesView$ShapeArgs;->strokeWidth:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, v14, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iget v2, v0, Lcom/intsig/document/widget/PagesView$ShapeArgs;->strokeWidth:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-virtual {v1, v2, v2}, Landroid/graphics/RectF;->inset(FF)V

    iget v0, v0, Lcom/intsig/document/widget/PagesView$ShapeArgs;->type:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_2b

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v0}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_1b

    :cond_2b
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_1b

    :cond_2c
    instance-of v1, v0, Lcom/intsig/document/widget/PagesView$TextArgs;

    if-eqz v1, :cond_2d

    check-cast v0, Lcom/intsig/document/widget/PagesView$TextArgs;

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v2, v0, Lcom/intsig/document/widget/PagesView$TextArgs;->color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget v1, v0, Lcom/intsig/document/widget/PagesView$TextArgs;->fontSize:F

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, v14, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->left:F

    iget v4, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v1

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$TextArgs;->layoutLines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v5, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-float/2addr v4, v1

    goto :goto_1a

    :cond_2d
    :goto_1b
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    const/4 v9, 0x1

    goto/16 :goto_15

    :cond_2e
    iget-boolean v0, v6, Lcom/intsig/document/widget/PagesView;->y0:Z

    if-eqz v0, :cond_3c

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->e0:Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;

    if-eqz v0, :cond_32

    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$ImageArgs;->bmp:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3c

    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->e0:Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;

    iget-object v1, v1, Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;->box:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iget v1, v0, Landroid/graphics/RectF;->left:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_30

    iget v1, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_2f

    goto :goto_1d

    :cond_2f
    iget-object v1, v12, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    invoke-virtual {v1}, Landroid/util/Size;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_1d

    :cond_30
    iget v1, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_31

    iget-object v1, v12, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    invoke-virtual {v1}, Landroid/util/Size;->getWidth()I

    move-result v1

    int-to-float v1, v1

    goto :goto_1c

    :cond_31
    iget-object v1, v12, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    invoke-virtual {v1}, Landroid/util/Size;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, v12, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    invoke-virtual {v2}, Landroid/util/Size;->getHeight()I

    move-result v2

    int-to-float v2, v2

    :goto_1c
    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    :goto_1d
    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->e0:Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;

    iget-object v1, v1, Lcom/intsig/document/widget/PagesView$ImageArgs;->bmp:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v7, v1, v2, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_23

    :cond_32
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    if-eqz v0, :cond_3c

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    .line 1
    iget-object v1, v12, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    invoke-virtual {v1}, Landroid/util/Size;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, v12, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    invoke-virtual {v2}, Landroid/util/Size;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    iget v3, v3, Lcom/intsig/document/widget/PagesView$WatermarkArgs;->color:I

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    iget-object v3, v3, Lcom/intsig/document/widget/PagesView$WatermarkArgs;->text:Ljava/lang/String;

    if-eqz v3, :cond_33

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_34

    :cond_33
    const-string v3, "Watermark"

    :cond_34
    iget-object v4, v6, Lcom/intsig/document/widget/PagesView;->z0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    iget v4, v4, Lcom/intsig/document/widget/PagesView$WatermarkArgs;->level:I

    const/16 v5, 0xa

    if-le v4, v5, :cond_35

    const/16 v12, 0xa

    goto :goto_1e

    :cond_35
    move v12, v4

    :goto_1e
    if-gez v12, :cond_36

    const/4 v12, 0x0

    :cond_36
    mul-int/lit8 v4, v12, 0x2

    const/4 v5, 0x1

    add-int/2addr v4, v5

    .line 2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v8, 0x2

    mul-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    div-float v5, v1, v5

    const/high16 v8, 0x3f800000    # 1.0f

    :goto_1f
    add-float v9, v5, v8

    invoke-virtual {v0, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v13

    cmpl-float v13, v13, v1

    if-lez v13, :cond_3b

    rsub-int/lit8 v8, v12, 0xa

    int-to-float v8, v8

    const/high16 v9, 0x41600000    # 14.0f

    sub-float/2addr v5, v9

    mul-float v5, v5, v8

    const/high16 v8, 0x41200000    # 10.0f

    div-float/2addr v5, v8

    add-float/2addr v5, v9

    .line 3
    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    add-int/lit8 v8, v4, 0x1

    int-to-float v8, v8

    div-float/2addr v2, v8

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x1

    :goto_20
    if-gt v9, v4, :cond_39

    rem-int/lit8 v12, v9, 0x2

    if-nez v12, :cond_37

    neg-float v12, v5

    goto :goto_21

    :cond_37
    const/4 v12, 0x0

    :goto_21
    int-to-float v13, v9

    mul-float v13, v13, v2

    :cond_38
    new-instance v14, Landroid/graphics/PointF;

    invoke-direct {v14, v12, v13}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-float v14, v5, v5

    add-float/2addr v12, v14

    cmpg-float v14, v12, v1

    if-ltz v14, :cond_38

    add-int/lit8 v9, v9, 0x1

    goto :goto_20

    :cond_39
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_22
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    iget v4, v2, Landroid/graphics/PointF;->x:F

    const/high16 v8, 0x40000000    # 2.0f

    div-float v9, v5, v8

    add-float/2addr v9, v4

    iget v4, v2, Landroid/graphics/PointF;->y:F

    const/high16 v8, -0x3dcc0000    # -45.0f

    invoke-virtual {v7, v8, v9, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v3, v4, v2, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_22

    :cond_3a
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_23

    :cond_3b
    move v5, v9

    goto :goto_1f

    .line 4
    :cond_3c
    :goto_23
    invoke-virtual {v7, v10}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_24

    :cond_3d
    move/from16 v24, v0

    move/from16 v22, v3

    move/from16 v17, v5

    move/from16 v23, v8

    move/from16 v18, v9

    :goto_24
    cmpl-float v0, v22, v24

    if-lez v0, :cond_3e

    goto :goto_25

    :cond_3e
    add-float v0, v22, v17

    add-int/lit8 v11, v11, 0x1

    move/from16 v9, v18

    move/from16 v8, v23

    goto/16 :goto_0

    .line 5
    :cond_3f
    :goto_25
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    iget v2, v6, Lcom/intsig/document/widget/PagesView;->Q:I

    int-to-float v2, v2

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v4, v6, Lcom/intsig/document/widget/PagesView;->P:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v3, Landroid/graphics/RectF;

    int-to-float v0, v0

    sub-float v2, v0, v2

    int-to-float v4, v1

    const/4 v5, 0x0

    invoke-direct {v3, v2, v5, v0, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v5, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v3, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget v3, v6, Lcom/intsig/document/widget/PagesView;->y:F

    iget v5, v6, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v3, v3, v5

    iget v5, v6, Lcom/intsig/document/widget/PagesView;->l:I

    const/4 v8, 0x1

    add-int/2addr v5, v8

    int-to-float v5, v5

    iget v8, v6, Lcom/intsig/document/widget/PagesView;->i:F

    mul-float v5, v5, v8

    add-float/2addr v5, v3

    mul-int v1, v1, v1

    int-to-float v1, v1

    div-float/2addr v1, v5

    const/high16 v3, 0x42200000    # 40.0f

    cmpg-float v8, v1, v3

    if-gez v8, :cond_40

    const/high16 v1, 0x42200000    # 40.0f

    :cond_40
    iget v3, v6, Lcom/intsig/document/widget/PagesView;->w:F

    mul-float v3, v3, v4

    div-float/2addr v3, v5

    add-float v8, v3, v1

    cmpl-float v9, v8, v4

    if-ltz v9, :cond_41

    sub-float v3, v4, v1

    move v8, v4

    :cond_41
    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v9, v6, Lcom/intsig/document/widget/PagesView;->O:I

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v2, v3, v0, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-boolean v0, v6, Lcom/intsig/document/widget/PagesView;->C:Z

    if-eqz v0, :cond_48

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const v1, -0x278ae3

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget v0, v6, Lcom/intsig/document/widget/PagesView;->G:F

    const/high16 v1, 0x42400000    # 48.0f

    mul-float v1, v1, v0

    float-to-int v1, v1

    const/high16 v9, 0x42000000    # 32.0f

    mul-float v0, v0, v9

    float-to-int v0, v0

    add-float/2addr v3, v8

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v3, v8

    int-to-float v8, v1

    sub-float/2addr v2, v8

    div-int/lit8 v9, v0, 0x2

    int-to-float v9, v9

    sub-float/2addr v3, v9

    const/4 v9, 0x0

    cmpg-float v10, v3, v9

    if-gez v10, :cond_42

    const/4 v12, 0x0

    goto :goto_26

    :cond_42
    move v12, v3

    :goto_26
    add-float/2addr v8, v2

    int-to-float v3, v0

    add-float v9, v12, v3

    cmpl-float v10, v9, v4

    if-lez v10, :cond_43

    sub-float v12, v4, v3

    move v9, v4

    :cond_43
    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->I:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_44

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v2, v12, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v10, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/high16 v11, 0x41000000    # 8.0f

    invoke-virtual {v7, v3, v11, v11, v10}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-boolean v3, v6, Lcom/intsig/document/widget/PagesView;->q:Z

    if-eqz v3, :cond_45

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/4 v10, -0x1

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/high16 v10, 0x40a00000    # 5.0f

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v10, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v2, v12, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v10, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const/high16 v11, 0x41000000    # 8.0f

    invoke-virtual {v7, v3, v11, v11, v10}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_27

    :cond_44
    float-to-int v10, v2

    float-to-int v11, v12

    float-to-int v13, v8

    float-to-int v14, v9

    invoke-virtual {v3, v10, v11, v13, v14}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->I:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v7}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_45
    :goto_27
    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p:Landroid/graphics/RectF;

    iput v2, v3, Landroid/graphics/RectF;->left:F

    iput v12, v3, Landroid/graphics/RectF;->top:F

    iput v8, v3, Landroid/graphics/RectF;->right:F

    iput v9, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v8, v6, Lcom/intsig/document/widget/PagesView;->L:I

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget v8, v6, Lcom/intsig/document/widget/PagesView;->G:F

    const/high16 v10, 0x41600000    # 14.0f

    mul-float v8, v8, v10

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v3, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v8, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget v3, v6, Lcom/intsig/document/widget/PagesView;->o:I

    const/4 v8, 0x1

    add-int/2addr v3, v8

    iget v8, v6, Lcom/intsig/document/widget/PagesView;->w:F

    float-to-double v10, v8

    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    cmpg-double v14, v10, v12

    if-gez v14, :cond_46

    const/4 v3, 0x1

    goto :goto_28

    :cond_46
    add-float/2addr v8, v4

    const/high16 v4, 0x41200000    # 10.0f

    sub-float/2addr v5, v4

    cmpl-float v4, v8, v5

    if-ltz v4, :cond_47

    iget v3, v6, Lcom/intsig/document/widget/PagesView;->n:I

    const/4 v4, 0x1

    add-int/2addr v3, v4

    :cond_47
    :goto_28
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    div-int/2addr v1, v4

    int-to-float v1, v1

    add-float/2addr v2, v1

    div-int/lit8 v0, v0, 0x3

    int-to-float v0, v0

    sub-float/2addr v9, v0

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v3, v2, v9, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 6
    :cond_48
    iget-boolean v0, v6, Lcom/intsig/document/widget/PagesView;->V:Z

    if-eqz v0, :cond_4a

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$p;->〇080()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_49

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->f0:Lcom/intsig/document/widget/PagesView$InkArgs;

    iget v2, v2, Lcom/intsig/document/widget/PagesView$InkArgs;->strokeWidth:F

    iget v3, v6, Lcom/intsig/document/widget/PagesView;->u:F

    mul-float v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->f0:Lcom/intsig/document/widget/PagesView$InkArgs;

    iget v2, v2, Lcom/intsig/document/widget/PagesView$InkArgs;->color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_29
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_49

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Path;

    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_29

    :cond_49
    iget-boolean v0, v6, Lcom/intsig/document/widget/PagesView;->W:Z

    if-eqz v0, :cond_4a

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->h0:Landroid/graphics/Path;

    if-eqz v0, :cond_4a

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    const v1, 0x66666666

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, v6, Lcom/intsig/document/widget/PagesView;->h0:Landroid/graphics/Path;

    iget-object v1, v6, Lcom/intsig/document/widget/PagesView;->p0:Landroid/graphics/Paint;

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_4a
    :goto_2a
    return-void

    :array_0
    .array-data 4
        0x41000000    # 8.0f
        0x40800000    # 4.0f
    .end array-data
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 16

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    move/from16 v0, p3

    .line 4
    .line 5
    move/from16 v1, p4

    .line 6
    .line 7
    new-instance v2, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v3, "MotionEvent onFling "

    .line 13
    .line 14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v3, ","

    .line 21
    .line 22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    iget-boolean v2, v6, Lcom/intsig/document/widget/PagesView;->t:Z

    .line 29
    .line 30
    const/4 v7, 0x1

    .line 31
    if-nez v2, :cond_0

    .line 32
    .line 33
    return v7

    .line 34
    :cond_0
    iget-boolean v2, v6, Lcom/intsig/document/widget/PagesView;->U:Z

    .line 35
    .line 36
    if-eqz v2, :cond_1

    .line 37
    .line 38
    return v7

    .line 39
    :cond_1
    iget v2, v6, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 40
    .line 41
    const/4 v3, 0x0

    .line 42
    if-eq v2, v7, :cond_2

    .line 43
    .line 44
    return v3

    .line 45
    :cond_2
    iget-boolean v2, v6, Lcom/intsig/document/widget/PagesView;->q:Z

    .line 46
    .line 47
    if-eqz v2, :cond_3

    .line 48
    .line 49
    return v3

    .line 50
    :cond_3
    new-instance v4, Lcom/intsig/document/widget/PagesView$q;

    .line 51
    .line 52
    invoke-direct {v4}, Lcom/intsig/document/widget/PagesView$q;-><init>()V

    .line 53
    .line 54
    .line 55
    iget-object v2, v6, Lcom/intsig/document/widget/PagesView;->g:Landroid/os/Handler;

    .line 56
    .line 57
    const/16 v5, 0x64

    .line 58
    .line 59
    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 60
    .line 61
    .line 62
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    iget v8, v6, Lcom/intsig/document/widget/PagesView;->w:F

    .line 71
    .line 72
    add-float/2addr v5, v8

    .line 73
    iget v8, v6, Lcom/intsig/document/widget/PagesView;->x:F

    .line 74
    .line 75
    add-float/2addr v2, v8

    .line 76
    iget v9, v6, Lcom/intsig/document/widget/PagesView;->z:F

    .line 77
    .line 78
    iget v10, v6, Lcom/intsig/document/widget/PagesView;->u:F

    .line 79
    .line 80
    mul-float v9, v9, v10

    .line 81
    .line 82
    const/high16 v11, 0x40000000    # 2.0f

    .line 83
    .line 84
    div-float/2addr v9, v11

    .line 85
    sub-float/2addr v2, v9

    .line 86
    iget v12, v6, Lcom/intsig/document/widget/PagesView;->y:F

    .line 87
    .line 88
    mul-float v12, v12, v10

    .line 89
    .line 90
    iget v10, v6, Lcom/intsig/document/widget/PagesView;->l:I

    .line 91
    .line 92
    add-int/2addr v10, v7

    .line 93
    int-to-float v10, v10

    .line 94
    iget v13, v6, Lcom/intsig/document/widget/PagesView;->i:F

    .line 95
    .line 96
    mul-float v10, v10, v13

    .line 97
    .line 98
    add-float/2addr v10, v12

    .line 99
    float-to-int v12, v2

    .line 100
    float-to-int v13, v5

    .line 101
    neg-float v0, v0

    .line 102
    float-to-int v0, v0

    .line 103
    neg-float v1, v1

    .line 104
    float-to-int v1, v1

    .line 105
    sub-float/2addr v8, v9

    .line 106
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    .line 107
    .line 108
    .line 109
    move-result v8

    .line 110
    iget v9, v6, Lcom/intsig/document/widget/PagesView;->x:F

    .line 111
    .line 112
    iget v14, v6, Lcom/intsig/document/widget/PagesView;->z:F

    .line 113
    .line 114
    iget v15, v6, Lcom/intsig/document/widget/PagesView;->u:F

    .line 115
    .line 116
    mul-float v14, v14, v15

    .line 117
    .line 118
    div-float/2addr v14, v11

    .line 119
    add-float/2addr v14, v9

    .line 120
    invoke-static {v14}, Ljava/lang/Math;->round(F)I

    .line 121
    .line 122
    .line 123
    move-result v9

    .line 124
    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    .line 125
    .line 126
    .line 127
    move-result v10

    .line 128
    iput v0, v4, Lcom/intsig/document/widget/PagesView$q;->〇080:I

    .line 129
    .line 130
    iput v1, v4, Lcom/intsig/document/widget/PagesView$q;->〇o00〇〇Oo:I

    .line 131
    .line 132
    iput v8, v4, Lcom/intsig/document/widget/PagesView$q;->〇o〇:I

    .line 133
    .line 134
    iput v9, v4, Lcom/intsig/document/widget/PagesView$q;->O8:I

    .line 135
    .line 136
    iput v10, v4, Lcom/intsig/document/widget/PagesView$q;->o〇0:I

    .line 137
    .line 138
    iput v3, v4, Lcom/intsig/document/widget/PagesView$q;->Oo08:I

    .line 139
    .line 140
    iput v12, v4, Lcom/intsig/document/widget/PagesView$q;->〇〇888:I

    .line 141
    .line 142
    iput v13, v4, Lcom/intsig/document/widget/PagesView$q;->oO80:I

    .line 143
    .line 144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 145
    .line 146
    .line 147
    move-result-wide v0

    .line 148
    iput-wide v0, v4, Lcom/intsig/document/widget/PagesView$q;->〇80〇808〇O:J

    .line 149
    .line 150
    new-instance v8, Ljava/util/Timer;

    .line 151
    .line 152
    invoke-direct {v8}, Ljava/util/Timer;-><init>()V

    .line 153
    .line 154
    .line 155
    iput-object v8, v6, Lcom/intsig/document/widget/PagesView;->s0:Ljava/util/Timer;

    .line 156
    .line 157
    new-instance v9, Lcom/intsig/document/widget/PagesView$a;

    .line 158
    .line 159
    move-object v0, v9

    .line 160
    move-object/from16 v1, p0

    .line 161
    .line 162
    move v3, v5

    .line 163
    move-object v5, v8

    .line 164
    invoke-direct/range {v0 .. v5}, Lcom/intsig/document/widget/PagesView$a;-><init>(Lcom/intsig/document/widget/PagesView;FFLcom/intsig/document/widget/PagesView$q;Ljava/util/Timer;)V

    .line 165
    .line 166
    .line 167
    const-wide/16 v10, 0xa

    .line 168
    .line 169
    const-wide/16 v12, 0x28

    .line 170
    .line 171
    invoke-virtual/range {v8 .. v13}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 172
    .line 173
    .line 174
    return v7
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->D:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget v0, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    if-eq v0, v1, :cond_1

    .line 10
    .line 11
    return-void

    .line 12
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    new-instance v2, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v3, "MotionEvent onLongPress "

    .line 26
    .line 27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v3, ","

    .line 34
    .line 35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    new-instance v2, Landroid/graphics/PointF;

    .line 42
    .line 43
    invoke-direct {v2, v0, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0, v2}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/PointF;)I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    const-string v0, "onLongPress pos "

    .line 51
    .line 52
    invoke-static {v0}, La/a;->〇080(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    iget v4, v2, Landroid/graphics/PointF;->x:F

    .line 57
    .line 58
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    iget v3, v2, Landroid/graphics/PointF;->y:F

    .line 65
    .line 66
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    const-string v3, " at page "

    .line 70
    .line 71
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 78
    .line 79
    iget v3, v2, Landroid/graphics/PointF;->x:F

    .line 80
    .line 81
    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 82
    .line 83
    invoke-virtual {v0, p1, v3, v2, v1}, Lb/a;->TextAtPos(IFFZ)Lcom/intsig/document/DocumentLoader$d;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    if-eqz p1, :cond_2

    .line 88
    .line 89
    const-string v0, "SelectText:"

    .line 90
    .line 91
    invoke-static {v0}, La/a;->〇080(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    iget-object v2, p1, Lcom/intsig/document/DocumentLoader$d;->〇080:Ljava/lang/String;

    .line 96
    .line 97
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 101
    .line 102
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 103
    .line 104
    .line 105
    iget v2, p1, Lcom/intsig/document/DocumentLoader$d;->〇o〇:I

    .line 106
    .line 107
    iput v2, v0, Lcom/intsig/document/widget/PagesView$a0;->〇o00〇〇Oo:I

    .line 108
    .line 109
    iget-object v2, v0, Lcom/intsig/document/widget/PagesView$a0;->〇080:Ljava/util/ArrayList;

    .line 110
    .line 111
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 112
    .line 113
    .line 114
    iget-object v2, v0, Lcom/intsig/document/widget/PagesView$a0;->〇080:Ljava/util/ArrayList;

    .line 115
    .line 116
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    .line 118
    .line 119
    iget-object v2, v0, Lcom/intsig/document/widget/PagesView$a0;->〇o〇:Ljava/util/ArrayList;

    .line 120
    .line 121
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 122
    .line 123
    .line 124
    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$a0;->〇o〇:Ljava/util/ArrayList;

    .line 125
    .line 126
    iget-object v2, p1, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    .line 127
    .line 128
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    iput v1, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 132
    .line 133
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 134
    .line 135
    .line 136
    iget-object p1, p1, Lcom/intsig/document/DocumentLoader$d;->〇080:Ljava/lang/String;

    .line 137
    .line 138
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 139
    .line 140
    iget v1, v0, Lcom/intsig/document/widget/PagesView$a0;->〇o00〇〇Oo:I

    .line 141
    .line 142
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$a0;->〇o00〇〇Oo()Landroid/graphics/PointF;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    invoke-virtual {p0, v1, v0}, Lcom/intsig/document/widget/PagesView;->a(ILandroid/graphics/PointF;)Landroid/graphics/PointF;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    invoke-virtual {p0, p1, v0}, Lcom/intsig/document/widget/PagesView;->a(Ljava/lang/String;Landroid/graphics/PointF;)V

    .line 151
    .line 152
    .line 153
    :cond_2
    return-void
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    new-instance v2, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v3, "MotionEvent onScale "

    .line 19
    .line 20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v3, " at "

    .line 27
    .line 28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v3, ","

    .line 35
    .line 36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    const-string v3, " offsetx "

    .line 43
    .line 44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    iget v3, p0, Lcom/intsig/document/widget/PagesView;->x:F

    .line 48
    .line 49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    iget-boolean v2, p0, Lcom/intsig/document/widget/PagesView;->V:Z

    .line 53
    .line 54
    const/4 v3, 0x1

    .line 55
    if-eqz v2, :cond_0

    .line 56
    .line 57
    return v3

    .line 58
    :cond_0
    invoke-virtual {p0, v0, v1, p1}, Lcom/intsig/document/widget/PagesView;->a(FFF)Z

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    if-eqz p1, :cond_1

    .line 63
    .line 64
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 65
    .line 66
    .line 67
    :cond_1
    return v3
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->q:Z

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-eq v0, v1, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v2, "MotionEvent onScaleBegin "

    .line 21
    .line 22
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->A:Z

    .line 29
    .line 30
    return v1

    .line 31
    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 32
    return p1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v1, "MotionEvent onScaleEnd "

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const/4 p1, 0x0

    .line 19
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->A:Z

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8

    .line 1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v0, "MotionEvent onScroll "

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, ","

    .line 15
    .line 16
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    iget p1, p0, Lcom/intsig/document/widget/PagesView;->d0:I

    .line 23
    .line 24
    const/4 v2, 0x2

    .line 25
    const/4 v3, 0x0

    .line 26
    const/4 v4, 0x0

    .line 27
    const/4 v5, 0x1

    .line 28
    if-eq p1, v5, :cond_e

    .line 29
    .line 30
    iget-object v6, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 31
    .line 32
    if-eqz v6, :cond_e

    .line 33
    .line 34
    iget-object v0, v6, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 35
    .line 36
    iget v1, p0, Lcom/intsig/document/widget/PagesView;->u:F

    .line 37
    .line 38
    div-float/2addr p3, v1

    .line 39
    div-float/2addr p4, v1

    .line 40
    if-ne p1, v2, :cond_5

    .line 41
    .line 42
    neg-float p1, p3

    .line 43
    neg-float p2, p4

    .line 44
    invoke-virtual {v0, p1, p2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->move(FF)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$AnnotBox;->center()Landroid/graphics/PointF;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    iget-object p2, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 52
    .line 53
    iget p3, v0, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 54
    .line 55
    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 56
    .line 57
    .line 58
    move-result-object p2

    .line 59
    check-cast p2, Lcom/intsig/document/widget/PagesView$s;

    .line 60
    .line 61
    iget-object p2, p2, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    .line 62
    .line 63
    invoke-virtual {p2}, Landroid/util/Size;->getHeight()I

    .line 64
    .line 65
    .line 66
    move-result p2

    .line 67
    int-to-float p2, p2

    .line 68
    iget p3, p1, Landroid/graphics/PointF;->y:F

    .line 69
    .line 70
    cmpg-float p4, p3, v3

    .line 71
    .line 72
    if-ltz p4, :cond_0

    .line 73
    .line 74
    cmpl-float p3, p3, p2

    .line 75
    .line 76
    if-lez p3, :cond_4

    .line 77
    .line 78
    :cond_0
    new-instance p3, Landroid/graphics/PointF;

    .line 79
    .line 80
    iget-object p4, v0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 81
    .line 82
    iget v1, p4, Landroid/graphics/RectF;->left:F

    .line 83
    .line 84
    iget p4, p4, Landroid/graphics/RectF;->top:F

    .line 85
    .line 86
    invoke-direct {p3, v1, p4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 87
    .line 88
    .line 89
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 90
    .line 91
    cmpl-float p4, p1, p2

    .line 92
    .line 93
    if-lez p4, :cond_1

    .line 94
    .line 95
    const/4 p4, 0x1

    .line 96
    goto :goto_0

    .line 97
    :cond_1
    const/4 p4, 0x0

    .line 98
    :goto_0
    iget v1, v0, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 99
    .line 100
    iget v2, p0, Lcom/intsig/document/widget/PagesView;->l:I

    .line 101
    .line 102
    sub-int/2addr v2, v5

    .line 103
    if-ge v1, v2, :cond_2

    .line 104
    .line 105
    const/4 v4, 0x1

    .line 106
    :cond_2
    and-int/2addr p4, v4

    .line 107
    if-eqz p4, :cond_3

    .line 108
    .line 109
    add-int/2addr v1, v5

    .line 110
    iget p1, p3, Landroid/graphics/PointF;->x:F

    .line 111
    .line 112
    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 113
    .line 114
    sub-float/2addr p2, p3

    .line 115
    neg-float p2, p2

    .line 116
    :goto_1
    invoke-virtual {v0, v1, p1, p2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->place(IFF)V

    .line 117
    .line 118
    .line 119
    goto :goto_2

    .line 120
    :cond_3
    cmpg-float p1, p1, v3

    .line 121
    .line 122
    if-gez p1, :cond_4

    .line 123
    .line 124
    if-lez v1, :cond_4

    .line 125
    .line 126
    sub-int/2addr v1, v5

    .line 127
    iget p1, p3, Landroid/graphics/PointF;->x:F

    .line 128
    .line 129
    iget-object p2, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 130
    .line 131
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 132
    .line 133
    .line 134
    move-result-object p2

    .line 135
    check-cast p2, Lcom/intsig/document/widget/PagesView$s;

    .line 136
    .line 137
    iget-object p2, p2, Lcom/intsig/document/widget/PagesView$s;->〇080:Landroid/util/Size;

    .line 138
    .line 139
    invoke-virtual {p2}, Landroid/util/Size;->getHeight()I

    .line 140
    .line 141
    .line 142
    move-result p2

    .line 143
    int-to-float p2, p2

    .line 144
    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 145
    .line 146
    add-float/2addr p2, p3

    .line 147
    goto :goto_1

    .line 148
    :cond_4
    :goto_2
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 149
    .line 150
    if-eqz p1, :cond_d

    .line 151
    .line 152
    iget-object p2, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 153
    .line 154
    sget-object p3, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Down:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 155
    .line 156
    if-ne p2, p3, :cond_d

    .line 157
    .line 158
    sget-object p2, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Move:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 159
    .line 160
    iput-object p2, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 161
    .line 162
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 163
    .line 164
    invoke-interface {p1, p3, p2}, Lcom/intsig/document/widget/PagesView$k;->onElementDataFocus(Lcom/intsig/document/widget/PagesView$ElementData;Lcom/intsig/document/widget/PagesView$AnnotFocusState;)V

    .line 165
    .line 166
    .line 167
    goto/16 :goto_3

    .line 168
    .line 169
    :cond_5
    const/4 v1, 0x4

    .line 170
    if-ne p1, v1, :cond_9

    .line 171
    .line 172
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 173
    .line 174
    if-eqz p1, :cond_6

    .line 175
    .line 176
    iget-object p2, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 177
    .line 178
    sget-object v1, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Down:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 179
    .line 180
    if-ne p2, v1, :cond_6

    .line 181
    .line 182
    sget-object p2, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Resize:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 183
    .line 184
    iput-object p2, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 185
    .line 186
    invoke-interface {p1, v6, p2}, Lcom/intsig/document/widget/PagesView$k;->onElementDataFocus(Lcom/intsig/document/widget/PagesView$ElementData;Lcom/intsig/document/widget/PagesView$AnnotFocusState;)V

    .line 187
    .line 188
    .line 189
    :cond_6
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 190
    .line 191
    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$ElementData;->args:Lcom/intsig/document/widget/PagesView$BaseArgs;

    .line 192
    .line 193
    instance-of p2, p1, Lcom/intsig/document/widget/PagesView$TextArgs;

    .line 194
    .line 195
    if-eqz p2, :cond_8

    .line 196
    .line 197
    check-cast p1, Lcom/intsig/document/widget/PagesView$TextArgs;

    .line 198
    .line 199
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$AnnotBox;->width()F

    .line 200
    .line 201
    .line 202
    move-result p2

    .line 203
    sub-float/2addr p2, p3

    .line 204
    invoke-virtual {p0, p1, p2}, Lcom/intsig/document/widget/PagesView;->a(Lcom/intsig/document/widget/PagesView$TextArgs;F)Landroid/util/Size;

    .line 205
    .line 206
    .line 207
    move-result-object p2

    .line 208
    invoke-virtual {p2}, Landroid/util/Size;->getHeight()I

    .line 209
    .line 210
    .line 211
    move-result p2

    .line 212
    int-to-float p2, p2

    .line 213
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$AnnotBox;->height()F

    .line 214
    .line 215
    .line 216
    move-result v1

    .line 217
    sub-float/2addr v1, p4

    .line 218
    cmpg-float p2, p2, v1

    .line 219
    .line 220
    if-gtz p2, :cond_7

    .line 221
    .line 222
    neg-float p2, p3

    .line 223
    neg-float p3, p4

    .line 224
    invoke-virtual {v0, p2, p3}, Lcom/intsig/document/widget/PagesView$AnnotBox;->resize(FF)V

    .line 225
    .line 226
    .line 227
    :cond_7
    new-instance p2, Lcom/intsig/document/widget/PagesView$y;

    .line 228
    .line 229
    invoke-direct {p2}, Lcom/intsig/document/widget/PagesView$y;-><init>()V

    .line 230
    .line 231
    .line 232
    iget-object p3, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->text:Ljava/lang/String;

    .line 233
    .line 234
    iget p4, p1, Lcom/intsig/document/widget/PagesView$TextArgs;->fontSize:F

    .line 235
    .line 236
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$AnnotBox;->width()F

    .line 237
    .line 238
    .line 239
    move-result v0

    .line 240
    new-instance v1, Landroid/graphics/Paint;

    .line 241
    .line 242
    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 243
    .line 244
    .line 245
    invoke-virtual {v1, p4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 246
    .line 247
    .line 248
    invoke-virtual {p2, p3, v1, v0}, Lcom/intsig/document/widget/PagesView$y;->〇080(Ljava/lang/String;Landroid/graphics/Paint;F)Ljava/util/List;

    .line 249
    .line 250
    .line 251
    move-result-object p2

    .line 252
    invoke-virtual {p1, p2}, Lcom/intsig/document/widget/PagesView$TextArgs;->setLayoutText(Ljava/util/List;)V

    .line 253
    .line 254
    .line 255
    goto :goto_3

    .line 256
    :cond_8
    neg-float p1, p3

    .line 257
    neg-float p2, p4

    .line 258
    const/high16 p3, 0x41200000    # 10.0f

    .line 259
    .line 260
    invoke-virtual {v0, p1, p2, p3, p3}, Lcom/intsig/document/widget/PagesView$AnnotBox;->checkSize(FFFF)Z

    .line 261
    .line 262
    .line 263
    move-result p3

    .line 264
    if-eqz p3, :cond_d

    .line 265
    .line 266
    invoke-virtual {v0, p1, p2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->resize(FF)V

    .line 267
    .line 268
    .line 269
    goto :goto_3

    .line 270
    :cond_9
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 271
    .line 272
    if-eqz p1, :cond_a

    .line 273
    .line 274
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 275
    .line 276
    sget-object p4, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Down:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 277
    .line 278
    if-ne p3, p4, :cond_a

    .line 279
    .line 280
    sget-object p3, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Rotate:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 281
    .line 282
    iput-object p3, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 283
    .line 284
    invoke-interface {p1, v6, p3}, Lcom/intsig/document/widget/PagesView$k;->onElementDataFocus(Lcom/intsig/document/widget/PagesView$ElementData;Lcom/intsig/document/widget/PagesView$AnnotFocusState;)V

    .line 285
    .line 286
    .line 287
    :cond_a
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 288
    .line 289
    .line 290
    move-result p1

    .line 291
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 292
    .line 293
    .line 294
    move-result p2

    .line 295
    new-instance p3, Landroid/graphics/PointF;

    .line 296
    .line 297
    invoke-direct {p3, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 298
    .line 299
    .line 300
    invoke-virtual {p0, p3}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/PointF;)I

    .line 301
    .line 302
    .line 303
    iget p1, p3, Landroid/graphics/PointF;->x:F

    .line 304
    .line 305
    iget p2, p3, Landroid/graphics/PointF;->y:F

    .line 306
    .line 307
    iget-object p3, v0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 308
    .line 309
    invoke-virtual {p3}, Landroid/graphics/RectF;->centerX()F

    .line 310
    .line 311
    .line 312
    move-result p3

    .line 313
    iget-object p4, v0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 314
    .line 315
    invoke-virtual {p4}, Landroid/graphics/RectF;->centerY()F

    .line 316
    .line 317
    .line 318
    move-result p4

    .line 319
    sub-float v1, p3, p1

    .line 320
    .line 321
    sub-float v2, p4, p2

    .line 322
    .line 323
    div-float/2addr v1, v2

    .line 324
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 325
    .line 326
    .line 327
    move-result v1

    .line 328
    float-to-double v1, v1

    .line 329
    invoke-static {v1, v2}, Ljava/lang/Math;->atan(D)D

    .line 330
    .line 331
    .line 332
    move-result-wide v1

    .line 333
    const-wide v3, 0x4066800000000000L    # 180.0

    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    mul-double v1, v1, v3

    .line 339
    .line 340
    const-wide v6, 0x400921fb54442d18L    # Math.PI

    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    div-double/2addr v1, v6

    .line 346
    cmpl-float p2, p2, p4

    .line 347
    .line 348
    if-lez p2, :cond_b

    .line 349
    .line 350
    sub-double v1, v3, v1

    .line 351
    .line 352
    :cond_b
    cmpg-float p1, p1, p3

    .line 353
    .line 354
    if-gez p1, :cond_c

    .line 355
    .line 356
    neg-double v1, v1

    .line 357
    :cond_c
    double-to-int p1, v1

    .line 358
    invoke-virtual {v0, p1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->setRotate(I)V

    .line 359
    .line 360
    .line 361
    :cond_d
    :goto_3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 362
    .line 363
    .line 364
    return v5

    .line 365
    :cond_e
    iget-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->U:Z

    .line 366
    .line 367
    if-eqz p1, :cond_f

    .line 368
    .line 369
    return v5

    .line 370
    :cond_f
    iput-boolean v5, p0, Lcom/intsig/document/widget/PagesView;->B:Z

    .line 371
    .line 372
    iget-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->q:Z

    .line 373
    .line 374
    if-eqz p1, :cond_10

    .line 375
    .line 376
    iget p1, p0, Lcom/intsig/document/widget/PagesView;->y:F

    .line 377
    .line 378
    iget p3, p0, Lcom/intsig/document/widget/PagesView;->u:F

    .line 379
    .line 380
    mul-float p1, p1, p3

    .line 381
    .line 382
    iget p3, p0, Lcom/intsig/document/widget/PagesView;->l:I

    .line 383
    .line 384
    add-int/2addr p3, v5

    .line 385
    int-to-float p3, p3

    .line 386
    iget p4, p0, Lcom/intsig/document/widget/PagesView;->i:F

    .line 387
    .line 388
    mul-float p3, p3, p4

    .line 389
    .line 390
    add-float/2addr p3, p1

    .line 391
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 392
    .line 393
    .line 394
    move-result p1

    .line 395
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 396
    .line 397
    .line 398
    move-result p2

    .line 399
    int-to-float p2, p2

    .line 400
    mul-float p3, p3, p1

    .line 401
    .line 402
    div-float/2addr p3, p2

    .line 403
    iget p1, p0, Lcom/intsig/document/widget/PagesView;->w:F

    .line 404
    .line 405
    sub-float/2addr p3, p1

    .line 406
    invoke-virtual {p0, v3, p3}, Lcom/intsig/document/widget/PagesView;->a(FF)Z

    .line 407
    .line 408
    .line 409
    iput-boolean v5, p0, Lcom/intsig/document/widget/PagesView;->C:Z

    .line 410
    .line 411
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 412
    .line 413
    .line 414
    return v5

    .line 415
    :cond_10
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 416
    .line 417
    invoke-virtual {p1}, Lcom/intsig/document/widget/PagesView$a0;->O8()Z

    .line 418
    .line 419
    .line 420
    move-result p1

    .line 421
    const/high16 v3, 0x40000000    # 2.0f

    .line 422
    .line 423
    if-eqz p1, :cond_1a

    .line 424
    .line 425
    iget p1, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 426
    .line 427
    if-eq p1, v5, :cond_1a

    .line 428
    .line 429
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 430
    .line 431
    .line 432
    move-result p1

    .line 433
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 434
    .line 435
    .line 436
    move-result p2

    .line 437
    new-instance p3, Ljava/lang/StringBuilder;

    .line 438
    .line 439
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 440
    .line 441
    .line 442
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    .line 444
    .line 445
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 446
    .line 447
    .line 448
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 449
    .line 450
    .line 451
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 452
    .line 453
    .line 454
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 455
    .line 456
    invoke-virtual {p3}, Lcom/intsig/document/widget/PagesView$a0;->〇080()Lcom/intsig/document/DocumentLoader$d;

    .line 457
    .line 458
    .line 459
    move-result-object p3

    .line 460
    iget-object p3, p3, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    .line 461
    .line 462
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    .line 463
    .line 464
    .line 465
    move-result p3

    .line 466
    div-float/2addr p3, v3

    .line 467
    iget p4, p0, Lcom/intsig/document/widget/PagesView;->u:F

    .line 468
    .line 469
    mul-float p3, p3, p4

    .line 470
    .line 471
    new-instance p4, Landroid/graphics/PointF;

    .line 472
    .line 473
    iget v0, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 474
    .line 475
    if-ne v0, v2, :cond_11

    .line 476
    .line 477
    const/high16 v0, 0x41f00000    # 30.0f

    .line 478
    .line 479
    goto :goto_4

    .line 480
    :cond_11
    const/high16 v0, -0x3e100000    # -30.0f

    .line 481
    .line 482
    :goto_4
    add-float/2addr p1, v0

    .line 483
    const/high16 v0, 0x42480000    # 50.0f

    .line 484
    .line 485
    sub-float/2addr p2, v0

    .line 486
    sub-float/2addr p2, p3

    .line 487
    invoke-direct {p4, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 488
    .line 489
    .line 490
    invoke-virtual {p0, p4}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/PointF;)I

    .line 491
    .line 492
    .line 493
    move-result p1

    .line 494
    const-string p2, "MotionEvent onScroll pos "

    .line 495
    .line 496
    invoke-static {p2}, La/a;->〇080(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 497
    .line 498
    .line 499
    move-result-object p2

    .line 500
    iget p3, p4, Landroid/graphics/PointF;->x:F

    .line 501
    .line 502
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 503
    .line 504
    .line 505
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    .line 507
    .line 508
    iget p3, p4, Landroid/graphics/PointF;->y:F

    .line 509
    .line 510
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 511
    .line 512
    .line 513
    const-string p3, " at page "

    .line 514
    .line 515
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    .line 517
    .line 518
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 519
    .line 520
    .line 521
    iget-object p2, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 522
    .line 523
    iget p3, p4, Landroid/graphics/PointF;->x:F

    .line 524
    .line 525
    iget p4, p4, Landroid/graphics/PointF;->y:F

    .line 526
    .line 527
    invoke-virtual {p2, p1, p3, p4, v4}, Lb/a;->TextAtPos(IFFZ)Lcom/intsig/document/DocumentLoader$d;

    .line 528
    .line 529
    .line 530
    move-result-object p2

    .line 531
    if-eqz p2, :cond_19

    .line 532
    .line 533
    iget p3, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 534
    .line 535
    if-ne p3, v2, :cond_12

    .line 536
    .line 537
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 538
    .line 539
    invoke-virtual {p3}, Lcom/intsig/document/widget/PagesView$a0;->〇080()Lcom/intsig/document/DocumentLoader$d;

    .line 540
    .line 541
    .line 542
    move-result-object p3

    .line 543
    iget p3, p3, Lcom/intsig/document/DocumentLoader$d;->〇o00〇〇Oo:I

    .line 544
    .line 545
    iget p4, p2, Lcom/intsig/document/DocumentLoader$d;->〇o00〇〇Oo:I

    .line 546
    .line 547
    if-eq p3, p4, :cond_13

    .line 548
    .line 549
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 550
    .line 551
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 552
    .line 553
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$a0;->Oo08()Lcom/intsig/document/DocumentLoader$d;

    .line 554
    .line 555
    .line 556
    move-result-object v0

    .line 557
    iget v0, v0, Lcom/intsig/document/DocumentLoader$d;->〇o00〇〇Oo:I

    .line 558
    .line 559
    invoke-virtual {p3, p1, p4, v0}, Lb/a;->SelectText(III)Ljava/util/ArrayList;

    .line 560
    .line 561
    .line 562
    move-result-object p1

    .line 563
    iget p2, p2, Lcom/intsig/document/DocumentLoader$d;->〇o00〇〇Oo:I

    .line 564
    .line 565
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 566
    .line 567
    invoke-virtual {p3}, Lcom/intsig/document/widget/PagesView$a0;->Oo08()Lcom/intsig/document/DocumentLoader$d;

    .line 568
    .line 569
    .line 570
    move-result-object p3

    .line 571
    iget p3, p3, Lcom/intsig/document/DocumentLoader$d;->〇o00〇〇Oo:I

    .line 572
    .line 573
    if-le p2, p3, :cond_14

    .line 574
    .line 575
    const/4 p2, 0x3

    .line 576
    iput p2, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 577
    .line 578
    goto :goto_5

    .line 579
    :cond_12
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 580
    .line 581
    invoke-virtual {p3}, Lcom/intsig/document/widget/PagesView$a0;->Oo08()Lcom/intsig/document/DocumentLoader$d;

    .line 582
    .line 583
    .line 584
    move-result-object p3

    .line 585
    iget p3, p3, Lcom/intsig/document/DocumentLoader$d;->〇o00〇〇Oo:I

    .line 586
    .line 587
    iget p4, p2, Lcom/intsig/document/DocumentLoader$d;->〇o00〇〇Oo:I

    .line 588
    .line 589
    if-eq p3, p4, :cond_13

    .line 590
    .line 591
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView;->r:Lb/a;

    .line 592
    .line 593
    iget-object p4, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 594
    .line 595
    invoke-virtual {p4}, Lcom/intsig/document/widget/PagesView$a0;->〇080()Lcom/intsig/document/DocumentLoader$d;

    .line 596
    .line 597
    .line 598
    move-result-object p4

    .line 599
    iget p4, p4, Lcom/intsig/document/DocumentLoader$d;->〇o00〇〇Oo:I

    .line 600
    .line 601
    iget v0, p2, Lcom/intsig/document/DocumentLoader$d;->〇o00〇〇Oo:I

    .line 602
    .line 603
    invoke-virtual {p3, p1, p4, v0}, Lb/a;->SelectText(III)Ljava/util/ArrayList;

    .line 604
    .line 605
    .line 606
    move-result-object p1

    .line 607
    iget p2, p2, Lcom/intsig/document/DocumentLoader$d;->〇o00〇〇Oo:I

    .line 608
    .line 609
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 610
    .line 611
    invoke-virtual {p3}, Lcom/intsig/document/widget/PagesView$a0;->〇080()Lcom/intsig/document/DocumentLoader$d;

    .line 612
    .line 613
    .line 614
    move-result-object p3

    .line 615
    iget p3, p3, Lcom/intsig/document/DocumentLoader$d;->〇o00〇〇Oo:I

    .line 616
    .line 617
    if-ge p2, p3, :cond_14

    .line 618
    .line 619
    iput v2, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 620
    .line 621
    goto :goto_5

    .line 622
    :cond_13
    const/4 p1, 0x0

    .line 623
    :cond_14
    :goto_5
    if-eqz p1, :cond_19

    .line 624
    .line 625
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 626
    .line 627
    .line 628
    move-result p2

    .line 629
    if-lez p2, :cond_19

    .line 630
    .line 631
    iget-object p2, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 632
    .line 633
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 634
    .line 635
    .line 636
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 637
    .line 638
    .line 639
    move-result-object p3

    .line 640
    check-cast p3, Lcom/intsig/document/DocumentLoader$d;

    .line 641
    .line 642
    iget p3, p3, Lcom/intsig/document/DocumentLoader$d;->〇o〇:I

    .line 643
    .line 644
    iput p3, p2, Lcom/intsig/document/widget/PagesView$a0;->〇o00〇〇Oo:I

    .line 645
    .line 646
    iput-object p1, p2, Lcom/intsig/document/widget/PagesView$a0;->〇080:Ljava/util/ArrayList;

    .line 647
    .line 648
    iget-object p1, p2, Lcom/intsig/document/widget/PagesView$a0;->〇o〇:Ljava/util/ArrayList;

    .line 649
    .line 650
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 651
    .line 652
    .line 653
    iget-object p1, p2, Lcom/intsig/document/widget/PagesView$a0;->〇080:Ljava/util/ArrayList;

    .line 654
    .line 655
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 656
    .line 657
    .line 658
    move-result-object p1

    .line 659
    check-cast p1, Lcom/intsig/document/DocumentLoader$d;

    .line 660
    .line 661
    new-instance p3, Landroid/graphics/RectF;

    .line 662
    .line 663
    iget-object p4, p1, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    .line 664
    .line 665
    invoke-direct {p3, p4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 666
    .line 667
    .line 668
    const/4 p4, 0x1

    .line 669
    :goto_6
    iget-object v0, p2, Lcom/intsig/document/widget/PagesView$a0;->〇080:Ljava/util/ArrayList;

    .line 670
    .line 671
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 672
    .line 673
    .line 674
    move-result v0

    .line 675
    if-ge p4, v0, :cond_18

    .line 676
    .line 677
    iget-object v0, p2, Lcom/intsig/document/widget/PagesView$a0;->〇080:Ljava/util/ArrayList;

    .line 678
    .line 679
    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 680
    .line 681
    .line 682
    move-result-object v0

    .line 683
    check-cast v0, Lcom/intsig/document/DocumentLoader$d;

    .line 684
    .line 685
    iget-object v1, v0, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    .line 686
    .line 687
    iget v2, v1, Landroid/graphics/RectF;->top:F

    .line 688
    .line 689
    iget-object p1, p1, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    .line 690
    .line 691
    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    .line 692
    .line 693
    cmpl-float v3, v2, v3

    .line 694
    .line 695
    if-lez v3, :cond_15

    .line 696
    .line 697
    iget p1, p1, Landroid/graphics/RectF;->right:F

    .line 698
    .line 699
    iput p1, p3, Landroid/graphics/RectF;->right:F

    .line 700
    .line 701
    iget-object p1, p2, Lcom/intsig/document/widget/PagesView$a0;->〇o〇:Ljava/util/ArrayList;

    .line 702
    .line 703
    new-instance v1, Landroid/graphics/RectF;

    .line 704
    .line 705
    invoke-direct {v1, p3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 706
    .line 707
    .line 708
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 709
    .line 710
    .line 711
    new-instance p1, Landroid/graphics/RectF;

    .line 712
    .line 713
    iget-object p3, v0, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    .line 714
    .line 715
    invoke-direct {p1, p3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 716
    .line 717
    .line 718
    move-object p3, p1

    .line 719
    goto :goto_7

    .line 720
    :cond_15
    iget p1, p3, Landroid/graphics/RectF;->top:F

    .line 721
    .line 722
    cmpl-float p1, p1, v2

    .line 723
    .line 724
    if-lez p1, :cond_16

    .line 725
    .line 726
    iput v2, p3, Landroid/graphics/RectF;->top:F

    .line 727
    .line 728
    :cond_16
    iget p1, p3, Landroid/graphics/RectF;->bottom:F

    .line 729
    .line 730
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    .line 731
    .line 732
    cmpg-float p1, p1, v1

    .line 733
    .line 734
    if-gez p1, :cond_17

    .line 735
    .line 736
    iput v1, p3, Landroid/graphics/RectF;->bottom:F

    .line 737
    .line 738
    :cond_17
    :goto_7
    add-int/lit8 p4, p4, 0x1

    .line 739
    .line 740
    move-object p1, v0

    .line 741
    goto :goto_6

    .line 742
    :cond_18
    iget-object p1, p2, Lcom/intsig/document/widget/PagesView$a0;->〇080:Ljava/util/ArrayList;

    .line 743
    .line 744
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 745
    .line 746
    .line 747
    move-result p4

    .line 748
    sub-int/2addr p4, v5

    .line 749
    invoke-virtual {p1, p4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 750
    .line 751
    .line 752
    move-result-object p1

    .line 753
    check-cast p1, Lcom/intsig/document/DocumentLoader$d;

    .line 754
    .line 755
    iget-object p1, p1, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    .line 756
    .line 757
    iget p4, p1, Landroid/graphics/RectF;->right:F

    .line 758
    .line 759
    iput p4, p3, Landroid/graphics/RectF;->right:F

    .line 760
    .line 761
    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    .line 762
    .line 763
    iput p1, p3, Landroid/graphics/RectF;->bottom:F

    .line 764
    .line 765
    iget-object p1, p2, Lcom/intsig/document/widget/PagesView$a0;->〇o〇:Ljava/util/ArrayList;

    .line 766
    .line 767
    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 768
    .line 769
    .line 770
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 771
    .line 772
    .line 773
    :cond_19
    return v5

    .line 774
    :cond_1a
    iput-boolean v5, p0, Lcom/intsig/document/widget/PagesView;->C:Z

    .line 775
    .line 776
    neg-float p1, p3

    .line 777
    invoke-virtual {p0, p1, p4}, Lcom/intsig/document/widget/PagesView;->a(FF)Z

    .line 778
    .line 779
    .line 780
    move-result p1

    .line 781
    if-eqz p1, :cond_1c

    .line 782
    .line 783
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 784
    .line 785
    .line 786
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 787
    .line 788
    invoke-virtual {p1}, Lcom/intsig/document/widget/PagesView$a0;->O8()Z

    .line 789
    .line 790
    .line 791
    move-result p1

    .line 792
    if-eqz p1, :cond_1b

    .line 793
    .line 794
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 795
    .line 796
    invoke-virtual {p1}, Lcom/intsig/document/widget/PagesView$a0;->〇o〇()Ljava/lang/String;

    .line 797
    .line 798
    .line 799
    move-result-object p1

    .line 800
    iget-object p2, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 801
    .line 802
    iget p3, p2, Lcom/intsig/document/widget/PagesView$a0;->〇o00〇〇Oo:I

    .line 803
    .line 804
    invoke-virtual {p2}, Lcom/intsig/document/widget/PagesView$a0;->〇o00〇〇Oo()Landroid/graphics/PointF;

    .line 805
    .line 806
    .line 807
    move-result-object p2

    .line 808
    invoke-virtual {p0, p3, p2}, Lcom/intsig/document/widget/PagesView;->a(ILandroid/graphics/PointF;)Landroid/graphics/PointF;

    .line 809
    .line 810
    .line 811
    move-result-object p2

    .line 812
    invoke-virtual {p0, p1, p2}, Lcom/intsig/document/widget/PagesView;->a(Ljava/lang/String;Landroid/graphics/PointF;)V

    .line 813
    .line 814
    .line 815
    :cond_1b
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->b:La/b;

    .line 816
    .line 817
    if-eqz p1, :cond_1c

    .line 818
    .line 819
    iget p1, p1, La/b;->pageNum:I

    .line 820
    .line 821
    new-instance p2, Landroid/graphics/PointF;

    .line 822
    .line 823
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView;->b:La/b;

    .line 824
    .line 825
    iget-object p3, p3, La/b;->rect:Landroid/graphics/RectF;

    .line 826
    .line 827
    iget p4, p3, Landroid/graphics/RectF;->left:F

    .line 828
    .line 829
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    .line 830
    .line 831
    .line 832
    move-result p3

    .line 833
    div-float/2addr p3, v3

    .line 834
    add-float/2addr p3, p4

    .line 835
    iget-object p4, p0, Lcom/intsig/document/widget/PagesView;->b:La/b;

    .line 836
    .line 837
    iget-object p4, p4, La/b;->rect:Landroid/graphics/RectF;

    .line 838
    .line 839
    iget p4, p4, Landroid/graphics/RectF;->top:F

    .line 840
    .line 841
    invoke-direct {p2, p3, p4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 842
    .line 843
    .line 844
    invoke-virtual {p0, p1, p2}, Lcom/intsig/document/widget/PagesView;->a(ILandroid/graphics/PointF;)Landroid/graphics/PointF;

    .line 845
    .line 846
    .line 847
    move-result-object p1

    .line 848
    iget-object p2, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 849
    .line 850
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView;->b:La/b;

    .line 851
    .line 852
    invoke-interface {p2, p3, p1}, Lcom/intsig/document/widget/PagesView$k;->onAnnotClick(La/b;Landroid/graphics/PointF;)V

    .line 853
    .line 854
    .line 855
    :cond_1c
    return v5
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/document/widget/PagesView$a0;->O8()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 11
    .line 12
    iget-object v0, p1, Lcom/intsig/document/widget/PagesView$a0;->〇080:Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 15
    .line 16
    .line 17
    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$a0;->〇o〇:Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 20
    .line 21
    .line 22
    iput v1, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 23
    .line 24
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->w0:Lcom/intsig/document/widget/PagesView$z;

    .line 28
    .line 29
    if-eqz p1, :cond_0

    .line 30
    .line 31
    invoke-interface {p1}, Lcom/intsig/document/widget/PagesView$z;->onSelectionClose()V

    .line 32
    .line 33
    .line 34
    :cond_0
    return v1

    .line 35
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    new-instance v2, Landroid/graphics/PointF;

    .line 44
    .line 45
    invoke-direct {v2, v0, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0, v2}, Lcom/intsig/document/widget/PagesView;->a(Landroid/graphics/PointF;)I

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    iget-boolean v4, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    .line 53
    .line 54
    const/4 v5, 0x0

    .line 55
    if-eqz v4, :cond_3

    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 58
    .line 59
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    sub-int/2addr v0, v1

    .line 64
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    check-cast p1, Lcom/intsig/document/widget/PagesView$ElementData;

    .line 69
    .line 70
    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 71
    .line 72
    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 73
    .line 74
    iget v0, v2, Landroid/graphics/PointF;->x:F

    .line 75
    .line 76
    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 77
    .line 78
    invoke-virtual {p1, v3, v0, v2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->place(IFF)V

    .line 79
    .line 80
    .line 81
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 82
    .line 83
    if-eqz p1, :cond_2

    .line 84
    .line 85
    new-instance v0, Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 86
    .line 87
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 88
    .line 89
    iget-object v2, v2, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 90
    .line 91
    invoke-direct {v0, v2}, Lcom/intsig/document/widget/PagesView$AnnotBox;-><init>(Lcom/intsig/document/widget/PagesView$AnnotBox;)V

    .line 92
    .line 93
    .line 94
    invoke-interface {p1, v0}, Lcom/intsig/document/widget/PagesView$k;->onAnnoBoxPlace(Lcom/intsig/document/widget/PagesView$AnnotBox;)V

    .line 95
    .line 96
    .line 97
    :cond_2
    iput-boolean v5, p0, Lcom/intsig/document/widget/PagesView;->a0:Z

    .line 98
    .line 99
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 100
    .line 101
    .line 102
    return v1

    .line 103
    :cond_3
    iget-object v4, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 104
    .line 105
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 106
    .line 107
    .line 108
    move-result-object v4

    .line 109
    const/4 v6, 0x0

    .line 110
    :cond_4
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 111
    .line 112
    .line 113
    move-result v7

    .line 114
    const/4 v8, 0x2

    .line 115
    const/4 v9, 0x0

    .line 116
    if-eqz v7, :cond_9

    .line 117
    .line 118
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 119
    .line 120
    .line 121
    move-result-object v7

    .line 122
    check-cast v7, Lcom/intsig/document/widget/PagesView$ElementData;

    .line 123
    .line 124
    iget-object v10, v7, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 125
    .line 126
    iget-object v11, v7, Lcom/intsig/document/widget/PagesView$ElementData;->args:Lcom/intsig/document/widget/PagesView$BaseArgs;

    .line 127
    .line 128
    iget-boolean v11, v11, Lcom/intsig/document/widget/PagesView$BaseArgs;->deleteable:Z

    .line 129
    .line 130
    invoke-virtual {p0, v10, v0, p1, v11}, Lcom/intsig/document/widget/PagesView;->a(Lcom/intsig/document/widget/PagesView$AnnotBox;FFZ)I

    .line 131
    .line 132
    .line 133
    move-result v10

    .line 134
    if-eq v10, v1, :cond_4

    .line 135
    .line 136
    sget-object v6, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->None:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 137
    .line 138
    iput-object v6, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 139
    .line 140
    iput-object v7, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 141
    .line 142
    if-ne v10, v8, :cond_6

    .line 143
    .line 144
    iget-object v6, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 145
    .line 146
    if-eqz v6, :cond_5

    .line 147
    .line 148
    invoke-interface {v6}, Lcom/intsig/document/widget/PagesView$k;->onAnnotBoxClick()V

    .line 149
    .line 150
    .line 151
    iget-object v6, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 152
    .line 153
    sget-object v8, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Tap:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 154
    .line 155
    invoke-interface {v6, v7, v8}, Lcom/intsig/document/widget/PagesView$k;->onElementDataFocus(Lcom/intsig/document/widget/PagesView$ElementData;Lcom/intsig/document/widget/PagesView$AnnotFocusState;)V

    .line 156
    .line 157
    .line 158
    :cond_5
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 159
    .line 160
    .line 161
    goto :goto_1

    .line 162
    :cond_6
    const/4 v6, 0x3

    .line 163
    if-ne v10, v6, :cond_8

    .line 164
    .line 165
    iput-object v9, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 166
    .line 167
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 168
    .line 169
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 170
    .line 171
    .line 172
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 173
    .line 174
    if-eqz p1, :cond_7

    .line 175
    .line 176
    invoke-interface {p1, v7}, Lcom/intsig/document/widget/PagesView$k;->onElementDataDelete(Lcom/intsig/document/widget/PagesView$ElementData;)V

    .line 177
    .line 178
    .line 179
    :cond_7
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 180
    .line 181
    .line 182
    const/4 v6, 0x1

    .line 183
    goto :goto_2

    .line 184
    :cond_8
    :goto_1
    const/4 v6, 0x1

    .line 185
    goto :goto_0

    .line 186
    :cond_9
    :goto_2
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 187
    .line 188
    if-nez p1, :cond_a

    .line 189
    .line 190
    if-nez v6, :cond_a

    .line 191
    .line 192
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 193
    .line 194
    if-eqz p1, :cond_a

    .line 195
    .line 196
    invoke-interface {p1}, Lcom/intsig/document/widget/PagesView$k;->onTap()V

    .line 197
    .line 198
    .line 199
    :cond_a
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->b0:Ljava/util/ArrayList;

    .line 200
    .line 201
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 202
    .line 203
    .line 204
    move-result p1

    .line 205
    if-lez p1, :cond_b

    .line 206
    .line 207
    return v1

    .line 208
    :cond_b
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 209
    .line 210
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 211
    .line 212
    .line 213
    move-result-object p1

    .line 214
    check-cast p1, Lcom/intsig/document/widget/PagesView$s;

    .line 215
    .line 216
    iget v0, v2, Landroid/graphics/PointF;->x:F

    .line 217
    .line 218
    iget v4, v2, Landroid/graphics/PointF;->y:F

    .line 219
    .line 220
    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$s;->O8:Ljava/util/ArrayList;

    .line 221
    .line 222
    if-eqz p1, :cond_e

    .line 223
    .line 224
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 225
    .line 226
    .line 227
    move-result-object p1

    .line 228
    :cond_c
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 229
    .line 230
    .line 231
    move-result v6

    .line 232
    if-eqz v6, :cond_e

    .line 233
    .line 234
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 235
    .line 236
    .line 237
    move-result-object v6

    .line 238
    check-cast v6, La/d;

    .line 239
    .line 240
    iget-object v7, v6, La/d;->regions:Ljava/util/ArrayList;

    .line 241
    .line 242
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 243
    .line 244
    .line 245
    move-result-object v7

    .line 246
    :cond_d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    .line 247
    .line 248
    .line 249
    move-result v10

    .line 250
    if-eqz v10, :cond_c

    .line 251
    .line 252
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 253
    .line 254
    .line 255
    move-result-object v10

    .line 256
    check-cast v10, Landroid/graphics/RectF;

    .line 257
    .line 258
    invoke-virtual {v10, v0, v4}, Landroid/graphics/RectF;->contains(FF)Z

    .line 259
    .line 260
    .line 261
    move-result v10

    .line 262
    if-eqz v10, :cond_d

    .line 263
    .line 264
    goto :goto_3

    .line 265
    :cond_e
    move-object v6, v9

    .line 266
    :goto_3
    const/high16 p1, 0x3f800000    # 1.0f

    .line 267
    .line 268
    if-eqz v6, :cond_10

    .line 269
    .line 270
    invoke-virtual {v6}, La/d;->isUrlLink()Z

    .line 271
    .line 272
    .line 273
    move-result v0

    .line 274
    if-eqz v0, :cond_f

    .line 275
    .line 276
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->w0:Lcom/intsig/document/widget/PagesView$z;

    .line 277
    .line 278
    iget-object v0, v6, La/d;->link:Ljava/lang/String;

    .line 279
    .line 280
    iget-object v1, v6, La/d;->text:Ljava/lang/String;

    .line 281
    .line 282
    invoke-interface {p1, v0, v1}, Lcom/intsig/document/widget/PagesView$z;->onLinkClick(Ljava/lang/String;Ljava/lang/String;)Z

    .line 283
    .line 284
    .line 285
    goto/16 :goto_5

    .line 286
    .line 287
    :cond_f
    iget v0, v6, La/d;->pageNum:I

    .line 288
    .line 289
    new-instance v2, Landroid/graphics/RectF;

    .line 290
    .line 291
    iget v3, v6, La/d;->x:F

    .line 292
    .line 293
    iget v4, v6, La/d;->y:F

    .line 294
    .line 295
    add-float v6, v3, p1

    .line 296
    .line 297
    add-float/2addr p1, v4

    .line 298
    invoke-direct {v2, v3, v4, v6, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 299
    .line 300
    .line 301
    invoke-virtual {p0, v0, v2, v1}, Lcom/intsig/document/widget/PagesView;->ScrollTo(ILandroid/graphics/RectF;Z)V

    .line 302
    .line 303
    .line 304
    goto/16 :goto_5

    .line 305
    .line 306
    :cond_10
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->s:Ljava/util/ArrayList;

    .line 307
    .line 308
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 309
    .line 310
    .line 311
    move-result-object v0

    .line 312
    check-cast v0, Lcom/intsig/document/widget/PagesView$s;

    .line 313
    .line 314
    iget v3, v2, Landroid/graphics/PointF;->x:F

    .line 315
    .line 316
    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 317
    .line 318
    iget-object v0, v0, Lcom/intsig/document/widget/PagesView$s;->Oo08:Ljava/util/ArrayList;

    .line 319
    .line 320
    if-eqz v0, :cond_12

    .line 321
    .line 322
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 323
    .line 324
    .line 325
    move-result-object v0

    .line 326
    :cond_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 327
    .line 328
    .line 329
    move-result v4

    .line 330
    if-eqz v4, :cond_12

    .line 331
    .line 332
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 333
    .line 334
    .line 335
    move-result-object v4

    .line 336
    check-cast v4, La/b;

    .line 337
    .line 338
    iget-object v6, v4, La/b;->rect:Landroid/graphics/RectF;

    .line 339
    .line 340
    invoke-virtual {v6, v3, v2}, Landroid/graphics/RectF;->contains(FF)Z

    .line 341
    .line 342
    .line 343
    move-result v6

    .line 344
    if-eqz v6, :cond_11

    .line 345
    .line 346
    goto :goto_4

    .line 347
    :cond_12
    move-object v4, v9

    .line 348
    :goto_4
    if-eqz v4, :cond_17

    .line 349
    .line 350
    iget v0, v4, La/b;->type:I

    .line 351
    .line 352
    if-ne v0, v1, :cond_13

    .line 353
    .line 354
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->w0:Lcom/intsig/document/widget/PagesView$z;

    .line 355
    .line 356
    iget-object v0, v4, La/b;->text:Ljava/lang/String;

    .line 357
    .line 358
    invoke-interface {p1, v0, v0}, Lcom/intsig/document/widget/PagesView$z;->onTextAnnotClick(Ljava/lang/String;Ljava/lang/String;)Z

    .line 359
    .line 360
    .line 361
    goto :goto_5

    .line 362
    :cond_13
    if-ne v0, v8, :cond_15

    .line 363
    .line 364
    iget-object v0, v4, La/b;->dest:La/b$a;

    .line 365
    .line 366
    iget-object v2, v0, La/b$a;->〇080:Ljava/lang/String;

    .line 367
    .line 368
    if-eqz v2, :cond_14

    .line 369
    .line 370
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->w0:Lcom/intsig/document/widget/PagesView$z;

    .line 371
    .line 372
    iget-object v0, v4, La/b;->text:Ljava/lang/String;

    .line 373
    .line 374
    invoke-interface {p1, v2, v0}, Lcom/intsig/document/widget/PagesView$z;->onLinkClick(Ljava/lang/String;Ljava/lang/String;)Z

    .line 375
    .line 376
    .line 377
    goto :goto_5

    .line 378
    :cond_14
    iget v0, v0, La/b$a;->〇o00〇〇Oo:I

    .line 379
    .line 380
    new-instance v2, Landroid/graphics/RectF;

    .line 381
    .line 382
    iget-object v3, v4, La/b;->dest:La/b$a;

    .line 383
    .line 384
    iget v4, v3, La/b$a;->〇o〇:F

    .line 385
    .line 386
    iget v3, v3, La/b$a;->O8:F

    .line 387
    .line 388
    add-float v6, v4, p1

    .line 389
    .line 390
    add-float/2addr p1, v3

    .line 391
    invoke-direct {v2, v4, v3, v6, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 392
    .line 393
    .line 394
    invoke-virtual {p0, v0, v2, v1}, Lcom/intsig/document/widget/PagesView;->ScrollTo(ILandroid/graphics/RectF;Z)V

    .line 395
    .line 396
    .line 397
    goto :goto_5

    .line 398
    :cond_15
    iget-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->a:Z

    .line 399
    .line 400
    if-eqz p1, :cond_16

    .line 401
    .line 402
    iput-object v4, p0, Lcom/intsig/document/widget/PagesView;->b:La/b;

    .line 403
    .line 404
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 405
    .line 406
    if-eqz p1, :cond_18

    .line 407
    .line 408
    iget p1, v4, La/b;->pageNum:I

    .line 409
    .line 410
    new-instance v0, Landroid/graphics/PointF;

    .line 411
    .line 412
    iget-object v2, v4, La/b;->rect:Landroid/graphics/RectF;

    .line 413
    .line 414
    iget v3, v2, Landroid/graphics/RectF;->left:F

    .line 415
    .line 416
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    .line 417
    .line 418
    .line 419
    move-result v2

    .line 420
    const/high16 v6, 0x40000000    # 2.0f

    .line 421
    .line 422
    div-float/2addr v2, v6

    .line 423
    add-float/2addr v2, v3

    .line 424
    iget-object v3, v4, La/b;->rect:Landroid/graphics/RectF;

    .line 425
    .line 426
    iget v3, v3, Landroid/graphics/RectF;->top:F

    .line 427
    .line 428
    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 429
    .line 430
    .line 431
    invoke-virtual {p0, p1, v0}, Lcom/intsig/document/widget/PagesView;->a(ILandroid/graphics/PointF;)Landroid/graphics/PointF;

    .line 432
    .line 433
    .line 434
    move-result-object p1

    .line 435
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 436
    .line 437
    invoke-interface {v0, v4, p1}, Lcom/intsig/document/widget/PagesView$k;->onAnnotClick(La/b;Landroid/graphics/PointF;)V

    .line 438
    .line 439
    .line 440
    goto :goto_6

    .line 441
    :cond_16
    :goto_5
    const/4 v1, 0x0

    .line 442
    goto :goto_6

    .line 443
    :cond_17
    iput-object v9, p0, Lcom/intsig/document/widget/PagesView;->b:La/b;

    .line 444
    .line 445
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 446
    .line 447
    if-eqz p1, :cond_18

    .line 448
    .line 449
    invoke-interface {p1}, Lcom/intsig/document/widget/PagesView$k;->cancleAnnotClick()V

    .line 450
    .line 451
    .line 452
    :cond_18
    :goto_6
    if-eqz v1, :cond_19

    .line 453
    .line 454
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 455
    .line 456
    .line 457
    :cond_19
    return v5
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method public final onSizeChanged(IIII)V
    .locals 3

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v1, "onSizeChanged "

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v2, ","

    .line 18
    .line 19
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string p4, "->"

    .line 26
    .line 27
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string p2, " "

    .line 40
    .line 41
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    iget p2, p0, Lcom/intsig/document/widget/PagesView;->z:F

    .line 45
    .line 46
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    iget p2, p0, Lcom/intsig/document/widget/PagesView;->z:F

    .line 50
    .line 51
    const/4 p4, 0x0

    .line 52
    cmpl-float v0, p2, p4

    .line 53
    .line 54
    if-lez v0, :cond_1

    .line 55
    .line 56
    if-nez p3, :cond_0

    .line 57
    .line 58
    if-lez p1, :cond_0

    .line 59
    .line 60
    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/PagesView;->a(I)V

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_0
    iget p3, p0, Lcom/intsig/document/widget/PagesView;->S:I

    .line 65
    .line 66
    sub-int/2addr p1, p3

    .line 67
    int-to-float p1, p1

    .line 68
    div-float/2addr p1, p2

    .line 69
    iput p1, p0, Lcom/intsig/document/widget/PagesView;->v:F

    .line 70
    .line 71
    invoke-static {v1}, La/a;->〇080(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    move-result-object p2

    .line 75
    iget p3, p0, Lcom/intsig/document/widget/PagesView;->v:F

    .line 76
    .line 77
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    const-string p3, " sca"

    .line 81
    .line 82
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {p0, p4, p4}, Lcom/intsig/document/widget/PagesView;->a(FF)Z

    .line 89
    .line 90
    .line 91
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 92
    .line 93
    .line 94
    :cond_1
    :goto_0
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const-string v2, "onTouchEvent MotionEvent "

    .line 10
    .line 11
    invoke-static {v2}, La/a;->〇080(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    iget-boolean v2, p0, Lcom/intsig/document/widget/PagesView;->t:Z

    .line 23
    .line 24
    if-nez v2, :cond_0

    .line 25
    .line 26
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    return p1

    .line 31
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    const/4 v3, 0x1

    .line 36
    if-nez v2, :cond_3

    .line 37
    .line 38
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->s0:Ljava/util/Timer;

    .line 39
    .line 40
    if-eqz v2, :cond_1

    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    .line 43
    .line 44
    .line 45
    :cond_1
    iget-boolean v2, p0, Lcom/intsig/document/widget/PagesView;->V:Z

    .line 46
    .line 47
    if-eqz v2, :cond_9

    .line 48
    .line 49
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    if-ne v2, v3, :cond_9

    .line 54
    .line 55
    new-instance v2, Landroid/graphics/Path;

    .line 56
    .line 57
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 58
    .line 59
    .line 60
    iput-object v2, p0, Lcom/intsig/document/widget/PagesView;->h0:Landroid/graphics/Path;

    .line 61
    .line 62
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 63
    .line 64
    .line 65
    iget-boolean v2, p0, Lcom/intsig/document/widget/PagesView;->W:Z

    .line 66
    .line 67
    if-nez v2, :cond_2

    .line 68
    .line 69
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    .line 70
    .line 71
    iget-object v4, p0, Lcom/intsig/document/widget/PagesView;->h0:Landroid/graphics/Path;

    .line 72
    .line 73
    const/4 v5, 0x0

    .line 74
    invoke-virtual {v2, v4, v5}, Lcom/intsig/document/widget/PagesView$p;->〇o00〇〇Oo(Landroid/graphics/Path;Z)V

    .line 75
    .line 76
    .line 77
    :cond_2
    iput v0, p0, Lcom/intsig/document/widget/PagesView;->k0:F

    .line 78
    .line 79
    iput v0, p0, Lcom/intsig/document/widget/PagesView;->i0:F

    .line 80
    .line 81
    iput v1, p0, Lcom/intsig/document/widget/PagesView;->l0:F

    .line 82
    .line 83
    iput v1, p0, Lcom/intsig/document/widget/PagesView;->j0:F

    .line 84
    .line 85
    goto/16 :goto_0

    .line 86
    .line 87
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    const/4 v4, 0x2

    .line 92
    if-ne v2, v4, :cond_6

    .line 93
    .line 94
    iget-boolean v2, p0, Lcom/intsig/document/widget/PagesView;->V:Z

    .line 95
    .line 96
    if-eqz v2, :cond_9

    .line 97
    .line 98
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 99
    .line 100
    .line 101
    move-result v2

    .line 102
    if-ne v2, v3, :cond_9

    .line 103
    .line 104
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->h0:Landroid/graphics/Path;

    .line 105
    .line 106
    iget v4, p0, Lcom/intsig/document/widget/PagesView;->i0:F

    .line 107
    .line 108
    iget v5, p0, Lcom/intsig/document/widget/PagesView;->j0:F

    .line 109
    .line 110
    add-float v6, v0, v4

    .line 111
    .line 112
    const/high16 v7, 0x40000000    # 2.0f

    .line 113
    .line 114
    div-float/2addr v6, v7

    .line 115
    add-float v8, v1, v5

    .line 116
    .line 117
    div-float/2addr v8, v7

    .line 118
    invoke-virtual {v2, v4, v5, v6, v8}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 119
    .line 120
    .line 121
    iget-boolean v2, p0, Lcom/intsig/document/widget/PagesView;->W:Z

    .line 122
    .line 123
    if-eqz v2, :cond_5

    .line 124
    .line 125
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    .line 126
    .line 127
    invoke-virtual {v2}, Lcom/intsig/document/widget/PagesView$p;->〇080()Ljava/util/List;

    .line 128
    .line 129
    .line 130
    move-result-object v2

    .line 131
    new-instance v4, Landroid/graphics/Paint;

    .line 132
    .line 133
    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 134
    .line 135
    .line 136
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 137
    .line 138
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 139
    .line 140
    .line 141
    sget-object v5, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    .line 142
    .line 143
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 144
    .line 145
    .line 146
    sget-object v5, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    .line 147
    .line 148
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 149
    .line 150
    .line 151
    iget-object v5, p0, Lcom/intsig/document/widget/PagesView;->f0:Lcom/intsig/document/widget/PagesView$InkArgs;

    .line 152
    .line 153
    iget v5, v5, Lcom/intsig/document/widget/PagesView$InkArgs;->strokeWidth:F

    .line 154
    .line 155
    iget v6, p0, Lcom/intsig/document/widget/PagesView;->u:F

    .line 156
    .line 157
    mul-float v5, v5, v6

    .line 158
    .line 159
    const v6, 0x3fe66666    # 1.8f

    .line 160
    .line 161
    .line 162
    mul-float v5, v5, v6

    .line 163
    .line 164
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 165
    .line 166
    .line 167
    new-instance v5, Landroid/graphics/Path;

    .line 168
    .line 169
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 170
    .line 171
    .line 172
    iget-object v6, p0, Lcom/intsig/document/widget/PagesView;->h0:Landroid/graphics/Path;

    .line 173
    .line 174
    invoke-virtual {v4, v6, v5}, Landroid/graphics/Paint;->getFillPath(Landroid/graphics/Path;Landroid/graphics/Path;)Z

    .line 175
    .line 176
    .line 177
    iget-object v6, p0, Lcom/intsig/document/widget/PagesView;->f0:Lcom/intsig/document/widget/PagesView$InkArgs;

    .line 178
    .line 179
    iget v6, v6, Lcom/intsig/document/widget/PagesView$InkArgs;->strokeWidth:F

    .line 180
    .line 181
    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 182
    .line 183
    .line 184
    check-cast v2, Ljava/util/ArrayList;

    .line 185
    .line 186
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 187
    .line 188
    .line 189
    move-result-object v2

    .line 190
    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 191
    .line 192
    .line 193
    move-result v6

    .line 194
    if-eqz v6, :cond_5

    .line 195
    .line 196
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 197
    .line 198
    .line 199
    move-result-object v6

    .line 200
    check-cast v6, Landroid/graphics/Path;

    .line 201
    .line 202
    new-instance v7, Landroid/graphics/Path;

    .line 203
    .line 204
    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 205
    .line 206
    .line 207
    invoke-virtual {v4, v6, v7}, Landroid/graphics/Paint;->getFillPath(Landroid/graphics/Path;Landroid/graphics/Path;)Z

    .line 208
    .line 209
    .line 210
    new-instance v8, Landroid/graphics/Path;

    .line 211
    .line 212
    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 213
    .line 214
    .line 215
    sget-object v9, Landroid/graphics/Path$Op;->INTERSECT:Landroid/graphics/Path$Op;

    .line 216
    .line 217
    invoke-virtual {v8, v5, v7, v9}, Landroid/graphics/Path;->op(Landroid/graphics/Path;Landroid/graphics/Path;Landroid/graphics/Path$Op;)Z

    .line 218
    .line 219
    .line 220
    invoke-virtual {v8}, Landroid/graphics/Path;->isEmpty()Z

    .line 221
    .line 222
    .line 223
    move-result v7

    .line 224
    xor-int/2addr v7, v3

    .line 225
    if-eqz v7, :cond_4

    .line 226
    .line 227
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    .line 228
    .line 229
    invoke-virtual {v2, v6, v3}, Lcom/intsig/document/widget/PagesView$p;->〇o00〇〇Oo(Landroid/graphics/Path;Z)V

    .line 230
    .line 231
    .line 232
    :cond_5
    iput v0, p0, Lcom/intsig/document/widget/PagesView;->i0:F

    .line 233
    .line 234
    iput v1, p0, Lcom/intsig/document/widget/PagesView;->j0:F

    .line 235
    .line 236
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 237
    .line 238
    .line 239
    goto :goto_0

    .line 240
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 241
    .line 242
    .line 243
    move-result v2

    .line 244
    if-ne v2, v3, :cond_9

    .line 245
    .line 246
    iget-boolean v2, p0, Lcom/intsig/document/widget/PagesView;->V:Z

    .line 247
    .line 248
    if-eqz v2, :cond_8

    .line 249
    .line 250
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 251
    .line 252
    .line 253
    move-result v2

    .line 254
    if-ne v2, v3, :cond_8

    .line 255
    .line 256
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->h0:Landroid/graphics/Path;

    .line 257
    .line 258
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 259
    .line 260
    .line 261
    iget v2, p0, Lcom/intsig/document/widget/PagesView;->k0:F

    .line 262
    .line 263
    cmpl-float v2, v2, v0

    .line 264
    .line 265
    if-nez v2, :cond_7

    .line 266
    .line 267
    iget v2, p0, Lcom/intsig/document/widget/PagesView;->l0:F

    .line 268
    .line 269
    cmpl-float v2, v2, v1

    .line 270
    .line 271
    if-nez v2, :cond_7

    .line 272
    .line 273
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->h0:Landroid/graphics/Path;

    .line 274
    .line 275
    const/high16 v4, 0x3f800000    # 1.0f

    .line 276
    .line 277
    add-float/2addr v0, v4

    .line 278
    add-float/2addr v1, v4

    .line 279
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 280
    .line 281
    .line 282
    :cond_7
    iget-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->W:Z

    .line 283
    .line 284
    if-nez v0, :cond_8

    .line 285
    .line 286
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 287
    .line 288
    if-eqz v0, :cond_8

    .line 289
    .line 290
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    .line 291
    .line 292
    iget-object v1, v1, Lcom/intsig/document/widget/PagesView$p;->〇080:Ljava/util/ArrayList;

    .line 293
    .line 294
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 295
    .line 296
    .line 297
    move-result v1

    .line 298
    invoke-interface {v0, v1}, Lcom/intsig/document/widget/PagesView$k;->onInkPaint(I)V

    .line 299
    .line 300
    .line 301
    :cond_8
    const/4 v0, 0x0

    .line 302
    iput-object v0, p0, Lcom/intsig/document/widget/PagesView;->h0:Landroid/graphics/Path;

    .line 303
    .line 304
    invoke-virtual {p0, p1}, Lcom/intsig/document/widget/PagesView;->onUp(Landroid/view/MotionEvent;)Z

    .line 305
    .line 306
    .line 307
    :cond_9
    :goto_0
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->d:Landroid/view/ScaleGestureDetector;

    .line 308
    .line 309
    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 310
    .line 311
    .line 312
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->c:Landroidx/core/view/GestureDetectorCompat;

    .line 313
    .line 314
    invoke-virtual {v0, p1}, Landroidx/core/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 315
    .line 316
    .line 317
    return v3
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method public onUp(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 1
    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->q:Z

    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput v0, p0, Lcom/intsig/document/widget/PagesView;->d0:I

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/document/widget/PagesView$a0;->O8()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 16
    .line 17
    if-eq v1, v0, :cond_0

    .line 18
    .line 19
    iput v0, p0, Lcom/intsig/document/widget/PagesView;->q0:I

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/document/widget/PagesView$a0;->〇o〇()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->r0:Lcom/intsig/document/widget/PagesView$a0;

    .line 28
    .line 29
    iget v3, v2, Lcom/intsig/document/widget/PagesView$a0;->〇o00〇〇Oo:I

    .line 30
    .line 31
    invoke-virtual {v2}, Lcom/intsig/document/widget/PagesView$a0;->〇o00〇〇Oo()Landroid/graphics/PointF;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-virtual {p0, v3, v2}, Lcom/intsig/document/widget/PagesView;->a(ILandroid/graphics/PointF;)Landroid/graphics/PointF;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-virtual {p0, v1, v2}, Lcom/intsig/document/widget/PagesView;->a(Ljava/lang/String;Landroid/graphics/PointF;)V

    .line 40
    .line 41
    .line 42
    :cond_0
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->g:Landroid/os/Handler;

    .line 43
    .line 44
    const/16 v2, 0x64

    .line 45
    .line 46
    const-wide/16 v3, 0x5dc

    .line 47
    .line 48
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 49
    .line 50
    .line 51
    iget-boolean v1, p0, Lcom/intsig/document/widget/PagesView;->B:Z

    .line 52
    .line 53
    if-eqz v1, :cond_1

    .line 54
    .line 55
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->B:Z

    .line 56
    .line 57
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 58
    .line 59
    .line 60
    :cond_1
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView;->c0:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 61
    .line 62
    if-eqz p1, :cond_3

    .line 63
    .line 64
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 65
    .line 66
    if-eqz v1, :cond_3

    .line 67
    .line 68
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 69
    .line 70
    sget-object v3, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Move:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 71
    .line 72
    if-eq v2, v3, :cond_2

    .line 73
    .line 74
    sget-object v3, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Resize:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 75
    .line 76
    if-eq v2, v3, :cond_2

    .line 77
    .line 78
    sget-object v3, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Rotate:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 79
    .line 80
    if-ne v2, v3, :cond_3

    .line 81
    .line 82
    :cond_2
    sget-object v2, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->Up:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 83
    .line 84
    invoke-interface {v1, p1, v2}, Lcom/intsig/document/widget/PagesView$k;->onElementDataFocus(Lcom/intsig/document/widget/PagesView$ElementData;Lcom/intsig/document/widget/PagesView$AnnotFocusState;)V

    .line 85
    .line 86
    .line 87
    sget-object p1, Lcom/intsig/document/widget/PagesView$AnnotFocusState;->None:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 88
    .line 89
    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->v0:Lcom/intsig/document/widget/PagesView$AnnotFocusState;

    .line 90
    .line 91
    :cond_3
    return v0
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public redoInk()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    .line 2
    .line 3
    iget v1, v0, Lcom/intsig/document/widget/PagesView$p;->〇o00〇〇Oo:I

    .line 4
    .line 5
    iget-object v2, v0, Lcom/intsig/document/widget/PagesView$p;->〇080:Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    add-int/lit8 v2, v2, -0x1

    .line 12
    .line 13
    if-ge v1, v2, :cond_0

    .line 14
    .line 15
    iget v1, v0, Lcom/intsig/document/widget/PagesView$p;->〇o00〇〇Oo:I

    .line 16
    .line 17
    add-int/lit8 v1, v1, 0x1

    .line 18
    .line 19
    iput v1, v0, Lcom/intsig/document/widget/PagesView$p;->〇o00〇〇Oo:I

    .line 20
    .line 21
    :cond_0
    iget v0, v0, Lcom/intsig/document/widget/PagesView$p;->〇o00〇〇Oo:I

    .line 22
    .line 23
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 24
    .line 25
    .line 26
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public renderPage(IIZLcom/intsig/document/widget/DocumentView$RenderCallback;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->e:Lcom/intsig/document/widget/PagesView$v;

    .line 2
    .line 3
    new-instance v7, Lcom/intsig/document/widget/PagesView$g;

    .line 4
    .line 5
    move-object v1, v7

    .line 6
    move-object v2, p0

    .line 7
    move v3, p1

    .line 8
    move v4, p2

    .line 9
    move v5, p3

    .line 10
    move-object v6, p4

    .line 11
    invoke-direct/range {v1 .. v6}, Lcom/intsig/document/widget/PagesView$g;-><init>(Lcom/intsig/document/widget/PagesView;IIZLcom/intsig/document/widget/DocumentView$RenderCallback;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v7}, Lcom/intsig/document/widget/PagesView$v;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public setAnnotBoxStyle(IF[FLandroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    iput p2, p0, Lcom/intsig/document/widget/PagesView;->A0:F

    .line 2
    .line 3
    iput p1, p0, Lcom/intsig/document/widget/PagesView;->B0:I

    .line 4
    .line 5
    iput-object p3, p0, Lcom/intsig/document/widget/PagesView;->C0:[F

    .line 6
    .line 7
    if-eqz p4, :cond_0

    .line 8
    .line 9
    iput-object p4, p0, Lcom/intsig/document/widget/PagesView;->J:Landroid/graphics/drawable/Drawable;

    .line 10
    .line 11
    :cond_0
    if-eqz p5, :cond_1

    .line 12
    .line 13
    iput-object p5, p0, Lcom/intsig/document/widget/PagesView;->K:Landroid/graphics/drawable/Drawable;

    .line 14
    .line 15
    :cond_1
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
.end method

.method public setAnnotListener(Lcom/intsig/document/widget/PagesView$k;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->x0:Lcom/intsig/document/widget/PagesView$k;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setCanvasBackgroundColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/document/widget/PagesView;->N:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setFastScrollBar(Landroid/graphics/drawable/Drawable;I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->I:Landroid/graphics/drawable/Drawable;

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/document/widget/PagesView;->L:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setInkErase(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->W:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPageBackground(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/document/widget/PagesView;->M:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPageBorderSize(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/document/widget/PagesView;->R:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setPageGap(I)V
    .locals 0

    .line 1
    int-to-float p1, p1

    .line 2
    iput p1, p0, Lcom/intsig/document/widget/PagesView;->i:F

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setScaleRange(FF)V
    .locals 0

    .line 1
    iput p2, p0, Lcom/intsig/document/widget/PagesView;->j:F

    .line 2
    .line 3
    iput p1, p0, Lcom/intsig/document/widget/PagesView;->k:F

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public setSideBarStyle(III)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/document/widget/PagesView;->P:I

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/document/widget/PagesView;->O:I

    .line 4
    .line 5
    iput p3, p0, Lcom/intsig/document/widget/PagesView;->Q:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public setTextSelectListener(Lcom/intsig/document/widget/PagesView$z;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->w0:Lcom/intsig/document/widget/PagesView$z;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public showAnnot(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/document/widget/PagesView;->T:Z

    .line 2
    .line 3
    if-eq v0, p1, :cond_0

    .line 4
    .line 5
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView;->T:Z

    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public showSearchResult(I)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->t0:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/document/DocumentLoader$d;

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    iput-object p1, p0, Lcom/intsig/document/widget/PagesView;->u0:Lcom/intsig/document/DocumentLoader$d;

    .line 13
    .line 14
    new-instance v1, Landroid/graphics/RectF;

    .line 15
    .line 16
    iget-object v2, p1, Lcom/intsig/document/DocumentLoader$d;->O8:Landroid/graphics/RectF;

    .line 17
    .line 18
    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 19
    .line 20
    .line 21
    iget v2, v1, Landroid/graphics/RectF;->top:F

    .line 22
    .line 23
    const/high16 v3, 0x41a00000    # 20.0f

    .line 24
    .line 25
    sub-float/2addr v2, v3

    .line 26
    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 27
    .line 28
    iget p1, p1, Lcom/intsig/document/DocumentLoader$d;->〇o〇:I

    .line 29
    .line 30
    invoke-virtual {p0, p1, v1, v0}, Lcom/intsig/document/widget/PagesView;->ScrollTo(ILandroid/graphics/RectF;Z)V

    .line 31
    .line 32
    .line 33
    const/4 p1, 0x1

    .line 34
    return p1

    .line 35
    :cond_0
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public undoInk()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView;->g0:Lcom/intsig/document/widget/PagesView$p;

    .line 2
    .line 3
    iget v1, v0, Lcom/intsig/document/widget/PagesView$p;->〇o00〇〇Oo:I

    .line 4
    .line 5
    if-ltz v1, :cond_0

    .line 6
    .line 7
    add-int/lit8 v1, v1, -0x1

    .line 8
    .line 9
    iput v1, v0, Lcom/intsig/document/widget/PagesView$p;->〇o00〇〇Oo:I

    .line 10
    .line 11
    :cond_0
    iget v0, v0, Lcom/intsig/document/widget/PagesView$p;->〇o00〇〇Oo:I

    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 14
    .line 15
    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public updateElement(Lcom/intsig/document/widget/PagesView$ElementData;)V
    .locals 3

    .line 1
    iget-object v0, p1, Lcom/intsig/document/widget/PagesView$ElementData;->args:Lcom/intsig/document/widget/PagesView$BaseArgs;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/document/widget/PagesView$TextArgs;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    check-cast v0, Lcom/intsig/document/widget/PagesView$TextArgs;

    .line 8
    .line 9
    iget-object v1, p1, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->width()F

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {p0, v0, v1}, Lcom/intsig/document/widget/PagesView;->a(Lcom/intsig/document/widget/PagesView$TextArgs;F)Landroid/util/Size;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {v0}, Landroid/util/Size;->getHeight()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    int-to-float v1, v1

    .line 24
    iget-object v2, p1, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/document/widget/PagesView$AnnotBox;->height()F

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    cmpl-float v1, v1, v2

    .line 31
    .line 32
    if-lez v1, :cond_0

    .line 33
    .line 34
    iget-object v1, p1, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 35
    .line 36
    invoke-virtual {v0}, Landroid/util/Size;->getHeight()I

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    int-to-float v0, v0

    .line 41
    iget-object p1, p1, Lcom/intsig/document/widget/PagesView$ElementData;->box:Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/document/widget/PagesView$AnnotBox;->height()F

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    sub-float/2addr v0, p1

    .line 48
    const/4 p1, 0x0

    .line 49
    invoke-virtual {v1, p1, v0}, Lcom/intsig/document/widget/PagesView$AnnotBox;->resize(FF)V

    .line 50
    .line 51
    .line 52
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public zoom(Z)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    const p1, 0x3f8ccccd    # 1.1f

    .line 5
    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const p1, 0x3f666666    # 0.9f

    .line 9
    .line 10
    .line 11
    :goto_0
    invoke-virtual {p0, p1, v0, v0}, Lcom/intsig/document/widget/PagesView;->a(FFF)Z

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

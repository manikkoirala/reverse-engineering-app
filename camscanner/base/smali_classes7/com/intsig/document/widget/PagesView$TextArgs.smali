.class public Lcom/intsig/document/widget/PagesView$TextArgs;
.super Lcom/intsig/document/widget/PagesView$BaseArgs;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/document/widget/PagesView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TextArgs"
.end annotation


# instance fields
.field public color:I

.field public font:Ljava/lang/String;

.field public fontSize:F

.field public freetext:Z

.field public layoutLines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public rect:Landroid/graphics/RectF;

.field public rotatable:Z

.field public text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/graphics/RectF;FI)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/document/widget/PagesView$BaseArgs;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->rotatable:Z

    iput-object p1, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->text:Ljava/lang/String;

    iput-object p2, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->rect:Landroid/graphics/RectF;

    iput p4, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->color:I

    iput p3, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->fontSize:F

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->freetext:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;FI)V
    .locals 6

    .line 2
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/intsig/document/widget/PagesView$TextArgs;-><init>(Ljava/lang/String;Ljava/lang/String;FIZ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;FIZ)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/intsig/document/widget/PagesView$BaseArgs;-><init>()V

    iput-object p1, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->text:Ljava/lang/String;

    iget-object p1, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->rect:Landroid/graphics/RectF;

    iput-object p1, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->rect:Landroid/graphics/RectF;

    iput p4, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->color:I

    iput-object p2, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->font:Ljava/lang/String;

    iput p3, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->fontSize:F

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->freetext:Z

    iput-boolean p5, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->rotatable:Z

    return-void
.end method


# virtual methods
.method public getText()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->layoutLines:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_1

    .line 12
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->layoutLines:Ljava/util/List;

    .line 18
    .line 19
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_1

    .line 28
    .line 29
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    check-cast v2, Ljava/lang/String;

    .line 34
    .line 35
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 40
    .line 41
    .line 42
    const-string v2, "\r"

    .line 43
    .line 44
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    return-object v0

    .line 53
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->text:Ljava/lang/String;

    .line 54
    .line 55
    return-object v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public setLayoutText(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->layoutLines:Ljava/util/List;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRotatable(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView$TextArgs;->rotatable:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

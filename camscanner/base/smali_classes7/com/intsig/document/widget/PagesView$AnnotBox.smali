.class public Lcom/intsig/document/widget/PagesView$AnnotBox;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/document/widget/PagesView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AnnotBox"
.end annotation


# instance fields
.field public angle:I

.field public box:Landroid/graphics/RectF;

.field public fixRatio:Z

.field public fixSize:Z

.field public pageNum:I

.field public rotateable:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixRatio:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixSize:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    iput v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    new-instance v0, Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/document/widget/PagesView$AnnotBox;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixRatio:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixSize:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    iput v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    iget v0, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    iput v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    iget-boolean v0, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixRatio:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixRatio:Z

    iget-boolean v0, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixSize:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixSize:Z

    iget-boolean v0, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    iget p1, p1, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    iput p1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    return-void
.end method


# virtual methods
.method public center()Landroid/graphics/PointF;
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/PointF;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 4
    .line 5
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iget-object v2, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 10
    .line 11
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 16
    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method public checkSize(FFFF)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    add-float/2addr v0, p1

    .line 8
    const/4 p1, 0x0

    .line 9
    cmpg-float p3, v0, p3

    .line 10
    .line 11
    if-gez p3, :cond_0

    .line 12
    .line 13
    return p1

    .line 14
    :cond_0
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 15
    .line 16
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    .line 17
    .line 18
    .line 19
    move-result p3

    .line 20
    add-float/2addr p3, p2

    .line 21
    cmpg-float p2, p3, p4

    .line 22
    .line 23
    if-gez p2, :cond_1

    .line 24
    .line 25
    return p1

    .line 26
    :cond_1
    const/4 p1, 0x1

    .line 27
    return p1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public clear()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 5
    .line 6
    .line 7
    const/4 v0, -0x1

    .line 8
    iput v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    iput-boolean v1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixRatio:Z

    .line 15
    .line 16
    iput-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixSize:Z

    .line 17
    .line 18
    iput v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    .line 19
    .line 20
    return-void
    .line 21
.end method

.method public empty()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public height()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isRotateable()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public move(FF)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 2
    .line 3
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 4
    .line 5
    add-float/2addr v1, p1

    .line 6
    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 7
    .line 8
    iget v1, v0, Landroid/graphics/RectF;->top:F

    .line 9
    .line 10
    add-float/2addr v1, p2

    .line 11
    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 12
    .line 13
    iget v1, v0, Landroid/graphics/RectF;->right:F

    .line 14
    .line 15
    add-float/2addr v1, p1

    .line 16
    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 17
    .line 18
    iget p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 19
    .line 20
    add-float/2addr p1, p2

    .line 21
    iput p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public place(IFF)V
    .locals 2

    .line 1
    iput p1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 4
    .line 5
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 16
    .line 17
    iput p2, v1, Landroid/graphics/RectF;->left:F

    .line 18
    .line 19
    iput p3, v1, Landroid/graphics/RectF;->top:F

    .line 20
    .line 21
    add-float/2addr p2, p1

    .line 22
    iput p2, v1, Landroid/graphics/RectF;->right:F

    .line 23
    .line 24
    add-float/2addr p3, v0

    .line 25
    iput p3, v1, Landroid/graphics/RectF;->bottom:F

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public reset(FFZ)V
    .locals 0

    .line 1
    iput-boolean p3, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixRatio:Z

    .line 2
    .line 3
    iget-object p3, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 4
    .line 5
    iput p1, p3, Landroid/graphics/RectF;->right:F

    .line 6
    .line 7
    iput p2, p3, Landroid/graphics/RectF;->bottom:F

    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    .line 11
    .line 12
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixSize:Z

    .line 13
    .line 14
    iput p1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public resize(FF)V
    .locals 8

    .line 1
    iget-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixSize:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->fixRatio:Z

    .line 7
    .line 8
    if-eqz v0, :cond_2

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    cmpl-float v0, p1, v0

    .line 12
    .line 13
    if-lez v0, :cond_1

    .line 14
    .line 15
    const/high16 v0, 0x3f800000    # 1.0f

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    const/high16 v0, -0x40800000    # -1.0f

    .line 19
    .line 20
    :goto_0
    mul-float p1, p1, p1

    .line 21
    .line 22
    mul-float p2, p2, p2

    .line 23
    .line 24
    add-float/2addr p2, p1

    .line 25
    float-to-double p1, p2

    .line 26
    invoke-static {p1, p2}, Ljava/lang/Math;->sqrt(D)D

    .line 27
    .line 28
    .line 29
    move-result-wide p1

    .line 30
    iget-object v1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 31
    .line 32
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    float-to-double v1, v1

    .line 37
    mul-double v1, v1, p1

    .line 38
    .line 39
    iget-object v3, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 40
    .line 41
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    float-to-double v3, v3

    .line 46
    div-double/2addr v1, v3

    .line 47
    iget-object v3, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 48
    .line 49
    iget v4, v3, Landroid/graphics/RectF;->right:F

    .line 50
    .line 51
    float-to-double v4, v4

    .line 52
    float-to-double v6, v0

    .line 53
    mul-double p1, p1, v6

    .line 54
    .line 55
    add-double/2addr p1, v4

    .line 56
    double-to-float p1, p1

    .line 57
    iput p1, v3, Landroid/graphics/RectF;->right:F

    .line 58
    .line 59
    iget p1, v3, Landroid/graphics/RectF;->bottom:F

    .line 60
    .line 61
    float-to-double p1, p1

    .line 62
    mul-double v1, v1, v6

    .line 63
    .line 64
    add-double/2addr v1, p1

    .line 65
    double-to-float p1, v1

    .line 66
    iput p1, v3, Landroid/graphics/RectF;->bottom:F

    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 70
    .line 71
    iget v1, v0, Landroid/graphics/RectF;->right:F

    .line 72
    .line 73
    add-float/2addr v1, p1

    .line 74
    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 75
    .line 76
    iget p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 77
    .line 78
    add-float/2addr p1, p2

    .line 79
    iput p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 80
    .line 81
    :goto_1
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public setRotatable(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->rotateable:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public setRotate(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->angle:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public width()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/document/widget/PagesView$AnnotBox;->box:Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

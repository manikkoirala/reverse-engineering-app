.class public final Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;
.super Ljava/lang/Object;
.source "TcpCmd.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/developer/lib_message/mode/TcpCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final O8(I)Lcom/intsig/developer/lib_message/mode/TcpCmd;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_UN_KNOW:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/developer/lib_message/mode/TcpCmd;->values()[Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    array-length v2, v1

    .line 8
    const/4 v3, 0x0

    .line 9
    :goto_0
    if-ge v3, v2, :cond_1

    .line 10
    .line 11
    aget-object v4, v1, v3

    .line 12
    .line 13
    invoke-virtual {v4}, Lcom/intsig/developer/lib_message/mode/TcpCmd;->getCmdCode()I

    .line 14
    .line 15
    .line 16
    move-result v5

    .line 17
    if-ne p1, v5, :cond_0

    .line 18
    .line 19
    move-object v0, v4

    .line 20
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    return-object v0
    .line 24
.end method

.method public final Oo08(Z)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/developer/lib_message/mode/TcpCmd;->access$setEnableFetchMsg$cp(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final 〇080(Lio/netty/channel/ChannelHandlerContext;J)V
    .locals 10
    .param p1    # Lio/netty/channel/ChannelHandlerContext;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "ctx"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;->〇o00〇〇Oo()Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    sget-object v1, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_FETCH_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/intsig/developer/lib_message/mode/TcpCmd;->getCmdCode()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    const/4 v2, 0x2

    .line 17
    invoke-static {v1, v2}, Lcom/intsig/developer/lib_message/util/NumberConvertUtilKt;->〇080(II)[B

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    invoke-virtual {v0}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->O8()[B

    .line 22
    .line 23
    .line 24
    move-result-object v4

    .line 25
    const/4 v5, 0x0

    .line 26
    const/4 v6, 0x0

    .line 27
    const/4 v7, 0x0

    .line 28
    const/16 v8, 0xe

    .line 29
    .line 30
    const/4 v9, 0x0

    .line 31
    invoke-static/range {v3 .. v9}, Lkotlin/collections/ArraysKt;->o〇0([B[BIIIILjava/lang/Object;)[B

    .line 32
    .line 33
    .line 34
    invoke-static {}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsRequest;->newBuilder()Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsRequest$Builder;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v1, p2, p3}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsRequest$Builder;->〇080(J)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsRequest$Builder;

    .line 39
    .line 40
    .line 41
    const/4 v2, 0x0

    .line 42
    invoke-virtual {v1, v2}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsRequest$Builder;->〇o00〇〇Oo(I)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsRequest$Builder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->build()Lcom/google/protobuf/GeneratedMessageLite;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    check-cast v1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsRequest;

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/google/protobuf/AbstractMessageLite;->toByteArray()[B

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-virtual {v0, v1}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->〇8o8o〇([B)V

    .line 56
    .line 57
    .line 58
    invoke-interface {p1, v0}, Lio/netty/channel/ChannelOutboundInvoker;->writeAndFlush(Ljava/lang/Object;)Lio/netty/channel/ChannelFuture;

    .line 59
    .line 60
    .line 61
    const-string p1, "fetchMsg maxUpdateTime:"

    .line 62
    .line 63
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->〇〇808〇(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    const-string p2, "TcpCmd"

    .line 72
    .line 73
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {p0, v2}, Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;->Oo08(Z)V

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public final 〇o00〇〇Oo()Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;
    .locals 10
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    invoke-static {v1, v1}, Lcom/intsig/developer/lib_message/util/NumberConvertUtilKt;->〇080(II)[B

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-virtual {v0}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->〇o〇()[B

    .line 12
    .line 13
    .line 14
    move-result-object v3

    .line 15
    const/4 v4, 0x0

    .line 16
    const/4 v5, 0x0

    .line 17
    const/4 v6, 0x0

    .line 18
    const/16 v7, 0xe

    .line 19
    .line 20
    const/4 v8, 0x0

    .line 21
    invoke-static/range {v2 .. v8}, Lkotlin/collections/ArraysKt;->o〇0([B[BIIIILjava/lang/Object;)[B

    .line 22
    .line 23
    .line 24
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 25
    .line 26
    .line 27
    move-result-wide v1

    .line 28
    invoke-static {v1, v2}, Lcom/intsig/developer/lib_message/util/NumberConvertUtilKt;->〇o00〇〇Oo(J)[B

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    invoke-virtual {v0}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->〇80〇808〇O()[B

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    const/4 v7, 0x0

    .line 37
    const/16 v8, 0xe

    .line 38
    .line 39
    const/4 v9, 0x0

    .line 40
    invoke-static/range {v3 .. v9}, Lkotlin/collections/ArraysKt;->o〇0([B[BIIIILjava/lang/Object;)[B

    .line 41
    .line 42
    .line 43
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final 〇o〇()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/developer/lib_message/mode/TcpCmd;->access$getEnableFetchMsg$cp()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

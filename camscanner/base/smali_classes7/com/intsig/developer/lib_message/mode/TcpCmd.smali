.class public enum Lcom/intsig/developer/lib_message/mode/TcpCmd;
.super Ljava/lang/Enum;
.source "TcpCmd.kt"

# interfaces
.implements Lcom/intsig/developer/lib_message/intferface/TcpCmdInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_UN_KNOW;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_AUTH;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_REQ_KICK_OUT;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_REQ_NEW_MSGS;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_MSGS;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_PING;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_PONG;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_MARK_AS_READ;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_EXTRA;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_HAND_SHAKE;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_HAND_SHAKE_RESP;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_HAND_SHAKE_ACK;,
        Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_CLIENT_DOWN;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/developer/lib_message/mode/TcpCmd;",
        ">;",
        "Lcom/intsig/developer/lib_message/intferface/TcpCmdInterface;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final AES_KEY_LENGTH:I = 0x10

.field public static final enum CMD_CLIENT_DOWN:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_HAND_SHAKE:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_HAND_SHAKE_ACK:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_HAND_SHAKE_RESP:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_PING:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_PONG:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_REQ_AUTH:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_REQ_EXTRA:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_REQ_FETCH_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_REQ_KICK_OUT:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_REQ_MARK_AS_READ:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_REQ_NEW_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_REQ_REPORT_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_RESP_AUTH:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_RESP_EXTRA:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_RESP_MARK_AS_READ:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_RESP_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_SYNC_INFO:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final enum CMD_UN_KNOW:Lcom/intsig/developer/lib_message/mode/TcpCmd;

.field public static final Companion:Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final MINI_PACKAGE_LENGTH:I = 0x14

.field public static final TAG:Ljava/lang/String; = "TcpCmd"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static enableFetchMsg:Z


# instance fields
.field private final cmdCode:I


# direct methods
.method private static final synthetic $values()[Lcom/intsig/developer/lib_message/mode/TcpCmd;
    .locals 3

    .line 1
    const/16 v0, 0x13

    .line 2
    .line 3
    new-array v0, v0, [Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_UN_KNOW:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_AUTH:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_RESP_AUTH:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_KICK_OUT:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_NEW_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_FETCH_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_RESP_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_PING:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_PONG:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    const/16 v1, 0x9

    .line 52
    .line 53
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_MARK_AS_READ:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 54
    .line 55
    aput-object v2, v0, v1

    .line 56
    .line 57
    const/16 v1, 0xa

    .line 58
    .line 59
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_RESP_MARK_AS_READ:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 60
    .line 61
    aput-object v2, v0, v1

    .line 62
    .line 63
    const/16 v1, 0xb

    .line 64
    .line 65
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_REPORT_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 66
    .line 67
    aput-object v2, v0, v1

    .line 68
    .line 69
    const/16 v1, 0xc

    .line 70
    .line 71
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_EXTRA:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 72
    .line 73
    aput-object v2, v0, v1

    .line 74
    .line 75
    const/16 v1, 0xd

    .line 76
    .line 77
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_RESP_EXTRA:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 78
    .line 79
    aput-object v2, v0, v1

    .line 80
    .line 81
    const/16 v1, 0xe

    .line 82
    .line 83
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_SYNC_INFO:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 84
    .line 85
    aput-object v2, v0, v1

    .line 86
    .line 87
    const/16 v1, 0xf

    .line 88
    .line 89
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_HAND_SHAKE:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 90
    .line 91
    aput-object v2, v0, v1

    .line 92
    .line 93
    const/16 v1, 0x10

    .line 94
    .line 95
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_HAND_SHAKE_RESP:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 96
    .line 97
    aput-object v2, v0, v1

    .line 98
    .line 99
    const/16 v1, 0x11

    .line 100
    .line 101
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_HAND_SHAKE_ACK:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 102
    .line 103
    aput-object v2, v0, v1

    .line 104
    .line 105
    const/16 v1, 0x12

    .line 106
    .line 107
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_CLIENT_DOWN:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 108
    .line 109
    aput-object v2, v0, v1

    .line 110
    .line 111
    return-object v0
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method static constructor <clinit>()V
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_UN_KNOW;

    .line 2
    .line 3
    const-string v1, "CMD_UN_KNOW"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_UN_KNOW;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_UN_KNOW:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 12
    .line 13
    const-string v1, "CMD_REQ_AUTH"

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-direct {v0, v1, v2, v2}, Lcom/intsig/developer/lib_message/mode/TcpCmd;-><init>(Ljava/lang/String;II)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_AUTH:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_AUTH;

    .line 22
    .line 23
    const-string v1, "CMD_RESP_AUTH"

    .line 24
    .line 25
    const/4 v3, 0x2

    .line 26
    invoke-direct {v0, v1, v3}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_AUTH;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_RESP_AUTH:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 30
    .line 31
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_REQ_KICK_OUT;

    .line 32
    .line 33
    const-string v1, "CMD_REQ_KICK_OUT"

    .line 34
    .line 35
    const/4 v3, 0x3

    .line 36
    invoke-direct {v0, v1, v3}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_REQ_KICK_OUT;-><init>(Ljava/lang/String;I)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_KICK_OUT:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 40
    .line 41
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_REQ_NEW_MSGS;

    .line 42
    .line 43
    const-string v1, "CMD_REQ_NEW_MSGS"

    .line 44
    .line 45
    const/4 v4, 0x4

    .line 46
    invoke-direct {v0, v1, v4}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_REQ_NEW_MSGS;-><init>(Ljava/lang/String;I)V

    .line 47
    .line 48
    .line 49
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_NEW_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 50
    .line 51
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 52
    .line 53
    const-string v1, "CMD_REQ_FETCH_MSGS"

    .line 54
    .line 55
    const/4 v4, 0x5

    .line 56
    invoke-direct {v0, v1, v4, v3}, Lcom/intsig/developer/lib_message/mode/TcpCmd;-><init>(Ljava/lang/String;II)V

    .line 57
    .line 58
    .line 59
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_FETCH_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 60
    .line 61
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_MSGS;

    .line 62
    .line 63
    const-string v1, "CMD_RESP_MSGS"

    .line 64
    .line 65
    const/4 v3, 0x6

    .line 66
    invoke-direct {v0, v1, v3}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_MSGS;-><init>(Ljava/lang/String;I)V

    .line 67
    .line 68
    .line 69
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_RESP_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 70
    .line 71
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_PING;

    .line 72
    .line 73
    const-string v1, "CMD_PING"

    .line 74
    .line 75
    const/4 v3, 0x7

    .line 76
    invoke-direct {v0, v1, v3}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_PING;-><init>(Ljava/lang/String;I)V

    .line 77
    .line 78
    .line 79
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_PING:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 80
    .line 81
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_PONG;

    .line 82
    .line 83
    const-string v1, "CMD_PONG"

    .line 84
    .line 85
    const/16 v3, 0x8

    .line 86
    .line 87
    invoke-direct {v0, v1, v3}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_PONG;-><init>(Ljava/lang/String;I)V

    .line 88
    .line 89
    .line 90
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_PONG:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 91
    .line 92
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 93
    .line 94
    const-string v1, "CMD_REQ_MARK_AS_READ"

    .line 95
    .line 96
    const/16 v3, 0x9

    .line 97
    .line 98
    invoke-direct {v0, v1, v3, v3}, Lcom/intsig/developer/lib_message/mode/TcpCmd;-><init>(Ljava/lang/String;II)V

    .line 99
    .line 100
    .line 101
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_MARK_AS_READ:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 102
    .line 103
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_MARK_AS_READ;

    .line 104
    .line 105
    const-string v1, "CMD_RESP_MARK_AS_READ"

    .line 106
    .line 107
    const/16 v3, 0xa

    .line 108
    .line 109
    invoke-direct {v0, v1, v3}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_MARK_AS_READ;-><init>(Ljava/lang/String;I)V

    .line 110
    .line 111
    .line 112
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_RESP_MARK_AS_READ:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 113
    .line 114
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 115
    .line 116
    const-string v1, "CMD_REQ_REPORT_MSGS"

    .line 117
    .line 118
    const/16 v3, 0xb

    .line 119
    .line 120
    const/16 v4, 0xd

    .line 121
    .line 122
    invoke-direct {v0, v1, v3, v4}, Lcom/intsig/developer/lib_message/mode/TcpCmd;-><init>(Ljava/lang/String;II)V

    .line 123
    .line 124
    .line 125
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_REPORT_MSGS:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 126
    .line 127
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 128
    .line 129
    const-string v1, "CMD_REQ_EXTRA"

    .line 130
    .line 131
    const/16 v3, 0xc

    .line 132
    .line 133
    const/16 v5, 0xf

    .line 134
    .line 135
    invoke-direct {v0, v1, v3, v5}, Lcom/intsig/developer/lib_message/mode/TcpCmd;-><init>(Ljava/lang/String;II)V

    .line 136
    .line 137
    .line 138
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_EXTRA:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 139
    .line 140
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_EXTRA;

    .line 141
    .line 142
    const-string v1, "CMD_RESP_EXTRA"

    .line 143
    .line 144
    invoke-direct {v0, v1, v4}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_RESP_EXTRA;-><init>(Ljava/lang/String;I)V

    .line 145
    .line 146
    .line 147
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_RESP_EXTRA:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 148
    .line 149
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 150
    .line 151
    const-string v1, "CMD_SYNC_INFO"

    .line 152
    .line 153
    const/16 v3, 0xe

    .line 154
    .line 155
    const/16 v4, 0x11

    .line 156
    .line 157
    invoke-direct {v0, v1, v3, v4}, Lcom/intsig/developer/lib_message/mode/TcpCmd;-><init>(Ljava/lang/String;II)V

    .line 158
    .line 159
    .line 160
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_SYNC_INFO:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 161
    .line 162
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_HAND_SHAKE;

    .line 163
    .line 164
    const-string v1, "CMD_HAND_SHAKE"

    .line 165
    .line 166
    invoke-direct {v0, v1, v5}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_HAND_SHAKE;-><init>(Ljava/lang/String;I)V

    .line 167
    .line 168
    .line 169
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_HAND_SHAKE:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 170
    .line 171
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_HAND_SHAKE_RESP;

    .line 172
    .line 173
    const-string v1, "CMD_HAND_SHAKE_RESP"

    .line 174
    .line 175
    const/16 v3, 0x10

    .line 176
    .line 177
    invoke-direct {v0, v1, v3}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_HAND_SHAKE_RESP;-><init>(Ljava/lang/String;I)V

    .line 178
    .line 179
    .line 180
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_HAND_SHAKE_RESP:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 181
    .line 182
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_HAND_SHAKE_ACK;

    .line 183
    .line 184
    const-string v1, "CMD_HAND_SHAKE_ACK"

    .line 185
    .line 186
    invoke-direct {v0, v1, v4}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_HAND_SHAKE_ACK;-><init>(Ljava/lang/String;I)V

    .line 187
    .line 188
    .line 189
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_HAND_SHAKE_ACK:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 190
    .line 191
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_CLIENT_DOWN;

    .line 192
    .line 193
    const-string v1, "CMD_CLIENT_DOWN"

    .line 194
    .line 195
    const/16 v3, 0x12

    .line 196
    .line 197
    invoke-direct {v0, v1, v3}, Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_CLIENT_DOWN;-><init>(Ljava/lang/String;I)V

    .line 198
    .line 199
    .line 200
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_CLIENT_DOWN:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 201
    .line 202
    invoke-static {}, Lcom/intsig/developer/lib_message/mode/TcpCmd;->$values()[Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 203
    .line 204
    .line 205
    move-result-object v0

    .line 206
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->$VALUES:[Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 207
    .line 208
    new-instance v0, Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;

    .line 209
    .line 210
    const/4 v1, 0x0

    .line 211
    invoke-direct {v0, v1}, Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 212
    .line 213
    .line 214
    sput-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->Companion:Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;

    .line 215
    .line 216
    sput-boolean v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->enableFetchMsg:Z

    .line 217
    .line 218
    return-void
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->cmdCode:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x1

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 3
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/developer/lib_message/mode/TcpCmd;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/developer/lib_message/mode/TcpCmd;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static final synthetic access$getEnableFetchMsg$cp()Z
    .locals 1

    .line 1
    sget-boolean v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->enableFetchMsg:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic access$setEnableFetchMsg$cp(Z)V
    .locals 0

    .line 1
    sput-boolean p0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->enableFetchMsg:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/developer/lib_message/mode/TcpCmd;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public static values()[Lcom/intsig/developer/lib_message/mode/TcpCmd;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->$VALUES:[Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getCmdCode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->cmdCode:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public responseTcp(Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;Lio/netty/channel/ChannelHandlerContext;Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;)V
    .locals 0
    .param p1    # Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lio/netty/channel/ChannelHandlerContext;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/developer/lib_message/intferface/TcpCmdInterface$DefaultImpls;->〇080(Lcom/intsig/developer/lib_message/intferface/TcpCmdInterface;Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;Lio/netty/channel/ChannelHandlerContext;Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public useAesForBody()Z
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/intsig/developer/lib_message/intferface/TcpCmdInterface$DefaultImpls;->〇o00〇〇Oo(Lcom/intsig/developer/lib_message/intferface/TcpCmdInterface;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

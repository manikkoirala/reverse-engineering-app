.class final Lcom/intsig/developer/lib_message/mode/TcpCmd$CMD_HAND_SHAKE_ACK;
.super Lcom/intsig/developer/lib_message/mode/TcpCmd;
.source "TcpCmd.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/developer/lib_message/mode/TcpCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "CMD_HAND_SHAKE_ACK"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/intsig/developer/lib_message/mode/TcpCmd;-><init>(Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public responseTcp(Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;Lio/netty/channel/ChannelHandlerContext;Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;)V
    .locals 8
    .param p1    # Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lio/netty/channel/ChannelHandlerContext;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "imsClientInterface"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "seSecurityEncrypt"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p2, "ctx"

    .line 12
    .line 13
    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string p2, "msg"

    .line 17
    .line 18
    invoke-static {p4, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    sget-object p2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->Companion:Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;

    .line 22
    .line 23
    invoke-virtual {p2}, Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;->〇o00〇〇Oo()Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;

    .line 24
    .line 25
    .line 26
    move-result-object p2

    .line 27
    sget-object p4, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_REQ_AUTH:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 28
    .line 29
    invoke-virtual {p4}, Lcom/intsig/developer/lib_message/mode/TcpCmd;->getCmdCode()I

    .line 30
    .line 31
    .line 32
    move-result p4

    .line 33
    const/4 v0, 0x2

    .line 34
    invoke-static {p4, v0}, Lcom/intsig/developer/lib_message/util/NumberConvertUtilKt;->〇080(II)[B

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {p2}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->O8()[B

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    const/4 v3, 0x0

    .line 43
    const/4 v4, 0x0

    .line 44
    const/4 v5, 0x0

    .line 45
    const/16 v6, 0xe

    .line 46
    .line 47
    const/4 v7, 0x0

    .line 48
    invoke-static/range {v1 .. v7}, Lkotlin/collections/ArraysKt;->o〇0([B[BIIIILjava/lang/Object;)[B

    .line 49
    .line 50
    .line 51
    invoke-static {}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->newBuilder()Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 52
    .line 53
    .line 54
    move-result-object p4

    .line 55
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;->〇8o8o〇()Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    if-nez p1, :cond_0

    .line 60
    .line 61
    const/4 p1, 0x0

    .line 62
    goto :goto_1

    .line 63
    :cond_0
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇080()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    if-nez v0, :cond_1

    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_1
    invoke-virtual {p4, v0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->〇80〇808〇O(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 71
    .line 72
    .line 73
    :goto_0
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->getDeviceId()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    invoke-virtual {p4, v1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->〇o〇(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 78
    .line 79
    .line 80
    const-string v1, "COM.CS.MAIN.01"

    .line 81
    .line 82
    invoke-virtual {p4, v1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->〇080(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 83
    .line 84
    .line 85
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇o〇()Z

    .line 86
    .line 87
    .line 88
    move-result v1

    .line 89
    invoke-virtual {p4, v1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->OO0o〇〇〇〇0(I)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 90
    .line 91
    .line 92
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->Oooo8o0〇()I

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    invoke-virtual {p4, v1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->〇〇888(I)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 97
    .line 98
    .line 99
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇o00〇〇Oo()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v1

    .line 103
    invoke-virtual {p4, v1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->OO0o〇〇(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 104
    .line 105
    .line 106
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇O00()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v1

    .line 110
    invoke-virtual {p4, v1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->O8(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 111
    .line 112
    .line 113
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->getCountry()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    invoke-virtual {p4, v1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 118
    .line 119
    .line 120
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 121
    .line 122
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    const-string v2, ""

    .line 127
    .line 128
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->〇〇808〇(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    invoke-virtual {p4, v1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->oO80(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 133
    .line 134
    .line 135
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->O8()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v1

    .line 139
    invoke-virtual {p4, v1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->〇O8o08O(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 140
    .line 141
    .line 142
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇80〇808〇O()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-virtual {p4, v1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->〇8o8o〇(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 147
    .line 148
    .line 149
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇〇888()Ljava/lang/String;

    .line 150
    .line 151
    .line 152
    move-result-object p1

    .line 153
    invoke-virtual {p4, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;->o〇0(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;

    .line 154
    .line 155
    .line 156
    move-object p1, v0

    .line 157
    :goto_1
    invoke-virtual {p4}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->build()Lcom/google/protobuf/GeneratedMessageLite;

    .line 158
    .line 159
    .line 160
    move-result-object p4

    .line 161
    check-cast p4, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 162
    .line 163
    invoke-virtual {p4}, Lcom/google/protobuf/AbstractMessageLite;->toByteArray()[B

    .line 164
    .line 165
    .line 166
    move-result-object p4

    .line 167
    invoke-virtual {p2, p4}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->〇8o8o〇([B)V

    .line 168
    .line 169
    .line 170
    invoke-interface {p3, p2}, Lio/netty/channel/ChannelOutboundInvoker;->writeAndFlush(Ljava/lang/Object;)Lio/netty/channel/ChannelFuture;

    .line 171
    .line 172
    .line 173
    const-string p2, "CMD_HAND_SHAKE_ACK cmd_req_auth token:"

    .line 174
    .line 175
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->〇〇808〇(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object p1

    .line 179
    const-string p2, "TcpCmd"

    .line 180
    .line 181
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    return-void
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

.method public useAesForBody()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/developer/lib_message/util/NumberConvertUtilKt;
.super Ljava/lang/Object;
.source "NumberConvertUtil.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public static final O8([B)J
    .locals 6
    .param p0    # [B
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<this>"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    array-length v0, p0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    if-nez v0, :cond_5

    .line 14
    .line 15
    array-length v0, p0

    .line 16
    const/16 v2, 0x8

    .line 17
    .line 18
    if-le v0, v2, :cond_1

    .line 19
    .line 20
    goto :goto_3

    .line 21
    :cond_1
    array-length v0, p0

    .line 22
    if-ge v0, v2, :cond_4

    .line 23
    .line 24
    new-array v0, v2, [B

    .line 25
    .line 26
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 27
    .line 28
    .line 29
    array-length v3, p0

    .line 30
    add-int/lit8 v3, v3, -0x1

    .line 31
    .line 32
    if-ltz v3, :cond_3

    .line 33
    .line 34
    :goto_1
    add-int/lit8 v4, v1, 0x1

    .line 35
    .line 36
    array-length v5, p0

    .line 37
    rsub-int/lit8 v5, v5, 0x8

    .line 38
    .line 39
    add-int/2addr v5, v1

    .line 40
    aget-byte v1, p0, v1

    .line 41
    .line 42
    aput-byte v1, v0, v5

    .line 43
    .line 44
    if-le v4, v3, :cond_2

    .line 45
    .line 46
    goto :goto_2

    .line 47
    :cond_2
    move v1, v4

    .line 48
    goto :goto_1

    .line 49
    :cond_3
    :goto_2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    .line 50
    .line 51
    .line 52
    move-result-object p0

    .line 53
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getLong()J

    .line 54
    .line 55
    .line 56
    move-result-wide v0

    .line 57
    return-wide v0

    .line 58
    :cond_4
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    .line 59
    .line 60
    .line 61
    move-result-object p0

    .line 62
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getLong()J

    .line 63
    .line 64
    .line 65
    move-result-wide v0

    .line 66
    return-wide v0

    .line 67
    :cond_5
    :goto_3
    const-wide/16 v0, 0x0

    .line 68
    .line 69
    return-wide v0
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static final 〇080(II)[B
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-array v0, p1, [B

    .line 2
    .line 3
    add-int/lit8 p1, p1, -0x1

    .line 4
    .line 5
    if-ltz p1, :cond_2

    .line 6
    .line 7
    move v1, p1

    .line 8
    :goto_0
    add-int/lit8 v2, v1, -0x1

    .line 9
    .line 10
    sub-int v3, p1, v1

    .line 11
    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    and-int/lit16 v1, p0, 0xff

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_0
    mul-int/lit8 v1, v1, 0x8

    .line 18
    .line 19
    shr-int v1, p0, v1

    .line 20
    .line 21
    and-int/lit16 v1, v1, 0xff

    .line 22
    .line 23
    :goto_1
    int-to-byte v1, v1

    .line 24
    aput-byte v1, v0, v3

    .line 25
    .line 26
    if-gez v2, :cond_1

    .line 27
    .line 28
    goto :goto_2

    .line 29
    :cond_1
    move v1, v2

    .line 30
    goto :goto_0

    .line 31
    :cond_2
    :goto_2
    return-object v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static final 〇o00〇〇Oo(J)[B
    .locals 8
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    new-array v0, v0, [B

    .line 4
    .line 5
    const/4 v1, 0x7

    .line 6
    :goto_0
    add-int/lit8 v2, v1, -0x1

    .line 7
    .line 8
    rsub-int/lit8 v3, v1, 0x7

    .line 9
    .line 10
    const-wide/16 v4, 0xff

    .line 11
    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    and-long/2addr v4, p0

    .line 15
    goto :goto_1

    .line 16
    :cond_0
    mul-int/lit8 v1, v1, 0x8

    .line 17
    .line 18
    shr-long v6, p0, v1

    .line 19
    .line 20
    and-long/2addr v4, v6

    .line 21
    :goto_1
    long-to-int v1, v4

    .line 22
    int-to-byte v1, v1

    .line 23
    aput-byte v1, v0, v3

    .line 24
    .line 25
    if-gez v2, :cond_1

    .line 26
    .line 27
    return-object v0

    .line 28
    :cond_1
    move v1, v2

    .line 29
    goto :goto_0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public static final 〇o〇([B)I
    .locals 6
    .param p0    # [B
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<this>"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    array-length v0, p0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    if-nez v0, :cond_5

    .line 14
    .line 15
    array-length v0, p0

    .line 16
    const/4 v2, 0x4

    .line 17
    if-le v0, v2, :cond_1

    .line 18
    .line 19
    goto :goto_3

    .line 20
    :cond_1
    array-length v0, p0

    .line 21
    if-ge v0, v2, :cond_4

    .line 22
    .line 23
    new-array v0, v2, [B

    .line 24
    .line 25
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 26
    .line 27
    .line 28
    array-length v3, p0

    .line 29
    add-int/lit8 v3, v3, -0x1

    .line 30
    .line 31
    if-ltz v3, :cond_3

    .line 32
    .line 33
    :goto_1
    add-int/lit8 v4, v1, 0x1

    .line 34
    .line 35
    array-length v5, p0

    .line 36
    rsub-int/lit8 v5, v5, 0x4

    .line 37
    .line 38
    add-int/2addr v5, v1

    .line 39
    aget-byte v1, p0, v1

    .line 40
    .line 41
    aput-byte v1, v0, v5

    .line 42
    .line 43
    if-le v4, v3, :cond_2

    .line 44
    .line 45
    goto :goto_2

    .line 46
    :cond_2
    move v1, v4

    .line 47
    goto :goto_1

    .line 48
    :cond_3
    :goto_2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    .line 49
    .line 50
    .line 51
    move-result-object p0

    .line 52
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    .line 53
    .line 54
    .line 55
    move-result p0

    .line 56
    return p0

    .line 57
    :cond_4
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    .line 58
    .line 59
    .line 60
    move-result-object p0

    .line 61
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    .line 62
    .line 63
    .line 64
    move-result p0

    .line 65
    return p0

    .line 66
    :cond_5
    :goto_3
    return v1
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

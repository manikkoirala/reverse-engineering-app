.class public final Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SocketConnectionCmd.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite<",
        "Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;",
        "Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest$Builder;",
        ">;",
        "Lcom/google/protobuf/MessageLiteOrBuilder;"
    }
.end annotation


# static fields
.field private static final DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

.field public static final LIST_FIELD_NUMBER:I = 0x1

.field private static volatile PARSER:Lcom/google/protobuf/Parser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser<",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private list_:Lcom/google/protobuf/Internal$ProtobufList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$ProtobufList<",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    .line 7
    .line 8
    const-class v1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    .line 9
    .line 10
    invoke-static {v1, v0}, Lcom/google/protobuf/GeneratedMessageLite;->registerDefaultInstance(Ljava/lang/Class;Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->emptyProtobufList()Lcom/google/protobuf/Internal$ProtobufList;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iput-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic access$14800()Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic access$14900(Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;ILcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->setList(ILcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static synthetic access$15000(Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->addList(Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static synthetic access$15100(Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;ILcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->addList(ILcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static synthetic access$15200(Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;Ljava/lang/Iterable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->addAllList(Ljava/lang/Iterable;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static synthetic access$15300(Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->clearList()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static synthetic access$15400(Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->removeList(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private addAllList(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->ensureListIsMutable()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 5
    .line 6
    invoke-static {p1, v0}, Lcom/google/protobuf/AbstractMessageLite;->addAll(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private addList(ILcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;)V
    .locals 1

    .line 4
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 5
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->ensureListIsMutable()V

    .line 6
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-void
.end method

.method private addList(Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->ensureListIsMutable()V

    .line 3
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private clearList()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->emptyProtobufList()Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private ensureListIsMutable()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/google/protobuf/Internal$ProtobufList;->isModifiable()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 10
    .line 11
    invoke-static {v0}, Lcom/google/protobuf/GeneratedMessageLite;->mutableCopy(Lcom/google/protobuf/Internal$ProtobufList;)Lcom/google/protobuf/Internal$ProtobufList;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static getDefaultInstance()Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static newBuilder()Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest$Builder;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite;->createBuilder()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest$Builder;

    return-object v0
.end method

.method public static newBuilder(Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest$Builder;
    .locals 1

    .line 2
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-virtual {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->createBuilder(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest$Builder;

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseDelimitedFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/io/InputStream;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseDelimitedFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 3
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 4
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/io/InputStream;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parseFrom(Ljava/nio/ByteBuffer;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parseFrom(Ljava/nio/ByteBuffer;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 2
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/nio/ByteBuffer;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parseFrom([B)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 5
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;[B)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 6
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;[BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    return-object p0
.end method

.method public static parser()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser<",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite;->getParserForType()Lcom/google/protobuf/Parser;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private removeList(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->ensureListIsMutable()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private setList(ILcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;)V
    .locals 1

    .line 1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->ensureListIsMutable()V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 8
    .line 9
    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method protected final dynamicMethod(Lcom/google/protobuf/GeneratedMessageLite$MethodToInvoke;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 1
    sget-object p2, Lcom/intsig/developer/lib_message/SocketConnectionCmd$1;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    aget p1, p2, p1

    .line 8
    .line 9
    const/4 p2, 0x1

    .line 10
    const/4 p3, 0x0

    .line 11
    packed-switch p1, :pswitch_data_0

    .line 12
    .line 13
    .line 14
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    .line 15
    .line 16
    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 17
    .line 18
    .line 19
    throw p1

    .line 20
    :pswitch_0
    return-object p3

    .line 21
    :pswitch_1
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    return-object p1

    .line 26
    :pswitch_2
    sget-object p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->PARSER:Lcom/google/protobuf/Parser;

    .line 27
    .line 28
    if-nez p1, :cond_1

    .line 29
    .line 30
    const-class p2, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    .line 31
    .line 32
    monitor-enter p2

    .line 33
    :try_start_0
    sget-object p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->PARSER:Lcom/google/protobuf/Parser;

    .line 34
    .line 35
    if-nez p1, :cond_0

    .line 36
    .line 37
    new-instance p1, Lcom/google/protobuf/GeneratedMessageLite$DefaultInstanceBasedParser;

    .line 38
    .line 39
    sget-object p3, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    .line 40
    .line 41
    invoke-direct {p1, p3}, Lcom/google/protobuf/GeneratedMessageLite$DefaultInstanceBasedParser;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 42
    .line 43
    .line 44
    sput-object p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->PARSER:Lcom/google/protobuf/Parser;

    .line 45
    .line 46
    :cond_0
    monitor-exit p2

    .line 47
    goto :goto_0

    .line 48
    :catchall_0
    move-exception p1

    .line 49
    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    throw p1

    .line 51
    :cond_1
    :goto_0
    return-object p1

    .line 52
    :pswitch_3
    sget-object p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    .line 53
    .line 54
    return-object p1

    .line 55
    :pswitch_4
    const/4 p1, 0x2

    .line 56
    new-array p1, p1, [Ljava/lang/Object;

    .line 57
    .line 58
    const/4 p3, 0x0

    .line 59
    const-string v0, "list_"

    .line 60
    .line 61
    aput-object v0, p1, p3

    .line 62
    .line 63
    const-class p3, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;

    .line 64
    .line 65
    aput-object p3, p1, p2

    .line 66
    .line 67
    const-string p2, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b"

    .line 68
    .line 69
    sget-object p3, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    .line 70
    .line 71
    invoke-static {p3, p2, p1}, Lcom/google/protobuf/GeneratedMessageLite;->newMessageInfo(Lcom/google/protobuf/MessageLite;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    return-object p1

    .line 76
    :pswitch_5
    new-instance p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest$Builder;

    .line 77
    .line 78
    invoke-direct {p1, p3}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest$Builder;-><init>(Lcom/intsig/developer/lib_message/SocketConnectionCmd$1;)V

    .line 79
    .line 80
    .line 81
    return-object p1

    .line 82
    :pswitch_6
    new-instance p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;

    .line 83
    .line 84
    invoke-direct {p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;-><init>()V

    .line 85
    .line 86
    .line 87
    return-object p1

    .line 88
    nop

    .line 89
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public getList(I)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getListCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getListList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getListOrBuilder(I)Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItemOrBuilder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItemOrBuilder;

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getListOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportItemOrBuilder;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$ReportMsgsRequest;->list_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

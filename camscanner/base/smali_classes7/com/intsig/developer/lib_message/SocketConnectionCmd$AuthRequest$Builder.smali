.class public final Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SocketConnectionCmd.java"

# interfaces
.implements Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequestOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder<",
        "Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;",
        "Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;",
        ">;",
        "Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequestOrBuilder;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 2
    invoke-static {}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$800()Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/developer/lib_message/SocketConnectionCmd$1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public O8(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$2200(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public OO0o〇〇(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$1900(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public OO0o〇〇〇〇0(I)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$900(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;I)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getType()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->getType()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$3100(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public o〇0(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$4000(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇080(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$2800(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇80〇808〇O(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$1100(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇8o8o〇(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$4300(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇O8o08O(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$3700(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$2500(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇o〇(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$1400(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇888(I)Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest$Builder;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->copyOnWrite()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$Builder;->instance:Lcom/google/protobuf/GeneratedMessageLite;

    .line 5
    .line 6
    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;

    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;->access$1700(Lcom/intsig/developer/lib_message/SocketConnectionCmd$AuthRequest;I)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

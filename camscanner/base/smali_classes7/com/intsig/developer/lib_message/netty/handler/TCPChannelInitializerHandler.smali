.class public final Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;
.super Lio/netty/channel/ChannelInitializer;
.source "TCPChannelInitializerHandler.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/netty/channel/ChannelInitializer<",
        "Lio/netty/channel/Channel;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO:Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final o0:Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$securityEncryptionInterface$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->OO:Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;)V
    .locals 1
    .param p1    # Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "imsClientInterface"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lio/netty/channel/ChannelInitializer;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->o0:Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;

    .line 10
    .line 11
    new-instance p1, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$securityEncryptionInterface$1;

    .line 12
    .line 13
    invoke-direct {p1}, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$securityEncryptionInterface$1;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->〇OOo8〇0:Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$securityEncryptionInterface$1;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public final Oo08()Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->o0:Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected initChannel(Lio/netty/channel/Channel;)V
    .locals 12

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    goto/16 :goto_0

    .line 4
    .line 5
    :cond_0
    invoke-interface {p1}, Lio/netty/channel/Channel;->pipeline()Lio/netty/channel/ChannelPipeline;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    if-nez p1, :cond_1

    .line 10
    .line 11
    goto/16 :goto_0

    .line 12
    .line 13
    :cond_1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->〇OOo8〇0:Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$securityEncryptionInterface$1;

    .line 14
    .line 15
    const/4 v1, 0x0

    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$securityEncryptionInterface$1;->〇080([B)V

    .line 17
    .line 18
    .line 19
    new-instance v0, Ljava/util/Random;

    .line 20
    .line 21
    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 22
    .line 23
    .line 24
    const/16 v1, 0x1e

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    int-to-long v0, v0

    .line 31
    const-wide/16 v2, 0x1e

    .line 32
    .line 33
    add-long v7, v0, v2

    .line 34
    .line 35
    const-class v0, Lio/netty/handler/timeout/IdleStateHandler;

    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    new-instance v1, Lio/netty/handler/timeout/IdleStateHandler;

    .line 42
    .line 43
    const-wide/16 v5, 0x0

    .line 44
    .line 45
    const-wide/16 v9, 0x0

    .line 46
    .line 47
    sget-object v11, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 48
    .line 49
    move-object v4, v1

    .line 50
    invoke-direct/range {v4 .. v11}, Lio/netty/handler/timeout/IdleStateHandler;-><init>(JJJLjava/util/concurrent/TimeUnit;)V

    .line 51
    .line 52
    .line 53
    invoke-interface {p1, v0, v1}, Lio/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelPipeline;

    .line 54
    .line 55
    .line 56
    const-class v0, Lcom/intsig/developer/lib_message/netty/handler/ByteFrameEncodeHandler;

    .line 57
    .line 58
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    new-instance v1, Lcom/intsig/developer/lib_message/netty/handler/ByteFrameEncodeHandler;

    .line 63
    .line 64
    invoke-direct {v1}, Lcom/intsig/developer/lib_message/netty/handler/ByteFrameEncodeHandler;-><init>()V

    .line 65
    .line 66
    .line 67
    invoke-interface {p1, v0, v1}, Lio/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelPipeline;

    .line 68
    .line 69
    .line 70
    const-class v0, Lio/netty/handler/codec/LengthFieldBasedFrameDecoder;

    .line 71
    .line 72
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    new-instance v7, Lio/netty/handler/codec/LengthFieldBasedFrameDecoder;

    .line 77
    .line 78
    const/high16 v2, 0x6400000

    .line 79
    .line 80
    const/4 v3, 0x0

    .line 81
    const/4 v4, 0x4

    .line 82
    const/4 v5, -0x4

    .line 83
    const/4 v6, 0x4

    .line 84
    move-object v1, v7

    .line 85
    invoke-direct/range {v1 .. v6}, Lio/netty/handler/codec/LengthFieldBasedFrameDecoder;-><init>(IIIII)V

    .line 86
    .line 87
    .line 88
    invoke-interface {p1, v0, v7}, Lio/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelPipeline;

    .line 89
    .line 90
    .line 91
    const-class v0, Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder;

    .line 92
    .line 93
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    new-instance v1, Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder;

    .line 98
    .line 99
    iget-object v2, p0, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->〇OOo8〇0:Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$securityEncryptionInterface$1;

    .line 100
    .line 101
    invoke-direct {v1, v2}, Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder;-><init>(Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;)V

    .line 102
    .line 103
    .line 104
    invoke-interface {p1, v0, v1}, Lio/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelPipeline;

    .line 105
    .line 106
    .line 107
    const-class v0, Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder;

    .line 108
    .line 109
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    new-instance v1, Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder;

    .line 114
    .line 115
    iget-object v2, p0, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->〇OOo8〇0:Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$securityEncryptionInterface$1;

    .line 116
    .line 117
    invoke-direct {v1, v2}, Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder;-><init>(Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;)V

    .line 118
    .line 119
    .line 120
    invoke-interface {p1, v0, v1}, Lio/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelPipeline;

    .line 121
    .line 122
    .line 123
    const-class v0, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler;

    .line 124
    .line 125
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    new-instance v1, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler;

    .line 130
    .line 131
    invoke-virtual {p0}, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->Oo08()Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;

    .line 132
    .line 133
    .line 134
    move-result-object v2

    .line 135
    iget-object v3, p0, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->〇OOo8〇0:Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$securityEncryptionInterface$1;

    .line 136
    .line 137
    invoke-direct {v1, v2, v3}, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler;-><init>(Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;)V

    .line 138
    .line 139
    .line 140
    invoke-interface {p1, v0, v1}, Lio/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelPipeline;

    .line 141
    .line 142
    .line 143
    const-class v0, Lcom/intsig/developer/lib_message/netty/handler/HeartbeatHandler;

    .line 144
    .line 145
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    new-instance v1, Lcom/intsig/developer/lib_message/netty/handler/HeartbeatHandler;

    .line 150
    .line 151
    invoke-virtual {p0}, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->Oo08()Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;

    .line 152
    .line 153
    .line 154
    move-result-object v2

    .line 155
    invoke-direct {v1, v2}, Lcom/intsig/developer/lib_message/netty/handler/HeartbeatHandler;-><init>(Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;)V

    .line 156
    .line 157
    .line 158
    invoke-interface {p1, v0, v1}, Lio/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelPipeline;

    .line 159
    .line 160
    .line 161
    const-class v0, Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler;

    .line 162
    .line 163
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 164
    .line 165
    .line 166
    move-result-object v0

    .line 167
    new-instance v1, Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler;

    .line 168
    .line 169
    invoke-virtual {p0}, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->Oo08()Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;

    .line 170
    .line 171
    .line 172
    move-result-object v2

    .line 173
    iget-object v3, p0, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;->〇OOo8〇0:Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler$securityEncryptionInterface$1;

    .line 174
    .line 175
    invoke-direct {v1, v2, v3}, Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler;-><init>(Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;)V

    .line 176
    .line 177
    .line 178
    invoke-interface {p1, v0, v1}, Lio/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lio/netty/channel/ChannelHandler;)Lio/netty/channel/ChannelPipeline;

    .line 179
    .line 180
    .line 181
    :goto_0
    return-void
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.class public final Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder;
.super Lio/netty/handler/codec/MessageToMessageDecoder;
.source "AESByteDecoder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/netty/handler/codec/MessageToMessageDecoder<",
        "Lio/netty/buffer/ByteBuf;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final 〇OOo8〇0:Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final o0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder;->〇OOo8〇0:Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;)V
    .locals 1
    .param p1    # Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "seSecurityEncrypt"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lio/netty/handler/codec/MessageToMessageDecoder;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder;->o0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method protected decode(Lio/netty/channel/ChannelHandlerContext;Lio/netty/buffer/ByteBuf;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/netty/channel/ChannelHandlerContext;",
            "Lio/netty/buffer/ByteBuf;",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "AESByteDecoder"

    if-nez p1, :cond_0

    const-string p1, "decode ctx == null"

    .line 2
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    if-nez p2, :cond_1

    const-string p1, "decode msg == null"

    .line 3
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 4
    :cond_1
    invoke-virtual {p2}, Lio/netty/buffer/ByteBuf;->readableBytes()I

    move-result p1

    const/16 v1, 0x14

    if-ge p1, v1, :cond_2

    .line 5
    invoke-virtual {p2}, Lio/netty/buffer/ByteBuf;->readableBytes()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string p2, "decode msg.readableBytes()="

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->〇〇808〇(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 6
    :cond_2
    new-instance p1, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;

    invoke-direct {p1}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;-><init>()V

    .line 7
    invoke-virtual {p1, p2}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->OO0o〇〇〇〇0(Lio/netty/buffer/ByteBuf;)V

    .line 8
    sget-object p2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->Companion:Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;

    invoke-virtual {p1}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->O8()[B

    move-result-object v1

    invoke-static {v1}, Lcom/intsig/developer/lib_message/util/NumberConvertUtilKt;->〇o〇([B)I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;->O8(I)Lcom/intsig/developer/lib_message/mode/TcpCmd;

    move-result-object p2

    .line 9
    invoke-virtual {p2}, Lcom/intsig/developer/lib_message/mode/TcpCmd;->useAesForBody()Z

    move-result p2

    if-eqz p2, :cond_4

    .line 10
    iget-object p2, p0, Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder;->o0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;

    invoke-interface {p2}, Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;->〇o00〇〇Oo()[B

    move-result-object p2

    if-nez p2, :cond_3

    const-string p1, "decode seSecurityEncrypt.getAesKeyByteArray() == null"

    .line 11
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 12
    :cond_3
    sget-object p2, Lcom/intsig/developer/lib_message/util/EncryptUtil$AES;->〇080:Lcom/intsig/developer/lib_message/util/EncryptUtil$AES$Companion;

    .line 13
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder;->o0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;

    invoke-interface {v0}, Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;->〇o00〇〇Oo()[B

    move-result-object v0

    .line 14
    invoke-virtual {p1}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->o〇0()[B

    move-result-object v1

    .line 15
    invoke-virtual {p2, v0, v1}, Lcom/intsig/developer/lib_message/util/EncryptUtil$AES$Companion;->〇080([B[B)[B

    move-result-object p2

    if-eqz p2, :cond_4

    .line 16
    invoke-virtual {p1, p2}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->〇8o8o〇([B)V

    :cond_4
    if-nez p3, :cond_5

    goto :goto_0

    .line 17
    :cond_5
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method

.method public bridge synthetic decode(Lio/netty/channel/ChannelHandlerContext;Ljava/lang/Object;Ljava/util/List;)V
    .locals 0

    .line 1
    check-cast p2, Lio/netty/buffer/ByteBuf;

    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/developer/lib_message/netty/handler/AESByteDecoder;->decode(Lio/netty/channel/ChannelHandlerContext;Lio/netty/buffer/ByteBuf;Ljava/util/List;)V

    return-void
.end method

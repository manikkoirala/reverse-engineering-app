.class public final Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder;
.super Lio/netty/handler/codec/MessageToMessageEncoder;
.source "AESByteEncoder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/netty/handler/codec/MessageToMessageEncoder<",
        "Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final 〇OOo8〇0:Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final o0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder;->〇OOo8〇0:Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;)V
    .locals 1
    .param p1    # Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "seSecurityEncrypt"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lio/netty/handler/codec/MessageToMessageEncoder;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder;->o0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method protected Oo08(Lio/netty/channel/ChannelHandlerContext;Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/netty/channel/ChannelHandlerContext;",
            "Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "AESByteEncoder"

    .line 2
    .line 3
    if-nez p2, :cond_0

    .line 4
    .line 5
    const-string p1, "encode msg == null"

    .line 6
    .line 7
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    if-nez p1, :cond_1

    .line 12
    .line 13
    const-string p1, "encode ctx == null"

    .line 14
    .line 15
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->O8()[B

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-static {v1}, Lcom/intsig/developer/lib_message/util/NumberConvertUtilKt;->〇o〇([B)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    sget-object v2, Lcom/intsig/developer/lib_message/mode/TcpCmd;->Companion:Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;

    .line 28
    .line 29
    invoke-virtual {v2, v1}, Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;->O8(I)Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    const-string v3, "encode cmdId="

    .line 34
    .line 35
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->〇〇808〇(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v2}, Lcom/intsig/developer/lib_message/mode/TcpCmd;->useAesForBody()Z

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    if-eqz v1, :cond_3

    .line 51
    .line 52
    iget-object v1, p0, Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder;->o0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;

    .line 53
    .line 54
    invoke-interface {v1}, Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;->〇o00〇〇Oo()[B

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    if-nez v1, :cond_2

    .line 59
    .line 60
    const-string p1, "encode seSecurityEncrypt.getAesKeyByteArray() == null"

    .line 61
    .line 62
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    return-void

    .line 66
    :cond_2
    sget-object v0, Lcom/intsig/developer/lib_message/util/EncryptUtil$AES;->〇080:Lcom/intsig/developer/lib_message/util/EncryptUtil$AES$Companion;

    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder;->o0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;

    .line 69
    .line 70
    invoke-interface {v1}, Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;->〇o00〇〇Oo()[B

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-virtual {p2}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->o〇0()[B

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    invoke-virtual {v0, v1, v2}, Lcom/intsig/developer/lib_message/util/EncryptUtil$AES$Companion;->〇o00〇〇Oo([B[B)[B

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    if-eqz v0, :cond_3

    .line 83
    .line 84
    invoke-virtual {p2, v0}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->〇8o8o〇([B)V

    .line 85
    .line 86
    .line 87
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->〇o00〇〇Oo()[B

    .line 88
    .line 89
    .line 90
    move-result-object p2

    .line 91
    invoke-interface {p1}, Lio/netty/channel/ChannelHandlerContext;->alloc()Lio/netty/buffer/ByteBufAllocator;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    array-length v0, p2

    .line 96
    invoke-interface {p1, v0}, Lio/netty/buffer/ByteBufAllocator;->buffer(I)Lio/netty/buffer/ByteBuf;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    invoke-virtual {p1, p2}, Lio/netty/buffer/ByteBuf;->writeBytes([B)Lio/netty/buffer/ByteBuf;

    .line 101
    .line 102
    .line 103
    if-nez p3, :cond_4

    .line 104
    .line 105
    goto :goto_0

    .line 106
    :cond_4
    invoke-virtual {p1}, Lio/netty/buffer/ByteBuf;->retain()Lio/netty/buffer/ByteBuf;

    .line 107
    .line 108
    .line 109
    move-result-object p1

    .line 110
    const-string p2, "outPutByteBuf.retain()"

    .line 111
    .line 112
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    .line 117
    .line 118
    :goto_0
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public bridge synthetic encode(Lio/netty/channel/ChannelHandlerContext;Ljava/lang/Object;Ljava/util/List;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/developer/lib_message/netty/handler/AESByteEncoder;->Oo08(Lio/netty/channel/ChannelHandlerContext;Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;Ljava/util/List;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

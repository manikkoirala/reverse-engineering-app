.class public final Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler;
.super Lio/netty/channel/ChannelInboundHandlerAdapter;
.source "TcpExceptionHandler.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final OO:Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final o0:Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler;->OO:Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;)V
    .locals 1
    .param p1    # Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "imsClientInterface"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "seSecurityEncrypt"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lio/netty/channel/ChannelInboundHandlerAdapter;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler;->o0:Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler;->〇OOo8〇0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public exceptionCaught(Lio/netty/channel/ChannelHandlerContext;Ljava/lang/Throwable;)V
    .locals 1

    .line 1
    const-string p1, "TcpExceptionHandler"

    .line 2
    .line 3
    const-string v0, "exceptionCaught "

    .line 4
    .line 5
    invoke-static {p1, v0, p2}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler;->o0:Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;

    .line 9
    .line 10
    const/4 p2, 0x1

    .line 11
    invoke-interface {p1, p2}, Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;->〇O8o08O(Z)V

    .line 12
    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/handler/TcpExceptionHandler;->〇OOo8〇0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;

    .line 15
    .line 16
    const/4 p2, 0x0

    .line 17
    invoke-interface {p1, p2}, Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;->〇080([B)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.class public final Lcom/intsig/developer/lib_message/netty/NettyTcpClient;
.super Ljava/lang/Object;
.source "NettyTcpClient.kt"

# interfaces
.implements Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/developer/lib_message/netty/NettyTcpClient$Companion;,
        Lcom/intsig/developer/lib_message/netty/NettyTcpClient$MessageTaskData;,
        Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO0o〇〇:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static Oooo8o0〇:I


# instance fields
.field private O8:Lcom/intsig/developer/lib_message/listener/IMSConnectStatusCallback;

.field private OO0o〇〇〇〇0:Landroid/os/Handler;

.field private Oo08:Lio/netty/bootstrap/Bootstrap;

.field private volatile oO80:I

.field private o〇0:Lio/netty/channel/ChannelFuture;

.field private 〇080:Ljava/lang/String;

.field private volatile 〇80〇808〇O:Z

.field private 〇8o8o〇:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue<",
            "Lcom/intsig/developer/lib_message/netty/NettyTcpClient$MessageTaskData;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇O8o08O:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o00〇〇Oo:I

.field private 〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

.field private 〇〇888:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$Companion;

    .line 8
    .line 9
    const/4 v0, -0x1

    .line 10
    sput v0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->Oooo8o0〇:I

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 8

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o00〇〇Oo:I

    .line 6
    .line 7
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    .line 8
    .line 9
    const/16 v1, 0x800

    .line 10
    .line 11
    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇8o8o〇:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇00()V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->O8ooOoo〇()V

    .line 20
    .line 21
    .line 22
    new-instance v0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;

    .line 23
    .line 24
    const/4 v3, 0x0

    .line 25
    const/4 v4, 0x0

    .line 26
    const/4 v5, 0x0

    .line 27
    const/4 v6, 0x7

    .line 28
    const/4 v7, 0x0

    .line 29
    move-object v2, v0

    .line 30
    invoke-direct/range {v2 .. v7}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;-><init>(Ljava/lang/String;ILjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 31
    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇O8o08O:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final O8ooOoo〇()V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/Thread;

    .line 2
    .line 3
    new-instance v1, LOoOO〇/〇o00〇〇Oo;

    .line 4
    .line 5
    invoke-direct {v1, p0}, LOoOO〇/〇o00〇〇Oo;-><init>(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic OoO8(I)V
    .locals 0

    .line 1
    sput p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->Oooo8o0〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private static final O〇8O8〇008(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;Landroid/os/Message;)Z
    .locals 5

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "msg"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget v0, p1, Landroid/os/Message;->what:I

    .line 12
    .line 13
    const/16 v1, 0x65

    .line 14
    .line 15
    const-string v2, "NettyTcpClient"

    .line 16
    .line 17
    const/4 v3, 0x0

    .line 18
    const/4 v4, 0x1

    .line 19
    packed-switch v0, :pswitch_data_0

    .line 20
    .line 21
    .line 22
    goto/16 :goto_8

    .line 23
    .line 24
    :pswitch_0
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 25
    .line 26
    if-nez p1, :cond_0

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/16 v0, 0x68

    .line 30
    .line 31
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 32
    .line 33
    .line 34
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->Oo08()V

    .line 35
    .line 36
    .line 37
    goto/16 :goto_7

    .line 38
    .line 39
    :pswitch_1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 40
    .line 41
    instance-of v0, p1, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$MessageTaskData;

    .line 42
    .line 43
    if-eqz v0, :cond_e

    .line 44
    .line 45
    :try_start_0
    iget-object p0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇8o8o〇:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 46
    .line 47
    if-eqz p1, :cond_1

    .line 48
    .line 49
    check-cast p1, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$MessageTaskData;

    .line 50
    .line 51
    invoke-virtual {p0, p1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    goto/16 :goto_7

    .line 55
    .line 56
    :cond_1
    new-instance p0, Ljava/lang/NullPointerException;

    .line 57
    .line 58
    const-string p1, "null cannot be cast to non-null type com.intsig.developer.lib_message.netty.NettyTcpClient.MessageTaskData"

    .line 59
    .line 60
    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    throw p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    :catchall_0
    move-exception p0

    .line 65
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object p0

    .line 69
    invoke-static {v2, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    goto/16 :goto_7

    .line 73
    .line 74
    :pswitch_2
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 75
    .line 76
    if-nez p1, :cond_2

    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_2
    const/16 v0, 0x66

    .line 80
    .line 81
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 82
    .line 83
    .line 84
    :goto_1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇oo〇()V

    .line 85
    .line 86
    .line 87
    goto/16 :goto_7

    .line 88
    .line 89
    :pswitch_3
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 90
    .line 91
    if-nez v0, :cond_3

    .line 92
    .line 93
    goto :goto_2

    .line 94
    :cond_3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 95
    .line 96
    .line 97
    :goto_2
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 98
    .line 99
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 100
    .line 101
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 102
    .line 103
    .line 104
    move-result p1

    .line 105
    invoke-direct {p0, p1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇0000OOO(Z)V

    .line 106
    .line 107
    .line 108
    goto/16 :goto_7

    .line 109
    .line 110
    :pswitch_4
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 111
    .line 112
    if-nez p1, :cond_4

    .line 113
    .line 114
    goto :goto_3

    .line 115
    :cond_4
    const/16 v0, 0x64

    .line 116
    .line 117
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 118
    .line 119
    .line 120
    :goto_3
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 121
    .line 122
    if-nez p1, :cond_5

    .line 123
    .line 124
    goto :goto_4

    .line 125
    :cond_5
    invoke-virtual {p1, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 126
    .line 127
    .line 128
    :goto_4
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 129
    .line 130
    if-nez p1, :cond_7

    .line 131
    .line 132
    :cond_6
    const/4 p1, 0x0

    .line 133
    goto :goto_5

    .line 134
    :cond_7
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇O〇()Z

    .line 135
    .line 136
    .line 137
    move-result p1

    .line 138
    if-ne p1, v4, :cond_6

    .line 139
    .line 140
    const/4 p1, 0x1

    .line 141
    :goto_5
    if-eqz p1, :cond_d

    .line 142
    .line 143
    invoke-virtual {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇()Z

    .line 144
    .line 145
    .line 146
    move-result p1

    .line 147
    if-eqz p1, :cond_8

    .line 148
    .line 149
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o800o8O()V

    .line 150
    .line 151
    .line 152
    goto :goto_7

    .line 153
    :cond_8
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇O8o08O:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;

    .line 154
    .line 155
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇080:Ljava/lang/String;

    .line 156
    .line 157
    iget v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o00〇〇Oo:I

    .line 158
    .line 159
    iget-object v3, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 160
    .line 161
    if-nez v3, :cond_9

    .line 162
    .line 163
    const/4 v3, 0x0

    .line 164
    goto :goto_6

    .line 165
    :cond_9
    invoke-interface {v3}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇080()Ljava/lang/String;

    .line 166
    .line 167
    .line 168
    move-result-object v3

    .line 169
    :goto_6
    invoke-virtual {p1, v0, v1, v3}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;->〇080(Ljava/lang/String;ILjava/lang/String;)Z

    .line 170
    .line 171
    .line 172
    move-result p1

    .line 173
    const-string v0, "socket"

    .line 174
    .line 175
    if-eqz p1, :cond_b

    .line 176
    .line 177
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇oo〇()V

    .line 178
    .line 179
    .line 180
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o800o8O()V

    .line 181
    .line 182
    .line 183
    iget-object p0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 184
    .line 185
    if-nez p0, :cond_a

    .line 186
    .line 187
    goto :goto_7

    .line 188
    :cond_a
    const-string p1, "reconnect"

    .line 189
    .line 190
    invoke-interface {p0, v0, p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->OO0o〇〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    goto :goto_7

    .line 194
    :cond_b
    const-string p1, "no need reconnect lastConnectParameter:"

    .line 195
    .line 196
    iget-object v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇O8o08O:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;

    .line 197
    .line 198
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇〇808〇(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object p1

    .line 202
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    iget-object p0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 206
    .line 207
    if-nez p0, :cond_c

    .line 208
    .line 209
    goto :goto_7

    .line 210
    :cond_c
    const-string p1, "nothing"

    .line 211
    .line 212
    invoke-interface {p0, v0, p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->OO0o〇〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    .line 214
    .line 215
    goto :goto_7

    .line 216
    :cond_d
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇O8o08O:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;

    .line 217
    .line 218
    invoke-virtual {p1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;->〇o00〇〇Oo()V

    .line 219
    .line 220
    .line 221
    iput v3, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oO80:I

    .line 222
    .line 223
    const-string p0, "MSG_CONNECT isNetworkAvailable = false"

    .line 224
    .line 225
    invoke-static {v2, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    :cond_e
    :goto_7
    const/4 v3, 0x1

    .line 229
    :goto_8
    return v3

    .line 230
    nop

    .line 231
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private final o800o8O()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const-string v1, ", tcpPort:"

    .line 8
    .line 9
    const-string v2, "NettyTcpClient"

    .line 10
    .line 11
    if-nez v0, :cond_a

    .line 12
    .line 13
    iget v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o00〇〇Oo:I

    .line 14
    .line 15
    if-gez v0, :cond_0

    .line 16
    .line 17
    goto/16 :goto_7

    .line 18
    .line 19
    :cond_0
    new-instance v0, Lio/netty/bootstrap/Bootstrap;

    .line 20
    .line 21
    invoke-direct {v0}, Lio/netty/bootstrap/Bootstrap;-><init>()V

    .line 22
    .line 23
    .line 24
    new-instance v3, Lio/netty/channel/nio/NioEventLoopGroup;

    .line 25
    .line 26
    const/4 v4, 0x1

    .line 27
    invoke-direct {v3, v4}, Lio/netty/channel/nio/NioEventLoopGroup;-><init>(I)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v3}, Lio/netty/bootstrap/AbstractBootstrap;->group(Lio/netty/channel/EventLoopGroup;)Lio/netty/bootstrap/AbstractBootstrap;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    check-cast v0, Lio/netty/bootstrap/Bootstrap;

    .line 35
    .line 36
    sget-object v3, Lio/netty/channel/ChannelOption;->TCP_NODELAY:Lio/netty/channel/ChannelOption;

    .line 37
    .line 38
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 39
    .line 40
    invoke-virtual {v0, v3, v4}, Lio/netty/bootstrap/AbstractBootstrap;->option(Lio/netty/channel/ChannelOption;Ljava/lang/Object;)Lio/netty/bootstrap/AbstractBootstrap;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    check-cast v0, Lio/netty/bootstrap/Bootstrap;

    .line 45
    .line 46
    sget-object v3, Lio/netty/channel/ChannelOption;->SO_KEEPALIVE:Lio/netty/channel/ChannelOption;

    .line 47
    .line 48
    invoke-virtual {v0, v3, v4}, Lio/netty/bootstrap/AbstractBootstrap;->option(Lio/netty/channel/ChannelOption;Ljava/lang/Object;)Lio/netty/bootstrap/AbstractBootstrap;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    check-cast v0, Lio/netty/bootstrap/Bootstrap;

    .line 53
    .line 54
    const-class v3, Lio/netty/channel/socket/nio/NioSocketChannel;

    .line 55
    .line 56
    invoke-virtual {v0, v3}, Lio/netty/bootstrap/AbstractBootstrap;->channel(Ljava/lang/Class;)Lio/netty/bootstrap/AbstractBootstrap;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    check-cast v0, Lio/netty/bootstrap/Bootstrap;

    .line 61
    .line 62
    new-instance v3, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;

    .line 63
    .line 64
    invoke-direct {v3, p0}, Lcom/intsig/developer/lib_message/netty/handler/TCPChannelInitializerHandler;-><init>(Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v3}, Lio/netty/bootstrap/AbstractBootstrap;->handler(Lio/netty/channel/ChannelHandler;)Lio/netty/bootstrap/AbstractBootstrap;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    check-cast v0, Lio/netty/bootstrap/Bootstrap;

    .line 72
    .line 73
    iget-object v3, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 74
    .line 75
    if-nez v3, :cond_1

    .line 76
    .line 77
    const/4 v3, 0x0

    .line 78
    goto :goto_0

    .line 79
    :cond_1
    invoke-interface {v3}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇O8o08O()I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    :goto_0
    if-lez v3, :cond_2

    .line 84
    .line 85
    sget-object v4, Lio/netty/channel/ChannelOption;->CONNECT_TIMEOUT_MILLIS:Lio/netty/channel/ChannelOption;

    .line 86
    .line 87
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 88
    .line 89
    .line 90
    move-result-object v3

    .line 91
    invoke-virtual {v0, v4, v3}, Lio/netty/bootstrap/AbstractBootstrap;->option(Lio/netty/channel/ChannelOption;Ljava/lang/Object;)Lio/netty/bootstrap/AbstractBootstrap;

    .line 92
    .line 93
    .line 94
    :cond_2
    sget-object v3, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 95
    .line 96
    iput-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->Oo08:Lio/netty/bootstrap/Bootstrap;

    .line 97
    .line 98
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->O8:Lcom/intsig/developer/lib_message/listener/IMSConnectStatusCallback;

    .line 99
    .line 100
    if-nez v0, :cond_3

    .line 101
    .line 102
    goto :goto_1

    .line 103
    :cond_3
    invoke-interface {v0}, Lcom/intsig/developer/lib_message/listener/IMSConnectStatusCallback;->〇080()V

    .line 104
    .line 105
    .line 106
    :goto_1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->Oo08:Lio/netty/bootstrap/Bootstrap;

    .line 107
    .line 108
    if-nez v0, :cond_4

    .line 109
    .line 110
    goto/16 :goto_6

    .line 111
    .line 112
    :cond_4
    const/4 v3, 0x0

    .line 113
    :try_start_0
    sget-object v4, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$Companion;

    .line 114
    .line 115
    invoke-static {v4}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$Companion;->〇080(Lcom/intsig/developer/lib_message/netty/NettyTcpClient$Companion;)V

    .line 116
    .line 117
    .line 118
    new-instance v4, Ljava/lang/StringBuilder;

    .line 119
    .line 120
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    .line 122
    .line 123
    const-string v5, "myPid:"

    .line 124
    .line 125
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    sget v5, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->Oooo8o0〇:I

    .line 129
    .line 130
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    const-string v5, ", host:"

    .line 134
    .line 135
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    iget-object v5, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇080:Ljava/lang/String;

    .line 139
    .line 140
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    .line 142
    .line 143
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    .line 145
    .line 146
    iget v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o00〇〇Oo:I

    .line 147
    .line 148
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    const-string v1, ", timeZone:"

    .line 152
    .line 153
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    .line 155
    .line 156
    iget-object v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 157
    .line 158
    if-nez v1, :cond_5

    .line 159
    .line 160
    move-object v1, v3

    .line 161
    goto :goto_2

    .line 162
    :cond_5
    invoke-interface {v1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇80〇808〇O()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v1

    .line 166
    :goto_2
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 167
    .line 168
    .line 169
    const-string v1, ", token:"

    .line 170
    .line 171
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    iget-object v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 175
    .line 176
    if-nez v1, :cond_6

    .line 177
    .line 178
    move-object v1, v3

    .line 179
    goto :goto_3

    .line 180
    :cond_6
    invoke-interface {v1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇080()Ljava/lang/String;

    .line 181
    .line 182
    .line 183
    move-result-object v1

    .line 184
    :goto_3
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 185
    .line 186
    .line 187
    const-string v1, " autoReConnectIndex:"

    .line 188
    .line 189
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    iget v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oO80:I

    .line 193
    .line 194
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 195
    .line 196
    .line 197
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 198
    .line 199
    .line 200
    move-result-object v1

    .line 201
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    .line 203
    .line 204
    iget-object v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇080:Ljava/lang/String;

    .line 205
    .line 206
    iget v4, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o00〇〇Oo:I

    .line 207
    .line 208
    invoke-virtual {v0, v1, v4}, Lio/netty/bootstrap/Bootstrap;->connect(Ljava/lang/String;I)Lio/netty/channel/ChannelFuture;

    .line 209
    .line 210
    .line 211
    move-result-object v0

    .line 212
    new-instance v1, LOoOO〇/O8;

    .line 213
    .line 214
    invoke-direct {v1, p0}, LOoOO〇/O8;-><init>(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;)V

    .line 215
    .line 216
    .line 217
    invoke-interface {v0, v1}, Lio/netty/channel/ChannelFuture;->addListener(Lio/netty/util/concurrent/GenericFutureListener;)Lio/netty/channel/ChannelFuture;

    .line 218
    .line 219
    .line 220
    move-result-object v0

    .line 221
    invoke-interface {v0}, Lio/netty/channel/ChannelFuture;->sync()Lio/netty/channel/ChannelFuture;

    .line 222
    .line 223
    .line 224
    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    goto :goto_4

    .line 226
    :catchall_0
    move-exception v0

    .line 227
    const-string v1, "connect try catch error: "

    .line 228
    .line 229
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 230
    .line 231
    .line 232
    move-result-object v0

    .line 233
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇808〇(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 234
    .line 235
    .line 236
    move-result-object v0

    .line 237
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    .line 239
    .line 240
    :goto_4
    invoke-virtual {p0, v3}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OOO〇O0(Lio/netty/channel/ChannelFuture;)V

    .line 241
    .line 242
    .line 243
    invoke-virtual {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o〇O8〇〇o()Lio/netty/channel/ChannelFuture;

    .line 244
    .line 245
    .line 246
    move-result-object v0

    .line 247
    if-nez v0, :cond_7

    .line 248
    .line 249
    goto :goto_5

    .line 250
    :cond_7
    invoke-interface {v0}, Lio/netty/channel/ChannelFuture;->channel()Lio/netty/channel/Channel;

    .line 251
    .line 252
    .line 253
    move-result-object v0

    .line 254
    if-nez v0, :cond_8

    .line 255
    .line 256
    goto :goto_5

    .line 257
    :cond_8
    invoke-interface {v0}, Lio/netty/channel/Channel;->closeFuture()Lio/netty/channel/ChannelFuture;

    .line 258
    .line 259
    .line 260
    move-result-object v0

    .line 261
    if-nez v0, :cond_9

    .line 262
    .line 263
    goto :goto_5

    .line 264
    :cond_9
    new-instance v1, LOoOO〇/Oo08;

    .line 265
    .line 266
    invoke-direct {v1, p0}, LOoOO〇/Oo08;-><init>(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;)V

    .line 267
    .line 268
    .line 269
    invoke-interface {v0, v1}, Lio/netty/channel/ChannelFuture;->addListener(Lio/netty/util/concurrent/GenericFutureListener;)Lio/netty/channel/ChannelFuture;

    .line 270
    .line 271
    .line 272
    :goto_5
    const-string v0, "connectServer end"

    .line 273
    .line 274
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    .line 276
    .line 277
    :goto_6
    return-void

    .line 278
    :cond_a
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 279
    .line 280
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 281
    .line 282
    .line 283
    const-string v3, "connectServer host:"

    .line 284
    .line 285
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    .line 287
    .line 288
    iget-object v3, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇080:Ljava/lang/String;

    .line 289
    .line 290
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 291
    .line 292
    .line 293
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    .line 295
    .line 296
    iget v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o00〇〇Oo:I

    .line 297
    .line 298
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 299
    .line 300
    .line 301
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 302
    .line 303
    .line 304
    move-result-object v0

    .line 305
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    .line 307
    .line 308
    return-void
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method private static final oo88o8O(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;Lio/netty/util/concurrent/Future;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Ljava/util/concurrent/Future;->isDone()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const-string v1, "closeFuture.cause(): "

    .line 11
    .line 12
    const-string v2, "NettyTcpClient"

    .line 13
    .line 14
    if-eqz v0, :cond_2

    .line 15
    .line 16
    invoke-interface {p1}, Lio/netty/util/concurrent/Future;->isSuccess()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_2

    .line 21
    .line 22
    iget-boolean v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇80〇808〇O:Z

    .line 23
    .line 24
    if-nez v0, :cond_1

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->O8:Lcom/intsig/developer/lib_message/listener/IMSConnectStatusCallback;

    .line 27
    .line 28
    if-nez v0, :cond_0

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    invoke-interface {v0}, Lcom/intsig/developer/lib_message/listener/IMSConnectStatusCallback;->onClose()V

    .line 32
    .line 33
    .line 34
    :goto_0
    const/4 v0, 0x1

    .line 35
    invoke-virtual {p0, v0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇O8o08O(Z)V

    .line 36
    .line 37
    .line 38
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 39
    .line 40
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-interface {p1}, Lio/netty/util/concurrent/Future;->cause()Ljava/lang/Throwable;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string p1, " isManualDisConnect:"

    .line 54
    .line 55
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    iget-boolean p0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇80〇808〇O:Z

    .line 59
    .line 60
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p0

    .line 67
    invoke-static {v2, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    .line 72
    .line 73
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    .line 75
    .line 76
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-interface {p1}, Lio/netty/util/concurrent/Future;->cause()Ljava/lang/Throwable;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    const-string v0, "  closeFuture.isDone:"

    .line 87
    .line 88
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-interface {p1}, Ljava/util/concurrent/Future;->isDone()Z

    .line 92
    .line 93
    .line 94
    move-result v0

    .line 95
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    const-string v0, ",  future.isSuccess:"

    .line 99
    .line 100
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-interface {p1}, Lio/netty/util/concurrent/Future;->isSuccess()Z

    .line 104
    .line 105
    .line 106
    move-result p1

    .line 107
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object p0

    .line 114
    invoke-static {v2, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    :goto_1
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method private final oo〇(J)V
    .locals 0

    .line 1
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    .line 10
    .line 11
    .line 12
    :goto_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private final o〇〇0〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇O8o08O:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇080:Ljava/lang/String;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;->〇o〇(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇O8o08O:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o00〇〇Oo:I

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;->O8(I)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇O8o08O:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 18
    .line 19
    if-nez v1, :cond_0

    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-interface {v1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇080()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;->Oo08(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final 〇00()V
    .locals 3

    .line 1
    new-instance v0, Landroid/os/HandlerThread;

    .line 2
    .line 3
    const-string v1, "NettyTcpClient"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 9
    .line 10
    .line 11
    new-instance v1, Landroid/os/Handler;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    new-instance v2, LOoOO〇/〇o〇;

    .line 18
    .line 19
    invoke-direct {v2, p0}, LOoOO〇/〇o〇;-><init>(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;)V

    .line 20
    .line 21
    .line 22
    invoke-direct {v1, v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 23
    .line 24
    .line 25
    iput-object v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private final 〇0000OOO(Z)V
    .locals 5

    .line 1
    const-string v0, "autoReConnect:"

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇〇808〇(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const-string v1, "NettyTcpClient"

    .line 12
    .line 13
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const/16 v0, 0x64

    .line 17
    .line 18
    if-eqz p1, :cond_4

    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇O8o08O:Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$ConnectParameter;->〇o00〇〇Oo()V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 26
    .line 27
    if-nez p1, :cond_0

    .line 28
    .line 29
    const/4 p1, 0x0

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->oO80()I

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    :goto_0
    if-lez p1, :cond_6

    .line 36
    .line 37
    iget v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oO80:I

    .line 38
    .line 39
    if-gt v1, p1, :cond_6

    .line 40
    .line 41
    iget p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oO80:I

    .line 42
    .line 43
    add-int/lit8 p1, p1, 0x1

    .line 44
    .line 45
    iput p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oO80:I

    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 48
    .line 49
    if-nez p1, :cond_1

    .line 50
    .line 51
    const-wide/16 v1, 0x0

    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_1
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇8o8o〇()J

    .line 55
    .line 56
    .line 57
    move-result-wide v1

    .line 58
    :goto_1
    const-wide/16 v3, 0x1f4

    .line 59
    .line 60
    cmp-long p1, v1, v3

    .line 61
    .line 62
    if-gez p1, :cond_2

    .line 63
    .line 64
    move-wide v1, v3

    .line 65
    :cond_2
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 66
    .line 67
    if-nez p1, :cond_3

    .line 68
    .line 69
    goto :goto_2

    .line 70
    :cond_3
    iget v3, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oO80:I

    .line 71
    .line 72
    int-to-long v3, v3

    .line 73
    mul-long v1, v1, v3

    .line 74
    .line 75
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 76
    .line 77
    .line 78
    goto :goto_2

    .line 79
    :cond_4
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 80
    .line 81
    if-nez p1, :cond_5

    .line 82
    .line 83
    goto :goto_2

    .line 84
    :cond_5
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 85
    .line 86
    .line 87
    :cond_6
    :goto_2
    return-void
.end method

.method public static final synthetic 〇0〇O0088o()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->Oooo8o0〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇O00(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->O〇8O8〇008(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final 〇O888o0o(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;Lio/netty/util/concurrent/Future;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v1, "future.isDone:"

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-interface {p1}, Ljava/util/concurrent/Future;->isDone()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v1, "  "

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const-string v1, "NettyTcpClient"

    .line 33
    .line 34
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-interface {p1}, Ljava/util/concurrent/Future;->isDone()Z

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-eqz v0, :cond_5

    .line 42
    .line 43
    invoke-interface {p1}, Lio/netty/util/concurrent/Future;->isSuccess()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_2

    .line 48
    .line 49
    const/4 p1, 0x0

    .line 50
    iput-boolean p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇80〇808〇O:Z

    .line 51
    .line 52
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o〇〇0〇()V

    .line 53
    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->O8:Lcom/intsig/developer/lib_message/listener/IMSConnectStatusCallback;

    .line 56
    .line 57
    if-nez v0, :cond_0

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_0
    invoke-interface {v0}, Lcom/intsig/developer/lib_message/listener/IMSConnectStatusCallback;->onConnected()V

    .line 61
    .line 62
    .line 63
    :goto_0
    iput p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oO80:I

    .line 64
    .line 65
    iget-object p0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 66
    .line 67
    if-nez p0, :cond_1

    .line 68
    .line 69
    goto :goto_3

    .line 70
    :cond_1
    const/16 p1, 0x68

    .line 71
    .line 72
    const-wide/32 v0, 0xea60

    .line 73
    .line 74
    .line 75
    invoke-virtual {p0, p1, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 76
    .line 77
    .line 78
    goto :goto_3

    .line 79
    :cond_2
    invoke-interface {p1}, Lio/netty/util/concurrent/Future;->cause()Ljava/lang/Throwable;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    if-eqz v0, :cond_5

    .line 84
    .line 85
    invoke-interface {p1}, Lio/netty/util/concurrent/Future;->cause()Ljava/lang/Throwable;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    if-nez p1, :cond_3

    .line 90
    .line 91
    const/4 p1, 0x0

    .line 92
    goto :goto_1

    .line 93
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    :goto_1
    const-string v0, "fait to connect: "

    .line 98
    .line 99
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->〇〇808〇(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->O8:Lcom/intsig/developer/lib_message/listener/IMSConnectStatusCallback;

    .line 107
    .line 108
    if-nez p1, :cond_4

    .line 109
    .line 110
    goto :goto_2

    .line 111
    :cond_4
    invoke-interface {p1}, Lcom/intsig/developer/lib_message/listener/IMSConnectStatusCallback;->onConnectFailed()V

    .line 112
    .line 113
    .line 114
    :goto_2
    const/4 p1, 0x1

    .line 115
    invoke-virtual {p0, p1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇O8o08O(Z)V

    .line 116
    .line 117
    .line 118
    :cond_5
    :goto_3
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.method public static synthetic 〇O〇(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;Lio/netty/util/concurrent/Future;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇O888o0o(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;Lio/netty/util/concurrent/Future;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private static final 〇oOO8O8(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;)V
    .locals 9

    .line 1
    const-string v0, "NettyTcpClient"

    .line 2
    .line 3
    const-string v1, "this$0"

    .line 4
    .line 5
    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇8o8o〇:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 9
    .line 10
    invoke-virtual {v1}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    check-cast v1, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$MessageTaskData;

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 17
    .line 18
    const/4 v3, 0x1

    .line 19
    const/4 v4, 0x0

    .line 20
    if-nez v2, :cond_2

    .line 21
    .line 22
    :cond_1
    const/4 v2, 0x0

    .line 23
    goto :goto_1

    .line 24
    :cond_2
    invoke-interface {v2}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->getUserId()J

    .line 25
    .line 26
    .line 27
    move-result-wide v5

    .line 28
    invoke-virtual {v1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$MessageTaskData;->〇o00〇〇Oo()J

    .line 29
    .line 30
    .line 31
    move-result-wide v7

    .line 32
    cmp-long v2, v5, v7

    .line 33
    .line 34
    if-nez v2, :cond_1

    .line 35
    .line 36
    const/4 v2, 0x1

    .line 37
    :goto_1
    if-eqz v2, :cond_0

    .line 38
    .line 39
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oO80()Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-eqz v2, :cond_a

    .line 44
    .line 45
    invoke-virtual {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇()Z

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    if-eqz v2, :cond_3

    .line 50
    .line 51
    goto :goto_7

    .line 52
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o〇O8〇〇o()Lio/netty/channel/ChannelFuture;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    if-nez v2, :cond_5

    .line 57
    .line 58
    :cond_4
    :goto_3
    const/4 v3, 0x0

    .line 59
    goto :goto_4

    .line 60
    :cond_5
    invoke-interface {v2}, Lio/netty/channel/ChannelFuture;->channel()Lio/netty/channel/Channel;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    if-nez v2, :cond_6

    .line 65
    .line 66
    goto :goto_3

    .line 67
    :cond_6
    invoke-interface {v2}, Lio/netty/channel/Channel;->isWritable()Z

    .line 68
    .line 69
    .line 70
    move-result v2

    .line 71
    if-ne v2, v3, :cond_4

    .line 72
    .line 73
    :goto_4
    if-eqz v3, :cond_9

    .line 74
    .line 75
    invoke-virtual {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o〇O8〇〇o()Lio/netty/channel/ChannelFuture;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    if-nez v2, :cond_7

    .line 80
    .line 81
    goto :goto_5

    .line 82
    :cond_7
    invoke-interface {v2}, Lio/netty/channel/ChannelFuture;->channel()Lio/netty/channel/Channel;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    if-nez v2, :cond_8

    .line 87
    .line 88
    goto :goto_5

    .line 89
    :cond_8
    invoke-virtual {v1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$MessageTaskData;->〇080()Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    invoke-interface {v2, v1}, Lio/netty/channel/ChannelOutboundInvoker;->writeAndFlush(Ljava/lang/Object;)Lio/netty/channel/ChannelFuture;

    .line 94
    .line 95
    .line 96
    :goto_5
    const-string v1, "send one msg"

    .line 97
    .line 98
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    goto :goto_0

    .line 102
    :cond_9
    const-wide/16 v2, 0x1388

    .line 103
    .line 104
    invoke-direct {p0, v2, v3}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oo〇(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 105
    .line 106
    .line 107
    :try_start_1
    iget-object v2, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇8o8o〇:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 108
    .line 109
    invoke-virtual {v2, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 110
    .line 111
    .line 112
    goto :goto_6

    .line 113
    :catchall_0
    move-exception v1

    .line 114
    :try_start_2
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 115
    .line 116
    .line 117
    :goto_6
    const-string v1, "send one msg write is busy"

    .line 118
    .line 119
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    goto :goto_0

    .line 123
    :cond_a
    :goto_7
    const-wide/16 v5, 0x3e8

    .line 124
    .line 125
    invoke-direct {p0, v5, v6}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oo〇(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 126
    .line 127
    .line 128
    goto :goto_2

    .line 129
    :catchall_1
    move-exception v1

    .line 130
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 131
    .line 132
    .line 133
    goto :goto_0

    .line 134
    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 135
    .line 136
    .line 137
    move-result-object v1

    .line 138
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 139
    .line 140
    .line 141
    goto/16 :goto_0
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private final 〇oo〇()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇80〇808〇O:Z

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇〇888:Z

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o〇0:Lio/netty/channel/ChannelFuture;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-interface {v0}, Lio/netty/channel/ChannelFuture;->channel()Lio/netty/channel/Channel;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    invoke-interface {v0}, Lio/netty/channel/ChannelOutboundInvoker;->close()Lio/netty/channel/ChannelFuture;

    .line 20
    .line 21
    .line 22
    :goto_0
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o〇0:Lio/netty/channel/ChannelFuture;

    .line 23
    .line 24
    if-nez v0, :cond_2

    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_2
    invoke-interface {v0}, Lio/netty/channel/ChannelFuture;->channel()Lio/netty/channel/Channel;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    if-nez v0, :cond_3

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_3
    invoke-interface {v0}, Lio/netty/channel/Channel;->eventLoop()Lio/netty/channel/EventLoop;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    if-nez v0, :cond_4

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_4
    invoke-interface {v0}, Lio/netty/util/concurrent/EventExecutorGroup;->shutdownGracefully()Lio/netty/util/concurrent/Future;

    .line 42
    .line 43
    .line 44
    :goto_1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->Oo08:Lio/netty/bootstrap/Bootstrap;

    .line 45
    .line 46
    if-nez v0, :cond_5

    .line 47
    .line 48
    goto :goto_2

    .line 49
    :cond_5
    invoke-virtual {v0}, Lio/netty/bootstrap/Bootstrap;->config()Lio/netty/bootstrap/BootstrapConfig;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    if-nez v0, :cond_6

    .line 54
    .line 55
    goto :goto_2

    .line 56
    :cond_6
    invoke-virtual {v0}, Lio/netty/bootstrap/AbstractBootstrapConfig;->group()Lio/netty/channel/EventLoopGroup;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    if-nez v0, :cond_7

    .line 61
    .line 62
    goto :goto_2

    .line 63
    :cond_7
    invoke-interface {v0}, Lio/netty/util/concurrent/EventExecutorGroup;->shutdownGracefully()Lio/netty/util/concurrent/Future;

    .line 64
    .line 65
    .line 66
    :goto_2
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public static synthetic 〇〇808〇(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;Lio/netty/util/concurrent/Future;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oo88o8O(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;Lio/netty/util/concurrent/Future;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public static synthetic 〇〇8O0〇8(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇oOO8O8(Lcom/intsig/developer/lib_message/netty/NettyTcpClient;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method public O8(Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;)V
    .locals 5

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const-string p1, "NettyTcpClient"

    .line 4
    .line 5
    const-string v0, "sendMsg msgBody == null"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 12
    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    goto :goto_1

    .line 16
    :cond_1
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    const-string v2, "it.obtainMessage()"

    .line 21
    .line 22
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/16 v2, 0x67

    .line 26
    .line 27
    iput v2, v1, Landroid/os/Message;->what:I

    .line 28
    .line 29
    new-instance v2, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$MessageTaskData;

    .line 30
    .line 31
    iget-object v3, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 32
    .line 33
    if-nez v3, :cond_2

    .line 34
    .line 35
    const-wide/16 v3, -0x1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_2
    invoke-interface {v3}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->getUserId()J

    .line 39
    .line 40
    .line 41
    move-result-wide v3

    .line 42
    :goto_0
    invoke-direct {v2, v3, v4, p1}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient$MessageTaskData;-><init>(JLcom/intsig/developer/lib_message/mode/MessageProtocolMode;)V

    .line 43
    .line 44
    .line 45
    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 46
    .line 47
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 48
    .line 49
    .line 50
    :goto_1
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public OO0o〇〇()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o〇0:Lio/netty/channel/ChannelFuture;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, 0x0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-interface {v0}, Lio/netty/channel/ChannelFuture;->channel()Lio/netty/channel/Channel;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    invoke-interface {v0}, Lio/netty/channel/Channel;->isOpen()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-ne v0, v1, :cond_2

    .line 20
    .line 21
    const/4 v2, 0x1

    .line 22
    :cond_2
    :goto_0
    xor-int/lit8 v0, v2, 0x1

    .line 23
    .line 24
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public OO0o〇〇〇〇0()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o〇0:Lio/netty/channel/ChannelFuture;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    goto :goto_0

    .line 9
    :cond_1
    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-ne v0, v2, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    :goto_0
    if-eqz v0, :cond_3

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o〇0:Lio/netty/channel/ChannelFuture;

    .line 19
    .line 20
    if-nez v0, :cond_2

    .line 21
    .line 22
    const/4 v0, 0x0

    .line 23
    goto :goto_1

    .line 24
    :cond_2
    invoke-interface {v0}, Lio/netty/util/concurrent/Future;->cause()Ljava/lang/Throwable;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    :goto_1
    if-eqz v0, :cond_3

    .line 29
    .line 30
    const/4 v1, 0x1

    .line 31
    :cond_3
    return v1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public final OOO〇O0(Lio/netty/channel/ChannelFuture;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o〇0:Lio/netty/channel/ChannelFuture;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public Oo08()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_1

    .line 6
    :cond_0
    invoke-interface {v0}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->o〇0()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const-string v2, "uploadPushTokenInfo, pushToken:"

    .line 11
    .line 12
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->〇〇808〇(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    const-string v3, "NettyTcpClient"

    .line 17
    .line 18
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-eqz v2, :cond_1

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_1
    invoke-static {}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$SyncInfoRequest;->newBuilder()Lcom/intsig/developer/lib_message/SocketConnectionCmd$SyncInfoRequest$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-virtual {v2, v1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$SyncInfoRequest$Builder;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$SyncInfoRequest$Builder;

    .line 33
    .line 34
    .line 35
    invoke-interface {v0}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->Oo08()Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    const/4 v1, 0x2

    .line 40
    if-eqz v0, :cond_2

    .line 41
    .line 42
    const/4 v0, 0x2

    .line 43
    goto :goto_0

    .line 44
    :cond_2
    const/4 v0, 0x1

    .line 45
    :goto_0
    invoke-virtual {v2, v0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$SyncInfoRequest$Builder;->〇080(I)Lcom/intsig/developer/lib_message/SocketConnectionCmd$SyncInfoRequest$Builder;

    .line 46
    .line 47
    .line 48
    new-instance v0, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;

    .line 49
    .line 50
    invoke-direct {v0}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;-><init>()V

    .line 51
    .line 52
    .line 53
    sget-object v3, Lcom/intsig/developer/lib_message/mode/TcpCmd;->CMD_SYNC_INFO:Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 54
    .line 55
    invoke-virtual {v3}, Lcom/intsig/developer/lib_message/mode/TcpCmd;->getCmdCode()I

    .line 56
    .line 57
    .line 58
    move-result v3

    .line 59
    invoke-static {v3, v1}, Lcom/intsig/developer/lib_message/util/NumberConvertUtilKt;->〇080(II)[B

    .line 60
    .line 61
    .line 62
    move-result-object v4

    .line 63
    invoke-virtual {v0}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->O8()[B

    .line 64
    .line 65
    .line 66
    move-result-object v5

    .line 67
    const/4 v6, 0x0

    .line 68
    const/4 v7, 0x0

    .line 69
    const/4 v8, 0x0

    .line 70
    const/16 v9, 0xe

    .line 71
    .line 72
    const/4 v10, 0x0

    .line 73
    invoke-static/range {v4 .. v10}, Lkotlin/collections/ArraysKt;->o〇0([B[BIIIILjava/lang/Object;)[B

    .line 74
    .line 75
    .line 76
    invoke-virtual {v2}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->build()Lcom/google/protobuf/GeneratedMessageLite;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    check-cast v1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$SyncInfoRequest;

    .line 81
    .line 82
    invoke-virtual {v1}, Lcom/google/protobuf/AbstractMessageLite;->toByteArray()[B

    .line 83
    .line 84
    .line 85
    move-result-object v1

    .line 86
    invoke-virtual {v0, v1}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->〇8o8o〇([B)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {p0, v0}, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->O8(Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;)V

    .line 90
    .line 91
    .line 92
    :goto_1
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.method public Oooo8o0〇(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "host"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇080:Ljava/lang/String;

    .line 7
    .line 8
    iput p2, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o00〇〇Oo:I

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method public oO80()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇〇888:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0(J)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const/16 v1, 0x64

    .line 7
    .line 8
    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 9
    .line 10
    .line 11
    :goto_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public final o〇O8〇〇o()Lio/netty/channel/ChannelFuture;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->o〇0:Lio/netty/channel/ChannelFuture;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇〇888:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇80〇808〇O(Lcom/intsig/developer/lib_message/listener/IMSConnectStatusCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->O8:Lcom/intsig/developer/lib_message/listener/IMSConnectStatusCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇8o8o〇()Lcom/intsig/developer/lib_message/listener/IMSEventListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O8o08O(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const-string v2, "it.obtainMessage()"

    .line 11
    .line 12
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const/16 v2, 0x65

    .line 16
    .line 17
    iput v2, v1, Landroid/os/Message;->what:I

    .line 18
    .line 19
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 26
    .line 27
    .line 28
    :goto_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method public 〇o00〇〇Oo()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-interface {v0}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->oO80()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    :goto_0
    const/4 v2, 0x1

    .line 13
    if-gt v2, v0, :cond_1

    .line 14
    .line 15
    iget v3, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oO80:I

    .line 16
    .line 17
    if-gt v0, v3, :cond_1

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    :cond_1
    if-eqz v1, :cond_5

    .line 21
    .line 22
    const-string v0, "NettyTcpClient"

    .line 23
    .line 24
    const-string v1, "autoReConnect"

    .line 25
    .line 26
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iput v2, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oO80:I

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 32
    .line 33
    if-nez v0, :cond_2

    .line 34
    .line 35
    const-wide/16 v0, 0x0

    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_2
    invoke-interface {v0}, Lcom/intsig/developer/lib_message/listener/IMSEventListener;->〇8o8o〇()J

    .line 39
    .line 40
    .line 41
    move-result-wide v0

    .line 42
    :goto_1
    const-wide/16 v2, 0x1f4

    .line 43
    .line 44
    cmp-long v4, v0, v2

    .line 45
    .line 46
    if-gez v4, :cond_3

    .line 47
    .line 48
    move-wide v0, v2

    .line 49
    :cond_3
    iget-object v2, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 50
    .line 51
    if-nez v2, :cond_4

    .line 52
    .line 53
    goto :goto_2

    .line 54
    :cond_4
    iget v3, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->oO80:I

    .line 55
    .line 56
    int-to-long v3, v3

    .line 57
    mul-long v0, v0, v3

    .line 58
    .line 59
    const/16 v3, 0x64

    .line 60
    .line 61
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 62
    .line 63
    .line 64
    :cond_5
    :goto_2
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method public 〇o〇(Lcom/intsig/developer/lib_message/listener/IMSEventListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->〇o〇:Lcom/intsig/developer/lib_message/listener/IMSEventListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public 〇〇888()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/netty/NettyTcpClient;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const/16 v1, 0x66

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 9
    .line 10
    .line 11
    :goto_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

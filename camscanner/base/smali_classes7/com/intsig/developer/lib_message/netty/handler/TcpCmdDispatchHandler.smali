.class public final Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler;
.super Lio/netty/channel/ChannelInboundHandlerAdapter;
.source "TcpCmdDispatchHandler.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO:Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final o0:Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler;->OO:Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;)V
    .locals 1
    .param p1    # Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "imsClientInterface"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "seSecurityEncrypt"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lio/netty/channel/ChannelInboundHandlerAdapter;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler;->o0:Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler;->〇OOo8〇0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method


# virtual methods
.method public channelRead(Lio/netty/channel/ChannelHandlerContext;Ljava/lang/Object;)V
    .locals 7

    .line 1
    const-string v0, "TcpCmdDispatchHandler"

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    const-string p1, "channelRead ctx == null"

    .line 6
    .line 7
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    instance-of v1, p2, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;

    .line 12
    .line 13
    if-eqz v1, :cond_2

    .line 14
    .line 15
    check-cast p2, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;

    .line 16
    .line 17
    invoke-virtual {p2}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->O8()[B

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-static {v1}, Lcom/intsig/developer/lib_message/util/NumberConvertUtilKt;->〇o〇([B)I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-virtual {p2}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->Oo08()[B

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-static {v2}, Lcom/intsig/developer/lib_message/util/NumberConvertUtilKt;->〇o〇([B)I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    invoke-virtual {p2}, Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;->〇80〇808〇O()[B

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    invoke-static {v3}, Lcom/intsig/developer/lib_message/util/NumberConvertUtilKt;->O8([B)J

    .line 38
    .line 39
    .line 40
    move-result-wide v3

    .line 41
    new-instance v5, Ljava/lang/StringBuilder;

    .line 42
    .line 43
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    .line 45
    .line 46
    const-string v6, "channelRead parseFrom code:"

    .line 47
    .line 48
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    const-string v6, ", cmdId:"

    .line 55
    .line 56
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    const-string v6, ", ts:"

    .line 63
    .line 64
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v3

    .line 74
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    if-eqz v2, :cond_1

    .line 78
    .line 79
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler;->〇OOo8〇0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;

    .line 80
    .line 81
    const/4 p2, 0x0

    .line 82
    invoke-interface {p1, p2}, Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;->〇080([B)V

    .line 83
    .line 84
    .line 85
    iget-object p1, p0, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler;->o0:Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;

    .line 86
    .line 87
    const/4 p2, 0x1

    .line 88
    invoke-interface {p1, p2}, Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;->〇O8o08O(Z)V

    .line 89
    .line 90
    .line 91
    const-string p1, "CMD_HAND_SHAKE_ACK reConnect"

    .line 92
    .line 93
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    return-void

    .line 97
    :cond_1
    sget-object v0, Lcom/intsig/developer/lib_message/mode/TcpCmd;->Companion:Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;

    .line 98
    .line 99
    invoke-virtual {v0, v1}, Lcom/intsig/developer/lib_message/mode/TcpCmd$Companion;->O8(I)Lcom/intsig/developer/lib_message/mode/TcpCmd;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler;->o0:Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;

    .line 104
    .line 105
    iget-object v2, p0, Lcom/intsig/developer/lib_message/netty/handler/TcpCmdDispatchHandler;->〇OOo8〇0:Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;

    .line 106
    .line 107
    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/intsig/developer/lib_message/mode/TcpCmd;->responseTcp(Lcom/intsig/developer/lib_message/intferface/IMSClientInterface;Lcom/intsig/developer/lib_message/intferface/SecurityEncryptionInterface;Lio/netty/channel/ChannelHandlerContext;Lcom/intsig/developer/lib_message/mode/MessageProtocolMode;)V

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_2
    invoke-super {p0, p1, p2}, Lio/netty/channel/ChannelInboundHandlerAdapter;->channelRead(Lio/netty/channel/ChannelHandlerContext;Ljava/lang/Object;)V

    .line 112
    .line 113
    .line 114
    :goto_0
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
.end method

.class public final Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SocketConnectionCmd.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite<",
        "Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;",
        "Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse$Builder;",
        ">;",
        "Lcom/google/protobuf/MessageLiteOrBuilder;"
    }
.end annotation


# static fields
.field public static final CODE_FIELD_NUMBER:I = 0x1

.field private static final DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

.field public static final MSGS_FIELD_NUMBER:I = 0x4

.field public static final MSG_FIELD_NUMBER:I = 0x2

.field private static volatile PARSER:Lcom/google/protobuf/Parser; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser<",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final SYNCED_FIELD_NUMBER:I = 0x3


# instance fields
.field private code_:I

.field private msg_:Ljava/lang/String;

.field private msgs_:Lcom/google/protobuf/Internal$ProtobufList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$ProtobufList<",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;",
            ">;"
        }
    .end annotation
.end field

.field private synced_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 7
    .line 8
    const-class v1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 9
    .line 10
    invoke-static {v1, v0}, Lcom/google/protobuf/GeneratedMessageLite;->registerDefaultInstance(Ljava/lang/Class;Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, ""

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msg_:Ljava/lang/String;

    .line 7
    .line 8
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->emptyProtobufList()Lcom/google/protobuf/Internal$ProtobufList;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic access$10000(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->clearSynced()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static synthetic access$10100(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;ILcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->setMsgs(ILcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static synthetic access$10200(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;Lcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->addMsgs(Lcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static synthetic access$10300(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;ILcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->addMsgs(ILcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method static synthetic access$10400(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;Ljava/lang/Iterable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->addAllMsgs(Ljava/lang/Iterable;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static synthetic access$10500(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->clearMsgs()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static synthetic access$10600(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->removeMsgs(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static synthetic access$9300()Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic access$9400(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->setCode(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static synthetic access$9500(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->clearCode()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static synthetic access$9600(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->setMsg(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static synthetic access$9700(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->clearMsg()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method static synthetic access$9800(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;Lcom/google/protobuf/ByteString;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->setMsgBytes(Lcom/google/protobuf/ByteString;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method static synthetic access$9900(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->setSynced(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private addAllMsgs(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->ensureMsgsIsMutable()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 5
    .line 6
    invoke-static {p1, v0}, Lcom/google/protobuf/AbstractMessageLite;->addAll(Ljava/lang/Iterable;Ljava/util/List;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private addMsgs(ILcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;)V
    .locals 1

    .line 4
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 5
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->ensureMsgsIsMutable()V

    .line 6
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-void
.end method

.method private addMsgs(Lcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->ensureMsgsIsMutable()V

    .line 3
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private clearCode()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->code_:I

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private clearMsg()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->getDefaultInstance()Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->getMsg()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msg_:Ljava/lang/String;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private clearMsgs()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->emptyProtobufList()Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private clearSynced()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->synced_:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private ensureMsgsIsMutable()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/google/protobuf/Internal$ProtobufList;->isModifiable()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 10
    .line 11
    invoke-static {v0}, Lcom/google/protobuf/GeneratedMessageLite;->mutableCopy(Lcom/google/protobuf/Internal$ProtobufList;)Lcom/google/protobuf/Internal$ProtobufList;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static getDefaultInstance()Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static newBuilder()Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse$Builder;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite;->createBuilder()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse$Builder;

    return-object v0
.end method

.method public static newBuilder(Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse$Builder;
    .locals 1

    .line 2
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-virtual {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->createBuilder(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse$Builder;

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseDelimitedFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/io/InputStream;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseDelimitedFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 3
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 4
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/io/InputStream;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parseFrom(Ljava/nio/ByteBuffer;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parseFrom(Ljava/nio/ByteBuffer;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 2
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;Ljava/nio/ByteBuffer;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parseFrom([B)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 5
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;[B)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .line 6
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    invoke-static {v0, p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;->parseFrom(Lcom/google/protobuf/GeneratedMessageLite;[BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    return-object p0
.end method

.method public static parser()Lcom/google/protobuf/Parser;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser<",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite;->getParserForType()Lcom/google/protobuf/Parser;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private removeMsgs(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->ensureMsgsIsMutable()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private setCode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->code_:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private setMsg(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msg_:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private setMsgBytes(Lcom/google/protobuf/ByteString;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/google/protobuf/AbstractMessageLite;->checkByteStringIsUtf8(Lcom/google/protobuf/ByteString;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    iput-object p1, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msg_:Ljava/lang/String;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private setMsgs(ILcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;)V
    .locals 1

    .line 1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->ensureMsgsIsMutable()V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 8
    .line 9
    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
.end method

.method private setSynced(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->synced_:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method


# virtual methods
.method protected final dynamicMethod(Lcom/google/protobuf/GeneratedMessageLite$MethodToInvoke;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 1
    sget-object p2, Lcom/intsig/developer/lib_message/SocketConnectionCmd$1;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    aget p1, p2, p1

    .line 8
    .line 9
    const/4 p2, 0x1

    .line 10
    const/4 p3, 0x0

    .line 11
    packed-switch p1, :pswitch_data_0

    .line 12
    .line 13
    .line 14
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    .line 15
    .line 16
    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    .line 17
    .line 18
    .line 19
    throw p1

    .line 20
    :pswitch_0
    return-object p3

    .line 21
    :pswitch_1
    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    return-object p1

    .line 26
    :pswitch_2
    sget-object p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->PARSER:Lcom/google/protobuf/Parser;

    .line 27
    .line 28
    if-nez p1, :cond_1

    .line 29
    .line 30
    const-class p2, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 31
    .line 32
    monitor-enter p2

    .line 33
    :try_start_0
    sget-object p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->PARSER:Lcom/google/protobuf/Parser;

    .line 34
    .line 35
    if-nez p1, :cond_0

    .line 36
    .line 37
    new-instance p1, Lcom/google/protobuf/GeneratedMessageLite$DefaultInstanceBasedParser;

    .line 38
    .line 39
    sget-object p3, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 40
    .line 41
    invoke-direct {p1, p3}, Lcom/google/protobuf/GeneratedMessageLite$DefaultInstanceBasedParser;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 42
    .line 43
    .line 44
    sput-object p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->PARSER:Lcom/google/protobuf/Parser;

    .line 45
    .line 46
    :cond_0
    monitor-exit p2

    .line 47
    goto :goto_0

    .line 48
    :catchall_0
    move-exception p1

    .line 49
    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    throw p1

    .line 51
    :cond_1
    :goto_0
    return-object p1

    .line 52
    :pswitch_3
    sget-object p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 53
    .line 54
    return-object p1

    .line 55
    :pswitch_4
    const/4 p1, 0x5

    .line 56
    new-array p1, p1, [Ljava/lang/Object;

    .line 57
    .line 58
    const/4 p3, 0x0

    .line 59
    const-string v0, "code_"

    .line 60
    .line 61
    aput-object v0, p1, p3

    .line 62
    .line 63
    const-string p3, "msg_"

    .line 64
    .line 65
    aput-object p3, p1, p2

    .line 66
    .line 67
    const/4 p2, 0x2

    .line 68
    const-string p3, "synced_"

    .line 69
    .line 70
    aput-object p3, p1, p2

    .line 71
    .line 72
    const/4 p2, 0x3

    .line 73
    const-string p3, "msgs_"

    .line 74
    .line 75
    aput-object p3, p1, p2

    .line 76
    .line 77
    const/4 p2, 0x4

    .line 78
    const-class p3, Lcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;

    .line 79
    .line 80
    aput-object p3, p1, p2

    .line 81
    .line 82
    const-string p2, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0001\u0000\u0001\u0004\u0002\u0208\u0003\u0007\u0004\u001b"

    .line 83
    .line 84
    sget-object p3, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->DEFAULT_INSTANCE:Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 85
    .line 86
    invoke-static {p3, p2, p1}, Lcom/google/protobuf/GeneratedMessageLite;->newMessageInfo(Lcom/google/protobuf/MessageLite;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    return-object p1

    .line 91
    :pswitch_5
    new-instance p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse$Builder;

    .line 92
    .line 93
    invoke-direct {p1, p3}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse$Builder;-><init>(Lcom/intsig/developer/lib_message/SocketConnectionCmd$1;)V

    .line 94
    .line 95
    .line 96
    return-object p1

    .line 97
    :pswitch_6
    new-instance p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;

    .line 98
    .line 99
    invoke-direct {p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;-><init>()V

    .line 100
    .line 101
    .line 102
    return-object p1

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
.end method

.method public getCode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->code_:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMsg()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msg_:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMsgBytes()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msg_:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMsgs(I)Lcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getMsgsCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMsgsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$Msg;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMsgsOrBuilder(I)Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgOrBuilder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgOrBuilder;

    .line 8
    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method public getMsgsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgOrBuilder;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->msgs_:Lcom/google/protobuf/Internal$ProtobufList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSynced()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/developer/lib_message/SocketConnectionCmd$FetchMsgsResponse;->synced_:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

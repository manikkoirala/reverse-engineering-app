.class public interface abstract Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;
.super Ljava/lang/Object;


# static fields
.field public static final O8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final O8ooOoo〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OO0o〇〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OO0o〇〇〇〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final Oo08:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OoO8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final Oooo8o0〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final O〇8O8〇008:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final o800o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final oO80:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final oo88o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final o〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final o〇O8〇〇o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇080:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇0〇O0088o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇80〇808〇O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇8o8o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O888o0o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O8o08O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇o00〇〇Oo:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇oo〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇808〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇888:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇8O0〇8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    new-instance v0, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 2
    .line 3
    const-string v1, "1.3.36.3"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇080:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 9
    .line 10
    const-string v1, "2.1"

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇o00〇〇Oo:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 17
    .line 18
    const-string v1, "2.2"

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 25
    .line 26
    const-string v1, "2.3"

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->O8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 33
    .line 34
    const-string v1, "3.1"

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->Oo08:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 41
    .line 42
    const-string v2, "2"

    .line 43
    .line 44
    invoke-virtual {v1, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    sput-object v3, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->o〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 49
    .line 50
    const-string v3, "3"

    .line 51
    .line 52
    invoke-virtual {v1, v3}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    sput-object v4, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇〇888:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 57
    .line 58
    const-string v4, "4"

    .line 59
    .line 60
    invoke-virtual {v1, v4}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->oO80:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 65
    .line 66
    const-string v1, "3.2"

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇80〇808〇O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 73
    .line 74
    const-string v5, "1"

    .line 75
    .line 76
    invoke-virtual {v1, v5}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 77
    .line 78
    .line 79
    move-result-object v6

    .line 80
    sput-object v6, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->OO0o〇〇〇〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 81
    .line 82
    invoke-virtual {v1, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 83
    .line 84
    .line 85
    move-result-object v1

    .line 86
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇8o8o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 87
    .line 88
    const-string v1, "3.2.8"

    .line 89
    .line 90
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    sput-object v0, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇O8o08O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 95
    .line 96
    invoke-virtual {v0, v5}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    sput-object v0, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->OO0o〇〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 101
    .line 102
    invoke-virtual {v0, v5}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    sput-object v0, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->Oooo8o0〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 107
    .line 108
    invoke-virtual {v0, v5}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇〇808〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 113
    .line 114
    invoke-virtual {v0, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 115
    .line 116
    .line 117
    move-result-object v1

    .line 118
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇O〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 119
    .line 120
    invoke-virtual {v0, v3}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇O00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 125
    .line 126
    invoke-virtual {v0, v4}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 127
    .line 128
    .line 129
    move-result-object v1

    .line 130
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇〇8O0〇8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 131
    .line 132
    const-string v1, "5"

    .line 133
    .line 134
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 135
    .line 136
    .line 137
    move-result-object v1

    .line 138
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇0〇O0088o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 139
    .line 140
    const-string v1, "6"

    .line 141
    .line 142
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->OoO8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 147
    .line 148
    const-string v1, "7"

    .line 149
    .line 150
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 151
    .line 152
    .line 153
    move-result-object v1

    .line 154
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->o800o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 155
    .line 156
    const-string v1, "8"

    .line 157
    .line 158
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 159
    .line 160
    .line 161
    move-result-object v1

    .line 162
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇O888o0o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 163
    .line 164
    const-string v1, "9"

    .line 165
    .line 166
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 167
    .line 168
    .line 169
    move-result-object v1

    .line 170
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->oo88o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 171
    .line 172
    const-string v1, "10"

    .line 173
    .line 174
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 175
    .line 176
    .line 177
    move-result-object v1

    .line 178
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇oo〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 179
    .line 180
    const-string v1, "11"

    .line 181
    .line 182
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 183
    .line 184
    .line 185
    move-result-object v1

    .line 186
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->o〇O8〇〇o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 187
    .line 188
    const-string v1, "12"

    .line 189
    .line 190
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 191
    .line 192
    .line 193
    move-result-object v1

    .line 194
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->〇00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 195
    .line 196
    const-string v1, "13"

    .line 197
    .line 198
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 199
    .line 200
    .line 201
    move-result-object v1

    .line 202
    sput-object v1, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->O〇8O8〇008:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 203
    .line 204
    const-string v1, "14"

    .line 205
    .line 206
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 207
    .line 208
    .line 209
    move-result-object v0

    .line 210
    sput-object v0, Lorg/bouncycastle/asn1/teletrust/TeleTrusTObjectIdentifiers;->O8ooOoo〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 211
    .line 212
    return-void
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

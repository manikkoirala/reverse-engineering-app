.class final Lorg/bouncycastle/asn1/x9/X962NamedCurves$12;
.super Lorg/bouncycastle/asn1/x9/X9ECParametersHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/bouncycastle/asn1/x9/X962NamedCurves;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/bouncycastle/asn1/x9/X9ECParametersHolder;-><init>()V

    return-void
.end method


# virtual methods
.method protected 〇080()Lorg/bouncycastle/asn1/x9/X9ECParameters;
    .locals 10

    .line 1
    new-instance v7, Ljava/math/BigInteger;

    .line 2
    .line 3
    const-string v0, "40000000000000000000000004A20E90C39067C893BBB9A5"

    .line 4
    .line 5
    const/16 v1, 0x10

    .line 6
    .line 7
    invoke-direct {v7, v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 8
    .line 9
    .line 10
    const-wide/16 v2, 0x2

    .line 11
    .line 12
    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    .line 13
    .line 14
    .line 15
    move-result-object v8

    .line 16
    new-instance v9, Lorg/bouncycastle/math/ec/ECCurve$F2m;

    .line 17
    .line 18
    const/16 v2, 0xbf

    .line 19
    .line 20
    const/16 v3, 0x9

    .line 21
    .line 22
    new-instance v4, Ljava/math/BigInteger;

    .line 23
    .line 24
    const-string v0, "2866537B676752636A68F56554E12640276B649EF7526267"

    .line 25
    .line 26
    invoke-direct {v4, v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    new-instance v5, Ljava/math/BigInteger;

    .line 30
    .line 31
    const-string v0, "2E45EF571F00786F67B0081B9495A3D95462F5DE0AA185EC"

    .line 32
    .line 33
    invoke-direct {v5, v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 34
    .line 35
    .line 36
    move-object v0, v9

    .line 37
    move v1, v2

    .line 38
    move v2, v3

    .line 39
    move-object v3, v4

    .line 40
    move-object v4, v5

    .line 41
    move-object v5, v7

    .line 42
    move-object v6, v8

    .line 43
    invoke-direct/range {v0 .. v6}, Lorg/bouncycastle/math/ec/ECCurve$F2m;-><init>(IILjava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 44
    .line 45
    .line 46
    new-instance v6, Lorg/bouncycastle/asn1/x9/X9ECParameters;

    .line 47
    .line 48
    new-instance v2, Lorg/bouncycastle/asn1/x9/X9ECPoint;

    .line 49
    .line 50
    const-string v0, "0236B3DAF8A23206F9C4F299D7B21A9C369137F2C84AE1AA0D"

    .line 51
    .line 52
    invoke-static {v0}, Lorg/bouncycastle/util/encoders/Hex;->〇080(Ljava/lang/String;)[B

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-direct {v2, v9, v0}, Lorg/bouncycastle/asn1/x9/X9ECPoint;-><init>(Lorg/bouncycastle/math/ec/ECCurve;[B)V

    .line 57
    .line 58
    .line 59
    const-string v0, "4E13CA542744D696E67687561517552F279A8C84"

    .line 60
    .line 61
    invoke-static {v0}, Lorg/bouncycastle/util/encoders/Hex;->〇080(Ljava/lang/String;)[B

    .line 62
    .line 63
    .line 64
    move-result-object v5

    .line 65
    move-object v0, v6

    .line 66
    move-object v1, v9

    .line 67
    move-object v3, v7

    .line 68
    move-object v4, v8

    .line 69
    invoke-direct/range {v0 .. v5}, Lorg/bouncycastle/asn1/x9/X9ECParameters;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/asn1/x9/X9ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    .line 70
    .line 71
    .line 72
    return-object v6
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

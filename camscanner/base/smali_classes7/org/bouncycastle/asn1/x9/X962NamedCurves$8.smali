.class final Lorg/bouncycastle/asn1/x9/X962NamedCurves$8;
.super Lorg/bouncycastle/asn1/x9/X9ECParametersHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/bouncycastle/asn1/x9/X962NamedCurves;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/bouncycastle/asn1/x9/X9ECParametersHolder;-><init>()V

    return-void
.end method


# virtual methods
.method protected 〇080()Lorg/bouncycastle/asn1/x9/X9ECParameters;
    .locals 12

    .line 1
    new-instance v9, Ljava/math/BigInteger;

    .line 2
    .line 3
    const-string v0, "0400000000000000000001E60FC8821CC74DAEAFC1"

    .line 4
    .line 5
    const/16 v1, 0x10

    .line 6
    .line 7
    invoke-direct {v9, v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 8
    .line 9
    .line 10
    const-wide/16 v2, 0x2

    .line 11
    .line 12
    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    .line 13
    .line 14
    .line 15
    move-result-object v10

    .line 16
    new-instance v11, Lorg/bouncycastle/math/ec/ECCurve$F2m;

    .line 17
    .line 18
    const/16 v2, 0xa3

    .line 19
    .line 20
    const/4 v3, 0x1

    .line 21
    const/4 v4, 0x2

    .line 22
    const/16 v5, 0x8

    .line 23
    .line 24
    new-instance v6, Ljava/math/BigInteger;

    .line 25
    .line 26
    const-string v0, "072546B5435234A422E0789675F432C89435DE5242"

    .line 27
    .line 28
    invoke-direct {v6, v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 29
    .line 30
    .line 31
    new-instance v7, Ljava/math/BigInteger;

    .line 32
    .line 33
    const-string v0, "00C9517D06D5240D3CFF38C74B20B6CD4D6F9DD4D9"

    .line 34
    .line 35
    invoke-direct {v7, v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 36
    .line 37
    .line 38
    move-object v0, v11

    .line 39
    move v1, v2

    .line 40
    move v2, v3

    .line 41
    move v3, v4

    .line 42
    move v4, v5

    .line 43
    move-object v5, v6

    .line 44
    move-object v6, v7

    .line 45
    move-object v7, v9

    .line 46
    move-object v8, v10

    .line 47
    invoke-direct/range {v0 .. v8}, Lorg/bouncycastle/math/ec/ECCurve$F2m;-><init>(IIIILjava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 48
    .line 49
    .line 50
    new-instance v6, Lorg/bouncycastle/asn1/x9/X9ECParameters;

    .line 51
    .line 52
    new-instance v2, Lorg/bouncycastle/asn1/x9/X9ECPoint;

    .line 53
    .line 54
    const-string v0, "0307AF69989546103D79329FCC3D74880F33BBE803CB"

    .line 55
    .line 56
    invoke-static {v0}, Lorg/bouncycastle/util/encoders/Hex;->〇080(Ljava/lang/String;)[B

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-direct {v2, v11, v0}, Lorg/bouncycastle/asn1/x9/X9ECPoint;-><init>(Lorg/bouncycastle/math/ec/ECCurve;[B)V

    .line 61
    .line 62
    .line 63
    const-string v0, "D2C0FB15760860DEF1EEF4D696E6768756151754"

    .line 64
    .line 65
    invoke-static {v0}, Lorg/bouncycastle/util/encoders/Hex;->〇080(Ljava/lang/String;)[B

    .line 66
    .line 67
    .line 68
    move-result-object v5

    .line 69
    move-object v0, v6

    .line 70
    move-object v1, v11

    .line 71
    move-object v3, v9

    .line 72
    move-object v4, v10

    .line 73
    invoke-direct/range {v0 .. v5}, Lorg/bouncycastle/asn1/x9/X9ECParameters;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/asn1/x9/X9ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    .line 74
    .line 75
    .line 76
    return-object v6
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

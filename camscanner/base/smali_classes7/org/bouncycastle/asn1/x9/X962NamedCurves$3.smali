.class final Lorg/bouncycastle/asn1/x9/X962NamedCurves$3;
.super Lorg/bouncycastle/asn1/x9/X9ECParametersHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/bouncycastle/asn1/x9/X962NamedCurves;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/bouncycastle/asn1/x9/X9ECParametersHolder;-><init>()V

    return-void
.end method


# virtual methods
.method protected 〇080()Lorg/bouncycastle/asn1/x9/X9ECParameters;
    .locals 10

    .line 1
    new-instance v6, Ljava/math/BigInteger;

    .line 2
    .line 3
    const-string v0, "ffffffffffffffffffffffff7a62d031c83f4294f640ec13"

    .line 4
    .line 5
    const/16 v1, 0x10

    .line 6
    .line 7
    invoke-direct {v6, v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 8
    .line 9
    .line 10
    const-wide/16 v2, 0x1

    .line 11
    .line 12
    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    .line 13
    .line 14
    .line 15
    move-result-object v7

    .line 16
    new-instance v8, Lorg/bouncycastle/math/ec/ECCurve$Fp;

    .line 17
    .line 18
    new-instance v2, Ljava/math/BigInteger;

    .line 19
    .line 20
    const-string v0, "6277101735386680763835789423207666416083908700390324961279"

    .line 21
    .line 22
    invoke-direct {v2, v0}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    new-instance v3, Ljava/math/BigInteger;

    .line 26
    .line 27
    const-string v0, "fffffffffffffffffffffffffffffffefffffffffffffffc"

    .line 28
    .line 29
    invoke-direct {v3, v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 30
    .line 31
    .line 32
    new-instance v4, Ljava/math/BigInteger;

    .line 33
    .line 34
    const-string v0, "22123dc2395a05caa7423daeccc94760a7d462256bd56916"

    .line 35
    .line 36
    invoke-direct {v4, v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 37
    .line 38
    .line 39
    move-object v0, v8

    .line 40
    move-object v1, v2

    .line 41
    move-object v2, v3

    .line 42
    move-object v3, v4

    .line 43
    move-object v4, v6

    .line 44
    move-object v5, v7

    .line 45
    invoke-direct/range {v0 .. v5}, Lorg/bouncycastle/math/ec/ECCurve$Fp;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 46
    .line 47
    .line 48
    new-instance v9, Lorg/bouncycastle/asn1/x9/X9ECParameters;

    .line 49
    .line 50
    new-instance v2, Lorg/bouncycastle/asn1/x9/X9ECPoint;

    .line 51
    .line 52
    const-string v0, "027d29778100c65a1da1783716588dce2b8b4aee8e228f1896"

    .line 53
    .line 54
    invoke-static {v0}, Lorg/bouncycastle/util/encoders/Hex;->〇080(Ljava/lang/String;)[B

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-direct {v2, v8, v0}, Lorg/bouncycastle/asn1/x9/X9ECPoint;-><init>(Lorg/bouncycastle/math/ec/ECCurve;[B)V

    .line 59
    .line 60
    .line 61
    const-string v0, "c469684435deb378c4b65ca9591e2a5763059a2e"

    .line 62
    .line 63
    invoke-static {v0}, Lorg/bouncycastle/util/encoders/Hex;->〇080(Ljava/lang/String;)[B

    .line 64
    .line 65
    .line 66
    move-result-object v5

    .line 67
    move-object v0, v9

    .line 68
    move-object v1, v8

    .line 69
    move-object v3, v6

    .line 70
    move-object v4, v7

    .line 71
    invoke-direct/range {v0 .. v5}, Lorg/bouncycastle/asn1/x9/X9ECParameters;-><init>(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/asn1/x9/X9ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V

    .line 72
    .line 73
    .line 74
    return-object v9
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

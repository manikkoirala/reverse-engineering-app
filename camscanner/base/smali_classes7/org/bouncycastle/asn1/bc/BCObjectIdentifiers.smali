.class public interface abstract Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;
.super Ljava/lang/Object;


# static fields
.field public static final O8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OO0o〇〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OO0o〇〇〇〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final Oo08:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OoO8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final Oooo8o0〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final o800o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final oO80:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final oo88o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final o〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇080:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇0〇O0088o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇80〇808〇O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇8o8o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O888o0o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O8o08O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇o00〇〇Oo:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇oo〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇808〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇888:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇8O0〇8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 1
    new-instance v0, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 2
    .line 3
    const-string v1, "1.3.6.1.4.1.22554"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇080:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 9
    .line 10
    const-string v1, "1"

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    sput-object v2, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇o00〇〇Oo:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 17
    .line 18
    invoke-virtual {v2, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    sput-object v3, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 23
    .line 24
    const-string v4, "2.1"

    .line 25
    .line 26
    invoke-virtual {v2, v4}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    sput-object v4, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->O8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 31
    .line 32
    const-string v5, "2.2"

    .line 33
    .line 34
    invoke-virtual {v2, v5}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 35
    .line 36
    .line 37
    move-result-object v5

    .line 38
    sput-object v5, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->Oo08:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 39
    .line 40
    const-string v5, "2.3"

    .line 41
    .line 42
    invoke-virtual {v2, v5}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 43
    .line 44
    .line 45
    move-result-object v5

    .line 46
    sput-object v5, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->o〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 47
    .line 48
    const-string v5, "2.4"

    .line 49
    .line 50
    invoke-virtual {v2, v5}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    sput-object v2, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇〇888:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 55
    .line 56
    invoke-virtual {v3, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    sput-object v2, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->oO80:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 61
    .line 62
    const-string v2, "2"

    .line 63
    .line 64
    invoke-virtual {v3, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 65
    .line 66
    .line 67
    move-result-object v3

    .line 68
    sput-object v3, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇80〇808〇O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 69
    .line 70
    invoke-virtual {v4, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    sput-object v5, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->OO0o〇〇〇〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 75
    .line 76
    invoke-virtual {v4, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 77
    .line 78
    .line 79
    move-result-object v4

    .line 80
    sput-object v4, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇8o8o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 81
    .line 82
    const-string v5, "1.2"

    .line 83
    .line 84
    invoke-virtual {v3, v5}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 85
    .line 86
    .line 87
    move-result-object v6

    .line 88
    sput-object v6, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇O8o08O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 89
    .line 90
    const-string v6, "1.22"

    .line 91
    .line 92
    invoke-virtual {v3, v6}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 93
    .line 94
    .line 95
    move-result-object v7

    .line 96
    sput-object v7, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->OO0o〇〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 97
    .line 98
    const-string v7, "1.42"

    .line 99
    .line 100
    invoke-virtual {v3, v7}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 101
    .line 102
    .line 103
    move-result-object v3

    .line 104
    sput-object v3, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->Oooo8o0〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 105
    .line 106
    invoke-virtual {v4, v5}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 107
    .line 108
    .line 109
    move-result-object v3

    .line 110
    sput-object v3, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇〇808〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 111
    .line 112
    invoke-virtual {v4, v6}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 113
    .line 114
    .line 115
    move-result-object v3

    .line 116
    sput-object v3, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇O〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 117
    .line 118
    invoke-virtual {v4, v7}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 119
    .line 120
    .line 121
    move-result-object v3

    .line 122
    sput-object v3, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇O00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 123
    .line 124
    invoke-virtual {v0, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 125
    .line 126
    .line 127
    move-result-object v3

    .line 128
    sput-object v3, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇〇8O0〇8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 129
    .line 130
    invoke-virtual {v3, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 131
    .line 132
    .line 133
    move-result-object v3

    .line 134
    sput-object v3, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇0〇O0088o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 135
    .line 136
    invoke-virtual {v3, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 137
    .line 138
    .line 139
    move-result-object v4

    .line 140
    sput-object v4, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->OoO8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 141
    .line 142
    invoke-virtual {v3, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 143
    .line 144
    .line 145
    move-result-object v2

    .line 146
    sput-object v2, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->o800o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 147
    .line 148
    const-string v2, "3"

    .line 149
    .line 150
    invoke-virtual {v3, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 151
    .line 152
    .line 153
    move-result-object v3

    .line 154
    sput-object v3, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇O888o0o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 155
    .line 156
    invoke-virtual {v0, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 157
    .line 158
    .line 159
    move-result-object v0

    .line 160
    sput-object v0, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->oo88o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 161
    .line 162
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 163
    .line 164
    .line 165
    move-result-object v0

    .line 166
    sput-object v0, Lorg/bouncycastle/asn1/bc/BCObjectIdentifiers;->〇oo〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 167
    .line 168
    return-void
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.class public interface abstract Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;
.super Ljava/lang/Object;


# static fields
.field public static final O8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final O8ooOoo〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OO0o〇〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OO0o〇〇〇〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final Oo08:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final OoO8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final Oooo8o0〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final O〇8O8〇008:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final o800o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final oO80:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final oo88o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final o〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final o〇O8〇〇o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇080:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇0〇O0088o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇80〇808〇O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇8o8o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O888o0o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O8o08O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇O〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇o00〇〇Oo:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇oo〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇808〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇888:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

.field public static final 〇〇8O0〇8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 2
    .line 3
    const-string v1, "1.2.643.2.2"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇080:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 9
    .line 10
    const-string v1, "9"

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇o00〇〇Oo:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 17
    .line 18
    const-string v1, "10"

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 25
    .line 26
    const-string v1, "21"

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->O8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 33
    .line 34
    const-string v1, "31.0"

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->Oo08:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 41
    .line 42
    const-string v1, "31.1"

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->o〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 49
    .line 50
    const-string v1, "31.2"

    .line 51
    .line 52
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇〇888:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 57
    .line 58
    const-string v1, "31.3"

    .line 59
    .line 60
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->oO80:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 65
    .line 66
    const-string v1, "31.4"

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇80〇808〇O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 73
    .line 74
    const-string v1, "20"

    .line 75
    .line 76
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->OO0o〇〇〇〇0:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 81
    .line 82
    const-string v1, "19"

    .line 83
    .line 84
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇8o8o〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 89
    .line 90
    const-string v1, "4"

    .line 91
    .line 92
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇O8o08O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 97
    .line 98
    const-string v1, "3"

    .line 99
    .line 100
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->OO0o〇〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 105
    .line 106
    const-string v1, "30.1"

    .line 107
    .line 108
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->Oooo8o0〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 113
    .line 114
    const-string v1, "32.2"

    .line 115
    .line 116
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇〇808〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 121
    .line 122
    const-string v1, "32.3"

    .line 123
    .line 124
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 125
    .line 126
    .line 127
    move-result-object v1

    .line 128
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇O〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 129
    .line 130
    const-string v1, "32.4"

    .line 131
    .line 132
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇O00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 137
    .line 138
    const-string v1, "32.5"

    .line 139
    .line 140
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 141
    .line 142
    .line 143
    move-result-object v1

    .line 144
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇〇8O0〇8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 145
    .line 146
    const-string v1, "33.1"

    .line 147
    .line 148
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇0〇O0088o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 153
    .line 154
    const-string v1, "33.2"

    .line 155
    .line 156
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 157
    .line 158
    .line 159
    move-result-object v1

    .line 160
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->OoO8:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 161
    .line 162
    const-string v1, "33.3"

    .line 163
    .line 164
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 165
    .line 166
    .line 167
    move-result-object v1

    .line 168
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->o800o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 169
    .line 170
    const-string v1, "35.1"

    .line 171
    .line 172
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 173
    .line 174
    .line 175
    move-result-object v1

    .line 176
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇O888o0o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 177
    .line 178
    const-string v1, "35.2"

    .line 179
    .line 180
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 181
    .line 182
    .line 183
    move-result-object v1

    .line 184
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->oo88o8O:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 185
    .line 186
    const-string v1, "35.3"

    .line 187
    .line 188
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 189
    .line 190
    .line 191
    move-result-object v1

    .line 192
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇oo〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 193
    .line 194
    const-string v1, "36.0"

    .line 195
    .line 196
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 197
    .line 198
    .line 199
    move-result-object v2

    .line 200
    sput-object v2, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->o〇O8〇〇o:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 201
    .line 202
    const-string v2, "36.1"

    .line 203
    .line 204
    invoke-virtual {v0, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 205
    .line 206
    .line 207
    move-result-object v3

    .line 208
    sput-object v3, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->〇00:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 209
    .line 210
    invoke-virtual {v0, v1}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 211
    .line 212
    .line 213
    move-result-object v1

    .line 214
    sput-object v1, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->O〇8O8〇008:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 215
    .line 216
    invoke-virtual {v0, v2}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->OO0o〇〇(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 217
    .line 218
    .line 219
    move-result-object v0

    .line 220
    sput-object v0, Lorg/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers;->O8ooOoo〇:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    .line 221
    .line 222
    return-void
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
.end method

.class public Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;
.super Ljava/security/SecureRandomSpi;
.source "SHA1PRNG_SecureRandomImpl.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/apache/harmony/security/provider/crypto/SHA1_DataImpl;


# static fields
.field private static final COUNTER_BASE:I = 0x0

.field private static final END_FLAGS:[I

.field private static final EXTRAFRAME_OFFSET:I = 0x5

.field private static final FRAME_LENGTH:I = 0x10

.field private static final FRAME_OFFSET:I = 0x15

.field private static final HASHBYTES_TO_USE:I = 0x14

.field private static final HASHCOPY_OFFSET:I = 0x0

.field private static final LEFT:[I

.field private static final MASK:[I

.field private static final MAX_BYTES:I = 0x30

.field private static final NEXT_BYTES:I = 0x2

.field private static final RIGHT1:[I

.field private static final RIGHT2:[I

.field private static final SET_SEED:I = 0x1

.field private static final UNDEFINED:I = 0x0

.field private static myRandom:Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl; = null

.field private static final serialVersionUID:J = 0x3f0091d1f89aebbL


# instance fields
.field private transient copies:[I

.field private transient counter:J

.field private transient nextBIndex:I

.field private transient nextBytes:[B

.field private transient seed:[I

.field private transient seedLength:J

.field private transient state:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/4 v0, 0x4

    .line 2
    new-array v1, v0, [I

    .line 3
    .line 4
    fill-array-data v1, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->END_FLAGS:[I

    .line 8
    .line 9
    new-array v1, v0, [I

    .line 10
    .line 11
    fill-array-data v1, :array_1

    .line 12
    .line 13
    .line 14
    sput-object v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->RIGHT1:[I

    .line 15
    .line 16
    new-array v1, v0, [I

    .line 17
    .line 18
    fill-array-data v1, :array_2

    .line 19
    .line 20
    .line 21
    sput-object v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->RIGHT2:[I

    .line 22
    .line 23
    new-array v1, v0, [I

    .line 24
    .line 25
    fill-array-data v1, :array_3

    .line 26
    .line 27
    .line 28
    sput-object v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->LEFT:[I

    .line 29
    .line 30
    new-array v0, v0, [I

    .line 31
    .line 32
    fill-array-data v0, :array_4

    .line 33
    .line 34
    .line 35
    sput-object v0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->MASK:[I

    .line 36
    .line 37
    return-void

    .line 38
    nop

    .line 39
    :array_0
    .array-data 4
        -0x80000000
        0x800000
        0x8000
        0x80
    .end array-data

    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    :array_1
    .array-data 4
        0x0
        0x28
        0x30
        0x38
    .end array-data

    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    :array_2
    .array-data 4
        0x0
        0x8
        0x10
        0x18
    .end array-data

    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    :array_3
    .array-data 4
        0x0
        0x18
        0x10
        0x8
    .end array-data

    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    :array_4
    .array-data 4
        -0x1
        0xffffff
        0xffff
        0xff
    .end array-data
.end method

.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/security/SecureRandomSpi;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x57

    .line 5
    .line 6
    new-array v0, v0, [I

    .line 7
    .line 8
    iput-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 9
    .line 10
    const/16 v1, 0x52

    .line 11
    .line 12
    const v2, 0x67452301

    .line 13
    .line 14
    .line 15
    aput v2, v0, v1

    .line 16
    .line 17
    const/16 v1, 0x53

    .line 18
    .line 19
    const v2, -0x10325477

    .line 20
    .line 21
    .line 22
    aput v2, v0, v1

    .line 23
    .line 24
    const/16 v1, 0x54

    .line 25
    .line 26
    const v2, -0x67452302

    .line 27
    .line 28
    .line 29
    aput v2, v0, v1

    .line 30
    .line 31
    const/16 v1, 0x55

    .line 32
    .line 33
    const v2, 0x10325476

    .line 34
    .line 35
    .line 36
    aput v2, v0, v1

    .line 37
    .line 38
    const/16 v1, 0x56

    .line 39
    .line 40
    const v2, -0x3c2d1e10

    .line 41
    .line 42
    .line 43
    aput v2, v0, v1

    .line 44
    .line 45
    const-wide/16 v0, 0x0

    .line 46
    .line 47
    iput-wide v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seedLength:J

    .line 48
    .line 49
    const/16 v2, 0x25

    .line 50
    .line 51
    new-array v2, v2, [I

    .line 52
    .line 53
    iput-object v2, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 54
    .line 55
    const/16 v2, 0x14

    .line 56
    .line 57
    new-array v3, v2, [B

    .line 58
    .line 59
    iput-object v3, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBytes:[B

    .line 60
    .line 61
    iput v2, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 62
    .line 63
    iput-wide v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->counter:J

    .line 64
    .line 65
    const/4 v0, 0x0

    .line 66
    iput v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->state:I

    .line 67
    .line 68
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 1
    const/16 v0, 0x57

    .line 2
    .line 3
    new-array v0, v0, [I

    .line 4
    .line 5
    iput-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 6
    .line 7
    const/16 v0, 0x25

    .line 8
    .line 9
    new-array v0, v0, [I

    .line 10
    .line 11
    iput-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 12
    .line 13
    const/16 v0, 0x14

    .line 14
    .line 15
    new-array v0, v0, [B

    .line 16
    .line 17
    iput-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBytes:[B

    .line 18
    .line 19
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    .line 20
    .line 21
    .line 22
    move-result-wide v0

    .line 23
    iput-wide v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seedLength:J

    .line 24
    .line 25
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    .line 26
    .line 27
    .line 28
    move-result-wide v0

    .line 29
    iput-wide v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->counter:J

    .line 30
    .line 31
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    iput v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->state:I

    .line 36
    .line 37
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 38
    .line 39
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    const/16 v2, 0x51

    .line 44
    .line 45
    aput v1, v0, v2

    .line 46
    .line 47
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 48
    .line 49
    aget v1, v0, v2

    .line 50
    .line 51
    add-int/lit8 v2, v1, 0x3

    .line 52
    .line 53
    const/4 v3, 0x2

    .line 54
    shr-int/2addr v2, v3

    .line 55
    iget v4, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->state:I

    .line 56
    .line 57
    const/4 v5, 0x5

    .line 58
    const/4 v6, 0x0

    .line 59
    if-eq v4, v3, :cond_1

    .line 60
    .line 61
    const/4 v0, 0x0

    .line 62
    :goto_0
    if-ge v0, v2, :cond_0

    .line 63
    .line 64
    iget-object v1, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 65
    .line 66
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 67
    .line 68
    .line 69
    move-result v3

    .line 70
    aput v3, v1, v0

    .line 71
    .line 72
    add-int/lit8 v0, v0, 0x1

    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_0
    :goto_1
    if-ge v6, v5, :cond_6

    .line 76
    .line 77
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 78
    .line 79
    add-int/lit8 v1, v6, 0x52

    .line 80
    .line 81
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 82
    .line 83
    .line 84
    move-result v2

    .line 85
    aput v2, v0, v1

    .line 86
    .line 87
    add-int/lit8 v6, v6, 0x1

    .line 88
    .line 89
    goto :goto_1

    .line 90
    :cond_1
    const/16 v3, 0x30

    .line 91
    .line 92
    const/16 v4, 0x10

    .line 93
    .line 94
    if-lt v1, v3, :cond_2

    .line 95
    .line 96
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    aput v1, v0, v4

    .line 101
    .line 102
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 103
    .line 104
    const/16 v1, 0x11

    .line 105
    .line 106
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 107
    .line 108
    .line 109
    move-result v3

    .line 110
    aput v3, v0, v1

    .line 111
    .line 112
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 113
    .line 114
    const/16 v1, 0x1e

    .line 115
    .line 116
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 117
    .line 118
    .line 119
    move-result v3

    .line 120
    aput v3, v0, v1

    .line 121
    .line 122
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 123
    .line 124
    const/16 v1, 0x1f

    .line 125
    .line 126
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 127
    .line 128
    .line 129
    move-result v3

    .line 130
    aput v3, v0, v1

    .line 131
    .line 132
    :cond_2
    const/4 v0, 0x0

    .line 133
    :goto_2
    if-ge v0, v4, :cond_3

    .line 134
    .line 135
    iget-object v1, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 136
    .line 137
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 138
    .line 139
    .line 140
    move-result v3

    .line 141
    aput v3, v1, v0

    .line 142
    .line 143
    add-int/lit8 v0, v0, 0x1

    .line 144
    .line 145
    goto :goto_2

    .line 146
    :cond_3
    const/4 v0, 0x0

    .line 147
    :goto_3
    if-ge v0, v2, :cond_4

    .line 148
    .line 149
    iget-object v1, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 150
    .line 151
    add-int/lit8 v3, v0, 0x15

    .line 152
    .line 153
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 154
    .line 155
    .line 156
    move-result v4

    .line 157
    aput v4, v1, v3

    .line 158
    .line 159
    add-int/lit8 v0, v0, 0x1

    .line 160
    .line 161
    goto :goto_3

    .line 162
    :cond_4
    const/4 v0, 0x0

    .line 163
    :goto_4
    if-ge v0, v5, :cond_5

    .line 164
    .line 165
    iget-object v1, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 166
    .line 167
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 168
    .line 169
    .line 170
    move-result v2

    .line 171
    aput v2, v1, v0

    .line 172
    .line 173
    add-int/lit8 v0, v0, 0x1

    .line 174
    .line 175
    goto :goto_4

    .line 176
    :cond_5
    :goto_5
    if-ge v6, v5, :cond_6

    .line 177
    .line 178
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 179
    .line 180
    add-int/lit8 v1, v6, 0x52

    .line 181
    .line 182
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 183
    .line 184
    .line 185
    move-result v2

    .line 186
    aput v2, v0, v1

    .line 187
    .line 188
    add-int/lit8 v6, v6, 0x1

    .line 189
    .line 190
    goto :goto_5

    .line 191
    :cond_6
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    .line 192
    .line 193
    .line 194
    move-result v0

    .line 195
    iput v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 196
    .line 197
    iget-object v1, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBytes:[B

    .line 198
    .line 199
    rsub-int/lit8 v2, v0, 0x14

    .line 200
    .line 201
    invoke-static {p1, v1, v0, v2}, Lorg/apache/harmony/security/provider/crypto/StreamsImpl;->readFully(Ljava/io/InputStream;[BII)V

    .line 202
    .line 203
    .line 204
    return-void
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method private updateSeed([B)V
    .locals 4

    .line 1
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 2
    .line 3
    array-length v1, p1

    .line 4
    add-int/lit8 v1, v1, -0x1

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-static {v0, p1, v2, v1}, Lorg/apache/harmony/security/provider/crypto/SHA1Impl;->updateHash([I[BII)V

    .line 8
    .line 9
    .line 10
    iget-wide v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seedLength:J

    .line 11
    .line 12
    array-length p1, p1

    .line 13
    int-to-long v2, p1

    .line 14
    add-long/2addr v0, v2

    .line 15
    iput-wide v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seedLength:J

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-wide v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seedLength:J

    .line 2
    .line 3
    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 4
    .line 5
    .line 6
    iget-wide v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->counter:J

    .line 7
    .line 8
    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 9
    .line 10
    .line 11
    iget v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->state:I

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 17
    .line 18
    const/16 v1, 0x51

    .line 19
    .line 20
    aget v0, v0, v1

    .line 21
    .line 22
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 26
    .line 27
    aget v1, v0, v1

    .line 28
    .line 29
    add-int/lit8 v2, v1, 0x3

    .line 30
    .line 31
    const/4 v3, 0x2

    .line 32
    shr-int/2addr v2, v3

    .line 33
    iget v4, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->state:I

    .line 34
    .line 35
    const/16 v5, 0x52

    .line 36
    .line 37
    const/4 v6, 0x5

    .line 38
    const/4 v7, 0x0

    .line 39
    if-eq v4, v3, :cond_0

    .line 40
    .line 41
    add-int/lit8 v1, v2, 0x5

    .line 42
    .line 43
    new-array v1, v1, [I

    .line 44
    .line 45
    invoke-static {v0, v7, v1, v7, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 49
    .line 50
    invoke-static {v0, v5, v1, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 51
    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_0
    const/16 v4, 0x30

    .line 55
    .line 56
    const/16 v8, 0x10

    .line 57
    .line 58
    if-ge v1, v4, :cond_1

    .line 59
    .line 60
    add-int/lit8 v1, v2, 0x1a

    .line 61
    .line 62
    new-array v1, v1, [I

    .line 63
    .line 64
    const/4 v3, 0x0

    .line 65
    goto :goto_0

    .line 66
    :cond_1
    add-int/lit8 v1, v2, 0x2a

    .line 67
    .line 68
    new-array v1, v1, [I

    .line 69
    .line 70
    aget v4, v0, v8

    .line 71
    .line 72
    aput v4, v1, v7

    .line 73
    .line 74
    const/16 v4, 0x11

    .line 75
    .line 76
    aget v4, v0, v4

    .line 77
    .line 78
    const/4 v9, 0x1

    .line 79
    aput v4, v1, v9

    .line 80
    .line 81
    const/16 v4, 0x1e

    .line 82
    .line 83
    aget v4, v0, v4

    .line 84
    .line 85
    aput v4, v1, v3

    .line 86
    .line 87
    const/16 v3, 0x1f

    .line 88
    .line 89
    aget v3, v0, v3

    .line 90
    .line 91
    const/4 v4, 0x3

    .line 92
    aput v3, v1, v4

    .line 93
    .line 94
    const/4 v3, 0x4

    .line 95
    :goto_0
    invoke-static {v0, v7, v1, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 96
    .line 97
    .line 98
    add-int/2addr v3, v8

    .line 99
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 100
    .line 101
    const/16 v4, 0x15

    .line 102
    .line 103
    invoke-static {v0, v4, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 104
    .line 105
    .line 106
    add-int/2addr v3, v2

    .line 107
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 108
    .line 109
    invoke-static {v0, v7, v1, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 110
    .line 111
    .line 112
    add-int/2addr v3, v6

    .line 113
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 114
    .line 115
    invoke-static {v0, v5, v1, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 116
    .line 117
    .line 118
    :goto_1
    array-length v0, v1

    .line 119
    if-ge v7, v0, :cond_2

    .line 120
    .line 121
    aget v0, v1, v7

    .line 122
    .line 123
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 124
    .line 125
    .line 126
    add-int/lit8 v7, v7, 0x1

    .line 127
    .line 128
    goto :goto_1

    .line 129
    :cond_2
    iget v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 130
    .line 131
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 132
    .line 133
    .line 134
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBytes:[B

    .line 135
    .line 136
    iget v1, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 137
    .line 138
    rsub-int/lit8 v2, v1, 0x14

    .line 139
    .line 140
    invoke-virtual {p1, v0, v1, v2}, Ljava/io/ObjectOutputStream;->write([BII)V

    .line 141
    .line 142
    .line 143
    return-void
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method


# virtual methods
.method protected declared-synchronized engineGenerateSeed(I)[B
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    if-ltz p1, :cond_2

    .line 3
    .line 4
    if-nez p1, :cond_0

    .line 5
    .line 6
    :try_start_0
    sget-object p1, Lorg/apache/harmony/security/provider/crypto/EmptyArrayimpl;->BYTE:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7
    .line 8
    monitor-exit p0

    .line 9
    return-object p1

    .line 10
    :cond_0
    :try_start_1
    sget-object v0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->myRandom:Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;

    .line 11
    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    new-instance v0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;

    .line 15
    .line 16
    invoke-direct {v0}, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;-><init>()V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->myRandom:Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;

    .line 20
    .line 21
    const/16 v1, 0x14

    .line 22
    .line 23
    invoke-static {v1}, Lorg/apache/harmony/security/provider/crypto/RandomBitsSupplier;->getRandomBits(I)[B

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->engineSetSeed([B)V

    .line 28
    .line 29
    .line 30
    :cond_1
    new-array p1, p1, [B

    .line 31
    .line 32
    sget-object v0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->myRandom:Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;

    .line 33
    .line 34
    invoke-virtual {v0, p1}, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->engineNextBytes([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 35
    .line 36
    .line 37
    monitor-exit p0

    .line 38
    return-object p1

    .line 39
    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/NegativeArraySizeException;

    .line 40
    .line 41
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-direct {v0, p1}, Ljava/lang/NegativeArraySizeException;-><init>(Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 49
    :catchall_0
    move-exception p1

    .line 50
    monitor-exit p0

    .line 51
    throw p1
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

.method protected declared-synchronized engineNextBytes([B)V
    .locals 19

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    monitor-enter p0

    .line 6
    if-eqz v0, :cond_10

    .line 7
    .line 8
    :try_start_0
    iget-object v2, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 9
    .line 10
    const/16 v3, 0x51

    .line 11
    .line 12
    aget v4, v2, v3

    .line 13
    .line 14
    const/4 v5, 0x2

    .line 15
    const/4 v6, 0x0

    .line 16
    if-nez v4, :cond_0

    .line 17
    .line 18
    const/4 v4, 0x0

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    add-int/lit8 v4, v4, 0x7

    .line 21
    .line 22
    shr-int/2addr v4, v5

    .line 23
    :goto_0
    iget v7, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->state:I

    .line 24
    .line 25
    const/16 v8, 0x20

    .line 26
    .line 27
    const/16 v9, 0x30

    .line 28
    .line 29
    const-wide/16 v10, -0x1

    .line 30
    .line 31
    const/4 v12, 0x3

    .line 32
    const/4 v13, 0x5

    .line 33
    const/16 v14, 0x14

    .line 34
    .line 35
    if-nez v7, :cond_1

    .line 36
    .line 37
    invoke-static {v14}, Lorg/apache/harmony/security/provider/crypto/RandomBitsSupplier;->getRandomBits(I)[B

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-direct {v1, v2}, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->updateSeed([B)V

    .line 42
    .line 43
    .line 44
    iput v14, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 45
    .line 46
    goto :goto_3

    .line 47
    :cond_1
    const/4 v15, 0x1

    .line 48
    if-ne v7, v15, :cond_4

    .line 49
    .line 50
    iget-object v7, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 51
    .line 52
    const/16 v15, 0x52

    .line 53
    .line 54
    invoke-static {v2, v15, v7, v6, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    .line 56
    .line 57
    add-int/lit8 v2, v4, 0x3

    .line 58
    .line 59
    :goto_1
    const/16 v7, 0x12

    .line 60
    .line 61
    if-ge v2, v7, :cond_2

    .line 62
    .line 63
    iget-object v7, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 64
    .line 65
    aput v6, v7, v2

    .line 66
    .line 67
    add-int/lit8 v2, v2, 0x1

    .line 68
    .line 69
    goto :goto_1

    .line 70
    :cond_2
    iget-wide v6, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seedLength:J

    .line 71
    .line 72
    shl-long/2addr v6, v12

    .line 73
    const-wide/16 v15, 0x40

    .line 74
    .line 75
    add-long/2addr v6, v15

    .line 76
    iget-object v15, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 77
    .line 78
    aget v2, v15, v3

    .line 79
    .line 80
    if-ge v2, v9, :cond_3

    .line 81
    .line 82
    ushr-long v12, v6, v8

    .line 83
    .line 84
    long-to-int v2, v12

    .line 85
    const/16 v12, 0xe

    .line 86
    .line 87
    aput v2, v15, v12

    .line 88
    .line 89
    and-long/2addr v6, v10

    .line 90
    long-to-int v2, v6

    .line 91
    const/16 v6, 0xf

    .line 92
    .line 93
    aput v2, v15, v6

    .line 94
    .line 95
    goto :goto_2

    .line 96
    :cond_3
    iget-object v2, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 97
    .line 98
    ushr-long v12, v6, v8

    .line 99
    .line 100
    long-to-int v13, v12

    .line 101
    const/16 v12, 0x13

    .line 102
    .line 103
    aput v13, v2, v12

    .line 104
    .line 105
    and-long/2addr v6, v10

    .line 106
    long-to-int v7, v6

    .line 107
    aput v7, v2, v14

    .line 108
    .line 109
    :goto_2
    iput v14, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 110
    .line 111
    :cond_4
    :goto_3
    iput v5, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->state:I

    .line 112
    .line 113
    array-length v2, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    if-nez v2, :cond_5

    .line 115
    .line 116
    monitor-exit p0

    .line 117
    return-void

    .line 118
    :cond_5
    :try_start_1
    iget v2, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 119
    .line 120
    rsub-int/lit8 v5, v2, 0x14

    .line 121
    .line 122
    array-length v6, v0

    .line 123
    const/4 v7, 0x0

    .line 124
    sub-int/2addr v6, v7

    .line 125
    if-ge v5, v6, :cond_6

    .line 126
    .line 127
    rsub-int/lit8 v5, v2, 0x14

    .line 128
    .line 129
    goto :goto_4

    .line 130
    :cond_6
    array-length v5, v0

    .line 131
    sub-int/2addr v5, v7

    .line 132
    :goto_4
    if-lez v5, :cond_7

    .line 133
    .line 134
    iget-object v6, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBytes:[B

    .line 135
    .line 136
    invoke-static {v6, v2, v0, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 137
    .line 138
    .line 139
    iget v2, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 140
    .line 141
    add-int/2addr v2, v5

    .line 142
    iput v2, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 143
    .line 144
    add-int/2addr v5, v7

    .line 145
    goto :goto_5

    .line 146
    :cond_7
    const/4 v5, 0x0

    .line 147
    :goto_5
    array-length v6, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    if-lt v5, v6, :cond_8

    .line 149
    .line 150
    monitor-exit p0

    .line 151
    return-void

    .line 152
    :cond_8
    :try_start_2
    iget-object v6, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 153
    .line 154
    aget v6, v6, v3

    .line 155
    .line 156
    const/4 v7, 0x3

    .line 157
    and-int/2addr v6, v7

    .line 158
    :goto_6
    if-nez v6, :cond_9

    .line 159
    .line 160
    iget-object v7, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 161
    .line 162
    iget-wide v12, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->counter:J

    .line 163
    .line 164
    ushr-long v14, v12, v8

    .line 165
    .line 166
    long-to-int v15, v14

    .line 167
    aput v15, v7, v4

    .line 168
    .line 169
    add-int/lit8 v14, v4, 0x1

    .line 170
    .line 171
    and-long/2addr v12, v10

    .line 172
    long-to-int v13, v12

    .line 173
    aput v13, v7, v14

    .line 174
    .line 175
    add-int/lit8 v12, v4, 0x2

    .line 176
    .line 177
    sget-object v13, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->END_FLAGS:[I

    .line 178
    .line 179
    const/4 v2, 0x0

    .line 180
    aget v13, v13, v2

    .line 181
    .line 182
    aput v13, v7, v12

    .line 183
    .line 184
    goto :goto_7

    .line 185
    :cond_9
    iget-object v7, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 186
    .line 187
    aget v12, v7, v4

    .line 188
    .line 189
    iget-wide v13, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->counter:J

    .line 190
    .line 191
    sget-object v15, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->RIGHT1:[I

    .line 192
    .line 193
    aget v15, v15, v6

    .line 194
    .line 195
    ushr-long v17, v13, v15

    .line 196
    .line 197
    sget-object v15, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->MASK:[I

    .line 198
    .line 199
    aget v15, v15, v6

    .line 200
    .line 201
    int-to-long v8, v15

    .line 202
    and-long v8, v17, v8

    .line 203
    .line 204
    long-to-int v9, v8

    .line 205
    or-int v8, v12, v9

    .line 206
    .line 207
    aput v8, v7, v4

    .line 208
    .line 209
    add-int/lit8 v8, v4, 0x1

    .line 210
    .line 211
    sget-object v9, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->RIGHT2:[I

    .line 212
    .line 213
    aget v9, v9, v6

    .line 214
    .line 215
    ushr-long v17, v13, v9

    .line 216
    .line 217
    and-long v2, v17, v10

    .line 218
    .line 219
    long-to-int v3, v2

    .line 220
    aput v3, v7, v8

    .line 221
    .line 222
    add-int/lit8 v2, v4, 0x2

    .line 223
    .line 224
    sget-object v3, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->LEFT:[I

    .line 225
    .line 226
    aget v3, v3, v6

    .line 227
    .line 228
    shl-long/2addr v13, v3

    .line 229
    sget-object v3, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->END_FLAGS:[I

    .line 230
    .line 231
    aget v3, v3, v6

    .line 232
    .line 233
    int-to-long v9, v3

    .line 234
    or-long/2addr v9, v13

    .line 235
    long-to-int v3, v9

    .line 236
    aput v3, v7, v2

    .line 237
    .line 238
    :goto_7
    iget-object v2, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 239
    .line 240
    const/16 v3, 0x51

    .line 241
    .line 242
    aget v7, v2, v3

    .line 243
    .line 244
    const/16 v3, 0x10

    .line 245
    .line 246
    const/16 v8, 0x30

    .line 247
    .line 248
    if-le v7, v8, :cond_a

    .line 249
    .line 250
    iget-object v7, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 251
    .line 252
    aget v8, v2, v3

    .line 253
    .line 254
    const/4 v10, 0x5

    .line 255
    aput v8, v7, v10

    .line 256
    .line 257
    const/16 v8, 0x11

    .line 258
    .line 259
    aget v8, v2, v8

    .line 260
    .line 261
    const/4 v10, 0x6

    .line 262
    aput v8, v7, v10

    .line 263
    .line 264
    :cond_a
    invoke-static {v2}, Lorg/apache/harmony/security/provider/crypto/SHA1Impl;->computeHash([I)V

    .line 265
    .line 266
    .line 267
    iget-object v2, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 268
    .line 269
    const/16 v7, 0x51

    .line 270
    .line 271
    aget v8, v2, v7

    .line 272
    .line 273
    const/16 v9, 0x30

    .line 274
    .line 275
    if-le v8, v9, :cond_b

    .line 276
    .line 277
    iget-object v8, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 278
    .line 279
    const/16 v10, 0x15

    .line 280
    .line 281
    const/4 v11, 0x0

    .line 282
    invoke-static {v2, v11, v8, v10, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 283
    .line 284
    .line 285
    iget-object v2, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 286
    .line 287
    iget-object v8, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 288
    .line 289
    const/4 v12, 0x5

    .line 290
    invoke-static {v2, v12, v8, v11, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 291
    .line 292
    .line 293
    iget-object v2, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 294
    .line 295
    invoke-static {v2}, Lorg/apache/harmony/security/provider/crypto/SHA1Impl;->computeHash([I)V

    .line 296
    .line 297
    .line 298
    iget-object v2, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 299
    .line 300
    iget-object v8, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 301
    .line 302
    invoke-static {v2, v10, v8, v11, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 303
    .line 304
    .line 305
    :cond_b
    iget-wide v10, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->counter:J

    .line 306
    .line 307
    const-wide/16 v12, 0x1

    .line 308
    .line 309
    add-long/2addr v10, v12

    .line 310
    iput-wide v10, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->counter:J

    .line 311
    .line 312
    const/4 v3, 0x0

    .line 313
    const/4 v8, 0x0

    .line 314
    const/4 v10, 0x5

    .line 315
    :goto_8
    if-ge v3, v10, :cond_c

    .line 316
    .line 317
    iget-object v11, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 318
    .line 319
    add-int/lit8 v12, v3, 0x52

    .line 320
    .line 321
    aget v11, v11, v12

    .line 322
    .line 323
    iget-object v12, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBytes:[B

    .line 324
    .line 325
    ushr-int/lit8 v13, v11, 0x18

    .line 326
    .line 327
    int-to-byte v13, v13

    .line 328
    aput-byte v13, v12, v8

    .line 329
    .line 330
    add-int/lit8 v13, v8, 0x1

    .line 331
    .line 332
    ushr-int/lit8 v14, v11, 0x10

    .line 333
    .line 334
    int-to-byte v14, v14

    .line 335
    aput-byte v14, v12, v13

    .line 336
    .line 337
    add-int/lit8 v13, v8, 0x2

    .line 338
    .line 339
    ushr-int/lit8 v14, v11, 0x8

    .line 340
    .line 341
    int-to-byte v14, v14

    .line 342
    aput-byte v14, v12, v13

    .line 343
    .line 344
    add-int/lit8 v13, v8, 0x3

    .line 345
    .line 346
    int-to-byte v11, v11

    .line 347
    aput-byte v11, v12, v13

    .line 348
    .line 349
    add-int/lit8 v8, v8, 0x4

    .line 350
    .line 351
    add-int/lit8 v3, v3, 0x1

    .line 352
    .line 353
    goto :goto_8

    .line 354
    :cond_c
    const/4 v2, 0x0

    .line 355
    iput v2, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 356
    .line 357
    array-length v3, v0

    .line 358
    sub-int/2addr v3, v5

    .line 359
    const/16 v8, 0x14

    .line 360
    .line 361
    if-ge v8, v3, :cond_d

    .line 362
    .line 363
    const/16 v3, 0x14

    .line 364
    .line 365
    goto :goto_9

    .line 366
    :cond_d
    array-length v3, v0

    .line 367
    sub-int/2addr v3, v5

    .line 368
    :goto_9
    if-lez v3, :cond_e

    .line 369
    .line 370
    iget-object v11, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBytes:[B

    .line 371
    .line 372
    const/4 v2, 0x0

    .line 373
    invoke-static {v11, v2, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 374
    .line 375
    .line 376
    add-int/2addr v5, v3

    .line 377
    iget v11, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 378
    .line 379
    add-int/2addr v11, v3

    .line 380
    iput v11, v1, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->nextBIndex:I

    .line 381
    .line 382
    goto :goto_a

    .line 383
    :cond_e
    const/4 v2, 0x0

    .line 384
    :goto_a
    array-length v3, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 385
    if-lt v5, v3, :cond_f

    .line 386
    .line 387
    monitor-exit p0

    .line 388
    return-void

    .line 389
    :cond_f
    const/16 v3, 0x51

    .line 390
    .line 391
    const/16 v8, 0x20

    .line 392
    .line 393
    const-wide/16 v10, -0x1

    .line 394
    .line 395
    const/16 v14, 0x14

    .line 396
    .line 397
    goto/16 :goto_6

    .line 398
    .line 399
    :catchall_0
    move-exception v0

    .line 400
    goto :goto_b

    .line 401
    :cond_10
    :try_start_3
    new-instance v0, Ljava/lang/NullPointerException;

    .line 402
    .line 403
    const-string v2, "bytes == null"

    .line 404
    .line 405
    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 406
    .line 407
    .line 408
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 409
    :goto_b
    monitor-exit p0

    .line 410
    throw v0
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
.end method

.method protected declared-synchronized engineSetSeed([B)V
    .locals 5

    .line 1
    monitor-enter p0

    .line 2
    if-eqz p1, :cond_2

    .line 3
    .line 4
    :try_start_0
    iget v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->state:I

    .line 5
    .line 6
    const/4 v1, 0x2

    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->copies:[I

    .line 10
    .line 11
    iget-object v1, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->seed:[I

    .line 12
    .line 13
    const/16 v2, 0x52

    .line 14
    .line 15
    const/4 v3, 0x5

    .line 16
    const/4 v4, 0x0

    .line 17
    invoke-static {v0, v4, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const/4 v0, 0x1

    .line 21
    iput v0, p0, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->state:I

    .line 22
    .line 23
    array-length v0, p1

    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    invoke-direct {p0, p1}, Lorg/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl;->updateSeed([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    .line 28
    .line 29
    :cond_1
    monitor-exit p0

    .line 30
    return-void

    .line 31
    :catchall_0
    move-exception p1

    .line 32
    goto :goto_0

    .line 33
    :cond_2
    :try_start_1
    new-instance p1, Ljava/lang/NullPointerException;

    .line 34
    .line 35
    const-string v0, "seed == null"

    .line 36
    .line 37
    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 41
    :goto_0
    monitor-exit p0

    .line 42
    throw p1
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
.end method

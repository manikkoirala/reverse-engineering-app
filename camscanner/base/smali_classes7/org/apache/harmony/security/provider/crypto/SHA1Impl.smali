.class Lorg/apache/harmony/security/provider/crypto/SHA1Impl;
.super Ljava/lang/Object;
.source "SHA1Impl.java"

# interfaces
.implements Lorg/apache/harmony/security/provider/crypto/SHA1_DataImpl;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static computeHash([I)V
    .locals 16

    .line 1
    const/16 v0, 0x52

    .line 2
    .line 3
    aget v1, p0, v0

    .line 4
    .line 5
    const/16 v2, 0x53

    .line 6
    .line 7
    aget v3, p0, v2

    .line 8
    .line 9
    const/16 v4, 0x54

    .line 10
    .line 11
    aget v5, p0, v4

    .line 12
    .line 13
    const/16 v6, 0x55

    .line 14
    .line 15
    aget v7, p0, v6

    .line 16
    .line 17
    const/16 v8, 0x56

    .line 18
    .line 19
    aget v9, p0, v8

    .line 20
    .line 21
    const/16 v10, 0x10

    .line 22
    .line 23
    :goto_0
    const/16 v11, 0x50

    .line 24
    .line 25
    if-ge v10, v11, :cond_0

    .line 26
    .line 27
    add-int/lit8 v11, v10, -0x3

    .line 28
    .line 29
    aget v11, p0, v11

    .line 30
    .line 31
    add-int/lit8 v12, v10, -0x8

    .line 32
    .line 33
    aget v12, p0, v12

    .line 34
    .line 35
    xor-int/2addr v11, v12

    .line 36
    add-int/lit8 v12, v10, -0xe

    .line 37
    .line 38
    aget v12, p0, v12

    .line 39
    .line 40
    xor-int/2addr v11, v12

    .line 41
    add-int/lit8 v12, v10, -0x10

    .line 42
    .line 43
    aget v12, p0, v12

    .line 44
    .line 45
    xor-int/2addr v11, v12

    .line 46
    shl-int/lit8 v12, v11, 0x1

    .line 47
    .line 48
    ushr-int/lit8 v11, v11, 0x1f

    .line 49
    .line 50
    or-int/2addr v11, v12

    .line 51
    aput v11, p0, v10

    .line 52
    .line 53
    add-int/lit8 v10, v10, 0x1

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_0
    const/4 v10, 0x0

    .line 57
    :goto_1
    const/16 v12, 0x14

    .line 58
    .line 59
    if-ge v10, v12, :cond_1

    .line 60
    .line 61
    shl-int/lit8 v12, v1, 0x5

    .line 62
    .line 63
    ushr-int/lit8 v13, v1, 0x1b

    .line 64
    .line 65
    or-int/2addr v12, v13

    .line 66
    and-int v13, v3, v5

    .line 67
    .line 68
    not-int v14, v3

    .line 69
    and-int/2addr v14, v7

    .line 70
    or-int/2addr v13, v14

    .line 71
    add-int/2addr v12, v13

    .line 72
    aget v13, p0, v10

    .line 73
    .line 74
    add-int/2addr v9, v13

    .line 75
    const v13, 0x5a827999

    .line 76
    .line 77
    .line 78
    add-int/2addr v9, v13

    .line 79
    add-int/2addr v9, v12

    .line 80
    shl-int/lit8 v12, v3, 0x1e

    .line 81
    .line 82
    ushr-int/lit8 v3, v3, 0x2

    .line 83
    .line 84
    or-int/2addr v3, v12

    .line 85
    add-int/lit8 v10, v10, 0x1

    .line 86
    .line 87
    move v15, v3

    .line 88
    move v3, v1

    .line 89
    move v1, v9

    .line 90
    move v9, v7

    .line 91
    move v7, v5

    .line 92
    move v5, v15

    .line 93
    goto :goto_1

    .line 94
    :cond_1
    :goto_2
    const/16 v10, 0x28

    .line 95
    .line 96
    if-ge v12, v10, :cond_2

    .line 97
    .line 98
    shl-int/lit8 v10, v1, 0x5

    .line 99
    .line 100
    ushr-int/lit8 v13, v1, 0x1b

    .line 101
    .line 102
    or-int/2addr v10, v13

    .line 103
    xor-int v13, v3, v5

    .line 104
    .line 105
    xor-int/2addr v13, v7

    .line 106
    add-int/2addr v10, v13

    .line 107
    aget v13, p0, v12

    .line 108
    .line 109
    add-int/2addr v9, v13

    .line 110
    const v13, 0x6ed9eba1

    .line 111
    .line 112
    .line 113
    add-int/2addr v9, v13

    .line 114
    add-int/2addr v9, v10

    .line 115
    shl-int/lit8 v10, v3, 0x1e

    .line 116
    .line 117
    ushr-int/lit8 v3, v3, 0x2

    .line 118
    .line 119
    or-int/2addr v3, v10

    .line 120
    add-int/lit8 v12, v12, 0x1

    .line 121
    .line 122
    move v15, v3

    .line 123
    move v3, v1

    .line 124
    move v1, v9

    .line 125
    move v9, v7

    .line 126
    move v7, v5

    .line 127
    move v5, v15

    .line 128
    goto :goto_2

    .line 129
    :cond_2
    :goto_3
    const/16 v12, 0x3c

    .line 130
    .line 131
    if-ge v10, v12, :cond_3

    .line 132
    .line 133
    shl-int/lit8 v12, v1, 0x5

    .line 134
    .line 135
    ushr-int/lit8 v13, v1, 0x1b

    .line 136
    .line 137
    or-int/2addr v12, v13

    .line 138
    and-int v13, v3, v5

    .line 139
    .line 140
    and-int v14, v3, v7

    .line 141
    .line 142
    or-int/2addr v13, v14

    .line 143
    and-int v14, v5, v7

    .line 144
    .line 145
    or-int/2addr v13, v14

    .line 146
    add-int/2addr v12, v13

    .line 147
    aget v13, p0, v10

    .line 148
    .line 149
    add-int/2addr v9, v13

    .line 150
    const v13, -0x70e44324

    .line 151
    .line 152
    .line 153
    add-int/2addr v9, v13

    .line 154
    add-int/2addr v9, v12

    .line 155
    shl-int/lit8 v12, v3, 0x1e

    .line 156
    .line 157
    ushr-int/lit8 v3, v3, 0x2

    .line 158
    .line 159
    or-int/2addr v3, v12

    .line 160
    add-int/lit8 v10, v10, 0x1

    .line 161
    .line 162
    move v15, v3

    .line 163
    move v3, v1

    .line 164
    move v1, v9

    .line 165
    move v9, v7

    .line 166
    move v7, v5

    .line 167
    move v5, v15

    .line 168
    goto :goto_3

    .line 169
    :cond_3
    :goto_4
    if-ge v12, v11, :cond_4

    .line 170
    .line 171
    shl-int/lit8 v10, v1, 0x5

    .line 172
    .line 173
    ushr-int/lit8 v13, v1, 0x1b

    .line 174
    .line 175
    or-int/2addr v10, v13

    .line 176
    xor-int v13, v3, v5

    .line 177
    .line 178
    xor-int/2addr v13, v7

    .line 179
    add-int/2addr v10, v13

    .line 180
    aget v13, p0, v12

    .line 181
    .line 182
    add-int/2addr v9, v13

    .line 183
    const v13, -0x359d3e2a    # -3715189.5f

    .line 184
    .line 185
    .line 186
    add-int/2addr v9, v13

    .line 187
    add-int/2addr v9, v10

    .line 188
    shl-int/lit8 v10, v3, 0x1e

    .line 189
    .line 190
    ushr-int/lit8 v3, v3, 0x2

    .line 191
    .line 192
    or-int/2addr v3, v10

    .line 193
    add-int/lit8 v12, v12, 0x1

    .line 194
    .line 195
    move v15, v3

    .line 196
    move v3, v1

    .line 197
    move v1, v9

    .line 198
    move v9, v7

    .line 199
    move v7, v5

    .line 200
    move v5, v15

    .line 201
    goto :goto_4

    .line 202
    :cond_4
    aget v10, p0, v0

    .line 203
    .line 204
    add-int/2addr v10, v1

    .line 205
    aput v10, p0, v0

    .line 206
    .line 207
    aget v0, p0, v2

    .line 208
    .line 209
    add-int/2addr v0, v3

    .line 210
    aput v0, p0, v2

    .line 211
    .line 212
    aget v0, p0, v4

    .line 213
    .line 214
    add-int/2addr v0, v5

    .line 215
    aput v0, p0, v4

    .line 216
    .line 217
    aget v0, p0, v6

    .line 218
    .line 219
    add-int/2addr v0, v7

    .line 220
    aput v0, p0, v6

    .line 221
    .line 222
    aget v0, p0, v8

    .line 223
    .line 224
    add-int/2addr v0, v9

    .line 225
    aput v0, p0, v8

    .line 226
    .line 227
    return-void
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
.end method

.method static updateHash([I[BII)V
    .locals 9

    .line 1
    const/16 v0, 0x51

    .line 2
    .line 3
    aget v1, p0, v0

    .line 4
    .line 5
    shr-int/lit8 v2, v1, 0x2

    .line 6
    .line 7
    and-int/lit8 v3, v1, 0x3

    .line 8
    .line 9
    add-int/2addr v1, p3

    .line 10
    sub-int/2addr v1, p2

    .line 11
    const/4 v4, 0x1

    .line 12
    add-int/2addr v1, v4

    .line 13
    and-int/lit8 v1, v1, 0x3f

    .line 14
    .line 15
    aput v1, p0, v0

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    const/16 v1, 0x10

    .line 19
    .line 20
    if-eqz v3, :cond_2

    .line 21
    .line 22
    :goto_0
    const/4 v5, 0x4

    .line 23
    if-gt p2, p3, :cond_0

    .line 24
    .line 25
    if-ge v3, v5, :cond_0

    .line 26
    .line 27
    aget v5, p0, v2

    .line 28
    .line 29
    aget-byte v6, p1, p2

    .line 30
    .line 31
    and-int/lit16 v6, v6, 0xff

    .line 32
    .line 33
    rsub-int/lit8 v7, v3, 0x3

    .line 34
    .line 35
    shl-int/lit8 v7, v7, 0x3

    .line 36
    .line 37
    shl-int/2addr v6, v7

    .line 38
    or-int/2addr v5, v6

    .line 39
    aput v5, p0, v2

    .line 40
    .line 41
    add-int/lit8 v3, v3, 0x1

    .line 42
    .line 43
    add-int/lit8 p2, p2, 0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    if-ne v3, v5, :cond_1

    .line 47
    .line 48
    add-int/lit8 v2, v2, 0x1

    .line 49
    .line 50
    if-ne v2, v1, :cond_1

    .line 51
    .line 52
    invoke-static {p0}, Lorg/apache/harmony/security/provider/crypto/SHA1Impl;->computeHash([I)V

    .line 53
    .line 54
    .line 55
    const/4 v2, 0x0

    .line 56
    :cond_1
    if-le p2, p3, :cond_2

    .line 57
    .line 58
    return-void

    .line 59
    :cond_2
    sub-int v3, p3, p2

    .line 60
    .line 61
    add-int/2addr v3, v4

    .line 62
    const/4 v5, 0x2

    .line 63
    shr-int/2addr v3, v5

    .line 64
    const/4 v6, 0x0

    .line 65
    :goto_1
    if-ge v6, v3, :cond_4

    .line 66
    .line 67
    aget-byte v7, p1, p2

    .line 68
    .line 69
    and-int/lit16 v7, v7, 0xff

    .line 70
    .line 71
    shl-int/lit8 v7, v7, 0x18

    .line 72
    .line 73
    add-int/lit8 v8, p2, 0x1

    .line 74
    .line 75
    aget-byte v8, p1, v8

    .line 76
    .line 77
    and-int/lit16 v8, v8, 0xff

    .line 78
    .line 79
    shl-int/2addr v8, v1

    .line 80
    or-int/2addr v7, v8

    .line 81
    add-int/lit8 v8, p2, 0x2

    .line 82
    .line 83
    aget-byte v8, p1, v8

    .line 84
    .line 85
    and-int/lit16 v8, v8, 0xff

    .line 86
    .line 87
    shl-int/lit8 v8, v8, 0x8

    .line 88
    .line 89
    or-int/2addr v7, v8

    .line 90
    add-int/lit8 v8, p2, 0x3

    .line 91
    .line 92
    aget-byte v8, p1, v8

    .line 93
    .line 94
    and-int/lit16 v8, v8, 0xff

    .line 95
    .line 96
    or-int/2addr v7, v8

    .line 97
    aput v7, p0, v2

    .line 98
    .line 99
    add-int/lit8 p2, p2, 0x4

    .line 100
    .line 101
    add-int/lit8 v2, v2, 0x1

    .line 102
    .line 103
    if-ge v2, v1, :cond_3

    .line 104
    .line 105
    goto :goto_2

    .line 106
    :cond_3
    invoke-static {p0}, Lorg/apache/harmony/security/provider/crypto/SHA1Impl;->computeHash([I)V

    .line 107
    .line 108
    .line 109
    const/4 v2, 0x0

    .line 110
    :goto_2
    add-int/lit8 v6, v6, 0x1

    .line 111
    .line 112
    goto :goto_1

    .line 113
    :cond_4
    sub-int/2addr p3, p2

    .line 114
    add-int/2addr p3, v4

    .line 115
    if-eqz p3, :cond_6

    .line 116
    .line 117
    aget-byte v0, p1, p2

    .line 118
    .line 119
    and-int/lit16 v0, v0, 0xff

    .line 120
    .line 121
    shl-int/lit8 v0, v0, 0x18

    .line 122
    .line 123
    if-eq p3, v4, :cond_5

    .line 124
    .line 125
    add-int/lit8 v3, p2, 0x1

    .line 126
    .line 127
    aget-byte v3, p1, v3

    .line 128
    .line 129
    and-int/lit16 v3, v3, 0xff

    .line 130
    .line 131
    shl-int/lit8 v1, v3, 0x10

    .line 132
    .line 133
    or-int/2addr v0, v1

    .line 134
    if-eq p3, v5, :cond_5

    .line 135
    .line 136
    add-int/2addr p2, v5

    .line 137
    aget-byte p1, p1, p2

    .line 138
    .line 139
    and-int/lit16 p1, p1, 0xff

    .line 140
    .line 141
    shl-int/lit8 p1, p1, 0x8

    .line 142
    .line 143
    or-int/2addr v0, p1

    .line 144
    :cond_5
    aput v0, p0, v2

    .line 145
    .line 146
    :cond_6
    return-void
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
.end method

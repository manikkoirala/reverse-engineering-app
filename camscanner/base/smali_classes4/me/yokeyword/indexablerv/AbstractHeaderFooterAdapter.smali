.class abstract Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;
.super Ljava/lang/Object;
.source "AbstractHeaderFooterAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter$OnItemLongClickListener;,
        Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter$OnItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private O8:Ljava/lang/String;

.field private Oo08:Ljava/lang/String;

.field private final 〇080:Lme/yokeyword/indexablerv/database/HeaderFooterDataObservable;

.field private final 〇o00〇〇Oo:Lme/yokeyword/indexablerv/database/IndexBarDataObservable;

.field 〇o〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lme/yokeyword/indexablerv/EntityWrapper<",
            "TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lme/yokeyword/indexablerv/database/HeaderFooterDataObservable;

    .line 5
    .line 6
    invoke-direct {v0}, Lme/yokeyword/indexablerv/database/HeaderFooterDataObservable;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->〇080:Lme/yokeyword/indexablerv/database/HeaderFooterDataObservable;

    .line 10
    .line 11
    new-instance v0, Lme/yokeyword/indexablerv/database/IndexBarDataObservable;

    .line 12
    .line 13
    invoke-direct {v0}, Lme/yokeyword/indexablerv/database/IndexBarDataObservable;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->〇o00〇〇Oo:Lme/yokeyword/indexablerv/database/IndexBarDataObservable;

    .line 17
    .line 18
    new-instance v0, Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->〇o〇:Ljava/util/ArrayList;

    .line 24
    .line 25
    iput-object p1, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->O8:Ljava/lang/String;

    .line 26
    .line 27
    iput-object p2, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->Oo08:Ljava/lang/String;

    .line 28
    .line 29
    if-eqz p2, :cond_0

    .line 30
    .line 31
    invoke-direct {p0}, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->OO0o〇〇〇〇0()Lme/yokeyword/indexablerv/EntityWrapper;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    const p2, 0x7ffffffe

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1, p2}, Lme/yokeyword/indexablerv/EntityWrapper;->Oooo8o0〇(I)V

    .line 39
    .line 40
    .line 41
    :cond_0
    const/4 p1, 0x0

    .line 42
    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    .line 43
    .line 44
    .line 45
    move-result p2

    .line 46
    if-ge p1, p2, :cond_1

    .line 47
    .line 48
    invoke-direct {p0}, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->OO0o〇〇〇〇0()Lme/yokeyword/indexablerv/EntityWrapper;

    .line 49
    .line 50
    .line 51
    move-result-object p2

    .line 52
    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {p2, v0}, Lme/yokeyword/indexablerv/EntityWrapper;->〇80〇808〇O(Ljava/lang/Object;)V

    .line 57
    .line 58
    .line 59
    add-int/lit8 p1, p1, 0x1

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_1
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
.end method

.method private OO0o〇〇〇〇0()Lme/yokeyword/indexablerv/EntityWrapper;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lme/yokeyword/indexablerv/EntityWrapper<",
            "TT;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lme/yokeyword/indexablerv/EntityWrapper;

    .line 2
    .line 3
    invoke-direct {v0}, Lme/yokeyword/indexablerv/EntityWrapper;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->O8:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lme/yokeyword/indexablerv/EntityWrapper;->〇8o8o〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->Oo08:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lme/yokeyword/indexablerv/EntityWrapper;->OO0o〇〇(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->〇o00〇〇Oo()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-virtual {v0, v1}, Lme/yokeyword/indexablerv/EntityWrapper;->OO0o〇〇〇〇0(I)V

    .line 21
    .line 22
    .line 23
    iget-object v1, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->〇o〇:Ljava/util/ArrayList;

    .line 24
    .line 25
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    return-object v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method


# virtual methods
.method O8()Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter$OnItemClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter$OnItemClickListener<",
            "TT;>;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method Oo08()Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter$OnItemLongClickListener;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method oO80(Lme/yokeyword/indexablerv/database/HeaderFooterDataObserver;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->〇080:Lme/yokeyword/indexablerv/database/HeaderFooterDataObservable;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/database/Observable;->registerObserver(Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public abstract o〇0(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
            "TT;)V"
        }
    .end annotation
.end method

.method 〇080()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lme/yokeyword/indexablerv/EntityWrapper<",
            "TT;>;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->〇o〇:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lme/yokeyword/indexablerv/EntityWrapper;

    .line 18
    .line 19
    invoke-virtual {v1}, Lme/yokeyword/indexablerv/EntityWrapper;->o〇0()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    const v3, 0x7fffffff

    .line 24
    .line 25
    .line 26
    if-ne v2, v3, :cond_0

    .line 27
    .line 28
    invoke-virtual {p0}, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->〇o〇()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    invoke-virtual {v1, v2}, Lme/yokeyword/indexablerv/EntityWrapper;->Oooo8o0〇(I)V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    iget-object v0, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->〇o〇:Ljava/util/ArrayList;

    .line 37
    .line 38
    return-object v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method 〇80〇808〇O(Lme/yokeyword/indexablerv/database/IndexBarDataObserver;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lme/yokeyword/indexablerv/AbstractHeaderFooterAdapter;->〇o00〇〇Oo:Lme/yokeyword/indexablerv/database/IndexBarDataObservable;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/database/Observable;->registerObserver(Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method 〇o00〇〇Oo()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public abstract 〇o〇()I
.end method

.method public abstract 〇〇888(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.end method

.class public abstract Lme/yokeyword/indexablerv/IndexableAdapter;
.super Ljava/lang/Object;
.source "IndexableAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lme/yokeyword/indexablerv/IndexableAdapter$OnItemContentLongClickListener;,
        Lme/yokeyword/indexablerv/IndexableAdapter$OnItemTitleLongClickListener;,
        Lme/yokeyword/indexablerv/IndexableAdapter$OnItemContentClickListener;,
        Lme/yokeyword/indexablerv/IndexableAdapter$OnItemTitleClickListener;,
        Lme/yokeyword/indexablerv/IndexableAdapter$IndexCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lme/yokeyword/indexablerv/IndexableEntity;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final 〇080:Lme/yokeyword/indexablerv/database/DataObservable;

.field private 〇o00〇〇Oo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field private 〇o〇:Lme/yokeyword/indexablerv/IndexableAdapter$OnItemContentClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lme/yokeyword/indexablerv/database/DataObservable;

    .line 5
    .line 6
    invoke-direct {v0}, Lme/yokeyword/indexablerv/database/DataObservable;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lme/yokeyword/indexablerv/IndexableAdapter;->〇080:Lme/yokeyword/indexablerv/database/DataObservable;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private oO80(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lme/yokeyword/indexablerv/IndexableAdapter;->〇080:Lme/yokeyword/indexablerv/database/DataObservable;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lme/yokeyword/indexablerv/database/DataObservable;->〇o00〇〇Oo(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇888()V
    .locals 1

    .line 1
    iget-object v0, p0, Lme/yokeyword/indexablerv/IndexableAdapter;->〇080:Lme/yokeyword/indexablerv/database/DataObservable;

    .line 2
    .line 3
    invoke-virtual {v0}, Lme/yokeyword/indexablerv/database/DataObservable;->〇080()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method


# virtual methods
.method O8()Lme/yokeyword/indexablerv/IndexableAdapter$OnItemContentLongClickListener;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method OO0o〇〇(Lme/yokeyword/indexablerv/database/DataObserver;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lme/yokeyword/indexablerv/IndexableAdapter;->〇080:Lme/yokeyword/indexablerv/database/DataObservable;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/database/Observable;->registerObserver(Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public abstract OO0o〇〇〇〇0(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Ljava/lang/String;)V
.end method

.method Oo08()Lme/yokeyword/indexablerv/IndexableAdapter$OnItemTitleClickListener;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public Oooo8o0〇(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0}, Lme/yokeyword/indexablerv/IndexableAdapter;->〇〇808〇(Ljava/util/List;Lme/yokeyword/indexablerv/IndexableAdapter$IndexCallback;)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method o〇0()Lme/yokeyword/indexablerv/IndexableAdapter$OnItemTitleLongClickListener;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method 〇080()Lme/yokeyword/indexablerv/IndexableAdapter$IndexCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lme/yokeyword/indexablerv/IndexableAdapter$IndexCallback<",
            "TT;>;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public abstract 〇80〇808〇O(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Lme/yokeyword/indexablerv/IndexableEntity;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
            "TT;)V"
        }
    .end annotation
.end method

.method public abstract 〇8o8o〇(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.end method

.method 〇O00(Lme/yokeyword/indexablerv/database/DataObserver;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lme/yokeyword/indexablerv/IndexableAdapter;->〇080:Lme/yokeyword/indexablerv/database/DataObservable;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/database/Observable;->unregisterObserver(Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public abstract 〇O8o08O(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.end method

.method public 〇O〇(Lme/yokeyword/indexablerv/IndexableAdapter$OnItemContentClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lme/yokeyword/indexablerv/IndexableAdapter$OnItemContentClickListener<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lme/yokeyword/indexablerv/IndexableAdapter;->〇o〇:Lme/yokeyword/indexablerv/IndexableAdapter$OnItemContentClickListener;

    .line 2
    .line 3
    const/4 p1, 0x2

    .line 4
    invoke-direct {p0, p1}, Lme/yokeyword/indexablerv/IndexableAdapter;->oO80(I)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o00〇〇Oo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lme/yokeyword/indexablerv/IndexableAdapter;->〇o00〇〇Oo:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method 〇o〇()Lme/yokeyword/indexablerv/IndexableAdapter$OnItemContentClickListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lme/yokeyword/indexablerv/IndexableAdapter;->〇o〇:Lme/yokeyword/indexablerv/IndexableAdapter$OnItemContentClickListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public 〇〇808〇(Ljava/util/List;Lme/yokeyword/indexablerv/IndexableAdapter$IndexCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;",
            "Lme/yokeyword/indexablerv/IndexableAdapter$IndexCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lme/yokeyword/indexablerv/IndexableAdapter;->〇o00〇〇Oo:Ljava/util/List;

    .line 2
    .line 3
    invoke-direct {p0}, Lme/yokeyword/indexablerv/IndexableAdapter;->〇〇888()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
.end method

.class public final Lcom/google/android/gms/internal/ads/zziv;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-ads@@22.4.0"


# instance fields
.field final zza:Landroid/content/Context;

.field zzb:Lcom/google/android/gms/internal/ads/zzdz;

.field zzc:Lcom/google/android/gms/internal/ads/zzfry;

.field zzd:Lcom/google/android/gms/internal/ads/zzfry;

.field zze:Lcom/google/android/gms/internal/ads/zzfry;

.field zzf:Lcom/google/android/gms/internal/ads/zzfry;

.field zzg:Lcom/google/android/gms/internal/ads/zzfry;

.field zzh:Lcom/google/android/gms/internal/ads/zzfqw;

.field zzi:Landroid/os/Looper;

.field zzj:Lcom/google/android/gms/internal/ads/zzk;

.field zzk:I

.field zzl:Z

.field zzm:Lcom/google/android/gms/internal/ads/zzlr;

.field zzn:J

.field zzo:J

.field zzp:Z

.field zzq:Z

.field zzr:Lcom/google/android/gms/internal/ads/zzig;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/ads/zzcer;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    new-instance v2, Lcom/google/android/gms/internal/ads/zzip;

    .line 6
    .line 7
    move-object/from16 v3, p2

    .line 8
    .line 9
    invoke-direct {v2, v3}, Lcom/google/android/gms/internal/ads/zzip;-><init>(Lcom/google/android/gms/internal/ads/zzcer;)V

    .line 10
    .line 11
    .line 12
    new-instance v3, Lcom/google/android/gms/internal/ads/zziq;

    .line 13
    .line 14
    invoke-direct {v3, v1}, Lcom/google/android/gms/internal/ads/zziq;-><init>(Landroid/content/Context;)V

    .line 15
    .line 16
    .line 17
    new-instance v4, Lcom/google/android/gms/internal/ads/zzir;

    .line 18
    .line 19
    invoke-direct {v4, v1}, Lcom/google/android/gms/internal/ads/zzir;-><init>(Landroid/content/Context;)V

    .line 20
    .line 21
    .line 22
    sget-object v5, Lcom/google/android/gms/internal/ads/zzis;->zza:Lcom/google/android/gms/internal/ads/zzis;

    .line 23
    .line 24
    new-instance v6, Lcom/google/android/gms/internal/ads/zzit;

    .line 25
    .line 26
    invoke-direct {v6, v1}, Lcom/google/android/gms/internal/ads/zzit;-><init>(Landroid/content/Context;)V

    .line 27
    .line 28
    .line 29
    sget-object v7, Lcom/google/android/gms/internal/ads/zziu;->zza:Lcom/google/android/gms/internal/ads/zziu;

    .line 30
    .line 31
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 32
    .line 33
    .line 34
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 35
    .line 36
    .line 37
    iput-object v1, v0, Lcom/google/android/gms/internal/ads/zziv;->zza:Landroid/content/Context;

    .line 38
    .line 39
    iput-object v2, v0, Lcom/google/android/gms/internal/ads/zziv;->zzc:Lcom/google/android/gms/internal/ads/zzfry;

    .line 40
    .line 41
    iput-object v3, v0, Lcom/google/android/gms/internal/ads/zziv;->zzd:Lcom/google/android/gms/internal/ads/zzfry;

    .line 42
    .line 43
    iput-object v4, v0, Lcom/google/android/gms/internal/ads/zziv;->zze:Lcom/google/android/gms/internal/ads/zzfry;

    .line 44
    .line 45
    iput-object v5, v0, Lcom/google/android/gms/internal/ads/zziv;->zzf:Lcom/google/android/gms/internal/ads/zzfry;

    .line 46
    .line 47
    iput-object v6, v0, Lcom/google/android/gms/internal/ads/zziv;->zzg:Lcom/google/android/gms/internal/ads/zzfry;

    .line 48
    .line 49
    iput-object v7, v0, Lcom/google/android/gms/internal/ads/zziv;->zzh:Lcom/google/android/gms/internal/ads/zzfqw;

    .line 50
    .line 51
    invoke-static {}, Lcom/google/android/gms/internal/ads/zzfk;->zzv()Landroid/os/Looper;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    iput-object v1, v0, Lcom/google/android/gms/internal/ads/zziv;->zzi:Landroid/os/Looper;

    .line 56
    .line 57
    sget-object v1, Lcom/google/android/gms/internal/ads/zzk;->zza:Lcom/google/android/gms/internal/ads/zzk;

    .line 58
    .line 59
    iput-object v1, v0, Lcom/google/android/gms/internal/ads/zziv;->zzj:Lcom/google/android/gms/internal/ads/zzk;

    .line 60
    .line 61
    const/4 v1, 0x1

    .line 62
    iput v1, v0, Lcom/google/android/gms/internal/ads/zziv;->zzk:I

    .line 63
    .line 64
    iput-boolean v1, v0, Lcom/google/android/gms/internal/ads/zziv;->zzl:Z

    .line 65
    .line 66
    sget-object v2, Lcom/google/android/gms/internal/ads/zzlr;->zze:Lcom/google/android/gms/internal/ads/zzlr;

    .line 67
    .line 68
    iput-object v2, v0, Lcom/google/android/gms/internal/ads/zziv;->zzm:Lcom/google/android/gms/internal/ads/zzlr;

    .line 69
    .line 70
    new-instance v2, Lcom/google/android/gms/internal/ads/zzig;

    .line 71
    .line 72
    const v4, 0x3f7851ec    # 0.97f

    .line 73
    .line 74
    .line 75
    const v5, 0x3f83d70a    # 1.03f

    .line 76
    .line 77
    .line 78
    const-wide/16 v6, 0x3e8

    .line 79
    .line 80
    const v8, 0x33d6bf95    # 1.0E-7f

    .line 81
    .line 82
    .line 83
    const-wide/16 v9, 0x14

    .line 84
    .line 85
    invoke-static {v9, v10}, Lcom/google/android/gms/internal/ads/zzfk;->zzp(J)J

    .line 86
    .line 87
    .line 88
    move-result-wide v9

    .line 89
    const-wide/16 v14, 0x1f4

    .line 90
    .line 91
    invoke-static {v14, v15}, Lcom/google/android/gms/internal/ads/zzfk;->zzp(J)J

    .line 92
    .line 93
    .line 94
    move-result-wide v11

    .line 95
    const v13, 0x3f7fbe77    # 0.999f

    .line 96
    .line 97
    .line 98
    const/16 v16, 0x0

    .line 99
    .line 100
    move-object v3, v2

    .line 101
    move-object/from16 v14, v16

    .line 102
    .line 103
    invoke-direct/range {v3 .. v14}, Lcom/google/android/gms/internal/ads/zzig;-><init>(FFJFJJFLcom/google/android/gms/internal/ads/zzif;)V

    .line 104
    .line 105
    .line 106
    iput-object v2, v0, Lcom/google/android/gms/internal/ads/zziv;->zzr:Lcom/google/android/gms/internal/ads/zzig;

    .line 107
    .line 108
    sget-object v2, Lcom/google/android/gms/internal/ads/zzdz;->zza:Lcom/google/android/gms/internal/ads/zzdz;

    .line 109
    .line 110
    iput-object v2, v0, Lcom/google/android/gms/internal/ads/zziv;->zzb:Lcom/google/android/gms/internal/ads/zzdz;

    .line 111
    .line 112
    const-wide/16 v2, 0x1f4

    .line 113
    .line 114
    iput-wide v2, v0, Lcom/google/android/gms/internal/ads/zziv;->zzn:J

    .line 115
    .line 116
    const-wide/16 v2, 0x7d0

    .line 117
    .line 118
    iput-wide v2, v0, Lcom/google/android/gms/internal/ads/zziv;->zzo:J

    .line 119
    .line 120
    iput-boolean v1, v0, Lcom/google/android/gms/internal/ads/zziv;->zzp:Z

    .line 121
    .line 122
    return-void
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
.end method

.method static synthetic zza(Landroid/content/Context;)Lcom/google/android/gms/internal/ads/zztr;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/ads/zztf;

    .line 2
    .line 3
    new-instance v1, Lcom/google/android/gms/internal/ads/zzaaw;

    .line 4
    .line 5
    invoke-direct {v1}, Lcom/google/android/gms/internal/ads/zzaaw;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/ads/zztf;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/ads/zzabi;)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

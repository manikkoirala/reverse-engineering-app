.class public final enum Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;
.super Ljava/lang/Enum;
.source "com.google.mlkit:vision-common@@16.5.0"

# interfaces
.implements Lcom/google/android/gms/internal/mlkit_vision_common/zzv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;",
        ">;",
        "Lcom/google/android/gms/internal/mlkit_vision_common/zzv;"
    }
.end annotation


# static fields
.field public static final enum zza:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

.field public static final enum zzb:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

.field public static final enum zzc:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

.field public static final enum zzd:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

.field public static final enum zze:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

.field public static final enum zzf:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

.field public static final enum zzg:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

.field public static final enum zzh:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

.field public static final enum zzi:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

.field public static final enum zzj:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

.field private static final synthetic zzk:[Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;


# instance fields
.field private final zzl:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 2
    .line 3
    const-string v1, "UNKNOWN_FORMAT"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;-><init>(Ljava/lang/String;II)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zza:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 10
    .line 11
    new-instance v1, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 12
    .line 13
    const-string v3, "NV16"

    .line 14
    .line 15
    const/4 v4, 0x1

    .line 16
    invoke-direct {v1, v3, v4, v4}, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;-><init>(Ljava/lang/String;II)V

    .line 17
    .line 18
    .line 19
    sput-object v1, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzb:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 20
    .line 21
    new-instance v3, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 22
    .line 23
    const-string v5, "NV21"

    .line 24
    .line 25
    const/4 v6, 0x2

    .line 26
    invoke-direct {v3, v5, v6, v6}, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;-><init>(Ljava/lang/String;II)V

    .line 27
    .line 28
    .line 29
    sput-object v3, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzc:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 30
    .line 31
    new-instance v5, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 32
    .line 33
    const-string v7, "YV12"

    .line 34
    .line 35
    const/4 v8, 0x3

    .line 36
    invoke-direct {v5, v7, v8, v8}, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;-><init>(Ljava/lang/String;II)V

    .line 37
    .line 38
    .line 39
    sput-object v5, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzd:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 40
    .line 41
    new-instance v7, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 42
    .line 43
    const-string v9, "YUV_420_888"

    .line 44
    .line 45
    const/4 v10, 0x4

    .line 46
    const/4 v11, 0x7

    .line 47
    invoke-direct {v7, v9, v10, v11}, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;-><init>(Ljava/lang/String;II)V

    .line 48
    .line 49
    .line 50
    sput-object v7, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zze:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 51
    .line 52
    new-instance v9, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 53
    .line 54
    const-string v12, "JPEG"

    .line 55
    .line 56
    const/4 v13, 0x5

    .line 57
    const/16 v14, 0x8

    .line 58
    .line 59
    invoke-direct {v9, v12, v13, v14}, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;-><init>(Ljava/lang/String;II)V

    .line 60
    .line 61
    .line 62
    sput-object v9, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzf:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 63
    .line 64
    new-instance v12, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 65
    .line 66
    const-string v15, "BITMAP"

    .line 67
    .line 68
    const/4 v8, 0x6

    .line 69
    invoke-direct {v12, v15, v8, v10}, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;-><init>(Ljava/lang/String;II)V

    .line 70
    .line 71
    .line 72
    sput-object v12, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzg:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 73
    .line 74
    new-instance v15, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 75
    .line 76
    const-string v10, "CM_SAMPLE_BUFFER_REF"

    .line 77
    .line 78
    invoke-direct {v15, v10, v11, v13}, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;-><init>(Ljava/lang/String;II)V

    .line 79
    .line 80
    .line 81
    sput-object v15, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzh:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 82
    .line 83
    new-instance v10, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 84
    .line 85
    const-string v11, "UI_IMAGE"

    .line 86
    .line 87
    invoke-direct {v10, v11, v14, v8}, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;-><init>(Ljava/lang/String;II)V

    .line 88
    .line 89
    .line 90
    sput-object v10, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzi:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 91
    .line 92
    new-instance v11, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 93
    .line 94
    const-string v14, "CV_PIXEL_BUFFER_REF"

    .line 95
    .line 96
    const/16 v8, 0x9

    .line 97
    .line 98
    invoke-direct {v11, v14, v8, v8}, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;-><init>(Ljava/lang/String;II)V

    .line 99
    .line 100
    .line 101
    sput-object v11, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzj:Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 102
    .line 103
    const/16 v14, 0xa

    .line 104
    .line 105
    new-array v14, v14, [Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 106
    .line 107
    aput-object v0, v14, v2

    .line 108
    .line 109
    aput-object v1, v14, v4

    .line 110
    .line 111
    aput-object v3, v14, v6

    .line 112
    .line 113
    const/4 v0, 0x3

    .line 114
    aput-object v5, v14, v0

    .line 115
    .line 116
    const/4 v0, 0x4

    .line 117
    aput-object v7, v14, v0

    .line 118
    .line 119
    aput-object v9, v14, v13

    .line 120
    .line 121
    const/4 v0, 0x6

    .line 122
    aput-object v12, v14, v0

    .line 123
    .line 124
    const/4 v0, 0x7

    .line 125
    aput-object v15, v14, v0

    .line 126
    .line 127
    const/16 v0, 0x8

    .line 128
    .line 129
    aput-object v10, v14, v0

    .line 130
    .line 131
    aput-object v11, v14, v8

    .line 132
    .line 133
    sput-object v14, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzk:[Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 134
    .line 135
    return-void
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzl:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
.end method

.method public static values()[Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzk:[Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method


# virtual methods
.method public final zza()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/android/gms/internal/mlkit_vision_common/zzfg;->zzl:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.class final Lcom/google/android/gms/internal/ads/zzfvs;
.super Lcom/google/android/gms/internal/ads/zzfug;
.source "com.google.android.gms:play-services-ads@@22.4.0"


# static fields
.field static final zza:Lcom/google/android/gms/internal/ads/zzfug;


# instance fields
.field final transient zzb:[Ljava/lang/Object;

.field private final transient zzc:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/CheckForNull;
    .end annotation
.end field

.field private final transient zzd:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/ads/zzfvs;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    new-array v2, v1, [Ljava/lang/Object;

    .line 5
    .line 6
    const/4 v3, 0x0

    .line 7
    invoke-direct {v0, v3, v2, v1}, Lcom/google/android/gms/internal/ads/zzfvs;-><init>(Ljava/lang/Object;[Ljava/lang/Object;I)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/google/android/gms/internal/ads/zzfvs;->zza:Lcom/google/android/gms/internal/ads/zzfug;

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private constructor <init>(Ljava/lang/Object;[Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/CheckForNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzfug;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzc:Ljava/lang/Object;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzb:[Ljava/lang/Object;

    .line 7
    .line 8
    iput p3, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzd:I

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
.end method

.method static zzj(I[Ljava/lang/Object;Lcom/google/android/gms/internal/ads/zzfuf;)Lcom/google/android/gms/internal/ads/zzfvs;
    .locals 16

    .line 1
    move/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p2

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    sget-object v0, Lcom/google/android/gms/internal/ads/zzfvs;->zza:Lcom/google/android/gms/internal/ads/zzfug;

    .line 10
    .line 11
    check-cast v0, Lcom/google/android/gms/internal/ads/zzfvs;

    .line 12
    .line 13
    return-object v0

    .line 14
    :cond_0
    const/4 v3, 0x0

    .line 15
    const/4 v4, 0x0

    .line 16
    const/4 v5, 0x1

    .line 17
    if-ne v0, v5, :cond_1

    .line 18
    .line 19
    aget-object v0, v1, v4

    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 22
    .line 23
    .line 24
    aget-object v2, v1, v5

    .line 25
    .line 26
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 27
    .line 28
    .line 29
    invoke-static {v0, v2}, Lcom/google/android/gms/internal/ads/zzfta;->zzb(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 30
    .line 31
    .line 32
    new-instance v0, Lcom/google/android/gms/internal/ads/zzfvs;

    .line 33
    .line 34
    invoke-direct {v0, v3, v1, v5}, Lcom/google/android/gms/internal/ads/zzfvs;-><init>(Ljava/lang/Object;[Ljava/lang/Object;I)V

    .line 35
    .line 36
    .line 37
    return-object v0

    .line 38
    :cond_1
    array-length v6, v1

    .line 39
    shr-int/2addr v6, v5

    .line 40
    const-string v7, "index"

    .line 41
    .line 42
    invoke-static {v0, v6, v7}, Lcom/google/android/gms/internal/ads/zzfri;->zzb(IILjava/lang/String;)I

    .line 43
    .line 44
    .line 45
    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfui;->zzh(I)I

    .line 46
    .line 47
    .line 48
    move-result v6

    .line 49
    if-ne v0, v5, :cond_2

    .line 50
    .line 51
    aget-object v6, v1, v4

    .line 52
    .line 53
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 54
    .line 55
    .line 56
    aget-object v8, v1, v5

    .line 57
    .line 58
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 59
    .line 60
    .line 61
    invoke-static {v6, v8}, Lcom/google/android/gms/internal/ads/zzfta;->zzb(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 62
    .line 63
    .line 64
    :goto_0
    const/4 v5, 0x2

    .line 65
    const/4 v7, 0x1

    .line 66
    goto/16 :goto_d

    .line 67
    .line 68
    :cond_2
    add-int/lit8 v8, v6, -0x1

    .line 69
    .line 70
    const/16 v9, 0x80

    .line 71
    .line 72
    const/4 v10, 0x3

    .line 73
    const/4 v11, -0x1

    .line 74
    if-gt v6, v9, :cond_8

    .line 75
    .line 76
    new-array v6, v6, [B

    .line 77
    .line 78
    invoke-static {v6, v11}, Ljava/util/Arrays;->fill([BB)V

    .line 79
    .line 80
    .line 81
    const/4 v9, 0x0

    .line 82
    const/4 v11, 0x0

    .line 83
    :goto_1
    if-ge v9, v0, :cond_6

    .line 84
    .line 85
    add-int v12, v11, v11

    .line 86
    .line 87
    add-int v13, v9, v9

    .line 88
    .line 89
    aget-object v14, v1, v13

    .line 90
    .line 91
    invoke-virtual {v14}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 92
    .line 93
    .line 94
    xor-int/2addr v13, v5

    .line 95
    aget-object v13, v1, v13

    .line 96
    .line 97
    invoke-virtual {v13}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 98
    .line 99
    .line 100
    invoke-static {v14, v13}, Lcom/google/android/gms/internal/ads/zzfta;->zzb(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {v14}, Ljava/lang/Object;->hashCode()I

    .line 104
    .line 105
    .line 106
    move-result v15

    .line 107
    invoke-static {v15}, Lcom/google/android/gms/internal/ads/zzftv;->zza(I)I

    .line 108
    .line 109
    .line 110
    move-result v15

    .line 111
    :goto_2
    and-int/2addr v15, v8

    .line 112
    aget-byte v7, v6, v15

    .line 113
    .line 114
    const/16 v5, 0xff

    .line 115
    .line 116
    and-int/2addr v7, v5

    .line 117
    if-ne v7, v5, :cond_4

    .line 118
    .line 119
    int-to-byte v5, v12

    .line 120
    aput-byte v5, v6, v15

    .line 121
    .line 122
    if-ge v11, v9, :cond_3

    .line 123
    .line 124
    aput-object v14, v1, v12

    .line 125
    .line 126
    xor-int/lit8 v5, v12, 0x1

    .line 127
    .line 128
    aput-object v13, v1, v5

    .line 129
    .line 130
    :cond_3
    add-int/lit8 v11, v11, 0x1

    .line 131
    .line 132
    goto :goto_3

    .line 133
    :cond_4
    aget-object v5, v1, v7

    .line 134
    .line 135
    invoke-virtual {v14, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 136
    .line 137
    .line 138
    move-result v5

    .line 139
    if-eqz v5, :cond_5

    .line 140
    .line 141
    xor-int/lit8 v3, v7, 0x1

    .line 142
    .line 143
    new-instance v5, Lcom/google/android/gms/internal/ads/zzfue;

    .line 144
    .line 145
    aget-object v7, v1, v3

    .line 146
    .line 147
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 148
    .line 149
    .line 150
    invoke-direct {v5, v14, v13, v7}, Lcom/google/android/gms/internal/ads/zzfue;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 151
    .line 152
    .line 153
    aput-object v13, v1, v3

    .line 154
    .line 155
    move-object v3, v5

    .line 156
    :goto_3
    add-int/lit8 v9, v9, 0x1

    .line 157
    .line 158
    const/4 v5, 0x1

    .line 159
    goto :goto_1

    .line 160
    :cond_5
    add-int/lit8 v15, v15, 0x1

    .line 161
    .line 162
    const/4 v5, 0x1

    .line 163
    goto :goto_2

    .line 164
    :cond_6
    if-ne v11, v0, :cond_7

    .line 165
    .line 166
    move-object v3, v6

    .line 167
    goto :goto_0

    .line 168
    :cond_7
    new-array v5, v10, [Ljava/lang/Object;

    .line 169
    .line 170
    aput-object v6, v5, v4

    .line 171
    .line 172
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 173
    .line 174
    .line 175
    move-result-object v6

    .line 176
    const/4 v7, 0x1

    .line 177
    aput-object v6, v5, v7

    .line 178
    .line 179
    const/4 v6, 0x2

    .line 180
    aput-object v3, v5, v6

    .line 181
    .line 182
    :goto_4
    move-object v3, v5

    .line 183
    goto :goto_0

    .line 184
    :cond_8
    const v5, 0x8000

    .line 185
    .line 186
    .line 187
    if-gt v6, v5, :cond_e

    .line 188
    .line 189
    new-array v5, v6, [S

    .line 190
    .line 191
    invoke-static {v5, v11}, Ljava/util/Arrays;->fill([SS)V

    .line 192
    .line 193
    .line 194
    const/4 v6, 0x0

    .line 195
    const/4 v7, 0x0

    .line 196
    :goto_5
    if-ge v6, v0, :cond_c

    .line 197
    .line 198
    add-int v9, v7, v7

    .line 199
    .line 200
    add-int v11, v6, v6

    .line 201
    .line 202
    aget-object v12, v1, v11

    .line 203
    .line 204
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 205
    .line 206
    .line 207
    const/4 v13, 0x1

    .line 208
    xor-int/2addr v11, v13

    .line 209
    aget-object v11, v1, v11

    .line 210
    .line 211
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 212
    .line 213
    .line 214
    invoke-static {v12, v11}, Lcom/google/android/gms/internal/ads/zzfta;->zzb(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 215
    .line 216
    .line 217
    invoke-virtual {v12}, Ljava/lang/Object;->hashCode()I

    .line 218
    .line 219
    .line 220
    move-result v13

    .line 221
    invoke-static {v13}, Lcom/google/android/gms/internal/ads/zzftv;->zza(I)I

    .line 222
    .line 223
    .line 224
    move-result v13

    .line 225
    :goto_6
    and-int/2addr v13, v8

    .line 226
    aget-short v14, v5, v13

    .line 227
    .line 228
    int-to-char v14, v14

    .line 229
    const v15, 0xffff

    .line 230
    .line 231
    .line 232
    if-ne v14, v15, :cond_a

    .line 233
    .line 234
    int-to-short v14, v9

    .line 235
    aput-short v14, v5, v13

    .line 236
    .line 237
    if-ge v7, v6, :cond_9

    .line 238
    .line 239
    aput-object v12, v1, v9

    .line 240
    .line 241
    xor-int/lit8 v9, v9, 0x1

    .line 242
    .line 243
    aput-object v11, v1, v9

    .line 244
    .line 245
    :cond_9
    add-int/lit8 v7, v7, 0x1

    .line 246
    .line 247
    goto :goto_7

    .line 248
    :cond_a
    aget-object v15, v1, v14

    .line 249
    .line 250
    invoke-virtual {v12, v15}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 251
    .line 252
    .line 253
    move-result v15

    .line 254
    if-eqz v15, :cond_b

    .line 255
    .line 256
    xor-int/lit8 v3, v14, 0x1

    .line 257
    .line 258
    new-instance v9, Lcom/google/android/gms/internal/ads/zzfue;

    .line 259
    .line 260
    aget-object v13, v1, v3

    .line 261
    .line 262
    invoke-virtual {v13}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 263
    .line 264
    .line 265
    invoke-direct {v9, v12, v11, v13}, Lcom/google/android/gms/internal/ads/zzfue;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 266
    .line 267
    .line 268
    aput-object v11, v1, v3

    .line 269
    .line 270
    move-object v3, v9

    .line 271
    :goto_7
    add-int/lit8 v6, v6, 0x1

    .line 272
    .line 273
    goto :goto_5

    .line 274
    :cond_b
    add-int/lit8 v13, v13, 0x1

    .line 275
    .line 276
    goto :goto_6

    .line 277
    :cond_c
    if-ne v7, v0, :cond_d

    .line 278
    .line 279
    goto :goto_b

    .line 280
    :cond_d
    new-array v6, v10, [Ljava/lang/Object;

    .line 281
    .line 282
    aput-object v5, v6, v4

    .line 283
    .line 284
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 285
    .line 286
    .line 287
    move-result-object v5

    .line 288
    const/4 v7, 0x1

    .line 289
    aput-object v5, v6, v7

    .line 290
    .line 291
    const/4 v5, 0x2

    .line 292
    aput-object v3, v6, v5

    .line 293
    .line 294
    goto :goto_c

    .line 295
    :cond_e
    const/4 v7, 0x1

    .line 296
    new-array v5, v6, [I

    .line 297
    .line 298
    invoke-static {v5, v11}, Ljava/util/Arrays;->fill([II)V

    .line 299
    .line 300
    .line 301
    const/4 v6, 0x0

    .line 302
    const/4 v9, 0x0

    .line 303
    :goto_8
    if-ge v6, v0, :cond_12

    .line 304
    .line 305
    add-int v12, v9, v9

    .line 306
    .line 307
    add-int v13, v6, v6

    .line 308
    .line 309
    aget-object v14, v1, v13

    .line 310
    .line 311
    invoke-virtual {v14}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 312
    .line 313
    .line 314
    xor-int/2addr v13, v7

    .line 315
    aget-object v7, v1, v13

    .line 316
    .line 317
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 318
    .line 319
    .line 320
    invoke-static {v14, v7}, Lcom/google/android/gms/internal/ads/zzfta;->zzb(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 321
    .line 322
    .line 323
    invoke-virtual {v14}, Ljava/lang/Object;->hashCode()I

    .line 324
    .line 325
    .line 326
    move-result v13

    .line 327
    invoke-static {v13}, Lcom/google/android/gms/internal/ads/zzftv;->zza(I)I

    .line 328
    .line 329
    .line 330
    move-result v13

    .line 331
    :goto_9
    and-int/2addr v13, v8

    .line 332
    aget v15, v5, v13

    .line 333
    .line 334
    if-ne v15, v11, :cond_10

    .line 335
    .line 336
    aput v12, v5, v13

    .line 337
    .line 338
    if-ge v9, v6, :cond_f

    .line 339
    .line 340
    aput-object v14, v1, v12

    .line 341
    .line 342
    xor-int/lit8 v12, v12, 0x1

    .line 343
    .line 344
    aput-object v7, v1, v12

    .line 345
    .line 346
    :cond_f
    add-int/lit8 v9, v9, 0x1

    .line 347
    .line 348
    goto :goto_a

    .line 349
    :cond_10
    aget-object v11, v1, v15

    .line 350
    .line 351
    invoke-virtual {v14, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 352
    .line 353
    .line 354
    move-result v11

    .line 355
    if-eqz v11, :cond_11

    .line 356
    .line 357
    xor-int/lit8 v3, v15, 0x1

    .line 358
    .line 359
    new-instance v11, Lcom/google/android/gms/internal/ads/zzfue;

    .line 360
    .line 361
    aget-object v12, v1, v3

    .line 362
    .line 363
    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 364
    .line 365
    .line 366
    invoke-direct {v11, v14, v7, v12}, Lcom/google/android/gms/internal/ads/zzfue;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 367
    .line 368
    .line 369
    aput-object v7, v1, v3

    .line 370
    .line 371
    move-object v3, v11

    .line 372
    :goto_a
    add-int/lit8 v6, v6, 0x1

    .line 373
    .line 374
    const/4 v7, 0x1

    .line 375
    const/4 v11, -0x1

    .line 376
    goto :goto_8

    .line 377
    :cond_11
    add-int/lit8 v13, v13, 0x1

    .line 378
    .line 379
    const/4 v11, -0x1

    .line 380
    goto :goto_9

    .line 381
    :cond_12
    if-ne v9, v0, :cond_13

    .line 382
    .line 383
    :goto_b
    goto/16 :goto_4

    .line 384
    .line 385
    :cond_13
    new-array v6, v10, [Ljava/lang/Object;

    .line 386
    .line 387
    aput-object v5, v6, v4

    .line 388
    .line 389
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 390
    .line 391
    .line 392
    move-result-object v5

    .line 393
    const/4 v7, 0x1

    .line 394
    aput-object v5, v6, v7

    .line 395
    .line 396
    const/4 v5, 0x2

    .line 397
    aput-object v3, v6, v5

    .line 398
    .line 399
    :goto_c
    move-object v3, v6

    .line 400
    :goto_d
    nop

    .line 401
    instance-of v6, v3, [Ljava/lang/Object;

    .line 402
    .line 403
    if-eqz v6, :cond_15

    .line 404
    .line 405
    check-cast v3, [Ljava/lang/Object;

    .line 406
    .line 407
    aget-object v0, v3, v5

    .line 408
    .line 409
    check-cast v0, Lcom/google/android/gms/internal/ads/zzfue;

    .line 410
    .line 411
    if-eqz v2, :cond_14

    .line 412
    .line 413
    iput-object v0, v2, Lcom/google/android/gms/internal/ads/zzfuf;->zzc:Lcom/google/android/gms/internal/ads/zzfue;

    .line 414
    .line 415
    aget-object v0, v3, v4

    .line 416
    .line 417
    aget-object v2, v3, v7

    .line 418
    .line 419
    check-cast v2, Ljava/lang/Integer;

    .line 420
    .line 421
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 422
    .line 423
    .line 424
    move-result v2

    .line 425
    add-int v3, v2, v2

    .line 426
    .line 427
    invoke-static {v1, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 428
    .line 429
    .line 430
    move-result-object v1

    .line 431
    move-object v3, v0

    .line 432
    move v0, v2

    .line 433
    goto :goto_e

    .line 434
    :cond_14
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzfue;->zza()Ljava/lang/IllegalArgumentException;

    .line 435
    .line 436
    .line 437
    move-result-object v0

    .line 438
    throw v0

    .line 439
    :cond_15
    :goto_e
    new-instance v2, Lcom/google/android/gms/internal/ads/zzfvs;

    .line 440
    .line 441
    invoke-direct {v2, v3, v1, v0}, Lcom/google/android/gms/internal/ads/zzfvs;-><init>(Ljava/lang/Object;[Ljava/lang/Object;I)V

    .line 442
    .line 443
    .line 444
    return-object v2
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
.end method


# virtual methods
.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/CheckForNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/CheckForNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzc:Ljava/lang/Object;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzb:[Ljava/lang/Object;

    .line 4
    .line 5
    iget v2, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzd:I

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    if-nez p1, :cond_1

    .line 9
    .line 10
    :cond_0
    :goto_0
    move-object p1, v3

    .line 11
    goto/16 :goto_4

    .line 12
    .line 13
    :cond_1
    const/4 v4, 0x1

    .line 14
    if-ne v2, v4, :cond_2

    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    aget-object v0, v1, v0

    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    if-eqz p1, :cond_0

    .line 27
    .line 28
    aget-object p1, v1, v4

    .line 29
    .line 30
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 31
    .line 32
    .line 33
    goto/16 :goto_4

    .line 34
    .line 35
    :cond_2
    if-nez v0, :cond_3

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_3
    instance-of v2, v0, [B

    .line 39
    .line 40
    const/4 v5, -0x1

    .line 41
    if-eqz v2, :cond_6

    .line 42
    .line 43
    move-object v2, v0

    .line 44
    check-cast v2, [B

    .line 45
    .line 46
    array-length v0, v2

    .line 47
    add-int/lit8 v6, v0, -0x1

    .line 48
    .line 49
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzftv;->zza(I)I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    :goto_1
    and-int/2addr v0, v6

    .line 58
    aget-byte v5, v2, v0

    .line 59
    .line 60
    const/16 v7, 0xff

    .line 61
    .line 62
    and-int/2addr v5, v7

    .line 63
    if-ne v5, v7, :cond_4

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_4
    aget-object v7, v1, v5

    .line 67
    .line 68
    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    move-result v7

    .line 72
    if-eqz v7, :cond_5

    .line 73
    .line 74
    xor-int/lit8 p1, v5, 0x1

    .line 75
    .line 76
    aget-object p1, v1, p1

    .line 77
    .line 78
    goto :goto_4

    .line 79
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_6
    instance-of v2, v0, [S

    .line 83
    .line 84
    if-eqz v2, :cond_9

    .line 85
    .line 86
    move-object v2, v0

    .line 87
    check-cast v2, [S

    .line 88
    .line 89
    array-length v0, v2

    .line 90
    add-int/lit8 v6, v0, -0x1

    .line 91
    .line 92
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    .line 93
    .line 94
    .line 95
    move-result v0

    .line 96
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzftv;->zza(I)I

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    :goto_2
    and-int/2addr v0, v6

    .line 101
    aget-short v5, v2, v0

    .line 102
    .line 103
    int-to-char v5, v5

    .line 104
    const v7, 0xffff

    .line 105
    .line 106
    .line 107
    if-ne v5, v7, :cond_7

    .line 108
    .line 109
    goto :goto_0

    .line 110
    :cond_7
    aget-object v7, v1, v5

    .line 111
    .line 112
    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 113
    .line 114
    .line 115
    move-result v7

    .line 116
    if-eqz v7, :cond_8

    .line 117
    .line 118
    xor-int/lit8 p1, v5, 0x1

    .line 119
    .line 120
    aget-object p1, v1, p1

    .line 121
    .line 122
    goto :goto_4

    .line 123
    :cond_8
    add-int/lit8 v0, v0, 0x1

    .line 124
    .line 125
    goto :goto_2

    .line 126
    :cond_9
    check-cast v0, [I

    .line 127
    .line 128
    array-length v2, v0

    .line 129
    add-int/2addr v2, v5

    .line 130
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    .line 131
    .line 132
    .line 133
    move-result v6

    .line 134
    invoke-static {v6}, Lcom/google/android/gms/internal/ads/zzftv;->zza(I)I

    .line 135
    .line 136
    .line 137
    move-result v6

    .line 138
    :goto_3
    and-int/2addr v6, v2

    .line 139
    aget v7, v0, v6

    .line 140
    .line 141
    if-ne v7, v5, :cond_a

    .line 142
    .line 143
    goto/16 :goto_0

    .line 144
    .line 145
    :cond_a
    aget-object v8, v1, v7

    .line 146
    .line 147
    invoke-virtual {p1, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 148
    .line 149
    .line 150
    move-result v8

    .line 151
    if-eqz v8, :cond_c

    .line 152
    .line 153
    xor-int/lit8 p1, v7, 0x1

    .line 154
    .line 155
    aget-object p1, v1, p1

    .line 156
    .line 157
    :goto_4
    if-nez p1, :cond_b

    .line 158
    .line 159
    return-object v3

    .line 160
    :cond_b
    return-object p1

    .line 161
    :cond_c
    add-int/lit8 v6, v6, 0x1

    .line 162
    .line 163
    goto :goto_3
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method public final size()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzd:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method final zza()Lcom/google/android/gms/internal/ads/zzfty;
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/ads/zzfvr;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzb:[Ljava/lang/Object;

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    iget v3, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzd:I

    .line 7
    .line 8
    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/internal/ads/zzfvr;-><init>([Ljava/lang/Object;II)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method final zzf()Lcom/google/android/gms/internal/ads/zzfui;
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/ads/zzfvp;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzb:[Ljava/lang/Object;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    iget v3, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzd:I

    .line 7
    .line 8
    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/internal/ads/zzfvp;-><init>(Lcom/google/android/gms/internal/ads/zzfug;[Ljava/lang/Object;II)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method final zzg()Lcom/google/android/gms/internal/ads/zzfui;
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/ads/zzfvr;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzb:[Ljava/lang/Object;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    iget v3, p0, Lcom/google/android/gms/internal/ads/zzfvs;->zzd:I

    .line 7
    .line 8
    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/internal/ads/zzfvr;-><init>([Ljava/lang/Object;II)V

    .line 9
    .line 10
    .line 11
    new-instance v1, Lcom/google/android/gms/internal/ads/zzfvq;

    .line 12
    .line 13
    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/internal/ads/zzfvq;-><init>(Lcom/google/android/gms/internal/ads/zzfug;Lcom/google/android/gms/internal/ads/zzfud;)V

    .line 14
    .line 15
    .line 16
    return-object v1
    .line 17
.end method

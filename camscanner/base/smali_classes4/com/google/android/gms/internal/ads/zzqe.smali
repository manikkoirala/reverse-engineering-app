.class public final Lcom/google/android/gms/internal/ads/zzqe;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-ads@@22.4.0"

# interfaces
.implements Lcom/google/android/gms/internal/ads/zzpe;


# static fields
.field private static final zza:Ljava/lang/Object;

.field private static zzb:Ljava/util/concurrent/ExecutorService;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "releaseExecutorLock"
    .end annotation

    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private static zzc:I
    .annotation build Landroidx/annotation/GuardedBy;
        value = "releaseExecutorLock"
    .end annotation
.end field


# instance fields
.field private zzA:J

.field private zzB:J

.field private zzC:J

.field private zzD:J

.field private zzE:I

.field private zzF:Z

.field private zzG:Z

.field private zzH:J

.field private zzI:F

.field private zzJ:Ljava/nio/ByteBuffer;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private zzK:I

.field private zzL:Ljava/nio/ByteBuffer;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private zzM:[B

.field private zzN:I

.field private zzO:Z

.field private zzP:Z

.field private zzQ:Z

.field private zzR:Z

.field private zzS:I

.field private zzT:Lcom/google/android/gms/internal/ads/zzl;

.field private zzU:Lcom/google/android/gms/internal/ads/zzpq;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private zzV:J

.field private zzW:Z

.field private zzX:Z

.field private final zzY:Lcom/google/android/gms/internal/ads/zzpu;

.field private final zzZ:Lcom/google/android/gms/internal/ads/zzpm;

.field private final zzd:Lcom/google/android/gms/internal/ads/zzpj;

.field private final zze:Lcom/google/android/gms/internal/ads/zzqo;

.field private final zzf:Lcom/google/android/gms/internal/ads/zzfud;

.field private final zzg:Lcom/google/android/gms/internal/ads/zzfud;

.field private final zzh:Lcom/google/android/gms/internal/ads/zzeb;

.field private final zzi:Lcom/google/android/gms/internal/ads/zzpi;

.field private final zzj:Ljava/util/ArrayDeque;

.field private zzk:Lcom/google/android/gms/internal/ads/zzqc;

.field private final zzl:Lcom/google/android/gms/internal/ads/zzpx;

.field private final zzm:Lcom/google/android/gms/internal/ads/zzpx;

.field private final zzn:Lcom/google/android/gms/internal/ads/zzpr;

.field private zzo:Lcom/google/android/gms/internal/ads/zzoh;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private zzp:Lcom/google/android/gms/internal/ads/zzpb;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private zzq:Lcom/google/android/gms/internal/ads/zzpt;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private zzr:Lcom/google/android/gms/internal/ads/zzpt;

.field private zzs:Lcom/google/android/gms/internal/ads/zzdo;

.field private zzt:Landroid/media/AudioTrack;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private zzu:Lcom/google/android/gms/internal/ads/zzoj;

.field private zzv:Lcom/google/android/gms/internal/ads/zzk;

.field private zzw:Lcom/google/android/gms/internal/ads/zzpw;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private zzx:Lcom/google/android/gms/internal/ads/zzpw;

.field private zzy:Lcom/google/android/gms/internal/ads/zzch;

.field private zzz:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/Object;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/google/android/gms/internal/ads/zzqe;->zza:Ljava/lang/Object;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/internal/ads/zzps;Lcom/google/android/gms/internal/ads/zzqd;)V
    .locals 8

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {p1}, Lcom/google/android/gms/internal/ads/zzps;->zza(Lcom/google/android/gms/internal/ads/zzps;)Lcom/google/android/gms/internal/ads/zzoj;

    .line 5
    .line 6
    .line 7
    move-result-object p2

    .line 8
    iput-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzu:Lcom/google/android/gms/internal/ads/zzoj;

    .line 9
    .line 10
    invoke-static {p1}, Lcom/google/android/gms/internal/ads/zzps;->zzf(Lcom/google/android/gms/internal/ads/zzps;)Lcom/google/android/gms/internal/ads/zzpu;

    .line 11
    .line 12
    .line 13
    move-result-object p2

    .line 14
    iput-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzY:Lcom/google/android/gms/internal/ads/zzpu;

    .line 15
    .line 16
    sget p2, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    .line 17
    .line 18
    invoke-static {p1}, Lcom/google/android/gms/internal/ads/zzps;->zzb(Lcom/google/android/gms/internal/ads/zzps;)Lcom/google/android/gms/internal/ads/zzpr;

    .line 19
    .line 20
    .line 21
    move-result-object p2

    .line 22
    iput-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzn:Lcom/google/android/gms/internal/ads/zzpr;

    .line 23
    .line 24
    invoke-static {p1}, Lcom/google/android/gms/internal/ads/zzps;->zzg(Lcom/google/android/gms/internal/ads/zzps;)Lcom/google/android/gms/internal/ads/zzpm;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 29
    .line 30
    .line 31
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzZ:Lcom/google/android/gms/internal/ads/zzpm;

    .line 32
    .line 33
    new-instance p1, Lcom/google/android/gms/internal/ads/zzeb;

    .line 34
    .line 35
    sget-object p2, Lcom/google/android/gms/internal/ads/zzdz;->zza:Lcom/google/android/gms/internal/ads/zzdz;

    .line 36
    .line 37
    invoke-direct {p1, p2}, Lcom/google/android/gms/internal/ads/zzeb;-><init>(Lcom/google/android/gms/internal/ads/zzdz;)V

    .line 38
    .line 39
    .line 40
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzh:Lcom/google/android/gms/internal/ads/zzeb;

    .line 41
    .line 42
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ads/zzeb;->zze()Z

    .line 43
    .line 44
    .line 45
    new-instance p1, Lcom/google/android/gms/internal/ads/zzpi;

    .line 46
    .line 47
    new-instance p2, Lcom/google/android/gms/internal/ads/zzpz;

    .line 48
    .line 49
    const/4 v0, 0x0

    .line 50
    invoke-direct {p2, p0, v0}, Lcom/google/android/gms/internal/ads/zzpz;-><init>(Lcom/google/android/gms/internal/ads/zzqe;Lcom/google/android/gms/internal/ads/zzpy;)V

    .line 51
    .line 52
    .line 53
    invoke-direct {p1, p2}, Lcom/google/android/gms/internal/ads/zzpi;-><init>(Lcom/google/android/gms/internal/ads/zzph;)V

    .line 54
    .line 55
    .line 56
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 57
    .line 58
    new-instance p1, Lcom/google/android/gms/internal/ads/zzpj;

    .line 59
    .line 60
    invoke-direct {p1}, Lcom/google/android/gms/internal/ads/zzpj;-><init>()V

    .line 61
    .line 62
    .line 63
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzd:Lcom/google/android/gms/internal/ads/zzpj;

    .line 64
    .line 65
    new-instance p2, Lcom/google/android/gms/internal/ads/zzqo;

    .line 66
    .line 67
    invoke-direct {p2}, Lcom/google/android/gms/internal/ads/zzqo;-><init>()V

    .line 68
    .line 69
    .line 70
    iput-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zze:Lcom/google/android/gms/internal/ads/zzqo;

    .line 71
    .line 72
    new-instance v0, Lcom/google/android/gms/internal/ads/zzdv;

    .line 73
    .line 74
    invoke-direct {v0}, Lcom/google/android/gms/internal/ads/zzdv;-><init>()V

    .line 75
    .line 76
    .line 77
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/internal/ads/zzfud;->zzo(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzf:Lcom/google/android/gms/internal/ads/zzfud;

    .line 82
    .line 83
    new-instance p1, Lcom/google/android/gms/internal/ads/zzqn;

    .line 84
    .line 85
    invoke-direct {p1}, Lcom/google/android/gms/internal/ads/zzqn;-><init>()V

    .line 86
    .line 87
    .line 88
    invoke-static {p1}, Lcom/google/android/gms/internal/ads/zzfud;->zzm(Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzg:Lcom/google/android/gms/internal/ads/zzfud;

    .line 93
    .line 94
    const/high16 p1, 0x3f800000    # 1.0f

    .line 95
    .line 96
    iput p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzI:F

    .line 97
    .line 98
    sget-object p1, Lcom/google/android/gms/internal/ads/zzk;->zza:Lcom/google/android/gms/internal/ads/zzk;

    .line 99
    .line 100
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzv:Lcom/google/android/gms/internal/ads/zzk;

    .line 101
    .line 102
    const/4 p1, 0x0

    .line 103
    iput p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzS:I

    .line 104
    .line 105
    new-instance p2, Lcom/google/android/gms/internal/ads/zzl;

    .line 106
    .line 107
    const/4 v0, 0x0

    .line 108
    invoke-direct {p2, p1, v0}, Lcom/google/android/gms/internal/ads/zzl;-><init>(IF)V

    .line 109
    .line 110
    .line 111
    iput-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzT:Lcom/google/android/gms/internal/ads/zzl;

    .line 112
    .line 113
    new-instance p2, Lcom/google/android/gms/internal/ads/zzpw;

    .line 114
    .line 115
    sget-object v0, Lcom/google/android/gms/internal/ads/zzch;->zza:Lcom/google/android/gms/internal/ads/zzch;

    .line 116
    .line 117
    const-wide/16 v3, 0x0

    .line 118
    .line 119
    const-wide/16 v5, 0x0

    .line 120
    .line 121
    const/4 v7, 0x0

    .line 122
    move-object v1, p2

    .line 123
    move-object v2, v0

    .line 124
    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/internal/ads/zzpw;-><init>(Lcom/google/android/gms/internal/ads/zzch;JJLcom/google/android/gms/internal/ads/zzpv;)V

    .line 125
    .line 126
    .line 127
    iput-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzx:Lcom/google/android/gms/internal/ads/zzpw;

    .line 128
    .line 129
    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzy:Lcom/google/android/gms/internal/ads/zzch;

    .line 130
    .line 131
    iput-boolean p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzz:Z

    .line 132
    .line 133
    new-instance p1, Ljava/util/ArrayDeque;

    .line 134
    .line 135
    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    .line 136
    .line 137
    .line 138
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzj:Ljava/util/ArrayDeque;

    .line 139
    .line 140
    new-instance p1, Lcom/google/android/gms/internal/ads/zzpx;

    .line 141
    .line 142
    const-wide/16 v0, 0x64

    .line 143
    .line 144
    invoke-direct {p1, v0, v1}, Lcom/google/android/gms/internal/ads/zzpx;-><init>(J)V

    .line 145
    .line 146
    .line 147
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzl:Lcom/google/android/gms/internal/ads/zzpx;

    .line 148
    .line 149
    new-instance p1, Lcom/google/android/gms/internal/ads/zzpx;

    .line 150
    .line 151
    invoke-direct {p1, v0, v1}, Lcom/google/android/gms/internal/ads/zzpx;-><init>(J)V

    .line 152
    .line 153
    .line 154
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzm:Lcom/google/android/gms/internal/ads/zzpx;

    .line 155
    .line 156
    return-void
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
.end method

.method static bridge synthetic zzA(Lcom/google/android/gms/internal/ads/zzqe;)J
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzG()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    return-wide v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic zzB(Lcom/google/android/gms/internal/ads/zzqe;)J
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzH()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    return-wide v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic zzC(Lcom/google/android/gms/internal/ads/zzqe;)Landroid/media/AudioTrack;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic zzD(Lcom/google/android/gms/internal/ads/zzqe;)Lcom/google/android/gms/internal/ads/zzpb;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzp:Lcom/google/android/gms/internal/ads/zzpb;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic zzE(Landroid/media/AudioTrack;Lcom/google/android/gms/internal/ads/zzeb;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    invoke-virtual {p0}, Landroid/media/AudioTrack;->flush()V

    .line 3
    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/media/AudioTrack;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ads/zzeb;->zze()Z

    .line 9
    .line 10
    .line 11
    sget-object p0, Lcom/google/android/gms/internal/ads/zzqe;->zza:Ljava/lang/Object;

    .line 12
    .line 13
    monitor-enter p0

    .line 14
    :try_start_1
    sget p1, Lcom/google/android/gms/internal/ads/zzqe;->zzc:I

    .line 15
    .line 16
    add-int/lit8 p1, p1, -0x1

    .line 17
    .line 18
    sput p1, Lcom/google/android/gms/internal/ads/zzqe;->zzc:I

    .line 19
    .line 20
    if-nez p1, :cond_0

    .line 21
    .line 22
    sget-object p1, Lcom/google/android/gms/internal/ads/zzqe;->zzb:Ljava/util/concurrent/ExecutorService;

    .line 23
    .line 24
    invoke-interface {p1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 25
    .line 26
    .line 27
    sput-object v0, Lcom/google/android/gms/internal/ads/zzqe;->zzb:Ljava/util/concurrent/ExecutorService;

    .line 28
    .line 29
    :cond_0
    monitor-exit p0

    .line 30
    return-void

    .line 31
    :catchall_0
    move-exception p1

    .line 32
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 33
    throw p1

    .line 34
    :catchall_1
    move-exception p0

    .line 35
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ads/zzeb;->zze()Z

    .line 36
    .line 37
    .line 38
    sget-object p1, Lcom/google/android/gms/internal/ads/zzqe;->zza:Ljava/lang/Object;

    .line 39
    .line 40
    monitor-enter p1

    .line 41
    :try_start_2
    sget v1, Lcom/google/android/gms/internal/ads/zzqe;->zzc:I

    .line 42
    .line 43
    add-int/lit8 v1, v1, -0x1

    .line 44
    .line 45
    sput v1, Lcom/google/android/gms/internal/ads/zzqe;->zzc:I

    .line 46
    .line 47
    if-nez v1, :cond_1

    .line 48
    .line 49
    sget-object v1, Lcom/google/android/gms/internal/ads/zzqe;->zzb:Ljava/util/concurrent/ExecutorService;

    .line 50
    .line 51
    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 52
    .line 53
    .line 54
    sput-object v0, Lcom/google/android/gms/internal/ads/zzqe;->zzb:Ljava/util/concurrent/ExecutorService;

    .line 55
    .line 56
    :cond_1
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 57
    throw p0

    .line 58
    :catchall_2
    move-exception p0

    .line 59
    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 60
    throw p0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
.end method

.method static bridge synthetic zzF(Lcom/google/android/gms/internal/ads/zzqe;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzQ:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final zzG()J
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 2
    .line 3
    iget v1, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzc:I

    .line 4
    .line 5
    if-nez v1, :cond_0

    .line 6
    .line 7
    iget-wide v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzA:J

    .line 8
    .line 9
    iget v0, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzb:I

    .line 10
    .line 11
    int-to-long v3, v0

    .line 12
    div-long/2addr v1, v3

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget-wide v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzB:J

    .line 15
    .line 16
    :goto_0
    return-wide v1
    .line 17
.end method

.method private final zzH()J
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 2
    .line 3
    iget v1, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzc:I

    .line 4
    .line 5
    if-nez v1, :cond_0

    .line 6
    .line 7
    iget-wide v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzC:J

    .line 8
    .line 9
    iget v0, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzd:I

    .line 10
    .line 11
    int-to-long v3, v0

    .line 12
    div-long/2addr v1, v3

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget-wide v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzD:J

    .line 15
    .line 16
    :goto_0
    return-wide v1
    .line 17
.end method

.method private final zzI(Lcom/google/android/gms/internal/ads/zzpt;)Landroid/media/AudioTrack;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzpa;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzv:Lcom/google/android/gms/internal/ads/zzk;

    .line 2
    .line 3
    iget v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzS:I

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-virtual {p1, v2, v0, v1}, Lcom/google/android/gms/internal/ads/zzpt;->zzb(ZLcom/google/android/gms/internal/ads/zzk;I)Landroid/media/AudioTrack;

    .line 7
    .line 8
    .line 9
    move-result-object p1
    :try_end_0
    .catch Lcom/google/android/gms/internal/ads/zzpa; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    return-object p1

    .line 11
    :catch_0
    move-exception p1

    .line 12
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzp:Lcom/google/android/gms/internal/ads/zzpb;

    .line 13
    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/ads/zzpb;->zza(Ljava/lang/Exception;)V

    .line 18
    .line 19
    .line 20
    :goto_0
    throw p1
.end method

.method private final zzJ(J)V
    .locals 9

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzT()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzY:Lcom/google/android/gms/internal/ads/zzpu;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzy:Lcom/google/android/gms/internal/ads/zzch;

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzpu;->zzc(Lcom/google/android/gms/internal/ads/zzch;)Lcom/google/android/gms/internal/ads/zzch;

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    sget-object v1, Lcom/google/android/gms/internal/ads/zzch;->zza:Lcom/google/android/gms/internal/ads/zzch;

    .line 16
    .line 17
    :goto_0
    move-object v3, v1

    .line 18
    iput-object v3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzy:Lcom/google/android/gms/internal/ads/zzch;

    .line 19
    .line 20
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzT()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzY:Lcom/google/android/gms/internal/ads/zzpu;

    .line 27
    .line 28
    iget-boolean v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzz:Z

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzpu;->zzd(Z)Z

    .line 31
    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_1
    const/4 v1, 0x0

    .line 35
    :goto_1
    iput-boolean v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzz:Z

    .line 36
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzj:Ljava/util/ArrayDeque;

    .line 38
    .line 39
    new-instance v1, Lcom/google/android/gms/internal/ads/zzpw;

    .line 40
    .line 41
    const-wide/16 v4, 0x0

    .line 42
    .line 43
    invoke-static {v4, v5, p1, p2}, Ljava/lang/Math;->max(JJ)J

    .line 44
    .line 45
    .line 46
    move-result-wide v4

    .line 47
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 48
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzH()J

    .line 50
    .line 51
    .line 52
    move-result-wide v6

    .line 53
    invoke-virtual {p1, v6, v7}, Lcom/google/android/gms/internal/ads/zzpt;->zza(J)J

    .line 54
    .line 55
    .line 56
    move-result-wide v6

    .line 57
    const/4 v8, 0x0

    .line 58
    move-object v2, v1

    .line 59
    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/internal/ads/zzpw;-><init>(Lcom/google/android/gms/internal/ads/zzch;JJLcom/google/android/gms/internal/ads/zzpv;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 63
    .line 64
    .line 65
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzO()V

    .line 66
    .line 67
    .line 68
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzp:Lcom/google/android/gms/internal/ads/zzpb;

    .line 69
    .line 70
    if-eqz p1, :cond_2

    .line 71
    .line 72
    iget-boolean p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzz:Z

    .line 73
    .line 74
    check-cast p1, Lcom/google/android/gms/internal/ads/zzqj;

    .line 75
    .line 76
    iget-object p1, p1, Lcom/google/android/gms/internal/ads/zzqj;->zza:Lcom/google/android/gms/internal/ads/zzqk;

    .line 77
    .line 78
    invoke-static {p1}, Lcom/google/android/gms/internal/ads/zzqk;->zzaa(Lcom/google/android/gms/internal/ads/zzqk;)Lcom/google/android/gms/internal/ads/zzox;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/ads/zzox;->zzs(Z)V

    .line 83
    .line 84
    .line 85
    :cond_2
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method private final zzK()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzP:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzP:Z

    .line 7
    .line 8
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzH()J

    .line 11
    .line 12
    .line 13
    move-result-wide v1

    .line 14
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/ads/zzpi;->zzc(J)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method private final zzL(J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzpd;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzs:Lcom/google/android/gms/internal/ads/zzdo;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzdo;->zzh()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_4

    .line 8
    .line 9
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzs:Lcom/google/android/gms/internal/ads/zzdo;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzdo;->zzg()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_3

    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzs:Lcom/google/android/gms/internal/ads/zzdo;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzdo;->zzb()Ljava/nio/ByteBuffer;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v0}, Ljava/nio/Buffer;->hasRemaining()Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-eqz v1, :cond_1

    .line 28
    .line 29
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/internal/ads/zzqe;->zzP(Ljava/nio/ByteBuffer;J)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0}, Ljava/nio/Buffer;->hasRemaining()Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_0

    .line 37
    .line 38
    return-void

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzJ:Ljava/nio/ByteBuffer;

    .line 40
    .line 41
    if-eqz v0, :cond_3

    .line 42
    .line 43
    invoke-virtual {v0}, Ljava/nio/Buffer;->hasRemaining()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-nez v0, :cond_2

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzs:Lcom/google/android/gms/internal/ads/zzdo;

    .line 51
    .line 52
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzJ:Ljava/nio/ByteBuffer;

    .line 53
    .line 54
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzdo;->zze(Ljava/nio/ByteBuffer;)V

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_3
    :goto_1
    return-void

    .line 59
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzJ:Ljava/nio/ByteBuffer;

    .line 60
    .line 61
    if-nez v0, :cond_5

    .line 62
    .line 63
    sget-object v0, Lcom/google/android/gms/internal/ads/zzdr;->zza:Ljava/nio/ByteBuffer;

    .line 64
    .line 65
    :cond_5
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/internal/ads/zzqe;->zzP(Ljava/nio/ByteBuffer;J)V

    .line 66
    .line 67
    .line 68
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method private final zzM(Lcom/google/android/gms/internal/ads/zzch;)V
    .locals 8

    .line 1
    new-instance v7, Lcom/google/android/gms/internal/ads/zzpw;

    .line 2
    .line 3
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    const/4 v6, 0x0

    .line 9
    move-object v0, v7

    .line 10
    move-object v1, p1

    .line 11
    move-wide v2, v4

    .line 12
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/ads/zzpw;-><init>(Lcom/google/android/gms/internal/ads/zzch;JJLcom/google/android/gms/internal/ads/zzpv;)V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzR()Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    iput-object v7, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzw:Lcom/google/android/gms/internal/ads/zzpw;

    .line 22
    .line 23
    return-void

    .line 24
    :cond_0
    iput-object v7, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzx:Lcom/google/android/gms/internal/ads/zzpw;

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final zzN()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzR()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    sget v0, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    .line 9
    .line 10
    const/16 v1, 0x15

    .line 11
    .line 12
    if-lt v0, v1, :cond_1

    .line 13
    .line 14
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 15
    .line 16
    iget v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzI:F

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/media/AudioTrack;->setVolume(F)I

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 23
    .line 24
    iget v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzI:F

    .line 25
    .line 26
    invoke-virtual {v0, v1, v1}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method private final zzO()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzi:Lcom/google/android/gms/internal/ads/zzdo;

    .line 4
    .line 5
    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzs:Lcom/google/android/gms/internal/ads/zzdo;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzdo;->zzc()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private final zzP(Ljava/nio/ByteBuffer;J)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzpd;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/nio/Buffer;->hasRemaining()Z

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    if-nez p2, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzL:Ljava/nio/ByteBuffer;

    .line 9
    .line 10
    const/16 p3, 0x15

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    const/4 v1, 0x0

    .line 14
    if-eqz p2, :cond_2

    .line 15
    .line 16
    if-ne p2, p1, :cond_1

    .line 17
    .line 18
    const/4 p2, 0x1

    .line 19
    goto :goto_0

    .line 20
    :cond_1
    const/4 p2, 0x0

    .line 21
    :goto_0
    invoke-static {p2}, Lcom/google/android/gms/internal/ads/zzdy;->zzd(Z)V

    .line 22
    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_2
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzL:Ljava/nio/ByteBuffer;

    .line 26
    .line 27
    sget p2, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    .line 28
    .line 29
    if-ge p2, p3, :cond_5

    .line 30
    .line 31
    invoke-virtual {p1}, Ljava/nio/Buffer;->remaining()I

    .line 32
    .line 33
    .line 34
    move-result p2

    .line 35
    iget-object v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzM:[B

    .line 36
    .line 37
    if-eqz v2, :cond_3

    .line 38
    .line 39
    array-length v2, v2

    .line 40
    if-ge v2, p2, :cond_4

    .line 41
    .line 42
    :cond_3
    new-array v2, p2, [B

    .line 43
    .line 44
    iput-object v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzM:[B

    .line 45
    .line 46
    :cond_4
    invoke-virtual {p1}, Ljava/nio/Buffer;->position()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    iget-object v3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzM:[B

    .line 51
    .line 52
    invoke-virtual {p1, v3, v1, p2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 56
    .line 57
    .line 58
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzN:I

    .line 59
    .line 60
    :cond_5
    :goto_1
    invoke-virtual {p1}, Ljava/nio/Buffer;->remaining()I

    .line 61
    .line 62
    .line 63
    move-result p2

    .line 64
    sget v2, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    .line 65
    .line 66
    if-ge v2, p3, :cond_7

    .line 67
    .line 68
    iget-object p3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 69
    .line 70
    iget-wide v3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzC:J

    .line 71
    .line 72
    invoke-virtual {p3, v3, v4}, Lcom/google/android/gms/internal/ads/zzpi;->zza(J)I

    .line 73
    .line 74
    .line 75
    move-result p3

    .line 76
    if-lez p3, :cond_6

    .line 77
    .line 78
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    .line 79
    .line 80
    .line 81
    move-result p3

    .line 82
    iget-object v3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 83
    .line 84
    iget-object v4, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzM:[B

    .line 85
    .line 86
    iget v5, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzN:I

    .line 87
    .line 88
    invoke-virtual {v3, v4, v5, p3}, Landroid/media/AudioTrack;->write([BII)I

    .line 89
    .line 90
    .line 91
    move-result p3

    .line 92
    if-lez p3, :cond_8

    .line 93
    .line 94
    iget v3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzN:I

    .line 95
    .line 96
    add-int/2addr v3, p3

    .line 97
    iput v3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzN:I

    .line 98
    .line 99
    invoke-virtual {p1}, Ljava/nio/Buffer;->position()I

    .line 100
    .line 101
    .line 102
    move-result v3

    .line 103
    add-int/2addr v3, p3

    .line 104
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 105
    .line 106
    .line 107
    goto :goto_2

    .line 108
    :cond_6
    const/4 p3, 0x0

    .line 109
    goto :goto_2

    .line 110
    :cond_7
    iget-object p3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 111
    .line 112
    invoke-virtual {p3, p1, p2, v0}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    .line 113
    .line 114
    .line 115
    move-result p3

    .line 116
    :cond_8
    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 117
    .line 118
    .line 119
    move-result-wide v3

    .line 120
    iput-wide v3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzV:J

    .line 121
    .line 122
    const-wide/16 v3, 0x0

    .line 123
    .line 124
    if-gez p3, :cond_e

    .line 125
    .line 126
    const/16 p1, 0x18

    .line 127
    .line 128
    if-lt v2, p1, :cond_9

    .line 129
    .line 130
    const/4 p1, -0x6

    .line 131
    if-eq p3, p1, :cond_a

    .line 132
    .line 133
    :cond_9
    const/16 p1, -0x20

    .line 134
    .line 135
    if-ne p3, p1, :cond_b

    .line 136
    .line 137
    :cond_a
    iget-wide p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzD:J

    .line 138
    .line 139
    cmp-long v2, p1, v3

    .line 140
    .line 141
    if-lez v2, :cond_b

    .line 142
    .line 143
    goto :goto_3

    .line 144
    :cond_b
    const/4 v0, 0x0

    .line 145
    :goto_3
    new-instance p1, Lcom/google/android/gms/internal/ads/zzpd;

    .line 146
    .line 147
    iget-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 148
    .line 149
    iget-object p2, p2, Lcom/google/android/gms/internal/ads/zzpt;->zza:Lcom/google/android/gms/internal/ads/zzam;

    .line 150
    .line 151
    invoke-direct {p1, p3, p2, v0}, Lcom/google/android/gms/internal/ads/zzpd;-><init>(ILcom/google/android/gms/internal/ads/zzam;Z)V

    .line 152
    .line 153
    .line 154
    iget-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzp:Lcom/google/android/gms/internal/ads/zzpb;

    .line 155
    .line 156
    if-eqz p2, :cond_c

    .line 157
    .line 158
    invoke-interface {p2, p1}, Lcom/google/android/gms/internal/ads/zzpb;->zza(Ljava/lang/Exception;)V

    .line 159
    .line 160
    .line 161
    :cond_c
    iget-boolean p2, p1, Lcom/google/android/gms/internal/ads/zzpd;->zzb:Z

    .line 162
    .line 163
    if-nez p2, :cond_d

    .line 164
    .line 165
    iget-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzm:Lcom/google/android/gms/internal/ads/zzpx;

    .line 166
    .line 167
    invoke-virtual {p2, p1}, Lcom/google/android/gms/internal/ads/zzpx;->zzb(Ljava/lang/Exception;)V

    .line 168
    .line 169
    .line 170
    return-void

    .line 171
    :cond_d
    sget-object p2, Lcom/google/android/gms/internal/ads/zzoj;->zza:Lcom/google/android/gms/internal/ads/zzoj;

    .line 172
    .line 173
    iput-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzu:Lcom/google/android/gms/internal/ads/zzoj;

    .line 174
    .line 175
    throw p1

    .line 176
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzm:Lcom/google/android/gms/internal/ads/zzpx;

    .line 177
    .line 178
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzpx;->zza()V

    .line 179
    .line 180
    .line 181
    iget-object v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 182
    .line 183
    invoke-static {v2}, Lcom/google/android/gms/internal/ads/zzqe;->zzS(Landroid/media/AudioTrack;)Z

    .line 184
    .line 185
    .line 186
    move-result v2

    .line 187
    if-eqz v2, :cond_10

    .line 188
    .line 189
    iget-wide v5, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzD:J

    .line 190
    .line 191
    cmp-long v2, v5, v3

    .line 192
    .line 193
    if-lez v2, :cond_f

    .line 194
    .line 195
    iput-boolean v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzX:Z

    .line 196
    .line 197
    :cond_f
    iget-boolean v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzQ:Z

    .line 198
    .line 199
    if-eqz v2, :cond_10

    .line 200
    .line 201
    iget-object v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzp:Lcom/google/android/gms/internal/ads/zzpb;

    .line 202
    .line 203
    if-eqz v2, :cond_10

    .line 204
    .line 205
    if-ge p3, p2, :cond_10

    .line 206
    .line 207
    check-cast v2, Lcom/google/android/gms/internal/ads/zzqj;

    .line 208
    .line 209
    iget-object v2, v2, Lcom/google/android/gms/internal/ads/zzqj;->zza:Lcom/google/android/gms/internal/ads/zzqk;

    .line 210
    .line 211
    invoke-static {v2}, Lcom/google/android/gms/internal/ads/zzqk;->zzZ(Lcom/google/android/gms/internal/ads/zzqk;)Lcom/google/android/gms/internal/ads/zzlm;

    .line 212
    .line 213
    .line 214
    move-result-object v3

    .line 215
    if-eqz v3, :cond_10

    .line 216
    .line 217
    invoke-static {v2}, Lcom/google/android/gms/internal/ads/zzqk;->zzZ(Lcom/google/android/gms/internal/ads/zzqk;)Lcom/google/android/gms/internal/ads/zzlm;

    .line 218
    .line 219
    .line 220
    move-result-object v2

    .line 221
    invoke-interface {v2}, Lcom/google/android/gms/internal/ads/zzlm;->zza()V

    .line 222
    .line 223
    .line 224
    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 225
    .line 226
    iget v2, v2, Lcom/google/android/gms/internal/ads/zzpt;->zzc:I

    .line 227
    .line 228
    if-nez v2, :cond_11

    .line 229
    .line 230
    iget-wide v3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzC:J

    .line 231
    .line 232
    int-to-long v5, p3

    .line 233
    add-long/2addr v3, v5

    .line 234
    iput-wide v3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzC:J

    .line 235
    .line 236
    :cond_11
    if-ne p3, p2, :cond_14

    .line 237
    .line 238
    if-eqz v2, :cond_13

    .line 239
    .line 240
    iget-object p2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzJ:Ljava/nio/ByteBuffer;

    .line 241
    .line 242
    if-ne p1, p2, :cond_12

    .line 243
    .line 244
    goto :goto_4

    .line 245
    :cond_12
    const/4 v0, 0x0

    .line 246
    :goto_4
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzdy;->zzf(Z)V

    .line 247
    .line 248
    .line 249
    iget-wide p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzD:J

    .line 250
    .line 251
    iget p3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzE:I

    .line 252
    .line 253
    int-to-long v0, p3

    .line 254
    iget p3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzK:I

    .line 255
    .line 256
    int-to-long v2, p3

    .line 257
    mul-long v0, v0, v2

    .line 258
    .line 259
    add-long/2addr p1, v0

    .line 260
    iput-wide p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzD:J

    .line 261
    .line 262
    :cond_13
    const/4 p1, 0x0

    .line 263
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzL:Ljava/nio/ByteBuffer;

    .line 264
    .line 265
    :cond_14
    return-void
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
.end method

.method private final zzQ()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzpd;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzs:Lcom/google/android/gms/internal/ads/zzdo;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzdo;->zzh()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const-wide/high16 v1, -0x8000000000000000L

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    const/4 v4, 0x1

    .line 11
    if-nez v0, :cond_2

    .line 12
    .line 13
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzL:Ljava/nio/ByteBuffer;

    .line 14
    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    return v4

    .line 18
    :cond_0
    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/internal/ads/zzqe;->zzP(Ljava/nio/ByteBuffer;J)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzL:Ljava/nio/ByteBuffer;

    .line 22
    .line 23
    if-nez v0, :cond_1

    .line 24
    .line 25
    return v4

    .line 26
    :cond_1
    return v3

    .line 27
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzs:Lcom/google/android/gms/internal/ads/zzdo;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzdo;->zzd()V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/internal/ads/zzqe;->zzL(J)V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzs:Lcom/google/android/gms/internal/ads/zzdo;

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzdo;->zzg()Z

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-eqz v0, :cond_5

    .line 42
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzL:Ljava/nio/ByteBuffer;

    .line 44
    .line 45
    if-eqz v0, :cond_4

    .line 46
    .line 47
    invoke-virtual {v0}, Ljava/nio/Buffer;->hasRemaining()Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-eqz v0, :cond_3

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_3
    return v4

    .line 55
    :cond_4
    const/4 v3, 0x1

    .line 56
    :cond_5
    :goto_0
    return v3
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method private final zzR()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    return v0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private static zzS(Landroid/media/AudioTrack;)Z
    .locals 2

    .line 1
    sget v0, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    .line 2
    .line 3
    const/16 v1, 0x1d

    .line 4
    .line 5
    if-lt v0, v1, :cond_0

    .line 6
    .line 7
    invoke-static {p0}, Lcom/applovin/exoplayer2/b/oo88o8O;->〇080(Landroid/media/AudioTrack;)Z

    .line 8
    .line 9
    .line 10
    move-result p0

    .line 11
    if-eqz p0, :cond_0

    .line 12
    .line 13
    const/4 p0, 0x1

    .line 14
    return p0

    .line 15
    :cond_0
    const/4 p0, 0x0

    .line 16
    return p0
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final zzT()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 2
    .line 3
    iget v1, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzc:I

    .line 4
    .line 5
    if-nez v1, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/google/android/gms/internal/ads/zzpt;->zza:Lcom/google/android/gms/internal/ads/zzam;

    .line 8
    .line 9
    iget v0, v0, Lcom/google/android/gms/internal/ads/zzam;->zzB:I

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    return v0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
.end method

.method static bridge synthetic zzz(Lcom/google/android/gms/internal/ads/zzqe;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzV:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final zza(Lcom/google/android/gms/internal/ads/zzam;)I
    .locals 3

    .line 1
    iget-object v0, p1, Lcom/google/android/gms/internal/ads/zzam;->zzm:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "audio/raw"

    .line 4
    .line 5
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x2

    .line 11
    if-eqz v0, :cond_2

    .line 12
    .line 13
    iget v0, p1, Lcom/google/android/gms/internal/ads/zzam;->zzB:I

    .line 14
    .line 15
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzfk;->zzE(I)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    iget p1, p1, Lcom/google/android/gms/internal/ads/zzam;->zzB:I

    .line 22
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v2, "Invalid PCM encoding: "

    .line 29
    .line 30
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    const-string v0, "DefaultAudioSink"

    .line 41
    .line 42
    invoke-static {v0, p1}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    return v1

    .line 46
    :cond_0
    iget p1, p1, Lcom/google/android/gms/internal/ads/zzam;->zzB:I

    .line 47
    .line 48
    if-eq p1, v2, :cond_1

    .line 49
    .line 50
    const/4 p1, 0x1

    .line 51
    return p1

    .line 52
    :cond_1
    return v2

    .line 53
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzu:Lcom/google/android/gms/internal/ads/zzoj;

    .line 54
    .line 55
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/ads/zzoj;->zza(Lcom/google/android/gms/internal/ads/zzam;)Landroid/util/Pair;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    if-eqz p1, :cond_3

    .line 60
    .line 61
    return v2

    .line 62
    :cond_3
    return v1
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final zzb(Z)J
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzR()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzG:Z

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto/16 :goto_2

    .line 12
    .line 13
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 14
    .line 15
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/ads/zzpi;->zzb(Z)J

    .line 16
    .line 17
    .line 18
    move-result-wide v0

    .line 19
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 20
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzH()J

    .line 22
    .line 23
    .line 24
    move-result-wide v2

    .line 25
    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/internal/ads/zzpt;->zza(J)J

    .line 26
    .line 27
    .line 28
    move-result-wide v2

    .line 29
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    .line 30
    .line 31
    .line 32
    move-result-wide v0

    .line 33
    :goto_0
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzj:Ljava/util/ArrayDeque;

    .line 34
    .line 35
    invoke-virtual {p1}, Ljava/util/ArrayDeque;->isEmpty()Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    if-nez p1, :cond_1

    .line 40
    .line 41
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzj:Ljava/util/ArrayDeque;

    .line 42
    .line 43
    invoke-virtual {p1}, Ljava/util/ArrayDeque;->getFirst()Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    check-cast p1, Lcom/google/android/gms/internal/ads/zzpw;

    .line 48
    .line 49
    iget-wide v2, p1, Lcom/google/android/gms/internal/ads/zzpw;->zzc:J

    .line 50
    .line 51
    cmp-long p1, v0, v2

    .line 52
    .line 53
    if-ltz p1, :cond_1

    .line 54
    .line 55
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzj:Ljava/util/ArrayDeque;

    .line 56
    .line 57
    invoke-virtual {p1}, Ljava/util/ArrayDeque;->remove()Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    check-cast p1, Lcom/google/android/gms/internal/ads/zzpw;

    .line 62
    .line 63
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzx:Lcom/google/android/gms/internal/ads/zzpw;

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_1
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzx:Lcom/google/android/gms/internal/ads/zzpw;

    .line 67
    .line 68
    iget-wide v2, p1, Lcom/google/android/gms/internal/ads/zzpw;->zzc:J

    .line 69
    .line 70
    sub-long v2, v0, v2

    .line 71
    .line 72
    iget-object p1, p1, Lcom/google/android/gms/internal/ads/zzpw;->zza:Lcom/google/android/gms/internal/ads/zzch;

    .line 73
    .line 74
    sget-object v4, Lcom/google/android/gms/internal/ads/zzch;->zza:Lcom/google/android/gms/internal/ads/zzch;

    .line 75
    .line 76
    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/ads/zzch;->equals(Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    move-result p1

    .line 80
    if-eqz p1, :cond_2

    .line 81
    .line 82
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzx:Lcom/google/android/gms/internal/ads/zzpw;

    .line 83
    .line 84
    iget-wide v0, p1, Lcom/google/android/gms/internal/ads/zzpw;->zzb:J

    .line 85
    .line 86
    add-long/2addr v0, v2

    .line 87
    goto :goto_1

    .line 88
    :cond_2
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzj:Ljava/util/ArrayDeque;

    .line 89
    .line 90
    invoke-virtual {p1}, Ljava/util/ArrayDeque;->isEmpty()Z

    .line 91
    .line 92
    .line 93
    move-result p1

    .line 94
    if-eqz p1, :cond_3

    .line 95
    .line 96
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzY:Lcom/google/android/gms/internal/ads/zzpu;

    .line 97
    .line 98
    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/internal/ads/zzpu;->zza(J)J

    .line 99
    .line 100
    .line 101
    move-result-wide v0

    .line 102
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzx:Lcom/google/android/gms/internal/ads/zzpw;

    .line 103
    .line 104
    iget-wide v2, p1, Lcom/google/android/gms/internal/ads/zzpw;->zzb:J

    .line 105
    .line 106
    add-long/2addr v0, v2

    .line 107
    goto :goto_1

    .line 108
    :cond_3
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzj:Ljava/util/ArrayDeque;

    .line 109
    .line 110
    invoke-virtual {p1}, Ljava/util/ArrayDeque;->getFirst()Ljava/lang/Object;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    check-cast p1, Lcom/google/android/gms/internal/ads/zzpw;

    .line 115
    .line 116
    iget-wide v2, p1, Lcom/google/android/gms/internal/ads/zzpw;->zzc:J

    .line 117
    .line 118
    sub-long/2addr v2, v0

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzx:Lcom/google/android/gms/internal/ads/zzpw;

    .line 120
    .line 121
    iget-object v0, v0, Lcom/google/android/gms/internal/ads/zzpw;->zza:Lcom/google/android/gms/internal/ads/zzch;

    .line 122
    .line 123
    iget v0, v0, Lcom/google/android/gms/internal/ads/zzch;->zzc:F

    .line 124
    .line 125
    invoke-static {v2, v3, v0}, Lcom/google/android/gms/internal/ads/zzfk;->zzn(JF)J

    .line 126
    .line 127
    .line 128
    move-result-wide v0

    .line 129
    iget-wide v2, p1, Lcom/google/android/gms/internal/ads/zzpw;->zzb:J

    .line 130
    .line 131
    sub-long v0, v2, v0

    .line 132
    .line 133
    :goto_1
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 134
    .line 135
    iget-object v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzY:Lcom/google/android/gms/internal/ads/zzpu;

    .line 136
    .line 137
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzpu;->zzb()J

    .line 138
    .line 139
    .line 140
    move-result-wide v2

    .line 141
    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/internal/ads/zzpt;->zza(J)J

    .line 142
    .line 143
    .line 144
    move-result-wide v2

    .line 145
    add-long/2addr v0, v2

    .line 146
    return-wide v0

    .line 147
    :cond_4
    :goto_2
    const-wide/high16 v0, -0x8000000000000000L

    .line 148
    .line 149
    return-wide v0
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method public final zzc()Lcom/google/android/gms/internal/ads/zzch;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzy:Lcom/google/android/gms/internal/ads/zzch;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final zzd(Lcom/google/android/gms/internal/ads/zzam;)Lcom/google/android/gms/internal/ads/zzom;
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzW:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    sget-object p1, Lcom/google/android/gms/internal/ads/zzom;->zza:Lcom/google/android/gms/internal/ads/zzom;

    .line 6
    .line 7
    return-object p1

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzZ:Lcom/google/android/gms/internal/ads/zzpm;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzv:Lcom/google/android/gms/internal/ads/zzk;

    .line 11
    .line 12
    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/internal/ads/zzpm;->zza(Lcom/google/android/gms/internal/ads/zzam;Lcom/google/android/gms/internal/ads/zzk;)Lcom/google/android/gms/internal/ads/zzom;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    return-object p1
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zze(Lcom/google/android/gms/internal/ads/zzam;I[I)V
    .locals 18
    .param p3    # [I
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzoz;
        }
    .end annotation

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v3, p1

    .line 4
    .line 5
    iget-object v0, v3, Lcom/google/android/gms/internal/ads/zzam;->zzm:Ljava/lang/String;

    .line 6
    .line 7
    const-string v2, "audio/raw"

    .line 8
    .line 9
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/16 v2, 0x8

    .line 14
    .line 15
    const/4 v5, -0x1

    .line 16
    if-eqz v0, :cond_3

    .line 17
    .line 18
    iget v0, v3, Lcom/google/android/gms/internal/ads/zzam;->zzB:I

    .line 19
    .line 20
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzfk;->zzE(I)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzdy;->zzd(Z)V

    .line 25
    .line 26
    .line 27
    iget v0, v3, Lcom/google/android/gms/internal/ads/zzam;->zzB:I

    .line 28
    .line 29
    iget v6, v3, Lcom/google/android/gms/internal/ads/zzam;->zzz:I

    .line 30
    .line 31
    invoke-static {v0, v6}, Lcom/google/android/gms/internal/ads/zzfk;->zzl(II)I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    new-instance v6, Lcom/google/android/gms/internal/ads/zzfua;

    .line 36
    .line 37
    invoke-direct {v6}, Lcom/google/android/gms/internal/ads/zzfua;-><init>()V

    .line 38
    .line 39
    .line 40
    iget-object v7, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzf:Lcom/google/android/gms/internal/ads/zzfud;

    .line 41
    .line 42
    invoke-virtual {v6, v7}, Lcom/google/android/gms/internal/ads/zzfua;->zzh(Ljava/lang/Iterable;)Lcom/google/android/gms/internal/ads/zzfua;

    .line 43
    .line 44
    .line 45
    iget-object v7, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzY:Lcom/google/android/gms/internal/ads/zzpu;

    .line 46
    .line 47
    invoke-virtual {v7}, Lcom/google/android/gms/internal/ads/zzpu;->zze()[Lcom/google/android/gms/internal/ads/zzdr;

    .line 48
    .line 49
    .line 50
    move-result-object v7

    .line 51
    invoke-virtual {v6, v7}, Lcom/google/android/gms/internal/ads/zzfua;->zzg([Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfua;

    .line 52
    .line 53
    .line 54
    new-instance v7, Lcom/google/android/gms/internal/ads/zzdo;

    .line 55
    .line 56
    invoke-virtual {v6}, Lcom/google/android/gms/internal/ads/zzfua;->zzi()Lcom/google/android/gms/internal/ads/zzfud;

    .line 57
    .line 58
    .line 59
    move-result-object v6

    .line 60
    invoke-direct {v7, v6}, Lcom/google/android/gms/internal/ads/zzdo;-><init>(Lcom/google/android/gms/internal/ads/zzfud;)V

    .line 61
    .line 62
    .line 63
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzs:Lcom/google/android/gms/internal/ads/zzdo;

    .line 64
    .line 65
    invoke-virtual {v7, v6}, Lcom/google/android/gms/internal/ads/zzdo;->equals(Ljava/lang/Object;)Z

    .line 66
    .line 67
    .line 68
    move-result v6

    .line 69
    if-eqz v6, :cond_0

    .line 70
    .line 71
    iget-object v7, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzs:Lcom/google/android/gms/internal/ads/zzdo;

    .line 72
    .line 73
    :cond_0
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zze:Lcom/google/android/gms/internal/ads/zzqo;

    .line 74
    .line 75
    iget v8, v3, Lcom/google/android/gms/internal/ads/zzam;->zzC:I

    .line 76
    .line 77
    iget v9, v3, Lcom/google/android/gms/internal/ads/zzam;->zzD:I

    .line 78
    .line 79
    invoke-virtual {v6, v8, v9}, Lcom/google/android/gms/internal/ads/zzqo;->zzq(II)V

    .line 80
    .line 81
    .line 82
    sget v6, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    .line 83
    .line 84
    const/16 v8, 0x15

    .line 85
    .line 86
    if-ge v6, v8, :cond_1

    .line 87
    .line 88
    iget v6, v3, Lcom/google/android/gms/internal/ads/zzam;->zzz:I

    .line 89
    .line 90
    if-ne v6, v2, :cond_1

    .line 91
    .line 92
    if-nez p3, :cond_1

    .line 93
    .line 94
    const/4 v6, 0x6

    .line 95
    new-array v8, v6, [I

    .line 96
    .line 97
    const/4 v9, 0x0

    .line 98
    :goto_0
    if-ge v9, v6, :cond_2

    .line 99
    .line 100
    aput v9, v8, v9

    .line 101
    .line 102
    add-int/lit8 v9, v9, 0x1

    .line 103
    .line 104
    goto :goto_0

    .line 105
    :cond_1
    move-object/from16 v8, p3

    .line 106
    .line 107
    :cond_2
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzd:Lcom/google/android/gms/internal/ads/zzpj;

    .line 108
    .line 109
    invoke-virtual {v6, v8}, Lcom/google/android/gms/internal/ads/zzpj;->zzo([I)V

    .line 110
    .line 111
    .line 112
    new-instance v6, Lcom/google/android/gms/internal/ads/zzdp;

    .line 113
    .line 114
    iget v8, v3, Lcom/google/android/gms/internal/ads/zzam;->zzA:I

    .line 115
    .line 116
    iget v9, v3, Lcom/google/android/gms/internal/ads/zzam;->zzz:I

    .line 117
    .line 118
    iget v10, v3, Lcom/google/android/gms/internal/ads/zzam;->zzB:I

    .line 119
    .line 120
    invoke-direct {v6, v8, v9, v10}, Lcom/google/android/gms/internal/ads/zzdp;-><init>(III)V

    .line 121
    .line 122
    .line 123
    :try_start_0
    invoke-virtual {v7, v6}, Lcom/google/android/gms/internal/ads/zzdo;->zza(Lcom/google/android/gms/internal/ads/zzdp;)Lcom/google/android/gms/internal/ads/zzdp;

    .line 124
    .line 125
    .line 126
    move-result-object v6
    :try_end_0
    .catch Lcom/google/android/gms/internal/ads/zzdq; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    iget v8, v6, Lcom/google/android/gms/internal/ads/zzdp;->zzd:I

    .line 128
    .line 129
    iget v9, v6, Lcom/google/android/gms/internal/ads/zzdp;->zzb:I

    .line 130
    .line 131
    iget v6, v6, Lcom/google/android/gms/internal/ads/zzdp;->zzc:I

    .line 132
    .line 133
    invoke-static {v6}, Lcom/google/android/gms/internal/ads/zzfk;->zzg(I)I

    .line 134
    .line 135
    .line 136
    move-result v10

    .line 137
    invoke-static {v8, v6}, Lcom/google/android/gms/internal/ads/zzfk;->zzl(II)I

    .line 138
    .line 139
    .line 140
    move-result v6

    .line 141
    move-object v11, v7

    .line 142
    move v7, v9

    .line 143
    const/4 v9, 0x0

    .line 144
    goto :goto_1

    .line 145
    :catch_0
    move-exception v0

    .line 146
    move-object v2, v0

    .line 147
    new-instance v0, Lcom/google/android/gms/internal/ads/zzoz;

    .line 148
    .line 149
    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/ads/zzoz;-><init>(Ljava/lang/Throwable;Lcom/google/android/gms/internal/ads/zzam;)V

    .line 150
    .line 151
    .line 152
    throw v0

    .line 153
    :cond_3
    new-instance v0, Lcom/google/android/gms/internal/ads/zzdo;

    .line 154
    .line 155
    invoke-static {}, Lcom/google/android/gms/internal/ads/zzfud;->zzl()Lcom/google/android/gms/internal/ads/zzfud;

    .line 156
    .line 157
    .line 158
    move-result-object v6

    .line 159
    invoke-direct {v0, v6}, Lcom/google/android/gms/internal/ads/zzdo;-><init>(Lcom/google/android/gms/internal/ads/zzfud;)V

    .line 160
    .line 161
    .line 162
    iget v6, v3, Lcom/google/android/gms/internal/ads/zzam;->zzA:I

    .line 163
    .line 164
    sget-object v7, Lcom/google/android/gms/internal/ads/zzom;->zza:Lcom/google/android/gms/internal/ads/zzom;

    .line 165
    .line 166
    iget-object v7, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzu:Lcom/google/android/gms/internal/ads/zzoj;

    .line 167
    .line 168
    invoke-virtual {v7, v3}, Lcom/google/android/gms/internal/ads/zzoj;->zza(Lcom/google/android/gms/internal/ads/zzam;)Landroid/util/Pair;

    .line 169
    .line 170
    .line 171
    move-result-object v7

    .line 172
    if-eqz v7, :cond_d

    .line 173
    .line 174
    iget-object v8, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    .line 175
    .line 176
    check-cast v8, Ljava/lang/Integer;

    .line 177
    .line 178
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    .line 179
    .line 180
    .line 181
    move-result v8

    .line 182
    iget-object v7, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 183
    .line 184
    check-cast v7, Ljava/lang/Integer;

    .line 185
    .line 186
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    .line 187
    .line 188
    .line 189
    move-result v7

    .line 190
    const/4 v9, 0x2

    .line 191
    move-object v11, v0

    .line 192
    move v10, v7

    .line 193
    const/4 v0, -0x1

    .line 194
    move v7, v6

    .line 195
    const/4 v6, -0x1

    .line 196
    :goto_1
    const-string v12, ") for: "

    .line 197
    .line 198
    if-eqz v8, :cond_c

    .line 199
    .line 200
    if-eqz v10, :cond_b

    .line 201
    .line 202
    invoke-static {v7, v10, v8}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    .line 203
    .line 204
    .line 205
    move-result v12

    .line 206
    const/4 v13, -0x2

    .line 207
    const/4 v14, 0x1

    .line 208
    if-eq v12, v13, :cond_4

    .line 209
    .line 210
    const/4 v13, 0x1

    .line 211
    goto :goto_2

    .line 212
    :cond_4
    const/4 v13, 0x0

    .line 213
    :goto_2
    invoke-static {v13}, Lcom/google/android/gms/internal/ads/zzdy;->zzf(Z)V

    .line 214
    .line 215
    .line 216
    if-eq v6, v5, :cond_5

    .line 217
    .line 218
    move v13, v6

    .line 219
    goto :goto_3

    .line 220
    :cond_5
    const/4 v13, 0x1

    .line 221
    :goto_3
    iget v15, v3, Lcom/google/android/gms/internal/ads/zzam;->zzi:I

    .line 222
    .line 223
    const v4, 0x3d090

    .line 224
    .line 225
    .line 226
    if-eqz v9, :cond_9

    .line 227
    .line 228
    const-wide/32 v16, 0xf4240

    .line 229
    .line 230
    .line 231
    if-eq v9, v14, :cond_8

    .line 232
    .line 233
    const/4 v14, 0x5

    .line 234
    if-ne v8, v14, :cond_6

    .line 235
    .line 236
    const v4, 0x7a120

    .line 237
    .line 238
    .line 239
    const/4 v8, 0x5

    .line 240
    goto :goto_4

    .line 241
    :cond_6
    move v14, v8

    .line 242
    :goto_4
    if-eq v15, v5, :cond_7

    .line 243
    .line 244
    sget-object v8, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    .line 245
    .line 246
    invoke-static {v15, v2, v8}, Lcom/google/android/gms/internal/ads/zzfwj;->zza(IILjava/math/RoundingMode;)I

    .line 247
    .line 248
    .line 249
    move-result v2

    .line 250
    goto :goto_5

    .line 251
    :cond_7
    invoke-static {v8}, Lcom/google/android/gms/internal/ads/zzqg;->zzb(I)I

    .line 252
    .line 253
    .line 254
    move-result v2

    .line 255
    :goto_5
    move/from16 p3, v6

    .line 256
    .line 257
    int-to-long v5, v4

    .line 258
    move v4, v14

    .line 259
    int-to-long v14, v2

    .line 260
    mul-long v5, v5, v14

    .line 261
    .line 262
    div-long v5, v5, v16

    .line 263
    .line 264
    invoke-static {v5, v6}, Lcom/google/android/gms/internal/ads/zzfwl;->zza(J)I

    .line 265
    .line 266
    .line 267
    move-result v2

    .line 268
    move v14, v4

    .line 269
    goto :goto_7

    .line 270
    :cond_8
    move/from16 p3, v6

    .line 271
    .line 272
    invoke-static {v8}, Lcom/google/android/gms/internal/ads/zzqg;->zzb(I)I

    .line 273
    .line 274
    .line 275
    move-result v2

    .line 276
    int-to-long v4, v2

    .line 277
    const-wide/32 v14, 0x2faf080

    .line 278
    .line 279
    .line 280
    mul-long v4, v4, v14

    .line 281
    .line 282
    div-long v4, v4, v16

    .line 283
    .line 284
    invoke-static {v4, v5}, Lcom/google/android/gms/internal/ads/zzfwl;->zza(J)I

    .line 285
    .line 286
    .line 287
    move-result v2

    .line 288
    goto :goto_6

    .line 289
    :cond_9
    move/from16 p3, v6

    .line 290
    .line 291
    mul-int/lit8 v2, v12, 0x4

    .line 292
    .line 293
    invoke-static {v4, v7, v13}, Lcom/google/android/gms/internal/ads/zzqg;->zza(III)I

    .line 294
    .line 295
    .line 296
    move-result v4

    .line 297
    const v5, 0xb71b0

    .line 298
    .line 299
    .line 300
    invoke-static {v5, v7, v13}, Lcom/google/android/gms/internal/ads/zzqg;->zza(III)I

    .line 301
    .line 302
    .line 303
    move-result v5

    .line 304
    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    .line 305
    .line 306
    .line 307
    move-result v2

    .line 308
    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    .line 309
    .line 310
    .line 311
    move-result v2

    .line 312
    :goto_6
    move v14, v8

    .line 313
    :goto_7
    int-to-double v4, v2

    .line 314
    double-to-int v2, v4

    .line 315
    invoke-static {v12, v2}, Ljava/lang/Math;->max(II)I

    .line 316
    .line 317
    .line 318
    move-result v2

    .line 319
    add-int/2addr v2, v13

    .line 320
    const/4 v4, -0x1

    .line 321
    add-int/2addr v2, v4

    .line 322
    div-int/2addr v2, v13

    .line 323
    mul-int v12, v2, v13

    .line 324
    .line 325
    const/4 v2, 0x0

    .line 326
    iput-boolean v2, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzW:Z

    .line 327
    .line 328
    new-instance v15, Lcom/google/android/gms/internal/ads/zzpt;

    .line 329
    .line 330
    const/4 v13, 0x0

    .line 331
    const/16 v16, 0x0

    .line 332
    .line 333
    move-object v2, v15

    .line 334
    move-object/from16 v3, p1

    .line 335
    .line 336
    move v4, v0

    .line 337
    move v5, v9

    .line 338
    move/from16 v6, p3

    .line 339
    .line 340
    move v8, v10

    .line 341
    move v9, v14

    .line 342
    move v10, v12

    .line 343
    move v12, v13

    .line 344
    move/from16 v13, v16

    .line 345
    .line 346
    invoke-direct/range {v2 .. v13}, Lcom/google/android/gms/internal/ads/zzpt;-><init>(Lcom/google/android/gms/internal/ads/zzam;IIIIIIILcom/google/android/gms/internal/ads/zzdo;ZZ)V

    .line 347
    .line 348
    .line 349
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzR()Z

    .line 350
    .line 351
    .line 352
    move-result v0

    .line 353
    if-eqz v0, :cond_a

    .line 354
    .line 355
    iput-object v15, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzq:Lcom/google/android/gms/internal/ads/zzpt;

    .line 356
    .line 357
    return-void

    .line 358
    :cond_a
    iput-object v15, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 359
    .line 360
    return-void

    .line 361
    :cond_b
    new-instance v0, Lcom/google/android/gms/internal/ads/zzoz;

    .line 362
    .line 363
    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 364
    .line 365
    .line 366
    move-result-object v2

    .line 367
    new-instance v4, Ljava/lang/StringBuilder;

    .line 368
    .line 369
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 370
    .line 371
    .line 372
    const-string v5, "Invalid output channel config (mode="

    .line 373
    .line 374
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    .line 376
    .line 377
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 378
    .line 379
    .line 380
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    .line 382
    .line 383
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    .line 385
    .line 386
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 387
    .line 388
    .line 389
    move-result-object v2

    .line 390
    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/ads/zzoz;-><init>(Ljava/lang/String;Lcom/google/android/gms/internal/ads/zzam;)V

    .line 391
    .line 392
    .line 393
    throw v0

    .line 394
    :cond_c
    new-instance v0, Lcom/google/android/gms/internal/ads/zzoz;

    .line 395
    .line 396
    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 397
    .line 398
    .line 399
    move-result-object v2

    .line 400
    new-instance v4, Ljava/lang/StringBuilder;

    .line 401
    .line 402
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 403
    .line 404
    .line 405
    const-string v5, "Invalid output encoding (mode="

    .line 406
    .line 407
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    .line 409
    .line 410
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 411
    .line 412
    .line 413
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    .line 415
    .line 416
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    .line 418
    .line 419
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 420
    .line 421
    .line 422
    move-result-object v2

    .line 423
    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/ads/zzoz;-><init>(Ljava/lang/String;Lcom/google/android/gms/internal/ads/zzam;)V

    .line 424
    .line 425
    .line 426
    throw v0

    .line 427
    :cond_d
    new-instance v0, Lcom/google/android/gms/internal/ads/zzoz;

    .line 428
    .line 429
    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 430
    .line 431
    .line 432
    move-result-object v2

    .line 433
    const-string v4, "Unable to configure passthrough for: "

    .line 434
    .line 435
    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 436
    .line 437
    .line 438
    move-result-object v2

    .line 439
    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/internal/ads/zzoz;-><init>(Ljava/lang/String;Lcom/google/android/gms/internal/ads/zzam;)V

    .line 440
    .line 441
    .line 442
    throw v0
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
.end method

.method public final zzf()V
    .locals 11

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzR()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_5

    .line 6
    .line 7
    const-wide/16 v0, 0x0

    .line 8
    .line 9
    iput-wide v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzA:J

    .line 10
    .line 11
    iput-wide v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzB:J

    .line 12
    .line 13
    iput-wide v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzC:J

    .line 14
    .line 15
    iput-wide v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzD:J

    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    iput-boolean v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzX:Z

    .line 19
    .line 20
    iput v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzE:I

    .line 21
    .line 22
    new-instance v10, Lcom/google/android/gms/internal/ads/zzpw;

    .line 23
    .line 24
    iget-object v4, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzy:Lcom/google/android/gms/internal/ads/zzch;

    .line 25
    .line 26
    const-wide/16 v5, 0x0

    .line 27
    .line 28
    const-wide/16 v7, 0x0

    .line 29
    .line 30
    const/4 v9, 0x0

    .line 31
    move-object v3, v10

    .line 32
    invoke-direct/range {v3 .. v9}, Lcom/google/android/gms/internal/ads/zzpw;-><init>(Lcom/google/android/gms/internal/ads/zzch;JJLcom/google/android/gms/internal/ads/zzpv;)V

    .line 33
    .line 34
    .line 35
    iput-object v10, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzx:Lcom/google/android/gms/internal/ads/zzpw;

    .line 36
    .line 37
    iput-wide v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzH:J

    .line 38
    .line 39
    const/4 v0, 0x0

    .line 40
    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzw:Lcom/google/android/gms/internal/ads/zzpw;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzj:Ljava/util/ArrayDeque;

    .line 43
    .line 44
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->clear()V

    .line 45
    .line 46
    .line 47
    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzJ:Ljava/nio/ByteBuffer;

    .line 48
    .line 49
    iput v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzK:I

    .line 50
    .line 51
    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzL:Ljava/nio/ByteBuffer;

    .line 52
    .line 53
    iput-boolean v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzP:Z

    .line 54
    .line 55
    iput-boolean v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzO:Z

    .line 56
    .line 57
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zze:Lcom/google/android/gms/internal/ads/zzqo;

    .line 58
    .line 59
    invoke-virtual {v1}, Lcom/google/android/gms/internal/ads/zzqo;->zzp()V

    .line 60
    .line 61
    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzO()V

    .line 63
    .line 64
    .line 65
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/google/android/gms/internal/ads/zzpi;->zzi()Z

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    if-eqz v1, :cond_0

    .line 72
    .line 73
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 74
    .line 75
    invoke-virtual {v1}, Landroid/media/AudioTrack;->pause()V

    .line 76
    .line 77
    .line 78
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 79
    .line 80
    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzqe;->zzS(Landroid/media/AudioTrack;)Z

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    if-eqz v1, :cond_1

    .line 85
    .line 86
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzk:Lcom/google/android/gms/internal/ads/zzqc;

    .line 87
    .line 88
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 89
    .line 90
    .line 91
    iget-object v3, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 92
    .line 93
    invoke-virtual {v1, v3}, Lcom/google/android/gms/internal/ads/zzqc;->zzb(Landroid/media/AudioTrack;)V

    .line 94
    .line 95
    .line 96
    :cond_1
    sget v1, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    .line 97
    .line 98
    const/16 v3, 0x15

    .line 99
    .line 100
    if-ge v1, v3, :cond_2

    .line 101
    .line 102
    iget-boolean v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzR:Z

    .line 103
    .line 104
    if-nez v1, :cond_2

    .line 105
    .line 106
    iput v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzS:I

    .line 107
    .line 108
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzq:Lcom/google/android/gms/internal/ads/zzpt;

    .line 109
    .line 110
    if-eqz v1, :cond_3

    .line 111
    .line 112
    iput-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 113
    .line 114
    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzq:Lcom/google/android/gms/internal/ads/zzpt;

    .line 115
    .line 116
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 117
    .line 118
    invoke-virtual {v1}, Lcom/google/android/gms/internal/ads/zzpi;->zzd()V

    .line 119
    .line 120
    .line 121
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 122
    .line 123
    iget-object v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzh:Lcom/google/android/gms/internal/ads/zzeb;

    .line 124
    .line 125
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzeb;->zzc()Z

    .line 126
    .line 127
    .line 128
    sget-object v3, Lcom/google/android/gms/internal/ads/zzqe;->zza:Ljava/lang/Object;

    .line 129
    .line 130
    monitor-enter v3

    .line 131
    :try_start_0
    sget-object v4, Lcom/google/android/gms/internal/ads/zzqe;->zzb:Ljava/util/concurrent/ExecutorService;

    .line 132
    .line 133
    if-nez v4, :cond_4

    .line 134
    .line 135
    const-string v4, "ExoPlayer:AudioTrackReleaseThread"

    .line 136
    .line 137
    invoke-static {v4}, Lcom/google/android/gms/internal/ads/zzfk;->zzB(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    .line 138
    .line 139
    .line 140
    move-result-object v4

    .line 141
    sput-object v4, Lcom/google/android/gms/internal/ads/zzqe;->zzb:Ljava/util/concurrent/ExecutorService;

    .line 142
    .line 143
    :cond_4
    sget v4, Lcom/google/android/gms/internal/ads/zzqe;->zzc:I

    .line 144
    .line 145
    add-int/lit8 v4, v4, 0x1

    .line 146
    .line 147
    sput v4, Lcom/google/android/gms/internal/ads/zzqe;->zzc:I

    .line 148
    .line 149
    sget-object v4, Lcom/google/android/gms/internal/ads/zzqe;->zzb:Ljava/util/concurrent/ExecutorService;

    .line 150
    .line 151
    new-instance v5, Lcom/google/android/gms/internal/ads/zzpn;

    .line 152
    .line 153
    invoke-direct {v5, v1, v2}, Lcom/google/android/gms/internal/ads/zzpn;-><init>(Landroid/media/AudioTrack;Lcom/google/android/gms/internal/ads/zzeb;)V

    .line 154
    .line 155
    .line 156
    invoke-interface {v4, v5}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 157
    .line 158
    .line 159
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 161
    .line 162
    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    .line 164
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165
    throw v0

    .line 166
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzm:Lcom/google/android/gms/internal/ads/zzpx;

    .line 167
    .line 168
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzpx;->zza()V

    .line 169
    .line 170
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzl:Lcom/google/android/gms/internal/ads/zzpx;

    .line 172
    .line 173
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzpx;->zza()V

    .line 174
    .line 175
    .line 176
    return-void
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
.end method

.method public final zzg()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzF:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final zzh()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzQ:Z

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzR()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzpi;->zzl()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 19
    .line 20
    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    .line 21
    .line 22
    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method public final zzi()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzQ:Z

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzR()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzpi;->zzg()V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method public final zzj()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzpd;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzO:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzR()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzQ()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzK()V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x1

    .line 21
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzO:Z

    .line 22
    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method public final zzk()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzf()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzf:Lcom/google/android/gms/internal/ads/zzfud;

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    const/4 v2, 0x0

    .line 11
    const/4 v3, 0x0

    .line 12
    :goto_0
    if-ge v3, v1, :cond_0

    .line 13
    .line 14
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v4

    .line 18
    check-cast v4, Lcom/google/android/gms/internal/ads/zzdr;

    .line 19
    .line 20
    invoke-interface {v4}, Lcom/google/android/gms/internal/ads/zzdr;->zzf()V

    .line 21
    .line 22
    .line 23
    add-int/lit8 v3, v3, 0x1

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzg:Lcom/google/android/gms/internal/ads/zzfud;

    .line 27
    .line 28
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    const/4 v3, 0x0

    .line 33
    :goto_1
    if-ge v3, v1, :cond_1

    .line 34
    .line 35
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    check-cast v4, Lcom/google/android/gms/internal/ads/zzdr;

    .line 40
    .line 41
    invoke-interface {v4}, Lcom/google/android/gms/internal/ads/zzdr;->zzf()V

    .line 42
    .line 43
    .line 44
    add-int/lit8 v3, v3, 0x1

    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzs:Lcom/google/android/gms/internal/ads/zzdo;

    .line 48
    .line 49
    if-eqz v0, :cond_2

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzdo;->zzf()V

    .line 52
    .line 53
    .line 54
    :cond_2
    iput-boolean v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzQ:Z

    .line 55
    .line 56
    iput-boolean v2, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzW:Z

    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method public final zzl(Lcom/google/android/gms/internal/ads/zzk;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzv:Lcom/google/android/gms/internal/ads/zzk;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/ads/zzk;->equals(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzv:Lcom/google/android/gms/internal/ads/zzk;

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzf()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzm(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzS:I

    .line 2
    .line 3
    if-eq v0, p1, :cond_1

    .line 4
    .line 5
    iput p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzS:I

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p1, 0x0

    .line 12
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzR:Z

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzf()V

    .line 15
    .line 16
    .line 17
    :cond_1
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method public final zzn(Lcom/google/android/gms/internal/ads/zzl;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzT:Lcom/google/android/gms/internal/ads/zzl;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/ads/zzl;->equals(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget v0, p1, Lcom/google/android/gms/internal/ads/zzl;->zza:I

    .line 11
    .line 12
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzT:Lcom/google/android/gms/internal/ads/zzl;

    .line 17
    .line 18
    iget v0, v0, Lcom/google/android/gms/internal/ads/zzl;->zza:I

    .line 19
    .line 20
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzT:Lcom/google/android/gms/internal/ads/zzl;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final zzo(Lcom/google/android/gms/internal/ads/zzdz;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/ads/zzpi;->zzf(Lcom/google/android/gms/internal/ads/zzdz;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzp(Lcom/google/android/gms/internal/ads/zzpb;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzp:Lcom/google/android/gms/internal/ads/zzpb;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzq(Lcom/google/android/gms/internal/ads/zzch;)V
    .locals 5

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/ads/zzch;

    .line 2
    .line 3
    iget v1, p1, Lcom/google/android/gms/internal/ads/zzch;->zzc:F

    .line 4
    .line 5
    const/high16 v2, 0x41000000    # 8.0f

    .line 6
    .line 7
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const v3, 0x3dcccccd    # 0.1f

    .line 12
    .line 13
    .line 14
    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    iget v4, p1, Lcom/google/android/gms/internal/ads/zzch;->zzd:F

    .line 19
    .line 20
    invoke-static {v4, v2}, Ljava/lang/Math;->min(FF)F

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/ads/zzch;-><init>(FF)V

    .line 29
    .line 30
    .line 31
    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzy:Lcom/google/android/gms/internal/ads/zzch;

    .line 32
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/ads/zzqe;->zzM(Lcom/google/android/gms/internal/ads/zzch;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final zzr(Lcom/google/android/gms/internal/ads/zzoh;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/internal/ads/zzoh;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzo:Lcom/google/android/gms/internal/ads/zzoh;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzs(Landroid/media/AudioDeviceInfo;)V
    .locals 1
    .param p1    # Landroid/media/AudioDeviceInfo;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x17
    .end annotation

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    goto :goto_0

    .line 5
    :cond_0
    new-instance v0, Lcom/google/android/gms/internal/ads/zzpq;

    .line 6
    .line 7
    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/ads/zzpq;-><init>(Landroid/media/AudioDeviceInfo;)V

    .line 8
    .line 9
    .line 10
    move-object p1, v0

    .line 11
    :goto_0
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzU:Lcom/google/android/gms/internal/ads/zzpq;

    .line 12
    .line 13
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 14
    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-static {v0, p1}, Lcom/google/android/gms/internal/ads/zzpo;->zza(Landroid/media/AudioTrack;Lcom/google/android/gms/internal/ads/zzpq;)V

    .line 18
    .line 19
    .line 20
    :cond_1
    return-void
.end method

.method public final zzt(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzz:Z

    .line 2
    .line 3
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzy:Lcom/google/android/gms/internal/ads/zzch;

    .line 4
    .line 5
    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/ads/zzqe;->zzM(Lcom/google/android/gms/internal/ads/zzch;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzu(F)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzI:F

    .line 2
    .line 3
    cmpl-float v0, v0, p1

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iput p1, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzI:F

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzN()V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzv(Ljava/nio/ByteBuffer;JI)Z
    .locals 27
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzpa;,
            Lcom/google/android/gms/internal/ads/zzpd;
        }
    .end annotation

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v2, p1

    .line 4
    .line 5
    move-wide/from16 v3, p2

    .line 6
    .line 7
    move/from16 v5, p4

    .line 8
    .line 9
    const-class v6, Ljava/lang/Throwable;

    .line 10
    .line 11
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzJ:Ljava/nio/ByteBuffer;

    .line 12
    .line 13
    const/4 v8, 0x0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    if-ne v2, v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    goto :goto_1

    .line 21
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 22
    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzdy;->zzd(Z)V

    .line 23
    .line 24
    .line 25
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzq:Lcom/google/android/gms/internal/ads/zzpt;

    .line 26
    .line 27
    const/4 v9, 0x0

    .line 28
    if-eqz v0, :cond_6

    .line 29
    .line 30
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzQ()Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-nez v0, :cond_2

    .line 35
    .line 36
    return v8

    .line 37
    :cond_2
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzq:Lcom/google/android/gms/internal/ads/zzpt;

    .line 38
    .line 39
    iget-object v10, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 40
    .line 41
    iget v11, v10, Lcom/google/android/gms/internal/ads/zzpt;->zzc:I

    .line 42
    .line 43
    iget v12, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzc:I

    .line 44
    .line 45
    if-ne v11, v12, :cond_3

    .line 46
    .line 47
    iget v11, v10, Lcom/google/android/gms/internal/ads/zzpt;->zzg:I

    .line 48
    .line 49
    iget v12, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzg:I

    .line 50
    .line 51
    if-ne v11, v12, :cond_3

    .line 52
    .line 53
    iget v11, v10, Lcom/google/android/gms/internal/ads/zzpt;->zze:I

    .line 54
    .line 55
    iget v12, v0, Lcom/google/android/gms/internal/ads/zzpt;->zze:I

    .line 56
    .line 57
    if-ne v11, v12, :cond_3

    .line 58
    .line 59
    iget v11, v10, Lcom/google/android/gms/internal/ads/zzpt;->zzf:I

    .line 60
    .line 61
    iget v12, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzf:I

    .line 62
    .line 63
    if-ne v11, v12, :cond_3

    .line 64
    .line 65
    iget v10, v10, Lcom/google/android/gms/internal/ads/zzpt;->zzd:I

    .line 66
    .line 67
    iget v11, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzd:I

    .line 68
    .line 69
    if-ne v10, v11, :cond_3

    .line 70
    .line 71
    iput-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 72
    .line 73
    iput-object v9, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzq:Lcom/google/android/gms/internal/ads/zzpt;

    .line 74
    .line 75
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 76
    .line 77
    if-eqz v0, :cond_5

    .line 78
    .line 79
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzqe;->zzS(Landroid/media/AudioTrack;)Z

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    if-eqz v0, :cond_5

    .line 84
    .line 85
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 86
    .line 87
    iget-boolean v0, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzk:Z

    .line 88
    .line 89
    goto :goto_2

    .line 90
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzK()V

    .line 91
    .line 92
    .line 93
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzw()Z

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    if-eqz v0, :cond_4

    .line 98
    .line 99
    return v8

    .line 100
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzf()V

    .line 101
    .line 102
    .line 103
    :cond_5
    :goto_2
    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/internal/ads/zzqe;->zzJ(J)V

    .line 104
    .line 105
    .line 106
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzR()Z

    .line 107
    .line 108
    .line 109
    move-result v0

    .line 110
    if-eqz v0, :cond_7

    .line 111
    .line 112
    goto/16 :goto_5

    .line 113
    .line 114
    :cond_7
    :try_start_0
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzh:Lcom/google/android/gms/internal/ads/zzeb;

    .line 115
    .line 116
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzeb;->zzd()Z

    .line 117
    .line 118
    .line 119
    move-result v0
    :try_end_0
    .catch Lcom/google/android/gms/internal/ads/zzpa; {:try_start_0 .. :try_end_0} :catch_3

    .line 120
    if-nez v0, :cond_8

    .line 121
    .line 122
    return v8

    .line 123
    :cond_8
    :try_start_1
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;
    :try_end_1
    .catch Lcom/google/android/gms/internal/ads/zzpa; {:try_start_1 .. :try_end_1} :catch_0

    .line 124
    .line 125
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 126
    .line 127
    .line 128
    :try_start_2
    invoke-direct {v1, v0}, Lcom/google/android/gms/internal/ads/zzqe;->zzI(Lcom/google/android/gms/internal/ads/zzpt;)Landroid/media/AudioTrack;

    .line 129
    .line 130
    .line 131
    move-result-object v0
    :try_end_2
    .catch Lcom/google/android/gms/internal/ads/zzpa; {:try_start_2 .. :try_end_2} :catch_0

    .line 132
    goto :goto_3

    .line 133
    :catch_0
    move-exception v0

    .line 134
    move-object v12, v0

    .line 135
    :try_start_3
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 136
    .line 137
    iget v13, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzh:I

    .line 138
    .line 139
    const v14, 0xf4240

    .line 140
    .line 141
    .line 142
    if-le v13, v14, :cond_29

    .line 143
    .line 144
    new-instance v13, Lcom/google/android/gms/internal/ads/zzpt;

    .line 145
    .line 146
    iget-object v14, v0, Lcom/google/android/gms/internal/ads/zzpt;->zza:Lcom/google/android/gms/internal/ads/zzam;

    .line 147
    .line 148
    iget v15, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzb:I

    .line 149
    .line 150
    iget v9, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzc:I

    .line 151
    .line 152
    iget v8, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzd:I

    .line 153
    .line 154
    iget v7, v0, Lcom/google/android/gms/internal/ads/zzpt;->zze:I

    .line 155
    .line 156
    iget v11, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzf:I

    .line 157
    .line 158
    iget v10, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzg:I

    .line 159
    .line 160
    const v23, 0xf4240

    .line 161
    .line 162
    .line 163
    iget-object v0, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzi:Lcom/google/android/gms/internal/ads/zzdo;

    .line 164
    .line 165
    const/16 v25, 0x0

    .line 166
    .line 167
    const/16 v26, 0x0

    .line 168
    .line 169
    move/from16 v17, v15

    .line 170
    .line 171
    move-object v15, v13

    .line 172
    move-object/from16 v16, v14

    .line 173
    .line 174
    move/from16 v18, v9

    .line 175
    .line 176
    move/from16 v19, v8

    .line 177
    .line 178
    move/from16 v20, v7

    .line 179
    .line 180
    move/from16 v21, v11

    .line 181
    .line 182
    move/from16 v22, v10

    .line 183
    .line 184
    move-object/from16 v24, v0

    .line 185
    .line 186
    invoke-direct/range {v15 .. v26}, Lcom/google/android/gms/internal/ads/zzpt;-><init>(Lcom/google/android/gms/internal/ads/zzam;IIIIIIILcom/google/android/gms/internal/ads/zzdo;ZZ)V
    :try_end_3
    .catch Lcom/google/android/gms/internal/ads/zzpa; {:try_start_3 .. :try_end_3} :catch_3

    .line 187
    .line 188
    .line 189
    :try_start_4
    invoke-direct {v1, v13}, Lcom/google/android/gms/internal/ads/zzqe;->zzI(Lcom/google/android/gms/internal/ads/zzpt;)Landroid/media/AudioTrack;

    .line 190
    .line 191
    .line 192
    move-result-object v0

    .line 193
    iput-object v13, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;
    :try_end_4
    .catch Lcom/google/android/gms/internal/ads/zzpa; {:try_start_4 .. :try_end_4} :catch_1

    .line 194
    .line 195
    :goto_3
    :try_start_5
    iput-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 196
    .line 197
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzqe;->zzS(Landroid/media/AudioTrack;)Z

    .line 198
    .line 199
    .line 200
    move-result v0

    .line 201
    if-eqz v0, :cond_a

    .line 202
    .line 203
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 204
    .line 205
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzk:Lcom/google/android/gms/internal/ads/zzqc;

    .line 206
    .line 207
    if-nez v6, :cond_9

    .line 208
    .line 209
    new-instance v6, Lcom/google/android/gms/internal/ads/zzqc;

    .line 210
    .line 211
    invoke-direct {v6, v1}, Lcom/google/android/gms/internal/ads/zzqc;-><init>(Lcom/google/android/gms/internal/ads/zzqe;)V

    .line 212
    .line 213
    .line 214
    iput-object v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzk:Lcom/google/android/gms/internal/ads/zzqc;

    .line 215
    .line 216
    :cond_9
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzk:Lcom/google/android/gms/internal/ads/zzqc;

    .line 217
    .line 218
    invoke-virtual {v6, v0}, Lcom/google/android/gms/internal/ads/zzqc;->zza(Landroid/media/AudioTrack;)V

    .line 219
    .line 220
    .line 221
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 222
    .line 223
    iget-boolean v0, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzk:Z

    .line 224
    .line 225
    :cond_a
    sget v0, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    .line 226
    .line 227
    const/16 v6, 0x1f

    .line 228
    .line 229
    if-lt v0, v6, :cond_b

    .line 230
    .line 231
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzo:Lcom/google/android/gms/internal/ads/zzoh;

    .line 232
    .line 233
    if-eqz v6, :cond_b

    .line 234
    .line 235
    iget-object v7, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 236
    .line 237
    invoke-static {v7, v6}, Lcom/google/android/gms/internal/ads/zzpp;->zza(Landroid/media/AudioTrack;Lcom/google/android/gms/internal/ads/zzoh;)V

    .line 238
    .line 239
    .line 240
    :cond_b
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 241
    .line 242
    invoke-virtual {v6}, Landroid/media/AudioTrack;->getAudioSessionId()I

    .line 243
    .line 244
    .line 245
    move-result v6

    .line 246
    iput v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzS:I

    .line 247
    .line 248
    iget-object v7, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 249
    .line 250
    iget-object v8, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 251
    .line 252
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 253
    .line 254
    iget v9, v6, Lcom/google/android/gms/internal/ads/zzpt;->zzc:I

    .line 255
    .line 256
    const/4 v10, 0x2

    .line 257
    if-ne v9, v10, :cond_c

    .line 258
    .line 259
    const/4 v9, 0x1

    .line 260
    goto :goto_4

    .line 261
    :cond_c
    const/4 v9, 0x0

    .line 262
    :goto_4
    iget v10, v6, Lcom/google/android/gms/internal/ads/zzpt;->zzg:I

    .line 263
    .line 264
    iget v11, v6, Lcom/google/android/gms/internal/ads/zzpt;->zzd:I

    .line 265
    .line 266
    iget v12, v6, Lcom/google/android/gms/internal/ads/zzpt;->zzh:I

    .line 267
    .line 268
    invoke-virtual/range {v7 .. v12}, Lcom/google/android/gms/internal/ads/zzpi;->zze(Landroid/media/AudioTrack;ZIII)V

    .line 269
    .line 270
    .line 271
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzN()V

    .line 272
    .line 273
    .line 274
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzT:Lcom/google/android/gms/internal/ads/zzl;

    .line 275
    .line 276
    iget v6, v6, Lcom/google/android/gms/internal/ads/zzl;->zza:I

    .line 277
    .line 278
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzU:Lcom/google/android/gms/internal/ads/zzpq;

    .line 279
    .line 280
    if-eqz v6, :cond_d

    .line 281
    .line 282
    const/16 v7, 0x17

    .line 283
    .line 284
    if-lt v0, v7, :cond_d

    .line 285
    .line 286
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzt:Landroid/media/AudioTrack;

    .line 287
    .line 288
    invoke-static {v0, v6}, Lcom/google/android/gms/internal/ads/zzpo;->zza(Landroid/media/AudioTrack;Lcom/google/android/gms/internal/ads/zzpq;)V

    .line 289
    .line 290
    .line 291
    :cond_d
    const/4 v6, 0x1

    .line 292
    iput-boolean v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzG:Z
    :try_end_5
    .catch Lcom/google/android/gms/internal/ads/zzpa; {:try_start_5 .. :try_end_5} :catch_3

    .line 293
    .line 294
    :goto_5
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzl:Lcom/google/android/gms/internal/ads/zzpx;

    .line 295
    .line 296
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzpx;->zza()V

    .line 297
    .line 298
    .line 299
    iget-boolean v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzG:Z

    .line 300
    .line 301
    const-wide/16 v6, 0x0

    .line 302
    .line 303
    if-eqz v0, :cond_e

    .line 304
    .line 305
    invoke-static {v6, v7, v3, v4}, Ljava/lang/Math;->max(JJ)J

    .line 306
    .line 307
    .line 308
    move-result-wide v8

    .line 309
    iput-wide v8, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzH:J

    .line 310
    .line 311
    const/4 v8, 0x0

    .line 312
    iput-boolean v8, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzF:Z

    .line 313
    .line 314
    iput-boolean v8, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzG:Z

    .line 315
    .line 316
    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/internal/ads/zzqe;->zzJ(J)V

    .line 317
    .line 318
    .line 319
    iget-boolean v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzQ:Z

    .line 320
    .line 321
    if-eqz v0, :cond_e

    .line 322
    .line 323
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzi()V

    .line 324
    .line 325
    .line 326
    :cond_e
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 327
    .line 328
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzH()J

    .line 329
    .line 330
    .line 331
    move-result-wide v8

    .line 332
    invoke-virtual {v0, v8, v9}, Lcom/google/android/gms/internal/ads/zzpi;->zzk(J)Z

    .line 333
    .line 334
    .line 335
    move-result v0

    .line 336
    if-nez v0, :cond_f

    .line 337
    .line 338
    const/4 v8, 0x0

    .line 339
    return v8

    .line 340
    :cond_f
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzJ:Ljava/nio/ByteBuffer;

    .line 341
    .line 342
    if-nez v0, :cond_26

    .line 343
    .line 344
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    .line 345
    .line 346
    .line 347
    move-result-object v0

    .line 348
    sget-object v8, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    .line 349
    .line 350
    if-ne v0, v8, :cond_10

    .line 351
    .line 352
    const/4 v0, 0x1

    .line 353
    goto :goto_6

    .line 354
    :cond_10
    const/4 v0, 0x0

    .line 355
    :goto_6
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzdy;->zzd(Z)V

    .line 356
    .line 357
    .line 358
    invoke-virtual/range {p1 .. p1}, Ljava/nio/Buffer;->hasRemaining()Z

    .line 359
    .line 360
    .line 361
    move-result v0

    .line 362
    if-nez v0, :cond_11

    .line 363
    .line 364
    const/4 v8, 0x1

    .line 365
    return v8

    .line 366
    :cond_11
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 367
    .line 368
    iget v8, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzc:I

    .line 369
    .line 370
    if-eqz v8, :cond_1e

    .line 371
    .line 372
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzE:I

    .line 373
    .line 374
    if-nez v8, :cond_1e

    .line 375
    .line 376
    iget v0, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzg:I

    .line 377
    .line 378
    const/4 v8, -0x2

    .line 379
    const/16 v9, 0x10

    .line 380
    .line 381
    const/16 v10, 0x400

    .line 382
    .line 383
    const/4 v11, -0x1

    .line 384
    packed-switch v0, :pswitch_data_0

    .line 385
    .line 386
    .line 387
    :pswitch_0
    new-instance v2, Ljava/lang/IllegalStateException;

    .line 388
    .line 389
    new-instance v3, Ljava/lang/StringBuilder;

    .line 390
    .line 391
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 392
    .line 393
    .line 394
    const-string v4, "Unexpected audio encoding: "

    .line 395
    .line 396
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    .line 398
    .line 399
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 400
    .line 401
    .line 402
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 403
    .line 404
    .line 405
    move-result-object v0

    .line 406
    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 407
    .line 408
    .line 409
    throw v2

    .line 410
    :pswitch_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/internal/ads/zzabw;->zza(Ljava/nio/ByteBuffer;)I

    .line 411
    .line 412
    .line 413
    move-result v0

    .line 414
    goto/16 :goto_b

    .line 415
    .line 416
    :pswitch_2
    sget v0, Lcom/google/android/gms/internal/ads/zzaaf;->zza:I

    .line 417
    .line 418
    new-array v0, v9, [B

    .line 419
    .line 420
    invoke-virtual/range {p1 .. p1}, Ljava/nio/Buffer;->position()I

    .line 421
    .line 422
    .line 423
    move-result v8

    .line 424
    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 425
    .line 426
    .line 427
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 428
    .line 429
    .line 430
    new-instance v8, Lcom/google/android/gms/internal/ads/zzfa;

    .line 431
    .line 432
    invoke-direct {v8, v0, v9}, Lcom/google/android/gms/internal/ads/zzfa;-><init>([BI)V

    .line 433
    .line 434
    .line 435
    invoke-static {v8}, Lcom/google/android/gms/internal/ads/zzaaf;->zza(Lcom/google/android/gms/internal/ads/zzfa;)Lcom/google/android/gms/internal/ads/zzaae;

    .line 436
    .line 437
    .line 438
    move-result-object v0

    .line 439
    iget v0, v0, Lcom/google/android/gms/internal/ads/zzaae;->zzc:I

    .line 440
    .line 441
    goto/16 :goto_b

    .line 442
    .line 443
    :pswitch_3
    const/16 v0, 0x200

    .line 444
    .line 445
    goto :goto_b

    .line 446
    :pswitch_4
    sget v0, Lcom/google/android/gms/internal/ads/zzaac;->zza:I

    .line 447
    .line 448
    invoke-virtual/range {p1 .. p1}, Ljava/nio/Buffer;->position()I

    .line 449
    .line 450
    .line 451
    move-result v0

    .line 452
    invoke-virtual/range {p1 .. p1}, Ljava/nio/Buffer;->limit()I

    .line 453
    .line 454
    .line 455
    move-result v10

    .line 456
    add-int/lit8 v10, v10, -0xa

    .line 457
    .line 458
    move v12, v0

    .line 459
    :goto_7
    if-gt v12, v10, :cond_13

    .line 460
    .line 461
    add-int/lit8 v13, v12, 0x4

    .line 462
    .line 463
    invoke-static {v2, v13}, Lcom/google/android/gms/internal/ads/zzfk;->zzh(Ljava/nio/ByteBuffer;I)I

    .line 464
    .line 465
    .line 466
    move-result v13

    .line 467
    and-int/2addr v13, v8

    .line 468
    const v14, -0x78d9046

    .line 469
    .line 470
    .line 471
    if-ne v13, v14, :cond_12

    .line 472
    .line 473
    sub-int/2addr v12, v0

    .line 474
    goto :goto_8

    .line 475
    :cond_12
    add-int/lit8 v12, v12, 0x1

    .line 476
    .line 477
    goto :goto_7

    .line 478
    :cond_13
    const/4 v12, -0x1

    .line 479
    :goto_8
    if-ne v12, v11, :cond_14

    .line 480
    .line 481
    const/4 v0, 0x0

    .line 482
    goto :goto_b

    .line 483
    :cond_14
    invoke-virtual/range {p1 .. p1}, Ljava/nio/Buffer;->position()I

    .line 484
    .line 485
    .line 486
    move-result v0

    .line 487
    add-int/2addr v0, v12

    .line 488
    add-int/lit8 v0, v0, 0x7

    .line 489
    .line 490
    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    .line 491
    .line 492
    .line 493
    move-result v0

    .line 494
    and-int/lit16 v0, v0, 0xff

    .line 495
    .line 496
    invoke-virtual/range {p1 .. p1}, Ljava/nio/Buffer;->position()I

    .line 497
    .line 498
    .line 499
    move-result v8

    .line 500
    add-int/2addr v8, v12

    .line 501
    const/16 v10, 0xbb

    .line 502
    .line 503
    if-ne v0, v10, :cond_15

    .line 504
    .line 505
    const/16 v0, 0x9

    .line 506
    .line 507
    goto :goto_9

    .line 508
    :cond_15
    const/16 v0, 0x8

    .line 509
    .line 510
    :goto_9
    add-int/2addr v8, v0

    .line 511
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->get(I)B

    .line 512
    .line 513
    .line 514
    move-result v0

    .line 515
    shr-int/lit8 v0, v0, 0x4

    .line 516
    .line 517
    and-int/lit8 v0, v0, 0x7

    .line 518
    .line 519
    const/16 v8, 0x28

    .line 520
    .line 521
    shl-int v0, v8, v0

    .line 522
    .line 523
    mul-int/lit8 v0, v0, 0x10

    .line 524
    .line 525
    goto :goto_b

    .line 526
    :pswitch_5
    const/16 v0, 0x800

    .line 527
    .line 528
    goto :goto_b

    .line 529
    :goto_a
    :pswitch_6
    const/16 v0, 0x400

    .line 530
    .line 531
    goto :goto_b

    .line 532
    :pswitch_7
    invoke-virtual/range {p1 .. p1}, Ljava/nio/Buffer;->position()I

    .line 533
    .line 534
    .line 535
    move-result v0

    .line 536
    invoke-static {v2, v0}, Lcom/google/android/gms/internal/ads/zzfk;->zzh(Ljava/nio/ByteBuffer;I)I

    .line 537
    .line 538
    .line 539
    move-result v0

    .line 540
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzabv;->zzc(I)I

    .line 541
    .line 542
    .line 543
    move-result v0

    .line 544
    if-eq v0, v11, :cond_16

    .line 545
    .line 546
    :goto_b
    const/4 v11, 0x1

    .line 547
    goto/16 :goto_f

    .line 548
    .line 549
    :cond_16
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 550
    .line 551
    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    .line 552
    .line 553
    .line 554
    throw v0

    .line 555
    :pswitch_8
    sget v0, Lcom/google/android/gms/internal/ads/zzaay;->zza:I

    .line 556
    .line 557
    const/4 v9, 0x0

    .line 558
    invoke-virtual {v2, v9}, Ljava/nio/ByteBuffer;->getInt(I)I

    .line 559
    .line 560
    .line 561
    move-result v0

    .line 562
    const v12, -0xde4bec0

    .line 563
    .line 564
    .line 565
    if-eq v0, v12, :cond_1c

    .line 566
    .line 567
    invoke-virtual {v2, v9}, Ljava/nio/ByteBuffer;->getInt(I)I

    .line 568
    .line 569
    .line 570
    move-result v0

    .line 571
    const v12, -0x17bd3b8f

    .line 572
    .line 573
    .line 574
    if-ne v0, v12, :cond_17

    .line 575
    .line 576
    goto :goto_a

    .line 577
    :cond_17
    invoke-virtual {v2, v9}, Ljava/nio/ByteBuffer;->getInt(I)I

    .line 578
    .line 579
    .line 580
    move-result v0

    .line 581
    const v9, 0x25205864

    .line 582
    .line 583
    .line 584
    if-ne v0, v9, :cond_18

    .line 585
    .line 586
    const/16 v0, 0x1000

    .line 587
    .line 588
    goto :goto_b

    .line 589
    :cond_18
    invoke-virtual/range {p1 .. p1}, Ljava/nio/Buffer;->position()I

    .line 590
    .line 591
    .line 592
    move-result v0

    .line 593
    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    .line 594
    .line 595
    .line 596
    move-result v9

    .line 597
    if-eq v9, v8, :cond_1b

    .line 598
    .line 599
    if-eq v9, v11, :cond_1a

    .line 600
    .line 601
    const/16 v8, 0x1f

    .line 602
    .line 603
    if-eq v9, v8, :cond_19

    .line 604
    .line 605
    add-int/lit8 v8, v0, 0x4

    .line 606
    .line 607
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->get(I)B

    .line 608
    .line 609
    .line 610
    move-result v8

    .line 611
    const/4 v9, 0x1

    .line 612
    and-int/2addr v8, v9

    .line 613
    shl-int/lit8 v8, v8, 0x6

    .line 614
    .line 615
    add-int/lit8 v0, v0, 0x5

    .line 616
    .line 617
    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    .line 618
    .line 619
    .line 620
    move-result v0

    .line 621
    and-int/lit16 v0, v0, 0xfc

    .line 622
    .line 623
    const/4 v9, 0x2

    .line 624
    goto :goto_d

    .line 625
    :cond_19
    const/4 v9, 0x2

    .line 626
    add-int/lit8 v8, v0, 0x5

    .line 627
    .line 628
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->get(I)B

    .line 629
    .line 630
    .line 631
    move-result v8

    .line 632
    and-int/lit8 v8, v8, 0x7

    .line 633
    .line 634
    shl-int/lit8 v8, v8, 0x4

    .line 635
    .line 636
    add-int/lit8 v0, v0, 0x6

    .line 637
    .line 638
    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    .line 639
    .line 640
    .line 641
    move-result v0

    .line 642
    goto :goto_c

    .line 643
    :cond_1a
    const/4 v9, 0x2

    .line 644
    add-int/lit8 v8, v0, 0x4

    .line 645
    .line 646
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->get(I)B

    .line 647
    .line 648
    .line 649
    move-result v8

    .line 650
    and-int/lit8 v8, v8, 0x7

    .line 651
    .line 652
    shl-int/lit8 v8, v8, 0x4

    .line 653
    .line 654
    add-int/lit8 v0, v0, 0x7

    .line 655
    .line 656
    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    .line 657
    .line 658
    .line 659
    move-result v0

    .line 660
    :goto_c
    and-int/lit8 v0, v0, 0x3c

    .line 661
    .line 662
    :goto_d
    shr-int/2addr v0, v9

    .line 663
    or-int/2addr v0, v8

    .line 664
    const/4 v11, 0x1

    .line 665
    goto :goto_e

    .line 666
    :cond_1b
    const/4 v9, 0x2

    .line 667
    add-int/lit8 v8, v0, 0x5

    .line 668
    .line 669
    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->get(I)B

    .line 670
    .line 671
    .line 672
    move-result v8

    .line 673
    const/4 v11, 0x1

    .line 674
    and-int/2addr v8, v11

    .line 675
    shl-int/lit8 v8, v8, 0x6

    .line 676
    .line 677
    add-int/lit8 v0, v0, 0x4

    .line 678
    .line 679
    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    .line 680
    .line 681
    .line 682
    move-result v0

    .line 683
    and-int/lit16 v0, v0, 0xfc

    .line 684
    .line 685
    shr-int/2addr v0, v9

    .line 686
    or-int/2addr v0, v8

    .line 687
    :goto_e
    add-int/2addr v0, v11

    .line 688
    mul-int/lit8 v0, v0, 0x20

    .line 689
    .line 690
    goto :goto_f

    .line 691
    :cond_1c
    const/4 v11, 0x1

    .line 692
    const/16 v0, 0x400

    .line 693
    .line 694
    goto :goto_f

    .line 695
    :pswitch_9
    const/4 v11, 0x1

    .line 696
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/internal/ads/zzaac;->zza(Ljava/nio/ByteBuffer;)I

    .line 697
    .line 698
    .line 699
    move-result v0

    .line 700
    :goto_f
    iput v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzE:I

    .line 701
    .line 702
    if-eqz v0, :cond_1d

    .line 703
    .line 704
    goto :goto_10

    .line 705
    :cond_1d
    return v11

    .line 706
    :cond_1e
    :goto_10
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzw:Lcom/google/android/gms/internal/ads/zzpw;

    .line 707
    .line 708
    if-eqz v0, :cond_20

    .line 709
    .line 710
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzQ()Z

    .line 711
    .line 712
    .line 713
    move-result v0

    .line 714
    if-nez v0, :cond_1f

    .line 715
    .line 716
    const/4 v8, 0x0

    .line 717
    return v8

    .line 718
    :cond_1f
    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/internal/ads/zzqe;->zzJ(J)V

    .line 719
    .line 720
    .line 721
    const/4 v8, 0x0

    .line 722
    iput-object v8, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzw:Lcom/google/android/gms/internal/ads/zzpw;

    .line 723
    .line 724
    :cond_20
    iget-wide v8, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzH:J

    .line 725
    .line 726
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 727
    .line 728
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzG()J

    .line 729
    .line 730
    .line 731
    move-result-wide v10

    .line 732
    iget-object v12, v1, Lcom/google/android/gms/internal/ads/zzqe;->zze:Lcom/google/android/gms/internal/ads/zzqo;

    .line 733
    .line 734
    invoke-virtual {v12}, Lcom/google/android/gms/internal/ads/zzqo;->zzo()J

    .line 735
    .line 736
    .line 737
    move-result-wide v12

    .line 738
    sub-long/2addr v10, v12

    .line 739
    iget-object v0, v0, Lcom/google/android/gms/internal/ads/zzpt;->zza:Lcom/google/android/gms/internal/ads/zzam;

    .line 740
    .line 741
    iget v0, v0, Lcom/google/android/gms/internal/ads/zzam;->zzA:I

    .line 742
    .line 743
    int-to-long v12, v0

    .line 744
    const-wide/32 v14, 0xf4240

    .line 745
    .line 746
    .line 747
    mul-long v10, v10, v14

    .line 748
    .line 749
    div-long/2addr v10, v12

    .line 750
    add-long/2addr v8, v10

    .line 751
    iget-boolean v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzF:Z

    .line 752
    .line 753
    if-nez v0, :cond_22

    .line 754
    .line 755
    sub-long v10, v8, v3

    .line 756
    .line 757
    invoke-static {v10, v11}, Ljava/lang/Math;->abs(J)J

    .line 758
    .line 759
    .line 760
    move-result-wide v10

    .line 761
    const-wide/32 v12, 0x30d40

    .line 762
    .line 763
    .line 764
    cmp-long v0, v10, v12

    .line 765
    .line 766
    if-lez v0, :cond_22

    .line 767
    .line 768
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzp:Lcom/google/android/gms/internal/ads/zzpb;

    .line 769
    .line 770
    if-eqz v0, :cond_21

    .line 771
    .line 772
    new-instance v10, Lcom/google/android/gms/internal/ads/zzpc;

    .line 773
    .line 774
    invoke-direct {v10, v3, v4, v8, v9}, Lcom/google/android/gms/internal/ads/zzpc;-><init>(JJ)V

    .line 775
    .line 776
    .line 777
    invoke-interface {v0, v10}, Lcom/google/android/gms/internal/ads/zzpb;->zza(Ljava/lang/Exception;)V

    .line 778
    .line 779
    .line 780
    :cond_21
    const/4 v10, 0x1

    .line 781
    iput-boolean v10, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzF:Z

    .line 782
    .line 783
    :cond_22
    iget-boolean v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzF:Z

    .line 784
    .line 785
    if-eqz v0, :cond_24

    .line 786
    .line 787
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzQ()Z

    .line 788
    .line 789
    .line 790
    move-result v0

    .line 791
    const/4 v10, 0x0

    .line 792
    if-nez v0, :cond_23

    .line 793
    .line 794
    return v10

    .line 795
    :cond_23
    sub-long v8, v3, v8

    .line 796
    .line 797
    iget-wide v11, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzH:J

    .line 798
    .line 799
    add-long/2addr v11, v8

    .line 800
    iput-wide v11, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzH:J

    .line 801
    .line 802
    iput-boolean v10, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzF:Z

    .line 803
    .line 804
    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/internal/ads/zzqe;->zzJ(J)V

    .line 805
    .line 806
    .line 807
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzp:Lcom/google/android/gms/internal/ads/zzpb;

    .line 808
    .line 809
    if-eqz v0, :cond_24

    .line 810
    .line 811
    cmp-long v10, v8, v6

    .line 812
    .line 813
    if-eqz v10, :cond_24

    .line 814
    .line 815
    check-cast v0, Lcom/google/android/gms/internal/ads/zzqj;

    .line 816
    .line 817
    iget-object v0, v0, Lcom/google/android/gms/internal/ads/zzqj;->zza:Lcom/google/android/gms/internal/ads/zzqk;

    .line 818
    .line 819
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzqk;->zzah()V

    .line 820
    .line 821
    .line 822
    :cond_24
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 823
    .line 824
    iget v0, v0, Lcom/google/android/gms/internal/ads/zzpt;->zzc:I

    .line 825
    .line 826
    if-nez v0, :cond_25

    .line 827
    .line 828
    iget-wide v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzA:J

    .line 829
    .line 830
    invoke-virtual/range {p1 .. p1}, Ljava/nio/Buffer;->remaining()I

    .line 831
    .line 832
    .line 833
    move-result v0

    .line 834
    int-to-long v8, v0

    .line 835
    add-long/2addr v6, v8

    .line 836
    iput-wide v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzA:J

    .line 837
    .line 838
    goto :goto_11

    .line 839
    :cond_25
    iget-wide v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzB:J

    .line 840
    .line 841
    iget v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzE:I

    .line 842
    .line 843
    int-to-long v8, v0

    .line 844
    int-to-long v10, v5

    .line 845
    mul-long v8, v8, v10

    .line 846
    .line 847
    add-long/2addr v6, v8

    .line 848
    iput-wide v6, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzB:J

    .line 849
    .line 850
    :goto_11
    iput-object v2, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzJ:Ljava/nio/ByteBuffer;

    .line 851
    .line 852
    iput v5, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzK:I

    .line 853
    .line 854
    :cond_26
    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/internal/ads/zzqe;->zzL(J)V

    .line 855
    .line 856
    .line 857
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzJ:Ljava/nio/ByteBuffer;

    .line 858
    .line 859
    invoke-virtual {v0}, Ljava/nio/Buffer;->hasRemaining()Z

    .line 860
    .line 861
    .line 862
    move-result v0

    .line 863
    if-nez v0, :cond_27

    .line 864
    .line 865
    const/4 v2, 0x0

    .line 866
    iput-object v2, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzJ:Ljava/nio/ByteBuffer;

    .line 867
    .line 868
    const/4 v2, 0x0

    .line 869
    iput v2, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzK:I

    .line 870
    .line 871
    const/4 v3, 0x1

    .line 872
    return v3

    .line 873
    :cond_27
    const/4 v2, 0x0

    .line 874
    const/4 v3, 0x1

    .line 875
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 876
    .line 877
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzH()J

    .line 878
    .line 879
    .line 880
    move-result-wide v4

    .line 881
    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/internal/ads/zzpi;->zzj(J)Z

    .line 882
    .line 883
    .line 884
    move-result v0

    .line 885
    if-eqz v0, :cond_28

    .line 886
    .line 887
    const-string v0, "DefaultAudioSink"

    .line 888
    .line 889
    const-string v2, "Resetting stalled audio track"

    .line 890
    .line 891
    invoke-static {v0, v2}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    .line 893
    .line 894
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzf()V

    .line 895
    .line 896
    .line 897
    return v3

    .line 898
    :cond_28
    return v2

    .line 899
    :catch_1
    move-exception v0

    .line 900
    :try_start_6
    const-string v2, "addSuppressed"

    .line 901
    .line 902
    const/4 v3, 0x1

    .line 903
    new-array v4, v3, [Ljava/lang/Class;

    .line 904
    .line 905
    const/4 v5, 0x0

    .line 906
    aput-object v6, v4, v5

    .line 907
    .line 908
    invoke-virtual {v6, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 909
    .line 910
    .line 911
    move-result-object v2

    .line 912
    new-array v4, v3, [Ljava/lang/Object;

    .line 913
    .line 914
    aput-object v0, v4, v5

    .line 915
    .line 916
    invoke-virtual {v2, v12, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .line 917
    .line 918
    .line 919
    :catch_2
    :cond_29
    :try_start_7
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzr:Lcom/google/android/gms/internal/ads/zzpt;

    .line 920
    .line 921
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzpt;->zzc()Z

    .line 922
    .line 923
    .line 924
    move-result v0

    .line 925
    if-eqz v0, :cond_2a

    .line 926
    .line 927
    const/4 v2, 0x1

    .line 928
    iput-boolean v2, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzW:Z

    .line 929
    .line 930
    :cond_2a
    throw v12
    :try_end_7
    .catch Lcom/google/android/gms/internal/ads/zzpa; {:try_start_7 .. :try_end_7} :catch_3

    .line 931
    :catch_3
    move-exception v0

    .line 932
    iget-boolean v2, v0, Lcom/google/android/gms/internal/ads/zzpa;->zzb:Z

    .line 933
    .line 934
    if-nez v2, :cond_2b

    .line 935
    .line 936
    iget-object v2, v1, Lcom/google/android/gms/internal/ads/zzqe;->zzl:Lcom/google/android/gms/internal/ads/zzpx;

    .line 937
    .line 938
    invoke-virtual {v2, v0}, Lcom/google/android/gms/internal/ads/zzpx;->zzb(Ljava/lang/Exception;)V

    .line 939
    .line 940
    .line 941
    const/4 v2, 0x0

    .line 942
    return v2

    .line 943
    :cond_2b
    throw v0

    .line 944
    nop

    .line 945
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_9
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_6
        :pswitch_2
        :pswitch_9
        :pswitch_0
        :pswitch_1
    .end packed-switch
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
.end method

.method public final zzw()Z
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzR()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzi:Lcom/google/android/gms/internal/ads/zzpi;

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzH()J

    .line 10
    .line 11
    .line 12
    move-result-wide v1

    .line 13
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/ads/zzpi;->zzh(J)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    return v0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method public final zzx()Z
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzR()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_2

    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzqe;->zzO:Z

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzqe;->zzw()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    return v2

    .line 21
    :cond_1
    const/4 v1, 0x0

    .line 22
    :cond_2
    :goto_0
    return v1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method public final zzy(Lcom/google/android/gms/internal/ads/zzam;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/ads/zzqe;->zza(Lcom/google/android/gms/internal/ads/zzam;)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x1

    .line 8
    return p1

    .line 9
    :cond_0
    const/4 p1, 0x0

    .line 10
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public interface abstract Lcom/google/android/gms/internal/ads/zzuc;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-ads@@22.4.0"


# virtual methods
.method public abstract zzac(ILcom/google/android/gms/internal/ads/zzts;Lcom/google/android/gms/internal/ads/zzto;)V
    .param p2    # Lcom/google/android/gms/internal/ads/zzts;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract zzad(ILcom/google/android/gms/internal/ads/zzts;Lcom/google/android/gms/internal/ads/zztj;Lcom/google/android/gms/internal/ads/zzto;)V
    .param p2    # Lcom/google/android/gms/internal/ads/zzts;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract zzae(ILcom/google/android/gms/internal/ads/zzts;Lcom/google/android/gms/internal/ads/zztj;Lcom/google/android/gms/internal/ads/zzto;)V
    .param p2    # Lcom/google/android/gms/internal/ads/zzts;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract zzaf(ILcom/google/android/gms/internal/ads/zzts;Lcom/google/android/gms/internal/ads/zztj;Lcom/google/android/gms/internal/ads/zzto;Ljava/io/IOException;Z)V
    .param p2    # Lcom/google/android/gms/internal/ads/zzts;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract zzag(ILcom/google/android/gms/internal/ads/zzts;Lcom/google/android/gms/internal/ads/zztj;Lcom/google/android/gms/internal/ads/zzto;)V
    .param p2    # Lcom/google/android/gms/internal/ads/zzts;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

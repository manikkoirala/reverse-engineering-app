.class final Lcom/google/android/gms/internal/ads/zzajs;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-ads@@22.4.0"


# instance fields
.field private final zza:Lcom/google/android/gms/internal/ads/zzfi;

.field private final zzb:Lcom/google/android/gms/internal/ads/zzfb;

.field private zzc:Z

.field private zzd:Z

.field private zze:Z

.field private zzf:J

.field private zzg:J

.field private zzh:J


# direct methods
.method constructor <init>(I)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance p1, Lcom/google/android/gms/internal/ads/zzfi;

    .line 5
    .line 6
    const-wide/16 v0, 0x0

    .line 7
    .line 8
    invoke-direct {p1, v0, v1}, Lcom/google/android/gms/internal/ads/zzfi;-><init>(J)V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzajs;->zza:Lcom/google/android/gms/internal/ads/zzfi;

    .line 12
    .line 13
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    iput-wide v0, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzf:J

    .line 19
    .line 20
    iput-wide v0, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzg:J

    .line 21
    .line 22
    iput-wide v0, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzh:J

    .line 23
    .line 24
    new-instance p1, Lcom/google/android/gms/internal/ads/zzfb;

    .line 25
    .line 26
    invoke-direct {p1}, Lcom/google/android/gms/internal/ads/zzfb;-><init>()V

    .line 27
    .line 28
    .line 29
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzb:Lcom/google/android/gms/internal/ads/zzfb;

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final zze(Lcom/google/android/gms/internal/ads/zzabc;)I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzb:Lcom/google/android/gms/internal/ads/zzfb;

    .line 2
    .line 3
    sget-object v1, Lcom/google/android/gms/internal/ads/zzfk;->zzf:[B

    .line 4
    .line 5
    array-length v2, v1

    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzE([BI)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzc:Z

    .line 12
    .line 13
    invoke-interface {p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzj()V

    .line 14
    .line 15
    .line 16
    return v2
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final zza(Lcom/google/android/gms/internal/ads/zzabc;Lcom/google/android/gms/internal/ads/zzabx;I)I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-gtz p3, :cond_0

    .line 3
    .line 4
    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/ads/zzajs;->zze(Lcom/google/android/gms/internal/ads/zzabc;)I

    .line 5
    .line 6
    .line 7
    return v0

    .line 8
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/internal/ads/zzajs;->zze:Z

    .line 9
    .line 10
    const/16 v2, 0x47

    .line 11
    .line 12
    const-wide/32 v3, 0x1b8a0

    .line 13
    .line 14
    .line 15
    const/4 v5, 0x1

    .line 16
    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    if-nez v1, :cond_7

    .line 22
    .line 23
    invoke-interface {p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzd()J

    .line 24
    .line 25
    .line 26
    move-result-wide v8

    .line 27
    invoke-static {v3, v4, v8, v9}, Ljava/lang/Math;->min(JJ)J

    .line 28
    .line 29
    .line 30
    move-result-wide v3

    .line 31
    long-to-int v1, v3

    .line 32
    int-to-long v3, v1

    .line 33
    sub-long/2addr v8, v3

    .line 34
    invoke-interface {p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzf()J

    .line 35
    .line 36
    .line 37
    move-result-wide v3

    .line 38
    cmp-long v10, v3, v8

    .line 39
    .line 40
    if-eqz v10, :cond_1

    .line 41
    .line 42
    iput-wide v8, p2, Lcom/google/android/gms/internal/ads/zzabx;->zza:J

    .line 43
    .line 44
    const/4 v0, 0x1

    .line 45
    goto :goto_4

    .line 46
    :cond_1
    iget-object p2, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzb:Lcom/google/android/gms/internal/ads/zzfb;

    .line 47
    .line 48
    invoke-virtual {p2, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzD(I)V

    .line 49
    .line 50
    .line 51
    invoke-interface {p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzj()V

    .line 52
    .line 53
    .line 54
    iget-object p2, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzb:Lcom/google/android/gms/internal/ads/zzfb;

    .line 55
    .line 56
    invoke-virtual {p2}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 57
    .line 58
    .line 59
    move-result-object p2

    .line 60
    check-cast p1, Lcom/google/android/gms/internal/ads/zzaar;

    .line 61
    .line 62
    invoke-virtual {p1, p2, v0, v1, v0}, Lcom/google/android/gms/internal/ads/zzaar;->zzm([BIIZ)Z

    .line 63
    .line 64
    .line 65
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzb:Lcom/google/android/gms/internal/ads/zzfb;

    .line 66
    .line 67
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 68
    .line 69
    .line 70
    move-result p2

    .line 71
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzd()I

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    add-int/lit16 v3, v1, -0xbc

    .line 76
    .line 77
    :goto_0
    if-lt v3, p2, :cond_6

    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 80
    .line 81
    .line 82
    move-result-object v4

    .line 83
    const/4 v8, -0x4

    .line 84
    const/4 v9, 0x0

    .line 85
    :goto_1
    const/4 v10, 0x4

    .line 86
    if-gt v8, v10, :cond_5

    .line 87
    .line 88
    mul-int/lit16 v10, v8, 0xbc

    .line 89
    .line 90
    add-int/2addr v10, v3

    .line 91
    if-lt v10, p2, :cond_3

    .line 92
    .line 93
    if-ge v10, v1, :cond_3

    .line 94
    .line 95
    aget-byte v10, v4, v10

    .line 96
    .line 97
    if-eq v10, v2, :cond_2

    .line 98
    .line 99
    goto :goto_2

    .line 100
    :cond_2
    add-int/2addr v9, v5

    .line 101
    const/4 v10, 0x5

    .line 102
    if-ne v9, v10, :cond_4

    .line 103
    .line 104
    invoke-static {p1, v3, p3}, Lcom/google/android/gms/internal/ads/zzakc;->zzb(Lcom/google/android/gms/internal/ads/zzfb;II)J

    .line 105
    .line 106
    .line 107
    move-result-wide v8

    .line 108
    cmp-long v4, v8, v6

    .line 109
    .line 110
    if-eqz v4, :cond_5

    .line 111
    .line 112
    move-wide v6, v8

    .line 113
    goto :goto_3

    .line 114
    :cond_3
    :goto_2
    const/4 v9, 0x0

    .line 115
    :cond_4
    add-int/lit8 v8, v8, 0x1

    .line 116
    .line 117
    goto :goto_1

    .line 118
    :cond_5
    add-int/lit8 v3, v3, -0x1

    .line 119
    .line 120
    goto :goto_0

    .line 121
    :cond_6
    :goto_3
    iput-wide v6, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzg:J

    .line 122
    .line 123
    iput-boolean v5, p0, Lcom/google/android/gms/internal/ads/zzajs;->zze:Z

    .line 124
    .line 125
    :goto_4
    return v0

    .line 126
    :cond_7
    iget-wide v8, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzg:J

    .line 127
    .line 128
    cmp-long v1, v8, v6

    .line 129
    .line 130
    if-nez v1, :cond_8

    .line 131
    .line 132
    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/ads/zzajs;->zze(Lcom/google/android/gms/internal/ads/zzabc;)I

    .line 133
    .line 134
    .line 135
    return v0

    .line 136
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzd:Z

    .line 137
    .line 138
    const-wide/16 v8, 0x0

    .line 139
    .line 140
    if-nez v1, :cond_d

    .line 141
    .line 142
    invoke-interface {p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzd()J

    .line 143
    .line 144
    .line 145
    move-result-wide v10

    .line 146
    invoke-static {v3, v4, v10, v11}, Ljava/lang/Math;->min(JJ)J

    .line 147
    .line 148
    .line 149
    move-result-wide v3

    .line 150
    long-to-int v1, v3

    .line 151
    invoke-interface {p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzf()J

    .line 152
    .line 153
    .line 154
    move-result-wide v3

    .line 155
    cmp-long v10, v3, v8

    .line 156
    .line 157
    if-eqz v10, :cond_9

    .line 158
    .line 159
    iput-wide v8, p2, Lcom/google/android/gms/internal/ads/zzabx;->zza:J

    .line 160
    .line 161
    const/4 v0, 0x1

    .line 162
    goto :goto_8

    .line 163
    :cond_9
    iget-object p2, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzb:Lcom/google/android/gms/internal/ads/zzfb;

    .line 164
    .line 165
    invoke-virtual {p2, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzD(I)V

    .line 166
    .line 167
    .line 168
    invoke-interface {p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzj()V

    .line 169
    .line 170
    .line 171
    iget-object p2, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzb:Lcom/google/android/gms/internal/ads/zzfb;

    .line 172
    .line 173
    invoke-virtual {p2}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 174
    .line 175
    .line 176
    move-result-object p2

    .line 177
    check-cast p1, Lcom/google/android/gms/internal/ads/zzaar;

    .line 178
    .line 179
    invoke-virtual {p1, p2, v0, v1, v0}, Lcom/google/android/gms/internal/ads/zzaar;->zzm([BIIZ)Z

    .line 180
    .line 181
    .line 182
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzb:Lcom/google/android/gms/internal/ads/zzfb;

    .line 183
    .line 184
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 185
    .line 186
    .line 187
    move-result p2

    .line 188
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzd()I

    .line 189
    .line 190
    .line 191
    move-result v1

    .line 192
    :goto_5
    if-ge p2, v1, :cond_c

    .line 193
    .line 194
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 195
    .line 196
    .line 197
    move-result-object v3

    .line 198
    aget-byte v3, v3, p2

    .line 199
    .line 200
    if-eq v3, v2, :cond_a

    .line 201
    .line 202
    goto :goto_6

    .line 203
    :cond_a
    invoke-static {p1, p2, p3}, Lcom/google/android/gms/internal/ads/zzakc;->zzb(Lcom/google/android/gms/internal/ads/zzfb;II)J

    .line 204
    .line 205
    .line 206
    move-result-wide v3

    .line 207
    cmp-long v8, v3, v6

    .line 208
    .line 209
    if-eqz v8, :cond_b

    .line 210
    .line 211
    move-wide v6, v3

    .line 212
    goto :goto_7

    .line 213
    :cond_b
    :goto_6
    add-int/lit8 p2, p2, 0x1

    .line 214
    .line 215
    goto :goto_5

    .line 216
    :cond_c
    :goto_7
    iput-wide v6, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzf:J

    .line 217
    .line 218
    iput-boolean v5, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzd:Z

    .line 219
    .line 220
    :goto_8
    return v0

    .line 221
    :cond_d
    iget-wide p2, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzf:J

    .line 222
    .line 223
    cmp-long v1, p2, v6

    .line 224
    .line 225
    if-nez v1, :cond_e

    .line 226
    .line 227
    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/ads/zzajs;->zze(Lcom/google/android/gms/internal/ads/zzabc;)I

    .line 228
    .line 229
    .line 230
    return v0

    .line 231
    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzajs;->zza:Lcom/google/android/gms/internal/ads/zzfi;

    .line 232
    .line 233
    invoke-virtual {v1, p2, p3}, Lcom/google/android/gms/internal/ads/zzfi;->zzb(J)J

    .line 234
    .line 235
    .line 236
    move-result-wide p2

    .line 237
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzajs;->zza:Lcom/google/android/gms/internal/ads/zzfi;

    .line 238
    .line 239
    iget-wide v2, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzg:J

    .line 240
    .line 241
    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/internal/ads/zzfi;->zzb(J)J

    .line 242
    .line 243
    .line 244
    move-result-wide v1

    .line 245
    sub-long/2addr v1, p2

    .line 246
    iput-wide v1, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzh:J

    .line 247
    .line 248
    cmp-long p2, v1, v8

    .line 249
    .line 250
    if-gez p2, :cond_f

    .line 251
    .line 252
    new-instance p2, Ljava/lang/StringBuilder;

    .line 253
    .line 254
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 255
    .line 256
    .line 257
    const-string p3, "Invalid duration: "

    .line 258
    .line 259
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    .line 261
    .line 262
    invoke-virtual {p2, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 263
    .line 264
    .line 265
    const-string p3, ". Using TIME_UNSET instead."

    .line 266
    .line 267
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    .line 269
    .line 270
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 271
    .line 272
    .line 273
    move-result-object p2

    .line 274
    const-string p3, "TsDurationReader"

    .line 275
    .line 276
    invoke-static {p3, p2}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    .line 278
    .line 279
    iput-wide v6, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzh:J

    .line 280
    .line 281
    :cond_f
    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/ads/zzajs;->zze(Lcom/google/android/gms/internal/ads/zzabc;)I

    .line 282
    .line 283
    .line 284
    return v0
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
.end method

.method public final zzb()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzh:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final zzc()Lcom/google/android/gms/internal/ads/zzfi;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzajs;->zza:Lcom/google/android/gms/internal/ads/zzfi;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final zzd()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzajs;->zzc:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

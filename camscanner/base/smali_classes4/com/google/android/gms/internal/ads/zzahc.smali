.class public final Lcom/google/android/gms/internal/ads/zzahc;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-ads@@22.4.0"

# interfaces
.implements Lcom/google/android/gms/internal/ads/zzabb;
.implements Lcom/google/android/gms/internal/ads/zzaca;


# static fields
.field public static final zza:Lcom/google/android/gms/internal/ads/zzabi;


# instance fields
.field private final zzb:Lcom/google/android/gms/internal/ads/zzfb;

.field private final zzc:Lcom/google/android/gms/internal/ads/zzfb;

.field private final zzd:Lcom/google/android/gms/internal/ads/zzfb;

.field private final zze:Lcom/google/android/gms/internal/ads/zzfb;

.field private final zzf:Ljava/util/ArrayDeque;

.field private final zzg:Lcom/google/android/gms/internal/ads/zzahe;

.field private final zzh:Ljava/util/List;

.field private zzi:I

.field private zzj:I

.field private zzk:J

.field private zzl:I

.field private zzm:Lcom/google/android/gms/internal/ads/zzfb;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private zzn:I

.field private zzo:I

.field private zzp:I

.field private zzq:I

.field private zzr:Lcom/google/android/gms/internal/ads/zzabe;

.field private zzs:[Lcom/google/android/gms/internal/ads/zzahb;

.field private zzt:[[J

.field private zzu:I

.field private zzv:J

.field private zzw:I

.field private zzx:Lcom/google/android/gms/internal/ads/zzaff;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/internal/ads/zzagz;->zza:Lcom/google/android/gms/internal/ads/zzagz;

    .line 2
    .line 3
    sput-object v0, Lcom/google/android/gms/internal/ads/zzahc;->zza:Lcom/google/android/gms/internal/ads/zzabi;

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/ads/zzahc;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzi:I

    new-instance v0, Lcom/google/android/gms/internal/ads/zzahe;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ads/zzahe;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzg:Lcom/google/android/gms/internal/ads/zzahe;

    new-instance v0, Ljava/util/ArrayList;

    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzh:Ljava/util/List;

    .line 4
    new-instance v0, Lcom/google/android/gms/internal/ads/zzfb;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ads/zzfb;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zze:Lcom/google/android/gms/internal/ads/zzfb;

    new-instance v0, Ljava/util/ArrayDeque;

    .line 5
    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    new-instance v0, Lcom/google/android/gms/internal/ads/zzfb;

    .line 6
    sget-object v1, Lcom/google/android/gms/internal/ads/zzfy;->zza:[B

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ads/zzfb;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzb:Lcom/google/android/gms/internal/ads/zzfb;

    new-instance v0, Lcom/google/android/gms/internal/ads/zzfb;

    const/4 v1, 0x4

    .line 7
    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ads/zzfb;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzc:Lcom/google/android/gms/internal/ads/zzfb;

    new-instance v0, Lcom/google/android/gms/internal/ads/zzfb;

    .line 8
    invoke-direct {v0}, Lcom/google/android/gms/internal/ads/zzfb;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzd:Lcom/google/android/gms/internal/ads/zzfb;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzn:I

    sget-object v0, Lcom/google/android/gms/internal/ads/zzabe;->zza:Lcom/google/android/gms/internal/ads/zzabe;

    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzr:Lcom/google/android/gms/internal/ads/zzabe;

    new-array p1, p1, [Lcom/google/android/gms/internal/ads/zzahb;

    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzs:[Lcom/google/android/gms/internal/ads/zzahb;

    return-void
.end method

.method private static zzf(I)I
    .locals 1

    .line 1
    const v0, 0x68656963

    .line 2
    .line 3
    .line 4
    if-eq p0, v0, :cond_1

    .line 5
    .line 6
    const v0, 0x71742020

    .line 7
    .line 8
    .line 9
    if-eq p0, v0, :cond_0

    .line 10
    .line 11
    const/4 p0, 0x0

    .line 12
    return p0

    .line 13
    :cond_0
    const/4 p0, 0x1

    .line 14
    return p0

    .line 15
    :cond_1
    const/4 p0, 0x2

    .line 16
    return p0
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static zzi(Lcom/google/android/gms/internal/ads/zzahj;J)I
    .locals 2

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/internal/ads/zzahj;->zza(J)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, -0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/internal/ads/zzahj;->zzb(J)I

    .line 9
    .line 10
    .line 11
    move-result p0

    .line 12
    return p0

    .line 13
    :cond_0
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
.end method

.method private static zzj(Lcom/google/android/gms/internal/ads/zzahj;JJ)J
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/ads/zzahc;->zzi(Lcom/google/android/gms/internal/ads/zzahj;J)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const/4 p2, -0x1

    .line 6
    if-ne p1, p2, :cond_0

    .line 7
    .line 8
    return-wide p3

    .line 9
    :cond_0
    iget-object p0, p0, Lcom/google/android/gms/internal/ads/zzahj;->zzc:[J

    .line 10
    .line 11
    aget-wide p1, p0, p1

    .line 12
    .line 13
    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->min(JJ)J

    .line 14
    .line 15
    .line 16
    move-result-wide p0

    .line 17
    return-wide p0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
.end method

.method private final zzk()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzi:I

    .line 3
    .line 4
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private final zzl(J)V
    .locals 27
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzcd;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    :cond_0
    :goto_0
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    .line 4
    .line 5
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_19

    .line 10
    .line 11
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    .line 12
    .line 13
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/google/android/gms/internal/ads/zzagh;

    .line 18
    .line 19
    iget-wide v3, v1, Lcom/google/android/gms/internal/ads/zzagh;->zza:J

    .line 20
    .line 21
    cmp-long v1, v3, p1

    .line 22
    .line 23
    if-nez v1, :cond_19

    .line 24
    .line 25
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    move-object v3, v1

    .line 32
    check-cast v3, Lcom/google/android/gms/internal/ads/zzagh;

    .line 33
    .line 34
    iget v1, v3, Lcom/google/android/gms/internal/ads/zzagj;->zzd:I

    .line 35
    .line 36
    const v4, 0x6d6f6f76

    .line 37
    .line 38
    .line 39
    if-ne v1, v4, :cond_18

    .line 40
    .line 41
    new-instance v1, Ljava/util/ArrayList;

    .line 42
    .line 43
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .line 45
    .line 46
    iget v4, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzw:I

    .line 47
    .line 48
    new-instance v11, Lcom/google/android/gms/internal/ads/zzabq;

    .line 49
    .line 50
    invoke-direct {v11}, Lcom/google/android/gms/internal/ads/zzabq;-><init>()V

    .line 51
    .line 52
    .line 53
    const v5, 0x75647461

    .line 54
    .line 55
    .line 56
    invoke-virtual {v3, v5}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    .line 57
    .line 58
    .line 59
    move-result-object v5

    .line 60
    if-eqz v5, :cond_1

    .line 61
    .line 62
    invoke-static {v5}, Lcom/google/android/gms/internal/ads/zzagr;->zzb(Lcom/google/android/gms/internal/ads/zzagi;)Lcom/google/android/gms/internal/ads/zzbz;

    .line 63
    .line 64
    .line 65
    move-result-object v5

    .line 66
    invoke-virtual {v11, v5}, Lcom/google/android/gms/internal/ads/zzabq;->zzb(Lcom/google/android/gms/internal/ads/zzbz;)Z

    .line 67
    .line 68
    .line 69
    move-object v13, v5

    .line 70
    goto :goto_1

    .line 71
    :cond_1
    const/4 v13, 0x0

    .line 72
    :goto_1
    const v5, 0x6d657461

    .line 73
    .line 74
    .line 75
    invoke-virtual {v3, v5}, Lcom/google/android/gms/internal/ads/zzagh;->zza(I)Lcom/google/android/gms/internal/ads/zzagh;

    .line 76
    .line 77
    .line 78
    move-result-object v5

    .line 79
    if-eqz v5, :cond_2

    .line 80
    .line 81
    invoke-static {v5}, Lcom/google/android/gms/internal/ads/zzagr;->zza(Lcom/google/android/gms/internal/ads/zzagh;)Lcom/google/android/gms/internal/ads/zzbz;

    .line 82
    .line 83
    .line 84
    move-result-object v5

    .line 85
    move-object v14, v5

    .line 86
    goto :goto_2

    .line 87
    :cond_2
    const/4 v14, 0x0

    .line 88
    :goto_2
    new-instance v15, Lcom/google/android/gms/internal/ads/zzbz;

    .line 89
    .line 90
    const/4 v10, 0x1

    .line 91
    new-array v5, v10, [Lcom/google/android/gms/internal/ads/zzby;

    .line 92
    .line 93
    const v6, 0x6d766864

    .line 94
    .line 95
    .line 96
    invoke-virtual {v3, v6}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    .line 97
    .line 98
    .line 99
    move-result-object v6

    .line 100
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 101
    .line 102
    .line 103
    const/4 v9, 0x0

    .line 104
    if-ne v4, v10, :cond_3

    .line 105
    .line 106
    const/16 v16, 0x1

    .line 107
    .line 108
    goto :goto_3

    .line 109
    :cond_3
    const/16 v16, 0x0

    .line 110
    .line 111
    :goto_3
    iget-object v4, v6, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    .line 112
    .line 113
    invoke-static {v4}, Lcom/google/android/gms/internal/ads/zzagr;->zzc(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzfu;

    .line 114
    .line 115
    .line 116
    move-result-object v4

    .line 117
    aput-object v4, v5, v9

    .line 118
    .line 119
    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    invoke-direct {v15, v7, v8, v5}, Lcom/google/android/gms/internal/ads/zzbz;-><init>(J[Lcom/google/android/gms/internal/ads/zzby;)V

    .line 125
    .line 126
    .line 127
    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    const/16 v17, 0x0

    .line 133
    .line 134
    const/16 v18, 0x0

    .line 135
    .line 136
    sget-object v19, Lcom/google/android/gms/internal/ads/zzaha;->zza:Lcom/google/android/gms/internal/ads/zzaha;

    .line 137
    .line 138
    move-object v4, v11

    .line 139
    move-object/from16 v20, v13

    .line 140
    .line 141
    move-wide v12, v7

    .line 142
    move-object/from16 v7, v17

    .line 143
    .line 144
    move/from16 v8, v18

    .line 145
    .line 146
    move/from16 v9, v16

    .line 147
    .line 148
    move-object/from16 v10, v19

    .line 149
    .line 150
    invoke-static/range {v3 .. v10}, Lcom/google/android/gms/internal/ads/zzagr;->zzd(Lcom/google/android/gms/internal/ads/zzagh;Lcom/google/android/gms/internal/ads/zzabq;JLcom/google/android/gms/internal/ads/zzad;ZZLcom/google/android/gms/internal/ads/zzfqw;)Ljava/util/List;

    .line 151
    .line 152
    .line 153
    move-result-object v3

    .line 154
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 155
    .line 156
    .line 157
    move-result v4

    .line 158
    move-wide v7, v12

    .line 159
    const/4 v6, -0x1

    .line 160
    const/4 v9, 0x0

    .line 161
    :goto_4
    const-wide/16 v16, 0x0

    .line 162
    .line 163
    if-ge v9, v4, :cond_12

    .line 164
    .line 165
    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 166
    .line 167
    .line 168
    move-result-object v10

    .line 169
    check-cast v10, Lcom/google/android/gms/internal/ads/zzahj;

    .line 170
    .line 171
    iget v5, v10, Lcom/google/android/gms/internal/ads/zzahj;->zzb:I

    .line 172
    .line 173
    if-nez v5, :cond_4

    .line 174
    .line 175
    move-object/from16 v22, v3

    .line 176
    .line 177
    move/from16 v24, v4

    .line 178
    .line 179
    move/from16 v21, v9

    .line 180
    .line 181
    const/4 v2, -0x1

    .line 182
    const/16 v17, 0x0

    .line 183
    .line 184
    goto/16 :goto_d

    .line 185
    .line 186
    :cond_4
    iget-object v5, v10, Lcom/google/android/gms/internal/ads/zzahj;->zza:Lcom/google/android/gms/internal/ads/zzahg;

    .line 187
    .line 188
    move-object/from16 v22, v3

    .line 189
    .line 190
    iget-wide v2, v5, Lcom/google/android/gms/internal/ads/zzahg;->zze:J

    .line 191
    .line 192
    cmp-long v23, v2, v12

    .line 193
    .line 194
    if-eqz v23, :cond_5

    .line 195
    .line 196
    goto :goto_5

    .line 197
    :cond_5
    iget-wide v2, v10, Lcom/google/android/gms/internal/ads/zzahj;->zzh:J

    .line 198
    .line 199
    :goto_5
    invoke-static {v7, v8, v2, v3}, Ljava/lang/Math;->max(JJ)J

    .line 200
    .line 201
    .line 202
    move-result-wide v7

    .line 203
    new-instance v12, Lcom/google/android/gms/internal/ads/zzahb;

    .line 204
    .line 205
    iget-object v13, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzr:Lcom/google/android/gms/internal/ads/zzabe;

    .line 206
    .line 207
    move/from16 v24, v4

    .line 208
    .line 209
    iget v4, v5, Lcom/google/android/gms/internal/ads/zzahg;->zzb:I

    .line 210
    .line 211
    invoke-interface {v13, v9, v4}, Lcom/google/android/gms/internal/ads/zzabe;->zzv(II)Lcom/google/android/gms/internal/ads/zzace;

    .line 212
    .line 213
    .line 214
    move-result-object v4

    .line 215
    invoke-direct {v12, v5, v10, v4}, Lcom/google/android/gms/internal/ads/zzahb;-><init>(Lcom/google/android/gms/internal/ads/zzahg;Lcom/google/android/gms/internal/ads/zzahj;Lcom/google/android/gms/internal/ads/zzace;)V

    .line 216
    .line 217
    .line 218
    iget-object v4, v5, Lcom/google/android/gms/internal/ads/zzahg;->zzf:Lcom/google/android/gms/internal/ads/zzam;

    .line 219
    .line 220
    iget-object v4, v4, Lcom/google/android/gms/internal/ads/zzam;->zzm:Ljava/lang/String;

    .line 221
    .line 222
    const-string v13, "audio/true-hd"

    .line 223
    .line 224
    invoke-virtual {v13, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 225
    .line 226
    .line 227
    move-result v4

    .line 228
    if-eqz v4, :cond_6

    .line 229
    .line 230
    iget v4, v10, Lcom/google/android/gms/internal/ads/zzahj;->zze:I

    .line 231
    .line 232
    mul-int/lit8 v4, v4, 0x10

    .line 233
    .line 234
    goto :goto_6

    .line 235
    :cond_6
    iget v4, v10, Lcom/google/android/gms/internal/ads/zzahj;->zze:I

    .line 236
    .line 237
    add-int/lit8 v4, v4, 0x1e

    .line 238
    .line 239
    :goto_6
    iget-object v13, v5, Lcom/google/android/gms/internal/ads/zzahg;->zzf:Lcom/google/android/gms/internal/ads/zzam;

    .line 240
    .line 241
    invoke-virtual {v13}, Lcom/google/android/gms/internal/ads/zzam;->zzb()Lcom/google/android/gms/internal/ads/zzak;

    .line 242
    .line 243
    .line 244
    move-result-object v13

    .line 245
    invoke-virtual {v13, v4}, Lcom/google/android/gms/internal/ads/zzak;->zzL(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 246
    .line 247
    .line 248
    iget v4, v5, Lcom/google/android/gms/internal/ads/zzahg;->zzb:I

    .line 249
    .line 250
    move-wide/from16 v25, v7

    .line 251
    .line 252
    const/4 v7, 0x2

    .line 253
    if-ne v4, v7, :cond_7

    .line 254
    .line 255
    cmp-long v4, v2, v16

    .line 256
    .line 257
    if-lez v4, :cond_7

    .line 258
    .line 259
    iget v4, v10, Lcom/google/android/gms/internal/ads/zzahj;->zzb:I

    .line 260
    .line 261
    const/4 v10, 0x1

    .line 262
    if-le v4, v10, :cond_8

    .line 263
    .line 264
    long-to-float v2, v2

    .line 265
    int-to-float v3, v4

    .line 266
    const v4, 0x49742400    # 1000000.0f

    .line 267
    .line 268
    .line 269
    div-float/2addr v2, v4

    .line 270
    div-float/2addr v3, v2

    .line 271
    invoke-virtual {v13, v3}, Lcom/google/android/gms/internal/ads/zzak;->zzE(F)Lcom/google/android/gms/internal/ads/zzak;

    .line 272
    .line 273
    .line 274
    goto :goto_7

    .line 275
    :cond_7
    const/4 v10, 0x1

    .line 276
    :cond_8
    :goto_7
    iget v2, v5, Lcom/google/android/gms/internal/ads/zzahg;->zzb:I

    .line 277
    .line 278
    sget v3, Lcom/google/android/gms/internal/ads/zzagy;->zzb:I

    .line 279
    .line 280
    if-ne v2, v10, :cond_9

    .line 281
    .line 282
    invoke-virtual {v11}, Lcom/google/android/gms/internal/ads/zzabq;->zza()Z

    .line 283
    .line 284
    .line 285
    move-result v2

    .line 286
    if-eqz v2, :cond_9

    .line 287
    .line 288
    iget v2, v11, Lcom/google/android/gms/internal/ads/zzabq;->zza:I

    .line 289
    .line 290
    invoke-virtual {v13, v2}, Lcom/google/android/gms/internal/ads/zzak;->zzC(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 291
    .line 292
    .line 293
    iget v2, v11, Lcom/google/android/gms/internal/ads/zzabq;->zzb:I

    .line 294
    .line 295
    invoke-virtual {v13, v2}, Lcom/google/android/gms/internal/ads/zzak;->zzD(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 296
    .line 297
    .line 298
    :cond_9
    iget v2, v5, Lcom/google/android/gms/internal/ads/zzahg;->zzb:I

    .line 299
    .line 300
    const/4 v3, 0x3

    .line 301
    new-array v4, v3, [Lcom/google/android/gms/internal/ads/zzbz;

    .line 302
    .line 303
    iget-object v7, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzh:Ljava/util/List;

    .line 304
    .line 305
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    .line 306
    .line 307
    .line 308
    move-result v7

    .line 309
    if-eqz v7, :cond_a

    .line 310
    .line 311
    const/4 v7, 0x0

    .line 312
    goto :goto_8

    .line 313
    :cond_a
    new-instance v7, Lcom/google/android/gms/internal/ads/zzbz;

    .line 314
    .line 315
    iget-object v8, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzh:Ljava/util/List;

    .line 316
    .line 317
    invoke-direct {v7, v8}, Lcom/google/android/gms/internal/ads/zzbz;-><init>(Ljava/util/List;)V

    .line 318
    .line 319
    .line 320
    :goto_8
    const/4 v8, 0x0

    .line 321
    aput-object v7, v4, v8

    .line 322
    .line 323
    aput-object v20, v4, v10

    .line 324
    .line 325
    const/4 v7, 0x2

    .line 326
    aput-object v15, v4, v7

    .line 327
    .line 328
    new-instance v7, Lcom/google/android/gms/internal/ads/zzbz;

    .line 329
    .line 330
    new-array v3, v8, [Lcom/google/android/gms/internal/ads/zzby;

    .line 331
    .line 332
    move/from16 v21, v9

    .line 333
    .line 334
    const-wide v8, -0x7fffffffffffffffL    # -4.9E-324

    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    invoke-direct {v7, v8, v9, v3}, Lcom/google/android/gms/internal/ads/zzbz;-><init>(J[Lcom/google/android/gms/internal/ads/zzby;)V

    .line 340
    .line 341
    .line 342
    if-eqz v14, :cond_d

    .line 343
    .line 344
    const/4 v3, 0x0

    .line 345
    :goto_9
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzbz;->zza()I

    .line 346
    .line 347
    .line 348
    move-result v8

    .line 349
    if-ge v3, v8, :cond_d

    .line 350
    .line 351
    invoke-virtual {v14, v3}, Lcom/google/android/gms/internal/ads/zzbz;->zzb(I)Lcom/google/android/gms/internal/ads/zzby;

    .line 352
    .line 353
    .line 354
    move-result-object v8

    .line 355
    instance-of v9, v8, Lcom/google/android/gms/internal/ads/zzfo;

    .line 356
    .line 357
    if-eqz v9, :cond_c

    .line 358
    .line 359
    check-cast v8, Lcom/google/android/gms/internal/ads/zzfo;

    .line 360
    .line 361
    iget-object v9, v8, Lcom/google/android/gms/internal/ads/zzfo;->zza:Ljava/lang/String;

    .line 362
    .line 363
    const-string v10, "com.android.capture.fps"

    .line 364
    .line 365
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 366
    .line 367
    .line 368
    move-result v9

    .line 369
    if-eqz v9, :cond_b

    .line 370
    .line 371
    const/4 v9, 0x2

    .line 372
    if-ne v2, v9, :cond_c

    .line 373
    .line 374
    const/4 v9, 0x1

    .line 375
    new-array v10, v9, [Lcom/google/android/gms/internal/ads/zzby;

    .line 376
    .line 377
    const/16 v17, 0x0

    .line 378
    .line 379
    aput-object v8, v10, v17

    .line 380
    .line 381
    invoke-virtual {v7, v10}, Lcom/google/android/gms/internal/ads/zzbz;->zzc([Lcom/google/android/gms/internal/ads/zzby;)Lcom/google/android/gms/internal/ads/zzbz;

    .line 382
    .line 383
    .line 384
    move-result-object v7

    .line 385
    goto :goto_a

    .line 386
    :cond_b
    const/4 v9, 0x1

    .line 387
    const/16 v17, 0x0

    .line 388
    .line 389
    new-array v10, v9, [Lcom/google/android/gms/internal/ads/zzby;

    .line 390
    .line 391
    aput-object v8, v10, v17

    .line 392
    .line 393
    invoke-virtual {v7, v10}, Lcom/google/android/gms/internal/ads/zzbz;->zzc([Lcom/google/android/gms/internal/ads/zzby;)Lcom/google/android/gms/internal/ads/zzbz;

    .line 394
    .line 395
    .line 396
    move-result-object v7

    .line 397
    goto :goto_a

    .line 398
    :cond_c
    const/16 v17, 0x0

    .line 399
    .line 400
    :goto_a
    add-int/lit8 v3, v3, 0x1

    .line 401
    .line 402
    const/4 v10, 0x1

    .line 403
    goto :goto_9

    .line 404
    :cond_d
    const/16 v17, 0x0

    .line 405
    .line 406
    const/4 v2, 0x3

    .line 407
    const/4 v9, 0x0

    .line 408
    :goto_b
    if-ge v9, v2, :cond_e

    .line 409
    .line 410
    aget-object v3, v4, v9

    .line 411
    .line 412
    invoke-virtual {v7, v3}, Lcom/google/android/gms/internal/ads/zzbz;->zzd(Lcom/google/android/gms/internal/ads/zzbz;)Lcom/google/android/gms/internal/ads/zzbz;

    .line 413
    .line 414
    .line 415
    move-result-object v7

    .line 416
    add-int/lit8 v9, v9, 0x1

    .line 417
    .line 418
    goto :goto_b

    .line 419
    :cond_e
    invoke-virtual {v7}, Lcom/google/android/gms/internal/ads/zzbz;->zza()I

    .line 420
    .line 421
    .line 422
    move-result v2

    .line 423
    if-lez v2, :cond_f

    .line 424
    .line 425
    invoke-virtual {v13, v7}, Lcom/google/android/gms/internal/ads/zzak;->zzM(Lcom/google/android/gms/internal/ads/zzbz;)Lcom/google/android/gms/internal/ads/zzak;

    .line 426
    .line 427
    .line 428
    :cond_f
    iget-object v2, v12, Lcom/google/android/gms/internal/ads/zzahb;->zzc:Lcom/google/android/gms/internal/ads/zzace;

    .line 429
    .line 430
    invoke-virtual {v13}, Lcom/google/android/gms/internal/ads/zzak;->zzY()Lcom/google/android/gms/internal/ads/zzam;

    .line 431
    .line 432
    .line 433
    move-result-object v3

    .line 434
    invoke-interface {v2, v3}, Lcom/google/android/gms/internal/ads/zzace;->zzk(Lcom/google/android/gms/internal/ads/zzam;)V

    .line 435
    .line 436
    .line 437
    iget v2, v5, Lcom/google/android/gms/internal/ads/zzahg;->zzb:I

    .line 438
    .line 439
    const/4 v3, 0x2

    .line 440
    if-ne v2, v3, :cond_10

    .line 441
    .line 442
    const/4 v2, -0x1

    .line 443
    if-ne v6, v2, :cond_11

    .line 444
    .line 445
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 446
    .line 447
    .line 448
    move-result v3

    .line 449
    move v6, v3

    .line 450
    goto :goto_c

    .line 451
    :cond_10
    const/4 v2, -0x1

    .line 452
    :cond_11
    :goto_c
    invoke-interface {v1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 453
    .line 454
    .line 455
    move-wide/from16 v7, v25

    .line 456
    .line 457
    :goto_d
    add-int/lit8 v9, v21, 0x1

    .line 458
    .line 459
    move-object/from16 v3, v22

    .line 460
    .line 461
    move/from16 v4, v24

    .line 462
    .line 463
    const-wide v12, -0x7fffffffffffffffL    # -4.9E-324

    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    goto/16 :goto_4

    .line 469
    .line 470
    :cond_12
    const/4 v2, -0x1

    .line 471
    const/4 v3, 0x0

    .line 472
    iput v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzu:I

    .line 473
    .line 474
    iput-wide v7, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzv:J

    .line 475
    .line 476
    new-array v4, v3, [Lcom/google/android/gms/internal/ads/zzahb;

    .line 477
    .line 478
    invoke-interface {v1, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 479
    .line 480
    .line 481
    move-result-object v1

    .line 482
    check-cast v1, [Lcom/google/android/gms/internal/ads/zzahb;

    .line 483
    .line 484
    iput-object v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzs:[Lcom/google/android/gms/internal/ads/zzahb;

    .line 485
    .line 486
    array-length v4, v1

    .line 487
    new-array v5, v4, [[J

    .line 488
    .line 489
    new-array v6, v4, [I

    .line 490
    .line 491
    new-array v7, v4, [J

    .line 492
    .line 493
    new-array v4, v4, [Z

    .line 494
    .line 495
    const/4 v9, 0x0

    .line 496
    :goto_e
    array-length v8, v1

    .line 497
    if-ge v9, v8, :cond_13

    .line 498
    .line 499
    aget-object v8, v1, v9

    .line 500
    .line 501
    iget-object v8, v8, Lcom/google/android/gms/internal/ads/zzahb;->zzb:Lcom/google/android/gms/internal/ads/zzahj;

    .line 502
    .line 503
    iget v8, v8, Lcom/google/android/gms/internal/ads/zzahj;->zzb:I

    .line 504
    .line 505
    new-array v8, v8, [J

    .line 506
    .line 507
    aput-object v8, v5, v9

    .line 508
    .line 509
    aget-object v8, v1, v9

    .line 510
    .line 511
    iget-object v8, v8, Lcom/google/android/gms/internal/ads/zzahb;->zzb:Lcom/google/android/gms/internal/ads/zzahj;

    .line 512
    .line 513
    iget-object v8, v8, Lcom/google/android/gms/internal/ads/zzahj;->zzf:[J

    .line 514
    .line 515
    aget-wide v10, v8, v3

    .line 516
    .line 517
    aput-wide v10, v7, v9

    .line 518
    .line 519
    add-int/lit8 v9, v9, 0x1

    .line 520
    .line 521
    goto :goto_e

    .line 522
    :cond_13
    const/4 v9, 0x0

    .line 523
    :goto_f
    array-length v8, v1

    .line 524
    if-ge v9, v8, :cond_17

    .line 525
    .line 526
    const-wide v10, 0x7fffffffffffffffL

    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    const/4 v8, 0x0

    .line 532
    const/4 v12, -0x1

    .line 533
    :goto_10
    array-length v13, v1

    .line 534
    if-ge v8, v13, :cond_15

    .line 535
    .line 536
    aget-boolean v13, v4, v8

    .line 537
    .line 538
    if-nez v13, :cond_14

    .line 539
    .line 540
    aget-wide v13, v7, v8

    .line 541
    .line 542
    cmp-long v15, v13, v10

    .line 543
    .line 544
    if-gtz v15, :cond_14

    .line 545
    .line 546
    move v12, v8

    .line 547
    move-wide v10, v13

    .line 548
    :cond_14
    add-int/lit8 v8, v8, 0x1

    .line 549
    .line 550
    goto :goto_10

    .line 551
    :cond_15
    aget v8, v6, v12

    .line 552
    .line 553
    aget-object v10, v5, v12

    .line 554
    .line 555
    aput-wide v16, v10, v8

    .line 556
    .line 557
    aget-object v11, v1, v12

    .line 558
    .line 559
    iget-object v11, v11, Lcom/google/android/gms/internal/ads/zzahb;->zzb:Lcom/google/android/gms/internal/ads/zzahj;

    .line 560
    .line 561
    iget-object v13, v11, Lcom/google/android/gms/internal/ads/zzahj;->zzd:[I

    .line 562
    .line 563
    aget v13, v13, v8

    .line 564
    .line 565
    int-to-long v13, v13

    .line 566
    add-long v16, v16, v13

    .line 567
    .line 568
    const/4 v13, 0x1

    .line 569
    add-int/2addr v8, v13

    .line 570
    aput v8, v6, v12

    .line 571
    .line 572
    array-length v10, v10

    .line 573
    if-ge v8, v10, :cond_16

    .line 574
    .line 575
    iget-object v10, v11, Lcom/google/android/gms/internal/ads/zzahj;->zzf:[J

    .line 576
    .line 577
    aget-wide v14, v10, v8

    .line 578
    .line 579
    aput-wide v14, v7, v12

    .line 580
    .line 581
    goto :goto_f

    .line 582
    :cond_16
    aput-boolean v13, v4, v12

    .line 583
    .line 584
    add-int/lit8 v9, v9, 0x1

    .line 585
    .line 586
    goto :goto_f

    .line 587
    :cond_17
    iput-object v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzt:[[J

    .line 588
    .line 589
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzr:Lcom/google/android/gms/internal/ads/zzabe;

    .line 590
    .line 591
    invoke-interface {v1}, Lcom/google/android/gms/internal/ads/zzabe;->zzC()V

    .line 592
    .line 593
    .line 594
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzr:Lcom/google/android/gms/internal/ads/zzabe;

    .line 595
    .line 596
    invoke-interface {v1, v0}, Lcom/google/android/gms/internal/ads/zzabe;->zzN(Lcom/google/android/gms/internal/ads/zzaca;)V

    .line 597
    .line 598
    .line 599
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    .line 600
    .line 601
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->clear()V

    .line 602
    .line 603
    .line 604
    const/4 v1, 0x2

    .line 605
    iput v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzi:I

    .line 606
    .line 607
    goto/16 :goto_0

    .line 608
    .line 609
    :cond_18
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    .line 610
    .line 611
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    .line 612
    .line 613
    .line 614
    move-result v1

    .line 615
    if-nez v1, :cond_0

    .line 616
    .line 617
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    .line 618
    .line 619
    invoke-virtual {v1}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    .line 620
    .line 621
    .line 622
    move-result-object v1

    .line 623
    check-cast v1, Lcom/google/android/gms/internal/ads/zzagh;

    .line 624
    .line 625
    invoke-virtual {v1, v3}, Lcom/google/android/gms/internal/ads/zzagh;->zzc(Lcom/google/android/gms/internal/ads/zzagh;)V

    .line 626
    .line 627
    .line 628
    goto/16 :goto_0

    .line 629
    .line 630
    :cond_19
    iget v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzi:I

    .line 631
    .line 632
    const/4 v2, 0x2

    .line 633
    if-eq v1, v2, :cond_1a

    .line 634
    .line 635
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzahc;->zzk()V

    .line 636
    .line 637
    .line 638
    :cond_1a
    return-void
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
.end method


# virtual methods
.method public final zza(Lcom/google/android/gms/internal/ads/zzabc;Lcom/google/android/gms/internal/ads/zzabx;)I
    .locals 32
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p2

    .line 6
    .line 7
    :cond_0
    :goto_0
    iget v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzi:I

    .line 8
    .line 9
    const v4, 0x66747970

    .line 10
    .line 11
    .line 12
    const-wide/16 v6, 0x0

    .line 13
    .line 14
    const/4 v8, -0x1

    .line 15
    const/16 v9, 0x8

    .line 16
    .line 17
    const/4 v10, 0x1

    .line 18
    if-eqz v3, :cond_1f

    .line 19
    .line 20
    const-wide/32 v13, 0x40000

    .line 21
    .line 22
    .line 23
    if-eq v3, v10, :cond_17

    .line 24
    .line 25
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzf()J

    .line 26
    .line 27
    .line 28
    move-result-wide v3

    .line 29
    iget v9, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzn:I

    .line 30
    .line 31
    if-ne v9, v8, :cond_a

    .line 32
    .line 33
    const-wide v16, 0x7fffffffffffffffL

    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    move-wide/from16 v18, v16

    .line 39
    .line 40
    move-wide/from16 v21, v18

    .line 41
    .line 42
    move-wide/from16 v23, v21

    .line 43
    .line 44
    const/4 v9, 0x0

    .line 45
    const/16 v20, 0x1

    .line 46
    .line 47
    const/16 v25, -0x1

    .line 48
    .line 49
    const/16 v26, -0x1

    .line 50
    .line 51
    const/16 v27, 0x1

    .line 52
    .line 53
    :goto_1
    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzs:[Lcom/google/android/gms/internal/ads/zzahb;

    .line 54
    .line 55
    array-length v15, v5

    .line 56
    if-ge v9, v15, :cond_8

    .line 57
    .line 58
    aget-object v5, v5, v9

    .line 59
    .line 60
    iget v15, v5, Lcom/google/android/gms/internal/ads/zzahb;->zze:I

    .line 61
    .line 62
    iget-object v5, v5, Lcom/google/android/gms/internal/ads/zzahb;->zzb:Lcom/google/android/gms/internal/ads/zzahj;

    .line 63
    .line 64
    iget v12, v5, Lcom/google/android/gms/internal/ads/zzahj;->zzb:I

    .line 65
    .line 66
    if-ne v15, v12, :cond_1

    .line 67
    .line 68
    goto :goto_6

    .line 69
    :cond_1
    iget-object v5, v5, Lcom/google/android/gms/internal/ads/zzahj;->zzc:[J

    .line 70
    .line 71
    aget-wide v28, v5, v15

    .line 72
    .line 73
    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzt:[[J

    .line 74
    .line 75
    sget v12, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    .line 76
    .line 77
    aget-object v5, v5, v9

    .line 78
    .line 79
    aget-wide v30, v5, v15

    .line 80
    .line 81
    sub-long v28, v28, v3

    .line 82
    .line 83
    cmp-long v5, v28, v6

    .line 84
    .line 85
    if-ltz v5, :cond_3

    .line 86
    .line 87
    cmp-long v5, v28, v13

    .line 88
    .line 89
    if-ltz v5, :cond_2

    .line 90
    .line 91
    goto :goto_2

    .line 92
    :cond_2
    const/4 v5, 0x0

    .line 93
    goto :goto_3

    .line 94
    :cond_3
    :goto_2
    const/4 v5, 0x1

    .line 95
    :goto_3
    if-nez v5, :cond_4

    .line 96
    .line 97
    if-nez v27, :cond_5

    .line 98
    .line 99
    const/4 v12, 0x0

    .line 100
    goto :goto_4

    .line 101
    :cond_4
    move/from16 v12, v27

    .line 102
    .line 103
    :goto_4
    if-ne v5, v12, :cond_6

    .line 104
    .line 105
    cmp-long v15, v28, v23

    .line 106
    .line 107
    if-gez v15, :cond_6

    .line 108
    .line 109
    :cond_5
    move/from16 v27, v5

    .line 110
    .line 111
    move/from16 v26, v9

    .line 112
    .line 113
    move-wide/from16 v23, v28

    .line 114
    .line 115
    move-wide/from16 v21, v30

    .line 116
    .line 117
    goto :goto_5

    .line 118
    :cond_6
    move/from16 v27, v12

    .line 119
    .line 120
    :goto_5
    cmp-long v12, v30, v18

    .line 121
    .line 122
    if-gez v12, :cond_7

    .line 123
    .line 124
    move/from16 v20, v5

    .line 125
    .line 126
    move/from16 v25, v9

    .line 127
    .line 128
    move-wide/from16 v18, v30

    .line 129
    .line 130
    :cond_7
    :goto_6
    add-int/lit8 v9, v9, 0x1

    .line 131
    .line 132
    goto :goto_1

    .line 133
    :cond_8
    cmp-long v5, v18, v16

    .line 134
    .line 135
    if-eqz v5, :cond_9

    .line 136
    .line 137
    if-eqz v20, :cond_9

    .line 138
    .line 139
    const-wide/32 v15, 0xa00000

    .line 140
    .line 141
    .line 142
    add-long v18, v18, v15

    .line 143
    .line 144
    cmp-long v5, v21, v18

    .line 145
    .line 146
    if-ltz v5, :cond_9

    .line 147
    .line 148
    move/from16 v9, v25

    .line 149
    .line 150
    goto :goto_7

    .line 151
    :cond_9
    move/from16 v9, v26

    .line 152
    .line 153
    :goto_7
    iput v9, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzn:I

    .line 154
    .line 155
    if-ne v9, v8, :cond_a

    .line 156
    .line 157
    goto/16 :goto_c

    .line 158
    .line 159
    :cond_a
    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzs:[Lcom/google/android/gms/internal/ads/zzahb;

    .line 160
    .line 161
    aget-object v5, v5, v9

    .line 162
    .line 163
    iget-object v9, v5, Lcom/google/android/gms/internal/ads/zzahb;->zzc:Lcom/google/android/gms/internal/ads/zzace;

    .line 164
    .line 165
    iget v12, v5, Lcom/google/android/gms/internal/ads/zzahb;->zze:I

    .line 166
    .line 167
    iget-object v15, v5, Lcom/google/android/gms/internal/ads/zzahb;->zzb:Lcom/google/android/gms/internal/ads/zzahj;

    .line 168
    .line 169
    iget-object v8, v15, Lcom/google/android/gms/internal/ads/zzahj;->zzc:[J

    .line 170
    .line 171
    aget-wide v10, v8, v12

    .line 172
    .line 173
    iget-object v8, v15, Lcom/google/android/gms/internal/ads/zzahj;->zzd:[I

    .line 174
    .line 175
    aget v8, v8, v12

    .line 176
    .line 177
    iget-object v15, v5, Lcom/google/android/gms/internal/ads/zzahb;->zzd:Lcom/google/android/gms/internal/ads/zzacf;

    .line 178
    .line 179
    sub-long v3, v10, v3

    .line 180
    .line 181
    iget v13, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzo:I

    .line 182
    .line 183
    int-to-long v13, v13

    .line 184
    add-long/2addr v3, v13

    .line 185
    cmp-long v13, v3, v6

    .line 186
    .line 187
    if-ltz v13, :cond_16

    .line 188
    .line 189
    const-wide/32 v6, 0x40000

    .line 190
    .line 191
    .line 192
    cmp-long v13, v3, v6

    .line 193
    .line 194
    if-ltz v13, :cond_b

    .line 195
    .line 196
    goto/16 :goto_b

    .line 197
    .line 198
    :cond_b
    iget-object v2, v5, Lcom/google/android/gms/internal/ads/zzahb;->zza:Lcom/google/android/gms/internal/ads/zzahg;

    .line 199
    .line 200
    iget v2, v2, Lcom/google/android/gms/internal/ads/zzahg;->zzg:I

    .line 201
    .line 202
    const/4 v6, 0x1

    .line 203
    if-ne v2, v6, :cond_c

    .line 204
    .line 205
    const-wide/16 v6, 0x8

    .line 206
    .line 207
    add-long/2addr v3, v6

    .line 208
    add-int/lit8 v8, v8, -0x8

    .line 209
    .line 210
    :cond_c
    long-to-int v2, v3

    .line 211
    invoke-interface {v1, v2}, Lcom/google/android/gms/internal/ads/zzabc;->zzk(I)V

    .line 212
    .line 213
    .line 214
    iget-object v2, v5, Lcom/google/android/gms/internal/ads/zzahb;->zza:Lcom/google/android/gms/internal/ads/zzahg;

    .line 215
    .line 216
    iget v3, v2, Lcom/google/android/gms/internal/ads/zzahg;->zzj:I

    .line 217
    .line 218
    if-eqz v3, :cond_f

    .line 219
    .line 220
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzc:Lcom/google/android/gms/internal/ads/zzfb;

    .line 221
    .line 222
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 223
    .line 224
    .line 225
    move-result-object v2

    .line 226
    const/4 v4, 0x0

    .line 227
    aput-byte v4, v2, v4

    .line 228
    .line 229
    const/4 v6, 0x1

    .line 230
    aput-byte v4, v2, v6

    .line 231
    .line 232
    const/4 v6, 0x2

    .line 233
    aput-byte v4, v2, v6

    .line 234
    .line 235
    rsub-int/lit8 v4, v3, 0x4

    .line 236
    .line 237
    :goto_8
    iget v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 238
    .line 239
    if-ge v6, v8, :cond_13

    .line 240
    .line 241
    iget v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzq:I

    .line 242
    .line 243
    if-nez v6, :cond_e

    .line 244
    .line 245
    invoke-interface {v1, v2, v4, v3}, Lcom/google/android/gms/internal/ads/zzabc;->zzi([BII)V

    .line 246
    .line 247
    .line 248
    iget v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzo:I

    .line 249
    .line 250
    add-int/2addr v6, v3

    .line 251
    iput v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzo:I

    .line 252
    .line 253
    iget-object v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzc:Lcom/google/android/gms/internal/ads/zzfb;

    .line 254
    .line 255
    const/4 v7, 0x0

    .line 256
    invoke-virtual {v6, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 257
    .line 258
    .line 259
    iget-object v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzc:Lcom/google/android/gms/internal/ads/zzfb;

    .line 260
    .line 261
    invoke-virtual {v6}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 262
    .line 263
    .line 264
    move-result v6

    .line 265
    if-ltz v6, :cond_d

    .line 266
    .line 267
    iput v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzq:I

    .line 268
    .line 269
    iget-object v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzb:Lcom/google/android/gms/internal/ads/zzfb;

    .line 270
    .line 271
    invoke-virtual {v6, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 272
    .line 273
    .line 274
    iget-object v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzb:Lcom/google/android/gms/internal/ads/zzfb;

    .line 275
    .line 276
    const/4 v10, 0x4

    .line 277
    invoke-interface {v9, v6, v10}, Lcom/google/android/gms/internal/ads/zzace;->zzq(Lcom/google/android/gms/internal/ads/zzfb;I)V

    .line 278
    .line 279
    .line 280
    iget v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 281
    .line 282
    add-int/2addr v6, v10

    .line 283
    iput v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 284
    .line 285
    add-int/2addr v8, v4

    .line 286
    goto :goto_8

    .line 287
    :cond_d
    const-string v1, "Invalid NAL length"

    .line 288
    .line 289
    const/4 v2, 0x0

    .line 290
    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    .line 291
    .line 292
    .line 293
    move-result-object v1

    .line 294
    throw v1

    .line 295
    :cond_e
    const/4 v7, 0x0

    .line 296
    invoke-interface {v9, v1, v6, v7}, Lcom/google/android/gms/internal/ads/zzace;->zze(Lcom/google/android/gms/internal/ads/zzt;IZ)I

    .line 297
    .line 298
    .line 299
    move-result v6

    .line 300
    iget v7, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzo:I

    .line 301
    .line 302
    add-int/2addr v7, v6

    .line 303
    iput v7, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzo:I

    .line 304
    .line 305
    iget v7, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 306
    .line 307
    add-int/2addr v7, v6

    .line 308
    iput v7, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 309
    .line 310
    iget v7, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzq:I

    .line 311
    .line 312
    sub-int/2addr v7, v6

    .line 313
    iput v7, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzq:I

    .line 314
    .line 315
    goto :goto_8

    .line 316
    :cond_f
    iget-object v2, v2, Lcom/google/android/gms/internal/ads/zzahg;->zzf:Lcom/google/android/gms/internal/ads/zzam;

    .line 317
    .line 318
    iget-object v2, v2, Lcom/google/android/gms/internal/ads/zzam;->zzm:Ljava/lang/String;

    .line 319
    .line 320
    const-string v3, "audio/ac4"

    .line 321
    .line 322
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 323
    .line 324
    .line 325
    move-result v2

    .line 326
    if-eqz v2, :cond_11

    .line 327
    .line 328
    iget v2, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 329
    .line 330
    if-nez v2, :cond_10

    .line 331
    .line 332
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzd:Lcom/google/android/gms/internal/ads/zzfb;

    .line 333
    .line 334
    invoke-static {v8, v2}, Lcom/google/android/gms/internal/ads/zzaaf;->zzb(ILcom/google/android/gms/internal/ads/zzfb;)V

    .line 335
    .line 336
    .line 337
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzd:Lcom/google/android/gms/internal/ads/zzfb;

    .line 338
    .line 339
    const/4 v3, 0x7

    .line 340
    invoke-interface {v9, v2, v3}, Lcom/google/android/gms/internal/ads/zzace;->zzq(Lcom/google/android/gms/internal/ads/zzfb;I)V

    .line 341
    .line 342
    .line 343
    iget v2, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 344
    .line 345
    add-int/2addr v2, v3

    .line 346
    iput v2, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 347
    .line 348
    :cond_10
    add-int/lit8 v8, v8, 0x7

    .line 349
    .line 350
    goto :goto_9

    .line 351
    :cond_11
    if-eqz v15, :cond_12

    .line 352
    .line 353
    invoke-virtual {v15, v1}, Lcom/google/android/gms/internal/ads/zzacf;->zzd(Lcom/google/android/gms/internal/ads/zzabc;)V

    .line 354
    .line 355
    .line 356
    :cond_12
    :goto_9
    iget v2, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 357
    .line 358
    if-ge v2, v8, :cond_13

    .line 359
    .line 360
    sub-int v2, v8, v2

    .line 361
    .line 362
    const/4 v3, 0x0

    .line 363
    invoke-interface {v9, v1, v2, v3}, Lcom/google/android/gms/internal/ads/zzace;->zze(Lcom/google/android/gms/internal/ads/zzt;IZ)I

    .line 364
    .line 365
    .line 366
    move-result v2

    .line 367
    iget v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzo:I

    .line 368
    .line 369
    add-int/2addr v3, v2

    .line 370
    iput v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzo:I

    .line 371
    .line 372
    iget v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 373
    .line 374
    add-int/2addr v3, v2

    .line 375
    iput v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 376
    .line 377
    iget v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzq:I

    .line 378
    .line 379
    sub-int/2addr v3, v2

    .line 380
    iput v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzq:I

    .line 381
    .line 382
    goto :goto_9

    .line 383
    :cond_13
    iget-object v1, v5, Lcom/google/android/gms/internal/ads/zzahb;->zzb:Lcom/google/android/gms/internal/ads/zzahj;

    .line 384
    .line 385
    iget-object v2, v1, Lcom/google/android/gms/internal/ads/zzahj;->zzf:[J

    .line 386
    .line 387
    aget-wide v3, v2, v12

    .line 388
    .line 389
    iget-object v1, v1, Lcom/google/android/gms/internal/ads/zzahj;->zzg:[I

    .line 390
    .line 391
    aget v1, v1, v12

    .line 392
    .line 393
    if-eqz v15, :cond_14

    .line 394
    .line 395
    const/16 v21, 0x0

    .line 396
    .line 397
    const/16 v22, 0x0

    .line 398
    .line 399
    move-object v2, v15

    .line 400
    move-object/from16 v16, v9

    .line 401
    .line 402
    move-wide/from16 v17, v3

    .line 403
    .line 404
    move/from16 v19, v1

    .line 405
    .line 406
    move/from16 v20, v8

    .line 407
    .line 408
    invoke-virtual/range {v15 .. v22}, Lcom/google/android/gms/internal/ads/zzacf;->zzc(Lcom/google/android/gms/internal/ads/zzace;JIIILcom/google/android/gms/internal/ads/zzacd;)V

    .line 409
    .line 410
    .line 411
    const/4 v1, 0x1

    .line 412
    add-int/2addr v12, v1

    .line 413
    iget-object v1, v5, Lcom/google/android/gms/internal/ads/zzahb;->zzb:Lcom/google/android/gms/internal/ads/zzahj;

    .line 414
    .line 415
    iget v1, v1, Lcom/google/android/gms/internal/ads/zzahj;->zzb:I

    .line 416
    .line 417
    if-ne v12, v1, :cond_15

    .line 418
    .line 419
    const/4 v1, 0x0

    .line 420
    invoke-virtual {v2, v9, v1}, Lcom/google/android/gms/internal/ads/zzacf;->zza(Lcom/google/android/gms/internal/ads/zzace;Lcom/google/android/gms/internal/ads/zzacd;)V

    .line 421
    .line 422
    .line 423
    goto :goto_a

    .line 424
    :cond_14
    const/16 v20, 0x0

    .line 425
    .line 426
    const/16 v21, 0x0

    .line 427
    .line 428
    move-object v15, v9

    .line 429
    move-wide/from16 v16, v3

    .line 430
    .line 431
    move/from16 v18, v1

    .line 432
    .line 433
    move/from16 v19, v8

    .line 434
    .line 435
    invoke-interface/range {v15 .. v21}, Lcom/google/android/gms/internal/ads/zzace;->zzs(JIIILcom/google/android/gms/internal/ads/zzacd;)V

    .line 436
    .line 437
    .line 438
    :cond_15
    :goto_a
    iget v1, v5, Lcom/google/android/gms/internal/ads/zzahb;->zze:I

    .line 439
    .line 440
    const/4 v2, 0x1

    .line 441
    add-int/2addr v1, v2

    .line 442
    iput v1, v5, Lcom/google/android/gms/internal/ads/zzahb;->zze:I

    .line 443
    .line 444
    const/4 v1, -0x1

    .line 445
    iput v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzn:I

    .line 446
    .line 447
    const/4 v1, 0x0

    .line 448
    iput v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzo:I

    .line 449
    .line 450
    iput v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 451
    .line 452
    iput v1, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzq:I

    .line 453
    .line 454
    const/4 v8, 0x0

    .line 455
    goto :goto_c

    .line 456
    :cond_16
    :goto_b
    iput-wide v10, v2, Lcom/google/android/gms/internal/ads/zzabx;->zza:J

    .line 457
    .line 458
    const/4 v8, 0x1

    .line 459
    :goto_c
    return v8

    .line 460
    :cond_17
    iget-wide v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzk:J

    .line 461
    .line 462
    iget v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 463
    .line 464
    int-to-long v7, v3

    .line 465
    sub-long/2addr v5, v7

    .line 466
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzf()J

    .line 467
    .line 468
    .line 469
    move-result-wide v7

    .line 470
    add-long/2addr v7, v5

    .line 471
    iget-object v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzm:Lcom/google/android/gms/internal/ads/zzfb;

    .line 472
    .line 473
    if-eqz v3, :cond_1c

    .line 474
    .line 475
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 476
    .line 477
    .line 478
    move-result-object v10

    .line 479
    iget v11, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 480
    .line 481
    long-to-int v6, v5

    .line 482
    invoke-interface {v1, v10, v11, v6}, Lcom/google/android/gms/internal/ads/zzabc;->zzi([BII)V

    .line 483
    .line 484
    .line 485
    iget v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzj:I

    .line 486
    .line 487
    if-ne v5, v4, :cond_1b

    .line 488
    .line 489
    invoke-virtual {v3, v9}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 490
    .line 491
    .line 492
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 493
    .line 494
    .line 495
    move-result v4

    .line 496
    invoke-static {v4}, Lcom/google/android/gms/internal/ads/zzahc;->zzf(I)I

    .line 497
    .line 498
    .line 499
    move-result v4

    .line 500
    if-eqz v4, :cond_18

    .line 501
    .line 502
    goto :goto_d

    .line 503
    :cond_18
    const/4 v4, 0x4

    .line 504
    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 505
    .line 506
    .line 507
    :cond_19
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zza()I

    .line 508
    .line 509
    .line 510
    move-result v4

    .line 511
    if-lez v4, :cond_1a

    .line 512
    .line 513
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 514
    .line 515
    .line 516
    move-result v4

    .line 517
    invoke-static {v4}, Lcom/google/android/gms/internal/ads/zzahc;->zzf(I)I

    .line 518
    .line 519
    .line 520
    move-result v4

    .line 521
    if-eqz v4, :cond_19

    .line 522
    .line 523
    goto :goto_d

    .line 524
    :cond_1a
    const/4 v4, 0x0

    .line 525
    :goto_d
    iput v4, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzw:I

    .line 526
    .line 527
    goto :goto_e

    .line 528
    :cond_1b
    iget-object v4, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    .line 529
    .line 530
    invoke-virtual {v4}, Ljava/util/ArrayDeque;->isEmpty()Z

    .line 531
    .line 532
    .line 533
    move-result v4

    .line 534
    if-nez v4, :cond_1d

    .line 535
    .line 536
    iget-object v4, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    .line 537
    .line 538
    invoke-virtual {v4}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    .line 539
    .line 540
    .line 541
    move-result-object v4

    .line 542
    check-cast v4, Lcom/google/android/gms/internal/ads/zzagh;

    .line 543
    .line 544
    new-instance v5, Lcom/google/android/gms/internal/ads/zzagi;

    .line 545
    .line 546
    iget v6, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzj:I

    .line 547
    .line 548
    invoke-direct {v5, v6, v3}, Lcom/google/android/gms/internal/ads/zzagi;-><init>(ILcom/google/android/gms/internal/ads/zzfb;)V

    .line 549
    .line 550
    .line 551
    invoke-virtual {v4, v5}, Lcom/google/android/gms/internal/ads/zzagh;->zzd(Lcom/google/android/gms/internal/ads/zzagi;)V

    .line 552
    .line 553
    .line 554
    goto :goto_e

    .line 555
    :cond_1c
    const-wide/32 v3, 0x40000

    .line 556
    .line 557
    .line 558
    cmp-long v9, v5, v3

    .line 559
    .line 560
    if-gez v9, :cond_1e

    .line 561
    .line 562
    long-to-int v3, v5

    .line 563
    invoke-interface {v1, v3}, Lcom/google/android/gms/internal/ads/zzabc;->zzk(I)V

    .line 564
    .line 565
    .line 566
    :cond_1d
    :goto_e
    const/4 v11, 0x0

    .line 567
    goto :goto_f

    .line 568
    :cond_1e
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzf()J

    .line 569
    .line 570
    .line 571
    move-result-wide v3

    .line 572
    add-long/2addr v3, v5

    .line 573
    iput-wide v3, v2, Lcom/google/android/gms/internal/ads/zzabx;->zza:J

    .line 574
    .line 575
    const/4 v11, 0x1

    .line 576
    :goto_f
    invoke-direct {v0, v7, v8}, Lcom/google/android/gms/internal/ads/zzahc;->zzl(J)V

    .line 577
    .line 578
    .line 579
    if-eqz v11, :cond_0

    .line 580
    .line 581
    iget v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzi:I

    .line 582
    .line 583
    const/4 v4, 0x2

    .line 584
    if-eq v3, v4, :cond_0

    .line 585
    .line 586
    const/4 v3, 0x1

    .line 587
    return v3

    .line 588
    :cond_1f
    const/4 v3, 0x1

    .line 589
    iget v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 590
    .line 591
    if-nez v5, :cond_21

    .line 592
    .line 593
    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zze:Lcom/google/android/gms/internal/ads/zzfb;

    .line 594
    .line 595
    invoke-virtual {v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 596
    .line 597
    .line 598
    move-result-object v5

    .line 599
    const/4 v8, 0x0

    .line 600
    invoke-interface {v1, v5, v8, v9, v3}, Lcom/google/android/gms/internal/ads/zzabc;->zzn([BIIZ)Z

    .line 601
    .line 602
    .line 603
    move-result v5

    .line 604
    if-nez v5, :cond_20

    .line 605
    .line 606
    const/4 v3, -0x1

    .line 607
    return v3

    .line 608
    :cond_20
    iput v9, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 609
    .line 610
    iget-object v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zze:Lcom/google/android/gms/internal/ads/zzfb;

    .line 611
    .line 612
    invoke-virtual {v3, v8}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 613
    .line 614
    .line 615
    iget-object v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zze:Lcom/google/android/gms/internal/ads/zzfb;

    .line 616
    .line 617
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzt()J

    .line 618
    .line 619
    .line 620
    move-result-wide v10

    .line 621
    iput-wide v10, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzk:J

    .line 622
    .line 623
    iget-object v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zze:Lcom/google/android/gms/internal/ads/zzfb;

    .line 624
    .line 625
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 626
    .line 627
    .line 628
    move-result v3

    .line 629
    iput v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzj:I

    .line 630
    .line 631
    :cond_21
    iget-wide v10, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzk:J

    .line 632
    .line 633
    const-wide/16 v12, 0x1

    .line 634
    .line 635
    cmp-long v3, v10, v12

    .line 636
    .line 637
    if-nez v3, :cond_22

    .line 638
    .line 639
    iget-object v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zze:Lcom/google/android/gms/internal/ads/zzfb;

    .line 640
    .line 641
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 642
    .line 643
    .line 644
    move-result-object v3

    .line 645
    invoke-interface {v1, v3, v9, v9}, Lcom/google/android/gms/internal/ads/zzabc;->zzi([BII)V

    .line 646
    .line 647
    .line 648
    iget v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 649
    .line 650
    add-int/2addr v3, v9

    .line 651
    iput v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 652
    .line 653
    iget-object v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zze:Lcom/google/android/gms/internal/ads/zzfb;

    .line 654
    .line 655
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzu()J

    .line 656
    .line 657
    .line 658
    move-result-wide v5

    .line 659
    iput-wide v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzk:J

    .line 660
    .line 661
    goto :goto_11

    .line 662
    :cond_22
    cmp-long v3, v10, v6

    .line 663
    .line 664
    if-nez v3, :cond_25

    .line 665
    .line 666
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzd()J

    .line 667
    .line 668
    .line 669
    move-result-wide v5

    .line 670
    const-wide/16 v7, -0x1

    .line 671
    .line 672
    cmp-long v3, v5, v7

    .line 673
    .line 674
    if-nez v3, :cond_24

    .line 675
    .line 676
    iget-object v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    .line 677
    .line 678
    invoke-virtual {v3}, Ljava/util/ArrayDeque;->peek()Ljava/lang/Object;

    .line 679
    .line 680
    .line 681
    move-result-object v3

    .line 682
    check-cast v3, Lcom/google/android/gms/internal/ads/zzagh;

    .line 683
    .line 684
    if-eqz v3, :cond_23

    .line 685
    .line 686
    iget-wide v5, v3, Lcom/google/android/gms/internal/ads/zzagh;->zza:J

    .line 687
    .line 688
    goto :goto_10

    .line 689
    :cond_23
    move-wide v5, v7

    .line 690
    :cond_24
    :goto_10
    cmp-long v3, v5, v7

    .line 691
    .line 692
    if-eqz v3, :cond_25

    .line 693
    .line 694
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzf()J

    .line 695
    .line 696
    .line 697
    move-result-wide v7

    .line 698
    sub-long/2addr v5, v7

    .line 699
    iget v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 700
    .line 701
    int-to-long v7, v3

    .line 702
    add-long/2addr v5, v7

    .line 703
    iput-wide v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzk:J

    .line 704
    .line 705
    :cond_25
    :goto_11
    iget-wide v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzk:J

    .line 706
    .line 707
    iget v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 708
    .line 709
    int-to-long v7, v3

    .line 710
    cmp-long v10, v5, v7

    .line 711
    .line 712
    if-ltz v10, :cond_2f

    .line 713
    .line 714
    iget v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzj:I

    .line 715
    .line 716
    const v6, 0x6d6f6f76

    .line 717
    .line 718
    .line 719
    const v7, 0x6d657461

    .line 720
    .line 721
    .line 722
    if-eq v5, v6, :cond_2c

    .line 723
    .line 724
    const v6, 0x7472616b

    .line 725
    .line 726
    .line 727
    if-eq v5, v6, :cond_2c

    .line 728
    .line 729
    const v6, 0x6d646961

    .line 730
    .line 731
    .line 732
    if-eq v5, v6, :cond_2c

    .line 733
    .line 734
    const v6, 0x6d696e66

    .line 735
    .line 736
    .line 737
    if-eq v5, v6, :cond_2c

    .line 738
    .line 739
    const v6, 0x7374626c

    .line 740
    .line 741
    .line 742
    if-eq v5, v6, :cond_2c

    .line 743
    .line 744
    const v6, 0x65647473

    .line 745
    .line 746
    .line 747
    if-eq v5, v6, :cond_2c

    .line 748
    .line 749
    if-ne v5, v7, :cond_26

    .line 750
    .line 751
    goto/16 :goto_15

    .line 752
    .line 753
    :cond_26
    const v6, 0x6d646864

    .line 754
    .line 755
    .line 756
    if-eq v5, v6, :cond_29

    .line 757
    .line 758
    const v6, 0x6d766864

    .line 759
    .line 760
    .line 761
    if-eq v5, v6, :cond_29

    .line 762
    .line 763
    const v6, 0x68646c72    # 4.3148E24f

    .line 764
    .line 765
    .line 766
    if-eq v5, v6, :cond_29

    .line 767
    .line 768
    const v6, 0x73747364

    .line 769
    .line 770
    .line 771
    if-eq v5, v6, :cond_29

    .line 772
    .line 773
    const v6, 0x73747473

    .line 774
    .line 775
    .line 776
    if-eq v5, v6, :cond_29

    .line 777
    .line 778
    const v6, 0x73747373

    .line 779
    .line 780
    .line 781
    if-eq v5, v6, :cond_29

    .line 782
    .line 783
    const v6, 0x63747473

    .line 784
    .line 785
    .line 786
    if-eq v5, v6, :cond_29

    .line 787
    .line 788
    const v6, 0x656c7374

    .line 789
    .line 790
    .line 791
    if-eq v5, v6, :cond_29

    .line 792
    .line 793
    const v6, 0x73747363

    .line 794
    .line 795
    .line 796
    if-eq v5, v6, :cond_29

    .line 797
    .line 798
    const v6, 0x7374737a

    .line 799
    .line 800
    .line 801
    if-eq v5, v6, :cond_29

    .line 802
    .line 803
    const v6, 0x73747a32

    .line 804
    .line 805
    .line 806
    if-eq v5, v6, :cond_29

    .line 807
    .line 808
    const v6, 0x7374636f

    .line 809
    .line 810
    .line 811
    if-eq v5, v6, :cond_29

    .line 812
    .line 813
    const v6, 0x636f3634

    .line 814
    .line 815
    .line 816
    if-eq v5, v6, :cond_29

    .line 817
    .line 818
    const v6, 0x746b6864

    .line 819
    .line 820
    .line 821
    if-eq v5, v6, :cond_29

    .line 822
    .line 823
    if-eq v5, v4, :cond_29

    .line 824
    .line 825
    const v4, 0x75647461

    .line 826
    .line 827
    .line 828
    if-eq v5, v4, :cond_29

    .line 829
    .line 830
    const v4, 0x6b657973

    .line 831
    .line 832
    .line 833
    if-eq v5, v4, :cond_29

    .line 834
    .line 835
    const v4, 0x696c7374

    .line 836
    .line 837
    .line 838
    if-ne v5, v4, :cond_27

    .line 839
    .line 840
    goto :goto_12

    .line 841
    :cond_27
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzf()J

    .line 842
    .line 843
    .line 844
    move-result-wide v3

    .line 845
    iget v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 846
    .line 847
    int-to-long v5, v5

    .line 848
    sub-long v10, v3, v5

    .line 849
    .line 850
    iget v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzj:I

    .line 851
    .line 852
    const v4, 0x6d707664

    .line 853
    .line 854
    .line 855
    if-ne v3, v4, :cond_28

    .line 856
    .line 857
    add-long v14, v10, v5

    .line 858
    .line 859
    new-instance v3, Lcom/google/android/gms/internal/ads/zzaff;

    .line 860
    .line 861
    const-wide/16 v8, 0x0

    .line 862
    .line 863
    iget-wide v12, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzk:J

    .line 864
    .line 865
    sub-long v4, v12, v5

    .line 866
    .line 867
    move-object v7, v3

    .line 868
    const-wide v12, -0x7fffffffffffffffL    # -4.9E-324

    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    move-wide/from16 v16, v4

    .line 874
    .line 875
    invoke-direct/range {v7 .. v17}, Lcom/google/android/gms/internal/ads/zzaff;-><init>(JJJJJ)V

    .line 876
    .line 877
    .line 878
    iput-object v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzx:Lcom/google/android/gms/internal/ads/zzaff;

    .line 879
    .line 880
    :cond_28
    const/4 v3, 0x0

    .line 881
    iput-object v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzm:Lcom/google/android/gms/internal/ads/zzfb;

    .line 882
    .line 883
    const/4 v3, 0x1

    .line 884
    iput v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzi:I

    .line 885
    .line 886
    goto/16 :goto_0

    .line 887
    .line 888
    :cond_29
    :goto_12
    if-ne v3, v9, :cond_2a

    .line 889
    .line 890
    const/4 v3, 0x1

    .line 891
    goto :goto_13

    .line 892
    :cond_2a
    const/4 v3, 0x0

    .line 893
    :goto_13
    invoke-static {v3}, Lcom/google/android/gms/internal/ads/zzdy;->zzf(Z)V

    .line 894
    .line 895
    .line 896
    iget-wide v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzk:J

    .line 897
    .line 898
    const-wide/32 v5, 0x7fffffff

    .line 899
    .line 900
    .line 901
    cmp-long v7, v3, v5

    .line 902
    .line 903
    if-gtz v7, :cond_2b

    .line 904
    .line 905
    const/4 v3, 0x1

    .line 906
    goto :goto_14

    .line 907
    :cond_2b
    const/4 v3, 0x0

    .line 908
    :goto_14
    invoke-static {v3}, Lcom/google/android/gms/internal/ads/zzdy;->zzf(Z)V

    .line 909
    .line 910
    .line 911
    new-instance v3, Lcom/google/android/gms/internal/ads/zzfb;

    .line 912
    .line 913
    iget-wide v4, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzk:J

    .line 914
    .line 915
    long-to-int v5, v4

    .line 916
    invoke-direct {v3, v5}, Lcom/google/android/gms/internal/ads/zzfb;-><init>(I)V

    .line 917
    .line 918
    .line 919
    iget-object v4, v0, Lcom/google/android/gms/internal/ads/zzahc;->zze:Lcom/google/android/gms/internal/ads/zzfb;

    .line 920
    .line 921
    invoke-virtual {v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 922
    .line 923
    .line 924
    move-result-object v4

    .line 925
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 926
    .line 927
    .line 928
    move-result-object v5

    .line 929
    const/4 v6, 0x0

    .line 930
    invoke-static {v4, v6, v5, v6, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 931
    .line 932
    .line 933
    iput-object v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzm:Lcom/google/android/gms/internal/ads/zzfb;

    .line 934
    .line 935
    const/4 v3, 0x1

    .line 936
    iput v3, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzi:I

    .line 937
    .line 938
    goto/16 :goto_0

    .line 939
    .line 940
    :cond_2c
    :goto_15
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzf()J

    .line 941
    .line 942
    .line 943
    move-result-wide v3

    .line 944
    iget-wide v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzk:J

    .line 945
    .line 946
    add-long/2addr v3, v5

    .line 947
    iget v8, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 948
    .line 949
    int-to-long v10, v8

    .line 950
    cmp-long v8, v5, v10

    .line 951
    .line 952
    if-eqz v8, :cond_2d

    .line 953
    .line 954
    iget v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzj:I

    .line 955
    .line 956
    if-ne v5, v7, :cond_2d

    .line 957
    .line 958
    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzd:Lcom/google/android/gms/internal/ads/zzfb;

    .line 959
    .line 960
    invoke-virtual {v5, v9}, Lcom/google/android/gms/internal/ads/zzfb;->zzD(I)V

    .line 961
    .line 962
    .line 963
    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzd:Lcom/google/android/gms/internal/ads/zzfb;

    .line 964
    .line 965
    invoke-virtual {v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 966
    .line 967
    .line 968
    move-result-object v5

    .line 969
    const/4 v6, 0x0

    .line 970
    invoke-interface {v1, v5, v6, v9}, Lcom/google/android/gms/internal/ads/zzabc;->zzh([BII)V

    .line 971
    .line 972
    .line 973
    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzd:Lcom/google/android/gms/internal/ads/zzfb;

    .line 974
    .line 975
    invoke-static {v5}, Lcom/google/android/gms/internal/ads/zzagr;->zze(Lcom/google/android/gms/internal/ads/zzfb;)V

    .line 976
    .line 977
    .line 978
    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzd:Lcom/google/android/gms/internal/ads/zzfb;

    .line 979
    .line 980
    invoke-virtual {v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 981
    .line 982
    .line 983
    move-result v5

    .line 984
    invoke-interface {v1, v5}, Lcom/google/android/gms/internal/ads/zzabc;->zzk(I)V

    .line 985
    .line 986
    .line 987
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/internal/ads/zzabc;->zzj()V

    .line 988
    .line 989
    .line 990
    :cond_2d
    sub-long/2addr v3, v10

    .line 991
    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    .line 992
    .line 993
    new-instance v6, Lcom/google/android/gms/internal/ads/zzagh;

    .line 994
    .line 995
    iget v7, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzj:I

    .line 996
    .line 997
    invoke-direct {v6, v7, v3, v4}, Lcom/google/android/gms/internal/ads/zzagh;-><init>(IJ)V

    .line 998
    .line 999
    .line 1000
    invoke-virtual {v5, v6}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 1001
    .line 1002
    .line 1003
    iget-wide v5, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzk:J

    .line 1004
    .line 1005
    iget v7, v0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 1006
    .line 1007
    int-to-long v7, v7

    .line 1008
    cmp-long v9, v5, v7

    .line 1009
    .line 1010
    if-nez v9, :cond_2e

    .line 1011
    .line 1012
    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/internal/ads/zzahc;->zzl(J)V

    .line 1013
    .line 1014
    .line 1015
    goto/16 :goto_0

    .line 1016
    .line 1017
    :cond_2e
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzahc;->zzk()V

    .line 1018
    .line 1019
    .line 1020
    goto/16 :goto_0

    .line 1021
    .line 1022
    :cond_2f
    const-string v1, "Atom size less than header length (unsupported)."

    .line 1023
    .line 1024
    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzcd;->zzc(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzcd;

    .line 1025
    .line 1026
    .line 1027
    move-result-object v1

    .line 1028
    throw v1
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
.end method

.method public final zzb(Lcom/google/android/gms/internal/ads/zzabe;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzr:Lcom/google/android/gms/internal/ads/zzabe;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzc(JJ)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzf:Ljava/util/ArrayDeque;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayDeque;->clear()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzl:I

    .line 8
    .line 9
    const/4 v1, -0x1

    .line 10
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzn:I

    .line 11
    .line 12
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzo:I

    .line 13
    .line 14
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzp:I

    .line 15
    .line 16
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzq:I

    .line 17
    .line 18
    const-wide/16 v2, 0x0

    .line 19
    .line 20
    cmp-long v4, p1, v2

    .line 21
    .line 22
    if-nez v4, :cond_0

    .line 23
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/internal/ads/zzahc;->zzk()V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzs:[Lcom/google/android/gms/internal/ads/zzahb;

    .line 29
    .line 30
    array-length p2, p1

    .line 31
    :goto_0
    if-ge v0, p2, :cond_3

    .line 32
    .line 33
    aget-object v2, p1, v0

    .line 34
    .line 35
    iget-object v3, v2, Lcom/google/android/gms/internal/ads/zzahb;->zzb:Lcom/google/android/gms/internal/ads/zzahj;

    .line 36
    .line 37
    invoke-virtual {v3, p3, p4}, Lcom/google/android/gms/internal/ads/zzahj;->zza(J)I

    .line 38
    .line 39
    .line 40
    move-result v4

    .line 41
    if-ne v4, v1, :cond_1

    .line 42
    .line 43
    invoke-virtual {v3, p3, p4}, Lcom/google/android/gms/internal/ads/zzahj;->zzb(J)I

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    :cond_1
    iput v4, v2, Lcom/google/android/gms/internal/ads/zzahb;->zze:I

    .line 48
    .line 49
    iget-object v2, v2, Lcom/google/android/gms/internal/ads/zzahb;->zzd:Lcom/google/android/gms/internal/ads/zzacf;

    .line 50
    .line 51
    if-eqz v2, :cond_2

    .line 52
    .line 53
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzacf;->zzb()V

    .line 54
    .line 55
    .line 56
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_3
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
.end method

.method public final zzd(Lcom/google/android/gms/internal/ads/zzabc;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ads/zzahf;->zzb(Lcom/google/android/gms/internal/ads/zzabc;Z)Z

    .line 3
    .line 4
    .line 5
    move-result p1

    .line 6
    return p1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zze()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzv:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final zzg(J)Lcom/google/android/gms/internal/ads/zzaby;
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzs:[Lcom/google/android/gms/internal/ads/zzahb;

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    if-nez v1, :cond_0

    .line 5
    .line 6
    new-instance p1, Lcom/google/android/gms/internal/ads/zzaby;

    .line 7
    .line 8
    sget-object p2, Lcom/google/android/gms/internal/ads/zzacb;->zza:Lcom/google/android/gms/internal/ads/zzacb;

    .line 9
    .line 10
    invoke-direct {p1, p2, p2}, Lcom/google/android/gms/internal/ads/zzaby;-><init>(Lcom/google/android/gms/internal/ads/zzacb;Lcom/google/android/gms/internal/ads/zzacb;)V

    .line 11
    .line 12
    .line 13
    goto/16 :goto_3

    .line 14
    .line 15
    :cond_0
    iget v1, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzu:I

    .line 16
    .line 17
    const/4 v2, -0x1

    .line 18
    const-wide/16 v3, -0x1

    .line 19
    .line 20
    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    if-eq v1, v2, :cond_3

    .line 26
    .line 27
    aget-object v0, v0, v1

    .line 28
    .line 29
    iget-object v0, v0, Lcom/google/android/gms/internal/ads/zzahb;->zzb:Lcom/google/android/gms/internal/ads/zzahj;

    .line 30
    .line 31
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/internal/ads/zzahc;->zzi(Lcom/google/android/gms/internal/ads/zzahj;J)I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    if-ne v1, v2, :cond_1

    .line 36
    .line 37
    new-instance p1, Lcom/google/android/gms/internal/ads/zzaby;

    .line 38
    .line 39
    sget-object p2, Lcom/google/android/gms/internal/ads/zzacb;->zza:Lcom/google/android/gms/internal/ads/zzacb;

    .line 40
    .line 41
    invoke-direct {p1, p2, p2}, Lcom/google/android/gms/internal/ads/zzaby;-><init>(Lcom/google/android/gms/internal/ads/zzacb;Lcom/google/android/gms/internal/ads/zzacb;)V

    .line 42
    .line 43
    .line 44
    goto/16 :goto_3

    .line 45
    .line 46
    :cond_1
    iget-object v7, v0, Lcom/google/android/gms/internal/ads/zzahj;->zzf:[J

    .line 47
    .line 48
    aget-wide v8, v7, v1

    .line 49
    .line 50
    iget-object v7, v0, Lcom/google/android/gms/internal/ads/zzahj;->zzc:[J

    .line 51
    .line 52
    aget-wide v10, v7, v1

    .line 53
    .line 54
    cmp-long v7, v8, p1

    .line 55
    .line 56
    if-gez v7, :cond_2

    .line 57
    .line 58
    iget v7, v0, Lcom/google/android/gms/internal/ads/zzahj;->zzb:I

    .line 59
    .line 60
    add-int/2addr v7, v2

    .line 61
    if-ge v1, v7, :cond_2

    .line 62
    .line 63
    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/ads/zzahj;->zzb(J)I

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    if-eq p1, v2, :cond_2

    .line 68
    .line 69
    if-eq p1, v1, :cond_2

    .line 70
    .line 71
    iget-object p2, v0, Lcom/google/android/gms/internal/ads/zzahj;->zzf:[J

    .line 72
    .line 73
    aget-wide v1, p2, p1

    .line 74
    .line 75
    iget-object p2, v0, Lcom/google/android/gms/internal/ads/zzahj;->zzc:[J

    .line 76
    .line 77
    aget-wide p1, p2, p1

    .line 78
    .line 79
    move-wide v3, p1

    .line 80
    goto :goto_0

    .line 81
    :cond_2
    move-wide v1, v5

    .line 82
    :goto_0
    move-wide p1, v8

    .line 83
    goto :goto_1

    .line 84
    :cond_3
    const-wide v10, 0x7fffffffffffffffL

    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    move-wide v1, v5

    .line 90
    :goto_1
    const/4 v0, 0x0

    .line 91
    :goto_2
    iget-object v7, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzs:[Lcom/google/android/gms/internal/ads/zzahb;

    .line 92
    .line 93
    array-length v8, v7

    .line 94
    if-ge v0, v8, :cond_6

    .line 95
    .line 96
    iget v8, p0, Lcom/google/android/gms/internal/ads/zzahc;->zzu:I

    .line 97
    .line 98
    if-eq v0, v8, :cond_5

    .line 99
    .line 100
    aget-object v7, v7, v0

    .line 101
    .line 102
    iget-object v7, v7, Lcom/google/android/gms/internal/ads/zzahb;->zzb:Lcom/google/android/gms/internal/ads/zzahj;

    .line 103
    .line 104
    invoke-static {v7, p1, p2, v10, v11}, Lcom/google/android/gms/internal/ads/zzahc;->zzj(Lcom/google/android/gms/internal/ads/zzahj;JJ)J

    .line 105
    .line 106
    .line 107
    move-result-wide v8

    .line 108
    cmp-long v10, v1, v5

    .line 109
    .line 110
    if-eqz v10, :cond_4

    .line 111
    .line 112
    invoke-static {v7, v1, v2, v3, v4}, Lcom/google/android/gms/internal/ads/zzahc;->zzj(Lcom/google/android/gms/internal/ads/zzahj;JJ)J

    .line 113
    .line 114
    .line 115
    move-result-wide v3

    .line 116
    :cond_4
    move-wide v10, v8

    .line 117
    :cond_5
    add-int/lit8 v0, v0, 0x1

    .line 118
    .line 119
    goto :goto_2

    .line 120
    :cond_6
    new-instance v0, Lcom/google/android/gms/internal/ads/zzacb;

    .line 121
    .line 122
    invoke-direct {v0, p1, p2, v10, v11}, Lcom/google/android/gms/internal/ads/zzacb;-><init>(JJ)V

    .line 123
    .line 124
    .line 125
    cmp-long p1, v1, v5

    .line 126
    .line 127
    if-nez p1, :cond_7

    .line 128
    .line 129
    new-instance p1, Lcom/google/android/gms/internal/ads/zzaby;

    .line 130
    .line 131
    invoke-direct {p1, v0, v0}, Lcom/google/android/gms/internal/ads/zzaby;-><init>(Lcom/google/android/gms/internal/ads/zzacb;Lcom/google/android/gms/internal/ads/zzacb;)V

    .line 132
    .line 133
    .line 134
    goto :goto_3

    .line 135
    :cond_7
    new-instance p1, Lcom/google/android/gms/internal/ads/zzacb;

    .line 136
    .line 137
    invoke-direct {p1, v1, v2, v3, v4}, Lcom/google/android/gms/internal/ads/zzacb;-><init>(JJ)V

    .line 138
    .line 139
    .line 140
    new-instance p2, Lcom/google/android/gms/internal/ads/zzaby;

    .line 141
    .line 142
    invoke-direct {p2, v0, p1}, Lcom/google/android/gms/internal/ads/zzaby;-><init>(Lcom/google/android/gms/internal/ads/zzacb;Lcom/google/android/gms/internal/ads/zzacb;)V

    .line 143
    .line 144
    .line 145
    move-object p1, p2

    .line 146
    :goto_3
    return-object p1
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method public final zzh()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

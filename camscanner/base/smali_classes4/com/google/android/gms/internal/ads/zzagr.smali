.class final Lcom/google/android/gms/internal/ads/zzagr;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-ads@@22.4.0"


# static fields
.field private static final zza:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    sget v0, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    .line 2
    .line 3
    const-string v0, "OpusHead"

    .line 4
    .line 5
    sget-object v1, Lcom/google/android/gms/internal/ads/zzfqu;->zzc:Ljava/nio/charset/Charset;

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    sput-object v0, Lcom/google/android/gms/internal/ads/zzagr;->zza:[B

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public static zza(Lcom/google/android/gms/internal/ads/zzagh;)Lcom/google/android/gms/internal/ads/zzbz;
    .locals 14
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const v0, 0x68646c72    # 4.3148E24f

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const v1, 0x6b657973

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const v2, 0x696c7374

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    .line 19
    .line 20
    .line 21
    move-result-object p0

    .line 22
    const/4 v2, 0x0

    .line 23
    if-eqz v0, :cond_8

    .line 24
    .line 25
    if-eqz v1, :cond_8

    .line 26
    .line 27
    if-eqz p0, :cond_8

    .line 28
    .line 29
    iget-object v0, v0, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    .line 30
    .line 31
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzagr;->zzh(Lcom/google/android/gms/internal/ads/zzfb;)I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    const v3, 0x6d647461

    .line 36
    .line 37
    .line 38
    if-eq v0, v3, :cond_0

    .line 39
    .line 40
    goto/16 :goto_5

    .line 41
    .line 42
    :cond_0
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    .line 43
    .line 44
    const/16 v1, 0xc

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    new-array v3, v1, [Ljava/lang/String;

    .line 54
    .line 55
    const/4 v4, 0x0

    .line 56
    const/4 v5, 0x0

    .line 57
    :goto_0
    if-ge v5, v1, :cond_1

    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 60
    .line 61
    .line 62
    move-result v6

    .line 63
    const/4 v7, 0x4

    .line 64
    invoke-virtual {v0, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 65
    .line 66
    .line 67
    add-int/lit8 v6, v6, -0x8

    .line 68
    .line 69
    sget-object v7, Lcom/google/android/gms/internal/ads/zzfqu;->zzc:Ljava/nio/charset/Charset;

    .line 70
    .line 71
    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzy(ILjava/nio/charset/Charset;)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v6

    .line 75
    aput-object v6, v3, v5

    .line 76
    .line 77
    add-int/lit8 v5, v5, 0x1

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_1
    iget-object p0, p0, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    .line 81
    .line 82
    const/16 v0, 0x8

    .line 83
    .line 84
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 85
    .line 86
    .line 87
    new-instance v5, Ljava/util/ArrayList;

    .line 88
    .line 89
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .line 91
    .line 92
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zza()I

    .line 93
    .line 94
    .line 95
    move-result v6

    .line 96
    if-le v6, v0, :cond_6

    .line 97
    .line 98
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 99
    .line 100
    .line 101
    move-result v6

    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 103
    .line 104
    .line 105
    move-result v7

    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 107
    .line 108
    .line 109
    move-result v8

    .line 110
    add-int/lit8 v8, v8, -0x1

    .line 111
    .line 112
    if-ltz v8, :cond_4

    .line 113
    .line 114
    if-ge v8, v1, :cond_4

    .line 115
    .line 116
    aget-object v8, v3, v8

    .line 117
    .line 118
    add-int v9, v6, v7

    .line 119
    .line 120
    sget v10, Lcom/google/android/gms/internal/ads/zzagy;->zzb:I

    .line 121
    .line 122
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 123
    .line 124
    .line 125
    move-result v10

    .line 126
    if-ge v10, v9, :cond_3

    .line 127
    .line 128
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 129
    .line 130
    .line 131
    move-result v11

    .line 132
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 133
    .line 134
    .line 135
    move-result v12

    .line 136
    const v13, 0x64617461

    .line 137
    .line 138
    .line 139
    if-ne v12, v13, :cond_2

    .line 140
    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 142
    .line 143
    .line 144
    move-result v9

    .line 145
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 146
    .line 147
    .line 148
    move-result v10

    .line 149
    add-int/lit8 v11, v11, -0x10

    .line 150
    .line 151
    new-array v12, v11, [B

    .line 152
    .line 153
    invoke-virtual {p0, v12, v4, v11}, Lcom/google/android/gms/internal/ads/zzfb;->zzC([BII)V

    .line 154
    .line 155
    .line 156
    new-instance v11, Lcom/google/android/gms/internal/ads/zzfo;

    .line 157
    .line 158
    invoke-direct {v11, v8, v12, v10, v9}, Lcom/google/android/gms/internal/ads/zzfo;-><init>(Ljava/lang/String;[BII)V

    .line 159
    .line 160
    .line 161
    goto :goto_3

    .line 162
    :cond_2
    add-int/2addr v10, v11

    .line 163
    invoke-virtual {p0, v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 164
    .line 165
    .line 166
    goto :goto_2

    .line 167
    :cond_3
    move-object v11, v2

    .line 168
    :goto_3
    if-eqz v11, :cond_5

    .line 169
    .line 170
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    .line 172
    .line 173
    goto :goto_4

    .line 174
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    .line 175
    .line 176
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    .line 178
    .line 179
    const-string v10, "Skipped metadata with unknown key index: "

    .line 180
    .line 181
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    .line 183
    .line 184
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 185
    .line 186
    .line 187
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object v8

    .line 191
    const-string v9, "AtomParsers"

    .line 192
    .line 193
    invoke-static {v9, v8}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    .line 195
    .line 196
    :cond_5
    :goto_4
    add-int/2addr v6, v7

    .line 197
    invoke-virtual {p0, v6}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 198
    .line 199
    .line 200
    goto :goto_1

    .line 201
    :cond_6
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    .line 202
    .line 203
    .line 204
    move-result p0

    .line 205
    if-eqz p0, :cond_7

    .line 206
    .line 207
    return-object v2

    .line 208
    :cond_7
    new-instance p0, Lcom/google/android/gms/internal/ads/zzbz;

    .line 209
    .line 210
    invoke-direct {p0, v5}, Lcom/google/android/gms/internal/ads/zzbz;-><init>(Ljava/util/List;)V

    .line 211
    .line 212
    .line 213
    return-object p0

    .line 214
    :cond_8
    :goto_5
    return-object v2
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method public static zzb(Lcom/google/android/gms/internal/ads/zzagi;)Lcom/google/android/gms/internal/ads/zzbz;
    .locals 14

    .line 1
    iget-object p0, p0, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    .line 2
    .line 3
    const/16 v0, 0x8

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 6
    .line 7
    .line 8
    new-instance v1, Lcom/google/android/gms/internal/ads/zzbz;

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    new-array v3, v2, [Lcom/google/android/gms/internal/ads/zzby;

    .line 12
    .line 13
    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    invoke-direct {v1, v4, v5, v3}, Lcom/google/android/gms/internal/ads/zzbz;-><init>(J[Lcom/google/android/gms/internal/ads/zzby;)V

    .line 19
    .line 20
    .line 21
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zza()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    if-lt v3, v0, :cond_d

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 32
    .line 33
    .line 34
    move-result v6

    .line 35
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 36
    .line 37
    .line 38
    move-result v7

    .line 39
    const v8, 0x6d657461

    .line 40
    .line 41
    .line 42
    const/4 v9, 0x0

    .line 43
    if-ne v7, v8, :cond_5

    .line 44
    .line 45
    invoke-virtual {p0, v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 46
    .line 47
    .line 48
    add-int v7, v3, v6

    .line 49
    .line 50
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 51
    .line 52
    .line 53
    invoke-static {p0}, Lcom/google/android/gms/internal/ads/zzagr;->zze(Lcom/google/android/gms/internal/ads/zzfb;)V

    .line 54
    .line 55
    .line 56
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 57
    .line 58
    .line 59
    move-result v8

    .line 60
    if-ge v8, v7, :cond_4

    .line 61
    .line 62
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 63
    .line 64
    .line 65
    move-result v8

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 67
    .line 68
    .line 69
    move-result v10

    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 71
    .line 72
    .line 73
    move-result v11

    .line 74
    const v12, 0x696c7374

    .line 75
    .line 76
    .line 77
    if-ne v11, v12, :cond_3

    .line 78
    .line 79
    invoke-virtual {p0, v8}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 80
    .line 81
    .line 82
    add-int/2addr v8, v10

    .line 83
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 84
    .line 85
    .line 86
    new-instance v7, Ljava/util/ArrayList;

    .line 87
    .line 88
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 89
    .line 90
    .line 91
    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 92
    .line 93
    .line 94
    move-result v10

    .line 95
    if-ge v10, v8, :cond_1

    .line 96
    .line 97
    invoke-static {p0}, Lcom/google/android/gms/internal/ads/zzagy;->zza(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzby;

    .line 98
    .line 99
    .line 100
    move-result-object v10

    .line 101
    if-eqz v10, :cond_0

    .line 102
    .line 103
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    .line 105
    .line 106
    goto :goto_2

    .line 107
    :cond_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    .line 108
    .line 109
    .line 110
    move-result v8

    .line 111
    if-eqz v8, :cond_2

    .line 112
    .line 113
    goto :goto_3

    .line 114
    :cond_2
    new-instance v9, Lcom/google/android/gms/internal/ads/zzbz;

    .line 115
    .line 116
    invoke-direct {v9, v7}, Lcom/google/android/gms/internal/ads/zzbz;-><init>(Ljava/util/List;)V

    .line 117
    .line 118
    .line 119
    goto :goto_3

    .line 120
    :cond_3
    add-int/2addr v8, v10

    .line 121
    invoke-virtual {p0, v8}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 122
    .line 123
    .line 124
    goto :goto_1

    .line 125
    :cond_4
    :goto_3
    invoke-virtual {v1, v9}, Lcom/google/android/gms/internal/ads/zzbz;->zzd(Lcom/google/android/gms/internal/ads/zzbz;)Lcom/google/android/gms/internal/ads/zzbz;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    goto/16 :goto_6

    .line 130
    .line 131
    :cond_5
    const v8, 0x736d7461

    .line 132
    .line 133
    .line 134
    if-ne v7, v8, :cond_b

    .line 135
    .line 136
    invoke-virtual {p0, v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 137
    .line 138
    .line 139
    add-int v7, v3, v6

    .line 140
    .line 141
    const/16 v8, 0xc

    .line 142
    .line 143
    invoke-virtual {p0, v8}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 144
    .line 145
    .line 146
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 147
    .line 148
    .line 149
    move-result v10

    .line 150
    if-ge v10, v7, :cond_a

    .line 151
    .line 152
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 153
    .line 154
    .line 155
    move-result v10

    .line 156
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 157
    .line 158
    .line 159
    move-result v11

    .line 160
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 161
    .line 162
    .line 163
    move-result v12

    .line 164
    const v13, 0x73617574

    .line 165
    .line 166
    .line 167
    if-ne v12, v13, :cond_9

    .line 168
    .line 169
    const/16 v7, 0xe

    .line 170
    .line 171
    if-ge v11, v7, :cond_6

    .line 172
    .line 173
    goto :goto_5

    .line 174
    :cond_6
    const/4 v7, 0x5

    .line 175
    invoke-virtual {p0, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 176
    .line 177
    .line 178
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    .line 179
    .line 180
    .line 181
    move-result v7

    .line 182
    const/high16 v10, 0x42f00000    # 120.0f

    .line 183
    .line 184
    if-eq v7, v8, :cond_7

    .line 185
    .line 186
    const/16 v8, 0xd

    .line 187
    .line 188
    if-eq v7, v8, :cond_8

    .line 189
    .line 190
    goto :goto_5

    .line 191
    :cond_7
    if-ne v7, v8, :cond_8

    .line 192
    .line 193
    const/high16 v10, 0x43700000    # 240.0f

    .line 194
    .line 195
    :cond_8
    const/4 v7, 0x1

    .line 196
    invoke-virtual {p0, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    .line 200
    .line 201
    .line 202
    move-result v8

    .line 203
    new-instance v9, Lcom/google/android/gms/internal/ads/zzbz;

    .line 204
    .line 205
    new-array v7, v7, [Lcom/google/android/gms/internal/ads/zzby;

    .line 206
    .line 207
    new-instance v11, Lcom/google/android/gms/internal/ads/zzafi;

    .line 208
    .line 209
    invoke-direct {v11, v10, v8}, Lcom/google/android/gms/internal/ads/zzafi;-><init>(FI)V

    .line 210
    .line 211
    .line 212
    aput-object v11, v7, v2

    .line 213
    .line 214
    invoke-direct {v9, v4, v5, v7}, Lcom/google/android/gms/internal/ads/zzbz;-><init>(J[Lcom/google/android/gms/internal/ads/zzby;)V

    .line 215
    .line 216
    .line 217
    goto :goto_5

    .line 218
    :cond_9
    add-int/2addr v10, v11

    .line 219
    invoke-virtual {p0, v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 220
    .line 221
    .line 222
    goto :goto_4

    .line 223
    :cond_a
    :goto_5
    invoke-virtual {v1, v9}, Lcom/google/android/gms/internal/ads/zzbz;->zzd(Lcom/google/android/gms/internal/ads/zzbz;)Lcom/google/android/gms/internal/ads/zzbz;

    .line 224
    .line 225
    .line 226
    move-result-object v1

    .line 227
    goto :goto_6

    .line 228
    :cond_b
    const v8, -0x56878686

    .line 229
    .line 230
    .line 231
    if-ne v7, v8, :cond_c

    .line 232
    .line 233
    invoke-static {p0}, Lcom/google/android/gms/internal/ads/zzagr;->zzl(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzbz;

    .line 234
    .line 235
    .line 236
    move-result-object v7

    .line 237
    invoke-virtual {v1, v7}, Lcom/google/android/gms/internal/ads/zzbz;->zzd(Lcom/google/android/gms/internal/ads/zzbz;)Lcom/google/android/gms/internal/ads/zzbz;

    .line 238
    .line 239
    .line 240
    move-result-object v1

    .line 241
    :cond_c
    :goto_6
    add-int/2addr v3, v6

    .line 242
    invoke-virtual {p0, v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 243
    .line 244
    .line 245
    goto/16 :goto_0

    .line 246
    .line 247
    :cond_d
    return-object v1
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
.end method

.method public static zzc(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzfu;
    .locals 11

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzagj;->zze(I)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzt()J

    .line 17
    .line 18
    .line 19
    move-result-wide v0

    .line 20
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzt()J

    .line 21
    .line 22
    .line 23
    move-result-wide v2

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzs()J

    .line 26
    .line 27
    .line 28
    move-result-wide v0

    .line 29
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzs()J

    .line 30
    .line 31
    .line 32
    move-result-wide v2

    .line 33
    :goto_0
    move-wide v5, v0

    .line 34
    move-wide v7, v2

    .line 35
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzt()J

    .line 36
    .line 37
    .line 38
    move-result-wide v9

    .line 39
    new-instance p0, Lcom/google/android/gms/internal/ads/zzfu;

    .line 40
    .line 41
    move-object v4, p0

    .line 42
    invoke-direct/range {v4 .. v10}, Lcom/google/android/gms/internal/ads/zzfu;-><init>(JJJ)V

    .line 43
    .line 44
    .line 45
    return-object p0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static zzd(Lcom/google/android/gms/internal/ads/zzagh;Lcom/google/android/gms/internal/ads/zzabq;JLcom/google/android/gms/internal/ads/zzad;ZZLcom/google/android/gms/internal/ads/zzfqw;)Ljava/util/List;
    .locals 59
    .param p4    # Lcom/google/android/gms/internal/ads/zzad;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzcd;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v12, p4

    .line 1
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    const/4 v15, 0x0

    .line 2
    :goto_0
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzagh;->zzc:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v15, v2, :cond_97

    .line 3
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzagh;->zzc:Ljava/util/List;

    invoke-interface {v2, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Lcom/google/android/gms/internal/ads/zzagh;

    .line 4
    iget v2, v11, Lcom/google/android/gms/internal/ads/zzagj;->zzd:I

    const v3, 0x7472616b

    if-eq v2, v3, :cond_0

    move-object v1, v13

    move/from16 v31, v15

    goto/16 :goto_66

    :cond_0
    const v2, 0x6d766864

    .line 5
    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v2

    .line 6
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v10, 0x6d646961

    .line 7
    invoke-virtual {v11, v10}, Lcom/google/android/gms/internal/ads/zzagh;->zza(I)Lcom/google/android/gms/internal/ads/zzagh;

    move-result-object v3

    .line 8
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x68646c72    # 4.3148E24f

    .line 9
    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v4

    .line 10
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 11
    iget-object v4, v4, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    invoke-static {v4}, Lcom/google/android/gms/internal/ads/zzagr;->zzh(Lcom/google/android/gms/internal/ads/zzfb;)I

    move-result v4

    invoke-static {v4}, Lcom/google/android/gms/internal/ads/zzagr;->zzf(I)I

    move-result v9

    const-string v6, "AtomParsers"

    move/from16 v31, v15

    const-wide/16 v14, 0x0

    const/4 v8, -0x1

    if-ne v9, v8, :cond_1

    move-object/from16 v1, p7

    move-object v2, v6

    move-object v3, v11

    move-object/from16 v34, v13

    const/4 v0, -0x1

    :goto_1
    const/4 v5, 0x0

    goto/16 :goto_38

    :cond_1
    const v4, 0x746b6864

    .line 12
    invoke-virtual {v11, v4}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v4

    .line 13
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 14
    iget-object v4, v4, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    const/16 v5, 0x8

    .line 15
    invoke-virtual {v4, v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 16
    invoke-virtual {v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lcom/google/android/gms/internal/ads/zzagj;->zze(I)I

    move-result v21

    if-nez v21, :cond_2

    goto :goto_2

    :cond_2
    const/16 v5, 0x10

    .line 17
    :goto_2
    invoke-virtual {v4, v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 18
    invoke-virtual {v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v5

    const/4 v10, 0x4

    .line 19
    invoke-virtual {v4, v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    invoke-virtual {v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    move-result v25

    const/4 v10, 0x0

    :goto_3
    if-nez v21, :cond_3

    const/4 v7, 0x4

    goto :goto_4

    :cond_3
    const/16 v7, 0x8

    :goto_4
    const-wide v28, -0x7fffffffffffffffL    # -4.9E-324

    if-ge v10, v7, :cond_7

    invoke-virtual {v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    move-result-object v7

    add-int v30, v25, v10

    .line 20
    aget-byte v7, v7, v30

    if-eq v7, v8, :cond_6

    if-nez v21, :cond_4

    .line 21
    invoke-virtual {v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzt()J

    move-result-wide v32

    goto :goto_5

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzu()J

    move-result-wide v32

    :goto_5
    cmp-long v7, v32, v14

    if-nez v7, :cond_5

    goto :goto_6

    :cond_5
    move v10, v9

    move-wide/from16 v8, v32

    goto :goto_7

    :cond_6
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 22
    :cond_7
    invoke-virtual {v4, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    :goto_6
    move v10, v9

    move-wide/from16 v8, v28

    :goto_7
    const/16 v7, 0x10

    .line 23
    invoke-virtual {v4, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 24
    invoke-virtual {v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v23

    .line 25
    invoke-virtual {v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v7

    const/4 v14, 0x4

    .line 26
    invoke-virtual {v4, v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 27
    invoke-virtual {v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v14

    .line 28
    invoke-virtual {v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v4

    const/high16 v15, 0x10000

    const/high16 v0, -0x10000

    if-nez v23, :cond_b

    if-ne v7, v15, :cond_a

    if-ne v14, v0, :cond_9

    if-nez v4, :cond_8

    const/16 v0, 0x5a

    goto :goto_a

    :cond_8
    const/high16 v7, 0x10000

    const/high16 v14, -0x10000

    goto :goto_8

    :cond_9
    const/high16 v7, 0x10000

    :cond_a
    :goto_8
    const/16 v23, 0x0

    :cond_b
    if-nez v23, :cond_f

    if-ne v7, v0, :cond_e

    if-ne v14, v15, :cond_c

    if-nez v4, :cond_d

    const/16 v0, 0x10e

    goto :goto_a

    :cond_c
    move v15, v14

    :cond_d
    const/4 v7, 0x0

    const/high16 v14, -0x10000

    goto :goto_9

    :cond_e
    move v15, v14

    move v14, v7

    const/4 v7, 0x0

    goto :goto_9

    :cond_f
    move v15, v14

    move v14, v7

    move/from16 v7, v23

    :goto_9
    if-ne v7, v0, :cond_10

    if-nez v14, :cond_10

    if-nez v15, :cond_10

    if-ne v4, v0, :cond_10

    const/16 v0, 0xb4

    goto :goto_a

    :cond_10
    const/4 v0, 0x0

    :goto_a
    new-instance v14, Lcom/google/android/gms/internal/ads/zzagq;

    invoke-direct {v14, v5, v8, v9, v0}, Lcom/google/android/gms/internal/ads/zzagq;-><init>(IJI)V

    cmp-long v0, p2, v28

    if-nez v0, :cond_11

    invoke-static {v14}, Lcom/google/android/gms/internal/ads/zzagq;->zzc(Lcom/google/android/gms/internal/ads/zzagq;)J

    move-result-wide v4

    move-wide/from16 v34, v4

    goto :goto_b

    :cond_11
    move-wide/from16 v34, p2

    :goto_b
    iget-object v0, v2, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    .line 29
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzagr;->zzc(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzfu;

    move-result-object v0

    iget-wide v8, v0, Lcom/google/android/gms/internal/ads/zzfu;->zzc:J

    cmp-long v0, v34, v28

    if-nez v0, :cond_12

    goto :goto_c

    :cond_12
    const-wide/32 v36, 0xf4240

    move-wide/from16 v38, v8

    .line 30
    invoke-static/range {v34 .. v39}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v4

    move-wide/from16 v28, v4

    :goto_c
    const v0, 0x6d696e66

    .line 31
    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/ads/zzagh;->zza(I)Lcom/google/android/gms/internal/ads/zzagh;

    move-result-object v2

    .line 32
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v15, 0x7374626c

    .line 33
    invoke-virtual {v2, v15}, Lcom/google/android/gms/internal/ads/zzagh;->zza(I)Lcom/google/android/gms/internal/ads/zzagh;

    move-result-object v2

    .line 34
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x6d646864

    .line 35
    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v3

    .line 36
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 37
    iget-object v3, v3, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    invoke-static {v3}, Lcom/google/android/gms/internal/ads/zzagr;->zzj(Lcom/google/android/gms/internal/ads/zzfb;)Landroid/util/Pair;

    move-result-object v7

    const v3, 0x73747364

    .line 38
    invoke-virtual {v2, v3}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v2

    if-eqz v2, :cond_96

    .line 39
    iget-object v5, v2, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    invoke-static {v14}, Lcom/google/android/gms/internal/ads/zzagq;->zza(Lcom/google/android/gms/internal/ads/zzagq;)I

    move-result v4

    invoke-static {v14}, Lcom/google/android/gms/internal/ads/zzagq;->zzb(Lcom/google/android/gms/internal/ads/zzagq;)I

    move-result v3

    .line 40
    iget-object v2, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    const/16 v0, 0xc

    .line 41
    invoke-virtual {v5, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 42
    invoke-virtual {v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v0

    new-instance v15, Lcom/google/android/gms/internal/ads/zzagn;

    .line 43
    invoke-direct {v15, v0}, Lcom/google/android/gms/internal/ads/zzagn;-><init>(I)V

    move-object/from16 v34, v13

    const/4 v13, 0x0

    :goto_d
    if-ge v13, v0, :cond_57

    move/from16 v23, v0

    invoke-virtual {v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    move-result v0

    .line 44
    invoke-virtual {v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v1

    if-lez v1, :cond_13

    move/from16 v25, v3

    move-object/from16 v26, v6

    const/4 v3, 0x1

    goto :goto_e

    :cond_13
    move/from16 v25, v3

    move-object/from16 v26, v6

    const/4 v3, 0x0

    :goto_e
    const-string v6, "childAtomSize must be positive"

    .line 45
    invoke-static {v3, v6}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    .line 46
    invoke-virtual {v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v3

    const v6, 0x61766331

    move-object/from16 v30, v7

    const v7, 0x656e6376

    if-eq v3, v6, :cond_1f

    const v6, 0x61766333

    if-eq v3, v6, :cond_1f

    if-eq v3, v7, :cond_1f

    const v6, 0x6d317620

    if-eq v3, v6, :cond_1f

    const v6, 0x6d703476

    if-eq v3, v6, :cond_1f

    const v6, 0x68766331

    if-eq v3, v6, :cond_1f

    const v6, 0x68657631

    if-eq v3, v6, :cond_1f

    const v6, 0x73323633

    if-eq v3, v6, :cond_1f

    const v6, 0x48323633

    if-eq v3, v6, :cond_1f

    const v6, 0x76703038

    if-eq v3, v6, :cond_1f

    const v6, 0x76703039

    if-eq v3, v6, :cond_1f

    const v6, 0x61763031

    if-eq v3, v6, :cond_1f

    const v6, 0x64766176

    if-eq v3, v6, :cond_1f

    const v6, 0x64766131

    if-eq v3, v6, :cond_1f

    const v6, 0x64766865

    if-eq v3, v6, :cond_1f

    const v6, 0x64766831

    if-ne v3, v6, :cond_14

    goto/16 :goto_16

    :cond_14
    const v6, 0x6d703461

    if-eq v3, v6, :cond_1e

    const v6, 0x656e6361

    if-eq v3, v6, :cond_1e

    const v6, 0x61632d33

    if-eq v3, v6, :cond_1e

    const v6, 0x65632d33

    if-eq v3, v6, :cond_1e

    const v6, 0x61632d34

    if-eq v3, v6, :cond_1e

    const v6, 0x6d6c7061

    if-eq v3, v6, :cond_1e

    const v6, 0x64747363

    if-eq v3, v6, :cond_1e

    const v6, 0x64747365

    if-eq v3, v6, :cond_1e

    const v6, 0x64747368

    if-eq v3, v6, :cond_1e

    const v6, 0x6474736c

    if-eq v3, v6, :cond_1e

    const v6, 0x64747378

    if-eq v3, v6, :cond_1e

    const v6, 0x73616d72

    if-eq v3, v6, :cond_1e

    const v6, 0x73617762

    if-eq v3, v6, :cond_1e

    const v6, 0x6c70636d

    if-eq v3, v6, :cond_1e

    const v6, 0x736f7774

    if-eq v3, v6, :cond_1e

    const v6, 0x74776f73

    if-eq v3, v6, :cond_1e

    const v6, 0x2e6d7032

    if-eq v3, v6, :cond_1e

    const v6, 0x2e6d7033

    if-eq v3, v6, :cond_1e

    const v6, 0x6d686131

    if-eq v3, v6, :cond_1e

    const v6, 0x6d686d31

    if-eq v3, v6, :cond_1e

    const v6, 0x616c6163

    if-eq v3, v6, :cond_1e

    const v6, 0x616c6177

    if-eq v3, v6, :cond_1e

    const v6, 0x756c6177

    if-eq v3, v6, :cond_1e

    const v6, 0x4f707573

    if-eq v3, v6, :cond_1e

    const v6, 0x664c6143

    if-ne v3, v6, :cond_15

    move-object/from16 v38, v2

    move-wide/from16 v36, v8

    goto/16 :goto_14

    :cond_15
    const v6, 0x54544d4c

    if-eq v3, v6, :cond_19

    const v6, 0x74783367

    if-eq v3, v6, :cond_19

    const v6, 0x77767474

    if-eq v3, v6, :cond_19

    const v6, 0x73747070

    if-eq v3, v6, :cond_19

    const v6, 0x63363038

    if-ne v3, v6, :cond_16

    goto :goto_10

    :cond_16
    const v6, 0x6d657474

    if-ne v3, v6, :cond_17

    const v3, 0x6d657474

    .line 47
    invoke-static {v5, v3, v0, v4, v15}, Lcom/google/android/gms/internal/ads/zzagr;->zzp(Lcom/google/android/gms/internal/ads/zzfb;IIILcom/google/android/gms/internal/ads/zzagn;)V

    goto :goto_f

    :cond_17
    const v6, 0x63616d6d

    if-ne v3, v6, :cond_18

    new-instance v3, Lcom/google/android/gms/internal/ads/zzak;

    invoke-direct {v3}, Lcom/google/android/gms/internal/ads/zzak;-><init>()V

    .line 48
    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/ads/zzak;->zzG(I)Lcom/google/android/gms/internal/ads/zzak;

    const-string v6, "application/x-camera-motion"

    .line 49
    invoke-virtual {v3, v6}, Lcom/google/android/gms/internal/ads/zzak;->zzS(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    .line 50
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzak;->zzY()Lcom/google/android/gms/internal/ads/zzam;

    move-result-object v3

    iput-object v3, v15, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    :cond_18
    :goto_f
    move/from16 v36, v1

    move-object/from16 v38, v2

    move v3, v4

    move-wide/from16 v21, v8

    move/from16 v18, v10

    move-object/from16 v45, v11

    move/from16 v20, v13

    move-object/from16 v16, v14

    move/from16 v4, v25

    move-object/from16 v2, v26

    move-object/from16 v42, v30

    move/from16 v30, v0

    goto/16 :goto_13

    :cond_19
    :goto_10
    add-int/lit8 v6, v0, 0x10

    .line 51
    invoke-virtual {v5, v6}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    const v6, 0x54544d4c

    const-wide v35, 0x7fffffffffffffffL

    if-ne v3, v6, :cond_1a

    const-string v3, "application/ttml+xml"

    :goto_11
    const/4 v6, 0x0

    move-wide/from16 v56, v8

    move-wide/from16 v7, v35

    move-wide/from16 v36, v56

    goto :goto_12

    :cond_1a
    const v6, 0x74783367

    if-ne v3, v6, :cond_1b

    add-int/lit8 v3, v1, -0x10

    .line 52
    new-array v6, v3, [B

    const/4 v7, 0x0

    .line 53
    invoke-virtual {v5, v6, v7, v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzC([BII)V

    .line 54
    invoke-static {v6}, Lcom/google/android/gms/internal/ads/zzfud;->zzm(Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    move-result-object v3

    const-string v6, "application/x-quicktime-tx3g"

    move-object/from16 v56, v6

    move-object v6, v3

    move-object/from16 v3, v56

    move-wide/from16 v57, v8

    move-wide/from16 v7, v35

    move-wide/from16 v36, v57

    goto :goto_12

    :cond_1b
    const v6, 0x77767474

    if-ne v3, v6, :cond_1c

    const-string v3, "application/x-mp4-vtt"

    goto :goto_11

    :cond_1c
    const v6, 0x73747070

    if-ne v3, v6, :cond_1d

    const-string v3, "application/ttml+xml"

    move-wide/from16 v36, v8

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    goto :goto_12

    :cond_1d
    const/4 v7, 0x1

    iput v7, v15, Lcom/google/android/gms/internal/ads/zzagn;->zzd:I

    const-string v3, "application/x-mp4-cea-608"

    goto :goto_11

    .line 55
    :goto_12
    new-instance v9, Lcom/google/android/gms/internal/ads/zzak;

    invoke-direct {v9}, Lcom/google/android/gms/internal/ads/zzak;-><init>()V

    .line 56
    invoke-virtual {v9, v4}, Lcom/google/android/gms/internal/ads/zzak;->zzG(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 57
    invoke-virtual {v9, v3}, Lcom/google/android/gms/internal/ads/zzak;->zzS(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    .line 58
    invoke-virtual {v9, v2}, Lcom/google/android/gms/internal/ads/zzak;->zzK(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    .line 59
    invoke-virtual {v9, v7, v8}, Lcom/google/android/gms/internal/ads/zzak;->zzW(J)Lcom/google/android/gms/internal/ads/zzak;

    .line 60
    invoke-virtual {v9, v6}, Lcom/google/android/gms/internal/ads/zzak;->zzI(Ljava/util/List;)Lcom/google/android/gms/internal/ads/zzak;

    .line 61
    invoke-virtual {v9}, Lcom/google/android/gms/internal/ads/zzak;->zzY()Lcom/google/android/gms/internal/ads/zzam;

    move-result-object v3

    iput-object v3, v15, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    move-object/from16 v38, v2

    move v3, v4

    move/from16 v18, v10

    move-object/from16 v45, v11

    move/from16 v20, v13

    move-object/from16 v16, v14

    move/from16 v4, v25

    move-object/from16 v2, v26

    move-object/from16 v42, v30

    move-wide/from16 v21, v36

    move/from16 v30, v0

    move/from16 v36, v1

    :goto_13
    move-object v14, v5

    move-object v5, v15

    goto :goto_15

    :cond_1e
    move-wide/from16 v36, v8

    move-object/from16 v38, v2

    :goto_14
    move-object v2, v5

    move/from16 v9, v25

    move v7, v4

    const/4 v8, 0x2

    move v4, v0

    move-object/from16 v16, v14

    const/4 v6, 0x0

    move-object v14, v5

    move v5, v1

    move-object/from16 v41, v26

    move v6, v7

    move/from16 v43, v7

    move-object/from16 v42, v30

    const/16 v12, 0x10

    move-object/from16 v7, v38

    move-wide/from16 v21, v36

    move/from16 v8, p6

    move/from16 v18, v10

    move v10, v9

    move-object/from16 v9, p4

    move/from16 v44, v10

    move-object v10, v15

    move-object/from16 v45, v11

    move v11, v13

    .line 62
    invoke-static/range {v2 .. v11}, Lcom/google/android/gms/internal/ads/zzagr;->zzo(Lcom/google/android/gms/internal/ads/zzfb;IIIILjava/lang/String;ZLcom/google/android/gms/internal/ads/zzad;Lcom/google/android/gms/internal/ads/zzagn;I)V

    move/from16 v30, v0

    move/from16 v36, v1

    move/from16 v20, v13

    move-object v5, v15

    move-object/from16 v2, v41

    move/from16 v3, v43

    move/from16 v4, v44

    :goto_15
    const/4 v0, -0x1

    goto/16 :goto_36

    :cond_1f
    :goto_16
    move-object/from16 v38, v2

    move/from16 v43, v4

    move-wide/from16 v21, v8

    move/from16 v18, v10

    move-object/from16 v45, v11

    move-object/from16 v16, v14

    move/from16 v44, v25

    move-object/from16 v41, v26

    move-object/from16 v42, v30

    const/16 v12, 0x10

    move-object v14, v5

    add-int/lit8 v2, v0, 0x10

    .line 63
    invoke-virtual {v14, v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 64
    invoke-virtual {v14, v12}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 65
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzp()I

    move-result v2

    .line 66
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzp()I

    move-result v4

    const/16 v5, 0x32

    .line 67
    invoke-virtual {v14, v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    move-result v5

    if-ne v3, v7, :cond_22

    .line 68
    invoke-static {v14, v0, v1}, Lcom/google/android/gms/internal/ads/zzagr;->zzk(Lcom/google/android/gms/internal/ads/zzfb;II)Landroid/util/Pair;

    move-result-object v3

    if-eqz v3, :cond_21

    .line 69
    iget-object v6, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move-object/from16 v8, p4

    const/16 v9, 0x10

    if-nez v8, :cond_20

    const/4 v7, 0x0

    goto :goto_17

    .line 70
    :cond_20
    iget-object v7, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Lcom/google/android/gms/internal/ads/zzahh;

    iget-object v7, v7, Lcom/google/android/gms/internal/ads/zzahh;->zzb:Ljava/lang/String;

    invoke-virtual {v8, v7}, Lcom/google/android/gms/internal/ads/zzad;->zzb(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzad;

    move-result-object v7

    .line 71
    :goto_17
    iget-object v10, v15, Lcom/google/android/gms/internal/ads/zzagn;->zza:[Lcom/google/android/gms/internal/ads/zzahh;

    .line 72
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/gms/internal/ads/zzahh;

    aput-object v3, v10, v13

    move v3, v6

    goto :goto_18

    :cond_21
    move-object/from16 v8, p4

    const/16 v9, 0x10

    move-object v7, v8

    const v3, 0x656e6376

    .line 73
    :goto_18
    invoke-virtual {v14, v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    goto :goto_19

    :cond_22
    move-object/from16 v8, p4

    const/16 v9, 0x10

    move-object v7, v8

    :goto_19
    const v6, 0x6d317620

    if-ne v3, v6, :cond_23

    const-string v6, "video/mpeg"

    move-object/from16 v56, v6

    move v6, v3

    move-object/from16 v3, v56

    goto :goto_1a

    :cond_23
    const v6, 0x48323633

    if-ne v3, v6, :cond_24

    const-string v3, "video/3gpp"

    goto :goto_1a

    :cond_24
    move v6, v3

    const/4 v3, 0x0

    :goto_1a
    const/high16 v10, 0x3f800000    # 1.0f

    move v11, v5

    move-object/from16 v24, v7

    move/from16 v20, v13

    const/4 v7, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v13, -0x1

    const/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v25, 0x0

    const/16 v26, -0x1

    const/16 v46, -0x1

    move-object v5, v3

    const/4 v3, 0x0

    :goto_1b
    sub-int v10, v11, v0

    if-ge v10, v1, :cond_4f

    .line 74
    invoke-virtual {v14, v11}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    move-result v10

    .line 75
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v30

    if-nez v30, :cond_26

    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    move-result v30

    move-object/from16 v35, v8

    sub-int v8, v30, v0

    if-ne v8, v1, :cond_25

    move/from16 v30, v0

    move/from16 v36, v1

    move/from16 v50, v2

    move/from16 v49, v4

    goto/16 :goto_32

    :cond_25
    const/4 v8, 0x0

    goto :goto_1c

    :cond_26
    move-object/from16 v35, v8

    move/from16 v8, v30

    :goto_1c
    if-lez v8, :cond_27

    move/from16 v30, v0

    move/from16 v36, v1

    const/4 v0, 0x1

    goto :goto_1d

    :cond_27
    move/from16 v30, v0

    move/from16 v36, v1

    const/4 v0, 0x0

    :goto_1d
    const-string v1, "childAtomSize must be positive"

    .line 76
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    .line 77
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v0

    const v1, 0x61766343

    if-ne v0, v1, :cond_2a

    if-nez v5, :cond_28

    const/4 v0, 0x1

    goto :goto_1e

    :cond_28
    const/4 v0, 0x0

    :goto_1e
    const/4 v1, 0x0

    .line 78
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    add-int/lit8 v10, v10, 0x8

    .line 79
    invoke-virtual {v14, v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 80
    invoke-static {v14}, Lcom/google/android/gms/internal/ads/zzaag;->zza(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzaag;

    move-result-object v0

    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzaag;->zza:Ljava/util/List;

    iget v7, v0, Lcom/google/android/gms/internal/ads/zzaag;->zzb:I

    iput v7, v15, Lcom/google/android/gms/internal/ads/zzagn;->zzc:I

    if-nez v3, :cond_29

    iget v12, v0, Lcom/google/android/gms/internal/ads/zzaag;->zzh:F

    :cond_29
    iget-object v7, v0, Lcom/google/android/gms/internal/ads/zzaag;->zzi:Ljava/lang/String;

    iget v9, v0, Lcom/google/android/gms/internal/ads/zzaag;->zze:I

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaag;->zzf:I

    iget v0, v0, Lcom/google/android/gms/internal/ads/zzaag;->zzg:I

    const-string v13, "video/avc"

    move/from16 v50, v2

    move/from16 v49, v4

    move-object/from16 v25, v5

    move/from16 v39, v6

    move/from16 v46, v10

    move-object v5, v13

    move-object/from16 v47, v15

    move-object/from16 v2, v41

    move v13, v9

    move-object v9, v7

    move v7, v0

    :goto_1f
    const/4 v0, -0x1

    goto/16 :goto_31

    :cond_2a
    const v1, 0x68766343

    if-ne v0, v1, :cond_2d

    if-nez v5, :cond_2b

    const/4 v0, 0x1

    goto :goto_20

    :cond_2b
    const/4 v0, 0x0

    :goto_20
    const/4 v1, 0x0

    .line 81
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    add-int/lit8 v10, v10, 0x8

    .line 82
    invoke-virtual {v14, v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 83
    invoke-static {v14}, Lcom/google/android/gms/internal/ads/zzabr;->zza(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzabr;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzabr;->zza:Ljava/util/List;

    iget v5, v0, Lcom/google/android/gms/internal/ads/zzabr;->zzb:I

    iput v5, v15, Lcom/google/android/gms/internal/ads/zzagn;->zzc:I

    if-nez v3, :cond_2c

    iget v12, v0, Lcom/google/android/gms/internal/ads/zzabr;->zzf:F

    :cond_2c
    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzabr;->zzg:Ljava/lang/String;

    iget v7, v0, Lcom/google/android/gms/internal/ads/zzabr;->zzc:I

    iget v9, v0, Lcom/google/android/gms/internal/ads/zzabr;->zzd:I

    iget v0, v0, Lcom/google/android/gms/internal/ads/zzabr;->zze:I

    const-string v10, "video/hevc"

    move-object/from16 v25, v1

    move/from16 v50, v2

    move/from16 v49, v4

    move/from16 v39, v6

    move v13, v7

    move/from16 v46, v9

    move-object/from16 v47, v15

    move-object/from16 v2, v41

    move v7, v0

    move-object v9, v5

    move-object v5, v10

    goto :goto_1f

    :cond_2d
    const v1, 0x64766343

    if-eq v0, v1, :cond_4d

    const v1, 0x64767643

    if-ne v0, v1, :cond_2e

    goto/16 :goto_2f

    :cond_2e
    const v1, 0x76706343

    if-ne v0, v1, :cond_32

    if-nez v5, :cond_2f

    const/4 v0, 0x1

    goto :goto_21

    :cond_2f
    const/4 v0, 0x0

    :goto_21
    const/4 v1, 0x0

    .line 84
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    add-int/lit8 v10, v10, 0xc

    .line 85
    invoke-virtual {v14, v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    const/4 v1, 0x2

    .line 86
    invoke-virtual {v14, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 87
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    move-result v0

    const/4 v5, 0x1

    and-int/2addr v0, v5

    .line 88
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    move-result v7

    .line 89
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    move-result v10

    .line 90
    invoke-static {v7}, Lcom/google/android/gms/internal/ads/zzs;->zza(I)I

    move-result v7

    if-eq v5, v0, :cond_30

    const/4 v0, 0x2

    goto :goto_22

    :cond_30
    const/4 v0, 0x1

    :goto_22
    invoke-static {v10}, Lcom/google/android/gms/internal/ads/zzs;->zzb(I)I

    move-result v10

    const v13, 0x76703038

    if-ne v6, v13, :cond_31

    const-string v13, "video/x-vnd.on2.vp8"

    goto :goto_23

    :cond_31
    const-string v13, "video/x-vnd.on2.vp9"

    :goto_23
    move/from16 v46, v0

    move/from16 v50, v2

    move/from16 v49, v4

    move/from16 v39, v6

    move-object v5, v13

    move-object/from16 v47, v15

    move-object/from16 v2, v41

    const/4 v0, -0x1

    move v13, v7

    move v7, v10

    goto/16 :goto_31

    :cond_32
    const v1, 0x61763143

    if-ne v0, v1, :cond_34

    if-nez v5, :cond_33

    const/4 v0, 0x1

    goto :goto_24

    :cond_33
    const/4 v0, 0x0

    :goto_24
    const/4 v1, 0x0

    .line 91
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    const-string v0, "video/av01"

    move-object v5, v0

    goto :goto_25

    :cond_34
    const v1, 0x636c6c69

    if-ne v0, v1, :cond_36

    if-nez v17, :cond_35

    .line 92
    invoke-static {}, Lcom/google/android/gms/internal/ads/zzagr;->zzn()Ljava/nio/ByteBuffer;

    move-result-object v17

    :cond_35
    move-object/from16 v0, v17

    const/16 v1, 0x15

    .line 93
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 94
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 95
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    move-object/from16 v17, v0

    :goto_25
    move/from16 v50, v2

    move/from16 v49, v4

    move/from16 v39, v6

    move-object/from16 v47, v15

    move-object/from16 v2, v41

    goto/16 :goto_1f

    :cond_36
    const v1, 0x6d646376

    if-ne v0, v1, :cond_38

    if-nez v17, :cond_37

    .line 96
    invoke-static {}, Lcom/google/android/gms/internal/ads/zzagr;->zzn()Ljava/nio/ByteBuffer;

    move-result-object v17

    :cond_37
    move-object/from16 v0, v17

    .line 97
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    move-result v1

    .line 98
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    move-result v10

    move/from16 v37, v3

    .line 99
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    move-result v3

    move/from16 v39, v6

    .line 100
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    move-result v6

    move-object/from16 v47, v15

    .line 101
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    move-result v15

    move/from16 v48, v12

    .line 102
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    move-result v12

    move/from16 v49, v4

    .line 103
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    move-result v4

    move/from16 v50, v2

    .line 104
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    move-result v2

    .line 105
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzt()J

    move-result-wide v51

    .line 106
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzt()J

    move-result-wide v53

    move-object/from16 v55, v9

    const/4 v9, 0x1

    .line 107
    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 108
    invoke-virtual {v0, v15}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 109
    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 110
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 111
    invoke-virtual {v0, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 112
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 113
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 114
    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 115
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    const-wide/16 v1, 0x2710

    div-long v1, v51, v1

    long-to-int v2, v1

    int-to-short v1, v2

    .line 116
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    const-wide/16 v1, 0x2710

    div-long v1, v53, v1

    long-to-int v2, v1

    int-to-short v1, v2

    .line 117
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    move-object/from16 v17, v0

    :goto_26
    move/from16 v3, v37

    move-object/from16 v2, v41

    move/from16 v12, v48

    move-object/from16 v9, v55

    goto/16 :goto_1f

    :cond_38
    move/from16 v50, v2

    move/from16 v37, v3

    move/from16 v49, v4

    move/from16 v39, v6

    move-object/from16 v55, v9

    move/from16 v48, v12

    move-object/from16 v47, v15

    const v1, 0x64323633

    if-ne v0, v1, :cond_3a

    if-nez v5, :cond_39

    const/4 v0, 0x1

    goto :goto_27

    :cond_39
    const/4 v0, 0x0

    :goto_27
    const/4 v1, 0x0

    .line 118
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    const-string v0, "video/3gpp"

    move-object v5, v0

    goto :goto_26

    :cond_3a
    const/4 v1, 0x0

    const v2, 0x65736473

    if-ne v0, v2, :cond_3d

    if-nez v5, :cond_3b

    const/4 v0, 0x1

    goto :goto_28

    :cond_3b
    const/4 v0, 0x0

    .line 119
    :goto_28
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    .line 120
    invoke-static {v14, v10}, Lcom/google/android/gms/internal/ads/zzagr;->zzm(Lcom/google/android/gms/internal/ads/zzfb;I)Lcom/google/android/gms/internal/ads/zzagl;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzagl;->zzc(Lcom/google/android/gms/internal/ads/zzagl;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzagl;->zzd(Lcom/google/android/gms/internal/ads/zzagl;)[B

    move-result-object v2

    if-eqz v2, :cond_3c

    .line 121
    invoke-static {v2}, Lcom/google/android/gms/internal/ads/zzfud;->zzm(Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    move-result-object v2

    move-object/from16 v19, v0

    move-object v5, v1

    move-object/from16 v25, v2

    goto :goto_26

    :cond_3c
    move-object/from16 v19, v0

    move-object v5, v1

    goto :goto_26

    :cond_3d
    const v1, 0x70617370

    if-ne v0, v1, :cond_3e

    add-int/lit8 v10, v10, 0x8

    .line 122
    invoke-virtual {v14, v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 123
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v0

    .line 124
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v1

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    move v12, v0

    move-object/from16 v2, v41

    move-object/from16 v9, v55

    const/4 v0, -0x1

    const/4 v3, 0x1

    goto/16 :goto_31

    :cond_3e
    const v1, 0x73763364

    if-ne v0, v1, :cond_3f

    .line 125
    invoke-static {v14, v10, v8}, Lcom/google/android/gms/internal/ads/zzagr;->zzs(Lcom/google/android/gms/internal/ads/zzfb;II)[B

    move-result-object v0

    move-object/from16 v35, v0

    goto :goto_26

    :cond_3f
    const v1, 0x73743364

    if-ne v0, v1, :cond_44

    .line 126
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    move-result v0

    const/4 v4, 0x3

    .line 127
    invoke-virtual {v14, v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    if-nez v0, :cond_4c

    .line 128
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    move-result v0

    if-eqz v0, :cond_43

    const/4 v1, 0x1

    if-eq v0, v1, :cond_42

    const/4 v1, 0x2

    if-eq v0, v1, :cond_41

    if-eq v0, v4, :cond_40

    goto/16 :goto_2e

    :cond_40
    move/from16 v3, v37

    move-object/from16 v2, v41

    move/from16 v12, v48

    move-object/from16 v9, v55

    const/4 v0, -0x1

    const/16 v26, 0x3

    goto/16 :goto_31

    :cond_41
    move/from16 v3, v37

    move-object/from16 v2, v41

    move/from16 v12, v48

    move-object/from16 v9, v55

    const/4 v0, -0x1

    const/16 v26, 0x2

    goto/16 :goto_31

    :cond_42
    move/from16 v3, v37

    move-object/from16 v2, v41

    move/from16 v12, v48

    move-object/from16 v9, v55

    const/4 v0, -0x1

    const/16 v26, 0x1

    goto/16 :goto_31

    :cond_43
    move/from16 v3, v37

    move-object/from16 v2, v41

    move/from16 v12, v48

    move-object/from16 v9, v55

    const/4 v0, -0x1

    const/16 v26, 0x0

    goto/16 :goto_31

    :cond_44
    const v1, 0x636f6c72

    if-ne v0, v1, :cond_4c

    const/4 v0, -0x1

    if-ne v13, v0, :cond_4b

    if-ne v7, v0, :cond_4a

    .line 129
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v1

    const v2, 0x6e636c78

    if-eq v1, v2, :cond_46

    const v2, 0x6e636c63

    if-ne v1, v2, :cond_45

    goto :goto_29

    :cond_45
    const-string v2, "Unsupported color type: "

    .line 130
    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzagj;->zzf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, v41

    invoke-static {v2, v1}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v3, v37

    move/from16 v12, v48

    move-object/from16 v9, v55

    const/4 v7, -0x1

    goto :goto_2d

    :cond_46
    :goto_29
    move-object/from16 v2, v41

    .line 131
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzp()I

    move-result v1

    .line 132
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzp()I

    move-result v3

    const/4 v4, 0x2

    .line 133
    invoke-virtual {v14, v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    const/16 v4, 0x13

    if-ne v8, v4, :cond_48

    .line 134
    invoke-virtual {v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    move-result v6

    and-int/lit16 v6, v6, 0x80

    if-eqz v6, :cond_47

    const/4 v4, 0x1

    goto :goto_2a

    :cond_47
    const/4 v4, 0x0

    :goto_2a
    const/16 v8, 0x13

    goto :goto_2b

    :cond_48
    const/4 v4, 0x0

    .line 135
    :goto_2b
    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzs;->zza(I)I

    move-result v1

    const/4 v6, 0x1

    if-eq v6, v4, :cond_49

    const/4 v4, 0x2

    goto :goto_2c

    :cond_49
    const/4 v4, 0x1

    :goto_2c
    invoke-static {v3}, Lcom/google/android/gms/internal/ads/zzs;->zzb(I)I

    move-result v3

    move v13, v1

    move v7, v3

    move/from16 v46, v4

    goto :goto_30

    :cond_4a
    move-object/from16 v2, v41

    move/from16 v3, v37

    move/from16 v12, v48

    move-object/from16 v9, v55

    :goto_2d
    const/4 v13, -0x1

    goto :goto_31

    :cond_4b
    move-object/from16 v2, v41

    goto :goto_30

    :cond_4c
    :goto_2e
    move-object/from16 v2, v41

    const/4 v0, -0x1

    goto :goto_30

    :cond_4d
    :goto_2f
    move/from16 v50, v2

    move/from16 v37, v3

    move/from16 v49, v4

    move/from16 v39, v6

    move-object/from16 v55, v9

    move/from16 v48, v12

    move-object/from16 v47, v15

    move-object/from16 v2, v41

    const/4 v0, -0x1

    .line 136
    invoke-static {v14}, Lcom/google/android/gms/internal/ads/zzaax;->zza(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzaax;

    move-result-object v1

    if-eqz v1, :cond_4e

    iget-object v1, v1, Lcom/google/android/gms/internal/ads/zzaax;->zza:Ljava/lang/String;

    const-string v3, "video/dolby-vision"

    move-object v9, v1

    move-object v5, v3

    move/from16 v3, v37

    move/from16 v12, v48

    goto :goto_31

    :cond_4e
    :goto_30
    move/from16 v3, v37

    move/from16 v12, v48

    move-object/from16 v9, v55

    :goto_31
    add-int/2addr v11, v8

    move-object/from16 v41, v2

    move/from16 v0, v30

    move-object/from16 v8, v35

    move/from16 v1, v36

    move/from16 v6, v39

    move-object/from16 v15, v47

    move/from16 v4, v49

    move/from16 v2, v50

    goto/16 :goto_1b

    :cond_4f
    move/from16 v30, v0

    move/from16 v36, v1

    move/from16 v50, v2

    move/from16 v49, v4

    move-object/from16 v35, v8

    :goto_32
    move-object/from16 v55, v9

    move/from16 v48, v12

    move-object/from16 v47, v15

    move-object/from16 v2, v41

    const/4 v0, -0x1

    if-nez v5, :cond_50

    move/from16 v3, v43

    move/from16 v4, v44

    move-object/from16 v5, v47

    goto/16 :goto_36

    .line 137
    :cond_50
    new-instance v1, Lcom/google/android/gms/internal/ads/zzak;

    invoke-direct {v1}, Lcom/google/android/gms/internal/ads/zzak;-><init>()V

    move/from16 v3, v43

    .line 138
    invoke-virtual {v1, v3}, Lcom/google/android/gms/internal/ads/zzak;->zzG(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 139
    invoke-virtual {v1, v5}, Lcom/google/android/gms/internal/ads/zzak;->zzS(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    move-object/from16 v9, v55

    .line 140
    invoke-virtual {v1, v9}, Lcom/google/android/gms/internal/ads/zzak;->zzx(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    move/from16 v4, v50

    .line 141
    invoke-virtual {v1, v4}, Lcom/google/android/gms/internal/ads/zzak;->zzX(I)Lcom/google/android/gms/internal/ads/zzak;

    move/from16 v4, v49

    .line 142
    invoke-virtual {v1, v4}, Lcom/google/android/gms/internal/ads/zzak;->zzF(I)Lcom/google/android/gms/internal/ads/zzak;

    move/from16 v10, v48

    .line 143
    invoke-virtual {v1, v10}, Lcom/google/android/gms/internal/ads/zzak;->zzP(F)Lcom/google/android/gms/internal/ads/zzak;

    move/from16 v4, v44

    .line 144
    invoke-virtual {v1, v4}, Lcom/google/android/gms/internal/ads/zzak;->zzR(I)Lcom/google/android/gms/internal/ads/zzak;

    move-object/from16 v5, v35

    .line 145
    invoke-virtual {v1, v5}, Lcom/google/android/gms/internal/ads/zzak;->zzQ([B)Lcom/google/android/gms/internal/ads/zzak;

    move/from16 v5, v26

    .line 146
    invoke-virtual {v1, v5}, Lcom/google/android/gms/internal/ads/zzak;->zzV(I)Lcom/google/android/gms/internal/ads/zzak;

    move-object/from16 v5, v25

    .line 147
    invoke-virtual {v1, v5}, Lcom/google/android/gms/internal/ads/zzak;->zzI(Ljava/util/List;)Lcom/google/android/gms/internal/ads/zzak;

    move-object/from16 v8, v24

    .line 148
    invoke-virtual {v1, v8}, Lcom/google/android/gms/internal/ads/zzak;->zzB(Lcom/google/android/gms/internal/ads/zzad;)Lcom/google/android/gms/internal/ads/zzak;

    move/from16 v5, v46

    if-ne v13, v0, :cond_53

    if-ne v5, v0, :cond_52

    if-ne v7, v0, :cond_51

    if-eqz v17, :cond_55

    const/4 v5, -0x1

    const/4 v7, -0x1

    goto :goto_33

    :cond_51
    const/4 v5, -0x1

    :cond_52
    :goto_33
    const/4 v8, -0x1

    goto :goto_34

    :cond_53
    move v8, v13

    .line 149
    :goto_34
    new-instance v6, Lcom/google/android/gms/internal/ads/zzs;

    if-eqz v17, :cond_54

    .line 150
    invoke-virtual/range {v17 .. v17}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v9

    goto :goto_35

    :cond_54
    const/4 v9, 0x0

    :goto_35
    invoke-direct {v6, v8, v5, v7, v9}, Lcom/google/android/gms/internal/ads/zzs;-><init>(III[B)V

    .line 151
    invoke-virtual {v1, v6}, Lcom/google/android/gms/internal/ads/zzak;->zzy(Lcom/google/android/gms/internal/ads/zzs;)Lcom/google/android/gms/internal/ads/zzak;

    :cond_55
    if-eqz v19, :cond_56

    invoke-static/range {v19 .. v19}, Lcom/google/android/gms/internal/ads/zzagl;->zza(Lcom/google/android/gms/internal/ads/zzagl;)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/google/android/gms/internal/ads/zzfwl;->zzc(J)I

    move-result v5

    .line 152
    invoke-virtual {v1, v5}, Lcom/google/android/gms/internal/ads/zzak;->zzv(I)Lcom/google/android/gms/internal/ads/zzak;

    invoke-static/range {v19 .. v19}, Lcom/google/android/gms/internal/ads/zzagl;->zzb(Lcom/google/android/gms/internal/ads/zzagl;)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/google/android/gms/internal/ads/zzfwl;->zzc(J)I

    move-result v5

    .line 153
    invoke-virtual {v1, v5}, Lcom/google/android/gms/internal/ads/zzak;->zzO(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 154
    :cond_56
    invoke-virtual {v1}, Lcom/google/android/gms/internal/ads/zzak;->zzY()Lcom/google/android/gms/internal/ads/zzam;

    move-result-object v1

    move-object/from16 v5, v47

    iput-object v1, v5, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    :goto_36
    add-int v1, v30, v36

    .line 155
    invoke-virtual {v14, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    add-int/lit8 v13, v20, 0x1

    move-object/from16 v1, p1

    move-object/from16 v12, p4

    move-object v6, v2

    move-object v15, v5

    move-object v5, v14

    move-object/from16 v14, v16

    move/from16 v10, v18

    move-wide/from16 v8, v21

    move/from16 v0, v23

    move-object/from16 v2, v38

    move-object/from16 v7, v42

    move-object/from16 v11, v45

    move/from16 v56, v4

    move v4, v3

    move/from16 v3, v56

    goto/16 :goto_d

    :cond_57
    move-object v2, v6

    move-object/from16 v42, v7

    move-wide/from16 v21, v8

    move/from16 v18, v10

    move-object/from16 v45, v11

    move-object/from16 v16, v14

    move-object v5, v15

    const/4 v0, -0x1

    const v1, 0x65647473

    move-object/from16 v3, v45

    .line 156
    invoke-virtual {v3, v1}, Lcom/google/android/gms/internal/ads/zzagh;->zza(I)Lcom/google/android/gms/internal/ads/zzagh;

    move-result-object v1

    if-eqz v1, :cond_58

    .line 157
    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzagr;->zzi(Lcom/google/android/gms/internal/ads/zzagh;)Landroid/util/Pair;

    move-result-object v1

    if-eqz v1, :cond_58

    .line 158
    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, [J

    .line 159
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, [J

    move-object/from16 v30, v1

    goto :goto_37

    :cond_58
    const/4 v4, 0x0

    const/16 v30, 0x0

    :goto_37
    iget-object v1, v5, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    if-nez v1, :cond_59

    move-object/from16 v1, p7

    goto/16 :goto_1

    :cond_59
    new-instance v1, Lcom/google/android/gms/internal/ads/zzahg;

    invoke-static/range {v16 .. v16}, Lcom/google/android/gms/internal/ads/zzagq;->zza(Lcom/google/android/gms/internal/ads/zzagq;)I

    move-result v17

    move-object/from16 v6, v42

    .line 160
    iget-object v6, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Long;

    .line 161
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    iget-object v6, v5, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    iget v7, v5, Lcom/google/android/gms/internal/ads/zzagn;->zzd:I

    iget-object v8, v5, Lcom/google/android/gms/internal/ads/zzagn;->zza:[Lcom/google/android/gms/internal/ads/zzahh;

    iget v5, v5, Lcom/google/android/gms/internal/ads/zzagn;->zzc:I

    move-object/from16 v16, v1

    move-wide/from16 v23, v28

    move-object/from16 v25, v6

    move/from16 v26, v7

    move-object/from16 v27, v8

    move/from16 v28, v5

    move-object/from16 v29, v4

    invoke-direct/range {v16 .. v30}, Lcom/google/android/gms/internal/ads/zzahg;-><init>(IIJJJLcom/google/android/gms/internal/ads/zzam;I[Lcom/google/android/gms/internal/ads/zzahh;I[J[J)V

    move-object v5, v1

    move-object/from16 v1, p7

    .line 162
    :goto_38
    invoke-interface {v1, v5}, Lcom/google/android/gms/internal/ads/zzfqw;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Lcom/google/android/gms/internal/ads/zzahg;

    if-eqz v6, :cond_95

    const v4, 0x6d646961

    .line 163
    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/ads/zzagh;->zza(I)Lcom/google/android/gms/internal/ads/zzagh;

    move-result-object v3

    .line 164
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x6d696e66

    .line 165
    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/ads/zzagh;->zza(I)Lcom/google/android/gms/internal/ads/zzagh;

    move-result-object v3

    .line 166
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7374626c

    .line 167
    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/ads/zzagh;->zza(I)Lcom/google/android/gms/internal/ads/zzagh;

    move-result-object v3

    .line 168
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v4, 0x7374737a

    .line 169
    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v4

    if-eqz v4, :cond_5a

    new-instance v5, Lcom/google/android/gms/internal/ads/zzago;

    iget-object v7, v6, Lcom/google/android/gms/internal/ads/zzahg;->zzf:Lcom/google/android/gms/internal/ads/zzam;

    .line 170
    invoke-direct {v5, v4, v7}, Lcom/google/android/gms/internal/ads/zzago;-><init>(Lcom/google/android/gms/internal/ads/zzagi;Lcom/google/android/gms/internal/ads/zzam;)V

    goto :goto_39

    :cond_5a
    const v4, 0x73747a32

    .line 171
    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v4

    if-eqz v4, :cond_94

    .line 172
    new-instance v5, Lcom/google/android/gms/internal/ads/zzagp;

    .line 173
    invoke-direct {v5, v4}, Lcom/google/android/gms/internal/ads/zzagp;-><init>(Lcom/google/android/gms/internal/ads/zzagi;)V

    .line 174
    :goto_39
    invoke-interface {v5}, Lcom/google/android/gms/internal/ads/zzagm;->zzb()I

    move-result v4

    if-nez v4, :cond_5b

    new-instance v0, Lcom/google/android/gms/internal/ads/zzahj;

    const/4 v2, 0x0

    new-array v7, v2, [J

    new-array v8, v2, [I

    const/4 v9, 0x0

    new-array v10, v2, [J

    new-array v11, v2, [I

    const-wide/16 v12, 0x0

    move-object v5, v0

    .line 175
    invoke-direct/range {v5 .. v13}, Lcom/google/android/gms/internal/ads/zzahj;-><init>(Lcom/google/android/gms/internal/ads/zzahg;[J[II[J[IJ)V

    :goto_3a
    move-object/from16 v1, v34

    goto/16 :goto_65

    :cond_5b
    const v7, 0x7374636f

    .line 176
    invoke-virtual {v3, v7}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v7

    if-nez v7, :cond_5c

    const v7, 0x636f3634

    .line 177
    invoke-virtual {v3, v7}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v7

    .line 178
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v8, v7

    const/4 v7, 0x1

    goto :goto_3b

    :cond_5c
    move-object v8, v7

    const/4 v7, 0x0

    .line 179
    :goto_3b
    iget-object v8, v8, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    const v9, 0x73747363

    .line 180
    invoke-virtual {v3, v9}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v9

    .line 181
    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 182
    iget-object v9, v9, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    const v10, 0x73747473

    .line 183
    invoke-virtual {v3, v10}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v10

    .line 184
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 185
    iget-object v10, v10, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    const v11, 0x73747373

    .line 186
    invoke-virtual {v3, v11}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v11

    if-eqz v11, :cond_5d

    iget-object v11, v11, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    goto :goto_3c

    :cond_5d
    const/4 v11, 0x0

    :goto_3c
    const v12, 0x63747473

    .line 187
    invoke-virtual {v3, v12}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    move-result-object v3

    if-eqz v3, :cond_5e

    iget-object v3, v3, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    goto :goto_3d

    :cond_5e
    const/4 v3, 0x0

    :goto_3d
    new-instance v12, Lcom/google/android/gms/internal/ads/zzagk;

    .line 188
    invoke-direct {v12, v9, v8, v7}, Lcom/google/android/gms/internal/ads/zzagk;-><init>(Lcom/google/android/gms/internal/ads/zzfb;Lcom/google/android/gms/internal/ads/zzfb;Z)V

    const/16 v7, 0xc

    .line 189
    invoke-virtual {v10, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 190
    invoke-virtual {v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v8

    add-int/2addr v8, v0

    .line 191
    invoke-virtual {v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v9

    .line 192
    invoke-virtual {v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v13

    if-eqz v3, :cond_5f

    .line 193
    invoke-virtual {v3, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 194
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v14

    goto :goto_3e

    :cond_5f
    const/4 v14, 0x0

    :goto_3e
    if-eqz v11, :cond_61

    .line 195
    invoke-virtual {v11, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 196
    invoke-virtual {v11}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v7

    if-lez v7, :cond_60

    .line 197
    invoke-virtual {v11}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v15

    add-int/2addr v15, v0

    goto :goto_40

    :cond_60
    const/4 v11, 0x0

    goto :goto_3f

    :cond_61
    const/4 v7, 0x0

    :goto_3f
    const/4 v15, -0x1

    :goto_40
    invoke-interface {v5}, Lcom/google/android/gms/internal/ads/zzagm;->zza()I

    move-result v0

    iget-object v1, v6, Lcom/google/android/gms/internal/ads/zzahg;->zzf:Lcom/google/android/gms/internal/ads/zzam;

    .line 198
    iget-object v1, v1, Lcom/google/android/gms/internal/ads/zzam;->zzm:Ljava/lang/String;

    move/from16 v16, v9

    const/4 v9, -0x1

    if-eq v0, v9, :cond_68

    const-string v9, "audio/raw"

    .line 199
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_62

    const-string v9, "audio/g711-mlaw"

    .line 200
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_62

    const-string v9, "audio/g711-alaw"

    .line 201
    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_68

    :cond_62
    if-nez v8, :cond_68

    if-nez v14, :cond_67

    if-nez v7, :cond_67

    iget v1, v12, Lcom/google/android/gms/internal/ads/zzagk;->zza:I

    new-array v2, v1, [J

    new-array v3, v1, [I

    .line 202
    :goto_41
    invoke-virtual {v12}, Lcom/google/android/gms/internal/ads/zzagk;->zza()Z

    move-result v5

    if-eqz v5, :cond_63

    iget v5, v12, Lcom/google/android/gms/internal/ads/zzagk;->zzb:I

    iget-wide v7, v12, Lcom/google/android/gms/internal/ads/zzagk;->zzd:J

    .line 203
    aput-wide v7, v2, v5

    iget v7, v12, Lcom/google/android/gms/internal/ads/zzagk;->zzc:I

    .line 204
    aput v7, v3, v5

    goto :goto_41

    :cond_63
    int-to-long v7, v13

    const/16 v5, 0x2000

    .line 205
    div-int/2addr v5, v0

    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_42
    if-ge v9, v1, :cond_64

    .line 206
    aget v11, v3, v9

    .line 207
    sget v12, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    add-int/2addr v11, v5

    const/4 v12, -0x1

    add-int/2addr v11, v12

    .line 208
    div-int/2addr v11, v5

    add-int/2addr v10, v11

    add-int/lit8 v9, v9, 0x1

    goto :goto_42

    .line 209
    :cond_64
    new-array v9, v10, [J

    .line 210
    new-array v11, v10, [I

    .line 211
    new-array v12, v10, [J

    .line 212
    new-array v10, v10, [I

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    :goto_43
    if-ge v13, v1, :cond_66

    .line 213
    aget v17, v3, v13

    .line 214
    aget-wide v18, v2, v13

    move/from16 v56, v17

    move/from16 v17, v1

    move/from16 v1, v56

    :goto_44
    if-lez v1, :cond_65

    .line 215
    invoke-static {v5, v1}, Ljava/lang/Math;->min(II)I

    move-result v20

    .line 216
    aput-wide v18, v9, v16

    move-object/from16 v21, v2

    mul-int v2, v0, v20

    .line 217
    aput v2, v11, v16

    .line 218
    invoke-static {v15, v2}, Ljava/lang/Math;->max(II)I

    move-result v15

    move-object/from16 v22, v3

    int-to-long v2, v14

    mul-long v2, v2, v7

    .line 219
    aput-wide v2, v12, v16

    const/4 v2, 0x1

    .line 220
    aput v2, v10, v16

    .line 221
    aget v2, v11, v16

    int-to-long v2, v2

    add-long v18, v18, v2

    add-int v14, v14, v20

    sub-int v1, v1, v20

    add-int/lit8 v16, v16, 0x1

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    goto :goto_44

    :cond_65
    move-object/from16 v21, v2

    move-object/from16 v22, v3

    add-int/lit8 v13, v13, 0x1

    move/from16 v1, v17

    goto :goto_43

    :cond_66
    int-to-long v0, v14

    mul-long v7, v7, v0

    move-wide v0, v7

    move-object v2, v9

    move-object v14, v10

    move-object v3, v11

    move v13, v15

    move-object v15, v6

    goto/16 :goto_55

    :cond_67
    const/4 v8, 0x0

    .line 222
    :cond_68
    new-array v0, v4, [J

    new-array v1, v4, [I

    new-array v9, v4, [J

    move/from16 v17, v7

    new-array v7, v4, [I

    move-object/from16 v20, v6

    move v6, v13

    move/from16 v21, v14

    move v14, v15

    const/4 v13, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v22, 0x0

    const-wide/16 v23, 0x0

    const-wide/16 v25, 0x0

    move v15, v8

    const/4 v8, 0x0

    :goto_45
    if-ge v8, v4, :cond_74

    move-wide/from16 v27, v23

    const/16 v23, 0x1

    :goto_46
    if-nez v18, :cond_6a

    .line 223
    invoke-virtual {v12}, Lcom/google/android/gms/internal/ads/zzagk;->zza()Z

    move-result v23

    if-eqz v23, :cond_69

    move/from16 v29, v14

    move/from16 v24, v15

    iget-wide v14, v12, Lcom/google/android/gms/internal/ads/zzagk;->zzd:J

    move/from16 v30, v4

    iget v4, v12, Lcom/google/android/gms/internal/ads/zzagk;->zzc:I

    move/from16 v18, v4

    move-wide/from16 v27, v14

    move/from16 v15, v24

    move/from16 v14, v29

    move/from16 v4, v30

    goto :goto_46

    :cond_69
    move/from16 v30, v4

    move/from16 v29, v14

    move/from16 v24, v15

    const/4 v4, 0x0

    goto :goto_47

    :cond_6a
    move/from16 v30, v4

    move/from16 v29, v14

    move/from16 v24, v15

    move/from16 v4, v18

    :goto_47
    if-nez v23, :cond_6b

    const-string v4, "Unexpected end of chunk data"

    .line 224
    invoke-static {v2, v4}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    invoke-static {v0, v8}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v0

    .line 226
    invoke-static {v1, v8}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    .line 227
    invoke-static {v9, v8}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v9

    .line 228
    invoke-static {v7, v8}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v7

    move v4, v8

    goto/16 :goto_4e

    :cond_6b
    move/from16 v14, v22

    if-nez v3, :cond_6c

    goto :goto_4a

    :cond_6c
    :goto_48
    if-nez v19, :cond_6e

    if-lez v21, :cond_6d

    add-int/lit8 v21, v21, -0x1

    .line 229
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v19

    .line 230
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v14

    goto :goto_48

    :cond_6d
    const/4 v15, -0x1

    const/16 v19, 0x0

    goto :goto_49

    :cond_6e
    const/4 v15, -0x1

    :goto_49
    add-int/lit8 v19, v19, -0x1

    .line 231
    :goto_4a
    aput-wide v27, v0, v8

    .line 232
    invoke-interface {v5}, Lcom/google/android/gms/internal/ads/zzagm;->zzc()I

    move-result v15

    aput v15, v1, v8

    if-le v15, v13, :cond_6f

    move/from16 v18, v15

    move-object v15, v12

    goto :goto_4b

    :cond_6f
    move-object v15, v12

    move/from16 v18, v13

    :goto_4b
    int-to-long v12, v14

    add-long v12, v25, v12

    .line 233
    aput-wide v12, v9, v8

    if-nez v11, :cond_70

    const/4 v12, 0x1

    goto :goto_4c

    :cond_70
    const/4 v12, 0x0

    .line 234
    :goto_4c
    aput v12, v7, v8

    move/from16 v12, v29

    if-ne v8, v12, :cond_71

    const/4 v13, 0x1

    .line 235
    aput v13, v7, v8

    add-int/lit8 v17, v17, -0x1

    if-lez v17, :cond_71

    .line 236
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 237
    invoke-virtual {v11}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v12

    const/4 v13, -0x1

    add-int/2addr v12, v13

    :cond_71
    move-object/from16 v23, v11

    move v13, v12

    int-to-long v11, v6

    add-long v25, v25, v11

    add-int/lit8 v11, v16, -0x1

    if-nez v11, :cond_73

    if-lez v24, :cond_72

    .line 238
    invoke-virtual {v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v6

    .line 239
    invoke-virtual {v10}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v11

    add-int/lit8 v12, v24, -0x1

    move/from16 v16, v6

    move v6, v11

    move/from16 v24, v12

    goto :goto_4d

    :cond_72
    const/16 v16, 0x0

    goto :goto_4d

    :cond_73
    move/from16 v16, v11

    .line 240
    :goto_4d
    aget v11, v1, v8

    int-to-long v11, v11

    add-long v11, v27, v11

    const/16 v27, -0x1

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v8, v8, 0x1

    move/from16 v22, v14

    move v14, v13

    move/from16 v13, v18

    move/from16 v18, v4

    move/from16 v4, v30

    move-wide/from16 v56, v11

    move-object v12, v15

    move-object/from16 v11, v23

    move/from16 v15, v24

    move-wide/from16 v23, v56

    goto/16 :goto_45

    :cond_74
    move/from16 v30, v4

    move/from16 v24, v15

    :goto_4e
    move/from16 v14, v22

    int-to-long v5, v14

    add-long v5, v25, v5

    if-eqz v3, :cond_76

    :goto_4f
    if-lez v21, :cond_76

    .line 241
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v8

    if-eqz v8, :cond_75

    const/4 v3, 0x0

    goto :goto_50

    .line 242
    :cond_75
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    add-int/lit8 v21, v21, -0x1

    goto :goto_4f

    :cond_76
    const/4 v3, 0x1

    :goto_50
    if-nez v17, :cond_7c

    if-nez v16, :cond_7b

    if-nez v18, :cond_7a

    if-nez v24, :cond_79

    if-nez v19, :cond_78

    if-nez v3, :cond_77

    move-object/from16 v16, v0

    move-object/from16 v15, v20

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    goto :goto_52

    :cond_77
    move-object/from16 v16, v0

    move-object/from16 v17, v1

    move/from16 v18, v4

    move-object/from16 v15, v20

    goto/16 :goto_54

    :cond_78
    move-object/from16 v16, v0

    move v14, v3

    move/from16 v12, v19

    move-object/from16 v15, v20

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    goto :goto_52

    :cond_79
    move-object/from16 v16, v0

    move v14, v3

    move/from16 v12, v19

    move-object/from16 v15, v20

    move/from16 v11, v24

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    goto :goto_52

    :cond_7a
    move-object/from16 v16, v0

    move v14, v3

    move/from16 v10, v18

    move/from16 v12, v19

    move-object/from16 v15, v20

    move/from16 v11, v24

    const/4 v3, 0x0

    const/4 v8, 0x0

    goto :goto_52

    :cond_7b
    move v14, v3

    move/from16 v8, v16

    move/from16 v10, v18

    move/from16 v12, v19

    move-object/from16 v15, v20

    move/from16 v11, v24

    const/4 v3, 0x0

    goto :goto_51

    :cond_7c
    move v14, v3

    move/from16 v8, v16

    move/from16 v3, v17

    move/from16 v10, v18

    move/from16 v12, v19

    move-object/from16 v15, v20

    move/from16 v11, v24

    :goto_51
    move-object/from16 v16, v0

    .line 243
    :goto_52
    iget v0, v15, Lcom/google/android/gms/internal/ads/zzahg;->zza:I

    move-object/from16 v17, v1

    new-instance v1, Ljava/lang/StringBuilder;

    .line 244
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v18, v4

    const-string v4, "Inconsistent stbl box for track "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ": remainingSynchronizationSamples "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", remainingSamplesAtTimestampDelta "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", remainingSamplesInChunk "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", remainingTimestampDeltaChanges "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", remainingSamplesAtTimestampOffset "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    if-eq v0, v14, :cond_7d

    const-string v0, ", ctts invalid"

    goto :goto_53

    :cond_7d
    const-string v0, ""

    :goto_53
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 245
    invoke-static {v2, v0}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    :goto_54
    move-wide v0, v5

    move-object v14, v7

    move-object v12, v9

    move-object/from16 v2, v16

    move-object/from16 v3, v17

    move/from16 v4, v18

    :goto_55
    const-wide/32 v7, 0xf4240

    .line 246
    iget-wide v9, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzc:J

    move-wide v5, v0

    .line 247
    invoke-static/range {v5 .. v10}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v16

    iget-object v5, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzh:[J

    if-nez v5, :cond_7e

    const-wide/32 v0, 0xf4240

    iget-wide v4, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzc:J

    .line 248
    invoke-static {v12, v0, v1, v4, v5}, Lcom/google/android/gms/internal/ads/zzfk;->zzC([JJJ)V

    new-instance v0, Lcom/google/android/gms/internal/ads/zzahj;

    move-object v5, v0

    move-object v6, v15

    move-object v7, v2

    move-object v8, v3

    move v9, v13

    move-object v10, v12

    move-object v11, v14

    move-wide/from16 v12, v16

    .line 249
    invoke-direct/range {v5 .. v13}, Lcom/google/android/gms/internal/ads/zzahj;-><init>(Lcom/google/android/gms/internal/ads/zzahg;[J[II[J[IJ)V

    goto/16 :goto_3a

    :cond_7e
    array-length v6, v5

    const/4 v7, 0x1

    if-ne v6, v7, :cond_80

    iget v6, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzb:I

    if-ne v6, v7, :cond_80

    .line 250
    array-length v6, v12

    const/4 v7, 0x2

    if-lt v6, v7, :cond_80

    iget-object v6, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzi:[J

    .line 251
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x0

    .line 252
    aget-wide v16, v6, v7

    .line 253
    aget-wide v18, v5, v7

    iget-wide v5, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzc:J

    iget-wide v7, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzd:J

    move-wide/from16 v20, v5

    move-wide/from16 v22, v7

    .line 254
    invoke-static/range {v18 .. v23}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v5

    add-long v18, v16, v5

    move-object v5, v12

    move-wide v6, v0

    move-wide/from16 v8, v16

    move-wide/from16 v10, v18

    .line 255
    invoke-static/range {v5 .. v11}, Lcom/google/android/gms/internal/ads/zzagr;->zzq([JJJJ)Z

    move-result v5

    if-eqz v5, :cond_80

    sub-long v6, v0, v18

    const/4 v5, 0x0

    .line 256
    aget-wide v8, v12, v5

    sub-long v18, v16, v8

    iget-object v5, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzf:Lcom/google/android/gms/internal/ads/zzam;

    iget v5, v5, Lcom/google/android/gms/internal/ads/zzam;->zzA:I

    int-to-long v8, v5

    iget-wide v10, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzc:J

    move-wide/from16 v20, v8

    move-wide/from16 v22, v10

    .line 257
    invoke-static/range {v18 .. v23}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v16

    iget-object v5, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzf:Lcom/google/android/gms/internal/ads/zzam;

    .line 258
    iget v5, v5, Lcom/google/android/gms/internal/ads/zzam;->zzA:I

    int-to-long v8, v5

    iget-wide v10, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzc:J

    .line 259
    invoke-static/range {v6 .. v11}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v9, v16, v7

    if-nez v9, :cond_7f

    cmp-long v9, v5, v7

    if-eqz v9, :cond_80

    const-wide/16 v7, 0x0

    goto :goto_56

    :cond_7f
    move-wide/from16 v7, v16

    :goto_56
    const-wide/32 v9, 0x7fffffff

    cmp-long v11, v7, v9

    if-gtz v11, :cond_80

    const-wide/32 v9, 0x7fffffff

    cmp-long v11, v5, v9

    if-gtz v11, :cond_80

    long-to-int v0, v7

    move-object/from16 v1, p1

    iput v0, v1, Lcom/google/android/gms/internal/ads/zzabq;->zza:I

    long-to-int v0, v5

    iput v0, v1, Lcom/google/android/gms/internal/ads/zzabq;->zzb:I

    const-wide/32 v4, 0xf4240

    iget-wide v6, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzc:J

    .line 260
    invoke-static {v12, v4, v5, v6, v7}, Lcom/google/android/gms/internal/ads/zzfk;->zzC([JJJ)V

    iget-object v0, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzh:[J

    const/4 v4, 0x0

    .line 261
    aget-wide v5, v0, v4

    const-wide/32 v7, 0xf4240

    iget-wide v9, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzd:J

    .line 262
    invoke-static/range {v5 .. v10}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v16

    new-instance v0, Lcom/google/android/gms/internal/ads/zzahj;

    move-object v5, v0

    move-object v6, v15

    move-object v7, v2

    move-object v8, v3

    move v9, v13

    move-object v10, v12

    move-object v11, v14

    move-wide/from16 v12, v16

    .line 263
    invoke-direct/range {v5 .. v13}, Lcom/google/android/gms/internal/ads/zzahj;-><init>(Lcom/google/android/gms/internal/ads/zzahg;[J[II[J[IJ)V

    goto/16 :goto_3a

    :cond_80
    iget-object v5, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzh:[J

    .line 264
    array-length v7, v5

    const/4 v6, 0x1

    if-ne v7, v6, :cond_83

    const/4 v6, 0x0

    aget-wide v7, v5, v6

    const-wide/16 v9, 0x0

    cmp-long v5, v7, v9

    if-nez v5, :cond_82

    iget-object v4, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzi:[J

    .line 265
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 266
    aget-wide v7, v4, v6

    const/4 v4, 0x0

    .line 267
    :goto_57
    array-length v5, v12

    if-ge v4, v5, :cond_81

    .line 268
    aget-wide v5, v12, v4

    sub-long v16, v5, v7

    const-wide/32 v18, 0xf4240

    iget-wide v5, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzc:J

    move-wide/from16 v20, v5

    .line 269
    invoke-static/range {v16 .. v21}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v5

    aput-wide v5, v12, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_57

    :cond_81
    sub-long v16, v0, v7

    const-wide/32 v18, 0xf4240

    iget-wide v0, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzc:J

    move-wide/from16 v20, v0

    .line 270
    invoke-static/range {v16 .. v21}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v0

    new-instance v4, Lcom/google/android/gms/internal/ads/zzahj;

    move-object v5, v4

    move-object v6, v15

    move-object v7, v2

    move-object v8, v3

    move v9, v13

    move-object v10, v12

    move-object v11, v14

    move-wide v12, v0

    .line 271
    invoke-direct/range {v5 .. v13}, Lcom/google/android/gms/internal/ads/zzahj;-><init>(Lcom/google/android/gms/internal/ads/zzahg;[J[II[J[IJ)V

    move-object v0, v4

    goto/16 :goto_3a

    :cond_82
    const/4 v7, 0x1

    :cond_83
    iget v0, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzb:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_84

    const/4 v0, 0x1

    goto :goto_58

    :cond_84
    const/4 v0, 0x0

    :goto_58
    new-array v1, v7, [I

    new-array v5, v7, [I

    iget-object v6, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzi:[J

    .line 272
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 273
    :goto_59
    iget-object v11, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzh:[J

    move/from16 v16, v13

    .line 274
    array-length v13, v11

    if-ge v7, v13, :cond_88

    move-object v13, v2

    move-object/from16 v17, v3

    .line 275
    aget-wide v2, v6, v7

    const-wide/16 v18, -0x1

    cmp-long v20, v2, v18

    if-eqz v20, :cond_87

    .line 276
    aget-wide v21, v11, v7

    move v11, v9

    move/from16 v18, v10

    iget-wide v9, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzc:J

    move-object/from16 v20, v13

    move-object/from16 v19, v14

    iget-wide v13, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzd:J

    move-wide/from16 v23, v9

    move-wide/from16 v25, v13

    .line 277
    invoke-static/range {v21 .. v26}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v9

    const/4 v13, 0x1

    .line 278
    invoke-static {v12, v2, v3, v13, v13}, Lcom/google/android/gms/internal/ads/zzfk;->zzc([JJZZ)I

    move-result v14

    aput v14, v1, v7

    add-long/2addr v2, v9

    const/4 v14, 0x0

    .line 279
    invoke-static {v12, v2, v3, v0, v14}, Lcom/google/android/gms/internal/ads/zzfk;->zza([JJZZ)I

    move-result v2

    aput v2, v5, v7

    .line 280
    :goto_5a
    aget v2, v1, v7

    aget v3, v5, v7

    if-ge v2, v3, :cond_85

    aget v9, v19, v2

    and-int/2addr v9, v13

    if-nez v9, :cond_85

    add-int/lit8 v2, v2, 0x1

    .line 281
    aput v2, v1, v7

    const/4 v13, 0x1

    goto :goto_5a

    :cond_85
    sub-int v9, v3, v2

    add-int/2addr v8, v9

    move/from16 v9, v18

    if-eq v9, v2, :cond_86

    const/4 v2, 0x1

    goto :goto_5b

    :cond_86
    const/4 v2, 0x0

    :goto_5b
    or-int/2addr v2, v11

    move v9, v2

    move v10, v3

    goto :goto_5c

    :cond_87
    move v11, v9

    move v9, v10

    move-object/from16 v20, v13

    move-object/from16 v19, v14

    const/4 v14, 0x0

    move v9, v11

    :goto_5c
    add-int/lit8 v7, v7, 0x1

    move/from16 v13, v16

    move-object/from16 v3, v17

    move-object/from16 v14, v19

    move-object/from16 v2, v20

    goto :goto_59

    :cond_88
    move-object/from16 v20, v2

    move-object/from16 v17, v3

    move v11, v9

    move-object/from16 v19, v14

    const/4 v14, 0x0

    if-eq v8, v4, :cond_89

    const/4 v7, 0x1

    goto :goto_5d

    :cond_89
    const/4 v7, 0x0

    :goto_5d
    or-int v0, v11, v7

    if-eqz v0, :cond_8a

    .line 282
    new-array v2, v8, [J

    move-object v7, v2

    goto :goto_5e

    :cond_8a
    move-object/from16 v7, v20

    :goto_5e
    if-eqz v0, :cond_8b

    .line 283
    new-array v2, v8, [I

    goto :goto_5f

    :cond_8b
    move-object/from16 v2, v17

    :goto_5f
    const/4 v3, 0x1

    if-ne v3, v0, :cond_8c

    const/16 v16, 0x0

    :cond_8c
    if-eqz v0, :cond_8d

    .line 284
    new-array v3, v8, [I

    move-object v11, v3

    goto :goto_60

    :cond_8d
    move-object/from16 v11, v19

    .line 285
    :goto_60
    new-array v10, v8, [J

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-wide/16 v8, 0x0

    :goto_61
    iget-object v6, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzh:[J

    .line 286
    array-length v6, v6

    if-ge v3, v6, :cond_93

    iget-object v6, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzi:[J

    .line 287
    aget-wide v27, v6, v3

    .line 288
    aget v6, v1, v3

    .line 289
    aget v13, v5, v3

    if-eqz v0, :cond_8e

    sub-int v14, v13, v6

    move-object/from16 v18, v1

    move-object/from16 v1, v20

    .line 290
    invoke-static {v1, v6, v7, v4, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v1, v17

    .line 291
    invoke-static {v1, v6, v2, v4, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v17, v5

    move-object/from16 v5, v19

    .line 292
    invoke-static {v5, v6, v11, v4, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_62

    :cond_8e
    move-object/from16 v18, v1

    move-object/from16 v1, v17

    move-object/from16 v17, v5

    move-object/from16 v5, v19

    :goto_62
    move/from16 v14, v16

    :goto_63
    if-ge v6, v13, :cond_92

    const-wide/32 v23, 0xf4240

    move/from16 v19, v13

    move/from16 v16, v14

    iget-wide v13, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzd:J

    move-wide/from16 v21, v8

    move-wide/from16 v25, v13

    .line 293
    invoke-static/range {v21 .. v26}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v13

    .line 294
    aget-wide v21, v12, v6

    sub-long v35, v21, v27

    const-wide/32 v37, 0xf4240

    move-object/from16 v29, v11

    move-object/from16 v21, v12

    iget-wide v11, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzc:J

    move-wide/from16 v39, v11

    .line 295
    invoke-static/range {v35 .. v40}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v11

    move-object/from16 v22, v5

    iget v5, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzb:I

    invoke-static {v5}, Lcom/google/android/gms/internal/ads/zzagr;->zzr(I)Z

    move-result v5

    move-object/from16 v30, v7

    move-wide/from16 v23, v8

    const-wide/16 v7, 0x0

    if-eqz v5, :cond_8f

    .line 296
    invoke-static {v7, v8, v11, v12}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v11

    :cond_8f
    add-long/2addr v13, v11

    .line 297
    aput-wide v13, v10, v4

    if-eqz v0, :cond_90

    .line 298
    aget v5, v2, v4

    move/from16 v9, v16

    if-le v5, v9, :cond_91

    .line 299
    aget v5, v1, v6

    move v14, v5

    goto :goto_64

    :cond_90
    move/from16 v9, v16

    :cond_91
    move v14, v9

    :goto_64
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v6, v6, 0x1

    move/from16 v13, v19

    move-object/from16 v12, v21

    move-object/from16 v5, v22

    move-wide/from16 v8, v23

    move-object/from16 v11, v29

    move-object/from16 v7, v30

    goto :goto_63

    :cond_92
    move-object/from16 v22, v5

    move-object/from16 v30, v7

    move-wide/from16 v23, v8

    move-object/from16 v29, v11

    move-object/from16 v21, v12

    move v9, v14

    const-wide/16 v7, 0x0

    iget-object v5, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzh:[J

    .line 300
    aget-wide v11, v5, v3

    add-long v5, v23, v11

    add-int/lit8 v3, v3, 0x1

    move/from16 v16, v9

    move-object/from16 v12, v21

    move-object/from16 v19, v22

    move-object/from16 v11, v29

    move-object/from16 v7, v30

    const/4 v14, 0x0

    move-wide v8, v5

    move-object/from16 v5, v17

    move-object/from16 v17, v1

    move-object/from16 v1, v18

    goto/16 :goto_61

    :cond_93
    move-object/from16 v30, v7

    move-wide/from16 v23, v8

    move-object/from16 v29, v11

    const-wide/32 v0, 0xf4240

    iget-wide v3, v15, Lcom/google/android/gms/internal/ads/zzahg;->zzd:J

    move-wide/from16 v21, v23

    move-wide/from16 v23, v0

    move-wide/from16 v25, v3

    .line 301
    invoke-static/range {v21 .. v26}, Lcom/google/android/gms/internal/ads/zzfk;->zzq(JJJ)J

    move-result-wide v12

    new-instance v0, Lcom/google/android/gms/internal/ads/zzahj;

    move-object v5, v0

    move-object v6, v15

    move-object v8, v2

    move/from16 v9, v16

    .line 302
    invoke-direct/range {v5 .. v13}, Lcom/google/android/gms/internal/ads/zzahj;-><init>(Lcom/google/android/gms/internal/ads/zzahg;[J[II[J[IJ)V

    goto/16 :goto_3a

    .line 303
    :goto_65
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_66

    :cond_94
    const-string v0, "Track has no sample table size information"

    const/4 v2, 0x0

    .line 304
    invoke-static {v0, v2}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    move-result-object v0

    throw v0

    :cond_95
    move-object/from16 v1, v34

    :goto_66
    add-int/lit8 v15, v31, 0x1

    move-object/from16 v0, p0

    move-object/from16 v12, p4

    move-object v13, v1

    move-object/from16 v1, p1

    goto/16 :goto_0

    :cond_96
    const/4 v2, 0x0

    const-string v0, "Malformed sample table (stbl) missing sample description (stsd)"

    .line 305
    invoke-static {v0, v2}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    move-result-object v0

    throw v0

    :cond_97
    move-object v1, v13

    return-object v1
.end method

.method public static zze(Lcom/google/android/gms/internal/ads/zzfb;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x4

    .line 6
    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const v2, 0x68646c72    # 4.3148E24f

    .line 14
    .line 15
    .line 16
    if-eq v1, v2, :cond_0

    .line 17
    .line 18
    add-int/lit8 v0, v0, 0x4

    .line 19
    .line 20
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static zzf(I)I
    .locals 1

    .line 1
    const v0, 0x736f756e

    .line 2
    .line 3
    .line 4
    if-ne p0, v0, :cond_0

    .line 5
    .line 6
    const/4 p0, 0x1

    .line 7
    return p0

    .line 8
    :cond_0
    const v0, 0x76696465

    .line 9
    .line 10
    .line 11
    if-ne p0, v0, :cond_1

    .line 12
    .line 13
    const/4 p0, 0x2

    .line 14
    return p0

    .line 15
    :cond_1
    const v0, 0x74657874

    .line 16
    .line 17
    .line 18
    if-eq p0, v0, :cond_4

    .line 19
    .line 20
    const v0, 0x7362746c

    .line 21
    .line 22
    .line 23
    if-eq p0, v0, :cond_4

    .line 24
    .line 25
    const v0, 0x73756274

    .line 26
    .line 27
    .line 28
    if-eq p0, v0, :cond_4

    .line 29
    .line 30
    const v0, 0x636c6370

    .line 31
    .line 32
    .line 33
    if-ne p0, v0, :cond_2

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_2
    const v0, 0x6d657461

    .line 37
    .line 38
    .line 39
    if-ne p0, v0, :cond_3

    .line 40
    .line 41
    const/4 p0, 0x5

    .line 42
    return p0

    .line 43
    :cond_3
    const/4 p0, -0x1

    .line 44
    return p0

    .line 45
    :cond_4
    :goto_0
    const/4 p0, 0x3

    .line 46
    return p0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static zzg(Lcom/google/android/gms/internal/ads/zzfb;)I
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    and-int/lit8 v1, v0, 0x7f

    .line 6
    .line 7
    :goto_0
    const/16 v2, 0x80

    .line 8
    .line 9
    and-int/2addr v0, v2

    .line 10
    if-ne v0, v2, :cond_0

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    shl-int/lit8 v1, v1, 0x7

    .line 17
    .line 18
    and-int/lit8 v2, v0, 0x7f

    .line 19
    .line 20
    or-int/2addr v1, v2

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    return v1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static zzh(Lcom/google/android/gms/internal/ads/zzfb;)I
    .locals 1

    .line 1
    const/16 v0, 0x10

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 7
    .line 8
    .line 9
    move-result p0

    .line 10
    return p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static zzi(Lcom/google/android/gms/internal/ads/zzagh;)Landroid/util/Pair;
    .locals 8
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const v0, 0x656c7374

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzagh;->zzb(I)Lcom/google/android/gms/internal/ads/zzagi;

    .line 5
    .line 6
    .line 7
    move-result-object p0

    .line 8
    if-nez p0, :cond_0

    .line 9
    .line 10
    const/4 p0, 0x0

    .line 11
    return-object p0

    .line 12
    :cond_0
    iget-object p0, p0, Lcom/google/android/gms/internal/ads/zzagi;->zza:Lcom/google/android/gms/internal/ads/zzfb;

    .line 13
    .line 14
    const/16 v0, 0x8

    .line 15
    .line 16
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzagj;->zze(I)I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    new-array v2, v1, [J

    .line 32
    .line 33
    new-array v3, v1, [J

    .line 34
    .line 35
    const/4 v4, 0x0

    .line 36
    :goto_0
    if-ge v4, v1, :cond_4

    .line 37
    .line 38
    const/4 v5, 0x1

    .line 39
    if-ne v0, v5, :cond_1

    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzu()J

    .line 42
    .line 43
    .line 44
    move-result-wide v6

    .line 45
    goto :goto_1

    .line 46
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzt()J

    .line 47
    .line 48
    .line 49
    move-result-wide v6

    .line 50
    :goto_1
    aput-wide v6, v2, v4

    .line 51
    .line 52
    if-ne v0, v5, :cond_2

    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzs()J

    .line 55
    .line 56
    .line 57
    move-result-wide v6

    .line 58
    goto :goto_2

    .line 59
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 60
    .line 61
    .line 62
    move-result v6

    .line 63
    int-to-long v6, v6

    .line 64
    :goto_2
    aput-wide v6, v3, v4

    .line 65
    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    .line 67
    .line 68
    .line 69
    move-result v6

    .line 70
    if-ne v6, v5, :cond_3

    .line 71
    .line 72
    const/4 v5, 0x2

    .line 73
    invoke-virtual {p0, v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 74
    .line 75
    .line 76
    add-int/lit8 v4, v4, 0x1

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 80
    .line 81
    const-string v0, "Unsupported media rate."

    .line 82
    .line 83
    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    throw p0

    .line 87
    :cond_4
    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 88
    .line 89
    .line 90
    move-result-object p0

    .line 91
    return-object p0
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method private static zzj(Lcom/google/android/gms/internal/ads/zzfb;)Landroid/util/Pair;
    .locals 5

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzagj;->zze(I)I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-nez v1, :cond_0

    .line 15
    .line 16
    const/16 v2, 0x8

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/16 v2, 0x10

    .line 20
    .line 21
    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzt()J

    .line 25
    .line 26
    .line 27
    move-result-wide v2

    .line 28
    if-nez v1, :cond_1

    .line 29
    .line 30
    const/4 v0, 0x4

    .line 31
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzp()I

    .line 35
    .line 36
    .line 37
    move-result p0

    .line 38
    shr-int/lit8 v0, p0, 0xa

    .line 39
    .line 40
    shr-int/lit8 v1, p0, 0x5

    .line 41
    .line 42
    and-int/lit8 p0, p0, 0x1f

    .line 43
    .line 44
    new-instance v4, Ljava/lang/StringBuilder;

    .line 45
    .line 46
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .line 48
    .line 49
    and-int/lit8 v0, v0, 0x1f

    .line 50
    .line 51
    add-int/lit8 v0, v0, 0x60

    .line 52
    .line 53
    int-to-char v0, v0

    .line 54
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    and-int/lit8 v0, v1, 0x1f

    .line 58
    .line 59
    add-int/lit8 v0, v0, 0x60

    .line 60
    .line 61
    int-to-char v0, v0

    .line 62
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    add-int/lit8 p0, p0, 0x60

    .line 66
    .line 67
    int-to-char p0, p0

    .line 68
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object p0

    .line 75
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    invoke-static {v0, p0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 80
    .line 81
    .line 82
    move-result-object p0

    .line 83
    return-object p0
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method private static zzk(Lcom/google/android/gms/internal/ads/zzfb;II)Landroid/util/Pair;
    .locals 17
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzcd;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    :goto_0
    sub-int v2, v1, p1

    .line 8
    .line 9
    move/from16 v4, p2

    .line 10
    .line 11
    if-ge v2, v4, :cond_11

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 14
    .line 15
    .line 16
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    const/4 v5, 0x1

    .line 21
    const/4 v6, 0x0

    .line 22
    if-lez v2, :cond_0

    .line 23
    .line 24
    const/4 v7, 0x1

    .line 25
    goto :goto_1

    .line 26
    :cond_0
    const/4 v7, 0x0

    .line 27
    :goto_1
    const-string v8, "childAtomSize must be positive"

    .line 28
    .line 29
    invoke-static {v7, v8}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 33
    .line 34
    .line 35
    move-result v7

    .line 36
    const v8, 0x73696e66

    .line 37
    .line 38
    .line 39
    if-ne v7, v8, :cond_10

    .line 40
    .line 41
    add-int/lit8 v7, v1, 0x8

    .line 42
    .line 43
    const/4 v8, -0x1

    .line 44
    const/4 v9, -0x1

    .line 45
    const/4 v10, 0x0

    .line 46
    const/4 v11, 0x0

    .line 47
    const/4 v15, 0x0

    .line 48
    :goto_2
    sub-int v12, v7, v1

    .line 49
    .line 50
    const/4 v13, 0x4

    .line 51
    if-ge v12, v2, :cond_4

    .line 52
    .line 53
    invoke-virtual {v0, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 54
    .line 55
    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 57
    .line 58
    .line 59
    move-result v12

    .line 60
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 61
    .line 62
    .line 63
    move-result v14

    .line 64
    const v3, 0x66726d61

    .line 65
    .line 66
    .line 67
    if-ne v14, v3, :cond_1

    .line 68
    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 74
    .line 75
    .line 76
    move-result-object v15

    .line 77
    goto :goto_3

    .line 78
    :cond_1
    const v3, 0x7363686d

    .line 79
    .line 80
    .line 81
    if-ne v14, v3, :cond_2

    .line 82
    .line 83
    invoke-virtual {v0, v13}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 84
    .line 85
    .line 86
    sget-object v3, Lcom/google/android/gms/internal/ads/zzfqu;->zzc:Ljava/nio/charset/Charset;

    .line 87
    .line 88
    invoke-virtual {v0, v13, v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzy(ILjava/nio/charset/Charset;)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v11

    .line 92
    goto :goto_3

    .line 93
    :cond_2
    const v3, 0x73636869

    .line 94
    .line 95
    .line 96
    if-ne v14, v3, :cond_3

    .line 97
    .line 98
    move v9, v7

    .line 99
    move v10, v12

    .line 100
    :cond_3
    :goto_3
    add-int/2addr v7, v12

    .line 101
    goto :goto_2

    .line 102
    :cond_4
    const-string v3, "cenc"

    .line 103
    .line 104
    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 105
    .line 106
    .line 107
    move-result v3

    .line 108
    if-nez v3, :cond_6

    .line 109
    .line 110
    const-string v3, "cbc1"

    .line 111
    .line 112
    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 113
    .line 114
    .line 115
    move-result v3

    .line 116
    if-nez v3, :cond_6

    .line 117
    .line 118
    const-string v3, "cens"

    .line 119
    .line 120
    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 121
    .line 122
    .line 123
    move-result v3

    .line 124
    if-nez v3, :cond_6

    .line 125
    .line 126
    const-string v3, "cbcs"

    .line 127
    .line 128
    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    move-result v3

    .line 132
    if-eqz v3, :cond_5

    .line 133
    .line 134
    goto :goto_4

    .line 135
    :cond_5
    const/4 v3, 0x0

    .line 136
    goto/16 :goto_d

    .line 137
    .line 138
    :cond_6
    :goto_4
    if-eqz v15, :cond_7

    .line 139
    .line 140
    const/4 v3, 0x1

    .line 141
    goto :goto_5

    .line 142
    :cond_7
    const/4 v3, 0x0

    .line 143
    :goto_5
    const-string v7, "frma atom is mandatory"

    .line 144
    .line 145
    invoke-static {v3, v7}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    .line 146
    .line 147
    .line 148
    if-eq v9, v8, :cond_8

    .line 149
    .line 150
    const/4 v3, 0x1

    .line 151
    goto :goto_6

    .line 152
    :cond_8
    const/4 v3, 0x0

    .line 153
    :goto_6
    const-string v7, "schi atom is mandatory"

    .line 154
    .line 155
    invoke-static {v3, v7}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    .line 156
    .line 157
    .line 158
    add-int/lit8 v3, v9, 0x8

    .line 159
    .line 160
    :goto_7
    sub-int v7, v3, v9

    .line 161
    .line 162
    if-ge v7, v10, :cond_d

    .line 163
    .line 164
    invoke-virtual {v0, v3}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 165
    .line 166
    .line 167
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 168
    .line 169
    .line 170
    move-result v7

    .line 171
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 172
    .line 173
    .line 174
    move-result v8

    .line 175
    const v12, 0x74656e63

    .line 176
    .line 177
    .line 178
    if-ne v8, v12, :cond_c

    .line 179
    .line 180
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 181
    .line 182
    .line 183
    move-result v3

    .line 184
    invoke-static {v3}, Lcom/google/android/gms/internal/ads/zzagj;->zze(I)I

    .line 185
    .line 186
    .line 187
    move-result v3

    .line 188
    invoke-virtual {v0, v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 189
    .line 190
    .line 191
    if-nez v3, :cond_9

    .line 192
    .line 193
    invoke-virtual {v0, v5}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 194
    .line 195
    .line 196
    const/4 v3, 0x0

    .line 197
    const/4 v14, 0x0

    .line 198
    goto :goto_8

    .line 199
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    .line 200
    .line 201
    .line 202
    move-result v3

    .line 203
    and-int/lit16 v7, v3, 0xf0

    .line 204
    .line 205
    and-int/lit8 v3, v3, 0xf

    .line 206
    .line 207
    shr-int/2addr v7, v13

    .line 208
    move v14, v7

    .line 209
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    .line 210
    .line 211
    .line 212
    move-result v7

    .line 213
    if-ne v7, v5, :cond_a

    .line 214
    .line 215
    const/4 v10, 0x1

    .line 216
    goto :goto_9

    .line 217
    :cond_a
    const/4 v10, 0x0

    .line 218
    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    .line 219
    .line 220
    .line 221
    move-result v12

    .line 222
    const/16 v7, 0x10

    .line 223
    .line 224
    new-array v13, v7, [B

    .line 225
    .line 226
    invoke-virtual {v0, v13, v6, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzC([BII)V

    .line 227
    .line 228
    .line 229
    if-eqz v10, :cond_b

    .line 230
    .line 231
    if-nez v12, :cond_b

    .line 232
    .line 233
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    .line 234
    .line 235
    .line 236
    move-result v7

    .line 237
    new-array v8, v7, [B

    .line 238
    .line 239
    invoke-virtual {v0, v8, v6, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzC([BII)V

    .line 240
    .line 241
    .line 242
    move-object/from16 v16, v8

    .line 243
    .line 244
    goto :goto_a

    .line 245
    :cond_b
    const/16 v16, 0x0

    .line 246
    .line 247
    :goto_a
    new-instance v7, Lcom/google/android/gms/internal/ads/zzahh;

    .line 248
    .line 249
    move-object v9, v7

    .line 250
    move-object v8, v15

    .line 251
    move v15, v3

    .line 252
    invoke-direct/range {v9 .. v16}, Lcom/google/android/gms/internal/ads/zzahh;-><init>(ZLjava/lang/String;I[BII[B)V

    .line 253
    .line 254
    .line 255
    move-object v3, v7

    .line 256
    goto :goto_b

    .line 257
    :cond_c
    move-object v8, v15

    .line 258
    add-int/2addr v3, v7

    .line 259
    goto :goto_7

    .line 260
    :cond_d
    move-object v8, v15

    .line 261
    const/4 v3, 0x0

    .line 262
    :goto_b
    if-eqz v3, :cond_e

    .line 263
    .line 264
    goto :goto_c

    .line 265
    :cond_e
    const/4 v5, 0x0

    .line 266
    :goto_c
    const-string v6, "tenc atom is mandatory"

    .line 267
    .line 268
    invoke-static {v5, v6}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    .line 269
    .line 270
    .line 271
    sget v5, Lcom/google/android/gms/internal/ads/zzfk;->zza:I

    .line 272
    .line 273
    invoke-static {v8, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 274
    .line 275
    .line 276
    move-result-object v3

    .line 277
    :goto_d
    if-nez v3, :cond_f

    .line 278
    .line 279
    goto :goto_e

    .line 280
    :cond_f
    return-object v3

    .line 281
    :cond_10
    :goto_e
    add-int/2addr v1, v2

    .line 282
    goto/16 :goto_0

    .line 283
    .line 284
    :cond_11
    const/4 v1, 0x0

    .line 285
    return-object v1
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
.end method

.method private static zzl(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzbz;
    .locals 5
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzz()S

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x2

    .line 6
    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 7
    .line 8
    .line 9
    sget-object v1, Lcom/google/android/gms/internal/ads/zzfqu;->zzc:Ljava/nio/charset/Charset;

    .line 10
    .line 11
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzy(ILjava/nio/charset/Charset;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    const/16 v0, 0x2b

    .line 16
    .line 17
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    const/16 v1, 0x2d

    .line 22
    .line 23
    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    const/4 v1, 0x0

    .line 32
    :try_start_0
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    add-int/lit8 v3, v3, -0x1

    .line 45
    .line 46
    invoke-virtual {p0, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p0

    .line 50
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 51
    .line 52
    .line 53
    move-result p0

    .line 54
    new-instance v0, Lcom/google/android/gms/internal/ads/zzbz;

    .line 55
    .line 56
    const/4 v3, 0x1

    .line 57
    new-array v3, v3, [Lcom/google/android/gms/internal/ads/zzby;

    .line 58
    .line 59
    new-instance v4, Lcom/google/android/gms/internal/ads/zzfr;

    .line 60
    .line 61
    invoke-direct {v4, v2, p0}, Lcom/google/android/gms/internal/ads/zzfr;-><init>(FF)V

    .line 62
    .line 63
    .line 64
    aput-object v4, v3, v1

    .line 65
    .line 66
    const-wide v1, -0x7fffffffffffffffL    # -4.9E-324

    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/internal/ads/zzbz;-><init>(J[Lcom/google/android/gms/internal/ads/zzby;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .line 73
    .line 74
    return-object v0

    .line 75
    :catch_0
    const/4 p0, 0x0

    .line 76
    return-object p0
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method private static zzm(Lcom/google/android/gms/internal/ads/zzfb;I)Lcom/google/android/gms/internal/ads/zzagl;
    .locals 10

    .line 1
    add-int/lit8 p1, p1, 0xc

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 8
    .line 9
    .line 10
    invoke-static {p0}, Lcom/google/android/gms/internal/ads/zzagr;->zzg(Lcom/google/android/gms/internal/ads/zzfb;)I

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x2

    .line 14
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    and-int/lit16 v2, v1, 0x80

    .line 22
    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 26
    .line 27
    .line 28
    :cond_0
    and-int/lit8 v2, v1, 0x40

    .line 29
    .line 30
    if-eqz v2, :cond_1

    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 37
    .line 38
    .line 39
    :cond_1
    and-int/lit8 v1, v1, 0x20

    .line 40
    .line 41
    if-eqz v1, :cond_2

    .line 42
    .line 43
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 44
    .line 45
    .line 46
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 47
    .line 48
    .line 49
    invoke-static {p0}, Lcom/google/android/gms/internal/ads/zzagr;->zzg(Lcom/google/android/gms/internal/ads/zzfb;)I

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    invoke-static {v0}, Lcom/google/android/gms/internal/ads/zzcc;->zzd(I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    const-string v0, "audio/mpeg"

    .line 61
    .line 62
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    if-nez v0, :cond_6

    .line 67
    .line 68
    const-string v0, "audio/vnd.dts"

    .line 69
    .line 70
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    if-nez v0, :cond_6

    .line 75
    .line 76
    const-string v0, "audio/vnd.dts.hd"

    .line 77
    .line 78
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    if-eqz v0, :cond_3

    .line 83
    .line 84
    goto :goto_2

    .line 85
    :cond_3
    const/4 v0, 0x4

    .line 86
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzt()J

    .line 90
    .line 91
    .line 92
    move-result-wide v0

    .line 93
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzt()J

    .line 94
    .line 95
    .line 96
    move-result-wide v3

    .line 97
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 98
    .line 99
    .line 100
    invoke-static {p0}, Lcom/google/android/gms/internal/ads/zzagr;->zzg(Lcom/google/android/gms/internal/ads/zzfb;)I

    .line 101
    .line 102
    .line 103
    move-result p1

    .line 104
    new-array v5, p1, [B

    .line 105
    .line 106
    const/4 v6, 0x0

    .line 107
    invoke-virtual {p0, v5, v6, p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzC([BII)V

    .line 108
    .line 109
    .line 110
    const-wide/16 p0, -0x1

    .line 111
    .line 112
    const-wide/16 v6, 0x0

    .line 113
    .line 114
    cmp-long v8, v3, v6

    .line 115
    .line 116
    if-gtz v8, :cond_4

    .line 117
    .line 118
    move-wide v8, p0

    .line 119
    goto :goto_0

    .line 120
    :cond_4
    move-wide v8, v3

    .line 121
    :goto_0
    cmp-long v3, v0, v6

    .line 122
    .line 123
    if-lez v3, :cond_5

    .line 124
    .line 125
    move-wide v6, v0

    .line 126
    goto :goto_1

    .line 127
    :cond_5
    move-wide v6, p0

    .line 128
    :goto_1
    new-instance p0, Lcom/google/android/gms/internal/ads/zzagl;

    .line 129
    .line 130
    move-object v1, p0

    .line 131
    move-object v3, v5

    .line 132
    move-wide v4, v8

    .line 133
    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/internal/ads/zzagl;-><init>(Ljava/lang/String;[BJJ)V

    .line 134
    .line 135
    .line 136
    return-object p0

    .line 137
    :cond_6
    :goto_2
    new-instance p0, Lcom/google/android/gms/internal/ads/zzagl;

    .line 138
    .line 139
    const/4 v3, 0x0

    .line 140
    const-wide/16 v6, -0x1

    .line 141
    .line 142
    move-object v1, p0

    .line 143
    move-wide v4, v6

    .line 144
    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/internal/ads/zzagl;-><init>(Ljava/lang/String;[BJJ)V

    .line 145
    .line 146
    .line 147
    return-object p0
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
.end method

.method private static zzn()Ljava/nio/ByteBuffer;
    .locals 2

    .line 1
    const/16 v0, 0x19

    .line 2
    .line 3
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private static zzo(Lcom/google/android/gms/internal/ads/zzfb;IIIILjava/lang/String;ZLcom/google/android/gms/internal/ads/zzad;Lcom/google/android/gms/internal/ads/zzagn;I)V
    .locals 25
    .param p7    # Lcom/google/android/gms/internal/ads/zzad;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzcd;
        }
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    add-int/lit8 v7, v1, 0x10

    .line 1
    invoke-virtual {v0, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    const/4 v7, 0x6

    const/16 v8, 0x8

    if-eqz p6, :cond_0

    .line 2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzp()I

    move-result v10

    .line 3
    invoke-virtual {v0, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {v0, v8}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    const/4 v10, 0x0

    :goto_0
    const/4 v14, 0x4

    const/4 v15, -0x1

    const/4 v11, 0x2

    const/4 v12, 0x1

    const/16 v9, 0x10

    if-eqz v10, :cond_b

    if-ne v10, v12, :cond_1

    goto :goto_2

    :cond_1
    if-ne v10, v11, :cond_a

    .line 5
    invoke-virtual {v0, v9}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzs()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v18

    .line 7
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->round(D)J

    move-result-wide v12

    long-to-int v7, v12

    .line 8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v10

    .line 9
    invoke-virtual {v0, v14}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 10
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v12

    .line 11
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v13

    and-int/lit8 v18, v13, 0x1

    and-int/2addr v13, v11

    if-nez v18, :cond_8

    if-ne v12, v8, :cond_2

    const/4 v9, 0x3

    goto :goto_1

    :cond_2
    if-ne v12, v9, :cond_4

    if-eqz v13, :cond_3

    const/high16 v9, 0x10000000

    goto :goto_1

    :cond_3
    const/4 v9, 0x2

    goto :goto_1

    :cond_4
    const/16 v9, 0x18

    if-ne v12, v9, :cond_6

    if-eqz v13, :cond_5

    const/high16 v9, 0x50000000

    goto :goto_1

    :cond_5
    const/high16 v9, 0x20000000

    goto :goto_1

    :cond_6
    const/16 v9, 0x20

    if-ne v12, v9, :cond_9

    if-eqz v13, :cond_7

    const/high16 v12, 0x60000000

    const/high16 v9, 0x60000000

    goto :goto_1

    :cond_7
    const/high16 v12, 0x30000000

    const/high16 v9, 0x30000000

    goto :goto_1

    :cond_8
    const/16 v9, 0x20

    if-ne v12, v9, :cond_9

    const/4 v9, 0x4

    goto :goto_1

    :cond_9
    const/4 v9, -0x1

    .line 12
    :goto_1
    invoke-virtual {v0, v8}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    const/4 v12, 0x0

    goto :goto_3

    :cond_a
    return-void

    .line 13
    :cond_b
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzp()I

    move-result v8

    .line 14
    invoke-virtual {v0, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 15
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzm()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    move-result v12

    add-int/lit8 v12, v12, -0x4

    .line 16
    invoke-virtual {v0, v12}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 17
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v12

    const/4 v13, 0x1

    if-ne v10, v13, :cond_c

    .line 18
    invoke-virtual {v0, v9}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    :cond_c
    move v10, v8

    const/4 v9, -0x1

    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    move-result v8

    const v13, 0x656e6361

    move/from16 v11, p1

    if-ne v11, v13, :cond_f

    .line 19
    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/ads/zzagr;->zzk(Lcom/google/android/gms/internal/ads/zzfb;II)Landroid/util/Pair;

    move-result-object v11

    if-eqz v11, :cond_e

    .line 20
    iget-object v13, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    if-nez v5, :cond_d

    const/4 v14, 0x0

    goto :goto_4

    .line 21
    :cond_d
    iget-object v14, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v14, Lcom/google/android/gms/internal/ads/zzahh;

    iget-object v14, v14, Lcom/google/android/gms/internal/ads/zzahh;->zzb:Ljava/lang/String;

    invoke-virtual {v5, v14}, Lcom/google/android/gms/internal/ads/zzad;->zzb(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzad;

    move-result-object v5

    move-object v14, v5

    .line 22
    :goto_4
    iget-object v5, v6, Lcom/google/android/gms/internal/ads/zzagn;->zza:[Lcom/google/android/gms/internal/ads/zzahh;

    .line 23
    iget-object v11, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Lcom/google/android/gms/internal/ads/zzahh;

    aput-object v11, v5, p9

    goto :goto_5

    :cond_e
    move-object v14, v5

    .line 24
    :goto_5
    invoke-virtual {v0, v8}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    move v11, v13

    goto :goto_6

    :cond_f
    move-object v14, v5

    :goto_6
    const v5, 0x61632d33

    const-string v13, "audio/ac4"

    if-ne v11, v5, :cond_10

    const-string v5, "audio/ac3"

    :goto_7
    move v11, v9

    goto/16 :goto_b

    :cond_10
    const v5, 0x65632d33

    if-ne v11, v5, :cond_11

    const-string v5, "audio/eac3"

    goto :goto_7

    :cond_11
    const v5, 0x61632d34

    if-ne v11, v5, :cond_12

    move v11, v9

    move-object v5, v13

    goto/16 :goto_b

    :cond_12
    const v5, 0x64747363

    if-ne v11, v5, :cond_13

    const-string v5, "audio/vnd.dts"

    goto :goto_7

    :cond_13
    const v5, 0x64747368

    if-eq v11, v5, :cond_27

    const v5, 0x6474736c

    if-ne v11, v5, :cond_14

    goto/16 :goto_a

    :cond_14
    const v5, 0x64747365

    if-ne v11, v5, :cond_15

    const-string v5, "audio/vnd.dts.hd;profile=lbr"

    goto :goto_7

    :cond_15
    const v5, 0x64747378

    if-ne v11, v5, :cond_16

    const-string v5, "audio/vnd.dts.uhd;profile=p2"

    goto :goto_7

    :cond_16
    const v5, 0x73616d72

    if-ne v11, v5, :cond_17

    const-string v5, "audio/3gpp"

    goto :goto_7

    :cond_17
    const v5, 0x73617762

    if-ne v11, v5, :cond_18

    const-string v5, "audio/amr-wb"

    goto :goto_7

    :cond_18
    const v5, 0x736f7774

    const-string v21, "audio/raw"

    if-ne v11, v5, :cond_19

    :goto_8
    move-object/from16 v5, v21

    const/4 v11, 0x2

    goto/16 :goto_b

    :cond_19
    const v5, 0x74776f73

    if-ne v11, v5, :cond_1a

    move-object/from16 v5, v21

    const/high16 v11, 0x10000000

    goto/16 :goto_b

    :cond_1a
    const v5, 0x6c70636d

    if-ne v11, v5, :cond_1c

    if-ne v9, v15, :cond_1b

    goto :goto_8

    :cond_1b
    move v11, v9

    move-object/from16 v5, v21

    goto :goto_b

    :cond_1c
    const v5, 0x2e6d7032

    if-eq v11, v5, :cond_26

    const v5, 0x2e6d7033

    if-ne v11, v5, :cond_1d

    goto :goto_9

    :cond_1d
    const v5, 0x6d686131

    if-ne v11, v5, :cond_1e

    const-string v5, "audio/mha1"

    goto :goto_7

    :cond_1e
    const v5, 0x6d686d31

    if-ne v11, v5, :cond_1f

    const-string v5, "audio/mhm1"

    goto/16 :goto_7

    :cond_1f
    const v5, 0x616c6163

    if-ne v11, v5, :cond_20

    const-string v5, "audio/alac"

    goto/16 :goto_7

    :cond_20
    const v5, 0x616c6177

    if-ne v11, v5, :cond_21

    const-string v5, "audio/g711-alaw"

    goto/16 :goto_7

    :cond_21
    const v5, 0x756c6177

    if-ne v11, v5, :cond_22

    const-string v5, "audio/g711-mlaw"

    goto/16 :goto_7

    :cond_22
    const v5, 0x4f707573

    if-ne v11, v5, :cond_23

    const-string v5, "audio/opus"

    goto/16 :goto_7

    :cond_23
    const v5, 0x664c6143

    if-ne v11, v5, :cond_24

    const-string v5, "audio/flac"

    goto/16 :goto_7

    :cond_24
    const v5, 0x6d6c7061

    if-ne v11, v5, :cond_25

    const-string v5, "audio/true-hd"

    goto/16 :goto_7

    :cond_25
    move v11, v9

    const/4 v5, 0x0

    goto :goto_b

    :cond_26
    :goto_9
    const-string v5, "audio/mpeg"

    goto/16 :goto_7

    :cond_27
    :goto_a
    const-string v5, "audio/vnd.dts.hd"

    goto/16 :goto_7

    :goto_b
    const/4 v9, 0x0

    const/16 v16, 0x0

    const/16 v22, 0x0

    :goto_c
    sub-int v15, v8, v1

    if-ge v15, v2, :cond_40

    .line 25
    invoke-virtual {v0, v8}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 26
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v15

    if-lez v15, :cond_28

    const/4 v1, 0x1

    goto :goto_d

    :cond_28
    const/4 v1, 0x0

    :goto_d
    const-string v2, "childAtomSize must be positive"

    .line 27
    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    .line 28
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v1

    move/from16 p7, v11

    const v11, 0x6d686143

    if-ne v1, v11, :cond_29

    add-int/lit8 v1, v15, -0xd

    add-int/lit8 v2, v8, 0xd

    .line 29
    new-array v11, v1, [B

    .line 30
    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    const/4 v2, 0x0

    .line 31
    invoke-virtual {v0, v11, v2, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzC([BII)V

    .line 32
    invoke-static {v11}, Lcom/google/android/gms/internal/ads/zzfud;->zzm(Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    move-result-object v22

    move/from16 v24, v12

    :goto_e
    const/4 v12, 0x0

    const/16 v17, 0x3

    goto/16 :goto_1d

    :cond_29
    const v11, 0x65736473

    if-eq v1, v11, :cond_39

    if-eqz p6, :cond_2e

    const v11, 0x77617665

    if-ne v1, v11, :cond_2e

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    move-result v1

    if-lt v1, v8, :cond_2a

    move/from16 v23, v1

    const/4 v1, 0x0

    const/4 v11, 0x1

    goto :goto_f

    :cond_2a
    move/from16 v23, v1

    const/4 v1, 0x0

    const/4 v11, 0x0

    .line 33
    :goto_f
    invoke-static {v11, v1}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    move/from16 v1, v23

    :goto_10
    sub-int v11, v1, v8

    if-ge v11, v15, :cond_2c

    .line 34
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 35
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v11

    if-lez v11, :cond_2b

    move-object/from16 v23, v9

    const/4 v9, 0x1

    goto :goto_11

    :cond_2b
    move-object/from16 v23, v9

    const/4 v9, 0x0

    .line 36
    :goto_11
    invoke-static {v9, v2}, Lcom/google/android/gms/internal/ads/zzabf;->zzb(ZLjava/lang/String;)V

    .line 37
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    move-result v9

    move-object/from16 v24, v2

    const v2, 0x65736473

    if-eq v9, v2, :cond_2d

    add-int/2addr v1, v11

    move-object/from16 v9, v23

    move-object/from16 v2, v24

    goto :goto_10

    :cond_2c
    move-object/from16 v23, v9

    const/4 v1, -0x1

    :cond_2d
    const/4 v2, -0x1

    const/4 v9, 0x4

    const/4 v11, 0x2

    const/16 v17, 0x3

    goto/16 :goto_16

    :cond_2e
    move-object/from16 v23, v9

    const v2, 0x64616333

    if-ne v1, v2, :cond_2f

    add-int/lit8 v1, v8, 0x8

    .line 38
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 39
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4, v14}, Lcom/google/android/gms/internal/ads/zzaac;->zzc(Lcom/google/android/gms/internal/ads/zzfb;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/ads/zzad;)Lcom/google/android/gms/internal/ads/zzam;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    :goto_12
    move/from16 v24, v12

    const/4 v12, 0x0

    const/16 v17, 0x3

    goto/16 :goto_1c

    :cond_2f
    const v2, 0x64656333

    if-ne v1, v2, :cond_30

    add-int/lit8 v1, v8, 0x8

    .line 40
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 41
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4, v14}, Lcom/google/android/gms/internal/ads/zzaac;->zzd(Lcom/google/android/gms/internal/ads/zzfb;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/ads/zzad;)Lcom/google/android/gms/internal/ads/zzam;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    goto :goto_12

    :cond_30
    const v2, 0x64616334

    if-ne v1, v2, :cond_32

    add-int/lit8 v1, v8, 0x8

    .line 42
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 43
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/internal/ads/zzaaf;->zza:I

    const/4 v2, 0x1

    .line 44
    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 45
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    move-result v9

    const/16 v11, 0x20

    and-int/2addr v9, v11

    new-instance v11, Lcom/google/android/gms/internal/ads/zzak;

    invoke-direct {v11}, Lcom/google/android/gms/internal/ads/zzak;-><init>()V

    .line 46
    invoke-virtual {v11, v1}, Lcom/google/android/gms/internal/ads/zzak;->zzH(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    .line 47
    invoke-virtual {v11, v13}, Lcom/google/android/gms/internal/ads/zzak;->zzS(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    const/4 v1, 0x2

    .line 48
    invoke-virtual {v11, v1}, Lcom/google/android/gms/internal/ads/zzak;->zzw(I)Lcom/google/android/gms/internal/ads/zzak;

    shr-int/lit8 v1, v9, 0x5

    if-eq v2, v1, :cond_31

    const v1, 0xac44

    goto :goto_13

    :cond_31
    const v1, 0xbb80

    .line 49
    :goto_13
    invoke-virtual {v11, v1}, Lcom/google/android/gms/internal/ads/zzak;->zzT(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 50
    invoke-virtual {v11, v14}, Lcom/google/android/gms/internal/ads/zzak;->zzB(Lcom/google/android/gms/internal/ads/zzad;)Lcom/google/android/gms/internal/ads/zzak;

    .line 51
    invoke-virtual {v11, v4}, Lcom/google/android/gms/internal/ads/zzak;->zzK(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    .line 52
    invoke-virtual {v11}, Lcom/google/android/gms/internal/ads/zzak;->zzY()Lcom/google/android/gms/internal/ads/zzam;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    goto :goto_12

    :cond_32
    const v2, 0x646d6c70

    if-ne v1, v2, :cond_34

    if-lez v12, :cond_33

    move v7, v12

    move/from16 v24, v7

    move-object/from16 v9, v23

    const/4 v10, 0x2

    goto/16 :goto_e

    .line 53
    :cond_33
    new-instance v0, Ljava/lang/StringBuilder;

    .line 54
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid sample rate for Dolby TrueHD MLP stream: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    move-result-object v0

    throw v0

    :cond_34
    const/4 v2, 0x0

    const v9, 0x64647473

    if-eq v1, v9, :cond_38

    const v9, 0x75647473

    if-ne v1, v9, :cond_35

    goto/16 :goto_15

    :cond_35
    const v9, 0x644f7073

    if-ne v1, v9, :cond_36

    add-int/lit8 v1, v15, -0x8

    .line 55
    sget-object v9, Lcom/google/android/gms/internal/ads/zzagr;->zza:[B

    .line 56
    array-length v11, v9

    add-int/2addr v11, v1

    invoke-static {v9, v11}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v11

    add-int/lit8 v2, v8, 0x8

    .line 57
    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 58
    array-length v2, v9

    invoke-virtual {v0, v11, v2, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzC([BII)V

    .line 59
    invoke-static {v11}, Lcom/google/android/gms/internal/ads/zzabw;->zzd([B)Ljava/util/List;

    move-result-object v22

    move/from16 v24, v12

    move-object/from16 v9, v23

    goto/16 :goto_e

    :cond_36
    const v2, 0x64664c61

    if-ne v1, v2, :cond_37

    add-int/lit8 v1, v15, -0xc

    add-int/lit8 v2, v1, 0x4

    .line 60
    new-array v2, v2, [B

    const/16 v9, 0x66

    const/4 v11, 0x0

    .line 61
    aput-byte v9, v2, v11

    const/16 v9, 0x4c

    const/4 v11, 0x1

    .line 62
    aput-byte v9, v2, v11

    const/16 v9, 0x61

    const/4 v11, 0x2

    .line 63
    aput-byte v9, v2, v11

    const/16 v9, 0x43

    const/16 v17, 0x3

    .line 64
    aput-byte v9, v2, v17

    add-int/lit8 v9, v8, 0xc

    .line 65
    invoke-virtual {v0, v9}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    const/4 v9, 0x4

    .line 66
    invoke-virtual {v0, v2, v9, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzC([BII)V

    .line 67
    invoke-static {v2}, Lcom/google/android/gms/internal/ads/zzfud;->zzm(Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    move-result-object v22

    :goto_14
    move/from16 v24, v12

    move-object/from16 v9, v23

    const/4 v12, 0x0

    goto/16 :goto_1d

    :cond_37
    const v2, 0x616c6163

    const/4 v9, 0x4

    const/4 v11, 0x2

    const/16 v17, 0x3

    if-ne v1, v2, :cond_3f

    add-int/lit8 v1, v15, -0xc

    add-int/lit8 v7, v8, 0xc

    .line 68
    new-array v10, v1, [B

    .line 69
    invoke-virtual {v0, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    const/4 v7, 0x0

    .line 70
    invoke-virtual {v0, v10, v7, v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzC([BII)V

    .line 71
    sget v1, Lcom/google/android/gms/internal/ads/zzea;->zza:I

    new-instance v1, Lcom/google/android/gms/internal/ads/zzfb;

    .line 72
    invoke-direct {v1, v10}, Lcom/google/android/gms/internal/ads/zzfb;-><init>([B)V

    const/16 v7, 0x9

    .line 73
    invoke-virtual {v1, v7}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 74
    invoke-virtual {v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    move-result v7

    const/16 v2, 0x14

    .line 75
    invoke-virtual {v1, v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 76
    invoke-virtual {v1}, Lcom/google/android/gms/internal/ads/zzfb;->zzo()I

    move-result v1

    .line 77
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    .line 78
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 79
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 80
    invoke-static {v10}, Lcom/google/android/gms/internal/ads/zzfud;->zzm(Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    move-result-object v22

    move v10, v1

    move v7, v2

    goto :goto_14

    :cond_38
    :goto_15
    const/4 v9, 0x4

    const/4 v11, 0x2

    const/16 v17, 0x3

    .line 81
    new-instance v1, Lcom/google/android/gms/internal/ads/zzak;

    invoke-direct {v1}, Lcom/google/android/gms/internal/ads/zzak;-><init>()V

    .line 82
    invoke-virtual {v1, v3}, Lcom/google/android/gms/internal/ads/zzak;->zzG(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 83
    invoke-virtual {v1, v5}, Lcom/google/android/gms/internal/ads/zzak;->zzS(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    .line 84
    invoke-virtual {v1, v10}, Lcom/google/android/gms/internal/ads/zzak;->zzw(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 85
    invoke-virtual {v1, v7}, Lcom/google/android/gms/internal/ads/zzak;->zzT(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 86
    invoke-virtual {v1, v14}, Lcom/google/android/gms/internal/ads/zzak;->zzB(Lcom/google/android/gms/internal/ads/zzad;)Lcom/google/android/gms/internal/ads/zzak;

    .line 87
    invoke-virtual {v1, v4}, Lcom/google/android/gms/internal/ads/zzak;->zzK(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    .line 88
    invoke-virtual {v1}, Lcom/google/android/gms/internal/ads/zzak;->zzY()Lcom/google/android/gms/internal/ads/zzam;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    goto/16 :goto_1b

    :cond_39
    move-object/from16 v23, v9

    const/4 v9, 0x4

    const/4 v11, 0x2

    const/16 v17, 0x3

    move v1, v8

    const/4 v2, -0x1

    :goto_16
    if-eq v1, v2, :cond_3f

    .line 89
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzagr;->zzm(Lcom/google/android/gms/internal/ads/zzfb;I)Lcom/google/android/gms/internal/ads/zzagl;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/google/android/gms/internal/ads/zzagl;->zzc(Lcom/google/android/gms/internal/ads/zzagl;)Ljava/lang/String;

    move-result-object v1

    invoke-static/range {v16 .. v16}, Lcom/google/android/gms/internal/ads/zzagl;->zzd(Lcom/google/android/gms/internal/ads/zzagl;)[B

    move-result-object v5

    if-eqz v5, :cond_3e

    const-string v2, "audio/vorbis"

    .line 90
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3c

    new-instance v2, Lcom/google/android/gms/internal/ads/zzfb;

    .line 91
    invoke-direct {v2, v5}, Lcom/google/android/gms/internal/ads/zzfb;-><init>([B)V

    const/4 v9, 0x1

    .line 92
    invoke-virtual {v2, v9}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    const/4 v11, 0x0

    :goto_17
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzfb;->zza()I

    move-result v20

    const/16 v9, 0xff

    if-lez v20, :cond_3a

    .line 93
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzfb;->zze()I

    move-result v0

    if-ne v0, v9, :cond_3a

    const/4 v0, 0x1

    .line 94
    invoke-virtual {v2, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    add-int/lit16 v11, v11, 0xff

    move-object/from16 v0, p0

    const/4 v9, 0x1

    goto :goto_17

    .line 95
    :cond_3a
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    move-result v0

    add-int/2addr v11, v0

    const/4 v0, 0x0

    :goto_18
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzfb;->zza()I

    move-result v22

    move/from16 v24, v12

    if-lez v22, :cond_3b

    .line 96
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzfb;->zze()I

    move-result v12

    if-ne v12, v9, :cond_3b

    const/4 v12, 0x1

    .line 97
    invoke-virtual {v2, v12}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    add-int/lit16 v0, v0, 0xff

    move/from16 v12, v24

    goto :goto_18

    :cond_3b
    const/4 v12, 0x1

    .line 98
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzl()I

    move-result v9

    add-int/2addr v0, v9

    .line 99
    new-array v9, v11, [B

    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    move-result v2

    const/4 v12, 0x0

    .line 100
    invoke-static {v5, v2, v9, v12, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v2, v11

    array-length v11, v5

    add-int/2addr v2, v0

    sub-int/2addr v11, v2

    .line 101
    new-array v0, v11, [B

    .line 102
    invoke-static {v5, v2, v0, v12, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 103
    invoke-static {v9, v0}, Lcom/google/android/gms/internal/ads/zzfud;->zzn(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    move-result-object v22

    goto :goto_1a

    :cond_3c
    move/from16 v24, v12

    const/4 v12, 0x0

    const-string v0, "audio/mp4a-latm"

    .line 104
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 105
    invoke-static {v5}, Lcom/google/android/gms/internal/ads/zzzz;->zza([B)Lcom/google/android/gms/internal/ads/zzzy;

    move-result-object v0

    iget v7, v0, Lcom/google/android/gms/internal/ads/zzzy;->zza:I

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzzy;->zzb:I

    iget-object v9, v0, Lcom/google/android/gms/internal/ads/zzzy;->zzc:Ljava/lang/String;

    goto :goto_19

    :cond_3d
    move-object/from16 v9, v23

    .line 106
    :goto_19
    invoke-static {v5}, Lcom/google/android/gms/internal/ads/zzfud;->zzm(Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    move-result-object v22

    move-object v5, v1

    goto :goto_1d

    :cond_3e
    move/from16 v24, v12

    const/4 v12, 0x0

    :goto_1a
    move-object v5, v1

    goto :goto_1c

    :cond_3f
    :goto_1b
    move/from16 v24, v12

    const/4 v12, 0x0

    :goto_1c
    move-object/from16 v9, v23

    :goto_1d
    add-int/2addr v8, v15

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v11, p7

    move/from16 v12, v24

    goto/16 :goto_c

    :cond_40
    move-object/from16 v23, v9

    move/from16 p7, v11

    .line 107
    iget-object v0, v6, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    if-nez v0, :cond_42

    if-eqz v5, :cond_42

    new-instance v0, Lcom/google/android/gms/internal/ads/zzak;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ads/zzak;-><init>()V

    .line 108
    invoke-virtual {v0, v3}, Lcom/google/android/gms/internal/ads/zzak;->zzG(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 109
    invoke-virtual {v0, v5}, Lcom/google/android/gms/internal/ads/zzak;->zzS(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    move-object/from16 v9, v23

    .line 110
    invoke-virtual {v0, v9}, Lcom/google/android/gms/internal/ads/zzak;->zzx(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    .line 111
    invoke-virtual {v0, v10}, Lcom/google/android/gms/internal/ads/zzak;->zzw(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 112
    invoke-virtual {v0, v7}, Lcom/google/android/gms/internal/ads/zzak;->zzT(I)Lcom/google/android/gms/internal/ads/zzak;

    move/from16 v9, p7

    .line 113
    invoke-virtual {v0, v9}, Lcom/google/android/gms/internal/ads/zzak;->zzN(I)Lcom/google/android/gms/internal/ads/zzak;

    move-object/from16 v1, v22

    .line 114
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzak;->zzI(Ljava/util/List;)Lcom/google/android/gms/internal/ads/zzak;

    .line 115
    invoke-virtual {v0, v14}, Lcom/google/android/gms/internal/ads/zzak;->zzB(Lcom/google/android/gms/internal/ads/zzad;)Lcom/google/android/gms/internal/ads/zzak;

    .line 116
    invoke-virtual {v0, v4}, Lcom/google/android/gms/internal/ads/zzak;->zzK(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    if-eqz v16, :cond_41

    invoke-static/range {v16 .. v16}, Lcom/google/android/gms/internal/ads/zzagl;->zza(Lcom/google/android/gms/internal/ads/zzagl;)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ads/zzfwl;->zzc(J)I

    move-result v1

    .line 117
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzak;->zzv(I)Lcom/google/android/gms/internal/ads/zzak;

    invoke-static/range {v16 .. v16}, Lcom/google/android/gms/internal/ads/zzagl;->zzb(Lcom/google/android/gms/internal/ads/zzagl;)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ads/zzfwl;->zzc(J)I

    move-result v1

    .line 118
    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/ads/zzak;->zzO(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 119
    :cond_41
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ads/zzak;->zzY()Lcom/google/android/gms/internal/ads/zzam;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    :cond_42
    return-void
.end method

.method private static zzp(Lcom/google/android/gms/internal/ads/zzfb;IIILcom/google/android/gms/internal/ads/zzagn;)V
    .locals 0

    .line 1
    add-int/lit8 p2, p2, 0x10

    .line 2
    .line 3
    invoke-virtual {p0, p2}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzw(C)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/ads/zzfb;->zzw(C)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    if-eqz p0, :cond_0

    .line 15
    .line 16
    new-instance p1, Lcom/google/android/gms/internal/ads/zzak;

    .line 17
    .line 18
    invoke-direct {p1}, Lcom/google/android/gms/internal/ads/zzak;-><init>()V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1, p3}, Lcom/google/android/gms/internal/ads/zzak;->zzG(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, p0}, Lcom/google/android/gms/internal/ads/zzak;->zzS(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/google/android/gms/internal/ads/zzak;->zzY()Lcom/google/android/gms/internal/ads/zzam;

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    iput-object p0, p4, Lcom/google/android/gms/internal/ads/zzagn;->zzb:Lcom/google/android/gms/internal/ads/zzam;

    .line 32
    .line 33
    :cond_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
.end method

.method private static zzq([JJJJ)Z
    .locals 6

    .line 1
    array-length v0, p0

    .line 2
    add-int/lit8 v1, v0, -0x1

    .line 3
    .line 4
    const/4 v2, 0x4

    .line 5
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    const/4 v3, 0x0

    .line 10
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    add-int/lit8 v0, v0, -0x4

    .line 15
    .line 16
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    aget-wide v4, p0, v3

    .line 25
    .line 26
    cmp-long v1, v4, p3

    .line 27
    .line 28
    if-gtz v1, :cond_0

    .line 29
    .line 30
    aget-wide v1, p0, v2

    .line 31
    .line 32
    cmp-long v4, p3, v1

    .line 33
    .line 34
    if-gez v4, :cond_0

    .line 35
    .line 36
    aget-wide p3, p0, v0

    .line 37
    .line 38
    cmp-long p0, p3, p5

    .line 39
    .line 40
    if-gez p0, :cond_0

    .line 41
    .line 42
    cmp-long p0, p5, p1

    .line 43
    .line 44
    if-gtz p0, :cond_0

    .line 45
    .line 46
    const/4 p0, 0x1

    .line 47
    return p0

    .line 48
    :cond_0
    return v3
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
.end method

.method private static zzr(I)Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eq p0, v0, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const/4 p0, 0x0

    .line 6
    return p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static zzs(Lcom/google/android/gms/internal/ads/zzfb;II)[B
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    add-int/lit8 v0, p1, 0x8

    .line 2
    .line 3
    :goto_0
    sub-int v1, v0, p1

    .line 4
    .line 5
    if-ge v1, p2, :cond_1

    .line 6
    .line 7
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzf()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    const v3, 0x70726f6a

    .line 19
    .line 20
    .line 21
    if-ne v2, v3, :cond_0

    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 24
    .line 25
    .line 26
    move-result-object p0

    .line 27
    add-int/2addr v1, v0

    .line 28
    invoke-static {p0, v0, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    .line 29
    .line 30
    .line 31
    move-result-object p0

    .line 32
    return-object p0

    .line 33
    :cond_0
    add-int/2addr v0, v1

    .line 34
    goto :goto_0

    .line 35
    :cond_1
    const/4 p0, 0x0

    .line 36
    return-object p0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
.end method

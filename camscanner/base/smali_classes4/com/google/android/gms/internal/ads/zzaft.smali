.class public final Lcom/google/android/gms/internal/ads/zzaft;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-ads@@22.4.0"


# instance fields
.field public zzA:I

.field public zzB:I

.field public zzC:F

.field public zzD:F

.field public zzE:F

.field public zzF:F

.field public zzG:F

.field public zzH:F

.field public zzI:F

.field public zzJ:F

.field public zzK:F

.field public zzL:F

.field public zzM:[B

.field public zzN:I

.field public zzO:I

.field public zzP:I

.field public zzQ:J

.field public zzR:J

.field public zzS:Lcom/google/android/gms/internal/ads/zzacf;

.field public zzT:Z

.field public zzU:Z

.field public zzV:Lcom/google/android/gms/internal/ads/zzace;

.field public zzW:I

.field private zzX:I

.field private zzY:Ljava/lang/String;

.field public zza:Ljava/lang/String;

.field public zzb:Ljava/lang/String;

.field public zzc:I

.field public zzd:I

.field public zze:I

.field public zzf:I

.field public zzg:Z

.field public zzh:[B

.field public zzi:Lcom/google/android/gms/internal/ads/zzacd;

.field public zzj:[B

.field public zzk:Lcom/google/android/gms/internal/ads/zzad;

.field public zzl:I

.field public zzm:I

.field public zzn:I

.field public zzo:I

.field public zzp:I

.field public zzq:I

.field public zzr:F

.field public zzs:F

.field public zzt:F

.field public zzu:[B

.field public zzv:I

.field public zzw:Z

.field public zzx:I

.field public zzy:I

.field public zzz:I


# direct methods
.method protected constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzl:I

    .line 6
    .line 7
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzm:I

    .line 8
    .line 9
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzn:I

    .line 10
    .line 11
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzo:I

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzp:I

    .line 15
    .line 16
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzq:I

    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    iput v2, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzr:F

    .line 20
    .line 21
    iput v2, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzs:F

    .line 22
    .line 23
    iput v2, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzt:F

    .line 24
    .line 25
    const/4 v2, 0x0

    .line 26
    iput-object v2, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzu:[B

    .line 27
    .line 28
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzv:I

    .line 29
    .line 30
    iput-boolean v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzw:Z

    .line 31
    .line 32
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzx:I

    .line 33
    .line 34
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzy:I

    .line 35
    .line 36
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzz:I

    .line 37
    .line 38
    const/16 v1, 0x3e8

    .line 39
    .line 40
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzA:I

    .line 41
    .line 42
    const/16 v1, 0xc8

    .line 43
    .line 44
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzB:I

    .line 45
    .line 46
    const/high16 v1, -0x40800000    # -1.0f

    .line 47
    .line 48
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzC:F

    .line 49
    .line 50
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzD:F

    .line 51
    .line 52
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzE:F

    .line 53
    .line 54
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzF:F

    .line 55
    .line 56
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzG:F

    .line 57
    .line 58
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzH:F

    .line 59
    .line 60
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzI:F

    .line 61
    .line 62
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzJ:F

    .line 63
    .line 64
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzK:F

    .line 65
    .line 66
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzL:F

    .line 67
    .line 68
    const/4 v1, 0x1

    .line 69
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzN:I

    .line 70
    .line 71
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzO:I

    .line 72
    .line 73
    const/16 v0, 0x1f40

    .line 74
    .line 75
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzP:I

    .line 76
    .line 77
    const-wide/16 v2, 0x0

    .line 78
    .line 79
    iput-wide v2, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzQ:J

    .line 80
    .line 81
    iput-wide v2, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzR:J

    .line 82
    .line 83
    iput-boolean v1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzU:Z

    .line 84
    .line 85
    const-string v0, "eng"

    .line 86
    .line 87
    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzY:Ljava/lang/String;

    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
.end method

.method static bridge synthetic zza(Lcom/google/android/gms/internal/ads/zzaft;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzX:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic zzb(Lcom/google/android/gms/internal/ads/zzaft;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzX:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
.end method

.method static bridge synthetic zzc(Lcom/google/android/gms/internal/ads/zzaft;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzY:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
.end method

.method static synthetic zzd(Lcom/google/android/gms/internal/ads/zzaft;)V
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzV:Lcom/google/android/gms/internal/ads/zzace;

    .line 2
    .line 3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static zzf(Lcom/google/android/gms/internal/ads/zzfb;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzcd;
        }
    .end annotation

    .line 1
    const/16 v0, 0x10

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzH(I)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzr()J

    .line 8
    .line 9
    .line 10
    move-result-wide v2

    .line 11
    const-wide/32 v4, 0x58564944

    .line 12
    .line 13
    .line 14
    cmp-long v0, v2, v4

    .line 15
    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    new-instance p0, Landroid/util/Pair;

    .line 19
    .line 20
    const-string v0, "video/divx"

    .line 21
    .line 22
    invoke-direct {p0, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 23
    .line 24
    .line 25
    return-object p0

    .line 26
    :cond_0
    const-wide/32 v4, 0x33363248

    .line 27
    .line 28
    .line 29
    cmp-long v0, v2, v4

    .line 30
    .line 31
    if-nez v0, :cond_1

    .line 32
    .line 33
    new-instance p0, Landroid/util/Pair;

    .line 34
    .line 35
    const-string v0, "video/3gpp"

    .line 36
    .line 37
    invoke-direct {p0, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 38
    .line 39
    .line 40
    return-object p0

    .line 41
    :cond_1
    const-wide/32 v4, 0x31435657

    .line 42
    .line 43
    .line 44
    cmp-long v0, v2, v4

    .line 45
    .line 46
    if-nez v0, :cond_4

    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzc()I

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    add-int/lit8 v0, v0, 0x14

    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzI()[B

    .line 55
    .line 56
    .line 57
    move-result-object p0

    .line 58
    :goto_0
    array-length v2, p0

    .line 59
    add-int/lit8 v3, v2, -0x4

    .line 60
    .line 61
    if-ge v0, v3, :cond_3

    .line 62
    .line 63
    aget-byte v3, p0, v0

    .line 64
    .line 65
    if-nez v3, :cond_2

    .line 66
    .line 67
    add-int/lit8 v3, v0, 0x1

    .line 68
    .line 69
    aget-byte v3, p0, v3

    .line 70
    .line 71
    if-nez v3, :cond_2

    .line 72
    .line 73
    add-int/lit8 v3, v0, 0x2

    .line 74
    .line 75
    aget-byte v3, p0, v3

    .line 76
    .line 77
    const/4 v4, 0x1

    .line 78
    if-ne v3, v4, :cond_2

    .line 79
    .line 80
    add-int/lit8 v3, v0, 0x3

    .line 81
    .line 82
    aget-byte v3, p0, v3

    .line 83
    .line 84
    const/16 v4, 0xf

    .line 85
    .line 86
    if-ne v3, v4, :cond_2

    .line 87
    .line 88
    invoke-static {p0, v0, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    .line 89
    .line 90
    .line 91
    move-result-object p0

    .line 92
    new-instance v0, Landroid/util/Pair;

    .line 93
    .line 94
    const-string v2, "video/wvc1"

    .line 95
    .line 96
    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    .line 97
    .line 98
    .line 99
    move-result-object p0

    .line 100
    invoke-direct {v0, v2, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 101
    .line 102
    .line 103
    return-object v0

    .line 104
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 105
    .line 106
    goto :goto_0

    .line 107
    :cond_3
    const-string p0, "Failed to find FourCC VC1 initialization data"

    .line 108
    .line 109
    invoke-static {p0, v1}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    .line 110
    .line 111
    .line 112
    move-result-object p0

    .line 113
    throw p0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :cond_4
    const-string p0, "MatroskaExtractor"

    .line 115
    .line 116
    const-string v0, "Unknown FourCC. Setting mimeType to video/x-unknown"

    .line 117
    .line 118
    invoke-static {p0, v0}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    new-instance p0, Landroid/util/Pair;

    .line 122
    .line 123
    const-string v0, "video/x-unknown"

    .line 124
    .line 125
    invoke-direct {p0, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 126
    .line 127
    .line 128
    return-object p0

    .line 129
    :catch_0
    const-string p0, "Error parsing FourCC private data"

    .line 130
    .line 131
    invoke-static {p0, v1}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    .line 132
    .line 133
    .line 134
    move-result-object p0

    .line 135
    throw p0
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method private static zzg([B)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzcd;
        }
    .end annotation

    .line 1
    const-string v0, "Error parsing vorbis codec private"

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x0

    .line 5
    :try_start_0
    aget-byte v3, p0, v2

    .line 6
    .line 7
    const/4 v4, 0x2

    .line 8
    if-ne v3, v4, :cond_5

    .line 9
    .line 10
    const/4 v3, 0x1

    .line 11
    const/4 v5, 0x1

    .line 12
    const/4 v6, 0x0

    .line 13
    :goto_0
    aget-byte v7, p0, v5

    .line 14
    .line 15
    const/16 v8, 0xff

    .line 16
    .line 17
    and-int/2addr v7, v8

    .line 18
    if-ne v7, v8, :cond_0

    .line 19
    .line 20
    add-int/lit8 v5, v5, 0x1

    .line 21
    .line 22
    add-int/lit16 v6, v6, 0xff

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    add-int/2addr v6, v7

    .line 26
    add-int/2addr v5, v3

    .line 27
    const/4 v7, 0x0

    .line 28
    :goto_1
    aget-byte v9, p0, v5

    .line 29
    .line 30
    and-int/2addr v9, v8

    .line 31
    if-ne v9, v8, :cond_1

    .line 32
    .line 33
    add-int/lit8 v5, v5, 0x1

    .line 34
    .line 35
    add-int/lit16 v7, v7, 0xff

    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_1
    add-int/2addr v5, v3

    .line 39
    add-int/2addr v7, v9

    .line 40
    aget-byte v8, p0, v5

    .line 41
    .line 42
    if-ne v8, v3, :cond_4

    .line 43
    .line 44
    new-array v3, v6, [B

    .line 45
    .line 46
    invoke-static {p0, v5, v3, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 47
    .line 48
    .line 49
    add-int/2addr v5, v6

    .line 50
    aget-byte v6, p0, v5

    .line 51
    .line 52
    const/4 v8, 0x3

    .line 53
    if-ne v6, v8, :cond_3

    .line 54
    .line 55
    add-int/2addr v5, v7

    .line 56
    aget-byte v6, p0, v5

    .line 57
    .line 58
    const/4 v7, 0x5

    .line 59
    if-ne v6, v7, :cond_2

    .line 60
    .line 61
    array-length v6, p0

    .line 62
    sub-int/2addr v6, v5

    .line 63
    new-array v7, v6, [B

    .line 64
    .line 65
    invoke-static {p0, v5, v7, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 66
    .line 67
    .line 68
    new-instance p0, Ljava/util/ArrayList;

    .line 69
    .line 70
    invoke-direct {p0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 71
    .line 72
    .line 73
    invoke-interface {p0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    .line 75
    .line 76
    invoke-interface {p0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    return-object p0

    .line 80
    :cond_2
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    .line 81
    .line 82
    .line 83
    move-result-object p0

    .line 84
    throw p0

    .line 85
    :cond_3
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    .line 86
    .line 87
    .line 88
    move-result-object p0

    .line 89
    throw p0

    .line 90
    :cond_4
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    .line 91
    .line 92
    .line 93
    move-result-object p0

    .line 94
    throw p0

    .line 95
    :cond_5
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    .line 96
    .line 97
    .line 98
    move-result-object p0

    .line 99
    throw p0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :catch_0
    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    .line 101
    .line 102
    .line 103
    move-result-object p0

    .line 104
    throw p0
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method private static zzh(Lcom/google/android/gms/internal/ads/zzfb;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzcd;
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzj()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    const v2, 0xfffe

    .line 10
    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    if-ne v0, v2, :cond_1

    .line 14
    .line 15
    const/16 v0, 0x18

    .line 16
    .line 17
    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/ads/zzfb;->zzG(I)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzs()J

    .line 21
    .line 22
    .line 23
    move-result-wide v4

    .line 24
    invoke-static {}, Lcom/google/android/gms/internal/ads/zzafu;->zzf()Ljava/util/UUID;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {v0}, Ljava/util/UUID;->getMostSignificantBits()J

    .line 29
    .line 30
    .line 31
    move-result-wide v6

    .line 32
    cmp-long v0, v4, v6

    .line 33
    .line 34
    if-nez v0, :cond_1

    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ads/zzfb;->zzs()J

    .line 37
    .line 38
    .line 39
    move-result-wide v4

    .line 40
    invoke-static {}, Lcom/google/android/gms/internal/ads/zzafu;->zzf()Ljava/util/UUID;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    invoke-virtual {p0}, Ljava/util/UUID;->getLeastSignificantBits()J

    .line 45
    .line 46
    .line 47
    move-result-wide v6
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    cmp-long p0, v4, v6

    .line 49
    .line 50
    if-nez p0, :cond_1

    .line 51
    .line 52
    return v1

    .line 53
    :cond_1
    return v3

    .line 54
    :catch_0
    const-string p0, "Error parsing MS/ACM codec private"

    .line 55
    .line 56
    const/4 v0, 0x0

    .line 57
    invoke-static {p0, v0}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    .line 58
    .line 59
    .line 60
    move-result-object p0

    .line 61
    throw p0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final zzi(Ljava/lang/String;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzcd;
        }
    .end annotation

    .annotation runtime Lorg/checkerframework/checker/nullness/qual/EnsuresNonNull;
        value = {
            "codecPrivate"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzaft;->zzj:[B

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    const-string v0, "Missing CodecPrivate for codec "

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    const/4 v0, 0x0

    .line 17
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    throw p1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public final zze(Lcom/google/android/gms/internal/ads/zzabe;I)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzcd;
        }
    .end annotation

    .annotation runtime Lorg/checkerframework/checker/nullness/qual/EnsuresNonNull;
        value = {
            "this.output"
        }
    .end annotation

    .annotation runtime Lorg/checkerframework/checker/nullness/qual/RequiresNonNull;
        value = {
            "codecId"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 1
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzb:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/16 v3, 0x18

    const/16 v5, 0x10

    const/4 v6, 0x1

    const/16 v8, 0x20

    const/16 v9, 0x8

    const/4 v10, 0x4

    const/4 v11, 0x0

    const/4 v12, 0x3

    const/4 v13, -0x1

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v2, "A_OPUS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0xc

    goto/16 :goto_1

    :sswitch_1
    const-string v2, "A_FLAC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x16

    goto/16 :goto_1

    :sswitch_2
    const-string v2, "A_EAC3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x11

    goto/16 :goto_1

    :sswitch_3
    const-string v2, "V_MPEG2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x3

    goto/16 :goto_1

    :sswitch_4
    const-string v2, "S_TEXT/UTF8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x1b

    goto/16 :goto_1

    :sswitch_5
    const-string v2, "S_TEXT/WEBVTT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x1d

    goto/16 :goto_1

    :sswitch_6
    const-string v2, "V_MPEGH/ISO/HEVC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x8

    goto/16 :goto_1

    :sswitch_7
    const-string v2, "S_TEXT/ASS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x1c

    goto/16 :goto_1

    :sswitch_8
    const-string v2, "A_PCM/INT/LIT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x18

    goto/16 :goto_1

    :sswitch_9
    const-string v2, "A_PCM/INT/BIG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x19

    goto/16 :goto_1

    :sswitch_a
    const-string v2, "A_PCM/FLOAT/IEEE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x1a

    goto/16 :goto_1

    :sswitch_b
    const-string v2, "A_DTS/EXPRESS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x14

    goto/16 :goto_1

    :sswitch_c
    const-string v2, "V_THEORA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0xa

    goto/16 :goto_1

    :sswitch_d
    const-string v2, "S_HDMV/PGS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x1f

    goto/16 :goto_1

    :sswitch_e
    const-string v2, "V_VP9"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto/16 :goto_1

    :sswitch_f
    const-string v2, "V_VP8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto/16 :goto_1

    :sswitch_10
    const-string v2, "V_AV1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    goto/16 :goto_1

    :sswitch_11
    const-string v2, "A_DTS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x13

    goto/16 :goto_1

    :sswitch_12
    const-string v2, "A_AC3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x10

    goto/16 :goto_1

    :sswitch_13
    const-string v2, "A_AAC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0xd

    goto/16 :goto_1

    :sswitch_14
    const-string v2, "A_DTS/LOSSLESS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x15

    goto/16 :goto_1

    :sswitch_15
    const-string v2, "S_VOBSUB"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x1e

    goto/16 :goto_1

    :sswitch_16
    const-string v2, "V_MPEG4/ISO/AVC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x7

    goto/16 :goto_1

    :sswitch_17
    const-string v2, "V_MPEG4/ISO/ASP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x5

    goto/16 :goto_1

    :sswitch_18
    const-string v2, "S_DVBSUB"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x20

    goto :goto_1

    :sswitch_19
    const-string v2, "V_MS/VFW/FOURCC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x9

    goto :goto_1

    :sswitch_1a
    const-string v2, "A_MPEG/L3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0xf

    goto :goto_1

    :sswitch_1b
    const-string v2, "A_MPEG/L2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0xe

    goto :goto_1

    :sswitch_1c
    const-string v2, "A_VORBIS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0xb

    goto :goto_1

    :sswitch_1d
    const-string v2, "A_TRUEHD"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x12

    goto :goto_1

    :sswitch_1e
    const-string v2, "A_MS/ACM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x17

    goto :goto_1

    :sswitch_1f
    const-string v2, "V_MPEG4/ISO/SP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x4

    goto :goto_1

    :sswitch_20
    const-string v2, "V_MPEG4/ISO/AP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x6

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v2, -0x1

    :goto_1
    const-string v14, "text/x-ssa"

    const-string v15, "application/x-subrip"

    const/16 v16, 0x1000

    const-string v17, "audio/raw"

    const-string v18, "audio/x-unknown"

    const-string v7, "MatroskaExtractor"

    const-string v4, ". Setting mimeType to audio/x-unknown"

    packed-switch v2, :pswitch_data_0

    const-string v1, "Unrecognized codec identifier."

    const/4 v2, 0x0

    .line 2
    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    move-result-object v1

    throw v1

    :pswitch_0
    new-array v2, v10, [B

    .line 3
    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ads/zzaft;->zzi(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1, v11, v2, v11, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4
    invoke-static {v2}, Lcom/google/android/gms/internal/ads/zzfud;->zzm(Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    move-result-object v1

    const-string v17, "application/dvbsubs"

    goto/16 :goto_5

    :pswitch_1
    const-string v17, "application/pgs"

    goto/16 :goto_9

    .line 5
    :pswitch_2
    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ads/zzaft;->zzi(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzfud;->zzm(Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    move-result-object v1

    const-string v17, "application/vobsub"

    goto/16 :goto_5

    :pswitch_3
    const-string v17, "text/vtt"

    goto/16 :goto_9

    .line 6
    :pswitch_4
    invoke-static {}, Lcom/google/android/gms/internal/ads/zzafu;->zzm()[B

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzb:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/google/android/gms/internal/ads/zzaft;->zzi(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ads/zzfud;->zzn(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfud;

    move-result-object v1

    move-object v2, v1

    move-object/from16 v17, v14

    goto/16 :goto_6

    :pswitch_5
    move-object/from16 v17, v15

    goto/16 :goto_9

    .line 7
    :pswitch_6
    iget v1, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzO:I

    if-ne v1, v8, :cond_1

    goto/16 :goto_2

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    .line 8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported floating point PCM bit depth: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 9
    :pswitch_7
    iget v1, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzO:I

    if-ne v1, v9, :cond_2

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v10, 0x3

    goto/16 :goto_d

    :cond_2
    if-ne v1, v5, :cond_3

    const/high16 v10, 0x10000000

    goto :goto_2

    :cond_3
    if-ne v1, v3, :cond_4

    const/high16 v10, 0x50000000

    goto :goto_2

    :cond_4
    if-ne v1, v8, :cond_5

    const/high16 v10, 0x60000000

    goto :goto_2

    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported big endian PCM bit depth: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 11
    :pswitch_8
    iget v1, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzO:I

    .line 12
    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzfk;->zzk(I)I

    move-result v10

    if-nez v10, :cond_6

    iget v1, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzO:I

    new-instance v2, Ljava/lang/StringBuilder;

    .line 13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported little endian PCM bit depth: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    :goto_2
    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    goto/16 :goto_d

    .line 14
    :pswitch_9
    new-instance v1, Lcom/google/android/gms/internal/ads/zzfb;

    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzb:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/google/android/gms/internal/ads/zzaft;->zzi(Ljava/lang/String;)[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/ads/zzfb;-><init>([B)V

    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzaft;->zzh(Lcom/google/android/gms/internal/ads/zzfb;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget v1, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzO:I

    .line 15
    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzfk;->zzk(I)I

    move-result v10

    if-nez v10, :cond_6

    iget v1, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzO:I

    new-instance v2, Ljava/lang/StringBuilder;

    .line 16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported PCM bit depth: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    const-string v1, "Non-PCM MS/ACM is unsupported. Setting mimeType to audio/x-unknown"

    .line 17
    invoke-static {v7, v1}, Lcom/google/android/gms/internal/ads/zzes;->zzf(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    move-object/from16 v17, v18

    goto/16 :goto_9

    .line 18
    :pswitch_a
    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ads/zzaft;->zzi(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v17, "audio/flac"

    goto/16 :goto_5

    :pswitch_b
    const-string v17, "audio/vnd.dts.hd"

    goto/16 :goto_9

    :pswitch_c
    const-string v17, "audio/vnd.dts"

    goto/16 :goto_9

    .line 19
    :pswitch_d
    new-instance v1, Lcom/google/android/gms/internal/ads/zzacf;

    invoke-direct {v1}, Lcom/google/android/gms/internal/ads/zzacf;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzS:Lcom/google/android/gms/internal/ads/zzacf;

    const-string v17, "audio/true-hd"

    goto/16 :goto_9

    :pswitch_e
    const-string v17, "audio/eac3"

    goto/16 :goto_9

    :pswitch_f
    const-string v17, "audio/ac3"

    goto/16 :goto_9

    :pswitch_10
    const-string v17, "audio/mpeg"

    goto :goto_4

    :pswitch_11
    const-string v17, "audio/mpeg-L2"

    :goto_4
    const/16 v1, 0x1000

    goto/16 :goto_a

    .line 20
    :pswitch_12
    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ads/zzaft;->zzi(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzj:[B

    .line 21
    invoke-static {v2}, Lcom/google/android/gms/internal/ads/zzzz;->zza([B)Lcom/google/android/gms/internal/ads/zzzy;

    move-result-object v2

    iget v3, v2, Lcom/google/android/gms/internal/ads/zzzy;->zza:I

    iput v3, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzP:I

    iget v3, v2, Lcom/google/android/gms/internal/ads/zzzy;->zzb:I

    iput v3, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzN:I

    iget-object v2, v2, Lcom/google/android/gms/internal/ads/zzzy;->zzc:Ljava/lang/String;

    const-string v17, "audio/mp4a-latm"

    move-object v3, v2

    const/4 v10, -0x1

    move-object v2, v1

    const/4 v1, -0x1

    goto/16 :goto_d

    :pswitch_13
    new-instance v1, Ljava/util/ArrayList;

    .line 22
    invoke-direct {v1, v12}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzb:Ljava/lang/String;

    .line 23
    invoke-direct {v0, v2}, Lcom/google/android/gms/internal/ads/zzaft;->zzi(Ljava/lang/String;)[B

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    invoke-static {v9}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzQ:J

    invoke-virtual {v2, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    .line 25
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    invoke-static {v9}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    iget-wide v3, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzR:J

    invoke-virtual {v2, v3, v4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    .line 27
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v16, 0x1680

    const-string v17, "audio/opus"

    move-object v2, v1

    const/16 v1, 0x1680

    goto/16 :goto_b

    .line 28
    :pswitch_14
    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ads/zzaft;->zzi(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzaft;->zzg([B)Ljava/util/List;

    move-result-object v1

    const/16 v16, 0x2000

    const-string v17, "audio/vorbis"

    move-object v2, v1

    const/16 v1, 0x2000

    goto/16 :goto_b

    :pswitch_15
    const-string v17, "video/x-unknown"

    goto/16 :goto_9

    .line 29
    :pswitch_16
    new-instance v1, Lcom/google/android/gms/internal/ads/zzfb;

    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzb:Ljava/lang/String;

    .line 30
    invoke-direct {v0, v2}, Lcom/google/android/gms/internal/ads/zzaft;->zzi(Ljava/lang/String;)[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/ads/zzfb;-><init>([B)V

    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzaft;->zzf(Lcom/google/android/gms/internal/ads/zzfb;)Landroid/util/Pair;

    move-result-object v1

    .line 31
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v17, v2

    check-cast v17, Ljava/lang/String;

    .line 32
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    :goto_5
    move-object v2, v1

    :goto_6
    const/4 v1, -0x1

    goto :goto_b

    .line 33
    :pswitch_17
    new-instance v1, Lcom/google/android/gms/internal/ads/zzfb;

    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzb:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/google/android/gms/internal/ads/zzaft;->zzi(Ljava/lang/String;)[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/ads/zzfb;-><init>([B)V

    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzabr;->zza(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzabr;

    move-result-object v1

    iget-object v2, v1, Lcom/google/android/gms/internal/ads/zzabr;->zza:Ljava/util/List;

    iget v3, v1, Lcom/google/android/gms/internal/ads/zzabr;->zzb:I

    iput v3, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzW:I

    iget-object v1, v1, Lcom/google/android/gms/internal/ads/zzabr;->zzg:Ljava/lang/String;

    const-string v17, "video/hevc"

    goto :goto_7

    .line 34
    :pswitch_18
    new-instance v1, Lcom/google/android/gms/internal/ads/zzfb;

    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzb:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/google/android/gms/internal/ads/zzaft;->zzi(Ljava/lang/String;)[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/ads/zzfb;-><init>([B)V

    invoke-static {v1}, Lcom/google/android/gms/internal/ads/zzaag;->zza(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzaag;

    move-result-object v1

    iget-object v2, v1, Lcom/google/android/gms/internal/ads/zzaag;->zza:Ljava/util/List;

    iget v3, v1, Lcom/google/android/gms/internal/ads/zzaag;->zzb:I

    iput v3, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzW:I

    iget-object v1, v1, Lcom/google/android/gms/internal/ads/zzaag;->zzi:Ljava/lang/String;

    const-string v17, "video/avc"

    :goto_7
    move-object v3, v1

    const/4 v1, -0x1

    goto :goto_c

    :pswitch_19
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzj:[B

    if-nez v1, :cond_8

    const/4 v1, 0x0

    goto :goto_8

    .line 35
    :cond_8
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    :goto_8
    const-string v17, "video/mp4v-es"

    goto :goto_5

    :pswitch_1a
    const-string v17, "video/mpeg2"

    goto :goto_9

    :pswitch_1b
    const-string v17, "video/av01"

    goto :goto_9

    :pswitch_1c
    const-string v17, "video/x-vnd.on2.vp9"

    goto :goto_9

    :pswitch_1d
    const-string v17, "video/x-vnd.on2.vp8"

    :goto_9
    const/4 v1, -0x1

    :goto_a
    const/4 v2, 0x0

    :goto_b
    const/4 v3, 0x0

    :goto_c
    const/4 v10, -0x1

    .line 36
    :goto_d
    iget-object v4, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzM:[B

    if-eqz v4, :cond_9

    .line 37
    new-instance v4, Lcom/google/android/gms/internal/ads/zzfb;

    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzM:[B

    invoke-direct {v4, v5}, Lcom/google/android/gms/internal/ads/zzfb;-><init>([B)V

    .line 38
    invoke-static {v4}, Lcom/google/android/gms/internal/ads/zzaax;->zza(Lcom/google/android/gms/internal/ads/zzfb;)Lcom/google/android/gms/internal/ads/zzaax;

    move-result-object v4

    if-eqz v4, :cond_9

    iget-object v3, v4, Lcom/google/android/gms/internal/ads/zzaax;->zza:Ljava/lang/String;

    const-string v17, "video/dolby-vision"

    :cond_9
    move-object/from16 v4, v17

    iget-boolean v5, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzU:Z

    iget-boolean v7, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzT:Z

    if-eq v6, v7, :cond_a

    const/4 v7, 0x0

    goto :goto_e

    :cond_a
    const/4 v7, 0x2

    :goto_e
    or-int/2addr v5, v7

    new-instance v7, Lcom/google/android/gms/internal/ads/zzak;

    invoke-direct {v7}, Lcom/google/android/gms/internal/ads/zzak;-><init>()V

    .line 39
    invoke-static {v4}, Lcom/google/android/gms/internal/ads/zzcc;->zzf(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    iget v8, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzN:I

    .line 40
    invoke-virtual {v7, v8}, Lcom/google/android/gms/internal/ads/zzak;->zzw(I)Lcom/google/android/gms/internal/ads/zzak;

    iget v8, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzP:I

    .line 41
    invoke-virtual {v7, v8}, Lcom/google/android/gms/internal/ads/zzak;->zzT(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 42
    invoke-virtual {v7, v10}, Lcom/google/android/gms/internal/ads/zzak;->zzN(I)Lcom/google/android/gms/internal/ads/zzak;

    goto/16 :goto_16

    .line 43
    :cond_b
    invoke-static {v4}, Lcom/google/android/gms/internal/ads/zzcc;->zzg(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_19

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzp:I

    if-nez v6, :cond_e

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzn:I

    if-ne v6, v13, :cond_c

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzl:I

    :cond_c
    iput v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzn:I

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzo:I

    if-ne v6, v13, :cond_d

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzm:I

    :cond_d
    iput v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzo:I

    :cond_e
    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzn:I

    const/high16 v8, -0x40800000    # -1.0f

    if-eq v6, v13, :cond_f

    iget v9, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzo:I

    if-eq v9, v13, :cond_f

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzm:I

    mul-int v10, v10, v6

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzl:I

    mul-int v6, v6, v9

    int-to-float v9, v10

    int-to-float v6, v6

    div-float/2addr v9, v6

    goto :goto_f

    :cond_f
    const/high16 v9, -0x40800000    # -1.0f

    :goto_f
    iget-boolean v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzw:Z

    if-eqz v6, :cond_12

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzC:F

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_11

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzD:F

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_11

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzE:F

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_11

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzF:F

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_11

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzG:F

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_11

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzH:F

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_11

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzI:F

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_11

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzJ:F

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_11

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzK:F

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_11

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzL:F

    cmpl-float v6, v6, v8

    if-nez v6, :cond_10

    goto/16 :goto_10

    :cond_10
    const/16 v6, 0x19

    new-array v6, v6, [B

    .line 44
    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v10, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 45
    invoke-virtual {v8, v11}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzC:F

    const v12, 0x47435000    # 50000.0f

    mul-float v10, v10, v12

    const/high16 v14, 0x3f000000    # 0.5f

    add-float/2addr v10, v14

    float-to-int v10, v10

    int-to-short v10, v10

    .line 46
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzD:F

    mul-float v10, v10, v12

    add-float/2addr v10, v14

    float-to-int v10, v10

    int-to-short v10, v10

    .line 47
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzE:F

    mul-float v10, v10, v12

    add-float/2addr v10, v14

    float-to-int v10, v10

    int-to-short v10, v10

    .line 48
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzF:F

    mul-float v10, v10, v12

    add-float/2addr v10, v14

    float-to-int v10, v10

    int-to-short v10, v10

    .line 49
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzG:F

    mul-float v10, v10, v12

    add-float/2addr v10, v14

    float-to-int v10, v10

    int-to-short v10, v10

    .line 50
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzH:F

    mul-float v10, v10, v12

    add-float/2addr v10, v14

    float-to-int v10, v10

    int-to-short v10, v10

    .line 51
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzI:F

    mul-float v10, v10, v12

    add-float/2addr v10, v14

    float-to-int v10, v10

    int-to-short v10, v10

    .line 52
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzJ:F

    mul-float v10, v10, v12

    add-float/2addr v10, v14

    float-to-int v10, v10

    int-to-short v10, v10

    .line 53
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzK:F

    add-float/2addr v10, v14

    float-to-int v10, v10

    int-to-short v10, v10

    .line 54
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzL:F

    add-float/2addr v10, v14

    float-to-int v10, v10

    int-to-short v10, v10

    .line 55
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzA:I

    int-to-short v10, v10

    .line 56
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzB:I

    int-to-short v10, v10

    .line 57
    invoke-virtual {v8, v10}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    goto :goto_11

    :cond_11
    :goto_10
    const/4 v6, 0x0

    .line 58
    :goto_11
    new-instance v8, Lcom/google/android/gms/internal/ads/zzs;

    iget v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzx:I

    iget v12, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzz:I

    iget v14, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzy:I

    invoke-direct {v8, v10, v12, v14, v6}, Lcom/google/android/gms/internal/ads/zzs;-><init>(III[B)V

    goto :goto_12

    :cond_12
    const/4 v8, 0x0

    :goto_12
    iget-object v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zza:Ljava/lang/String;

    if-eqz v6, :cond_13

    .line 59
    invoke-static {}, Lcom/google/android/gms/internal/ads/zzafu;->zze()Ljava/util/Map;

    move-result-object v6

    iget-object v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zza:Ljava/lang/String;

    invoke-interface {v6, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    invoke-static {}, Lcom/google/android/gms/internal/ads/zzafu;->zze()Ljava/util/Map;

    move-result-object v6

    iget-object v10, v0, Lcom/google/android/gms/internal/ads/zzaft;->zza:Ljava/lang/String;

    .line 60
    invoke-interface {v6, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v13

    :cond_13
    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzq:I

    if-nez v6, :cond_18

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzr:F

    const/4 v10, 0x0

    .line 61
    invoke-static {v6, v10}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-nez v6, :cond_18

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzs:F

    .line 62
    invoke-static {v6, v10}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-nez v6, :cond_18

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzt:F

    .line 63
    invoke-static {v6, v10}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-nez v6, :cond_14

    goto :goto_14

    .line 64
    :cond_14
    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzt:F

    const/high16 v10, 0x42b40000    # 90.0f

    .line 65
    invoke-static {v6, v10}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-nez v6, :cond_15

    const/16 v11, 0x5a

    goto :goto_14

    :cond_15
    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzt:F

    const/high16 v10, -0x3ccc0000    # -180.0f

    .line 66
    invoke-static {v6, v10}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-eqz v6, :cond_17

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzt:F

    const/high16 v10, 0x43340000    # 180.0f

    .line 67
    invoke-static {v6, v10}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-nez v6, :cond_16

    goto :goto_13

    :cond_16
    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzt:F

    const/high16 v10, -0x3d4c0000    # -90.0f

    .line 68
    invoke-static {v6, v10}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-nez v6, :cond_18

    const/16 v11, 0x10e

    goto :goto_14

    :cond_17
    :goto_13
    const/16 v11, 0xb4

    goto :goto_14

    :cond_18
    move v11, v13

    .line 69
    :goto_14
    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzl:I

    .line 70
    invoke-virtual {v7, v6}, Lcom/google/android/gms/internal/ads/zzak;->zzX(I)Lcom/google/android/gms/internal/ads/zzak;

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzm:I

    .line 71
    invoke-virtual {v7, v6}, Lcom/google/android/gms/internal/ads/zzak;->zzF(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 72
    invoke-virtual {v7, v9}, Lcom/google/android/gms/internal/ads/zzak;->zzP(F)Lcom/google/android/gms/internal/ads/zzak;

    .line 73
    invoke-virtual {v7, v11}, Lcom/google/android/gms/internal/ads/zzak;->zzR(I)Lcom/google/android/gms/internal/ads/zzak;

    iget-object v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzu:[B

    .line 74
    invoke-virtual {v7, v6}, Lcom/google/android/gms/internal/ads/zzak;->zzQ([B)Lcom/google/android/gms/internal/ads/zzak;

    iget v6, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzv:I

    .line 75
    invoke-virtual {v7, v6}, Lcom/google/android/gms/internal/ads/zzak;->zzV(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 76
    invoke-virtual {v7, v8}, Lcom/google/android/gms/internal/ads/zzak;->zzy(Lcom/google/android/gms/internal/ads/zzs;)Lcom/google/android/gms/internal/ads/zzak;

    const/4 v6, 0x2

    goto :goto_16

    .line 77
    :cond_19
    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1b

    .line 78
    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1b

    const-string v6, "text/vtt"

    .line 79
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1b

    const-string v6, "application/vobsub"

    .line 80
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1b

    const-string v6, "application/pgs"

    .line 81
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1b

    const-string v6, "application/dvbsubs"

    .line 82
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    goto :goto_15

    :cond_1a
    const-string v1, "Unexpected MIME type."

    const/4 v2, 0x0

    .line 83
    invoke-static {v1, v2}, Lcom/google/android/gms/internal/ads/zzcd;->zza(Ljava/lang/String;Ljava/lang/Throwable;)Lcom/google/android/gms/internal/ads/zzcd;

    move-result-object v1

    throw v1

    :cond_1b
    :goto_15
    const/4 v6, 0x3

    .line 84
    :goto_16
    iget-object v8, v0, Lcom/google/android/gms/internal/ads/zzaft;->zza:Ljava/lang/String;

    if-eqz v8, :cond_1c

    .line 85
    invoke-static {}, Lcom/google/android/gms/internal/ads/zzafu;->zze()Ljava/util/Map;

    move-result-object v8

    iget-object v9, v0, Lcom/google/android/gms/internal/ads/zzaft;->zza:Ljava/lang/String;

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1c

    iget-object v8, v0, Lcom/google/android/gms/internal/ads/zzaft;->zza:Ljava/lang/String;

    .line 86
    invoke-virtual {v7, v8}, Lcom/google/android/gms/internal/ads/zzak;->zzJ(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    :cond_1c
    move/from16 v8, p2

    .line 87
    invoke-virtual {v7, v8}, Lcom/google/android/gms/internal/ads/zzak;->zzG(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 88
    invoke-virtual {v7, v4}, Lcom/google/android/gms/internal/ads/zzak;->zzS(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    .line 89
    invoke-virtual {v7, v1}, Lcom/google/android/gms/internal/ads/zzak;->zzL(I)Lcom/google/android/gms/internal/ads/zzak;

    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzY:Ljava/lang/String;

    .line 90
    invoke-virtual {v7, v1}, Lcom/google/android/gms/internal/ads/zzak;->zzK(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    .line 91
    invoke-virtual {v7, v5}, Lcom/google/android/gms/internal/ads/zzak;->zzU(I)Lcom/google/android/gms/internal/ads/zzak;

    .line 92
    invoke-virtual {v7, v2}, Lcom/google/android/gms/internal/ads/zzak;->zzI(Ljava/util/List;)Lcom/google/android/gms/internal/ads/zzak;

    .line 93
    invoke-virtual {v7, v3}, Lcom/google/android/gms/internal/ads/zzak;->zzx(Ljava/lang/String;)Lcom/google/android/gms/internal/ads/zzak;

    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzk:Lcom/google/android/gms/internal/ads/zzad;

    .line 94
    invoke-virtual {v7, v1}, Lcom/google/android/gms/internal/ads/zzak;->zzB(Lcom/google/android/gms/internal/ads/zzad;)Lcom/google/android/gms/internal/ads/zzak;

    .line 95
    invoke-virtual {v7}, Lcom/google/android/gms/internal/ads/zzak;->zzY()Lcom/google/android/gms/internal/ads/zzam;

    move-result-object v1

    iget v2, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzc:I

    move-object/from16 v3, p1

    .line 96
    invoke-interface {v3, v2, v6}, Lcom/google/android/gms/internal/ads/zzabe;->zzv(II)Lcom/google/android/gms/internal/ads/zzace;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/internal/ads/zzaft;->zzV:Lcom/google/android/gms/internal/ads/zzace;

    .line 97
    invoke-interface {v2, v1}, Lcom/google/android/gms/internal/ads/zzace;->zzk(Lcom/google/android/gms/internal/ads/zzam;)V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ce7f5de -> :sswitch_20
        -0x7ce7f3b0 -> :sswitch_1f
        -0x76567dc0 -> :sswitch_1e
        -0x6a615338 -> :sswitch_1d
        -0x672350af -> :sswitch_1c
        -0x585f4fce -> :sswitch_1b
        -0x585f4fcd -> :sswitch_1a
        -0x51dc40b2 -> :sswitch_19
        -0x37a9c464 -> :sswitch_18
        -0x2016c535 -> :sswitch_17
        -0x2016c4e5 -> :sswitch_16
        -0x19552dbd -> :sswitch_15
        -0x1538b2ba -> :sswitch_14
        0x3c02325 -> :sswitch_13
        0x3c02353 -> :sswitch_12
        0x3c030c5 -> :sswitch_11
        0x4e81333 -> :sswitch_10
        0x4e86155 -> :sswitch_f
        0x4e86156 -> :sswitch_e
        0x5e8da3e -> :sswitch_d
        0x1a8350d6 -> :sswitch_c
        0x2056f406 -> :sswitch_b
        0x25e26ee2 -> :sswitch_a
        0x2b45174d -> :sswitch_9
        0x2b453ce4 -> :sswitch_8
        0x2c0618eb -> :sswitch_7
        0x32fdf009 -> :sswitch_6
        0x3e4ca2d8 -> :sswitch_5
        0x54c61e47 -> :sswitch_4
        0x6bd6c624 -> :sswitch_3
        0x7446132a -> :sswitch_2
        0x7446b0a6 -> :sswitch_1
        0x744ad97d -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

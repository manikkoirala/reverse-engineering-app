.class final Lcom/google/android/gms/internal/ads/zzajl;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-ads@@22.4.0"


# instance fields
.field private final zza:Lcom/google/android/gms/internal/ads/zzaio;

.field private final zzb:Lcom/google/android/gms/internal/ads/zzfi;

.field private final zzc:Lcom/google/android/gms/internal/ads/zzfa;

.field private zzd:Z

.field private zze:Z

.field private zzf:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/ads/zzaio;Lcom/google/android/gms/internal/ads/zzfi;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzajl;->zza:Lcom/google/android/gms/internal/ads/zzaio;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/google/android/gms/internal/ads/zzajl;->zzb:Lcom/google/android/gms/internal/ads/zzfi;

    .line 7
    .line 8
    new-instance p1, Lcom/google/android/gms/internal/ads/zzfa;

    .line 9
    .line 10
    const/16 p2, 0x40

    .line 11
    .line 12
    new-array v0, p2, [B

    .line 13
    .line 14
    invoke-direct {p1, v0, p2}, Lcom/google/android/gms/internal/ads/zzfa;-><init>([BI)V

    .line 15
    .line 16
    .line 17
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
.end method


# virtual methods
.method public final zza(Lcom/google/android/gms/internal/ads/zzfb;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/ads/zzcd;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 6
    .line 7
    iget-object v2, v2, Lcom/google/android/gms/internal/ads/zzfa;->zza:[B

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    const/4 v4, 0x3

    .line 11
    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/internal/ads/zzfb;->zzC([BII)V

    .line 12
    .line 13
    .line 14
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 15
    .line 16
    invoke-virtual {v2, v3}, Lcom/google/android/gms/internal/ads/zzfa;->zzj(I)V

    .line 17
    .line 18
    .line 19
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 20
    .line 21
    const/16 v5, 0x8

    .line 22
    .line 23
    invoke-virtual {v2, v5}, Lcom/google/android/gms/internal/ads/zzfa;->zzl(I)V

    .line 24
    .line 25
    .line 26
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 27
    .line 28
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzfa;->zzn()Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    iput-boolean v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzd:Z

    .line 33
    .line 34
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 35
    .line 36
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ads/zzfa;->zzn()Z

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    iput-boolean v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zze:Z

    .line 41
    .line 42
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 43
    .line 44
    const/4 v6, 0x6

    .line 45
    invoke-virtual {v2, v6}, Lcom/google/android/gms/internal/ads/zzfa;->zzl(I)V

    .line 46
    .line 47
    .line 48
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 49
    .line 50
    invoke-virtual {v2, v5}, Lcom/google/android/gms/internal/ads/zzfa;->zzd(I)I

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    iget-object v5, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 55
    .line 56
    iget-object v5, v5, Lcom/google/android/gms/internal/ads/zzfa;->zza:[B

    .line 57
    .line 58
    invoke-virtual {v1, v5, v3, v2}, Lcom/google/android/gms/internal/ads/zzfb;->zzC([BII)V

    .line 59
    .line 60
    .line 61
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 62
    .line 63
    invoke-virtual {v2, v3}, Lcom/google/android/gms/internal/ads/zzfa;->zzj(I)V

    .line 64
    .line 65
    .line 66
    iget-boolean v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzd:Z

    .line 67
    .line 68
    const/4 v5, 0x4

    .line 69
    if-eqz v2, :cond_1

    .line 70
    .line 71
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 72
    .line 73
    invoke-virtual {v2, v5}, Lcom/google/android/gms/internal/ads/zzfa;->zzl(I)V

    .line 74
    .line 75
    .line 76
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 77
    .line 78
    invoke-virtual {v2, v4}, Lcom/google/android/gms/internal/ads/zzfa;->zzd(I)I

    .line 79
    .line 80
    .line 81
    move-result v2

    .line 82
    int-to-long v6, v2

    .line 83
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 84
    .line 85
    const/4 v8, 0x1

    .line 86
    invoke-virtual {v2, v8}, Lcom/google/android/gms/internal/ads/zzfa;->zzl(I)V

    .line 87
    .line 88
    .line 89
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 90
    .line 91
    const/16 v9, 0xf

    .line 92
    .line 93
    invoke-virtual {v2, v9}, Lcom/google/android/gms/internal/ads/zzfa;->zzd(I)I

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    shl-int/2addr v2, v9

    .line 98
    iget-object v10, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 99
    .line 100
    invoke-virtual {v10, v8}, Lcom/google/android/gms/internal/ads/zzfa;->zzl(I)V

    .line 101
    .line 102
    .line 103
    iget-object v10, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 104
    .line 105
    invoke-virtual {v10, v9}, Lcom/google/android/gms/internal/ads/zzfa;->zzd(I)I

    .line 106
    .line 107
    .line 108
    move-result v10

    .line 109
    int-to-long v10, v10

    .line 110
    iget-object v12, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 111
    .line 112
    invoke-virtual {v12, v8}, Lcom/google/android/gms/internal/ads/zzfa;->zzl(I)V

    .line 113
    .line 114
    .line 115
    iget-boolean v12, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzf:Z

    .line 116
    .line 117
    const/16 v13, 0x1e

    .line 118
    .line 119
    if-nez v12, :cond_0

    .line 120
    .line 121
    iget-boolean v12, v0, Lcom/google/android/gms/internal/ads/zzajl;->zze:Z

    .line 122
    .line 123
    if-eqz v12, :cond_0

    .line 124
    .line 125
    iget-object v12, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 126
    .line 127
    invoke-virtual {v12, v5}, Lcom/google/android/gms/internal/ads/zzfa;->zzl(I)V

    .line 128
    .line 129
    .line 130
    iget-object v12, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 131
    .line 132
    invoke-virtual {v12, v4}, Lcom/google/android/gms/internal/ads/zzfa;->zzd(I)I

    .line 133
    .line 134
    .line 135
    move-result v4

    .line 136
    int-to-long v14, v4

    .line 137
    shl-long/2addr v14, v13

    .line 138
    iget-object v4, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 139
    .line 140
    invoke-virtual {v4, v8}, Lcom/google/android/gms/internal/ads/zzfa;->zzl(I)V

    .line 141
    .line 142
    .line 143
    iget-object v4, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 144
    .line 145
    invoke-virtual {v4, v9}, Lcom/google/android/gms/internal/ads/zzfa;->zzd(I)I

    .line 146
    .line 147
    .line 148
    move-result v4

    .line 149
    shl-int/2addr v4, v9

    .line 150
    iget-object v12, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 151
    .line 152
    invoke-virtual {v12, v8}, Lcom/google/android/gms/internal/ads/zzfa;->zzl(I)V

    .line 153
    .line 154
    .line 155
    iget-object v12, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 156
    .line 157
    invoke-virtual {v12, v9}, Lcom/google/android/gms/internal/ads/zzfa;->zzd(I)I

    .line 158
    .line 159
    .line 160
    move-result v9

    .line 161
    move-wide/from16 v16, v6

    .line 162
    .line 163
    int-to-long v5, v9

    .line 164
    iget-object v7, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzc:Lcom/google/android/gms/internal/ads/zzfa;

    .line 165
    .line 166
    invoke-virtual {v7, v8}, Lcom/google/android/gms/internal/ads/zzfa;->zzl(I)V

    .line 167
    .line 168
    .line 169
    iget-object v7, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzb:Lcom/google/android/gms/internal/ads/zzfi;

    .line 170
    .line 171
    int-to-long v3, v4

    .line 172
    or-long/2addr v3, v14

    .line 173
    or-long/2addr v3, v5

    .line 174
    invoke-virtual {v7, v3, v4}, Lcom/google/android/gms/internal/ads/zzfi;->zzb(J)J

    .line 175
    .line 176
    .line 177
    iput-boolean v8, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzf:Z

    .line 178
    .line 179
    goto :goto_0

    .line 180
    :cond_0
    move-wide/from16 v16, v6

    .line 181
    .line 182
    :goto_0
    shl-long v3, v16, v13

    .line 183
    .line 184
    int-to-long v5, v2

    .line 185
    or-long v2, v3, v5

    .line 186
    .line 187
    or-long/2addr v2, v10

    .line 188
    iget-object v4, v0, Lcom/google/android/gms/internal/ads/zzajl;->zzb:Lcom/google/android/gms/internal/ads/zzfi;

    .line 189
    .line 190
    invoke-virtual {v4, v2, v3}, Lcom/google/android/gms/internal/ads/zzfi;->zzb(J)J

    .line 191
    .line 192
    .line 193
    move-result-wide v2

    .line 194
    goto :goto_1

    .line 195
    :cond_1
    const-wide/16 v2, 0x0

    .line 196
    .line 197
    :goto_1
    iget-object v4, v0, Lcom/google/android/gms/internal/ads/zzajl;->zza:Lcom/google/android/gms/internal/ads/zzaio;

    .line 198
    .line 199
    const/4 v5, 0x4

    .line 200
    invoke-interface {v4, v2, v3, v5}, Lcom/google/android/gms/internal/ads/zzaio;->zzd(JI)V

    .line 201
    .line 202
    .line 203
    iget-object v2, v0, Lcom/google/android/gms/internal/ads/zzajl;->zza:Lcom/google/android/gms/internal/ads/zzaio;

    .line 204
    .line 205
    invoke-interface {v2, v1}, Lcom/google/android/gms/internal/ads/zzaio;->zza(Lcom/google/android/gms/internal/ads/zzfb;)V

    .line 206
    .line 207
    .line 208
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzajl;->zza:Lcom/google/android/gms/internal/ads/zzaio;

    .line 209
    .line 210
    const/4 v2, 0x0

    .line 211
    invoke-interface {v1, v2}, Lcom/google/android/gms/internal/ads/zzaio;->zzc(Z)V

    .line 212
    .line 213
    .line 214
    return-void
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method public final zzb()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzajl;->zzf:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzajl;->zza:Lcom/google/android/gms/internal/ads/zzaio;

    .line 5
    .line 6
    invoke-interface {v0}, Lcom/google/android/gms/internal/ads/zzaio;->zze()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

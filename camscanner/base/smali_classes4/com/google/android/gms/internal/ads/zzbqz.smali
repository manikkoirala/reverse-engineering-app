.class public final Lcom/google/android/gms/internal/ads/zzbqz;
.super Lcom/google/android/gms/internal/ads/zzbrf;
.source "com.google.android.gms:play-services-ads@@22.4.0"


# static fields
.field static final zza:Ljava/util/Set;


# instance fields
.field private zzb:Ljava/lang/String;

.field private zzc:Z

.field private zzd:I

.field private zze:I

.field private zzf:I

.field private zzg:I

.field private zzh:I

.field private zzi:I

.field private final zzj:Ljava/lang/Object;

.field private final zzk:Lcom/google/android/gms/internal/ads/zzcfi;

.field private final zzl:Landroid/app/Activity;

.field private zzm:Lcom/google/android/gms/internal/ads/zzcgx;

.field private zzn:Landroid/widget/ImageView;

.field private zzo:Landroid/widget/LinearLayout;

.field private final zzp:Lcom/google/android/gms/internal/ads/zzbrg;

.field private zzq:Landroid/widget/PopupWindow;

.field private zzr:Landroid/widget/RelativeLayout;

.field private zzs:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    const-string v0, "top-left"

    .line 2
    .line 3
    const-string v1, "top-right"

    .line 4
    .line 5
    const-string v2, "top-center"

    .line 6
    .line 7
    const-string v3, "center"

    .line 8
    .line 9
    const-string v4, "bottom-left"

    .line 10
    .line 11
    const-string v5, "bottom-right"

    .line 12
    .line 13
    const-string v6, "bottom-center"

    .line 14
    .line 15
    filled-new-array/range {v0 .. v6}, [Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {v0}, Lcom/google/android/gms/common/util/CollectionUtils;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    sput-object v0, Lcom/google/android/gms/internal/ads/zzbqz;->zza:Ljava/util/Set;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/ads/zzcfi;Lcom/google/android/gms/internal/ads/zzbrg;)V
    .locals 2

    .line 1
    const-string v0, "resize"

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/ads/zzbrf;-><init>(Lcom/google/android/gms/internal/ads/zzcfi;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "top-right"

    .line 7
    .line 8
    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzb:Ljava/lang/String;

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzc:Z

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 15
    .line 16
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 17
    .line 18
    const/4 v1, -0x1

    .line 19
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzf:I

    .line 20
    .line 21
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzg:I

    .line 22
    .line 23
    iput v0, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzh:I

    .line 24
    .line 25
    iput v1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzi:I

    .line 26
    .line 27
    new-instance v0, Ljava/lang/Object;

    .line 28
    .line 29
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 30
    .line 31
    .line 32
    iput-object v0, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzj:Ljava/lang/Object;

    .line 33
    .line 34
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 35
    .line 36
    invoke-interface {p1}, Lcom/google/android/gms/internal/ads/zzcfi;->zzi()Landroid/app/Activity;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 41
    .line 42
    iput-object p2, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzp:Lcom/google/android/gms/internal/ads/zzbrg;

    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
.end method


# virtual methods
.method public final zza(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzj:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzq:Landroid/widget/PopupWindow;

    .line 5
    .line 6
    if-eqz v1, :cond_2

    .line 7
    .line 8
    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzr:Landroid/widget/RelativeLayout;

    .line 12
    .line 13
    iget-object v2, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 14
    .line 15
    check-cast v2, Landroid/view/View;

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 18
    .line 19
    .line 20
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzs:Landroid/view/ViewGroup;

    .line 21
    .line 22
    if-eqz v1, :cond_0

    .line 23
    .line 24
    iget-object v2, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzn:Landroid/widget/ImageView;

    .line 25
    .line 26
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 27
    .line 28
    .line 29
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzs:Landroid/view/ViewGroup;

    .line 30
    .line 31
    iget-object v2, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 32
    .line 33
    check-cast v2, Landroid/view/View;

    .line 34
    .line 35
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 36
    .line 37
    .line 38
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 39
    .line 40
    iget-object v2, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzm:Lcom/google/android/gms/internal/ads/zzcgx;

    .line 41
    .line 42
    invoke-interface {v1, v2}, Lcom/google/android/gms/internal/ads/zzcfi;->zzag(Lcom/google/android/gms/internal/ads/zzcgx;)V

    .line 43
    .line 44
    .line 45
    :cond_0
    if-eqz p1, :cond_1

    .line 46
    .line 47
    const-string p1, "default"

    .line 48
    .line 49
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/ads/zzbrf;->zzk(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    iget-object p1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzp:Lcom/google/android/gms/internal/ads/zzbrg;

    .line 53
    .line 54
    if-eqz p1, :cond_1

    .line 55
    .line 56
    invoke-interface {p1}, Lcom/google/android/gms/internal/ads/zzbrg;->zzb()V

    .line 57
    .line 58
    .line 59
    :cond_1
    const/4 p1, 0x0

    .line 60
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzq:Landroid/widget/PopupWindow;

    .line 61
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzr:Landroid/widget/RelativeLayout;

    .line 63
    .line 64
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzs:Landroid/view/ViewGroup;

    .line 65
    .line 66
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzo:Landroid/widget/LinearLayout;

    .line 67
    .line 68
    :cond_2
    monitor-exit v0

    .line 69
    return-void

    .line 70
    :catchall_0
    move-exception p1

    .line 71
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    throw p1
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method public final zzb(Ljava/util/Map;)V
    .locals 17

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    iget-object v2, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzj:Ljava/lang/Object;

    .line 6
    .line 7
    monitor-enter v2

    .line 8
    :try_start_0
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 9
    .line 10
    if-nez v3, :cond_0

    .line 11
    .line 12
    const-string v0, "Not an activity context. Cannot resize."

    .line 13
    .line 14
    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ads/zzbrf;->zzg(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    monitor-exit v2

    .line 18
    return-void

    .line 19
    :cond_0
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 20
    .line 21
    invoke-interface {v3}, Lcom/google/android/gms/internal/ads/zzcfi;->zzO()Lcom/google/android/gms/internal/ads/zzcgx;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    if-nez v3, :cond_1

    .line 26
    .line 27
    const-string v0, "Webview is not yet available, size is not set."

    .line 28
    .line 29
    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ads/zzbrf;->zzg(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    monitor-exit v2

    .line 33
    return-void

    .line 34
    :cond_1
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 35
    .line 36
    invoke-interface {v3}, Lcom/google/android/gms/internal/ads/zzcfi;->zzO()Lcom/google/android/gms/internal/ads/zzcgx;

    .line 37
    .line 38
    .line 39
    move-result-object v3

    .line 40
    invoke-virtual {v3}, Lcom/google/android/gms/internal/ads/zzcgx;->zzi()Z

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    if-eqz v3, :cond_2

    .line 45
    .line 46
    const-string v0, "Is interstitial. Cannot resize an interstitial."

    .line 47
    .line 48
    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ads/zzbrf;->zzg(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    monitor-exit v2

    .line 52
    return-void

    .line 53
    :cond_2
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 54
    .line 55
    invoke-interface {v3}, Lcom/google/android/gms/internal/ads/zzcfi;->zzaA()Z

    .line 56
    .line 57
    .line 58
    move-result v3

    .line 59
    if-nez v3, :cond_2b

    .line 60
    .line 61
    const-string v3, "width"

    .line 62
    .line 63
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object v3

    .line 67
    check-cast v3, Ljava/lang/CharSequence;

    .line 68
    .line 69
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    if-nez v3, :cond_3

    .line 74
    .line 75
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzt;->zzp()Lcom/google/android/gms/ads/internal/util/zzs;

    .line 76
    .line 77
    .line 78
    const-string v3, "width"

    .line 79
    .line 80
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    .line 82
    .line 83
    move-result-object v3

    .line 84
    check-cast v3, Ljava/lang/String;

    .line 85
    .line 86
    invoke-static {v3}, Lcom/google/android/gms/ads/internal/util/zzs;->zzL(Ljava/lang/String;)I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    iput v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzi:I

    .line 91
    .line 92
    :cond_3
    const-string v3, "height"

    .line 93
    .line 94
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    .line 96
    .line 97
    move-result-object v3

    .line 98
    check-cast v3, Ljava/lang/CharSequence;

    .line 99
    .line 100
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 101
    .line 102
    .line 103
    move-result v3

    .line 104
    if-nez v3, :cond_4

    .line 105
    .line 106
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzt;->zzp()Lcom/google/android/gms/ads/internal/util/zzs;

    .line 107
    .line 108
    .line 109
    const-string v3, "height"

    .line 110
    .line 111
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    .line 113
    .line 114
    move-result-object v3

    .line 115
    check-cast v3, Ljava/lang/String;

    .line 116
    .line 117
    invoke-static {v3}, Lcom/google/android/gms/ads/internal/util/zzs;->zzL(Ljava/lang/String;)I

    .line 118
    .line 119
    .line 120
    move-result v3

    .line 121
    iput v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzf:I

    .line 122
    .line 123
    :cond_4
    const-string v3, "offsetX"

    .line 124
    .line 125
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    .line 127
    .line 128
    move-result-object v3

    .line 129
    check-cast v3, Ljava/lang/CharSequence;

    .line 130
    .line 131
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 132
    .line 133
    .line 134
    move-result v3

    .line 135
    if-nez v3, :cond_5

    .line 136
    .line 137
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzt;->zzp()Lcom/google/android/gms/ads/internal/util/zzs;

    .line 138
    .line 139
    .line 140
    const-string v3, "offsetX"

    .line 141
    .line 142
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    .line 144
    .line 145
    move-result-object v3

    .line 146
    check-cast v3, Ljava/lang/String;

    .line 147
    .line 148
    invoke-static {v3}, Lcom/google/android/gms/ads/internal/util/zzs;->zzL(Ljava/lang/String;)I

    .line 149
    .line 150
    .line 151
    move-result v3

    .line 152
    iput v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzg:I

    .line 153
    .line 154
    :cond_5
    const-string v3, "offsetY"

    .line 155
    .line 156
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    .line 158
    .line 159
    move-result-object v3

    .line 160
    check-cast v3, Ljava/lang/CharSequence;

    .line 161
    .line 162
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 163
    .line 164
    .line 165
    move-result v3

    .line 166
    if-nez v3, :cond_6

    .line 167
    .line 168
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzt;->zzp()Lcom/google/android/gms/ads/internal/util/zzs;

    .line 169
    .line 170
    .line 171
    const-string v3, "offsetY"

    .line 172
    .line 173
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    .line 175
    .line 176
    move-result-object v3

    .line 177
    check-cast v3, Ljava/lang/String;

    .line 178
    .line 179
    invoke-static {v3}, Lcom/google/android/gms/ads/internal/util/zzs;->zzL(Ljava/lang/String;)I

    .line 180
    .line 181
    .line 182
    move-result v3

    .line 183
    iput v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzh:I

    .line 184
    .line 185
    :cond_6
    const-string v3, "allowOffscreen"

    .line 186
    .line 187
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    .line 189
    .line 190
    move-result-object v3

    .line 191
    check-cast v3, Ljava/lang/CharSequence;

    .line 192
    .line 193
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 194
    .line 195
    .line 196
    move-result v3

    .line 197
    if-nez v3, :cond_7

    .line 198
    .line 199
    const-string v3, "allowOffscreen"

    .line 200
    .line 201
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    .line 203
    .line 204
    move-result-object v3

    .line 205
    check-cast v3, Ljava/lang/String;

    .line 206
    .line 207
    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    .line 208
    .line 209
    .line 210
    move-result v3

    .line 211
    iput-boolean v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzc:Z

    .line 212
    .line 213
    :cond_7
    const-string v3, "customClosePosition"

    .line 214
    .line 215
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    .line 217
    .line 218
    move-result-object v0

    .line 219
    check-cast v0, Ljava/lang/String;

    .line 220
    .line 221
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 222
    .line 223
    .line 224
    move-result v3

    .line 225
    if-nez v3, :cond_8

    .line 226
    .line 227
    iput-object v0, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzb:Ljava/lang/String;

    .line 228
    .line 229
    :cond_8
    iget v0, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzi:I

    .line 230
    .line 231
    if-ltz v0, :cond_2a

    .line 232
    .line 233
    iget v0, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzf:I

    .line 234
    .line 235
    if-ltz v0, :cond_2a

    .line 236
    .line 237
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 238
    .line 239
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 240
    .line 241
    .line 242
    move-result-object v0

    .line 243
    if-eqz v0, :cond_29

    .line 244
    .line 245
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 246
    .line 247
    .line 248
    move-result-object v3

    .line 249
    if-nez v3, :cond_9

    .line 250
    .line 251
    goto/16 :goto_11

    .line 252
    .line 253
    :cond_9
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzt;->zzp()Lcom/google/android/gms/ads/internal/util/zzs;

    .line 254
    .line 255
    .line 256
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 257
    .line 258
    invoke-static {v3}, Lcom/google/android/gms/ads/internal/util/zzs;->zzS(Landroid/app/Activity;)[I

    .line 259
    .line 260
    .line 261
    move-result-object v3

    .line 262
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzt;->zzp()Lcom/google/android/gms/ads/internal/util/zzs;

    .line 263
    .line 264
    .line 265
    iget-object v4, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 266
    .line 267
    invoke-static {v4}, Lcom/google/android/gms/ads/internal/util/zzs;->zzO(Landroid/app/Activity;)[I

    .line 268
    .line 269
    .line 270
    move-result-object v4

    .line 271
    const/4 v5, 0x0

    .line 272
    aget v6, v3, v5

    .line 273
    .line 274
    const/4 v7, 0x1

    .line 275
    aget v3, v3, v7

    .line 276
    .line 277
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzi:I

    .line 278
    .line 279
    const/4 v9, 0x5

    .line 280
    const/4 v10, 0x4

    .line 281
    const/4 v11, 0x3

    .line 282
    const/4 v13, 0x2

    .line 283
    const/16 v14, 0x32

    .line 284
    .line 285
    if-lt v8, v14, :cond_1b

    .line 286
    .line 287
    if-le v8, v6, :cond_a

    .line 288
    .line 289
    goto/16 :goto_a

    .line 290
    .line 291
    :cond_a
    iget v15, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzf:I

    .line 292
    .line 293
    if-lt v15, v14, :cond_1a

    .line 294
    .line 295
    if-le v15, v3, :cond_b

    .line 296
    .line 297
    goto/16 :goto_9

    .line 298
    .line 299
    :cond_b
    if-ne v15, v3, :cond_c

    .line 300
    .line 301
    if-ne v8, v6, :cond_c

    .line 302
    .line 303
    const-string v3, "Cannot resize to a full-screen ad."

    .line 304
    .line 305
    invoke-static {v3}, Lcom/google/android/gms/internal/ads/zzcaa;->zzj(Ljava/lang/String;)V

    .line 306
    .line 307
    .line 308
    goto/16 :goto_b

    .line 309
    .line 310
    :cond_c
    iget-boolean v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzc:Z

    .line 311
    .line 312
    if-eqz v3, :cond_15

    .line 313
    .line 314
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzb:Ljava/lang/String;

    .line 315
    .line 316
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    .line 317
    .line 318
    .line 319
    move-result v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    sparse-switch v16, :sswitch_data_0

    .line 321
    .line 322
    .line 323
    goto :goto_0

    .line 324
    :sswitch_0
    const-string v12, "top-center"

    .line 325
    .line 326
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 327
    .line 328
    .line 329
    move-result v3

    .line 330
    if-eqz v3, :cond_d

    .line 331
    .line 332
    const/4 v3, 0x1

    .line 333
    goto :goto_1

    .line 334
    :sswitch_1
    const-string v12, "bottom-center"

    .line 335
    .line 336
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 337
    .line 338
    .line 339
    move-result v3

    .line 340
    if-eqz v3, :cond_d

    .line 341
    .line 342
    const/4 v3, 0x4

    .line 343
    goto :goto_1

    .line 344
    :sswitch_2
    const-string v12, "bottom-right"

    .line 345
    .line 346
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 347
    .line 348
    .line 349
    move-result v3

    .line 350
    if-eqz v3, :cond_d

    .line 351
    .line 352
    const/4 v3, 0x5

    .line 353
    goto :goto_1

    .line 354
    :sswitch_3
    const-string v12, "bottom-left"

    .line 355
    .line 356
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 357
    .line 358
    .line 359
    move-result v3

    .line 360
    if-eqz v3, :cond_d

    .line 361
    .line 362
    const/4 v3, 0x3

    .line 363
    goto :goto_1

    .line 364
    :sswitch_4
    const-string v12, "top-left"

    .line 365
    .line 366
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 367
    .line 368
    .line 369
    move-result v3

    .line 370
    if-eqz v3, :cond_d

    .line 371
    .line 372
    const/4 v3, 0x0

    .line 373
    goto :goto_1

    .line 374
    :sswitch_5
    const-string v12, "center"

    .line 375
    .line 376
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 377
    .line 378
    .line 379
    move-result v3

    .line 380
    if-eqz v3, :cond_d

    .line 381
    .line 382
    const/4 v3, 0x2

    .line 383
    goto :goto_1

    .line 384
    :cond_d
    :goto_0
    const/4 v3, -0x1

    .line 385
    :goto_1
    if-eqz v3, :cond_13

    .line 386
    .line 387
    if-eq v3, v7, :cond_12

    .line 388
    .line 389
    if-eq v3, v13, :cond_11

    .line 390
    .line 391
    if-eq v3, v11, :cond_10

    .line 392
    .line 393
    if-eq v3, v10, :cond_f

    .line 394
    .line 395
    if-eq v3, v9, :cond_e

    .line 396
    .line 397
    :try_start_1
    iget v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 398
    .line 399
    iget v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzg:I

    .line 400
    .line 401
    add-int/2addr v3, v12

    .line 402
    add-int/2addr v3, v8

    .line 403
    add-int/lit8 v3, v3, -0x32

    .line 404
    .line 405
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 406
    .line 407
    goto :goto_4

    .line 408
    :cond_e
    iget v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 409
    .line 410
    iget v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzg:I

    .line 411
    .line 412
    add-int/2addr v3, v12

    .line 413
    add-int/2addr v3, v8

    .line 414
    add-int/lit8 v3, v3, -0x32

    .line 415
    .line 416
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 417
    .line 418
    goto :goto_2

    .line 419
    :cond_f
    iget v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 420
    .line 421
    iget v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzg:I

    .line 422
    .line 423
    add-int/2addr v3, v12

    .line 424
    shr-int/2addr v8, v7

    .line 425
    add-int/2addr v3, v8

    .line 426
    add-int/lit8 v3, v3, -0x19

    .line 427
    .line 428
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 429
    .line 430
    :goto_2
    iget v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzh:I

    .line 431
    .line 432
    goto :goto_3

    .line 433
    :cond_10
    iget v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 434
    .line 435
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzg:I

    .line 436
    .line 437
    add-int/2addr v3, v8

    .line 438
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 439
    .line 440
    goto :goto_2

    .line 441
    :goto_3
    add-int/2addr v8, v12

    .line 442
    add-int/2addr v8, v15

    .line 443
    add-int/lit8 v8, v8, -0x32

    .line 444
    .line 445
    goto :goto_6

    .line 446
    :cond_11
    iget v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 447
    .line 448
    iget v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzg:I

    .line 449
    .line 450
    add-int/2addr v3, v12

    .line 451
    shr-int/2addr v8, v7

    .line 452
    add-int/2addr v3, v8

    .line 453
    add-int/lit8 v3, v3, -0x19

    .line 454
    .line 455
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 456
    .line 457
    iget v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzh:I

    .line 458
    .line 459
    add-int/2addr v8, v12

    .line 460
    shr-int/lit8 v12, v15, 0x1

    .line 461
    .line 462
    add-int/2addr v8, v12

    .line 463
    add-int/lit8 v8, v8, -0x19

    .line 464
    .line 465
    goto :goto_6

    .line 466
    :cond_12
    iget v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 467
    .line 468
    iget v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzg:I

    .line 469
    .line 470
    add-int/2addr v3, v12

    .line 471
    shr-int/2addr v8, v7

    .line 472
    add-int/2addr v3, v8

    .line 473
    add-int/lit8 v3, v3, -0x19

    .line 474
    .line 475
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 476
    .line 477
    :goto_4
    iget v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzh:I

    .line 478
    .line 479
    goto :goto_5

    .line 480
    :cond_13
    iget v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 481
    .line 482
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzg:I

    .line 483
    .line 484
    add-int/2addr v3, v8

    .line 485
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 486
    .line 487
    goto :goto_4

    .line 488
    :goto_5
    add-int/2addr v8, v12

    .line 489
    :goto_6
    if-ltz v3, :cond_1c

    .line 490
    .line 491
    add-int/2addr v3, v14

    .line 492
    if-gt v3, v6, :cond_1c

    .line 493
    .line 494
    aget v3, v4, v5

    .line 495
    .line 496
    if-lt v8, v3, :cond_1c

    .line 497
    .line 498
    add-int/2addr v8, v14

    .line 499
    aget v3, v4, v7

    .line 500
    .line 501
    if-le v8, v3, :cond_14

    .line 502
    .line 503
    goto :goto_b

    .line 504
    :cond_14
    new-array v15, v13, [I

    .line 505
    .line 506
    iget v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 507
    .line 508
    iget v4, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzg:I

    .line 509
    .line 510
    add-int/2addr v3, v4

    .line 511
    aput v3, v15, v5

    .line 512
    .line 513
    iget v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 514
    .line 515
    iget v4, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzh:I

    .line 516
    .line 517
    add-int/2addr v3, v4

    .line 518
    aput v3, v15, v7

    .line 519
    .line 520
    goto :goto_c

    .line 521
    :cond_15
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzt;->zzp()Lcom/google/android/gms/ads/internal/util/zzs;

    .line 522
    .line 523
    .line 524
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 525
    .line 526
    invoke-static {v3}, Lcom/google/android/gms/ads/internal/util/zzs;->zzS(Landroid/app/Activity;)[I

    .line 527
    .line 528
    .line 529
    move-result-object v3

    .line 530
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzt;->zzp()Lcom/google/android/gms/ads/internal/util/zzs;

    .line 531
    .line 532
    .line 533
    iget-object v4, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 534
    .line 535
    invoke-static {v4}, Lcom/google/android/gms/ads/internal/util/zzs;->zzO(Landroid/app/Activity;)[I

    .line 536
    .line 537
    .line 538
    move-result-object v4

    .line 539
    aget v3, v3, v5

    .line 540
    .line 541
    iget v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 542
    .line 543
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzg:I

    .line 544
    .line 545
    add-int/2addr v6, v8

    .line 546
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 547
    .line 548
    iget v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzh:I

    .line 549
    .line 550
    add-int/2addr v8, v12

    .line 551
    if-gez v6, :cond_16

    .line 552
    .line 553
    const/4 v6, 0x0

    .line 554
    goto :goto_7

    .line 555
    :cond_16
    iget v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzi:I

    .line 556
    .line 557
    add-int v15, v6, v12

    .line 558
    .line 559
    if-le v15, v3, :cond_17

    .line 560
    .line 561
    sub-int v6, v3, v12

    .line 562
    .line 563
    :cond_17
    :goto_7
    aget v3, v4, v5

    .line 564
    .line 565
    if-ge v8, v3, :cond_18

    .line 566
    .line 567
    move v8, v3

    .line 568
    goto :goto_8

    .line 569
    :cond_18
    iget v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzf:I

    .line 570
    .line 571
    add-int v12, v8, v3

    .line 572
    .line 573
    aget v4, v4, v7

    .line 574
    .line 575
    if-le v12, v4, :cond_19

    .line 576
    .line 577
    sub-int v8, v4, v3

    .line 578
    .line 579
    :cond_19
    :goto_8
    new-array v15, v13, [I

    .line 580
    .line 581
    aput v6, v15, v5

    .line 582
    .line 583
    aput v8, v15, v7

    .line 584
    .line 585
    goto :goto_c

    .line 586
    :cond_1a
    :goto_9
    const-string v3, "Height is too small or too large."

    .line 587
    .line 588
    invoke-static {v3}, Lcom/google/android/gms/internal/ads/zzcaa;->zzj(Ljava/lang/String;)V

    .line 589
    .line 590
    .line 591
    goto :goto_b

    .line 592
    :cond_1b
    :goto_a
    const-string v3, "Width is too small or too large."

    .line 593
    .line 594
    invoke-static {v3}, Lcom/google/android/gms/internal/ads/zzcaa;->zzj(Ljava/lang/String;)V

    .line 595
    .line 596
    .line 597
    :cond_1c
    :goto_b
    const/4 v15, 0x0

    .line 598
    :goto_c
    if-nez v15, :cond_1d

    .line 599
    .line 600
    const-string v0, "Resize location out of screen or close button is not visible."

    .line 601
    .line 602
    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ads/zzbrf;->zzg(Ljava/lang/String;)V

    .line 603
    .line 604
    .line 605
    monitor-exit v2

    .line 606
    return-void

    .line 607
    :cond_1d
    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzay;->zzb()Lcom/google/android/gms/internal/ads/zzbzt;

    .line 608
    .line 609
    .line 610
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 611
    .line 612
    iget v4, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzi:I

    .line 613
    .line 614
    invoke-static {v3, v4}, Lcom/google/android/gms/internal/ads/zzbzt;->zzx(Landroid/content/Context;I)I

    .line 615
    .line 616
    .line 617
    move-result v3

    .line 618
    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzay;->zzb()Lcom/google/android/gms/internal/ads/zzbzt;

    .line 619
    .line 620
    .line 621
    iget-object v4, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 622
    .line 623
    iget v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzf:I

    .line 624
    .line 625
    invoke-static {v4, v6}, Lcom/google/android/gms/internal/ads/zzbzt;->zzx(Landroid/content/Context;I)I

    .line 626
    .line 627
    .line 628
    move-result v4

    .line 629
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 630
    .line 631
    check-cast v6, Landroid/view/View;

    .line 632
    .line 633
    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 634
    .line 635
    .line 636
    move-result-object v6

    .line 637
    if-eqz v6, :cond_28

    .line 638
    .line 639
    instance-of v8, v6, Landroid/view/ViewGroup;

    .line 640
    .line 641
    if-eqz v8, :cond_28

    .line 642
    .line 643
    check-cast v6, Landroid/view/ViewGroup;

    .line 644
    .line 645
    iget-object v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 646
    .line 647
    check-cast v8, Landroid/view/View;

    .line 648
    .line 649
    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 650
    .line 651
    .line 652
    iget-object v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzq:Landroid/widget/PopupWindow;

    .line 653
    .line 654
    if-nez v8, :cond_1e

    .line 655
    .line 656
    iput-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzs:Landroid/view/ViewGroup;

    .line 657
    .line 658
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzt;->zzp()Lcom/google/android/gms/ads/internal/util/zzs;

    .line 659
    .line 660
    .line 661
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 662
    .line 663
    move-object v8, v6

    .line 664
    check-cast v8, Landroid/view/View;

    .line 665
    .line 666
    invoke-virtual {v8, v7}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 667
    .line 668
    .line 669
    move-object v8, v6

    .line 670
    check-cast v8, Landroid/view/View;

    .line 671
    .line 672
    invoke-virtual {v8}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    .line 673
    .line 674
    .line 675
    move-result-object v8

    .line 676
    invoke-static {v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 677
    .line 678
    .line 679
    move-result-object v8

    .line 680
    check-cast v6, Landroid/view/View;

    .line 681
    .line 682
    invoke-virtual {v6, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 683
    .line 684
    .line 685
    new-instance v6, Landroid/widget/ImageView;

    .line 686
    .line 687
    iget-object v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 688
    .line 689
    invoke-direct {v6, v12}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 690
    .line 691
    .line 692
    iput-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzn:Landroid/widget/ImageView;

    .line 693
    .line 694
    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 695
    .line 696
    .line 697
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 698
    .line 699
    invoke-interface {v6}, Lcom/google/android/gms/internal/ads/zzcfi;->zzO()Lcom/google/android/gms/internal/ads/zzcgx;

    .line 700
    .line 701
    .line 702
    move-result-object v6

    .line 703
    iput-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzm:Lcom/google/android/gms/internal/ads/zzcgx;

    .line 704
    .line 705
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzs:Landroid/view/ViewGroup;

    .line 706
    .line 707
    iget-object v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzn:Landroid/widget/ImageView;

    .line 708
    .line 709
    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 710
    .line 711
    .line 712
    goto :goto_d

    .line 713
    :cond_1e
    invoke-virtual {v8}, Landroid/widget/PopupWindow;->dismiss()V

    .line 714
    .line 715
    .line 716
    :goto_d
    new-instance v6, Landroid/widget/RelativeLayout;

    .line 717
    .line 718
    iget-object v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 719
    .line 720
    invoke-direct {v6, v8}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 721
    .line 722
    .line 723
    iput-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzr:Landroid/widget/RelativeLayout;

    .line 724
    .line 725
    invoke-virtual {v6, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 726
    .line 727
    .line 728
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzr:Landroid/widget/RelativeLayout;

    .line 729
    .line 730
    new-instance v8, Landroid/view/ViewGroup$LayoutParams;

    .line 731
    .line 732
    invoke-direct {v8, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 733
    .line 734
    .line 735
    invoke-virtual {v6, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 736
    .line 737
    .line 738
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzt;->zzp()Lcom/google/android/gms/ads/internal/util/zzs;

    .line 739
    .line 740
    .line 741
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzr:Landroid/widget/RelativeLayout;

    .line 742
    .line 743
    new-instance v8, Landroid/widget/PopupWindow;

    .line 744
    .line 745
    invoke-direct {v8, v6, v3, v4, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    .line 746
    .line 747
    .line 748
    iput-object v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzq:Landroid/widget/PopupWindow;

    .line 749
    .line 750
    invoke-virtual {v8, v5}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 751
    .line 752
    .line 753
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzq:Landroid/widget/PopupWindow;

    .line 754
    .line 755
    invoke-virtual {v6, v7}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 756
    .line 757
    .line 758
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzq:Landroid/widget/PopupWindow;

    .line 759
    .line 760
    iget-boolean v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzc:Z

    .line 761
    .line 762
    xor-int/2addr v8, v7

    .line 763
    invoke-virtual {v6, v8}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 764
    .line 765
    .line 766
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzr:Landroid/widget/RelativeLayout;

    .line 767
    .line 768
    iget-object v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 769
    .line 770
    check-cast v8, Landroid/view/View;

    .line 771
    .line 772
    const/4 v12, -0x1

    .line 773
    invoke-virtual {v6, v8, v12, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 774
    .line 775
    .line 776
    new-instance v6, Landroid/widget/LinearLayout;

    .line 777
    .line 778
    iget-object v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 779
    .line 780
    invoke-direct {v6, v8}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 781
    .line 782
    .line 783
    iput-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzo:Landroid/widget/LinearLayout;

    .line 784
    .line 785
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 786
    .line 787
    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzay;->zzb()Lcom/google/android/gms/internal/ads/zzbzt;

    .line 788
    .line 789
    .line 790
    iget-object v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 791
    .line 792
    invoke-static {v8, v14}, Lcom/google/android/gms/internal/ads/zzbzt;->zzx(Landroid/content/Context;I)I

    .line 793
    .line 794
    .line 795
    move-result v8

    .line 796
    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzay;->zzb()Lcom/google/android/gms/internal/ads/zzbzt;

    .line 797
    .line 798
    .line 799
    iget-object v12, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 800
    .line 801
    invoke-static {v12, v14}, Lcom/google/android/gms/internal/ads/zzbzt;->zzx(Landroid/content/Context;I)I

    .line 802
    .line 803
    .line 804
    move-result v12

    .line 805
    invoke-direct {v6, v8, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 806
    .line 807
    .line 808
    iget-object v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzb:Ljava/lang/String;

    .line 809
    .line 810
    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    .line 811
    .line 812
    .line 813
    move-result v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 814
    sparse-switch v12, :sswitch_data_1

    .line 815
    .line 816
    .line 817
    goto :goto_e

    .line 818
    :sswitch_6
    const-string v12, "top-center"

    .line 819
    .line 820
    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 821
    .line 822
    .line 823
    move-result v8

    .line 824
    if-eqz v8, :cond_1f

    .line 825
    .line 826
    const/4 v12, 0x1

    .line 827
    goto :goto_f

    .line 828
    :sswitch_7
    const-string v12, "bottom-center"

    .line 829
    .line 830
    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 831
    .line 832
    .line 833
    move-result v8

    .line 834
    if-eqz v8, :cond_1f

    .line 835
    .line 836
    const/4 v12, 0x4

    .line 837
    goto :goto_f

    .line 838
    :sswitch_8
    const-string v12, "bottom-right"

    .line 839
    .line 840
    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 841
    .line 842
    .line 843
    move-result v8

    .line 844
    if-eqz v8, :cond_1f

    .line 845
    .line 846
    const/4 v12, 0x5

    .line 847
    goto :goto_f

    .line 848
    :sswitch_9
    const-string v12, "bottom-left"

    .line 849
    .line 850
    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 851
    .line 852
    .line 853
    move-result v8

    .line 854
    if-eqz v8, :cond_1f

    .line 855
    .line 856
    const/4 v12, 0x3

    .line 857
    goto :goto_f

    .line 858
    :sswitch_a
    const-string v12, "top-left"

    .line 859
    .line 860
    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 861
    .line 862
    .line 863
    move-result v8

    .line 864
    if-eqz v8, :cond_1f

    .line 865
    .line 866
    const/4 v12, 0x0

    .line 867
    goto :goto_f

    .line 868
    :sswitch_b
    const-string v12, "center"

    .line 869
    .line 870
    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 871
    .line 872
    .line 873
    move-result v8

    .line 874
    if-eqz v8, :cond_1f

    .line 875
    .line 876
    const/4 v12, 0x2

    .line 877
    goto :goto_f

    .line 878
    :cond_1f
    :goto_e
    const/4 v12, -0x1

    .line 879
    :goto_f
    const/16 v8, 0x9

    .line 880
    .line 881
    const/16 v14, 0xa

    .line 882
    .line 883
    if-eqz v12, :cond_25

    .line 884
    .line 885
    const/16 v5, 0xe

    .line 886
    .line 887
    if-eq v12, v7, :cond_24

    .line 888
    .line 889
    if-eq v12, v13, :cond_23

    .line 890
    .line 891
    const/16 v13, 0xc

    .line 892
    .line 893
    if-eq v12, v11, :cond_22

    .line 894
    .line 895
    if-eq v12, v10, :cond_21

    .line 896
    .line 897
    const/16 v5, 0xb

    .line 898
    .line 899
    if-eq v12, v9, :cond_20

    .line 900
    .line 901
    :try_start_2
    invoke-virtual {v6, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 902
    .line 903
    .line 904
    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 905
    .line 906
    .line 907
    goto :goto_10

    .line 908
    :cond_20
    invoke-virtual {v6, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 909
    .line 910
    .line 911
    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 912
    .line 913
    .line 914
    goto :goto_10

    .line 915
    :cond_21
    invoke-virtual {v6, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 916
    .line 917
    .line 918
    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 919
    .line 920
    .line 921
    goto :goto_10

    .line 922
    :cond_22
    invoke-virtual {v6, v13}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 923
    .line 924
    .line 925
    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 926
    .line 927
    .line 928
    goto :goto_10

    .line 929
    :cond_23
    const/16 v5, 0xd

    .line 930
    .line 931
    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 932
    .line 933
    .line 934
    goto :goto_10

    .line 935
    :cond_24
    invoke-virtual {v6, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 936
    .line 937
    .line 938
    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 939
    .line 940
    .line 941
    goto :goto_10

    .line 942
    :cond_25
    invoke-virtual {v6, v14}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 943
    .line 944
    .line 945
    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 946
    .line 947
    .line 948
    :goto_10
    iget-object v5, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzo:Landroid/widget/LinearLayout;

    .line 949
    .line 950
    new-instance v8, Lcom/google/android/gms/internal/ads/zzbqy;

    .line 951
    .line 952
    invoke-direct {v8, v1}, Lcom/google/android/gms/internal/ads/zzbqy;-><init>(Lcom/google/android/gms/internal/ads/zzbqz;)V

    .line 953
    .line 954
    .line 955
    invoke-virtual {v5, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 956
    .line 957
    .line 958
    iget-object v5, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzo:Landroid/widget/LinearLayout;

    .line 959
    .line 960
    const-string v8, "Close button"

    .line 961
    .line 962
    invoke-virtual {v5, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 963
    .line 964
    .line 965
    iget-object v5, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzr:Landroid/widget/RelativeLayout;

    .line 966
    .line 967
    iget-object v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzo:Landroid/widget/LinearLayout;

    .line 968
    .line 969
    invoke-virtual {v5, v8, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 970
    .line 971
    .line 972
    :try_start_3
    iget-object v5, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzq:Landroid/widget/PopupWindow;

    .line 973
    .line 974
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 975
    .line 976
    .line 977
    move-result-object v0

    .line 978
    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzay;->zzb()Lcom/google/android/gms/internal/ads/zzbzt;

    .line 979
    .line 980
    .line 981
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 982
    .line 983
    const/4 v8, 0x0

    .line 984
    aget v9, v15, v8

    .line 985
    .line 986
    invoke-static {v6, v9}, Lcom/google/android/gms/internal/ads/zzbzt;->zzx(Landroid/content/Context;I)I

    .line 987
    .line 988
    .line 989
    move-result v6

    .line 990
    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzay;->zzb()Lcom/google/android/gms/internal/ads/zzbzt;

    .line 991
    .line 992
    .line 993
    iget-object v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 994
    .line 995
    aget v9, v15, v7

    .line 996
    .line 997
    invoke-static {v8, v9}, Lcom/google/android/gms/internal/ads/zzbzt;->zzx(Landroid/content/Context;I)I

    .line 998
    .line 999
    .line 1000
    move-result v8

    .line 1001
    const/4 v9, 0x0

    .line 1002
    invoke-virtual {v5, v0, v9, v6, v8}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1003
    .line 1004
    .line 1005
    :try_start_4
    aget v0, v15, v9

    .line 1006
    .line 1007
    aget v5, v15, v7

    .line 1008
    .line 1009
    iget-object v6, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzp:Lcom/google/android/gms/internal/ads/zzbrg;

    .line 1010
    .line 1011
    if-eqz v6, :cond_26

    .line 1012
    .line 1013
    iget v8, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzi:I

    .line 1014
    .line 1015
    iget v9, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzf:I

    .line 1016
    .line 1017
    invoke-interface {v6, v0, v5, v8, v9}, Lcom/google/android/gms/internal/ads/zzbrg;->zza(IIII)V

    .line 1018
    .line 1019
    .line 1020
    :cond_26
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 1021
    .line 1022
    invoke-static {v3, v4}, Lcom/google/android/gms/internal/ads/zzcgx;->zzb(II)Lcom/google/android/gms/internal/ads/zzcgx;

    .line 1023
    .line 1024
    .line 1025
    move-result-object v3

    .line 1026
    invoke-interface {v0, v3}, Lcom/google/android/gms/internal/ads/zzcfi;->zzag(Lcom/google/android/gms/internal/ads/zzcgx;)V

    .line 1027
    .line 1028
    .line 1029
    const/4 v0, 0x0

    .line 1030
    aget v3, v15, v0

    .line 1031
    .line 1032
    aget v0, v15, v7

    .line 1033
    .line 1034
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzt;->zzp()Lcom/google/android/gms/ads/internal/util/zzs;

    .line 1035
    .line 1036
    .line 1037
    iget-object v4, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzl:Landroid/app/Activity;

    .line 1038
    .line 1039
    invoke-static {v4}, Lcom/google/android/gms/ads/internal/util/zzs;->zzO(Landroid/app/Activity;)[I

    .line 1040
    .line 1041
    .line 1042
    move-result-object v4

    .line 1043
    const/4 v5, 0x0

    .line 1044
    aget v4, v4, v5

    .line 1045
    .line 1046
    sub-int/2addr v0, v4

    .line 1047
    iget v4, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzi:I

    .line 1048
    .line 1049
    iget v5, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzf:I

    .line 1050
    .line 1051
    invoke-virtual {v1, v3, v0, v4, v5}, Lcom/google/android/gms/internal/ads/zzbrf;->zzj(IIII)V

    .line 1052
    .line 1053
    .line 1054
    const-string v0, "resized"

    .line 1055
    .line 1056
    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ads/zzbrf;->zzk(Ljava/lang/String;)V

    .line 1057
    .line 1058
    .line 1059
    monitor-exit v2

    .line 1060
    return-void

    .line 1061
    :catch_0
    move-exception v0

    .line 1062
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 1063
    .line 1064
    .line 1065
    move-result-object v0

    .line 1066
    new-instance v3, Ljava/lang/StringBuilder;

    .line 1067
    .line 1068
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1069
    .line 1070
    .line 1071
    const-string v4, "Cannot show popup window: "

    .line 1072
    .line 1073
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1074
    .line 1075
    .line 1076
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1077
    .line 1078
    .line 1079
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1080
    .line 1081
    .line 1082
    move-result-object v0

    .line 1083
    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ads/zzbrf;->zzg(Ljava/lang/String;)V

    .line 1084
    .line 1085
    .line 1086
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzr:Landroid/widget/RelativeLayout;

    .line 1087
    .line 1088
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 1089
    .line 1090
    check-cast v3, Landroid/view/View;

    .line 1091
    .line 1092
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1093
    .line 1094
    .line 1095
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzs:Landroid/view/ViewGroup;

    .line 1096
    .line 1097
    if-eqz v0, :cond_27

    .line 1098
    .line 1099
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzn:Landroid/widget/ImageView;

    .line 1100
    .line 1101
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1102
    .line 1103
    .line 1104
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzs:Landroid/view/ViewGroup;

    .line 1105
    .line 1106
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 1107
    .line 1108
    check-cast v3, Landroid/view/View;

    .line 1109
    .line 1110
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1111
    .line 1112
    .line 1113
    iget-object v0, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzk:Lcom/google/android/gms/internal/ads/zzcfi;

    .line 1114
    .line 1115
    iget-object v3, v1, Lcom/google/android/gms/internal/ads/zzbqz;->zzm:Lcom/google/android/gms/internal/ads/zzcgx;

    .line 1116
    .line 1117
    invoke-interface {v0, v3}, Lcom/google/android/gms/internal/ads/zzcfi;->zzag(Lcom/google/android/gms/internal/ads/zzcgx;)V

    .line 1118
    .line 1119
    .line 1120
    :cond_27
    monitor-exit v2

    .line 1121
    return-void

    .line 1122
    :cond_28
    const-string v0, "Webview is detached, probably in the middle of a resize or expand."

    .line 1123
    .line 1124
    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ads/zzbrf;->zzg(Ljava/lang/String;)V

    .line 1125
    .line 1126
    .line 1127
    monitor-exit v2

    .line 1128
    return-void

    .line 1129
    :cond_29
    :goto_11
    const-string v0, "Activity context is not ready, cannot get window or decor view."

    .line 1130
    .line 1131
    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ads/zzbrf;->zzg(Ljava/lang/String;)V

    .line 1132
    .line 1133
    .line 1134
    monitor-exit v2

    .line 1135
    return-void

    .line 1136
    :cond_2a
    const-string v0, "Invalid width and height options. Cannot resize."

    .line 1137
    .line 1138
    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ads/zzbrf;->zzg(Ljava/lang/String;)V

    .line 1139
    .line 1140
    .line 1141
    monitor-exit v2

    .line 1142
    return-void

    .line 1143
    :cond_2b
    const-string v0, "Cannot resize an expanded banner."

    .line 1144
    .line 1145
    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ads/zzbrf;->zzg(Ljava/lang/String;)V

    .line 1146
    .line 1147
    .line 1148
    monitor-exit v2

    .line 1149
    return-void

    .line 1150
    :catchall_0
    move-exception v0

    .line 1151
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1152
    throw v0

    .line 1153
    :sswitch_data_0
    .sparse-switch
        -0x514d33ab -> :sswitch_5
        -0x3c587281 -> :sswitch_4
        -0x27103597 -> :sswitch_3
        0x455fe3fa -> :sswitch_2
        0x4ccee637 -> :sswitch_1
        0x68a23bcd -> :sswitch_0
    .end sparse-switch

    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    :sswitch_data_1
    .sparse-switch
        -0x514d33ab -> :sswitch_b
        -0x3c587281 -> :sswitch_a
        -0x27103597 -> :sswitch_9
        0x455fe3fa -> :sswitch_8
        0x4ccee637 -> :sswitch_7
        0x68a23bcd -> :sswitch_6
    .end sparse-switch
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
.end method

.method public final zzc(IIZ)V
    .locals 0

    .line 1
    iget-object p3, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzj:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter p3

    .line 4
    :try_start_0
    iput p1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 5
    .line 6
    iput p2, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 7
    .line 8
    monitor-exit p3

    .line 9
    return-void

    .line 10
    :catchall_0
    move-exception p1

    .line 11
    monitor-exit p3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 12
    throw p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
.end method

.method public final zzd(II)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzd:I

    .line 2
    .line 3
    iput p2, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zze:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
.end method

.method public final zze()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzj:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/internal/ads/zzbqz;->zzq:Landroid/widget/PopupWindow;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v1, 0x0

    .line 11
    :goto_0
    monitor-exit v0

    .line 12
    return v1

    .line 13
    :catchall_0
    move-exception v1

    .line 14
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15
    throw v1
    .line 16
    .line 17
.end method

.class public final Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-mlkit-barcode-scanning@@16.2.1"


# instance fields
.field private zza:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzkz;

.field private zzb:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjf;

.field private zzc:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjd;

.field private zzd:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzju;

.field private zze:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjx;

.field private zzf:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzds;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static bridge synthetic zza(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzds;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zzf:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzds;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic zzb(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjd;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zzc:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjd;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic zzc(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjf;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zzb:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjf;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic zzk(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzju;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zzd:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzju;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic zzl(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjx;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zze:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjx;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic zzm(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzkz;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zza:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzkz;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final zzd(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzds;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zzf:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzds;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zze(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjd;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zzc:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjd;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzf(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjf;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zzb:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjf;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzg(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzju;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zzd:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzju;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzh(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjx;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zze:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjx;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzi(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzkz;)Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;->zza:Lcom/google/android/gms/internal/mlkit_vision_barcode/zzkz;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final zzj()Lcom/google/android/gms/internal/mlkit_vision_barcode/zzji;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzji;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/mlkit_vision_barcode/zzji;-><init>(Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjg;Lcom/google/android/gms/internal/mlkit_vision_barcode/zzjh;)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.class public final synthetic Lcom/google/android/gms/internal/ads/zzwd;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-ads@@22.4.0"

# interfaces
.implements Lcom/google/android/gms/internal/ads/zzwy;


# instance fields
.field public final synthetic zza:Lcom/google/android/gms/internal/ads/zzxd;

.field public final synthetic zzb:Lcom/google/android/gms/internal/ads/zzwr;

.field public final synthetic zzc:Z


# direct methods
.method public synthetic constructor <init>(Lcom/google/android/gms/internal/ads/zzxd;Lcom/google/android/gms/internal/ads/zzwr;Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/google/android/gms/internal/ads/zzwd;->zza:Lcom/google/android/gms/internal/ads/zzxd;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/google/android/gms/internal/ads/zzwd;->zzb:Lcom/google/android/gms/internal/ads/zzwr;

    .line 7
    .line 8
    iput-boolean p3, p0, Lcom/google/android/gms/internal/ads/zzwd;->zzc:Z

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
.end method


# virtual methods
.method public final zza(ILcom/google/android/gms/internal/ads/zzcy;[I)Ljava/util/List;
    .locals 16

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget-object v1, v0, Lcom/google/android/gms/internal/ads/zzwd;->zza:Lcom/google/android/gms/internal/ads/zzxd;

    .line 4
    .line 5
    iget-object v10, v0, Lcom/google/android/gms/internal/ads/zzwd;->zzb:Lcom/google/android/gms/internal/ads/zzwr;

    .line 6
    .line 7
    iget-boolean v11, v0, Lcom/google/android/gms/internal/ads/zzwd;->zzc:Z

    .line 8
    .line 9
    new-instance v12, Lcom/google/android/gms/internal/ads/zzwc;

    .line 10
    .line 11
    invoke-direct {v12, v1}, Lcom/google/android/gms/internal/ads/zzwc;-><init>(Lcom/google/android/gms/internal/ads/zzxd;)V

    .line 12
    .line 13
    .line 14
    new-instance v1, Lcom/google/android/gms/internal/ads/zzfua;

    .line 15
    .line 16
    invoke-direct {v1}, Lcom/google/android/gms/internal/ads/zzfua;-><init>()V

    .line 17
    .line 18
    .line 19
    const/4 v2, 0x0

    .line 20
    move-object/from16 v14, p2

    .line 21
    .line 22
    const/4 v13, 0x0

    .line 23
    :goto_0
    iget v2, v14, Lcom/google/android/gms/internal/ads/zzcy;->zzb:I

    .line 24
    .line 25
    if-gtz v13, :cond_0

    .line 26
    .line 27
    new-instance v15, Lcom/google/android/gms/internal/ads/zzwl;

    .line 28
    .line 29
    aget v7, p3, v13

    .line 30
    .line 31
    move-object v2, v15

    .line 32
    move/from16 v3, p1

    .line 33
    .line 34
    move-object/from16 v4, p2

    .line 35
    .line 36
    move v5, v13

    .line 37
    move-object v6, v10

    .line 38
    move v8, v11

    .line 39
    move-object v9, v12

    .line 40
    invoke-direct/range {v2 .. v9}, Lcom/google/android/gms/internal/ads/zzwl;-><init>(ILcom/google/android/gms/internal/ads/zzcy;ILcom/google/android/gms/internal/ads/zzwr;IZLcom/google/android/gms/internal/ads/zzfrj;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1, v15}, Lcom/google/android/gms/internal/ads/zzfua;->zzf(Ljava/lang/Object;)Lcom/google/android/gms/internal/ads/zzfua;

    .line 44
    .line 45
    .line 46
    add-int/lit8 v13, v13, 0x1

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/internal/ads/zzfua;->zzi()Lcom/google/android/gms/internal/ads/zzfud;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    return-object v1
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
.end method

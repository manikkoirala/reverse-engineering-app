.class public interface abstract Lcom/google/android/gms/internal/ads/zzko;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-ads@@22.4.0"


# static fields
.field public static final zza:Lcom/google/android/gms/internal/ads/zzbw;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/gms/internal/ads/zzbw;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/Object;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 6
    .line 7
    .line 8
    const-wide/16 v2, -0x1

    .line 9
    .line 10
    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/internal/ads/zzbw;-><init>(Ljava/lang/Object;J)V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/google/android/gms/internal/ads/zzko;->zza:Lcom/google/android/gms/internal/ads/zzbw;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
.end method


# virtual methods
.method public abstract zza()J
.end method

.method public abstract zzb()V
.end method

.method public abstract zzc()V
.end method

.method public abstract zzd()V
.end method

.method public abstract zze(Lcom/google/android/gms/internal/ads/zzcw;Lcom/google/android/gms/internal/ads/zzbw;[Lcom/google/android/gms/internal/ads/zzln;Lcom/google/android/gms/internal/ads/zzvs;[Lcom/google/android/gms/internal/ads/zzxf;)V
.end method

.method public abstract zzf()Z
.end method

.method public abstract zzg(JJF)Z
.end method

.method public abstract zzh(Lcom/google/android/gms/internal/ads/zzcw;Lcom/google/android/gms/internal/ads/zzbw;JFZJ)Z
.end method

.method public abstract zzi()Lcom/google/android/gms/internal/ads/zzxu;
.end method

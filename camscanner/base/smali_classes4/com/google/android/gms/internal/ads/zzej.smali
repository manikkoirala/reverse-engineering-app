.class public interface abstract Lcom/google/android/gms/internal/ads/zzej;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-ads@@22.4.0"


# virtual methods
.method public abstract zza()Landroid/os/Looper;
.end method

.method public abstract zzb(I)Lcom/google/android/gms/internal/ads/zzei;
.end method

.method public abstract zzc(ILjava/lang/Object;)Lcom/google/android/gms/internal/ads/zzei;
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract zzd(III)Lcom/google/android/gms/internal/ads/zzei;
.end method

.method public abstract zze(Ljava/lang/Object;)V
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract zzf(I)V
.end method

.method public abstract zzg(I)Z
.end method

.method public abstract zzh(Ljava/lang/Runnable;)Z
.end method

.method public abstract zzi(I)Z
.end method

.method public abstract zzj(IJ)Z
.end method

.method public abstract zzk(Lcom/google/android/gms/internal/ads/zzei;)Z
.end method

.class public final Lcom/google/android/material/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/material/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f130493

.field public static final abc_action_bar_up_description:I = 0x7f130494

.field public static final abc_action_menu_overflow_description:I = 0x7f130495

.field public static final abc_action_mode_done:I = 0x7f130496

.field public static final abc_activity_chooser_view_see_all:I = 0x7f130497

.field public static final abc_activitychooserview_choose_application:I = 0x7f130498

.field public static final abc_capital_off:I = 0x7f130499

.field public static final abc_capital_on:I = 0x7f13049a

.field public static final abc_menu_alt_shortcut_label:I = 0x7f13049b

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f13049c

.field public static final abc_menu_delete_shortcut_label:I = 0x7f13049d

.field public static final abc_menu_enter_shortcut_label:I = 0x7f13049e

.field public static final abc_menu_function_shortcut_label:I = 0x7f13049f

.field public static final abc_menu_meta_shortcut_label:I = 0x7f1304a0

.field public static final abc_menu_shift_shortcut_label:I = 0x7f1304a1

.field public static final abc_menu_space_shortcut_label:I = 0x7f1304a2

.field public static final abc_menu_sym_shortcut_label:I = 0x7f1304a3

.field public static final abc_prepend_shortcut_label:I = 0x7f1304a4

.field public static final abc_search_hint:I = 0x7f1304a5

.field public static final abc_searchview_description_clear:I = 0x7f1304a6

.field public static final abc_searchview_description_query:I = 0x7f1304a7

.field public static final abc_searchview_description_search:I = 0x7f1304a8

.field public static final abc_searchview_description_submit:I = 0x7f1304a9

.field public static final abc_searchview_description_voice:I = 0x7f1304aa

.field public static final abc_shareactionprovider_share_with:I = 0x7f1304ab

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f1304ac

.field public static final abc_toolbar_collapse_description:I = 0x7f1304ad

.field public static final appbar_scrolling_view_behavior:I = 0x7f1304ef

.field public static final bottom_sheet_behavior:I = 0x7f130522

.field public static final bottomsheet_action_collapse:I = 0x7f130524

.field public static final bottomsheet_action_expand:I = 0x7f130525

.field public static final bottomsheet_action_expand_halfway:I = 0x7f130526

.field public static final bottomsheet_drag_handle_clicked:I = 0x7f130527

.field public static final bottomsheet_drag_handle_content_description:I = 0x7f130528

.field public static final character_counter_content_description:I = 0x7f130585

.field public static final character_counter_overflowed_content_description:I = 0x7f130586

.field public static final character_counter_pattern:I = 0x7f130587

.field public static final clear_text_end_icon_content_description:I = 0x7f13058d

.field public static final error_a11y_label:I = 0x7f131d18

.field public static final error_icon_content_description:I = 0x7f131d19

.field public static final exposed_dropdown_menu_content_description:I = 0x7f131d21

.field public static final fab_transformation_scrim_behavior:I = 0x7f131d22

.field public static final fab_transformation_sheet_behavior:I = 0x7f131d23

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f131d3d

.field public static final icon_content_description:I = 0x7f131d44

.field public static final item_view_role_description:I = 0x7f131d4b

.field public static final m3_ref_typeface_brand_medium:I = 0x7f131d8a

.field public static final m3_ref_typeface_brand_regular:I = 0x7f131d8b

.field public static final m3_ref_typeface_plain_medium:I = 0x7f131d8c

.field public static final m3_ref_typeface_plain_regular:I = 0x7f131d8d

.field public static final m3_sys_motion_easing_emphasized:I = 0x7f131d8e

.field public static final m3_sys_motion_easing_emphasized_accelerate:I = 0x7f131d8f

.field public static final m3_sys_motion_easing_emphasized_decelerate:I = 0x7f131d90

.field public static final m3_sys_motion_easing_emphasized_path_data:I = 0x7f131d91

.field public static final m3_sys_motion_easing_legacy:I = 0x7f131d92

.field public static final m3_sys_motion_easing_legacy_accelerate:I = 0x7f131d93

.field public static final m3_sys_motion_easing_legacy_decelerate:I = 0x7f131d94

.field public static final m3_sys_motion_easing_linear:I = 0x7f131d95

.field public static final m3_sys_motion_easing_standard:I = 0x7f131d96

.field public static final m3_sys_motion_easing_standard_accelerate:I = 0x7f131d97

.field public static final m3_sys_motion_easing_standard_decelerate:I = 0x7f131d98

.field public static final material_clock_display_divider:I = 0x7f131d99

.field public static final material_clock_toggle_content_description:I = 0x7f131d9a

.field public static final material_hour_24h_suffix:I = 0x7f131d9b

.field public static final material_hour_selection:I = 0x7f131d9c

.field public static final material_hour_suffix:I = 0x7f131d9d

.field public static final material_minute_selection:I = 0x7f131d9e

.field public static final material_minute_suffix:I = 0x7f131d9f

.field public static final material_motion_easing_accelerated:I = 0x7f131da0

.field public static final material_motion_easing_decelerated:I = 0x7f131da1

.field public static final material_motion_easing_emphasized:I = 0x7f131da2

.field public static final material_motion_easing_linear:I = 0x7f131da3

.field public static final material_motion_easing_standard:I = 0x7f131da4

.field public static final material_slider_range_end:I = 0x7f131da5

.field public static final material_slider_range_start:I = 0x7f131da6

.field public static final material_slider_value:I = 0x7f131da7

.field public static final material_timepicker_am:I = 0x7f131da8

.field public static final material_timepicker_clock_mode_description:I = 0x7f131da9

.field public static final material_timepicker_hour:I = 0x7f131daa

.field public static final material_timepicker_minute:I = 0x7f131dab

.field public static final material_timepicker_pm:I = 0x7f131dac

.field public static final material_timepicker_select_time:I = 0x7f131dad

.field public static final material_timepicker_text_input_mode_description:I = 0x7f131dae

.field public static final mtrl_badge_numberless_content_description:I = 0x7f131dc4

.field public static final mtrl_checkbox_button_icon_path_checked:I = 0x7f131dc5

.field public static final mtrl_checkbox_button_icon_path_group_name:I = 0x7f131dc6

.field public static final mtrl_checkbox_button_icon_path_indeterminate:I = 0x7f131dc7

.field public static final mtrl_checkbox_button_icon_path_name:I = 0x7f131dc8

.field public static final mtrl_checkbox_button_path_checked:I = 0x7f131dc9

.field public static final mtrl_checkbox_button_path_group_name:I = 0x7f131dca

.field public static final mtrl_checkbox_button_path_name:I = 0x7f131dcb

.field public static final mtrl_checkbox_button_path_unchecked:I = 0x7f131dcc

.field public static final mtrl_checkbox_state_description_checked:I = 0x7f131dcd

.field public static final mtrl_checkbox_state_description_indeterminate:I = 0x7f131dce

.field public static final mtrl_checkbox_state_description_unchecked:I = 0x7f131dcf

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f131dd0

.field public static final mtrl_exceed_max_badge_number_content_description:I = 0x7f131dd1

.field public static final mtrl_exceed_max_badge_number_suffix:I = 0x7f131dd2

.field public static final mtrl_picker_a11y_next_month:I = 0x7f131dd3

.field public static final mtrl_picker_a11y_prev_month:I = 0x7f131dd4

.field public static final mtrl_picker_announce_current_range_selection:I = 0x7f131dd5

.field public static final mtrl_picker_announce_current_selection:I = 0x7f131dd6

.field public static final mtrl_picker_announce_current_selection_none:I = 0x7f131dd7

.field public static final mtrl_picker_cancel:I = 0x7f131dd8

.field public static final mtrl_picker_confirm:I = 0x7f131dd9

.field public static final mtrl_picker_date_header_selected:I = 0x7f131dda

.field public static final mtrl_picker_date_header_title:I = 0x7f131ddb

.field public static final mtrl_picker_date_header_unselected:I = 0x7f131ddc

.field public static final mtrl_picker_day_of_week_column_header:I = 0x7f131ddd

.field public static final mtrl_picker_end_date_description:I = 0x7f131dde

.field public static final mtrl_picker_invalid_format:I = 0x7f131ddf

.field public static final mtrl_picker_invalid_format_example:I = 0x7f131de0

.field public static final mtrl_picker_invalid_format_use:I = 0x7f131de1

.field public static final mtrl_picker_invalid_range:I = 0x7f131de2

.field public static final mtrl_picker_navigate_to_current_year_description:I = 0x7f131de3

.field public static final mtrl_picker_navigate_to_year_description:I = 0x7f131de4

.field public static final mtrl_picker_out_of_range:I = 0x7f131de5

.field public static final mtrl_picker_range_header_only_end_selected:I = 0x7f131de6

.field public static final mtrl_picker_range_header_only_start_selected:I = 0x7f131de7

.field public static final mtrl_picker_range_header_selected:I = 0x7f131de8

.field public static final mtrl_picker_range_header_title:I = 0x7f131de9

.field public static final mtrl_picker_range_header_unselected:I = 0x7f131dea

.field public static final mtrl_picker_save:I = 0x7f131deb

.field public static final mtrl_picker_start_date_description:I = 0x7f131dec

.field public static final mtrl_picker_text_input_date_hint:I = 0x7f131ded

.field public static final mtrl_picker_text_input_date_range_end_hint:I = 0x7f131dee

.field public static final mtrl_picker_text_input_date_range_start_hint:I = 0x7f131def

.field public static final mtrl_picker_text_input_day_abbr:I = 0x7f131df0

.field public static final mtrl_picker_text_input_month_abbr:I = 0x7f131df1

.field public static final mtrl_picker_text_input_year_abbr:I = 0x7f131df2

.field public static final mtrl_picker_today_description:I = 0x7f131df3

.field public static final mtrl_picker_toggle_to_calendar_input_mode:I = 0x7f131df4

.field public static final mtrl_picker_toggle_to_day_selection:I = 0x7f131df5

.field public static final mtrl_picker_toggle_to_text_input_mode:I = 0x7f131df6

.field public static final mtrl_picker_toggle_to_year_selection:I = 0x7f131df7

.field public static final mtrl_switch_thumb_group_name:I = 0x7f131df8

.field public static final mtrl_switch_thumb_path_checked:I = 0x7f131df9

.field public static final mtrl_switch_thumb_path_morphing:I = 0x7f131dfa

.field public static final mtrl_switch_thumb_path_name:I = 0x7f131dfb

.field public static final mtrl_switch_thumb_path_pressed:I = 0x7f131dfc

.field public static final mtrl_switch_thumb_path_unchecked:I = 0x7f131dfd

.field public static final mtrl_switch_track_decoration_path:I = 0x7f131dfe

.field public static final mtrl_switch_track_path:I = 0x7f131dff

.field public static final mtrl_timepicker_cancel:I = 0x7f131e00

.field public static final mtrl_timepicker_confirm:I = 0x7f131e01

.field public static final password_toggle_content_description:I = 0x7f131e44

.field public static final path_password_eye:I = 0x7f131e45

.field public static final path_password_eye_mask_strike_through:I = 0x7f131e46

.field public static final path_password_eye_mask_visible:I = 0x7f131e47

.field public static final path_password_strike_through:I = 0x7f131e48

.field public static final search_menu_title:I = 0x7f131e6d

.field public static final searchbar_scrolling_view_behavior:I = 0x7f131e6e

.field public static final searchview_clear_text_content_description:I = 0x7f131e6f

.field public static final searchview_navigation_content_description:I = 0x7f131e70

.field public static final side_sheet_accessibility_pane_title:I = 0x7f131e84

.field public static final side_sheet_behavior:I = 0x7f131e85

.field public static final status_bar_notification_info_overflow:I = 0x7f131eca


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

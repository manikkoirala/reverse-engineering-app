.class public final Lcom/google/firebase/icing/R$styleable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/firebase/icing/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AppDataSearch:[I

.field public static final Corpus:[I

.field public static final Corpus_contentProviderUri:I = 0x0

.field public static final Corpus_corpusId:I = 0x1

.field public static final Corpus_corpusVersion:I = 0x2

.field public static final Corpus_documentMaxAgeSecs:I = 0x3

.field public static final Corpus_perAccountTemplate:I = 0x4

.field public static final Corpus_schemaOrgType:I = 0x5

.field public static final Corpus_semanticallySearchable:I = 0x6

.field public static final Corpus_trimmable:I = 0x7

.field public static final FeatureParam:[I

.field public static final FeatureParam_paramName:I = 0x0

.field public static final FeatureParam_paramValue:I = 0x1

.field public static final GlobalSearch:[I

.field public static final GlobalSearchCorpus:[I

.field public static final GlobalSearchCorpus_allowShortcuts:I = 0x0

.field public static final GlobalSearchSection:[I

.field public static final GlobalSearchSection_sectionContent:I = 0x0

.field public static final GlobalSearchSection_sectionType:I = 0x1

.field public static final GlobalSearch_defaultIntentAction:I = 0x0

.field public static final GlobalSearch_defaultIntentActivity:I = 0x1

.field public static final GlobalSearch_defaultIntentData:I = 0x2

.field public static final GlobalSearch_searchEnabled:I = 0x3

.field public static final GlobalSearch_searchLabel:I = 0x4

.field public static final GlobalSearch_settingsDescription:I = 0x5

.field public static final IMECorpus:[I

.field public static final IMECorpus_inputEnabled:I = 0x0

.field public static final IMECorpus_sourceClass:I = 0x1

.field public static final IMECorpus_toAddressesSection:I = 0x2

.field public static final IMECorpus_userInputSection:I = 0x3

.field public static final IMECorpus_userInputTag:I = 0x4

.field public static final IMECorpus_userInputValue:I = 0x5

.field public static final Section:[I

.field public static final SectionFeature:[I

.field public static final SectionFeature_featureType:I = 0x0

.field public static final Section_indexPrefixes:I = 0x0

.field public static final Section_noIndex:I = 0x1

.field public static final Section_schemaOrgProperty:I = 0x2

.field public static final Section_sectionFormat:I = 0x3

.field public static final Section_sectionId:I = 0x4

.field public static final Section_sectionWeight:I = 0x5

.field public static final Section_subsectionSeparator:I = 0x6


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    new-array v1, v0, [I

    .line 3
    .line 4
    sput-object v1, Lcom/google/firebase/icing/R$styleable;->AppDataSearch:[I

    .line 5
    .line 6
    const/16 v1, 0x8

    .line 7
    .line 8
    new-array v1, v1, [I

    .line 9
    .line 10
    fill-array-data v1, :array_0

    .line 11
    .line 12
    .line 13
    sput-object v1, Lcom/google/firebase/icing/R$styleable;->Corpus:[I

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    new-array v2, v1, [I

    .line 17
    .line 18
    fill-array-data v2, :array_1

    .line 19
    .line 20
    .line 21
    sput-object v2, Lcom/google/firebase/icing/R$styleable;->FeatureParam:[I

    .line 22
    .line 23
    const/4 v2, 0x6

    .line 24
    new-array v3, v2, [I

    .line 25
    .line 26
    fill-array-data v3, :array_2

    .line 27
    .line 28
    .line 29
    sput-object v3, Lcom/google/firebase/icing/R$styleable;->GlobalSearch:[I

    .line 30
    .line 31
    const/4 v3, 0x1

    .line 32
    new-array v4, v3, [I

    .line 33
    .line 34
    const v5, 0x7f040059

    .line 35
    .line 36
    .line 37
    aput v5, v4, v0

    .line 38
    .line 39
    sput-object v4, Lcom/google/firebase/icing/R$styleable;->GlobalSearchCorpus:[I

    .line 40
    .line 41
    new-array v1, v1, [I

    .line 42
    .line 43
    fill-array-data v1, :array_3

    .line 44
    .line 45
    .line 46
    sput-object v1, Lcom/google/firebase/icing/R$styleable;->GlobalSearchSection:[I

    .line 47
    .line 48
    new-array v1, v2, [I

    .line 49
    .line 50
    fill-array-data v1, :array_4

    .line 51
    .line 52
    .line 53
    sput-object v1, Lcom/google/firebase/icing/R$styleable;->IMECorpus:[I

    .line 54
    .line 55
    const/4 v1, 0x7

    .line 56
    new-array v1, v1, [I

    .line 57
    .line 58
    fill-array-data v1, :array_5

    .line 59
    .line 60
    .line 61
    sput-object v1, Lcom/google/firebase/icing/R$styleable;->Section:[I

    .line 62
    .line 63
    new-array v1, v3, [I

    .line 64
    .line 65
    const v2, 0x7f040297

    .line 66
    .line 67
    .line 68
    aput v2, v1, v0

    .line 69
    .line 70
    sput-object v1, Lcom/google/firebase/icing/R$styleable;->SectionFeature:[I

    .line 71
    .line 72
    return-void

    .line 73
    :array_0
    .array-data 4
        0x7f0401af
        0x7f0401c0
        0x7f0401c1
        0x7f040208
        0x7f0404c9
        0x7f04051c
        0x7f040536
        0x7f040696
    .end array-data

    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    :array_1
    .array-data 4
        0x7f0404c0
        0x7f0404c1
    .end array-data

    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    :array_2
    .array-data 4
        0x7f0401e6
        0x7f0401e7
        0x7f0401e8
        0x7f040522
        0x7f040525
        0x7f040538
    .end array-data

    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    :array_3
    .array-data 4
        0x7f040528
        0x7f04052b
    .end array-data

    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    :array_4
    .array-data 4
        0x7f040328
        0x7f040579
        0x7f040670
        0x7f0406a4
        0x7f0406a5
        0x7f0406a6
    .end array-data

    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    :array_5
    .array-data 4
        0x7f04031e
        0x7f0404a1
        0x7f04051b
        0x7f040529
        0x7f04052a
        0x7f04052c
        0x7f0405a8
    .end array-data
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

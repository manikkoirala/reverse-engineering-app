.class public final Lcom/google/firebase/icing/R$attr;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/firebase/icing/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final allowShortcuts:I = 0x7f040059

.field public static final contentProviderUri:I = 0x7f0401af

.field public static final corpusId:I = 0x7f0401c0

.field public static final corpusVersion:I = 0x7f0401c1

.field public static final defaultIntentAction:I = 0x7f0401e6

.field public static final defaultIntentActivity:I = 0x7f0401e7

.field public static final defaultIntentData:I = 0x7f0401e8

.field public static final documentMaxAgeSecs:I = 0x7f040208

.field public static final featureType:I = 0x7f040297

.field public static final indexPrefixes:I = 0x7f04031e

.field public static final inputEnabled:I = 0x7f040328

.field public static final noIndex:I = 0x7f0404a1

.field public static final paramName:I = 0x7f0404c0

.field public static final paramValue:I = 0x7f0404c1

.field public static final perAccountTemplate:I = 0x7f0404c9

.field public static final schemaOrgProperty:I = 0x7f04051b

.field public static final schemaOrgType:I = 0x7f04051c

.field public static final searchEnabled:I = 0x7f040522

.field public static final searchLabel:I = 0x7f040525

.field public static final sectionContent:I = 0x7f040528

.field public static final sectionFormat:I = 0x7f040529

.field public static final sectionId:I = 0x7f04052a

.field public static final sectionType:I = 0x7f04052b

.field public static final sectionWeight:I = 0x7f04052c

.field public static final semanticallySearchable:I = 0x7f040536

.field public static final settingsDescription:I = 0x7f040538

.field public static final sourceClass:I = 0x7f040579

.field public static final subsectionSeparator:I = 0x7f0405a8

.field public static final toAddressesSection:I = 0x7f040670

.field public static final trimmable:I = 0x7f040696

.field public static final userInputSection:I = 0x7f0406a4

.field public static final userInputTag:I = 0x7f0406a5

.field public static final userInputValue:I = 0x7f0406a6


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

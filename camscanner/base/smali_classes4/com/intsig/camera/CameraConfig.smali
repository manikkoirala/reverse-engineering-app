.class public final Lcom/intsig/camera/CameraConfig;
.super Ljava/lang/Object;
.source "CameraConfig.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camera/CameraConfig;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o00〇〇Oo:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/intsig/camera/CameraPlan;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o〇:Lcom/google/android/camera/lifecycle/RealInterceptorChain;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 1
    new-instance v0, Lcom/intsig/camera/CameraConfig;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camera/CameraConfig;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camera/CameraConfig;->〇080:Lcom/intsig/camera/CameraConfig;

    .line 7
    .line 8
    new-instance v1, Landroid/util/SparseArray;

    .line 9
    .line 10
    const/4 v2, 0x4

    .line 11
    invoke-direct {v1, v2}, Landroid/util/SparseArray;-><init>(I)V

    .line 12
    .line 13
    .line 14
    sput-object v1, Lcom/intsig/camera/CameraConfig;->〇o00〇〇Oo:Landroid/util/SparseArray;

    .line 15
    .line 16
    const/4 v1, 0x3

    .line 17
    new-array v2, v1, [Lcom/intsig/camera/CameraPlan;

    .line 18
    .line 19
    new-instance v3, Lcom/intsig/camera/CameraPlan;

    .line 20
    .line 21
    sget-object v4, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    .line 22
    .line 23
    invoke-virtual {v4}, Lcom/google/android/camera/data/CameraApi$Companion;->〇o00〇〇Oo()I

    .line 24
    .line 25
    .line 26
    move-result v5

    .line 27
    const-class v6, Lcom/google/android/camera/Camera2;

    .line 28
    .line 29
    invoke-direct {v3, v5, v6}, Lcom/intsig/camera/CameraPlan;-><init>(ILjava/lang/Class;)V

    .line 30
    .line 31
    .line 32
    const/4 v5, 0x0

    .line 33
    aput-object v3, v2, v5

    .line 34
    .line 35
    new-instance v3, Lcom/intsig/camera/CameraPlan;

    .line 36
    .line 37
    invoke-virtual {v4}, Lcom/google/android/camera/data/CameraApi$Companion;->〇080()I

    .line 38
    .line 39
    .line 40
    move-result v6

    .line 41
    const-class v7, Lcom/google/android/camera/Camera1;

    .line 42
    .line 43
    invoke-direct {v3, v6, v7}, Lcom/intsig/camera/CameraPlan;-><init>(ILjava/lang/Class;)V

    .line 44
    .line 45
    .line 46
    const/4 v6, 0x1

    .line 47
    aput-object v3, v2, v6

    .line 48
    .line 49
    new-instance v3, Lcom/intsig/camera/CameraPlan;

    .line 50
    .line 51
    invoke-virtual {v4}, Lcom/google/android/camera/data/CameraApi$Companion;->o〇0()I

    .line 52
    .line 53
    .line 54
    move-result v4

    .line 55
    const-class v7, Landroidx/camera/core/CameraX;

    .line 56
    .line 57
    invoke-direct {v3, v4, v7}, Lcom/intsig/camera/CameraPlan;-><init>(ILjava/lang/Class;)V

    .line 58
    .line 59
    .line 60
    const/4 v4, 0x2

    .line 61
    aput-object v3, v2, v4

    .line 62
    .line 63
    invoke-virtual {v0, v2}, Lcom/intsig/camera/CameraConfig;->〇080([Lcom/intsig/camera/CameraPlan;)Lcom/intsig/camera/CameraConfig;

    .line 64
    .line 65
    .line 66
    new-instance v0, Lcom/google/android/camera/lifecycle/RealInterceptorChain;

    .line 67
    .line 68
    invoke-direct {v0}, Lcom/google/android/camera/lifecycle/RealInterceptorChain;-><init>()V

    .line 69
    .line 70
    .line 71
    new-array v1, v1, [Lcom/google/android/camera/lifecycle/CameraInterceptor;

    .line 72
    .line 73
    new-instance v2, Lcom/google/android/camera/lifecycle/Camera1Interceptor;

    .line 74
    .line 75
    invoke-direct {v2}, Lcom/google/android/camera/lifecycle/Camera1Interceptor;-><init>()V

    .line 76
    .line 77
    .line 78
    aput-object v2, v1, v5

    .line 79
    .line 80
    new-instance v2, Lcom/google/android/camera/lifecycle/Camera2Interceptor;

    .line 81
    .line 82
    invoke-direct {v2}, Lcom/google/android/camera/lifecycle/Camera2Interceptor;-><init>()V

    .line 83
    .line 84
    .line 85
    aput-object v2, v1, v6

    .line 86
    .line 87
    new-instance v2, Lcom/intsig/android/camerax/CameraXInterceptor;

    .line 88
    .line 89
    invoke-direct {v2}, Lcom/intsig/android/camerax/CameraXInterceptor;-><init>()V

    .line 90
    .line 91
    .line 92
    aput-object v2, v1, v4

    .line 93
    .line 94
    invoke-virtual {v0, v1}, Lcom/google/android/camera/lifecycle/RealInterceptorChain;->oO80([Lcom/google/android/camera/lifecycle/CameraInterceptor;)Lcom/google/android/camera/lifecycle/RealInterceptorChain;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    sput-object v0, Lcom/intsig/camera/CameraConfig;->〇o〇:Lcom/google/android/camera/lifecycle/RealInterceptorChain;

    .line 99
    .line 100
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private final 〇o〇(Landroid/content/Context;ILcom/google/android/camera/CameraViewImpl$Callback;Lcom/google/android/camera/PreviewImpl;)Ljava/lang/Object;
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/intsig/camera/CameraConfig;->O8(I)Lcom/intsig/camera/CameraPlan;

    .line 3
    .line 4
    .line 5
    move-result-object p2

    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-virtual {p2}, Lcom/intsig/camera/CameraPlan;->〇080()Ljava/lang/Class;

    .line 9
    .line 10
    .line 11
    move-result-object p2

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object p2, v0

    .line 14
    :goto_0
    if-eqz p2, :cond_1

    .line 15
    .line 16
    const/4 v1, 0x3

    .line 17
    new-array v2, v1, [Ljava/lang/Class;

    .line 18
    .line 19
    const-class v3, Landroid/content/Context;

    .line 20
    .line 21
    const/4 v4, 0x0

    .line 22
    aput-object v3, v2, v4

    .line 23
    .line 24
    const-class v3, Lcom/google/android/camera/CameraViewImpl$Callback;

    .line 25
    .line 26
    const/4 v5, 0x1

    .line 27
    aput-object v3, v2, v5

    .line 28
    .line 29
    const-class v3, Lcom/google/android/camera/PreviewImpl;

    .line 30
    .line 31
    const/4 v6, 0x2

    .line 32
    aput-object v3, v2, v6

    .line 33
    .line 34
    invoke-virtual {p2, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    new-array v1, v1, [Ljava/lang/Object;

    .line 39
    .line 40
    aput-object p1, v1, v4

    .line 41
    .line 42
    aput-object p3, v1, v5

    .line 43
    .line 44
    aput-object p4, v1, v6

    .line 45
    .line 46
    invoke-virtual {p2, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    goto :goto_1

    .line 51
    :catch_0
    move-exception p1

    .line 52
    const-string p2, "Camerax-CameraConfig"

    .line 53
    .line 54
    const-string p3, "getCameraInstance error"

    .line 55
    .line 56
    invoke-static {p2, p3, p1}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 57
    .line 58
    .line 59
    :cond_1
    :goto_1
    return-object v0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
.end method


# virtual methods
.method public final O8(I)Lcom/intsig/camera/CameraPlan;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camera/CameraConfig;->〇o00〇〇Oo:Landroid/util/SparseArray;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    check-cast p1, Lcom/intsig/camera/CameraPlan;

    .line 9
    .line 10
    return-object p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oo08(IILkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 1
    .param p3    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/google/android/camera/lifecycle/CameraUse;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camera/CameraConfig;->〇o〇:Lcom/google/android/camera/lifecycle/RealInterceptorChain;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/camera/lifecycle/RealInterceptorChain;->〇o00〇〇Oo(IILkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
.end method

.method public final o〇0(Landroid/content/Context;ILcom/google/android/camera/CameraViewImpl$Callback;Lcom/google/android/camera/PreviewImpl;)Lcom/google/android/camera/CameraViewImpl;
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camera/CameraConfig;->〇o〇(Landroid/content/Context;ILcom/google/android/camera/CameraViewImpl$Callback;Lcom/google/android/camera/PreviewImpl;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    instance-of p2, p1, Lcom/google/android/camera/CameraViewImpl;

    .line 11
    .line 12
    if-eqz p2, :cond_0

    .line 13
    .line 14
    check-cast p1, Lcom/google/android/camera/CameraViewImpl;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    .line 16
    return-object p1

    .line 17
    :catch_0
    move-exception p1

    .line 18
    const-string p2, "Camerax-CameraConfig"

    .line 19
    .line 20
    const-string p3, "loadInternalCamera error"

    .line 21
    .line 22
    invoke-static {p2, p3, p1}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    const/4 p1, 0x0

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
.end method

.method public final varargs 〇080([Lcom/intsig/camera/CameraPlan;)Lcom/intsig/camera/CameraConfig;
    .locals 5
    .param p1    # [Lcom/intsig/camera/CameraPlan;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "plans"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    array-length v0, p1

    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    if-ge v1, v0, :cond_0

    .line 9
    .line 10
    aget-object v2, p1, v1

    .line 11
    .line 12
    sget-object v3, Lcom/intsig/camera/CameraConfig;->〇o00〇〇Oo:Landroid/util/SparseArray;

    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/intsig/camera/CameraPlan;->〇o00〇〇Oo()I

    .line 15
    .line 16
    .line 17
    move-result v4

    .line 18
    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 19
    .line 20
    .line 21
    add-int/lit8 v1, v1, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    return-object p0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final varargs 〇o00〇〇Oo([Lcom/google/android/camera/lifecycle/CameraInterceptor;)Lcom/intsig/camera/CameraConfig;
    .locals 2
    .param p1    # [Lcom/google/android/camera/lifecycle/CameraInterceptor;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "interceptors"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camera/CameraConfig;->〇o〇:Lcom/google/android/camera/lifecycle/RealInterceptorChain;

    .line 7
    .line 8
    array-length v1, p1

    .line 9
    invoke-static {p1, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    check-cast p1, [Lcom/google/android/camera/lifecycle/CameraInterceptor;

    .line 14
    .line 15
    invoke-virtual {v0, p1}, Lcom/google/android/camera/lifecycle/RealInterceptorChain;->oO80([Lcom/google/android/camera/lifecycle/CameraInterceptor;)Lcom/google/android/camera/lifecycle/RealInterceptorChain;

    .line 16
    .line 17
    .line 18
    return-object p0
    .line 19
    .line 20
.end method

.class public final Lcom/intsig/camera/CameraView;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "CameraView.kt"

# interfaces
.implements Lcom/google/android/camera/ICamera;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ClickableViewAccessibility"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camera/CameraView$CallbackBridge;,
        Lcom/intsig/camera/CameraView$IOnTouchListener;,
        Lcom/intsig/camera/CameraView$Callback;,
        Lcom/intsig/camera/CameraView$CameraViewSavedState;,
        Lcom/intsig/camera/CameraView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇00O0:Lcom/intsig/camera/CameraView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:Lcom/google/android/camera/size/CameraSize;

.field private final O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O8o08O8O:Z

.field private volatile OO:Lcom/google/android/camera/PreviewImpl;

.field private OO〇00〇8oO:I

.field private final Oo80:Landroid/os/ConditionVariable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private volatile O〇o88o08〇:Z

.field private final o0:Lkotlinx/coroutines/flow/MutableSharedFlow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/flow/MutableSharedFlow<",
            "Lcom/google/android/camera/data/CameraImage;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8o:I

.field private o8oOOo:Lcom/google/android/camera/size/AspectRatio;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO0〇0o:I

.field private oOO〇〇:I

.field private oOo0:Lcom/google/android/camera/callback/OnAutoFocusCallback;

.field private oOo〇8o008:I

.field private oo8ooo8O:Z

.field private ooo0〇〇O:Z

.field private final o〇00O:Lcom/intsig/camera/CameraView$CallbackBridge;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇oO:I

.field private 〇080OO8〇0:J

.field private volatile 〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

.field private 〇08〇o0O:Z

.field private 〇0O:I

.field private 〇8〇oO〇〇8o:I

.field private 〇OOo8〇0:Z

.field private 〇O〇〇O8:Lcom/google/android/camera/data/CameraModel;

.field private 〇o0O:Lkotlinx/coroutines/CoroutineScope;

.field private 〇〇08O:F

.field private 〇〇o〇:Lcom/google/android/camera/view/FocusMarkerView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camera/CameraView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camera/CameraView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camera/CameraView;->〇00O0:Lcom/intsig/camera/CameraView$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/intsig/camera/CameraView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    sget-object v0, Lkotlinx/coroutines/channels/BufferOverflow;->DROP_OLDEST:Lkotlinx/coroutines/channels/BufferOverflow;

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 5
    invoke-static {v1, v2, v0}, Lkotlinx/coroutines/flow/SharedFlowKt;->〇080(IILkotlinx/coroutines/channels/BufferOverflow;)Lkotlinx/coroutines/flow/MutableSharedFlow;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camera/CameraView;->o0:Lkotlinx/coroutines/flow/MutableSharedFlow;

    .line 6
    iput-boolean v2, p0, Lcom/intsig/camera/CameraView;->〇OOo8〇0:Z

    .line 7
    new-instance v0, Lcom/intsig/camera/CameraView$CallbackBridge;

    invoke-direct {v0, p0}, Lcom/intsig/camera/CameraView$CallbackBridge;-><init>(Lcom/intsig/camera/CameraView;)V

    iput-object v0, p0, Lcom/intsig/camera/CameraView;->o〇00O:Lcom/intsig/camera/CameraView$CallbackBridge;

    const-wide/16 v3, 0x1388

    .line 8
    iput-wide v3, p0, Lcom/intsig/camera/CameraView;->〇080OO8〇0:J

    .line 9
    iput v2, p0, Lcom/intsig/camera/CameraView;->oOo〇8o008:I

    .line 10
    sget-object v0, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraApi$Companion;->O8()I

    move-result v3

    iput v3, p0, Lcom/intsig/camera/CameraView;->OO〇00〇8oO:I

    .line 11
    sget-object v3, Lcom/google/android/camera/data/CameraFacing;->〇o00〇〇Oo:Lcom/google/android/camera/data/CameraFacing$Companion;

    invoke-virtual {v3}, Lcom/google/android/camera/data/CameraFacing$Companion;->〇080()I

    move-result v4

    iput v4, p0, Lcom/intsig/camera/CameraView;->o8〇OO0〇0o:I

    .line 12
    sget-object v4, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    invoke-virtual {v4}, Lcom/google/android/camera/data/Flash$Companion;->〇080()I

    move-result v5

    iput v5, p0, Lcom/intsig/camera/CameraView;->〇8〇oO〇〇8o:I

    .line 13
    iput-boolean v2, p0, Lcom/intsig/camera/CameraView;->ooo0〇〇O:Z

    const/high16 v5, -0x40800000    # -1.0f

    .line 14
    iput v5, p0, Lcom/intsig/camera/CameraView;->〇〇08O:F

    .line 15
    sget-object v5, Lcom/google/android/camera/CameraConstants;->〇080:Lcom/google/android/camera/CameraConstants$Companion;

    invoke-virtual {v5}, Lcom/google/android/camera/CameraConstants$Companion;->〇o〇()Lcom/google/android/camera/size/AspectRatio;

    move-result-object v5

    iput-object v5, p0, Lcom/intsig/camera/CameraView;->o8oOOo:Lcom/google/android/camera/size/AspectRatio;

    .line 16
    new-instance v5, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    invoke-direct {v5}, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;-><init>()V

    iput-object v5, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 17
    sget-object v5, Lcom/google/android/camera/data/PreviewMode;->O8:Lcom/google/android/camera/data/PreviewMode$Companion;

    invoke-virtual {v5}, Lcom/google/android/camera/data/PreviewMode$Companion;->〇080()I

    move-result v6

    iput v6, p0, Lcom/intsig/camera/CameraView;->oOO〇〇:I

    const v6, -0xf69112

    .line 18
    iput v6, p0, Lcom/intsig/camera/CameraView;->o〇oO:I

    .line 19
    new-instance v7, Landroid/os/ConditionVariable;

    invoke-direct {v7}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v7, p0, Lcom/intsig/camera/CameraView;->Oo80:Landroid/os/ConditionVariable;

    .line 20
    sget-object v7, Lcom/intig/camera/R$styleable;->CameraView:[I

    .line 21
    sget v8, Lcom/intig/camera/R$style;->Widget_CameraView:I

    .line 22
    invoke-virtual {p1, p2, v7, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    const-string p3, "context.obtainStyledAttr\u2026dget_CameraView\n        )"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget p3, Lcom/intig/camera/R$styleable;->CameraView_cameraApi:I

    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraApi$Companion;->O8()I

    move-result v0

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p3

    iput p3, p0, Lcom/intsig/camera/CameraView;->OO〇00〇8oO:I

    .line 24
    sget p3, Lcom/intig/camera/R$styleable;->CameraView_previewMode:I

    invoke-virtual {v5}, Lcom/google/android/camera/data/PreviewMode$Companion;->〇080()I

    move-result v0

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p3

    iput p3, p0, Lcom/intsig/camera/CameraView;->oOO〇〇:I

    .line 25
    sget p3, Lcom/intig/camera/R$styleable;->CameraView_facing:I

    invoke-virtual {v3}, Lcom/google/android/camera/data/CameraFacing$Companion;->〇080()I

    move-result v0

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p3

    iput p3, p0, Lcom/intsig/camera/CameraView;->o8〇OO0〇0o:I

    .line 26
    sget p3, Lcom/intig/camera/R$styleable;->CameraView_android_adjustViewBounds:I

    invoke-virtual {p2, p3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    iput-boolean p3, p0, Lcom/intsig/camera/CameraView;->〇OOo8〇0:Z

    .line 27
    sget p3, Lcom/intig/camera/R$styleable;->CameraView_aspectRatio:I

    invoke-virtual {p2, p3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 28
    invoke-static {p3}, Lcom/google/android/camera/size/AspectRatio;->〇80〇808〇O(Ljava/lang/String;)Lcom/google/android/camera/size/AspectRatio;

    move-result-object p3

    const-string v0, "parse(aspectRatio)"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/intsig/camera/CameraView;->o8oOOo:Lcom/google/android/camera/size/AspectRatio;

    .line 29
    :cond_0
    sget p3, Lcom/intig/camera/R$styleable;->CameraView_autoFocus:I

    invoke-virtual {p2, p3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    iput-boolean p3, p0, Lcom/intsig/camera/CameraView;->ooo0〇〇O:Z

    .line 30
    sget p3, Lcom/intig/camera/R$styleable;->CameraView_touchFocus:I

    invoke-virtual {p2, p3, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    iput-boolean p3, p0, Lcom/intsig/camera/CameraView;->oo8ooo8O:Z

    .line 31
    sget p3, Lcom/intig/camera/R$styleable;->CameraView_focusColor:I

    invoke-virtual {p2, p3, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    iput p3, p0, Lcom/intsig/camera/CameraView;->o〇oO:I

    .line 32
    sget p3, Lcom/intig/camera/R$styleable;->CameraView_flash:I

    invoke-virtual {v4}, Lcom/google/android/camera/data/Flash$Companion;->〇080()I

    move-result v0

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p3

    iput p3, p0, Lcom/intsig/camera/CameraView;->〇8〇oO〇〇8o:I

    .line 33
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 34
    sget-object p2, Lcom/google/android/camera/CameraHelper;->〇080:Lcom/google/android/camera/CameraHelper;

    invoke-virtual {p2, p1}, Lcom/google/android/camera/CameraHelper;->OoO8(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 35
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getCoroutineScope()Lkotlinx/coroutines/CoroutineScope;

    move-result-object v0

    invoke-static {}, Lcom/google/android/camera/lifecycle/CameraDispatchers;->〇080()Lkotlinx/coroutines/ExecutorCoroutineDispatcher;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/intsig/camera/CameraView$2;

    const/4 p1, 0x0

    invoke-direct {v3, p0, p1}, Lcom/intsig/camera/CameraView$2;-><init>(Lcom/intsig/camera/CameraView;Lkotlin/coroutines/Continuation;)V

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    :cond_1
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 2
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camera/CameraView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic OO0o〇〇(Lcom/intsig/camera/CameraView;)Lcom/intsig/camera/CameraView$IOnTouchListener;
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    const/4 p0, 0x0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OO0o〇〇〇〇0(Lcom/intsig/camera/CameraView;)Lcom/google/android/camera/view/FocusMarkerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camera/CameraView;->〇〇o〇:Lcom/google/android/camera/view/FocusMarkerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic Oo08(Lcom/intsig/camera/CameraView;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camera/CameraView;->〇oo〇(Lcom/intsig/camera/CameraView;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camera/CameraView;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camera/CameraView;->oo88o8O(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
.end method

.method private static synthetic getMCameraCore$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private static synthetic getMCameraFacing$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private static synthetic getMFlashMode$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method private static synthetic getMOutputImageFormat$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public static final synthetic oO80(Lcom/intsig/camera/CameraView;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camera/CameraView;->o8o:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oo88o8O(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    instance-of v0, p1, Lcom/intsig/camera/CameraView$initCameraConfig$1;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move-object v0, p1

    .line 6
    check-cast v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;

    .line 7
    .line 8
    iget v1, v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;->〇08O〇00〇o:I

    .line 9
    .line 10
    const/high16 v2, -0x80000000

    .line 11
    .line 12
    and-int v3, v1, v2

    .line 13
    .line 14
    if-eqz v3, :cond_0

    .line 15
    .line 16
    sub-int/2addr v1, v2

    .line 17
    iput v1, v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;->〇08O〇00〇o:I

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    new-instance v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;

    .line 21
    .line 22
    invoke-direct {v0, p0, p1}, Lcom/intsig/camera/CameraView$initCameraConfig$1;-><init>(Lcom/intsig/camera/CameraView;Lkotlin/coroutines/Continuation;)V

    .line 23
    .line 24
    .line 25
    :goto_0
    iget-object p1, v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 26
    .line 27
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    iget v2, v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;->〇08O〇00〇o:I

    .line 32
    .line 33
    const/4 v3, 0x2

    .line 34
    const/4 v4, 0x1

    .line 35
    if-eqz v2, :cond_3

    .line 36
    .line 37
    if-eq v2, v4, :cond_2

    .line 38
    .line 39
    if-ne v2, v3, :cond_1

    .line 40
    .line 41
    iget-object v0, v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;->o0:Ljava/lang/Object;

    .line 42
    .line 43
    check-cast v0, Lcom/intsig/camera/CameraView;

    .line 44
    .line 45
    invoke-static {p1}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 46
    .line 47
    .line 48
    goto :goto_2

    .line 49
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 50
    .line 51
    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    .line 52
    .line 53
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    throw p1

    .line 57
    :cond_2
    iget-object v0, v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;->o0:Ljava/lang/Object;

    .line 58
    .line 59
    check-cast v0, Lcom/intsig/camera/CameraView;

    .line 60
    .line 61
    invoke-static {p1}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 62
    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_3
    invoke-static {p1}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 66
    .line 67
    .line 68
    iget-object p1, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 69
    .line 70
    if-eqz p1, :cond_4

    .line 71
    .line 72
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 73
    .line 74
    return-object p1

    .line 75
    :cond_4
    iget-boolean p1, p0, Lcom/intsig/camera/CameraView;->O〇o88o08〇:Z

    .line 76
    .line 77
    if-eqz p1, :cond_5

    .line 78
    .line 79
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 80
    .line 81
    return-object p1

    .line 82
    :cond_5
    iput-boolean v4, p0, Lcom/intsig/camera/CameraView;->O〇o88o08〇:Z

    .line 83
    .line 84
    new-array p1, v4, [Ljava/lang/Object;

    .line 85
    .line 86
    iget v2, p0, Lcom/intsig/camera/CameraView;->o8〇OO0〇0o:I

    .line 87
    .line 88
    invoke-static {v2}, Lkotlin/coroutines/jvm/internal/Boxing;->〇o〇(I)Ljava/lang/Integer;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    const/4 v5, 0x0

    .line 93
    aput-object v2, p1, v5

    .line 94
    .line 95
    const-string v2, "CameraX-CameraView"

    .line 96
    .line 97
    const-string v5, "chooseCameraFacing, facing = %s"

    .line 98
    .line 99
    invoke-static {v2, v5, p1}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    .line 101
    .line 102
    iget p1, p0, Lcom/intsig/camera/CameraView;->OO〇00〇8oO:I

    .line 103
    .line 104
    sget-object v2, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    .line 105
    .line 106
    invoke-virtual {v2}, Lcom/google/android/camera/data/CameraApi$Companion;->O8()I

    .line 107
    .line 108
    .line 109
    move-result v2

    .line 110
    if-ne p1, v2, :cond_7

    .line 111
    .line 112
    sget-object p1, Lcom/google/android/camera/CameraHelper;->〇080:Lcom/google/android/camera/CameraHelper;

    .line 113
    .line 114
    invoke-virtual {p1}, Lcom/google/android/camera/CameraHelper;->〇〇8O0〇8()I

    .line 115
    .line 116
    .line 117
    move-result p1

    .line 118
    sget-object v2, Lcom/intsig/camera/CameraConfig;->〇080:Lcom/intsig/camera/CameraConfig;

    .line 119
    .line 120
    iget v3, p0, Lcom/intsig/camera/CameraView;->o8〇OO0〇0o:I

    .line 121
    .line 122
    iput-object p0, v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;->o0:Ljava/lang/Object;

    .line 123
    .line 124
    iput v4, v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;->〇08O〇00〇o:I

    .line 125
    .line 126
    invoke-virtual {v2, p1, v3, v0}, Lcom/intsig/camera/CameraConfig;->Oo08(IILkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    if-ne p1, v1, :cond_6

    .line 131
    .line 132
    return-object v1

    .line 133
    :cond_6
    move-object v0, p0

    .line 134
    :goto_1
    check-cast p1, Lcom/google/android/camera/lifecycle/CameraUse;

    .line 135
    .line 136
    goto :goto_3

    .line 137
    :cond_7
    sget-object p1, Lcom/intsig/camera/CameraConfig;->〇080:Lcom/intsig/camera/CameraConfig;

    .line 138
    .line 139
    iget v2, p0, Lcom/intsig/camera/CameraView;->OO〇00〇8oO:I

    .line 140
    .line 141
    iget v4, p0, Lcom/intsig/camera/CameraView;->o8〇OO0〇0o:I

    .line 142
    .line 143
    iput-object p0, v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;->o0:Ljava/lang/Object;

    .line 144
    .line 145
    iput v3, v0, Lcom/intsig/camera/CameraView$initCameraConfig$1;->〇08O〇00〇o:I

    .line 146
    .line 147
    invoke-virtual {p1, v2, v4, v0}, Lcom/intsig/camera/CameraConfig;->Oo08(IILkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 148
    .line 149
    .line 150
    move-result-object p1

    .line 151
    if-ne p1, v1, :cond_8

    .line 152
    .line 153
    return-object v1

    .line 154
    :cond_8
    move-object v0, p0

    .line 155
    :goto_2
    check-cast p1, Lcom/google/android/camera/lifecycle/CameraUse;

    .line 156
    .line 157
    :goto_3
    invoke-virtual {p1}, Lcom/google/android/camera/lifecycle/CameraUse;->〇o00〇〇Oo()I

    .line 158
    .line 159
    .line 160
    move-result v1

    .line 161
    iput v1, v0, Lcom/intsig/camera/CameraView;->o8o:I

    .line 162
    .line 163
    invoke-virtual {p1}, Lcom/google/android/camera/lifecycle/CameraUse;->〇080()I

    .line 164
    .line 165
    .line 166
    move-result p1

    .line 167
    invoke-static {}, Lcom/google/android/camera/compat/imagereader/MainThreadAsyncHandler;->〇080()Landroid/os/Handler;

    .line 168
    .line 169
    .line 170
    move-result-object v1

    .line 171
    new-instance v2, Lcom/intsig/camera/〇080;

    .line 172
    .line 173
    invoke-direct {v2, v0, p1}, Lcom/intsig/camera/〇080;-><init>(Lcom/intsig/camera/CameraView;I)V

    .line 174
    .line 175
    .line 176
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 177
    .line 178
    .line 179
    iget-object p1, v0, Lcom/intsig/camera/CameraView;->Oo80:Landroid/os/ConditionVariable;

    .line 180
    .line 181
    invoke-virtual {p1}, Landroid/os/ConditionVariable;->close()V

    .line 182
    .line 183
    .line 184
    iget-object p1, v0, Lcom/intsig/camera/CameraView;->Oo80:Landroid/os/ConditionVariable;

    .line 185
    .line 186
    const-wide/16 v1, 0x3e8

    .line 187
    .line 188
    invoke-virtual {p1, v1, v2}, Landroid/os/ConditionVariable;->block(J)Z

    .line 189
    .line 190
    .line 191
    iget p1, v0, Lcom/intsig/camera/CameraView;->OO〇00〇8oO:I

    .line 192
    .line 193
    sget-object v1, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    .line 194
    .line 195
    invoke-virtual {v1}, Lcom/google/android/camera/data/CameraApi$Companion;->O8()I

    .line 196
    .line 197
    .line 198
    move-result v2

    .line 199
    if-ne p1, v2, :cond_9

    .line 200
    .line 201
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->getCameraApi()I

    .line 202
    .line 203
    .line 204
    move-result p1

    .line 205
    invoke-virtual {v1}, Lcom/google/android/camera/data/CameraApi$Companion;->O8()I

    .line 206
    .line 207
    .line 208
    move-result v1

    .line 209
    if-eq p1, v1, :cond_9

    .line 210
    .line 211
    sget-object v1, Lcom/google/android/camera/CameraHelper;->〇080:Lcom/google/android/camera/CameraHelper;

    .line 212
    .line 213
    invoke-virtual {v1, p1}, Lcom/google/android/camera/CameraHelper;->o〇〇0〇(I)V

    .line 214
    .line 215
    .line 216
    :cond_9
    invoke-direct {v0}, Lcom/intsig/camera/CameraView;->〇O888o0o()V

    .line 217
    .line 218
    .line 219
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 220
    .line 221
    return-object p1
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method public static final synthetic o〇0(Lcom/intsig/camera/CameraView;)Lkotlinx/coroutines/flow/MutableSharedFlow;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camera/CameraView;->o0:Lkotlinx/coroutines/flow/MutableSharedFlow;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇O8〇〇o()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camera/CameraView;->oo8ooo8O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇〇o〇:Lcom/google/android/camera/view/FocusMarkerView;

    .line 7
    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    return-void

    .line 11
    :cond_1
    new-instance v0, Lcom/google/android/camera/view/FocusMarkerView;

    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const-string v2, "context"

    .line 18
    .line 19
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/4 v2, 0x2

    .line 23
    const/4 v3, 0x0

    .line 24
    invoke-direct {v0, v1, v3, v2, v3}, Lcom/google/android/camera/view/FocusMarkerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 25
    .line 26
    .line 27
    iput-object v0, p0, Lcom/intsig/camera/CameraView;->〇〇o〇:Lcom/google/android/camera/view/FocusMarkerView;

    .line 28
    .line 29
    iget v1, p0, Lcom/intsig/camera/CameraView;->o〇oO:I

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Lcom/google/android/camera/view/FocusMarkerView;->setFocusColor(I)V

    .line 32
    .line 33
    .line 34
    new-instance v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 35
    .line 36
    const/4 v1, -0x1

    .line 37
    invoke-direct {v0, v1, v1}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    .line 38
    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camera/CameraView;->〇〇o〇:Lcom/google/android/camera/view/FocusMarkerView;

    .line 41
    .line 42
    if-nez v1, :cond_2

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 46
    .line 47
    .line 48
    :goto_0
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇〇o〇:Lcom/google/android/camera/view/FocusMarkerView;

    .line 49
    .line 50
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 51
    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇〇o〇:Lcom/google/android/camera/view/FocusMarkerView;

    .line 54
    .line 55
    if-eqz v0, :cond_3

    .line 56
    .line 57
    new-instance v1, Lcom/intsig/camera/CameraView$initFocusView$1;

    .line 58
    .line 59
    invoke-direct {v1, p0}, Lcom/intsig/camera/CameraView$initFocusView$1;-><init>(Lcom/intsig/camera/CameraView;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 63
    .line 64
    .line 65
    :cond_3
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
.end method

.method private final 〇0〇O0088o(II)Lcom/google/android/camera/PreviewImpl;
    .locals 2

    .line 1
    sget-object v0, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraApi$Companion;->o〇0()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const-string v1, "CameraX-CameraView"

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    const-string p1, "createPreviewImpl, camerax use CameraXPreview"

    .line 12
    .line 13
    invoke-static {v1, p1}, Lcom/google/android/camera/log/CameraLog;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    new-instance p1, Lcom/intsig/android/camerax/CameraXPreview;

    .line 17
    .line 18
    invoke-direct {p1, p0, p2}, Lcom/intsig/android/camerax/CameraXPreview;-><init>(Landroid/view/ViewGroup;I)V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const-string p1, "createPreviewImpl, other use CameraCompatPreview"

    .line 23
    .line 24
    invoke-static {v1, p1}, Lcom/google/android/camera/log/CameraLog;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    new-instance p1, Lcom/google/android/camera/compat/view/CameraCompatPreview;

    .line 28
    .line 29
    invoke-direct {p1, p0, p2}, Lcom/google/android/camera/compat/view/CameraCompatPreview;-><init>(Landroid/view/ViewGroup;I)V

    .line 30
    .line 31
    .line 32
    :goto_0
    return-object p1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camera/CameraView;)Lcom/google/android/camera/CameraViewImpl;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camera/CameraView;)Lcom/google/android/camera/PreviewImpl;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camera/CameraView;->OO:Lcom/google/android/camera/PreviewImpl;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O888o0o()V
    .locals 4

    .line 1
    const-string v0, "initCamera start"

    .line 2
    .line 3
    const-string v1, "CameraX-CameraView"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/google/android/camera/log/CameraLog;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 9
    .line 10
    if-eqz v0, :cond_4

    .line 11
    .line 12
    iget v2, p0, Lcom/intsig/camera/CameraView;->o8〇OO0〇0o:I

    .line 13
    .line 14
    invoke-virtual {v0, v2}, Lcom/google/android/camera/CameraViewImpl;->〇O(I)Z

    .line 15
    .line 16
    .line 17
    iget-boolean v2, p0, Lcom/intsig/camera/CameraView;->oo8ooo8O:Z

    .line 18
    .line 19
    invoke-virtual {v0, v2}, Lcom/google/android/camera/CameraViewImpl;->O〇OO(Z)V

    .line 20
    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/camera/CameraView;->o8oOOo:Lcom/google/android/camera/size/AspectRatio;

    .line 23
    .line 24
    invoke-virtual {v0, v2}, Lcom/google/android/camera/CameraViewImpl;->o8O〇(Lcom/google/android/camera/size/AspectRatio;)Z

    .line 25
    .line 26
    .line 27
    iget-boolean v2, p0, Lcom/intsig/camera/CameraView;->ooo0〇〇O:Z

    .line 28
    .line 29
    invoke-virtual {v0, v2}, Lcom/google/android/camera/CameraViewImpl;->ooOO(Z)Z

    .line 30
    .line 31
    .line 32
    iget v2, p0, Lcom/intsig/camera/CameraView;->〇8〇oO〇〇8o:I

    .line 33
    .line 34
    sget-object v3, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 35
    .line 36
    invoke-virtual {v3}, Lcom/google/android/camera/data/Flash$Companion;->〇o00〇〇Oo()I

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    if-le v2, v3, :cond_0

    .line 41
    .line 42
    iget v2, p0, Lcom/intsig/camera/CameraView;->〇8〇oO〇〇8o:I

    .line 43
    .line 44
    invoke-virtual {v0, v2}, Lcom/google/android/camera/CameraViewImpl;->〇000O0(I)Z

    .line 45
    .line 46
    .line 47
    :cond_0
    iget v2, p0, Lcom/intsig/camera/CameraView;->〇〇08O:F

    .line 48
    .line 49
    const/high16 v3, -0x40800000    # -1.0f

    .line 50
    .line 51
    cmpl-float v3, v2, v3

    .line 52
    .line 53
    if-lez v3, :cond_1

    .line 54
    .line 55
    invoke-virtual {v0, v2}, Lcom/google/android/camera/CameraViewImpl;->O〇08(F)Z

    .line 56
    .line 57
    .line 58
    :cond_1
    iget-object v2, p0, Lcom/intsig/camera/CameraView;->O0O:Lcom/google/android/camera/size/CameraSize;

    .line 59
    .line 60
    if-eqz v2, :cond_2

    .line 61
    .line 62
    invoke-virtual {v0, v2}, Lcom/google/android/camera/CameraViewImpl;->O〇Oooo〇〇(Lcom/google/android/camera/size/CameraSize;)Z

    .line 63
    .line 64
    .line 65
    :cond_2
    iget-object v2, p0, Lcom/intsig/camera/CameraView;->〇O〇〇O8:Lcom/google/android/camera/data/CameraModel;

    .line 66
    .line 67
    if-eqz v2, :cond_3

    .line 68
    .line 69
    invoke-interface {v0, v2}, Lcom/google/android/camera/ICamera;->setCameraModel(Lcom/google/android/camera/data/CameraModel;)V

    .line 70
    .line 71
    .line 72
    :cond_3
    iget v2, p0, Lcom/intsig/camera/CameraView;->oOo〇8o008:I

    .line 73
    .line 74
    invoke-virtual {v0, v2}, Lcom/google/android/camera/CameraViewImpl;->O880oOO08(I)V

    .line 75
    .line 76
    .line 77
    iget-boolean v2, p0, Lcom/intsig/camera/CameraView;->O8o08O8O:Z

    .line 78
    .line 79
    invoke-virtual {v0, v2}, Lcom/google/android/camera/CameraViewImpl;->〇O00(Z)V

    .line 80
    .line 81
    .line 82
    iget-object v2, p0, Lcom/intsig/camera/CameraView;->oOo0:Lcom/google/android/camera/callback/OnAutoFocusCallback;

    .line 83
    .line 84
    invoke-virtual {p0, v2}, Lcom/intsig/camera/CameraView;->setAutoFocusCallback(Lcom/google/android/camera/callback/OnAutoFocusCallback;)V

    .line 85
    .line 86
    .line 87
    iget-wide v2, p0, Lcom/intsig/camera/CameraView;->〇080OO8〇0:J

    .line 88
    .line 89
    invoke-virtual {v0, v2, v3}, Lcom/google/android/camera/CameraViewImpl;->O0(J)V

    .line 90
    .line 91
    .line 92
    iget v2, p0, Lcom/intsig/camera/CameraView;->〇0O:I

    .line 93
    .line 94
    invoke-virtual {v0, v2}, Lcom/google/android/camera/CameraViewImpl;->OOo0O(I)V

    .line 95
    .line 96
    .line 97
    sget-object v2, Lcom/google/android/camera/CameraHelper;->〇080:Lcom/google/android/camera/CameraHelper;

    .line 98
    .line 99
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->getContext()Landroid/content/Context;

    .line 100
    .line 101
    .line 102
    move-result-object v3

    .line 103
    invoke-virtual {v2, v3}, Lcom/google/android/camera/CameraHelper;->OoO8(Landroid/content/Context;)Z

    .line 104
    .line 105
    .line 106
    move-result v2

    .line 107
    if-eqz v2, :cond_4

    .line 108
    .line 109
    iget v2, p0, Lcom/intsig/camera/CameraView;->o8o:I

    .line 110
    .line 111
    invoke-virtual {v0, v2}, Lcom/google/android/camera/CameraViewImpl;->〇O8o08O(I)V

    .line 112
    .line 113
    .line 114
    :cond_4
    const-string v0, "initCamera end"

    .line 115
    .line 116
    invoke-static {v1, v0}, Lcom/google/android/camera/log/CameraLog;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
.end method

.method public static final synthetic 〇O8o08O(Lcom/intsig/camera/CameraView;)Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇oo〇(Lcom/intsig/camera/CameraView;I)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "CameraX-CameraView"

    .line 7
    .line 8
    const-string v1, "createPreviewImpl in main thread"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/google/android/camera/log/CameraLog;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget v0, p0, Lcom/intsig/camera/CameraView;->oOO〇〇:I

    .line 14
    .line 15
    invoke-direct {p0, p1, v0}, Lcom/intsig/camera/CameraView;->〇0〇O0088o(II)Lcom/google/android/camera/PreviewImpl;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iput-object v0, p0, Lcom/intsig/camera/CameraView;->OO:Lcom/google/android/camera/PreviewImpl;

    .line 20
    .line 21
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v1, "context"

    .line 26
    .line 27
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/camera/CameraView;->OO:Lcom/google/android/camera/PreviewImpl;

    .line 31
    .line 32
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 33
    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/camera/CameraView;->o〇00O:Lcom/intsig/camera/CameraView$CallbackBridge;

    .line 36
    .line 37
    invoke-direct {p0, v0, v1, v2, p1}, Lcom/intsig/camera/CameraView;->〇〇8O0〇8(Landroid/content/Context;Lcom/google/android/camera/PreviewImpl;Lcom/intsig/camera/CameraView$CallbackBridge;I)Lcom/google/android/camera/CameraViewImpl;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    iput-object p1, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camera/CameraView;->o〇O8〇〇o()V

    .line 44
    .line 45
    .line 46
    iget-object p0, p0, Lcom/intsig/camera/CameraView;->Oo80:Landroid/os/ConditionVariable;

    .line 47
    .line 48
    invoke-virtual {p0}, Landroid/os/ConditionVariable;->open()V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
.end method

.method public static final synthetic 〇〇808〇(Lcom/intsig/camera/CameraView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camera/CameraView;->〇08〇o0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
.end method

.method public static final synthetic 〇〇888(Lcom/intsig/camera/CameraView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camera/CameraView;->〇08〇o0O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇8O0〇8(Landroid/content/Context;Lcom/google/android/camera/PreviewImpl;Lcom/intsig/camera/CameraView$CallbackBridge;I)Lcom/google/android/camera/CameraViewImpl;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ObsoleteSdkInt"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraApi$Companion;->o〇0()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x0

    .line 8
    const/4 v3, 0x1

    .line 9
    const-string v4, "CameraX-CameraView"

    .line 10
    .line 11
    if-ne p4, v1, :cond_0

    .line 12
    .line 13
    new-array p4, v3, [Ljava/lang/Object;

    .line 14
    .line 15
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 16
    .line 17
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    aput-object v0, p4, v2

    .line 22
    .line 23
    const-string v0, "createCameraViewImpl, sdk version = %d, create CameraX"

    .line 24
    .line 25
    invoke-static {v4, v0, p4}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    .line 27
    .line 28
    new-instance p4, Lcom/intsig/android/camerax/CameraX;

    .line 29
    .line 30
    invoke-direct {p4, p1, p3, p2}, Lcom/intsig/android/camerax/CameraX;-><init>(Landroid/content/Context;Lcom/google/android/camera/CameraViewImpl$Callback;Lcom/google/android/camera/PreviewImpl;)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraApi$Companion;->〇o00〇〇Oo()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-ne p4, v1, :cond_1

    .line 39
    .line 40
    new-array p4, v3, [Ljava/lang/Object;

    .line 41
    .line 42
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 43
    .line 44
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    aput-object v0, p4, v2

    .line 49
    .line 50
    const-string v0, "createCameraViewImpl, sdk version = %d, create Camera2"

    .line 51
    .line 52
    invoke-static {v4, v0, p4}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    .line 54
    .line 55
    new-instance p4, Lcom/google/android/camera/Camera2;

    .line 56
    .line 57
    invoke-direct {p4, p1, p3, p2}, Lcom/google/android/camera/Camera2;-><init>(Landroid/content/Context;Lcom/google/android/camera/CameraViewImpl$Callback;Lcom/google/android/camera/PreviewImpl;)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraApi$Companion;->〇080()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    if-ne p4, v0, :cond_2

    .line 66
    .line 67
    new-array p4, v3, [Ljava/lang/Object;

    .line 68
    .line 69
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 70
    .line 71
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    aput-object v0, p4, v2

    .line 76
    .line 77
    const-string v0, "createCameraViewImpl, sdk version = %d, create Camera1 (for no set)"

    .line 78
    .line 79
    invoke-static {v4, v0, p4}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 80
    .line 81
    .line 82
    new-instance p4, Lcom/google/android/camera/Camera1;

    .line 83
    .line 84
    invoke-direct {p4, p1, p3, p2}, Lcom/google/android/camera/Camera1;-><init>(Landroid/content/Context;Lcom/google/android/camera/CameraViewImpl$Callback;Lcom/google/android/camera/PreviewImpl;)V

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_2
    sget-object v0, Lcom/intsig/camera/CameraConfig;->〇080:Lcom/intsig/camera/CameraConfig;

    .line 89
    .line 90
    invoke-virtual {v0, p1, p4, p3, p2}, Lcom/intsig/camera/CameraConfig;->o〇0(Landroid/content/Context;ILcom/google/android/camera/CameraViewImpl$Callback;Lcom/google/android/camera/PreviewImpl;)Lcom/google/android/camera/CameraViewImpl;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    if-eqz v0, :cond_3

    .line 95
    .line 96
    move-object p4, v0

    .line 97
    goto :goto_0

    .line 98
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 99
    .line 100
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .line 102
    .line 103
    const-string v1, "createCameraViewImpl api = "

    .line 104
    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    const-string p4, " failed, create Camera1 (for set Camera1)"

    .line 112
    .line 113
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object p4

    .line 120
    new-array v0, v3, [Ljava/lang/Object;

    .line 121
    .line 122
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 123
    .line 124
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 125
    .line 126
    .line 127
    move-result-object v1

    .line 128
    aput-object v1, v0, v2

    .line 129
    .line 130
    invoke-static {v4, p4, v0}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    .line 132
    .line 133
    new-instance p4, Lcom/google/android/camera/Camera1;

    .line 134
    .line 135
    invoke-direct {p4, p1, p3, p2}, Lcom/google/android/camera/Camera1;-><init>(Landroid/content/Context;Lcom/google/android/camera/CameraViewImpl$Callback;Lcom/google/android/camera/PreviewImpl;)V

    .line 136
    .line 137
    .line 138
    :goto_0
    return-object p4
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
.end method


# virtual methods
.method public O8(Lcom/google/android/camera/size/AspectRatio;)Z
    .locals 3
    .param p1    # Lcom/google/android/camera/size/AspectRatio;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "ratio"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    new-array v0, v0, [Ljava/lang/Object;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/google/android/camera/size/AspectRatio;->toString()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x0

    .line 14
    aput-object v1, v0, v2

    .line 15
    .line 16
    const-string v1, "CameraX-CameraView"

    .line 17
    .line 18
    const-string v2, "setAspectRatio, ratio = %s"

    .line 19
    .line 20
    invoke-static {v1, v2, v0}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camera/CameraView;->o8oOOo:Lcom/google/android/camera/size/AspectRatio;

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 26
    .line 27
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v0, v1, p0, p0, p1}, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;->〇80〇808〇O(Landroid/content/Context;Lcom/google/android/camera/ICamera;Landroid/view/View;Lcom/google/android/camera/size/AspectRatio;)Z

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    return p1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public O8ooOoo〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->Ooo()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-static {p0}, Lcom/google/android/camera/ICamera$DefaultImpls;->oO80(Lcom/google/android/camera/ICamera;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
.end method

.method public O8〇o()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->〇0()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-static {p0}, Lcom/google/android/camera/ICamera$DefaultImpls;->Oooo8o0〇(Lcom/google/android/camera/ICamera;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
.end method

.method public OOO〇O0()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->OO8oO0o〇()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-static {p0}, Lcom/google/android/camera/ICamera$DefaultImpls;->〇O8o08O(Lcom/google/android/camera/ICamera;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
.end method

.method public Oo8Oo00oo()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->Oo〇O8o〇8()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public OoO8(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camera/CameraView;->O8o08O8O:Z

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/google/android/camera/CameraViewImpl;->〇O00(Z)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O〇8O8〇008()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->〇80()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-static {p0}, Lcom/google/android/camera/ICamera$DefaultImpls;->〇〇888(Lcom/google/android/camera/ICamera;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
.end method

.method public getAspectRatio()Lcom/google/android/camera/size/AspectRatio;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->〇0〇O0088o()Lcom/google/android/camera/size/AspectRatio;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    :cond_0
    sget-object v0, Lcom/google/android/camera/CameraConstants;->〇080:Lcom/google/android/camera/CameraConstants$Companion;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/google/android/camera/CameraConstants$Companion;->〇o〇()Lcom/google/android/camera/size/AspectRatio;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :cond_1
    return-object v0
.end method

.method public final getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->OO:Lcom/google/android/camera/PreviewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/PreviewImpl;->〇o00〇〇Oo()Landroid/graphics/Bitmap;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getCameraApi()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/google/android/camera/ICamera;->getCameraApi()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getCameraFacing()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->getCameraFacing()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    iget v0, p0, Lcom/intsig/camera/CameraView;->o8〇OO0〇0o:I

    .line 11
    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final getCameraFacingNum()I
    .locals 3

    .line 1
    :try_start_0
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    .line 2
    .line 3
    .line 4
    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    return v0

    .line 6
    :catch_0
    move-exception v0

    .line 7
    const-string v1, "CameraX-CameraView"

    .line 8
    .line 9
    const-string v2, "cameraFacingNum"

    .line 10
    .line 11
    invoke-static {v1, v2, v0}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 12
    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    return v0
    .line 16
    .line 17
.end method

.method public final getCameraImageFlow()Lkotlinx/coroutines/flow/MutableSharedFlow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/MutableSharedFlow<",
            "Lcom/google/android/camera/data/CameraImage;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->o0:Lkotlinx/coroutines/flow/MutableSharedFlow;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getCameraViewImpl()Lcom/google/android/camera/CameraViewImpl;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getCoroutineScope()Lkotlinx/coroutines/CoroutineScope;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->o800o8O()Lkotlinx/coroutines/CoroutineScope;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    :cond_0
    sget-object v0, Lkotlinx/coroutines/GlobalScope;->o0:Lkotlinx/coroutines/GlobalScope;

    .line 12
    .line 13
    :cond_1
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getCurrentPictureSize()Lcom/google/android/camera/size/CameraSize;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->〇O888o0o()Lcom/google/android/camera/size/CameraSize;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getCurrentPreviewSize()Lcom/google/android/camera/size/CameraSize;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->oo88o8O()Lcom/google/android/camera/size/CameraSize;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getFlash()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/google/android/camera/ICamera;->getFlash()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    sget-object v0, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/google/android/camera/data/Flash$Companion;->O8()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    :goto_0
    return v0
    .line 17
.end method

.method public getLinearZoom()F
    .locals 1
    .annotation build Landroidx/annotation/FloatRange;
        from = 0.0
        to = 1.0
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->〇oo〇()F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getMaxZoom()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/google/android/camera/ICamera;->getMaxZoom()F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 11
    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getMinZoom()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/google/android/camera/ICamera;->getMinZoom()F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 11
    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final getPreview()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->OO:Lcom/google/android/camera/PreviewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/PreviewImpl;->〇o〇()Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final getPreviewView()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->OO:Lcom/google/android/camera/PreviewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/PreviewImpl;->Oo08()Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getSupportedAllPictureSize()Lcom/google/android/camera/size/CameraSizeMap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->〇8〇0〇o〇O()Lcom/google/android/camera/size/CameraSizeMap;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getSupportedAllPreviewSize()Lcom/google/android/camera/size/CameraSizeMap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->O〇O〇oO()Lcom/google/android/camera/size/CameraSizeMap;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getSupportedAspectRatios()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/google/android/camera/size/AspectRatio;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/google/android/camera/ICamera;->getSupportedAspectRatios()Ljava/util/Set;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getSupportedPictureSize()Ljava/util/SortedSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedSet<",
            "Lcom/google/android/camera/size/CameraSize;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->o8oO〇()Ljava/util/SortedSet;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getTouchFocus()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->o〇O()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getZoomRange()[F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/google/android/camera/ICamera;->getZoomRange()[F

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public getZoomRatio()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->oO00OOO()F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 11
    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public o0ooO()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->〇0O〇Oo()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public o8(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/google/android/camera/CameraViewImpl;->O0〇OO8(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o800o8O(Z)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/google/android/camera/CameraViewImpl;->〇〇8O0〇8(Z)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/camera/ICamera$DefaultImpls;->O8(Lcom/google/android/camera/ICamera;Z)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    :goto_0
    return p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->isInEditMode()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-super {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->onMeasure(II)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camera/CameraView;->〇OOo8〇0:Z

    .line 12
    .line 13
    const/high16 v1, 0x40000000    # 2.0f

    .line 14
    .line 15
    if-eqz v0, :cond_6

    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->〇o〇()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->o〇00O:Lcom/intsig/camera/CameraView$CallbackBridge;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView$CallbackBridge;->〇O8o08O()V

    .line 26
    .line 27
    .line 28
    invoke-super {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->onMeasure(II)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    const/high16 v3, -0x80000000

    .line 41
    .line 42
    if-ne v0, v1, :cond_3

    .line 43
    .line 44
    if-eq v2, v1, :cond_3

    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getAspectRatio()Lcom/google/android/camera/size/AspectRatio;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 51
    .line 52
    .line 53
    move-result v4

    .line 54
    int-to-float v4, v4

    .line 55
    invoke-virtual {v0}, Lcom/google/android/camera/size/AspectRatio;->Oooo8o0〇()F

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    mul-float v4, v4, v0

    .line 60
    .line 61
    float-to-int v0, v4

    .line 62
    if-ne v2, v3, :cond_2

    .line 63
    .line 64
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 65
    .line 66
    .line 67
    move-result p2

    .line 68
    invoke-static {v0, p2}, Lkotlin/ranges/RangesKt;->o〇0(II)I

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    :cond_2
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 73
    .line 74
    .line 75
    move-result p2

    .line 76
    invoke-super {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->onMeasure(II)V

    .line 77
    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_3
    if-eq v0, v1, :cond_5

    .line 81
    .line 82
    if-ne v2, v1, :cond_5

    .line 83
    .line 84
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getAspectRatio()Lcom/google/android/camera/size/AspectRatio;

    .line 85
    .line 86
    .line 87
    move-result-object v2

    .line 88
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 89
    .line 90
    .line 91
    move-result v4

    .line 92
    int-to-float v4, v4

    .line 93
    invoke-virtual {v2}, Lcom/google/android/camera/size/AspectRatio;->Oooo8o0〇()F

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    mul-float v4, v4, v2

    .line 98
    .line 99
    float-to-int v2, v4

    .line 100
    if-ne v0, v3, :cond_4

    .line 101
    .line 102
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 103
    .line 104
    .line 105
    move-result p1

    .line 106
    invoke-static {v2, p1}, Lkotlin/ranges/RangesKt;->o〇0(II)I

    .line 107
    .line 108
    .line 109
    move-result v2

    .line 110
    :cond_4
    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 111
    .line 112
    .line 113
    move-result p1

    .line 114
    invoke-super {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->onMeasure(II)V

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :cond_5
    invoke-super {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->onMeasure(II)V

    .line 119
    .line 120
    .line 121
    goto :goto_0

    .line 122
    :cond_6
    invoke-super {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->onMeasure(II)V

    .line 123
    .line 124
    .line 125
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    .line 126
    .line 127
    .line 128
    move-result p1

    .line 129
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    .line 130
    .line 131
    .line 132
    move-result p2

    .line 133
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getAspectRatio()Lcom/google/android/camera/size/AspectRatio;

    .line 134
    .line 135
    .line 136
    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lcom/google/android/camera/size/AspectRatio;->O8()I

    .line 138
    .line 139
    .line 140
    move-result v2

    .line 141
    mul-int v2, v2, p1

    .line 142
    .line 143
    invoke-virtual {v0}, Lcom/google/android/camera/size/AspectRatio;->〇o〇()I

    .line 144
    .line 145
    .line 146
    move-result v3

    .line 147
    div-int/2addr v2, v3

    .line 148
    if-ge p2, v2, :cond_7

    .line 149
    .line 150
    iget-object p2, p0, Lcom/intsig/camera/CameraView;->OO:Lcom/google/android/camera/PreviewImpl;

    .line 151
    .line 152
    if-eqz p2, :cond_8

    .line 153
    .line 154
    invoke-virtual {p2}, Lcom/google/android/camera/PreviewImpl;->〇o〇()Landroid/view/View;

    .line 155
    .line 156
    .line 157
    move-result-object p2

    .line 158
    if-eqz p2, :cond_8

    .line 159
    .line 160
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 161
    .line 162
    .line 163
    move-result v2

    .line 164
    invoke-virtual {v0}, Lcom/google/android/camera/size/AspectRatio;->O8()I

    .line 165
    .line 166
    .line 167
    move-result v3

    .line 168
    mul-int p1, p1, v3

    .line 169
    .line 170
    invoke-virtual {v0}, Lcom/google/android/camera/size/AspectRatio;->〇o〇()I

    .line 171
    .line 172
    .line 173
    move-result v0

    .line 174
    div-int/2addr p1, v0

    .line 175
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 176
    .line 177
    .line 178
    move-result p1

    .line 179
    invoke-virtual {p2, v2, p1}, Landroid/view/View;->measure(II)V

    .line 180
    .line 181
    .line 182
    goto :goto_1

    .line 183
    :cond_7
    iget-object p1, p0, Lcom/intsig/camera/CameraView;->OO:Lcom/google/android/camera/PreviewImpl;

    .line 184
    .line 185
    if-eqz p1, :cond_8

    .line 186
    .line 187
    invoke-virtual {p1}, Lcom/google/android/camera/PreviewImpl;->〇o〇()Landroid/view/View;

    .line 188
    .line 189
    .line 190
    move-result-object p1

    .line 191
    if-eqz p1, :cond_8

    .line 192
    .line 193
    invoke-virtual {v0}, Lcom/google/android/camera/size/AspectRatio;->〇o〇()I

    .line 194
    .line 195
    .line 196
    move-result v2

    .line 197
    mul-int v2, v2, p2

    .line 198
    .line 199
    invoke-virtual {v0}, Lcom/google/android/camera/size/AspectRatio;->O8()I

    .line 200
    .line 201
    .line 202
    move-result v0

    .line 203
    div-int/2addr v2, v0

    .line 204
    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 205
    .line 206
    .line 207
    move-result v0

    .line 208
    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 209
    .line 210
    .line 211
    move-result p2

    .line 212
    invoke-virtual {p1, v0, p2}, Landroid/view/View;->measure(II)V

    .line 213
    .line 214
    .line 215
    :cond_8
    :goto_1
    return-void
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 5
    .param p1    # Landroid/os/Parcelable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "state"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    instance-of v0, p1, Lcom/intsig/camera/CameraView$CameraViewSavedState;

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 11
    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    check-cast p1, Lcom/intsig/camera/CameraView$CameraViewSavedState;

    .line 15
    .line 16
    invoke-virtual {p1}, Landroid/view/AbsSavedState;->getSuperState()Landroid/os/Parcelable;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camera/CameraView$CameraViewSavedState;->〇o00〇〇Oo()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    invoke-virtual {p0, v0}, Lcom/intsig/camera/CameraView;->setCameraFacing(I)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/camera/CameraView$CameraViewSavedState;->O8()Lcom/google/android/camera/size/AspectRatio;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    if-eqz v1, :cond_1

    .line 35
    .line 36
    invoke-virtual {p0, v1}, Lcom/intsig/camera/CameraView;->O8(Lcom/google/android/camera/size/AspectRatio;)Z

    .line 37
    .line 38
    .line 39
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camera/CameraView$CameraViewSavedState;->〇080()Z

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    invoke-virtual {p0, v1}, Lcom/intsig/camera/CameraView;->setAutoFocus(Z)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/camera/CameraView$CameraViewSavedState;->Oo08()Z

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    invoke-virtual {p0, v1}, Lcom/intsig/camera/CameraView;->setTouchFocus(Z)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/camera/CameraView$CameraViewSavedState;->〇o〇()I

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    invoke-virtual {p0, p1}, Lcom/intsig/camera/CameraView;->setFlash(I)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->〇00()Z

    .line 61
    .line 62
    .line 63
    move-result p1

    .line 64
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getFlash()I

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getAspectRatio()Lcom/google/android/camera/size/AspectRatio;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    const/4 v3, 0x5

    .line 73
    new-array v3, v3, [Ljava/lang/Object;

    .line 74
    .line 75
    const/4 v4, 0x0

    .line 76
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    aput-object v0, v3, v4

    .line 81
    .line 82
    const/4 v0, 0x1

    .line 83
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    aput-object p1, v3, v0

    .line 88
    .line 89
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getTouchFocus()Z

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    const/4 v0, 0x2

    .line 98
    aput-object p1, v3, v0

    .line 99
    .line 100
    const/4 p1, 0x3

    .line 101
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    aput-object v0, v3, p1

    .line 106
    .line 107
    const/4 p1, 0x4

    .line 108
    aput-object v2, v3, p1

    .line 109
    .line 110
    const-string p1, "CameraX-CameraView"

    .line 111
    .line 112
    const-string v0, "onRestoreInstanceState: facing = %d, autofocus = %s, touchfocus = %s, flash = %d, ratio = %s"

    .line 113
    .line 114
    invoke-static {p1, v0, v3}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    .line 116
    .line 117
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 7
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->〇00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getFlash()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getAspectRatio()Lcom/google/android/camera/size/AspectRatio;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getCameraFacing()I

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    const/4 v4, 0x5

    .line 18
    new-array v4, v4, [Ljava/lang/Object;

    .line 19
    .line 20
    const/4 v5, 0x0

    .line 21
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object v6

    .line 25
    aput-object v6, v4, v5

    .line 26
    .line 27
    const/4 v5, 0x1

    .line 28
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 29
    .line 30
    .line 31
    move-result-object v6

    .line 32
    aput-object v6, v4, v5

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getTouchFocus()Z

    .line 35
    .line 36
    .line 37
    move-result v5

    .line 38
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 39
    .line 40
    .line 41
    move-result-object v5

    .line 42
    const/4 v6, 0x2

    .line 43
    aput-object v5, v4, v6

    .line 44
    .line 45
    const/4 v5, 0x3

    .line 46
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 47
    .line 48
    .line 49
    move-result-object v6

    .line 50
    aput-object v6, v4, v5

    .line 51
    .line 52
    const/4 v5, 0x4

    .line 53
    aput-object v2, v4, v5

    .line 54
    .line 55
    const-string v5, "CameraX-CameraView"

    .line 56
    .line 57
    const-string v6, "onSaveInstanceState: facing = %d, autofocus = %s, touchfocus = %s, flash = %d, ratio = %s"

    .line 58
    .line 59
    invoke-static {v5, v6, v4}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    .line 61
    .line 62
    new-instance v4, Lcom/intsig/camera/CameraView$CameraViewSavedState;

    .line 63
    .line 64
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    .line 65
    .line 66
    .line 67
    move-result-object v5

    .line 68
    invoke-direct {v4, v5}, Lcom/intsig/camera/CameraView$CameraViewSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v4, v3}, Lcom/intsig/camera/CameraView$CameraViewSavedState;->OO0o〇〇〇〇0(I)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {v4, v2}, Lcom/intsig/camera/CameraView$CameraViewSavedState;->〇O00(Lcom/google/android/camera/size/AspectRatio;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v4, v0}, Lcom/intsig/camera/CameraView$CameraViewSavedState;->〇80〇808〇O(Z)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getTouchFocus()Z

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    invoke-virtual {v4, v0}, Lcom/intsig/camera/CameraView$CameraViewSavedState;->〇0〇O0088o(Z)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {v4, v1}, Lcom/intsig/camera/CameraView$CameraViewSavedState;->Oooo8o0〇(I)V

    .line 88
    .line 89
    .line 90
    return-object v4
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
.end method

.method public oo〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->o0O0()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-static {p0}, Lcom/google/android/camera/ICamera$DefaultImpls;->OO0o〇〇(Lcom/google/android/camera/ICamera;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
.end method

.method public o〇8()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getCoroutineScope()Lkotlinx/coroutines/CoroutineScope;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-static {}, Lcom/google/android/camera/lifecycle/CameraDispatchers;->〇080()Lkotlinx/coroutines/ExecutorCoroutineDispatcher;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v3, 0x0

    .line 14
    new-instance v4, Lcom/intsig/camera/CameraView$start$1;

    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    invoke-direct {v4, p0, v0}, Lcom/intsig/camera/CameraView$start$1;-><init>(Lcom/intsig/camera/CameraView;Lkotlin/coroutines/Continuation;)V

    .line 18
    .line 19
    .line 20
    const/4 v5, 0x2

    .line 21
    const/4 v6, 0x0

    .line 22
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 27
    .line 28
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    iget v2, p0, Lcom/intsig/camera/CameraView;->o8o:I

    .line 33
    .line 34
    invoke-virtual {v0, v1, p0, v2}, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;->〇080(Landroid/content/Context;Lcom/google/android/camera/ICamera;I)V

    .line 35
    .line 36
    .line 37
    :goto_0
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method public o〇〇0〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->O0o〇〇Oo()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-static {p0}, Lcom/google/android/camera/ICamera$DefaultImpls;->〇8o8o〇(Lcom/google/android/camera/ICamera;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
.end method

.method public setAutoCancelDuration(J)V
    .locals 1

    .line 1
    iput-wide p1, p0, Lcom/intsig/camera/CameraView;->〇080OO8〇0:J

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0, p1, p2}, Lcom/google/android/camera/CameraViewImpl;->O0(J)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setAutoFocus(Z)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    aput-object v2, v0, v1

    .line 10
    .line 11
    const-string v1, "CameraX-CameraView"

    .line 12
    .line 13
    const-string v2, "setAutoFocus, autoFocus = %s"

    .line 14
    .line 15
    invoke-static {v1, v2, v0}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    .line 17
    .line 18
    iput-boolean p1, p0, Lcom/intsig/camera/CameraView;->ooo0〇〇O:Z

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 21
    .line 22
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v0, v1, p0, p1}, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;->OO0o〇〇〇〇0(Landroid/content/Context;Lcom/google/android/camera/ICamera;Z)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final setAutoFocusCallback(Lcom/google/android/camera/callback/OnAutoFocusCallback;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/camera/CameraView;->oOo0:Lcom/google/android/camera/callback/OnAutoFocusCallback;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/google/android/camera/CameraViewImpl;->OOo8o〇O(Lcom/google/android/camera/callback/OnAutoFocusCallback;)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setCameraFacing(I)V
    .locals 3

    .line 1
    sget-object v0, Lcom/google/android/camera/data/CameraFacing;->〇o00〇〇Oo:Lcom/google/android/camera/data/CameraFacing$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraFacing$Companion;->〇080()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-ne p1, v1, :cond_0

    .line 8
    .line 9
    const-string v0, "back"

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraFacing$Companion;->〇o00〇〇Oo()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-ne p1, v1, :cond_1

    .line 17
    .line 18
    const-string v0, "front"

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraFacing$Companion;->〇o〇()I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-ne p1, v0, :cond_2

    .line 26
    .line 27
    const-string v0, "wide"

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_2
    const-string v0, "null"

    .line 31
    .line 32
    :goto_0
    iput p1, p0, Lcom/intsig/camera/CameraView;->o8〇OO0〇0o:I

    .line 33
    .line 34
    const/4 v1, 0x1

    .line 35
    new-array v1, v1, [Ljava/lang/Object;

    .line 36
    .line 37
    const/4 v2, 0x0

    .line 38
    aput-object v0, v1, v2

    .line 39
    .line 40
    const-string v0, "CameraX-CameraView"

    .line 41
    .line 42
    const-string v2, "setCameraFacing, facing = %s"

    .line 43
    .line 44
    invoke-static {v0, v2, v1}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 48
    .line 49
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-virtual {v0, v1, p0, p1}, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;->〇8o8o〇(Landroid/content/Context;Lcom/google/android/camera/ICamera;I)V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public setCameraModel(Lcom/google/android/camera/data/CameraModel;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/camera/CameraView;->〇O〇〇O8:Lcom/google/android/camera/data/CameraModel;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, v1, p0, p1}, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;->〇O8o08O(Landroid/content/Context;Lcom/google/android/camera/ICamera;Lcom/google/android/camera/data/CameraModel;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setCoroutineScope(Lkotlinx/coroutines/CoroutineScope;)V
    .locals 1
    .param p1    # Lkotlinx/coroutines/CoroutineScope;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "scope"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camera/CameraView;->〇o0O:Lkotlinx/coroutines/CoroutineScope;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Lcom/google/android/camera/CameraViewImpl;->〇00O0O0(Lkotlinx/coroutines/CoroutineScope;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDisplayOrientation(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/google/android/camera/ICamera;->setDisplayOrientation(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setFlash(I)V
    .locals 3

    .line 1
    iput p1, p0, Lcom/intsig/camera/CameraView;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    new-array v0, v0, [Ljava/lang/Object;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    aput-object v2, v0, v1

    .line 12
    .line 13
    const-string v1, "CameraX-CameraView"

    .line 14
    .line 15
    const-string v2, "setFlash, flash = %d (0-off,1-on,2-torch,3-auto)"

    .line 16
    .line 17
    invoke-static {v1, v2, v0}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 21
    .line 22
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v0, v1, p0, p1}, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;->OO0o〇〇(Landroid/content/Context;Lcom/google/android/camera/ICamera;I)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public setLinearZoom(F)V
    .locals 3
    .param p1    # F
        .annotation build Landroidx/annotation/FloatRange;
            from = 0.0
            to = 1.0
        .end annotation
    .end param

    .line 1
    const/4 v0, 0x0

    .line 2
    const/high16 v1, 0x3f800000    # 1.0f

    .line 3
    .line 4
    invoke-static {p1, v0, v1}, Lcom/google/android/camera/util/CameraExtKt;->〇o〇(FFF)F

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getMinZoom()F

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {p0}, Lcom/intsig/camera/CameraView;->getMaxZoom()F

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    sget-object v2, Lcom/google/android/camera/util/CameraSizeUtils;->〇080:Lcom/google/android/camera/util/CameraSizeUtils$Companion;

    .line 17
    .line 18
    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/camera/util/CameraSizeUtils$Companion;->〇〇8O0〇8(FFF)F

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    iput v0, p0, Lcom/intsig/camera/CameraView;->〇〇08O:F

    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 25
    .line 26
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1, p0, p1}, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;->〇〇808〇(Landroid/content/Context;Lcom/google/android/camera/ICamera;F)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public setOutputImageFormat(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camera/CameraView;->oOo〇8o008:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/google/android/camera/CameraViewImpl;->O880oOO08(I)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPhotoJpegCompressionQuality(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camera/CameraView;->〇0O:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/google/android/camera/CameraViewImpl;->OOo0O(I)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPictureSize(Lcom/google/android/camera/size/CameraSize;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/camera/CameraView;->O0O:Lcom/google/android/camera/size/CameraSize;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, v1, p0, p1}, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;->〇O〇(Landroid/content/Context;Lcom/google/android/camera/ICamera;Lcom/google/android/camera/size/CameraSize;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setTouchCallback(Lcom/intsig/camera/CameraView$IOnTouchListener;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setTouchFocus(Z)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    aput-object v2, v0, v1

    .line 10
    .line 11
    const-string v1, "CameraX-CameraView"

    .line 12
    .line 13
    const-string v2, "setTapFocus, touchFocus = %s"

    .line 14
    .line 15
    invoke-static {v1, v2, v0}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 19
    .line 20
    if-nez v0, :cond_0

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/camera/CameraViewImpl;->O〇OO(Z)V

    .line 24
    .line 25
    .line 26
    :goto_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public setZoomRatio(F)V
    .locals 2

    .line 1
    iput p1, p0, Lcom/intsig/camera/CameraView;->〇〇08O:F

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, v1, p0, p1}, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;->〇O00(Landroid/content/Context;Lcom/google/android/camera/ICamera;F)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇00()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->O000()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public 〇0000OOO()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->OOO()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-static {p0}, Lcom/google/android/camera/ICamera$DefaultImpls;->OO0o〇〇〇〇0(Lcom/google/android/camera/ICamera;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
.end method

.method public 〇00〇8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->O0O8OO088()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public 〇080(Ljava/lang/Integer;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/google/android/camera/ICamera;->〇080(Ljava/lang/Integer;)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    :goto_0
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O00()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->OO0o〇〇〇〇0()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public final 〇O〇(Lcom/intsig/camera/CameraView$Callback;)V
    .locals 1
    .param p1    # Lcom/intsig/camera/CameraView$Callback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "callback"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->o〇00O:Lcom/intsig/camera/CameraView$CallbackBridge;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/camera/CameraView$CallbackBridge;->〇8o8o〇(Lcom/intsig/camera/CameraView$Callback;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->〇〇o8()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public 〇o00〇〇Oo(I)V
    .locals 2

    .line 1
    sget-object v0, Lcom/google/android/camera/CameraHelper;->〇080:Lcom/google/android/camera/CameraHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/android/camera/CameraHelper;->O8()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->O88O:Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;

    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-virtual {v0, v1, p0, p1}, Lcom/google/android/camera/lifecycle/LifecycleCameraRepository;->〇0〇O0088o(Landroid/content/Context;Lcom/google/android/camera/ICamera;I)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇oOO8O8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->〇O〇80o08O()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-static {p0}, Lcom/google/android/camera/ICamera$DefaultImpls;->〇80〇808〇O(Lcom/google/android/camera/ICamera;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
.end method

.method public 〇o〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/google/android/camera/ICamera;->〇o〇()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method

.method public 〇〇〇0〇〇0(FFIIII)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camera/CameraView;->〇08O〇00〇o:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move v1, p1

    .line 6
    move v2, p2

    .line 7
    move v3, p3

    .line 8
    move v4, p4

    .line 9
    move v5, p5

    .line 10
    move v6, p6

    .line 11
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/camera/CameraViewImpl;->oO〇(FFIIII)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
.end method

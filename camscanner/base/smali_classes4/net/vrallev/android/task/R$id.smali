.class public final Lnet/vrallev/android/task/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/vrallev/android/task/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action0:I = 0x7f0a0051

.field public static final action_bar:I = 0x7f0a005a

.field public static final action_bar_activity_content:I = 0x7f0a005b

.field public static final action_bar_container:I = 0x7f0a005c

.field public static final action_bar_root:I = 0x7f0a0060

.field public static final action_bar_spinner:I = 0x7f0a0061

.field public static final action_bar_subtitle:I = 0x7f0a0062

.field public static final action_bar_title:I = 0x7f0a0063

.field public static final action_context_bar:I = 0x7f0a0067

.field public static final action_divider:I = 0x7f0a0068

.field public static final action_menu_divider:I = 0x7f0a006b

.field public static final action_menu_presenter:I = 0x7f0a006c

.field public static final action_mode_bar:I = 0x7f0a006d

.field public static final action_mode_bar_stub:I = 0x7f0a006e

.field public static final action_mode_close_button:I = 0x7f0a006f

.field public static final activity_chooser_view_content:I = 0x7f0a0075

.field public static final alertTitle:I = 0x7f0a0133

.field public static final always:I = 0x7f0a0137

.field public static final beginning:I = 0x7f0a01d1

.field public static final buttonPanel:I = 0x7f0a02e2

.field public static final cancel_action:I = 0x7f0a02ef

.field public static final checkbox:I = 0x7f0a035f

.field public static final chronometer:I = 0x7f0a0365

.field public static final collapseActionView:I = 0x7f0a047a

.field public static final contentPanel:I = 0x7f0a04ae

.field public static final custom:I = 0x7f0a04ed

.field public static final customPanel:I = 0x7f0a04ee

.field public static final decor_content_parent:I = 0x7f0a0509

.field public static final default_activity_button:I = 0x7f0a050a

.field public static final disableHome:I = 0x7f0a052c

.field public static final edit_query:I = 0x7f0a0573

.field public static final end:I = 0x7f0a0581

.field public static final end_padder:I = 0x7f0a0584

.field public static final expand_activities_button:I = 0x7f0a05d1

.field public static final expanded_menu:I = 0x7f0a05d2

.field public static final home:I = 0x7f0a0707

.field public static final homeAsUp:I = 0x7f0a0708

.field public static final icon:I = 0x7f0a071f

.field public static final ifRoom:I = 0x7f0a0726

.field public static final image:I = 0x7f0a0730

.field public static final info:I = 0x7f0a078d

.field public static final line1:I = 0x7f0a0b47

.field public static final line3:I = 0x7f0a0b4a

.field public static final listMode:I = 0x7f0a0b59

.field public static final list_item:I = 0x7f0a0b5c

.field public static final media_actions:I = 0x7f0a0da4

.field public static final middle:I = 0x7f0a0db8

.field public static final multiply:I = 0x7f0a0dea

.field public static final never:I = 0x7f0a0dfc

.field public static final none:I = 0x7f0a0e06

.field public static final normal:I = 0x7f0a0e07

.field public static final parentPanel:I = 0x7f0a0e51

.field public static final progress_circular:I = 0x7f0a0e9a

.field public static final progress_horizontal:I = 0x7f0a0e9b

.field public static final radio:I = 0x7f0a0ece

.field public static final screen:I = 0x7f0a1096

.field public static final scrollView:I = 0x7f0a109b

.field public static final search_badge:I = 0x7f0a10a7

.field public static final search_bar:I = 0x7f0a10a8

.field public static final search_button:I = 0x7f0a10aa

.field public static final search_close_btn:I = 0x7f0a10ab

.field public static final search_edit_frame:I = 0x7f0a10ac

.field public static final search_go_btn:I = 0x7f0a10ad

.field public static final search_mag_icon:I = 0x7f0a10af

.field public static final search_plate:I = 0x7f0a10b0

.field public static final search_src_text:I = 0x7f0a10b1

.field public static final search_voice_btn:I = 0x7f0a10bf

.field public static final select_dialog_listview:I = 0x7f0a10c3

.field public static final shortcut:I = 0x7f0a10d4

.field public static final showCustom:I = 0x7f0a10d5

.field public static final showHome:I = 0x7f0a10d6

.field public static final showTitle:I = 0x7f0a10d7

.field public static final split_action_bar:I = 0x7f0a1116

.field public static final src_atop:I = 0x7f0a111b

.field public static final src_in:I = 0x7f0a111c

.field public static final src_over:I = 0x7f0a111d

.field public static final status_bar_latest_event_content:I = 0x7f0a112a

.field public static final submit_area:I = 0x7f0a1155

.field public static final tabMode:I = 0x7f0a1174

.field public static final text:I = 0x7f0a119d

.field public static final text2:I = 0x7f0a119f

.field public static final textSpacerNoButtons:I = 0x7f0a11a3

.field public static final time:I = 0x7f0a11c8

.field public static final title:I = 0x7f0a11d1

.field public static final title_template:I = 0x7f0a11d6

.field public static final topPanel:I = 0x7f0a11ef

.field public static final up:I = 0x7f0a1971

.field public static final useLogo:I = 0x7f0a197b

.field public static final withText:I = 0x7f0a1ac4

.field public static final wrap_content:I = 0x7f0a1acd


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

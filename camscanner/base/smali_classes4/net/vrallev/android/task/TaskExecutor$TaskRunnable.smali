.class final Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;
.super Ljava/lang/Object;
.source "TaskExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/vrallev/android/task/TaskExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TaskRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;",
        "Landroid/app/Application$ActivityLifecycleCallbacks;"
    }
.end annotation


# instance fields
.field private final mTask:Lnet/vrallev/android/task/Task;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/vrallev/android/task/Task<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final mWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lnet/vrallev/android/task/TaskCacheFragmentInterface;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lnet/vrallev/android/task/TaskExecutor;


# direct methods
.method private constructor <init>(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lnet/vrallev/android/task/Task<",
            "TT;>;",
            "Lnet/vrallev/android/task/TaskCacheFragmentInterface;",
            ")V"
        }
    .end annotation

    .line 2
    iput-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 4
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mWeakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method synthetic constructor <init>(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;Lnet/vrallev/android/task/TaskCacheFragmentInterface;Lnet/vrallev/android/task/TaskExecutor$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;-><init>(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V

    return-void
.end method

.method static synthetic access$500(Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;)Lnet/vrallev/android/task/Task;
    .locals 0

    .line 1
    iget-object p0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private postResult(Ljava/lang/Object;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lnet/vrallev/android/task/TaskCacheFragmentInterface;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 2
    .line 3
    invoke-virtual {v0}, Lnet/vrallev/android/task/TaskExecutor;->isShutdown()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 10
    .line 11
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 12
    .line 13
    invoke-static {p1, p2}, Lnet/vrallev/android/task/TaskExecutor;->access$100(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;)V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 17
    .line 18
    invoke-static {p1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {p1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 23
    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 27
    .line 28
    invoke-static {v0}, Lnet/vrallev/android/task/TaskExecutor;->access$300(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TargetMethodFinder;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 33
    .line 34
    invoke-static {v1}, Lnet/vrallev/android/task/TaskExecutor;->access$300(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TargetMethodFinder;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    iget-object v2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 39
    .line 40
    invoke-virtual {v1, p1, v2}, Lnet/vrallev/android/task/TargetMethodFinder;->getResultType(Ljava/lang/Object;Lnet/vrallev/android/task/Task;)Ljava/lang/Class;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    iget-object v2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 45
    .line 46
    invoke-virtual {v0, p2, v1, v2}, Lnet/vrallev/android/task/TargetMethodFinder;->getMethod(Lnet/vrallev/android/task/TaskCacheFragmentInterface;Ljava/lang/Class;Lnet/vrallev/android/task/Task;)Landroid/util/Pair;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    if-nez v0, :cond_1

    .line 51
    .line 52
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 53
    .line 54
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 55
    .line 56
    invoke-static {p1, p2}, Lnet/vrallev/android/task/TaskExecutor;->access$100(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;)V

    .line 57
    .line 58
    .line 59
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 60
    .line 61
    invoke-static {p1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    invoke-virtual {p1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 66
    .line 67
    .line 68
    return-void

    .line 69
    :cond_1
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 70
    .line 71
    invoke-static {v1}, Lnet/vrallev/android/task/TaskExecutor;->access$400(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TaskExecutor$PostResult;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    sget-object v2, Lnet/vrallev/android/task/TaskExecutor$PostResult;->IMMEDIATELY:Lnet/vrallev/android/task/TaskExecutor$PostResult;

    .line 76
    .line 77
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    if-eqz v1, :cond_2

    .line 82
    .line 83
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 84
    .line 85
    invoke-static {p2}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    .line 86
    .line 87
    .line 88
    move-result-object p2

    .line 89
    invoke-virtual {p2, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 90
    .line 91
    .line 92
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 93
    .line 94
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 95
    .line 96
    invoke-virtual {p2, v0, p1, v1}, Lnet/vrallev/android/task/TaskExecutor;->postResultNow(Landroid/util/Pair;Ljava/lang/Object;Lnet/vrallev/android/task/Task;)V

    .line 97
    .line 98
    .line 99
    return-void

    .line 100
    :cond_2
    invoke-interface {p2}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->canSaveInstanceState()Z

    .line 101
    .line 102
    .line 103
    move-result v1

    .line 104
    if-eqz v1, :cond_4

    .line 105
    .line 106
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 107
    .line 108
    invoke-static {v1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    invoke-virtual {v1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 113
    .line 114
    .line 115
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 116
    .line 117
    invoke-static {v1}, Lnet/vrallev/android/task/TaskExecutor;->access$400(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TaskExecutor$PostResult;

    .line 118
    .line 119
    .line 120
    move-result-object v1

    .line 121
    sget-object v2, Lnet/vrallev/android/task/TaskExecutor$PostResult;->ON_ANY_THREAD:Lnet/vrallev/android/task/TaskExecutor$PostResult;

    .line 122
    .line 123
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 124
    .line 125
    .line 126
    move-result v1

    .line 127
    if-eqz v1, :cond_3

    .line 128
    .line 129
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 130
    .line 131
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 132
    .line 133
    invoke-virtual {p2, v0, p1, v1}, Lnet/vrallev/android/task/TaskExecutor;->postResultNow(Landroid/util/Pair;Ljava/lang/Object;Lnet/vrallev/android/task/Task;)V

    .line 134
    .line 135
    .line 136
    goto :goto_0

    .line 137
    :cond_3
    invoke-interface {p2}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->getParentActivity()Landroid/app/Activity;

    .line 138
    .line 139
    .line 140
    move-result-object p2

    .line 141
    new-instance v1, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable$1;

    .line 142
    .line 143
    invoke-direct {v1, p0, v0, p1}, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable$1;-><init>(Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;Landroid/util/Pair;Ljava/lang/Object;)V

    .line 144
    .line 145
    .line 146
    invoke-virtual {p2, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 147
    .line 148
    .line 149
    goto :goto_0

    .line 150
    :cond_4
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 151
    .line 152
    invoke-static {v0}, Lnet/vrallev/android/task/TaskExecutor;->access$300(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TargetMethodFinder;

    .line 153
    .line 154
    .line 155
    move-result-object v0

    .line 156
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 157
    .line 158
    invoke-virtual {v0, p1, v1}, Lnet/vrallev/android/task/TargetMethodFinder;->getResultType(Ljava/lang/Object;Lnet/vrallev/android/task/Task;)Ljava/lang/Class;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    if-eqz v0, :cond_5

    .line 163
    .line 164
    new-instance v1, Lnet/vrallev/android/task/TaskPendingResult;

    .line 165
    .line 166
    iget-object v2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 167
    .line 168
    iget-object v3, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 169
    .line 170
    invoke-direct {v1, v0, p1, v2, v3}, Lnet/vrallev/android/task/TaskPendingResult;-><init>(Ljava/lang/Class;Ljava/lang/Object;Lnet/vrallev/android/task/Task;Lnet/vrallev/android/task/TaskExecutor;)V

    .line 171
    .line 172
    .line 173
    invoke-interface {p2, v1}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->putPendingResult(Lnet/vrallev/android/task/TaskPendingResult;)V

    .line 174
    .line 175
    .line 176
    :cond_5
    :goto_0
    return-void
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2

    .line 1
    if-eqz p2, :cond_3

    .line 2
    .line 3
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 4
    .line 5
    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->isExecuting()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 13
    .line 14
    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->getKey()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const/4 v1, -0x1

    .line 23
    invoke-virtual {p2, v0, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;I)I

    .line 24
    .line 25
    .line 26
    move-result p2

    .line 27
    if-ne p2, v1, :cond_1

    .line 28
    .line 29
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 30
    .line 31
    invoke-static {p1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-virtual {p1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_1
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 40
    .line 41
    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->getKey()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-eq p2, v0, :cond_2

    .line 46
    .line 47
    return-void

    .line 48
    :cond_2
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 49
    .line 50
    invoke-static {p2}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    .line 51
    .line 52
    .line 53
    move-result-object p2

    .line 54
    invoke-virtual {p2, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 55
    .line 56
    .line 57
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 58
    .line 59
    invoke-static {p2}, Lnet/vrallev/android/task/TaskExecutor;->access$600(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    .line 60
    .line 61
    .line 62
    move-result-object p2

    .line 63
    invoke-interface {p2, p1}, Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;->create(Landroid/app/Activity;)Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    :try_start_0
    iget-object p2, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 68
    .line 69
    invoke-virtual {p2}, Lnet/vrallev/android/task/Task;->getResult()Ljava/lang/Object;

    .line 70
    .line 71
    .line 72
    move-result-object p2

    .line 73
    invoke-direct {p0, p2, p1}, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->postResult(Ljava/lang/Object;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    .line 75
    .line 76
    :catch_0
    :cond_3
    :goto_0
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mWeakReference:Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-interface {v0}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->getParentActivity()Landroid/app/Activity;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eq v0, p1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 19
    .line 20
    invoke-virtual {p1}, Lnet/vrallev/android/task/Task;->getKey()I

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 29
    .line 30
    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->getKey()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    invoke-virtual {p2, p1, v0}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 35
    .line 36
    .line 37
    :cond_1
    :goto_0
    return-void
    .line 38
    .line 39
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 2
    .line 3
    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->isExecuting()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 11
    .line 12
    invoke-static {v0}, Lnet/vrallev/android/task/TaskExecutor;->access$600(Lnet/vrallev/android/task/TaskExecutor;)Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-interface {v0, p1}, Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;->create(Landroid/app/Activity;)Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const-string v0, "PENDING_RESULT_KEY"

    .line 21
    .line 22
    invoke-interface {p1, v0}, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->get(Ljava/lang/String;)Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    check-cast p1, Ljava/util/List;

    .line 27
    .line 28
    if-eqz p1, :cond_1

    .line 29
    .line 30
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    if-nez p1, :cond_1

    .line 35
    .line 36
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 37
    .line 38
    invoke-static {p1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-virtual {p1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 43
    .line 44
    .line 45
    :cond_1
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    iget-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 8
    .line 9
    invoke-static {p1}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-virtual {p1, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public run()V
    .locals 3

    .line 1
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 2
    .line 3
    invoke-virtual {v0}, Lnet/vrallev/android/task/Task;->executeInner()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mTask:Lnet/vrallev/android/task/Task;

    .line 8
    .line 9
    instance-of v2, v1, Lnet/vrallev/android/task/TaskNoCallback;

    .line 10
    .line 11
    if-eqz v2, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 14
    .line 15
    invoke-static {v0, v1}, Lnet/vrallev/android/task/TaskExecutor;->access$100(Lnet/vrallev/android/task/TaskExecutor;Lnet/vrallev/android/task/Task;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->this$0:Lnet/vrallev/android/task/TaskExecutor;

    .line 19
    .line 20
    invoke-static {v0}, Lnet/vrallev/android/task/TaskExecutor;->access$200(Lnet/vrallev/android/task/TaskExecutor;)Landroid/app/Application;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0, p0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->mWeakReference:Ljava/lang/ref/WeakReference;

    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    check-cast v1, Lnet/vrallev/android/task/TaskCacheFragmentInterface;

    .line 35
    .line 36
    if-eqz v1, :cond_1

    .line 37
    .line 38
    invoke-direct {p0, v0, v1}, Lnet/vrallev/android/task/TaskExecutor$TaskRunnable;->postResult(Ljava/lang/Object;Lnet/vrallev/android/task/TaskCacheFragmentInterface;)V

    .line 39
    .line 40
    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

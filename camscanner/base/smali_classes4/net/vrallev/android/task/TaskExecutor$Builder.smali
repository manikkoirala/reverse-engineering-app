.class public Lnet/vrallev/android/task/TaskExecutor$Builder;
.super Ljava/lang/Object;
.source "TaskExecutor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/vrallev/android/task/TaskExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mCacheFactory:Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

.field private mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private mPostResult:Lnet/vrallev/android/task/TaskExecutor$PostResult;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
.end method


# virtual methods
.method public build()Lnet/vrallev/android/task/TaskExecutor;
    .locals 5

    .line 1
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mPostResult:Lnet/vrallev/android/task/TaskExecutor$PostResult;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lnet/vrallev/android/task/TaskExecutor$PostResult;->UI_THREAD:Lnet/vrallev/android/task/TaskExecutor$PostResult;

    .line 6
    .line 7
    iput-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mPostResult:Lnet/vrallev/android/task/TaskExecutor$PostResult;

    .line 8
    .line 9
    :cond_0
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 10
    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 18
    .line 19
    :cond_1
    iget-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mCacheFactory:Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    .line 20
    .line 21
    if-nez v0, :cond_2

    .line 22
    .line 23
    sget-object v0, Lnet/vrallev/android/task/TaskCacheFragmentInterface;->DEFAULT_FACTORY:Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    .line 24
    .line 25
    iput-object v0, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mCacheFactory:Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    .line 26
    .line 27
    :cond_2
    new-instance v0, Lnet/vrallev/android/task/TaskExecutor;

    .line 28
    .line 29
    iget-object v1, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 30
    .line 31
    iget-object v2, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mPostResult:Lnet/vrallev/android/task/TaskExecutor$PostResult;

    .line 32
    .line 33
    iget-object v3, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mCacheFactory:Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    .line 34
    .line 35
    const/4 v4, 0x0

    .line 36
    invoke-direct {v0, v1, v2, v3, v4}, Lnet/vrallev/android/task/TaskExecutor;-><init>(Ljava/util/concurrent/ExecutorService;Lnet/vrallev/android/task/TaskExecutor$PostResult;Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;Lnet/vrallev/android/task/TaskExecutor$1;)V

    .line 37
    .line 38
    .line 39
    return-object v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
.end method

.method public setCacheFactory(Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;)Lnet/vrallev/android/task/TaskExecutor$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mCacheFactory:Lnet/vrallev/android/task/TaskCacheFragmentInterface$Factory;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setExecutorService(Ljava/util/concurrent/ExecutorService;)Lnet/vrallev/android/task/TaskExecutor$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPostResult(Lnet/vrallev/android/task/TaskExecutor$PostResult;)Lnet/vrallev/android/task/TaskExecutor$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lnet/vrallev/android/task/TaskExecutor$Builder;->mPostResult:Lnet/vrallev/android/task/TaskExecutor$PostResult;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

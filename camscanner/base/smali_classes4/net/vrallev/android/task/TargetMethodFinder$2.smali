.class final Lnet/vrallev/android/task/TargetMethodFinder$2;
.super Landroidx/collection/LruCache;
.source "TargetMethodFinder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/vrallev/android/task/TargetMethodFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/collection/LruCache<",
        "Lnet/vrallev/android/task/MethodHolderKey;",
        "Lnet/vrallev/android/task/MethodHolder;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Landroidx/collection/LruCache;-><init>(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method protected bridge synthetic create(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, Lnet/vrallev/android/task/MethodHolderKey;

    invoke-virtual {p0, p1}, Lnet/vrallev/android/task/TargetMethodFinder$2;->create(Lnet/vrallev/android/task/MethodHolderKey;)Lnet/vrallev/android/task/MethodHolder;

    move-result-object p1

    return-object p1
.end method

.method protected create(Lnet/vrallev/android/task/MethodHolderKey;)Lnet/vrallev/android/task/MethodHolder;
    .locals 0

    .line 2
    invoke-static {p1}, Lnet/vrallev/android/task/TargetMethodFinder;->access$100(Lnet/vrallev/android/task/MethodHolderKey;)Ljava/lang/reflect/Method;

    move-result-object p1

    invoke-static {p1}, Lnet/vrallev/android/task/MethodHolder;->obtain(Ljava/lang/reflect/Method;)Lnet/vrallev/android/task/MethodHolder;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lnet/vrallev/android/task/MethodHolderKey;

    check-cast p3, Lnet/vrallev/android/task/MethodHolder;

    check-cast p4, Lnet/vrallev/android/task/MethodHolder;

    invoke-virtual {p0, p1, p2, p3, p4}, Lnet/vrallev/android/task/TargetMethodFinder$2;->entryRemoved(ZLnet/vrallev/android/task/MethodHolderKey;Lnet/vrallev/android/task/MethodHolder;Lnet/vrallev/android/task/MethodHolder;)V

    return-void
.end method

.method protected entryRemoved(ZLnet/vrallev/android/task/MethodHolderKey;Lnet/vrallev/android/task/MethodHolder;Lnet/vrallev/android/task/MethodHolder;)V
    .locals 0

    .line 2
    invoke-virtual {p2}, Lnet/vrallev/android/task/MethodHolderKey;->recycle()V

    if-eqz p3, :cond_0

    .line 3
    invoke-virtual {p3}, Lnet/vrallev/android/task/MethodHolder;->recycle()V

    :cond_0
    return-void
.end method

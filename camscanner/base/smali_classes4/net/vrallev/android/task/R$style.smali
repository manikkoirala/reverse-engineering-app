.class public final Lnet/vrallev/android/task/R$style;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/vrallev/android/task/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AlertDialog_AppCompat:I = 0x7f14000b

.field public static final AlertDialog_AppCompat_Light:I = 0x7f14000c

.field public static final Animation_AppCompat_Dialog:I = 0x7f14000e

.field public static final Animation_AppCompat_DropDownUp:I = 0x7f14000f

.field public static final Base_AlertDialog_AppCompat:I = 0x7f14003a

.field public static final Base_AlertDialog_AppCompat_Light:I = 0x7f14003b

.field public static final Base_Animation_AppCompat_Dialog:I = 0x7f14003c

.field public static final Base_Animation_AppCompat_DropDownUp:I = 0x7f14003d

.field public static final Base_DialogWindowTitleBackground_AppCompat:I = 0x7f140041

.field public static final Base_DialogWindowTitle_AppCompat:I = 0x7f140040

.field public static final Base_TextAppearance_AppCompat:I = 0x7f140045

.field public static final Base_TextAppearance_AppCompat_Body1:I = 0x7f140046

.field public static final Base_TextAppearance_AppCompat_Body2:I = 0x7f140047

.field public static final Base_TextAppearance_AppCompat_Button:I = 0x7f140048

.field public static final Base_TextAppearance_AppCompat_Caption:I = 0x7f140049

.field public static final Base_TextAppearance_AppCompat_Display1:I = 0x7f14004a

.field public static final Base_TextAppearance_AppCompat_Display2:I = 0x7f14004b

.field public static final Base_TextAppearance_AppCompat_Display3:I = 0x7f14004c

.field public static final Base_TextAppearance_AppCompat_Display4:I = 0x7f14004d

.field public static final Base_TextAppearance_AppCompat_Headline:I = 0x7f14004e

.field public static final Base_TextAppearance_AppCompat_Inverse:I = 0x7f14004f

.field public static final Base_TextAppearance_AppCompat_Large:I = 0x7f140050

.field public static final Base_TextAppearance_AppCompat_Large_Inverse:I = 0x7f140051

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f140052

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f140053

.field public static final Base_TextAppearance_AppCompat_Medium:I = 0x7f140054

.field public static final Base_TextAppearance_AppCompat_Medium_Inverse:I = 0x7f140055

.field public static final Base_TextAppearance_AppCompat_Menu:I = 0x7f140056

.field public static final Base_TextAppearance_AppCompat_SearchResult:I = 0x7f140057

.field public static final Base_TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f140058

.field public static final Base_TextAppearance_AppCompat_SearchResult_Title:I = 0x7f140059

.field public static final Base_TextAppearance_AppCompat_Small:I = 0x7f14005a

.field public static final Base_TextAppearance_AppCompat_Small_Inverse:I = 0x7f14005b

.field public static final Base_TextAppearance_AppCompat_Subhead:I = 0x7f14005c

.field public static final Base_TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f14005d

.field public static final Base_TextAppearance_AppCompat_Title:I = 0x7f14005e

.field public static final Base_TextAppearance_AppCompat_Title_Inverse:I = 0x7f14005f

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f140061

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f140062

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f140063

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f140064

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f140065

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f140066

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f140067

.field public static final Base_TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f14006c

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f14006e

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f14006f

.field public static final Base_TextAppearance_AppCompat_Widget_Switch:I = 0x7f140070

.field public static final Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I = 0x7f140071

.field public static final Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f140077

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f140078

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f140079

.field public static final Base_ThemeOverlay_AppCompat:I = 0x7f1400a3

.field public static final Base_ThemeOverlay_AppCompat_ActionBar:I = 0x7f1400a4

.field public static final Base_ThemeOverlay_AppCompat_Dark:I = 0x7f1400a5

.field public static final Base_ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f1400a6

.field public static final Base_ThemeOverlay_AppCompat_Light:I = 0x7f1400a9

.field public static final Base_Theme_AppCompat:I = 0x7f14007a

.field public static final Base_Theme_AppCompat_CompactMenu:I = 0x7f14007b

.field public static final Base_Theme_AppCompat_Dialog:I = 0x7f14007c

.field public static final Base_Theme_AppCompat_DialogWhenLarge:I = 0x7f140080

.field public static final Base_Theme_AppCompat_Dialog_Alert:I = 0x7f14007d

.field public static final Base_Theme_AppCompat_Dialog_FixedSize:I = 0x7f14007e

.field public static final Base_Theme_AppCompat_Dialog_MinWidth:I = 0x7f14007f

.field public static final Base_Theme_AppCompat_Light:I = 0x7f140081

.field public static final Base_Theme_AppCompat_Light_DarkActionBar:I = 0x7f140082

.field public static final Base_Theme_AppCompat_Light_Dialog:I = 0x7f140083

.field public static final Base_Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f140087

.field public static final Base_Theme_AppCompat_Light_Dialog_Alert:I = 0x7f140084

.field public static final Base_Theme_AppCompat_Light_Dialog_FixedSize:I = 0x7f140085

.field public static final Base_Theme_AppCompat_Light_Dialog_MinWidth:I = 0x7f140086

.field public static final Base_V21_Theme_AppCompat:I = 0x7f1400cc

.field public static final Base_V21_Theme_AppCompat_Dialog:I = 0x7f1400cd

.field public static final Base_V21_Theme_AppCompat_Light:I = 0x7f1400ce

.field public static final Base_V21_Theme_AppCompat_Light_Dialog:I = 0x7f1400cf

.field public static final Base_V7_Theme_AppCompat:I = 0x7f1400e5

.field public static final Base_V7_Theme_AppCompat_Dialog:I = 0x7f1400e6

.field public static final Base_V7_Theme_AppCompat_Light:I = 0x7f1400e7

.field public static final Base_V7_Theme_AppCompat_Light_Dialog:I = 0x7f1400e8

.field public static final Base_V7_Widget_AppCompat_AutoCompleteTextView:I = 0x7f1400ea

.field public static final Base_V7_Widget_AppCompat_EditText:I = 0x7f1400eb

.field public static final Base_Widget_AppCompat_ActionBar:I = 0x7f1400ed

.field public static final Base_Widget_AppCompat_ActionBar_Solid:I = 0x7f1400ee

.field public static final Base_Widget_AppCompat_ActionBar_TabBar:I = 0x7f1400ef

.field public static final Base_Widget_AppCompat_ActionBar_TabText:I = 0x7f1400f0

.field public static final Base_Widget_AppCompat_ActionBar_TabView:I = 0x7f1400f1

.field public static final Base_Widget_AppCompat_ActionButton:I = 0x7f1400f2

.field public static final Base_Widget_AppCompat_ActionButton_CloseMode:I = 0x7f1400f3

.field public static final Base_Widget_AppCompat_ActionButton_Overflow:I = 0x7f1400f4

.field public static final Base_Widget_AppCompat_ActionMode:I = 0x7f1400f5

.field public static final Base_Widget_AppCompat_ActivityChooserView:I = 0x7f1400f6

.field public static final Base_Widget_AppCompat_AutoCompleteTextView:I = 0x7f1400f7

.field public static final Base_Widget_AppCompat_Button:I = 0x7f1400f8

.field public static final Base_Widget_AppCompat_ButtonBar:I = 0x7f1400fe

.field public static final Base_Widget_AppCompat_ButtonBar_AlertDialog:I = 0x7f1400ff

.field public static final Base_Widget_AppCompat_Button_Borderless:I = 0x7f1400f9

.field public static final Base_Widget_AppCompat_Button_Borderless_Colored:I = 0x7f1400fa

.field public static final Base_Widget_AppCompat_Button_ButtonBar_AlertDialog:I = 0x7f1400fb

.field public static final Base_Widget_AppCompat_Button_Small:I = 0x7f1400fd

.field public static final Base_Widget_AppCompat_CompoundButton_CheckBox:I = 0x7f140100

.field public static final Base_Widget_AppCompat_CompoundButton_RadioButton:I = 0x7f140101

.field public static final Base_Widget_AppCompat_CompoundButton_Switch:I = 0x7f140102

.field public static final Base_Widget_AppCompat_DrawerArrowToggle:I = 0x7f140103

.field public static final Base_Widget_AppCompat_DrawerArrowToggle_Common:I = 0x7f140104

.field public static final Base_Widget_AppCompat_DropDownItem_Spinner:I = 0x7f140105

.field public static final Base_Widget_AppCompat_EditText:I = 0x7f140106

.field public static final Base_Widget_AppCompat_Light_ActionBar:I = 0x7f140108

.field public static final Base_Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f140109

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f14010a

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f14010b

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f14010c

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f14010d

.field public static final Base_Widget_AppCompat_Light_PopupMenu:I = 0x7f14010e

.field public static final Base_Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f14010f

.field public static final Base_Widget_AppCompat_ListPopupWindow:I = 0x7f140111

.field public static final Base_Widget_AppCompat_ListView:I = 0x7f140112

.field public static final Base_Widget_AppCompat_ListView_DropDown:I = 0x7f140113

.field public static final Base_Widget_AppCompat_ListView_Menu:I = 0x7f140114

.field public static final Base_Widget_AppCompat_PopupMenu:I = 0x7f140115

.field public static final Base_Widget_AppCompat_PopupMenu_Overflow:I = 0x7f140116

.field public static final Base_Widget_AppCompat_PopupWindow:I = 0x7f140117

.field public static final Base_Widget_AppCompat_ProgressBar:I = 0x7f140118

.field public static final Base_Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f140119

.field public static final Base_Widget_AppCompat_RatingBar:I = 0x7f14011a

.field public static final Base_Widget_AppCompat_SearchView:I = 0x7f14011d

.field public static final Base_Widget_AppCompat_SearchView_ActionBar:I = 0x7f14011e

.field public static final Base_Widget_AppCompat_Spinner:I = 0x7f140121

.field public static final Base_Widget_AppCompat_Spinner_Underlined:I = 0x7f140122

.field public static final Base_Widget_AppCompat_TextView_SpinnerItem:I = 0x7f140124

.field public static final Base_Widget_AppCompat_Toolbar:I = 0x7f140125

.field public static final Base_Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f140126

.field public static final Platform_AppCompat:I = 0x7f1401db

.field public static final Platform_AppCompat_Light:I = 0x7f1401dc

.field public static final Platform_ThemeOverlay_AppCompat_Dark:I = 0x7f1401e2

.field public static final Platform_ThemeOverlay_AppCompat_Light:I = 0x7f1401e3

.field public static final RtlOverlay_DialogWindowTitle_AppCompat:I = 0x7f1401f6

.field public static final RtlOverlay_Widget_AppCompat_ActionBar_TitleItem:I = 0x7f1401f7

.field public static final RtlOverlay_Widget_AppCompat_DialogTitle_Icon:I = 0x7f1401f8

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem:I = 0x7f1401f9

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup:I = 0x7f1401fa

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_Text:I = 0x7f1401fd

.field public static final RtlOverlay_Widget_AppCompat_SearchView_MagIcon:I = 0x7f140204

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown:I = 0x7f1401ff

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1:I = 0x7f140200

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2:I = 0x7f140201

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Query:I = 0x7f140202

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Text:I = 0x7f140203

.field public static final TextAppearance_AppCompat:I = 0x7f14024e

.field public static final TextAppearance_AppCompat_Body1:I = 0x7f14024f

.field public static final TextAppearance_AppCompat_Body2:I = 0x7f140250

.field public static final TextAppearance_AppCompat_Button:I = 0x7f140251

.field public static final TextAppearance_AppCompat_Caption:I = 0x7f140252

.field public static final TextAppearance_AppCompat_Display1:I = 0x7f140253

.field public static final TextAppearance_AppCompat_Display2:I = 0x7f140254

.field public static final TextAppearance_AppCompat_Display3:I = 0x7f140255

.field public static final TextAppearance_AppCompat_Display4:I = 0x7f140256

.field public static final TextAppearance_AppCompat_Headline:I = 0x7f140257

.field public static final TextAppearance_AppCompat_Inverse:I = 0x7f140258

.field public static final TextAppearance_AppCompat_Large:I = 0x7f140259

.field public static final TextAppearance_AppCompat_Large_Inverse:I = 0x7f14025a

.field public static final TextAppearance_AppCompat_Light_SearchResult_Subtitle:I = 0x7f14025b

.field public static final TextAppearance_AppCompat_Light_SearchResult_Title:I = 0x7f14025c

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f14025d

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f14025e

.field public static final TextAppearance_AppCompat_Medium:I = 0x7f14025f

.field public static final TextAppearance_AppCompat_Medium_Inverse:I = 0x7f140260

.field public static final TextAppearance_AppCompat_Menu:I = 0x7f140261

.field public static final TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f140262

.field public static final TextAppearance_AppCompat_SearchResult_Title:I = 0x7f140263

.field public static final TextAppearance_AppCompat_Small:I = 0x7f140264

.field public static final TextAppearance_AppCompat_Small_Inverse:I = 0x7f140265

.field public static final TextAppearance_AppCompat_Subhead:I = 0x7f140266

.field public static final TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f140267

.field public static final TextAppearance_AppCompat_Title:I = 0x7f140268

.field public static final TextAppearance_AppCompat_Title_Inverse:I = 0x7f140269

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f14026b

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f14026c

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f14026d

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f14026e

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f14026f

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f140270

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse:I = 0x7f140271

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f140272

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse:I = 0x7f140273

.field public static final TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f140278

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f14027a

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f14027b

.field public static final TextAppearance_AppCompat_Widget_Switch:I = 0x7f14027c

.field public static final TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I = 0x7f14027d

.field public static final TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f1402c8

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f1402c9

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f1402ca

.field public static final ThemeOverlay_AppCompat:I = 0x7f140337

.field public static final ThemeOverlay_AppCompat_ActionBar:I = 0x7f140338

.field public static final ThemeOverlay_AppCompat_Dark:I = 0x7f140339

.field public static final ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f14033a

.field public static final ThemeOverlay_AppCompat_Light:I = 0x7f14033f

.field public static final Theme_AppCompat:I = 0x7f1402cf

.field public static final Theme_AppCompat_CompactMenu:I = 0x7f1402d0

.field public static final Theme_AppCompat_Dialog:I = 0x7f1402d8

.field public static final Theme_AppCompat_DialogWhenLarge:I = 0x7f1402db

.field public static final Theme_AppCompat_Dialog_Alert:I = 0x7f1402d9

.field public static final Theme_AppCompat_Dialog_MinWidth:I = 0x7f1402da

.field public static final Theme_AppCompat_Light:I = 0x7f1402dd

.field public static final Theme_AppCompat_Light_DarkActionBar:I = 0x7f1402de

.field public static final Theme_AppCompat_Light_Dialog:I = 0x7f1402df

.field public static final Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f1402e2

.field public static final Theme_AppCompat_Light_Dialog_Alert:I = 0x7f1402e0

.field public static final Theme_AppCompat_Light_Dialog_MinWidth:I = 0x7f1402e1

.field public static final Theme_AppCompat_Light_NoActionBar:I = 0x7f1402e3

.field public static final Theme_AppCompat_NoActionBar:I = 0x7f1402e4

.field public static final Widget_AppCompat_ActionBar:I = 0x7f1403af

.field public static final Widget_AppCompat_ActionBar_Solid:I = 0x7f1403b0

.field public static final Widget_AppCompat_ActionBar_TabBar:I = 0x7f1403b1

.field public static final Widget_AppCompat_ActionBar_TabText:I = 0x7f1403b2

.field public static final Widget_AppCompat_ActionBar_TabView:I = 0x7f1403b3

.field public static final Widget_AppCompat_ActionButton:I = 0x7f1403b4

.field public static final Widget_AppCompat_ActionButton_CloseMode:I = 0x7f1403b5

.field public static final Widget_AppCompat_ActionButton_Overflow:I = 0x7f1403b6

.field public static final Widget_AppCompat_ActionMode:I = 0x7f1403b7

.field public static final Widget_AppCompat_ActivityChooserView:I = 0x7f1403b8

.field public static final Widget_AppCompat_AutoCompleteTextView:I = 0x7f1403b9

.field public static final Widget_AppCompat_Button:I = 0x7f1403ba

.field public static final Widget_AppCompat_ButtonBar:I = 0x7f1403c0

.field public static final Widget_AppCompat_ButtonBar_AlertDialog:I = 0x7f1403c1

.field public static final Widget_AppCompat_Button_Borderless:I = 0x7f1403bb

.field public static final Widget_AppCompat_Button_Borderless_Colored:I = 0x7f1403bc

.field public static final Widget_AppCompat_Button_ButtonBar_AlertDialog:I = 0x7f1403bd

.field public static final Widget_AppCompat_Button_Small:I = 0x7f1403bf

.field public static final Widget_AppCompat_CompoundButton_CheckBox:I = 0x7f1403c2

.field public static final Widget_AppCompat_CompoundButton_RadioButton:I = 0x7f1403c3

.field public static final Widget_AppCompat_CompoundButton_Switch:I = 0x7f1403c4

.field public static final Widget_AppCompat_DrawerArrowToggle:I = 0x7f1403c5

.field public static final Widget_AppCompat_DropDownItem_Spinner:I = 0x7f1403c6

.field public static final Widget_AppCompat_EditText:I = 0x7f1403c7

.field public static final Widget_AppCompat_Light_ActionBar:I = 0x7f1403c9

.field public static final Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f1403ca

.field public static final Widget_AppCompat_Light_ActionBar_Solid_Inverse:I = 0x7f1403cb

.field public static final Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f1403cc

.field public static final Widget_AppCompat_Light_ActionBar_TabBar_Inverse:I = 0x7f1403cd

.field public static final Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f1403ce

.field public static final Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f1403cf

.field public static final Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f1403d0

.field public static final Widget_AppCompat_Light_ActionBar_TabView_Inverse:I = 0x7f1403d1

.field public static final Widget_AppCompat_Light_ActionButton:I = 0x7f1403d2

.field public static final Widget_AppCompat_Light_ActionButton_CloseMode:I = 0x7f1403d3

.field public static final Widget_AppCompat_Light_ActionButton_Overflow:I = 0x7f1403d4

.field public static final Widget_AppCompat_Light_ActionMode_Inverse:I = 0x7f1403d5

.field public static final Widget_AppCompat_Light_ActivityChooserView:I = 0x7f1403d6

.field public static final Widget_AppCompat_Light_AutoCompleteTextView:I = 0x7f1403d7

.field public static final Widget_AppCompat_Light_DropDownItem_Spinner:I = 0x7f1403d8

.field public static final Widget_AppCompat_Light_ListPopupWindow:I = 0x7f1403d9

.field public static final Widget_AppCompat_Light_ListView_DropDown:I = 0x7f1403da

.field public static final Widget_AppCompat_Light_PopupMenu:I = 0x7f1403db

.field public static final Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f1403dc

.field public static final Widget_AppCompat_Light_SearchView:I = 0x7f1403dd

.field public static final Widget_AppCompat_Light_Spinner_DropDown_ActionBar:I = 0x7f1403de

.field public static final Widget_AppCompat_ListPopupWindow:I = 0x7f1403e0

.field public static final Widget_AppCompat_ListView:I = 0x7f1403e1

.field public static final Widget_AppCompat_ListView_DropDown:I = 0x7f1403e2

.field public static final Widget_AppCompat_ListView_Menu:I = 0x7f1403e3

.field public static final Widget_AppCompat_PopupMenu:I = 0x7f1403e4

.field public static final Widget_AppCompat_PopupMenu_Overflow:I = 0x7f1403e5

.field public static final Widget_AppCompat_PopupWindow:I = 0x7f1403e6

.field public static final Widget_AppCompat_ProgressBar:I = 0x7f1403e7

.field public static final Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f1403e8

.field public static final Widget_AppCompat_RatingBar:I = 0x7f1403e9

.field public static final Widget_AppCompat_SearchView:I = 0x7f1403ec

.field public static final Widget_AppCompat_SearchView_ActionBar:I = 0x7f1403ed

.field public static final Widget_AppCompat_Spinner:I = 0x7f1403f0

.field public static final Widget_AppCompat_Spinner_DropDown:I = 0x7f1403f1

.field public static final Widget_AppCompat_Spinner_DropDown_ActionBar:I = 0x7f1403f2

.field public static final Widget_AppCompat_Spinner_Underlined:I = 0x7f1403f3

.field public static final Widget_AppCompat_TextView_SpinnerItem:I = 0x7f1403f5

.field public static final Widget_AppCompat_Toolbar:I = 0x7f1403f6

.field public static final Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f1403f7


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

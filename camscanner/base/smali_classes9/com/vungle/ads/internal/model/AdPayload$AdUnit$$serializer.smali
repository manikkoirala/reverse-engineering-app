.class public final Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;
.super Ljava/lang/Object;
.source "AdPayload.kt"

# interfaces
.implements Lkotlinx/serialization/internal/GeneratedSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vungle/ads/internal/model/AdPayload$AdUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "$serializer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlinx/serialization/internal/GeneratedSerializer<",
        "Lcom/vungle/ads/internal/model/AdPayload$AdUnit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final INSTANCE:Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final synthetic descriptor:Lkotlinx/serialization/descriptors/SerialDescriptor;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;->INSTANCE:Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;

    .line 7
    .line 8
    new-instance v1, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;

    .line 9
    .line 10
    const-string v2, "com.vungle.ads.internal.model.AdPayload.AdUnit"

    .line 11
    .line 12
    const/16 v3, 0x1a

    .line 13
    .line 14
    invoke-direct {v1, v2, v0, v3}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;-><init>(Ljava/lang/String;Lkotlinx/serialization/internal/GeneratedSerializer;I)V

    .line 15
    .line 16
    .line 17
    const-string v0, "id"

    .line 18
    .line 19
    const/4 v2, 0x1

    .line 20
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 21
    .line 22
    .line 23
    const-string v0, "adType"

    .line 24
    .line 25
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 26
    .line 27
    .line 28
    const-string v0, "adSource"

    .line 29
    .line 30
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 31
    .line 32
    .line 33
    const-string v0, "campaign"

    .line 34
    .line 35
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 36
    .line 37
    .line 38
    const-string v0, "expiry"

    .line 39
    .line 40
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 41
    .line 42
    .line 43
    const-string v0, "app_id"

    .line 44
    .line 45
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 46
    .line 47
    .line 48
    const-string v0, "callToActionUrl"

    .line 49
    .line 50
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 51
    .line 52
    .line 53
    const-string v0, "deeplinkUrl"

    .line 54
    .line 55
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 56
    .line 57
    .line 58
    const-string v0, "click_coordinates_enabled"

    .line 59
    .line 60
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 61
    .line 62
    .line 63
    const-string/jumbo v0, "tpat"

    .line 64
    .line 65
    .line 66
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 67
    .line 68
    .line 69
    const-string/jumbo v0, "templateURL"

    .line 70
    .line 71
    .line 72
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 73
    .line 74
    .line 75
    const-string/jumbo v0, "templateId"

    .line 76
    .line 77
    .line 78
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 79
    .line 80
    .line 81
    const-string/jumbo v0, "template_type"

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 85
    .line 86
    .line 87
    const-string/jumbo v0, "templateSettings"

    .line 88
    .line 89
    .line 90
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 91
    .line 92
    .line 93
    const-string v0, "bid_token"

    .line 94
    .line 95
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 96
    .line 97
    .line 98
    const-string v0, "ad_market_id"

    .line 99
    .line 100
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 101
    .line 102
    .line 103
    const-string v0, "info"

    .line 104
    .line 105
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 106
    .line 107
    .line 108
    const-string/jumbo v0, "sleep"

    .line 109
    .line 110
    .line 111
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 112
    .line 113
    .line 114
    const-string/jumbo v0, "viewability"

    .line 115
    .line 116
    .line 117
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 118
    .line 119
    .line 120
    const-string v0, "adExt"

    .line 121
    .line 122
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 123
    .line 124
    .line 125
    const-string v0, "notification"

    .line 126
    .line 127
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 128
    .line 129
    .line 130
    const-string v0, "load_ad"

    .line 131
    .line 132
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 133
    .line 134
    .line 135
    const-string/jumbo v0, "timestamp"

    .line 136
    .line 137
    .line 138
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 139
    .line 140
    .line 141
    const-string/jumbo v0, "showCloseIncentivized"

    .line 142
    .line 143
    .line 144
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 145
    .line 146
    .line 147
    const-string/jumbo v0, "showClose"

    .line 148
    .line 149
    .line 150
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 151
    .line 152
    .line 153
    const-string v0, "error_code"

    .line 154
    .line 155
    invoke-virtual {v1, v0, v2}, Lkotlinx/serialization/internal/PluginGeneratedSerialDescriptor;->〇8o8o〇(Ljava/lang/String;Z)V

    .line 156
    .line 157
    .line 158
    sput-object v1, Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;->descriptor:Lkotlinx/serialization/descriptors/SerialDescriptor;

    .line 159
    .line 160
    return-void
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public childSerializers()[Lkotlinx/serialization/KSerializer;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Lkotlinx/serialization/KSerializer<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const/16 v0, 0x1a

    .line 2
    .line 3
    new-array v0, v0, [Lkotlinx/serialization/KSerializer;

    .line 4
    .line 5
    sget-object v1, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    .line 6
    .line 7
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    const/4 v3, 0x0

    .line 12
    aput-object v2, v0, v3

    .line 13
    .line 14
    const/4 v2, 0x1

    .line 15
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    aput-object v3, v0, v2

    .line 20
    .line 21
    const/4 v2, 0x2

    .line 22
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    aput-object v3, v0, v2

    .line 27
    .line 28
    const/4 v2, 0x3

    .line 29
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    aput-object v3, v0, v2

    .line 34
    .line 35
    sget-object v2, Lkotlinx/serialization/internal/IntSerializer;->〇080:Lkotlinx/serialization/internal/IntSerializer;

    .line 36
    .line 37
    invoke-static {v2}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    const/4 v4, 0x4

    .line 42
    aput-object v3, v0, v4

    .line 43
    .line 44
    const/4 v3, 0x5

    .line 45
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 46
    .line 47
    .line 48
    move-result-object v4

    .line 49
    aput-object v4, v0, v3

    .line 50
    .line 51
    const/4 v3, 0x6

    .line 52
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    aput-object v4, v0, v3

    .line 57
    .line 58
    const/4 v3, 0x7

    .line 59
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 60
    .line 61
    .line 62
    move-result-object v4

    .line 63
    aput-object v4, v0, v3

    .line 64
    .line 65
    sget-object v3, Lkotlinx/serialization/internal/BooleanSerializer;->〇080:Lkotlinx/serialization/internal/BooleanSerializer;

    .line 66
    .line 67
    invoke-static {v3}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    const/16 v4, 0x8

    .line 72
    .line 73
    aput-object v3, v0, v4

    .line 74
    .line 75
    sget-object v3, Lcom/vungle/ads/internal/model/AdPayload$TpatSerializer;->INSTANCE:Lcom/vungle/ads/internal/model/AdPayload$TpatSerializer;

    .line 76
    .line 77
    invoke-static {v3}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 78
    .line 79
    .line 80
    move-result-object v3

    .line 81
    const/16 v4, 0x9

    .line 82
    .line 83
    aput-object v3, v0, v4

    .line 84
    .line 85
    const/16 v3, 0xa

    .line 86
    .line 87
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 88
    .line 89
    .line 90
    move-result-object v4

    .line 91
    aput-object v4, v0, v3

    .line 92
    .line 93
    const/16 v3, 0xb

    .line 94
    .line 95
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 96
    .line 97
    .line 98
    move-result-object v4

    .line 99
    aput-object v4, v0, v3

    .line 100
    .line 101
    const/16 v3, 0xc

    .line 102
    .line 103
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 104
    .line 105
    .line 106
    move-result-object v4

    .line 107
    aput-object v4, v0, v3

    .line 108
    .line 109
    sget-object v3, Lcom/vungle/ads/internal/model/AdPayload$TemplateSettings$$serializer;->INSTANCE:Lcom/vungle/ads/internal/model/AdPayload$TemplateSettings$$serializer;

    .line 110
    .line 111
    invoke-static {v3}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 112
    .line 113
    .line 114
    move-result-object v3

    .line 115
    const/16 v4, 0xd

    .line 116
    .line 117
    aput-object v3, v0, v4

    .line 118
    .line 119
    const/16 v3, 0xe

    .line 120
    .line 121
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 122
    .line 123
    .line 124
    move-result-object v4

    .line 125
    aput-object v4, v0, v3

    .line 126
    .line 127
    const/16 v3, 0xf

    .line 128
    .line 129
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 130
    .line 131
    .line 132
    move-result-object v4

    .line 133
    aput-object v4, v0, v3

    .line 134
    .line 135
    const/16 v3, 0x10

    .line 136
    .line 137
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 138
    .line 139
    .line 140
    move-result-object v4

    .line 141
    aput-object v4, v0, v3

    .line 142
    .line 143
    const/16 v3, 0x11

    .line 144
    .line 145
    invoke-static {v2}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 146
    .line 147
    .line 148
    move-result-object v4

    .line 149
    aput-object v4, v0, v3

    .line 150
    .line 151
    sget-object v3, Lcom/vungle/ads/internal/model/AdPayload$Viewability$$serializer;->INSTANCE:Lcom/vungle/ads/internal/model/AdPayload$Viewability$$serializer;

    .line 152
    .line 153
    invoke-static {v3}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 154
    .line 155
    .line 156
    move-result-object v3

    .line 157
    const/16 v4, 0x12

    .line 158
    .line 159
    aput-object v3, v0, v4

    .line 160
    .line 161
    const/16 v3, 0x13

    .line 162
    .line 163
    invoke-static {v1}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 164
    .line 165
    .line 166
    move-result-object v4

    .line 167
    aput-object v4, v0, v3

    .line 168
    .line 169
    new-instance v3, Lkotlinx/serialization/internal/ArrayListSerializer;

    .line 170
    .line 171
    invoke-direct {v3, v1}, Lkotlinx/serialization/internal/ArrayListSerializer;-><init>(Lkotlinx/serialization/KSerializer;)V

    .line 172
    .line 173
    .line 174
    invoke-static {v3}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 175
    .line 176
    .line 177
    move-result-object v3

    .line 178
    const/16 v4, 0x14

    .line 179
    .line 180
    aput-object v3, v0, v4

    .line 181
    .line 182
    new-instance v3, Lkotlinx/serialization/internal/ArrayListSerializer;

    .line 183
    .line 184
    invoke-direct {v3, v1}, Lkotlinx/serialization/internal/ArrayListSerializer;-><init>(Lkotlinx/serialization/KSerializer;)V

    .line 185
    .line 186
    .line 187
    invoke-static {v3}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 188
    .line 189
    .line 190
    move-result-object v1

    .line 191
    const/16 v3, 0x15

    .line 192
    .line 193
    aput-object v1, v0, v3

    .line 194
    .line 195
    const/16 v1, 0x16

    .line 196
    .line 197
    invoke-static {v2}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 198
    .line 199
    .line 200
    move-result-object v3

    .line 201
    aput-object v3, v0, v1

    .line 202
    .line 203
    const/16 v1, 0x17

    .line 204
    .line 205
    invoke-static {v2}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 206
    .line 207
    .line 208
    move-result-object v3

    .line 209
    aput-object v3, v0, v1

    .line 210
    .line 211
    const/16 v1, 0x18

    .line 212
    .line 213
    invoke-static {v2}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 214
    .line 215
    .line 216
    move-result-object v3

    .line 217
    aput-object v3, v0, v1

    .line 218
    .line 219
    const/16 v1, 0x19

    .line 220
    .line 221
    invoke-static {v2}, Lkotlinx/serialization/builtins/BuiltinSerializersKt;->〇0〇O0088o(Lkotlinx/serialization/KSerializer;)Lkotlinx/serialization/KSerializer;

    .line 222
    .line 223
    .line 224
    move-result-object v2

    .line 225
    aput-object v2, v0, v1

    .line 226
    .line 227
    return-object v0
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method public deserialize(Lkotlinx/serialization/encoding/Decoder;)Lcom/vungle/ads/internal/model/AdPayload$AdUnit;
    .locals 64
    .param p1    # Lkotlinx/serialization/encoding/Decoder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "decoder"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual/range {p0 .. p0}, Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlinx/serialization/encoding/Decoder;->〇o00〇〇Oo(Lkotlinx/serialization/descriptors/SerialDescriptor;)Lkotlinx/serialization/encoding/CompositeDecoder;

    move-result-object v0

    invoke-interface {v0}, Lkotlinx/serialization/encoding/CompositeDecoder;->〇8o8o〇()Z

    move-result v2

    const/16 v13, 0x9

    const/4 v14, 0x7

    const/4 v15, 0x6

    const/4 v3, 0x5

    const/4 v4, 0x3

    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v7, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    if-eqz v2, :cond_0

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    invoke-interface {v0, v1, v9, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v0, v1, v8, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v0, v1, v7, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v0, v1, v4, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    sget-object v11, Lkotlinx/serialization/internal/IntSerializer;->〇080:Lkotlinx/serialization/internal/IntSerializer;

    invoke-interface {v0, v1, v5, v11, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0, v1, v3, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v1, v15, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    invoke-interface {v0, v1, v14, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    sget-object v12, Lkotlinx/serialization/internal/BooleanSerializer;->〇080:Lkotlinx/serialization/internal/BooleanSerializer;

    invoke-interface {v0, v1, v6, v12, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    sget-object v12, Lcom/vungle/ads/internal/model/AdPayload$TpatSerializer;->INSTANCE:Lcom/vungle/ads/internal/model/AdPayload$TpatSerializer;

    invoke-interface {v0, v1, v13, v12, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    const/16 v13, 0xa

    invoke-interface {v0, v1, v13, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    move-object/from16 v25, v3

    const/16 v3, 0xb

    invoke-interface {v0, v1, v3, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v24, v3

    const/16 v3, 0xc

    invoke-interface {v0, v1, v3, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v23, v3

    sget-object v3, Lcom/vungle/ads/internal/model/AdPayload$TemplateSettings$$serializer;->INSTANCE:Lcom/vungle/ads/internal/model/AdPayload$TemplateSettings$$serializer;

    move-object/from16 v26, v4

    const/16 v4, 0xd

    invoke-interface {v0, v1, v4, v3, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    const/16 v4, 0xe

    invoke-interface {v0, v1, v4, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v21, v3

    const/16 v3, 0xf

    invoke-interface {v0, v1, v3, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v20, v3

    const/16 v3, 0x10

    invoke-interface {v0, v1, v3, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v19, v3

    const/16 v3, 0x11

    invoke-interface {v0, v1, v3, v11, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v18, v3

    sget-object v3, Lcom/vungle/ads/internal/model/AdPayload$Viewability$$serializer;->INSTANCE:Lcom/vungle/ads/internal/model/AdPayload$Viewability$$serializer;

    move-object/from16 v22, v4

    const/16 v4, 0x12

    invoke-interface {v0, v1, v4, v3, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    const/16 v4, 0x13

    invoke-interface {v0, v1, v4, v2, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v16, v3

    new-instance v3, Lkotlinx/serialization/internal/ArrayListSerializer;

    invoke-direct {v3, v2}, Lkotlinx/serialization/internal/ArrayListSerializer;-><init>(Lkotlinx/serialization/KSerializer;)V

    move-object/from16 v17, v4

    const/16 v4, 0x14

    invoke-interface {v0, v1, v4, v3, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    new-instance v4, Lkotlinx/serialization/internal/ArrayListSerializer;

    invoke-direct {v4, v2}, Lkotlinx/serialization/internal/ArrayListSerializer;-><init>(Lkotlinx/serialization/KSerializer;)V

    const/16 v2, 0x15

    invoke-interface {v0, v1, v2, v4, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const/16 v4, 0x16

    invoke-interface {v0, v1, v4, v11, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 p1, v2

    const/16 v2, 0x17

    invoke-interface {v0, v1, v2, v11, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v27, v2

    const/16 v2, 0x18

    invoke-interface {v0, v1, v2, v11, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v28, v2

    const/16 v2, 0x19

    invoke-interface {v0, v1, v2, v11, v10}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const v10, 0x3ffffff

    move-object/from16 v29, v5

    move-object/from16 v33, v6

    move-object v10, v13

    move-object/from16 v32, v14

    move-object/from16 v31, v15

    move-object/from16 v14, v27

    move-object/from16 v6, v28

    const v35, 0x3ffffff

    move-object v13, v2

    move-object v5, v3

    move-object v15, v4

    move-object/from16 v3, v25

    move-object/from16 v4, v26

    move-object/from16 v2, p1

    goto/16 :goto_2

    :cond_0
    move-object v2, v10

    move-object v3, v2

    move-object v4, v3

    move-object v5, v4

    move-object v6, v5

    move-object v7, v6

    move-object v8, v7

    move-object v9, v8

    move-object v11, v9

    move-object v12, v11

    move-object v13, v12

    move-object v14, v13

    move-object v15, v14

    move-object/from16 v36, v15

    move-object/from16 v37, v36

    move-object/from16 v38, v37

    move-object/from16 v39, v38

    move-object/from16 v40, v39

    move-object/from16 v41, v40

    move-object/from16 v42, v41

    move-object/from16 v43, v42

    move-object/from16 v44, v43

    move-object/from16 v45, v44

    move-object/from16 v46, v45

    move-object/from16 v47, v46

    move-object/from16 v48, v47

    const/4 v10, 0x0

    const/16 v49, 0x1

    :goto_0
    if-eqz v49, :cond_1

    move-object/from16 v50, v2

    invoke-interface {v0, v1}, Lkotlinx/serialization/encoding/CompositeDecoder;->oo88o8O(Lkotlinx/serialization/descriptors/SerialDescriptor;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    new-instance v0, Lkotlinx/serialization/UnknownFieldException;

    invoke-direct {v0, v2}, Lkotlinx/serialization/UnknownFieldException;-><init>(I)V

    throw v0

    :pswitch_0
    const/16 v2, 0x19

    move-object/from16 v51, v11

    sget-object v11, Lkotlinx/serialization/internal/IntSerializer;->〇080:Lkotlinx/serialization/internal/IntSerializer;

    invoke-interface {v0, v1, v2, v11, v13}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    const/high16 v2, 0x2000000

    goto/16 :goto_1

    :pswitch_1
    move-object/from16 v51, v11

    const/16 v2, 0x18

    sget-object v11, Lkotlinx/serialization/internal/IntSerializer;->〇080:Lkotlinx/serialization/internal/IntSerializer;

    invoke-interface {v0, v1, v2, v11, v6}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    const/high16 v2, 0x1000000

    goto/16 :goto_1

    :pswitch_2
    move-object/from16 v51, v11

    const/16 v2, 0x17

    sget-object v11, Lkotlinx/serialization/internal/IntSerializer;->〇080:Lkotlinx/serialization/internal/IntSerializer;

    invoke-interface {v0, v1, v2, v11, v14}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    const/high16 v2, 0x800000

    goto/16 :goto_1

    :pswitch_3
    move-object/from16 v51, v11

    const/16 v2, 0x16

    sget-object v11, Lkotlinx/serialization/internal/IntSerializer;->〇080:Lkotlinx/serialization/internal/IntSerializer;

    invoke-interface {v0, v1, v2, v11, v15}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    const/high16 v2, 0x400000

    goto/16 :goto_1

    :pswitch_4
    move-object/from16 v51, v11

    new-instance v2, Lkotlinx/serialization/internal/ArrayListSerializer;

    sget-object v11, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    invoke-direct {v2, v11}, Lkotlinx/serialization/internal/ArrayListSerializer;-><init>(Lkotlinx/serialization/KSerializer;)V

    const/16 v11, 0x15

    invoke-interface {v0, v1, v11, v2, v3}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    const/high16 v2, 0x200000

    goto :goto_1

    :pswitch_5
    move-object/from16 v51, v11

    new-instance v2, Lkotlinx/serialization/internal/ArrayListSerializer;

    sget-object v11, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    invoke-direct {v2, v11}, Lkotlinx/serialization/internal/ArrayListSerializer;-><init>(Lkotlinx/serialization/KSerializer;)V

    const/16 v11, 0x14

    invoke-interface {v0, v1, v11, v2, v5}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    const/high16 v2, 0x100000

    goto :goto_1

    :pswitch_6
    move-object/from16 v51, v11

    const/16 v11, 0x14

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    const/16 v11, 0x13

    invoke-interface {v0, v1, v11, v2, v4}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    const/high16 v2, 0x80000

    goto :goto_1

    :pswitch_7
    move-object/from16 v51, v11

    const/16 v11, 0x13

    sget-object v2, Lcom/vungle/ads/internal/model/AdPayload$Viewability$$serializer;->INSTANCE:Lcom/vungle/ads/internal/model/AdPayload$Viewability$$serializer;

    const/16 v11, 0x12

    invoke-interface {v0, v1, v11, v2, v7}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    const/high16 v2, 0x40000

    goto :goto_1

    :pswitch_8
    move-object/from16 v51, v11

    const/16 v11, 0x12

    sget-object v2, Lkotlinx/serialization/internal/IntSerializer;->〇080:Lkotlinx/serialization/internal/IntSerializer;

    const/16 v11, 0x11

    invoke-interface {v0, v1, v11, v2, v8}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    const/high16 v2, 0x20000

    goto :goto_1

    :pswitch_9
    move-object/from16 v51, v11

    const/16 v11, 0x11

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    const/16 v11, 0x10

    invoke-interface {v0, v1, v11, v2, v9}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    const/high16 v2, 0x10000

    goto :goto_1

    :pswitch_a
    move-object/from16 v51, v11

    const/16 v11, 0x10

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    const/16 v11, 0xf

    invoke-interface {v0, v1, v11, v2, v12}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    const v2, 0x8000

    :goto_1
    or-int/2addr v10, v2

    move-object/from16 v2, v50

    move-object/from16 v11, v51

    goto/16 :goto_0

    :pswitch_b
    move-object/from16 v51, v11

    const/16 v11, 0xf

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    move-object/from16 v21, v3

    move-object/from16 v11, v51

    const/16 v3, 0xe

    invoke-interface {v0, v1, v3, v2, v11}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    or-int/lit16 v10, v10, 0x4000

    move-object/from16 v3, v21

    move-object/from16 v2, v50

    goto/16 :goto_0

    :pswitch_c
    move-object/from16 v21, v3

    const/16 v3, 0xe

    sget-object v2, Lcom/vungle/ads/internal/model/AdPayload$TemplateSettings$$serializer;->INSTANCE:Lcom/vungle/ads/internal/model/AdPayload$TemplateSettings$$serializer;

    move-object/from16 v22, v4

    move-object/from16 v3, v50

    const/16 v4, 0xd

    invoke-interface {v0, v1, v4, v2, v3}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    or-int/lit16 v10, v10, 0x2000

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    goto/16 :goto_0

    :pswitch_d
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v3, v50

    const/16 v4, 0xd

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    move-object/from16 v23, v3

    move-object/from16 v4, v48

    const/16 v3, 0xc

    invoke-interface {v0, v1, v3, v2, v4}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v48

    or-int/lit16 v10, v10, 0x1000

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    goto/16 :goto_0

    :pswitch_e
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v4, v48

    move-object/from16 v23, v50

    const/16 v3, 0xc

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    move-object/from16 v24, v4

    move-object/from16 v3, v47

    const/16 v4, 0xb

    invoke-interface {v0, v1, v4, v2, v3}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v47

    or-int/lit16 v10, v10, 0x800

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v48, v24

    goto/16 :goto_0

    :pswitch_f
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v3, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/16 v4, 0xb

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    move-object/from16 v25, v3

    move-object/from16 v4, v46

    const/16 v3, 0xa

    invoke-interface {v0, v1, v3, v2, v4}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v46

    or-int/lit16 v10, v10, 0x400

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v47, v25

    goto/16 :goto_0

    :pswitch_10
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v4, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/16 v3, 0xa

    sget-object v2, Lcom/vungle/ads/internal/model/AdPayload$TpatSerializer;->INSTANCE:Lcom/vungle/ads/internal/model/AdPayload$TpatSerializer;

    move-object/from16 v35, v4

    move-object/from16 v3, v45

    const/16 v4, 0x9

    invoke-interface {v0, v1, v4, v2, v3}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v45

    or-int/lit16 v10, v10, 0x200

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v46, v35

    goto/16 :goto_0

    :pswitch_11
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v3, v45

    move-object/from16 v35, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/16 v4, 0x9

    sget-object v2, Lkotlinx/serialization/internal/BooleanSerializer;->〇080:Lkotlinx/serialization/internal/BooleanSerializer;

    move-object/from16 v34, v3

    move-object/from16 v4, v44

    const/16 v3, 0x8

    invoke-interface {v0, v1, v3, v2, v4}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v44

    or-int/lit16 v10, v10, 0x100

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v45, v34

    goto/16 :goto_0

    :pswitch_12
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v4, v44

    move-object/from16 v34, v45

    move-object/from16 v35, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/16 v3, 0x8

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    move-object/from16 v33, v4

    move-object/from16 v3, v43

    const/4 v4, 0x7

    invoke-interface {v0, v1, v4, v2, v3}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v43

    or-int/lit16 v10, v10, 0x80

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v44, v33

    goto/16 :goto_0

    :pswitch_13
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v3, v43

    move-object/from16 v33, v44

    move-object/from16 v34, v45

    move-object/from16 v35, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/4 v4, 0x7

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    move-object/from16 v32, v3

    move-object/from16 v4, v42

    const/4 v3, 0x6

    invoke-interface {v0, v1, v3, v2, v4}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v42

    or-int/lit8 v10, v10, 0x40

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v43, v32

    goto/16 :goto_0

    :pswitch_14
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v4, v42

    move-object/from16 v32, v43

    move-object/from16 v33, v44

    move-object/from16 v34, v45

    move-object/from16 v35, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/4 v3, 0x6

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    move-object/from16 v31, v4

    move-object/from16 v3, v41

    const/4 v4, 0x5

    invoke-interface {v0, v1, v4, v2, v3}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v41

    or-int/lit8 v10, v10, 0x20

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v42, v31

    goto/16 :goto_0

    :pswitch_15
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v3, v41

    move-object/from16 v31, v42

    move-object/from16 v32, v43

    move-object/from16 v33, v44

    move-object/from16 v34, v45

    move-object/from16 v35, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/4 v4, 0x5

    sget-object v2, Lkotlinx/serialization/internal/IntSerializer;->〇080:Lkotlinx/serialization/internal/IntSerializer;

    move-object/from16 v30, v3

    move-object/from16 v4, v40

    const/4 v3, 0x4

    invoke-interface {v0, v1, v3, v2, v4}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v40

    or-int/lit8 v10, v10, 0x10

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v41, v30

    goto/16 :goto_0

    :pswitch_16
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v4, v40

    move-object/from16 v30, v41

    move-object/from16 v31, v42

    move-object/from16 v32, v43

    move-object/from16 v33, v44

    move-object/from16 v34, v45

    move-object/from16 v35, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/4 v3, 0x4

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    move-object/from16 v29, v4

    move-object/from16 v3, v39

    const/4 v4, 0x3

    invoke-interface {v0, v1, v4, v2, v3}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    or-int/lit8 v10, v10, 0x8

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v40, v29

    goto/16 :goto_0

    :pswitch_17
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v3, v39

    move-object/from16 v29, v40

    move-object/from16 v30, v41

    move-object/from16 v31, v42

    move-object/from16 v32, v43

    move-object/from16 v33, v44

    move-object/from16 v34, v45

    move-object/from16 v35, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/4 v4, 0x3

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    move-object/from16 v28, v3

    move-object/from16 v4, v38

    const/4 v3, 0x2

    invoke-interface {v0, v1, v3, v2, v4}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v38

    or-int/lit8 v10, v10, 0x4

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v39, v28

    goto/16 :goto_0

    :pswitch_18
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v4, v38

    move-object/from16 v28, v39

    move-object/from16 v29, v40

    move-object/from16 v30, v41

    move-object/from16 v31, v42

    move-object/from16 v32, v43

    move-object/from16 v33, v44

    move-object/from16 v34, v45

    move-object/from16 v35, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/4 v3, 0x2

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    move-object/from16 v27, v4

    move-object/from16 v3, v37

    const/4 v4, 0x1

    invoke-interface {v0, v1, v4, v2, v3}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v37

    or-int/lit8 v10, v10, 0x2

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v38, v27

    goto/16 :goto_0

    :pswitch_19
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v3, v37

    move-object/from16 v27, v38

    move-object/from16 v28, v39

    move-object/from16 v29, v40

    move-object/from16 v30, v41

    move-object/from16 v31, v42

    move-object/from16 v32, v43

    move-object/from16 v33, v44

    move-object/from16 v34, v45

    move-object/from16 v35, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/4 v4, 0x1

    sget-object v2, Lkotlinx/serialization/internal/StringSerializer;->〇080:Lkotlinx/serialization/internal/StringSerializer;

    move-object/from16 v26, v3

    move-object/from16 v4, v36

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3, v2, v4}, Lkotlinx/serialization/encoding/CompositeDecoder;->OO0o〇〇〇〇0(Lkotlinx/serialization/descriptors/SerialDescriptor;ILkotlinx/serialization/DeserializationStrategy;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v36

    or-int/lit8 v10, v10, 0x1

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    move-object/from16 v37, v26

    goto/16 :goto_0

    :pswitch_1a
    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v4, v36

    move-object/from16 v26, v37

    move-object/from16 v27, v38

    move-object/from16 v28, v39

    move-object/from16 v29, v40

    move-object/from16 v30, v41

    move-object/from16 v31, v42

    move-object/from16 v32, v43

    move-object/from16 v33, v44

    move-object/from16 v34, v45

    move-object/from16 v35, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v23, v50

    const/4 v3, 0x0

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    move-object/from16 v2, v23

    const/16 v49, 0x0

    goto/16 :goto_0

    :cond_1
    move-object/from16 v23, v2

    move-object/from16 v21, v3

    move-object/from16 v22, v4

    move-object/from16 v4, v36

    move-object/from16 v26, v37

    move-object/from16 v27, v38

    move-object/from16 v28, v39

    move-object/from16 v29, v40

    move-object/from16 v30, v41

    move-object/from16 v31, v42

    move-object/from16 v32, v43

    move-object/from16 v33, v44

    move-object/from16 v34, v45

    move-object/from16 v35, v46

    move-object/from16 v25, v47

    move-object/from16 v24, v48

    move-object/from16 v16, v7

    move-object/from16 v18, v8

    move-object/from16 v19, v9

    move-object/from16 v20, v12

    move-object/from16 v2, v21

    move-object/from16 v17, v22

    move-object/from16 v21, v23

    move-object/from16 v23, v24

    move-object/from16 v24, v25

    move-object/from16 v8, v26

    move-object/from16 v7, v27

    move-object/from16 v3, v30

    move-object/from16 v12, v34

    move-object v9, v4

    move-object/from16 v22, v11

    move-object/from16 v4, v28

    move-object/from16 v63, v35

    move/from16 v35, v10

    move-object/from16 v10, v63

    :goto_2
    invoke-interface {v0, v1}, Lkotlinx/serialization/encoding/CompositeDecoder;->〇o〇(Lkotlinx/serialization/descriptors/SerialDescriptor;)V

    new-instance v0, Lcom/vungle/ads/internal/model/AdPayload$AdUnit;

    move-object/from16 v34, v0

    move-object/from16 v36, v9

    check-cast v36, Ljava/lang/String;

    move-object/from16 v37, v8

    check-cast v37, Ljava/lang/String;

    move-object/from16 v38, v7

    check-cast v38, Ljava/lang/String;

    move-object/from16 v39, v4

    check-cast v39, Ljava/lang/String;

    move-object/from16 v40, v29

    check-cast v40, Ljava/lang/Integer;

    move-object/from16 v41, v3

    check-cast v41, Ljava/lang/String;

    move-object/from16 v42, v31

    check-cast v42, Ljava/lang/String;

    move-object/from16 v43, v32

    check-cast v43, Ljava/lang/String;

    move-object/from16 v44, v33

    check-cast v44, Ljava/lang/Boolean;

    move-object/from16 v45, v12

    check-cast v45, Ljava/util/Map;

    move-object/from16 v46, v10

    check-cast v46, Ljava/lang/String;

    move-object/from16 v47, v24

    check-cast v47, Ljava/lang/String;

    move-object/from16 v48, v23

    check-cast v48, Ljava/lang/String;

    move-object/from16 v49, v21

    check-cast v49, Lcom/vungle/ads/internal/model/AdPayload$TemplateSettings;

    move-object/from16 v50, v22

    check-cast v50, Ljava/lang/String;

    move-object/from16 v51, v20

    check-cast v51, Ljava/lang/String;

    move-object/from16 v52, v19

    check-cast v52, Ljava/lang/String;

    move-object/from16 v53, v18

    check-cast v53, Ljava/lang/Integer;

    move-object/from16 v54, v16

    check-cast v54, Lcom/vungle/ads/internal/model/AdPayload$Viewability;

    move-object/from16 v55, v17

    check-cast v55, Ljava/lang/String;

    move-object/from16 v56, v5

    check-cast v56, Ljava/util/List;

    move-object/from16 v57, v2

    check-cast v57, Ljava/util/List;

    move-object/from16 v58, v15

    check-cast v58, Ljava/lang/Integer;

    move-object/from16 v59, v14

    check-cast v59, Ljava/lang/Integer;

    move-object/from16 v60, v6

    check-cast v60, Ljava/lang/Integer;

    move-object/from16 v61, v13

    check-cast v61, Ljava/lang/Integer;

    const/16 v62, 0x0

    invoke-direct/range {v34 .. v62}, Lcom/vungle/ads/internal/model/AdPayload$AdUnit;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vungle/ads/internal/model/AdPayload$TemplateSettings;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/vungle/ads/internal/model/AdPayload$Viewability;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lkotlinx/serialization/internal/SerializationConstructorMarker;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic deserialize(Lkotlinx/serialization/encoding/Decoder;)Ljava/lang/Object;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;->deserialize(Lkotlinx/serialization/encoding/Decoder;)Lcom/vungle/ads/internal/model/AdPayload$AdUnit;

    move-result-object p1

    return-object p1
.end method

.method public getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;->descriptor:Lkotlinx/serialization/descriptors/SerialDescriptor;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public serialize(Lkotlinx/serialization/encoding/Encoder;Lcom/vungle/ads/internal/model/AdPayload$AdUnit;)V
    .locals 1
    .param p1    # Lkotlinx/serialization/encoding/Encoder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/vungle/ads/internal/model/AdPayload$AdUnit;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "encoder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-virtual {p0}, Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;->getDescriptor()Lkotlinx/serialization/descriptors/SerialDescriptor;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlinx/serialization/encoding/Encoder;->〇o00〇〇Oo(Lkotlinx/serialization/descriptors/SerialDescriptor;)Lkotlinx/serialization/encoding/CompositeEncoder;

    move-result-object p1

    invoke-static {p2, p1, v0}, Lcom/vungle/ads/internal/model/AdPayload$AdUnit;->write$Self(Lcom/vungle/ads/internal/model/AdPayload$AdUnit;Lkotlinx/serialization/encoding/CompositeEncoder;Lkotlinx/serialization/descriptors/SerialDescriptor;)V

    invoke-interface {p1, v0}, Lkotlinx/serialization/encoding/CompositeEncoder;->〇o〇(Lkotlinx/serialization/descriptors/SerialDescriptor;)V

    return-void
.end method

.method public bridge synthetic serialize(Lkotlinx/serialization/encoding/Encoder;Ljava/lang/Object;)V
    .locals 0

    .line 2
    check-cast p2, Lcom/vungle/ads/internal/model/AdPayload$AdUnit;

    invoke-virtual {p0, p1, p2}, Lcom/vungle/ads/internal/model/AdPayload$AdUnit$$serializer;->serialize(Lkotlinx/serialization/encoding/Encoder;Lcom/vungle/ads/internal/model/AdPayload$AdUnit;)V

    return-void
.end method

.method public typeParametersSerializers()[Lkotlinx/serialization/KSerializer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Lkotlinx/serialization/KSerializer<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {p0}, Lkotlinx/serialization/internal/GeneratedSerializer$DefaultImpls;->〇080(Lkotlinx/serialization/internal/GeneratedSerializer;)[Lkotlinx/serialization/KSerializer;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

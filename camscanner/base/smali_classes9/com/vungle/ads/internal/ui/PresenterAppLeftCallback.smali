.class public final Lcom/vungle/ads/internal/ui/PresenterAppLeftCallback;
.super Ljava/lang/Object;
.source "PresenterAppLeftCallback.kt"

# interfaces
.implements Lcom/vungle/ads/internal/util/ActivityManager$LeftApplicationCallback;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final bus:Lcom/vungle/ads/internal/presenter/AdEventListener;

.field private final placementRefId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vungle/ads/internal/presenter/AdEventListener;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/vungle/ads/internal/ui/PresenterAppLeftCallback;->bus:Lcom/vungle/ads/internal/presenter/AdEventListener;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/vungle/ads/internal/ui/PresenterAppLeftCallback;->placementRefId:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public onLeftApplication()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/vungle/ads/internal/ui/PresenterAppLeftCallback;->bus:Lcom/vungle/ads/internal/presenter/AdEventListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v1, "adLeftApplication"

    .line 6
    .line 7
    iget-object v2, p0, Lcom/vungle/ads/internal/ui/PresenterAppLeftCallback;->placementRefId:Ljava/lang/String;

    .line 8
    .line 9
    const-string v3, "open"

    .line 10
    .line 11
    invoke-virtual {v0, v3, v1, v2}, Lcom/vungle/ads/internal/presenter/AdEventListener;->onNext(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

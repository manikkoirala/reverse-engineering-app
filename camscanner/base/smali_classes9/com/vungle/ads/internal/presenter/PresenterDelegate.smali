.class public interface abstract Lcom/vungle/ads/internal/presenter/PresenterDelegate;
.super Ljava/lang/Object;
.source "PresenterDelegate.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract getAlertBodyText()Ljava/lang/String;
.end method

.method public abstract getAlertCloseButtonText()Ljava/lang/String;
.end method

.method public abstract getAlertContinueButtonText()Ljava/lang/String;
.end method

.method public abstract getAlertTitleText()Ljava/lang/String;
.end method

.method public abstract getUserId()Ljava/lang/String;
.end method

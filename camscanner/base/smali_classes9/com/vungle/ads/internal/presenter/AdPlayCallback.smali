.class public interface abstract Lcom/vungle/ads/internal/presenter/AdPlayCallback;
.super Ljava/lang/Object;
.source "AdPlayCallback.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract onAdClick(Ljava/lang/String;)V
.end method

.method public abstract onAdEnd(Ljava/lang/String;)V
.end method

.method public abstract onAdImpression(Ljava/lang/String;)V
.end method

.method public abstract onAdLeftApplication(Ljava/lang/String;)V
.end method

.method public abstract onAdRewarded(Ljava/lang/String;)V
.end method

.method public abstract onAdStart(Ljava/lang/String;)V
.end method

.method public abstract onFailure(Lcom/vungle/ads/VungleError;)V
    .param p1    # Lcom/vungle/ads/VungleError;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

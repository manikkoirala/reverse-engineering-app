.class public final enum Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;
.super Ljava/lang/Enum;
.source "Sdk.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vungle/ads/internal/protos/Sdk$SDKError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Reason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason$ReasonVerifier;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final enum AD_ALREADY_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_ALREADY_FAILED_VALUE:I = 0xce

.field public static final enum AD_ALREADY_LOADED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_ALREADY_LOADED_VALUE:I = 0xcc

.field public static final enum AD_CLOSED_MISSING_HEARTBEAT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_CLOSED_MISSING_HEARTBEAT_VALUE:I = 0x13e

.field public static final enum AD_CLOSED_TEMPLATE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_CLOSED_TEMPLATE_ERROR_VALUE:I = 0x13d

.field public static final enum AD_CONSUMED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_CONSUMED_VALUE:I = 0xca

.field public static final enum AD_EXPIRED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final enum AD_EXPIRED_ON_PLAY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_EXPIRED_ON_PLAY_VALUE:I = 0x133

.field public static final AD_EXPIRED_VALUE:I = 0x130

.field public static final enum AD_HTML_FAILED_TO_LOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_HTML_FAILED_TO_LOAD_VALUE:I = 0x136

.field public static final enum AD_INTERNAL_INTEGRATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_INTERNAL_INTEGRATION_ERROR_VALUE:I = 0x7532

.field public static final enum AD_IS_LOADING:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_IS_LOADING_VALUE:I = 0xcb

.field public static final enum AD_IS_PLAYING:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_IS_PLAYING_VALUE:I = 0xcd

.field public static final enum AD_LOAD_FAIL_RETRY_AFTER:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_LOAD_FAIL_RETRY_AFTER_VALUE:I = 0xdd

.field public static final enum AD_LOAD_TOO_FREQUENTLY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_LOAD_TOO_FREQUENTLY_VALUE:I = 0x2712

.field public static final enum AD_NOT_LOADED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_NOT_LOADED_VALUE:I = 0xd2

.field public static final enum AD_NO_FILL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_NO_FILL_VALUE:I = 0x2711

.field public static final enum AD_PUBLISHER_MISMATCH:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_PUBLISHER_MISMATCH_VALUE:I = 0x7531

.field public static final enum AD_RESPONSE_EMPTY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_RESPONSE_EMPTY_VALUE:I = 0xd7

.field public static final enum AD_RESPONSE_INVALID_TEMPLATE_TYPE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_RESPONSE_INVALID_TEMPLATE_TYPE_VALUE:I = 0xd8

.field public static final enum AD_RESPONSE_RETRY_AFTER:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_RESPONSE_RETRY_AFTER_VALUE:I = 0xdc

.field public static final enum AD_RESPONSE_TIMED_OUT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_RESPONSE_TIMED_OUT_VALUE:I = 0xd9

.field public static final enum AD_SERVER_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_SERVER_ERROR_VALUE:I = 0x4e21

.field public static final enum AD_WIN_NOTIFICATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final AD_WIN_NOTIFICATION_ERROR_VALUE:I = 0x134

.field public static final enum ALREADY_INITIALIZED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final ALREADY_INITIALIZED_VALUE:I = 0x4

.field public static final enum API_FAILED_STATUS_CODE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final API_FAILED_STATUS_CODE_VALUE:I = 0x68

.field public static final enum API_REQUEST_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final API_REQUEST_ERROR_VALUE:I = 0x65

.field public static final enum API_RESPONSE_DATA_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final API_RESPONSE_DATA_ERROR_VALUE:I = 0x66

.field public static final enum API_RESPONSE_DECODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final API_RESPONSE_DECODE_ERROR_VALUE:I = 0x67

.field public static final enum ASSET_FAILED_INSUFFICIENT_SPACE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final ASSET_FAILED_INSUFFICIENT_SPACE_VALUE:I = 0x7e

.field public static final enum ASSET_FAILED_MAX_SPACE_EXCEEDED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final ASSET_FAILED_MAX_SPACE_EXCEEDED_VALUE:I = 0x7f

.field public static final enum ASSET_FAILED_STATUS_CODE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final ASSET_FAILED_STATUS_CODE_VALUE:I = 0x75

.field public static final enum ASSET_FAILED_TO_DELETE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final ASSET_FAILED_TO_DELETE_VALUE:I = 0x135

.field public static final enum ASSET_REQUEST_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final ASSET_REQUEST_ERROR_VALUE:I = 0x70

.field public static final enum ASSET_RESPONSE_DATA_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final ASSET_RESPONSE_DATA_ERROR_VALUE:I = 0x71

.field public static final enum ASSET_WRITE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final ASSET_WRITE_ERROR_VALUE:I = 0x72

.field public static final enum BANNER_VIEW_INVALID_SIZE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final BANNER_VIEW_INVALID_SIZE_VALUE:I = 0x1f4

.field public static final enum CONCURRENT_PLAYBACK_UNSUPPORTED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final CONCURRENT_PLAYBACK_UNSUPPORTED_VALUE:I = 0x190

.field public static final enum CONFIG_REFRESH_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final CONFIG_REFRESH_FAILED_VALUE:I = 0x8a

.field public static final enum CURRENTLY_INITIALIZING:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final CURRENTLY_INITIALIZING_VALUE:I = 0x3

.field public static final enum DEEPLINK_OPEN_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final DEEPLINK_OPEN_FAILED_VALUE:I = 0x138

.field public static final enum EMPTY_TPAT_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final EMPTY_TPAT_ERROR_VALUE:I = 0x81

.field public static final enum EVALUATE_JAVASCRIPT_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final EVALUATE_JAVASCRIPT_FAILED_VALUE:I = 0x139

.field public static final enum GENERATE_JSON_DATA_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final GENERATE_JSON_DATA_ERROR_VALUE:I = 0x13c

.field public static final enum GZIP_ENCODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final GZIP_ENCODE_ERROR_VALUE:I = 0x74

.field public static final enum INVALID_ADS_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_ADS_ENDPOINT_VALUE:I = 0x7a

.field public static final enum INVALID_ADUNIT_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_ADUNIT_BID_PAYLOAD_VALUE:I = 0xd5

.field public static final enum INVALID_APP_ID:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_APP_ID_VALUE:I = 0x2

.field public static final enum INVALID_ASSET_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_ASSET_URL_VALUE:I = 0x6f

.field public static final enum INVALID_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_BID_PAYLOAD_VALUE:I = 0xd0

.field public static final enum INVALID_CONFIG_RESPONSE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_CONFIG_RESPONSE_VALUE:I = 0x87

.field public static final enum INVALID_CTA_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_CTA_URL_VALUE:I = 0x6e

.field public static final enum INVALID_EVENT_ID_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_EVENT_ID_ERROR_VALUE:I = 0xc8

.field public static final enum INVALID_GZIP_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_GZIP_BID_PAYLOAD_VALUE:I = 0xd6

.field public static final enum INVALID_IFA_STATUS:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_IFA_STATUS_VALUE:I = 0x12e

.field public static final enum INVALID_INDEX_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_INDEX_URL_VALUE:I = 0x73

.field public static final enum INVALID_JSON_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_JSON_BID_PAYLOAD_VALUE:I = 0xd1

.field public static final enum INVALID_LOG_ERROR_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_LOG_ERROR_ENDPOINT_VALUE:I = 0x7c

.field public static final enum INVALID_METRICS_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_METRICS_ENDPOINT_VALUE:I = 0x7d

.field public static final enum INVALID_PLACEMENT_ID:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_PLACEMENT_ID_VALUE:I = 0xc9

.field public static final enum INVALID_REQUEST_BUILDER_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_REQUEST_BUILDER_ERROR_VALUE:I = 0x6a

.field public static final enum INVALID_RI_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_RI_ENDPOINT_VALUE:I = 0x7b

.field public static final enum INVALID_TEMPLATE_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_TEMPLATE_URL_VALUE:I = 0x69

.field public static final enum INVALID_TPAT_KEY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_TPAT_KEY_VALUE:I = 0x80

.field public static final enum INVALID_WATERFALL_PLACEMENT_ID:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final INVALID_WATERFALL_PLACEMENT_ID_VALUE:I = 0xde

.field public static final enum JSON_ENCODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final JSON_ENCODE_ERROR_VALUE:I = 0x77

.field public static final enum JSON_PARAMS_ENCODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final JSON_PARAMS_ENCODE_ERROR_VALUE:I = 0x13b

.field public static final enum LINK_COMMAND_OPEN_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final LINK_COMMAND_OPEN_FAILED_VALUE:I = 0x13a

.field public static final enum MRAID_BRIDGE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final MRAID_BRIDGE_ERROR_VALUE:I = 0x131

.field public static final enum MRAID_DOWNLOAD_JS_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final MRAID_DOWNLOAD_JS_ERROR_VALUE:I = 0x82

.field public static final enum MRAID_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final MRAID_ERROR_VALUE:I = 0x12d

.field public static final enum MRAID_JS_CALL_EMPTY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final MRAID_JS_CALL_EMPTY_VALUE:I = 0x137

.field public static final enum MRAID_JS_COPY_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final MRAID_JS_COPY_FAILED_VALUE:I = 0xdb

.field public static final enum MRAID_JS_DOES_NOT_EXIST:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final MRAID_JS_DOES_NOT_EXIST_VALUE:I = 0xda

.field public static final enum MRAID_JS_WRITE_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final MRAID_JS_WRITE_FAILED_VALUE:I = 0x83

.field public static final enum NATIVE_ASSET_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final NATIVE_ASSET_ERROR_VALUE:I = 0x258

.field public static final enum OMSDK_COPY_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final OMSDK_COPY_ERROR_VALUE:I = 0x7d3

.field public static final enum OMSDK_DOWNLOAD_JS_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final OMSDK_DOWNLOAD_JS_ERROR_VALUE:I = 0x84

.field public static final enum OMSDK_JS_WRITE_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final OMSDK_JS_WRITE_FAILED_VALUE:I = 0x85

.field public static final enum OUT_OF_MEMORY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final OUT_OF_MEMORY_VALUE:I = 0xbb9

.field public static final enum PLACEMENT_AD_TYPE_MISMATCH:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final PLACEMENT_AD_TYPE_MISMATCH_VALUE:I = 0xcf

.field public static final enum PLACEMENT_SLEEP:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final PLACEMENT_SLEEP_VALUE:I = 0xd4

.field public static final enum PRIVACY_URL_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final PRIVACY_URL_ERROR_VALUE:I = 0x88

.field public static final enum PROTOBUF_SERIALIZATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final PROTOBUF_SERIALIZATION_ERROR_VALUE:I = 0x76

.field public static final enum REACHABILITY_INITIALIZATION_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final REACHABILITY_INITIALIZATION_FAILED_VALUE:I = 0x7d5

.field public static final enum SDK_NOT_INITIALIZED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final SDK_NOT_INITIALIZED_VALUE:I = 0x6

.field public static final enum STORE_KIT_LOAD_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final STORE_KIT_LOAD_ERROR_VALUE:I = 0x7d2

.field public static final enum STORE_KIT_PRESENTATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final STORE_KIT_PRESENTATION_ERROR_VALUE:I = 0x7d7

.field public static final enum STORE_OVERLAY_LOAD_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final STORE_OVERLAY_LOAD_ERROR_VALUE:I = 0x7d4

.field public static final enum STORE_REGION_CODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final STORE_REGION_CODE_ERROR_VALUE:I = 0x86

.field public static final enum TEMPLATE_UNZIP_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final TEMPLATE_UNZIP_ERROR_VALUE:I = 0x6d

.field public static final enum TPAT_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final TPAT_ERROR_VALUE:I = 0x79

.field public static final enum TPAT_RETRY_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final TPAT_RETRY_FAILED_VALUE:I = 0x89

.field public static final enum UNKNOWN_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final UNKNOWN_ERROR_VALUE:I = 0x0

.field public static final enum UNKNOWN_RADIO_ACCESS_TECHNOLOGY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final UNKNOWN_RADIO_ACCESS_TECHNOLOGY_VALUE:I = 0x7d6

.field public static final enum UNRECOGNIZED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final enum USER_AGENT_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final USER_AGENT_ERROR_VALUE:I = 0x7

.field public static final enum WEB_VIEW_FAILED_NAVIGATION:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final WEB_VIEW_FAILED_NAVIGATION_VALUE:I = 0x7d1

.field public static final enum WEB_VIEW_WEB_CONTENT_PROCESS_DID_TERMINATE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

.field public static final WEB_VIEW_WEB_CONTENT_PROCESS_DID_TERMINATE_VALUE:I = 0x7d0

.field private static final internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap<",
            "Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 100

    .line 1
    new-instance v0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 2
    .line 3
    const-string v1, "UNKNOWN_ERROR"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2, v2}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->UNKNOWN_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 10
    .line 11
    new-instance v1, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 12
    .line 13
    const-string v3, "INVALID_APP_ID"

    .line 14
    .line 15
    const/4 v4, 0x1

    .line 16
    const/4 v5, 0x2

    .line 17
    invoke-direct {v1, v3, v4, v5}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 18
    .line 19
    .line 20
    sput-object v1, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_APP_ID:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 21
    .line 22
    new-instance v3, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 23
    .line 24
    const-string v6, "CURRENTLY_INITIALIZING"

    .line 25
    .line 26
    const/4 v7, 0x3

    .line 27
    invoke-direct {v3, v6, v5, v7}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 28
    .line 29
    .line 30
    sput-object v3, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->CURRENTLY_INITIALIZING:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 31
    .line 32
    new-instance v6, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 33
    .line 34
    const-string v8, "ALREADY_INITIALIZED"

    .line 35
    .line 36
    const/4 v9, 0x4

    .line 37
    invoke-direct {v6, v8, v7, v9}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 38
    .line 39
    .line 40
    sput-object v6, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ALREADY_INITIALIZED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 41
    .line 42
    new-instance v8, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 43
    .line 44
    const-string v10, "SDK_NOT_INITIALIZED"

    .line 45
    .line 46
    const/4 v11, 0x6

    .line 47
    invoke-direct {v8, v10, v9, v11}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 48
    .line 49
    .line 50
    sput-object v8, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->SDK_NOT_INITIALIZED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 51
    .line 52
    new-instance v10, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 53
    .line 54
    const-string v12, "USER_AGENT_ERROR"

    .line 55
    .line 56
    const/4 v13, 0x5

    .line 57
    const/4 v14, 0x7

    .line 58
    invoke-direct {v10, v12, v13, v14}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 59
    .line 60
    .line 61
    sput-object v10, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->USER_AGENT_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 62
    .line 63
    new-instance v12, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 64
    .line 65
    const-string v15, "API_REQUEST_ERROR"

    .line 66
    .line 67
    const/16 v13, 0x65

    .line 68
    .line 69
    invoke-direct {v12, v15, v11, v13}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 70
    .line 71
    .line 72
    sput-object v12, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->API_REQUEST_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 73
    .line 74
    new-instance v13, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 75
    .line 76
    const-string v15, "API_RESPONSE_DATA_ERROR"

    .line 77
    .line 78
    const/16 v11, 0x66

    .line 79
    .line 80
    invoke-direct {v13, v15, v14, v11}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 81
    .line 82
    .line 83
    sput-object v13, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->API_RESPONSE_DATA_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 84
    .line 85
    new-instance v11, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 86
    .line 87
    const/16 v15, 0x67

    .line 88
    .line 89
    const-string v14, "API_RESPONSE_DECODE_ERROR"

    .line 90
    .line 91
    const/16 v9, 0x8

    .line 92
    .line 93
    invoke-direct {v11, v14, v9, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 94
    .line 95
    .line 96
    sput-object v11, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->API_RESPONSE_DECODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 97
    .line 98
    new-instance v14, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 99
    .line 100
    const/16 v15, 0x68

    .line 101
    .line 102
    const-string v9, "API_FAILED_STATUS_CODE"

    .line 103
    .line 104
    const/16 v7, 0x9

    .line 105
    .line 106
    invoke-direct {v14, v9, v7, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 107
    .line 108
    .line 109
    sput-object v14, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->API_FAILED_STATUS_CODE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 110
    .line 111
    new-instance v9, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 112
    .line 113
    const/16 v15, 0x69

    .line 114
    .line 115
    const-string v7, "INVALID_TEMPLATE_URL"

    .line 116
    .line 117
    const/16 v5, 0xa

    .line 118
    .line 119
    invoke-direct {v9, v7, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 120
    .line 121
    .line 122
    sput-object v9, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_TEMPLATE_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 123
    .line 124
    new-instance v7, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 125
    .line 126
    const/16 v15, 0x6a

    .line 127
    .line 128
    const-string v5, "INVALID_REQUEST_BUILDER_ERROR"

    .line 129
    .line 130
    const/16 v4, 0xb

    .line 131
    .line 132
    invoke-direct {v7, v5, v4, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 133
    .line 134
    .line 135
    sput-object v7, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_REQUEST_BUILDER_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 136
    .line 137
    new-instance v5, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 138
    .line 139
    const/16 v15, 0x6d

    .line 140
    .line 141
    const-string v4, "TEMPLATE_UNZIP_ERROR"

    .line 142
    .line 143
    const/16 v2, 0xc

    .line 144
    .line 145
    invoke-direct {v5, v4, v2, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 146
    .line 147
    .line 148
    sput-object v5, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->TEMPLATE_UNZIP_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 149
    .line 150
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 151
    .line 152
    const/16 v15, 0x6e

    .line 153
    .line 154
    const-string v2, "INVALID_CTA_URL"

    .line 155
    .line 156
    move-object/from16 v16, v5

    .line 157
    .line 158
    const/16 v5, 0xd

    .line 159
    .line 160
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 161
    .line 162
    .line 163
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_CTA_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 164
    .line 165
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 166
    .line 167
    const/16 v15, 0x6f

    .line 168
    .line 169
    const-string v5, "INVALID_ASSET_URL"

    .line 170
    .line 171
    move-object/from16 v17, v4

    .line 172
    .line 173
    const/16 v4, 0xe

    .line 174
    .line 175
    invoke-direct {v2, v5, v4, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 176
    .line 177
    .line 178
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_ASSET_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 179
    .line 180
    new-instance v5, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 181
    .line 182
    const/16 v15, 0x70

    .line 183
    .line 184
    const-string v4, "ASSET_REQUEST_ERROR"

    .line 185
    .line 186
    move-object/from16 v18, v2

    .line 187
    .line 188
    const/16 v2, 0xf

    .line 189
    .line 190
    invoke-direct {v5, v4, v2, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 191
    .line 192
    .line 193
    sput-object v5, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_REQUEST_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 194
    .line 195
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 196
    .line 197
    const/16 v15, 0x71

    .line 198
    .line 199
    const-string v2, "ASSET_RESPONSE_DATA_ERROR"

    .line 200
    .line 201
    move-object/from16 v19, v5

    .line 202
    .line 203
    const/16 v5, 0x10

    .line 204
    .line 205
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 206
    .line 207
    .line 208
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_RESPONSE_DATA_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 209
    .line 210
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 211
    .line 212
    const/16 v15, 0x72

    .line 213
    .line 214
    const-string v5, "ASSET_WRITE_ERROR"

    .line 215
    .line 216
    move-object/from16 v20, v4

    .line 217
    .line 218
    const/16 v4, 0x11

    .line 219
    .line 220
    invoke-direct {v2, v5, v4, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 221
    .line 222
    .line 223
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_WRITE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 224
    .line 225
    new-instance v5, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 226
    .line 227
    const/16 v15, 0x73

    .line 228
    .line 229
    const-string v4, "INVALID_INDEX_URL"

    .line 230
    .line 231
    move-object/from16 v21, v2

    .line 232
    .line 233
    const/16 v2, 0x12

    .line 234
    .line 235
    invoke-direct {v5, v4, v2, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 236
    .line 237
    .line 238
    sput-object v5, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_INDEX_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 239
    .line 240
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 241
    .line 242
    const/16 v15, 0x74

    .line 243
    .line 244
    const-string v2, "GZIP_ENCODE_ERROR"

    .line 245
    .line 246
    move-object/from16 v22, v5

    .line 247
    .line 248
    const/16 v5, 0x13

    .line 249
    .line 250
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 251
    .line 252
    .line 253
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->GZIP_ENCODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 254
    .line 255
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 256
    .line 257
    const/16 v15, 0x75

    .line 258
    .line 259
    const-string v5, "ASSET_FAILED_STATUS_CODE"

    .line 260
    .line 261
    move-object/from16 v23, v4

    .line 262
    .line 263
    const/16 v4, 0x14

    .line 264
    .line 265
    invoke-direct {v2, v5, v4, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 266
    .line 267
    .line 268
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_FAILED_STATUS_CODE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 269
    .line 270
    new-instance v5, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 271
    .line 272
    const/16 v15, 0x76

    .line 273
    .line 274
    const-string v4, "PROTOBUF_SERIALIZATION_ERROR"

    .line 275
    .line 276
    move-object/from16 v24, v2

    .line 277
    .line 278
    const/16 v2, 0x15

    .line 279
    .line 280
    invoke-direct {v5, v4, v2, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 281
    .line 282
    .line 283
    sput-object v5, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->PROTOBUF_SERIALIZATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 284
    .line 285
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 286
    .line 287
    const/16 v15, 0x16

    .line 288
    .line 289
    const/16 v2, 0x77

    .line 290
    .line 291
    move-object/from16 v25, v5

    .line 292
    .line 293
    const-string v5, "JSON_ENCODE_ERROR"

    .line 294
    .line 295
    invoke-direct {v4, v5, v15, v2}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 296
    .line 297
    .line 298
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->JSON_ENCODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 299
    .line 300
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 301
    .line 302
    const/16 v5, 0x17

    .line 303
    .line 304
    const/16 v15, 0x79

    .line 305
    .line 306
    move-object/from16 v26, v4

    .line 307
    .line 308
    const-string v4, "TPAT_ERROR"

    .line 309
    .line 310
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 311
    .line 312
    .line 313
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->TPAT_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 314
    .line 315
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 316
    .line 317
    const/16 v5, 0x18

    .line 318
    .line 319
    const/16 v15, 0x7a

    .line 320
    .line 321
    move-object/from16 v27, v2

    .line 322
    .line 323
    const-string v2, "INVALID_ADS_ENDPOINT"

    .line 324
    .line 325
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 326
    .line 327
    .line 328
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_ADS_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 329
    .line 330
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 331
    .line 332
    const/16 v5, 0x19

    .line 333
    .line 334
    const/16 v15, 0x7b

    .line 335
    .line 336
    move-object/from16 v28, v4

    .line 337
    .line 338
    const-string v4, "INVALID_RI_ENDPOINT"

    .line 339
    .line 340
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 341
    .line 342
    .line 343
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_RI_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 344
    .line 345
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 346
    .line 347
    const/16 v5, 0x1a

    .line 348
    .line 349
    const/16 v15, 0x7c

    .line 350
    .line 351
    move-object/from16 v29, v2

    .line 352
    .line 353
    const-string v2, "INVALID_LOG_ERROR_ENDPOINT"

    .line 354
    .line 355
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 356
    .line 357
    .line 358
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_LOG_ERROR_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 359
    .line 360
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 361
    .line 362
    const/16 v5, 0x1b

    .line 363
    .line 364
    const/16 v15, 0x7d

    .line 365
    .line 366
    move-object/from16 v30, v4

    .line 367
    .line 368
    const-string v4, "INVALID_METRICS_ENDPOINT"

    .line 369
    .line 370
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 371
    .line 372
    .line 373
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_METRICS_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 374
    .line 375
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 376
    .line 377
    const/16 v5, 0x1c

    .line 378
    .line 379
    const/16 v15, 0x7e

    .line 380
    .line 381
    move-object/from16 v31, v2

    .line 382
    .line 383
    const-string v2, "ASSET_FAILED_INSUFFICIENT_SPACE"

    .line 384
    .line 385
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 386
    .line 387
    .line 388
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_FAILED_INSUFFICIENT_SPACE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 389
    .line 390
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 391
    .line 392
    const/16 v5, 0x1d

    .line 393
    .line 394
    const/16 v15, 0x7f

    .line 395
    .line 396
    move-object/from16 v32, v4

    .line 397
    .line 398
    const-string v4, "ASSET_FAILED_MAX_SPACE_EXCEEDED"

    .line 399
    .line 400
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 401
    .line 402
    .line 403
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_FAILED_MAX_SPACE_EXCEEDED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 404
    .line 405
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 406
    .line 407
    const/16 v5, 0x1e

    .line 408
    .line 409
    const/16 v15, 0x80

    .line 410
    .line 411
    move-object/from16 v33, v2

    .line 412
    .line 413
    const-string v2, "INVALID_TPAT_KEY"

    .line 414
    .line 415
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 416
    .line 417
    .line 418
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_TPAT_KEY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 419
    .line 420
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 421
    .line 422
    const/16 v5, 0x1f

    .line 423
    .line 424
    const/16 v15, 0x81

    .line 425
    .line 426
    move-object/from16 v34, v4

    .line 427
    .line 428
    const-string v4, "EMPTY_TPAT_ERROR"

    .line 429
    .line 430
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 431
    .line 432
    .line 433
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->EMPTY_TPAT_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 434
    .line 435
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 436
    .line 437
    const/16 v5, 0x20

    .line 438
    .line 439
    const/16 v15, 0x82

    .line 440
    .line 441
    move-object/from16 v35, v2

    .line 442
    .line 443
    const-string v2, "MRAID_DOWNLOAD_JS_ERROR"

    .line 444
    .line 445
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 446
    .line 447
    .line 448
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_DOWNLOAD_JS_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 449
    .line 450
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 451
    .line 452
    const/16 v5, 0x21

    .line 453
    .line 454
    const/16 v15, 0x83

    .line 455
    .line 456
    move-object/from16 v36, v4

    .line 457
    .line 458
    const-string v4, "MRAID_JS_WRITE_FAILED"

    .line 459
    .line 460
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 461
    .line 462
    .line 463
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_JS_WRITE_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 464
    .line 465
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 466
    .line 467
    const/16 v5, 0x22

    .line 468
    .line 469
    const/16 v15, 0x84

    .line 470
    .line 471
    move-object/from16 v37, v2

    .line 472
    .line 473
    const-string v2, "OMSDK_DOWNLOAD_JS_ERROR"

    .line 474
    .line 475
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 476
    .line 477
    .line 478
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->OMSDK_DOWNLOAD_JS_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 479
    .line 480
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 481
    .line 482
    const/16 v5, 0x23

    .line 483
    .line 484
    const/16 v15, 0x85

    .line 485
    .line 486
    move-object/from16 v38, v4

    .line 487
    .line 488
    const-string v4, "OMSDK_JS_WRITE_FAILED"

    .line 489
    .line 490
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 491
    .line 492
    .line 493
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->OMSDK_JS_WRITE_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 494
    .line 495
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 496
    .line 497
    const/16 v5, 0x24

    .line 498
    .line 499
    const/16 v15, 0x86

    .line 500
    .line 501
    move-object/from16 v39, v2

    .line 502
    .line 503
    const-string v2, "STORE_REGION_CODE_ERROR"

    .line 504
    .line 505
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 506
    .line 507
    .line 508
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->STORE_REGION_CODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 509
    .line 510
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 511
    .line 512
    const/16 v5, 0x25

    .line 513
    .line 514
    const/16 v15, 0x87

    .line 515
    .line 516
    move-object/from16 v40, v4

    .line 517
    .line 518
    const-string v4, "INVALID_CONFIG_RESPONSE"

    .line 519
    .line 520
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 521
    .line 522
    .line 523
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_CONFIG_RESPONSE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 524
    .line 525
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 526
    .line 527
    const/16 v5, 0x26

    .line 528
    .line 529
    const/16 v15, 0x88

    .line 530
    .line 531
    move-object/from16 v41, v2

    .line 532
    .line 533
    const-string v2, "PRIVACY_URL_ERROR"

    .line 534
    .line 535
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 536
    .line 537
    .line 538
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->PRIVACY_URL_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 539
    .line 540
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 541
    .line 542
    const/16 v5, 0x27

    .line 543
    .line 544
    const/16 v15, 0x89

    .line 545
    .line 546
    move-object/from16 v42, v4

    .line 547
    .line 548
    const-string v4, "TPAT_RETRY_FAILED"

    .line 549
    .line 550
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 551
    .line 552
    .line 553
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->TPAT_RETRY_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 554
    .line 555
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 556
    .line 557
    const/16 v5, 0x28

    .line 558
    .line 559
    const/16 v15, 0x8a

    .line 560
    .line 561
    move-object/from16 v43, v2

    .line 562
    .line 563
    const-string v2, "CONFIG_REFRESH_FAILED"

    .line 564
    .line 565
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 566
    .line 567
    .line 568
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->CONFIG_REFRESH_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 569
    .line 570
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 571
    .line 572
    const/16 v5, 0x29

    .line 573
    .line 574
    const/16 v15, 0xc8

    .line 575
    .line 576
    move-object/from16 v44, v4

    .line 577
    .line 578
    const-string v4, "INVALID_EVENT_ID_ERROR"

    .line 579
    .line 580
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 581
    .line 582
    .line 583
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_EVENT_ID_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 584
    .line 585
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 586
    .line 587
    const/16 v5, 0x2a

    .line 588
    .line 589
    const/16 v15, 0xc9

    .line 590
    .line 591
    move-object/from16 v45, v2

    .line 592
    .line 593
    const-string v2, "INVALID_PLACEMENT_ID"

    .line 594
    .line 595
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 596
    .line 597
    .line 598
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_PLACEMENT_ID:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 599
    .line 600
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 601
    .line 602
    const/16 v5, 0x2b

    .line 603
    .line 604
    const/16 v15, 0xca

    .line 605
    .line 606
    move-object/from16 v46, v4

    .line 607
    .line 608
    const-string v4, "AD_CONSUMED"

    .line 609
    .line 610
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 611
    .line 612
    .line 613
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_CONSUMED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 614
    .line 615
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 616
    .line 617
    const/16 v5, 0x2c

    .line 618
    .line 619
    const/16 v15, 0xcb

    .line 620
    .line 621
    move-object/from16 v47, v2

    .line 622
    .line 623
    const-string v2, "AD_IS_LOADING"

    .line 624
    .line 625
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 626
    .line 627
    .line 628
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_IS_LOADING:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 629
    .line 630
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 631
    .line 632
    const/16 v5, 0x2d

    .line 633
    .line 634
    const/16 v15, 0xcc

    .line 635
    .line 636
    move-object/from16 v48, v4

    .line 637
    .line 638
    const-string v4, "AD_ALREADY_LOADED"

    .line 639
    .line 640
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 641
    .line 642
    .line 643
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_ALREADY_LOADED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 644
    .line 645
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 646
    .line 647
    const/16 v5, 0x2e

    .line 648
    .line 649
    const/16 v15, 0xcd

    .line 650
    .line 651
    move-object/from16 v49, v2

    .line 652
    .line 653
    const-string v2, "AD_IS_PLAYING"

    .line 654
    .line 655
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 656
    .line 657
    .line 658
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_IS_PLAYING:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 659
    .line 660
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 661
    .line 662
    const/16 v5, 0x2f

    .line 663
    .line 664
    const/16 v15, 0xce

    .line 665
    .line 666
    move-object/from16 v50, v4

    .line 667
    .line 668
    const-string v4, "AD_ALREADY_FAILED"

    .line 669
    .line 670
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 671
    .line 672
    .line 673
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_ALREADY_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 674
    .line 675
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 676
    .line 677
    const/16 v5, 0x30

    .line 678
    .line 679
    const/16 v15, 0xcf

    .line 680
    .line 681
    move-object/from16 v51, v2

    .line 682
    .line 683
    const-string v2, "PLACEMENT_AD_TYPE_MISMATCH"

    .line 684
    .line 685
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 686
    .line 687
    .line 688
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->PLACEMENT_AD_TYPE_MISMATCH:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 689
    .line 690
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 691
    .line 692
    const/16 v5, 0x31

    .line 693
    .line 694
    const/16 v15, 0xd0

    .line 695
    .line 696
    move-object/from16 v52, v4

    .line 697
    .line 698
    const-string v4, "INVALID_BID_PAYLOAD"

    .line 699
    .line 700
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 701
    .line 702
    .line 703
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 704
    .line 705
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 706
    .line 707
    const/16 v5, 0x32

    .line 708
    .line 709
    const/16 v15, 0xd1

    .line 710
    .line 711
    move-object/from16 v53, v2

    .line 712
    .line 713
    const-string v2, "INVALID_JSON_BID_PAYLOAD"

    .line 714
    .line 715
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 716
    .line 717
    .line 718
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_JSON_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 719
    .line 720
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 721
    .line 722
    const/16 v5, 0x33

    .line 723
    .line 724
    const/16 v15, 0xd2

    .line 725
    .line 726
    move-object/from16 v54, v4

    .line 727
    .line 728
    const-string v4, "AD_NOT_LOADED"

    .line 729
    .line 730
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 731
    .line 732
    .line 733
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_NOT_LOADED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 734
    .line 735
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 736
    .line 737
    const/16 v5, 0x34

    .line 738
    .line 739
    const/16 v15, 0xd4

    .line 740
    .line 741
    move-object/from16 v55, v2

    .line 742
    .line 743
    const-string v2, "PLACEMENT_SLEEP"

    .line 744
    .line 745
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 746
    .line 747
    .line 748
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->PLACEMENT_SLEEP:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 749
    .line 750
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 751
    .line 752
    const/16 v5, 0x35

    .line 753
    .line 754
    const/16 v15, 0xd5

    .line 755
    .line 756
    move-object/from16 v56, v4

    .line 757
    .line 758
    const-string v4, "INVALID_ADUNIT_BID_PAYLOAD"

    .line 759
    .line 760
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 761
    .line 762
    .line 763
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_ADUNIT_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 764
    .line 765
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 766
    .line 767
    const/16 v5, 0x36

    .line 768
    .line 769
    const/16 v15, 0xd6

    .line 770
    .line 771
    move-object/from16 v57, v2

    .line 772
    .line 773
    const-string v2, "INVALID_GZIP_BID_PAYLOAD"

    .line 774
    .line 775
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 776
    .line 777
    .line 778
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_GZIP_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 779
    .line 780
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 781
    .line 782
    const/16 v5, 0x37

    .line 783
    .line 784
    const/16 v15, 0xd7

    .line 785
    .line 786
    move-object/from16 v58, v4

    .line 787
    .line 788
    const-string v4, "AD_RESPONSE_EMPTY"

    .line 789
    .line 790
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 791
    .line 792
    .line 793
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_RESPONSE_EMPTY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 794
    .line 795
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 796
    .line 797
    const/16 v5, 0x38

    .line 798
    .line 799
    const/16 v15, 0xd8

    .line 800
    .line 801
    move-object/from16 v59, v2

    .line 802
    .line 803
    const-string v2, "AD_RESPONSE_INVALID_TEMPLATE_TYPE"

    .line 804
    .line 805
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 806
    .line 807
    .line 808
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_RESPONSE_INVALID_TEMPLATE_TYPE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 809
    .line 810
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 811
    .line 812
    const/16 v5, 0x39

    .line 813
    .line 814
    const/16 v15, 0xd9

    .line 815
    .line 816
    move-object/from16 v60, v4

    .line 817
    .line 818
    const-string v4, "AD_RESPONSE_TIMED_OUT"

    .line 819
    .line 820
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 821
    .line 822
    .line 823
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_RESPONSE_TIMED_OUT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 824
    .line 825
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 826
    .line 827
    const/16 v5, 0x3a

    .line 828
    .line 829
    const/16 v15, 0xda

    .line 830
    .line 831
    move-object/from16 v61, v2

    .line 832
    .line 833
    const-string v2, "MRAID_JS_DOES_NOT_EXIST"

    .line 834
    .line 835
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 836
    .line 837
    .line 838
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_JS_DOES_NOT_EXIST:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 839
    .line 840
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 841
    .line 842
    const/16 v5, 0x3b

    .line 843
    .line 844
    const/16 v15, 0xdb

    .line 845
    .line 846
    move-object/from16 v62, v4

    .line 847
    .line 848
    const-string v4, "MRAID_JS_COPY_FAILED"

    .line 849
    .line 850
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 851
    .line 852
    .line 853
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_JS_COPY_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 854
    .line 855
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 856
    .line 857
    const/16 v5, 0x3c

    .line 858
    .line 859
    const/16 v15, 0xdc

    .line 860
    .line 861
    move-object/from16 v63, v2

    .line 862
    .line 863
    const-string v2, "AD_RESPONSE_RETRY_AFTER"

    .line 864
    .line 865
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 866
    .line 867
    .line 868
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_RESPONSE_RETRY_AFTER:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 869
    .line 870
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 871
    .line 872
    const/16 v5, 0x3d

    .line 873
    .line 874
    const/16 v15, 0xdd

    .line 875
    .line 876
    move-object/from16 v64, v4

    .line 877
    .line 878
    const-string v4, "AD_LOAD_FAIL_RETRY_AFTER"

    .line 879
    .line 880
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 881
    .line 882
    .line 883
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_LOAD_FAIL_RETRY_AFTER:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 884
    .line 885
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 886
    .line 887
    const/16 v5, 0x3e

    .line 888
    .line 889
    const/16 v15, 0xde

    .line 890
    .line 891
    move-object/from16 v65, v2

    .line 892
    .line 893
    const-string v2, "INVALID_WATERFALL_PLACEMENT_ID"

    .line 894
    .line 895
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 896
    .line 897
    .line 898
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_WATERFALL_PLACEMENT_ID:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 899
    .line 900
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 901
    .line 902
    const/16 v5, 0x3f

    .line 903
    .line 904
    const/16 v15, 0x2711

    .line 905
    .line 906
    move-object/from16 v66, v4

    .line 907
    .line 908
    const-string v4, "AD_NO_FILL"

    .line 909
    .line 910
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 911
    .line 912
    .line 913
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_NO_FILL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 914
    .line 915
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 916
    .line 917
    const/16 v5, 0x40

    .line 918
    .line 919
    const/16 v15, 0x2712

    .line 920
    .line 921
    move-object/from16 v67, v2

    .line 922
    .line 923
    const-string v2, "AD_LOAD_TOO_FREQUENTLY"

    .line 924
    .line 925
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 926
    .line 927
    .line 928
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_LOAD_TOO_FREQUENTLY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 929
    .line 930
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 931
    .line 932
    const/16 v5, 0x41

    .line 933
    .line 934
    const/16 v15, 0x4e21

    .line 935
    .line 936
    move-object/from16 v68, v4

    .line 937
    .line 938
    const-string v4, "AD_SERVER_ERROR"

    .line 939
    .line 940
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 941
    .line 942
    .line 943
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_SERVER_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 944
    .line 945
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 946
    .line 947
    const/16 v5, 0x42

    .line 948
    .line 949
    const/16 v15, 0x7531

    .line 950
    .line 951
    move-object/from16 v69, v2

    .line 952
    .line 953
    const-string v2, "AD_PUBLISHER_MISMATCH"

    .line 954
    .line 955
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 956
    .line 957
    .line 958
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_PUBLISHER_MISMATCH:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 959
    .line 960
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 961
    .line 962
    const/16 v5, 0x43

    .line 963
    .line 964
    const/16 v15, 0x7532

    .line 965
    .line 966
    move-object/from16 v70, v4

    .line 967
    .line 968
    const-string v4, "AD_INTERNAL_INTEGRATION_ERROR"

    .line 969
    .line 970
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 971
    .line 972
    .line 973
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_INTERNAL_INTEGRATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 974
    .line 975
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 976
    .line 977
    const/16 v5, 0x44

    .line 978
    .line 979
    const/16 v15, 0x12d

    .line 980
    .line 981
    move-object/from16 v71, v2

    .line 982
    .line 983
    const-string v2, "MRAID_ERROR"

    .line 984
    .line 985
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 986
    .line 987
    .line 988
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 989
    .line 990
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 991
    .line 992
    const/16 v5, 0x45

    .line 993
    .line 994
    const/16 v15, 0x12e

    .line 995
    .line 996
    move-object/from16 v72, v4

    .line 997
    .line 998
    const-string v4, "INVALID_IFA_STATUS"

    .line 999
    .line 1000
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1001
    .line 1002
    .line 1003
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_IFA_STATUS:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1004
    .line 1005
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1006
    .line 1007
    const/16 v5, 0x46

    .line 1008
    .line 1009
    const/16 v15, 0x130

    .line 1010
    .line 1011
    move-object/from16 v73, v2

    .line 1012
    .line 1013
    const-string v2, "AD_EXPIRED"

    .line 1014
    .line 1015
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1016
    .line 1017
    .line 1018
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_EXPIRED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1019
    .line 1020
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1021
    .line 1022
    const/16 v5, 0x47

    .line 1023
    .line 1024
    const/16 v15, 0x131

    .line 1025
    .line 1026
    move-object/from16 v74, v4

    .line 1027
    .line 1028
    const-string v4, "MRAID_BRIDGE_ERROR"

    .line 1029
    .line 1030
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1031
    .line 1032
    .line 1033
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_BRIDGE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1034
    .line 1035
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1036
    .line 1037
    const/16 v5, 0x48

    .line 1038
    .line 1039
    const/16 v15, 0x133

    .line 1040
    .line 1041
    move-object/from16 v75, v2

    .line 1042
    .line 1043
    const-string v2, "AD_EXPIRED_ON_PLAY"

    .line 1044
    .line 1045
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1046
    .line 1047
    .line 1048
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_EXPIRED_ON_PLAY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1049
    .line 1050
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1051
    .line 1052
    const/16 v5, 0x49

    .line 1053
    .line 1054
    const/16 v15, 0x134

    .line 1055
    .line 1056
    move-object/from16 v76, v4

    .line 1057
    .line 1058
    const-string v4, "AD_WIN_NOTIFICATION_ERROR"

    .line 1059
    .line 1060
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1061
    .line 1062
    .line 1063
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_WIN_NOTIFICATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1064
    .line 1065
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1066
    .line 1067
    const/16 v5, 0x4a

    .line 1068
    .line 1069
    const/16 v15, 0x135

    .line 1070
    .line 1071
    move-object/from16 v77, v2

    .line 1072
    .line 1073
    const-string v2, "ASSET_FAILED_TO_DELETE"

    .line 1074
    .line 1075
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1076
    .line 1077
    .line 1078
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_FAILED_TO_DELETE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1079
    .line 1080
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1081
    .line 1082
    const/16 v5, 0x4b

    .line 1083
    .line 1084
    const/16 v15, 0x136

    .line 1085
    .line 1086
    move-object/from16 v78, v4

    .line 1087
    .line 1088
    const-string v4, "AD_HTML_FAILED_TO_LOAD"

    .line 1089
    .line 1090
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1091
    .line 1092
    .line 1093
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_HTML_FAILED_TO_LOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1094
    .line 1095
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1096
    .line 1097
    const/16 v5, 0x4c

    .line 1098
    .line 1099
    const/16 v15, 0x137

    .line 1100
    .line 1101
    move-object/from16 v79, v2

    .line 1102
    .line 1103
    const-string v2, "MRAID_JS_CALL_EMPTY"

    .line 1104
    .line 1105
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1106
    .line 1107
    .line 1108
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_JS_CALL_EMPTY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1109
    .line 1110
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1111
    .line 1112
    const/16 v5, 0x4d

    .line 1113
    .line 1114
    const/16 v15, 0x138

    .line 1115
    .line 1116
    move-object/from16 v80, v4

    .line 1117
    .line 1118
    const-string v4, "DEEPLINK_OPEN_FAILED"

    .line 1119
    .line 1120
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1121
    .line 1122
    .line 1123
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->DEEPLINK_OPEN_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1124
    .line 1125
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1126
    .line 1127
    const/16 v5, 0x4e

    .line 1128
    .line 1129
    const/16 v15, 0x139

    .line 1130
    .line 1131
    move-object/from16 v81, v2

    .line 1132
    .line 1133
    const-string v2, "EVALUATE_JAVASCRIPT_FAILED"

    .line 1134
    .line 1135
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1136
    .line 1137
    .line 1138
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->EVALUATE_JAVASCRIPT_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1139
    .line 1140
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1141
    .line 1142
    const/16 v5, 0x4f

    .line 1143
    .line 1144
    const/16 v15, 0x13a

    .line 1145
    .line 1146
    move-object/from16 v82, v4

    .line 1147
    .line 1148
    const-string v4, "LINK_COMMAND_OPEN_FAILED"

    .line 1149
    .line 1150
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1151
    .line 1152
    .line 1153
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->LINK_COMMAND_OPEN_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1154
    .line 1155
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1156
    .line 1157
    const/16 v5, 0x50

    .line 1158
    .line 1159
    const/16 v15, 0x13b

    .line 1160
    .line 1161
    move-object/from16 v83, v2

    .line 1162
    .line 1163
    const-string v2, "JSON_PARAMS_ENCODE_ERROR"

    .line 1164
    .line 1165
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1166
    .line 1167
    .line 1168
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->JSON_PARAMS_ENCODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1169
    .line 1170
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1171
    .line 1172
    const/16 v5, 0x51

    .line 1173
    .line 1174
    const/16 v15, 0x13c

    .line 1175
    .line 1176
    move-object/from16 v84, v4

    .line 1177
    .line 1178
    const-string v4, "GENERATE_JSON_DATA_ERROR"

    .line 1179
    .line 1180
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1181
    .line 1182
    .line 1183
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->GENERATE_JSON_DATA_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1184
    .line 1185
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1186
    .line 1187
    const/16 v5, 0x52

    .line 1188
    .line 1189
    const/16 v15, 0x13d

    .line 1190
    .line 1191
    move-object/from16 v85, v2

    .line 1192
    .line 1193
    const-string v2, "AD_CLOSED_TEMPLATE_ERROR"

    .line 1194
    .line 1195
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1196
    .line 1197
    .line 1198
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_CLOSED_TEMPLATE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1199
    .line 1200
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1201
    .line 1202
    const/16 v5, 0x53

    .line 1203
    .line 1204
    const/16 v15, 0x13e

    .line 1205
    .line 1206
    move-object/from16 v86, v4

    .line 1207
    .line 1208
    const-string v4, "AD_CLOSED_MISSING_HEARTBEAT"

    .line 1209
    .line 1210
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1211
    .line 1212
    .line 1213
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_CLOSED_MISSING_HEARTBEAT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1214
    .line 1215
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1216
    .line 1217
    const/16 v5, 0x54

    .line 1218
    .line 1219
    const/16 v15, 0x190

    .line 1220
    .line 1221
    move-object/from16 v87, v2

    .line 1222
    .line 1223
    const-string v2, "CONCURRENT_PLAYBACK_UNSUPPORTED"

    .line 1224
    .line 1225
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1226
    .line 1227
    .line 1228
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->CONCURRENT_PLAYBACK_UNSUPPORTED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1229
    .line 1230
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1231
    .line 1232
    const/16 v5, 0x55

    .line 1233
    .line 1234
    const/16 v15, 0x1f4

    .line 1235
    .line 1236
    move-object/from16 v88, v4

    .line 1237
    .line 1238
    const-string v4, "BANNER_VIEW_INVALID_SIZE"

    .line 1239
    .line 1240
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1241
    .line 1242
    .line 1243
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->BANNER_VIEW_INVALID_SIZE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1244
    .line 1245
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1246
    .line 1247
    const/16 v5, 0x56

    .line 1248
    .line 1249
    const/16 v15, 0x258

    .line 1250
    .line 1251
    move-object/from16 v89, v2

    .line 1252
    .line 1253
    const-string v2, "NATIVE_ASSET_ERROR"

    .line 1254
    .line 1255
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1256
    .line 1257
    .line 1258
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->NATIVE_ASSET_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1259
    .line 1260
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1261
    .line 1262
    const/16 v5, 0x57

    .line 1263
    .line 1264
    const/16 v15, 0x7d0

    .line 1265
    .line 1266
    move-object/from16 v90, v4

    .line 1267
    .line 1268
    const-string v4, "WEB_VIEW_WEB_CONTENT_PROCESS_DID_TERMINATE"

    .line 1269
    .line 1270
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1271
    .line 1272
    .line 1273
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->WEB_VIEW_WEB_CONTENT_PROCESS_DID_TERMINATE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1274
    .line 1275
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1276
    .line 1277
    const/16 v5, 0x58

    .line 1278
    .line 1279
    const/16 v15, 0x7d1

    .line 1280
    .line 1281
    move-object/from16 v91, v2

    .line 1282
    .line 1283
    const-string v2, "WEB_VIEW_FAILED_NAVIGATION"

    .line 1284
    .line 1285
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1286
    .line 1287
    .line 1288
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->WEB_VIEW_FAILED_NAVIGATION:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1289
    .line 1290
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1291
    .line 1292
    const/16 v5, 0x59

    .line 1293
    .line 1294
    const/16 v15, 0x7d2

    .line 1295
    .line 1296
    move-object/from16 v92, v4

    .line 1297
    .line 1298
    const-string v4, "STORE_KIT_LOAD_ERROR"

    .line 1299
    .line 1300
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1301
    .line 1302
    .line 1303
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->STORE_KIT_LOAD_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1304
    .line 1305
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1306
    .line 1307
    const/16 v5, 0x5a

    .line 1308
    .line 1309
    const/16 v15, 0x7d3

    .line 1310
    .line 1311
    move-object/from16 v93, v2

    .line 1312
    .line 1313
    const-string v2, "OMSDK_COPY_ERROR"

    .line 1314
    .line 1315
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1316
    .line 1317
    .line 1318
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->OMSDK_COPY_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1319
    .line 1320
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1321
    .line 1322
    const/16 v5, 0x5b

    .line 1323
    .line 1324
    const/16 v15, 0x7d4

    .line 1325
    .line 1326
    move-object/from16 v94, v4

    .line 1327
    .line 1328
    const-string v4, "STORE_OVERLAY_LOAD_ERROR"

    .line 1329
    .line 1330
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1331
    .line 1332
    .line 1333
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->STORE_OVERLAY_LOAD_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1334
    .line 1335
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1336
    .line 1337
    const/16 v5, 0x5c

    .line 1338
    .line 1339
    const/16 v15, 0x7d5

    .line 1340
    .line 1341
    move-object/from16 v95, v2

    .line 1342
    .line 1343
    const-string v2, "REACHABILITY_INITIALIZATION_FAILED"

    .line 1344
    .line 1345
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1346
    .line 1347
    .line 1348
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->REACHABILITY_INITIALIZATION_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1349
    .line 1350
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1351
    .line 1352
    const/16 v5, 0x5d

    .line 1353
    .line 1354
    const/16 v15, 0x7d6

    .line 1355
    .line 1356
    move-object/from16 v96, v4

    .line 1357
    .line 1358
    const-string v4, "UNKNOWN_RADIO_ACCESS_TECHNOLOGY"

    .line 1359
    .line 1360
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1361
    .line 1362
    .line 1363
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->UNKNOWN_RADIO_ACCESS_TECHNOLOGY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1364
    .line 1365
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1366
    .line 1367
    const/16 v5, 0x5e

    .line 1368
    .line 1369
    const/16 v15, 0x7d7

    .line 1370
    .line 1371
    move-object/from16 v97, v2

    .line 1372
    .line 1373
    const-string v2, "STORE_KIT_PRESENTATION_ERROR"

    .line 1374
    .line 1375
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1376
    .line 1377
    .line 1378
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->STORE_KIT_PRESENTATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1379
    .line 1380
    new-instance v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1381
    .line 1382
    const/16 v5, 0x5f

    .line 1383
    .line 1384
    const/16 v15, 0xbb9

    .line 1385
    .line 1386
    move-object/from16 v98, v4

    .line 1387
    .line 1388
    const-string v4, "OUT_OF_MEMORY"

    .line 1389
    .line 1390
    invoke-direct {v2, v4, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1391
    .line 1392
    .line 1393
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->OUT_OF_MEMORY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1394
    .line 1395
    new-instance v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1396
    .line 1397
    const/16 v5, 0x60

    .line 1398
    .line 1399
    const/4 v15, -0x1

    .line 1400
    move-object/from16 v99, v2

    .line 1401
    .line 1402
    const-string v2, "UNRECOGNIZED"

    .line 1403
    .line 1404
    invoke-direct {v4, v2, v5, v15}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;-><init>(Ljava/lang/String;II)V

    .line 1405
    .line 1406
    .line 1407
    sput-object v4, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->UNRECOGNIZED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1408
    .line 1409
    const/16 v2, 0x61

    .line 1410
    .line 1411
    new-array v2, v2, [Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1412
    .line 1413
    const/4 v5, 0x0

    .line 1414
    aput-object v0, v2, v5

    .line 1415
    .line 1416
    const/4 v0, 0x1

    .line 1417
    aput-object v1, v2, v0

    .line 1418
    .line 1419
    const/4 v0, 0x2

    .line 1420
    aput-object v3, v2, v0

    .line 1421
    .line 1422
    const/4 v0, 0x3

    .line 1423
    aput-object v6, v2, v0

    .line 1424
    .line 1425
    const/4 v0, 0x4

    .line 1426
    aput-object v8, v2, v0

    .line 1427
    .line 1428
    const/4 v0, 0x5

    .line 1429
    aput-object v10, v2, v0

    .line 1430
    .line 1431
    const/4 v0, 0x6

    .line 1432
    aput-object v12, v2, v0

    .line 1433
    .line 1434
    const/4 v0, 0x7

    .line 1435
    aput-object v13, v2, v0

    .line 1436
    .line 1437
    const/16 v0, 0x8

    .line 1438
    .line 1439
    aput-object v11, v2, v0

    .line 1440
    .line 1441
    const/16 v0, 0x9

    .line 1442
    .line 1443
    aput-object v14, v2, v0

    .line 1444
    .line 1445
    const/16 v0, 0xa

    .line 1446
    .line 1447
    aput-object v9, v2, v0

    .line 1448
    .line 1449
    const/16 v0, 0xb

    .line 1450
    .line 1451
    aput-object v7, v2, v0

    .line 1452
    .line 1453
    const/16 v0, 0xc

    .line 1454
    .line 1455
    aput-object v16, v2, v0

    .line 1456
    .line 1457
    const/16 v0, 0xd

    .line 1458
    .line 1459
    aput-object v17, v2, v0

    .line 1460
    .line 1461
    const/16 v0, 0xe

    .line 1462
    .line 1463
    aput-object v18, v2, v0

    .line 1464
    .line 1465
    const/16 v0, 0xf

    .line 1466
    .line 1467
    aput-object v19, v2, v0

    .line 1468
    .line 1469
    const/16 v0, 0x10

    .line 1470
    .line 1471
    aput-object v20, v2, v0

    .line 1472
    .line 1473
    const/16 v0, 0x11

    .line 1474
    .line 1475
    aput-object v21, v2, v0

    .line 1476
    .line 1477
    const/16 v0, 0x12

    .line 1478
    .line 1479
    aput-object v22, v2, v0

    .line 1480
    .line 1481
    const/16 v0, 0x13

    .line 1482
    .line 1483
    aput-object v23, v2, v0

    .line 1484
    .line 1485
    const/16 v0, 0x14

    .line 1486
    .line 1487
    aput-object v24, v2, v0

    .line 1488
    .line 1489
    const/16 v0, 0x15

    .line 1490
    .line 1491
    aput-object v25, v2, v0

    .line 1492
    .line 1493
    const/16 v0, 0x16

    .line 1494
    .line 1495
    aput-object v26, v2, v0

    .line 1496
    .line 1497
    const/16 v0, 0x17

    .line 1498
    .line 1499
    aput-object v27, v2, v0

    .line 1500
    .line 1501
    const/16 v0, 0x18

    .line 1502
    .line 1503
    aput-object v28, v2, v0

    .line 1504
    .line 1505
    const/16 v0, 0x19

    .line 1506
    .line 1507
    aput-object v29, v2, v0

    .line 1508
    .line 1509
    const/16 v0, 0x1a

    .line 1510
    .line 1511
    aput-object v30, v2, v0

    .line 1512
    .line 1513
    const/16 v0, 0x1b

    .line 1514
    .line 1515
    aput-object v31, v2, v0

    .line 1516
    .line 1517
    const/16 v0, 0x1c

    .line 1518
    .line 1519
    aput-object v32, v2, v0

    .line 1520
    .line 1521
    const/16 v0, 0x1d

    .line 1522
    .line 1523
    aput-object v33, v2, v0

    .line 1524
    .line 1525
    const/16 v0, 0x1e

    .line 1526
    .line 1527
    aput-object v34, v2, v0

    .line 1528
    .line 1529
    const/16 v0, 0x1f

    .line 1530
    .line 1531
    aput-object v35, v2, v0

    .line 1532
    .line 1533
    const/16 v0, 0x20

    .line 1534
    .line 1535
    aput-object v36, v2, v0

    .line 1536
    .line 1537
    const/16 v0, 0x21

    .line 1538
    .line 1539
    aput-object v37, v2, v0

    .line 1540
    .line 1541
    const/16 v0, 0x22

    .line 1542
    .line 1543
    aput-object v38, v2, v0

    .line 1544
    .line 1545
    const/16 v0, 0x23

    .line 1546
    .line 1547
    aput-object v39, v2, v0

    .line 1548
    .line 1549
    const/16 v0, 0x24

    .line 1550
    .line 1551
    aput-object v40, v2, v0

    .line 1552
    .line 1553
    const/16 v0, 0x25

    .line 1554
    .line 1555
    aput-object v41, v2, v0

    .line 1556
    .line 1557
    const/16 v0, 0x26

    .line 1558
    .line 1559
    aput-object v42, v2, v0

    .line 1560
    .line 1561
    const/16 v0, 0x27

    .line 1562
    .line 1563
    aput-object v43, v2, v0

    .line 1564
    .line 1565
    const/16 v0, 0x28

    .line 1566
    .line 1567
    aput-object v44, v2, v0

    .line 1568
    .line 1569
    const/16 v0, 0x29

    .line 1570
    .line 1571
    aput-object v45, v2, v0

    .line 1572
    .line 1573
    const/16 v0, 0x2a

    .line 1574
    .line 1575
    aput-object v46, v2, v0

    .line 1576
    .line 1577
    const/16 v0, 0x2b

    .line 1578
    .line 1579
    aput-object v47, v2, v0

    .line 1580
    .line 1581
    const/16 v0, 0x2c

    .line 1582
    .line 1583
    aput-object v48, v2, v0

    .line 1584
    .line 1585
    const/16 v0, 0x2d

    .line 1586
    .line 1587
    aput-object v49, v2, v0

    .line 1588
    .line 1589
    const/16 v0, 0x2e

    .line 1590
    .line 1591
    aput-object v50, v2, v0

    .line 1592
    .line 1593
    const/16 v0, 0x2f

    .line 1594
    .line 1595
    aput-object v51, v2, v0

    .line 1596
    .line 1597
    const/16 v0, 0x30

    .line 1598
    .line 1599
    aput-object v52, v2, v0

    .line 1600
    .line 1601
    const/16 v0, 0x31

    .line 1602
    .line 1603
    aput-object v53, v2, v0

    .line 1604
    .line 1605
    const/16 v0, 0x32

    .line 1606
    .line 1607
    aput-object v54, v2, v0

    .line 1608
    .line 1609
    const/16 v0, 0x33

    .line 1610
    .line 1611
    aput-object v55, v2, v0

    .line 1612
    .line 1613
    const/16 v0, 0x34

    .line 1614
    .line 1615
    aput-object v56, v2, v0

    .line 1616
    .line 1617
    const/16 v0, 0x35

    .line 1618
    .line 1619
    aput-object v57, v2, v0

    .line 1620
    .line 1621
    const/16 v0, 0x36

    .line 1622
    .line 1623
    aput-object v58, v2, v0

    .line 1624
    .line 1625
    const/16 v0, 0x37

    .line 1626
    .line 1627
    aput-object v59, v2, v0

    .line 1628
    .line 1629
    const/16 v0, 0x38

    .line 1630
    .line 1631
    aput-object v60, v2, v0

    .line 1632
    .line 1633
    const/16 v0, 0x39

    .line 1634
    .line 1635
    aput-object v61, v2, v0

    .line 1636
    .line 1637
    const/16 v0, 0x3a

    .line 1638
    .line 1639
    aput-object v62, v2, v0

    .line 1640
    .line 1641
    const/16 v0, 0x3b

    .line 1642
    .line 1643
    aput-object v63, v2, v0

    .line 1644
    .line 1645
    const/16 v0, 0x3c

    .line 1646
    .line 1647
    aput-object v64, v2, v0

    .line 1648
    .line 1649
    const/16 v0, 0x3d

    .line 1650
    .line 1651
    aput-object v65, v2, v0

    .line 1652
    .line 1653
    const/16 v0, 0x3e

    .line 1654
    .line 1655
    aput-object v66, v2, v0

    .line 1656
    .line 1657
    const/16 v0, 0x3f

    .line 1658
    .line 1659
    aput-object v67, v2, v0

    .line 1660
    .line 1661
    const/16 v0, 0x40

    .line 1662
    .line 1663
    aput-object v68, v2, v0

    .line 1664
    .line 1665
    const/16 v0, 0x41

    .line 1666
    .line 1667
    aput-object v69, v2, v0

    .line 1668
    .line 1669
    const/16 v0, 0x42

    .line 1670
    .line 1671
    aput-object v70, v2, v0

    .line 1672
    .line 1673
    const/16 v0, 0x43

    .line 1674
    .line 1675
    aput-object v71, v2, v0

    .line 1676
    .line 1677
    const/16 v0, 0x44

    .line 1678
    .line 1679
    aput-object v72, v2, v0

    .line 1680
    .line 1681
    const/16 v0, 0x45

    .line 1682
    .line 1683
    aput-object v73, v2, v0

    .line 1684
    .line 1685
    const/16 v0, 0x46

    .line 1686
    .line 1687
    aput-object v74, v2, v0

    .line 1688
    .line 1689
    const/16 v0, 0x47

    .line 1690
    .line 1691
    aput-object v75, v2, v0

    .line 1692
    .line 1693
    const/16 v0, 0x48

    .line 1694
    .line 1695
    aput-object v76, v2, v0

    .line 1696
    .line 1697
    const/16 v0, 0x49

    .line 1698
    .line 1699
    aput-object v77, v2, v0

    .line 1700
    .line 1701
    const/16 v0, 0x4a

    .line 1702
    .line 1703
    aput-object v78, v2, v0

    .line 1704
    .line 1705
    const/16 v0, 0x4b

    .line 1706
    .line 1707
    aput-object v79, v2, v0

    .line 1708
    .line 1709
    const/16 v0, 0x4c

    .line 1710
    .line 1711
    aput-object v80, v2, v0

    .line 1712
    .line 1713
    const/16 v0, 0x4d

    .line 1714
    .line 1715
    aput-object v81, v2, v0

    .line 1716
    .line 1717
    const/16 v0, 0x4e

    .line 1718
    .line 1719
    aput-object v82, v2, v0

    .line 1720
    .line 1721
    const/16 v0, 0x4f

    .line 1722
    .line 1723
    aput-object v83, v2, v0

    .line 1724
    .line 1725
    const/16 v0, 0x50

    .line 1726
    .line 1727
    aput-object v84, v2, v0

    .line 1728
    .line 1729
    const/16 v0, 0x51

    .line 1730
    .line 1731
    aput-object v85, v2, v0

    .line 1732
    .line 1733
    const/16 v0, 0x52

    .line 1734
    .line 1735
    aput-object v86, v2, v0

    .line 1736
    .line 1737
    const/16 v0, 0x53

    .line 1738
    .line 1739
    aput-object v87, v2, v0

    .line 1740
    .line 1741
    const/16 v0, 0x54

    .line 1742
    .line 1743
    aput-object v88, v2, v0

    .line 1744
    .line 1745
    const/16 v0, 0x55

    .line 1746
    .line 1747
    aput-object v89, v2, v0

    .line 1748
    .line 1749
    const/16 v0, 0x56

    .line 1750
    .line 1751
    aput-object v90, v2, v0

    .line 1752
    .line 1753
    const/16 v0, 0x57

    .line 1754
    .line 1755
    aput-object v91, v2, v0

    .line 1756
    .line 1757
    const/16 v0, 0x58

    .line 1758
    .line 1759
    aput-object v92, v2, v0

    .line 1760
    .line 1761
    const/16 v0, 0x59

    .line 1762
    .line 1763
    aput-object v93, v2, v0

    .line 1764
    .line 1765
    const/16 v0, 0x5a

    .line 1766
    .line 1767
    aput-object v94, v2, v0

    .line 1768
    .line 1769
    const/16 v0, 0x5b

    .line 1770
    .line 1771
    aput-object v95, v2, v0

    .line 1772
    .line 1773
    const/16 v0, 0x5c

    .line 1774
    .line 1775
    aput-object v96, v2, v0

    .line 1776
    .line 1777
    const/16 v0, 0x5d

    .line 1778
    .line 1779
    aput-object v97, v2, v0

    .line 1780
    .line 1781
    const/16 v0, 0x5e

    .line 1782
    .line 1783
    aput-object v98, v2, v0

    .line 1784
    .line 1785
    const/16 v0, 0x5f

    .line 1786
    .line 1787
    aput-object v99, v2, v0

    .line 1788
    .line 1789
    const/16 v0, 0x60

    .line 1790
    .line 1791
    aput-object v4, v2, v0

    .line 1792
    .line 1793
    sput-object v2, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->$VALUES:[Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 1794
    .line 1795
    new-instance v0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason$1;

    .line 1796
    .line 1797
    invoke-direct {v0}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason$1;-><init>()V

    .line 1798
    .line 1799
    .line 1800
    sput-object v0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    .line 1801
    .line 1802
    return-void
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
    .line 2087
    .line 2088
    .line 2089
    .line 2090
    .line 2091
    .line 2092
    .line 2093
    .line 2094
    .line 2095
    .line 2096
    .line 2097
    .line 2098
    .line 2099
    .line 2100
    .line 2101
    .line 2102
    .line 2103
    .line 2104
    .line 2105
    .line 2106
    .line 2107
    .line 2108
    .line 2109
    .line 2110
    .line 2111
    .line 2112
    .line 2113
    .line 2114
    .line 2115
    .line 2116
    .line 2117
    .line 2118
    .line 2119
    .line 2120
    .line 2121
    .line 2122
    .line 2123
    .line 2124
    .line 2125
    .line 2126
    .line 2127
    .line 2128
    .line 2129
    .line 2130
    .line 2131
    .line 2132
    .line 2133
    .line 2134
    .line 2135
    .line 2136
    .line 2137
    .line 2138
    .line 2139
    .line 2140
    .line 2141
    .line 2142
    .line 2143
    .line 2144
    .line 2145
    .line 2146
    .line 2147
    .line 2148
    .line 2149
    .line 2150
    .line 2151
    .line 2152
    .line 2153
    .line 2154
    .line 2155
    .line 2156
    .line 2157
    .line 2158
    .line 2159
    .line 2160
    .line 2161
    .line 2162
    .line 2163
    .line 2164
    .line 2165
    .line 2166
    .line 2167
    .line 2168
    .line 2169
    .line 2170
    .line 2171
    .line 2172
    .line 2173
    .line 2174
    .line 2175
    .line 2176
    .line 2177
    .line 2178
    .line 2179
    .line 2180
    .line 2181
    .line 2182
    .line 2183
    .line 2184
    .line 2185
    .line 2186
    .line 2187
    .line 2188
    .line 2189
    .line 2190
    .line 2191
    .line 2192
    .line 2193
    .line 2194
    .line 2195
    .line 2196
    .line 2197
    .line 2198
    .line 2199
    .line 2200
    .line 2201
    .line 2202
    .line 2203
    .line 2204
    .line 2205
    .line 2206
    .line 2207
    .line 2208
    .line 2209
    .line 2210
    .line 2211
    .line 2212
    .line 2213
    .line 2214
    .line 2215
    .line 2216
    .line 2217
    .line 2218
    .line 2219
    .line 2220
    .line 2221
    .line 2222
    .line 2223
    .line 2224
    .line 2225
    .line 2226
    .line 2227
    .line 2228
    .line 2229
    .line 2230
    .line 2231
    .line 2232
    .line 2233
    .line 2234
    .line 2235
    .line 2236
    .line 2237
    .line 2238
    .line 2239
    .line 2240
    .line 2241
    .line 2242
    .line 2243
    .line 2244
    .line 2245
    .line 2246
    .line 2247
    .line 2248
    .line 2249
    .line 2250
    .line 2251
    .line 2252
    .line 2253
    .line 2254
    .line 2255
    .line 2256
    .line 2257
    .line 2258
    .line 2259
    .line 2260
    .line 2261
    .line 2262
    .line 2263
    .line 2264
    .line 2265
    .line 2266
    .line 2267
    .line 2268
    .line 2269
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->value:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public static forNumber(I)Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;
    .locals 1

    .line 1
    if-eqz p0, :cond_9

    .line 2
    .line 3
    const/4 v0, 0x2

    .line 4
    if-eq p0, v0, :cond_8

    .line 5
    .line 6
    const/4 v0, 0x3

    .line 7
    if-eq p0, v0, :cond_7

    .line 8
    .line 9
    const/4 v0, 0x4

    .line 10
    if-eq p0, v0, :cond_6

    .line 11
    .line 12
    const/4 v0, 0x6

    .line 13
    if-eq p0, v0, :cond_5

    .line 14
    .line 15
    const/4 v0, 0x7

    .line 16
    if-eq p0, v0, :cond_4

    .line 17
    .line 18
    const/16 v0, 0x12d

    .line 19
    .line 20
    if-eq p0, v0, :cond_3

    .line 21
    .line 22
    const/16 v0, 0x12e

    .line 23
    .line 24
    if-eq p0, v0, :cond_2

    .line 25
    .line 26
    const/16 v0, 0x130

    .line 27
    .line 28
    if-eq p0, v0, :cond_1

    .line 29
    .line 30
    const/16 v0, 0x131

    .line 31
    .line 32
    if-eq p0, v0, :cond_0

    .line 33
    .line 34
    sparse-switch p0, :sswitch_data_0

    .line 35
    .line 36
    .line 37
    packed-switch p0, :pswitch_data_0

    .line 38
    .line 39
    .line 40
    packed-switch p0, :pswitch_data_1

    .line 41
    .line 42
    .line 43
    packed-switch p0, :pswitch_data_2

    .line 44
    .line 45
    .line 46
    packed-switch p0, :pswitch_data_3

    .line 47
    .line 48
    .line 49
    packed-switch p0, :pswitch_data_4

    .line 50
    .line 51
    .line 52
    const/4 p0, 0x0

    .line 53
    return-object p0

    .line 54
    :pswitch_0
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_CLOSED_MISSING_HEARTBEAT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 55
    .line 56
    return-object p0

    .line 57
    :pswitch_1
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_CLOSED_TEMPLATE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 58
    .line 59
    return-object p0

    .line 60
    :pswitch_2
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->GENERATE_JSON_DATA_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 61
    .line 62
    return-object p0

    .line 63
    :pswitch_3
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->JSON_PARAMS_ENCODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 64
    .line 65
    return-object p0

    .line 66
    :pswitch_4
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->LINK_COMMAND_OPEN_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 67
    .line 68
    return-object p0

    .line 69
    :pswitch_5
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->EVALUATE_JAVASCRIPT_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 70
    .line 71
    return-object p0

    .line 72
    :pswitch_6
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->DEEPLINK_OPEN_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 73
    .line 74
    return-object p0

    .line 75
    :pswitch_7
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_JS_CALL_EMPTY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 76
    .line 77
    return-object p0

    .line 78
    :pswitch_8
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_HTML_FAILED_TO_LOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 79
    .line 80
    return-object p0

    .line 81
    :pswitch_9
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_FAILED_TO_DELETE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 82
    .line 83
    return-object p0

    .line 84
    :pswitch_a
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_WIN_NOTIFICATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 85
    .line 86
    return-object p0

    .line 87
    :pswitch_b
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_EXPIRED_ON_PLAY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 88
    .line 89
    return-object p0

    .line 90
    :pswitch_c
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_WATERFALL_PLACEMENT_ID:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 91
    .line 92
    return-object p0

    .line 93
    :pswitch_d
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_LOAD_FAIL_RETRY_AFTER:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 94
    .line 95
    return-object p0

    .line 96
    :pswitch_e
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_RESPONSE_RETRY_AFTER:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 97
    .line 98
    return-object p0

    .line 99
    :pswitch_f
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_JS_COPY_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 100
    .line 101
    return-object p0

    .line 102
    :pswitch_10
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_JS_DOES_NOT_EXIST:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 103
    .line 104
    return-object p0

    .line 105
    :pswitch_11
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_RESPONSE_TIMED_OUT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 106
    .line 107
    return-object p0

    .line 108
    :pswitch_12
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_RESPONSE_INVALID_TEMPLATE_TYPE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 109
    .line 110
    return-object p0

    .line 111
    :pswitch_13
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_RESPONSE_EMPTY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 112
    .line 113
    return-object p0

    .line 114
    :pswitch_14
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_GZIP_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 115
    .line 116
    return-object p0

    .line 117
    :pswitch_15
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_ADUNIT_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 118
    .line 119
    return-object p0

    .line 120
    :pswitch_16
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->PLACEMENT_SLEEP:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 121
    .line 122
    return-object p0

    .line 123
    :pswitch_17
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_NOT_LOADED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 124
    .line 125
    return-object p0

    .line 126
    :pswitch_18
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_JSON_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 127
    .line 128
    return-object p0

    .line 129
    :pswitch_19
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_BID_PAYLOAD:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 130
    .line 131
    return-object p0

    .line 132
    :pswitch_1a
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->PLACEMENT_AD_TYPE_MISMATCH:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 133
    .line 134
    return-object p0

    .line 135
    :pswitch_1b
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_ALREADY_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 136
    .line 137
    return-object p0

    .line 138
    :pswitch_1c
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_IS_PLAYING:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 139
    .line 140
    return-object p0

    .line 141
    :pswitch_1d
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_ALREADY_LOADED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 142
    .line 143
    return-object p0

    .line 144
    :pswitch_1e
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_IS_LOADING:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 145
    .line 146
    return-object p0

    .line 147
    :pswitch_1f
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_CONSUMED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 148
    .line 149
    return-object p0

    .line 150
    :pswitch_20
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_PLACEMENT_ID:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 151
    .line 152
    return-object p0

    .line 153
    :pswitch_21
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_EVENT_ID_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 154
    .line 155
    return-object p0

    .line 156
    :pswitch_22
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->JSON_ENCODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 157
    .line 158
    return-object p0

    .line 159
    :pswitch_23
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->PROTOBUF_SERIALIZATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 160
    .line 161
    return-object p0

    .line 162
    :pswitch_24
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_FAILED_STATUS_CODE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 163
    .line 164
    return-object p0

    .line 165
    :pswitch_25
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->GZIP_ENCODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 166
    .line 167
    return-object p0

    .line 168
    :pswitch_26
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_INDEX_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 169
    .line 170
    return-object p0

    .line 171
    :pswitch_27
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_WRITE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 172
    .line 173
    return-object p0

    .line 174
    :pswitch_28
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_RESPONSE_DATA_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 175
    .line 176
    return-object p0

    .line 177
    :pswitch_29
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_REQUEST_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 178
    .line 179
    return-object p0

    .line 180
    :pswitch_2a
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_ASSET_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 181
    .line 182
    return-object p0

    .line 183
    :pswitch_2b
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_CTA_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 184
    .line 185
    return-object p0

    .line 186
    :pswitch_2c
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->TEMPLATE_UNZIP_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 187
    .line 188
    return-object p0

    .line 189
    :pswitch_2d
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_REQUEST_BUILDER_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 190
    .line 191
    return-object p0

    .line 192
    :pswitch_2e
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_TEMPLATE_URL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 193
    .line 194
    return-object p0

    .line 195
    :pswitch_2f
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->API_FAILED_STATUS_CODE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 196
    .line 197
    return-object p0

    .line 198
    :pswitch_30
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->API_RESPONSE_DECODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 199
    .line 200
    return-object p0

    .line 201
    :pswitch_31
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->API_RESPONSE_DATA_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 202
    .line 203
    return-object p0

    .line 204
    :pswitch_32
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->API_REQUEST_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 205
    .line 206
    return-object p0

    .line 207
    :sswitch_0
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_INTERNAL_INTEGRATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 208
    .line 209
    return-object p0

    .line 210
    :sswitch_1
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_PUBLISHER_MISMATCH:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 211
    .line 212
    return-object p0

    .line 213
    :sswitch_2
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_SERVER_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 214
    .line 215
    return-object p0

    .line 216
    :sswitch_3
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_LOAD_TOO_FREQUENTLY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 217
    .line 218
    return-object p0

    .line 219
    :sswitch_4
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_NO_FILL:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 220
    .line 221
    return-object p0

    .line 222
    :sswitch_5
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->OUT_OF_MEMORY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 223
    .line 224
    return-object p0

    .line 225
    :sswitch_6
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->STORE_KIT_PRESENTATION_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 226
    .line 227
    return-object p0

    .line 228
    :sswitch_7
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->UNKNOWN_RADIO_ACCESS_TECHNOLOGY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 229
    .line 230
    return-object p0

    .line 231
    :sswitch_8
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->REACHABILITY_INITIALIZATION_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 232
    .line 233
    return-object p0

    .line 234
    :sswitch_9
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->STORE_OVERLAY_LOAD_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 235
    .line 236
    return-object p0

    .line 237
    :sswitch_a
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->OMSDK_COPY_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 238
    .line 239
    return-object p0

    .line 240
    :sswitch_b
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->STORE_KIT_LOAD_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 241
    .line 242
    return-object p0

    .line 243
    :sswitch_c
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->WEB_VIEW_FAILED_NAVIGATION:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 244
    .line 245
    return-object p0

    .line 246
    :sswitch_d
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->WEB_VIEW_WEB_CONTENT_PROCESS_DID_TERMINATE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 247
    .line 248
    return-object p0

    .line 249
    :sswitch_e
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->NATIVE_ASSET_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 250
    .line 251
    return-object p0

    .line 252
    :sswitch_f
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->BANNER_VIEW_INVALID_SIZE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 253
    .line 254
    return-object p0

    .line 255
    :sswitch_10
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->CONCURRENT_PLAYBACK_UNSUPPORTED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 256
    .line 257
    return-object p0

    .line 258
    :sswitch_11
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->CONFIG_REFRESH_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 259
    .line 260
    return-object p0

    .line 261
    :sswitch_12
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->TPAT_RETRY_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 262
    .line 263
    return-object p0

    .line 264
    :sswitch_13
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->PRIVACY_URL_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 265
    .line 266
    return-object p0

    .line 267
    :sswitch_14
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_CONFIG_RESPONSE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 268
    .line 269
    return-object p0

    .line 270
    :sswitch_15
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->STORE_REGION_CODE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 271
    .line 272
    return-object p0

    .line 273
    :sswitch_16
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->OMSDK_JS_WRITE_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 274
    .line 275
    return-object p0

    .line 276
    :sswitch_17
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->OMSDK_DOWNLOAD_JS_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 277
    .line 278
    return-object p0

    .line 279
    :sswitch_18
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_JS_WRITE_FAILED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 280
    .line 281
    return-object p0

    .line 282
    :sswitch_19
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_DOWNLOAD_JS_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 283
    .line 284
    return-object p0

    .line 285
    :sswitch_1a
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->EMPTY_TPAT_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 286
    .line 287
    return-object p0

    .line 288
    :sswitch_1b
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_TPAT_KEY:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 289
    .line 290
    return-object p0

    .line 291
    :sswitch_1c
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_FAILED_MAX_SPACE_EXCEEDED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 292
    .line 293
    return-object p0

    .line 294
    :sswitch_1d
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ASSET_FAILED_INSUFFICIENT_SPACE:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 295
    .line 296
    return-object p0

    .line 297
    :sswitch_1e
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_METRICS_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 298
    .line 299
    return-object p0

    .line 300
    :sswitch_1f
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_LOG_ERROR_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 301
    .line 302
    return-object p0

    .line 303
    :sswitch_20
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_RI_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 304
    .line 305
    return-object p0

    .line 306
    :sswitch_21
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_ADS_ENDPOINT:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 307
    .line 308
    return-object p0

    .line 309
    :sswitch_22
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->TPAT_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 310
    .line 311
    return-object p0

    .line 312
    :cond_0
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_BRIDGE_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 313
    .line 314
    return-object p0

    .line 315
    :cond_1
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->AD_EXPIRED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 316
    .line 317
    return-object p0

    .line 318
    :cond_2
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_IFA_STATUS:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 319
    .line 320
    return-object p0

    .line 321
    :cond_3
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->MRAID_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 322
    .line 323
    return-object p0

    .line 324
    :cond_4
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->USER_AGENT_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 325
    .line 326
    return-object p0

    .line 327
    :cond_5
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->SDK_NOT_INITIALIZED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 328
    .line 329
    return-object p0

    .line 330
    :cond_6
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->ALREADY_INITIALIZED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 331
    .line 332
    return-object p0

    .line 333
    :cond_7
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->CURRENTLY_INITIALIZING:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 334
    .line 335
    return-object p0

    .line 336
    :cond_8
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->INVALID_APP_ID:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 337
    .line 338
    return-object p0

    .line 339
    :cond_9
    :sswitch_23
    sget-object p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->UNKNOWN_ERROR:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 340
    .line 341
    return-object p0

    .line 342
    nop

    .line 343
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_23
        0x79 -> :sswitch_22
        0x7a -> :sswitch_21
        0x7b -> :sswitch_20
        0x7c -> :sswitch_1f
        0x7d -> :sswitch_1e
        0x7e -> :sswitch_1d
        0x7f -> :sswitch_1c
        0x80 -> :sswitch_1b
        0x81 -> :sswitch_1a
        0x82 -> :sswitch_19
        0x83 -> :sswitch_18
        0x84 -> :sswitch_17
        0x85 -> :sswitch_16
        0x86 -> :sswitch_15
        0x87 -> :sswitch_14
        0x88 -> :sswitch_13
        0x89 -> :sswitch_12
        0x8a -> :sswitch_11
        0x190 -> :sswitch_10
        0x1f4 -> :sswitch_f
        0x258 -> :sswitch_e
        0x7d0 -> :sswitch_d
        0x7d1 -> :sswitch_c
        0x7d2 -> :sswitch_b
        0x7d3 -> :sswitch_a
        0x7d4 -> :sswitch_9
        0x7d5 -> :sswitch_8
        0x7d6 -> :sswitch_7
        0x7d7 -> :sswitch_6
        0xbb9 -> :sswitch_5
        0x2711 -> :sswitch_4
        0x2712 -> :sswitch_3
        0x4e21 -> :sswitch_2
        0x7531 -> :sswitch_1
        0x7532 -> :sswitch_0
    .end sparse-switch

    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x6d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0xc8
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0xd4
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x133
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap<",
            "Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static internalGetVerifier()Lcom/google/protobuf/Internal$EnumVerifier;
    .locals 1

    .line 1
    sget-object v0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason$ReasonVerifier;->INSTANCE:Lcom/google/protobuf/Internal$EnumVerifier;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static valueOf(I)Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    invoke-static {p0}, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->forNumber(I)Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;
    .locals 1

    .line 1
    const-class v0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    return-object p0
.end method

.method public static values()[Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;
    .locals 1

    .line 1
    sget-object v0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->$VALUES:[Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public final getNumber()I
    .locals 2

    .line 1
    sget-object v0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->UNRECOGNIZED:Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;

    .line 2
    .line 3
    if-eq p0, v0, :cond_0

    .line 4
    .line 5
    iget v0, p0, Lcom/vungle/ads/internal/protos/Sdk$SDKError$Reason;->value:I

    .line 6
    .line 7
    return v0

    .line 8
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 9
    .line 10
    const-string v1, "Can\'t get the number of an unknown enum value."

    .line 11
    .line 12
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    throw v0
    .line 16
    .line 17
    .line 18
    .line 19
.end method

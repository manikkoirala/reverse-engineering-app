.class public final Lcom/vungle/ads/internal/downloader/AssetDownloader;
.super Ljava/lang/Object;
.source "AssetDownloader.kt"

# interfaces
.implements Lcom/vungle/ads/internal/downloader/Downloader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vungle/ads/internal/downloader/AssetDownloader$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final CONTENT_ENCODING:Ljava/lang/String; = "Content-Encoding"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final CONTENT_TYPE:Ljava/lang/String; = "Content-Type"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final Companion:Lcom/vungle/ads/internal/downloader/AssetDownloader$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final DOWNLOAD_CHUNK_SIZE:I = 0x800

.field private static final GZIP:Ljava/lang/String; = "gzip"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final IDENTITY:Ljava/lang/String; = "identity"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final MAX_PERCENT:J = 0x64L

.field private static final MINIMUM_SPACE_REQUIRED_MB:I = 0x1400000

.field private static final PROGRESS_STEP:I = 0x5

.field private static final TAG:Ljava/lang/String; = "AssetDownloader"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final TIMEOUT:I = 0x1e


# instance fields
.field private final downloadExecutor:Lcom/vungle/ads/internal/executor/VungleThreadPoolExecutor;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private okHttpClient:Lokhttp3/OkHttpClient;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final pathProvider:Lcom/vungle/ads/internal/util/PathProvider;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final progressStep:I

.field private final transitioning:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/vungle/ads/internal/downloader/DownloadRequest;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/vungle/ads/internal/downloader/AssetDownloader$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/vungle/ads/internal/downloader/AssetDownloader$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/vungle/ads/internal/downloader/AssetDownloader;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloader$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public constructor <init>(Lcom/vungle/ads/internal/executor/VungleThreadPoolExecutor;Lcom/vungle/ads/internal/util/PathProvider;)V
    .locals 7
    .param p1    # Lcom/vungle/ads/internal/executor/VungleThreadPoolExecutor;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/vungle/ads/internal/util/PathProvider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "downloadExecutor"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "pathProvider"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/vungle/ads/internal/downloader/AssetDownloader;->downloadExecutor:Lcom/vungle/ads/internal/executor/VungleThreadPoolExecutor;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/vungle/ads/internal/downloader/AssetDownloader;->pathProvider:Lcom/vungle/ads/internal/util/PathProvider;

    .line 17
    .line 18
    const/4 p1, 0x5

    .line 19
    iput p1, p0, Lcom/vungle/ads/internal/downloader/AssetDownloader;->progressStep:I

    .line 20
    .line 21
    new-instance p1, Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 24
    .line 25
    .line 26
    iput-object p1, p0, Lcom/vungle/ads/internal/downloader/AssetDownloader;->transitioning:Ljava/util/List;

    .line 27
    .line 28
    new-instance p1, Lokhttp3/OkHttpClient$Builder;

    .line 29
    .line 30
    invoke-direct {p1}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 31
    .line 32
    .line 33
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 34
    .line 35
    const-wide/16 v1, 0x1e

    .line 36
    .line 37
    invoke-virtual {p1, v1, v2, v0}, Lokhttp3/OkHttpClient$Builder;->〇〇〇0〇〇0(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-virtual {p1, v1, v2, v0}, Lokhttp3/OkHttpClient$Builder;->Oo08(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    const/4 v0, 0x0

    .line 46
    invoke-virtual {p1, v0}, Lokhttp3/OkHttpClient$Builder;->O8(Lokhttp3/Cache;)Lokhttp3/OkHttpClient$Builder;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    const/4 v0, 0x1

    .line 51
    invoke-virtual {p1, v0}, Lokhttp3/OkHttpClient$Builder;->〇〇888(Z)Lokhttp3/OkHttpClient$Builder;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    invoke-virtual {p1, v0}, Lokhttp3/OkHttpClient$Builder;->oO80(Z)Lokhttp3/OkHttpClient$Builder;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    sget-object v0, Lcom/vungle/ads/internal/ConfigManager;->INSTANCE:Lcom/vungle/ads/internal/ConfigManager;

    .line 60
    .line 61
    invoke-virtual {v0}, Lcom/vungle/ads/internal/ConfigManager;->isCleverCacheEnabled()Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    if-eqz v1, :cond_0

    .line 66
    .line 67
    invoke-virtual {v0}, Lcom/vungle/ads/internal/ConfigManager;->getCleverCacheDiskSize()J

    .line 68
    .line 69
    .line 70
    move-result-wide v1

    .line 71
    invoke-virtual {v0}, Lcom/vungle/ads/internal/ConfigManager;->getCleverCacheDiskPercentage()I

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    invoke-virtual {p2}, Lcom/vungle/ads/internal/util/PathProvider;->getCleverCacheDir()Ljava/io/File;

    .line 76
    .line 77
    .line 78
    move-result-object v3

    .line 79
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v3

    .line 83
    const-string v4, "pathProvider.getCleverCacheDir().absolutePath"

    .line 84
    .line 85
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {p2, v3}, Lcom/vungle/ads/internal/util/PathProvider;->getAvailableBytes(Ljava/lang/String;)J

    .line 89
    .line 90
    .line 91
    move-result-wide v3

    .line 92
    int-to-long v5, v0

    .line 93
    mul-long v3, v3, v5

    .line 94
    .line 95
    const/16 v0, 0x64

    .line 96
    .line 97
    int-to-long v5, v0

    .line 98
    div-long/2addr v3, v5

    .line 99
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    .line 100
    .line 101
    .line 102
    move-result-wide v0

    .line 103
    const-wide/16 v2, 0x0

    .line 104
    .line 105
    cmp-long v4, v0, v2

    .line 106
    .line 107
    if-lez v4, :cond_0

    .line 108
    .line 109
    new-instance v2, Lokhttp3/Cache;

    .line 110
    .line 111
    invoke-virtual {p2}, Lcom/vungle/ads/internal/util/PathProvider;->getCleverCacheDir()Ljava/io/File;

    .line 112
    .line 113
    .line 114
    move-result-object p2

    .line 115
    invoke-direct {v2, p2, v0, v1}, Lokhttp3/Cache;-><init>(Ljava/io/File;J)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {p1, v2}, Lokhttp3/OkHttpClient$Builder;->O8(Lokhttp3/Cache;)Lokhttp3/OkHttpClient$Builder;

    .line 119
    .line 120
    .line 121
    :cond_0
    invoke-virtual {p1}, Lokhttp3/OkHttpClient$Builder;->〇o〇()Lokhttp3/OkHttpClient;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    iput-object p1, p0, Lcom/vungle/ads/internal/downloader/AssetDownloader;->okHttpClient:Lokhttp3/OkHttpClient;

    .line 126
    .line 127
    return-void
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public static final synthetic access$launchRequest(Lcom/vungle/ads/internal/downloader/AssetDownloader;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->launchRequest(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private final checkSpaceAvailable()Z
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/vungle/ads/internal/downloader/AssetDownloader;->pathProvider:Lcom/vungle/ads/internal/util/PathProvider;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/vungle/ads/internal/util/PathProvider;->getVungleDir()Ljava/io/File;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const-string v2, "pathProvider.getVungleDir().absolutePath"

    .line 12
    .line 13
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/vungle/ads/internal/util/PathProvider;->getAvailableBytes(Ljava/lang/String;)J

    .line 17
    .line 18
    .line 19
    move-result-wide v0

    .line 20
    const-wide/32 v2, 0x1400000

    .line 21
    .line 22
    .line 23
    cmp-long v4, v0, v2

    .line 24
    .line 25
    if-gez v4, :cond_0

    .line 26
    .line 27
    sget-object v5, Lcom/vungle/ads/AnalyticsClient;->INSTANCE:Lcom/vungle/ads/AnalyticsClient;

    .line 28
    .line 29
    const/16 v6, 0x7e

    .line 30
    .line 31
    new-instance v2, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v3, "Insufficient space "

    .line 37
    .line 38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v7

    .line 48
    const/4 v8, 0x0

    .line 49
    const/4 v9, 0x0

    .line 50
    const/4 v10, 0x0

    .line 51
    const/16 v11, 0x1c

    .line 52
    .line 53
    const/4 v12, 0x0

    .line 54
    invoke-static/range {v5 .. v12}, Lcom/vungle/ads/AnalyticsClient;->logError$vungle_ads_release$default(Lcom/vungle/ads/AnalyticsClient;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 55
    .line 56
    .line 57
    const/4 v0, 0x0

    .line 58
    return v0

    .line 59
    :cond_0
    const/4 v0, 0x1

    .line 60
    return v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method private final decodeGzipIfNeeded(Lokhttp3/Response;)Lokhttp3/ResponseBody;
    .locals 6

    .line 1
    invoke-virtual {p1}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "Content-Encoding"

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    const/4 v3, 0x2

    .line 9
    invoke-static {p1, v1, v2, v3, v2}, Lokhttp3/Response;->〇〇8O0〇8(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v4, 0x1

    .line 14
    const-string v5, "gzip"

    .line 15
    .line 16
    invoke-static {v5, v1, v4}, Lkotlin/text/StringsKt;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    new-instance v1, Lokio/GzipSource;

    .line 25
    .line 26
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->source()Lokio/BufferedSource;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-direct {v1, v0}, Lokio/GzipSource;-><init>(Lokio/Source;)V

    .line 31
    .line 32
    .line 33
    const-string v0, "Content-Type"

    .line 34
    .line 35
    invoke-static {p1, v0, v2, v3, v2}, Lokhttp3/Response;->〇〇8O0〇8(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    new-instance v0, Lokhttp3/internal/http/RealResponseBody;

    .line 40
    .line 41
    const-wide/16 v2, -0x1

    .line 42
    .line 43
    invoke-static {v1}, Lokio/Okio;->buffer(Lokio/Source;)Lokio/BufferedSource;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-direct {v0, p1, v2, v3, v1}, Lokhttp3/internal/http/RealResponseBody;-><init>(Ljava/lang/String;JLokio/BufferedSource;)V

    .line 48
    .line 49
    .line 50
    :cond_0
    return-object v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private final deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V
    .locals 0

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    invoke-interface {p2, p3, p1}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener;->onError(Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;Lcom/vungle/ads/internal/downloader/DownloadRequest;)V

    .line 4
    .line 5
    .line 6
    :cond_0
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private final deliverProgress(Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "On progress "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    if-eqz p3, :cond_0

    .line 15
    .line 16
    invoke-interface {p3, p1, p2}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener;->onProgress(Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;Lcom/vungle/ads/internal/downloader/DownloadRequest;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private final deliverSuccess(Ljava/io/File;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "On success "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    if-eqz p3, :cond_0

    .line 15
    .line 16
    invoke-interface {p3, p1, p2}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener;->onSuccess(Ljava/io/File;Lcom/vungle/ads/internal/downloader/DownloadRequest;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private static final download$lambda-0(Lcom/vungle/ads/internal/downloader/AssetDownloader;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V
    .locals 5

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    new-instance v0, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;

    .line 8
    .line 9
    new-instance v1, Lcom/vungle/ads/InternalError;

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x2

    .line 13
    const/16 v4, 0xbb9

    .line 14
    .line 15
    invoke-direct {v1, v4, v2, v3, v2}, Lcom/vungle/ads/InternalError;-><init>(ILjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 16
    .line 17
    .line 18
    sget-object v2, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason$Companion;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason$Companion;->getINTERNAL_ERROR()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    const/4 v3, -0x1

    .line 25
    invoke-direct {v0, v3, v1, v2}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;-><init>(ILjava/lang/Throwable;I)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0, p1, p2, v0}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private final getContentLength(Lokhttp3/Response;)J
    .locals 5

    .line 1
    invoke-virtual {p1}, Lokhttp3/Response;->o800o8O()Lokhttp3/Headers;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "Content-Length"

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Lokhttp3/Headers;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x1

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 16
    .line 17
    .line 18
    move-result v4

    .line 19
    if-nez v4, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v4, 0x0

    .line 23
    goto :goto_1

    .line 24
    :cond_1
    :goto_0
    const/4 v4, 0x1

    .line 25
    :goto_1
    if-eqz v4, :cond_2

    .line 26
    .line 27
    invoke-virtual {p1}, Lokhttp3/Response;->o〇〇0〇()Lokhttp3/Response;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    const/4 v0, 0x0

    .line 32
    if-eqz p1, :cond_2

    .line 33
    .line 34
    const/4 v4, 0x2

    .line 35
    invoke-static {p1, v1, v0, v4, v0}, Lokhttp3/Response;->〇〇8O0〇8(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    move-object v0, p1

    .line 40
    :cond_2
    if-eqz v0, :cond_3

    .line 41
    .line 42
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    if-nez p1, :cond_4

    .line 47
    .line 48
    :cond_3
    const/4 v2, 0x1

    .line 49
    :cond_4
    const-wide/16 v3, -0x1

    .line 50
    .line 51
    if-eqz v2, :cond_5

    .line 52
    .line 53
    goto :goto_2

    .line 54
    :cond_5
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 55
    .line 56
    .line 57
    move-result-wide v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    move-wide v3, v0

    .line 59
    :catchall_0
    :goto_2
    return-wide v3
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private final isValidUrl(Ljava/lang/String;)Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    if-nez v2, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v2, 0x0

    .line 13
    goto :goto_1

    .line 14
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 15
    :goto_1
    if-nez v2, :cond_2

    .line 16
    .line 17
    sget-object v2, Lokhttp3/HttpUrl;->〇O8o08O:Lokhttp3/HttpUrl$Companion;

    .line 18
    .line 19
    invoke-virtual {v2, p1}, Lokhttp3/HttpUrl$Companion;->o〇0(Ljava/lang/String;)Lokhttp3/HttpUrl;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    if-eqz p1, :cond_2

    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    :cond_2
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private final launchRequest(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V
    .locals 38

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v2, p1

    .line 4
    .line 5
    move-object/from16 v3, p2

    .line 6
    .line 7
    const-string/jumbo v4, "status:"

    .line 8
    .line 9
    .line 10
    new-instance v0, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v5, "launch request in thread:"

    .line 16
    .line 17
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 21
    .line 22
    .line 23
    move-result-object v5

    .line 24
    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    .line 25
    .line 26
    .line 27
    move-result-wide v5

    .line 28
    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v5, " request: "

    .line 32
    .line 33
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->getUrl()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v5

    .line 40
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->isCancelled()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_0

    .line 48
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    .line 50
    .line 51
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .line 53
    .line 54
    const-string v4, "Request "

    .line 55
    .line 56
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->getUrl()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v4

    .line 63
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v4, " is cancelled before starting"

    .line 67
    .line 68
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    new-instance v0, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;

    .line 72
    .line 73
    invoke-direct {v0}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;-><init>()V

    .line 74
    .line 75
    .line 76
    sget-object v4, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;

    .line 77
    .line 78
    invoke-virtual {v4}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getCANCELLED()I

    .line 79
    .line 80
    .line 81
    move-result v4

    .line 82
    invoke-virtual {v0, v4}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->setStatus(I)V

    .line 83
    .line 84
    .line 85
    invoke-direct {v1, v0, v2, v3}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverProgress(Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 86
    .line 87
    .line 88
    return-void

    .line 89
    :cond_0
    new-instance v5, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;

    .line 90
    .line 91
    invoke-direct {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;-><init>()V

    .line 92
    .line 93
    .line 94
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 95
    .line 96
    .line 97
    move-result-wide v6

    .line 98
    invoke-virtual {v5, v6, v7}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->setTimestampDownloadStart(J)V

    .line 99
    .line 100
    .line 101
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->getUrl()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v6

    .line 105
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->getPath()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    const/4 v7, 0x0

    .line 110
    const/4 v14, 0x1

    .line 111
    if-eqz v6, :cond_2

    .line 112
    .line 113
    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    .line 114
    .line 115
    .line 116
    move-result v8

    .line 117
    if-nez v8, :cond_1

    .line 118
    .line 119
    goto :goto_0

    .line 120
    :cond_1
    const/4 v8, 0x0

    .line 121
    goto :goto_1

    .line 122
    :cond_2
    :goto_0
    const/4 v8, 0x1

    .line 123
    :goto_1
    const/4 v15, -0x1

    .line 124
    if-nez v8, :cond_31

    .line 125
    .line 126
    invoke-direct {v1, v6}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->isValidUrl(Ljava/lang/String;)Z

    .line 127
    .line 128
    .line 129
    move-result v8

    .line 130
    if-nez v8, :cond_3

    .line 131
    .line 132
    goto/16 :goto_34

    .line 133
    .line 134
    :cond_3
    if-eqz v0, :cond_5

    .line 135
    .line 136
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 137
    .line 138
    .line 139
    move-result v8

    .line 140
    if-nez v8, :cond_4

    .line 141
    .line 142
    goto :goto_2

    .line 143
    :cond_4
    const/4 v8, 0x0

    .line 144
    goto :goto_3

    .line 145
    :cond_5
    :goto_2
    const/4 v8, 0x1

    .line 146
    :goto_3
    if-eqz v8, :cond_6

    .line 147
    .line 148
    new-instance v0, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;

    .line 149
    .line 150
    new-instance v4, Lcom/vungle/ads/AssetDownloadError;

    .line 151
    .line 152
    invoke-direct {v4}, Lcom/vungle/ads/AssetDownloadError;-><init>()V

    .line 153
    .line 154
    .line 155
    sget-object v5, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason$Companion;

    .line 156
    .line 157
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason$Companion;->getFILE_NOT_FOUND_ERROR()I

    .line 158
    .line 159
    .line 160
    move-result v5

    .line 161
    invoke-direct {v0, v15, v4, v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;-><init>(ILjava/lang/Throwable;I)V

    .line 162
    .line 163
    .line 164
    invoke-direct {v1, v2, v3, v0}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 165
    .line 166
    .line 167
    return-void

    .line 168
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->checkSpaceAvailable()Z

    .line 169
    .line 170
    .line 171
    move-result v8

    .line 172
    const/4 v13, 0x0

    .line 173
    if-nez v8, :cond_7

    .line 174
    .line 175
    new-instance v0, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;

    .line 176
    .line 177
    new-instance v4, Lcom/vungle/ads/InternalError;

    .line 178
    .line 179
    const/16 v5, 0x2723

    .line 180
    .line 181
    const/4 v6, 0x2

    .line 182
    invoke-direct {v4, v5, v13, v6, v13}, Lcom/vungle/ads/InternalError;-><init>(ILjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 183
    .line 184
    .line 185
    sget-object v5, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason$Companion;

    .line 186
    .line 187
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason$Companion;->getDISK_ERROR()I

    .line 188
    .line 189
    .line 190
    move-result v5

    .line 191
    invoke-direct {v0, v15, v4, v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;-><init>(ILjava/lang/Throwable;I)V

    .line 192
    .line 193
    .line 194
    invoke-direct {v1, v2, v3, v0}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 195
    .line 196
    .line 197
    return-void

    .line 198
    :cond_7
    new-instance v12, Ljava/io/File;

    .line 199
    .line 200
    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 201
    .line 202
    .line 203
    move-object v11, v13

    .line 204
    const/4 v0, 0x0

    .line 205
    :goto_4
    if-nez v0, :cond_30

    .line 206
    .line 207
    :try_start_0
    invoke-virtual {v12}, Ljava/io/File;->getParentFile()Ljava/io/File;

    .line 208
    .line 209
    .line 210
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_12
    .catchall {:try_start_0 .. :try_end_0} :catchall_13

    .line 211
    if-eqz v0, :cond_8

    .line 212
    .line 213
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    .line 214
    .line 215
    .line 216
    move-result v8

    .line 217
    if-nez v8, :cond_8

    .line 218
    .line 219
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220
    .line 221
    .line 222
    goto :goto_5

    .line 223
    :catchall_0
    move-exception v0

    .line 224
    move-object v14, v11

    .line 225
    move-object v10, v12

    .line 226
    move-object v7, v13

    .line 227
    move-object v15, v7

    .line 228
    move-object/from16 v18, v15

    .line 229
    .line 230
    move-object/from16 v19, v18

    .line 231
    .line 232
    goto/16 :goto_32

    .line 233
    .line 234
    :catch_0
    move-exception v0

    .line 235
    move-object v14, v11

    .line 236
    move-object v7, v13

    .line 237
    move-object v15, v7

    .line 238
    move-object/from16 v18, v15

    .line 239
    .line 240
    move-object/from16 v24, v18

    .line 241
    .line 242
    goto/16 :goto_2a

    .line 243
    .line 244
    :cond_8
    :goto_5
    :try_start_2
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    .line 245
    .line 246
    .line 247
    move-result v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_12
    .catchall {:try_start_2 .. :try_end_2} :catchall_13

    .line 248
    const-wide/16 v16, 0x0

    .line 249
    .line 250
    if-eqz v0, :cond_9

    .line 251
    .line 252
    :try_start_3
    invoke-virtual {v12}, Ljava/io/File;->length()J

    .line 253
    .line 254
    .line 255
    move-result-wide v8
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 256
    move-wide v9, v8

    .line 257
    goto :goto_6

    .line 258
    :cond_9
    move-wide/from16 v9, v16

    .line 259
    .line 260
    :goto_6
    :try_start_4
    new-instance v0, Lokhttp3/Request$Builder;

    .line 261
    .line 262
    invoke-direct {v0}, Lokhttp3/Request$Builder;-><init>()V

    .line 263
    .line 264
    .line 265
    invoke-virtual {v0, v6}, Lokhttp3/Request$Builder;->〇O〇(Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 266
    .line 267
    .line 268
    move-result-object v0

    .line 269
    iget-object v8, v1, Lcom/vungle/ads/internal/downloader/AssetDownloader;->okHttpClient:Lokhttp3/OkHttpClient;

    .line 270
    .line 271
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->〇o00〇〇Oo()Lokhttp3/Request;

    .line 272
    .line 273
    .line 274
    move-result-object v0

    .line 275
    invoke-virtual {v8, v0}, Lokhttp3/OkHttpClient;->〇080(Lokhttp3/Request;)Lokhttp3/Call;

    .line 276
    .line 277
    .line 278
    move-result-object v18
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_12
    .catchall {:try_start_4 .. :try_end_4} :catchall_13

    .line 279
    :try_start_5
    invoke-interface/range {v18 .. v18}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    .line 280
    .line 281
    .line 282
    move-result-object v8
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_11
    .catchall {:try_start_5 .. :try_end_5} :catchall_12

    .line 283
    :try_start_6
    invoke-direct {v1, v8}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->getContentLength(Lokhttp3/Response;)J

    .line 284
    .line 285
    .line 286
    move-result-wide v19

    .line 287
    invoke-virtual {v8}, Lokhttp3/Response;->〇O8o08O()I

    .line 288
    .line 289
    .line 290
    move-result v0

    .line 291
    invoke-virtual {v8}, Lokhttp3/Response;->〇oo〇()Z

    .line 292
    .line 293
    .line 294
    move-result v21

    .line 295
    if-eqz v21, :cond_1d

    .line 296
    .line 297
    invoke-virtual {v8}, Lokhttp3/Response;->oO80()Lokhttp3/Response;

    .line 298
    .line 299
    .line 300
    move-result-object v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_10
    .catchall {:try_start_6 .. :try_end_6} :catchall_11

    .line 301
    if-eqz v0, :cond_a

    .line 302
    .line 303
    :try_start_7
    sget-object v0, Lcom/vungle/ads/AnalyticsClient;->INSTANCE:Lcom/vungle/ads/AnalyticsClient;

    .line 304
    .line 305
    new-instance v13, Lcom/vungle/ads/SingleValueMetric;

    .line 306
    .line 307
    sget-object v15, Lcom/vungle/ads/internal/protos/Sdk$SDKMetric$SDKMetricType;->CACHED_ASSETS_USED:Lcom/vungle/ads/internal/protos/Sdk$SDKMetric$SDKMetricType;

    .line 308
    .line 309
    invoke-direct {v13, v15}, Lcom/vungle/ads/SingleValueMetric;-><init>(Lcom/vungle/ads/internal/protos/Sdk$SDKMetric$SDKMetricType;)V

    .line 310
    .line 311
    .line 312
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->getPlacementId()Ljava/lang/String;

    .line 313
    .line 314
    .line 315
    move-result-object v15

    .line 316
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->getCreativeId()Ljava/lang/String;

    .line 317
    .line 318
    .line 319
    move-result-object v22

    .line 320
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->getEventId()Ljava/lang/String;

    .line 321
    .line 322
    .line 323
    move-result-object v23
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 324
    move-object/from16 v24, v8

    .line 325
    .line 326
    move-object v8, v0

    .line 327
    move-wide/from16 v25, v9

    .line 328
    .line 329
    move-object v9, v13

    .line 330
    move-object v10, v15

    .line 331
    move-object v15, v11

    .line 332
    move-object/from16 v11, v22

    .line 333
    .line 334
    move-object v13, v12

    .line 335
    move-object/from16 v12, v23

    .line 336
    .line 337
    move-object/from16 v27, v13

    .line 338
    .line 339
    move-object v13, v6

    .line 340
    :try_start_8
    invoke-virtual/range {v8 .. v13}, Lcom/vungle/ads/AnalyticsClient;->logMetric$vungle_ads_release(Lcom/vungle/ads/SingleValueMetric;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    .line 342
    .line 343
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 344
    .line 345
    goto :goto_7

    .line 346
    :catchall_1
    move-exception v0

    .line 347
    move-object/from16 v24, v8

    .line 348
    .line 349
    move-object v14, v11

    .line 350
    move-object v10, v12

    .line 351
    move-object/from16 v13, v24

    .line 352
    .line 353
    goto/16 :goto_24

    .line 354
    .line 355
    :catch_1
    move-exception v0

    .line 356
    move-object/from16 v24, v8

    .line 357
    .line 358
    move-object v14, v11

    .line 359
    goto/16 :goto_26

    .line 360
    .line 361
    :cond_a
    move-object/from16 v24, v8

    .line 362
    .line 363
    move-wide/from16 v25, v9

    .line 364
    .line 365
    move-object v15, v11

    .line 366
    move-object/from16 v27, v12

    .line 367
    .line 368
    :goto_7
    :try_start_9
    invoke-virtual/range {v24 .. v24}, Lokhttp3/Response;->o800o8O()Lokhttp3/Headers;

    .line 369
    .line 370
    .line 371
    move-result-object v0

    .line 372
    const-string v8, "Content-Encoding"

    .line 373
    .line 374
    invoke-virtual {v0, v8}, Lokhttp3/Headers;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 375
    .line 376
    .line 377
    move-result-object v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_e
    .catchall {:try_start_9 .. :try_end_9} :catchall_f

    .line 378
    if-eqz v0, :cond_c

    .line 379
    .line 380
    :try_start_a
    const-string v8, "gzip"

    .line 381
    .line 382
    invoke-static {v8, v0, v14}, Lkotlin/text/StringsKt;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 383
    .line 384
    .line 385
    move-result v8

    .line 386
    if-nez v8, :cond_c

    .line 387
    .line 388
    const-string v8, "identity"

    .line 389
    .line 390
    invoke-static {v8, v0, v14}, Lkotlin/text/StringsKt;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 391
    .line 392
    .line 393
    move-result v8

    .line 394
    if-eqz v8, :cond_b

    .line 395
    .line 396
    goto :goto_9

    .line 397
    :cond_b
    new-instance v8, Ljava/lang/StringBuilder;

    .line 398
    .line 399
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 400
    .line 401
    .line 402
    const-string v9, "loadAd: Unknown Content-Encoding "

    .line 403
    .line 404
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 405
    .line 406
    .line 407
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    .line 409
    .line 410
    new-instance v8, Ljava/io/IOException;

    .line 411
    .line 412
    new-instance v9, Ljava/lang/StringBuilder;

    .line 413
    .line 414
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 415
    .line 416
    .line 417
    const-string v10, "Unknown Content-Encoding "

    .line 418
    .line 419
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    .line 421
    .line 422
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    .line 424
    .line 425
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 426
    .line 427
    .line 428
    move-result-object v0

    .line 429
    invoke-direct {v8, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 430
    .line 431
    .line 432
    throw v8
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 433
    :catchall_2
    move-exception v0

    .line 434
    move-object v14, v15

    .line 435
    move-object/from16 v13, v24

    .line 436
    .line 437
    goto/16 :goto_21

    .line 438
    .line 439
    :catch_2
    move-exception v0

    .line 440
    :goto_8
    move-object v14, v15

    .line 441
    goto/16 :goto_22

    .line 442
    .line 443
    :cond_c
    :goto_9
    move-object/from16 v8, v24

    .line 444
    .line 445
    :try_start_b
    invoke-direct {v1, v8}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->decodeGzipIfNeeded(Lokhttp3/Response;)Lokhttp3/ResponseBody;

    .line 446
    .line 447
    .line 448
    move-result-object v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_d
    .catchall {:try_start_b .. :try_end_b} :catchall_e

    .line 449
    if-eqz v0, :cond_d

    .line 450
    .line 451
    :try_start_c
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->source()Lokio/BufferedSource;

    .line 452
    .line 453
    .line 454
    move-result-object v9
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 455
    move-object v13, v9

    .line 456
    goto :goto_a

    .line 457
    :catchall_3
    move-exception v0

    .line 458
    move-object v13, v8

    .line 459
    move-object v14, v15

    .line 460
    goto/16 :goto_21

    .line 461
    .line 462
    :catch_3
    move-exception v0

    .line 463
    move-object/from16 v24, v8

    .line 464
    .line 465
    goto :goto_8

    .line 466
    :cond_d
    const/4 v13, 0x0

    .line 467
    :goto_a
    :try_start_d
    new-instance v9, Ljava/lang/StringBuilder;

    .line 468
    .line 469
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 470
    .line 471
    .line 472
    const-string v10, "Start download from bytes:"

    .line 473
    .line 474
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475
    .line 476
    .line 477
    move-wide/from16 v10, v25

    .line 478
    .line 479
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 480
    .line 481
    .line 482
    const-string v12, ", url: "

    .line 483
    .line 484
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 485
    .line 486
    .line 487
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    .line 489
    .line 490
    add-long v19, v19, v10

    .line 491
    .line 492
    new-instance v9, Ljava/lang/StringBuilder;

    .line 493
    .line 494
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 495
    .line 496
    .line 497
    const-string v12, "final offset = "

    .line 498
    .line 499
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 500
    .line 501
    .line 502
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_c
    .catchall {:try_start_d .. :try_end_d} :catchall_d

    .line 503
    .line 504
    .line 505
    cmp-long v9, v10, v16

    .line 506
    .line 507
    if-nez v9, :cond_e

    .line 508
    .line 509
    move-object/from16 v12, v27

    .line 510
    .line 511
    const/4 v9, 0x0

    .line 512
    :try_start_e
    invoke-static {v12, v7, v14, v9}, Lokio/Okio;->sink$default(Ljava/io/File;ZILjava/lang/Object;)Lokio/Sink;

    .line 513
    .line 514
    .line 515
    move-result-object v21
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 516
    goto :goto_e

    .line 517
    :catchall_4
    move-exception v0

    .line 518
    move-object v7, v9

    .line 519
    move-object/from16 v19, v7

    .line 520
    .line 521
    move-object v10, v12

    .line 522
    move-object v14, v15

    .line 523
    :goto_b
    move-object v15, v13

    .line 524
    goto/16 :goto_1e

    .line 525
    .line 526
    :catch_4
    move-exception v0

    .line 527
    move-object/from16 v24, v8

    .line 528
    .line 529
    move-object v7, v9

    .line 530
    :goto_c
    move-object v14, v15

    .line 531
    :goto_d
    move-object v15, v13

    .line 532
    goto/16 :goto_2a

    .line 533
    .line 534
    :cond_e
    move-object/from16 v12, v27

    .line 535
    .line 536
    const/4 v9, 0x0

    .line 537
    :try_start_f
    invoke-static {v12}, Lokio/Okio;->appendingSink(Ljava/io/File;)Lokio/Sink;

    .line 538
    .line 539
    .line 540
    move-result-object v21

    .line 541
    :goto_e
    invoke-static/range {v21 .. v21}, Lokio/Okio;->buffer(Lokio/Sink;)Lokio/BufferedSink;

    .line 542
    .line 543
    .line 544
    move-result-object v7
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_b
    .catchall {:try_start_f .. :try_end_f} :catchall_c

    .line 545
    :try_start_10
    sget-object v21, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_a
    .catchall {:try_start_10 .. :try_end_10} :catchall_b

    .line 546
    .line 547
    :try_start_11
    invoke-virtual/range {v21 .. v21}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getSTARTED()I

    .line 548
    .line 549
    .line 550
    move-result v9

    .line 551
    invoke-virtual {v5, v9}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->setStatus(I)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_a
    .catchall {:try_start_11 .. :try_end_11} :catchall_a

    .line 552
    .line 553
    .line 554
    if-eqz v0, :cond_f

    .line 555
    .line 556
    :try_start_12
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->contentLength()J

    .line 557
    .line 558
    .line 559
    move-result-wide v23
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_5
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    .line 560
    move-wide/from16 v36, v23

    .line 561
    .line 562
    move-object/from16 v23, v15

    .line 563
    .line 564
    move-wide/from16 v14, v36

    .line 565
    .line 566
    goto :goto_f

    .line 567
    :catchall_5
    move-exception v0

    .line 568
    move-object v10, v12

    .line 569
    move-object v14, v15

    .line 570
    const/16 v19, 0x0

    .line 571
    .line 572
    goto :goto_b

    .line 573
    :catch_5
    move-exception v0

    .line 574
    move-object/from16 v24, v8

    .line 575
    .line 576
    goto :goto_c

    .line 577
    :cond_f
    move-object/from16 v23, v15

    .line 578
    .line 579
    move-wide/from16 v14, v16

    .line 580
    .line 581
    :goto_f
    :try_start_13
    invoke-virtual {v5, v14, v15}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->setSizeBytes(J)V

    .line 582
    .line 583
    .line 584
    invoke-virtual {v5, v10, v11}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->setStartBytes(J)V

    .line 585
    .line 586
    .line 587
    invoke-direct {v1, v5, v2, v3}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverProgress(Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_9
    .catchall {:try_start_13 .. :try_end_13} :catchall_9

    .line 588
    .line 589
    .line 590
    move-wide/from16 v14, v16

    .line 591
    .line 592
    const/4 v0, 0x0

    .line 593
    :goto_10
    if-eqz v13, :cond_10

    .line 594
    .line 595
    :try_start_14
    invoke-interface {v7}, Lokio/BufferedSink;->getBuffer()Lokio/Buffer;

    .line 596
    .line 597
    .line 598
    move-result-object v9

    .line 599
    const-wide/16 v2, 0x800

    .line 600
    .line 601
    invoke-interface {v13, v9, v2, v3}, Lokio/Source;->read(Lokio/Buffer;J)J

    .line 602
    .line 603
    .line 604
    move-result-wide v2
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_6
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    .line 605
    goto :goto_13

    .line 606
    :catchall_6
    move-exception v0

    .line 607
    move-object/from16 v2, p1

    .line 608
    .line 609
    move-object/from16 v3, p2

    .line 610
    .line 611
    :goto_11
    move-object v10, v12

    .line 612
    move-object v15, v13

    .line 613
    move-object/from16 v14, v23

    .line 614
    .line 615
    goto/16 :goto_1d

    .line 616
    .line 617
    :catch_6
    move-exception v0

    .line 618
    move-object/from16 v2, p1

    .line 619
    .line 620
    move-object/from16 v3, p2

    .line 621
    .line 622
    :goto_12
    move-object/from16 v24, v8

    .line 623
    .line 624
    move-object v15, v13

    .line 625
    move-object/from16 v14, v23

    .line 626
    .line 627
    goto/16 :goto_2a

    .line 628
    .line 629
    :cond_10
    const-wide/16 v2, -0x1

    .line 630
    .line 631
    :goto_13
    :try_start_15
    sget-object v9, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_8
    .catchall {:try_start_15 .. :try_end_15} :catchall_8

    .line 632
    .line 633
    cmp-long v9, v2, v16

    .line 634
    .line 635
    if-lez v9, :cond_15

    .line 636
    .line 637
    :try_start_16
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    .line 638
    .line 639
    .line 640
    move-result v9

    .line 641
    if-eqz v9, :cond_14

    .line 642
    .line 643
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->isCancelled()Z

    .line 644
    .line 645
    .line 646
    move-result v9

    .line 647
    if-eqz v9, :cond_11

    .line 648
    .line 649
    sget-object v0, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;

    .line 650
    .line 651
    invoke-virtual {v0}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getCANCELLED()I

    .line 652
    .line 653
    .line 654
    move-result v0

    .line 655
    invoke-virtual {v5, v0}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->setStatus(I)V

    .line 656
    .line 657
    .line 658
    goto/16 :goto_15

    .line 659
    .line 660
    :cond_11
    invoke-interface {v7}, Lokio/BufferedSink;->emit()Lokio/BufferedSink;

    .line 661
    .line 662
    .line 663
    add-long/2addr v14, v2

    .line 664
    add-long v2, v10, v14

    .line 665
    .line 666
    const-wide/16 v24, 0x64

    .line 667
    .line 668
    cmp-long v9, v19, v16

    .line 669
    .line 670
    if-lez v9, :cond_12

    .line 671
    .line 672
    mul-long v2, v2, v24

    .line 673
    .line 674
    div-long v2, v2, v19

    .line 675
    .line 676
    long-to-int v0, v2

    .line 677
    :cond_12
    :goto_14
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->getProgressPercent()I

    .line 678
    .line 679
    .line 680
    move-result v2

    .line 681
    iget v3, v1, Lcom/vungle/ads/internal/downloader/AssetDownloader;->progressStep:I

    .line 682
    .line 683
    add-int/2addr v2, v3

    .line 684
    if-gt v2, v0, :cond_13

    .line 685
    .line 686
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->getProgressPercent()I

    .line 687
    .line 688
    .line 689
    move-result v2

    .line 690
    iget v3, v1, Lcom/vungle/ads/internal/downloader/AssetDownloader;->progressStep:I

    .line 691
    .line 692
    add-int/2addr v2, v3

    .line 693
    int-to-long v2, v2

    .line 694
    cmp-long v9, v2, v24

    .line 695
    .line 696
    if-gtz v9, :cond_13

    .line 697
    .line 698
    sget-object v2, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;

    .line 699
    .line 700
    invoke-virtual {v2}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getIN_PROGRESS()I

    .line 701
    .line 702
    .line 703
    move-result v2

    .line 704
    invoke-virtual {v5, v2}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->setStatus(I)V

    .line 705
    .line 706
    .line 707
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->getProgressPercent()I

    .line 708
    .line 709
    .line 710
    move-result v2

    .line 711
    iget v3, v1, Lcom/vungle/ads/internal/downloader/AssetDownloader;->progressStep:I

    .line 712
    .line 713
    add-int/2addr v2, v3

    .line 714
    invoke-virtual {v5, v2}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->setProgressPercent(I)V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_6
    .catchall {:try_start_16 .. :try_end_16} :catchall_6

    .line 715
    .line 716
    .line 717
    move-object/from16 v2, p1

    .line 718
    .line 719
    move-object/from16 v3, p2

    .line 720
    .line 721
    :try_start_17
    invoke-direct {v1, v5, v2, v3}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverProgress(Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 722
    .line 723
    .line 724
    goto :goto_14

    .line 725
    :cond_13
    move-object/from16 v2, p1

    .line 726
    .line 727
    move-object/from16 v3, p2

    .line 728
    .line 729
    goto/16 :goto_10

    .line 730
    .line 731
    :cond_14
    move-object/from16 v2, p1

    .line 732
    .line 733
    move-object/from16 v3, p2

    .line 734
    .line 735
    sget-object v27, Lcom/vungle/ads/AnalyticsClient;->INSTANCE:Lcom/vungle/ads/AnalyticsClient;

    .line 736
    .line 737
    const/16 v28, 0x72

    .line 738
    .line 739
    new-instance v0, Ljava/lang/StringBuilder;

    .line 740
    .line 741
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 742
    .line 743
    .line 744
    const-string v9, "Asset save error "

    .line 745
    .line 746
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 747
    .line 748
    .line 749
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 750
    .line 751
    .line 752
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 753
    .line 754
    .line 755
    move-result-object v29

    .line 756
    const/16 v30, 0x0

    .line 757
    .line 758
    const/16 v31, 0x0

    .line 759
    .line 760
    const/16 v32, 0x0

    .line 761
    .line 762
    const/16 v33, 0x1c

    .line 763
    .line 764
    const/16 v34, 0x0

    .line 765
    .line 766
    invoke-static/range {v27 .. v34}, Lcom/vungle/ads/AnalyticsClient;->logError$vungle_ads_release$default(Lcom/vungle/ads/AnalyticsClient;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 767
    .line 768
    .line 769
    new-instance v0, Lcom/vungle/ads/internal/downloader/Downloader$RequestException;

    .line 770
    .line 771
    const-string v9, "File is not existing"

    .line 772
    .line 773
    invoke-direct {v0, v9}, Lcom/vungle/ads/internal/downloader/Downloader$RequestException;-><init>(Ljava/lang/String;)V

    .line 774
    .line 775
    .line 776
    throw v0
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_7
    .catchall {:try_start_17 .. :try_end_17} :catchall_7

    .line 777
    :catchall_7
    move-exception v0

    .line 778
    goto/16 :goto_11

    .line 779
    .line 780
    :catch_7
    move-exception v0

    .line 781
    goto/16 :goto_12

    .line 782
    .line 783
    :cond_15
    :goto_15
    move-object/from16 v2, p1

    .line 784
    .line 785
    move-object/from16 v3, p2

    .line 786
    .line 787
    :try_start_18
    invoke-interface {v7}, Lokio/BufferedSink;->flush()V

    .line 788
    .line 789
    .line 790
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->getStatus()I

    .line 791
    .line 792
    .line 793
    move-result v0

    .line 794
    sget-object v9, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;

    .line 795
    .line 796
    invoke-virtual {v9}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getIN_PROGRESS()I

    .line 797
    .line 798
    .line 799
    move-result v10
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_9
    .catchall {:try_start_18 .. :try_end_18} :catchall_9

    .line 800
    if-ne v0, v10, :cond_16

    .line 801
    .line 802
    :try_start_19
    invoke-virtual {v9}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getDONE()I

    .line 803
    .line 804
    .line 805
    move-result v0

    .line 806
    invoke-virtual {v5, v0}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->setStatus(I)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_7
    .catchall {:try_start_19 .. :try_end_19} :catchall_7

    .line 807
    .line 808
    .line 809
    :cond_16
    invoke-virtual {v8}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 810
    .line 811
    .line 812
    move-result-object v0

    .line 813
    if-eqz v0, :cond_17

    .line 814
    .line 815
    invoke-virtual {v8}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 816
    .line 817
    .line 818
    move-result-object v0

    .line 819
    if-eqz v0, :cond_17

    .line 820
    .line 821
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V

    .line 822
    .line 823
    .line 824
    :cond_17
    invoke-interface/range {v18 .. v18}, Lokhttp3/Call;->cancel()V

    .line 825
    .line 826
    .line 827
    sget-object v0, Lcom/vungle/ads/internal/util/FileUtility;->INSTANCE:Lcom/vungle/ads/internal/util/FileUtility;

    .line 828
    .line 829
    invoke-virtual {v0, v7}, Lcom/vungle/ads/internal/util/FileUtility;->closeQuietly(Ljava/io/Closeable;)V

    .line 830
    .line 831
    .line 832
    invoke-virtual {v0, v13}, Lcom/vungle/ads/internal/util/FileUtility;->closeQuietly(Ljava/io/Closeable;)V

    .line 833
    .line 834
    .line 835
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->getStatus()I

    .line 836
    .line 837
    .line 838
    move-result v0

    .line 839
    invoke-virtual {v9}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getCANCELLED()I

    .line 840
    .line 841
    .line 842
    move-result v7

    .line 843
    if-ne v0, v7, :cond_18

    .line 844
    .line 845
    invoke-direct {v1, v5, v2, v3}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverProgress(Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 846
    .line 847
    .line 848
    :goto_16
    move-object/from16 v14, v23

    .line 849
    .line 850
    goto :goto_17

    .line 851
    :cond_18
    invoke-virtual {v9}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getDONE()I

    .line 852
    .line 853
    .line 854
    move-result v7

    .line 855
    if-ne v0, v7, :cond_19

    .line 856
    .line 857
    invoke-direct {v1, v12, v2, v3}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverSuccess(Ljava/io/File;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 858
    .line 859
    .line 860
    goto :goto_16

    .line 861
    :cond_19
    invoke-virtual {v9}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getSTARTED()I

    .line 862
    .line 863
    .line 864
    move-result v7

    .line 865
    if-ne v0, v7, :cond_1a

    .line 866
    .line 867
    move-object/from16 v14, v23

    .line 868
    .line 869
    if-eqz v23, :cond_1c

    .line 870
    .line 871
    invoke-direct {v1, v2, v3, v14}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 872
    .line 873
    .line 874
    goto :goto_17

    .line 875
    :cond_1a
    move-object/from16 v14, v23

    .line 876
    .line 877
    invoke-virtual {v9}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getERROR()I

    .line 878
    .line 879
    .line 880
    move-result v7

    .line 881
    if-ne v0, v7, :cond_1b

    .line 882
    .line 883
    invoke-direct {v1, v2, v3, v14}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 884
    .line 885
    .line 886
    goto :goto_17

    .line 887
    :cond_1b
    new-instance v0, Ljava/lang/StringBuilder;

    .line 888
    .line 889
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 890
    .line 891
    .line 892
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 893
    .line 894
    .line 895
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->getStatus()I

    .line 896
    .line 897
    .line 898
    move-result v7

    .line 899
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 900
    .line 901
    .line 902
    invoke-direct {v1, v2, v3, v14}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 903
    .line 904
    .line 905
    :cond_1c
    :goto_17
    move-object v10, v12

    .line 906
    move-object v11, v14

    .line 907
    const/16 v19, 0x0

    .line 908
    .line 909
    goto/16 :goto_30

    .line 910
    .line 911
    :catchall_8
    move-exception v0

    .line 912
    move-object/from16 v2, p1

    .line 913
    .line 914
    move-object/from16 v3, p2

    .line 915
    .line 916
    goto :goto_18

    .line 917
    :catch_8
    move-exception v0

    .line 918
    move-object/from16 v2, p1

    .line 919
    .line 920
    move-object/from16 v3, p2

    .line 921
    .line 922
    goto :goto_19

    .line 923
    :catchall_9
    move-exception v0

    .line 924
    :goto_18
    move-object/from16 v14, v23

    .line 925
    .line 926
    goto :goto_1a

    .line 927
    :catch_9
    move-exception v0

    .line 928
    :goto_19
    move-object/from16 v14, v23

    .line 929
    .line 930
    goto :goto_1b

    .line 931
    :catchall_a
    move-exception v0

    .line 932
    move-object v14, v15

    .line 933
    :goto_1a
    move-object v10, v12

    .line 934
    move-object v15, v13

    .line 935
    goto :goto_1d

    .line 936
    :catchall_b
    move-exception v0

    .line 937
    move-object v14, v15

    .line 938
    move-object/from16 v19, v9

    .line 939
    .line 940
    move-object v10, v12

    .line 941
    goto/16 :goto_b

    .line 942
    .line 943
    :catch_a
    move-exception v0

    .line 944
    move-object v14, v15

    .line 945
    :goto_1b
    move-object/from16 v24, v8

    .line 946
    .line 947
    goto/16 :goto_d

    .line 948
    .line 949
    :catchall_c
    move-exception v0

    .line 950
    move-object v14, v15

    .line 951
    move-object v10, v12

    .line 952
    move-object v15, v13

    .line 953
    goto :goto_1c

    .line 954
    :catch_b
    move-exception v0

    .line 955
    move-object v14, v15

    .line 956
    goto :goto_1f

    .line 957
    :catchall_d
    move-exception v0

    .line 958
    move-object v14, v15

    .line 959
    move-object v15, v13

    .line 960
    move-object/from16 v10, v27

    .line 961
    .line 962
    :goto_1c
    const/4 v7, 0x0

    .line 963
    :goto_1d
    const/16 v19, 0x0

    .line 964
    .line 965
    :goto_1e
    move-object v13, v8

    .line 966
    goto/16 :goto_32

    .line 967
    .line 968
    :catch_c
    move-exception v0

    .line 969
    move-object v14, v15

    .line 970
    move-object/from16 v12, v27

    .line 971
    .line 972
    :goto_1f
    move-object/from16 v24, v8

    .line 973
    .line 974
    move-object v15, v13

    .line 975
    const/4 v7, 0x0

    .line 976
    goto/16 :goto_2a

    .line 977
    .line 978
    :catchall_e
    move-exception v0

    .line 979
    move-object v14, v15

    .line 980
    goto :goto_20

    .line 981
    :catch_d
    move-exception v0

    .line 982
    move-object v14, v15

    .line 983
    move-object/from16 v12, v27

    .line 984
    .line 985
    goto :goto_25

    .line 986
    :catchall_f
    move-exception v0

    .line 987
    move-object v14, v15

    .line 988
    move-object/from16 v8, v24

    .line 989
    .line 990
    :goto_20
    move-object v13, v8

    .line 991
    :goto_21
    move-object/from16 v10, v27

    .line 992
    .line 993
    goto :goto_24

    .line 994
    :catch_e
    move-exception v0

    .line 995
    move-object v14, v15

    .line 996
    move-object/from16 v8, v24

    .line 997
    .line 998
    :goto_22
    move-object/from16 v12, v27

    .line 999
    .line 1000
    goto :goto_26

    .line 1001
    :cond_1d
    move-object v14, v11

    .line 1002
    :try_start_1a
    new-instance v7, Lcom/vungle/ads/AssetFailedStatusCodeError;

    .line 1003
    .line 1004
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 1005
    .line 1006
    .line 1007
    move-result-object v9

    .line 1008
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->getPlacementId()Ljava/lang/String;

    .line 1009
    .line 1010
    .line 1011
    move-result-object v10

    .line 1012
    invoke-direct {v7, v6, v9, v10}, Lcom/vungle/ads/AssetFailedStatusCodeError;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 1013
    .line 1014
    .line 1015
    invoke-virtual {v7}, Lcom/vungle/ads/VungleError;->logErrorNoReturnValue$vungle_ads_release()V

    .line 1016
    .line 1017
    .line 1018
    new-instance v7, Lcom/vungle/ads/internal/downloader/Downloader$RequestException;

    .line 1019
    .line 1020
    new-instance v9, Ljava/lang/StringBuilder;

    .line 1021
    .line 1022
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 1023
    .line 1024
    .line 1025
    const-string v10, "Code: "

    .line 1026
    .line 1027
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1028
    .line 1029
    .line 1030
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1031
    .line 1032
    .line 1033
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1034
    .line 1035
    .line 1036
    move-result-object v0

    .line 1037
    invoke-direct {v7, v0}, Lcom/vungle/ads/internal/downloader/Downloader$RequestException;-><init>(Ljava/lang/String;)V

    .line 1038
    .line 1039
    .line 1040
    throw v7
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_f
    .catchall {:try_start_1a .. :try_end_1a} :catchall_10

    .line 1041
    :catchall_10
    move-exception v0

    .line 1042
    goto :goto_23

    .line 1043
    :catch_f
    move-exception v0

    .line 1044
    goto :goto_25

    .line 1045
    :catchall_11
    move-exception v0

    .line 1046
    move-object v14, v11

    .line 1047
    :goto_23
    move-object v13, v8

    .line 1048
    move-object v10, v12

    .line 1049
    :goto_24
    const/4 v7, 0x0

    .line 1050
    goto :goto_27

    .line 1051
    :catch_10
    move-exception v0

    .line 1052
    move-object v14, v11

    .line 1053
    :goto_25
    move-object/from16 v24, v8

    .line 1054
    .line 1055
    :goto_26
    const/4 v7, 0x0

    .line 1056
    const/4 v15, 0x0

    .line 1057
    goto :goto_2a

    .line 1058
    :catchall_12
    move-exception v0

    .line 1059
    move-object v14, v11

    .line 1060
    move-object v10, v12

    .line 1061
    const/4 v7, 0x0

    .line 1062
    const/4 v13, 0x0

    .line 1063
    :goto_27
    const/4 v15, 0x0

    .line 1064
    goto :goto_28

    .line 1065
    :catch_11
    move-exception v0

    .line 1066
    move-object v14, v11

    .line 1067
    const/4 v7, 0x0

    .line 1068
    const/4 v15, 0x0

    .line 1069
    goto :goto_29

    .line 1070
    :catchall_13
    move-exception v0

    .line 1071
    move-object v14, v11

    .line 1072
    move-object v10, v12

    .line 1073
    const/4 v7, 0x0

    .line 1074
    const/4 v13, 0x0

    .line 1075
    const/4 v15, 0x0

    .line 1076
    const/16 v18, 0x0

    .line 1077
    .line 1078
    :goto_28
    const/16 v19, 0x0

    .line 1079
    .line 1080
    goto/16 :goto_32

    .line 1081
    .line 1082
    :catch_12
    move-exception v0

    .line 1083
    move-object v14, v11

    .line 1084
    const/4 v7, 0x0

    .line 1085
    const/4 v15, 0x0

    .line 1086
    const/16 v18, 0x0

    .line 1087
    .line 1088
    :goto_29
    const/16 v24, 0x0

    .line 1089
    .line 1090
    :goto_2a
    :try_start_1b
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 1091
    .line 1092
    .line 1093
    instance-of v8, v0, Ljava/net/ProtocolException;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_16

    .line 1094
    .line 1095
    if-eqz v8, :cond_1e

    .line 1096
    .line 1097
    :try_start_1c
    sget-object v27, Lcom/vungle/ads/AnalyticsClient;->INSTANCE:Lcom/vungle/ads/AnalyticsClient;

    .line 1098
    .line 1099
    const/16 v28, 0x70

    .line 1100
    .line 1101
    new-instance v8, Ljava/lang/StringBuilder;

    .line 1102
    .line 1103
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1104
    .line 1105
    .line 1106
    const-string v9, "Failed to load asset: "

    .line 1107
    .line 1108
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1109
    .line 1110
    .line 1111
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->getUrl()Ljava/lang/String;

    .line 1112
    .line 1113
    .line 1114
    move-result-object v9

    .line 1115
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1116
    .line 1117
    .line 1118
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1119
    .line 1120
    .line 1121
    move-result-object v29

    .line 1122
    const/16 v30, 0x0

    .line 1123
    .line 1124
    const/16 v31, 0x0

    .line 1125
    .line 1126
    const/16 v32, 0x0

    .line 1127
    .line 1128
    const/16 v33, 0x1c

    .line 1129
    .line 1130
    const/16 v34, 0x0

    .line 1131
    .line 1132
    invoke-static/range {v27 .. v34}, Lcom/vungle/ads/AnalyticsClient;->logError$vungle_ads_release$default(Lcom/vungle/ads/AnalyticsClient;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_14

    .line 1133
    .line 1134
    .line 1135
    goto :goto_2b

    .line 1136
    :catchall_14
    move-exception v0

    .line 1137
    move-object v10, v12

    .line 1138
    move-object/from16 v13, v24

    .line 1139
    .line 1140
    goto :goto_28

    .line 1141
    :cond_1e
    :try_start_1d
    instance-of v8, v0, Ljava/net/UnknownHostException;
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_16

    .line 1142
    .line 1143
    if-nez v8, :cond_20

    .line 1144
    .line 1145
    :try_start_1e
    instance-of v8, v0, Ljava/io/IOException;
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_14

    .line 1146
    .line 1147
    if-eqz v8, :cond_1f

    .line 1148
    .line 1149
    goto :goto_2c

    .line 1150
    :cond_1f
    :goto_2b
    move-object/from16 v35, v12

    .line 1151
    .line 1152
    const/16 v19, 0x0

    .line 1153
    .line 1154
    goto :goto_2d

    .line 1155
    :cond_20
    :goto_2c
    :try_start_1f
    new-instance v16, Lcom/vungle/ads/AssetFailedStatusCodeError;

    .line 1156
    .line 1157
    const/4 v10, 0x0

    .line 1158
    invoke-virtual/range {p1 .. p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->getPlacementId()Ljava/lang/String;

    .line 1159
    .line 1160
    .line 1161
    move-result-object v11
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_16

    .line 1162
    const/4 v13, 0x2

    .line 1163
    const/16 v17, 0x0

    .line 1164
    .line 1165
    move-object/from16 v8, v16

    .line 1166
    .line 1167
    const/16 v19, 0x0

    .line 1168
    .line 1169
    move-object v9, v6

    .line 1170
    move-object/from16 v35, v12

    .line 1171
    .line 1172
    move v12, v13

    .line 1173
    move-object/from16 v13, v17

    .line 1174
    .line 1175
    :try_start_20
    invoke-direct/range {v8 .. v13}, Lcom/vungle/ads/AssetFailedStatusCodeError;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 1176
    .line 1177
    .line 1178
    invoke-virtual/range {v16 .. v16}, Lcom/vungle/ads/VungleError;->logErrorNoReturnValue$vungle_ads_release()V

    .line 1179
    .line 1180
    .line 1181
    :goto_2d
    sget-object v8, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;

    .line 1182
    .line 1183
    invoke-virtual {v8}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getERROR()I

    .line 1184
    .line 1185
    .line 1186
    move-result v9

    .line 1187
    invoke-virtual {v5, v9}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->setStatus(I)V

    .line 1188
    .line 1189
    .line 1190
    new-instance v9, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;

    .line 1191
    .line 1192
    sget-object v10, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason$Companion;

    .line 1193
    .line 1194
    invoke-virtual {v10}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason$Companion;->getREQUEST_ERROR()I

    .line 1195
    .line 1196
    .line 1197
    move-result v10

    .line 1198
    const/4 v11, -0x1

    .line 1199
    invoke-direct {v9, v11, v0, v10}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;-><init>(ILjava/lang/Throwable;I)V
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_15

    .line 1200
    .line 1201
    .line 1202
    if-eqz v24, :cond_21

    .line 1203
    .line 1204
    invoke-virtual/range {v24 .. v24}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 1205
    .line 1206
    .line 1207
    move-result-object v13

    .line 1208
    goto :goto_2e

    .line 1209
    :cond_21
    move-object/from16 v13, v19

    .line 1210
    .line 1211
    :goto_2e
    if-eqz v13, :cond_22

    .line 1212
    .line 1213
    invoke-virtual/range {v24 .. v24}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 1214
    .line 1215
    .line 1216
    move-result-object v0

    .line 1217
    if-eqz v0, :cond_22

    .line 1218
    .line 1219
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V

    .line 1220
    .line 1221
    .line 1222
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 1223
    .line 1224
    :cond_22
    if-eqz v18, :cond_23

    .line 1225
    .line 1226
    invoke-interface/range {v18 .. v18}, Lokhttp3/Call;->cancel()V

    .line 1227
    .line 1228
    .line 1229
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 1230
    .line 1231
    :cond_23
    sget-object v0, Lcom/vungle/ads/internal/util/FileUtility;->INSTANCE:Lcom/vungle/ads/internal/util/FileUtility;

    .line 1232
    .line 1233
    invoke-virtual {v0, v7}, Lcom/vungle/ads/internal/util/FileUtility;->closeQuietly(Ljava/io/Closeable;)V

    .line 1234
    .line 1235
    .line 1236
    invoke-virtual {v0, v15}, Lcom/vungle/ads/internal/util/FileUtility;->closeQuietly(Ljava/io/Closeable;)V

    .line 1237
    .line 1238
    .line 1239
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->getStatus()I

    .line 1240
    .line 1241
    .line 1242
    move-result v0

    .line 1243
    invoke-virtual {v8}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getCANCELLED()I

    .line 1244
    .line 1245
    .line 1246
    move-result v7

    .line 1247
    if-ne v0, v7, :cond_24

    .line 1248
    .line 1249
    invoke-direct {v1, v5, v2, v3}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverProgress(Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 1250
    .line 1251
    .line 1252
    move-object/from16 v10, v35

    .line 1253
    .line 1254
    goto :goto_2f

    .line 1255
    :cond_24
    invoke-virtual {v8}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getDONE()I

    .line 1256
    .line 1257
    .line 1258
    move-result v7

    .line 1259
    if-ne v0, v7, :cond_25

    .line 1260
    .line 1261
    move-object/from16 v10, v35

    .line 1262
    .line 1263
    invoke-direct {v1, v10, v2, v3}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverSuccess(Ljava/io/File;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 1264
    .line 1265
    .line 1266
    goto :goto_2f

    .line 1267
    :cond_25
    move-object/from16 v10, v35

    .line 1268
    .line 1269
    invoke-virtual {v8}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getSTARTED()I

    .line 1270
    .line 1271
    .line 1272
    move-result v7

    .line 1273
    if-ne v0, v7, :cond_26

    .line 1274
    .line 1275
    invoke-direct {v1, v2, v3, v9}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 1276
    .line 1277
    .line 1278
    goto :goto_2f

    .line 1279
    :cond_26
    invoke-virtual {v8}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getERROR()I

    .line 1280
    .line 1281
    .line 1282
    move-result v7

    .line 1283
    if-ne v0, v7, :cond_27

    .line 1284
    .line 1285
    invoke-direct {v1, v2, v3, v9}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 1286
    .line 1287
    .line 1288
    goto :goto_2f

    .line 1289
    :cond_27
    new-instance v0, Ljava/lang/StringBuilder;

    .line 1290
    .line 1291
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1292
    .line 1293
    .line 1294
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1295
    .line 1296
    .line 1297
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->getStatus()I

    .line 1298
    .line 1299
    .line 1300
    move-result v7

    .line 1301
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1302
    .line 1303
    .line 1304
    invoke-direct {v1, v2, v3, v9}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 1305
    .line 1306
    .line 1307
    :goto_2f
    move-object v11, v9

    .line 1308
    :goto_30
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 1309
    .line 1310
    move-object v12, v10

    .line 1311
    move-object/from16 v13, v19

    .line 1312
    .line 1313
    const/4 v0, 0x1

    .line 1314
    const/4 v7, 0x0

    .line 1315
    const/4 v14, 0x1

    .line 1316
    const/4 v15, -0x1

    .line 1317
    goto/16 :goto_4

    .line 1318
    .line 1319
    :catchall_15
    move-exception v0

    .line 1320
    move-object/from16 v10, v35

    .line 1321
    .line 1322
    goto :goto_31

    .line 1323
    :catchall_16
    move-exception v0

    .line 1324
    move-object v10, v12

    .line 1325
    const/16 v19, 0x0

    .line 1326
    .line 1327
    :goto_31
    move-object/from16 v13, v24

    .line 1328
    .line 1329
    :goto_32
    if-eqz v13, :cond_28

    .line 1330
    .line 1331
    invoke-virtual {v13}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 1332
    .line 1333
    .line 1334
    move-result-object v6

    .line 1335
    move-object/from16 v19, v6

    .line 1336
    .line 1337
    :cond_28
    if-eqz v19, :cond_29

    .line 1338
    .line 1339
    invoke-virtual {v13}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 1340
    .line 1341
    .line 1342
    move-result-object v6

    .line 1343
    if-eqz v6, :cond_29

    .line 1344
    .line 1345
    invoke-virtual {v6}, Lokhttp3/ResponseBody;->close()V

    .line 1346
    .line 1347
    .line 1348
    sget-object v6, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 1349
    .line 1350
    :cond_29
    if-eqz v18, :cond_2a

    .line 1351
    .line 1352
    invoke-interface/range {v18 .. v18}, Lokhttp3/Call;->cancel()V

    .line 1353
    .line 1354
    .line 1355
    sget-object v6, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 1356
    .line 1357
    :cond_2a
    sget-object v6, Lcom/vungle/ads/internal/util/FileUtility;->INSTANCE:Lcom/vungle/ads/internal/util/FileUtility;

    .line 1358
    .line 1359
    invoke-virtual {v6, v7}, Lcom/vungle/ads/internal/util/FileUtility;->closeQuietly(Ljava/io/Closeable;)V

    .line 1360
    .line 1361
    .line 1362
    invoke-virtual {v6, v15}, Lcom/vungle/ads/internal/util/FileUtility;->closeQuietly(Ljava/io/Closeable;)V

    .line 1363
    .line 1364
    .line 1365
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->getStatus()I

    .line 1366
    .line 1367
    .line 1368
    move-result v6

    .line 1369
    sget-object v7, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;

    .line 1370
    .line 1371
    invoke-virtual {v7}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getCANCELLED()I

    .line 1372
    .line 1373
    .line 1374
    move-result v8

    .line 1375
    if-eq v6, v8, :cond_2e

    .line 1376
    .line 1377
    invoke-virtual {v7}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getDONE()I

    .line 1378
    .line 1379
    .line 1380
    move-result v8

    .line 1381
    if-eq v6, v8, :cond_2d

    .line 1382
    .line 1383
    invoke-virtual {v7}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getSTARTED()I

    .line 1384
    .line 1385
    .line 1386
    move-result v8

    .line 1387
    if-ne v6, v8, :cond_2b

    .line 1388
    .line 1389
    if-eqz v14, :cond_2f

    .line 1390
    .line 1391
    invoke-direct {v1, v2, v3, v14}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 1392
    .line 1393
    .line 1394
    goto :goto_33

    .line 1395
    :cond_2b
    invoke-virtual {v7}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress$ProgressStatus$Companion;->getERROR()I

    .line 1396
    .line 1397
    .line 1398
    move-result v7

    .line 1399
    if-ne v6, v7, :cond_2c

    .line 1400
    .line 1401
    invoke-direct {v1, v2, v3, v14}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 1402
    .line 1403
    .line 1404
    goto :goto_33

    .line 1405
    :cond_2c
    new-instance v6, Ljava/lang/StringBuilder;

    .line 1406
    .line 1407
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1408
    .line 1409
    .line 1410
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1411
    .line 1412
    .line 1413
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;->getStatus()I

    .line 1414
    .line 1415
    .line 1416
    move-result v4

    .line 1417
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1418
    .line 1419
    .line 1420
    invoke-direct {v1, v2, v3, v14}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 1421
    .line 1422
    .line 1423
    goto :goto_33

    .line 1424
    :cond_2d
    invoke-direct {v1, v10, v2, v3}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverSuccess(Ljava/io/File;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 1425
    .line 1426
    .line 1427
    goto :goto_33

    .line 1428
    :cond_2e
    invoke-direct {v1, v5, v2, v3}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverProgress(Lcom/vungle/ads/internal/downloader/AssetDownloadListener$Progress;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 1429
    .line 1430
    .line 1431
    :cond_2f
    :goto_33
    throw v0

    .line 1432
    :cond_30
    return-void

    .line 1433
    :cond_31
    :goto_34
    new-instance v0, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;

    .line 1434
    .line 1435
    new-instance v4, Lcom/vungle/ads/AssetDownloadError;

    .line 1436
    .line 1437
    invoke-direct {v4}, Lcom/vungle/ads/AssetDownloadError;-><init>()V

    .line 1438
    .line 1439
    .line 1440
    sget-object v5, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason;->Companion:Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason$Companion;

    .line 1441
    .line 1442
    invoke-virtual {v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError$ErrorReason$Companion;->getINTERNAL_ERROR()I

    .line 1443
    .line 1444
    .line 1445
    move-result v5

    .line 1446
    const/4 v6, -0x1

    .line 1447
    invoke-direct {v0, v6, v4, v5}, Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;-><init>(ILjava/lang/Throwable;I)V

    .line 1448
    .line 1449
    .line 1450
    invoke-direct {v1, v2, v3, v0}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->deliverError(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;Lcom/vungle/ads/internal/downloader/AssetDownloadListener$DownloadError;)V

    .line 1451
    .line 1452
    .line 1453
    return-void
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
.end method

.method public static synthetic 〇080(Lcom/vungle/ads/internal/downloader/AssetDownloader;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->download$lambda-0(Lcom/vungle/ads/internal/downloader/AssetDownloader;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public cancel(Lcom/vungle/ads/internal/downloader/DownloadRequest;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->isCancelled()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p1}, Lcom/vungle/ads/internal/downloader/DownloadRequest;->cancel()V

    .line 11
    .line 12
    .line 13
    :cond_1
    :goto_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public cancelAll()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/vungle/ads/internal/downloader/AssetDownloader;->transitioning:Ljava/util/List;

    .line 2
    .line 3
    check-cast v0, Ljava/lang/Iterable;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/vungle/ads/internal/downloader/DownloadRequest;

    .line 20
    .line 21
    invoke-virtual {p0, v1}, Lcom/vungle/ads/internal/downloader/AssetDownloader;->cancel(Lcom/vungle/ads/internal/downloader/DownloadRequest;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/vungle/ads/internal/downloader/AssetDownloader;->transitioning:Ljava/util/List;

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public download(Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V
    .locals 3

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/vungle/ads/internal/downloader/AssetDownloader;->transitioning:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/vungle/ads/internal/downloader/AssetDownloader;->downloadExecutor:Lcom/vungle/ads/internal/executor/VungleThreadPoolExecutor;

    .line 10
    .line 11
    new-instance v1, Lcom/vungle/ads/internal/downloader/AssetDownloader$download$1;

    .line 12
    .line 13
    invoke-direct {v1, p0, p1, p2}, Lcom/vungle/ads/internal/downloader/AssetDownloader$download$1;-><init>(Lcom/vungle/ads/internal/downloader/AssetDownloader;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 14
    .line 15
    .line 16
    new-instance v2, Lcom/vungle/ads/internal/downloader/〇080;

    .line 17
    .line 18
    invoke-direct {v2, p0, p1, p2}, Lcom/vungle/ads/internal/downloader/〇080;-><init>(Lcom/vungle/ads/internal/downloader/AssetDownloader;Lcom/vungle/ads/internal/downloader/DownloadRequest;Lcom/vungle/ads/internal/downloader/AssetDownloadListener;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1, v2}, Lcom/vungle/ads/internal/executor/VungleThreadPoolExecutor;->execute(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.class public final enum Lcom/vungle/ads/VungleAds$WrapperFramework;
.super Ljava/lang/Enum;
.source "VungleAds.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vungle/ads/VungleAds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WrapperFramework"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/vungle/ads/VungleAds$WrapperFramework;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum admob:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum adtoapp:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum aerserv:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum air:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum appodeal:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum cocos2dx:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum corona:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum dfp:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum fyber:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum heyzap:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum ironsource:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum marmalade:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum max:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum mopub:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum none:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum tapdaq:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum unity:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum upsight:Lcom/vungle/ads/VungleAds$WrapperFramework;

.field public static final enum vunglehbs:Lcom/vungle/ads/VungleAds$WrapperFramework;


# direct methods
.method private static final synthetic $values()[Lcom/vungle/ads/VungleAds$WrapperFramework;
    .locals 3

    .line 1
    const/16 v0, 0x13

    .line 2
    .line 3
    new-array v0, v0, [Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->admob:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->air:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->cocos2dx:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->corona:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->dfp:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->heyzap:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->marmalade:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->mopub:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->unity:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    const/16 v1, 0x9

    .line 52
    .line 53
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->fyber:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 54
    .line 55
    aput-object v2, v0, v1

    .line 56
    .line 57
    const/16 v1, 0xa

    .line 58
    .line 59
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->ironsource:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 60
    .line 61
    aput-object v2, v0, v1

    .line 62
    .line 63
    const/16 v1, 0xb

    .line 64
    .line 65
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->upsight:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 66
    .line 67
    aput-object v2, v0, v1

    .line 68
    .line 69
    const/16 v1, 0xc

    .line 70
    .line 71
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->appodeal:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 72
    .line 73
    aput-object v2, v0, v1

    .line 74
    .line 75
    const/16 v1, 0xd

    .line 76
    .line 77
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->aerserv:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 78
    .line 79
    aput-object v2, v0, v1

    .line 80
    .line 81
    const/16 v1, 0xe

    .line 82
    .line 83
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->adtoapp:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 84
    .line 85
    aput-object v2, v0, v1

    .line 86
    .line 87
    const/16 v1, 0xf

    .line 88
    .line 89
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->tapdaq:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 90
    .line 91
    aput-object v2, v0, v1

    .line 92
    .line 93
    const/16 v1, 0x10

    .line 94
    .line 95
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->vunglehbs:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 96
    .line 97
    aput-object v2, v0, v1

    .line 98
    .line 99
    const/16 v1, 0x11

    .line 100
    .line 101
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->max:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 102
    .line 103
    aput-object v2, v0, v1

    .line 104
    .line 105
    const/16 v1, 0x12

    .line 106
    .line 107
    sget-object v2, Lcom/vungle/ads/VungleAds$WrapperFramework;->none:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 108
    .line 109
    aput-object v2, v0, v1

    .line 110
    .line 111
    return-object v0
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 2
    .line 3
    const-string v1, "admob"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->admob:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 10
    .line 11
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 12
    .line 13
    const-string v1, "air"

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->air:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 20
    .line 21
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 22
    .line 23
    const-string v1, "cocos2dx"

    .line 24
    .line 25
    const/4 v2, 0x2

    .line 26
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->cocos2dx:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 30
    .line 31
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 32
    .line 33
    const-string v1, "corona"

    .line 34
    .line 35
    const/4 v2, 0x3

    .line 36
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->corona:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 40
    .line 41
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 42
    .line 43
    const-string v1, "dfp"

    .line 44
    .line 45
    const/4 v2, 0x4

    .line 46
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 47
    .line 48
    .line 49
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->dfp:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 50
    .line 51
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 52
    .line 53
    const-string v1, "heyzap"

    .line 54
    .line 55
    const/4 v2, 0x5

    .line 56
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 57
    .line 58
    .line 59
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->heyzap:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 60
    .line 61
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 62
    .line 63
    const-string v1, "marmalade"

    .line 64
    .line 65
    const/4 v2, 0x6

    .line 66
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 67
    .line 68
    .line 69
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->marmalade:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 70
    .line 71
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 72
    .line 73
    const-string v1, "mopub"

    .line 74
    .line 75
    const/4 v2, 0x7

    .line 76
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 77
    .line 78
    .line 79
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->mopub:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 80
    .line 81
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 82
    .line 83
    const-string/jumbo v1, "unity"

    .line 84
    .line 85
    .line 86
    const/16 v2, 0x8

    .line 87
    .line 88
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 89
    .line 90
    .line 91
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->unity:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 92
    .line 93
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 94
    .line 95
    const-string v1, "fyber"

    .line 96
    .line 97
    const/16 v2, 0x9

    .line 98
    .line 99
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 100
    .line 101
    .line 102
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->fyber:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 103
    .line 104
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 105
    .line 106
    const-string v1, "ironsource"

    .line 107
    .line 108
    const/16 v2, 0xa

    .line 109
    .line 110
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 111
    .line 112
    .line 113
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->ironsource:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 114
    .line 115
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 116
    .line 117
    const-string/jumbo v1, "upsight"

    .line 118
    .line 119
    .line 120
    const/16 v2, 0xb

    .line 121
    .line 122
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 123
    .line 124
    .line 125
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->upsight:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 126
    .line 127
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 128
    .line 129
    const-string v1, "appodeal"

    .line 130
    .line 131
    const/16 v2, 0xc

    .line 132
    .line 133
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 134
    .line 135
    .line 136
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->appodeal:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 137
    .line 138
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 139
    .line 140
    const-string v1, "aerserv"

    .line 141
    .line 142
    const/16 v2, 0xd

    .line 143
    .line 144
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 145
    .line 146
    .line 147
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->aerserv:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 148
    .line 149
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 150
    .line 151
    const-string v1, "adtoapp"

    .line 152
    .line 153
    const/16 v2, 0xe

    .line 154
    .line 155
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 156
    .line 157
    .line 158
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->adtoapp:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 159
    .line 160
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 161
    .line 162
    const-string/jumbo v1, "tapdaq"

    .line 163
    .line 164
    .line 165
    const/16 v2, 0xf

    .line 166
    .line 167
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 168
    .line 169
    .line 170
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->tapdaq:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 171
    .line 172
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 173
    .line 174
    const-string/jumbo v1, "vunglehbs"

    .line 175
    .line 176
    .line 177
    const/16 v2, 0x10

    .line 178
    .line 179
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 180
    .line 181
    .line 182
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->vunglehbs:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 183
    .line 184
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 185
    .line 186
    const-string v1, "max"

    .line 187
    .line 188
    const/16 v2, 0x11

    .line 189
    .line 190
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 191
    .line 192
    .line 193
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->max:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 194
    .line 195
    new-instance v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 196
    .line 197
    const-string v1, "none"

    .line 198
    .line 199
    const/16 v2, 0x12

    .line 200
    .line 201
    invoke-direct {v0, v1, v2}, Lcom/vungle/ads/VungleAds$WrapperFramework;-><init>(Ljava/lang/String;I)V

    .line 202
    .line 203
    .line 204
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->none:Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 205
    .line 206
    invoke-static {}, Lcom/vungle/ads/VungleAds$WrapperFramework;->$values()[Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 207
    .line 208
    .line 209
    move-result-object v0

    .line 210
    sput-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->$VALUES:[Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 211
    .line 212
    return-void
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vungle/ads/VungleAds$WrapperFramework;
    .locals 1

    .line 1
    const-class v0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lcom/vungle/ads/VungleAds$WrapperFramework;
    .locals 1

    .line 1
    sget-object v0, Lcom/vungle/ads/VungleAds$WrapperFramework;->$VALUES:[Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/vungle/ads/VungleAds$WrapperFramework;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.class public Lcom/yubico/yubikit/core/smartcard/Apdu;
.super Ljava/lang/Object;
.source "Apdu.java"


# instance fields
.field private final O8:B

.field private final Oo08:[B

.field private final 〇080:B

.field private final 〇o00〇〇Oo:B

.field private final 〇o〇:B


# direct methods
.method private constructor <init>(BBBB[B)V
    .locals 0
    .param p5    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-byte p1, p0, Lcom/yubico/yubikit/core/smartcard/Apdu;->〇080:B

    .line 3
    iput-byte p2, p0, Lcom/yubico/yubikit/core/smartcard/Apdu;->〇o00〇〇Oo:B

    .line 4
    iput-byte p3, p0, Lcom/yubico/yubikit/core/smartcard/Apdu;->〇o〇:B

    .line 5
    iput-byte p4, p0, Lcom/yubico/yubikit/core/smartcard/Apdu;->O8:B

    if-nez p5, :cond_0

    const/4 p1, 0x0

    new-array p5, p1, [B

    .line 6
    :cond_0
    iput-object p5, p0, Lcom/yubico/yubikit/core/smartcard/Apdu;->Oo08:[B

    return-void
.end method

.method public constructor <init>(IIII[B)V
    .locals 7
    .param p5    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    const-string v0, "CLA"

    .line 7
    invoke-static {p1, v0}, Lcom/yubico/yubikit/core/smartcard/Apdu;->o〇0(ILjava/lang/String;)B

    move-result v2

    const-string p1, "INS"

    .line 8
    invoke-static {p2, p1}, Lcom/yubico/yubikit/core/smartcard/Apdu;->o〇0(ILjava/lang/String;)B

    move-result v3

    const-string p1, "P1"

    .line 9
    invoke-static {p3, p1}, Lcom/yubico/yubikit/core/smartcard/Apdu;->o〇0(ILjava/lang/String;)B

    move-result v4

    const-string p1, "P2"

    .line 10
    invoke-static {p4, p1}, Lcom/yubico/yubikit/core/smartcard/Apdu;->o〇0(ILjava/lang/String;)B

    move-result v5

    move-object v1, p0

    move-object v6, p5

    .line 11
    invoke-direct/range {v1 .. v6}, Lcom/yubico/yubikit/core/smartcard/Apdu;-><init>(BBBB[B)V

    return-void
.end method

.method private static o〇0(ILjava/lang/String;)B
    .locals 2

    .line 1
    const/16 v0, 0xff

    .line 2
    .line 3
    if-gt p0, v0, :cond_0

    .line 4
    .line 5
    const/16 v0, -0x80

    .line 6
    .line 7
    if-lt p0, v0, :cond_0

    .line 8
    .line 9
    int-to-byte p0, p0

    .line 10
    return p0

    .line 11
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 12
    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v1, "Invalid value for "

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string p1, ", must fit in a byte"

    .line 27
    .line 28
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    throw p0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public O8()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/yubico/yubikit/core/smartcard/Apdu;->〇o〇:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public Oo08()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/yubico/yubikit/core/smartcard/Apdu;->O8:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇080()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/yubico/yubikit/core/smartcard/Apdu;->〇080:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇o00〇〇Oo()[B
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/core/smartcard/Apdu;->Oo08:[B

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇o〇()B
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/yubico/yubikit/core/smartcard/Apdu;->〇o00〇〇Oo:B

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.class public final enum Lcom/yubico/yubikit/core/UsbInterface$Mode;
.super Ljava/lang/Enum;
.source "UsbInterface.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/yubico/yubikit/core/UsbInterface$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/yubico/yubikit/core/UsbInterface$Mode;

.field public static final enum CCID:Lcom/yubico/yubikit/core/UsbInterface$Mode;

.field public static final enum FIDO:Lcom/yubico/yubikit/core/UsbInterface$Mode;

.field public static final enum FIDO_CCID:Lcom/yubico/yubikit/core/UsbInterface$Mode;

.field public static final enum OTP:Lcom/yubico/yubikit/core/UsbInterface$Mode;

.field public static final enum OTP_CCID:Lcom/yubico/yubikit/core/UsbInterface$Mode;

.field public static final enum OTP_FIDO:Lcom/yubico/yubikit/core/UsbInterface$Mode;

.field public static final enum OTP_FIDO_CCID:Lcom/yubico/yubikit/core/UsbInterface$Mode;


# instance fields
.field public final interfaces:I

.field public final value:B


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 2
    .line 3
    const-string v1, "OTP"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x1

    .line 7
    invoke-direct {v0, v1, v2, v2, v3}, Lcom/yubico/yubikit/core/UsbInterface$Mode;-><init>(Ljava/lang/String;IBI)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/yubico/yubikit/core/UsbInterface$Mode;->OTP:Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 11
    .line 12
    new-instance v1, Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 13
    .line 14
    const-string v4, "CCID"

    .line 15
    .line 16
    const/4 v5, 0x4

    .line 17
    invoke-direct {v1, v4, v3, v3, v5}, Lcom/yubico/yubikit/core/UsbInterface$Mode;-><init>(Ljava/lang/String;IBI)V

    .line 18
    .line 19
    .line 20
    sput-object v1, Lcom/yubico/yubikit/core/UsbInterface$Mode;->CCID:Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 21
    .line 22
    new-instance v4, Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 23
    .line 24
    const-string v6, "OTP_CCID"

    .line 25
    .line 26
    const/4 v7, 0x2

    .line 27
    const/4 v8, 0x5

    .line 28
    invoke-direct {v4, v6, v7, v7, v8}, Lcom/yubico/yubikit/core/UsbInterface$Mode;-><init>(Ljava/lang/String;IBI)V

    .line 29
    .line 30
    .line 31
    sput-object v4, Lcom/yubico/yubikit/core/UsbInterface$Mode;->OTP_CCID:Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 32
    .line 33
    new-instance v6, Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 34
    .line 35
    const-string v9, "FIDO"

    .line 36
    .line 37
    const/4 v10, 0x3

    .line 38
    invoke-direct {v6, v9, v10, v10, v7}, Lcom/yubico/yubikit/core/UsbInterface$Mode;-><init>(Ljava/lang/String;IBI)V

    .line 39
    .line 40
    .line 41
    sput-object v6, Lcom/yubico/yubikit/core/UsbInterface$Mode;->FIDO:Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 42
    .line 43
    new-instance v9, Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 44
    .line 45
    const-string v11, "OTP_FIDO"

    .line 46
    .line 47
    invoke-direct {v9, v11, v5, v5, v10}, Lcom/yubico/yubikit/core/UsbInterface$Mode;-><init>(Ljava/lang/String;IBI)V

    .line 48
    .line 49
    .line 50
    sput-object v9, Lcom/yubico/yubikit/core/UsbInterface$Mode;->OTP_FIDO:Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 51
    .line 52
    new-instance v11, Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 53
    .line 54
    const-string v12, "FIDO_CCID"

    .line 55
    .line 56
    const/4 v13, 0x6

    .line 57
    invoke-direct {v11, v12, v8, v8, v13}, Lcom/yubico/yubikit/core/UsbInterface$Mode;-><init>(Ljava/lang/String;IBI)V

    .line 58
    .line 59
    .line 60
    sput-object v11, Lcom/yubico/yubikit/core/UsbInterface$Mode;->FIDO_CCID:Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 61
    .line 62
    new-instance v12, Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 63
    .line 64
    const-string v14, "OTP_FIDO_CCID"

    .line 65
    .line 66
    const/4 v15, 0x7

    .line 67
    invoke-direct {v12, v14, v13, v13, v15}, Lcom/yubico/yubikit/core/UsbInterface$Mode;-><init>(Ljava/lang/String;IBI)V

    .line 68
    .line 69
    .line 70
    sput-object v12, Lcom/yubico/yubikit/core/UsbInterface$Mode;->OTP_FIDO_CCID:Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 71
    .line 72
    new-array v14, v15, [Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 73
    .line 74
    aput-object v0, v14, v2

    .line 75
    .line 76
    aput-object v1, v14, v3

    .line 77
    .line 78
    aput-object v4, v14, v7

    .line 79
    .line 80
    aput-object v6, v14, v10

    .line 81
    .line 82
    aput-object v9, v14, v5

    .line 83
    .line 84
    aput-object v11, v14, v8

    .line 85
    .line 86
    aput-object v12, v14, v13

    .line 87
    .line 88
    sput-object v14, Lcom/yubico/yubikit/core/UsbInterface$Mode;->$VALUES:[Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 89
    .line 90
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method private constructor <init>(Ljava/lang/String;IBI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(BI)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-byte p3, p0, Lcom/yubico/yubikit/core/UsbInterface$Mode;->value:B

    .line 5
    .line 6
    iput p4, p0, Lcom/yubico/yubikit/core/UsbInterface$Mode;->interfaces:I

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method public static getMode(I)Lcom/yubico/yubikit/core/UsbInterface$Mode;
    .locals 5

    .line 1
    invoke-static {}, Lcom/yubico/yubikit/core/UsbInterface$Mode;->values()[Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    array-length v1, v0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    if-ge v2, v1, :cond_1

    .line 8
    .line 9
    aget-object v3, v0, v2

    .line 10
    .line 11
    iget v4, v3, Lcom/yubico/yubikit/core/UsbInterface$Mode;->interfaces:I

    .line 12
    .line 13
    if-ne v4, p0, :cond_0

    .line 14
    .line 15
    return-object v3

    .line 16
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 20
    .line 21
    const-string v0, "Invalid interfaces for Mode"

    .line 22
    .line 23
    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    throw p0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/yubico/yubikit/core/UsbInterface$Mode;
    .locals 1

    .line 1
    const-class v0, Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lcom/yubico/yubikit/core/UsbInterface$Mode;
    .locals 1

    .line 1
    sget-object v0, Lcom/yubico/yubikit/core/UsbInterface$Mode;->$VALUES:[Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/yubico/yubikit/core/UsbInterface$Mode;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/yubico/yubikit/core/UsbInterface$Mode;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.class public final Lcom/yubico/yubikit/android/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yubico/yubikit/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final yubikit_otp_activity_title:I = 0x7f131fa7

.field public static final yubikit_otp_touch:I = 0x7f131fa8

.field public static final yubikit_prompt_activity_title:I = 0x7f131fa9

.field public static final yubikit_prompt_enable_nfc:I = 0x7f131faa

.field public static final yubikit_prompt_image_desc:I = 0x7f131fab

.field public static final yubikit_prompt_plug_in:I = 0x7f131fac

.field public static final yubikit_prompt_plug_in_or_tap:I = 0x7f131fad

.field public static final yubikit_prompt_remove:I = 0x7f131fae

.field public static final yubikit_prompt_uv:I = 0x7f131faf

.field public static final yubikit_prompt_wait:I = 0x7f131fb0


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

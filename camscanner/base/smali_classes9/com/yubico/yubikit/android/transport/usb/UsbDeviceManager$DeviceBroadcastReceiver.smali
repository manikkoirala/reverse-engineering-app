.class Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager$DeviceBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UsbDeviceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeviceBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;


# direct methods
.method private constructor <init>(Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager$DeviceBroadcastReceiver;->〇080:Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager$1;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager$DeviceBroadcastReceiver;-><init>(Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .line 1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "device"

    .line 6
    .line 7
    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    check-cast p2, Landroid/hardware/usb/UsbDevice;

    .line 12
    .line 13
    if-eqz p2, :cond_2

    .line 14
    .line 15
    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    const/16 v2, 0x1050

    .line 20
    .line 21
    if-eq v1, v2, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const-string v1, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    .line 25
    .line 26
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-eqz v1, :cond_1

    .line 31
    .line 32
    iget-object p1, p0, Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager$DeviceBroadcastReceiver;->〇080:Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;

    .line 33
    .line 34
    invoke-static {p1, p2}, Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;->〇080(Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;Landroid/hardware/usb/UsbDevice;)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    .line 39
    .line 40
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-eqz v0, :cond_2

    .line 45
    .line 46
    iget-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager$DeviceBroadcastReceiver;->〇080:Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;

    .line 47
    .line 48
    invoke-static {v0, p1, p2}, Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;->〇o00〇〇Oo(Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;Landroid/content/Context;Landroid/hardware/usb/UsbDevice;)V

    .line 49
    .line 50
    .line 51
    :cond_2
    :goto_0
    return-void
    .line 52
.end method

.class public Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;
.super Ljava/lang/Object;
.source "UsbYubiKeyDevice.java"

# interfaces
.implements Lcom/yubico/yubikit/core/YubiKeyDevice;
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;
    }
.end annotation


# static fields
.field private static final 〇0O:Lcom/yubico/yubikit/core/util/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Result<",
            "Lcom/yubico/yubikit/core/otp/OtpConnection;",
            "Ljava/io/IOException;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final OO:Landroid/hardware/usb/UsbManager;

.field private final o0:Ljava/util/concurrent/ExecutorService;

.field private final o〇00O:Lcom/yubico/yubikit/core/UsbPid;

.field private 〇080OO8〇0:Ljava/lang/Runnable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Landroid/hardware/usb/UsbDevice;

.field private final 〇OOo8〇0:Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, L〇o8〇〇/〇o〇;

    .line 2
    .line 3
    invoke-direct {v0}, L〇o8〇〇/〇o〇;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇0O:Lcom/yubico/yubikit/core/util/Callback;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbManager;Landroid/hardware/usb/UsbDevice;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iput-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->o0:Ljava/util/concurrent/ExecutorService;

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    iput-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->O8o08O8O:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇080OO8〇0:Ljava/lang/Runnable;

    .line 14
    .line 15
    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    const/16 v1, 0x1050

    .line 20
    .line 21
    if-ne v0, v1, :cond_0

    .line 22
    .line 23
    invoke-virtual {p2}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    invoke-static {v0}, Lcom/yubico/yubikit/core/UsbPid;->fromValue(I)Lcom/yubico/yubikit/core/UsbPid;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iput-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->o〇00O:Lcom/yubico/yubikit/core/UsbPid;

    .line 32
    .line 33
    new-instance v0, Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;

    .line 34
    .line 35
    invoke-direct {v0, p1, p2}, Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;-><init>(Landroid/hardware/usb/UsbManager;Landroid/hardware/usb/UsbDevice;)V

    .line 36
    .line 37
    .line 38
    iput-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇OOo8〇0:Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;

    .line 39
    .line 40
    iput-object p2, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇08O〇00〇o:Landroid/hardware/usb/UsbDevice;

    .line 41
    .line 42
    iput-object p1, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->OO:Landroid/hardware/usb/UsbManager;

    .line 43
    .line 44
    return-void

    .line 45
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 46
    .line 47
    const-string p2, "Invalid vendor id"

    .line 48
    .line 49
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    throw p1
.end method

.method static synthetic OO0o〇〇(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;)Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇OOo8〇0:Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static synthetic Oo08(Lcom/yubico/yubikit/core/util/Callback;Lcom/yubico/yubikit/core/util/Result;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇O〇(Lcom/yubico/yubikit/core/util/Callback;Lcom/yubico/yubikit/core/util/Result;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static synthetic oO80(Lcom/yubico/yubikit/core/util/Result;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇〇8O0〇8(Lcom/yubico/yubikit/core/util/Result;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇8o8o〇(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;)Ljava/util/concurrent/ExecutorService;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->o0:Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private synthetic 〇O00(Ljava/lang/Class;Lcom/yubico/yubikit/core/util/Callback;)V
    .locals 1

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇OOo8〇0:Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;->〇o00〇〇Oo(Ljava/lang/Class;)Lcom/yubico/yubikit/core/YubiKeyConnection;

    .line 4
    .line 5
    .line 6
    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7
    :try_start_1
    invoke-static {p1}, Lcom/yubico/yubikit/core/util/Result;->O8(Ljava/lang/Object;)Lcom/yubico/yubikit/core/util/Result;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-interface {p2, v0}, Lcom/yubico/yubikit/core/util/Callback;->invoke(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 12
    .line 13
    .line 14
    if-eqz p1, :cond_1

    .line 15
    .line 16
    :try_start_2
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 17
    .line 18
    .line 19
    goto :goto_1

    .line 20
    :catchall_0
    move-exception v0

    .line 21
    if-eqz p1, :cond_0

    .line 22
    .line 23
    :try_start_3
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :catchall_1
    move-exception p1

    .line 28
    :try_start_4
    invoke-virtual {v0, p1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .line 29
    .line 30
    .line 31
    :cond_0
    :goto_0
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 32
    :catch_0
    move-exception p1

    .line 33
    invoke-static {p1}, Lcom/yubico/yubikit/core/util/Result;->〇080(Ljava/lang/Throwable;)Lcom/yubico/yubikit/core/util/Result;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-interface {p2, p1}, Lcom/yubico/yubikit/core/util/Callback;->invoke(Ljava/lang/Object;)V

    .line 38
    .line 39
    .line 40
    :cond_1
    :goto_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic 〇O8o08O()Lcom/yubico/yubikit/core/util/Callback;
    .locals 1

    .line 1
    sget-object v0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇0O:Lcom/yubico/yubikit/core/util/Callback;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private static synthetic 〇O〇(Lcom/yubico/yubikit/core/util/Callback;Lcom/yubico/yubikit/core/util/Result;)V
    .locals 0

    .line 1
    invoke-interface {p0, p1}, Lcom/yubico/yubikit/core/util/Callback;->invoke(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static synthetic 〇〇888(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;Ljava/lang/Class;Lcom/yubico/yubikit/core/util/Callback;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇O00(Ljava/lang/Class;Lcom/yubico/yubikit/core/util/Callback;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private static synthetic 〇〇8O0〇8(Lcom/yubico/yubikit/core/util/Result;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public O〇8O8〇008(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/yubico/yubikit/core/YubiKeyConnection;",
            ">;)Z"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇OOo8〇0:Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;->Oo08(Ljava/lang/Class;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public close()V
    .locals 2

    .line 1
    const-string v0, "Closing YubiKey device"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/yubico/yubikit/core/Logger;->〇080(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->O8o08O8O:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;->close()V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    iput-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->O8o08O8O:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;

    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇080OO8〇0:Ljava/lang/Runnable;

    .line 17
    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    iget-object v1, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->o0:Ljava/util/concurrent/ExecutorService;

    .line 21
    .line 22
    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 23
    .line 24
    .line 25
    :cond_1
    iget-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->o0:Ljava/util/concurrent/ExecutorService;

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public o800o8O(Ljava/lang/Class;Lcom/yubico/yubikit/core/util/Callback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/yubico/yubikit/core/YubiKeyConnection;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Result<",
            "TT;",
            "Ljava/io/IOException;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇〇808〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    invoke-virtual {p0, p1}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->O〇8O8〇008(Ljava/lang/Class;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_3

    .line 12
    .line 13
    const-class v0, Lcom/yubico/yubikit/core/otp/OtpConnection;

    .line 14
    .line 15
    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    const/4 v1, 0x0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    new-instance p1, L〇o8〇〇/〇080;

    .line 23
    .line 24
    invoke-direct {p1, p2}, L〇o8〇〇/〇080;-><init>(Lcom/yubico/yubikit/core/util/Callback;)V

    .line 25
    .line 26
    .line 27
    iget-object p2, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->O8o08O8O:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;

    .line 28
    .line 29
    if-nez p2, :cond_0

    .line 30
    .line 31
    new-instance p2, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;

    .line 32
    .line 33
    invoke-direct {p2, p0, p1, v1}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;-><init>(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;Lcom/yubico/yubikit/core/util/Callback;Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$1;)V

    .line 34
    .line 35
    .line 36
    iput-object p2, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->O8o08O8O:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    invoke-static {p2}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;->〇〇888(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;)Ljava/util/concurrent/LinkedBlockingQueue;

    .line 40
    .line 41
    .line 42
    move-result-object p2

    .line 43
    invoke-virtual {p2, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->O8o08O8O:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;

    .line 48
    .line 49
    if-eqz v0, :cond_2

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;->close()V

    .line 52
    .line 53
    .line 54
    iput-object v1, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->O8o08O8O:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice$CachedOtpConnection;

    .line 55
    .line 56
    :cond_2
    iget-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->o0:Ljava/util/concurrent/ExecutorService;

    .line 57
    .line 58
    new-instance v1, L〇o8〇〇/〇o00〇〇Oo;

    .line 59
    .line 60
    invoke-direct {v1, p0, p1, p2}, L〇o8〇〇/〇o00〇〇Oo;-><init>(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;Ljava/lang/Class;Lcom/yubico/yubikit/core/util/Callback;)V

    .line 61
    .line 62
    .line 63
    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 64
    .line 65
    .line 66
    :goto_0
    return-void

    .line 67
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 68
    .line 69
    const-string p2, "Unsupported connection type"

    .line 70
    .line 71
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    throw p1

    .line 75
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 76
    .line 77
    const-string p2, "Device access not permitted"

    .line 78
    .line 79
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    throw p1
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public 〇oo〇(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->o0:Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isTerminated()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iput-object p1, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇080OO8〇0:Ljava/lang/Runnable;

    .line 14
    .line 15
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇〇808〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->OO:Landroid/hardware/usb/UsbManager;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;->〇08O〇00〇o:Landroid/hardware/usb/UsbDevice;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

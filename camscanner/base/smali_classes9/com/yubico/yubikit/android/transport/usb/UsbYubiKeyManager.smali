.class public Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;
.super Ljava/lang/Object;
.source "UsbYubiKeyManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager$MyDeviceListener;
    }
.end annotation


# instance fields
.field private final 〇080:Landroid/content/Context;

.field private final 〇o00〇〇Oo:Landroid/hardware/usb/UsbManager;

.field private 〇o〇:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager$MyDeviceListener;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/yubico/yubikit/android/transport/usb/connection/SmartCardConnectionHandler;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/yubico/yubikit/android/transport/usb/connection/SmartCardConnectionHandler;-><init>()V

    .line 4
    .line 5
    .line 6
    const-class v1, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection;

    .line 7
    .line 8
    invoke-static {v1, v0}, Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;->O8(Ljava/lang/Class;Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionHandler;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/yubico/yubikit/android/transport/usb/connection/OtpConnectionHandler;

    .line 12
    .line 13
    invoke-direct {v0}, Lcom/yubico/yubikit/android/transport/usb/connection/OtpConnectionHandler;-><init>()V

    .line 14
    .line 15
    .line 16
    const-class v1, Lcom/yubico/yubikit/android/transport/usb/connection/UsbOtpConnection;

    .line 17
    .line 18
    invoke-static {v1, v0}, Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionManager;->O8(Ljava/lang/Class;Lcom/yubico/yubikit/android/transport/usb/connection/ConnectionHandler;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->〇o〇:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager$MyDeviceListener;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->〇080:Landroid/content/Context;

    .line 8
    .line 9
    const-string/jumbo v0, "usb"

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    check-cast p1, Landroid/hardware/usb/UsbManager;

    .line 17
    .line 18
    iput-object p1, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->〇o00〇〇Oo:Landroid/hardware/usb/UsbManager;

    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇080(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;)Landroid/hardware/usb/UsbManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->〇o00〇〇Oo:Landroid/hardware/usb/UsbManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇o00〇〇Oo(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->〇080:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇o〇(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;)Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager$MyDeviceListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->〇o〇:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager$MyDeviceListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public declared-synchronized O8()V
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->〇o〇:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager$MyDeviceListener;

    .line 3
    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iget-object v1, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->〇080:Landroid/content/Context;

    .line 7
    .line 8
    invoke-static {v1, v0}, Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;->OO0o〇〇(Landroid/content/Context;Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager$UsbDeviceListener;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->〇o〇:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager$MyDeviceListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    .line 14
    :cond_0
    monitor-exit p0

    .line 15
    return-void

    .line 16
    :catchall_0
    move-exception v0

    .line 17
    monitor-exit p0

    .line 18
    throw v0
    .line 19
.end method

.method public declared-synchronized Oo08(Lcom/yubico/yubikit/android/transport/usb/UsbConfiguration;Lcom/yubico/yubikit/core/util/Callback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yubico/yubikit/android/transport/usb/UsbConfiguration;",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "-",
            "Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;",
            ">;)V"
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-virtual {p0}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->O8()V

    .line 3
    .line 4
    .line 5
    new-instance v0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager$MyDeviceListener;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-direct {v0, p0, p1, p2, v1}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager$MyDeviceListener;-><init>(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;Lcom/yubico/yubikit/android/transport/usb/UsbConfiguration;Lcom/yubico/yubikit/core/util/Callback;Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager$1;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->〇o〇:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager$MyDeviceListener;

    .line 12
    .line 13
    iget-object p1, p0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->〇080:Landroid/content/Context;

    .line 14
    .line 15
    invoke-static {p1, v0}, Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager;->〇80〇808〇O(Landroid/content/Context;Lcom/yubico/yubikit/android/transport/usb/UsbDeviceManager$UsbDeviceListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 16
    .line 17
    .line 18
    monitor-exit p0

    .line 19
    return-void

    .line 20
    :catchall_0
    move-exception p1

    .line 21
    monitor-exit p0

    .line 22
    throw p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

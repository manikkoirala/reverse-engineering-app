.class Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;
.super Ljava/lang/Object;
.source "UsbSmartCardConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageHeader"
.end annotation


# static fields
.field private static final oO80:[B


# instance fields
.field private O8:B

.field private Oo08:B

.field private o〇0:B

.field private 〇080:B

.field private 〇o00〇〇Oo:I

.field private 〇o〇:B

.field private 〇〇888:B
    .annotation build Ledu/umd/cs/findbugs/annotations/SuppressFBWarnings;
        value = {
            "URF_UNREAD_FIELD"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x3

    .line 2
    new-array v0, v0, [B

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->oO80:[B

    .line 8
    .line 9
    return-void

    .line 10
    nop

    .line 11
    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
    .end array-data
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private constructor <init>(BIB)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-byte p1, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇080:B

    .line 15
    iput p2, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇o00〇〇Oo:I

    const/4 p1, 0x0

    .line 16
    iput-byte p1, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇o〇:B

    .line 17
    iput-byte p3, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->O8:B

    return-void
.end method

.method synthetic constructor <init>(BIBLcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;-><init>(BIB)V

    return-void
.end method

.method private constructor <init>([B)V
    .locals 2

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    array-length v0, p1

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    .line 5
    invoke-static {p1, v0, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object p1

    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object p1

    .line 6
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    iput-byte v0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇080:B

    .line 7
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇o00〇〇Oo:I

    .line 8
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    iput-byte v0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇o〇:B

    .line 9
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    iput-byte v0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->O8:B

    .line 10
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    iput-byte v0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->Oo08:B

    .line 11
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    iput-byte v0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->o〇0:B

    .line 12
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result p1

    iput-byte p1, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇〇888:B

    :cond_0
    return-void
.end method

.method synthetic constructor <init>([BLcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$1;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;-><init>([B)V

    return-void
.end method

.method static synthetic O8(Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;B)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇80〇808〇O(B)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic Oo08(Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;)B
    .locals 0

    .line 1
    iget-byte p0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->o〇0:B

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private oO80()I
    .locals 1

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method static synthetic o〇0(Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇080(Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;)[B
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇〇888()[B

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private 〇80〇808〇O(B)Z
    .locals 3

    .line 1
    iget-byte v0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇080:B

    .line 2
    .line 3
    const/16 v1, -0x80

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eq v0, v1, :cond_0

    .line 7
    .line 8
    return v2

    .line 9
    :cond_0
    iget-byte v0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇o〇:B

    .line 10
    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    return v2

    .line 14
    :cond_1
    iget-byte v0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->O8:B

    .line 15
    .line 16
    if-eq v0, p1, :cond_2

    .line 17
    .line 18
    return v2

    .line 19
    :cond_2
    iget-byte p1, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->Oo08:B

    .line 20
    .line 21
    if-eqz p1, :cond_3

    .line 22
    .line 23
    return v2

    .line 24
    :cond_3
    const/4 p1, 0x1

    .line 25
    return p1
.end method

.method static synthetic 〇o00〇〇Oo(Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;)I
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->oO80()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇o〇(Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;)B
    .locals 0

    .line 1
    iget-byte p0, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->Oo08:B

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private 〇〇888()[B
    .locals 2

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget-byte v1, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇080:B

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iget v1, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇o00〇〇Oo:I

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iget-byte v1, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->〇o〇:B

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iget-byte v1, p0, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->O8:B

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    sget-object v1, Lcom/yubico/yubikit/android/transport/usb/connection/UsbSmartCardConnection$MessageHeader;->oO80:[B

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

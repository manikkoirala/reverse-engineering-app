.class public final Lcom/yubico/yubikit/android/YubiKitManager;
.super Ljava/lang/Object;
.source "YubiKitManager.java"


# instance fields
.field private final 〇080:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;

.field private final 〇o00〇〇Oo:Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/yubico/yubikit/android/YubiKitManager;->〇080(Landroid/content/Context;)Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/yubico/yubikit/android/YubiKitManager;-><init>(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;)V

    return-void
.end method

.method public constructor <init>(Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;)V
    .locals 0
    .param p2    # Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/yubico/yubikit/android/YubiKitManager;->〇080:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;

    .line 4
    iput-object p2, p0, Lcom/yubico/yubikit/android/YubiKitManager;->〇o00〇〇Oo:Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;

    return-void
.end method

.method private static 〇080(Landroid/content/Context;)Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    new-instance v1, Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;

    .line 3
    .line 4
    invoke-direct {v1, p0, v0}, Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;-><init>(Landroid/content/Context;Lcom/yubico/yubikit/android/transport/nfc/NfcDispatcher;)V
    :try_end_0
    .catch Lcom/yubico/yubikit/android/transport/nfc/NfcNotAvailable; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    .line 6
    .line 7
    return-object v1

    .line 8
    :catch_0
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public O8(Landroid/app/Activity;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/android/YubiKitManager;->〇o00〇〇Oo:Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;->〇o〇(Landroid/app/Activity;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public Oo08()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/android/YubiKitManager;->〇080:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->O8()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇o00〇〇Oo(Lcom/yubico/yubikit/android/transport/nfc/NfcConfiguration;Landroid/app/Activity;Lcom/yubico/yubikit/core/util/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yubico/yubikit/android/transport/nfc/NfcConfiguration;",
            "Landroid/app/Activity;",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "-",
            "Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyDevice;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/yubico/yubikit/android/transport/nfc/NfcNotAvailable;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/android/YubiKitManager;->〇o00〇〇Oo:Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p2, p1, p3}, Lcom/yubico/yubikit/android/transport/nfc/NfcYubiKeyManager;->O8(Landroid/app/Activity;Lcom/yubico/yubikit/android/transport/nfc/NfcConfiguration;Lcom/yubico/yubikit/core/util/Callback;)V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    new-instance p1, Lcom/yubico/yubikit/android/transport/nfc/NfcNotAvailable;

    .line 10
    .line 11
    const-string p2, "NFC is not available on this device"

    .line 12
    .line 13
    const/4 p3, 0x0

    .line 14
    invoke-direct {p1, p2, p3}, Lcom/yubico/yubikit/android/transport/nfc/NfcNotAvailable;-><init>(Ljava/lang/String;Z)V

    .line 15
    .line 16
    .line 17
    throw p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public 〇o〇(Lcom/yubico/yubikit/android/transport/usb/UsbConfiguration;Lcom/yubico/yubikit/core/util/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yubico/yubikit/android/transport/usb/UsbConfiguration;",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "-",
            "Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyDevice;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/android/YubiKitManager;->〇080:Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/yubico/yubikit/android/transport/usb/UsbYubiKeyManager;->Oo08(Lcom/yubico/yubikit/android/transport/usb/UsbConfiguration;Lcom/yubico/yubikit/core/util/Callback;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.class abstract Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;
.super Ljava/security/KeyPairGeneratorSpi;
.source "PivKeyPairGeneratorSpi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi$Ec;,
        Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi$Rsa;
    }
.end annotation


# instance fields
.field private final 〇080:Lcom/yubico/yubikit/core/util/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Result<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            "Ljava/lang/Exception;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

.field 〇o〇:Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/yubico/yubikit/core/util/Callback;Lcom/yubico/yubikit/piv/KeyType$Algorithm;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Result<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            "Ljava/lang/Exception;",
            ">;>;>;",
            "Lcom/yubico/yubikit/piv/KeyType$Algorithm;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/security/KeyPairGeneratorSpi;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->〇080:Lcom/yubico/yubikit/core/util/Callback;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->〇o00〇〇Oo:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private synthetic O8(Ljava/util/concurrent/BlockingQueue;Lcom/yubico/yubikit/core/util/Result;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/yubico/yubikit/piv/jca/〇o00〇〇Oo;

    .line 2
    .line 3
    invoke-direct {v0, p0, p2}, Lcom/yubico/yubikit/piv/jca/〇o00〇〇Oo;-><init>(Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;Lcom/yubico/yubikit/core/util/Result;)V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Lcom/yubico/yubikit/core/util/Result;->〇o〇(Ljava/util/concurrent/Callable;)Lcom/yubico/yubikit/core/util/Result;

    .line 7
    .line 8
    .line 9
    move-result-object p2

    .line 10
    invoke-interface {p1, p2}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static synthetic 〇080(Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;Lcom/yubico/yubikit/core/util/Result;)Ljava/security/KeyPair;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->〇o〇(Lcom/yubico/yubikit/core/util/Result;)Ljava/security/KeyPair;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;Ljava/util/concurrent/BlockingQueue;Lcom/yubico/yubikit/core/util/Result;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->O8(Ljava/util/concurrent/BlockingQueue;Lcom/yubico/yubikit/core/util/Result;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private synthetic 〇o〇(Lcom/yubico/yubikit/core/util/Result;)Ljava/security/KeyPair;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/yubico/yubikit/core/util/Result;->〇o00〇〇Oo()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    check-cast p1, Lcom/yubico/yubikit/piv/PivSession;

    .line 6
    .line 7
    iget-object v0, p0, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->〇o〇:Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;

    .line 8
    .line 9
    iget-object v1, v0, Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;->o0:Lcom/yubico/yubikit/piv/Slot;

    .line 10
    .line 11
    iget-object v2, v0, Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;->〇OOo8〇0:Lcom/yubico/yubikit/piv/KeyType;

    .line 12
    .line 13
    iget-object v3, v0, Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;->OO:Lcom/yubico/yubikit/piv/PinPolicy;

    .line 14
    .line 15
    iget-object v0, v0, Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;->〇08O〇00〇o:Lcom/yubico/yubikit/piv/TouchPolicy;

    .line 16
    .line 17
    invoke-virtual {p1, v1, v2, v3, v0}, Lcom/yubico/yubikit/piv/PivSession;->〇O〇(Lcom/yubico/yubikit/piv/Slot;Lcom/yubico/yubikit/piv/KeyType;Lcom/yubico/yubikit/piv/PinPolicy;Lcom/yubico/yubikit/piv/TouchPolicy;)Ljava/security/PublicKey;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    iget-object v0, p0, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->〇o〇:Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;

    .line 22
    .line 23
    iget-object v1, v0, Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;->o0:Lcom/yubico/yubikit/piv/Slot;

    .line 24
    .line 25
    iget-object v2, v0, Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;->OO:Lcom/yubico/yubikit/piv/PinPolicy;

    .line 26
    .line 27
    iget-object v3, v0, Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;->〇08O〇00〇o:Lcom/yubico/yubikit/piv/TouchPolicy;

    .line 28
    .line 29
    iget-object v0, v0, Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;->o〇00O:[C

    .line 30
    .line 31
    invoke-static {p1, v1, v2, v3, v0}, Lcom/yubico/yubikit/piv/jca/PivPrivateKey;->〇o〇(Ljava/security/PublicKey;Lcom/yubico/yubikit/piv/Slot;Lcom/yubico/yubikit/piv/PinPolicy;Lcom/yubico/yubikit/piv/TouchPolicy;[C)Lcom/yubico/yubikit/piv/jca/PivPrivateKey;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    new-instance v1, Ljava/security/KeyPair;

    .line 36
    .line 37
    invoke-direct {v1, p1, v0}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V

    .line 38
    .line 39
    .line 40
    return-object v1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method


# virtual methods
.method public generateKeyPair()Ljava/security/KeyPair;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->〇o〇:Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->〇080:Lcom/yubico/yubikit/core/util/Callback;

    .line 12
    .line 13
    new-instance v2, Lcom/yubico/yubikit/piv/jca/〇080;

    .line 14
    .line 15
    invoke-direct {v2, p0, v0}, Lcom/yubico/yubikit/piv/jca/〇080;-><init>(Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;Ljava/util/concurrent/BlockingQueue;)V

    .line 16
    .line 17
    .line 18
    invoke-interface {v1, v2}, Lcom/yubico/yubikit/core/util/Callback;->invoke(Ljava/lang/Object;)V

    .line 19
    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Lcom/yubico/yubikit/core/util/Result;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/yubico/yubikit/core/util/Result;->〇o00〇〇Oo()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    check-cast v0, Ljava/security/KeyPair;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    .line 33
    return-object v0

    .line 34
    :catch_0
    move-exception v0

    .line 35
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 36
    .line 37
    const-string v2, "An error occurred when generating the key pair"

    .line 38
    .line 39
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 40
    .line 41
    .line 42
    throw v1

    .line 43
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 44
    .line 45
    const-string v1, "KeyPairGenerator not initialized!"

    .line 46
    .line 47
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    throw v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public initialize(ILjava/security/SecureRandom;)V
    .locals 0

    .line 6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Initialize with PivAlgorithmParameterSpec!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public initialize(Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    .line 1
    instance-of p2, p1, Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;

    if-eqz p2, :cond_1

    .line 2
    check-cast p1, Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;

    iput-object p1, p0, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->〇o〇:Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;

    .line 3
    iget-object p1, p1, Lcom/yubico/yubikit/piv/jca/PivAlgorithmParameterSpec;->〇OOo8〇0:Lcom/yubico/yubikit/piv/KeyType;

    iget-object p1, p1, Lcom/yubico/yubikit/piv/KeyType;->params:Lcom/yubico/yubikit/piv/KeyType$KeyParams;

    iget-object p1, p1, Lcom/yubico/yubikit/piv/KeyType$KeyParams;->〇080:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    iget-object p2, p0, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->〇o00〇〇Oo:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    if-ne p1, p2, :cond_0

    return-void

    .line 4
    :cond_0
    new-instance p1, Ljava/security/InvalidAlgorithmParameterException;

    const-string p2, "Invalid key algorithm for this KeyPairGenerator"

    invoke-direct {p1, p2}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 5
    :cond_1
    new-instance p1, Ljava/security/InvalidAlgorithmParameterException;

    const-string p2, "Must be instance of PivAlgorithmParameterSpec"

    invoke-direct {p1, p2}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

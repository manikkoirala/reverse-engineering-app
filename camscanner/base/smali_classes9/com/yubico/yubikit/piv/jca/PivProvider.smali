.class public Lcom/yubico/yubikit/piv/jca/PivProvider;
.super Ljava/security/Provider;
.source "PivProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yubico/yubikit/piv/jca/PivProvider$PivRsaCipherService;,
        Lcom/yubico/yubikit/piv/jca/PivProvider$PivRsaSignatureService;,
        Lcom/yubico/yubikit/piv/jca/PivProvider$PivEcSignatureService;
    }
.end annotation


# static fields
.field private static final ecAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final rsaAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final rsaDummyKeys:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/yubico/yubikit/piv/KeyType;",
            "Ljava/security/KeyPair;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionRequester:Lcom/yubico/yubikit/core/util/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Result<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            "Ljava/lang/Exception;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const-class v0, Lcom/yubico/yubikit/piv/jca/PivPrivateKey$EcKey;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "SupportedKeyClasses"

    .line 8
    .line 9
    invoke-static {v1, v0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    sput-object v0, Lcom/yubico/yubikit/piv/jca/PivProvider;->ecAttributes:Ljava/util/Map;

    .line 14
    .line 15
    const-class v0, Lcom/yubico/yubikit/piv/jca/PivPrivateKey$RsaKey;

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-static {v1, v0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    sput-object v0, Lcom/yubico/yubikit/piv/jca/PivProvider;->rsaAttributes:Ljava/util/Map;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public constructor <init>(Lcom/yubico/yubikit/core/util/Callback;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Result<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            "Ljava/lang/Exception;",
            ">;>;>;)V"
        }
    .end annotation

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-string v2, "JCA Provider for YubiKey PIV"

    const-string v3, "YKPiv"

    .line 2
    invoke-direct {p0, v3, v0, v1, v2}, Ljava/security/Provider;-><init>(Ljava/lang/String;DLjava/lang/String;)V

    .line 3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/yubico/yubikit/piv/jca/PivProvider;->rsaDummyKeys:Ljava/util/Map;

    .line 4
    iput-object p1, p0, Lcom/yubico/yubikit/piv/jca/PivProvider;->sessionRequester:Lcom/yubico/yubikit/core/util/Callback;

    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EC "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v8, Lcom/yubico/yubikit/piv/jca/PivProvider;->ecAttributes:Ljava/util/Map;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/yubico/yubikit/core/Logger;->〇080(Ljava/lang/String;)V

    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RSA "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/yubico/yubikit/piv/jca/PivProvider;->rsaAttributes:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/yubico/yubikit/core/Logger;->〇080(Ljava/lang/String;)V

    .line 7
    new-instance v0, Lcom/yubico/yubikit/piv/jca/PivProvider$1;

    const-string v4, "Signature"

    const-string v5, "NONEwithECDSA"

    const-class v1, Lcom/yubico/yubikit/piv/jca/PivEcSignatureSpi$Prehashed;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v3, p0

    move-object v9, p1

    invoke-direct/range {v1 .. v9}, Lcom/yubico/yubikit/piv/jca/PivProvider$1;-><init>(Lcom/yubico/yubikit/piv/jca/PivProvider;Ljava/security/Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Lcom/yubico/yubikit/core/util/Callback;)V

    invoke-virtual {p0, v0}, Ljava/security/Provider;->putService(Ljava/security/Provider$Service;)V

    const/4 v1, 0x0

    :try_start_0
    const-string v0, "RSA"

    .line 8
    invoke-static {v0}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v0

    .line 9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v4, 0x2

    new-array v5, v4, [Lcom/yubico/yubikit/piv/KeyType;

    .line 10
    sget-object v6, Lcom/yubico/yubikit/piv/KeyType;->RSA1024:Lcom/yubico/yubikit/piv/KeyType;

    aput-object v6, v5, v1

    sget-object v6, Lcom/yubico/yubikit/piv/KeyType;->RSA2048:Lcom/yubico/yubikit/piv/KeyType;

    const/4 v7, 0x1

    aput-object v6, v5, v7

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v4, :cond_0

    aget-object v7, v5, v6

    .line 11
    iget-object v8, v7, Lcom/yubico/yubikit/piv/KeyType;->params:Lcom/yubico/yubikit/piv/KeyType$KeyParams;

    iget v8, v8, Lcom/yubico/yubikit/piv/KeyType$KeyParams;->〇o00〇〇Oo:I

    invoke-virtual {v0, v8}, Ljava/security/KeyPairGenerator;->initialize(I)V

    .line 12
    iget-object v8, p0, Lcom/yubico/yubikit/piv/jca/PivProvider;->rsaDummyKeys:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v9

    invoke-interface {v8, v7, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 13
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TIME TAKEN: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sub-long/2addr v4, v2

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/yubico/yubikit/core/Logger;->〇080(Ljava/lang/String;)V

    .line 15
    new-instance v0, Lcom/yubico/yubikit/piv/jca/PivProvider$PivRsaCipherService;

    invoke-direct {v0, p0}, Lcom/yubico/yubikit/piv/jca/PivProvider$PivRsaCipherService;-><init>(Lcom/yubico/yubikit/piv/jca/PivProvider;)V

    invoke-virtual {p0, v0}, Ljava/security/Provider;->putService(Ljava/security/Provider$Service;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v2, "Unable to support RSA, no underlying Provider with RSA capability"

    .line 16
    invoke-static {v2, v0}, Lcom/yubico/yubikit/core/Logger;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    const-string v0, "MessageDigest"

    .line 17
    invoke-static {v0}, Ljava/security/Security;->getAlgorithms(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    const-string v2, "Signature"

    .line 18
    invoke-static {v2}, Ljava/security/Security;->getAlgorithms(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 19
    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "WITHECDSA"

    .line 20
    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 21
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x9

    invoke-virtual {v3, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 22
    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "SHA"

    const-string v6, "SHA-"

    .line 23
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 24
    :cond_2
    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 25
    new-instance v5, Lcom/yubico/yubikit/piv/jca/PivProvider$PivEcSignatureService;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v3, v4, v6}, Lcom/yubico/yubikit/piv/jca/PivProvider$PivEcSignatureService;-><init>(Lcom/yubico/yubikit/piv/jca/PivProvider;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0, v5}, Ljava/security/Provider;->putService(Ljava/security/Provider$Service;)V

    goto :goto_2

    .line 26
    :cond_3
    iget-object v4, p0, Lcom/yubico/yubikit/piv/jca/PivProvider;->rsaDummyKeys:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "WITHRSA"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 27
    new-instance v4, Lcom/yubico/yubikit/piv/jca/PivProvider$PivRsaSignatureService;

    invoke-direct {v4, p0, v3}, Lcom/yubico/yubikit/piv/jca/PivProvider$PivRsaSignatureService;-><init>(Lcom/yubico/yubikit/piv/jca/PivProvider;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljava/security/Provider;->putService(Ljava/security/Provider$Service;)V

    goto :goto_2

    .line 28
    :cond_4
    iget-object v4, p0, Lcom/yubico/yubikit/piv/jca/PivProvider;->rsaDummyKeys:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "PSS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 29
    new-instance v4, Lcom/yubico/yubikit/piv/jca/PivProvider$PivRsaSignatureService;

    invoke-direct {v4, p0, v3}, Lcom/yubico/yubikit/piv/jca/PivProvider$PivRsaSignatureService;-><init>(Lcom/yubico/yubikit/piv/jca/PivProvider;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljava/security/Provider;->putService(Ljava/security/Provider$Service;)V

    goto :goto_2

    :cond_5
    const-string v4, "ECDSA"

    .line 30
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 31
    new-instance v3, Lcom/yubico/yubikit/piv/jca/PivProvider$PivEcSignatureService;

    const-string v5, "SHA1withECDSA"

    invoke-static {v5}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    const-string v6, "SHA-1"

    invoke-direct {v3, p0, v4, v6, v5}, Lcom/yubico/yubikit/piv/jca/PivProvider$PivEcSignatureService;-><init>(Lcom/yubico/yubikit/piv/jca/PivProvider;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0, v3}, Ljava/security/Provider;->putService(Ljava/security/Provider$Service;)V

    goto/16 :goto_2

    .line 32
    :cond_6
    new-instance v0, Lcom/yubico/yubikit/piv/jca/PivProvider$2;

    const-string v4, "KeyPairGenerator"

    const-string v5, "YKPivRSA"

    const-class v1, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi$Rsa;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v3, p0

    move-object v9, p1

    invoke-direct/range {v1 .. v9}, Lcom/yubico/yubikit/piv/jca/PivProvider$2;-><init>(Lcom/yubico/yubikit/piv/jca/PivProvider;Ljava/security/Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Lcom/yubico/yubikit/core/util/Callback;)V

    invoke-virtual {p0, v0}, Ljava/security/Provider;->putService(Ljava/security/Provider$Service;)V

    .line 33
    new-instance v0, Lcom/yubico/yubikit/piv/jca/PivProvider$3;

    const-string v4, "KeyPairGenerator"

    const-string v5, "YKPivEC"

    const-class v1, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi$Ec;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/yubico/yubikit/piv/jca/PivProvider$3;-><init>(Lcom/yubico/yubikit/piv/jca/PivProvider;Ljava/security/Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Lcom/yubico/yubikit/core/util/Callback;)V

    invoke-virtual {p0, v0}, Ljava/security/Provider;->putService(Ljava/security/Provider$Service;)V

    .line 34
    new-instance v0, Lcom/yubico/yubikit/piv/jca/PivProvider$4;

    const-string v4, "KeyStore"

    const-string v5, "YKPiv"

    const-class v1, Lcom/yubico/yubikit/piv/jca/PivKeyStoreSpi;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/yubico/yubikit/piv/jca/PivProvider$4;-><init>(Lcom/yubico/yubikit/piv/jca/PivProvider;Ljava/security/Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Lcom/yubico/yubikit/core/util/Callback;)V

    invoke-virtual {p0, v0}, Ljava/security/Provider;->putService(Ljava/security/Provider$Service;)V

    .line 35
    new-instance v0, Lcom/yubico/yubikit/piv/jca/PivProvider$5;

    const-string v4, "KeyAgreement"

    const-string v5, "ECDH"

    const-class v1, Lcom/yubico/yubikit/piv/jca/PivKeyAgreementSpi;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    sget-object v8, Lcom/yubico/yubikit/piv/jca/PivProvider;->ecAttributes:Ljava/util/Map;

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/yubico/yubikit/piv/jca/PivProvider$5;-><init>(Lcom/yubico/yubikit/piv/jca/PivProvider;Ljava/security/Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Lcom/yubico/yubikit/core/util/Callback;)V

    invoke-virtual {p0, v0}, Ljava/security/Provider;->putService(Ljava/security/Provider$Service;)V

    return-void
.end method

.method public constructor <init>(Lcom/yubico/yubikit/piv/PivSession;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/yubico/yubikit/piv/jca/〇〇8O0〇8;

    invoke-direct {v0, p1}, Lcom/yubico/yubikit/piv/jca/〇〇8O0〇8;-><init>(Lcom/yubico/yubikit/piv/PivSession;)V

    invoke-direct {p0, v0}, Lcom/yubico/yubikit/piv/jca/PivProvider;-><init>(Lcom/yubico/yubikit/core/util/Callback;)V

    return-void
.end method

.method static synthetic O8(Lcom/yubico/yubikit/piv/jca/PivProvider;)Lcom/yubico/yubikit/core/util/Callback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/yubico/yubikit/piv/jca/PivProvider;->sessionRequester:Lcom/yubico/yubikit/core/util/Callback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private static synthetic lambda$new$0(Lcom/yubico/yubikit/piv/PivSession;Lcom/yubico/yubikit/core/util/Callback;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/yubico/yubikit/core/util/Result;->O8(Ljava/lang/Object;)Lcom/yubico/yubikit/core/util/Result;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-interface {p1, p0}, Lcom/yubico/yubikit/core/util/Callback;->invoke(Ljava/lang/Object;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic o〇0()Ljava/util/Map;
    .locals 1

    .line 1
    sget-object v0, Lcom/yubico/yubikit/piv/jca/PivProvider;->rsaAttributes:Ljava/util/Map;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static synthetic 〇080(Lcom/yubico/yubikit/piv/PivSession;Lcom/yubico/yubikit/core/util/Callback;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/yubico/yubikit/piv/jca/PivProvider;->lambda$new$0(Lcom/yubico/yubikit/piv/PivSession;Lcom/yubico/yubikit/core/util/Callback;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic 〇o〇()Ljava/util/Map;
    .locals 1

    .line 1
    sget-object v0, Lcom/yubico/yubikit/piv/jca/PivProvider;->ecAttributes:Ljava/util/Map;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method static synthetic 〇〇888(Lcom/yubico/yubikit/piv/jca/PivProvider;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/yubico/yubikit/piv/jca/PivProvider;->rsaDummyKeys:Ljava/util/Map;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public declared-synchronized equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    instance-of v0, p1, Lcom/yubico/yubikit/piv/jca/PivProvider;

    .line 3
    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-super {p0, p1}, Ljava/security/Provider;->equals(Ljava/lang/Object;)Z

    .line 7
    .line 8
    .line 9
    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    const/4 p1, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 p1, 0x0

    .line 15
    :goto_0
    monitor-exit p0

    .line 16
    return p1

    .line 17
    :catchall_0
    move-exception p1

    .line 18
    monitor-exit p0

    .line 19
    throw p1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public declared-synchronized hashCode()I
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-super {p0}, Ljava/security/Provider;->hashCode()I

    .line 3
    .line 4
    .line 5
    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    monitor-exit p0

    .line 7
    return v0

    .line 8
    :catchall_0
    move-exception v0

    .line 9
    monitor-exit p0

    .line 10
    throw v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.class public Lcom/yubico/yubikit/piv/jca/PivEcSignatureSpi$Hashed;
.super Lcom/yubico/yubikit/piv/jca/PivEcSignatureSpi;
.source "PivEcSignatureSpi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yubico/yubikit/piv/jca/PivEcSignatureSpi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Hashed"
.end annotation


# instance fields
.field private final 〇o〇:Ljava/security/MessageDigest;


# direct methods
.method constructor <init>(Lcom/yubico/yubikit/core/util/Callback;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Result<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            "Ljava/lang/Exception;",
            ">;>;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/yubico/yubikit/piv/jca/PivEcSignatureSpi;-><init>(Lcom/yubico/yubikit/core/util/Callback;)V

    .line 2
    .line 3
    .line 4
    invoke-static {p2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    iput-object p1, p0, Lcom/yubico/yubikit/piv/jca/PivEcSignatureSpi$Hashed;->〇o〇:Ljava/security/MessageDigest;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method protected engineInitSign(Ljava/security/PrivateKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 1
    invoke-super {p0, p1}, Lcom/yubico/yubikit/piv/jca/PivEcSignatureSpi;->engineInitSign(Ljava/security/PrivateKey;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/yubico/yubikit/piv/jca/PivEcSignatureSpi$Hashed;->〇o〇:Ljava/security/MessageDigest;

    .line 5
    .line 6
    invoke-virtual {p1}, Ljava/security/MessageDigest;->reset()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method protected update(B)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/piv/jca/PivEcSignatureSpi$Hashed;->〇o〇:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update(B)V

    return-void
.end method

.method protected update([BII)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/yubico/yubikit/piv/jca/PivEcSignatureSpi$Hashed;->〇o〇:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1, p2, p3}, Ljava/security/MessageDigest;->update([BII)V

    return-void
.end method

.method protected 〇080()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/piv/jca/PivEcSignatureSpi$Hashed;->〇o〇:Ljava/security/MessageDigest;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

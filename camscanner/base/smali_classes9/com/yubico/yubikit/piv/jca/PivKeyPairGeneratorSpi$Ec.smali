.class public Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi$Ec;
.super Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;
.source "PivKeyPairGeneratorSpi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Ec"
.end annotation


# direct methods
.method constructor <init>(Lcom/yubico/yubikit/core/util/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Callback<",
            "Lcom/yubico/yubikit/core/util/Result<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            "Ljava/lang/Exception;",
            ">;>;>;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/yubico/yubikit/piv/KeyType$Algorithm;->EC:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;-><init>(Lcom/yubico/yubikit/core/util/Callback;Lcom/yubico/yubikit/piv/KeyType$Algorithm;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public bridge synthetic generateKeyPair()Ljava/security/KeyPair;
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->generateKeyPair()Ljava/security/KeyPair;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public bridge synthetic initialize(ILjava/security/SecureRandom;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->initialize(ILjava/security/SecureRandom;)V

    return-void
.end method

.method public bridge synthetic initialize(Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    .line 2
    invoke-super {p0, p1, p2}, Lcom/yubico/yubikit/piv/jca/PivKeyPairGeneratorSpi;->initialize(Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V

    return-void
.end method

.class public Lcom/yubico/yubikit/piv/PivSession;
.super Lcom/yubico/yubikit/core/application/ApplicationSession;
.source "PivSession.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/yubico/yubikit/core/application/ApplicationSession<",
        "Lcom/yubico/yubikit/piv/PivSession;",
        ">;"
    }
.end annotation


# static fields
.field public static final O8o08O8O:Lcom/yubico/yubikit/core/application/Feature;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yubico/yubikit/core/application/Feature<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            ">;"
        }
    .end annotation
.end field

.field public static final OO〇00〇8oO:Lcom/yubico/yubikit/core/application/Feature;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yubico/yubikit/core/application/Feature<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            ">;"
        }
    .end annotation
.end field

.field public static final o8〇OO0〇0o:Lcom/yubico/yubikit/core/application/Feature;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yubico/yubikit/core/application/Feature<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            ">;"
        }
    .end annotation
.end field

.field public static final oOo0:Lcom/yubico/yubikit/core/application/Feature;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yubico/yubikit/core/application/Feature<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            ">;"
        }
    .end annotation
.end field

.field public static final oOo〇8o008:Lcom/yubico/yubikit/core/application/Feature;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yubico/yubikit/core/application/Feature<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            ">;"
        }
    .end annotation
.end field

.field private static final ooo0〇〇O:[B

.field public static final o〇00O:Lcom/yubico/yubikit/core/application/Feature;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yubico/yubikit/core/application/Feature<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇080OO8〇0:Lcom/yubico/yubikit/core/application/Feature;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yubico/yubikit/core/application/Feature<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇0O:Lcom/yubico/yubikit/core/application/Feature;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yubico/yubikit/core/application/Feature<",
            "Lcom/yubico/yubikit/piv/PivSession;",
            ">;"
        }
    .end annotation
.end field

.field private static final 〇8〇oO〇〇8o:[B

.field private static final 〇〇08O:[B


# instance fields
.field private OO:I

.field private final o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

.field private 〇08O〇00〇o:I

.field private final 〇OOo8〇0:Lcom/yubico/yubikit/core/Version;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    new-instance v0, Lcom/yubico/yubikit/core/application/Feature$Versioned;

    .line 2
    .line 3
    const-string v1, "Curve P384"

    .line 4
    .line 5
    const/4 v2, 0x4

    .line 6
    const/4 v3, 0x0

    .line 7
    invoke-direct {v0, v1, v2, v3, v3}, Lcom/yubico/yubikit/core/application/Feature$Versioned;-><init>(Ljava/lang/String;III)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/yubico/yubikit/piv/PivSession;->o〇00O:Lcom/yubico/yubikit/core/application/Feature;

    .line 11
    .line 12
    new-instance v0, Lcom/yubico/yubikit/core/application/Feature$Versioned;

    .line 13
    .line 14
    const-string v1, "PIN/Touch Policy"

    .line 15
    .line 16
    invoke-direct {v0, v1, v2, v3, v3}, Lcom/yubico/yubikit/core/application/Feature$Versioned;-><init>(Ljava/lang/String;III)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/yubico/yubikit/piv/PivSession;->O8o08O8O:Lcom/yubico/yubikit/core/application/Feature;

    .line 20
    .line 21
    new-instance v0, Lcom/yubico/yubikit/core/application/Feature$Versioned;

    .line 22
    .line 23
    const-string v1, "Cached Touch Policy"

    .line 24
    .line 25
    const/4 v4, 0x3

    .line 26
    invoke-direct {v0, v1, v2, v4, v3}, Lcom/yubico/yubikit/core/application/Feature$Versioned;-><init>(Ljava/lang/String;III)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/yubico/yubikit/piv/PivSession;->〇080OO8〇0:Lcom/yubico/yubikit/core/application/Feature;

    .line 30
    .line 31
    new-instance v0, Lcom/yubico/yubikit/core/application/Feature$Versioned;

    .line 32
    .line 33
    const-string v1, "Attestation"

    .line 34
    .line 35
    invoke-direct {v0, v1, v2, v4, v3}, Lcom/yubico/yubikit/core/application/Feature$Versioned;-><init>(Ljava/lang/String;III)V

    .line 36
    .line 37
    .line 38
    sput-object v0, Lcom/yubico/yubikit/piv/PivSession;->〇0O:Lcom/yubico/yubikit/core/application/Feature;

    .line 39
    .line 40
    new-instance v0, Lcom/yubico/yubikit/core/application/Feature$Versioned;

    .line 41
    .line 42
    const-string v1, "Serial Number"

    .line 43
    .line 44
    const/4 v5, 0x5

    .line 45
    invoke-direct {v0, v1, v5, v3, v3}, Lcom/yubico/yubikit/core/application/Feature$Versioned;-><init>(Ljava/lang/String;III)V

    .line 46
    .line 47
    .line 48
    sput-object v0, Lcom/yubico/yubikit/piv/PivSession;->oOo〇8o008:Lcom/yubico/yubikit/core/application/Feature;

    .line 49
    .line 50
    new-instance v0, Lcom/yubico/yubikit/core/application/Feature$Versioned;

    .line 51
    .line 52
    const-string v1, "Metadata"

    .line 53
    .line 54
    invoke-direct {v0, v1, v5, v4, v3}, Lcom/yubico/yubikit/core/application/Feature$Versioned;-><init>(Ljava/lang/String;III)V

    .line 55
    .line 56
    .line 57
    sput-object v0, Lcom/yubico/yubikit/piv/PivSession;->oOo0:Lcom/yubico/yubikit/core/application/Feature;

    .line 58
    .line 59
    new-instance v0, Lcom/yubico/yubikit/core/application/Feature$Versioned;

    .line 60
    .line 61
    const-string v1, "AES Management Key"

    .line 62
    .line 63
    invoke-direct {v0, v1, v5, v2, v3}, Lcom/yubico/yubikit/core/application/Feature$Versioned;-><init>(Ljava/lang/String;III)V

    .line 64
    .line 65
    .line 66
    sput-object v0, Lcom/yubico/yubikit/piv/PivSession;->OO〇00〇8oO:Lcom/yubico/yubikit/core/application/Feature;

    .line 67
    .line 68
    new-instance v0, Lcom/yubico/yubikit/piv/PivSession$1;

    .line 69
    .line 70
    const-string v1, "RSA key generation"

    .line 71
    .line 72
    invoke-direct {v0, v1}, Lcom/yubico/yubikit/piv/PivSession$1;-><init>(Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    sput-object v0, Lcom/yubico/yubikit/piv/PivSession;->o8〇OO0〇0o:Lcom/yubico/yubikit/core/application/Feature;

    .line 76
    .line 77
    new-array v0, v5, [B

    .line 78
    .line 79
    fill-array-data v0, :array_0

    .line 80
    .line 81
    .line 82
    sput-object v0, Lcom/yubico/yubikit/piv/PivSession;->〇8〇oO〇〇8o:[B

    .line 83
    .line 84
    const/16 v0, 0x1a

    .line 85
    .line 86
    new-array v0, v0, [B

    .line 87
    .line 88
    fill-array-data v0, :array_1

    .line 89
    .line 90
    .line 91
    sput-object v0, Lcom/yubico/yubikit/piv/PivSession;->ooo0〇〇O:[B

    .line 92
    .line 93
    const/16 v0, 0x17

    .line 94
    .line 95
    new-array v0, v0, [B

    .line 96
    .line 97
    fill-array-data v0, :array_2

    .line 98
    .line 99
    .line 100
    sput-object v0, Lcom/yubico/yubikit/piv/PivSession;->〇〇08O:[B

    .line 101
    .line 102
    return-void

    .line 103
    :array_0
    .array-data 1
        -0x60t
        0x0t
        0x0t
        0x3t
        0x8t
    .end array-data

    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    nop

    .line 111
    :array_1
    .array-data 1
        0x30t
        0x59t
        0x30t
        0x13t
        0x6t
        0x7t
        0x2at
        -0x7at
        0x48t
        -0x32t
        0x3dt
        0x2t
        0x1t
        0x6t
        0x8t
        0x2at
        -0x7at
        0x48t
        -0x32t
        0x3dt
        0x3t
        0x1t
        0x7t
        0x3t
        0x42t
        0x0t
    .end array-data

    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    nop

    .line 129
    :array_2
    .array-data 1
        0x30t
        0x76t
        0x30t
        0x10t
        0x6t
        0x7t
        0x2at
        -0x7at
        0x48t
        -0x32t
        0x3dt
        0x2t
        0x1t
        0x6t
        0x5t
        0x2bt
        -0x7ft
        0x4t
        0x0t
        0x22t
        0x3t
        0x62t
        0x0t
    .end array-data
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method public constructor <init>(Lcom/yubico/yubikit/core/smartcard/SmartCardConnection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;,
            Lcom/yubico/yubikit/core/application/ApplicationNotAvailableException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/yubico/yubikit/core/application/ApplicationSession;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x3

    .line 5
    iput v0, p0, Lcom/yubico/yubikit/piv/PivSession;->OO:I

    .line 6
    .line 7
    iput v0, p0, Lcom/yubico/yubikit/piv/PivSession;->〇08O〇00〇o:I

    .line 8
    .line 9
    new-instance v0, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 10
    .line 11
    invoke-direct {v0, p1}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;-><init>(Lcom/yubico/yubikit/core/smartcard/SmartCardConnection;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/yubico/yubikit/piv/PivSession;->o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 15
    .line 16
    sget-object v1, Lcom/yubico/yubikit/piv/PivSession;->〇8〇oO〇〇8o:[B

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->〇8o8o〇([B)[B

    .line 19
    .line 20
    .line 21
    new-instance v1, Lcom/yubico/yubikit/core/smartcard/Apdu;

    .line 22
    .line 23
    const/4 v3, 0x0

    .line 24
    const/4 v4, -0x3

    .line 25
    const/4 v5, 0x0

    .line 26
    const/4 v6, 0x0

    .line 27
    const/4 v7, 0x0

    .line 28
    move-object v2, v1

    .line 29
    invoke-direct/range {v2 .. v7}, Lcom/yubico/yubikit/core/smartcard/Apdu;-><init>(IIII[B)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v1}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->〇O8o08O(Lcom/yubico/yubikit/core/smartcard/Apdu;)[B

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-static {v1}, Lcom/yubico/yubikit/core/Version;->O8([B)Lcom/yubico/yubikit/core/Version;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    iput-object v1, p0, Lcom/yubico/yubikit/piv/PivSession;->〇OOo8〇0:Lcom/yubico/yubikit/core/Version;

    .line 41
    .line 42
    invoke-virtual {v0, v1}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->Oo08(Lcom/yubico/yubikit/core/Version;)V

    .line 43
    .line 44
    .line 45
    invoke-interface {p1}, Lcom/yubico/yubikit/core/smartcard/SmartCardConnection;->〇〇0o()Z

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    if-eqz p1, :cond_0

    .line 50
    .line 51
    const/4 p1, 0x4

    .line 52
    const/4 v2, 0x0

    .line 53
    invoke-virtual {v1, p1, v2, v2}, Lcom/yubico/yubikit/core/Version;->Oo08(III)Z

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    if-eqz p1, :cond_0

    .line 58
    .line 59
    sget-object p1, Lcom/yubico/yubikit/core/smartcard/ApduFormat;->EXTENDED:Lcom/yubico/yubikit/core/smartcard/ApduFormat;

    .line 60
    .line 61
    invoke-virtual {v0, p1}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->OO0o〇〇(Lcom/yubico/yubikit/core/smartcard/ApduFormat;)V

    .line 62
    .line 63
    .line 64
    :cond_0
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method static O08000(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/security/PublicKey;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/spec/InvalidKeySpecException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/yubico/yubikit/piv/KeyType$Algorithm;->RSA:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v1, Ljava/security/spec/RSAPublicKeySpec;

    .line 12
    .line 13
    invoke-direct {v1, p0, p1}, Ljava/security/spec/RSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    return-object p0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private O8〇o([B)Ljava/security/cert/X509Certificate;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/ByteArrayInputStream;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 4
    .line 5
    .line 6
    const-string p1, "X.509"

    .line 7
    .line 8
    invoke-static {p1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {p1, v0}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    check-cast p1, Ljava/security/cert/X509Certificate;

    .line 17
    .line 18
    return-object p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private O〇8O8〇008(B)Lcom/yubico/yubikit/piv/PinMetadata;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/yubico/yubikit/piv/PivSession;->oOo0:Lcom/yubico/yubikit/core/application/Feature;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/yubico/yubikit/core/application/ApplicationSession;->〇〇888(Lcom/yubico/yubikit/core/application/Feature;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/yubico/yubikit/piv/PivSession;->o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 7
    .line 8
    new-instance v7, Lcom/yubico/yubikit/core/smartcard/Apdu;

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    const/16 v3, -0x9

    .line 12
    .line 13
    const/4 v4, 0x0

    .line 14
    const/4 v6, 0x0

    .line 15
    move-object v1, v7

    .line 16
    move v5, p1

    .line 17
    invoke-direct/range {v1 .. v6}, Lcom/yubico/yubikit/core/smartcard/Apdu;-><init>(IIII[B)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v7}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->〇O8o08O(Lcom/yubico/yubikit/core/smartcard/Apdu;)[B

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-static {p1}, Lcom/yubico/yubikit/core/util/Tlvs;->〇o00〇〇Oo([B)Ljava/util/Map;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const/4 v0, 0x6

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    check-cast v0, [B

    .line 38
    .line 39
    new-instance v1, Lcom/yubico/yubikit/piv/PinMetadata;

    .line 40
    .line 41
    const/4 v2, 0x5

    .line 42
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    check-cast p1, [B

    .line 51
    .line 52
    const/4 v2, 0x0

    .line 53
    aget-byte p1, p1, v2

    .line 54
    .line 55
    const/4 v3, 0x1

    .line 56
    if-eqz p1, :cond_0

    .line 57
    .line 58
    const/4 p1, 0x1

    .line 59
    goto :goto_0

    .line 60
    :cond_0
    const/4 p1, 0x0

    .line 61
    :goto_0
    aget-byte v2, v0, v2

    .line 62
    .line 63
    aget-byte v0, v0, v3

    .line 64
    .line 65
    invoke-direct {v1, p1, v2, v0}, Lcom/yubico/yubikit/piv/PinMetadata;-><init>(ZII)V

    .line 66
    .line 67
    .line 68
    return-object v1
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private static oO([C)[B
    .locals 5

    .line 1
    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    .line 2
    .line 3
    invoke-static {p0}, Ljava/nio/CharBuffer;->wrap([C)Ljava/nio/CharBuffer;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-virtual {v0, p0}, Ljava/nio/charset/Charset;->encode(Ljava/nio/CharBuffer;)Ljava/nio/ByteBuffer;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    const/4 v0, 0x0

    .line 12
    :try_start_0
    invoke-virtual {p0}, Ljava/nio/Buffer;->limit()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    invoke-virtual {p0}, Ljava/nio/Buffer;->position()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    sub-int/2addr v1, v2

    .line 21
    const/16 v2, 0x8

    .line 22
    .line 23
    if-gt v1, v2, :cond_0

    .line 24
    .line 25
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    const/4 v4, -0x1

    .line 34
    invoke-static {v3, v1, v2, v4}, Ljava/util/Arrays;->fill([BIIB)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    invoke-static {p0, v0}, Ljava/util/Arrays;->fill([BB)V

    .line 42
    .line 43
    .line 44
    return-object v3

    .line 45
    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 46
    .line 47
    const-string v2, "PIN/PUK must be no longer than 8 bytes"

    .line 48
    .line 49
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 53
    :catchall_0
    move-exception v1

    .line 54
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    .line 55
    .line 56
    .line 57
    move-result-object p0

    .line 58
    invoke-static {p0, v0}, Ljava/util/Arrays;->fill([BB)V

    .line 59
    .line 60
    .line 61
    throw v1
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private o〇O(Lcom/yubico/yubikit/piv/Slot;Lcom/yubico/yubikit/piv/KeyType;[BZ)[B
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;,
            Lcom/yubico/yubikit/core/application/BadResponseException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/LinkedHashMap;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x82

    .line 7
    .line 8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    const/4 v3, 0x0

    .line 13
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    if-eqz p4, :cond_0

    .line 17
    .line 18
    const/16 p4, 0x85

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/16 p4, 0x81

    .line 22
    .line 23
    :goto_0
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 24
    .line 25
    .line 26
    move-result-object p4

    .line 27
    invoke-interface {v0, p4, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    new-instance p3, Lcom/yubico/yubikit/core/util/Tlv;

    .line 31
    .line 32
    invoke-static {v0}, Lcom/yubico/yubikit/core/util/Tlvs;->〇o〇(Ljava/util/Map;)[B

    .line 33
    .line 34
    .line 35
    move-result-object p4

    .line 36
    const/16 v0, 0x7c

    .line 37
    .line 38
    invoke-direct {p3, v0, p4}, Lcom/yubico/yubikit/core/util/Tlv;-><init>(I[B)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p3}, Lcom/yubico/yubikit/core/util/Tlv;->〇080()[B

    .line 42
    .line 43
    .line 44
    move-result-object v7

    .line 45
    :try_start_0
    iget-object p3, p0, Lcom/yubico/yubikit/piv/PivSession;->o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 46
    .line 47
    new-instance p4, Lcom/yubico/yubikit/core/smartcard/Apdu;

    .line 48
    .line 49
    const/4 v3, 0x0

    .line 50
    const/16 v4, -0x79

    .line 51
    .line 52
    iget-byte v5, p2, Lcom/yubico/yubikit/piv/KeyType;->value:B

    .line 53
    .line 54
    iget v6, p1, Lcom/yubico/yubikit/piv/Slot;->value:I

    .line 55
    .line 56
    move-object v2, p4

    .line 57
    invoke-direct/range {v2 .. v7}, Lcom/yubico/yubikit/core/smartcard/Apdu;-><init>(IIII[B)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p3, p4}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->〇O8o08O(Lcom/yubico/yubikit/core/smartcard/Apdu;)[B

    .line 61
    .line 62
    .line 63
    move-result-object p3

    .line 64
    invoke-static {v0, p3}, Lcom/yubico/yubikit/core/util/Tlvs;->O8(I[B)[B

    .line 65
    .line 66
    .line 67
    move-result-object p3

    .line 68
    invoke-static {v1, p3}, Lcom/yubico/yubikit/core/util/Tlvs;->O8(I[B)[B

    .line 69
    .line 70
    .line 71
    move-result-object p1
    :try_end_0
    .catch Lcom/yubico/yubikit/core/smartcard/ApduException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    return-object p1

    .line 73
    :catch_0
    move-exception p3

    .line 74
    const/16 p4, 0x6a80

    .line 75
    .line 76
    invoke-virtual {p3}, Lcom/yubico/yubikit/core/smartcard/ApduException;->getSw()S

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    if-ne p4, v0, :cond_1

    .line 81
    .line 82
    new-instance p4, Lcom/yubico/yubikit/core/smartcard/ApduException;

    .line 83
    .line 84
    invoke-virtual {p3}, Lcom/yubico/yubikit/core/smartcard/ApduException;->getSw()S

    .line 85
    .line 86
    .line 87
    move-result p3

    .line 88
    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    .line 89
    .line 90
    const/4 v1, 0x2

    .line 91
    new-array v1, v1, [Ljava/lang/Object;

    .line 92
    .line 93
    const/4 v2, 0x0

    .line 94
    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object p2

    .line 98
    aput-object p2, v1, v2

    .line 99
    .line 100
    iget p1, p1, Lcom/yubico/yubikit/piv/Slot;->value:I

    .line 101
    .line 102
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    const/4 p2, 0x1

    .line 107
    aput-object p1, v1, p2

    .line 108
    .line 109
    const-string p1, "Make sure that %s key is generated on slot %02X"

    .line 110
    .line 111
    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    invoke-direct {p4, p3, p1}, Lcom/yubico/yubikit/core/smartcard/ApduException;-><init>(SLjava/lang/String;)V

    .line 116
    .line 117
    .line 118
    throw p4

    .line 119
    :cond_1
    throw p3
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method private o〇〇0〇(I)I
    .locals 4

    .line 1
    const/16 v0, 0x6983

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-ne p1, v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/yubico/yubikit/piv/PivSession;->〇OOo8〇0:Lcom/yubico/yubikit/core/Version;

    .line 8
    .line 9
    const/4 v2, 0x1

    .line 10
    const/4 v3, 0x4

    .line 11
    invoke-virtual {v0, v2, v1, v3}, Lcom/yubico/yubikit/core/Version;->o〇0(III)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    const/16 v0, 0x6300

    .line 18
    .line 19
    if-lt p1, v0, :cond_2

    .line 20
    .line 21
    const/16 v0, 0x63ff

    .line 22
    .line 23
    if-gt p1, v0, :cond_2

    .line 24
    .line 25
    and-int/lit16 p1, p1, 0xff

    .line 26
    .line 27
    return p1

    .line 28
    :cond_1
    const/16 v0, 0x63c0

    .line 29
    .line 30
    if-lt p1, v0, :cond_2

    .line 31
    .line 32
    const/16 v0, 0x63cf

    .line 33
    .line 34
    if-gt p1, v0, :cond_2

    .line 35
    .line 36
    and-int/lit8 p1, p1, 0xf

    .line 37
    .line 38
    return p1

    .line 39
    :cond_2
    const/4 p1, -0x1

    .line 40
    return p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method static 〇00〇8([B)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .line 1
    const/16 v0, 0x30

    .line 2
    .line 3
    :try_start_0
    invoke-static {v0, p0}, Lcom/yubico/yubikit/core/util/Tlvs;->O8(I[B)[B

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-static {p0}, Lcom/yubico/yubikit/core/util/Tlvs;->〇o00〇〇Oo([B)Ljava/util/Map;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    const/4 v1, 0x4

    .line 12
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    check-cast p0, [B

    .line 21
    .line 22
    invoke-static {p0}, Lcom/yubico/yubikit/core/util/Tlvs;->〇o00〇〇Oo([B)Ljava/util/Map;

    .line 23
    .line 24
    .line 25
    move-result-object p0

    .line 26
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    check-cast p0, [B

    .line 35
    .line 36
    invoke-static {p0}, Lcom/yubico/yubikit/core/util/Tlvs;->〇080([B)Ljava/util/List;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    .line 41
    .line 42
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .line 44
    .line 45
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 46
    .line 47
    .line 48
    move-result-object p0

    .line 49
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-eqz v1, :cond_0

    .line 54
    .line 55
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    check-cast v1, Lcom/yubico/yubikit/core/util/Tlv;

    .line 60
    .line 61
    new-instance v2, Ljava/math/BigInteger;

    .line 62
    .line 63
    invoke-virtual {v1}, Lcom/yubico/yubikit/core/util/Tlv;->〇o〇()[B

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    invoke-direct {v2, v1}, Ljava/math/BigInteger;-><init>([B)V

    .line 68
    .line 69
    .line 70
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_0
    const/4 p0, 0x0

    .line 75
    invoke-interface {v0, p0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 76
    .line 77
    .line 78
    move-result-object p0

    .line 79
    check-cast p0, Ljava/math/BigInteger;

    .line 80
    .line 81
    invoke-virtual {p0}, Ljava/math/BigInteger;->intValue()I

    .line 82
    .line 83
    .line 84
    move-result p0

    .line 85
    if-nez p0, :cond_1

    .line 86
    .line 87
    return-object v0

    .line 88
    :cond_1
    new-instance p0, Ljava/io/UnsupportedEncodingException;

    .line 89
    .line 90
    const-string v0, "Expected value 0"

    .line 91
    .line 92
    invoke-direct {p0, v0}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    throw p0
    :try_end_0
    .catch Lcom/yubico/yubikit/core/application/BadResponseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :catch_0
    move-exception p0

    .line 97
    new-instance v0, Ljava/io/UnsupportedEncodingException;

    .line 98
    .line 99
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object p0

    .line 103
    invoke-direct {v0, p0}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    throw v0
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method static 〇8(Lcom/yubico/yubikit/piv/KeyType;[B)Ljava/security/PublicKey;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/spec/InvalidKeySpecException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/yubico/yubikit/piv/PivSession$2;->〇o00〇〇Oo:[I

    .line 2
    .line 3
    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    aget v0, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    if-eq v0, v1, :cond_1

    .line 11
    .line 12
    const/4 v1, 0x2

    .line 13
    if-ne v0, v1, :cond_0

    .line 14
    .line 15
    sget-object v0, Lcom/yubico/yubikit/piv/PivSession;->〇〇08O:[B

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    .line 19
    .line 20
    const-string p1, "Unsupported key type"

    .line 21
    .line 22
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    throw p0

    .line 26
    :cond_1
    sget-object v0, Lcom/yubico/yubikit/piv/PivSession;->ooo0〇〇O:[B

    .line 27
    .line 28
    :goto_0
    iget-object p0, p0, Lcom/yubico/yubikit/piv/KeyType;->params:Lcom/yubico/yubikit/piv/KeyType$KeyParams;

    .line 29
    .line 30
    iget-object p0, p0, Lcom/yubico/yubikit/piv/KeyType$KeyParams;->〇080:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    .line 31
    .line 32
    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p0

    .line 36
    invoke-static {p0}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    new-instance v1, Ljava/security/spec/X509EncodedKeySpec;

    .line 41
    .line 42
    array-length v2, v0

    .line 43
    array-length v3, p1

    .line 44
    add-int/2addr v2, v3

    .line 45
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-direct {v1, p1}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p0, v1}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    .line 65
    .line 66
    .line 67
    move-result-object p0

    .line 68
    return-object p0
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method private static 〇8o8o〇(Ljava/math/BigInteger;I)[B
    .locals 3

    .line 1
    invoke-virtual {p0}, Ljava/math/BigInteger;->toByteArray()[B

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    array-length v0, p0

    .line 6
    if-ne v0, p1, :cond_0

    .line 7
    .line 8
    return-object p0

    .line 9
    :cond_0
    array-length v0, p0

    .line 10
    if-le v0, p1, :cond_1

    .line 11
    .line 12
    array-length v0, p0

    .line 13
    sub-int/2addr v0, p1

    .line 14
    array-length p1, p0

    .line 15
    invoke-static {p0, v0, p1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    .line 16
    .line 17
    .line 18
    move-result-object p0

    .line 19
    return-object p0

    .line 20
    :cond_1
    new-array v0, p1, [B

    .line 21
    .line 22
    array-length v1, p0

    .line 23
    sub-int/2addr p1, v1

    .line 24
    array-length v1, p0

    .line 25
    const/4 v2, 0x0

    .line 26
    invoke-static {p0, v2, v0, p1, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 27
    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static 〇〇〇0〇〇0(Lcom/yubico/yubikit/piv/KeyType;[B)Ljava/security/PublicKey;
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/yubico/yubikit/core/util/Tlvs;->〇o00〇〇Oo([B)Ljava/util/Map;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    :try_start_0
    iget-object v0, p0, Lcom/yubico/yubikit/piv/KeyType;->params:Lcom/yubico/yubikit/piv/KeyType$KeyParams;

    .line 6
    .line 7
    iget-object v0, v0, Lcom/yubico/yubikit/piv/KeyType$KeyParams;->〇080:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    .line 8
    .line 9
    sget-object v1, Lcom/yubico/yubikit/piv/KeyType$Algorithm;->RSA:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    .line 10
    .line 11
    if-ne v0, v1, :cond_0

    .line 12
    .line 13
    new-instance p0, Ljava/math/BigInteger;

    .line 14
    .line 15
    const/16 v0, 0x81

    .line 16
    .line 17
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, [B

    .line 26
    .line 27
    const/4 v1, 0x1

    .line 28
    invoke-direct {p0, v1, v0}, Ljava/math/BigInteger;-><init>(I[B)V

    .line 29
    .line 30
    .line 31
    new-instance v0, Ljava/math/BigInteger;

    .line 32
    .line 33
    const/16 v2, 0x82

    .line 34
    .line 35
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    check-cast p1, [B

    .line 44
    .line 45
    invoke-direct {v0, v1, p1}, Ljava/math/BigInteger;-><init>(I[B)V

    .line 46
    .line 47
    .line 48
    invoke-static {p0, v0}, Lcom/yubico/yubikit/piv/PivSession;->O08000(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/security/PublicKey;

    .line 49
    .line 50
    .line 51
    move-result-object p0

    .line 52
    return-object p0

    .line 53
    :cond_0
    const/16 v0, 0x86

    .line 54
    .line 55
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    check-cast p1, [B

    .line 64
    .line 65
    invoke-static {p0, p1}, Lcom/yubico/yubikit/piv/PivSession;->〇8(Lcom/yubico/yubikit/piv/KeyType;[B)Ljava/security/PublicKey;

    .line 66
    .line 67
    .line 68
    move-result-object p0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    return-object p0

    .line 70
    :catch_0
    move-exception p0

    .line 71
    goto :goto_0

    .line 72
    :catch_1
    move-exception p0

    .line 73
    :goto_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 74
    .line 75
    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 76
    .line 77
    .line 78
    throw p1
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method


# virtual methods
.method public OO0o〇〇(Lcom/yubico/yubikit/piv/KeyType;Lcom/yubico/yubikit/piv/PinPolicy;Lcom/yubico/yubikit/piv/TouchPolicy;Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/piv/PivSession;->〇OOo8〇0:Lcom/yubico/yubikit/core/Version;

    .line 2
    .line 3
    iget-byte v0, v0, Lcom/yubico/yubikit/core/Version;->o0:B

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    sget-object v0, Lcom/yubico/yubikit/piv/KeyType;->ECCP384:Lcom/yubico/yubikit/piv/KeyType;

    .line 9
    .line 10
    if-ne p1, v0, :cond_1

    .line 11
    .line 12
    sget-object v0, Lcom/yubico/yubikit/piv/PivSession;->o〇00O:Lcom/yubico/yubikit/core/application/Feature;

    .line 13
    .line 14
    invoke-virtual {p0, v0}, Lcom/yubico/yubikit/core/application/ApplicationSession;->〇〇888(Lcom/yubico/yubikit/core/application/Feature;)V

    .line 15
    .line 16
    .line 17
    :cond_1
    sget-object v0, Lcom/yubico/yubikit/piv/PinPolicy;->DEFAULT:Lcom/yubico/yubikit/piv/PinPolicy;

    .line 18
    .line 19
    if-ne p2, v0, :cond_2

    .line 20
    .line 21
    sget-object v0, Lcom/yubico/yubikit/piv/TouchPolicy;->DEFAULT:Lcom/yubico/yubikit/piv/TouchPolicy;

    .line 22
    .line 23
    if-eq p3, v0, :cond_3

    .line 24
    .line 25
    :cond_2
    sget-object v0, Lcom/yubico/yubikit/piv/PivSession;->O8o08O8O:Lcom/yubico/yubikit/core/application/Feature;

    .line 26
    .line 27
    invoke-virtual {p0, v0}, Lcom/yubico/yubikit/core/application/ApplicationSession;->〇〇888(Lcom/yubico/yubikit/core/application/Feature;)V

    .line 28
    .line 29
    .line 30
    sget-object v0, Lcom/yubico/yubikit/piv/TouchPolicy;->CACHED:Lcom/yubico/yubikit/piv/TouchPolicy;

    .line 31
    .line 32
    if-ne p3, v0, :cond_3

    .line 33
    .line 34
    sget-object p3, Lcom/yubico/yubikit/piv/PivSession;->〇080OO8〇0:Lcom/yubico/yubikit/core/application/Feature;

    .line 35
    .line 36
    invoke-virtual {p0, p3}, Lcom/yubico/yubikit/core/application/ApplicationSession;->〇〇888(Lcom/yubico/yubikit/core/application/Feature;)V

    .line 37
    .line 38
    .line 39
    :cond_3
    if-eqz p4, :cond_4

    .line 40
    .line 41
    iget-object p3, p1, Lcom/yubico/yubikit/piv/KeyType;->params:Lcom/yubico/yubikit/piv/KeyType$KeyParams;

    .line 42
    .line 43
    iget-object p3, p3, Lcom/yubico/yubikit/piv/KeyType$KeyParams;->〇080:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    .line 44
    .line 45
    sget-object p4, Lcom/yubico/yubikit/piv/KeyType$Algorithm;->RSA:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    .line 46
    .line 47
    if-ne p3, p4, :cond_4

    .line 48
    .line 49
    sget-object p3, Lcom/yubico/yubikit/piv/PivSession;->o8〇OO0〇0o:Lcom/yubico/yubikit/core/application/Feature;

    .line 50
    .line 51
    invoke-virtual {p0, p3}, Lcom/yubico/yubikit/core/application/ApplicationSession;->〇〇888(Lcom/yubico/yubikit/core/application/Feature;)V

    .line 52
    .line 53
    .line 54
    :cond_4
    iget-object p3, p0, Lcom/yubico/yubikit/piv/PivSession;->〇OOo8〇0:Lcom/yubico/yubikit/core/Version;

    .line 55
    .line 56
    const/4 p4, 0x4

    .line 57
    const/4 v0, 0x0

    .line 58
    invoke-virtual {p3, p4, p4, v0}, Lcom/yubico/yubikit/core/Version;->Oo08(III)Z

    .line 59
    .line 60
    .line 61
    move-result p3

    .line 62
    if-eqz p3, :cond_7

    .line 63
    .line 64
    iget-object p3, p0, Lcom/yubico/yubikit/piv/PivSession;->〇OOo8〇0:Lcom/yubico/yubikit/core/Version;

    .line 65
    .line 66
    const/4 v1, 0x5

    .line 67
    invoke-virtual {p3, p4, v1, v0}, Lcom/yubico/yubikit/core/Version;->o〇0(III)Z

    .line 68
    .line 69
    .line 70
    move-result p3

    .line 71
    if-eqz p3, :cond_7

    .line 72
    .line 73
    sget-object p3, Lcom/yubico/yubikit/piv/KeyType;->RSA1024:Lcom/yubico/yubikit/piv/KeyType;

    .line 74
    .line 75
    if-eq p1, p3, :cond_6

    .line 76
    .line 77
    sget-object p1, Lcom/yubico/yubikit/piv/PinPolicy;->NEVER:Lcom/yubico/yubikit/piv/PinPolicy;

    .line 78
    .line 79
    if-eq p2, p1, :cond_5

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_5
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    .line 83
    .line 84
    const-string p2, "PinPolicy.NEVER is not allowed on YubiKey FIPS"

    .line 85
    .line 86
    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    throw p1

    .line 90
    :cond_6
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    .line 91
    .line 92
    const-string p2, "RSA 1024 is not supported on YubiKey FIPS"

    .line 93
    .line 94
    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    throw p1

    .line 98
    :cond_7
    :goto_0
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method public OOO〇O0(Lcom/yubico/yubikit/piv/Slot;)Lcom/yubico/yubikit/piv/SlotMetadata;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/yubico/yubikit/piv/PivSession;->oOo0:Lcom/yubico/yubikit/core/application/Feature;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/yubico/yubikit/core/application/ApplicationSession;->〇〇888(Lcom/yubico/yubikit/core/application/Feature;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/yubico/yubikit/piv/PivSession;->o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 7
    .line 8
    new-instance v7, Lcom/yubico/yubikit/core/smartcard/Apdu;

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    const/16 v3, -0x9

    .line 12
    .line 13
    const/4 v4, 0x0

    .line 14
    iget v5, p1, Lcom/yubico/yubikit/piv/Slot;->value:I

    .line 15
    .line 16
    const/4 v6, 0x0

    .line 17
    move-object v1, v7

    .line 18
    invoke-direct/range {v1 .. v6}, Lcom/yubico/yubikit/core/smartcard/Apdu;-><init>(IIII[B)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v7}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->〇O8o08O(Lcom/yubico/yubikit/core/smartcard/Apdu;)[B

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-static {p1}, Lcom/yubico/yubikit/core/util/Tlvs;->〇o00〇〇Oo([B)Ljava/util/Map;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    const/4 v0, 0x2

    .line 30
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    check-cast v0, [B

    .line 39
    .line 40
    new-instance v7, Lcom/yubico/yubikit/piv/SlotMetadata;

    .line 41
    .line 42
    const/4 v1, 0x1

    .line 43
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    check-cast v2, [B

    .line 52
    .line 53
    const/4 v3, 0x0

    .line 54
    aget-byte v2, v2, v3

    .line 55
    .line 56
    invoke-static {v2}, Lcom/yubico/yubikit/piv/KeyType;->fromValue(I)Lcom/yubico/yubikit/piv/KeyType;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    aget-byte v4, v0, v3

    .line 61
    .line 62
    invoke-static {v4}, Lcom/yubico/yubikit/piv/PinPolicy;->fromValue(I)Lcom/yubico/yubikit/piv/PinPolicy;

    .line 63
    .line 64
    .line 65
    move-result-object v4

    .line 66
    aget-byte v0, v0, v1

    .line 67
    .line 68
    invoke-static {v0}, Lcom/yubico/yubikit/piv/TouchPolicy;->fromValue(I)Lcom/yubico/yubikit/piv/TouchPolicy;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    const/4 v5, 0x3

    .line 73
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 74
    .line 75
    .line 76
    move-result-object v5

    .line 77
    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object v5

    .line 81
    check-cast v5, [B

    .line 82
    .line 83
    aget-byte v5, v5, v3

    .line 84
    .line 85
    if-ne v5, v1, :cond_0

    .line 86
    .line 87
    const/4 v5, 0x1

    .line 88
    goto :goto_0

    .line 89
    :cond_0
    const/4 v5, 0x0

    .line 90
    :goto_0
    const/4 v1, 0x4

    .line 91
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 92
    .line 93
    .line 94
    move-result-object v1

    .line 95
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    move-object v6, p1

    .line 100
    check-cast v6, [B

    .line 101
    .line 102
    move-object v1, v7

    .line 103
    move-object v3, v4

    .line 104
    move-object v4, v0

    .line 105
    invoke-direct/range {v1 .. v6}, Lcom/yubico/yubikit/piv/SlotMetadata;-><init>(Lcom/yubico/yubikit/piv/KeyType;Lcom/yubico/yubikit/piv/PinPolicy;Lcom/yubico/yubikit/piv/TouchPolicy;Z[B)V

    .line 106
    .line 107
    .line 108
    return-object v7
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method public Oo08()Lcom/yubico/yubikit/core/Version;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/piv/PivSession;->〇OOo8〇0:Lcom/yubico/yubikit/core/Version;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public O〇O〇oO(Lcom/yubico/yubikit/piv/Slot;Ljava/security/PrivateKey;Lcom/yubico/yubikit/piv/PinPolicy;Lcom/yubico/yubikit/piv/TouchPolicy;)Lcom/yubico/yubikit/piv/KeyType;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    move-object/from16 v2, p3

    .line 6
    .line 7
    move-object/from16 v3, p4

    .line 8
    .line 9
    invoke-static/range {p2 .. p2}, Lcom/yubico/yubikit/piv/KeyType;->fromKey(Ljava/security/Key;)Lcom/yubico/yubikit/piv/KeyType;

    .line 10
    .line 11
    .line 12
    move-result-object v4

    .line 13
    const/4 v5, 0x0

    .line 14
    invoke-virtual {v0, v4, v2, v3, v5}, Lcom/yubico/yubikit/piv/PivSession;->OO0o〇〇(Lcom/yubico/yubikit/piv/KeyType;Lcom/yubico/yubikit/piv/PinPolicy;Lcom/yubico/yubikit/piv/TouchPolicy;Z)V

    .line 15
    .line 16
    .line 17
    iget-object v6, v4, Lcom/yubico/yubikit/piv/KeyType;->params:Lcom/yubico/yubikit/piv/KeyType$KeyParams;

    .line 18
    .line 19
    new-instance v7, Ljava/util/LinkedHashMap;

    .line 20
    .line 21
    invoke-direct {v7}, Ljava/util/LinkedHashMap;-><init>()V

    .line 22
    .line 23
    .line 24
    sget-object v8, Lcom/yubico/yubikit/piv/PivSession$2;->〇080:[I

    .line 25
    .line 26
    iget-object v9, v6, Lcom/yubico/yubikit/piv/KeyType$KeyParams;->〇080:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    .line 27
    .line 28
    invoke-virtual {v9}, Ljava/lang/Enum;->ordinal()I

    .line 29
    .line 30
    .line 31
    move-result v9

    .line 32
    aget v8, v8, v9

    .line 33
    .line 34
    const/16 v9, 0x8

    .line 35
    .line 36
    const/4 v10, 0x6

    .line 37
    const/4 v11, 0x2

    .line 38
    const/4 v12, 0x1

    .line 39
    if-eq v8, v12, :cond_1

    .line 40
    .line 41
    if-eq v8, v11, :cond_0

    .line 42
    .line 43
    goto/16 :goto_1

    .line 44
    .line 45
    :cond_0
    check-cast v1, Ljava/security/interfaces/ECPrivateKey;

    .line 46
    .line 47
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 48
    .line 49
    .line 50
    move-result-object v8

    .line 51
    invoke-interface {v1}, Ljava/security/interfaces/ECPrivateKey;->getS()Ljava/math/BigInteger;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    iget v6, v6, Lcom/yubico/yubikit/piv/KeyType$KeyParams;->〇o00〇〇Oo:I

    .line 56
    .line 57
    div-int/2addr v6, v9

    .line 58
    invoke-static {v1, v6}, Lcom/yubico/yubikit/piv/PivSession;->〇8o8o〇(Ljava/math/BigInteger;I)[B

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    invoke-interface {v7, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    goto/16 :goto_1

    .line 66
    .line 67
    :cond_1
    instance-of v8, v1, Ljava/security/interfaces/RSAPrivateCrtKey;

    .line 68
    .line 69
    const/4 v14, 0x5

    .line 70
    const/4 v15, 0x4

    .line 71
    const/4 v13, 0x3

    .line 72
    if-eqz v8, :cond_2

    .line 73
    .line 74
    check-cast v1, Ljava/security/interfaces/RSAPrivateCrtKey;

    .line 75
    .line 76
    new-array v8, v9, [Ljava/math/BigInteger;

    .line 77
    .line 78
    invoke-interface {v1}, Ljava/security/interfaces/RSAKey;->getModulus()Ljava/math/BigInteger;

    .line 79
    .line 80
    .line 81
    move-result-object v17

    .line 82
    aput-object v17, v8, v5

    .line 83
    .line 84
    invoke-interface {v1}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPublicExponent()Ljava/math/BigInteger;

    .line 85
    .line 86
    .line 87
    move-result-object v17

    .line 88
    aput-object v17, v8, v12

    .line 89
    .line 90
    invoke-interface {v1}, Ljava/security/interfaces/RSAPrivateKey;->getPrivateExponent()Ljava/math/BigInteger;

    .line 91
    .line 92
    .line 93
    move-result-object v17

    .line 94
    aput-object v17, v8, v11

    .line 95
    .line 96
    invoke-interface {v1}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeP()Ljava/math/BigInteger;

    .line 97
    .line 98
    .line 99
    move-result-object v17

    .line 100
    aput-object v17, v8, v13

    .line 101
    .line 102
    invoke-interface {v1}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeQ()Ljava/math/BigInteger;

    .line 103
    .line 104
    .line 105
    move-result-object v17

    .line 106
    aput-object v17, v8, v15

    .line 107
    .line 108
    invoke-interface {v1}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeExponentP()Ljava/math/BigInteger;

    .line 109
    .line 110
    .line 111
    move-result-object v17

    .line 112
    aput-object v17, v8, v14

    .line 113
    .line 114
    invoke-interface {v1}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeExponentQ()Ljava/math/BigInteger;

    .line 115
    .line 116
    .line 117
    move-result-object v17

    .line 118
    aput-object v17, v8, v10

    .line 119
    .line 120
    invoke-interface {v1}, Ljava/security/interfaces/RSAPrivateCrtKey;->getCrtCoefficient()Ljava/math/BigInteger;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    const/16 v16, 0x7

    .line 125
    .line 126
    aput-object v1, v8, v16

    .line 127
    .line 128
    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    goto :goto_0

    .line 133
    :cond_2
    invoke-interface/range {p2 .. p2}, Ljava/security/Key;->getFormat()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v8

    .line 137
    const-string v5, "PKCS#8"

    .line 138
    .line 139
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 140
    .line 141
    .line 142
    move-result v5

    .line 143
    if-eqz v5, :cond_6

    .line 144
    .line 145
    invoke-interface/range {p2 .. p2}, Ljava/security/Key;->getEncoded()[B

    .line 146
    .line 147
    .line 148
    move-result-object v1

    .line 149
    invoke-static {v1}, Lcom/yubico/yubikit/piv/PivSession;->〇00〇8([B)Ljava/util/List;

    .line 150
    .line 151
    .line 152
    move-result-object v1

    .line 153
    :goto_0
    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 154
    .line 155
    .line 156
    move-result-object v5

    .line 157
    check-cast v5, Ljava/math/BigInteger;

    .line 158
    .line 159
    invoke-virtual {v5}, Ljava/math/BigInteger;->intValue()I

    .line 160
    .line 161
    .line 162
    move-result v5

    .line 163
    const v8, 0x10001

    .line 164
    .line 165
    .line 166
    if-ne v5, v8, :cond_5

    .line 167
    .line 168
    iget v5, v6, Lcom/yubico/yubikit/piv/KeyType$KeyParams;->〇o00〇〇Oo:I

    .line 169
    .line 170
    div-int/2addr v5, v9

    .line 171
    div-int/2addr v5, v11

    .line 172
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 173
    .line 174
    .line 175
    move-result-object v6

    .line 176
    invoke-interface {v1, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 177
    .line 178
    .line 179
    move-result-object v8

    .line 180
    check-cast v8, Ljava/math/BigInteger;

    .line 181
    .line 182
    invoke-static {v8, v5}, Lcom/yubico/yubikit/piv/PivSession;->〇8o8o〇(Ljava/math/BigInteger;I)[B

    .line 183
    .line 184
    .line 185
    move-result-object v8

    .line 186
    invoke-interface {v7, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    .line 188
    .line 189
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 190
    .line 191
    .line 192
    move-result-object v6

    .line 193
    invoke-interface {v1, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 194
    .line 195
    .line 196
    move-result-object v8

    .line 197
    check-cast v8, Ljava/math/BigInteger;

    .line 198
    .line 199
    invoke-static {v8, v5}, Lcom/yubico/yubikit/piv/PivSession;->〇8o8o〇(Ljava/math/BigInteger;I)[B

    .line 200
    .line 201
    .line 202
    move-result-object v8

    .line 203
    invoke-interface {v7, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    .line 205
    .line 206
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 207
    .line 208
    .line 209
    move-result-object v6

    .line 210
    invoke-interface {v1, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 211
    .line 212
    .line 213
    move-result-object v8

    .line 214
    check-cast v8, Ljava/math/BigInteger;

    .line 215
    .line 216
    invoke-static {v8, v5}, Lcom/yubico/yubikit/piv/PivSession;->〇8o8o〇(Ljava/math/BigInteger;I)[B

    .line 217
    .line 218
    .line 219
    move-result-object v8

    .line 220
    invoke-interface {v7, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    .line 222
    .line 223
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 224
    .line 225
    .line 226
    move-result-object v6

    .line 227
    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 228
    .line 229
    .line 230
    move-result-object v8

    .line 231
    check-cast v8, Ljava/math/BigInteger;

    .line 232
    .line 233
    invoke-static {v8, v5}, Lcom/yubico/yubikit/piv/PivSession;->〇8o8o〇(Ljava/math/BigInteger;I)[B

    .line 234
    .line 235
    .line 236
    move-result-object v8

    .line 237
    invoke-interface {v7, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    .line 239
    .line 240
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 241
    .line 242
    .line 243
    move-result-object v6

    .line 244
    const/4 v8, 0x7

    .line 245
    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 246
    .line 247
    .line 248
    move-result-object v1

    .line 249
    check-cast v1, Ljava/math/BigInteger;

    .line 250
    .line 251
    invoke-static {v1, v5}, Lcom/yubico/yubikit/piv/PivSession;->〇8o8o〇(Ljava/math/BigInteger;I)[B

    .line 252
    .line 253
    .line 254
    move-result-object v1

    .line 255
    invoke-interface {v7, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    .line 257
    .line 258
    :goto_1
    sget-object v1, Lcom/yubico/yubikit/piv/PinPolicy;->DEFAULT:Lcom/yubico/yubikit/piv/PinPolicy;

    .line 259
    .line 260
    if-eq v2, v1, :cond_3

    .line 261
    .line 262
    const/16 v1, 0xaa

    .line 263
    .line 264
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 265
    .line 266
    .line 267
    move-result-object v1

    .line 268
    new-array v5, v12, [B

    .line 269
    .line 270
    iget v2, v2, Lcom/yubico/yubikit/piv/PinPolicy;->value:I

    .line 271
    .line 272
    int-to-byte v2, v2

    .line 273
    const/4 v6, 0x0

    .line 274
    aput-byte v2, v5, v6

    .line 275
    .line 276
    invoke-interface {v7, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    .line 278
    .line 279
    goto :goto_2

    .line 280
    :cond_3
    const/4 v6, 0x0

    .line 281
    :goto_2
    sget-object v1, Lcom/yubico/yubikit/piv/TouchPolicy;->DEFAULT:Lcom/yubico/yubikit/piv/TouchPolicy;

    .line 282
    .line 283
    if-eq v3, v1, :cond_4

    .line 284
    .line 285
    const/16 v1, 0xab

    .line 286
    .line 287
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 288
    .line 289
    .line 290
    move-result-object v1

    .line 291
    new-array v2, v12, [B

    .line 292
    .line 293
    iget v3, v3, Lcom/yubico/yubikit/piv/TouchPolicy;->value:I

    .line 294
    .line 295
    int-to-byte v3, v3

    .line 296
    aput-byte v3, v2, v6

    .line 297
    .line 298
    invoke-interface {v7, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    .line 300
    .line 301
    :cond_4
    iget-object v1, v0, Lcom/yubico/yubikit/piv/PivSession;->o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 302
    .line 303
    new-instance v2, Lcom/yubico/yubikit/core/smartcard/Apdu;

    .line 304
    .line 305
    const/4 v9, 0x0

    .line 306
    const/4 v10, -0x2

    .line 307
    iget-byte v11, v4, Lcom/yubico/yubikit/piv/KeyType;->value:B

    .line 308
    .line 309
    move-object/from16 v3, p1

    .line 310
    .line 311
    iget v12, v3, Lcom/yubico/yubikit/piv/Slot;->value:I

    .line 312
    .line 313
    invoke-static {v7}, Lcom/yubico/yubikit/core/util/Tlvs;->〇o〇(Ljava/util/Map;)[B

    .line 314
    .line 315
    .line 316
    move-result-object v13

    .line 317
    move-object v8, v2

    .line 318
    invoke-direct/range {v8 .. v13}, Lcom/yubico/yubikit/core/smartcard/Apdu;-><init>(IIII[B)V

    .line 319
    .line 320
    .line 321
    invoke-virtual {v1, v2}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->〇O8o08O(Lcom/yubico/yubikit/core/smartcard/Apdu;)[B

    .line 322
    .line 323
    .line 324
    return-object v4

    .line 325
    :cond_5
    new-instance v1, Ljava/io/UnsupportedEncodingException;

    .line 326
    .line 327
    const-string v2, "Unsupported RSA public exponent"

    .line 328
    .line 329
    invoke-direct {v1, v2}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    .line 330
    .line 331
    .line 332
    throw v1

    .line 333
    :cond_6
    new-instance v1, Ljava/io/UnsupportedEncodingException;

    .line 334
    .line 335
    const-string v2, "Unsupported private key encoding"

    .line 336
    .line 337
    invoke-direct {v1, v2}, Ljava/io/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    .line 338
    .line 339
    .line 340
    throw v1
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/yubico/yubikit/piv/PivSession;->o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->close()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o800o8O()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/yubico/yubikit/piv/PivSession;->oOo0:Lcom/yubico/yubikit/core/application/Feature;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/yubico/yubikit/core/application/ApplicationSession;->oO80(Lcom/yubico/yubikit/core/application/Feature;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/yubico/yubikit/piv/PivSession;->〇oo〇()Lcom/yubico/yubikit/piv/PinMetadata;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/yubico/yubikit/piv/PinMetadata;->〇080()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0

    .line 18
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/yubico/yubikit/piv/PivSession;->o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 19
    .line 20
    new-instance v7, Lcom/yubico/yubikit/core/smartcard/Apdu;

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    const/16 v3, 0x20

    .line 24
    .line 25
    const/4 v4, 0x0

    .line 26
    const/16 v5, -0x80

    .line 27
    .line 28
    const/4 v6, 0x0

    .line 29
    move-object v1, v7

    .line 30
    invoke-direct/range {v1 .. v6}, Lcom/yubico/yubikit/core/smartcard/Apdu;-><init>(IIII[B)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v7}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->〇O8o08O(Lcom/yubico/yubikit/core/smartcard/Apdu;)[B

    .line 34
    .line 35
    .line 36
    iget v0, p0, Lcom/yubico/yubikit/piv/PivSession;->OO:I
    :try_end_0
    .catch Lcom/yubico/yubikit/core/smartcard/ApduException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    .line 38
    return v0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    invoke-virtual {v0}, Lcom/yubico/yubikit/core/smartcard/ApduException;->getSw()S

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    invoke-direct {p0, v1}, Lcom/yubico/yubikit/piv/PivSession;->o〇〇0〇(I)I

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    if-ltz v1, :cond_1

    .line 49
    .line 50
    iput v1, p0, Lcom/yubico/yubikit/piv/PivSession;->OO:I

    .line 51
    .line 52
    return v1

    .line 53
    :cond_1
    throw v0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public o8oO〇(I[B)V
    .locals 7
    .param p2    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/LinkedHashMap;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x5c

    .line 7
    .line 8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-static {p1}, Lcom/yubico/yubikit/piv/ObjectId;->〇080(I)[B

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    const/16 p1, 0x53

    .line 20
    .line 21
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    iget-object p1, p0, Lcom/yubico/yubikit/piv/PivSession;->o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 29
    .line 30
    new-instance p2, Lcom/yubico/yubikit/core/smartcard/Apdu;

    .line 31
    .line 32
    const/4 v2, 0x0

    .line 33
    const/16 v3, -0x25

    .line 34
    .line 35
    const/16 v4, 0x3f

    .line 36
    .line 37
    const/16 v5, 0xff

    .line 38
    .line 39
    invoke-static {v0}, Lcom/yubico/yubikit/core/util/Tlvs;->〇o〇(Ljava/util/Map;)[B

    .line 40
    .line 41
    .line 42
    move-result-object v6

    .line 43
    move-object v1, p2

    .line 44
    invoke-direct/range {v1 .. v6}, Lcom/yubico/yubikit/core/smartcard/Apdu;-><init>(IIII[B)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1, p2}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->〇O8o08O(Lcom/yubico/yubikit/core/smartcard/Apdu;)[B

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
.end method

.method public oO00OOO([C)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;,
            Lcom/yubico/yubikit/piv/InvalidPinException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/yubico/yubikit/piv/PivSession;->o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 2
    .line 3
    new-instance v7, Lcom/yubico/yubikit/core/smartcard/Apdu;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/16 v3, 0x20

    .line 7
    .line 8
    const/4 v4, 0x0

    .line 9
    const/16 v5, -0x80

    .line 10
    .line 11
    invoke-static {p1}, Lcom/yubico/yubikit/piv/PivSession;->oO([C)[B

    .line 12
    .line 13
    .line 14
    move-result-object v6

    .line 15
    move-object v1, v7

    .line 16
    invoke-direct/range {v1 .. v6}, Lcom/yubico/yubikit/core/smartcard/Apdu;-><init>(IIII[B)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v7}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->〇O8o08O(Lcom/yubico/yubikit/core/smartcard/Apdu;)[B

    .line 20
    .line 21
    .line 22
    iget p1, p0, Lcom/yubico/yubikit/piv/PivSession;->〇08O〇00〇o:I

    .line 23
    .line 24
    iput p1, p0, Lcom/yubico/yubikit/piv/PivSession;->OO:I
    :try_end_0
    .catch Lcom/yubico/yubikit/core/smartcard/ApduException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    .line 26
    return-void

    .line 27
    :catch_0
    move-exception p1

    .line 28
    invoke-virtual {p1}, Lcom/yubico/yubikit/core/smartcard/ApduException;->getSw()S

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    invoke-direct {p0, v0}, Lcom/yubico/yubikit/piv/PivSession;->o〇〇0〇(I)I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-ltz v0, :cond_0

    .line 37
    .line 38
    iput v0, p0, Lcom/yubico/yubikit/piv/PivSession;->OO:I

    .line 39
    .line 40
    new-instance p1, Lcom/yubico/yubikit/piv/InvalidPinException;

    .line 41
    .line 42
    invoke-direct {p1, v0}, Lcom/yubico/yubikit/piv/InvalidPinException;-><init>(I)V

    .line 43
    .line 44
    .line 45
    throw p1

    .line 46
    :cond_0
    throw p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public o〇8oOO88(Lcom/yubico/yubikit/piv/Slot;Lcom/yubico/yubikit/piv/KeyType;[B)[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;,
            Lcom/yubico/yubikit/core/application/BadResponseException;
        }
    .end annotation

    .line 1
    iget-object v0, p2, Lcom/yubico/yubikit/piv/KeyType;->params:Lcom/yubico/yubikit/piv/KeyType$KeyParams;

    .line 2
    .line 3
    iget v1, v0, Lcom/yubico/yubikit/piv/KeyType$KeyParams;->〇o00〇〇Oo:I

    .line 4
    .line 5
    div-int/lit8 v1, v1, 0x8

    .line 6
    .line 7
    array-length v2, p3

    .line 8
    const/4 v3, 0x0

    .line 9
    if-le v2, v1, :cond_1

    .line 10
    .line 11
    iget-object v0, v0, Lcom/yubico/yubikit/piv/KeyType$KeyParams;->〇080:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    .line 12
    .line 13
    sget-object v2, Lcom/yubico/yubikit/piv/KeyType$Algorithm;->EC:Lcom/yubico/yubikit/piv/KeyType$Algorithm;

    .line 14
    .line 15
    if-ne v0, v2, :cond_0

    .line 16
    .line 17
    invoke-static {p3, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    .line 18
    .line 19
    .line 20
    move-result-object p3

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 23
    .line 24
    const-string p2, "Payload too large for key"

    .line 25
    .line 26
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    throw p1

    .line 30
    :cond_1
    array-length v0, p3

    .line 31
    if-ge v0, v1, :cond_2

    .line 32
    .line 33
    new-array v0, v1, [B

    .line 34
    .line 35
    array-length v2, p3

    .line 36
    sub-int/2addr v1, v2

    .line 37
    array-length v2, p3

    .line 38
    invoke-static {p3, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 39
    .line 40
    .line 41
    move-object p3, v0

    .line 42
    :cond_2
    :goto_0
    invoke-direct {p0, p1, p2, p3, v3}, Lcom/yubico/yubikit/piv/PivSession;->o〇O(Lcom/yubico/yubikit/piv/Slot;Lcom/yubico/yubikit/piv/KeyType;[BZ)[B

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    return-object p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public 〇8〇0〇o〇O(Lcom/yubico/yubikit/piv/Slot;Ljava/security/cert/X509Certificate;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-virtual {p2}, Ljava/security/cert/Certificate;->getEncoded()[B

    .line 2
    .line 3
    .line 4
    move-result-object p2
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    new-instance v0, Ljava/util/LinkedHashMap;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 8
    .line 9
    .line 10
    const/16 v1, 0x70

    .line 11
    .line 12
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    const/16 p2, 0x71

    .line 20
    .line 21
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    const/4 v1, 0x1

    .line 26
    new-array v1, v1, [B

    .line 27
    .line 28
    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    const/16 p2, 0xfe

    .line 32
    .line 33
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    const/4 v1, 0x0

    .line 38
    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    iget p1, p1, Lcom/yubico/yubikit/piv/Slot;->objectId:I

    .line 42
    .line 43
    invoke-static {v0}, Lcom/yubico/yubikit/core/util/Tlvs;->〇o〇(Ljava/util/Map;)[B

    .line 44
    .line 45
    .line 46
    move-result-object p2

    .line 47
    invoke-virtual {p0, p1, p2}, Lcom/yubico/yubikit/piv/PivSession;->o8oO〇(I[B)V

    .line 48
    .line 49
    .line 50
    return-void

    .line 51
    :catch_0
    move-exception p1

    .line 52
    new-instance p2, Ljava/lang/IllegalArgumentException;

    .line 53
    .line 54
    const-string v0, "Failed to get encoded version of certificate"

    .line 55
    .line 56
    invoke-direct {p2, v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 57
    .line 58
    .line 59
    throw p2
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public 〇O00(Lcom/yubico/yubikit/piv/Slot;)Ljava/security/cert/X509Certificate;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;,
            Lcom/yubico/yubikit/core/application/BadResponseException;
        }
    .end annotation

    .line 1
    iget p1, p1, Lcom/yubico/yubikit/piv/Slot;->objectId:I

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/yubico/yubikit/piv/PivSession;->〇〇8O0〇8(I)[B

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-static {p1}, Lcom/yubico/yubikit/core/util/Tlvs;->〇o00〇〇Oo([B)Ljava/util/Map;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    const/16 v0, 0x71

    .line 12
    .line 13
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, [B

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    array-length v1, v0

    .line 26
    if-lez v1, :cond_1

    .line 27
    .line 28
    const/4 v1, 0x0

    .line 29
    aget-byte v0, v0, v1

    .line 30
    .line 31
    if-nez v0, :cond_0

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    new-instance p1, Lcom/yubico/yubikit/core/application/BadResponseException;

    .line 35
    .line 36
    const-string v0, "Compressed certificates are not supported"

    .line 37
    .line 38
    invoke-direct {p1, v0}, Lcom/yubico/yubikit/core/application/BadResponseException;-><init>(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    throw p1

    .line 42
    :cond_1
    :goto_0
    const/16 v0, 0x70

    .line 43
    .line 44
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    check-cast p1, [B

    .line 53
    .line 54
    invoke-direct {p0, p1}, Lcom/yubico/yubikit/piv/PivSession;->O8〇o([B)Ljava/security/cert/X509Certificate;

    .line 55
    .line 56
    .line 57
    move-result-object p1
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    return-object p1

    .line 59
    :catch_0
    move-exception p1

    .line 60
    new-instance v0, Lcom/yubico/yubikit/core/application/BadResponseException;

    .line 61
    .line 62
    const-string v1, "Failed to parse certificate: "

    .line 63
    .line 64
    invoke-direct {v0, v1, p1}, Lcom/yubico/yubikit/core/application/BadResponseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    .line 66
    .line 67
    throw v0
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public 〇O8o08O(Lcom/yubico/yubikit/piv/Slot;Ljava/security/interfaces/ECPublicKey;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;,
            Lcom/yubico/yubikit/core/application/BadResponseException;
        }
    .end annotation

    .line 1
    invoke-static {p2}, Lcom/yubico/yubikit/piv/KeyType;->fromKey(Ljava/security/Key;)Lcom/yubico/yubikit/piv/KeyType;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, v0, Lcom/yubico/yubikit/piv/KeyType;->params:Lcom/yubico/yubikit/piv/KeyType$KeyParams;

    .line 6
    .line 7
    iget v1, v1, Lcom/yubico/yubikit/piv/KeyType$KeyParams;->〇o00〇〇Oo:I

    .line 8
    .line 9
    div-int/lit8 v1, v1, 0x8

    .line 10
    .line 11
    mul-int/lit8 v2, v1, 0x2

    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    add-int/2addr v2, v3

    .line 15
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const/4 v4, 0x4

    .line 20
    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    .line 25
    .line 26
    .line 27
    move-result-object v4

    .line 28
    invoke-virtual {v4}, Ljava/security/spec/ECPoint;->getAffineX()Ljava/math/BigInteger;

    .line 29
    .line 30
    .line 31
    move-result-object v4

    .line 32
    invoke-static {v4, v1}, Lcom/yubico/yubikit/piv/PivSession;->〇8o8o〇(Ljava/math/BigInteger;I)[B

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    .line 41
    .line 42
    .line 43
    move-result-object p2

    .line 44
    invoke-virtual {p2}, Ljava/security/spec/ECPoint;->getAffineY()Ljava/math/BigInteger;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    invoke-static {p2, v1}, Lcom/yubico/yubikit/piv/PivSession;->〇8o8o〇(Ljava/math/BigInteger;I)[B

    .line 49
    .line 50
    .line 51
    move-result-object p2

    .line 52
    invoke-virtual {v2, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 53
    .line 54
    .line 55
    move-result-object p2

    .line 56
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    .line 57
    .line 58
    .line 59
    move-result-object p2

    .line 60
    invoke-direct {p0, p1, v0, p2, v3}, Lcom/yubico/yubikit/piv/PivSession;->o〇O(Lcom/yubico/yubikit/piv/Slot;Lcom/yubico/yubikit/piv/KeyType;[BZ)[B

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    return-object p1
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public 〇O〇(Lcom/yubico/yubikit/piv/Slot;Lcom/yubico/yubikit/piv/KeyType;Lcom/yubico/yubikit/piv/PinPolicy;Lcom/yubico/yubikit/piv/TouchPolicy;)Ljava/security/PublicKey;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;,
            Lcom/yubico/yubikit/core/application/BadResponseException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, p2, p3, p4, v0}, Lcom/yubico/yubikit/piv/PivSession;->OO0o〇〇(Lcom/yubico/yubikit/piv/KeyType;Lcom/yubico/yubikit/piv/PinPolicy;Lcom/yubico/yubikit/piv/TouchPolicy;Z)V

    .line 3
    .line 4
    .line 5
    new-instance v1, Ljava/util/LinkedHashMap;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 8
    .line 9
    .line 10
    const/16 v2, 0x80

    .line 11
    .line 12
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    new-array v3, v0, [B

    .line 17
    .line 18
    iget-byte v4, p2, Lcom/yubico/yubikit/piv/KeyType;->value:B

    .line 19
    .line 20
    const/4 v5, 0x0

    .line 21
    aput-byte v4, v3, v5

    .line 22
    .line 23
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    sget-object v2, Lcom/yubico/yubikit/piv/PinPolicy;->DEFAULT:Lcom/yubico/yubikit/piv/PinPolicy;

    .line 27
    .line 28
    if-eq p3, v2, :cond_0

    .line 29
    .line 30
    const/16 v2, 0xaa

    .line 31
    .line 32
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    new-array v3, v0, [B

    .line 37
    .line 38
    iget p3, p3, Lcom/yubico/yubikit/piv/PinPolicy;->value:I

    .line 39
    .line 40
    int-to-byte p3, p3

    .line 41
    aput-byte p3, v3, v5

    .line 42
    .line 43
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    :cond_0
    sget-object p3, Lcom/yubico/yubikit/piv/TouchPolicy;->DEFAULT:Lcom/yubico/yubikit/piv/TouchPolicy;

    .line 47
    .line 48
    if-eq p4, p3, :cond_1

    .line 49
    .line 50
    const/16 p3, 0xab

    .line 51
    .line 52
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 53
    .line 54
    .line 55
    move-result-object p3

    .line 56
    new-array v0, v0, [B

    .line 57
    .line 58
    iget p4, p4, Lcom/yubico/yubikit/piv/TouchPolicy;->value:I

    .line 59
    .line 60
    int-to-byte p4, p4

    .line 61
    aput-byte p4, v0, v5

    .line 62
    .line 63
    invoke-interface {v1, p3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    :cond_1
    iget-object p3, p0, Lcom/yubico/yubikit/piv/PivSession;->o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 67
    .line 68
    new-instance p4, Lcom/yubico/yubikit/core/smartcard/Apdu;

    .line 69
    .line 70
    const/4 v3, 0x0

    .line 71
    const/16 v4, 0x47

    .line 72
    .line 73
    const/4 v5, 0x0

    .line 74
    iget v6, p1, Lcom/yubico/yubikit/piv/Slot;->value:I

    .line 75
    .line 76
    new-instance p1, Lcom/yubico/yubikit/core/util/Tlv;

    .line 77
    .line 78
    const/16 v0, -0x54

    .line 79
    .line 80
    invoke-static {v1}, Lcom/yubico/yubikit/core/util/Tlvs;->〇o〇(Ljava/util/Map;)[B

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    invoke-direct {p1, v0, v1}, Lcom/yubico/yubikit/core/util/Tlv;-><init>(I[B)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p1}, Lcom/yubico/yubikit/core/util/Tlv;->〇080()[B

    .line 88
    .line 89
    .line 90
    move-result-object v7

    .line 91
    move-object v2, p4

    .line 92
    invoke-direct/range {v2 .. v7}, Lcom/yubico/yubikit/core/smartcard/Apdu;-><init>(IIII[B)V

    .line 93
    .line 94
    .line 95
    invoke-virtual {p3, p4}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->〇O8o08O(Lcom/yubico/yubikit/core/smartcard/Apdu;)[B

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    const/16 p3, 0x7f49

    .line 100
    .line 101
    invoke-static {p3, p1}, Lcom/yubico/yubikit/core/util/Tlvs;->O8(I[B)[B

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    invoke-static {p2, p1}, Lcom/yubico/yubikit/piv/PivSession;->〇〇〇0〇〇0(Lcom/yubico/yubikit/piv/KeyType;[B)Ljava/security/PublicKey;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    return-object p1
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method public 〇oo〇()Lcom/yubico/yubikit/piv/PinMetadata;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;
        }
    .end annotation

    .line 1
    const/16 v0, -0x80

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/yubico/yubikit/piv/PivSession;->O〇8O8〇008(B)Lcom/yubico/yubikit/piv/PinMetadata;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇〇808〇(Lcom/yubico/yubikit/piv/Slot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;
        }
    .end annotation

    .line 1
    iget p1, p1, Lcom/yubico/yubikit/piv/Slot;->objectId:I

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, p1, v0}, Lcom/yubico/yubikit/piv/PivSession;->o8oO〇(I[B)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇〇8O0〇8(I)[B
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/yubico/yubikit/core/smartcard/ApduException;,
            Lcom/yubico/yubikit/core/application/BadResponseException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/yubico/yubikit/core/util/Tlv;

    .line 2
    .line 3
    const/16 v1, 0x5c

    .line 4
    .line 5
    invoke-static {p1}, Lcom/yubico/yubikit/piv/ObjectId;->〇080(I)[B

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-direct {v0, v1, p1}, Lcom/yubico/yubikit/core/util/Tlv;-><init>(I[B)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/yubico/yubikit/core/util/Tlv;->〇080()[B

    .line 13
    .line 14
    .line 15
    move-result-object v7

    .line 16
    iget-object p1, p0, Lcom/yubico/yubikit/piv/PivSession;->o0:Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;

    .line 17
    .line 18
    new-instance v0, Lcom/yubico/yubikit/core/smartcard/Apdu;

    .line 19
    .line 20
    const/4 v3, 0x0

    .line 21
    const/16 v4, -0x35

    .line 22
    .line 23
    const/16 v5, 0x3f

    .line 24
    .line 25
    const/16 v6, 0xff

    .line 26
    .line 27
    move-object v2, v0

    .line 28
    invoke-direct/range {v2 .. v7}, Lcom/yubico/yubikit/core/smartcard/Apdu;-><init>(IIII[B)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1, v0}, Lcom/yubico/yubikit/core/smartcard/SmartCardProtocol;->〇O8o08O(Lcom/yubico/yubikit/core/smartcard/Apdu;)[B

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    const/16 v0, 0x53

    .line 36
    .line 37
    invoke-static {v0, p1}, Lcom/yubico/yubikit/core/util/Tlvs;->O8(I[B)[B

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    return-object p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.class public final Lcom/intsig/webview/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/webview/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final BOTTOM_END:I = 0x7f0a0002

.field public static final BOTTOM_START:I = 0x7f0a0003

.field public static final NO_DEBUG:I = 0x7f0a000e

.field public static final SHOW_ALL:I = 0x7f0a0012

.field public static final SHOW_PATH:I = 0x7f0a0013

.field public static final SHOW_PROGRESS:I = 0x7f0a0014

.field public static final TOP_END:I = 0x7f0a0017

.field public static final TOP_START:I = 0x7f0a0018

.field public static final accelerate:I = 0x7f0a001c

.field public static final accessibility_action_clickable_span:I = 0x7f0a001d

.field public static final accessibility_custom_action_0:I = 0x7f0a001e

.field public static final accessibility_custom_action_1:I = 0x7f0a001f

.field public static final accessibility_custom_action_10:I = 0x7f0a0020

.field public static final accessibility_custom_action_11:I = 0x7f0a0021

.field public static final accessibility_custom_action_12:I = 0x7f0a0022

.field public static final accessibility_custom_action_13:I = 0x7f0a0023

.field public static final accessibility_custom_action_14:I = 0x7f0a0024

.field public static final accessibility_custom_action_15:I = 0x7f0a0025

.field public static final accessibility_custom_action_16:I = 0x7f0a0026

.field public static final accessibility_custom_action_17:I = 0x7f0a0027

.field public static final accessibility_custom_action_18:I = 0x7f0a0028

.field public static final accessibility_custom_action_19:I = 0x7f0a0029

.field public static final accessibility_custom_action_2:I = 0x7f0a002a

.field public static final accessibility_custom_action_20:I = 0x7f0a002b

.field public static final accessibility_custom_action_21:I = 0x7f0a002c

.field public static final accessibility_custom_action_22:I = 0x7f0a002d

.field public static final accessibility_custom_action_23:I = 0x7f0a002e

.field public static final accessibility_custom_action_24:I = 0x7f0a002f

.field public static final accessibility_custom_action_25:I = 0x7f0a0030

.field public static final accessibility_custom_action_26:I = 0x7f0a0031

.field public static final accessibility_custom_action_27:I = 0x7f0a0032

.field public static final accessibility_custom_action_28:I = 0x7f0a0033

.field public static final accessibility_custom_action_29:I = 0x7f0a0034

.field public static final accessibility_custom_action_3:I = 0x7f0a0035

.field public static final accessibility_custom_action_30:I = 0x7f0a0036

.field public static final accessibility_custom_action_31:I = 0x7f0a0037

.field public static final accessibility_custom_action_4:I = 0x7f0a0038

.field public static final accessibility_custom_action_5:I = 0x7f0a0039

.field public static final accessibility_custom_action_6:I = 0x7f0a003a

.field public static final accessibility_custom_action_7:I = 0x7f0a003b

.field public static final accessibility_custom_action_8:I = 0x7f0a003c

.field public static final accessibility_custom_action_9:I = 0x7f0a003d

.field public static final action0:I = 0x7f0a0051

.field public static final actionDown:I = 0x7f0a0053

.field public static final actionDownUp:I = 0x7f0a0054

.field public static final actionUp:I = 0x7f0a0059

.field public static final action_bar:I = 0x7f0a005a

.field public static final action_bar_activity_content:I = 0x7f0a005b

.field public static final action_bar_container:I = 0x7f0a005c

.field public static final action_bar_root:I = 0x7f0a0060

.field public static final action_bar_spinner:I = 0x7f0a0061

.field public static final action_bar_subtitle:I = 0x7f0a0062

.field public static final action_bar_title:I = 0x7f0a0063

.field public static final action_btn:I = 0x7f0a0064

.field public static final action_container:I = 0x7f0a0066

.field public static final action_context_bar:I = 0x7f0a0067

.field public static final action_divider:I = 0x7f0a0068

.field public static final action_image:I = 0x7f0a0069

.field public static final action_menu_divider:I = 0x7f0a006b

.field public static final action_menu_presenter:I = 0x7f0a006c

.field public static final action_mode_bar:I = 0x7f0a006d

.field public static final action_mode_bar_stub:I = 0x7f0a006e

.field public static final action_mode_close_button:I = 0x7f0a006f

.field public static final action_share:I = 0x7f0a0071

.field public static final action_text:I = 0x7f0a0072

.field public static final actions:I = 0x7f0a0073

.field public static final activity_chooser_view_content:I = 0x7f0a0075

.field public static final ad_choices_container:I = 0x7f0a007e

.field public static final ad_container:I = 0x7f0a0080

.field public static final ad_control_button:I = 0x7f0a0082

.field public static final ad_controls_view:I = 0x7f0a0083

.field public static final ad_presenter_view:I = 0x7f0a0087

.field public static final ad_root:I = 0x7f0a0088

.field public static final ad_tag:I = 0x7f0a0089

.field public static final ad_view_container:I = 0x7f0a008c

.field public static final add:I = 0x7f0a008e

.field public static final adjust_height:I = 0x7f0a0097

.field public static final adjust_width:I = 0x7f0a009f

.field public static final al_exo_ad_overlay:I = 0x7f0a0109

.field public static final al_exo_artwork:I = 0x7f0a010a

.field public static final al_exo_audio_track:I = 0x7f0a010b

.field public static final al_exo_basic_controls:I = 0x7f0a010c

.field public static final al_exo_bottom_bar:I = 0x7f0a010d

.field public static final al_exo_buffering:I = 0x7f0a010e

.field public static final al_exo_center_controls:I = 0x7f0a010f

.field public static final al_exo_content_frame:I = 0x7f0a0110

.field public static final al_exo_controller:I = 0x7f0a0111

.field public static final al_exo_controller_placeholder:I = 0x7f0a0112

.field public static final al_exo_controls_background:I = 0x7f0a0113

.field public static final al_exo_duration:I = 0x7f0a0114

.field public static final al_exo_error_message:I = 0x7f0a0115

.field public static final al_exo_extra_controls:I = 0x7f0a0116

.field public static final al_exo_extra_controls_scroll_view:I = 0x7f0a0117

.field public static final al_exo_ffwd:I = 0x7f0a0118

.field public static final al_exo_ffwd_with_amount:I = 0x7f0a0119

.field public static final al_exo_fullscreen:I = 0x7f0a011a

.field public static final al_exo_minimal_controls:I = 0x7f0a011b

.field public static final al_exo_minimal_fullscreen:I = 0x7f0a011c

.field public static final al_exo_next:I = 0x7f0a011d

.field public static final al_exo_overflow_hide:I = 0x7f0a011e

.field public static final al_exo_overflow_show:I = 0x7f0a011f

.field public static final al_exo_overlay:I = 0x7f0a0120

.field public static final al_exo_pause:I = 0x7f0a0121

.field public static final al_exo_play:I = 0x7f0a0122

.field public static final al_exo_play_pause:I = 0x7f0a0123

.field public static final al_exo_playback_speed:I = 0x7f0a0124

.field public static final al_exo_position:I = 0x7f0a0125

.field public static final al_exo_prev:I = 0x7f0a0126

.field public static final al_exo_progress:I = 0x7f0a0127

.field public static final al_exo_progress_placeholder:I = 0x7f0a0128

.field public static final al_exo_repeat_toggle:I = 0x7f0a0129

.field public static final al_exo_rew:I = 0x7f0a012a

.field public static final al_exo_rew_with_amount:I = 0x7f0a012b

.field public static final al_exo_settings:I = 0x7f0a012c

.field public static final al_exo_shuffle:I = 0x7f0a012d

.field public static final al_exo_shutter:I = 0x7f0a012e

.field public static final al_exo_subtitle:I = 0x7f0a012f

.field public static final al_exo_subtitles:I = 0x7f0a0130

.field public static final al_exo_time:I = 0x7f0a0131

.field public static final al_exo_vr:I = 0x7f0a0132

.field public static final alertTitle:I = 0x7f0a0133

.field public static final aligned:I = 0x7f0a0134

.field public static final all:I = 0x7f0a0135

.field public static final allStates:I = 0x7f0a0136

.field public static final always:I = 0x7f0a0137

.field public static final analytics_purposes_switch:I = 0x7f0a013a

.field public static final analytics_purposes_switch_textview:I = 0x7f0a013b

.field public static final animateToEnd:I = 0x7f0a013c

.field public static final animateToStart:I = 0x7f0a013d

.field public static final antiClockwise:I = 0x7f0a013e

.field public static final anticipate:I = 0x7f0a013f

.field public static final app_bar_layout:I = 0x7f0a0143

.field public static final app_launch_ll_tips:I = 0x7f0a0145

.field public static final app_open_ad_control_button:I = 0x7f0a0147

.field public static final app_open_ad_control_view:I = 0x7f0a0148

.field public static final applovin_native_ad_badge_and_title_text_view:I = 0x7f0a014b

.field public static final applovin_native_ad_content_linear_layout:I = 0x7f0a014c

.field public static final applovin_native_ad_view_container:I = 0x7f0a014d

.field public static final applovin_native_advertiser_text_view:I = 0x7f0a014e

.field public static final applovin_native_badge_text_view:I = 0x7f0a014f

.field public static final applovin_native_body_text_view:I = 0x7f0a0150

.field public static final applovin_native_cta_button:I = 0x7f0a0151

.field public static final applovin_native_guideline:I = 0x7f0a0152

.field public static final applovin_native_icon_and_text_layout:I = 0x7f0a0153

.field public static final applovin_native_icon_image_view:I = 0x7f0a0154

.field public static final applovin_native_icon_view:I = 0x7f0a0155

.field public static final applovin_native_inner_linear_layout:I = 0x7f0a0156

.field public static final applovin_native_inner_parent_layout:I = 0x7f0a0157

.field public static final applovin_native_leader_icon_and_text_layout:I = 0x7f0a0158

.field public static final applovin_native_media_content_view:I = 0x7f0a0159

.field public static final applovin_native_options_view:I = 0x7f0a015a

.field public static final applovin_native_star_rating_view:I = 0x7f0a015b

.field public static final applovin_native_title_text_view:I = 0x7f0a015c

.field public static final arc:I = 0x7f0a015d

.field public static final arrow:I = 0x7f0a015e

.field public static final asConfigured:I = 0x7f0a0161

.field public static final async:I = 0x7f0a0163

.field public static final auto:I = 0x7f0a01b2

.field public static final autoComplete:I = 0x7f0a01b3

.field public static final autoCompleteToEnd:I = 0x7f0a01b4

.field public static final autoCompleteToStart:I = 0x7f0a01b5

.field public static final automatic:I = 0x7f0a01b8

.field public static final back_button:I = 0x7f0a01ba

.field public static final baiv_email:I = 0x7f0a01bb

.field public static final baiv_google:I = 0x7f0a01bc

.field public static final baiv_phone:I = 0x7f0a01bd

.field public static final baiv_wx:I = 0x7f0a01be

.field public static final banner_ad_view_container:I = 0x7f0a01c2

.field public static final banner_control_button:I = 0x7f0a01c3

.field public static final banner_control_view:I = 0x7f0a01c4

.field public static final banner_label:I = 0x7f0a01c5

.field public static final barrier:I = 0x7f0a01c7

.field public static final baseline:I = 0x7f0a01cf

.field public static final bestChoice:I = 0x7f0a01d2

.field public static final blocking:I = 0x7f0a01db

.field public static final bottom:I = 0x7f0a01e2

.field public static final bottom_panel:I = 0x7f0a01ed

.field public static final bounce:I = 0x7f0a01f9

.field public static final browser_actions_header_text:I = 0x7f0a0203

.field public static final browser_actions_menu_item_icon:I = 0x7f0a0204

.field public static final browser_actions_menu_item_text:I = 0x7f0a0205

.field public static final browser_actions_menu_items:I = 0x7f0a0206

.field public static final browser_actions_menu_view:I = 0x7f0a0207

.field public static final btn_a_key_login_a_key_login:I = 0x7f0a020e

.field public static final btn_action:I = 0x7f0a020f

.field public static final btn_area_code_confirm_next:I = 0x7f0a0222

.field public static final btn_bind_phone_for_certain:I = 0x7f0a0226

.field public static final btn_clear:I = 0x7f0a0236

.field public static final btn_common:I = 0x7f0a0237

.field public static final btn_create_account:I = 0x7f0a0240

.field public static final btn_done:I = 0x7f0a024c

.field public static final btn_drag:I = 0x7f0a024e

.field public static final btn_email_login_next:I = 0x7f0a0252

.field public static final btn_email_login_sign_in:I = 0x7f0a0253

.field public static final btn_email_register_next:I = 0x7f0a0254

.field public static final btn_filter:I = 0x7f0a025a

.field public static final btn_forget_pwd_get_verify_code:I = 0x7f0a025c

.field public static final btn_hide:I = 0x7f0a0266

.field public static final btn_login_main_next:I = 0x7f0a0275

.field public static final btn_phone_pwd_login_sign_in:I = 0x7f0a028d

.field public static final btn_phone_verify_code_login_get_verify_code:I = 0x7f0a028e

.field public static final btn_pwd_login_next:I = 0x7f0a0299

.field public static final btn_reopen:I = 0x7f0a029d

.field public static final btn_scale:I = 0x7f0a02a2

.field public static final btn_send_verify_code:I = 0x7f0a02a8

.field public static final btn_setting_pwd_start:I = 0x7f0a02a9

.field public static final btn_shortcut:I = 0x7f0a02ac

.field public static final btn_simulator:I = 0x7f0a02af

.field public static final btn_start:I = 0x7f0a02b1

.field public static final btn_super_vcode_validate_ok:I = 0x7f0a02b3

.field public static final btn_verify_login_next:I = 0x7f0a02ce

.field public static final btn_verify_new_email_get_verify_code:I = 0x7f0a02cf

.field public static final btn_verify_new_phone_get_verify_code:I = 0x7f0a02d0

.field public static final btn_verify_old_account_get_verify_code:I = 0x7f0a02d1

.field public static final btn_verify_phone_next:I = 0x7f0a02d2

.field public static final button:I = 0x7f0a02dd

.field public static final buttonPanel:I = 0x7f0a02e2

.field public static final cancel_action:I = 0x7f0a02ef

.field public static final cancel_button:I = 0x7f0a02f0

.field public static final card_view:I = 0x7f0a02ff

.field public static final carryVelocity:I = 0x7f0a0303

.field public static final cav_login_tips:I = 0x7f0a0304

.field public static final cb_age:I = 0x7f0a0306

.field public static final cb_contracts_check:I = 0x7f0a0313

.field public static final cb_email_login_pwd_eye:I = 0x7f0a0316

.field public static final cb_email_pwd_show:I = 0x7f0a0317

.field public static final cb_info_collect_agreement:I = 0x7f0a031a

.field public static final cb_login_agree:I = 0x7f0a031b

.field public static final cb_login_main_check:I = 0x7f0a031c

.field public static final cb_login_ways_check:I = 0x7f0a031d

.field public static final cb_mobile_number_agree:I = 0x7f0a031e

.field public static final cb_mobile_pwd_login_input_pwd:I = 0x7f0a031f

.field public static final cb_mobile_pwd_login_protocol:I = 0x7f0a0320

.field public static final cb_phone_pwd_login_eye:I = 0x7f0a0327

.field public static final cb_phone_pwd_login_pwd_eye:I = 0x7f0a0328

.field public static final cb_privacy_agreement:I = 0x7f0a032a

.field public static final cb_register_agree:I = 0x7f0a032e

.field public static final cb_register_show:I = 0x7f0a032f

.field public static final cb_reset_pwd_show:I = 0x7f0a0332

.field public static final cb_set_pwd_show:I = 0x7f0a0337

.field public static final cb_setting_pwd_pwd_eye:I = 0x7f0a0338

.field public static final cb_user_agreement:I = 0x7f0a033d

.field public static final center:I = 0x7f0a0346

.field public static final centerCrop:I = 0x7f0a0347

.field public static final centerInside:I = 0x7f0a0348

.field public static final center_horizontal:I = 0x7f0a0349

.field public static final center_vertical:I = 0x7f0a034a

.field public static final chain:I = 0x7f0a035b

.field public static final checkbox:I = 0x7f0a035f

.field public static final checked:I = 0x7f0a0363

.field public static final chronometer:I = 0x7f0a0365

.field public static final circle_center:I = 0x7f0a0366

.field public static final cl_on_convert:I = 0x7f0a03f0

.field public static final clear_text:I = 0x7f0a0464

.field public static final clip_horizontal:I = 0x7f0a0467

.field public static final clip_vertical:I = 0x7f0a0468

.field public static final clockwise:I = 0x7f0a0469

.field public static final close_ad_tv:I = 0x7f0a046b

.field public static final closest:I = 0x7f0a046c

.field public static final column:I = 0x7f0a0486

.field public static final column_reverse:I = 0x7f0a0487

.field public static final common_other_login:I = 0x7f0a049f

.field public static final compress:I = 0x7f0a04a2

.field public static final confirm_button:I = 0x7f0a04a3

.field public static final constraint:I = 0x7f0a04a5

.field public static final contact:I = 0x7f0a04ab

.field public static final container:I = 0x7f0a04ac

.field public static final content:I = 0x7f0a04ad

.field public static final contentPanel:I = 0x7f0a04ae

.field public static final content_panel:I = 0x7f0a04af

.field public static final contiguous:I = 0x7f0a04b1

.field public static final continue_button:I = 0x7f0a04b2

.field public static final continuousVelocity:I = 0x7f0a04b3

.field public static final controls_view:I = 0x7f0a04b4

.field public static final coordinator:I = 0x7f0a04b5

.field public static final cos:I = 0x7f0a04b7

.field public static final counterclockwise:I = 0x7f0a04be

.field public static final country_code:I = 0x7f0a04bf

.field public static final cover:I = 0x7f0a04c0

.field public static final cradle:I = 0x7f0a04c2

.field public static final cs_app_contentview_id:I = 0x7f0a04c3

.field public static final cs_app_floatview_id:I = 0x7f0a04c4

.field public static final cs_media_view_id:I = 0x7f0a04c5

.field public static final cs_vendor_id:I = 0x7f0a04c9

.field public static final currentState:I = 0x7f0a04ec

.field public static final custom:I = 0x7f0a04ed

.field public static final customPanel:I = 0x7f0a04ee

.field public static final custom_panel:I = 0x7f0a04ef

.field public static final cut:I = 0x7f0a04f1

.field public static final dark:I = 0x7f0a0500

.field public static final date:I = 0x7f0a0502

.field public static final date_picker_actions:I = 0x7f0a0504

.field public static final decelerate:I = 0x7f0a0507

.field public static final decelerateAndComplete:I = 0x7f0a0508

.field public static final decor_content_parent:I = 0x7f0a0509

.field public static final default_activity_button:I = 0x7f0a050a

.field public static final deltaRelative:I = 0x7f0a050b

.field public static final demote_common_words:I = 0x7f0a050c

.field public static final demote_rfc822_hostnames:I = 0x7f0a050d

.field public static final design_bottom_sheet:I = 0x7f0a0512

.field public static final design_menu_item_action_area:I = 0x7f0a0513

.field public static final design_menu_item_action_area_stub:I = 0x7f0a0514

.field public static final design_menu_item_text:I = 0x7f0a0515

.field public static final design_navigation_view:I = 0x7f0a0516

.field public static final detailImageView:I = 0x7f0a0517

.field public static final dialog_button:I = 0x7f0a0518

.field public static final dialog_view:I = 0x7f0a0528

.field public static final disjoint:I = 0x7f0a0530

.field public static final dot:I = 0x7f0a054c

.field public static final dragAnticlockwise:I = 0x7f0a0554

.field public static final dragClockwise:I = 0x7f0a0555

.field public static final dragDown:I = 0x7f0a0556

.field public static final dragEnd:I = 0x7f0a0557

.field public static final dragLeft:I = 0x7f0a0558

.field public static final dragRight:I = 0x7f0a0559

.field public static final dragStart:I = 0x7f0a055a

.field public static final dragUp:I = 0x7f0a055b

.field public static final dropdown_menu:I = 0x7f0a0561

.field public static final easeIn:I = 0x7f0a0567

.field public static final easeInOut:I = 0x7f0a0568

.field public static final easeOut:I = 0x7f0a0569

.field public static final east:I = 0x7f0a056a

.field public static final edge:I = 0x7f0a056e

.field public static final edit_query:I = 0x7f0a0573

.field public static final edit_text_view:I = 0x7f0a0575

.field public static final elastic:I = 0x7f0a057a

.field public static final email:I = 0x7f0a057c

.field public static final email_report_tv:I = 0x7f0a057d

.field public static final embed:I = 0x7f0a057e

.field public static final end:I = 0x7f0a0581

.field public static final endToStart:I = 0x7f0a0582

.field public static final end_padder:I = 0x7f0a0584

.field public static final entrance_collage:I = 0x7f0a0589

.field public static final et_area_code_confirm_phone_number:I = 0x7f0a058d

.field public static final et_bind_account:I = 0x7f0a058e

.field public static final et_change_account_add_nickname:I = 0x7f0a058f

.field public static final et_email_login_password:I = 0x7f0a0595

.field public static final et_email_login_pwd:I = 0x7f0a0596

.field public static final et_filter:I = 0x7f0a059a

.field public static final et_input_new_phone_number:I = 0x7f0a059b

.field public static final et_login_account:I = 0x7f0a05a2

.field public static final et_phone_number:I = 0x7f0a05ac

.field public static final et_phone_pwd_login:I = 0x7f0a05ad

.field public static final et_phone_pwd_login_password:I = 0x7f0a05ae

.field public static final et_setting_pwd_password:I = 0x7f0a05b2

.field public static final et_super_vcode_validate_input:I = 0x7f0a05b4

.field public static final et_verify_code:I = 0x7f0a05b8

.field public static final et_verify_code_input:I = 0x7f0a05b9

.field public static final exo_check:I = 0x7f0a05ca

.field public static final exo_icon:I = 0x7f0a05cb

.field public static final exo_main_text:I = 0x7f0a05cc

.field public static final exo_settings_listview:I = 0x7f0a05cd

.field public static final exo_sub_text:I = 0x7f0a05ce

.field public static final exo_text:I = 0x7f0a05cf

.field public static final exo_track_selection_view:I = 0x7f0a05d0

.field public static final expand_activities_button:I = 0x7f0a05d1

.field public static final expanded_menu:I = 0x7f0a05d2

.field public static final fade:I = 0x7f0a05df

.field public static final fill:I = 0x7f0a05e8

.field public static final fill_horizontal:I = 0x7f0a05ec

.field public static final fill_vertical:I = 0x7f0a05ed

.field public static final filled:I = 0x7f0a05ee

.field public static final fit:I = 0x7f0a05ef

.field public static final fitCenter:I = 0x7f0a05f0

.field public static final fitEnd:I = 0x7f0a05f1

.field public static final fitStart:I = 0x7f0a05f2

.field public static final fitXY:I = 0x7f0a05f4

.field public static final fixed:I = 0x7f0a05f5

.field public static final fixed_height:I = 0x7f0a05f6

.field public static final fixed_width:I = 0x7f0a05f7

.field public static final fl_content:I = 0x7f0a060b

.field public static final fl_edit_account_number:I = 0x7f0a0610

.field public static final fl_edit_email_login_pwd:I = 0x7f0a0612

.field public static final fl_edit_phone_pwd:I = 0x7f0a0613

.field public static final fl_email:I = 0x7f0a0614

.field public static final fl_email_input:I = 0x7f0a0615

.field public static final fl_email_login_account:I = 0x7f0a0616

.field public static final fl_email_pwd:I = 0x7f0a0617

.field public static final fl_google:I = 0x7f0a0622

.field public static final fl_ko_agreement:I = 0x7f0a062c

.field public static final fl_login_input:I = 0x7f0a062d

.field public static final fl_login_main:I = 0x7f0a062e

.field public static final fl_logo:I = 0x7f0a062f

.field public static final fl_mobile_number_input:I = 0x7f0a0630

.field public static final fl_mobile_pwd_login_input_number:I = 0x7f0a0631

.field public static final fl_mobile_pwd_login_input_pwd:I = 0x7f0a0632

.field public static final fl_phone:I = 0x7f0a0640

.field public static final fl_register_input:I = 0x7f0a0644

.field public static final fl_register_protocol:I = 0x7f0a0645

.field public static final fl_register_pwd:I = 0x7f0a0646

.field public static final fl_reset_pwd:I = 0x7f0a0647

.field public static final fl_set_pwd:I = 0x7f0a064d

.field public static final flex_end:I = 0x7f0a065e

.field public static final flex_start:I = 0x7f0a065f

.field public static final flip:I = 0x7f0a0661

.field public static final float_name:I = 0x7f0a0663

.field public static final floating:I = 0x7f0a0664

.field public static final forever:I = 0x7f0a066b

.field public static final fragment_container:I = 0x7f0a0671

.field public static final fragment_container_id:I = 0x7f0a0672

.field public static final fragment_container_view_tag:I = 0x7f0a0673

.field public static final frame_parent:I = 0x7f0a0679

.field public static final frost:I = 0x7f0a067b

.field public static final fullscreen_header:I = 0x7f0a067c

.field public static final ghost_view:I = 0x7f0a0692

.field public static final ghost_view_holder:I = 0x7f0a0693

.field public static final glide_custom_view_target_tag:I = 0x7f0a06b0

.field public static final gone:I = 0x7f0a06b3

.field public static final grid_dlgshares_other:I = 0x7f0a06ba

.field public static final grid_dlgshares_recent:I = 0x7f0a06bb

.field public static final group_divider:I = 0x7f0a06ca

.field public static final guideline:I = 0x7f0a06fc

.field public static final hardware:I = 0x7f0a0701

.field public static final header_title:I = 0x7f0a0703

.field public static final home:I = 0x7f0a0707

.field public static final honorRequest:I = 0x7f0a070a

.field public static final horizontal_only:I = 0x7f0a070f

.field public static final html:I = 0x7f0a0715

.field public static final ic_check_compliance:I = 0x7f0a0718

.field public static final icon:I = 0x7f0a071f

.field public static final icon_group:I = 0x7f0a0721

.field public static final icon_only:I = 0x7f0a0722

.field public static final icon_rv:I = 0x7f0a0723

.field public static final icon_uri:I = 0x7f0a0724

.field public static final icon_view:I = 0x7f0a0725

.field public static final ignore:I = 0x7f0a0727

.field public static final ignoreRequest:I = 0x7f0a0728

.field public static final image:I = 0x7f0a0730

.field public static final imageView:I = 0x7f0a0732

.field public static final image_progressbar:I = 0x7f0a0743

.field public static final image_view:I = 0x7f0a075c

.field public static final immediateStop:I = 0x7f0a0763

.field public static final include_edit_layout:I = 0x7f0a0771

.field public static final include_login_tips:I = 0x7f0a0779

.field public static final include_login_tips_age:I = 0x7f0a077a

.field public static final include_login_tips_info_collect:I = 0x7f0a077b

.field public static final include_login_tips_privacy:I = 0x7f0a077c

.field public static final include_login_tips_user:I = 0x7f0a077d

.field public static final included:I = 0x7f0a0787

.field public static final indeterminate:I = 0x7f0a0788

.field public static final index_entity_types:I = 0x7f0a078a

.field public static final indicator_container:I = 0x7f0a078b

.field public static final info:I = 0x7f0a078d

.field public static final instant_message:I = 0x7f0a0792

.field public static final intent_action:I = 0x7f0a0793

.field public static final intent_activity:I = 0x7f0a0794

.field public static final intent_data:I = 0x7f0a0795

.field public static final intent_data_id:I = 0x7f0a0796

.field public static final intent_extra_data:I = 0x7f0a0797

.field public static final interstitial_control_button:I = 0x7f0a0799

.field public static final interstitial_control_view:I = 0x7f0a079a

.field public static final invisible:I = 0x7f0a079b

.field public static final inward:I = 0x7f0a079c

.field public static final italic:I = 0x7f0a07ad

.field public static final item_gp_question_image:I = 0x7f0a081d

.field public static final item_gp_question_image_desc:I = 0x7f0a081e

.field public static final item_gp_question_image_desc2:I = 0x7f0a081f

.field public static final item_gp_question_image_top_dec:I = 0x7f0a0820

.field public static final item_touch_helper_previous_elevation:I = 0x7f0a0826

.field public static final iv:I = 0x7f0a082e

.field public static final iv_account_clear:I = 0x7f0a0835

.field public static final iv_ad_icon:I = 0x7f0a0837

.field public static final iv_back:I = 0x7f0a0847

.field public static final iv_bg:I = 0x7f0a0853

.field public static final iv_bg_1:I = 0x7f0a0854

.field public static final iv_bg_2:I = 0x7f0a0855

.field public static final iv_cancel_account:I = 0x7f0a0866

.field public static final iv_change_existed_account_cancel:I = 0x7f0a0871

.field public static final iv_clear:I = 0x7f0a0876

.field public static final iv_close:I = 0x7f0a087d

.field public static final iv_close_protocol:I = 0x7f0a0883

.field public static final iv_convert:I = 0x7f0a0899

.field public static final iv_email_clear:I = 0x7f0a08d3

.field public static final iv_email_login:I = 0x7f0a08d4

.field public static final iv_email_pwd_back:I = 0x7f0a08d5

.field public static final iv_flag:I = 0x7f0a08ef

.field public static final iv_forget_pwd_back:I = 0x7f0a08fa

.field public static final iv_google_login:I = 0x7f0a091a

.field public static final iv_guide_close:I = 0x7f0a0924

.field public static final iv_guide_intro:I = 0x7f0a0925

.field public static final iv_icon:I = 0x7f0a0931

.field public static final iv_img:I = 0x7f0a093c

.field public static final iv_introduce:I = 0x7f0a0945

.field public static final iv_last_login_close:I = 0x7f0a095b

.field public static final iv_last_login_user_icon:I = 0x7f0a095c

.field public static final iv_login_clear:I = 0x7f0a096f

.field public static final iv_login_close:I = 0x7f0a0970

.field public static final iv_login_ways_back:I = 0x7f0a0971

.field public static final iv_main_image:I = 0x7f0a0977

.field public static final iv_mask:I = 0x7f0a0981

.field public static final iv_mobile_number_clear:I = 0x7f0a0993

.field public static final iv_mobile_number_close:I = 0x7f0a0994

.field public static final iv_mobile_pwd_login_close:I = 0x7f0a0995

.field public static final iv_mobile_pwd_login_input_number_clear:I = 0x7f0a0996

.field public static final iv_naire_1:I = 0x7f0a099d

.field public static final iv_naire_2:I = 0x7f0a099e

.field public static final iv_naire_3:I = 0x7f0a099f

.field public static final iv_naire_4:I = 0x7f0a09a0

.field public static final iv_naire_5:I = 0x7f0a09a1

.field public static final iv_new_flag:I = 0x7f0a09a3

.field public static final iv_one_login_auth_back:I = 0x7f0a09ba

.field public static final iv_one_login_auth_logo:I = 0x7f0a09bb

.field public static final iv_one_login_auth_mail:I = 0x7f0a09bc

.field public static final iv_one_login_auth_mobile:I = 0x7f0a09bd

.field public static final iv_one_login_auth_wechat:I = 0x7f0a09be

.field public static final iv_one_login_half_close:I = 0x7f0a09bf

.field public static final iv_one_login_half_close_for_login:I = 0x7f0a09c0

.field public static final iv_one_login_return_id:I = 0x7f0a09c1

.field public static final iv_premium_icon:I = 0x7f0a09ea

.field public static final iv_pwd_login_over_five_cancel:I = 0x7f0a09f1

.field public static final iv_pwd_login_over_three_cancel:I = 0x7f0a09f2

.field public static final iv_register_back:I = 0x7f0a0a02

.field public static final iv_register_clear:I = 0x7f0a0a03

.field public static final iv_reset_pwd_back:I = 0x7f0a0a07

.field public static final iv_show_thumb:I = 0x7f0a0a3c

.field public static final iv_squared_icon:I = 0x7f0a0a41

.field public static final iv_super_vcode_over_five_cancel:I = 0x7f0a0a4a

.field public static final iv_super_vcode_over_there_cancel:I = 0x7f0a0a4b

.field public static final iv_top_banner:I = 0x7f0a0a72

.field public static final iv_verify_code_input_back:I = 0x7f0a0aa7

.field public static final iv_vip:I = 0x7f0a0aab

.field public static final iv_waring:I = 0x7f0a0abe

.field public static final iv_web_refresh:I = 0x7f0a0ac4

.field public static final iv_wechat_login:I = 0x7f0a0ac6

.field public static final jumpToEnd:I = 0x7f0a0ad6

.field public static final jumpToStart:I = 0x7f0a0ad7

.field public static final l_code:I = 0x7f0a0adf

.field public static final label_title:I = 0x7f0a0af9

.field public static final labeled:I = 0x7f0a0afa

.field public static final large_icon_uri:I = 0x7f0a0afd

.field public static final lav_loading:I = 0x7f0a0b00

.field public static final layout:I = 0x7f0a0b03

.field public static final layout_activation_code:I = 0x7f0a0b08

.field public static final layout_bottom:I = 0x7f0a0b0d

.field public static final layout_change_account_change_account:I = 0x7f0a0b0f

.field public static final layout_change_account_modify_nickname:I = 0x7f0a0b10

.field public static final layout_verify_phone_country:I = 0x7f0a0b35

.field public static final layout_verify_phone_num:I = 0x7f0a0b36

.field public static final learn_more_button:I = 0x7f0a0b3e

.field public static final left:I = 0x7f0a0b3f

.field public static final leftToRight:I = 0x7f0a0b40

.field public static final left_bottom:I = 0x7f0a0b41

.field public static final legacy:I = 0x7f0a0b44

.field public static final light:I = 0x7f0a0b45

.field public static final line1:I = 0x7f0a0b47

.field public static final line3:I = 0x7f0a0b4a

.field public static final linear:I = 0x7f0a0b54

.field public static final listMode:I = 0x7f0a0b59

.field public static final listView:I = 0x7f0a0b5a

.field public static final list_item:I = 0x7f0a0b5c

.field public static final list_panel:I = 0x7f0a0b5e

.field public static final ll_age:I = 0x7f0a0b6b

.field public static final ll_bind_select_county_code:I = 0x7f0a0b7d

.field public static final ll_btn:I = 0x7f0a0b8e

.field public static final ll_cancel_account:I = 0x7f0a0b94

.field public static final ll_cancel_warning:I = 0x7f0a0b95

.field public static final ll_change_account_account_entire_container:I = 0x7f0a0ba7

.field public static final ll_common_other_login_way:I = 0x7f0a0bb6

.field public static final ll_content:I = 0x7f0a0bbb

.field public static final ll_content_title:I = 0x7f0a0bbd

.field public static final ll_contracts:I = 0x7f0a0bbf

.field public static final ll_detail_des_container:I = 0x7f0a0bcc

.field public static final ll_dialog_result_bottom:I = 0x7f0a0bcf

.field public static final ll_email_protocol:I = 0x7f0a0bee

.field public static final ll_func:I = 0x7f0a0c04

.field public static final ll_info_collect_agreement:I = 0x7f0a0c31

.field public static final ll_input_root:I = 0x7f0a0c33

.field public static final ll_ko_agree:I = 0x7f0a0c42

.field public static final ll_login_contracts:I = 0x7f0a0c45

.field public static final ll_login_main_google_login_title:I = 0x7f0a0c46

.field public static final ll_login_main_other_login_title:I = 0x7f0a0c47

.field public static final ll_login_select_county_code:I = 0x7f0a0c48

.field public static final ll_login_ways_other_login_title:I = 0x7f0a0c4a

.field public static final ll_login_ways_protocol_with_cb:I = 0x7f0a0c4b

.field public static final ll_mobile_number_protocol:I = 0x7f0a0c59

.field public static final ll_mobile_pwd_login_protocol:I = 0x7f0a0c5a

.field public static final ll_normal:I = 0x7f0a0c63

.field public static final ll_one_login_auth_more_login_way:I = 0x7f0a0c70

.field public static final ll_one_login_auth_more_login_ways:I = 0x7f0a0c71

.field public static final ll_other_login_ways:I = 0x7f0a0c75

.field public static final ll_phone_pwd_login_area_code_container:I = 0x7f0a0c9b

.field public static final ll_privacy_agreement:I = 0x7f0a0ca5

.field public static final ll_protocol_korea:I = 0x7f0a0ca9

.field public static final ll_reason_container:I = 0x7f0a0cb3

.field public static final ll_root:I = 0x7f0a0cc0

.field public static final ll_set_pwd:I = 0x7f0a0cd7

.field public static final ll_user_agreement:I = 0x7f0a0d16

.field public static final ll_verify_root:I = 0x7f0a0d18

.field public static final lottie_layer_name:I = 0x7f0a0d56

.field public static final m3_side_sheet:I = 0x7f0a0d64

.field public static final main_container:I = 0x7f0a0d70

.field public static final main_fragment_id:I = 0x7f0a0d72

.field public static final marquee:I = 0x7f0a0d80

.field public static final masked:I = 0x7f0a0d89

.field public static final massage_blank:I = 0x7f0a0d8a

.field public static final match_constraint:I = 0x7f0a0d8b

.field public static final match_global_nicknames:I = 0x7f0a0d8c

.field public static final match_parent:I = 0x7f0a0d8d

.field public static final material_clock_display:I = 0x7f0a0d8e

.field public static final material_clock_display_and_toggle:I = 0x7f0a0d8f

.field public static final material_clock_face:I = 0x7f0a0d90

.field public static final material_clock_hand:I = 0x7f0a0d91

.field public static final material_clock_level:I = 0x7f0a0d92

.field public static final material_clock_period_am_button:I = 0x7f0a0d93

.field public static final material_clock_period_pm_button:I = 0x7f0a0d94

.field public static final material_clock_period_toggle:I = 0x7f0a0d95

.field public static final material_hour_text_input:I = 0x7f0a0d96

.field public static final material_hour_tv:I = 0x7f0a0d97

.field public static final material_label:I = 0x7f0a0d98

.field public static final material_minute_text_input:I = 0x7f0a0d99

.field public static final material_minute_tv:I = 0x7f0a0d9a

.field public static final material_textinput_timepicker:I = 0x7f0a0d9b

.field public static final material_timepicker_cancel_button:I = 0x7f0a0d9c

.field public static final material_timepicker_container:I = 0x7f0a0d9d

.field public static final material_timepicker_mode_button:I = 0x7f0a0d9e

.field public static final material_timepicker_ok_button:I = 0x7f0a0d9f

.field public static final material_timepicker_view:I = 0x7f0a0da0

.field public static final material_value_index:I = 0x7f0a0da1

.field public static final matrix:I = 0x7f0a0da2

.field public static final media_actions:I = 0x7f0a0da4

.field public static final menu_list:I = 0x7f0a0da8

.field public static final menu_title:I = 0x7f0a0dad

.field public static final message:I = 0x7f0a0daf

.field public static final message_close:I = 0x7f0a0db0

.field public static final message_close_test_time:I = 0x7f0a0db1

.field public static final message_content:I = 0x7f0a0db2

.field public static final message_icon:I = 0x7f0a0db3

.field public static final message_panel:I = 0x7f0a0db4

.field public static final message_root:I = 0x7f0a0db5

.field public static final message_textview:I = 0x7f0a0db6

.field public static final middle:I = 0x7f0a0db8

.field public static final mini:I = 0x7f0a0dbd

.field public static final month_grid:I = 0x7f0a0dc3

.field public static final month_navigation_bar:I = 0x7f0a0dc4

.field public static final month_navigation_fragment_toggle:I = 0x7f0a0dc5

.field public static final month_navigation_next:I = 0x7f0a0dc6

.field public static final month_navigation_previous:I = 0x7f0a0dc7

.field public static final month_title:I = 0x7f0a0dc8

.field public static final motion_base:I = 0x7f0a0dc9

.field public static final mrec_ad_view_container:I = 0x7f0a0dcd

.field public static final mrec_control_button:I = 0x7f0a0dce

.field public static final mrec_control_view:I = 0x7f0a0dcf

.field public static final mtrl_anchor_parent:I = 0x7f0a0dd1

.field public static final mtrl_calendar_day_selector_frame:I = 0x7f0a0dd2

.field public static final mtrl_calendar_days_of_week:I = 0x7f0a0dd3

.field public static final mtrl_calendar_frame:I = 0x7f0a0dd4

.field public static final mtrl_calendar_main_pane:I = 0x7f0a0dd5

.field public static final mtrl_calendar_months:I = 0x7f0a0dd6

.field public static final mtrl_calendar_selection_frame:I = 0x7f0a0dd7

.field public static final mtrl_calendar_text_input_frame:I = 0x7f0a0dd8

.field public static final mtrl_calendar_year_selector_frame:I = 0x7f0a0dd9

.field public static final mtrl_card_checked_layer_id:I = 0x7f0a0dda

.field public static final mtrl_child_content_container:I = 0x7f0a0ddb

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a0ddc

.field public static final mtrl_motion_snapshot_view:I = 0x7f0a0ddd

.field public static final mtrl_picker_fullscreen:I = 0x7f0a0dde

.field public static final mtrl_picker_header:I = 0x7f0a0ddf

.field public static final mtrl_picker_header_selection_text:I = 0x7f0a0de0

.field public static final mtrl_picker_header_title_and_selection:I = 0x7f0a0de1

.field public static final mtrl_picker_header_toggle:I = 0x7f0a0de2

.field public static final mtrl_picker_text_input_date:I = 0x7f0a0de3

.field public static final mtrl_picker_text_input_range_end:I = 0x7f0a0de4

.field public static final mtrl_picker_text_input_range_start:I = 0x7f0a0de5

.field public static final mtrl_picker_title_text:I = 0x7f0a0de6

.field public static final mtrl_view_tag_bottom_padding:I = 0x7f0a0de7

.field public static final multiply:I = 0x7f0a0dea

.field public static final native_ad_view_container:I = 0x7f0a0def

.field public static final native_control_button:I = 0x7f0a0df0

.field public static final native_control_view:I = 0x7f0a0df1

.field public static final navigation_bar_item_active_indicator_view:I = 0x7f0a0df2

.field public static final navigation_bar_item_icon_container:I = 0x7f0a0df3

.field public static final navigation_bar_item_icon_view:I = 0x7f0a0df4

.field public static final navigation_bar_item_labels_group:I = 0x7f0a0df5

.field public static final navigation_bar_item_large_label_view:I = 0x7f0a0df6

.field public static final navigation_bar_item_small_label_view:I = 0x7f0a0df7

.field public static final navigation_header_container:I = 0x7f0a0df8

.field public static final never:I = 0x7f0a0dfc

.field public static final neverCompleteToEnd:I = 0x7f0a0dfd

.field public static final neverCompleteToStart:I = 0x7f0a0dfe

.field public static final new_toolbar:I = 0x7f0a0e02

.field public static final noState:I = 0x7f0a0e05

.field public static final none:I = 0x7f0a0e06

.field public static final normal:I = 0x7f0a0e07

.field public static final north:I = 0x7f0a0e0e

.field public static final notification_background:I = 0x7f0a0e10

.field public static final notification_main_column:I = 0x7f0a0e11

.field public static final notification_main_column_container:I = 0x7f0a0e12

.field public static final nowrap:I = 0x7f0a0e15

.field public static final off:I = 0x7f0a0e29

.field public static final omnibox_title_section:I = 0x7f0a0e2c

.field public static final omnibox_url_section:I = 0x7f0a0e2d

.field public static final on:I = 0x7f0a0e2e

.field public static final outline:I = 0x7f0a0e3e

.field public static final outward:I = 0x7f0a0e3f

.field public static final overshoot:I = 0x7f0a0e40

.field public static final packed:I = 0x7f0a0e41

.field public static final page_list_ad_id:I = 0x7f0a0e48

.field public static final parallax:I = 0x7f0a0e4f

.field public static final parent:I = 0x7f0a0e50

.field public static final parentPanel:I = 0x7f0a0e51

.field public static final parentRelative:I = 0x7f0a0e52

.field public static final parent_matrix:I = 0x7f0a0e53

.field public static final partner_links_textview:I = 0x7f0a0e54

.field public static final partners_content_view:I = 0x7f0a0e55

.field public static final password_toggle:I = 0x7f0a0e57

.field public static final path:I = 0x7f0a0e58

.field public static final pathRelative:I = 0x7f0a0e59

.field public static final pb_loading:I = 0x7f0a0e5f

.field public static final percent:I = 0x7f0a0e78

.field public static final personalized_advertising_switch:I = 0x7f0a0e7b

.field public static final personalized_advertising_switch_textview:I = 0x7f0a0e7c

.field public static final pgbar_progress:I = 0x7f0a0e7d

.field public static final pin:I = 0x7f0a0e81

.field public static final plain:I = 0x7f0a0e86

.field public static final popup_menu_listview:I = 0x7f0a0e88

.field public static final position:I = 0x7f0a0e89

.field public static final postLayout:I = 0x7f0a0e8a

.field public static final pressed:I = 0x7f0a0e8f

.field public static final privacy_policy_switch:I = 0x7f0a0e94

.field public static final privacy_policy_switch_textview:I = 0x7f0a0e95

.field public static final progress:I = 0x7f0a0e96

.field public static final progressBar:I = 0x7f0a0e97

.field public static final progressNum:I = 0x7f0a0e98

.field public static final progress_circular:I = 0x7f0a0e9a

.field public static final progress_horizontal:I = 0x7f0a0e9b

.field public static final progress_number:I = 0x7f0a0e9c

.field public static final progress_percent:I = 0x7f0a0e9d

.field public static final protocol_korea:I = 0x7f0a0e9f

.field public static final purchase_viewpager:I = 0x7f0a0ebe

.field public static final purchase_viewpager_dots:I = 0x7f0a0ebf

.field public static final radio:I = 0x7f0a0ece

.field public static final rectangles:I = 0x7f0a0f0f

.field public static final regis_country_name:I = 0x7f0a0f2a

.field public static final rename_dialog_edit:I = 0x7f0a0f2c

.field public static final report_ad_button:I = 0x7f0a0f2d

.field public static final report_ad_tv:I = 0x7f0a0f2e

.field public static final report_complain:I = 0x7f0a0f2f

.field public static final restart:I = 0x7f0a0f30

.field public static final reverse:I = 0x7f0a0f34

.field public static final reverseSawtooth:I = 0x7f0a0f35

.field public static final reward_video_view:I = 0x7f0a0f3c

.field public static final rewarded_control_button:I = 0x7f0a0f3d

.field public static final rewarded_control_view:I = 0x7f0a0f3e

.field public static final rewarded_interstitial_control_button:I = 0x7f0a0f3f

.field public static final rewarded_interstitial_control_view:I = 0x7f0a0f40

.field public static final rfc822:I = 0x7f0a0f41

.field public static final right:I = 0x7f0a0f4f

.field public static final rightToLeft:I = 0x7f0a0f50

.field public static final right_icon:I = 0x7f0a0f51

.field public static final right_side:I = 0x7f0a0f53

.field public static final rl_ad_item:I = 0x7f0a0f67

.field public static final rl_bottom:I = 0x7f0a0f6f

.field public static final rl_cs_loading_root:I = 0x7f0a0f82

.field public static final rl_email_pwd_root:I = 0x7f0a0f8d

.field public static final rl_forget_pwd:I = 0x7f0a0fa0

.field public static final rl_google_login:I = 0x7f0a0fa5

.field public static final rl_guide:I = 0x7f0a0fa9

.field public static final rl_header:I = 0x7f0a0fae

.field public static final rl_input_root:I = 0x7f0a0fb2

.field public static final rl_last_login:I = 0x7f0a0fb4

.field public static final rl_login_main:I = 0x7f0a0fb7

.field public static final rl_login_main_google_login:I = 0x7f0a0fb8

.field public static final rl_login_main_other_login:I = 0x7f0a0fb9

.field public static final rl_mobile_area_code:I = 0x7f0a0fbf

.field public static final rl_mobile_pwd_login_area_code:I = 0x7f0a0fc0

.field public static final rl_one_login_auth_title_bar:I = 0x7f0a0fc9

.field public static final rl_skip_container:I = 0x7f0a0fee

.field public static final rl_web_custom_root:I = 0x7f0a0ffd

.field public static final rl_web_fail_root:I = 0x7f0a0ffe

.field public static final rl_web_root:I = 0x7f0a0fff

.field public static final root_rotate:I = 0x7f0a1007

.field public static final rounded:I = 0x7f0a1012

.field public static final row:I = 0x7f0a1013

.field public static final row_index_key:I = 0x7f0a1014

.field public static final row_reverse:I = 0x7f0a1015

.field public static final rv_main:I = 0x7f0a1042

.field public static final rv_main_view_container:I = 0x7f0a1044

.field public static final save_non_transition_alpha:I = 0x7f0a107c

.field public static final save_overlay_view:I = 0x7f0a107d

.field public static final sawtooth:I = 0x7f0a107e

.field public static final scale:I = 0x7f0a1093

.field public static final screen:I = 0x7f0a1096

.field public static final scrollIndicatorDown:I = 0x7f0a1099

.field public static final scrollIndicatorUp:I = 0x7f0a109a

.field public static final scrollView:I = 0x7f0a109b

.field public static final scroll_view:I = 0x7f0a10a1

.field public static final scrollable:I = 0x7f0a10a3

.field public static final search_badge:I = 0x7f0a10a7

.field public static final search_bar:I = 0x7f0a10a8

.field public static final search_bar_text_view:I = 0x7f0a10a9

.field public static final search_button:I = 0x7f0a10aa

.field public static final search_close_btn:I = 0x7f0a10ab

.field public static final search_edit_frame:I = 0x7f0a10ac

.field public static final search_go_btn:I = 0x7f0a10ad

.field public static final search_input_box:I = 0x7f0a10ae

.field public static final search_mag_icon:I = 0x7f0a10af

.field public static final search_plate:I = 0x7f0a10b0

.field public static final search_src_text:I = 0x7f0a10b1

.field public static final search_view_background:I = 0x7f0a10b2

.field public static final search_view_clear_button:I = 0x7f0a10b3

.field public static final search_view_content_container:I = 0x7f0a10b4

.field public static final search_view_divider:I = 0x7f0a10b5

.field public static final search_view_dummy_toolbar:I = 0x7f0a10b6

.field public static final search_view_edit_text:I = 0x7f0a10b7

.field public static final search_view_header_container:I = 0x7f0a10b8

.field public static final search_view_root:I = 0x7f0a10b9

.field public static final search_view_scrim:I = 0x7f0a10ba

.field public static final search_view_search_prefix:I = 0x7f0a10bb

.field public static final search_view_status_bar_spacer:I = 0x7f0a10bc

.field public static final search_view_toolbar:I = 0x7f0a10bd

.field public static final search_view_toolbar_container:I = 0x7f0a10be

.field public static final search_voice_btn:I = 0x7f0a10bf

.field public static final sectioning_adapter_tag_key_view_viewholder:I = 0x7f0a10c1

.field public static final select_dialog_listview:I = 0x7f0a10c3

.field public static final selected:I = 0x7f0a10c6

.field public static final selection_type:I = 0x7f0a10c7

.field public static final sep_recent_other:I = 0x7f0a10cd

.field public static final sharedValueSet:I = 0x7f0a10d2

.field public static final sharedValueUnset:I = 0x7f0a10d3

.field public static final shortcut:I = 0x7f0a10d4

.field public static final show_mrec_button:I = 0x7f0a10d8

.field public static final show_native_button:I = 0x7f0a10d9

.field public static final sign_in_btn_with_google:I = 0x7f0a10df

.field public static final sin:I = 0x7f0a10e4

.field public static final skipped:I = 0x7f0a10e8

.field public static final slide:I = 0x7f0a10ea

.field public static final snackbar_action:I = 0x7f0a10ee

.field public static final snackbar_text:I = 0x7f0a10ef

.field public static final software:I = 0x7f0a10f2

.field public static final south:I = 0x7f0a10f3

.field public static final space_around:I = 0x7f0a10f8

.field public static final space_between:I = 0x7f0a10fb

.field public static final space_evenly:I = 0x7f0a1105

.field public static final spacer:I = 0x7f0a1111

.field public static final special_effects_controller_view_tag:I = 0x7f0a1112

.field public static final spherical_gl_surface_view:I = 0x7f0a1113

.field public static final spline:I = 0x7f0a1115

.field public static final split_action_bar:I = 0x7f0a1116

.field public static final spread:I = 0x7f0a1117

.field public static final spread_inside:I = 0x7f0a1118

.field public static final spring:I = 0x7f0a1119

.field public static final square:I = 0x7f0a111a

.field public static final src_atop:I = 0x7f0a111b

.field public static final src_in:I = 0x7f0a111c

.field public static final src_over:I = 0x7f0a111d

.field public static final standard:I = 0x7f0a111e

.field public static final start:I = 0x7f0a111f

.field public static final startHorizontal:I = 0x7f0a1120

.field public static final startToEnd:I = 0x7f0a1121

.field public static final startVertical:I = 0x7f0a1122

.field public static final staticLayout:I = 0x7f0a1125

.field public static final staticPostLayout:I = 0x7f0a1126

.field public static final status_bar_latest_event_content:I = 0x7f0a112a

.field public static final status_textview:I = 0x7f0a112b

.field public static final stop:I = 0x7f0a1135

.field public static final stretch:I = 0x7f0a1136

.field public static final submenuarrow:I = 0x7f0a1154

.field public static final submit_area:I = 0x7f0a1155

.field public static final surface_view:I = 0x7f0a1158

.field public static final switch_auto_open_website:I = 0x7f0a1161

.field public static final syncIcon:I = 0x7f0a1170

.field public static final tabMode:I = 0x7f0a1174

.field public static final tag_accessibility_actions:I = 0x7f0a1180

.field public static final tag_accessibility_clickable_spans:I = 0x7f0a1181

.field public static final tag_accessibility_heading:I = 0x7f0a1182

.field public static final tag_accessibility_pane_title:I = 0x7f0a1183

.field public static final tag_ad_id:I = 0x7f0a1184

.field public static final tag_doc_tencent_id:I = 0x7f0a1187

.field public static final tag_feed_back_id:I = 0x7f0a1188

.field public static final tag_on_apply_window_listener:I = 0x7f0a118c

.field public static final tag_on_receive_content_listener:I = 0x7f0a118d

.field public static final tag_on_receive_content_mime_types:I = 0x7f0a118e

.field public static final tag_screen_reader_focusable:I = 0x7f0a118f

.field public static final tag_state_description:I = 0x7f0a1191

.field public static final tag_transition_group:I = 0x7f0a1193

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a1194

.field public static final tag_unhandled_key_listeners:I = 0x7f0a1195

.field public static final tag_window_insets_animation_callback:I = 0x7f0a1197

.field public static final text:I = 0x7f0a119d

.field public static final text1:I = 0x7f0a119e

.field public static final text2:I = 0x7f0a119f

.field public static final textSpacerNoButtons:I = 0x7f0a11a3

.field public static final textSpacerNoTitle:I = 0x7f0a11a4

.field public static final textView:I = 0x7f0a11a8

.field public static final text_input_end_icon:I = 0x7f0a11b6

.field public static final text_input_error_icon:I = 0x7f0a11b7

.field public static final text_input_start_icon:I = 0x7f0a11b8

.field public static final textinput_counter:I = 0x7f0a11bd

.field public static final textinput_error:I = 0x7f0a11be

.field public static final textinput_helper_text:I = 0x7f0a11bf

.field public static final textinput_placeholder:I = 0x7f0a11c0

.field public static final textinput_prefix_text:I = 0x7f0a11c1

.field public static final textinput_suffix_text:I = 0x7f0a11c2

.field public static final texture_view:I = 0x7f0a11c4

.field public static final thing_proto:I = 0x7f0a11c5

.field public static final time:I = 0x7f0a11c8

.field public static final title:I = 0x7f0a11d1

.field public static final titleDividerNoCustom:I = 0x7f0a11d2

.field public static final title_container:I = 0x7f0a11d3

.field public static final title_panel:I = 0x7f0a11d4

.field public static final title_template:I = 0x7f0a11d6

.field public static final title_textview:I = 0x7f0a11d7

.field public static final toolbar:I = 0x7f0a11df

.field public static final toolbar_menu_container:I = 0x7f0a11e5

.field public static final toolbar_title:I = 0x7f0a11eb

.field public static final toolbar_title_container_layout:I = 0x7f0a11ec

.field public static final toolbar_title_layout:I = 0x7f0a11ed

.field public static final top:I = 0x7f0a11ee

.field public static final topPanel:I = 0x7f0a11ef

.field public static final touch_outside:I = 0x7f0a11fc

.field public static final transition_current_scene:I = 0x7f0a1200

.field public static final transition_layout_save:I = 0x7f0a1202

.field public static final transition_position:I = 0x7f0a1205

.field public static final transition_scene_layoutid_cache:I = 0x7f0a1206

.field public static final transition_transform:I = 0x7f0a1207

.field public static final triangle:I = 0x7f0a1211

.field public static final tt_id_area_rect_info:I = 0x7f0a1214

.field public static final tt_id_click_area_id:I = 0x7f0a1215

.field public static final tt_id_click_area_type:I = 0x7f0a1216

.field public static final tt_id_click_begin:I = 0x7f0a1217

.field public static final tt_id_click_end:I = 0x7f0a1218

.field public static final tt_id_click_tag:I = 0x7f0a1219

.field public static final tt_id_direction:I = 0x7f0a121a

.field public static final tt_id_is_video_picture:I = 0x7f0a121b

.field public static final tt_id_open_landing_page:I = 0x7f0a121c

.field public static final tt_id_ripple_bg:I = 0x7f0a121d

.field public static final tt_id_root_web_view:I = 0x7f0a121e

.field public static final tt_id_shine_width:I = 0x7f0a121f

.field public static final tt_id_width:I = 0x7f0a1220

.field public static final tv:I = 0x7f0a1221

.field public static final tv_0:I = 0x7f0a1227

.field public static final tv_1:I = 0x7f0a1228

.field public static final tv_2:I = 0x7f0a122b

.field public static final tv_3:I = 0x7f0a1230

.field public static final tv_4:I = 0x7f0a1232

.field public static final tv_5:I = 0x7f0a1235

.field public static final tv_a_key_login_account:I = 0x7f0a1239

.field public static final tv_a_key_login_email:I = 0x7f0a123a

.field public static final tv_a_key_login_error_msg:I = 0x7f0a123b

.field public static final tv_a_key_login_pwd_login:I = 0x7f0a123c

.field public static final tv_a_key_login_wechat:I = 0x7f0a123d

.field public static final tv_account:I = 0x7f0a1240

.field public static final tv_account_des:I = 0x7f0a1241

.field public static final tv_account_disable:I = 0x7f0a1242

.field public static final tv_account_google_desc:I = 0x7f0a1243

.field public static final tv_account_warning:I = 0x7f0a1246

.field public static final tv_action:I = 0x7f0a1248

.field public static final tv_ad:I = 0x7f0a124a

.field public static final tv_ad_policy:I = 0x7f0a124d

.field public static final tv_age_des:I = 0x7f0a125c

.field public static final tv_agree_protocol:I = 0x7f0a125f

.field public static final tv_alert:I = 0x7f0a1266

.field public static final tv_area_code_confirm_area_code:I = 0x7f0a1274

.field public static final tv_area_code_confirm_area_code_name:I = 0x7f0a1275

.field public static final tv_area_code_confirm_error_msg:I = 0x7f0a1276

.field public static final tv_auto_open_website:I = 0x7f0a127b

.field public static final tv_base:I = 0x7f0a1290

.field public static final tv_bind_exist_account:I = 0x7f0a1297

.field public static final tv_bind_intro:I = 0x7f0a1299

.field public static final tv_btn_feedback:I = 0x7f0a12af

.field public static final tv_cancel_account:I = 0x7f0a12bf

.field public static final tv_cancel_account_des:I = 0x7f0a12c0

.field public static final tv_cancel_convert:I = 0x7f0a12c3

.field public static final tv_change_account_account:I = 0x7f0a12db

.field public static final tv_change_account_account_type:I = 0x7f0a12dc

.field public static final tv_change_account_bottom_desc:I = 0x7f0a12dd

.field public static final tv_change_account_change_account:I = 0x7f0a12de

.field public static final tv_change_account_modify_nickname:I = 0x7f0a12df

.field public static final tv_change_existed_account_contact_us:I = 0x7f0a12e1

.field public static final tv_change_existed_account_msg:I = 0x7f0a12e2

.field public static final tv_change_existed_account_try_later:I = 0x7f0a12e3

.field public static final tv_change_login_mode:I = 0x7f0a12e7

.field public static final tv_choose_country_code:I = 0x7f0a12ef

.field public static final tv_chose_img:I = 0x7f0a12f0

.field public static final tv_clarify_content_title:I = 0x7f0a12f3

.field public static final tv_commit_account_cancel:I = 0x7f0a1311

.field public static final tv_confirm:I = 0x7f0a131a

.field public static final tv_contact_us:I = 0x7f0a131f

.field public static final tv_content_fifth:I = 0x7f0a1322

.field public static final tv_content_first:I = 0x7f0a1323

.field public static final tv_content_fourth:I = 0x7f0a1324

.field public static final tv_content_second:I = 0x7f0a1326

.field public static final tv_content_sixth:I = 0x7f0a1327

.field public static final tv_content_subtitle:I = 0x7f0a1328

.field public static final tv_content_third:I = 0x7f0a1329

.field public static final tv_contracts_link:I = 0x7f0a1330

.field public static final tv_convert:I = 0x7f0a1333

.field public static final tv_convert_des:I = 0x7f0a1335

.field public static final tv_convert_result_des:I = 0x7f0a1336

.field public static final tv_convert_result_title:I = 0x7f0a1337

.field public static final tv_cover_num:I = 0x7f0a1346

.field public static final tv_des:I = 0x7f0a136d

.field public static final tv_dialog_enable:I = 0x7f0a1381

.field public static final tv_dialog_result_cancel:I = 0x7f0a1382

.field public static final tv_dialog_result_description:I = 0x7f0a1383

.field public static final tv_dialog_result_know:I = 0x7f0a1384

.field public static final tv_dialog_result_merge:I = 0x7f0a1385

.field public static final tv_dialog_result_subtitle:I = 0x7f0a1386

.field public static final tv_dialog_result_title:I = 0x7f0a1387

.field public static final tv_dialog_title:I = 0x7f0a1388

.field public static final tv_dialog_unable:I = 0x7f0a1398

.field public static final tv_done:I = 0x7f0a13c6

.field public static final tv_email_forget_pwd:I = 0x7f0a13e0

.field public static final tv_email_input:I = 0x7f0a13e1

.field public static final tv_email_login:I = 0x7f0a13e2

.field public static final tv_email_login_account:I = 0x7f0a13e3

.field public static final tv_email_login_email:I = 0x7f0a13e4

.field public static final tv_email_login_error_msg:I = 0x7f0a13e5

.field public static final tv_email_login_forget_password:I = 0x7f0a13e6

.field public static final tv_email_login_title:I = 0x7f0a13e7

.field public static final tv_email_pwd:I = 0x7f0a13e8

.field public static final tv_email_pwd_error:I = 0x7f0a13e9

.field public static final tv_email_pwd_other_login_ways:I = 0x7f0a13ea

.field public static final tv_email_pwd_title:I = 0x7f0a13eb

.field public static final tv_email_register:I = 0x7f0a13ec

.field public static final tv_email_register_title:I = 0x7f0a13ed

.field public static final tv_error_msg:I = 0x7f0a13f9

.field public static final tv_fail_title:I = 0x7f0a1421

.field public static final tv_feed_back_more:I = 0x7f0a1428

.field public static final tv_find_pwd:I = 0x7f0a143d

.field public static final tv_first_login_create_hint:I = 0x7f0a1443

.field public static final tv_flag:I = 0x7f0a1448

.field public static final tv_forget_pwd_account:I = 0x7f0a1455

.field public static final tv_forget_pwd_area_code:I = 0x7f0a1456

.field public static final tv_forget_pwd_contact_us:I = 0x7f0a1457

.field public static final tv_forget_pwd_error:I = 0x7f0a1458

.field public static final tv_forget_pwd_error_msg:I = 0x7f0a1459

.field public static final tv_forget_pwd_get_vcode:I = 0x7f0a145a

.field public static final tv_forget_pwd_title:I = 0x7f0a145b

.field public static final tv_get_vcode:I = 0x7f0a148c

.field public static final tv_go_to_setting:I = 0x7f0a14a4

.field public static final tv_guide_intro:I = 0x7f0a14ca

.field public static final tv_guide_login:I = 0x7f0a14cb

.field public static final tv_hint:I = 0x7f0a14da

.field public static final tv_i_know:I = 0x7f0a14e7

.field public static final tv_info:I = 0x7f0a14fc

.field public static final tv_info_collect_agreement_desc:I = 0x7f0a14fd

.field public static final tv_item_bind_account_action:I = 0x7f0a1519

.field public static final tv_item_bind_account_subtitle:I = 0x7f0a151a

.field public static final tv_item_bind_account_title:I = 0x7f0a151b

.field public static final tv_last_intro:I = 0x7f0a1550

.field public static final tv_last_login:I = 0x7f0a1551

.field public static final tv_last_login_nick_name:I = 0x7f0a1552

.field public static final tv_last_login_other_login_ways:I = 0x7f0a1553

.field public static final tv_last_login_tips:I = 0x7f0a1554

.field public static final tv_left:I = 0x7f0a155a

.field public static final tv_license_tips:I = 0x7f0a1567

.field public static final tv_login_account:I = 0x7f0a1586

.field public static final tv_login_des:I = 0x7f0a1587

.field public static final tv_login_intro:I = 0x7f0a1588

.field public static final tv_login_main_account:I = 0x7f0a1589

.field public static final tv_login_main_contracts_link:I = 0x7f0a158a

.field public static final tv_login_main_error_msg:I = 0x7f0a158b

.field public static final tv_login_main_last_login_tips:I = 0x7f0a158c

.field public static final tv_login_main_we_chat:I = 0x7f0a158d

.field public static final tv_login_mobile:I = 0x7f0a158e

.field public static final tv_login_protocol:I = 0x7f0a1591

.field public static final tv_login_ways_contracts_link:I = 0x7f0a1592

.field public static final tv_login_ways_last_login_tips:I = 0x7f0a1593

.field public static final tv_login_ways_learn:I = 0x7f0a1594

.field public static final tv_login_ways_license_tips:I = 0x7f0a1595

.field public static final tv_login_ways_protocol_no_cb:I = 0x7f0a1596

.field public static final tv_main_title:I = 0x7f0a1597

.field public static final tv_menu_text:I = 0x7f0a15bd

.field public static final tv_message:I = 0x7f0a15be

.field public static final tv_middle:I = 0x7f0a15bf

.field public static final tv_mobile_area_code:I = 0x7f0a15c3

.field public static final tv_mobile_country:I = 0x7f0a15c4

.field public static final tv_mobile_number_error:I = 0x7f0a15c7

.field public static final tv_mobile_number_get_vcode:I = 0x7f0a15c8

.field public static final tv_mobile_number_input:I = 0x7f0a15c9

.field public static final tv_mobile_number_protocol:I = 0x7f0a15ca

.field public static final tv_mobile_number_pwd_login:I = 0x7f0a15cb

.field public static final tv_mobile_number_title:I = 0x7f0a15cc

.field public static final tv_mobile_pwd_login:I = 0x7f0a15cd

.field public static final tv_mobile_pwd_login_area_code:I = 0x7f0a15ce

.field public static final tv_mobile_pwd_login_country:I = 0x7f0a15cf

.field public static final tv_mobile_pwd_login_error:I = 0x7f0a15d0

.field public static final tv_mobile_pwd_login_find_pwd:I = 0x7f0a15d1

.field public static final tv_mobile_pwd_login_input_number:I = 0x7f0a15d2

.field public static final tv_mobile_pwd_login_input_pwd:I = 0x7f0a15d3

.field public static final tv_mobile_pwd_login_protocol:I = 0x7f0a15d4

.field public static final tv_mobile_pwd_login_title:I = 0x7f0a15d5

.field public static final tv_mobile_sms_login:I = 0x7f0a15d6

.field public static final tv_not_start:I = 0x7f0a1614

.field public static final tv_one_binding_other_phone:I = 0x7f0a1634

.field public static final tv_one_login_auth_mail:I = 0x7f0a1636

.field public static final tv_one_login_auth_more_login_way:I = 0x7f0a1637

.field public static final tv_one_login_auth_wechat:I = 0x7f0a1638

.field public static final tv_one_login_half_bind_other_phone:I = 0x7f0a1639

.field public static final tv_one_login_half_intro:I = 0x7f0a163a

.field public static final tv_one_login_half_intro_for_login:I = 0x7f0a163b

.field public static final tv_one_login_other_phone_login:I = 0x7f0a163c

.field public static final tv_one_login_skip:I = 0x7f0a163d

.field public static final tv_one_login_title:I = 0x7f0a163e

.field public static final tv_open_camera:I = 0x7f0a1648

.field public static final tv_operator_type:I = 0x7f0a1651

.field public static final tv_opt_1:I = 0x7f0a1652

.field public static final tv_opt_2:I = 0x7f0a1653

.field public static final tv_password_login:I = 0x7f0a167f

.field public static final tv_password_tip:I = 0x7f0a1680

.field public static final tv_phone_area_code:I = 0x7f0a16a5

.field public static final tv_phone_area_code_name:I = 0x7f0a16a6

.field public static final tv_phone_pwd_login_area_code:I = 0x7f0a16a9

.field public static final tv_phone_pwd_login_error_msg:I = 0x7f0a16aa

.field public static final tv_phone_pwd_login_forget_password:I = 0x7f0a16ab

.field public static final tv_phone_pwd_login_phone_number:I = 0x7f0a16ac

.field public static final tv_phone_pwd_login_verify_code_login:I = 0x7f0a16ad

.field public static final tv_phone_verify_code_login_account:I = 0x7f0a16ae

.field public static final tv_phone_verify_code_login_error_msg:I = 0x7f0a16af

.field public static final tv_phone_verify_code_login_pwd_login:I = 0x7f0a16b0

.field public static final tv_privacy_agreement_des:I = 0x7f0a16d8

.field public static final tv_protocol_detail:I = 0x7f0a16e4

.field public static final tv_pwd_login_over_five_contact_us:I = 0x7f0a16fa

.field public static final tv_pwd_login_over_five_find_pwd:I = 0x7f0a16fb

.field public static final tv_pwd_login_over_five_find_pwd_tip:I = 0x7f0a16fc

.field public static final tv_pwd_login_over_three_contact_us:I = 0x7f0a16fd

.field public static final tv_pwd_login_over_three_find_pwd:I = 0x7f0a16fe

.field public static final tv_pwd_or_verify_code_login:I = 0x7f0a16ff

.field public static final tv_register:I = 0x7f0a171f

.field public static final tv_register_account:I = 0x7f0a1720

.field public static final tv_register_error:I = 0x7f0a1721

.field public static final tv_register_new_account:I = 0x7f0a1722

.field public static final tv_register_protocol:I = 0x7f0a1723

.field public static final tv_register_pwd:I = 0x7f0a1724

.field public static final tv_register_pwd_require:I = 0x7f0a1725

.field public static final tv_register_title:I = 0x7f0a1726

.field public static final tv_reset_pwd:I = 0x7f0a1737

.field public static final tv_reset_pwd_content:I = 0x7f0a1738

.field public static final tv_reset_pwd_error_msg:I = 0x7f0a1739

.field public static final tv_reset_pwd_require:I = 0x7f0a173a

.field public static final tv_reset_pwd_title:I = 0x7f0a173b

.field public static final tv_right:I = 0x7f0a174d

.field public static final tv_rights_left:I = 0x7f0a1759

.field public static final tv_rights_middle:I = 0x7f0a175a

.field public static final tv_rights_right:I = 0x7f0a175b

.field public static final tv_satis:I = 0x7f0a175e

.field public static final tv_save_screenshot:I = 0x7f0a1767

.field public static final tv_set_pwd:I = 0x7f0a1793

.field public static final tv_set_pwd_error:I = 0x7f0a1794

.field public static final tv_set_pwd_intro:I = 0x7f0a1795

.field public static final tv_set_pwd_login:I = 0x7f0a1796

.field public static final tv_set_pwd_require:I = 0x7f0a1797

.field public static final tv_setting_pwd_error_msg:I = 0x7f0a179d

.field public static final tv_setting_pwd_title:I = 0x7f0a179e

.field public static final tv_show_thumb:I = 0x7f0a17cf

.field public static final tv_skip:I = 0x7f0a17e9

.field public static final tv_squared_label:I = 0x7f0a17f1

.field public static final tv_sub_title:I = 0x7f0a1810

.field public static final tv_super_vcode_over_five_contact_us:I = 0x7f0a181b

.field public static final tv_super_vcode_over_there_contact_us:I = 0x7f0a181c

.field public static final tv_super_vcode_validate_error_msg:I = 0x7f0a181d

.field public static final tv_sync_tips_msg:I = 0x7f0a1828

.field public static final tv_thanks:I = 0x7f0a184a

.field public static final tv_title:I = 0x7f0a187b

.field public static final tv_title_bind:I = 0x7f0a1881

.field public static final tv_title_tips:I = 0x7f0a1891

.field public static final tv_top_title:I = 0x7f0a18ae

.field public static final tv_unsatis:I = 0x7f0a18d9

.field public static final tv_user_agreement_des:I = 0x7f0a18ea

.field public static final tv_verify_bind:I = 0x7f0a18fa

.field public static final tv_verify_code_1:I = 0x7f0a18fb

.field public static final tv_verify_code_2:I = 0x7f0a18fc

.field public static final tv_verify_code_3:I = 0x7f0a18fd

.field public static final tv_verify_code_4:I = 0x7f0a18fe

.field public static final tv_verify_code_5:I = 0x7f0a18ff

.field public static final tv_verify_code_6:I = 0x7f0a1900

.field public static final tv_verify_code_check_activate_mail:I = 0x7f0a1901

.field public static final tv_verify_code_check_email:I = 0x7f0a1902

.field public static final tv_verify_code_check_email_hint:I = 0x7f0a1903

.field public static final tv_verify_code_contact_us:I = 0x7f0a1904

.field public static final tv_verify_code_error:I = 0x7f0a1905

.field public static final tv_verify_code_error_msg:I = 0x7f0a1906

.field public static final tv_verify_code_input_title:I = 0x7f0a1907

.field public static final tv_verify_code_login:I = 0x7f0a1908

.field public static final tv_verify_code_none_other_login_way:I = 0x7f0a1909

.field public static final tv_verify_code_none_receive_contact_us:I = 0x7f0a190a

.field public static final tv_verify_code_none_receive_use_super_verify_code:I = 0x7f0a190b

.field public static final tv_verify_code_not_receive:I = 0x7f0a190c

.field public static final tv_verify_code_register_account:I = 0x7f0a190d

.field public static final tv_verify_code_resend:I = 0x7f0a190e

.field public static final tv_verify_code_send_to:I = 0x7f0a190f

.field public static final tv_verify_count_down:I = 0x7f0a1910

.field public static final tv_verify_error:I = 0x7f0a1911

.field public static final tv_verify_intro:I = 0x7f0a1912

.field public static final tv_verify_new_email_email:I = 0x7f0a1913

.field public static final tv_verify_new_email_error_msg:I = 0x7f0a1914

.field public static final tv_verify_new_phone_area_code:I = 0x7f0a1915

.field public static final tv_verify_new_phone_area_code_name:I = 0x7f0a1916

.field public static final tv_verify_new_phone_error_msg:I = 0x7f0a1917

.field public static final tv_verify_old_account_account:I = 0x7f0a1918

.field public static final tv_verify_old_account_contact_us:I = 0x7f0a1919

.field public static final tv_verify_old_account_error_msg:I = 0x7f0a191a

.field public static final tv_warning:I = 0x7f0a192f

.field public static final tv_webview_menu_url_source:I = 0x7f0a193a

.field public static final unchecked:I = 0x7f0a196b

.field public static final understand_and_confirm_button:I = 0x7f0a196c

.field public static final uniform:I = 0x7f0a196d

.field public static final unlabeled:I = 0x7f0a196f

.field public static final up:I = 0x7f0a1971

.field public static final url:I = 0x7f0a197a

.field public static final v_boarder:I = 0x7f0a1989

.field public static final v_top_divider:I = 0x7f0a19e5

.field public static final v_water_spread:I = 0x7f0a19ed

.field public static final vc_code:I = 0x7f0a19ef

.field public static final vc_code_verify_code:I = 0x7f0a19f0

.field public static final vcv_verify_code_input:I = 0x7f0a19f1

.field public static final vertical_only:I = 0x7f0a19f6

.field public static final video_decoder_gl_surface_view:I = 0x7f0a19f7

.field public static final view_container:I = 0x7f0a1a0d

.field public static final view_dialog_result_divider:I = 0x7f0a1a12

.field public static final view_divider:I = 0x7f0a1a13

.field public static final view_flipper:I = 0x7f0a1a1c

.field public static final view_offset_helper:I = 0x7f0a1a3b

.field public static final view_stub_ko_agreement:I = 0x7f0a1a5f

.field public static final view_transition:I = 0x7f0a1a71

.field public static final view_tree_lifecycle_owner:I = 0x7f0a1a72

.field public static final view_tree_on_back_pressed_dispatcher_owner:I = 0x7f0a1a73

.field public static final view_tree_saved_state_registry_owner:I = 0x7f0a1a74

.field public static final view_tree_view_model_store_owner:I = 0x7f0a1a75

.field public static final visible:I = 0x7f0a1a84

.field public static final visible_removing_fragment_view_tag:I = 0x7f0a1a85

.field public static final webView:I = 0x7f0a1ab4

.field public static final west:I = 0x7f0a1ab8

.field public static final when_playing:I = 0x7f0a1abe

.field public static final wide:I = 0x7f0a1abf

.field public static final widget_Rotate:I = 0x7f0a1ac0

.field public static final widget_imageview:I = 0x7f0a1ac1

.field public static final widget_textview:I = 0x7f0a1ac3

.field public static final with_icon:I = 0x7f0a1ac5

.field public static final withinBounds:I = 0x7f0a1ac6

.field public static final wrap:I = 0x7f0a1acc

.field public static final wrap_content:I = 0x7f0a1acd

.field public static final wrap_content_constrained:I = 0x7f0a1ace

.field public static final wrap_reverse:I = 0x7f0a1acf

.field public static final x_left:I = 0x7f0a1ad0

.field public static final x_right:I = 0x7f0a1ad1

.field public static final xbutton:I = 0x7f0a1ad3

.field public static final xedit:I = 0x7f0a1ad4

.field public static final xedit_layout:I = 0x7f0a1ad5

.field public static final zoom:I = 0x7f0a1adf


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

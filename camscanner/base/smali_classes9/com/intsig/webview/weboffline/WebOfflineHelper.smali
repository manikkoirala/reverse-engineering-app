.class public final Lcom/intsig/webview/weboffline/WebOfflineHelper;
.super Ljava/lang/Object;
.source "WebOfflineHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/webview/weboffline/WebOfflineHelper$Companion;,
        Lcom/intsig/webview/weboffline/WebOfflineHelper$Holder;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8:Lcom/intsig/webview/weboffline/WebOfflineHelper$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final Oo08:Lcom/intsig/webview/weboffline/WebOfflineHelper;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private 〇080:Ljava/lang/String;

.field private final 〇o00〇〇Oo:Lcom/intsig/webview/weboffline/WebOfflineHelper$mDelegate$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lcom/intsig/webview/weboffline/WebOfflineHelper$mEventListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/webview/weboffline/WebOfflineHelper$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/webview/weboffline/WebOfflineHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/webview/weboffline/WebOfflineHelper;->O8:Lcom/intsig/webview/weboffline/WebOfflineHelper$Companion;

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/webview/weboffline/WebOfflineHelper$Holder;->〇080:Lcom/intsig/webview/weboffline/WebOfflineHelper$Holder;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/webview/weboffline/WebOfflineHelper$Holder;->〇080()Lcom/intsig/webview/weboffline/WebOfflineHelper;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sput-object v0, Lcom/intsig/webview/weboffline/WebOfflineHelper;->Oo08:Lcom/intsig/webview/weboffline/WebOfflineHelper;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/webview/weboffline/WebOfflineHelper$mDelegate$1;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/webview/weboffline/WebOfflineHelper$mDelegate$1;-><init>(Lcom/intsig/webview/weboffline/WebOfflineHelper;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/webview/weboffline/WebOfflineHelper;->〇o00〇〇Oo:Lcom/intsig/webview/weboffline/WebOfflineHelper$mDelegate$1;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/webview/weboffline/WebOfflineHelper$mEventListener$1;

    .line 12
    .line 13
    invoke-direct {v0}, Lcom/intsig/webview/weboffline/WebOfflineHelper$mEventListener$1;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/webview/weboffline/WebOfflineHelper;->〇o〇:Lcom/intsig/webview/weboffline/WebOfflineHelper$mEventListener$1;

    .line 17
    .line 18
    return-void
    .line 19
.end method

.method private final Oo08()Ljava/lang/String;
    .locals 8

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇o〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v6

    .line 5
    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v7, 0x0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    if-eqz v0, :cond_1

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    return-object v0

    .line 19
    :cond_1
    const-string v1, "."

    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    const/4 v3, 0x0

    .line 23
    const/4 v4, 0x6

    .line 24
    const/4 v5, 0x0

    .line 25
    move-object v0, v6

    .line 26
    invoke-static/range {v0 .. v5}, Lkotlin/text/StringsKt;->ooo〇8oO(Ljava/lang/CharSequence;Ljava/lang/String;IZILjava/lang/Object;)I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-lez v0, :cond_2

    .line 31
    .line 32
    invoke-virtual {v6, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const-string/jumbo v1, "this as java.lang.String\u2026ing(startIndex, endIndex)"

    .line 37
    .line 38
    .line 39
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    return-object v0

    .line 43
    :cond_2
    return-object v6
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public static final synthetic 〇080()Lcom/intsig/webview/weboffline/WebOfflineHelper;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/webview/weboffline/WebOfflineHelper;->Oo08:Lcom/intsig/webview/weboffline/WebOfflineHelper;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/webview/weboffline/WebOfflineHelper;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/webview/weboffline/WebOfflineHelper;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static final 〇o〇()Lcom/intsig/webview/weboffline/WebOfflineHelper;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/webview/weboffline/WebOfflineHelper;->O8:Lcom/intsig/webview/weboffline/WebOfflineHelper$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/webview/weboffline/WebOfflineHelper$Companion;->〇080()Lcom/intsig/webview/weboffline/WebOfflineHelper;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public final O8()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/weboffline/WebOfflineManager;->〇080:Lcom/intsig/weboffline/WebOfflineManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/weboffline/WebOfflineManager;->OO0o〇〇()Ljava/util/Map;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public final o〇0(Landroid/app/Application;Ljava/lang/String;)V
    .locals 11
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/AnyThread;
    .end annotation

    .line 1
    const-string v0, "application"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/webview/weboffline/WebOfflineHelper;->Oo08()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v3

    .line 10
    if-eqz v3, :cond_1

    .line 11
    .line 12
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    goto :goto_1

    .line 21
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 22
    :goto_1
    if-eqz v0, :cond_2

    .line 23
    .line 24
    const-string p1, "WebOfflineHelper"

    .line 25
    .line 26
    const-string p2, "install shortVersion is empty"

    .line 27
    .line 28
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_2
    iput-object p2, p0, Lcom/intsig/webview/weboffline/WebOfflineHelper;->〇080:Ljava/lang/String;

    .line 33
    .line 34
    new-instance p2, Lcom/intsig/weboffline/WebOfflineParams;

    .line 35
    .line 36
    const-string v2, "COM.CS.MAIN.01"

    .line 37
    .line 38
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇0〇O0088o()Z

    .line 39
    .line 40
    .line 41
    move-result v4

    .line 42
    const/4 v5, 0x1

    .line 43
    iget-object v6, p0, Lcom/intsig/webview/weboffline/WebOfflineHelper;->〇o00〇〇Oo:Lcom/intsig/webview/weboffline/WebOfflineHelper$mDelegate$1;

    .line 44
    .line 45
    const/4 v7, 0x0

    .line 46
    const/4 v8, 0x0

    .line 47
    const/16 v9, 0x60

    .line 48
    .line 49
    const/4 v10, 0x0

    .line 50
    move-object v1, p2

    .line 51
    invoke-direct/range {v1 .. v10}, Lcom/intsig/weboffline/WebOfflineParams;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/intsig/weboffline/WebOfflineDelegate;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 52
    .line 53
    .line 54
    sget-object v0, Lcom/intsig/weboffline/WebOfflineManager;->〇080:Lcom/intsig/weboffline/WebOfflineManager;

    .line 55
    .line 56
    invoke-virtual {v0, p1, p2}, Lcom/intsig/weboffline/WebOfflineManager;->Oooo8o0〇(Landroid/app/Application;Lcom/intsig/weboffline/WebOfflineParams;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/weboffline/WebOfflineManager;->O8()Lcom/intsig/weboffline/listener/EventDispatchDelegate;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    iget-object p2, p0, Lcom/intsig/webview/weboffline/WebOfflineHelper;->〇o〇:Lcom/intsig/webview/weboffline/WebOfflineHelper$mEventListener$1;

    .line 64
    .line 65
    invoke-interface {p1, p2}, Lcom/intsig/weboffline/listener/EventDispatchDelegate;->O8(Lcom/intsig/weboffline/listener/EventListener;)V

    .line 66
    .line 67
    .line 68
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public final 〇〇888(Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;
    .locals 1
    .param p1    # Landroid/webkit/WebResourceRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "request"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/weboffline/WebOfflineManager;->〇080:Lcom/intsig/weboffline/WebOfflineManager;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/weboffline/WebOfflineManager;->〇O〇(Landroid/webkit/WebResourceRequest;)Landroid/webkit/WebResourceResponse;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

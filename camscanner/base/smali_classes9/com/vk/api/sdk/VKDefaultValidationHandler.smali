.class public final Lcom/vk/api/sdk/VKDefaultValidationHandler;
.super Ljava/lang/Object;
.source "VKDefaultValidationHandler.kt"

# interfaces
.implements Lcom/vk/api/sdk/VKApiValidationHandler;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/vk/api/sdk/VKDefaultValidationHandler;->〇080:Landroid/content/Context;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private final O8(Lcom/vk/api/sdk/VKApiValidationHandler$Callback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vk/api/sdk/VKApiValidationHandler$Callback<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/vk/api/sdk/ui/VKCaptchaActivity;->o〇00O:Lcom/vk/api/sdk/ui/VKCaptchaActivity$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/vk/api/sdk/ui/VKCaptchaActivity$Companion;->〇080()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    if-eqz v1, :cond_1

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/vk/api/sdk/ui/VKCaptchaActivity$Companion;->〇080()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->〇〇8O0〇8()V

    .line 16
    .line 17
    .line 18
    :cond_0
    invoke-virtual {p1, v0}, Lcom/vk/api/sdk/VKApiValidationHandler$Callback;->〇o〇(Ljava/lang/Object;)V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_1
    invoke-virtual {p1}, Lcom/vk/api/sdk/VKApiValidationHandler$Callback;->〇080()V

    .line 23
    .line 24
    .line 25
    :goto_0
    return-void
.end method


# virtual methods
.method public final getContext()Landroid/content/Context;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/vk/api/sdk/VKDefaultValidationHandler;->〇080:Landroid/content/Context;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇080(Ljava/lang/String;Lcom/vk/api/sdk/VKApiValidationHandler$Callback;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/vk/api/sdk/VKApiValidationHandler$Callback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/vk/api/sdk/VKApiValidationHandler$Callback<",
            "Lcom/vk/api/sdk/VKApiValidationHandler$Credentials;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string/jumbo v0, "validationUrl"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "cb"

    .line 8
    .line 9
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    sget-object v0, Lcom/vk/api/sdk/ui/VKWebViewAuthActivity;->o〇00O:Lcom/vk/api/sdk/ui/VKWebViewAuthActivity$Companion;

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    invoke-virtual {v0, v1}, Lcom/vk/api/sdk/ui/VKWebViewAuthActivity$Companion;->〇o00〇〇Oo(Lcom/vk/api/sdk/VKApiValidationHandler$Credentials;)V

    .line 16
    .line 17
    .line 18
    iget-object v2, p0, Lcom/vk/api/sdk/VKDefaultValidationHandler;->〇080:Landroid/content/Context;

    .line 19
    .line 20
    invoke-virtual {v0, v2, p1}, Lcom/vk/api/sdk/ui/VKWebViewAuthActivity$Companion;->O8(Landroid/content/Context;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sget-object p1, Lcom/vk/api/sdk/utils/VKValidationLocker;->〇o〇:Lcom/vk/api/sdk/utils/VKValidationLocker;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/vk/api/sdk/utils/VKValidationLocker;->〇080()V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/vk/api/sdk/ui/VKWebViewAuthActivity$Companion;->〇080()Lcom/vk/api/sdk/VKApiValidationHandler$Credentials;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    if-eqz p1, :cond_0

    .line 33
    .line 34
    invoke-virtual {p2, p1}, Lcom/vk/api/sdk/VKApiValidationHandler$Callback;->〇o〇(Ljava/lang/Object;)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    invoke-virtual {p2}, Lcom/vk/api/sdk/VKApiValidationHandler$Callback;->〇080()V

    .line 39
    .line 40
    .line 41
    :goto_0
    invoke-virtual {v0, v1}, Lcom/vk/api/sdk/ui/VKWebViewAuthActivity$Companion;->〇o00〇〇Oo(Lcom/vk/api/sdk/VKApiValidationHandler$Credentials;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public 〇o00〇〇Oo(Ljava/lang/String;Lcom/vk/api/sdk/VKApiValidationHandler$Callback;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/vk/api/sdk/VKApiValidationHandler$Callback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/vk/api/sdk/VKApiValidationHandler$Callback<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "confirmationText"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "cb"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sget-object v0, Lcom/vk/api/sdk/ui/VKConfirmationActivity;->〇OOo8〇0:Lcom/vk/api/sdk/ui/VKConfirmationActivity$Companion;

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, v1}, Lcom/vk/api/sdk/ui/VKConfirmationActivity$Companion;->setResult(Z)V

    .line 15
    .line 16
    .line 17
    iget-object v2, p0, Lcom/vk/api/sdk/VKDefaultValidationHandler;->〇080:Landroid/content/Context;

    .line 18
    .line 19
    invoke-virtual {v0, v2, p1}, Lcom/vk/api/sdk/ui/VKConfirmationActivity$Companion;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    sget-object p1, Lcom/vk/api/sdk/utils/VKValidationLocker;->〇o〇:Lcom/vk/api/sdk/utils/VKValidationLocker;

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/vk/api/sdk/utils/VKValidationLocker;->〇080()V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/vk/api/sdk/ui/VKConfirmationActivity$Companion;->〇080()Z

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-virtual {p2, p1}, Lcom/vk/api/sdk/VKApiValidationHandler$Callback;->〇o〇(Ljava/lang/Object;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v1}, Lcom/vk/api/sdk/ui/VKConfirmationActivity$Companion;->setResult(Z)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public 〇o〇(Ljava/lang/String;Lcom/vk/api/sdk/VKApiValidationHandler$Callback;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/vk/api/sdk/VKApiValidationHandler$Callback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/vk/api/sdk/VKApiValidationHandler$Callback<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "img"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "cb"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sget-object v0, Lcom/vk/api/sdk/ui/VKCaptchaActivity;->o〇00O:Lcom/vk/api/sdk/ui/VKCaptchaActivity$Companion;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/vk/api/sdk/VKDefaultValidationHandler;->〇080:Landroid/content/Context;

    .line 14
    .line 15
    invoke-virtual {v0, v1, p1}, Lcom/vk/api/sdk/ui/VKCaptchaActivity$Companion;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    sget-object p1, Lcom/vk/api/sdk/utils/VKValidationLocker;->〇o〇:Lcom/vk/api/sdk/utils/VKValidationLocker;

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/vk/api/sdk/utils/VKValidationLocker;->〇080()V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0, p2}, Lcom/vk/api/sdk/VKDefaultValidationHandler;->O8(Lcom/vk/api/sdk/VKApiValidationHandler$Callback;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

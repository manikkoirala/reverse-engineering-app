.class public final enum Lcom/vk/api/sdk/auth/VKScope;
.super Ljava/lang/Enum;
.source "VKScope.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/vk/api/sdk/auth/VKScope;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum ADS:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum AUDIO:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum DOCS:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum EMAIL:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum FRIENDS:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum GROUPS:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum MARKET:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum MESSAGES:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum NOTES:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum NOTIFICATIONS:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum NOTIFY:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum OFFLINE:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum PAGES:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum PHOTOS:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum STATS:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum STATUS:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum STORIES:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum VIDEO:Lcom/vk/api/sdk/auth/VKScope;

.field public static final enum WALL:Lcom/vk/api/sdk/auth/VKScope;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    const/16 v0, 0x13

    .line 2
    .line 3
    new-array v0, v0, [Lcom/vk/api/sdk/auth/VKScope;

    .line 4
    .line 5
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 6
    .line 7
    const-string v2, "NOTIFY"

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 11
    .line 12
    .line 13
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->NOTIFY:Lcom/vk/api/sdk/auth/VKScope;

    .line 14
    .line 15
    aput-object v1, v0, v3

    .line 16
    .line 17
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 18
    .line 19
    const-string v2, "FRIENDS"

    .line 20
    .line 21
    const/4 v3, 0x1

    .line 22
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 23
    .line 24
    .line 25
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->FRIENDS:Lcom/vk/api/sdk/auth/VKScope;

    .line 26
    .line 27
    aput-object v1, v0, v3

    .line 28
    .line 29
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 30
    .line 31
    const-string v2, "PHOTOS"

    .line 32
    .line 33
    const/4 v3, 0x2

    .line 34
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 35
    .line 36
    .line 37
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->PHOTOS:Lcom/vk/api/sdk/auth/VKScope;

    .line 38
    .line 39
    aput-object v1, v0, v3

    .line 40
    .line 41
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 42
    .line 43
    const-string v2, "AUDIO"

    .line 44
    .line 45
    const/4 v3, 0x3

    .line 46
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 47
    .line 48
    .line 49
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->AUDIO:Lcom/vk/api/sdk/auth/VKScope;

    .line 50
    .line 51
    aput-object v1, v0, v3

    .line 52
    .line 53
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 54
    .line 55
    const-string v2, "VIDEO"

    .line 56
    .line 57
    const/4 v3, 0x4

    .line 58
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 59
    .line 60
    .line 61
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->VIDEO:Lcom/vk/api/sdk/auth/VKScope;

    .line 62
    .line 63
    aput-object v1, v0, v3

    .line 64
    .line 65
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 66
    .line 67
    const-string v2, "STORIES"

    .line 68
    .line 69
    const/4 v3, 0x5

    .line 70
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 71
    .line 72
    .line 73
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->STORIES:Lcom/vk/api/sdk/auth/VKScope;

    .line 74
    .line 75
    aput-object v1, v0, v3

    .line 76
    .line 77
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 78
    .line 79
    const-string v2, "PAGES"

    .line 80
    .line 81
    const/4 v3, 0x6

    .line 82
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 83
    .line 84
    .line 85
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->PAGES:Lcom/vk/api/sdk/auth/VKScope;

    .line 86
    .line 87
    aput-object v1, v0, v3

    .line 88
    .line 89
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 90
    .line 91
    const-string v2, "STATUS"

    .line 92
    .line 93
    const/4 v3, 0x7

    .line 94
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 95
    .line 96
    .line 97
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->STATUS:Lcom/vk/api/sdk/auth/VKScope;

    .line 98
    .line 99
    aput-object v1, v0, v3

    .line 100
    .line 101
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 102
    .line 103
    const-string v2, "NOTES"

    .line 104
    .line 105
    const/16 v3, 0x8

    .line 106
    .line 107
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 108
    .line 109
    .line 110
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->NOTES:Lcom/vk/api/sdk/auth/VKScope;

    .line 111
    .line 112
    aput-object v1, v0, v3

    .line 113
    .line 114
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 115
    .line 116
    const-string v2, "MESSAGES"

    .line 117
    .line 118
    const/16 v3, 0x9

    .line 119
    .line 120
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 121
    .line 122
    .line 123
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->MESSAGES:Lcom/vk/api/sdk/auth/VKScope;

    .line 124
    .line 125
    aput-object v1, v0, v3

    .line 126
    .line 127
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 128
    .line 129
    const-string v2, "WALL"

    .line 130
    .line 131
    const/16 v3, 0xa

    .line 132
    .line 133
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 134
    .line 135
    .line 136
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->WALL:Lcom/vk/api/sdk/auth/VKScope;

    .line 137
    .line 138
    aput-object v1, v0, v3

    .line 139
    .line 140
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 141
    .line 142
    const-string v2, "ADS"

    .line 143
    .line 144
    const/16 v3, 0xb

    .line 145
    .line 146
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 147
    .line 148
    .line 149
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->ADS:Lcom/vk/api/sdk/auth/VKScope;

    .line 150
    .line 151
    aput-object v1, v0, v3

    .line 152
    .line 153
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 154
    .line 155
    const-string v2, "OFFLINE"

    .line 156
    .line 157
    const/16 v3, 0xc

    .line 158
    .line 159
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 160
    .line 161
    .line 162
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->OFFLINE:Lcom/vk/api/sdk/auth/VKScope;

    .line 163
    .line 164
    aput-object v1, v0, v3

    .line 165
    .line 166
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 167
    .line 168
    const-string v2, "DOCS"

    .line 169
    .line 170
    const/16 v3, 0xd

    .line 171
    .line 172
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 173
    .line 174
    .line 175
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->DOCS:Lcom/vk/api/sdk/auth/VKScope;

    .line 176
    .line 177
    aput-object v1, v0, v3

    .line 178
    .line 179
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 180
    .line 181
    const-string v2, "GROUPS"

    .line 182
    .line 183
    const/16 v3, 0xe

    .line 184
    .line 185
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 186
    .line 187
    .line 188
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->GROUPS:Lcom/vk/api/sdk/auth/VKScope;

    .line 189
    .line 190
    aput-object v1, v0, v3

    .line 191
    .line 192
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 193
    .line 194
    const-string v2, "NOTIFICATIONS"

    .line 195
    .line 196
    const/16 v3, 0xf

    .line 197
    .line 198
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 199
    .line 200
    .line 201
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->NOTIFICATIONS:Lcom/vk/api/sdk/auth/VKScope;

    .line 202
    .line 203
    aput-object v1, v0, v3

    .line 204
    .line 205
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 206
    .line 207
    const-string v2, "STATS"

    .line 208
    .line 209
    const/16 v3, 0x10

    .line 210
    .line 211
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 212
    .line 213
    .line 214
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->STATS:Lcom/vk/api/sdk/auth/VKScope;

    .line 215
    .line 216
    aput-object v1, v0, v3

    .line 217
    .line 218
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 219
    .line 220
    const-string v2, "EMAIL"

    .line 221
    .line 222
    const/16 v3, 0x11

    .line 223
    .line 224
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 225
    .line 226
    .line 227
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->EMAIL:Lcom/vk/api/sdk/auth/VKScope;

    .line 228
    .line 229
    aput-object v1, v0, v3

    .line 230
    .line 231
    new-instance v1, Lcom/vk/api/sdk/auth/VKScope;

    .line 232
    .line 233
    const-string v2, "MARKET"

    .line 234
    .line 235
    const/16 v3, 0x12

    .line 236
    .line 237
    invoke-direct {v1, v2, v3}, Lcom/vk/api/sdk/auth/VKScope;-><init>(Ljava/lang/String;I)V

    .line 238
    .line 239
    .line 240
    sput-object v1, Lcom/vk/api/sdk/auth/VKScope;->MARKET:Lcom/vk/api/sdk/auth/VKScope;

    .line 241
    .line 242
    aput-object v1, v0, v3

    .line 243
    .line 244
    sput-object v0, Lcom/vk/api/sdk/auth/VKScope;->$VALUES:[Lcom/vk/api/sdk/auth/VKScope;

    .line 245
    .line 246
    return-void
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vk/api/sdk/auth/VKScope;
    .locals 1

    .line 1
    const-class v0, Lcom/vk/api/sdk/auth/VKScope;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/vk/api/sdk/auth/VKScope;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lcom/vk/api/sdk/auth/VKScope;
    .locals 1

    .line 1
    sget-object v0, Lcom/vk/api/sdk/auth/VKScope;->$VALUES:[Lcom/vk/api/sdk/auth/VKScope;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/vk/api/sdk/auth/VKScope;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/vk/api/sdk/auth/VKScope;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

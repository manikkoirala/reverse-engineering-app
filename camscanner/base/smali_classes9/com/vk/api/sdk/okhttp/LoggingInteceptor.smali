.class public final Lcom/vk/api/sdk/okhttp/LoggingInteceptor;
.super Ljava/lang/Object;
.source "LoggingInteceptor.kt"

# interfaces
.implements Lokhttp3/Interceptor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vk/api/sdk/okhttp/LoggingInteceptor$LogLevelMap;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic O8:[Lkotlin/reflect/KProperty;


# instance fields
.field private final 〇080:Lcom/vk/api/sdk/utils/ThreadLocalDelegate;

.field private final 〇o00〇〇Oo:Z

.field private final 〇o〇:Lcom/vk/api/sdk/utils/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-class v2, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;

    .line 7
    .line 8
    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    const-string v3, "delegate"

    .line 13
    .line 14
    const-string v4, "getDelegate()Lokhttp3/logging/HttpLoggingInterceptor;"

    .line 15
    .line 16
    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    const/4 v2, 0x0

    .line 24
    aput-object v1, v0, v2

    .line 25
    .line 26
    sput-object v0, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;->O8:[Lkotlin/reflect/KProperty;

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public constructor <init>(ZLcom/vk/api/sdk/utils/log/Logger;)V
    .locals 1
    .param p2    # Lcom/vk/api/sdk/utils/log/Logger;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "logger"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-boolean p1, p0, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;->〇o00〇〇Oo:Z

    .line 10
    .line 11
    iput-object p2, p0, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;->〇o〇:Lcom/vk/api/sdk/utils/log/Logger;

    .line 12
    .line 13
    new-instance p1, Lcom/vk/api/sdk/okhttp/LoggingInteceptor$delegate$2;

    .line 14
    .line 15
    invoke-direct {p1, p0}, Lcom/vk/api/sdk/okhttp/LoggingInteceptor$delegate$2;-><init>(Lcom/vk/api/sdk/okhttp/LoggingInteceptor;)V

    .line 16
    .line 17
    .line 18
    invoke-static {p1}, Lcom/vk/api/sdk/utils/ThreadLocalDelegateKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lcom/vk/api/sdk/utils/ThreadLocalDelegate;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    iput-object p1, p0, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;->〇080:Lcom/vk/api/sdk/utils/ThreadLocalDelegate;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static final synthetic 〇080(Lcom/vk/api/sdk/okhttp/LoggingInteceptor;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;->〇o00〇〇Oo:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/vk/api/sdk/okhttp/LoggingInteceptor;)Lcom/vk/api/sdk/utils/log/Logger;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object p0, p0, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;->〇o〇:Lcom/vk/api/sdk/utils/log/Logger;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private final 〇o〇()Lokhttp3/logging/HttpLoggingInterceptor;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;->〇080:Lcom/vk/api/sdk/utils/ThreadLocalDelegate;

    .line 2
    .line 3
    sget-object v1, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;->O8:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-static {v0, p0, v1}, Lcom/vk/api/sdk/utils/ThreadLocalDelegateKt;->〇080(Lcom/vk/api/sdk/utils/ThreadLocalDelegate;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lokhttp3/logging/HttpLoggingInterceptor;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 6
    .param p1    # Lokhttp3/Interceptor$Chain;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "chain"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->Oo08()Lokhttp3/Request;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lokhttp3/Request;->〇080()Lokhttp3/RequestBody;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    invoke-virtual {v0}, Lokhttp3/RequestBody;->contentLength()J

    .line 17
    .line 18
    .line 19
    move-result-wide v0

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const-wide/16 v0, 0x0

    .line 22
    .line 23
    :goto_0
    invoke-direct {p0}, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;->〇o〇()Lokhttp3/logging/HttpLoggingInterceptor;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    const-wide/16 v3, 0x400

    .line 28
    .line 29
    cmp-long v5, v0, v3

    .line 30
    .line 31
    if-lez v5, :cond_1

    .line 32
    .line 33
    sget-object v0, Lokhttp3/logging/HttpLoggingInterceptor$Level;->BASIC:Lokhttp3/logging/HttpLoggingInterceptor$Level;

    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_1
    sget-object v0, Lcom/vk/api/sdk/okhttp/LoggingInteceptor$LogLevelMap;->〇o00〇〇Oo:Lcom/vk/api/sdk/okhttp/LoggingInteceptor$LogLevelMap;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/vk/api/sdk/okhttp/LoggingInteceptor$LogLevelMap;->〇080()Landroidx/collection/ArrayMap;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;->〇o〇:Lcom/vk/api/sdk/utils/log/Logger;

    .line 43
    .line 44
    invoke-interface {v1}, Lcom/vk/api/sdk/utils/log/Logger;->〇080()Lcom/vk/api/sdk/utils/log/Logger$LogLevel;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {v0, v1}, Landroidx/collection/SimpleArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    check-cast v0, Lokhttp3/logging/HttpLoggingInterceptor$Level;

    .line 53
    .line 54
    :goto_1
    invoke-virtual {v2, v0}, Lokhttp3/logging/HttpLoggingInterceptor;->〇o〇(Lokhttp3/logging/HttpLoggingInterceptor$Level;)Lokhttp3/logging/HttpLoggingInterceptor;

    .line 55
    .line 56
    .line 57
    invoke-direct {p0}, Lcom/vk/api/sdk/okhttp/LoggingInteceptor;->〇o〇()Lokhttp3/logging/HttpLoggingInterceptor;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-virtual {v0, p1}, Lokhttp3/logging/HttpLoggingInterceptor;->intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    const-string v0, "delegate.intercept(chain)"

    .line 66
    .line 67
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->O8(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    return-object p1
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.class public final Lcom/vk/api/sdk/chain/InternalErrorRetryChainCall;
.super Lcom/vk/api/sdk/chain/RetryChainCall;
.source "InternalErrorRetryChainCall.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/vk/api/sdk/chain/RetryChainCall<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:Lcom/vk/api/sdk/chain/ChainCall;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/vk/api/sdk/chain/ChainCall<",
            "TT;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lcom/vk/api/sdk/utils/ExponentialBackoff;


# direct methods
.method public constructor <init>(Lcom/vk/api/sdk/VKApiManager;ILcom/vk/api/sdk/chain/ChainCall;)V
    .locals 10
    .param p1    # Lcom/vk/api/sdk/VKApiManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/vk/api/sdk/chain/ChainCall;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vk/api/sdk/VKApiManager;",
            "I",
            "Lcom/vk/api/sdk/chain/ChainCall<",
            "+TT;>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "manager"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "chain"

    .line 7
    .line 8
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/vk/api/sdk/chain/RetryChainCall;-><init>(Lcom/vk/api/sdk/VKApiManager;I)V

    .line 12
    .line 13
    .line 14
    iput-object p3, p0, Lcom/vk/api/sdk/chain/InternalErrorRetryChainCall;->O8:Lcom/vk/api/sdk/chain/ChainCall;

    .line 15
    .line 16
    new-instance p1, Lcom/vk/api/sdk/utils/ExponentialBackoff;

    .line 17
    .line 18
    const-wide/16 v2, 0x3e8

    .line 19
    .line 20
    const-wide/32 v4, 0xea60

    .line 21
    .line 22
    .line 23
    const/high16 v6, 0x3fc00000    # 1.5f

    .line 24
    .line 25
    const/4 v7, 0x0

    .line 26
    const/16 v8, 0x8

    .line 27
    .line 28
    const/4 v9, 0x0

    .line 29
    move-object v1, p1

    .line 30
    invoke-direct/range {v1 .. v9}, Lcom/vk/api/sdk/utils/ExponentialBackoff;-><init>(JJFFILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 31
    .line 32
    .line 33
    iput-object p1, p0, Lcom/vk/api/sdk/chain/InternalErrorRetryChainCall;->〇o〇:Lcom/vk/api/sdk/utils/ExponentialBackoff;

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public call(Lcom/vk/api/sdk/chain/ChainArgs;)Ljava/lang/Object;
    .locals 4
    .param p1    # Lcom/vk/api/sdk/chain/ChainArgs;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vk/api/sdk/chain/ChainArgs;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    const-string v1, "args"

    .line 4
    .line 5
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    :goto_0
    invoke-virtual {p0}, Lcom/vk/api/sdk/chain/RetryChainCall;->O8()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    if-ltz v2, :cond_2

    .line 14
    .line 15
    iget-object v2, p0, Lcom/vk/api/sdk/chain/InternalErrorRetryChainCall;->〇o〇:Lcom/vk/api/sdk/utils/ExponentialBackoff;

    .line 16
    .line 17
    invoke-virtual {v2}, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o00〇〇Oo()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    invoke-virtual {p0}, Lcom/vk/api/sdk/chain/RetryChainCall;->O8()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    if-gt v2, v3, :cond_0

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_0
    if-nez v1, :cond_1

    .line 29
    .line 30
    new-instance v1, Lcom/vk/api/sdk/exceptions/VKApiException;

    .line 31
    .line 32
    const-string p1, "api-call failed due to retry limits, but no exception has tracked"

    .line 33
    .line 34
    invoke-direct {v1, p1}, Lcom/vk/api/sdk/exceptions/VKApiException;-><init>(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    :cond_1
    throw v1

    .line 38
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/vk/api/sdk/chain/InternalErrorRetryChainCall;->〇o〇:Lcom/vk/api/sdk/utils/ExponentialBackoff;

    .line 39
    .line 40
    invoke-virtual {v1}, Lcom/vk/api/sdk/utils/ExponentialBackoff;->O8()Z

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-eqz v1, :cond_3

    .line 45
    .line 46
    iget-object v1, p0, Lcom/vk/api/sdk/chain/InternalErrorRetryChainCall;->〇o〇:Lcom/vk/api/sdk/utils/ExponentialBackoff;

    .line 47
    .line 48
    invoke-virtual {v1}, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇080()J

    .line 49
    .line 50
    .line 51
    move-result-wide v1

    .line 52
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 53
    .line 54
    .line 55
    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/vk/api/sdk/chain/InternalErrorRetryChainCall;->O8:Lcom/vk/api/sdk/chain/ChainCall;

    .line 56
    .line 57
    invoke-virtual {v1, p1}, Lcom/vk/api/sdk/chain/ChainCall;->call(Lcom/vk/api/sdk/chain/ChainArgs;)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object p1
    :try_end_0
    .catch Lcom/vk/api/sdk/exceptions/VKApiExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/vk/api/sdk/exceptions/VKApiIllegalResponseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    return-object p1

    .line 62
    :catch_0
    move-exception v1

    .line 63
    invoke-virtual {p0, v0, v1}, Lcom/vk/api/sdk/chain/ChainCall;->〇o〇(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 64
    .line 65
    .line 66
    iget-object v2, p0, Lcom/vk/api/sdk/chain/InternalErrorRetryChainCall;->〇o〇:Lcom/vk/api/sdk/utils/ExponentialBackoff;

    .line 67
    .line 68
    invoke-virtual {v2}, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o〇()V

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :catch_1
    move-exception v1

    .line 73
    invoke-virtual {v1}, Lcom/vk/api/sdk/exceptions/VKApiExecutionException;->isInternalServerError()Z

    .line 74
    .line 75
    .line 76
    move-result v2

    .line 77
    if-eqz v2, :cond_4

    .line 78
    .line 79
    invoke-virtual {p0, v0, v1}, Lcom/vk/api/sdk/chain/ChainCall;->〇o〇(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 80
    .line 81
    .line 82
    iget-object v2, p0, Lcom/vk/api/sdk/chain/InternalErrorRetryChainCall;->〇o〇:Lcom/vk/api/sdk/utils/ExponentialBackoff;

    .line 83
    .line 84
    invoke-virtual {v2}, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o〇()V

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_4
    throw v1
    .line 89
    .line 90
.end method

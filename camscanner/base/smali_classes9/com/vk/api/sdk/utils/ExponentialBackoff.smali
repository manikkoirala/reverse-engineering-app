.class public final Lcom/vk/api/sdk/utils/ExponentialBackoff;
.super Ljava/lang/Object;
.source "ExponentialBackoff.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vk/api/sdk/utils/ExponentialBackoff$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oO80:Lcom/vk/api/sdk/utils/ExponentialBackoff$Companion;


# instance fields
.field private final O8:J

.field private final Oo08:J

.field private final o〇0:F

.field private final 〇080:Ljava/util/Random;

.field private 〇o00〇〇Oo:J

.field private 〇o〇:I

.field private final 〇〇888:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/vk/api/sdk/utils/ExponentialBackoff$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/vk/api/sdk/utils/ExponentialBackoff$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->oO80:Lcom/vk/api/sdk/utils/ExponentialBackoff$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public constructor <init>(JJFF)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->O8:J

    iput-wide p3, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->Oo08:J

    iput p5, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->o〇0:F

    iput p6, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇〇888:F

    .line 2
    new-instance p3, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p4

    invoke-direct {p3, p4, p5}, Ljava/util/Random;-><init>(J)V

    iput-object p3, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇080:Ljava/util/Random;

    .line 3
    iput-wide p1, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o00〇〇Oo:J

    return-void
.end method

.method public synthetic constructor <init>(JJFFILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    .line 4
    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v0, 0x64

    invoke-virtual {p1, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p1

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p7, 0x2

    if-eqz p1, :cond_1

    .line 5
    sget-object p1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 p2, 0x5

    invoke-virtual {p1, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p3

    :cond_1
    move-wide v3, p3

    and-int/lit8 p1, p7, 0x4

    if-eqz p1, :cond_2

    const/high16 p5, 0x40000000    # 2.0f

    const/high16 v5, 0x40000000    # 2.0f

    goto :goto_0

    :cond_2
    move v5, p5

    :goto_0
    and-int/lit8 p1, p7, 0x8

    if-eqz p1, :cond_3

    const p6, 0x3dcccccd    # 0.1f

    const v6, 0x3dcccccd    # 0.1f

    goto :goto_1

    :cond_3
    move v6, p6

    :goto_1
    move-object v0, p0

    .line 6
    invoke-direct/range {v0 .. v6}, Lcom/vk/api/sdk/utils/ExponentialBackoff;-><init>(JJFF)V

    return-void
.end method

.method private final Oo08(F)J
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇080:Ljava/util/Random;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/Random;->nextGaussian()D

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    float-to-double v2, p1

    .line 8
    mul-double v0, v0, v2

    .line 9
    .line 10
    double-to-long v0, v0

    .line 11
    return-wide v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public final O8()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o〇:I

    .line 2
    .line 3
    if-lez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public final 〇080()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o00〇〇Oo:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public final 〇o00〇〇Oo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public final 〇o〇()V
    .locals 4

    .line 1
    iget-wide v0, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o00〇〇Oo:J

    .line 2
    .line 3
    long-to-float v0, v0

    .line 4
    iget v1, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->o〇0:F

    .line 5
    .line 6
    mul-float v0, v0, v1

    .line 7
    .line 8
    iget-wide v1, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->Oo08:J

    .line 9
    .line 10
    long-to-float v1, v1

    .line 11
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    float-to-long v0, v0

    .line 16
    iput-wide v0, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o00〇〇Oo:J

    .line 17
    .line 18
    long-to-float v2, v0

    .line 19
    iget v3, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇〇888:F

    .line 20
    .line 21
    mul-float v2, v2, v3

    .line 22
    .line 23
    invoke-direct {p0, v2}, Lcom/vk/api/sdk/utils/ExponentialBackoff;->Oo08(F)J

    .line 24
    .line 25
    .line 26
    move-result-wide v2

    .line 27
    add-long/2addr v0, v2

    .line 28
    iput-wide v0, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o00〇〇Oo:J

    .line 29
    .line 30
    iget v0, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o〇:I

    .line 31
    .line 32
    add-int/lit8 v0, v0, 0x1

    .line 33
    .line 34
    iput v0, p0, Lcom/vk/api/sdk/utils/ExponentialBackoff;->〇o〇:I

    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

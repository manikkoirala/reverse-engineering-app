.class public abstract Lcom/onedrive/sdk/core/DefaultClientConfig;
.super Ljava/lang/Object;
.source "DefaultClientConfig.java"

# interfaces
.implements Lcom/onedrive/sdk/core/IClientConfig;


# instance fields
.field private mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

.field private mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

.field private mHttpProvider:Lcom/onedrive/sdk/http/DefaultHttpProvider;

.field private mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

.field private mSerializer:Lcom/onedrive/sdk/serializer/DefaultSerializer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static createWithAuthenticator(Lcom/onedrive/sdk/authentication/IAuthenticator;)Lcom/onedrive/sdk/core/IClientConfig;
    .locals 2

    .line 1
    new-instance v0, Lcom/onedrive/sdk/core/DefaultClientConfig$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/onedrive/sdk/core/DefaultClientConfig$1;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object p0, v0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 9
    .line 10
    .line 11
    move-result-object p0

    .line 12
    const-string v1, "Using provided authenticator"

    .line 13
    .line 14
    invoke-interface {p0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static createWithAuthenticators(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/onedrive/sdk/authentication/ADALAuthenticator;)Lcom/onedrive/sdk/core/IClientConfig;
    .locals 2

    .line 1
    new-instance v0, Lcom/onedrive/sdk/core/DefaultClientConfig$2;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/onedrive/sdk/core/DefaultClientConfig$2;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;

    .line 7
    .line 8
    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;-><init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/onedrive/sdk/authentication/ADALAuthenticator;)V

    .line 9
    .line 10
    .line 11
    iput-object v1, v0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    const-string p1, "Created DisambiguationAuthenticator"

    .line 18
    .line 19
    invoke-interface {p0, p1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private getRequestInterceptor()Lcom/onedrive/sdk/http/IRequestInterceptor;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-direct {v0, v1, v2}, Lcom/onedrive/sdk/authentication/AuthorizationInterceptor;-><init>(Lcom/onedrive/sdk/authentication/IAuthenticator;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

    .line 19
    .line 20
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method


# virtual methods
.method public getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-direct {v0, v1}, Lcom/onedrive/sdk/concurrency/DefaultExecutors;-><init>(Lcom/onedrive/sdk/logger/ILogger;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 15
    .line 16
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 17
    .line 18
    const-string v1, "Created DefaultExecutors"

    .line 19
    .line 20
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 24
    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mHttpProvider:Lcom/onedrive/sdk/http/DefaultHttpProvider;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/onedrive/sdk/http/DefaultHttpProvider;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-direct {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getRequestInterceptor()Lcom/onedrive/sdk/http/IRequestInterceptor;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 20
    .line 21
    .line 22
    move-result-object v4

    .line 23
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/onedrive/sdk/http/DefaultHttpProvider;-><init>(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/http/IRequestInterceptor;Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mHttpProvider:Lcom/onedrive/sdk/http/DefaultHttpProvider;

    .line 27
    .line 28
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 29
    .line 30
    const-string v1, "Created DefaultHttpProvider"

    .line 31
    .line 32
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mHttpProvider:Lcom/onedrive/sdk/http/DefaultHttpProvider;

    .line 36
    .line 37
    return-object v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public getLogger()Lcom/onedrive/sdk/logger/ILogger;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/onedrive/sdk/logger/DefaultLogger;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/onedrive/sdk/logger/DefaultLogger;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 11
    .line 12
    const-string v1, "Created DefaultLogger"

    .line 13
    .line 14
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 18
    .line 19
    return-object v0
.end method

.method public getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mSerializer:Lcom/onedrive/sdk/serializer/DefaultSerializer;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/onedrive/sdk/serializer/DefaultSerializer;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/onedrive/sdk/core/DefaultClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-direct {v0, v1}, Lcom/onedrive/sdk/serializer/DefaultSerializer;-><init>(Lcom/onedrive/sdk/logger/ILogger;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mSerializer:Lcom/onedrive/sdk/serializer/DefaultSerializer;

    .line 15
    .line 16
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 17
    .line 18
    const-string v1, "Created DefaultSerializer"

    .line 19
    .line 20
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/core/DefaultClientConfig;->mSerializer:Lcom/onedrive/sdk/serializer/DefaultSerializer;

    .line 24
    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

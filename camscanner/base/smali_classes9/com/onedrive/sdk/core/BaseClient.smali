.class public Lcom/onedrive/sdk/core/BaseClient;
.super Ljava/lang/Object;
.source "BaseClient.java"

# interfaces
.implements Lcom/onedrive/sdk/core/IBaseClient;


# instance fields
.field private mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

.field private mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

.field private mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

.field private mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getLogger()Lcom/onedrive/sdk/logger/ILogger;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getServiceRoot()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/onedrive/sdk/core/BaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAccountInfo;->getServiceRoot()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method protected setAuthenticator(Lcom/onedrive/sdk/authentication/IAuthenticator;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/onedrive/sdk/core/BaseClient;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method protected setExecutors(Lcom/onedrive/sdk/concurrency/IExecutors;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/onedrive/sdk/core/BaseClient;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method protected setHttpProvider(Lcom/onedrive/sdk/http/IHttpProvider;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/onedrive/sdk/core/BaseClient;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method protected setLogger(Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/onedrive/sdk/core/BaseClient;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSerializer(Lcom/onedrive/sdk/serializer/ISerializer;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/onedrive/sdk/core/BaseClient;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public validate()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mAuthenticator:Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 6
    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    .line 10
    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    iget-object v0, p0, Lcom/onedrive/sdk/core/BaseClient;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    .line 19
    .line 20
    const-string v1, "Serializer"

    .line 21
    .line 22
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    throw v0

    .line 26
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    .line 27
    .line 28
    const-string v1, "HttpProvider"

    .line 29
    .line 30
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    throw v0

    .line 34
    :cond_2
    new-instance v0, Ljava/lang/NullPointerException;

    .line 35
    .line 36
    const-string v1, "Executors"

    .line 37
    .line 38
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    throw v0

    .line 42
    :cond_3
    new-instance v0, Ljava/lang/NullPointerException;

    .line 43
    .line 44
    const-string v1, "Authenticator"

    .line 45
    .line 46
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    throw v0
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

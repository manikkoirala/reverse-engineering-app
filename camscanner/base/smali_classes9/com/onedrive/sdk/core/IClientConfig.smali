.class public interface abstract Lcom/onedrive/sdk/core/IClientConfig;
.super Ljava/lang/Object;
.source "IClientConfig.java"


# virtual methods
.method public abstract getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;
.end method

.method public abstract getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;
.end method

.method public abstract getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;
.end method

.method public abstract getLogger()Lcom/onedrive/sdk/logger/ILogger;
.end method

.method public abstract getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
.end method

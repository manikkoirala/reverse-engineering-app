.class public Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;
.super Lcom/onedrive/sdk/core/ClientException;
.source "ClientAuthenticatorException.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, v0, p2}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    return-void
.end method

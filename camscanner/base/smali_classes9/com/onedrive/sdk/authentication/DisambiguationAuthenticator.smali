.class public Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;
.super Ljava/lang/Object;
.source "DisambiguationAuthenticator.java"

# interfaces
.implements Lcom/onedrive/sdk/authentication/IAuthenticator;


# static fields
.field public static final ACCOUNT_TYPE_KEY:Ljava/lang/String; = "accountType"

.field private static final DISAMBIGUATION_AUTHENTICATOR_PREFS:Ljava/lang/String; = "DisambiguationAuthenticatorPrefs"

.field public static final VERSION_CODE_KEY:Ljava/lang/String; = "versionCode"


# instance fields
.field private final mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

.field private final mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mActivity:Landroid/app/Activity;

.field private mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

.field private mInitialized:Z

.field private mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private final mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Lcom/onedrive/sdk/authentication/ADALAuthenticator;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    .line 10
    .line 11
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    .line 12
    .line 13
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;)Lcom/onedrive/sdk/concurrency/IExecutors;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$100(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private getAccountTypeInPreferences()Lcom/onedrive/sdk/authentication/AccountType;
    .locals 3
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "accountType"

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    return-object v2

    .line 15
    :cond_0
    invoke-static {v0}, Lcom/onedrive/sdk/authentication/AccountType;->valueOf(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/AccountType;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    return-object v0
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mActivity:Landroid/app/Activity;

    .line 2
    .line 3
    const-string v1, "DisambiguationAuthenticatorPrefs"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private setAccountTypeInPreferences(Lcom/onedrive/sdk/authentication/AccountType;)V
    .locals 2
    .param p1    # Lcom/onedrive/sdk/authentication/AccountType;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "accountType"

    .line 13
    .line 14
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const-string/jumbo v0, "versionCode"

    .line 23
    .line 24
    .line 25
    const/16 v1, 0x283d

    .line 26
    .line 27
    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method


# virtual methods
.method public getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/onedrive/sdk/authentication/IAccountInfo;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public declared-synchronized init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    monitor-exit p0

    .line 7
    return-void

    .line 8
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 9
    .line 10
    iput-object p3, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mActivity:Landroid/app/Activity;

    .line 11
    .line 12
    iput-object p4, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 13
    .line 14
    const-string v0, "Initializing MSA and ADAL authenticators"

    .line 15
    .line 16
    invoke-interface {p4, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    .line 20
    .line 21
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    .line 25
    .line 26
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 27
    .line 28
    .line 29
    const/4 p1, 0x1

    .line 30
    iput-boolean p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 31
    .line 32
    monitor-exit p0

    .line 33
    return-void

    .line 34
    :catchall_0
    move-exception p1

    .line 35
    monitor-exit p0

    .line 36
    throw p1
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method public declared-synchronized login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 6
    :try_start_0
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v0, "Starting login"

    invoke-interface {p1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 7
    new-instance p1, Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-direct {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;-><init>()V

    .line 8
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 9
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 10
    new-instance v2, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;

    invoke-direct {v2, p0, v0, p1, v1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$2;-><init>(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 11
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->getAccountTypeInPreferences()Lcom/onedrive/sdk/authentication/AccountType;

    move-result-object v3

    const/4 v4, 0x1

    if-eqz v3, :cond_0

    .line 12
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v0, "Found saved account information %s type of account"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    const/4 p1, 0x0

    goto :goto_0

    .line 13
    :cond_0
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v5, "Creating disambiguation ui, waiting for user to sign in"

    invoke-interface {v3, v5}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 14
    new-instance v3, Lcom/onedrive/sdk/authentication/DisambiguationRequest;

    iget-object v5, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mActivity:Landroid/app/Activity;

    iget-object v6, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    invoke-direct {v3, v5, v2, v6}, Lcom/onedrive/sdk/authentication/DisambiguationRequest;-><init>(Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;Lcom/onedrive/sdk/logger/ILogger;)V

    invoke-virtual {v3}, Lcom/onedrive/sdk/authentication/DisambiguationRequest;->execute()V

    .line 15
    invoke-virtual {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->waitForSignal()V

    .line 16
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_3

    .line 17
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/authentication/DisambiguationResponse;

    .line 18
    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/DisambiguationResponse;->getAccountType()Lcom/onedrive/sdk/authentication/AccountType;

    move-result-object v3

    .line 19
    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/DisambiguationResponse;->getAccount()Ljava/lang/String;

    move-result-object p1

    .line 20
    :goto_0
    sget-object v0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$5;->$SwitchMap$com$onedrive$sdk$authentication$AccountType:[I

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    if-eq v0, v4, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 21
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-virtual {v0, p1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object p1

    goto :goto_1

    .line 22
    :cond_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized account type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Unrecognized account type"

    invoke-interface {v0, v1, p1}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 24
    throw p1

    .line 25
    :cond_2
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-virtual {v0, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object p1

    .line 26
    :goto_1
    invoke-direct {p0, v3}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->setAccountTypeInPreferences(Lcom/onedrive/sdk/authentication/AccountType;)V

    .line 27
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 28
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/authentication/IAccountInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    .line 29
    :cond_3
    :try_start_1
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/core/ClientException;

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public login(Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 2
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$1;-><init>(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 4
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string p2, "loginCallback"

    invoke-direct {p1, p2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 5
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "init must be called"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 6
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_3

    .line 7
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login silent"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->getAccountTypeInPreferences()Lcom/onedrive/sdk/authentication/AccountType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 9
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v2, "Expecting %s type of account"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 10
    :cond_0
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v2, "Checking MSA"

    invoke-interface {v1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 11
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-virtual {v1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 12
    iget-object v2, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "Found account info in MSA"

    invoke-interface {v2, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 13
    invoke-direct {p0, v0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->setAccountTypeInPreferences(Lcom/onedrive/sdk/authentication/AccountType;)V

    .line 14
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15
    monitor-exit p0

    return-object v1

    .line 16
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v2, "Checking ADAL"

    invoke-interface {v1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 17
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-virtual {v1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v1

    .line 18
    iget-object v2, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    if-eqz v1, :cond_2

    .line 19
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v2, "Found account info in ADAL"

    invoke-interface {v1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, v0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->setAccountTypeInPreferences(Lcom/onedrive/sdk/authentication/AccountType;)V

    .line 21
    :cond_2
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/authentication/IAccountInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 22
    :cond_3
    :try_start_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "init must be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public loginSilent(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login silent async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$3;-><init>(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 4
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string v0, "loginCallback"

    invoke-direct {p1, v0}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 5
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized logout()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 6
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_2

    .line 7
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 8
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 9
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout of MSA account"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 10
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mMSAAuthenticator:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->logout()V

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 12
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout of ADAL account"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 13
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mADALAuthenticator:Lcom/onedrive/sdk/authentication/ADALAuthenticator;

    invoke-virtual {v0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->logout()V

    .line 14
    :cond_1
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "versionCode"

    const/16 v2, 0x283d

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15
    monitor-exit p0

    return-void

    .line 16
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "init must be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public logout(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$4;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator$4;-><init>(Lcom/onedrive/sdk/authentication/DisambiguationAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 4
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string v0, "logoutCallback"

    invoke-direct {p1, v0}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 5
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

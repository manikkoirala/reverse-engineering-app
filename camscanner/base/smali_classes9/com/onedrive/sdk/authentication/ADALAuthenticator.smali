.class public abstract Lcom/onedrive/sdk/authentication/ADALAuthenticator;
.super Ljava/lang/Object;
.source "ADALAuthenticator.java"

# interfaces
.implements Lcom/onedrive/sdk/authentication/IAuthenticator;


# static fields
.field private static final ADAL_AUTHENTICATOR_PREFS:Ljava/lang/String; = "ADALAuthenticatorPrefs"

.field private static final DISCOVERY_SERVICE_URL:Ljava/lang/String; = "https://api.office.com/discovery/v2.0/me/Services"

.field private static final DISCOVER_SERVICE_RESOURCE_ID:Ljava/lang/String; = "https://api.office.com/discovery/"

.field private static final LOGIN_AUTHORITY:Ljava/lang/String; = "https://login.windows.net/common/oauth2/authorize"

.field private static final RESOURCE_URL_KEY:Ljava/lang/String; = "resourceUrl"

.field private static final SERVICE_INFO_KEY:Ljava/lang/String; = "serviceInfo"

.field private static final USER_ID_KEY:Ljava/lang/String; = "userId"

.field private static final VALIDATE_AUTHORITY:Z = true

.field private static final VERSION_CODE_KEY:Ljava/lang/String; = "versionCode"


# instance fields
.field private final mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mActivity:Landroid/app/Activity;

.field private mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;

.field private mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

.field private mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

.field private mInitialized:Z

.field private mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private final mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/onedrive/sdk/authentication/ServiceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mUserId:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    .line 10
    .line 11
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    .line 17
    .line 18
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    .line 19
    .line 20
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    .line 24
    .line 25
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    .line 26
    .line 27
    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/authentication/ADALAuthenticator;)Lcom/onedrive/sdk/concurrency/IExecutors;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$100(Lcom/onedrive/sdk/authentication/ADALAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private getDiscoveryServiceAuthResult(Ljava/lang/String;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 11

    .line 1
    new-instance v0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 9
    .line 10
    .line 11
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    .line 12
    .line 13
    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 14
    .line 15
    .line 16
    new-instance v10, Lcom/onedrive/sdk/authentication/ADALAuthenticator$5;

    .line 17
    .line 18
    invoke-direct {v10, p0, v2, v0, v1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$5;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 19
    .line 20
    .line 21
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 22
    .line 23
    const-string v4, "Starting interactive login for the discover service access token"

    .line 24
    .line 25
    invoke-interface {v3, v4}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 29
    .line 30
    const-string v4, "https://api.office.com/discovery/"

    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getClientId()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v5

    .line 36
    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getRedirectUrl()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v6

    .line 40
    sget-object v8, Lcom/microsoft/aad/adal/PromptBehavior;->Auto:Lcom/microsoft/aad/adal/PromptBehavior;

    .line 41
    .line 42
    const/4 v9, 0x0

    .line 43
    move-object v7, p1

    .line 44
    invoke-virtual/range {v3 .. v10}, Lcom/microsoft/aad/adal/AuthenticationContext;->acquireToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/PromptBehavior;Ljava/lang/String;Lcom/microsoft/aad/adal/AuthenticationCallback;)V

    .line 45
    .line 46
    .line 47
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 48
    .line 49
    const-string v3, "Waiting for interactive login to complete"

    .line 50
    .line 51
    invoke-interface {p1, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->waitForSignal()V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    if-nez p1, :cond_0

    .line 62
    .line 63
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    check-cast p1, Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 68
    .line 69
    return-object p1

    .line 70
    :cond_0
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    check-cast p1, Lcom/onedrive/sdk/core/ClientException;

    .line 75
    .line 76
    throw p1
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private getOneDriveApiService([Lcom/onedrive/sdk/authentication/ServiceInfo;)Lcom/onedrive/sdk/authentication/ServiceInfo;
    .locals 8

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    :goto_0
    if-ge v2, v0, :cond_1

    .line 5
    .line 6
    aget-object v3, p1, v2

    .line 7
    .line 8
    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 9
    .line 10
    const/4 v5, 0x3

    .line 11
    new-array v5, v5, [Ljava/lang/Object;

    .line 12
    .line 13
    iget-object v6, v3, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceResourceId:Ljava/lang/String;

    .line 14
    .line 15
    aput-object v6, v5, v1

    .line 16
    .line 17
    iget-object v6, v3, Lcom/onedrive/sdk/authentication/ServiceInfo;->capability:Ljava/lang/String;

    .line 18
    .line 19
    const/4 v7, 0x1

    .line 20
    aput-object v6, v5, v7

    .line 21
    .line 22
    const/4 v6, 0x2

    .line 23
    iget-object v7, v3, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceApiVersion:Ljava/lang/String;

    .line 24
    .line 25
    aput-object v7, v5, v6

    .line 26
    .line 27
    const-string v6, "Service info resource id%s capabilities %s version %s"

    .line 28
    .line 29
    invoke-static {v6, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v5

    .line 33
    invoke-interface {v4, v5}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iget-object v4, v3, Lcom/onedrive/sdk/authentication/ServiceInfo;->capability:Ljava/lang/String;

    .line 37
    .line 38
    const-string v5, "MyFiles"

    .line 39
    .line 40
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    if-eqz v4, :cond_0

    .line 45
    .line 46
    iget-object v4, v3, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceApiVersion:Ljava/lang/String;

    .line 47
    .line 48
    const-string/jumbo v5, "v2.0"

    .line 49
    .line 50
    .line 51
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 52
    .line 53
    .line 54
    move-result v4

    .line 55
    if-eqz v4, :cond_0

    .line 56
    .line 57
    return-object v3

    .line 58
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    new-instance p1, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    .line 62
    .line 63
    const-string v0, "Unable to file the files services from the directory provider"

    .line 64
    .line 65
    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 66
    .line 67
    invoke-direct {p1, v0, v1}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 68
    .line 69
    .line 70
    throw p1
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private getOneDriveServiceAuthResult(Lcom/onedrive/sdk/authentication/ServiceInfo;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 10

    .line 1
    new-instance v0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 9
    .line 10
    .line 11
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    .line 12
    .line 13
    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 14
    .line 15
    .line 16
    new-instance v9, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;

    .line 17
    .line 18
    invoke-direct {v9, p0, v2, v0, v1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$7;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 19
    .line 20
    .line 21
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 22
    .line 23
    const-string v4, "Starting OneDrive resource refresh token request"

    .line 24
    .line 25
    invoke-interface {v3, v4}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 29
    .line 30
    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mActivity:Landroid/app/Activity;

    .line 31
    .line 32
    iget-object v5, p1, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceResourceId:Ljava/lang/String;

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getClientId()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v6

    .line 38
    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getRedirectUrl()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v7

    .line 42
    sget-object v8, Lcom/microsoft/aad/adal/PromptBehavior;->Auto:Lcom/microsoft/aad/adal/PromptBehavior;

    .line 43
    .line 44
    invoke-virtual/range {v3 .. v9}, Lcom/microsoft/aad/adal/AuthenticationContext;->acquireToken(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/PromptBehavior;Lcom/microsoft/aad/adal/AuthenticationCallback;)V

    .line 45
    .line 46
    .line 47
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 48
    .line 49
    const-string v3, "Waiting for token refresh"

    .line 50
    .line 51
    invoke-interface {p1, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->waitForSignal()V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    if-nez p1, :cond_0

    .line 62
    .line 63
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    check-cast p1, Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 68
    .line 69
    return-object p1

    .line 70
    :cond_0
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    check-cast p1, Lcom/onedrive/sdk/core/ClientException;

    .line 75
    .line 76
    throw p1
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private getOneDriveServiceInfoFromDiscoveryService(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/ServiceInfo;
    .locals 6

    .line 1
    new-instance v4, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/onedrive/sdk/options/HeaderOption;

    .line 7
    .line 8
    new-instance v1, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v2, "bearer "

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    const-string v1, "Authorization"

    .line 26
    .line 27
    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/HeaderOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 34
    .line 35
    const-string v0, "Starting discovery service request"

    .line 36
    .line 37
    invoke-interface {p1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    new-instance p1, Lcom/onedrive/sdk/authentication/ADALAuthenticator$6;

    .line 41
    .line 42
    const-string v2, "https://api.office.com/discovery/v2.0/me/Services"

    .line 43
    .line 44
    const/4 v3, 0x0

    .line 45
    const/4 v5, 0x0

    .line 46
    move-object v0, p1

    .line 47
    move-object v1, p0

    .line 48
    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$6;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    .line 49
    .line 50
    .line 51
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    .line 52
    .line 53
    invoke-virtual {p1, v0}, Lcom/onedrive/sdk/http/BaseRequest;->setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    .line 57
    .line 58
    const-class v1, Lcom/onedrive/sdk/authentication/DiscoveryServiceResponse;

    .line 59
    .line 60
    const/4 v2, 0x0

    .line 61
    invoke-interface {v0, p1, v1, v2}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    check-cast p1, Lcom/onedrive/sdk/authentication/DiscoveryServiceResponse;

    .line 66
    .line 67
    iget-object p1, p1, Lcom/onedrive/sdk/authentication/DiscoveryServiceResponse;->services:[Lcom/onedrive/sdk/authentication/ServiceInfo;

    .line 68
    .line 69
    invoke-direct {p0, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getOneDriveApiService([Lcom/onedrive/sdk/authentication/ServiceInfo;)Lcom/onedrive/sdk/authentication/ServiceInfo;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    return-object p1
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mActivity:Landroid/app/Activity;

    .line 2
    .line 3
    const-string v1, "ADALAuthenticatorPrefs"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/onedrive/sdk/authentication/IAccountInfo;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method protected abstract getClientId()Ljava/lang/String;
.end method

.method protected abstract getRedirectUrl()Ljava/lang/String;
.end method

.method public declared-synchronized init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    monitor-exit p0

    .line 7
    return-void

    .line 8
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 9
    .line 10
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    .line 11
    .line 12
    iput-object p3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mActivity:Landroid/app/Activity;

    .line 13
    .line 14
    iput-object p4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 15
    .line 16
    new-instance p1, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;

    .line 17
    .line 18
    invoke-direct {p1, p3, p4}, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;-><init>(Landroid/content/Context;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->check()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 22
    .line 23
    .line 24
    :try_start_2
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 25
    .line 26
    const-string p2, "https://login.windows.net/common/oauth2/authorize"

    .line 27
    .line 28
    const/4 p4, 0x1

    .line 29
    invoke-direct {p1, p3, p2, p4}, Lcom/microsoft/aad/adal/AuthenticationContext;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 30
    .line 31
    .line 32
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 33
    .line 34
    :try_start_3
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    .line 39
    .line 40
    const-string/jumbo p3, "userId"

    .line 41
    .line 42
    .line 43
    const/4 v0, 0x0

    .line 44
    invoke-interface {p1, p3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p3

    .line 48
    invoke-virtual {p2, p3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 49
    .line 50
    .line 51
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    .line 52
    .line 53
    const-string p3, "resourceUrl"

    .line 54
    .line 55
    invoke-interface {p1, p3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p3

    .line 59
    invoke-virtual {p2, p3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 60
    .line 61
    .line 62
    const-string p2, "serviceInfo"

    .line 63
    .line 64
    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 68
    if-eqz p1, :cond_1

    .line 69
    .line 70
    :try_start_4
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    .line 71
    .line 72
    invoke-interface {p2}, Lcom/onedrive/sdk/http/IHttpProvider;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    .line 73
    .line 74
    .line 75
    move-result-object p2

    .line 76
    const-class p3, Lcom/onedrive/sdk/authentication/ServiceInfo;

    .line 77
    .line 78
    invoke-interface {p2, p1, p3}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    check-cast p1, Lcom/onedrive/sdk/authentication/ServiceInfo;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 83
    .line 84
    move-object v0, p1

    .line 85
    goto :goto_0

    .line 86
    :catch_0
    move-exception p1

    .line 87
    :try_start_5
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 88
    .line 89
    const-string p3, "Unable to parse serviceInfo from saved preferences"

    .line 90
    .line 91
    invoke-interface {p2, p3, p1}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92
    .line 93
    .line 94
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    .line 95
    .line 96
    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 97
    .line 98
    .line 99
    iput-boolean p4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    .line 100
    .line 101
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    .line 102
    .line 103
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    if-nez p1, :cond_2

    .line 108
    .line 109
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    .line 110
    .line 111
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    if-nez p1, :cond_2

    .line 116
    .line 117
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    .line 118
    .line 119
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 120
    .line 121
    .line 122
    move-result-object p1

    .line 123
    if-eqz p1, :cond_4

    .line 124
    .line 125
    :cond_2
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 126
    .line 127
    const-string p2, "Found existing login information"

    .line 128
    .line 129
    invoke-interface {p1, p2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    .line 133
    .line 134
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    if-eqz p1, :cond_3

    .line 139
    .line 140
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    .line 141
    .line 142
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    if-eqz p1, :cond_3

    .line 147
    .line 148
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    .line 149
    .line 150
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 151
    .line 152
    .line 153
    move-result-object p1

    .line 154
    if-nez p1, :cond_4

    .line 155
    .line 156
    :cond_3
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 157
    .line 158
    const-string p2, "Existing login information was incompletely, flushing sign in state"

    .line 159
    .line 160
    invoke-interface {p1, p2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->logout()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 164
    .line 165
    .line 166
    :cond_4
    monitor-exit p0

    .line 167
    return-void

    .line 168
    :catch_1
    move-exception p1

    .line 169
    goto :goto_1

    .line 170
    :catch_2
    move-exception p1

    .line 171
    :goto_1
    :try_start_6
    new-instance p2, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    .line 172
    .line 173
    const-string p3, "Unable to access required cryptographic libraries for ADAL"

    .line 174
    .line 175
    sget-object p4, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 176
    .line 177
    invoke-direct {p2, p3, p1, p4}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 178
    .line 179
    .line 180
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 181
    .line 182
    const-string p3, "Problem creating the AuthenticationContext for ADAL"

    .line 183
    .line 184
    invoke-interface {p1, p3, p2}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 185
    .line 186
    .line 187
    throw p2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 188
    :catchall_0
    move-exception p1

    .line 189
    monitor-exit p0

    .line 190
    throw p1
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method public declared-synchronized login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 6
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    .line 7
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 8
    invoke-direct {p0, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getDiscoveryServiceAuthResult(Ljava/lang/String;)Lcom/microsoft/aad/adal/AuthenticationResult;

    move-result-object p1

    .line 9
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getStatus()Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;->Succeeded:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    if-ne v0, v1, :cond_0

    .line 10
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getOneDriveServiceInfoFromDiscoveryService(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/ServiceInfo;

    move-result-object v0

    .line 11
    invoke-direct {p0, v0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getOneDriveServiceAuthResult(Lcom/onedrive/sdk/authentication/ServiceInfo;)Lcom/microsoft/aad/adal/AuthenticationResult;

    move-result-object v1

    .line 12
    iget-object v2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mHttpProvider:Lcom/onedrive/sdk/http/IHttpProvider;

    invoke-interface {v2}, Lcom/onedrive/sdk/http/IHttpProvider;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/onedrive/sdk/serializer/ISerializer;->serializeObject(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 13
    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v4, "Successful login, saving information for silent re-auth"

    invoke-interface {v3, v4}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 15
    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v5, v0, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceEndpointUri:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 16
    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getUserInfo()Lcom/microsoft/aad/adal/UserInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/UserInfo;->getUserId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 17
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 18
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v3, "resourceUrl"

    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string/jumbo v3, "userId"

    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v3, "serviceInfo"

    invoke-interface {p1, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string/jumbo v3, "versionCode"

    const/16 v4, 0x283d

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 19
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v3, "Successfully retrieved login information"

    invoke-interface {p1, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 20
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   Resource Url: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 21
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   User ID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 22
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   Service Info: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 23
    new-instance p1, Lcom/onedrive/sdk/authentication/ADALAccountInfo;

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    invoke-direct {p1, p0, v1, v0, v2}, Lcom/onedrive/sdk/authentication/ADALAccountInfo;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/microsoft/aad/adal/AuthenticationResult;Lcom/onedrive/sdk/authentication/ServiceInfo;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 24
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 25
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/authentication/IAccountInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    .line 26
    :cond_0
    :try_start_1
    new-instance v0, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to authenticate user with ADAL, Error Code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getErrorCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " Error Message"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getErrorDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {v0, p1, v1}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 27
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Unsuccessful login attempt"

    invoke-interface {p1, v1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 28
    throw v0

    .line 29
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public login(Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 2
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$1;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 4
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "loginCallback"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 5
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "init must be called"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 6
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_2

    .line 7
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login silent"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 8
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 9
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "No login information found for silent authentication"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    monitor-exit p0

    const/4 v0, 0x0

    return-object v0

    .line 11
    :cond_0
    :try_start_1
    new-instance v0, Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    invoke-direct {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;-><init>()V

    .line 12
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 13
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 14
    new-instance v3, Lcom/onedrive/sdk/authentication/ADALAuthenticator$3;

    invoke-direct {v3, p0, v1, v0, v2}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$3;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 15
    iget-object v4, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    iget-object v5, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/onedrive/sdk/authentication/ServiceInfo;

    iget-object v5, v5, Lcom/onedrive/sdk/authentication/ServiceInfo;->serviceResourceId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getClientId()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7, v3}, Lcom/microsoft/aad/adal/AuthenticationContext;->acquireTokenSilent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/AuthenticationCallback;)Ljava/util/concurrent/Future;

    .line 16
    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->waitForSignal()V

    .line 17
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 18
    new-instance v0, Lcom/onedrive/sdk/authentication/ADALAccountInfo;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/aad/adal/AuthenticationResult;

    iget-object v2, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mOneDriveServiceInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/onedrive/sdk/authentication/ServiceInfo;

    iget-object v3, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/onedrive/sdk/authentication/ADALAccountInfo;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/microsoft/aad/adal/AuthenticationResult;Lcom/onedrive/sdk/authentication/ServiceInfo;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 19
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 20
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAccountInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/authentication/IAccountInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 21
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/core/ClientException;

    throw v0

    .line 22
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "init must be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public loginSilent(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting login silent async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/ADALAuthenticator$2;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$2;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 4
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "loginCallback"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 5
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized logout()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    monitor-enter p0

    .line 6
    :try_start_0
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_0

    .line 7
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 8
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Clearing ADAL cache"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 9
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mAdalContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationContext;->getCache()Lcom/microsoft/aad/adal/ITokenCacheStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/aad/adal/ITokenCacheStore;->removeAll()V

    .line 10
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Clearing all webview cookies"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 11
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 12
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 13
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 14
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 15
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Clearing all ADAL Authenticator shared preferences"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 17
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "versionCode"

    const/16 v2, 0x283d

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 18
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mUserId:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 19
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mResourceUrl:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    monitor-exit p0

    return-void

    .line 21
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "init must be called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public logout(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mInitialized:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 2
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    const-string v1, "Starting logout async"

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/ADALAuthenticator;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v1, Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/authentication/ADALAuthenticator$4;-><init>(Lcom/onedrive/sdk/authentication/ADALAuthenticator;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void

    .line 4
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "logoutCallback"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 5
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "init must be called"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class public interface abstract Lcom/onedrive/sdk/authentication/IAccountInfo;
.super Ljava/lang/Object;
.source "IAccountInfo.java"


# virtual methods
.method public abstract getAccessToken()Ljava/lang/String;
.end method

.method public abstract getAccountType()Lcom/onedrive/sdk/authentication/AccountType;
.end method

.method public abstract getServiceRoot()Ljava/lang/String;
.end method

.method public abstract isExpired()Z
.end method

.method public abstract refresh()V
.end method

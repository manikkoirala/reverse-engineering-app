.class public Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;
.super Ljava/lang/Object;
.source "BrokerPermissionsChecker.java"


# instance fields
.field private final mAdalProjectUrl:Ljava/lang/String;

.field private final mBrokerRequirePermissions:[Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mLogger:Lcom/onedrive/sdk/logger/ILogger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "https://github.com/AzureAD/azure-activedirectory-library-for-android"

    .line 5
    .line 6
    iput-object v0, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mAdalProjectUrl:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "android.permission.MANAGE_ACCOUNTS"

    .line 9
    .line 10
    const-string v1, "android.permission.USE_CREDENTIALS"

    .line 11
    .line 12
    const-string v2, "android.permission.GET_ACCOUNTS"

    .line 13
    .line 14
    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iput-object v0, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mBrokerRequirePermissions:[Ljava/lang/String;

    .line 19
    .line 20
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mContext:Landroid/content/Context;

    .line 21
    .line 22
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public check()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/microsoft/aad/adal/AuthenticationSettings;->INSTANCE:Lcom/microsoft/aad/adal/AuthenticationSettings;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationSettings;->getSkipBroker()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_2

    .line 8
    .line 9
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 10
    .line 11
    const-string v1, "Checking permissions for use with the ADAL Broker."

    .line 12
    .line 13
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mBrokerRequirePermissions:[Ljava/lang/String;

    .line 17
    .line 18
    array-length v1, v0

    .line 19
    const/4 v2, 0x0

    .line 20
    const/4 v3, 0x0

    .line 21
    :goto_0
    if-ge v3, v1, :cond_1

    .line 22
    .line 23
    aget-object v4, v0, v3

    .line 24
    .line 25
    iget-object v5, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mContext:Landroid/content/Context;

    .line 26
    .line 27
    invoke-static {v5, v4}, Landroidx/core/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    const/4 v6, -0x1

    .line 32
    if-eq v5, v6, :cond_0

    .line 33
    .line 34
    add-int/lit8 v3, v3, 0x1

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 v0, 0x2

    .line 38
    new-array v0, v0, [Ljava/lang/Object;

    .line 39
    .line 40
    aput-object v4, v0, v2

    .line 41
    .line 42
    const-string v1, "https://github.com/AzureAD/azure-activedirectory-library-for-android"

    .line 43
    .line 44
    const/4 v2, 0x1

    .line 45
    aput-object v1, v0, v2

    .line 46
    .line 47
    const-string v1, "Required permissions to use the Broker are denied: %s, see %s for more details."

    .line 48
    .line 49
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 54
    .line 55
    invoke-interface {v1, v0}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    new-instance v1, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    .line 59
    .line 60
    sget-object v2, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenicationPermissionsDenied:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 61
    .line 62
    invoke-direct {v1, v0, v2}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 63
    .line 64
    .line 65
    throw v1

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/adal/BrokerPermissionsChecker;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 67
    .line 68
    const-string v1, "All required permissions found."

    .line 69
    .line 70
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    :cond_2
    return-void
    .line 74
    .line 75
.end method

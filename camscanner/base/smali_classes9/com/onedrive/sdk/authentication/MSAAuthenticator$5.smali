.class Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;
.super Ljava/lang/Object;
.source "MSAAuthenticator.java"

# interfaces
.implements Lcom/microsoft/services/msa/LiveAuthListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/authentication/MSAAuthenticator;->loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

.field final synthetic val$error:Ljava/util/concurrent/atomic/AtomicReference;

.field final synthetic val$loginSilentWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/authentication/MSAAuthenticator;Ljava/util/concurrent/atomic/AtomicReference;Lcom/onedrive/sdk/concurrency/SimpleWaiter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    .line 4
    .line 5
    iput-object p3, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$loginSilentWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    .line 6
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public onAuthComplete(Lcom/microsoft/services/msa/LiveStatus;Lcom/microsoft/services/msa/LiveConnectSession;Ljava/lang/Object;)V
    .locals 1

    .line 1
    sget-object p2, Lcom/microsoft/services/msa/LiveStatus;->NOT_CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

    .line 2
    .line 3
    if-ne p1, p2, :cond_0

    .line 4
    .line 5
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    .line 6
    .line 7
    new-instance p2, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    .line 8
    .line 9
    const-string p3, "Failed silent login, interactive login required"

    .line 10
    .line 11
    sget-object v0, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 12
    .line 13
    invoke-direct {p2, p3, v0}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    .line 20
    .line 21
    invoke-static {p1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->access$100(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    .line 26
    .line 27
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    check-cast p2, Lcom/onedrive/sdk/core/ClientException;

    .line 32
    .line 33
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    iget-object p3, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    .line 38
    .line 39
    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object p3

    .line 43
    check-cast p3, Ljava/lang/Throwable;

    .line 44
    .line 45
    invoke-interface {p1, p2, p3}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    .line 50
    .line 51
    invoke-static {p1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->access$100(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    const-string p2, "Successful silent login"

    .line 56
    .line 57
    invoke-interface {p1, p2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    :goto_0
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$loginSilentWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    .line 61
    .line 62
    invoke-virtual {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->signal()V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public onAuthError(Lcom/microsoft/services/msa/LiveAuthException;Ljava/lang/Object;)V
    .locals 3

    .line 1
    sget-object p2, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveAuthException;->getError()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "The user cancelled the login operation."

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    sget-object p2, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationCancelled:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    .line 18
    .line 19
    new-instance v1, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    .line 20
    .line 21
    const-string v2, "Login silent authentication error"

    .line 22
    .line 23
    invoke-direct {v1, v2, p1, p2}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->this$0:Lcom/onedrive/sdk/authentication/MSAAuthenticator;

    .line 30
    .line 31
    invoke-static {p1}, Lcom/onedrive/sdk/authentication/MSAAuthenticator;->access$100(Lcom/onedrive/sdk/authentication/MSAAuthenticator;)Lcom/onedrive/sdk/logger/ILogger;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    iget-object p2, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    .line 36
    .line 37
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    check-cast p2, Lcom/onedrive/sdk/core/ClientException;

    .line 42
    .line 43
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p2

    .line 47
    iget-object v0, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$error:Ljava/util/concurrent/atomic/AtomicReference;

    .line 48
    .line 49
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    check-cast v0, Ljava/lang/Throwable;

    .line 54
    .line 55
    invoke-interface {p1, p2, v0}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 56
    .line 57
    .line 58
    iget-object p1, p0, Lcom/onedrive/sdk/authentication/MSAAuthenticator$5;->val$loginSilentWaiter:Lcom/onedrive/sdk/concurrency/SimpleWaiter;

    .line 59
    .line 60
    invoke-virtual {p1}, Lcom/onedrive/sdk/concurrency/SimpleWaiter;->signal()V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

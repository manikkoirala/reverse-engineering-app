.class public interface abstract Lcom/onedrive/sdk/authentication/IAuthenticator;
.super Ljava/lang/Object;
.source "IAuthenticator.java"


# virtual methods
.method public abstract getAccountInfo()Lcom/onedrive/sdk/authentication/IAccountInfo;
.end method

.method public abstract init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V
.end method

.method public abstract login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract login(Ljava/lang/String;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract loginSilent(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/authentication/IAccountInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract logout()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract logout(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation
.end method

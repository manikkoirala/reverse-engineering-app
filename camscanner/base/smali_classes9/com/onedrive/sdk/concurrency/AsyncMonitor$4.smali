.class Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;
.super Ljava/lang/Object;
.source "AsyncMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/onedrive/sdk/concurrency/AsyncMonitor;->pollForResult(JLcom/onedrive/sdk/concurrency/IProgressCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

.field final synthetic val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

.field final synthetic val$millisBetweenPoll:J


# direct methods
.method constructor <init>(Lcom/onedrive/sdk/concurrency/AsyncMonitor;JLcom/onedrive/sdk/concurrency/IProgressCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 2
    .line 3
    iput-wide p2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$millisBetweenPoll:J

    .line 4
    .line 5
    iput-object p4, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    .line 6
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    :cond_0
    if-eqz v0, :cond_1

    .line 3
    .line 4
    :try_start_0
    iget-wide v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$millisBetweenPoll:J

    .line 5
    .line 6
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_0 .. :try_end_0} :catch_1

    .line 7
    .line 8
    .line 9
    goto :goto_0

    .line 10
    :catch_0
    :try_start_1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 11
    .line 12
    invoke-static {v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "InterruptedException ignored"

    .line 21
    .line 22
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->getStatus()Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iget-object v1, v0, Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;->percentageComplete:Ljava/lang/Double;

    .line 32
    .line 33
    if-eqz v1, :cond_2

    .line 34
    .line 35
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 36
    .line 37
    invoke-static {v1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-interface {v1}, Lcom/onedrive/sdk/core/IBaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    iget-object v2, v0, Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;->percentageComplete:Ljava/lang/Double;

    .line 46
    .line 47
    invoke-virtual {v2}, Ljava/lang/Double;->intValue()I

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    iget-object v3, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    .line 52
    .line 53
    const/16 v4, 0x64

    .line 54
    .line 55
    invoke-interface {v1, v2, v4, v3}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(IILcom/onedrive/sdk/concurrency/IProgressCallback;)V

    .line 56
    .line 57
    .line 58
    :cond_2
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 59
    .line 60
    invoke-static {v1, v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$100(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    if-nez v1, :cond_3

    .line 65
    .line 66
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 67
    .line 68
    invoke-static {v1, v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$200(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    if-eqz v1, :cond_0

    .line 73
    .line 74
    :cond_3
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 75
    .line 76
    invoke-static {v1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    invoke-interface {v1}, Lcom/onedrive/sdk/core/IBaseClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    new-instance v2, Ljava/lang/StringBuilder;

    .line 85
    .line 86
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .line 88
    .line 89
    const-string v3, "Polling has completed, got final status: "

    .line 90
    .line 91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    iget-object v3, v0, Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;->status:Ljava/lang/String;

    .line 95
    .line 96
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    invoke-interface {v1, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 107
    .line 108
    invoke-static {v1, v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$200(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z

    .line 109
    .line 110
    .line 111
    move-result v1

    .line 112
    if-eqz v1, :cond_4

    .line 113
    .line 114
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 115
    .line 116
    invoke-static {v1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    invoke-interface {v1}, Lcom/onedrive/sdk/core/IBaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    new-instance v2, Lcom/onedrive/sdk/concurrency/AsyncOperationException;

    .line 125
    .line 126
    invoke-direct {v2, v0}, Lcom/onedrive/sdk/concurrency/AsyncOperationException;-><init>(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)V

    .line 127
    .line 128
    .line 129
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    .line 130
    .line 131
    invoke-interface {v1, v2, v0}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Lcom/onedrive/sdk/core/ClientException;Lcom/onedrive/sdk/concurrency/ICallback;)V

    .line 132
    .line 133
    .line 134
    :cond_4
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 135
    .line 136
    invoke-static {v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 145
    .line 146
    invoke-virtual {v1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->getResult()Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    move-result-object v1

    .line 150
    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    .line 151
    .line 152
    invoke-interface {v0, v1, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/ICallback;)V
    :try_end_1
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_1 .. :try_end_1} :catch_1

    .line 153
    .line 154
    .line 155
    goto :goto_1

    .line 156
    :catch_1
    move-exception v0

    .line 157
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->this$0:Lcom/onedrive/sdk/concurrency/AsyncMonitor;

    .line 158
    .line 159
    invoke-static {v1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 160
    .line 161
    .line 162
    move-result-object v1

    .line 163
    invoke-interface {v1}, Lcom/onedrive/sdk/core/IBaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 164
    .line 165
    .line 166
    move-result-object v1

    .line 167
    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;->val$callback:Lcom/onedrive/sdk/concurrency/IProgressCallback;

    .line 168
    .line 169
    invoke-interface {v1, v0, v2}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(Lcom/onedrive/sdk/core/ClientException;Lcom/onedrive/sdk/concurrency/ICallback;)V

    .line 170
    .line 171
    .line 172
    :goto_1
    return-void
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

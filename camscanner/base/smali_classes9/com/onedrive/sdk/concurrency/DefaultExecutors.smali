.class public Lcom/onedrive/sdk/concurrency/DefaultExecutors;
.super Ljava/lang/Object;
.source "DefaultExecutors.java"

# interfaces
.implements Lcom/onedrive/sdk/concurrency/IExecutors;


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

.field private final mLogger:Lcom/onedrive/sdk/logger/ILogger;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 5
    .line 6
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 11
    .line 12
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mBackgroundExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 13
    .line 14
    new-instance p1, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    .line 15
    .line 16
    invoke-direct {p1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public performOnBackground(Ljava/lang/Runnable;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "Starting background task, current active count: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mBackgroundExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 14
    .line 15
    invoke-virtual {v2}, Ljava/util/concurrent/ThreadPoolExecutor;->getActiveCount()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mBackgroundExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 30
    .line 31
    invoke-virtual {v0, p1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public performOnForeground(IILcom/onedrive/sdk/concurrency/IProgressCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(II",
            "Lcom/onedrive/sdk/concurrency/IProgressCallback<",
            "TResult;>;)V"
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting foreground task, current active count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    invoke-virtual {v2}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->getActiveCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", with progress  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", max progress"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    new-instance v1, Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/onedrive/sdk/concurrency/DefaultExecutors$2;-><init>(Lcom/onedrive/sdk/concurrency/DefaultExecutors;Lcom/onedrive/sdk/concurrency/IProgressCallback;II)V

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public performOnForeground(Lcom/onedrive/sdk/core/ClientException;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/core/ClientException;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TResult;>;)V"
        }
    .end annotation

    .line 5
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting foreground task, current active count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    invoke-virtual {v2}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->getActiveCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", with exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    new-instance v1, Lcom/onedrive/sdk/concurrency/DefaultExecutors$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/onedrive/sdk/concurrency/DefaultExecutors$3;-><init>(Lcom/onedrive/sdk/concurrency/DefaultExecutors;Lcom/onedrive/sdk/concurrency/ICallback;Lcom/onedrive/sdk/core/ClientException;)V

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public performOnForeground(Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(TResult;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TResult;>;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting foreground task, current active count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    invoke-virtual {v2}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->getActiveCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", with result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/DefaultExecutors;->mForegroundExecutor:Lcom/onedrive/sdk/concurrency/SynchronousExecutor;

    new-instance v1, Lcom/onedrive/sdk/concurrency/DefaultExecutors$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/onedrive/sdk/concurrency/DefaultExecutors$1;-><init>(Lcom/onedrive/sdk/concurrency/DefaultExecutors;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/concurrency/SynchronousExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.class public Lcom/onedrive/sdk/concurrency/AsyncMonitor;
.super Ljava/lang/Object;
.source "AsyncMonitor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

.field private final mHandler:Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;

.field private final mMonitorLocation:Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

.field private final mResultGetter:Lcom/onedrive/sdk/concurrency/ResultGetter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/onedrive/sdk/concurrency/ResultGetter<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/extensions/IOneDriveClient;Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;Lcom/onedrive/sdk/concurrency/ResultGetter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;",
            "Lcom/onedrive/sdk/concurrency/ResultGetter<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mMonitorLocation:Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    .line 7
    .line 8
    iput-object p3, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mResultGetter:Lcom/onedrive/sdk/concurrency/ResultGetter;

    .line 9
    .line 10
    new-instance p1, Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;

    .line 11
    .line 12
    invoke-direct {p1}, Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;-><init>()V

    .line 13
    .line 14
    .line 15
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mHandler:Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/concurrency/AsyncMonitor;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$100(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->isCompleted(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic access$200(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->isFailed(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private isCompleted(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z
    .locals 1

    .line 1
    iget-object p1, p1, Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;->status:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "completed"

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private isFailed(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)Z
    .locals 1

    .line 1
    iget-object p1, p1, Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;->status:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "failed"

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public getResult()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 2
    invoke-virtual {p0}, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->getStatus()Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    move-result-object v0

    .line 3
    iget-object v1, v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->seeOther:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mResultGetter:Lcom/onedrive/sdk/concurrency/ResultGetter;

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {v0, v1, v2}, Lcom/onedrive/sdk/concurrency/ResultGetter;->getResultFrom(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 5
    :cond_0
    new-instance v1, Lcom/onedrive/sdk/core/ClientException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Async operation \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;->operation:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\' has not completed!"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v3, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AsyncTaskNotCompleted:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {v1, v0, v2, v3}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    throw v1
.end method

.method public getResult(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/concurrency/AsyncMonitor$3;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor$3;-><init>(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getStatus()Lcom/onedrive/sdk/extensions/AsyncOperationStatus;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 2
    new-instance v6, Lcom/onedrive/sdk/concurrency/AsyncMonitor$2;

    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mMonitorLocation:Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    invoke-virtual {v0}, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;->getLocation()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/concurrency/AsyncMonitor$2;-><init>(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    .line 3
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {v6, v0}, Lcom/onedrive/sdk/http/BaseRequest;->setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V

    .line 4
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mHandler:Lcom/onedrive/sdk/concurrency/AsyncMonitorResponseHandler;

    const-class v3, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    invoke-interface {v0, v6, v3, v1, v2}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    return-object v0
.end method

.method public getStatus(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/AsyncOperationStatus;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/concurrency/AsyncMonitor$1;

    invoke-direct {v1, p0, p1}, Lcom/onedrive/sdk/concurrency/AsyncMonitor$1;-><init>(Lcom/onedrive/sdk/concurrency/AsyncMonitor;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method public pollForResult(JLcom/onedrive/sdk/concurrency/IProgressCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/onedrive/sdk/concurrency/IProgressCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "Starting to poll for request "

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    iget-object v2, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mMonitorLocation:Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    .line 18
    .line 19
    invoke-virtual {v2}, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;->getLocation()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncMonitor;->mClient:Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 34
    .line 35
    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    new-instance v1, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;

    .line 40
    .line 41
    invoke-direct {v1, p0, p1, p2, p3}, Lcom/onedrive/sdk/concurrency/AsyncMonitor$4;-><init>(Lcom/onedrive/sdk/concurrency/AsyncMonitor;JLcom/onedrive/sdk/concurrency/IProgressCallback;)V

    .line 42
    .line 43
    .line 44
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

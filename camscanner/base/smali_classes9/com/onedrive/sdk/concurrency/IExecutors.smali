.class public interface abstract Lcom/onedrive/sdk/concurrency/IExecutors;
.super Ljava/lang/Object;
.source "IExecutors.java"


# virtual methods
.method public abstract performOnBackground(Ljava/lang/Runnable;)V
.end method

.method public abstract performOnForeground(IILcom/onedrive/sdk/concurrency/IProgressCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(II",
            "Lcom/onedrive/sdk/concurrency/IProgressCallback<",
            "TResult;>;)V"
        }
    .end annotation
.end method

.method public abstract performOnForeground(Lcom/onedrive/sdk/core/ClientException;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/core/ClientException;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TResult;>;)V"
        }
    .end annotation
.end method

.method public abstract performOnForeground(Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(TResult;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TResult;>;)V"
        }
    .end annotation
.end method

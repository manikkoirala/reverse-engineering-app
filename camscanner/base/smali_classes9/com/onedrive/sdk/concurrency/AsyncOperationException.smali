.class public Lcom/onedrive/sdk/concurrency/AsyncOperationException;
.super Lcom/onedrive/sdk/core/ClientException;
.source "AsyncOperationException.java"


# instance fields
.field private final mStatus:Lcom/onedrive/sdk/extensions/AsyncOperationStatus;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/extensions/AsyncOperationStatus;)V
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p1, Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;->status:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    const-string v1, ": "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    iget-object v1, p1, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->statusDescription:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const/4 v1, 0x0

    .line 26
    sget-object v2, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AsyncTaskFailed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 27
    .line 28
    invoke-direct {p0, v0, v1, v2}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 29
    .line 30
    .line 31
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/AsyncOperationException;->mStatus:Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method


# virtual methods
.method public getStatus()Lcom/onedrive/sdk/extensions/AsyncOperationStatus;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/AsyncOperationException;->mStatus:Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

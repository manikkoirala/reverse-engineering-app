.class public Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;
.super Ljava/lang/Object;
.source "ChunkedUploadRequest.java"


# static fields
.field private static final CONTENT_RANGE_FORMAT:Ljava/lang/String; = "bytes %1$d-%2$d/%3$d"

.field private static final CONTENT_RANGE_HEADER_NAME:Ljava/lang/String; = "Content-Range"

.field private static final RETRY_DELAY:I = 0x7d0


# instance fields
.field private final mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

.field private final mData:[B

.field private final mMaxRetry:I

.field private mRetryCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;[BIIII)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;[BIIII)V"
        }
    .end annotation

    .line 1
    move-object v6, p0

    .line 2
    move v7, p5

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    new-array v0, v7, [B

    .line 7
    .line 8
    iput-object v0, v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mData:[B

    .line 9
    .line 10
    const/4 v8, 0x0

    .line 11
    move-object v1, p4

    .line 12
    invoke-static {p4, v8, v0, v8, p5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 13
    .line 14
    .line 15
    iput v8, v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mRetryCount:I

    .line 16
    .line 17
    move/from16 v0, p6

    .line 18
    .line 19
    iput v0, v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mMaxRetry:I

    .line 20
    .line 21
    new-instance v9, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest$1;

    .line 22
    .line 23
    const-class v5, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;

    .line 24
    .line 25
    move-object v0, v9

    .line 26
    move-object v1, p0

    .line 27
    move-object v2, p1

    .line 28
    move-object v3, p2

    .line 29
    move-object v4, p3

    .line 30
    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest$1;-><init>(Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    .line 31
    .line 32
    .line 33
    iput-object v9, v6, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    .line 34
    .line 35
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->PUT:Lcom/onedrive/sdk/http/HttpMethod;

    .line 36
    .line 37
    invoke-virtual {v9, v0}, Lcom/onedrive/sdk/http/BaseRequest;->setHttpMethod(Lcom/onedrive/sdk/http/HttpMethod;)V

    .line 38
    .line 39
    .line 40
    const/4 v0, 0x3

    .line 41
    new-array v0, v0, [Ljava/lang/Object;

    .line 42
    .line 43
    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    aput-object v1, v0, v8

    .line 48
    .line 49
    add-int v1, p7, v7

    .line 50
    .line 51
    const/4 v2, 0x1

    .line 52
    sub-int/2addr v1, v2

    .line 53
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    aput-object v1, v0, v2

    .line 58
    .line 59
    const/4 v1, 0x2

    .line 60
    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    aput-object v2, v0, v1

    .line 65
    .line 66
    const-string v1, "bytes %1$d-%2$d/%3$d"

    .line 67
    .line 68
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    const-string v1, "Content-Range"

    .line 73
    .line 74
    invoke-virtual {v9, v1, v0}, Lcom/onedrive/sdk/http/BaseRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
.end method


# virtual methods
.method public upload(Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;)Lcom/onedrive/sdk/extensions/ChunkedUploadResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<UploadType:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler<",
            "TUploadType;>;)",
            "Lcom/onedrive/sdk/extensions/ChunkedUploadResult;"
        }
    .end annotation

    .line 1
    :goto_0
    iget v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mRetryCount:I

    .line 2
    .line 3
    iget v1, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mMaxRetry:I

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-ge v0, v1, :cond_1

    .line 7
    .line 8
    mul-int/lit16 v1, v0, 0x7d0

    .line 9
    .line 10
    mul-int v1, v1, v0

    .line 11
    .line 12
    int-to-long v0, v1

    .line 13
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    .line 15
    .line 16
    goto :goto_1

    .line 17
    :catch_0
    move-exception v0

    .line 18
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-interface {v1}, Lcom/onedrive/sdk/core/IBaseClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    const-string v3, "Exception while waiting upload file retry"

    .line 29
    .line 30
    invoke-interface {v1, v3, v0}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 31
    .line 32
    .line 33
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    .line 44
    .line 45
    const-class v3, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;

    .line 46
    .line 47
    iget-object v4, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mData:[B

    .line 48
    .line 49
    invoke-interface {v0, v1, v3, v4, p1}, Lcom/onedrive/sdk/http/IHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    check-cast v0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;
    :try_end_1
    .catch Lcom/onedrive/sdk/core/ClientException; {:try_start_1 .. :try_end_1} :catch_1

    .line 54
    .line 55
    move-object v2, v0

    .line 56
    goto :goto_2

    .line 57
    :catch_1
    iget-object v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mBaseRequest:Lcom/onedrive/sdk/http/BaseRequest;

    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    const-string v1, "Request failed with, retry if necessary."

    .line 68
    .line 69
    invoke-interface {v0, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    :goto_2
    if-eqz v2, :cond_0

    .line 73
    .line 74
    invoke-virtual {v2}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->chunkCompleted()Z

    .line 75
    .line 76
    .line 77
    move-result v0

    .line 78
    if-eqz v0, :cond_0

    .line 79
    .line 80
    return-object v2

    .line 81
    :cond_0
    iget v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mRetryCount:I

    .line 82
    .line 83
    add-int/lit8 v0, v0, 0x1

    .line 84
    .line 85
    iput v0, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadRequest;->mRetryCount:I

    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_1
    new-instance p1, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;

    .line 89
    .line 90
    new-instance v0, Lcom/onedrive/sdk/core/ClientException;

    .line 91
    .line 92
    const-string v1, "Upload session failed to many times."

    .line 93
    .line 94
    sget-object v3, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->UploadSessionIncomplete:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 95
    .line 96
    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 97
    .line 98
    .line 99
    invoke-direct {p1, v0}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;-><init>(Lcom/onedrive/sdk/core/ClientException;)V

    .line 100
    .line 101
    .line 102
    return-object p1
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

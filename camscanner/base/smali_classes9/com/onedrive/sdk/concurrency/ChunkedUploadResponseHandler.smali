.class public Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;
.super Ljava/lang/Object;
.source "ChunkedUploadResponseHandler.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IStatefulResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<UploadType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/onedrive/sdk/http/IStatefulResponseHandler<",
        "Lcom/onedrive/sdk/extensions/ChunkedUploadResult;",
        "TUploadType;>;"
    }
.end annotation


# instance fields
.field private final mDeserializeTypeClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TUploadType;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TUploadType;>;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;->mDeserializeTypeClass:Ljava/lang/Class;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public configConnection(Lcom/onedrive/sdk/http/IConnection;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public generateResult(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/http/IConnection;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/logger/ILogger;)Lcom/onedrive/sdk/extensions/ChunkedUploadResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 2
    :try_start_0
    invoke-interface {p2}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    move-result v1

    const/16 v2, 0xca

    if-ne v1, v2, :cond_0

    const-string p1, "Chunk bytes has been accepted by the server."

    .line 3
    invoke-interface {p4, p1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 4
    new-instance p1, Ljava/io/BufferedInputStream;

    invoke-interface {p2}, Lcom/onedrive/sdk/http/IConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5
    :try_start_1
    invoke-static {p1}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->streamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p2

    const-class p4, Lcom/onedrive/sdk/extensions/UploadSession;

    invoke-interface {p3, p2, p4}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/onedrive/sdk/extensions/UploadSession;

    .line 6
    new-instance p3, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;

    invoke-direct {p3, p2}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;-><init>(Lcom/onedrive/sdk/extensions/UploadSession;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-object p3

    :catchall_0
    move-exception p2

    move-object v0, p1

    goto :goto_1

    .line 8
    :cond_0
    :try_start_2
    invoke-interface {p2}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    move-result v1

    const/16 v2, 0xc9

    if-eq v1, v2, :cond_3

    invoke-interface {p2}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 9
    :cond_1
    invoke-interface {p2}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    move-result v1

    const/16 v2, 0x190

    if-lt v1, v2, :cond_2

    const-string v1, "Receiving error during upload, see detail on result error"

    .line 10
    invoke-interface {p4, v1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 11
    new-instance p4, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;

    invoke-static {p1, v0, p3, p2}, Lcom/onedrive/sdk/http/OneDriveServiceException;->createFromConnection(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Object;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/http/IConnection;)Lcom/onedrive/sdk/http/OneDriveServiceException;

    move-result-object p1

    invoke-direct {p4, p1}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;-><init>(Lcom/onedrive/sdk/http/OneDriveServiceException;)V

    return-object p4

    :cond_2
    return-object v0

    :cond_3
    :goto_0
    const-string p1, "Upload session is completed, uploaded item returned."

    .line 12
    invoke-interface {p4, p1}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 13
    new-instance p1, Ljava/io/BufferedInputStream;

    invoke-interface {p2}, Lcom/onedrive/sdk/http/IConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 14
    :try_start_3
    invoke-static {p1}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->streamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p2

    .line 15
    iget-object p4, p0, Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;->mDeserializeTypeClass:Ljava/lang/Class;

    invoke-interface {p3, p2, p4}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    .line 16
    new-instance p3, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;

    invoke-direct {p3, p2}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;-><init>(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 17
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-object p3

    :catchall_1
    move-exception p2

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_4
    throw p2
.end method

.method public bridge synthetic generateResult(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/http/IConnection;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/logger/ILogger;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/onedrive/sdk/concurrency/ChunkedUploadResponseHandler;->generateResult(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/http/IConnection;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/logger/ILogger;)Lcom/onedrive/sdk/extensions/ChunkedUploadResult;

    move-result-object p1

    return-object p1
.end method

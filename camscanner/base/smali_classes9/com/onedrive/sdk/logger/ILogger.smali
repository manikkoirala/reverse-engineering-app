.class public interface abstract Lcom/onedrive/sdk/logger/ILogger;
.super Ljava/lang/Object;
.source "ILogger.java"


# virtual methods
.method public abstract getLoggingLevel()Lcom/onedrive/sdk/logger/LoggerLevel;
.end method

.method public abstract logDebug(Ljava/lang/String;)V
.end method

.method public abstract logError(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract setLoggingLevel(Lcom/onedrive/sdk/logger/LoggerLevel;)V
.end method

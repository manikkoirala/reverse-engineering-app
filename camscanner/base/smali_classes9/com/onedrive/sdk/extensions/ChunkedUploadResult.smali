.class public Lcom/onedrive/sdk/extensions/ChunkedUploadResult;
.super Ljava/lang/Object;
.source "ChunkedUploadResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<UploadType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mError:Lcom/onedrive/sdk/core/ClientException;

.field private final mSession:Lcom/onedrive/sdk/extensions/UploadSession;

.field private final mUploadedItem:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TUploadType;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/core/ClientException;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mError:Lcom/onedrive/sdk/core/ClientException;

    const/4 p1, 0x0

    .line 11
    iput-object p1, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mUploadedItem:Ljava/lang/Object;

    .line 12
    iput-object p1, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mSession:Lcom/onedrive/sdk/extensions/UploadSession;

    return-void
.end method

.method public constructor <init>(Lcom/onedrive/sdk/extensions/UploadSession;)V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object p1, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mSession:Lcom/onedrive/sdk/extensions/UploadSession;

    const/4 p1, 0x0

    .line 7
    iput-object p1, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mUploadedItem:Ljava/lang/Object;

    .line 8
    iput-object p1, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mError:Lcom/onedrive/sdk/core/ClientException;

    return-void
.end method

.method public constructor <init>(Lcom/onedrive/sdk/http/OneDriveServiceException;)V
    .locals 3

    .line 13
    new-instance v0, Lcom/onedrive/sdk/core/ClientException;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/onedrive/sdk/http/OneDriveServiceException;->getMessage(Z)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->UploadSessionFailed:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {v0, v1, p1, v2}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    invoke-direct {p0, v0}, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;-><init>(Lcom/onedrive/sdk/core/ClientException;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TUploadType;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mUploadedItem:Ljava/lang/Object;

    const/4 p1, 0x0

    .line 3
    iput-object p1, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mSession:Lcom/onedrive/sdk/extensions/UploadSession;

    .line 4
    iput-object p1, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mError:Lcom/onedrive/sdk/core/ClientException;

    return-void
.end method


# virtual methods
.method public chunkCompleted()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mUploadedItem:Ljava/lang/Object;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mSession:Lcom/onedrive/sdk/extensions/UploadSession;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 13
    :goto_1
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getError()Lcom/onedrive/sdk/core/ClientException;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mError:Lcom/onedrive/sdk/core/ClientException;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getItem()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TUploadType;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mUploadedItem:Ljava/lang/Object;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getSession()Lcom/onedrive/sdk/extensions/UploadSession;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mSession:Lcom/onedrive/sdk/extensions/UploadSession;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public hasError()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mError:Lcom/onedrive/sdk/core/ClientException;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public uploadCompleted()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/ChunkedUploadResult;->mUploadedItem:Ljava/lang/Object;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.class public Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
.super Ljava/lang/Object;
.source "OneDriveClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/onedrive/sdk/extensions/OneDriveClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/onedrive/sdk/extensions/OneDriveClient;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/onedrive/sdk/extensions/OneDriveClient;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method static synthetic access$400(Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;)Lcom/onedrive/sdk/extensions/OneDriveClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$500(Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;Landroid/app/Activity;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->loginAndBuildClient(Landroid/app/Activity;)Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private logger(Lcom/onedrive/sdk/logger/ILogger;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->access$300(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/logger/ILogger;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private loginAndBuildClient(Landroid/app/Activity;)Lcom/onedrive/sdk/extensions/IOneDriveClient;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/core/BaseClient;->validate()V

    .line 4
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/core/BaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object v0

    iget-object v1, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v1}, Lcom/onedrive/sdk/core/BaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v1

    iget-object v2, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v2}, Lcom/onedrive/sdk/core/BaseClient;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    move-result-object v2

    iget-object v3, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v3}, Lcom/onedrive/sdk/core/BaseClient;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    move-result-object v3

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/onedrive/sdk/authentication/IAuthenticator;->init(Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/http/IHttpProvider;Landroid/app/Activity;Lcom/onedrive/sdk/logger/ILogger;)V

    const/4 p1, 0x0

    .line 5
    :try_start_0
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/core/BaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/authentication/IAuthenticator;->loginSilent()Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    move-object v0, p1

    :goto_0
    if-nez v0, :cond_1

    .line 6
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/core/BaseClient;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/onedrive/sdk/authentication/IAuthenticator;->login(Ljava/lang/String;)Lcom/onedrive/sdk/authentication/IAccountInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_1

    .line 7
    :cond_0
    new-instance p1, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;

    const-string v0, "Unable to authenticate silently or interactively"

    sget-object v1, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->AuthenticationFailure:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    invoke-direct {p1, v0, v1}, Lcom/onedrive/sdk/authentication/ClientAuthenticatorException;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    throw p1

    .line 8
    :cond_1
    :goto_1
    iget-object p1, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    return-object p1
.end method


# virtual methods
.method public authenticator(Lcom/onedrive/sdk/authentication/IAuthenticator;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->access$100(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/authentication/IAuthenticator;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public executors(Lcom/onedrive/sdk/concurrency/IExecutors;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->access$200(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/concurrency/IExecutors;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public fromConfig(Lcom/onedrive/sdk/core/IClientConfig;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 2

    .line 1
    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getAuthenticator()Lcom/onedrive/sdk/authentication/IAuthenticator;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->authenticator(Lcom/onedrive/sdk/authentication/IAuthenticator;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->executors(Lcom/onedrive/sdk/concurrency/IExecutors;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getHttpProvider()Lcom/onedrive/sdk/http/IHttpProvider;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v0, v1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->httpProvider(Lcom/onedrive/sdk/http/IHttpProvider;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getLogger()Lcom/onedrive/sdk/logger/ILogger;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-direct {v0, v1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->logger(Lcom/onedrive/sdk/logger/ILogger;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-interface {p1}, Lcom/onedrive/sdk/core/IClientConfig;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-virtual {v0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->serializer(Lcom/onedrive/sdk/serializer/ISerializer;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    return-object p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public httpProvider(Lcom/onedrive/sdk/http/IHttpProvider;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/onedrive/sdk/extensions/OneDriveClient;->access$000(Lcom/onedrive/sdk/extensions/OneDriveClient;Lcom/onedrive/sdk/http/IHttpProvider;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public loginAndBuildClient(Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/core/BaseClient;->validate()V

    .line 2
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    invoke-virtual {v0}, Lcom/onedrive/sdk/core/BaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder$1;-><init>(Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;Landroid/app/Activity;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method public serializer(Lcom/onedrive/sdk/serializer/ISerializer;)Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/extensions/OneDriveClient$Builder;->mClient:Lcom/onedrive/sdk/extensions/OneDriveClient;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/onedrive/sdk/core/BaseClient;->setSerializer(Lcom/onedrive/sdk/serializer/ISerializer;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

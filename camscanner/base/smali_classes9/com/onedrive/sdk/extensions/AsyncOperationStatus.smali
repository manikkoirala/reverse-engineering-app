.class public Lcom/onedrive/sdk/extensions/AsyncOperationStatus;
.super Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;
.source "AsyncOperationStatus.java"


# instance fields
.field public seeOther:Ljava/lang/String;

.field public statusDescription:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static createdCompleted(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/AsyncOperationStatus;
    .locals 3

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object p0, v0, Lcom/onedrive/sdk/extensions/AsyncOperationStatus;->seeOther:Ljava/lang/String;

    .line 7
    .line 8
    const-wide/high16 v1, 0x4059000000000000L    # 100.0

    .line 9
    .line 10
    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    iput-object p0, v0, Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;->percentageComplete:Ljava/lang/Double;

    .line 15
    .line 16
    const-string p0, "Completed"

    .line 17
    .line 18
    iput-object p0, v0, Lcom/onedrive/sdk/generated/BaseAsyncOperationStatus;->status:Ljava/lang/String;

    .line 19
    .line 20
    return-object v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

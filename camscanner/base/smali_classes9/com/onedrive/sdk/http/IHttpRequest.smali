.class public interface abstract Lcom/onedrive/sdk/http/IHttpRequest;
.super Ljava/lang/Object;
.source "IHttpRequest.java"


# virtual methods
.method public abstract addHeader(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract getHeaders()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/HeaderOption;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getHttpMethod()Lcom/onedrive/sdk/http/HttpMethod;
.end method

.method public abstract getOptions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRequestUrl()Ljava/net/URL;
.end method

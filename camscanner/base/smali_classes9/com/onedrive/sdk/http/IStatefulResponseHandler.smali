.class public interface abstract Lcom/onedrive/sdk/http/IStatefulResponseHandler;
.super Ljava/lang/Object;
.source "IStatefulResponseHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ResultType:",
        "Ljava/lang/Object;",
        "DeserializedType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract configConnection(Lcom/onedrive/sdk/http/IConnection;)V
.end method

.method public abstract generateResult(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/http/IConnection;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/logger/ILogger;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Lcom/onedrive/sdk/http/IConnection;",
            "Lcom/onedrive/sdk/serializer/ISerializer;",
            "Lcom/onedrive/sdk/logger/ILogger;",
            ")TResultType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.class public Lcom/onedrive/sdk/http/DefaultHttpProvider;
.super Ljava/lang/Object;
.source "DefaultHttpProvider.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IHttpProvider;


# static fields
.field static final CONTENT_TYPE_HEADER_NAME:Ljava/lang/String; = "Content-Type"

.field static final JSON_CONTENT_TYPE:Ljava/lang/String; = "application/json"


# instance fields
.field private mConnectionFactory:Lcom/onedrive/sdk/http/IConnectionFactory;

.field private final mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

.field private final mLogger:Lcom/onedrive/sdk/logger/ILogger;

.field private final mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

.field private final mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;


# direct methods
.method public constructor <init>(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/http/IRequestInterceptor;Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/logger/ILogger;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

    .line 7
    .line 8
    iput-object p3, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 9
    .line 10
    iput-object p4, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 11
    .line 12
    new-instance p1, Lcom/onedrive/sdk/http/DefaultConnectionFactory;

    .line 13
    .line 14
    invoke-direct {p1}, Lcom/onedrive/sdk/http/DefaultConnectionFactory;-><init>()V

    .line 15
    .line 16
    .line 17
    iput-object p1, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mConnectionFactory:Lcom/onedrive/sdk/http/IConnectionFactory;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method static synthetic access$000(Lcom/onedrive/sdk/http/DefaultHttpProvider;Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->sendRequestInternal(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
.end method

.method static synthetic access$100(Lcom/onedrive/sdk/http/DefaultHttpProvider;)Lcom/onedrive/sdk/concurrency/IExecutors;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private handleBinaryStream(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 0

    .line 1
    return-object p1
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private handleErrorResponse(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Object;Lcom/onedrive/sdk/http/IConnection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Body:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "TBody;",
            "Lcom/onedrive/sdk/http/IConnection;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 2
    .line 3
    invoke-static {p1, p2, v0, p3}, Lcom/onedrive/sdk/http/OneDriveServiceException;->createFromConnection(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Object;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/http/IConnection;)Lcom/onedrive/sdk/http/OneDriveServiceException;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    throw p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private handleJsonResponse(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/InputStream;",
            "Ljava/lang/Class<",
            "TResult;>;)TResult;"
        }
    .end annotation

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return-object p1

    .line 5
    :cond_0
    invoke-static {p1}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->streamToString(Ljava/io/InputStream;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-interface {v0, p1, p2}, Lcom/onedrive/sdk/serializer/ISerializer;->deserializeObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private sendRequestInternal(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            "Body:",
            "Ljava/lang/Object;",
            "DeserializeType:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Ljava/lang/Class<",
            "TResult;>;TBody;",
            "Lcom/onedrive/sdk/concurrency/IProgressCallback<",
            "TResult;>;",
            "Lcom/onedrive/sdk/http/IStatefulResponseHandler<",
            "TResult;TDeserializeType;>;)TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    move-object/from16 v2, p2

    .line 6
    .line 7
    move-object/from16 v3, p3

    .line 8
    .line 9
    move-object/from16 v4, p4

    .line 10
    .line 11
    move-object/from16 v5, p5

    .line 12
    .line 13
    :try_start_0
    iget-object v8, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mRequestInterceptor:Lcom/onedrive/sdk/http/IRequestInterceptor;

    .line 14
    .line 15
    if-eqz v8, :cond_0

    .line 16
    .line 17
    invoke-interface {v8, v0}, Lcom/onedrive/sdk/http/IRequestInterceptor;->intercept(Lcom/onedrive/sdk/http/IHttpRequest;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    invoke-interface/range {p1 .. p1}, Lcom/onedrive/sdk/http/IHttpRequest;->getRequestUrl()Ljava/net/URL;

    .line 21
    .line 22
    .line 23
    move-result-object v8

    .line 24
    iget-object v9, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 25
    .line 26
    new-instance v10, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v11, "Starting to send request, URL "

    .line 32
    .line 33
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v8

    .line 40
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v8

    .line 47
    invoke-interface {v9, v8}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    iget-object v8, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mConnectionFactory:Lcom/onedrive/sdk/http/IConnectionFactory;

    .line 51
    .line 52
    invoke-interface {v8, v0}, Lcom/onedrive/sdk/http/IConnectionFactory;->createFromRequest(Lcom/onedrive/sdk/http/IHttpRequest;)Lcom/onedrive/sdk/http/IConnection;

    .line 53
    .line 54
    .line 55
    move-result-object v8
    :try_end_0
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 56
    :try_start_1
    iget-object v10, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 57
    .line 58
    new-instance v11, Ljava/lang/StringBuilder;

    .line 59
    .line 60
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    .line 62
    .line 63
    const-string v12, "Request Method "

    .line 64
    .line 65
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-interface/range {p1 .. p1}, Lcom/onedrive/sdk/http/IHttpRequest;->getHttpMethod()Lcom/onedrive/sdk/http/HttpMethod;

    .line 69
    .line 70
    .line 71
    move-result-object v12

    .line 72
    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v12

    .line 76
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v11

    .line 83
    invoke-interface {v10, v11}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_9

    .line 84
    .line 85
    .line 86
    const-string v10, "application/json"

    .line 87
    .line 88
    const-string v11, "Content-Type"

    .line 89
    .line 90
    if-nez v3, :cond_1

    .line 91
    .line 92
    const/4 v12, 0x0

    .line 93
    goto :goto_1

    .line 94
    :cond_1
    :try_start_2
    instance-of v12, v3, [B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_9

    .line 95
    .line 96
    if-eqz v12, :cond_2

    .line 97
    .line 98
    :try_start_3
    iget-object v12, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 99
    .line 100
    const-string v13, "Sending byte[] as request body"

    .line 101
    .line 102
    invoke-interface {v12, v13}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    move-object v12, v3

    .line 106
    check-cast v12, [B

    .line 107
    .line 108
    const-string v13, "application/octet-stream"

    .line 109
    .line 110
    invoke-interface {v8, v11, v13}, Lcom/onedrive/sdk/http/IConnection;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    array-length v13, v12

    .line 114
    invoke-interface {v8, v13}, Lcom/onedrive/sdk/http/IConnection;->setContentLength(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 115
    .line 116
    .line 117
    goto :goto_1

    .line 118
    :catchall_0
    move-exception v0

    .line 119
    const/4 v2, 0x0

    .line 120
    const/4 v9, 0x0

    .line 121
    :goto_0
    const/4 v12, 0x0

    .line 122
    const/4 v14, 0x1

    .line 123
    goto/16 :goto_8

    .line 124
    .line 125
    :cond_2
    :try_start_4
    iget-object v12, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 126
    .line 127
    new-instance v13, Ljava/lang/StringBuilder;

    .line 128
    .line 129
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 130
    .line 131
    .line 132
    const-string v14, "Sending "

    .line 133
    .line 134
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 138
    .line 139
    .line 140
    move-result-object v14

    .line 141
    invoke-virtual {v14}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v14

    .line 145
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    .line 147
    .line 148
    const-string v14, " as request body"

    .line 149
    .line 150
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    .line 152
    .line 153
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 154
    .line 155
    .line 156
    move-result-object v13

    .line 157
    invoke-interface {v12, v13}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    iget-object v12, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 161
    .line 162
    invoke-interface {v12, v3}, Lcom/onedrive/sdk/serializer/ISerializer;->serializeObject(Ljava/lang/Object;)Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v12

    .line 166
    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    .line 167
    .line 168
    .line 169
    move-result-object v12

    .line 170
    invoke-interface {v8, v11, v10}, Lcom/onedrive/sdk/http/IConnection;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    .line 172
    .line 173
    array-length v13, v12

    .line 174
    invoke-interface {v8, v13}, Lcom/onedrive/sdk/http/IConnection;->setContentLength(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_9

    .line 175
    .line 176
    .line 177
    :goto_1
    if-eqz v12, :cond_5

    .line 178
    .line 179
    :try_start_5
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getOutputStream()Ljava/io/OutputStream;

    .line 180
    .line 181
    .line 182
    move-result-object v13
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 183
    :try_start_6
    new-instance v14, Ljava/io/BufferedOutputStream;

    .line 184
    .line 185
    invoke-direct {v14, v13}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 186
    .line 187
    .line 188
    const/4 v15, 0x0

    .line 189
    :cond_3
    array-length v9, v12

    .line 190
    sub-int/2addr v9, v15

    .line 191
    const/16 v6, 0x1000

    .line 192
    .line 193
    invoke-static {v6, v9}, Ljava/lang/Math;->min(II)I

    .line 194
    .line 195
    .line 196
    move-result v6

    .line 197
    invoke-virtual {v14, v12, v15, v6}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 198
    .line 199
    .line 200
    add-int/2addr v15, v6

    .line 201
    if-eqz v4, :cond_4

    .line 202
    .line 203
    iget-object v9, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    .line 204
    .line 205
    array-length v7, v12

    .line 206
    invoke-interface {v9, v15, v7, v4}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnForeground(IILcom/onedrive/sdk/concurrency/IProgressCallback;)V

    .line 207
    .line 208
    .line 209
    :cond_4
    if-gtz v6, :cond_3

    .line 210
    .line 211
    invoke-virtual {v14}, Ljava/io/OutputStream;->close()V

    .line 212
    .line 213
    .line 214
    goto :goto_2

    .line 215
    :cond_5
    const/4 v13, 0x0

    .line 216
    :goto_2
    if-eqz v5, :cond_6

    .line 217
    .line 218
    invoke-interface {v5, v8}, Lcom/onedrive/sdk/http/IStatefulResponseHandler;->configConnection(Lcom/onedrive/sdk/http/IConnection;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 219
    .line 220
    .line 221
    goto :goto_3

    .line 222
    :catchall_1
    move-exception v0

    .line 223
    move-object v9, v13

    .line 224
    const/4 v2, 0x0

    .line 225
    goto :goto_0

    .line 226
    :cond_6
    :goto_3
    :try_start_7
    iget-object v4, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 227
    .line 228
    const-string v6, "Response code %d, %s"

    .line 229
    .line 230
    const/4 v7, 0x2

    .line 231
    new-array v7, v7, [Ljava/lang/Object;

    .line 232
    .line 233
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    .line 234
    .line 235
    .line 236
    move-result v9

    .line 237
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 238
    .line 239
    .line 240
    move-result-object v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_8

    .line 241
    const/4 v12, 0x0

    .line 242
    :try_start_8
    aput-object v9, v7, v12

    .line 243
    .line 244
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseMessage()Ljava/lang/String;

    .line 245
    .line 246
    .line 247
    move-result-object v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_7

    .line 248
    const/4 v14, 0x1

    .line 249
    :try_start_9
    aput-object v9, v7, v14

    .line 250
    .line 251
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 252
    .line 253
    .line 254
    move-result-object v6

    .line 255
    invoke-interface {v4, v6}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    .line 256
    .line 257
    .line 258
    if-eqz v5, :cond_8

    .line 259
    .line 260
    :try_start_a
    iget-object v2, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 261
    .line 262
    const-string v3, "StatefulResponse is handling the HTTP response."

    .line 263
    .line 264
    invoke-interface {v2, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 265
    .line 266
    .line 267
    invoke-virtual/range {p0 .. p0}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    .line 268
    .line 269
    .line 270
    move-result-object v2

    .line 271
    iget-object v3, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 272
    .line 273
    invoke-interface {v5, v0, v8, v2, v3}, Lcom/onedrive/sdk/http/IStatefulResponseHandler;->generateResult(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/http/IConnection;Lcom/onedrive/sdk/serializer/ISerializer;Lcom/onedrive/sdk/logger/ILogger;)Ljava/lang/Object;

    .line 274
    .line 275
    .line 276
    move-result-object v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 277
    if-eqz v13, :cond_7

    .line 278
    .line 279
    :try_start_b
    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V
    :try_end_b
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1

    .line 280
    .line 281
    .line 282
    :cond_7
    return-object v0

    .line 283
    :catchall_2
    move-exception v0

    .line 284
    move-object v9, v13

    .line 285
    const/4 v2, 0x0

    .line 286
    goto/16 :goto_8

    .line 287
    .line 288
    :cond_8
    :try_start_c
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    .line 289
    .line 290
    .line 291
    move-result v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    .line 292
    const/16 v5, 0x190

    .line 293
    .line 294
    if-lt v4, v5, :cond_9

    .line 295
    .line 296
    :try_start_d
    iget-object v4, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 297
    .line 298
    const-string v5, "Handling error response"

    .line 299
    .line 300
    invoke-interface {v4, v5}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 301
    .line 302
    .line 303
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getInputStream()Ljava/io/InputStream;

    .line 304
    .line 305
    .line 306
    move-result-object v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 307
    :try_start_e
    invoke-direct {v1, v0, v3, v8}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->handleErrorResponse(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Object;Lcom/onedrive/sdk/http/IConnection;)V

    .line 308
    .line 309
    .line 310
    goto :goto_4

    .line 311
    :cond_9
    const/4 v4, 0x0

    .line 312
    :goto_4
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    .line 313
    .line 314
    .line 315
    move-result v0

    .line 316
    const/16 v3, 0xcc

    .line 317
    .line 318
    if-eq v0, v3, :cond_11

    .line 319
    .line 320
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    .line 321
    .line 322
    .line 323
    move-result v0

    .line 324
    const/16 v3, 0x130

    .line 325
    .line 326
    if-ne v0, v3, :cond_a

    .line 327
    .line 328
    goto/16 :goto_5

    .line 329
    .line 330
    :cond_a
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getResponseCode()I

    .line 331
    .line 332
    .line 333
    move-result v0

    .line 334
    const/16 v3, 0xca

    .line 335
    .line 336
    if-ne v0, v3, :cond_d

    .line 337
    .line 338
    iget-object v0, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 339
    .line 340
    const-string v3, "Handling accepted response"

    .line 341
    .line 342
    invoke-interface {v0, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 343
    .line 344
    .line 345
    const-class v0, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    .line 346
    .line 347
    if-ne v2, v0, :cond_d

    .line 348
    .line 349
    new-instance v0, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;

    .line 350
    .line 351
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getHeaders()Ljava/util/Map;

    .line 352
    .line 353
    .line 354
    move-result-object v2

    .line 355
    const-string v3, "Location"

    .line 356
    .line 357
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    .line 359
    .line 360
    move-result-object v2

    .line 361
    check-cast v2, Ljava/lang/String;

    .line 362
    .line 363
    invoke-direct {v0, v2}, Lcom/onedrive/sdk/concurrency/AsyncMonitorLocation;-><init>(Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    .line 364
    .line 365
    .line 366
    if-eqz v13, :cond_b

    .line 367
    .line 368
    :try_start_f
    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V

    .line 369
    .line 370
    .line 371
    :cond_b
    if-eqz v4, :cond_c

    .line 372
    .line 373
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 374
    .line 375
    .line 376
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->close()V
    :try_end_f
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1

    .line 377
    .line 378
    .line 379
    :cond_c
    return-object v0

    .line 380
    :cond_d
    :try_start_10
    new-instance v9, Ljava/io/BufferedInputStream;

    .line 381
    .line 382
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getInputStream()Ljava/io/InputStream;

    .line 383
    .line 384
    .line 385
    move-result-object v0

    .line 386
    invoke-direct {v9, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    .line 387
    .line 388
    .line 389
    :try_start_11
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->getHeaders()Ljava/util/Map;

    .line 390
    .line 391
    .line 392
    move-result-object v0

    .line 393
    invoke-interface {v0, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    .line 395
    .line 396
    move-result-object v0

    .line 397
    check-cast v0, Ljava/lang/String;

    .line 398
    .line 399
    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 400
    .line 401
    .line 402
    move-result v0

    .line 403
    if-eqz v0, :cond_f

    .line 404
    .line 405
    iget-object v0, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 406
    .line 407
    const-string v3, "Response json"

    .line 408
    .line 409
    invoke-interface {v0, v3}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V

    .line 410
    .line 411
    .line 412
    invoke-direct {v1, v9, v2}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->handleJsonResponse(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    .line 413
    .line 414
    .line 415
    move-result-object v0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    .line 416
    if-eqz v13, :cond_e

    .line 417
    .line 418
    :try_start_12
    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V

    .line 419
    .line 420
    .line 421
    :cond_e
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 422
    .line 423
    .line 424
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->close()V
    :try_end_12
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_12 .. :try_end_12} :catch_0
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_1

    .line 425
    .line 426
    .line 427
    return-object v0

    .line 428
    :cond_f
    :try_start_13
    iget-object v0, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 429
    .line 430
    const-string v2, "Response binary"

    .line 431
    .line 432
    invoke-interface {v0, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    .line 433
    .line 434
    .line 435
    :try_start_14
    invoke-direct {v1, v9}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->handleBinaryStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    .line 436
    .line 437
    .line 438
    move-result-object v0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    .line 439
    if-eqz v13, :cond_10

    .line 440
    .line 441
    :try_start_15
    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V
    :try_end_15
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_15 .. :try_end_15} :catch_0
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_1

    .line 442
    .line 443
    .line 444
    :cond_10
    return-object v0

    .line 445
    :catchall_3
    move-exception v0

    .line 446
    move-object v2, v9

    .line 447
    move-object v9, v13

    .line 448
    const/16 v16, 0x1

    .line 449
    .line 450
    goto :goto_9

    .line 451
    :catchall_4
    move-exception v0

    .line 452
    move-object v2, v9

    .line 453
    goto :goto_7

    .line 454
    :cond_11
    :goto_5
    :try_start_16
    iget-object v0, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 455
    .line 456
    const-string v2, "Handling response with no body"

    .line 457
    .line 458
    invoke-interface {v0, v2}, Lcom/onedrive/sdk/logger/ILogger;->logDebug(Ljava/lang/String;)V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_5

    .line 459
    .line 460
    .line 461
    if-eqz v13, :cond_12

    .line 462
    .line 463
    :try_start_17
    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V

    .line 464
    .line 465
    .line 466
    :cond_12
    if-eqz v4, :cond_13

    .line 467
    .line 468
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 469
    .line 470
    .line 471
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->close()V

    .line 472
    .line 473
    .line 474
    :cond_13
    const/4 v2, 0x0

    .line 475
    return-object v2

    .line 476
    :catchall_5
    move-exception v0

    .line 477
    move-object v2, v4

    .line 478
    goto :goto_7

    .line 479
    :catchall_6
    move-exception v0

    .line 480
    const/4 v2, 0x0

    .line 481
    goto :goto_7

    .line 482
    :catchall_7
    move-exception v0

    .line 483
    const/4 v2, 0x0

    .line 484
    goto :goto_6

    .line 485
    :catchall_8
    move-exception v0

    .line 486
    const/4 v2, 0x0

    .line 487
    const/4 v12, 0x0

    .line 488
    :goto_6
    const/4 v14, 0x1

    .line 489
    :goto_7
    move-object v9, v13

    .line 490
    goto :goto_8

    .line 491
    :catchall_9
    move-exception v0

    .line 492
    const/4 v2, 0x0

    .line 493
    const/4 v12, 0x0

    .line 494
    const/4 v14, 0x1

    .line 495
    move-object v9, v2

    .line 496
    :goto_8
    const/16 v16, 0x0

    .line 497
    .line 498
    :goto_9
    if-eqz v9, :cond_14

    .line 499
    .line 500
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 501
    .line 502
    .line 503
    goto :goto_a

    .line 504
    :catch_0
    move-exception v0

    .line 505
    goto :goto_b

    .line 506
    :cond_14
    :goto_a
    if-nez v16, :cond_15

    .line 507
    .line 508
    if-eqz v2, :cond_15

    .line 509
    .line 510
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 511
    .line 512
    .line 513
    invoke-interface {v8}, Lcom/onedrive/sdk/http/IConnection;->close()V

    .line 514
    .line 515
    .line 516
    :cond_15
    throw v0
    :try_end_17
    .catch Lcom/onedrive/sdk/http/OneDriveServiceException; {:try_start_17 .. :try_end_17} :catch_0
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_1

    .line 517
    :catch_1
    move-exception v0

    .line 518
    new-instance v2, Lcom/onedrive/sdk/core/ClientException;

    .line 519
    .line 520
    sget-object v3, Lcom/onedrive/sdk/core/OneDriveErrorCodes;->GeneralException:Lcom/onedrive/sdk/core/OneDriveErrorCodes;

    .line 521
    .line 522
    const-string v4, "Error during http request"

    .line 523
    .line 524
    invoke-direct {v2, v4, v0, v3}, Lcom/onedrive/sdk/core/ClientException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/onedrive/sdk/core/OneDriveErrorCodes;)V

    .line 525
    .line 526
    .line 527
    iget-object v0, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 528
    .line 529
    invoke-interface {v0, v4, v2}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 530
    .line 531
    .line 532
    throw v2

    .line 533
    :catch_2
    move-exception v0

    .line 534
    const/4 v12, 0x0

    .line 535
    const/4 v14, 0x1

    .line 536
    :goto_b
    iget-object v2, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 537
    .line 538
    invoke-interface {v2}, Lcom/onedrive/sdk/logger/ILogger;->getLoggingLevel()Lcom/onedrive/sdk/logger/LoggerLevel;

    .line 539
    .line 540
    .line 541
    move-result-object v2

    .line 542
    sget-object v3, Lcom/onedrive/sdk/logger/LoggerLevel;->Debug:Lcom/onedrive/sdk/logger/LoggerLevel;

    .line 543
    .line 544
    if-ne v2, v3, :cond_16

    .line 545
    .line 546
    const/4 v6, 0x1

    .line 547
    goto :goto_c

    .line 548
    :cond_16
    const/4 v6, 0x0

    .line 549
    :goto_c
    iget-object v2, v1, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mLogger:Lcom/onedrive/sdk/logger/ILogger;

    .line 550
    .line 551
    new-instance v3, Ljava/lang/StringBuilder;

    .line 552
    .line 553
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 554
    .line 555
    .line 556
    const-string v4, "OneDrive Service exception "

    .line 557
    .line 558
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559
    .line 560
    .line 561
    invoke-virtual {v0, v6}, Lcom/onedrive/sdk/http/OneDriveServiceException;->getMessage(Z)Ljava/lang/String;

    .line 562
    .line 563
    .line 564
    move-result-object v4

    .line 565
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    .line 567
    .line 568
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 569
    .line 570
    .line 571
    move-result-object v3

    .line 572
    invoke-interface {v2, v3, v0}, Lcom/onedrive/sdk/logger/ILogger;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 573
    .line 574
    .line 575
    throw v0
.end method

.method public static streamToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/util/Scanner;

    .line 2
    .line 3
    const-string v1, "UTF-8"

    .line 4
    .line 5
    invoke-direct {v0, p0, v1}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string p0, "\\A"

    .line 9
    .line 10
    invoke-virtual {v0, p0}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    invoke-virtual {p0}, Ljava/util/Scanner;->next()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    return-object p0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            "Body:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Ljava/lang/Class<",
            "TResult;>;TBody;)TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public send(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            "Body:",
            "Ljava/lang/Object;",
            "DeserializeType:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Ljava/lang/Class<",
            "TResult;>;TBody;",
            "Lcom/onedrive/sdk/http/IStatefulResponseHandler<",
            "TResult;TDeserializeType;>;)TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    .line 5
    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/http/DefaultHttpProvider;->sendRequestInternal(Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/http/IStatefulResponseHandler;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public send(Lcom/onedrive/sdk/http/IHttpRequest;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Result:",
            "Ljava/lang/Object;",
            "Body:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/onedrive/sdk/http/IHttpRequest;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "TResult;>;",
            "Ljava/lang/Class<",
            "TResult;>;TBody;)V"
        }
    .end annotation

    .line 1
    instance-of v0, p2, Lcom/onedrive/sdk/concurrency/IProgressCallback;

    if-eqz v0, :cond_0

    .line 2
    move-object v0, p2

    check-cast v0, Lcom/onedrive/sdk/concurrency/IProgressCallback;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v6, v0

    .line 3
    iget-object v0, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mExecutors:Lcom/onedrive/sdk/concurrency/IExecutors;

    new-instance v8, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/onedrive/sdk/http/DefaultHttpProvider$1;-><init>(Lcom/onedrive/sdk/http/DefaultHttpProvider;Lcom/onedrive/sdk/http/IHttpRequest;Ljava/lang/Class;Ljava/lang/Object;Lcom/onedrive/sdk/concurrency/IProgressCallback;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v8}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method setConnectionFactory(Lcom/onedrive/sdk/http/IConnectionFactory;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/onedrive/sdk/http/DefaultHttpProvider;->mConnectionFactory:Lcom/onedrive/sdk/http/IConnectionFactory;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.class public interface abstract Lcom/onedrive/sdk/generated/IBaseItemRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseItemRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/IItemRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IItemRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IItemRequest;"
        }
    .end annotation
.end method

.method public abstract getChildren()Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;
.end method

.method public abstract getChildren(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;
.end method

.method public abstract getContent()Lcom/onedrive/sdk/extensions/IItemStreamRequestBuilder;
.end method

.method public abstract getCopy(Ljava/lang/String;Lcom/onedrive/sdk/extensions/ItemReference;)Lcom/onedrive/sdk/extensions/ICopyRequestBuilder;
.end method

.method public abstract getCreateLink(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ICreateLinkRequestBuilder;
.end method

.method public abstract getCreateSession(Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;)Lcom/onedrive/sdk/extensions/ICreateSessionRequestBuilder;
.end method

.method public abstract getDelta(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDeltaRequestBuilder;
.end method

.method public abstract getPermissions()Lcom/onedrive/sdk/extensions/IPermissionCollectionRequestBuilder;
.end method

.method public abstract getPermissions(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IPermissionRequestBuilder;
.end method

.method public abstract getSearch(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ISearchRequestBuilder;
.end method

.method public abstract getThumbnails()Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequestBuilder;
.end method

.method public abstract getThumbnails(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IThumbnailSetRequestBuilder;
.end method

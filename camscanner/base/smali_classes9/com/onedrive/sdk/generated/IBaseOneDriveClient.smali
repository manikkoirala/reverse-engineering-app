.class public interface abstract Lcom/onedrive/sdk/generated/IBaseOneDriveClient;
.super Ljava/lang/Object;
.source "IBaseOneDriveClient.java"

# interfaces
.implements Lcom/onedrive/sdk/core/IBaseClient;


# virtual methods
.method public abstract getDrive(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;
.end method

.method public abstract getDrives()Lcom/onedrive/sdk/extensions/IDriveCollectionRequestBuilder;
.end method

.method public abstract getShare(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IShareRequestBuilder;
.end method

.method public abstract getShares()Lcom/onedrive/sdk/extensions/IShareCollectionRequestBuilder;
.end method

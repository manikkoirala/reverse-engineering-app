.class public interface abstract Lcom/onedrive/sdk/generated/IBaseThumbnailSetCollectionRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseThumbnailSetCollectionRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequest;"
        }
    .end annotation
.end method

.method public abstract byId(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IThumbnailSetRequestBuilder;
.end method

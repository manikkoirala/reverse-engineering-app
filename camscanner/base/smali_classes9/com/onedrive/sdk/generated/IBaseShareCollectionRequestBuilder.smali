.class public interface abstract Lcom/onedrive/sdk/generated/IBaseShareCollectionRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseShareCollectionRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/IShareCollectionRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IShareCollectionRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IShareCollectionRequest;"
        }
    .end annotation
.end method

.method public abstract byId(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IShareRequestBuilder;
.end method

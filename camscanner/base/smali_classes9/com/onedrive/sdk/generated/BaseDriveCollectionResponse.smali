.class public Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;
.super Ljava/lang/Object;
.source "BaseDriveCollectionResponse.java"

# interfaces
.implements Lcom/onedrive/sdk/serializer/IJsonBackedObject;


# instance fields
.field private transient mRawObject:Lcom/google/gson/JsonObject;

.field private transient mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

.field public nextLink:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "@odata.nextLink"
    .end annotation
.end field

.field public value:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "value"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/extensions/Drive;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public getRawObject()Lcom/google/gson/JsonObject;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;->mRawObject:Lcom/google/gson/JsonObject;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method protected getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;->mRawObject:Lcom/google/gson/JsonObject;

    .line 4
    .line 5
    const-string/jumbo p1, "value"

    .line 6
    .line 7
    .line 8
    invoke-virtual {p2, p1}, Lcom/google/gson/JsonObject;->〇O00(Ljava/lang/String;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-virtual {p2, p1}, Lcom/google/gson/JsonObject;->〇〇808〇(Ljava/lang/String;)Lcom/google/gson/JsonArray;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const/4 p2, 0x0

    .line 19
    :goto_0
    invoke-virtual {p1}, Lcom/google/gson/JsonArray;->size()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-ge p2, v0, :cond_0

    .line 24
    .line 25
    iget-object v0, p0, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;->value:Ljava/util/List;

    .line 26
    .line 27
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    check-cast v0, Lcom/onedrive/sdk/extensions/Drive;

    .line 32
    .line 33
    iget-object v1, p0, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;->mSerializer:Lcom/onedrive/sdk/serializer/ISerializer;

    .line 34
    .line 35
    invoke-virtual {p1, p2}, Lcom/google/gson/JsonArray;->〇8o8o〇(I)Lcom/google/gson/JsonElement;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    check-cast v2, Lcom/google/gson/JsonObject;

    .line 40
    .line 41
    invoke-virtual {v0, v1, v2}, Lcom/onedrive/sdk/generated/BaseDrive;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    .line 42
    .line 43
    .line 44
    add-int/lit8 p2, p2, 0x1

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.class public Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;
.super Lcom/onedrive/sdk/http/BaseRequestBuilder;
.source "BaseItemRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseItemRequestBuilder;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/onedrive/sdk/http/BaseRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public buildRequest()Lcom/onedrive/sdk/extensions/IItemRequest;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getOptions()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseItemRequestBuilder;->buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IItemRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IItemRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IItemRequest;"
        }
    .end annotation

    .line 2
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemRequest;

    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/onedrive/sdk/extensions/ItemRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getChildren()Lcom/onedrive/sdk/extensions/IItemCollectionRequestBuilder;
    .locals 4

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemCollectionRequestBuilder;

    const-string v1, "children"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/ItemCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getChildren(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;
    .locals 3

    .line 2
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "children"

    invoke-virtual {p0, v2}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getContent()Lcom/onedrive/sdk/extensions/IItemStreamRequestBuilder;
    .locals 4

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemStreamRequestBuilder;

    .line 2
    .line 3
    const-string v1, "content"

    .line 4
    .line 5
    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v3, 0x0

    .line 14
    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/ItemStreamRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getCopy(Ljava/lang/String;Lcom/onedrive/sdk/extensions/ItemReference;)Lcom/onedrive/sdk/extensions/ICopyRequestBuilder;
    .locals 7

    .line 1
    new-instance v6, Lcom/onedrive/sdk/extensions/CopyRequestBuilder;

    .line 2
    .line 3
    const-string v0, "action.copy"

    .line 4
    .line 5
    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v3, 0x0

    .line 14
    move-object v0, v6

    .line 15
    move-object v4, p1

    .line 16
    move-object v5, p2

    .line 17
    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/extensions/CopyRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;Lcom/onedrive/sdk/extensions/ItemReference;)V

    .line 18
    .line 19
    .line 20
    return-object v6
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public getCreateLink(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ICreateLinkRequestBuilder;
    .locals 4

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/CreateLinkRequestBuilder;

    .line 2
    .line 3
    const-string v1, "action.createLink"

    .line 4
    .line 5
    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v3, 0x0

    .line 14
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/onedrive/sdk/extensions/CreateLinkRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public getCreateSession(Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;)Lcom/onedrive/sdk/extensions/ICreateSessionRequestBuilder;
    .locals 4

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/CreateSessionRequestBuilder;

    .line 2
    .line 3
    const-string v1, "action.createUploadSession"

    .line 4
    .line 5
    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v3, 0x0

    .line 14
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/onedrive/sdk/extensions/CreateSessionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Lcom/onedrive/sdk/extensions/ChunkedUploadSessionDescriptor;)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public getDelta(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDeltaRequestBuilder;
    .locals 4

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/DeltaRequestBuilder;

    .line 2
    .line 3
    const-string/jumbo v1, "view.delta"

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    const/4 v3, 0x0

    .line 15
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/onedrive/sdk/extensions/DeltaRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public getPermissions()Lcom/onedrive/sdk/extensions/IPermissionCollectionRequestBuilder;
    .locals 4

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/PermissionCollectionRequestBuilder;

    const-string v1, "permissions"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/PermissionCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getPermissions(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IPermissionRequestBuilder;
    .locals 3

    .line 2
    new-instance v0, Lcom/onedrive/sdk/extensions/PermissionRequestBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "permissions"

    invoke-virtual {p0, v2}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/PermissionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getSearch(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ISearchRequestBuilder;
    .locals 4

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/SearchRequestBuilder;

    .line 2
    .line 3
    const-string/jumbo v1, "view.search"

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    const/4 v3, 0x0

    .line 15
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/onedrive/sdk/extensions/SearchRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public getThumbnails()Lcom/onedrive/sdk/extensions/IThumbnailSetCollectionRequestBuilder;
    .locals 4

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/ThumbnailSetCollectionRequestBuilder;

    const-string/jumbo v1, "thumbnails"

    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/ThumbnailSetCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getThumbnails(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IThumbnailSetRequestBuilder;
    .locals 3

    .line 2
    new-instance v0, Lcom/onedrive/sdk/extensions/ThumbnailSetRequestBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "thumbnails"

    invoke-virtual {p0, v2}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/ThumbnailSetRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

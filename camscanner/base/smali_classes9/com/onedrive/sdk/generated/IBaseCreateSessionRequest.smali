.class public interface abstract Lcom/onedrive/sdk/generated/IBaseCreateSessionRequest;
.super Ljava/lang/Object;
.source "IBaseCreateSessionRequest.java"


# virtual methods
.method public abstract create()Lcom/onedrive/sdk/extensions/UploadSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract create(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/UploadSession;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ICreateSessionRequest;
.end method

.method public abstract post()Lcom/onedrive/sdk/extensions/UploadSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract post(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/UploadSession;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/ICreateSessionRequest;
.end method

.method public abstract top(I)Lcom/onedrive/sdk/extensions/ICreateSessionRequest;
.end method

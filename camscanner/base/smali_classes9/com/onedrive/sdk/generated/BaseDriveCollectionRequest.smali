.class public Lcom/onedrive/sdk/generated/BaseDriveCollectionRequest;
.super Lcom/onedrive/sdk/http/BaseCollectionRequest;
.source "BaseDriveCollectionRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseDriveCollectionRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/onedrive/sdk/http/BaseCollectionRequest<",
        "Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;",
        "Lcom/onedrive/sdk/extensions/IDriveCollectionPage;",
        ">;",
        "Lcom/onedrive/sdk/generated/IBaseDriveCollectionRequest;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-class v4, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;

    .line 2
    .line 3
    const-class v5, Lcom/onedrive/sdk/extensions/IDriveCollectionPage;

    .line 4
    .line 5
    move-object v0, p0

    .line 6
    move-object v1, p1

    .line 7
    move-object v2, p2

    .line 8
    move-object v3, p3

    .line 9
    invoke-direct/range {v0 .. v5}, Lcom/onedrive/sdk/http/BaseCollectionRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public buildFromResponse(Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;)Lcom/onedrive/sdk/extensions/IDriveCollectionPage;
    .locals 4

    .line 1
    iget-object v0, p1, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;->nextLink:Ljava/lang/String;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    new-instance v2, Lcom/onedrive/sdk/extensions/DriveCollectionRequestBuilder;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseCollectionRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    .line 9
    .line 10
    .line 11
    move-result-object v3

    .line 12
    invoke-virtual {v3}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    invoke-direct {v2, v0, v3, v1}, Lcom/onedrive/sdk/extensions/DriveCollectionRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    .line 17
    .line 18
    .line 19
    move-object v1, v2

    .line 20
    :cond_0
    new-instance v0, Lcom/onedrive/sdk/extensions/DriveCollectionPage;

    .line 21
    .line 22
    invoke-direct {v0, p1, v1}, Lcom/onedrive/sdk/extensions/DriveCollectionPage;-><init>(Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;Lcom/onedrive/sdk/extensions/IDriveCollectionRequestBuilder;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;->getSerializer()Lcom/onedrive/sdk/serializer/ISerializer;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {p1}, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;->getRawObject()Lcom/google/gson/JsonObject;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-virtual {v0, v1, p1}, Lcom/onedrive/sdk/http/BaseCollectionPage;->setRawObject(Lcom/onedrive/sdk/serializer/ISerializer;Lcom/google/gson/JsonObject;)V

    .line 34
    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDriveCollectionRequest;
    .locals 2

    .line 1
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    .line 2
    .line 3
    const-string v1, "expand"

    .line 4
    .line 5
    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/http/BaseCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 9
    .line 10
    .line 11
    move-object p1, p0

    .line 12
    check-cast p1, Lcom/onedrive/sdk/extensions/DriveCollectionRequest;

    .line 13
    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public get()Lcom/onedrive/sdk/extensions/IDriveCollectionPage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 3
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseCollectionRequest;->send()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;

    .line 4
    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseDriveCollectionRequest;->buildFromResponse(Lcom/onedrive/sdk/generated/BaseDriveCollectionResponse;)Lcom/onedrive/sdk/extensions/IDriveCollectionPage;

    move-result-object v0

    return-object v0
.end method

.method public get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/IDriveCollectionPage;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseCollectionRequest;->getBaseRequest()Lcom/onedrive/sdk/http/BaseRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/onedrive/sdk/http/BaseRequest;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/onedrive/sdk/core/IBaseClient;->getExecutors()Lcom/onedrive/sdk/concurrency/IExecutors;

    move-result-object v0

    .line 2
    new-instance v1, Lcom/onedrive/sdk/generated/BaseDriveCollectionRequest$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/onedrive/sdk/generated/BaseDriveCollectionRequest$1;-><init>(Lcom/onedrive/sdk/generated/BaseDriveCollectionRequest;Lcom/onedrive/sdk/concurrency/IExecutors;Lcom/onedrive/sdk/concurrency/ICallback;)V

    invoke-interface {v0, v1}, Lcom/onedrive/sdk/concurrency/IExecutors;->performOnBackground(Ljava/lang/Runnable;)V

    return-void
.end method

.method public select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDriveCollectionRequest;
    .locals 2

    .line 1
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    .line 2
    .line 3
    const-string v1, "select"

    .line 4
    .line 5
    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/http/BaseCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 9
    .line 10
    .line 11
    move-object p1, p0

    .line 12
    check-cast p1, Lcom/onedrive/sdk/extensions/DriveCollectionRequest;

    .line 13
    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public top(I)Lcom/onedrive/sdk/extensions/IDriveCollectionRequest;
    .locals 2

    .line 1
    new-instance v0, Lcom/onedrive/sdk/options/QueryOption;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    const-string p1, ""

    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const-string/jumbo v1, "top"

    .line 21
    .line 22
    .line 23
    invoke-direct {v0, v1, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/http/BaseCollectionRequest;->addQueryOption(Lcom/onedrive/sdk/options/QueryOption;)V

    .line 27
    .line 28
    .line 29
    move-object p1, p0

    .line 30
    check-cast p1, Lcom/onedrive/sdk/extensions/DriveCollectionRequest;

    .line 31
    .line 32
    return-object p1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

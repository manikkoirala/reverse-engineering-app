.class public Lcom/onedrive/sdk/generated/BaseDriveRequestBuilder;
.super Lcom/onedrive/sdk/http/BaseRequestBuilder;
.source "BaseDriveRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBaseDriveRequestBuilder;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/onedrive/sdk/http/BaseRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public buildRequest()Lcom/onedrive/sdk/extensions/IDriveRequest;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getOptions()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/onedrive/sdk/generated/BaseDriveRequestBuilder;->buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IDriveRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IDriveRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IDriveRequest;"
        }
    .end annotation

    .line 2
    new-instance v0, Lcom/onedrive/sdk/extensions/DriveRequest;

    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/onedrive/sdk/extensions/DriveRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    return-object v0
.end method

.method public getItems(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;
    .locals 3

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "items"

    .line 9
    .line 10
    invoke-virtual {p0, v2}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v2, "/"

    .line 18
    .line 19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    const/4 v2, 0x0

    .line 34
    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    .line 35
    .line 36
    .line 37
    return-object v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public getRecent()Lcom/onedrive/sdk/extensions/IRecentRequestBuilder;
    .locals 4

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/RecentRequestBuilder;

    .line 2
    .line 3
    const-string/jumbo v1, "view.recent"

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v1}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    const/4 v3, 0x0

    .line 15
    invoke-direct {v0, v1, v2, v3}, Lcom/onedrive/sdk/extensions/RecentRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    .line 16
    .line 17
    .line 18
    return-object v0
    .line 19
.end method

.method public getShared(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;
    .locals 3

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string/jumbo v2, "shared"

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v2}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v2, "/"

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    const/4 v2, 0x0

    .line 35
    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    .line 36
    .line 37
    .line 38
    return-object v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public getSpecial(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IItemRequestBuilder;
    .locals 3

    .line 1
    new-instance v0, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string/jumbo v2, "special"

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v2}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getRequestUrlWithAdditionalSegment(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v2, "/"

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequestBuilder;->getClient()Lcom/onedrive/sdk/extensions/IOneDriveClient;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    const/4 v2, 0x0

    .line 35
    invoke-direct {v0, p1, v1, v2}, Lcom/onedrive/sdk/extensions/ItemRequestBuilder;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V

    .line 36
    .line 37
    .line 38
    return-object v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

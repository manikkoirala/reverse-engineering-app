.class public interface abstract Lcom/onedrive/sdk/generated/IBaseDriveCollectionRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseDriveCollectionRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/IDriveCollectionRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IDriveCollectionRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IDriveCollectionRequest;"
        }
    .end annotation
.end method

.method public abstract byId(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IDriveRequestBuilder;
.end method

.class public interface abstract Lcom/onedrive/sdk/generated/IBaseThumbnailRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseThumbnailRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/IThumbnailRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IThumbnailRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IThumbnailRequest;"
        }
    .end annotation
.end method

.method public abstract getContent()Lcom/onedrive/sdk/extensions/IThumbnailStreamRequestBuilder;
.end method

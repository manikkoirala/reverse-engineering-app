.class public Lcom/onedrive/sdk/generated/BasePermissionRequest;
.super Lcom/onedrive/sdk/http/BaseRequest;
.source "BasePermissionRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/generated/IBasePermissionRequest;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/onedrive/sdk/extensions/IOneDriveClient;",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-class v0, Lcom/onedrive/sdk/extensions/Permission;

    .line 2
    .line 3
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/onedrive/sdk/http/BaseRequest;-><init>(Ljava/lang/String;Lcom/onedrive/sdk/extensions/IOneDriveClient;Ljava/util/List;Ljava/lang/Class;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public create(Lcom/onedrive/sdk/extensions/Permission;)Lcom/onedrive/sdk/extensions/Permission;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BasePermissionRequest;->post(Lcom/onedrive/sdk/extensions/Permission;)Lcom/onedrive/sdk/extensions/Permission;

    move-result-object p1

    return-object p1
.end method

.method public create(Lcom/onedrive/sdk/extensions/Permission;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Permission;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Permission;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/onedrive/sdk/generated/BasePermissionRequest;->post(Lcom/onedrive/sdk/extensions/Permission;Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

.method public delete()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 2
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->DELETE:Lcom/onedrive/sdk/http/HttpMethod;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/onedrive/sdk/http/BaseRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public delete(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->DELETE:Lcom/onedrive/sdk/http/HttpMethod;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/onedrive/sdk/http/BaseRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V

    return-void
.end method

.method public expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IPermissionRequest;
    .locals 3

    .line 2
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    const-string v2, "expand"

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/PermissionRequest;

    return-object p1
.end method

.method public bridge synthetic expand(Ljava/lang/String;)Lcom/onedrive/sdk/generated/IBasePermissionRequest;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BasePermissionRequest;->expand(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IPermissionRequest;

    move-result-object p1

    return-object p1
.end method

.method public get()Lcom/onedrive/sdk/extensions/Permission;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 2
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/onedrive/sdk/http/BaseRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/onedrive/sdk/extensions/Permission;

    return-object v0
.end method

.method public get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Permission;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->GET:Lcom/onedrive/sdk/http/HttpMethod;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/onedrive/sdk/http/BaseRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V

    return-void
.end method

.method public patch(Lcom/onedrive/sdk/extensions/Permission;)Lcom/onedrive/sdk/extensions/Permission;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 2
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->PATCH:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {p0, v0, p1}, Lcom/onedrive/sdk/http/BaseRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/extensions/Permission;

    return-object p1
.end method

.method public patch(Lcom/onedrive/sdk/extensions/Permission;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Permission;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Permission;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->PATCH:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {p0, v0, p2, p1}, Lcom/onedrive/sdk/http/BaseRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V

    return-void
.end method

.method public post(Lcom/onedrive/sdk/extensions/Permission;)Lcom/onedrive/sdk/extensions/Permission;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .line 2
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->POST:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {p0, v0, p1}, Lcom/onedrive/sdk/http/BaseRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/onedrive/sdk/extensions/Permission;

    return-object p1
.end method

.method public post(Lcom/onedrive/sdk/extensions/Permission;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Permission;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Permission;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/onedrive/sdk/http/HttpMethod;->POST:Lcom/onedrive/sdk/http/HttpMethod;

    invoke-virtual {p0, v0, p2, p1}, Lcom/onedrive/sdk/http/BaseRequest;->send(Lcom/onedrive/sdk/http/HttpMethod;Lcom/onedrive/sdk/concurrency/ICallback;Ljava/lang/Object;)V

    return-void
.end method

.method public select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IPermissionRequest;
    .locals 3

    .line 2
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    const-string v2, "select"

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/PermissionRequest;

    return-object p1
.end method

.method public bridge synthetic select(Ljava/lang/String;)Lcom/onedrive/sdk/generated/IBasePermissionRequest;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BasePermissionRequest;->select(Ljava/lang/String;)Lcom/onedrive/sdk/extensions/IPermissionRequest;

    move-result-object p1

    return-object p1
.end method

.method public top(I)Lcom/onedrive/sdk/extensions/IPermissionRequest;
    .locals 3

    .line 2
    invoke-virtual {p0}, Lcom/onedrive/sdk/http/BaseRequest;->getQueryOptions()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/onedrive/sdk/options/QueryOption;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ""

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v2, "top"

    invoke-direct {v1, v2, p1}, Lcom/onedrive/sdk/options/QueryOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3
    move-object p1, p0

    check-cast p1, Lcom/onedrive/sdk/extensions/PermissionRequest;

    return-object p1
.end method

.method public bridge synthetic top(I)Lcom/onedrive/sdk/generated/IBasePermissionRequest;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BasePermissionRequest;->top(I)Lcom/onedrive/sdk/extensions/IPermissionRequest;

    move-result-object p1

    return-object p1
.end method

.method public update(Lcom/onedrive/sdk/extensions/Permission;)Lcom/onedrive/sdk/extensions/Permission;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    invoke-virtual {p0, p1}, Lcom/onedrive/sdk/generated/BasePermissionRequest;->patch(Lcom/onedrive/sdk/extensions/Permission;)Lcom/onedrive/sdk/extensions/Permission;

    move-result-object p1

    return-object p1
.end method

.method public update(Lcom/onedrive/sdk/extensions/Permission;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Permission;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Permission;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/onedrive/sdk/generated/BasePermissionRequest;->patch(Lcom/onedrive/sdk/extensions/Permission;Lcom/onedrive/sdk/concurrency/ICallback;)V

    return-void
.end method

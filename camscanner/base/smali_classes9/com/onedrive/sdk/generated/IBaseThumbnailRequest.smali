.class public interface abstract Lcom/onedrive/sdk/generated/IBaseThumbnailRequest;
.super Ljava/lang/Object;
.source "IBaseThumbnailRequest.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IHttpRequest;


# virtual methods
.method public abstract create(Lcom/onedrive/sdk/extensions/Thumbnail;)Lcom/onedrive/sdk/extensions/Thumbnail;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract create(Lcom/onedrive/sdk/extensions/Thumbnail;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Thumbnail;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Thumbnail;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract delete()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract delete(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract expand(Ljava/lang/String;)Lcom/onedrive/sdk/generated/IBaseThumbnailRequest;
.end method

.method public abstract get()Lcom/onedrive/sdk/extensions/Thumbnail;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract get(Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Thumbnail;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract patch(Lcom/onedrive/sdk/extensions/Thumbnail;)Lcom/onedrive/sdk/extensions/Thumbnail;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract patch(Lcom/onedrive/sdk/extensions/Thumbnail;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Thumbnail;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Thumbnail;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract post(Lcom/onedrive/sdk/extensions/Thumbnail;)Lcom/onedrive/sdk/extensions/Thumbnail;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation
.end method

.method public abstract post(Lcom/onedrive/sdk/extensions/Thumbnail;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Thumbnail;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Thumbnail;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract select(Ljava/lang/String;)Lcom/onedrive/sdk/generated/IBaseThumbnailRequest;
.end method

.method public abstract top(I)Lcom/onedrive/sdk/generated/IBaseThumbnailRequest;
.end method

.method public abstract update(Lcom/onedrive/sdk/extensions/Thumbnail;)Lcom/onedrive/sdk/extensions/Thumbnail;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/onedrive/sdk/core/ClientException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract update(Lcom/onedrive/sdk/extensions/Thumbnail;Lcom/onedrive/sdk/concurrency/ICallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/onedrive/sdk/extensions/Thumbnail;",
            "Lcom/onedrive/sdk/concurrency/ICallback<",
            "Lcom/onedrive/sdk/extensions/Thumbnail;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.class public interface abstract Lcom/onedrive/sdk/generated/IBaseThumbnailStreamRequestBuilder;
.super Ljava/lang/Object;
.source "IBaseThumbnailStreamRequestBuilder.java"

# interfaces
.implements Lcom/onedrive/sdk/http/IRequestBuilder;


# virtual methods
.method public abstract buildRequest()Lcom/onedrive/sdk/extensions/IThumbnailStreamRequest;
.end method

.method public abstract buildRequest(Ljava/util/List;)Lcom/onedrive/sdk/extensions/IThumbnailStreamRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/onedrive/sdk/options/Option;",
            ">;)",
            "Lcom/onedrive/sdk/extensions/IThumbnailStreamRequest;"
        }
    .end annotation
.end method

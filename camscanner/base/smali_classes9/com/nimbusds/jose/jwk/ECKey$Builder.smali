.class public Lcom/nimbusds/jose/jwk/ECKey$Builder;
.super Ljava/lang/Object;
.source "ECKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nimbusds/jose/jwk/ECKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private O8:Lcom/nimbusds/jose/util/Base64URL;

.field private OO0o〇〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;"
        }
    .end annotation
.end field

.field private OO0o〇〇〇〇0:Ljava/net/URI;

.field private Oo08:Ljava/security/PrivateKey;

.field private Oooo8o0〇:Ljava/security/KeyStore;

.field private oO80:Lcom/nimbusds/jose/Algorithm;

.field private o〇0:Lcom/nimbusds/jose/jwk/KeyUse;

.field private final 〇080:Lcom/nimbusds/jose/jwk/Curve;

.field private 〇80〇808〇O:Ljava/lang/String;

.field private 〇8o8o〇:Lcom/nimbusds/jose/util/Base64URL;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private 〇O8o08O:Lcom/nimbusds/jose/util/Base64URL;

.field private final 〇o00〇〇Oo:Lcom/nimbusds/jose/util/Base64URL;

.field private final 〇o〇:Lcom/nimbusds/jose/util/Base64URL;

.field private 〇〇888:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/nimbusds/jose/jwk/Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_2

    .line 2
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇080:Lcom/nimbusds/jose/jwk/Curve;

    if-eqz p2, :cond_1

    .line 3
    iput-object p2, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇o00〇〇Oo:Lcom/nimbusds/jose/util/Base64URL;

    if-eqz p3, :cond_0

    .line 4
    iput-object p3, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇o〇:Lcom/nimbusds/jose/util/Base64URL;

    return-void

    .line 5
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The \'y\' coordinate must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 6
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The \'x\' coordinate must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 7
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The curve must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lcom/nimbusds/jose/jwk/Curve;Ljava/security/interfaces/ECPublicKey;)V
    .locals 2

    .line 8
    invoke-interface {p2}, Ljava/security/interfaces/ECKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/spec/ECField;->getFieldSize()I

    move-result v0

    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v1

    invoke-virtual {v1}, Ljava/security/spec/ECPoint;->getAffineX()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/nimbusds/jose/jwk/ECKey;->encodeCoordinate(ILjava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v0

    .line 9
    invoke-interface {p2}, Ljava/security/interfaces/ECKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v1

    invoke-virtual {v1}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v1

    invoke-virtual {v1}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object v1

    invoke-interface {v1}, Ljava/security/spec/ECField;->getFieldSize()I

    move-result v1

    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object p2

    invoke-virtual {p2}, Ljava/security/spec/ECPoint;->getAffineY()Ljava/math/BigInteger;

    move-result-object p2

    invoke-static {v1, p2}, Lcom/nimbusds/jose/jwk/ECKey;->encodeCoordinate(ILjava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object p2

    .line 10
    invoke-direct {p0, p1, v0, p2}, Lcom/nimbusds/jose/jwk/ECKey$Builder;-><init>(Lcom/nimbusds/jose/jwk/Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;)V

    return-void
.end method

.method public constructor <init>(Lcom/nimbusds/jose/jwk/ECKey;)V
    .locals 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    invoke-static {p1}, Lcom/nimbusds/jose/jwk/ECKey;->〇080(Lcom/nimbusds/jose/jwk/ECKey;)Lcom/nimbusds/jose/jwk/Curve;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇080:Lcom/nimbusds/jose/jwk/Curve;

    .line 13
    invoke-static {p1}, Lcom/nimbusds/jose/jwk/ECKey;->〇o00〇〇Oo(Lcom/nimbusds/jose/jwk/ECKey;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇o00〇〇Oo:Lcom/nimbusds/jose/util/Base64URL;

    .line 14
    invoke-static {p1}, Lcom/nimbusds/jose/jwk/ECKey;->〇o〇(Lcom/nimbusds/jose/jwk/ECKey;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇o〇:Lcom/nimbusds/jose/util/Base64URL;

    .line 15
    invoke-static {p1}, Lcom/nimbusds/jose/jwk/ECKey;->O8(Lcom/nimbusds/jose/jwk/ECKey;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->O8:Lcom/nimbusds/jose/util/Base64URL;

    .line 16
    invoke-static {p1}, Lcom/nimbusds/jose/jwk/ECKey;->Oo08(Lcom/nimbusds/jose/jwk/ECKey;)Ljava/security/PrivateKey;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->Oo08:Ljava/security/PrivateKey;

    .line 17
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/JWK;->getKeyUse()Lcom/nimbusds/jose/jwk/KeyUse;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->o〇0:Lcom/nimbusds/jose/jwk/KeyUse;

    .line 18
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/JWK;->getKeyOperations()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇〇888:Ljava/util/Set;

    .line 19
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/JWK;->getAlgorithm()Lcom/nimbusds/jose/Algorithm;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->oO80:Lcom/nimbusds/jose/Algorithm;

    .line 20
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/JWK;->getKeyID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇80〇808〇O:Ljava/lang/String;

    .line 21
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/JWK;->getX509CertURL()Ljava/net/URI;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->OO0o〇〇〇〇0:Ljava/net/URI;

    .line 22
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/JWK;->getX509CertThumbprint()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇8o8o〇:Lcom/nimbusds/jose/util/Base64URL;

    .line 23
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/JWK;->getX509CertSHA256Thumbprint()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇O8o08O:Lcom/nimbusds/jose/util/Base64URL;

    .line 24
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/JWK;->getX509CertChain()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->OO0o〇〇:Ljava/util/List;

    .line 25
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/JWK;->getKeyStore()Ljava/security/KeyStore;

    move-result-object p1

    iput-object p1, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->Oooo8o0〇:Ljava/security/KeyStore;

    return-void
.end method


# virtual methods
.method public O8(Lcom/nimbusds/jose/jwk/KeyUse;)Lcom/nimbusds/jose/jwk/ECKey$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->o〇0:Lcom/nimbusds/jose/jwk/KeyUse;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public Oo08(Ljava/security/PrivateKey;)Lcom/nimbusds/jose/jwk/ECKey$Builder;
    .locals 2

    .line 1
    instance-of v0, p1, Ljava/security/interfaces/ECPrivateKey;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Ljava/security/interfaces/ECPrivateKey;

    .line 6
    .line 7
    invoke-virtual {p0, p1}, Lcom/nimbusds/jose/jwk/ECKey$Builder;->o〇0(Ljava/security/interfaces/ECPrivateKey;)Lcom/nimbusds/jose/jwk/ECKey$Builder;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1

    .line 12
    :cond_0
    invoke-interface {p1}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const-string v1, "EC"

    .line 17
    .line 18
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->Oo08:Ljava/security/PrivateKey;

    .line 25
    .line 26
    return-object p0

    .line 27
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 28
    .line 29
    const-string v0, "The private key algorithm must be EC"

    .line 30
    .line 31
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    throw p1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public oO80(Lcom/nimbusds/jose/util/Base64URL;)Lcom/nimbusds/jose/jwk/ECKey$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇O8o08O:Lcom/nimbusds/jose/util/Base64URL;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public o〇0(Ljava/security/interfaces/ECPrivateKey;)Lcom/nimbusds/jose/jwk/ECKey$Builder;
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-interface {p1}, Ljava/security/interfaces/ECKey;->getParams()Ljava/security/spec/ECParameterSpec;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Ljava/security/spec/ECField;->getFieldSize()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-interface {p1}, Ljava/security/interfaces/ECPrivateKey;->getS()Ljava/math/BigInteger;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-static {v0, p1}, Lcom/nimbusds/jose/jwk/ECKey;->encodeCoordinate(ILjava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->O8:Lcom/nimbusds/jose/util/Base64URL;

    .line 28
    .line 29
    :cond_0
    return-object p0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public 〇080()Lcom/nimbusds/jose/jwk/ECKey;
    .locals 15

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->O8:Lcom/nimbusds/jose/util/Base64URL;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->Oo08:Ljava/security/PrivateKey;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/nimbusds/jose/jwk/ECKey;

    .line 10
    .line 11
    iget-object v2, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇080:Lcom/nimbusds/jose/jwk/Curve;

    .line 12
    .line 13
    iget-object v3, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇o00〇〇Oo:Lcom/nimbusds/jose/util/Base64URL;

    .line 14
    .line 15
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇o〇:Lcom/nimbusds/jose/util/Base64URL;

    .line 16
    .line 17
    iget-object v5, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->o〇0:Lcom/nimbusds/jose/jwk/KeyUse;

    .line 18
    .line 19
    iget-object v6, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇〇888:Ljava/util/Set;

    .line 20
    .line 21
    iget-object v7, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->oO80:Lcom/nimbusds/jose/Algorithm;

    .line 22
    .line 23
    iget-object v8, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇80〇808〇O:Ljava/lang/String;

    .line 24
    .line 25
    iget-object v9, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->OO0o〇〇〇〇0:Ljava/net/URI;

    .line 26
    .line 27
    iget-object v10, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇8o8o〇:Lcom/nimbusds/jose/util/Base64URL;

    .line 28
    .line 29
    iget-object v11, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇O8o08O:Lcom/nimbusds/jose/util/Base64URL;

    .line 30
    .line 31
    iget-object v12, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->OO0o〇〇:Ljava/util/List;

    .line 32
    .line 33
    iget-object v13, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->Oooo8o0〇:Ljava/security/KeyStore;

    .line 34
    .line 35
    move-object v1, v0

    .line 36
    invoke-direct/range {v1 .. v13}, Lcom/nimbusds/jose/jwk/ECKey;-><init>(Lcom/nimbusds/jose/jwk/Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Ljava/security/KeyStore;)V

    .line 37
    .line 38
    .line 39
    return-object v0

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->Oo08:Ljava/security/PrivateKey;

    .line 41
    .line 42
    if-eqz v0, :cond_1

    .line 43
    .line 44
    new-instance v0, Lcom/nimbusds/jose/jwk/ECKey;

    .line 45
    .line 46
    iget-object v2, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇080:Lcom/nimbusds/jose/jwk/Curve;

    .line 47
    .line 48
    iget-object v3, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇o00〇〇Oo:Lcom/nimbusds/jose/util/Base64URL;

    .line 49
    .line 50
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇o〇:Lcom/nimbusds/jose/util/Base64URL;

    .line 51
    .line 52
    iget-object v5, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->Oo08:Ljava/security/PrivateKey;

    .line 53
    .line 54
    iget-object v6, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->o〇0:Lcom/nimbusds/jose/jwk/KeyUse;

    .line 55
    .line 56
    iget-object v7, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇〇888:Ljava/util/Set;

    .line 57
    .line 58
    iget-object v8, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->oO80:Lcom/nimbusds/jose/Algorithm;

    .line 59
    .line 60
    iget-object v9, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇80〇808〇O:Ljava/lang/String;

    .line 61
    .line 62
    iget-object v10, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->OO0o〇〇〇〇0:Ljava/net/URI;

    .line 63
    .line 64
    iget-object v11, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇8o8o〇:Lcom/nimbusds/jose/util/Base64URL;

    .line 65
    .line 66
    iget-object v12, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇O8o08O:Lcom/nimbusds/jose/util/Base64URL;

    .line 67
    .line 68
    iget-object v13, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->OO0o〇〇:Ljava/util/List;

    .line 69
    .line 70
    iget-object v14, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->Oooo8o0〇:Ljava/security/KeyStore;

    .line 71
    .line 72
    move-object v1, v0

    .line 73
    invoke-direct/range {v1 .. v14}, Lcom/nimbusds/jose/jwk/ECKey;-><init>(Lcom/nimbusds/jose/jwk/Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/security/PrivateKey;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Ljava/security/KeyStore;)V

    .line 74
    .line 75
    .line 76
    return-object v0

    .line 77
    :cond_1
    new-instance v0, Lcom/nimbusds/jose/jwk/ECKey;

    .line 78
    .line 79
    iget-object v2, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇080:Lcom/nimbusds/jose/jwk/Curve;

    .line 80
    .line 81
    iget-object v3, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇o00〇〇Oo:Lcom/nimbusds/jose/util/Base64URL;

    .line 82
    .line 83
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇o〇:Lcom/nimbusds/jose/util/Base64URL;

    .line 84
    .line 85
    iget-object v5, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->O8:Lcom/nimbusds/jose/util/Base64URL;

    .line 86
    .line 87
    iget-object v6, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->o〇0:Lcom/nimbusds/jose/jwk/KeyUse;

    .line 88
    .line 89
    iget-object v7, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇〇888:Ljava/util/Set;

    .line 90
    .line 91
    iget-object v8, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->oO80:Lcom/nimbusds/jose/Algorithm;

    .line 92
    .line 93
    iget-object v9, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇80〇808〇O:Ljava/lang/String;

    .line 94
    .line 95
    iget-object v10, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->OO0o〇〇〇〇0:Ljava/net/URI;

    .line 96
    .line 97
    iget-object v11, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇8o8o〇:Lcom/nimbusds/jose/util/Base64URL;

    .line 98
    .line 99
    iget-object v12, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇O8o08O:Lcom/nimbusds/jose/util/Base64URL;

    .line 100
    .line 101
    iget-object v13, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->OO0o〇〇:Ljava/util/List;

    .line 102
    .line 103
    iget-object v14, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->Oooo8o0〇:Ljava/security/KeyStore;

    .line 104
    .line 105
    move-object v1, v0

    .line 106
    invoke-direct/range {v1 .. v14}, Lcom/nimbusds/jose/jwk/ECKey;-><init>(Lcom/nimbusds/jose/jwk/Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Ljava/security/KeyStore;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    .line 108
    .line 109
    return-object v0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 112
    .line 113
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v2

    .line 117
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 118
    .line 119
    .line 120
    throw v1
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method public 〇o00〇〇Oo(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/ECKey$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->〇80〇808〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇o〇(Ljava/security/KeyStore;)Lcom/nimbusds/jose/jwk/ECKey$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->Oooo8o0〇:Ljava/security/KeyStore;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇〇888(Ljava/util/List;)Lcom/nimbusds/jose/jwk/ECKey$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)",
            "Lcom/nimbusds/jose/jwk/ECKey$Builder;"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/ECKey$Builder;->OO0o〇〇:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

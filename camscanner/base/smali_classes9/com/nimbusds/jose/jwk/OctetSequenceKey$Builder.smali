.class public Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;
.super Ljava/lang/Object;
.source "OctetSequenceKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nimbusds/jose/jwk/OctetSequenceKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private O8:Lcom/nimbusds/jose/Algorithm;

.field private OO0o〇〇〇〇0:Ljava/security/KeyStore;

.field private Oo08:Ljava/lang/String;

.field private oO80:Lcom/nimbusds/jose/util/Base64URL;

.field private o〇0:Ljava/net/URI;

.field private final 〇080:Lcom/nimbusds/jose/util/Base64URL;

.field private 〇80〇808〇O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o00〇〇Oo:Lcom/nimbusds/jose/jwk/KeyUse;

.field private 〇o〇:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;"
        }
    .end annotation
.end field

.field private 〇〇888:Lcom/nimbusds/jose/util/Base64URL;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/nimbusds/jose/util/Base64URL;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 2
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->〇080:Lcom/nimbusds/jose/util/Base64URL;

    return-void

    .line 3
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The key value must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Ljavax/crypto/SecretKey;)V
    .locals 0

    .line 7
    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;-><init>([B)V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .line 4
    invoke-static {p1}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;-><init>(Lcom/nimbusds/jose/util/Base64URL;)V

    .line 5
    array-length p1, p1

    if-eqz p1, :cond_0

    return-void

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The key must have a positive length"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public 〇080()Lcom/nimbusds/jose/jwk/OctetSequenceKey;
    .locals 12

    .line 1
    :try_start_0
    new-instance v11, Lcom/nimbusds/jose/jwk/OctetSequenceKey;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->〇080:Lcom/nimbusds/jose/util/Base64URL;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->〇o00〇〇Oo:Lcom/nimbusds/jose/jwk/KeyUse;

    .line 6
    .line 7
    iget-object v3, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->〇o〇:Ljava/util/Set;

    .line 8
    .line 9
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->O8:Lcom/nimbusds/jose/Algorithm;

    .line 10
    .line 11
    iget-object v5, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->Oo08:Ljava/lang/String;

    .line 12
    .line 13
    iget-object v6, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->o〇0:Ljava/net/URI;

    .line 14
    .line 15
    iget-object v7, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->〇〇888:Lcom/nimbusds/jose/util/Base64URL;

    .line 16
    .line 17
    iget-object v8, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->oO80:Lcom/nimbusds/jose/util/Base64URL;

    .line 18
    .line 19
    iget-object v9, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->〇80〇808〇O:Ljava/util/List;

    .line 20
    .line 21
    iget-object v10, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->OO0o〇〇〇〇0:Ljava/security/KeyStore;

    .line 22
    .line 23
    move-object v0, v11

    .line 24
    invoke-direct/range {v0 .. v10}, Lcom/nimbusds/jose/jwk/OctetSequenceKey;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Ljava/security/KeyStore;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    .line 26
    .line 27
    return-object v11

    .line 28
    :catch_0
    move-exception v0

    .line 29
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 30
    .line 31
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 36
    .line 37
    .line 38
    throw v1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public 〇o00〇〇Oo(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇o〇(Ljava/security/KeyStore;)Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/OctetSequenceKey$Builder;->OO0o〇〇〇〇0:Ljava/security/KeyStore;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

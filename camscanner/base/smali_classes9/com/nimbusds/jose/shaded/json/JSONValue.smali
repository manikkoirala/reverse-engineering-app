.class public Lcom/nimbusds/jose/shaded/json/JSONValue;
.super Ljava/lang/Object;
.source "JSONValue.java"


# static fields
.field public static 〇080:Lcom/nimbusds/jose/shaded/json/JSONStyle;

.field private static final 〇o00〇〇Oo:Lcom/nimbusds/jose/shaded/json/parser/FakeContainerFactory;

.field public static 〇o〇:Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/nimbusds/jose/shaded/json/JSONStyle;->oO80:Lcom/nimbusds/jose/shaded/json/JSONStyle;

    .line 2
    .line 3
    sput-object v0, Lcom/nimbusds/jose/shaded/json/JSONValue;->〇080:Lcom/nimbusds/jose/shaded/json/JSONStyle;

    .line 4
    .line 5
    new-instance v0, Lcom/nimbusds/jose/shaded/json/parser/FakeContainerFactory;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/nimbusds/jose/shaded/json/parser/FakeContainerFactory;-><init>()V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/nimbusds/jose/shaded/json/JSONValue;->〇o00〇〇Oo:Lcom/nimbusds/jose/shaded/json/parser/FakeContainerFactory;

    .line 11
    .line 12
    new-instance v0, Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;

    .line 13
    .line 14
    invoke-direct {v0}, Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;-><init>()V

    .line 15
    .line 16
    .line 17
    sput-object v0, Lcom/nimbusds/jose/shaded/json/JSONValue;->〇o〇:Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;

    .line 18
    .line 19
    return-void
.end method

.method public static O8(Ljava/lang/Object;Ljava/lang/Appendable;Lcom/nimbusds/jose/shaded/json/JSONStyle;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    if-nez p0, :cond_0

    .line 2
    .line 3
    const-string p0, "null"

    .line 4
    .line 5
    invoke-interface {p1, p0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    sget-object v1, Lcom/nimbusds/jose/shaded/json/JSONValue;->〇o〇:Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;

    .line 14
    .line 15
    invoke-virtual {v1, v0}, Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;->〇080(Ljava/lang/Class;)Lcom/nimbusds/jose/shaded/json/reader/JsonWriterI;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    if-nez v1, :cond_3

    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_1

    .line 26
    .line 27
    sget-object v1, Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;->〇8o8o〇:Lcom/nimbusds/jose/shaded/json/reader/JsonWriterI;

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    sget-object v1, Lcom/nimbusds/jose/shaded/json/JSONValue;->〇o〇:Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;

    .line 31
    .line 32
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-virtual {v1, v2}, Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;->〇o00〇〇Oo(Ljava/lang/Class;)Lcom/nimbusds/jose/shaded/json/reader/JsonWriterI;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    if-nez v1, :cond_2

    .line 41
    .line 42
    sget-object v1, Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;->OO0o〇〇〇〇0:Lcom/nimbusds/jose/shaded/json/reader/JsonWriterI;

    .line 43
    .line 44
    :cond_2
    :goto_0
    sget-object v2, Lcom/nimbusds/jose/shaded/json/JSONValue;->〇o〇:Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;

    .line 45
    .line 46
    const/4 v3, 0x1

    .line 47
    new-array v3, v3, [Ljava/lang/Class;

    .line 48
    .line 49
    const/4 v4, 0x0

    .line 50
    aput-object v0, v3, v4

    .line 51
    .line 52
    invoke-virtual {v2, v1, v3}, Lcom/nimbusds/jose/shaded/json/reader/JsonWriter;->O8(Lcom/nimbusds/jose/shaded/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 53
    .line 54
    .line 55
    :cond_3
    invoke-interface {v1, p0, p1, p2}, Lcom/nimbusds/jose/shaded/json/reader/JsonWriterI;->〇080(Ljava/lang/Object;Ljava/lang/Appendable;Lcom/nimbusds/jose/shaded/json/JSONStyle;)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public static 〇080(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/nimbusds/jose/shaded/json/JSONValue;->〇080:Lcom/nimbusds/jose/shaded/json/JSONStyle;

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/nimbusds/jose/shaded/json/JSONValue;->〇o00〇〇Oo(Ljava/lang/String;Lcom/nimbusds/jose/shaded/json/JSONStyle;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    return-object p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static 〇o00〇〇Oo(Ljava/lang/String;Lcom/nimbusds/jose/shaded/json/JSONStyle;)Ljava/lang/String;
    .locals 1

    .line 1
    if-nez p0, :cond_0

    .line 2
    .line 3
    const/4 p0, 0x0

    .line 4
    return-object p0

    .line 5
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1, p0, v0}, Lcom/nimbusds/jose/shaded/json/JSONStyle;->o〇0(Ljava/lang/String;Ljava/lang/Appendable;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    return-object p0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static 〇o〇(Ljava/lang/String;Ljava/lang/Appendable;Lcom/nimbusds/jose/shaded/json/JSONStyle;)V
    .locals 0

    .line 1
    if-nez p0, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p2, p0, p1}, Lcom/nimbusds/jose/shaded/json/JSONStyle;->o〇0(Ljava/lang/String;Ljava/lang/Appendable;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

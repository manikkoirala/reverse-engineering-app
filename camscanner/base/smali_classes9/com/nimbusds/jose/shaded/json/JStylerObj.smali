.class Lcom/nimbusds/jose/shaded/json/JStylerObj;
.super Ljava/lang/Object;
.source "JStylerObj.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nimbusds/jose/shaded/json/JStylerObj$Escape4Web;,
        Lcom/nimbusds/jose/shaded/json/JStylerObj$EscapeLT;,
        Lcom/nimbusds/jose/shaded/json/JStylerObj$StringProtector;,
        Lcom/nimbusds/jose/shaded/json/JStylerObj$MPAgressive;,
        Lcom/nimbusds/jose/shaded/json/JStylerObj$MPSimple;,
        Lcom/nimbusds/jose/shaded/json/JStylerObj$MPTrue;,
        Lcom/nimbusds/jose/shaded/json/JStylerObj$MustProtect;
    }
.end annotation


# static fields
.field public static final O8:Lcom/nimbusds/jose/shaded/json/JStylerObj$EscapeLT;

.field public static final Oo08:Lcom/nimbusds/jose/shaded/json/JStylerObj$Escape4Web;

.field public static final 〇080:Lcom/nimbusds/jose/shaded/json/JStylerObj$MPSimple;

.field public static final 〇o00〇〇Oo:Lcom/nimbusds/jose/shaded/json/JStylerObj$MPTrue;

.field public static final 〇o〇:Lcom/nimbusds/jose/shaded/json/JStylerObj$MPAgressive;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/nimbusds/jose/shaded/json/JStylerObj$MPSimple;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/nimbusds/jose/shaded/json/JStylerObj$MPSimple;-><init>(Lcom/nimbusds/jose/shaded/json/JStylerObj$1;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/nimbusds/jose/shaded/json/JStylerObj;->〇080:Lcom/nimbusds/jose/shaded/json/JStylerObj$MPSimple;

    .line 8
    .line 9
    new-instance v0, Lcom/nimbusds/jose/shaded/json/JStylerObj$MPTrue;

    .line 10
    .line 11
    invoke-direct {v0, v1}, Lcom/nimbusds/jose/shaded/json/JStylerObj$MPTrue;-><init>(Lcom/nimbusds/jose/shaded/json/JStylerObj$1;)V

    .line 12
    .line 13
    .line 14
    sput-object v0, Lcom/nimbusds/jose/shaded/json/JStylerObj;->〇o00〇〇Oo:Lcom/nimbusds/jose/shaded/json/JStylerObj$MPTrue;

    .line 15
    .line 16
    new-instance v0, Lcom/nimbusds/jose/shaded/json/JStylerObj$MPAgressive;

    .line 17
    .line 18
    invoke-direct {v0, v1}, Lcom/nimbusds/jose/shaded/json/JStylerObj$MPAgressive;-><init>(Lcom/nimbusds/jose/shaded/json/JStylerObj$1;)V

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/nimbusds/jose/shaded/json/JStylerObj;->〇o〇:Lcom/nimbusds/jose/shaded/json/JStylerObj$MPAgressive;

    .line 22
    .line 23
    new-instance v0, Lcom/nimbusds/jose/shaded/json/JStylerObj$EscapeLT;

    .line 24
    .line 25
    invoke-direct {v0, v1}, Lcom/nimbusds/jose/shaded/json/JStylerObj$EscapeLT;-><init>(Lcom/nimbusds/jose/shaded/json/JStylerObj$1;)V

    .line 26
    .line 27
    .line 28
    sput-object v0, Lcom/nimbusds/jose/shaded/json/JStylerObj;->O8:Lcom/nimbusds/jose/shaded/json/JStylerObj$EscapeLT;

    .line 29
    .line 30
    new-instance v0, Lcom/nimbusds/jose/shaded/json/JStylerObj$Escape4Web;

    .line 31
    .line 32
    invoke-direct {v0, v1}, Lcom/nimbusds/jose/shaded/json/JStylerObj$Escape4Web;-><init>(Lcom/nimbusds/jose/shaded/json/JStylerObj$1;)V

    .line 33
    .line 34
    .line 35
    sput-object v0, Lcom/nimbusds/jose/shaded/json/JStylerObj;->Oo08:Lcom/nimbusds/jose/shaded/json/JStylerObj$Escape4Web;

    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public static O8(C)Z
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    const/16 v0, 0xc

    .line 6
    .line 7
    if-eq p0, v0, :cond_1

    .line 8
    .line 9
    const/16 v0, 0xa

    .line 10
    .line 11
    if-ne p0, v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 p0, 0x0

    .line 15
    goto :goto_1

    .line 16
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 17
    :goto_1
    return p0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static Oo08(C)Z
    .locals 1

    .line 1
    const/16 v0, 0x7d

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    const/16 v0, 0x5d

    .line 6
    .line 7
    if-eq p0, v0, :cond_1

    .line 8
    .line 9
    const/16 v0, 0x2c

    .line 10
    .line 11
    if-eq p0, v0, :cond_1

    .line 12
    .line 13
    const/16 v0, 0x3a

    .line 14
    .line 15
    if-ne p0, v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 p0, 0x0

    .line 19
    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 21
    :goto_1
    return p0
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static o〇0(C)Z
    .locals 1

    .line 1
    if-ltz p0, :cond_0

    .line 2
    .line 3
    const/16 v0, 0x1f

    .line 4
    .line 5
    if-le p0, v0, :cond_2

    .line 6
    .line 7
    :cond_0
    const/16 v0, 0x7f

    .line 8
    .line 9
    if-lt p0, v0, :cond_1

    .line 10
    .line 11
    const/16 v0, 0x9f

    .line 12
    .line 13
    if-le p0, v0, :cond_2

    .line 14
    .line 15
    :cond_1
    const/16 v0, 0x2000

    .line 16
    .line 17
    if-lt p0, v0, :cond_3

    .line 18
    .line 19
    const/16 v0, 0x20ff

    .line 20
    .line 21
    if-gt p0, v0, :cond_3

    .line 22
    .line 23
    :cond_2
    const/4 p0, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_3
    const/4 p0, 0x0

    .line 26
    :goto_0
    return p0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public static 〇080(Ljava/lang/String;)Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x3

    .line 6
    const/4 v2, 0x0

    .line 7
    if-ge v0, v1, :cond_0

    .line 8
    .line 9
    return v2

    .line 10
    :cond_0
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/16 v1, 0x6e

    .line 15
    .line 16
    if-ne v0, v1, :cond_1

    .line 17
    .line 18
    const-string v0, "null"

    .line 19
    .line 20
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    move-result p0

    .line 24
    return p0

    .line 25
    :cond_1
    const/16 v1, 0x74

    .line 26
    .line 27
    if-ne v0, v1, :cond_2

    .line 28
    .line 29
    const-string/jumbo v0, "true"

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    move-result p0

    .line 36
    return p0

    .line 37
    :cond_2
    const/16 v1, 0x66

    .line 38
    .line 39
    if-ne v0, v1, :cond_3

    .line 40
    .line 41
    const-string v0, "false"

    .line 42
    .line 43
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    move-result p0

    .line 47
    return p0

    .line 48
    :cond_3
    const/16 v1, 0x4e

    .line 49
    .line 50
    if-ne v0, v1, :cond_4

    .line 51
    .line 52
    const-string v0, "NaN"

    .line 53
    .line 54
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 55
    .line 56
    .line 57
    move-result p0

    .line 58
    return p0

    .line 59
    :cond_4
    return v2
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public static 〇o00〇〇Oo(C)Z
    .locals 1

    .line 1
    const/16 v0, 0xd

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    const/16 v0, 0xa

    .line 6
    .line 7
    if-eq p0, v0, :cond_1

    .line 8
    .line 9
    const/16 v0, 0x9

    .line 10
    .line 11
    if-eq p0, v0, :cond_1

    .line 12
    .line 13
    const/16 v0, 0x20

    .line 14
    .line 15
    if-ne p0, v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 p0, 0x0

    .line 19
    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 21
    :goto_1
    return p0
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static 〇o〇(C)Z
    .locals 1

    .line 1
    const/16 v0, 0x7b

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    const/16 v0, 0x5b

    .line 6
    .line 7
    if-eq p0, v0, :cond_1

    .line 8
    .line 9
    const/16 v0, 0x2c

    .line 10
    .line 11
    if-eq p0, v0, :cond_1

    .line 12
    .line 13
    const/16 v0, 0x7d

    .line 14
    .line 15
    if-eq p0, v0, :cond_1

    .line 16
    .line 17
    const/16 v0, 0x5d

    .line 18
    .line 19
    if-eq p0, v0, :cond_1

    .line 20
    .line 21
    const/16 v0, 0x3a

    .line 22
    .line 23
    if-eq p0, v0, :cond_1

    .line 24
    .line 25
    const/16 v0, 0x27

    .line 26
    .line 27
    if-eq p0, v0, :cond_1

    .line 28
    .line 29
    const/16 v0, 0x22

    .line 30
    .line 31
    if-ne p0, v0, :cond_0

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    const/4 p0, 0x0

    .line 35
    goto :goto_1

    .line 36
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 37
    :goto_1
    return p0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

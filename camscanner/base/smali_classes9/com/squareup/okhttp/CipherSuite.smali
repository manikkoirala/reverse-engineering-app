.class public final enum Lcom/squareup/okhttp/CipherSuite;
.super Ljava/lang/Enum;
.source "CipherSuite.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/okhttp/CipherSuite;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_DSS_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_DSS_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_DSS_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_DSS_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_DSS_WITH_AES_256_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_DSS_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_DSS_WITH_DES_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_RSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_RSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_RSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_RSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_RSA_WITH_AES_256_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_RSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DHE_RSA_WITH_DES_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DH_anon_EXPORT_WITH_DES40_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DH_anon_EXPORT_WITH_RC4_40_MD5:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DH_anon_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DH_anon_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DH_anon_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DH_anon_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DH_anon_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DH_anon_WITH_AES_256_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DH_anon_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DH_anon_WITH_DES_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_DH_anon_WITH_RC4_128_MD5:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_ECDSA_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_ECDSA_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_RSA_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDHE_RSA_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_ECDSA_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_ECDSA_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_RSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_RSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_RSA_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_RSA_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_anon_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_anon_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_anon_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_ECDH_anon_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_EMPTY_RENEGOTIATION_INFO_SCSV:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_KRB5_EXPORT_WITH_RC4_40_MD5:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_KRB5_EXPORT_WITH_RC4_40_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_KRB5_WITH_3DES_EDE_CBC_MD5:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_KRB5_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_KRB5_WITH_DES_CBC_MD5:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_KRB5_WITH_DES_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_KRB5_WITH_RC4_128_MD5:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_KRB5_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_EXPORT_WITH_DES40_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_EXPORT_WITH_RC4_40_MD5:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_AES_256_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_DES_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_NULL_MD5:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_NULL_SHA256:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_RC4_128_MD5:Lcom/squareup/okhttp/CipherSuite;

.field public static final enum TLS_RSA_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;


# instance fields
.field final o0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 115

    .line 1
    new-instance v8, Lcom/squareup/okhttp/CipherSuite;

    .line 2
    .line 3
    const-string v1, "TLS_RSA_WITH_NULL_MD5"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const-string v3, "SSL_RSA_WITH_NULL_MD5"

    .line 7
    .line 8
    const/4 v4, 0x1

    .line 9
    const/16 v5, 0x147e

    .line 10
    .line 11
    const/4 v6, 0x6

    .line 12
    const/16 v7, 0xa

    .line 13
    .line 14
    move-object v0, v8

    .line 15
    invoke-direct/range {v0 .. v7}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 16
    .line 17
    .line 18
    sput-object v8, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_NULL_MD5:Lcom/squareup/okhttp/CipherSuite;

    .line 19
    .line 20
    new-instance v0, Lcom/squareup/okhttp/CipherSuite;

    .line 21
    .line 22
    const-string v10, "TLS_RSA_WITH_NULL_SHA"

    .line 23
    .line 24
    const/4 v11, 0x1

    .line 25
    const-string v12, "SSL_RSA_WITH_NULL_SHA"

    .line 26
    .line 27
    const/4 v13, 0x2

    .line 28
    const/16 v14, 0x147e

    .line 29
    .line 30
    const/4 v15, 0x6

    .line 31
    const/16 v16, 0xa

    .line 32
    .line 33
    move-object v9, v0

    .line 34
    invoke-direct/range {v9 .. v16}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 35
    .line 36
    .line 37
    sput-object v0, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 38
    .line 39
    new-instance v1, Lcom/squareup/okhttp/CipherSuite;

    .line 40
    .line 41
    const-string v18, "TLS_RSA_EXPORT_WITH_RC4_40_MD5"

    .line 42
    .line 43
    const/16 v19, 0x2

    .line 44
    .line 45
    const-string v20, "SSL_RSA_EXPORT_WITH_RC4_40_MD5"

    .line 46
    .line 47
    const/16 v21, 0x3

    .line 48
    .line 49
    const/16 v22, 0x10fa

    .line 50
    .line 51
    const/16 v23, 0x6

    .line 52
    .line 53
    const/16 v24, 0xa

    .line 54
    .line 55
    move-object/from16 v17, v1

    .line 56
    .line 57
    invoke-direct/range {v17 .. v24}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 58
    .line 59
    .line 60
    sput-object v1, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_EXPORT_WITH_RC4_40_MD5:Lcom/squareup/okhttp/CipherSuite;

    .line 61
    .line 62
    new-instance v2, Lcom/squareup/okhttp/CipherSuite;

    .line 63
    .line 64
    const-string v10, "TLS_RSA_WITH_RC4_128_MD5"

    .line 65
    .line 66
    const/4 v11, 0x3

    .line 67
    const-string v12, "SSL_RSA_WITH_RC4_128_MD5"

    .line 68
    .line 69
    const/4 v13, 0x4

    .line 70
    move-object v9, v2

    .line 71
    invoke-direct/range {v9 .. v16}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 72
    .line 73
    .line 74
    sput-object v2, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_RC4_128_MD5:Lcom/squareup/okhttp/CipherSuite;

    .line 75
    .line 76
    new-instance v3, Lcom/squareup/okhttp/CipherSuite;

    .line 77
    .line 78
    const-string v18, "TLS_RSA_WITH_RC4_128_SHA"

    .line 79
    .line 80
    const/16 v19, 0x4

    .line 81
    .line 82
    const-string v20, "SSL_RSA_WITH_RC4_128_SHA"

    .line 83
    .line 84
    const/16 v21, 0x5

    .line 85
    .line 86
    const/16 v22, 0x147e

    .line 87
    .line 88
    move-object/from16 v17, v3

    .line 89
    .line 90
    invoke-direct/range {v17 .. v24}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 91
    .line 92
    .line 93
    sput-object v3, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 94
    .line 95
    new-instance v4, Lcom/squareup/okhttp/CipherSuite;

    .line 96
    .line 97
    const-string v10, "TLS_RSA_EXPORT_WITH_DES40_CBC_SHA"

    .line 98
    .line 99
    const/4 v11, 0x5

    .line 100
    const-string v12, "SSL_RSA_EXPORT_WITH_DES40_CBC_SHA"

    .line 101
    .line 102
    const/16 v13, 0x8

    .line 103
    .line 104
    const/16 v14, 0x10fa

    .line 105
    .line 106
    move-object v9, v4

    .line 107
    invoke-direct/range {v9 .. v16}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 108
    .line 109
    .line 110
    sput-object v4, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_EXPORT_WITH_DES40_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 111
    .line 112
    new-instance v5, Lcom/squareup/okhttp/CipherSuite;

    .line 113
    .line 114
    const-string v18, "TLS_RSA_WITH_DES_CBC_SHA"

    .line 115
    .line 116
    const/16 v19, 0x6

    .line 117
    .line 118
    const-string v20, "SSL_RSA_WITH_DES_CBC_SHA"

    .line 119
    .line 120
    const/16 v21, 0x9

    .line 121
    .line 122
    const/16 v22, 0x155d

    .line 123
    .line 124
    move-object/from16 v17, v5

    .line 125
    .line 126
    invoke-direct/range {v17 .. v24}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 127
    .line 128
    .line 129
    sput-object v5, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_DES_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 130
    .line 131
    new-instance v6, Lcom/squareup/okhttp/CipherSuite;

    .line 132
    .line 133
    const-string v10, "TLS_RSA_WITH_3DES_EDE_CBC_SHA"

    .line 134
    .line 135
    const/4 v11, 0x7

    .line 136
    const-string v12, "SSL_RSA_WITH_3DES_EDE_CBC_SHA"

    .line 137
    .line 138
    const/16 v13, 0xa

    .line 139
    .line 140
    const/16 v14, 0x147e

    .line 141
    .line 142
    move-object v9, v6

    .line 143
    invoke-direct/range {v9 .. v16}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 144
    .line 145
    .line 146
    sput-object v6, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 147
    .line 148
    new-instance v7, Lcom/squareup/okhttp/CipherSuite;

    .line 149
    .line 150
    const-string v18, "TLS_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA"

    .line 151
    .line 152
    const/16 v19, 0x8

    .line 153
    .line 154
    const-string v20, "SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA"

    .line 155
    .line 156
    const/16 v21, 0x11

    .line 157
    .line 158
    const/16 v22, 0x10fa

    .line 159
    .line 160
    move-object/from16 v17, v7

    .line 161
    .line 162
    invoke-direct/range {v17 .. v24}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 163
    .line 164
    .line 165
    sput-object v7, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 166
    .line 167
    new-instance v17, Lcom/squareup/okhttp/CipherSuite;

    .line 168
    .line 169
    const-string v10, "TLS_DHE_DSS_WITH_DES_CBC_SHA"

    .line 170
    .line 171
    const/16 v11, 0x9

    .line 172
    .line 173
    const-string v12, "SSL_DHE_DSS_WITH_DES_CBC_SHA"

    .line 174
    .line 175
    const/16 v13, 0x12

    .line 176
    .line 177
    const/16 v14, 0x155d

    .line 178
    .line 179
    move-object/from16 v9, v17

    .line 180
    .line 181
    invoke-direct/range {v9 .. v16}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 182
    .line 183
    .line 184
    sput-object v17, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_DSS_WITH_DES_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 185
    .line 186
    new-instance v9, Lcom/squareup/okhttp/CipherSuite;

    .line 187
    .line 188
    const-string v19, "TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA"

    .line 189
    .line 190
    const/16 v20, 0xa

    .line 191
    .line 192
    const-string v21, "SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA"

    .line 193
    .line 194
    const/16 v22, 0x13

    .line 195
    .line 196
    const/16 v23, 0x147e

    .line 197
    .line 198
    const/16 v24, 0x6

    .line 199
    .line 200
    const/16 v25, 0xa

    .line 201
    .line 202
    move-object/from16 v18, v9

    .line 203
    .line 204
    invoke-direct/range {v18 .. v25}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 205
    .line 206
    .line 207
    sput-object v9, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 208
    .line 209
    new-instance v10, Lcom/squareup/okhttp/CipherSuite;

    .line 210
    .line 211
    const-string v27, "TLS_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA"

    .line 212
    .line 213
    const/16 v28, 0xb

    .line 214
    .line 215
    const-string v29, "SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA"

    .line 216
    .line 217
    const/16 v30, 0x14

    .line 218
    .line 219
    const/16 v31, 0x10fa

    .line 220
    .line 221
    const/16 v32, 0x6

    .line 222
    .line 223
    const/16 v33, 0xa

    .line 224
    .line 225
    move-object/from16 v26, v10

    .line 226
    .line 227
    invoke-direct/range {v26 .. v33}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 228
    .line 229
    .line 230
    sput-object v10, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 231
    .line 232
    new-instance v11, Lcom/squareup/okhttp/CipherSuite;

    .line 233
    .line 234
    const-string v19, "TLS_DHE_RSA_WITH_DES_CBC_SHA"

    .line 235
    .line 236
    const/16 v20, 0xc

    .line 237
    .line 238
    const-string v21, "SSL_DHE_RSA_WITH_DES_CBC_SHA"

    .line 239
    .line 240
    const/16 v22, 0x15

    .line 241
    .line 242
    const/16 v23, 0x155d

    .line 243
    .line 244
    move-object/from16 v18, v11

    .line 245
    .line 246
    invoke-direct/range {v18 .. v25}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 247
    .line 248
    .line 249
    sput-object v11, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_RSA_WITH_DES_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 250
    .line 251
    new-instance v12, Lcom/squareup/okhttp/CipherSuite;

    .line 252
    .line 253
    const-string v27, "TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA"

    .line 254
    .line 255
    const/16 v28, 0xd

    .line 256
    .line 257
    const-string v29, "SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA"

    .line 258
    .line 259
    const/16 v30, 0x16

    .line 260
    .line 261
    const/16 v31, 0x147e

    .line 262
    .line 263
    move-object/from16 v26, v12

    .line 264
    .line 265
    invoke-direct/range {v26 .. v33}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 266
    .line 267
    .line 268
    sput-object v12, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 269
    .line 270
    new-instance v13, Lcom/squareup/okhttp/CipherSuite;

    .line 271
    .line 272
    const-string v19, "TLS_DH_anon_EXPORT_WITH_RC4_40_MD5"

    .line 273
    .line 274
    const/16 v20, 0xe

    .line 275
    .line 276
    const-string v21, "SSL_DH_anon_EXPORT_WITH_RC4_40_MD5"

    .line 277
    .line 278
    const/16 v22, 0x17

    .line 279
    .line 280
    const/16 v23, 0x10fa

    .line 281
    .line 282
    move-object/from16 v18, v13

    .line 283
    .line 284
    invoke-direct/range {v18 .. v25}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 285
    .line 286
    .line 287
    sput-object v13, Lcom/squareup/okhttp/CipherSuite;->TLS_DH_anon_EXPORT_WITH_RC4_40_MD5:Lcom/squareup/okhttp/CipherSuite;

    .line 288
    .line 289
    new-instance v14, Lcom/squareup/okhttp/CipherSuite;

    .line 290
    .line 291
    const-string v27, "TLS_DH_anon_WITH_RC4_128_MD5"

    .line 292
    .line 293
    const/16 v28, 0xf

    .line 294
    .line 295
    const-string v29, "SSL_DH_anon_WITH_RC4_128_MD5"

    .line 296
    .line 297
    const/16 v30, 0x18

    .line 298
    .line 299
    move-object/from16 v26, v14

    .line 300
    .line 301
    invoke-direct/range {v26 .. v33}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 302
    .line 303
    .line 304
    sput-object v14, Lcom/squareup/okhttp/CipherSuite;->TLS_DH_anon_WITH_RC4_128_MD5:Lcom/squareup/okhttp/CipherSuite;

    .line 305
    .line 306
    new-instance v15, Lcom/squareup/okhttp/CipherSuite;

    .line 307
    .line 308
    const-string v19, "TLS_DH_anon_EXPORT_WITH_DES40_CBC_SHA"

    .line 309
    .line 310
    const/16 v20, 0x10

    .line 311
    .line 312
    const-string v21, "SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA"

    .line 313
    .line 314
    const/16 v22, 0x19

    .line 315
    .line 316
    move-object/from16 v18, v15

    .line 317
    .line 318
    invoke-direct/range {v18 .. v25}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 319
    .line 320
    .line 321
    sput-object v15, Lcom/squareup/okhttp/CipherSuite;->TLS_DH_anon_EXPORT_WITH_DES40_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 322
    .line 323
    new-instance v16, Lcom/squareup/okhttp/CipherSuite;

    .line 324
    .line 325
    const-string v27, "TLS_DH_anon_WITH_DES_CBC_SHA"

    .line 326
    .line 327
    const/16 v28, 0x11

    .line 328
    .line 329
    const-string v29, "SSL_DH_anon_WITH_DES_CBC_SHA"

    .line 330
    .line 331
    const/16 v30, 0x1a

    .line 332
    .line 333
    const/16 v31, 0x155d

    .line 334
    .line 335
    move-object/from16 v26, v16

    .line 336
    .line 337
    invoke-direct/range {v26 .. v33}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 338
    .line 339
    .line 340
    sput-object v16, Lcom/squareup/okhttp/CipherSuite;->TLS_DH_anon_WITH_DES_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 341
    .line 342
    new-instance v26, Lcom/squareup/okhttp/CipherSuite;

    .line 343
    .line 344
    const-string v19, "TLS_DH_anon_WITH_3DES_EDE_CBC_SHA"

    .line 345
    .line 346
    const/16 v20, 0x12

    .line 347
    .line 348
    const-string v21, "SSL_DH_anon_WITH_3DES_EDE_CBC_SHA"

    .line 349
    .line 350
    const/16 v22, 0x1b

    .line 351
    .line 352
    const/16 v23, 0x147e

    .line 353
    .line 354
    move-object/from16 v18, v26

    .line 355
    .line 356
    invoke-direct/range {v18 .. v25}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 357
    .line 358
    .line 359
    sput-object v26, Lcom/squareup/okhttp/CipherSuite;->TLS_DH_anon_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 360
    .line 361
    new-instance v18, Lcom/squareup/okhttp/CipherSuite;

    .line 362
    .line 363
    const-string v28, "TLS_KRB5_WITH_DES_CBC_SHA"

    .line 364
    .line 365
    const/16 v29, 0x13

    .line 366
    .line 367
    const-string v30, "TLS_KRB5_WITH_DES_CBC_SHA"

    .line 368
    .line 369
    const/16 v31, 0x1e

    .line 370
    .line 371
    const/16 v32, 0xa98

    .line 372
    .line 373
    const/16 v33, 0x6

    .line 374
    .line 375
    const v34, 0x7fffffff

    .line 376
    .line 377
    .line 378
    move-object/from16 v27, v18

    .line 379
    .line 380
    invoke-direct/range {v27 .. v34}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 381
    .line 382
    .line 383
    sput-object v18, Lcom/squareup/okhttp/CipherSuite;->TLS_KRB5_WITH_DES_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 384
    .line 385
    new-instance v19, Lcom/squareup/okhttp/CipherSuite;

    .line 386
    .line 387
    const-string v36, "TLS_KRB5_WITH_3DES_EDE_CBC_SHA"

    .line 388
    .line 389
    const/16 v37, 0x14

    .line 390
    .line 391
    const-string v38, "TLS_KRB5_WITH_3DES_EDE_CBC_SHA"

    .line 392
    .line 393
    const/16 v39, 0x1f

    .line 394
    .line 395
    const/16 v40, 0xa98

    .line 396
    .line 397
    const/16 v41, 0x6

    .line 398
    .line 399
    const v42, 0x7fffffff

    .line 400
    .line 401
    .line 402
    move-object/from16 v35, v19

    .line 403
    .line 404
    invoke-direct/range {v35 .. v42}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 405
    .line 406
    .line 407
    sput-object v19, Lcom/squareup/okhttp/CipherSuite;->TLS_KRB5_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 408
    .line 409
    new-instance v20, Lcom/squareup/okhttp/CipherSuite;

    .line 410
    .line 411
    const-string v28, "TLS_KRB5_WITH_RC4_128_SHA"

    .line 412
    .line 413
    const/16 v29, 0x15

    .line 414
    .line 415
    const-string v30, "TLS_KRB5_WITH_RC4_128_SHA"

    .line 416
    .line 417
    const/16 v31, 0x20

    .line 418
    .line 419
    move-object/from16 v27, v20

    .line 420
    .line 421
    invoke-direct/range {v27 .. v34}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 422
    .line 423
    .line 424
    sput-object v20, Lcom/squareup/okhttp/CipherSuite;->TLS_KRB5_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 425
    .line 426
    new-instance v21, Lcom/squareup/okhttp/CipherSuite;

    .line 427
    .line 428
    const-string v36, "TLS_KRB5_WITH_DES_CBC_MD5"

    .line 429
    .line 430
    const/16 v37, 0x16

    .line 431
    .line 432
    const-string v38, "TLS_KRB5_WITH_DES_CBC_MD5"

    .line 433
    .line 434
    const/16 v39, 0x22

    .line 435
    .line 436
    move-object/from16 v35, v21

    .line 437
    .line 438
    invoke-direct/range {v35 .. v42}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 439
    .line 440
    .line 441
    sput-object v21, Lcom/squareup/okhttp/CipherSuite;->TLS_KRB5_WITH_DES_CBC_MD5:Lcom/squareup/okhttp/CipherSuite;

    .line 442
    .line 443
    new-instance v22, Lcom/squareup/okhttp/CipherSuite;

    .line 444
    .line 445
    const-string v28, "TLS_KRB5_WITH_3DES_EDE_CBC_MD5"

    .line 446
    .line 447
    const/16 v29, 0x17

    .line 448
    .line 449
    const-string v30, "TLS_KRB5_WITH_3DES_EDE_CBC_MD5"

    .line 450
    .line 451
    const/16 v31, 0x23

    .line 452
    .line 453
    move-object/from16 v27, v22

    .line 454
    .line 455
    invoke-direct/range {v27 .. v34}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 456
    .line 457
    .line 458
    sput-object v22, Lcom/squareup/okhttp/CipherSuite;->TLS_KRB5_WITH_3DES_EDE_CBC_MD5:Lcom/squareup/okhttp/CipherSuite;

    .line 459
    .line 460
    new-instance v23, Lcom/squareup/okhttp/CipherSuite;

    .line 461
    .line 462
    const-string v36, "TLS_KRB5_WITH_RC4_128_MD5"

    .line 463
    .line 464
    const/16 v37, 0x18

    .line 465
    .line 466
    const-string v38, "TLS_KRB5_WITH_RC4_128_MD5"

    .line 467
    .line 468
    const/16 v39, 0x24

    .line 469
    .line 470
    move-object/from16 v35, v23

    .line 471
    .line 472
    invoke-direct/range {v35 .. v42}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 473
    .line 474
    .line 475
    sput-object v23, Lcom/squareup/okhttp/CipherSuite;->TLS_KRB5_WITH_RC4_128_MD5:Lcom/squareup/okhttp/CipherSuite;

    .line 476
    .line 477
    new-instance v24, Lcom/squareup/okhttp/CipherSuite;

    .line 478
    .line 479
    const-string v28, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA"

    .line 480
    .line 481
    const/16 v29, 0x19

    .line 482
    .line 483
    const-string v30, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA"

    .line 484
    .line 485
    const/16 v31, 0x26

    .line 486
    .line 487
    move-object/from16 v27, v24

    .line 488
    .line 489
    invoke-direct/range {v27 .. v34}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 490
    .line 491
    .line 492
    sput-object v24, Lcom/squareup/okhttp/CipherSuite;->TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 493
    .line 494
    new-instance v25, Lcom/squareup/okhttp/CipherSuite;

    .line 495
    .line 496
    const-string v36, "TLS_KRB5_EXPORT_WITH_RC4_40_SHA"

    .line 497
    .line 498
    const/16 v37, 0x1a

    .line 499
    .line 500
    const-string v38, "TLS_KRB5_EXPORT_WITH_RC4_40_SHA"

    .line 501
    .line 502
    const/16 v39, 0x28

    .line 503
    .line 504
    move-object/from16 v35, v25

    .line 505
    .line 506
    invoke-direct/range {v35 .. v42}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 507
    .line 508
    .line 509
    sput-object v25, Lcom/squareup/okhttp/CipherSuite;->TLS_KRB5_EXPORT_WITH_RC4_40_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 510
    .line 511
    new-instance v35, Lcom/squareup/okhttp/CipherSuite;

    .line 512
    .line 513
    const-string v28, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5"

    .line 514
    .line 515
    const/16 v29, 0x1b

    .line 516
    .line 517
    const-string v30, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5"

    .line 518
    .line 519
    const/16 v31, 0x29

    .line 520
    .line 521
    move-object/from16 v27, v35

    .line 522
    .line 523
    invoke-direct/range {v27 .. v34}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 524
    .line 525
    .line 526
    sput-object v35, Lcom/squareup/okhttp/CipherSuite;->TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5:Lcom/squareup/okhttp/CipherSuite;

    .line 527
    .line 528
    new-instance v27, Lcom/squareup/okhttp/CipherSuite;

    .line 529
    .line 530
    const-string v37, "TLS_KRB5_EXPORT_WITH_RC4_40_MD5"

    .line 531
    .line 532
    const/16 v38, 0x1c

    .line 533
    .line 534
    const-string v39, "TLS_KRB5_EXPORT_WITH_RC4_40_MD5"

    .line 535
    .line 536
    const/16 v40, 0x2b

    .line 537
    .line 538
    const/16 v41, 0xa98

    .line 539
    .line 540
    const/16 v42, 0x6

    .line 541
    .line 542
    const v43, 0x7fffffff

    .line 543
    .line 544
    .line 545
    move-object/from16 v36, v27

    .line 546
    .line 547
    invoke-direct/range {v36 .. v43}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 548
    .line 549
    .line 550
    sput-object v27, Lcom/squareup/okhttp/CipherSuite;->TLS_KRB5_EXPORT_WITH_RC4_40_MD5:Lcom/squareup/okhttp/CipherSuite;

    .line 551
    .line 552
    new-instance v28, Lcom/squareup/okhttp/CipherSuite;

    .line 553
    .line 554
    const-string v45, "TLS_RSA_WITH_AES_128_CBC_SHA"

    .line 555
    .line 556
    const/16 v46, 0x1d

    .line 557
    .line 558
    const-string v47, "TLS_RSA_WITH_AES_128_CBC_SHA"

    .line 559
    .line 560
    const/16 v48, 0x2f

    .line 561
    .line 562
    const/16 v49, 0x147e

    .line 563
    .line 564
    const/16 v50, 0x6

    .line 565
    .line 566
    const/16 v51, 0xa

    .line 567
    .line 568
    move-object/from16 v44, v28

    .line 569
    .line 570
    invoke-direct/range {v44 .. v51}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 571
    .line 572
    .line 573
    sput-object v28, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 574
    .line 575
    new-instance v29, Lcom/squareup/okhttp/CipherSuite;

    .line 576
    .line 577
    const-string v37, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA"

    .line 578
    .line 579
    const/16 v38, 0x1e

    .line 580
    .line 581
    const-string v39, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA"

    .line 582
    .line 583
    const/16 v40, 0x32

    .line 584
    .line 585
    const/16 v41, 0x147e

    .line 586
    .line 587
    const/16 v43, 0xa

    .line 588
    .line 589
    move-object/from16 v36, v29

    .line 590
    .line 591
    invoke-direct/range {v36 .. v43}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 592
    .line 593
    .line 594
    sput-object v29, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_DSS_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 595
    .line 596
    new-instance v30, Lcom/squareup/okhttp/CipherSuite;

    .line 597
    .line 598
    const-string v45, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA"

    .line 599
    .line 600
    const/16 v46, 0x1f

    .line 601
    .line 602
    const-string v47, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA"

    .line 603
    .line 604
    const/16 v48, 0x33

    .line 605
    .line 606
    move-object/from16 v44, v30

    .line 607
    .line 608
    invoke-direct/range {v44 .. v51}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 609
    .line 610
    .line 611
    sput-object v30, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_RSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 612
    .line 613
    new-instance v31, Lcom/squareup/okhttp/CipherSuite;

    .line 614
    .line 615
    const-string v37, "TLS_DH_anon_WITH_AES_128_CBC_SHA"

    .line 616
    .line 617
    const/16 v38, 0x20

    .line 618
    .line 619
    const-string v39, "TLS_DH_anon_WITH_AES_128_CBC_SHA"

    .line 620
    .line 621
    const/16 v40, 0x34

    .line 622
    .line 623
    move-object/from16 v36, v31

    .line 624
    .line 625
    invoke-direct/range {v36 .. v43}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 626
    .line 627
    .line 628
    sput-object v31, Lcom/squareup/okhttp/CipherSuite;->TLS_DH_anon_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 629
    .line 630
    new-instance v32, Lcom/squareup/okhttp/CipherSuite;

    .line 631
    .line 632
    const-string v45, "TLS_RSA_WITH_AES_256_CBC_SHA"

    .line 633
    .line 634
    const/16 v46, 0x21

    .line 635
    .line 636
    const-string v47, "TLS_RSA_WITH_AES_256_CBC_SHA"

    .line 637
    .line 638
    const/16 v48, 0x35

    .line 639
    .line 640
    move-object/from16 v44, v32

    .line 641
    .line 642
    invoke-direct/range {v44 .. v51}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 643
    .line 644
    .line 645
    sput-object v32, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 646
    .line 647
    new-instance v33, Lcom/squareup/okhttp/CipherSuite;

    .line 648
    .line 649
    const-string v37, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA"

    .line 650
    .line 651
    const/16 v38, 0x22

    .line 652
    .line 653
    const-string v39, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA"

    .line 654
    .line 655
    const/16 v40, 0x38

    .line 656
    .line 657
    move-object/from16 v36, v33

    .line 658
    .line 659
    invoke-direct/range {v36 .. v43}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 660
    .line 661
    .line 662
    sput-object v33, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_DSS_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 663
    .line 664
    new-instance v34, Lcom/squareup/okhttp/CipherSuite;

    .line 665
    .line 666
    const-string v45, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA"

    .line 667
    .line 668
    const/16 v46, 0x23

    .line 669
    .line 670
    const-string v47, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA"

    .line 671
    .line 672
    const/16 v48, 0x39

    .line 673
    .line 674
    move-object/from16 v44, v34

    .line 675
    .line 676
    invoke-direct/range {v44 .. v51}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 677
    .line 678
    .line 679
    sput-object v34, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_RSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 680
    .line 681
    new-instance v44, Lcom/squareup/okhttp/CipherSuite;

    .line 682
    .line 683
    const-string v37, "TLS_DH_anon_WITH_AES_256_CBC_SHA"

    .line 684
    .line 685
    const/16 v38, 0x24

    .line 686
    .line 687
    const-string v39, "TLS_DH_anon_WITH_AES_256_CBC_SHA"

    .line 688
    .line 689
    const/16 v40, 0x3a

    .line 690
    .line 691
    move-object/from16 v36, v44

    .line 692
    .line 693
    invoke-direct/range {v36 .. v43}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 694
    .line 695
    .line 696
    sput-object v44, Lcom/squareup/okhttp/CipherSuite;->TLS_DH_anon_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 697
    .line 698
    new-instance v36, Lcom/squareup/okhttp/CipherSuite;

    .line 699
    .line 700
    const-string v46, "TLS_RSA_WITH_NULL_SHA256"

    .line 701
    .line 702
    const/16 v47, 0x25

    .line 703
    .line 704
    const-string v48, "TLS_RSA_WITH_NULL_SHA256"

    .line 705
    .line 706
    const/16 v49, 0x3b

    .line 707
    .line 708
    const/16 v50, 0x147e

    .line 709
    .line 710
    const/16 v51, 0x7

    .line 711
    .line 712
    const/16 v52, 0x15

    .line 713
    .line 714
    move-object/from16 v45, v36

    .line 715
    .line 716
    invoke-direct/range {v45 .. v52}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 717
    .line 718
    .line 719
    sput-object v36, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_NULL_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 720
    .line 721
    new-instance v37, Lcom/squareup/okhttp/CipherSuite;

    .line 722
    .line 723
    const-string v54, "TLS_RSA_WITH_AES_128_CBC_SHA256"

    .line 724
    .line 725
    const/16 v55, 0x26

    .line 726
    .line 727
    const-string v56, "TLS_RSA_WITH_AES_128_CBC_SHA256"

    .line 728
    .line 729
    const/16 v57, 0x3c

    .line 730
    .line 731
    const/16 v58, 0x147e

    .line 732
    .line 733
    const/16 v59, 0x7

    .line 734
    .line 735
    const/16 v60, 0x15

    .line 736
    .line 737
    move-object/from16 v53, v37

    .line 738
    .line 739
    invoke-direct/range {v53 .. v60}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 740
    .line 741
    .line 742
    sput-object v37, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 743
    .line 744
    new-instance v38, Lcom/squareup/okhttp/CipherSuite;

    .line 745
    .line 746
    const-string v46, "TLS_RSA_WITH_AES_256_CBC_SHA256"

    .line 747
    .line 748
    const/16 v47, 0x27

    .line 749
    .line 750
    const-string v48, "TLS_RSA_WITH_AES_256_CBC_SHA256"

    .line 751
    .line 752
    const/16 v49, 0x3d

    .line 753
    .line 754
    move-object/from16 v45, v38

    .line 755
    .line 756
    invoke-direct/range {v45 .. v52}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 757
    .line 758
    .line 759
    sput-object v38, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_AES_256_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 760
    .line 761
    new-instance v39, Lcom/squareup/okhttp/CipherSuite;

    .line 762
    .line 763
    const-string v54, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256"

    .line 764
    .line 765
    const/16 v55, 0x28

    .line 766
    .line 767
    const-string v56, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256"

    .line 768
    .line 769
    const/16 v57, 0x40

    .line 770
    .line 771
    move-object/from16 v53, v39

    .line 772
    .line 773
    invoke-direct/range {v53 .. v60}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 774
    .line 775
    .line 776
    sput-object v39, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_DSS_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 777
    .line 778
    new-instance v40, Lcom/squareup/okhttp/CipherSuite;

    .line 779
    .line 780
    const-string v46, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256"

    .line 781
    .line 782
    const/16 v47, 0x29

    .line 783
    .line 784
    const-string v48, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256"

    .line 785
    .line 786
    const/16 v49, 0x67

    .line 787
    .line 788
    move-object/from16 v45, v40

    .line 789
    .line 790
    invoke-direct/range {v45 .. v52}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 791
    .line 792
    .line 793
    sput-object v40, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_RSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 794
    .line 795
    new-instance v41, Lcom/squareup/okhttp/CipherSuite;

    .line 796
    .line 797
    const-string v54, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256"

    .line 798
    .line 799
    const/16 v55, 0x2a

    .line 800
    .line 801
    const-string v56, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256"

    .line 802
    .line 803
    const/16 v57, 0x6a

    .line 804
    .line 805
    move-object/from16 v53, v41

    .line 806
    .line 807
    invoke-direct/range {v53 .. v60}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 808
    .line 809
    .line 810
    sput-object v41, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_DSS_WITH_AES_256_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 811
    .line 812
    new-instance v42, Lcom/squareup/okhttp/CipherSuite;

    .line 813
    .line 814
    const-string v46, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256"

    .line 815
    .line 816
    const/16 v47, 0x2b

    .line 817
    .line 818
    const-string v48, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256"

    .line 819
    .line 820
    const/16 v49, 0x6b

    .line 821
    .line 822
    move-object/from16 v45, v42

    .line 823
    .line 824
    invoke-direct/range {v45 .. v52}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 825
    .line 826
    .line 827
    sput-object v42, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_RSA_WITH_AES_256_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 828
    .line 829
    new-instance v43, Lcom/squareup/okhttp/CipherSuite;

    .line 830
    .line 831
    const-string v54, "TLS_DH_anon_WITH_AES_128_CBC_SHA256"

    .line 832
    .line 833
    const/16 v55, 0x2c

    .line 834
    .line 835
    const-string v56, "TLS_DH_anon_WITH_AES_128_CBC_SHA256"

    .line 836
    .line 837
    const/16 v57, 0x6c

    .line 838
    .line 839
    move-object/from16 v53, v43

    .line 840
    .line 841
    invoke-direct/range {v53 .. v60}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 842
    .line 843
    .line 844
    sput-object v43, Lcom/squareup/okhttp/CipherSuite;->TLS_DH_anon_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 845
    .line 846
    new-instance v53, Lcom/squareup/okhttp/CipherSuite;

    .line 847
    .line 848
    const-string v46, "TLS_DH_anon_WITH_AES_256_CBC_SHA256"

    .line 849
    .line 850
    const/16 v47, 0x2d

    .line 851
    .line 852
    const-string v48, "TLS_DH_anon_WITH_AES_256_CBC_SHA256"

    .line 853
    .line 854
    const/16 v49, 0x6d

    .line 855
    .line 856
    move-object/from16 v45, v53

    .line 857
    .line 858
    invoke-direct/range {v45 .. v52}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 859
    .line 860
    .line 861
    sput-object v53, Lcom/squareup/okhttp/CipherSuite;->TLS_DH_anon_WITH_AES_256_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 862
    .line 863
    new-instance v45, Lcom/squareup/okhttp/CipherSuite;

    .line 864
    .line 865
    const-string v55, "TLS_RSA_WITH_AES_128_GCM_SHA256"

    .line 866
    .line 867
    const/16 v56, 0x2e

    .line 868
    .line 869
    const-string v57, "TLS_RSA_WITH_AES_128_GCM_SHA256"

    .line 870
    .line 871
    const/16 v58, 0x9c

    .line 872
    .line 873
    const/16 v59, 0x14a8

    .line 874
    .line 875
    const/16 v60, 0x8

    .line 876
    .line 877
    const/16 v61, 0x15

    .line 878
    .line 879
    move-object/from16 v54, v45

    .line 880
    .line 881
    invoke-direct/range {v54 .. v61}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 882
    .line 883
    .line 884
    sput-object v45, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 885
    .line 886
    new-instance v46, Lcom/squareup/okhttp/CipherSuite;

    .line 887
    .line 888
    const-string v63, "TLS_RSA_WITH_AES_256_GCM_SHA384"

    .line 889
    .line 890
    const/16 v64, 0x2f

    .line 891
    .line 892
    const-string v65, "TLS_RSA_WITH_AES_256_GCM_SHA384"

    .line 893
    .line 894
    const/16 v66, 0x9d

    .line 895
    .line 896
    const/16 v67, 0x14a8

    .line 897
    .line 898
    const/16 v68, 0x8

    .line 899
    .line 900
    const/16 v69, 0x15

    .line 901
    .line 902
    move-object/from16 v62, v46

    .line 903
    .line 904
    invoke-direct/range {v62 .. v69}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 905
    .line 906
    .line 907
    sput-object v46, Lcom/squareup/okhttp/CipherSuite;->TLS_RSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 908
    .line 909
    new-instance v47, Lcom/squareup/okhttp/CipherSuite;

    .line 910
    .line 911
    const-string v55, "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256"

    .line 912
    .line 913
    const/16 v56, 0x30

    .line 914
    .line 915
    const-string v57, "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256"

    .line 916
    .line 917
    const/16 v58, 0x9e

    .line 918
    .line 919
    move-object/from16 v54, v47

    .line 920
    .line 921
    invoke-direct/range {v54 .. v61}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 922
    .line 923
    .line 924
    sput-object v47, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_RSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 925
    .line 926
    new-instance v48, Lcom/squareup/okhttp/CipherSuite;

    .line 927
    .line 928
    const-string v63, "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384"

    .line 929
    .line 930
    const/16 v64, 0x31

    .line 931
    .line 932
    const-string v65, "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384"

    .line 933
    .line 934
    const/16 v66, 0x9f

    .line 935
    .line 936
    move-object/from16 v62, v48

    .line 937
    .line 938
    invoke-direct/range {v62 .. v69}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 939
    .line 940
    .line 941
    sput-object v48, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_RSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 942
    .line 943
    new-instance v49, Lcom/squareup/okhttp/CipherSuite;

    .line 944
    .line 945
    const-string v55, "TLS_DHE_DSS_WITH_AES_128_GCM_SHA256"

    .line 946
    .line 947
    const/16 v56, 0x32

    .line 948
    .line 949
    const-string v57, "TLS_DHE_DSS_WITH_AES_128_GCM_SHA256"

    .line 950
    .line 951
    const/16 v58, 0xa2

    .line 952
    .line 953
    move-object/from16 v54, v49

    .line 954
    .line 955
    invoke-direct/range {v54 .. v61}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 956
    .line 957
    .line 958
    sput-object v49, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_DSS_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 959
    .line 960
    new-instance v50, Lcom/squareup/okhttp/CipherSuite;

    .line 961
    .line 962
    const-string v63, "TLS_DHE_DSS_WITH_AES_256_GCM_SHA384"

    .line 963
    .line 964
    const/16 v64, 0x33

    .line 965
    .line 966
    const-string v65, "TLS_DHE_DSS_WITH_AES_256_GCM_SHA384"

    .line 967
    .line 968
    const/16 v66, 0xa3

    .line 969
    .line 970
    move-object/from16 v62, v50

    .line 971
    .line 972
    invoke-direct/range {v62 .. v69}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 973
    .line 974
    .line 975
    sput-object v50, Lcom/squareup/okhttp/CipherSuite;->TLS_DHE_DSS_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 976
    .line 977
    new-instance v51, Lcom/squareup/okhttp/CipherSuite;

    .line 978
    .line 979
    const-string v55, "TLS_DH_anon_WITH_AES_128_GCM_SHA256"

    .line 980
    .line 981
    const/16 v56, 0x34

    .line 982
    .line 983
    const-string v57, "TLS_DH_anon_WITH_AES_128_GCM_SHA256"

    .line 984
    .line 985
    const/16 v58, 0xa6

    .line 986
    .line 987
    move-object/from16 v54, v51

    .line 988
    .line 989
    invoke-direct/range {v54 .. v61}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 990
    .line 991
    .line 992
    sput-object v51, Lcom/squareup/okhttp/CipherSuite;->TLS_DH_anon_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 993
    .line 994
    new-instance v52, Lcom/squareup/okhttp/CipherSuite;

    .line 995
    .line 996
    const-string v63, "TLS_DH_anon_WITH_AES_256_GCM_SHA384"

    .line 997
    .line 998
    const/16 v64, 0x35

    .line 999
    .line 1000
    const-string v65, "TLS_DH_anon_WITH_AES_256_GCM_SHA384"

    .line 1001
    .line 1002
    const/16 v66, 0xa7

    .line 1003
    .line 1004
    move-object/from16 v62, v52

    .line 1005
    .line 1006
    invoke-direct/range {v62 .. v69}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1007
    .line 1008
    .line 1009
    sput-object v52, Lcom/squareup/okhttp/CipherSuite;->TLS_DH_anon_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 1010
    .line 1011
    new-instance v62, Lcom/squareup/okhttp/CipherSuite;

    .line 1012
    .line 1013
    const-string v55, "TLS_EMPTY_RENEGOTIATION_INFO_SCSV"

    .line 1014
    .line 1015
    const/16 v56, 0x36

    .line 1016
    .line 1017
    const-string v57, "TLS_EMPTY_RENEGOTIATION_INFO_SCSV"

    .line 1018
    .line 1019
    const/16 v58, 0xff

    .line 1020
    .line 1021
    const/16 v59, 0x1672

    .line 1022
    .line 1023
    const/16 v60, 0x6

    .line 1024
    .line 1025
    const/16 v61, 0xe

    .line 1026
    .line 1027
    move-object/from16 v54, v62

    .line 1028
    .line 1029
    invoke-direct/range {v54 .. v61}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1030
    .line 1031
    .line 1032
    sput-object v62, Lcom/squareup/okhttp/CipherSuite;->TLS_EMPTY_RENEGOTIATION_INFO_SCSV:Lcom/squareup/okhttp/CipherSuite;

    .line 1033
    .line 1034
    new-instance v54, Lcom/squareup/okhttp/CipherSuite;

    .line 1035
    .line 1036
    const-string v64, "TLS_ECDH_ECDSA_WITH_NULL_SHA"

    .line 1037
    .line 1038
    const/16 v65, 0x37

    .line 1039
    .line 1040
    const-string v66, "TLS_ECDH_ECDSA_WITH_NULL_SHA"

    .line 1041
    .line 1042
    const v67, 0xc001

    .line 1043
    .line 1044
    .line 1045
    const/16 v68, 0x118c

    .line 1046
    .line 1047
    const/16 v69, 0x7

    .line 1048
    .line 1049
    const/16 v70, 0xe

    .line 1050
    .line 1051
    move-object/from16 v63, v54

    .line 1052
    .line 1053
    invoke-direct/range {v63 .. v70}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1054
    .line 1055
    .line 1056
    sput-object v54, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_ECDSA_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1057
    .line 1058
    new-instance v55, Lcom/squareup/okhttp/CipherSuite;

    .line 1059
    .line 1060
    const-string v72, "TLS_ECDH_ECDSA_WITH_RC4_128_SHA"

    .line 1061
    .line 1062
    const/16 v73, 0x38

    .line 1063
    .line 1064
    const-string v74, "TLS_ECDH_ECDSA_WITH_RC4_128_SHA"

    .line 1065
    .line 1066
    const v75, 0xc002

    .line 1067
    .line 1068
    .line 1069
    const/16 v76, 0x118c

    .line 1070
    .line 1071
    const/16 v77, 0x7

    .line 1072
    .line 1073
    const/16 v78, 0xe

    .line 1074
    .line 1075
    move-object/from16 v71, v55

    .line 1076
    .line 1077
    invoke-direct/range {v71 .. v78}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1078
    .line 1079
    .line 1080
    sput-object v55, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_ECDSA_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1081
    .line 1082
    new-instance v56, Lcom/squareup/okhttp/CipherSuite;

    .line 1083
    .line 1084
    const-string v64, "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA"

    .line 1085
    .line 1086
    const/16 v65, 0x39

    .line 1087
    .line 1088
    const-string v66, "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA"

    .line 1089
    .line 1090
    const v67, 0xc003

    .line 1091
    .line 1092
    .line 1093
    move-object/from16 v63, v56

    .line 1094
    .line 1095
    invoke-direct/range {v63 .. v70}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1096
    .line 1097
    .line 1098
    sput-object v56, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1099
    .line 1100
    new-instance v57, Lcom/squareup/okhttp/CipherSuite;

    .line 1101
    .line 1102
    const-string v72, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA"

    .line 1103
    .line 1104
    const/16 v73, 0x3a

    .line 1105
    .line 1106
    const-string v74, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA"

    .line 1107
    .line 1108
    const v75, 0xc004

    .line 1109
    .line 1110
    .line 1111
    move-object/from16 v71, v57

    .line 1112
    .line 1113
    invoke-direct/range {v71 .. v78}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1114
    .line 1115
    .line 1116
    sput-object v57, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1117
    .line 1118
    new-instance v58, Lcom/squareup/okhttp/CipherSuite;

    .line 1119
    .line 1120
    const-string v64, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA"

    .line 1121
    .line 1122
    const/16 v65, 0x3b

    .line 1123
    .line 1124
    const-string v66, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA"

    .line 1125
    .line 1126
    const v67, 0xc005

    .line 1127
    .line 1128
    .line 1129
    move-object/from16 v63, v58

    .line 1130
    .line 1131
    invoke-direct/range {v63 .. v70}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1132
    .line 1133
    .line 1134
    sput-object v58, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1135
    .line 1136
    new-instance v59, Lcom/squareup/okhttp/CipherSuite;

    .line 1137
    .line 1138
    const-string v72, "TLS_ECDHE_ECDSA_WITH_NULL_SHA"

    .line 1139
    .line 1140
    const/16 v73, 0x3c

    .line 1141
    .line 1142
    const-string v74, "TLS_ECDHE_ECDSA_WITH_NULL_SHA"

    .line 1143
    .line 1144
    const v75, 0xc006

    .line 1145
    .line 1146
    .line 1147
    move-object/from16 v71, v59

    .line 1148
    .line 1149
    invoke-direct/range {v71 .. v78}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1150
    .line 1151
    .line 1152
    sput-object v59, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_ECDSA_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1153
    .line 1154
    new-instance v60, Lcom/squareup/okhttp/CipherSuite;

    .line 1155
    .line 1156
    const-string v64, "TLS_ECDHE_ECDSA_WITH_RC4_128_SHA"

    .line 1157
    .line 1158
    const/16 v65, 0x3d

    .line 1159
    .line 1160
    const-string v66, "TLS_ECDHE_ECDSA_WITH_RC4_128_SHA"

    .line 1161
    .line 1162
    const v67, 0xc007

    .line 1163
    .line 1164
    .line 1165
    move-object/from16 v63, v60

    .line 1166
    .line 1167
    invoke-direct/range {v63 .. v70}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1168
    .line 1169
    .line 1170
    sput-object v60, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_ECDSA_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1171
    .line 1172
    new-instance v61, Lcom/squareup/okhttp/CipherSuite;

    .line 1173
    .line 1174
    const-string v72, "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA"

    .line 1175
    .line 1176
    const/16 v73, 0x3e

    .line 1177
    .line 1178
    const-string v74, "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA"

    .line 1179
    .line 1180
    const v75, 0xc008

    .line 1181
    .line 1182
    .line 1183
    move-object/from16 v71, v61

    .line 1184
    .line 1185
    invoke-direct/range {v71 .. v78}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1186
    .line 1187
    .line 1188
    sput-object v61, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1189
    .line 1190
    new-instance v71, Lcom/squareup/okhttp/CipherSuite;

    .line 1191
    .line 1192
    const-string v64, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA"

    .line 1193
    .line 1194
    const/16 v65, 0x3f

    .line 1195
    .line 1196
    const-string v66, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA"

    .line 1197
    .line 1198
    const v67, 0xc009

    .line 1199
    .line 1200
    .line 1201
    move-object/from16 v63, v71

    .line 1202
    .line 1203
    invoke-direct/range {v63 .. v70}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1204
    .line 1205
    .line 1206
    sput-object v71, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1207
    .line 1208
    new-instance v63, Lcom/squareup/okhttp/CipherSuite;

    .line 1209
    .line 1210
    const-string v73, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA"

    .line 1211
    .line 1212
    const/16 v74, 0x40

    .line 1213
    .line 1214
    const-string v75, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA"

    .line 1215
    .line 1216
    const v76, 0xc00a

    .line 1217
    .line 1218
    .line 1219
    const/16 v77, 0x118c

    .line 1220
    .line 1221
    const/16 v78, 0x7

    .line 1222
    .line 1223
    const/16 v79, 0xe

    .line 1224
    .line 1225
    move-object/from16 v72, v63

    .line 1226
    .line 1227
    invoke-direct/range {v72 .. v79}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1228
    .line 1229
    .line 1230
    sput-object v63, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1231
    .line 1232
    new-instance v64, Lcom/squareup/okhttp/CipherSuite;

    .line 1233
    .line 1234
    const-string v81, "TLS_ECDH_RSA_WITH_NULL_SHA"

    .line 1235
    .line 1236
    const/16 v82, 0x41

    .line 1237
    .line 1238
    const-string v83, "TLS_ECDH_RSA_WITH_NULL_SHA"

    .line 1239
    .line 1240
    const v84, 0xc00b

    .line 1241
    .line 1242
    .line 1243
    const/16 v85, 0x118c

    .line 1244
    .line 1245
    const/16 v86, 0x7

    .line 1246
    .line 1247
    const/16 v87, 0xe

    .line 1248
    .line 1249
    move-object/from16 v80, v64

    .line 1250
    .line 1251
    invoke-direct/range {v80 .. v87}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1252
    .line 1253
    .line 1254
    sput-object v64, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_RSA_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1255
    .line 1256
    new-instance v65, Lcom/squareup/okhttp/CipherSuite;

    .line 1257
    .line 1258
    const-string v73, "TLS_ECDH_RSA_WITH_RC4_128_SHA"

    .line 1259
    .line 1260
    const/16 v74, 0x42

    .line 1261
    .line 1262
    const-string v75, "TLS_ECDH_RSA_WITH_RC4_128_SHA"

    .line 1263
    .line 1264
    const v76, 0xc00c

    .line 1265
    .line 1266
    .line 1267
    move-object/from16 v72, v65

    .line 1268
    .line 1269
    invoke-direct/range {v72 .. v79}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1270
    .line 1271
    .line 1272
    sput-object v65, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_RSA_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1273
    .line 1274
    new-instance v66, Lcom/squareup/okhttp/CipherSuite;

    .line 1275
    .line 1276
    const-string v81, "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA"

    .line 1277
    .line 1278
    const/16 v82, 0x43

    .line 1279
    .line 1280
    const-string v83, "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA"

    .line 1281
    .line 1282
    const v84, 0xc00d

    .line 1283
    .line 1284
    .line 1285
    move-object/from16 v80, v66

    .line 1286
    .line 1287
    invoke-direct/range {v80 .. v87}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1288
    .line 1289
    .line 1290
    sput-object v66, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1291
    .line 1292
    new-instance v67, Lcom/squareup/okhttp/CipherSuite;

    .line 1293
    .line 1294
    const-string v73, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA"

    .line 1295
    .line 1296
    const/16 v74, 0x44

    .line 1297
    .line 1298
    const-string v75, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA"

    .line 1299
    .line 1300
    const v76, 0xc00e

    .line 1301
    .line 1302
    .line 1303
    move-object/from16 v72, v67

    .line 1304
    .line 1305
    invoke-direct/range {v72 .. v79}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1306
    .line 1307
    .line 1308
    sput-object v67, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_RSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1309
    .line 1310
    new-instance v68, Lcom/squareup/okhttp/CipherSuite;

    .line 1311
    .line 1312
    const-string v81, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA"

    .line 1313
    .line 1314
    const/16 v82, 0x45

    .line 1315
    .line 1316
    const-string v83, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA"

    .line 1317
    .line 1318
    const v84, 0xc00f

    .line 1319
    .line 1320
    .line 1321
    move-object/from16 v80, v68

    .line 1322
    .line 1323
    invoke-direct/range {v80 .. v87}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1324
    .line 1325
    .line 1326
    sput-object v68, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_RSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1327
    .line 1328
    new-instance v69, Lcom/squareup/okhttp/CipherSuite;

    .line 1329
    .line 1330
    const-string v73, "TLS_ECDHE_RSA_WITH_NULL_SHA"

    .line 1331
    .line 1332
    const/16 v74, 0x46

    .line 1333
    .line 1334
    const-string v75, "TLS_ECDHE_RSA_WITH_NULL_SHA"

    .line 1335
    .line 1336
    const v76, 0xc010

    .line 1337
    .line 1338
    .line 1339
    move-object/from16 v72, v69

    .line 1340
    .line 1341
    invoke-direct/range {v72 .. v79}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1342
    .line 1343
    .line 1344
    sput-object v69, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_RSA_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1345
    .line 1346
    new-instance v70, Lcom/squareup/okhttp/CipherSuite;

    .line 1347
    .line 1348
    const-string v81, "TLS_ECDHE_RSA_WITH_RC4_128_SHA"

    .line 1349
    .line 1350
    const/16 v82, 0x47

    .line 1351
    .line 1352
    const-string v83, "TLS_ECDHE_RSA_WITH_RC4_128_SHA"

    .line 1353
    .line 1354
    const v84, 0xc011

    .line 1355
    .line 1356
    .line 1357
    move-object/from16 v80, v70

    .line 1358
    .line 1359
    invoke-direct/range {v80 .. v87}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1360
    .line 1361
    .line 1362
    sput-object v70, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_RSA_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1363
    .line 1364
    new-instance v80, Lcom/squareup/okhttp/CipherSuite;

    .line 1365
    .line 1366
    const-string v73, "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA"

    .line 1367
    .line 1368
    const/16 v74, 0x48

    .line 1369
    .line 1370
    const-string v75, "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA"

    .line 1371
    .line 1372
    const v76, 0xc012

    .line 1373
    .line 1374
    .line 1375
    move-object/from16 v72, v80

    .line 1376
    .line 1377
    invoke-direct/range {v72 .. v79}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1378
    .line 1379
    .line 1380
    sput-object v80, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1381
    .line 1382
    new-instance v72, Lcom/squareup/okhttp/CipherSuite;

    .line 1383
    .line 1384
    const-string v82, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA"

    .line 1385
    .line 1386
    const/16 v83, 0x49

    .line 1387
    .line 1388
    const-string v84, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA"

    .line 1389
    .line 1390
    const v85, 0xc013

    .line 1391
    .line 1392
    .line 1393
    const/16 v86, 0x118c

    .line 1394
    .line 1395
    const/16 v87, 0x7

    .line 1396
    .line 1397
    const/16 v88, 0xe

    .line 1398
    .line 1399
    move-object/from16 v81, v72

    .line 1400
    .line 1401
    invoke-direct/range {v81 .. v88}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1402
    .line 1403
    .line 1404
    sput-object v72, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1405
    .line 1406
    new-instance v73, Lcom/squareup/okhttp/CipherSuite;

    .line 1407
    .line 1408
    const-string v90, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA"

    .line 1409
    .line 1410
    const/16 v91, 0x4a

    .line 1411
    .line 1412
    const-string v92, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA"

    .line 1413
    .line 1414
    const v93, 0xc014

    .line 1415
    .line 1416
    .line 1417
    const/16 v94, 0x118c

    .line 1418
    .line 1419
    const/16 v95, 0x7

    .line 1420
    .line 1421
    const/16 v96, 0xe

    .line 1422
    .line 1423
    move-object/from16 v89, v73

    .line 1424
    .line 1425
    invoke-direct/range {v89 .. v96}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1426
    .line 1427
    .line 1428
    sput-object v73, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1429
    .line 1430
    new-instance v74, Lcom/squareup/okhttp/CipherSuite;

    .line 1431
    .line 1432
    const-string v82, "TLS_ECDH_anon_WITH_NULL_SHA"

    .line 1433
    .line 1434
    const/16 v83, 0x4b

    .line 1435
    .line 1436
    const-string v84, "TLS_ECDH_anon_WITH_NULL_SHA"

    .line 1437
    .line 1438
    const v85, 0xc015

    .line 1439
    .line 1440
    .line 1441
    move-object/from16 v81, v74

    .line 1442
    .line 1443
    invoke-direct/range {v81 .. v88}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1444
    .line 1445
    .line 1446
    sput-object v74, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_anon_WITH_NULL_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1447
    .line 1448
    new-instance v75, Lcom/squareup/okhttp/CipherSuite;

    .line 1449
    .line 1450
    const-string v90, "TLS_ECDH_anon_WITH_RC4_128_SHA"

    .line 1451
    .line 1452
    const/16 v91, 0x4c

    .line 1453
    .line 1454
    const-string v92, "TLS_ECDH_anon_WITH_RC4_128_SHA"

    .line 1455
    .line 1456
    const v93, 0xc016

    .line 1457
    .line 1458
    .line 1459
    move-object/from16 v89, v75

    .line 1460
    .line 1461
    invoke-direct/range {v89 .. v96}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1462
    .line 1463
    .line 1464
    sput-object v75, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_anon_WITH_RC4_128_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1465
    .line 1466
    new-instance v76, Lcom/squareup/okhttp/CipherSuite;

    .line 1467
    .line 1468
    const-string v82, "TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA"

    .line 1469
    .line 1470
    const/16 v83, 0x4d

    .line 1471
    .line 1472
    const-string v84, "TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA"

    .line 1473
    .line 1474
    const v85, 0xc017

    .line 1475
    .line 1476
    .line 1477
    move-object/from16 v81, v76

    .line 1478
    .line 1479
    invoke-direct/range {v81 .. v88}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1480
    .line 1481
    .line 1482
    sput-object v76, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1483
    .line 1484
    new-instance v77, Lcom/squareup/okhttp/CipherSuite;

    .line 1485
    .line 1486
    const-string v90, "TLS_ECDH_anon_WITH_AES_128_CBC_SHA"

    .line 1487
    .line 1488
    const/16 v91, 0x4e

    .line 1489
    .line 1490
    const-string v92, "TLS_ECDH_anon_WITH_AES_128_CBC_SHA"

    .line 1491
    .line 1492
    const v93, 0xc018

    .line 1493
    .line 1494
    .line 1495
    move-object/from16 v89, v77

    .line 1496
    .line 1497
    invoke-direct/range {v89 .. v96}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1498
    .line 1499
    .line 1500
    sput-object v77, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_anon_WITH_AES_128_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1501
    .line 1502
    new-instance v78, Lcom/squareup/okhttp/CipherSuite;

    .line 1503
    .line 1504
    const-string v82, "TLS_ECDH_anon_WITH_AES_256_CBC_SHA"

    .line 1505
    .line 1506
    const/16 v83, 0x4f

    .line 1507
    .line 1508
    const-string v84, "TLS_ECDH_anon_WITH_AES_256_CBC_SHA"

    .line 1509
    .line 1510
    const v85, 0xc019

    .line 1511
    .line 1512
    .line 1513
    move-object/from16 v81, v78

    .line 1514
    .line 1515
    invoke-direct/range {v81 .. v88}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1516
    .line 1517
    .line 1518
    sput-object v78, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_anon_WITH_AES_256_CBC_SHA:Lcom/squareup/okhttp/CipherSuite;

    .line 1519
    .line 1520
    new-instance v79, Lcom/squareup/okhttp/CipherSuite;

    .line 1521
    .line 1522
    const-string v90, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"

    .line 1523
    .line 1524
    const/16 v91, 0x50

    .line 1525
    .line 1526
    const-string v92, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"

    .line 1527
    .line 1528
    const v93, 0xc023

    .line 1529
    .line 1530
    .line 1531
    const/16 v94, 0x14a9

    .line 1532
    .line 1533
    const/16 v96, 0x15

    .line 1534
    .line 1535
    move-object/from16 v89, v79

    .line 1536
    .line 1537
    invoke-direct/range {v89 .. v96}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1538
    .line 1539
    .line 1540
    sput-object v79, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 1541
    .line 1542
    new-instance v89, Lcom/squareup/okhttp/CipherSuite;

    .line 1543
    .line 1544
    const-string v82, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384"

    .line 1545
    .line 1546
    const/16 v83, 0x51

    .line 1547
    .line 1548
    const-string v84, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384"

    .line 1549
    .line 1550
    const v85, 0xc024

    .line 1551
    .line 1552
    .line 1553
    const/16 v86, 0x14a9

    .line 1554
    .line 1555
    const/16 v88, 0x15

    .line 1556
    .line 1557
    move-object/from16 v81, v89

    .line 1558
    .line 1559
    invoke-direct/range {v81 .. v88}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1560
    .line 1561
    .line 1562
    sput-object v89, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 1563
    .line 1564
    new-instance v81, Lcom/squareup/okhttp/CipherSuite;

    .line 1565
    .line 1566
    const-string v91, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256"

    .line 1567
    .line 1568
    const/16 v92, 0x52

    .line 1569
    .line 1570
    const-string v93, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256"

    .line 1571
    .line 1572
    const v94, 0xc025

    .line 1573
    .line 1574
    .line 1575
    const/16 v95, 0x14a9

    .line 1576
    .line 1577
    const/16 v96, 0x7

    .line 1578
    .line 1579
    const/16 v97, 0x15

    .line 1580
    .line 1581
    move-object/from16 v90, v81

    .line 1582
    .line 1583
    invoke-direct/range {v90 .. v97}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1584
    .line 1585
    .line 1586
    sput-object v81, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 1587
    .line 1588
    new-instance v82, Lcom/squareup/okhttp/CipherSuite;

    .line 1589
    .line 1590
    const-string v99, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384"

    .line 1591
    .line 1592
    const/16 v100, 0x53

    .line 1593
    .line 1594
    const-string v101, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384"

    .line 1595
    .line 1596
    const v102, 0xc026

    .line 1597
    .line 1598
    .line 1599
    const/16 v103, 0x14a9

    .line 1600
    .line 1601
    const/16 v104, 0x7

    .line 1602
    .line 1603
    const/16 v105, 0x15

    .line 1604
    .line 1605
    move-object/from16 v98, v82

    .line 1606
    .line 1607
    invoke-direct/range {v98 .. v105}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1608
    .line 1609
    .line 1610
    sput-object v82, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 1611
    .line 1612
    new-instance v83, Lcom/squareup/okhttp/CipherSuite;

    .line 1613
    .line 1614
    const-string v91, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256"

    .line 1615
    .line 1616
    const/16 v92, 0x54

    .line 1617
    .line 1618
    const-string v93, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256"

    .line 1619
    .line 1620
    const v94, 0xc027

    .line 1621
    .line 1622
    .line 1623
    move-object/from16 v90, v83

    .line 1624
    .line 1625
    invoke-direct/range {v90 .. v97}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1626
    .line 1627
    .line 1628
    sput-object v83, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 1629
    .line 1630
    new-instance v84, Lcom/squareup/okhttp/CipherSuite;

    .line 1631
    .line 1632
    const-string v99, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384"

    .line 1633
    .line 1634
    const/16 v100, 0x55

    .line 1635
    .line 1636
    const-string v101, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384"

    .line 1637
    .line 1638
    const v102, 0xc028

    .line 1639
    .line 1640
    .line 1641
    move-object/from16 v98, v84

    .line 1642
    .line 1643
    invoke-direct/range {v98 .. v105}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1644
    .line 1645
    .line 1646
    sput-object v84, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 1647
    .line 1648
    new-instance v85, Lcom/squareup/okhttp/CipherSuite;

    .line 1649
    .line 1650
    const-string v91, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256"

    .line 1651
    .line 1652
    const/16 v92, 0x56

    .line 1653
    .line 1654
    const-string v93, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256"

    .line 1655
    .line 1656
    const v94, 0xc029

    .line 1657
    .line 1658
    .line 1659
    move-object/from16 v90, v85

    .line 1660
    .line 1661
    invoke-direct/range {v90 .. v97}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1662
    .line 1663
    .line 1664
    sput-object v85, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 1665
    .line 1666
    new-instance v86, Lcom/squareup/okhttp/CipherSuite;

    .line 1667
    .line 1668
    const-string v99, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384"

    .line 1669
    .line 1670
    const/16 v100, 0x57

    .line 1671
    .line 1672
    const-string v101, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384"

    .line 1673
    .line 1674
    const v102, 0xc02a

    .line 1675
    .line 1676
    .line 1677
    move-object/from16 v98, v86

    .line 1678
    .line 1679
    invoke-direct/range {v98 .. v105}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1680
    .line 1681
    .line 1682
    sput-object v86, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 1683
    .line 1684
    new-instance v87, Lcom/squareup/okhttp/CipherSuite;

    .line 1685
    .line 1686
    const-string v91, "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"

    .line 1687
    .line 1688
    const/16 v92, 0x58

    .line 1689
    .line 1690
    const-string v93, "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"

    .line 1691
    .line 1692
    const v94, 0xc02b

    .line 1693
    .line 1694
    .line 1695
    const/16 v96, 0x8

    .line 1696
    .line 1697
    move-object/from16 v90, v87

    .line 1698
    .line 1699
    invoke-direct/range {v90 .. v97}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1700
    .line 1701
    .line 1702
    sput-object v87, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 1703
    .line 1704
    new-instance v88, Lcom/squareup/okhttp/CipherSuite;

    .line 1705
    .line 1706
    const-string v99, "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384"

    .line 1707
    .line 1708
    const/16 v100, 0x59

    .line 1709
    .line 1710
    const-string v101, "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384"

    .line 1711
    .line 1712
    const v102, 0xc02c

    .line 1713
    .line 1714
    .line 1715
    const/16 v104, 0x8

    .line 1716
    .line 1717
    move-object/from16 v98, v88

    .line 1718
    .line 1719
    invoke-direct/range {v98 .. v105}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1720
    .line 1721
    .line 1722
    sput-object v88, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 1723
    .line 1724
    new-instance v98, Lcom/squareup/okhttp/CipherSuite;

    .line 1725
    .line 1726
    const-string v91, "TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256"

    .line 1727
    .line 1728
    const/16 v92, 0x5a

    .line 1729
    .line 1730
    const-string v93, "TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256"

    .line 1731
    .line 1732
    const v94, 0xc02d

    .line 1733
    .line 1734
    .line 1735
    move-object/from16 v90, v98

    .line 1736
    .line 1737
    invoke-direct/range {v90 .. v97}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1738
    .line 1739
    .line 1740
    sput-object v98, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 1741
    .line 1742
    new-instance v90, Lcom/squareup/okhttp/CipherSuite;

    .line 1743
    .line 1744
    const-string v100, "TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384"

    .line 1745
    .line 1746
    const/16 v101, 0x5b

    .line 1747
    .line 1748
    const-string v102, "TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384"

    .line 1749
    .line 1750
    const v103, 0xc02e

    .line 1751
    .line 1752
    .line 1753
    const/16 v104, 0x14a9

    .line 1754
    .line 1755
    const/16 v105, 0x8

    .line 1756
    .line 1757
    const/16 v106, 0x15

    .line 1758
    .line 1759
    move-object/from16 v99, v90

    .line 1760
    .line 1761
    invoke-direct/range {v99 .. v106}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1762
    .line 1763
    .line 1764
    sput-object v90, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 1765
    .line 1766
    new-instance v91, Lcom/squareup/okhttp/CipherSuite;

    .line 1767
    .line 1768
    const-string v108, "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"

    .line 1769
    .line 1770
    const/16 v109, 0x5c

    .line 1771
    .line 1772
    const-string v110, "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"

    .line 1773
    .line 1774
    const v111, 0xc02f

    .line 1775
    .line 1776
    .line 1777
    const/16 v112, 0x14a9

    .line 1778
    .line 1779
    const/16 v113, 0x8

    .line 1780
    .line 1781
    const/16 v114, 0x15

    .line 1782
    .line 1783
    move-object/from16 v107, v91

    .line 1784
    .line 1785
    invoke-direct/range {v107 .. v114}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1786
    .line 1787
    .line 1788
    sput-object v91, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 1789
    .line 1790
    new-instance v92, Lcom/squareup/okhttp/CipherSuite;

    .line 1791
    .line 1792
    const-string v100, "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"

    .line 1793
    .line 1794
    const/16 v101, 0x5d

    .line 1795
    .line 1796
    const-string v102, "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"

    .line 1797
    .line 1798
    const v103, 0xc030

    .line 1799
    .line 1800
    .line 1801
    move-object/from16 v99, v92

    .line 1802
    .line 1803
    invoke-direct/range {v99 .. v106}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1804
    .line 1805
    .line 1806
    sput-object v92, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 1807
    .line 1808
    new-instance v93, Lcom/squareup/okhttp/CipherSuite;

    .line 1809
    .line 1810
    const-string v108, "TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256"

    .line 1811
    .line 1812
    const/16 v109, 0x5e

    .line 1813
    .line 1814
    const-string v110, "TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256"

    .line 1815
    .line 1816
    const v111, 0xc031

    .line 1817
    .line 1818
    .line 1819
    move-object/from16 v107, v93

    .line 1820
    .line 1821
    invoke-direct/range {v107 .. v114}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1822
    .line 1823
    .line 1824
    sput-object v93, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256:Lcom/squareup/okhttp/CipherSuite;

    .line 1825
    .line 1826
    new-instance v94, Lcom/squareup/okhttp/CipherSuite;

    .line 1827
    .line 1828
    const-string v100, "TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384"

    .line 1829
    .line 1830
    const/16 v101, 0x5f

    .line 1831
    .line 1832
    const-string v102, "TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384"

    .line 1833
    .line 1834
    const v103, 0xc032

    .line 1835
    .line 1836
    .line 1837
    move-object/from16 v99, v94

    .line 1838
    .line 1839
    invoke-direct/range {v99 .. v106}, Lcom/squareup/okhttp/CipherSuite;-><init>(Ljava/lang/String;ILjava/lang/String;IIII)V

    .line 1840
    .line 1841
    .line 1842
    sput-object v94, Lcom/squareup/okhttp/CipherSuite;->TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384:Lcom/squareup/okhttp/CipherSuite;

    .line 1843
    .line 1844
    move-object/from16 v95, v15

    .line 1845
    .line 1846
    const/16 v15, 0x60

    .line 1847
    .line 1848
    new-array v15, v15, [Lcom/squareup/okhttp/CipherSuite;

    .line 1849
    .line 1850
    const/16 v96, 0x0

    .line 1851
    .line 1852
    aput-object v8, v15, v96

    .line 1853
    .line 1854
    const/4 v8, 0x1

    .line 1855
    aput-object v0, v15, v8

    .line 1856
    .line 1857
    const/4 v0, 0x2

    .line 1858
    aput-object v1, v15, v0

    .line 1859
    .line 1860
    const/4 v0, 0x3

    .line 1861
    aput-object v2, v15, v0

    .line 1862
    .line 1863
    const/4 v0, 0x4

    .line 1864
    aput-object v3, v15, v0

    .line 1865
    .line 1866
    const/4 v0, 0x5

    .line 1867
    aput-object v4, v15, v0

    .line 1868
    .line 1869
    const/4 v0, 0x6

    .line 1870
    aput-object v5, v15, v0

    .line 1871
    .line 1872
    const/4 v0, 0x7

    .line 1873
    aput-object v6, v15, v0

    .line 1874
    .line 1875
    const/16 v0, 0x8

    .line 1876
    .line 1877
    aput-object v7, v15, v0

    .line 1878
    .line 1879
    const/16 v0, 0x9

    .line 1880
    .line 1881
    aput-object v17, v15, v0

    .line 1882
    .line 1883
    const/16 v0, 0xa

    .line 1884
    .line 1885
    aput-object v9, v15, v0

    .line 1886
    .line 1887
    const/16 v0, 0xb

    .line 1888
    .line 1889
    aput-object v10, v15, v0

    .line 1890
    .line 1891
    const/16 v0, 0xc

    .line 1892
    .line 1893
    aput-object v11, v15, v0

    .line 1894
    .line 1895
    const/16 v0, 0xd

    .line 1896
    .line 1897
    aput-object v12, v15, v0

    .line 1898
    .line 1899
    const/16 v0, 0xe

    .line 1900
    .line 1901
    aput-object v13, v15, v0

    .line 1902
    .line 1903
    const/16 v0, 0xf

    .line 1904
    .line 1905
    aput-object v14, v15, v0

    .line 1906
    .line 1907
    const/16 v0, 0x10

    .line 1908
    .line 1909
    aput-object v95, v15, v0

    .line 1910
    .line 1911
    const/16 v0, 0x11

    .line 1912
    .line 1913
    aput-object v16, v15, v0

    .line 1914
    .line 1915
    const/16 v0, 0x12

    .line 1916
    .line 1917
    aput-object v26, v15, v0

    .line 1918
    .line 1919
    const/16 v0, 0x13

    .line 1920
    .line 1921
    aput-object v18, v15, v0

    .line 1922
    .line 1923
    const/16 v0, 0x14

    .line 1924
    .line 1925
    aput-object v19, v15, v0

    .line 1926
    .line 1927
    const/16 v0, 0x15

    .line 1928
    .line 1929
    aput-object v20, v15, v0

    .line 1930
    .line 1931
    const/16 v0, 0x16

    .line 1932
    .line 1933
    aput-object v21, v15, v0

    .line 1934
    .line 1935
    const/16 v0, 0x17

    .line 1936
    .line 1937
    aput-object v22, v15, v0

    .line 1938
    .line 1939
    const/16 v0, 0x18

    .line 1940
    .line 1941
    aput-object v23, v15, v0

    .line 1942
    .line 1943
    const/16 v0, 0x19

    .line 1944
    .line 1945
    aput-object v24, v15, v0

    .line 1946
    .line 1947
    const/16 v0, 0x1a

    .line 1948
    .line 1949
    aput-object v25, v15, v0

    .line 1950
    .line 1951
    const/16 v0, 0x1b

    .line 1952
    .line 1953
    aput-object v35, v15, v0

    .line 1954
    .line 1955
    const/16 v0, 0x1c

    .line 1956
    .line 1957
    aput-object v27, v15, v0

    .line 1958
    .line 1959
    const/16 v0, 0x1d

    .line 1960
    .line 1961
    aput-object v28, v15, v0

    .line 1962
    .line 1963
    const/16 v0, 0x1e

    .line 1964
    .line 1965
    aput-object v29, v15, v0

    .line 1966
    .line 1967
    const/16 v0, 0x1f

    .line 1968
    .line 1969
    aput-object v30, v15, v0

    .line 1970
    .line 1971
    const/16 v0, 0x20

    .line 1972
    .line 1973
    aput-object v31, v15, v0

    .line 1974
    .line 1975
    const/16 v0, 0x21

    .line 1976
    .line 1977
    aput-object v32, v15, v0

    .line 1978
    .line 1979
    const/16 v0, 0x22

    .line 1980
    .line 1981
    aput-object v33, v15, v0

    .line 1982
    .line 1983
    const/16 v0, 0x23

    .line 1984
    .line 1985
    aput-object v34, v15, v0

    .line 1986
    .line 1987
    const/16 v0, 0x24

    .line 1988
    .line 1989
    aput-object v44, v15, v0

    .line 1990
    .line 1991
    const/16 v0, 0x25

    .line 1992
    .line 1993
    aput-object v36, v15, v0

    .line 1994
    .line 1995
    const/16 v0, 0x26

    .line 1996
    .line 1997
    aput-object v37, v15, v0

    .line 1998
    .line 1999
    const/16 v0, 0x27

    .line 2000
    .line 2001
    aput-object v38, v15, v0

    .line 2002
    .line 2003
    const/16 v0, 0x28

    .line 2004
    .line 2005
    aput-object v39, v15, v0

    .line 2006
    .line 2007
    const/16 v0, 0x29

    .line 2008
    .line 2009
    aput-object v40, v15, v0

    .line 2010
    .line 2011
    const/16 v0, 0x2a

    .line 2012
    .line 2013
    aput-object v41, v15, v0

    .line 2014
    .line 2015
    const/16 v0, 0x2b

    .line 2016
    .line 2017
    aput-object v42, v15, v0

    .line 2018
    .line 2019
    const/16 v0, 0x2c

    .line 2020
    .line 2021
    aput-object v43, v15, v0

    .line 2022
    .line 2023
    const/16 v0, 0x2d

    .line 2024
    .line 2025
    aput-object v53, v15, v0

    .line 2026
    .line 2027
    const/16 v0, 0x2e

    .line 2028
    .line 2029
    aput-object v45, v15, v0

    .line 2030
    .line 2031
    const/16 v0, 0x2f

    .line 2032
    .line 2033
    aput-object v46, v15, v0

    .line 2034
    .line 2035
    const/16 v0, 0x30

    .line 2036
    .line 2037
    aput-object v47, v15, v0

    .line 2038
    .line 2039
    const/16 v0, 0x31

    .line 2040
    .line 2041
    aput-object v48, v15, v0

    .line 2042
    .line 2043
    const/16 v0, 0x32

    .line 2044
    .line 2045
    aput-object v49, v15, v0

    .line 2046
    .line 2047
    const/16 v0, 0x33

    .line 2048
    .line 2049
    aput-object v50, v15, v0

    .line 2050
    .line 2051
    const/16 v0, 0x34

    .line 2052
    .line 2053
    aput-object v51, v15, v0

    .line 2054
    .line 2055
    const/16 v0, 0x35

    .line 2056
    .line 2057
    aput-object v52, v15, v0

    .line 2058
    .line 2059
    const/16 v0, 0x36

    .line 2060
    .line 2061
    aput-object v62, v15, v0

    .line 2062
    .line 2063
    const/16 v0, 0x37

    .line 2064
    .line 2065
    aput-object v54, v15, v0

    .line 2066
    .line 2067
    const/16 v0, 0x38

    .line 2068
    .line 2069
    aput-object v55, v15, v0

    .line 2070
    .line 2071
    const/16 v0, 0x39

    .line 2072
    .line 2073
    aput-object v56, v15, v0

    .line 2074
    .line 2075
    const/16 v0, 0x3a

    .line 2076
    .line 2077
    aput-object v57, v15, v0

    .line 2078
    .line 2079
    const/16 v0, 0x3b

    .line 2080
    .line 2081
    aput-object v58, v15, v0

    .line 2082
    .line 2083
    const/16 v0, 0x3c

    .line 2084
    .line 2085
    aput-object v59, v15, v0

    .line 2086
    .line 2087
    const/16 v0, 0x3d

    .line 2088
    .line 2089
    aput-object v60, v15, v0

    .line 2090
    .line 2091
    const/16 v0, 0x3e

    .line 2092
    .line 2093
    aput-object v61, v15, v0

    .line 2094
    .line 2095
    const/16 v0, 0x3f

    .line 2096
    .line 2097
    aput-object v71, v15, v0

    .line 2098
    .line 2099
    const/16 v0, 0x40

    .line 2100
    .line 2101
    aput-object v63, v15, v0

    .line 2102
    .line 2103
    const/16 v0, 0x41

    .line 2104
    .line 2105
    aput-object v64, v15, v0

    .line 2106
    .line 2107
    const/16 v0, 0x42

    .line 2108
    .line 2109
    aput-object v65, v15, v0

    .line 2110
    .line 2111
    const/16 v0, 0x43

    .line 2112
    .line 2113
    aput-object v66, v15, v0

    .line 2114
    .line 2115
    const/16 v0, 0x44

    .line 2116
    .line 2117
    aput-object v67, v15, v0

    .line 2118
    .line 2119
    const/16 v0, 0x45

    .line 2120
    .line 2121
    aput-object v68, v15, v0

    .line 2122
    .line 2123
    const/16 v0, 0x46

    .line 2124
    .line 2125
    aput-object v69, v15, v0

    .line 2126
    .line 2127
    const/16 v0, 0x47

    .line 2128
    .line 2129
    aput-object v70, v15, v0

    .line 2130
    .line 2131
    const/16 v0, 0x48

    .line 2132
    .line 2133
    aput-object v80, v15, v0

    .line 2134
    .line 2135
    const/16 v0, 0x49

    .line 2136
    .line 2137
    aput-object v72, v15, v0

    .line 2138
    .line 2139
    const/16 v0, 0x4a

    .line 2140
    .line 2141
    aput-object v73, v15, v0

    .line 2142
    .line 2143
    const/16 v0, 0x4b

    .line 2144
    .line 2145
    aput-object v74, v15, v0

    .line 2146
    .line 2147
    const/16 v0, 0x4c

    .line 2148
    .line 2149
    aput-object v75, v15, v0

    .line 2150
    .line 2151
    const/16 v0, 0x4d

    .line 2152
    .line 2153
    aput-object v76, v15, v0

    .line 2154
    .line 2155
    const/16 v0, 0x4e

    .line 2156
    .line 2157
    aput-object v77, v15, v0

    .line 2158
    .line 2159
    const/16 v0, 0x4f

    .line 2160
    .line 2161
    aput-object v78, v15, v0

    .line 2162
    .line 2163
    const/16 v0, 0x50

    .line 2164
    .line 2165
    aput-object v79, v15, v0

    .line 2166
    .line 2167
    const/16 v0, 0x51

    .line 2168
    .line 2169
    aput-object v89, v15, v0

    .line 2170
    .line 2171
    const/16 v0, 0x52

    .line 2172
    .line 2173
    aput-object v81, v15, v0

    .line 2174
    .line 2175
    const/16 v0, 0x53

    .line 2176
    .line 2177
    aput-object v82, v15, v0

    .line 2178
    .line 2179
    const/16 v0, 0x54

    .line 2180
    .line 2181
    aput-object v83, v15, v0

    .line 2182
    .line 2183
    const/16 v0, 0x55

    .line 2184
    .line 2185
    aput-object v84, v15, v0

    .line 2186
    .line 2187
    const/16 v0, 0x56

    .line 2188
    .line 2189
    aput-object v85, v15, v0

    .line 2190
    .line 2191
    const/16 v0, 0x57

    .line 2192
    .line 2193
    aput-object v86, v15, v0

    .line 2194
    .line 2195
    const/16 v0, 0x58

    .line 2196
    .line 2197
    aput-object v87, v15, v0

    .line 2198
    .line 2199
    const/16 v0, 0x59

    .line 2200
    .line 2201
    aput-object v88, v15, v0

    .line 2202
    .line 2203
    const/16 v0, 0x5a

    .line 2204
    .line 2205
    aput-object v98, v15, v0

    .line 2206
    .line 2207
    const/16 v0, 0x5b

    .line 2208
    .line 2209
    aput-object v90, v15, v0

    .line 2210
    .line 2211
    const/16 v0, 0x5c

    .line 2212
    .line 2213
    aput-object v91, v15, v0

    .line 2214
    .line 2215
    const/16 v0, 0x5d

    .line 2216
    .line 2217
    aput-object v92, v15, v0

    .line 2218
    .line 2219
    const/16 v0, 0x5e

    .line 2220
    .line 2221
    aput-object v93, v15, v0

    .line 2222
    .line 2223
    const/16 v0, 0x5f

    .line 2224
    .line 2225
    aput-object v94, v15, v0

    .line 2226
    .line 2227
    sput-object v15, Lcom/squareup/okhttp/CipherSuite;->$VALUES:[Lcom/squareup/okhttp/CipherSuite;

    .line 2228
    .line 2229
    return-void
    .line 2230
    .line 2231
    .line 2232
    .line 2233
    .line 2234
    .line 2235
    .line 2236
    .line 2237
    .line 2238
    .line 2239
    .line 2240
    .line 2241
    .line 2242
    .line 2243
    .line 2244
    .line 2245
    .line 2246
    .line 2247
    .line 2248
    .line 2249
    .line 2250
    .line 2251
    .line 2252
    .line 2253
    .line 2254
    .line 2255
    .line 2256
    .line 2257
    .line 2258
    .line 2259
    .line 2260
    .line 2261
    .line 2262
    .line 2263
    .line 2264
    .line 2265
    .line 2266
    .line 2267
    .line 2268
    .line 2269
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIII)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lcom/squareup/okhttp/CipherSuite;->o0:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
.end method

.method public static forJavaName(Ljava/lang/String;)Lcom/squareup/okhttp/CipherSuite;
    .locals 2

    .line 1
    const-string v0, "SSL_"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v1, "TLS_"

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const/4 v1, 0x4

    .line 20
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p0

    .line 24
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    invoke-static {p0}, Lcom/squareup/okhttp/CipherSuite;->valueOf(Ljava/lang/String;)Lcom/squareup/okhttp/CipherSuite;

    .line 32
    .line 33
    .line 34
    move-result-object p0

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    invoke-static {p0}, Lcom/squareup/okhttp/CipherSuite;->valueOf(Ljava/lang/String;)Lcom/squareup/okhttp/CipherSuite;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    :goto_0
    return-object p0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/okhttp/CipherSuite;
    .locals 1

    .line 1
    const-class v0, Lcom/squareup/okhttp/CipherSuite;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/squareup/okhttp/CipherSuite;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lcom/squareup/okhttp/CipherSuite;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/okhttp/CipherSuite;->$VALUES:[Lcom/squareup/okhttp/CipherSuite;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/squareup/okhttp/CipherSuite;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/squareup/okhttp/CipherSuite;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

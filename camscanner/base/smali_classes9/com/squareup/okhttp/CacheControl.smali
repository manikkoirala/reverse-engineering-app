.class public final Lcom/squareup/okhttp/CacheControl;
.super Ljava/lang/Object;
.source "CacheControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/okhttp/CacheControl$Builder;
    }
.end annotation


# static fields
.field public static final OO0o〇〇:Lcom/squareup/okhttp/CacheControl;

.field public static final Oooo8o0〇:Lcom/squareup/okhttp/CacheControl;


# instance fields
.field private final O8:I

.field private final OO0o〇〇〇〇0:Z

.field private final Oo08:Z

.field private final oO80:I

.field private final o〇0:Z

.field private final 〇080:Z

.field private final 〇80〇808〇O:I

.field private final 〇8o8o〇:Z

.field 〇O8o08O:Ljava/lang/String;

.field private final 〇o00〇〇Oo:Z

.field private final 〇o〇:I

.field private final 〇〇888:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/squareup/okhttp/CacheControl$Builder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/squareup/okhttp/CacheControl$Builder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/squareup/okhttp/CacheControl$Builder;->〇o〇()Lcom/squareup/okhttp/CacheControl$Builder;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/squareup/okhttp/CacheControl$Builder;->〇080()Lcom/squareup/okhttp/CacheControl;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    sput-object v0, Lcom/squareup/okhttp/CacheControl;->OO0o〇〇:Lcom/squareup/okhttp/CacheControl;

    .line 15
    .line 16
    new-instance v0, Lcom/squareup/okhttp/CacheControl$Builder;

    .line 17
    .line 18
    invoke-direct {v0}, Lcom/squareup/okhttp/CacheControl$Builder;-><init>()V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/squareup/okhttp/CacheControl$Builder;->O8()Lcom/squareup/okhttp/CacheControl$Builder;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const v1, 0x7fffffff

    .line 26
    .line 27
    .line 28
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 29
    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/CacheControl$Builder;->〇o00〇〇Oo(ILjava/util/concurrent/TimeUnit;)Lcom/squareup/okhttp/CacheControl$Builder;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/squareup/okhttp/CacheControl$Builder;->〇080()Lcom/squareup/okhttp/CacheControl;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    sput-object v0, Lcom/squareup/okhttp/CacheControl;->Oooo8o0〇:Lcom/squareup/okhttp/CacheControl;

    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method private constructor <init>(Lcom/squareup/okhttp/CacheControl$Builder;)V
    .locals 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iget-boolean v0, p1, Lcom/squareup/okhttp/CacheControl$Builder;->〇080:Z

    iput-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->〇080:Z

    .line 17
    iget-boolean v0, p1, Lcom/squareup/okhttp/CacheControl$Builder;->〇o00〇〇Oo:Z

    iput-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->〇o00〇〇Oo:Z

    .line 18
    iget v0, p1, Lcom/squareup/okhttp/CacheControl$Builder;->〇o〇:I

    iput v0, p0, Lcom/squareup/okhttp/CacheControl;->〇o〇:I

    const/4 v0, -0x1

    .line 19
    iput v0, p0, Lcom/squareup/okhttp/CacheControl;->O8:I

    const/4 v0, 0x0

    .line 20
    iput-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->Oo08:Z

    .line 21
    iput-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->o〇0:Z

    .line 22
    iput-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->〇〇888:Z

    .line 23
    iget v0, p1, Lcom/squareup/okhttp/CacheControl$Builder;->O8:I

    iput v0, p0, Lcom/squareup/okhttp/CacheControl;->oO80:I

    .line 24
    iget v0, p1, Lcom/squareup/okhttp/CacheControl$Builder;->Oo08:I

    iput v0, p0, Lcom/squareup/okhttp/CacheControl;->〇80〇808〇O:I

    .line 25
    iget-boolean v0, p1, Lcom/squareup/okhttp/CacheControl$Builder;->o〇0:Z

    iput-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->OO0o〇〇〇〇0:Z

    .line 26
    iget-boolean p1, p1, Lcom/squareup/okhttp/CacheControl$Builder;->〇〇888:Z

    iput-boolean p1, p0, Lcom/squareup/okhttp/CacheControl;->〇8o8o〇:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/CacheControl$Builder;Lcom/squareup/okhttp/CacheControl$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/CacheControl;-><init>(Lcom/squareup/okhttp/CacheControl$Builder;)V

    return-void
.end method

.method private constructor <init>(ZZIIZZZIIZZLjava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-boolean p1, p0, Lcom/squareup/okhttp/CacheControl;->〇080:Z

    .line 4
    iput-boolean p2, p0, Lcom/squareup/okhttp/CacheControl;->〇o00〇〇Oo:Z

    .line 5
    iput p3, p0, Lcom/squareup/okhttp/CacheControl;->〇o〇:I

    .line 6
    iput p4, p0, Lcom/squareup/okhttp/CacheControl;->O8:I

    .line 7
    iput-boolean p5, p0, Lcom/squareup/okhttp/CacheControl;->Oo08:Z

    .line 8
    iput-boolean p6, p0, Lcom/squareup/okhttp/CacheControl;->o〇0:Z

    .line 9
    iput-boolean p7, p0, Lcom/squareup/okhttp/CacheControl;->〇〇888:Z

    .line 10
    iput p8, p0, Lcom/squareup/okhttp/CacheControl;->oO80:I

    .line 11
    iput p9, p0, Lcom/squareup/okhttp/CacheControl;->〇80〇808〇O:I

    .line 12
    iput-boolean p10, p0, Lcom/squareup/okhttp/CacheControl;->OO0o〇〇〇〇0:Z

    .line 13
    iput-boolean p11, p0, Lcom/squareup/okhttp/CacheControl;->〇8o8o〇:Z

    .line 14
    iput-object p12, p0, Lcom/squareup/okhttp/CacheControl;->〇O8o08O:Ljava/lang/String;

    return-void
.end method

.method private 〇080()Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-boolean v1, p0, Lcom/squareup/okhttp/CacheControl;->〇080:Z

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    const-string v1, "no-cache, "

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-boolean v1, p0, Lcom/squareup/okhttp/CacheControl;->〇o00〇〇Oo:Z

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    const-string v1, "no-store, "

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    :cond_1
    iget v1, p0, Lcom/squareup/okhttp/CacheControl;->〇o〇:I

    .line 25
    .line 26
    const-string v2, ", "

    .line 27
    .line 28
    const/4 v3, -0x1

    .line 29
    if-eq v1, v3, :cond_2

    .line 30
    .line 31
    const-string v1, "max-age="

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    iget v1, p0, Lcom/squareup/okhttp/CacheControl;->〇o〇:I

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    :cond_2
    iget v1, p0, Lcom/squareup/okhttp/CacheControl;->O8:I

    .line 45
    .line 46
    if-eq v1, v3, :cond_3

    .line 47
    .line 48
    const-string v1, "s-maxage="

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    iget v1, p0, Lcom/squareup/okhttp/CacheControl;->O8:I

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    :cond_3
    iget-boolean v1, p0, Lcom/squareup/okhttp/CacheControl;->Oo08:Z

    .line 62
    .line 63
    if-eqz v1, :cond_4

    .line 64
    .line 65
    const-string v1, "private, "

    .line 66
    .line 67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    :cond_4
    iget-boolean v1, p0, Lcom/squareup/okhttp/CacheControl;->o〇0:Z

    .line 71
    .line 72
    if-eqz v1, :cond_5

    .line 73
    .line 74
    const-string v1, "public, "

    .line 75
    .line 76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    :cond_5
    iget-boolean v1, p0, Lcom/squareup/okhttp/CacheControl;->〇〇888:Z

    .line 80
    .line 81
    if-eqz v1, :cond_6

    .line 82
    .line 83
    const-string v1, "must-revalidate, "

    .line 84
    .line 85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    :cond_6
    iget v1, p0, Lcom/squareup/okhttp/CacheControl;->oO80:I

    .line 89
    .line 90
    if-eq v1, v3, :cond_7

    .line 91
    .line 92
    const-string v1, "max-stale="

    .line 93
    .line 94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    iget v1, p0, Lcom/squareup/okhttp/CacheControl;->oO80:I

    .line 98
    .line 99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    :cond_7
    iget v1, p0, Lcom/squareup/okhttp/CacheControl;->〇80〇808〇O:I

    .line 106
    .line 107
    if-eq v1, v3, :cond_8

    .line 108
    .line 109
    const-string v1, "min-fresh="

    .line 110
    .line 111
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    iget v1, p0, Lcom/squareup/okhttp/CacheControl;->〇80〇808〇O:I

    .line 115
    .line 116
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    :cond_8
    iget-boolean v1, p0, Lcom/squareup/okhttp/CacheControl;->OO0o〇〇〇〇0:Z

    .line 123
    .line 124
    if-eqz v1, :cond_9

    .line 125
    .line 126
    const-string v1, "only-if-cached, "

    .line 127
    .line 128
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    :cond_9
    iget-boolean v1, p0, Lcom/squareup/okhttp/CacheControl;->〇8o8o〇:Z

    .line 132
    .line 133
    if-eqz v1, :cond_a

    .line 134
    .line 135
    const-string v1, "no-transform, "

    .line 136
    .line 137
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    :cond_a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 141
    .line 142
    .line 143
    move-result v1

    .line 144
    if-nez v1, :cond_b

    .line 145
    .line 146
    const-string v0, ""

    .line 147
    .line 148
    return-object v0

    .line 149
    :cond_b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 150
    .line 151
    .line 152
    move-result v1

    .line 153
    add-int/lit8 v1, v1, -0x2

    .line 154
    .line 155
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 156
    .line 157
    .line 158
    move-result v2

    .line 159
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v0

    .line 166
    return-object v0
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method public static 〇8o8o〇(Lcom/squareup/okhttp/Headers;)Lcom/squareup/okhttp/CacheControl;
    .locals 21

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/okhttp/Headers;->〇〇888()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v6, 0x0

    .line 8
    const/4 v7, 0x1

    .line 9
    const/4 v8, 0x0

    .line 10
    const/4 v9, 0x0

    .line 11
    const/4 v10, 0x0

    .line 12
    const/4 v11, -0x1

    .line 13
    const/4 v12, -0x1

    .line 14
    const/4 v13, 0x0

    .line 15
    const/4 v14, 0x0

    .line 16
    const/4 v15, 0x0

    .line 17
    const/16 v16, -0x1

    .line 18
    .line 19
    const/16 v17, -0x1

    .line 20
    .line 21
    const/16 v18, 0x0

    .line 22
    .line 23
    const/16 v19, 0x0

    .line 24
    .line 25
    :goto_0
    if-ge v6, v1, :cond_11

    .line 26
    .line 27
    invoke-virtual {v0, v6}, Lcom/squareup/okhttp/Headers;->O8(I)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-virtual {v0, v6}, Lcom/squareup/okhttp/Headers;->oO80(I)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v5

    .line 35
    const-string v3, "Cache-Control"

    .line 36
    .line 37
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    if-eqz v3, :cond_1

    .line 42
    .line 43
    if-eqz v8, :cond_0

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_0
    move-object v8, v5

    .line 47
    goto :goto_2

    .line 48
    :cond_1
    const-string v3, "Pragma"

    .line 49
    .line 50
    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    if-eqz v2, :cond_10

    .line 55
    .line 56
    :goto_1
    const/4 v7, 0x0

    .line 57
    :goto_2
    const/4 v2, 0x0

    .line 58
    :goto_3
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 59
    .line 60
    .line 61
    move-result v3

    .line 62
    if-ge v2, v3, :cond_10

    .line 63
    .line 64
    const-string v3, "=,;"

    .line 65
    .line 66
    invoke-static {v5, v2, v3}, Lcom/squareup/okhttp/internal/http/HeaderParser;->〇o00〇〇Oo(Ljava/lang/String;ILjava/lang/String;)I

    .line 67
    .line 68
    .line 69
    move-result v3

    .line 70
    invoke-virtual {v5, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v2

    .line 74
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 79
    .line 80
    .line 81
    move-result v4

    .line 82
    if-eq v3, v4, :cond_4

    .line 83
    .line 84
    invoke-virtual {v5, v3}, Ljava/lang/String;->charAt(I)C

    .line 85
    .line 86
    .line 87
    move-result v4

    .line 88
    const/16 v0, 0x2c

    .line 89
    .line 90
    if-eq v4, v0, :cond_4

    .line 91
    .line 92
    invoke-virtual {v5, v3}, Ljava/lang/String;->charAt(I)C

    .line 93
    .line 94
    .line 95
    move-result v0

    .line 96
    const/16 v4, 0x3b

    .line 97
    .line 98
    if-ne v0, v4, :cond_2

    .line 99
    .line 100
    goto :goto_4

    .line 101
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 102
    .line 103
    invoke-static {v5, v3}, Lcom/squareup/okhttp/internal/http/HeaderParser;->〇o〇(Ljava/lang/String;I)I

    .line 104
    .line 105
    .line 106
    move-result v0

    .line 107
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 108
    .line 109
    .line 110
    move-result v3

    .line 111
    if-ge v0, v3, :cond_3

    .line 112
    .line 113
    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    .line 114
    .line 115
    .line 116
    move-result v3

    .line 117
    const/16 v4, 0x22

    .line 118
    .line 119
    if-ne v3, v4, :cond_3

    .line 120
    .line 121
    add-int/lit8 v0, v0, 0x1

    .line 122
    .line 123
    const-string v3, "\""

    .line 124
    .line 125
    invoke-static {v5, v0, v3}, Lcom/squareup/okhttp/internal/http/HeaderParser;->〇o00〇〇Oo(Ljava/lang/String;ILjava/lang/String;)I

    .line 126
    .line 127
    .line 128
    move-result v3

    .line 129
    invoke-virtual {v5, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    const/4 v4, 0x1

    .line 134
    add-int/2addr v3, v4

    .line 135
    goto :goto_5

    .line 136
    :cond_3
    const/4 v4, 0x1

    .line 137
    const-string v3, ",;"

    .line 138
    .line 139
    invoke-static {v5, v0, v3}, Lcom/squareup/okhttp/internal/http/HeaderParser;->〇o00〇〇Oo(Ljava/lang/String;ILjava/lang/String;)I

    .line 140
    .line 141
    .line 142
    move-result v3

    .line 143
    invoke-virtual {v5, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 144
    .line 145
    .line 146
    move-result-object v0

    .line 147
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object v0

    .line 151
    goto :goto_5

    .line 152
    :cond_4
    :goto_4
    const/4 v4, 0x1

    .line 153
    add-int/lit8 v3, v3, 0x1

    .line 154
    .line 155
    const/4 v0, 0x0

    .line 156
    :goto_5
    const-string v4, "no-cache"

    .line 157
    .line 158
    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 159
    .line 160
    .line 161
    move-result v4

    .line 162
    if-eqz v4, :cond_5

    .line 163
    .line 164
    const/4 v4, -0x1

    .line 165
    const/4 v9, 0x1

    .line 166
    goto/16 :goto_6

    .line 167
    .line 168
    :cond_5
    const-string v4, "no-store"

    .line 169
    .line 170
    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 171
    .line 172
    .line 173
    move-result v4

    .line 174
    if-eqz v4, :cond_6

    .line 175
    .line 176
    const/4 v4, -0x1

    .line 177
    const/4 v10, 0x1

    .line 178
    goto/16 :goto_6

    .line 179
    .line 180
    :cond_6
    const-string v4, "max-age"

    .line 181
    .line 182
    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 183
    .line 184
    .line 185
    move-result v4

    .line 186
    if-eqz v4, :cond_7

    .line 187
    .line 188
    const/4 v4, -0x1

    .line 189
    invoke-static {v0, v4}, Lcom/squareup/okhttp/internal/http/HeaderParser;->〇080(Ljava/lang/String;I)I

    .line 190
    .line 191
    .line 192
    move-result v11

    .line 193
    goto :goto_6

    .line 194
    :cond_7
    const-string v4, "s-maxage"

    .line 195
    .line 196
    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 197
    .line 198
    .line 199
    move-result v4

    .line 200
    if-eqz v4, :cond_8

    .line 201
    .line 202
    const/4 v4, -0x1

    .line 203
    invoke-static {v0, v4}, Lcom/squareup/okhttp/internal/http/HeaderParser;->〇080(Ljava/lang/String;I)I

    .line 204
    .line 205
    .line 206
    move-result v12

    .line 207
    goto :goto_6

    .line 208
    :cond_8
    const-string v4, "private"

    .line 209
    .line 210
    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 211
    .line 212
    .line 213
    move-result v4

    .line 214
    if-eqz v4, :cond_9

    .line 215
    .line 216
    const/4 v4, -0x1

    .line 217
    const/4 v13, 0x1

    .line 218
    goto :goto_6

    .line 219
    :cond_9
    const-string v4, "public"

    .line 220
    .line 221
    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 222
    .line 223
    .line 224
    move-result v4

    .line 225
    if-eqz v4, :cond_a

    .line 226
    .line 227
    const/4 v4, -0x1

    .line 228
    const/4 v14, 0x1

    .line 229
    goto :goto_6

    .line 230
    :cond_a
    const-string v4, "must-revalidate"

    .line 231
    .line 232
    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 233
    .line 234
    .line 235
    move-result v4

    .line 236
    if-eqz v4, :cond_b

    .line 237
    .line 238
    const/4 v4, -0x1

    .line 239
    const/4 v15, 0x1

    .line 240
    goto :goto_6

    .line 241
    :cond_b
    const-string v4, "max-stale"

    .line 242
    .line 243
    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 244
    .line 245
    .line 246
    move-result v4

    .line 247
    if-eqz v4, :cond_c

    .line 248
    .line 249
    const v2, 0x7fffffff

    .line 250
    .line 251
    .line 252
    invoke-static {v0, v2}, Lcom/squareup/okhttp/internal/http/HeaderParser;->〇080(Ljava/lang/String;I)I

    .line 253
    .line 254
    .line 255
    move-result v16

    .line 256
    const/4 v4, -0x1

    .line 257
    goto :goto_6

    .line 258
    :cond_c
    const-string v4, "min-fresh"

    .line 259
    .line 260
    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 261
    .line 262
    .line 263
    move-result v4

    .line 264
    if-eqz v4, :cond_d

    .line 265
    .line 266
    const/4 v4, -0x1

    .line 267
    invoke-static {v0, v4}, Lcom/squareup/okhttp/internal/http/HeaderParser;->〇080(Ljava/lang/String;I)I

    .line 268
    .line 269
    .line 270
    move-result v17

    .line 271
    goto :goto_6

    .line 272
    :cond_d
    const/4 v4, -0x1

    .line 273
    const-string v0, "only-if-cached"

    .line 274
    .line 275
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 276
    .line 277
    .line 278
    move-result v0

    .line 279
    if-eqz v0, :cond_e

    .line 280
    .line 281
    const/16 v18, 0x1

    .line 282
    .line 283
    goto :goto_6

    .line 284
    :cond_e
    const-string v0, "no-transform"

    .line 285
    .line 286
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 287
    .line 288
    .line 289
    move-result v0

    .line 290
    if-eqz v0, :cond_f

    .line 291
    .line 292
    const/16 v19, 0x1

    .line 293
    .line 294
    :cond_f
    :goto_6
    move-object/from16 v0, p0

    .line 295
    .line 296
    move v2, v3

    .line 297
    goto/16 :goto_3

    .line 298
    .line 299
    :cond_10
    const/4 v4, -0x1

    .line 300
    add-int/lit8 v6, v6, 0x1

    .line 301
    .line 302
    move-object/from16 v0, p0

    .line 303
    .line 304
    goto/16 :goto_0

    .line 305
    .line 306
    :cond_11
    if-nez v7, :cond_12

    .line 307
    .line 308
    const/16 v20, 0x0

    .line 309
    .line 310
    goto :goto_7

    .line 311
    :cond_12
    move-object/from16 v20, v8

    .line 312
    .line 313
    :goto_7
    new-instance v0, Lcom/squareup/okhttp/CacheControl;

    .line 314
    .line 315
    move-object v8, v0

    .line 316
    invoke-direct/range {v8 .. v20}, Lcom/squareup/okhttp/CacheControl;-><init>(ZZIIZZZIIZZLjava/lang/String;)V

    .line 317
    .line 318
    .line 319
    return-object v0
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method


# virtual methods
.method public O8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/squareup/okhttp/CacheControl;->〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public OO0o〇〇〇〇0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->OO0o〇〇〇〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public Oo08()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/squareup/okhttp/CacheControl;->oO80:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public oO80()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->〇080:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o〇0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/squareup/okhttp/CacheControl;->〇80〇808〇O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/CacheControl;->〇O8o08O:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    invoke-direct {p0}, Lcom/squareup/okhttp/CacheControl;->〇080()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p0, Lcom/squareup/okhttp/CacheControl;->〇O8o08O:Ljava/lang/String;

    .line 11
    .line 12
    :goto_0
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇80〇808〇O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->〇o00〇〇Oo:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇o00〇〇Oo()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->Oo08:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇o〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->o〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇〇888()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/squareup/okhttp/CacheControl;->〇〇888:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

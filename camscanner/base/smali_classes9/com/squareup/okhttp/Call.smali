.class public Lcom/squareup/okhttp/Call;
.super Ljava/lang/Object;
.source "Call.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/okhttp/Call$ApplicationInterceptorChain;,
        Lcom/squareup/okhttp/Call$AsyncCall;
    }
.end annotation


# instance fields
.field O8:Lcom/squareup/okhttp/Request;

.field Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

.field private final 〇080:Lcom/squareup/okhttp/OkHttpClient;

.field private 〇o00〇〇Oo:Z

.field volatile 〇o〇:Z


# direct methods
.method constructor <init>(Lcom/squareup/okhttp/OkHttpClient;Lcom/squareup/okhttp/Request;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/squareup/okhttp/OkHttpClient;->〇o〇()Lcom/squareup/okhttp/OkHttpClient;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    iput-object p1, p0, Lcom/squareup/okhttp/Call;->〇080:Lcom/squareup/okhttp/OkHttpClient;

    .line 9
    .line 10
    iput-object p2, p0, Lcom/squareup/okhttp/Call;->O8:Lcom/squareup/okhttp/Request;

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private OO0o〇〇〇〇0()Ljava/lang/String;
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/squareup/okhttp/Call;->〇o〇:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v0, "canceled call"

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const-string v0, "call"

    .line 9
    .line 10
    :goto_0
    :try_start_0
    new-instance v1, Ljava/net/URL;

    .line 11
    .line 12
    iget-object v2, p0, Lcom/squareup/okhttp/Call;->O8:Lcom/squareup/okhttp/Request;

    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/squareup/okhttp/Request;->〇〇808〇()Ljava/net/URL;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    const-string v3, "/..."

    .line 19
    .line 20
    invoke-direct {v1, v2, v3}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    new-instance v2, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    const-string v3, " to "

    .line 36
    .line 37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :catch_0
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method static synthetic 〇080(Lcom/squareup/okhttp/Call;Z)Lcom/squareup/okhttp/Response;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/Call;->〇80〇808〇O(Z)Lcom/squareup/okhttp/Response;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private 〇80〇808〇O(Z)Lcom/squareup/okhttp/Response;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/squareup/okhttp/Call$ApplicationInterceptorChain;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    iget-object v2, p0, Lcom/squareup/okhttp/Call;->O8:Lcom/squareup/okhttp/Request;

    .line 5
    .line 6
    invoke-direct {v0, p0, v1, v2, p1}, Lcom/squareup/okhttp/Call$ApplicationInterceptorChain;-><init>(Lcom/squareup/okhttp/Call;ILcom/squareup/okhttp/Request;Z)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/squareup/okhttp/Call;->O8:Lcom/squareup/okhttp/Request;

    .line 10
    .line 11
    invoke-interface {v0, p1}, Lcom/squareup/okhttp/Interceptor$Chain;->〇080(Lcom/squareup/okhttp/Request;)Lcom/squareup/okhttp/Response;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇o00〇〇Oo(Lcom/squareup/okhttp/Call;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/squareup/okhttp/Call;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇o〇(Lcom/squareup/okhttp/Call;)Lcom/squareup/okhttp/OkHttpClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/squareup/okhttp/Call;->〇080:Lcom/squareup/okhttp/OkHttpClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public O8()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/squareup/okhttp/Call;->〇o〇:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇O8o08O()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public Oo08(Lcom/squareup/okhttp/Callback;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0}, Lcom/squareup/okhttp/Call;->o〇0(Lcom/squareup/okhttp/Callback;Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method oO80(Lcom/squareup/okhttp/Request;Z)Lcom/squareup/okhttp/Response;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/squareup/okhttp/Request;->o〇0()Lcom/squareup/okhttp/RequestBody;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/squareup/okhttp/Request;->OO0o〇〇()Lcom/squareup/okhttp/Request$Builder;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    invoke-virtual {v0}, Lcom/squareup/okhttp/RequestBody;->contentType()Lcom/squareup/okhttp/MediaType;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    const-string v2, "Content-Type"

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/squareup/okhttp/MediaType;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {p1, v2, v1}, Lcom/squareup/okhttp/Request$Builder;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/Request$Builder;

    .line 24
    .line 25
    .line 26
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/okhttp/RequestBody;->contentLength()J

    .line 27
    .line 28
    .line 29
    move-result-wide v0

    .line 30
    const-wide/16 v2, -0x1

    .line 31
    .line 32
    const-string v4, "Content-Length"

    .line 33
    .line 34
    const-string v5, "Transfer-Encoding"

    .line 35
    .line 36
    cmp-long v6, v0, v2

    .line 37
    .line 38
    if-eqz v6, :cond_1

    .line 39
    .line 40
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {p1, v4, v0}, Lcom/squareup/okhttp/Request$Builder;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/Request$Builder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1, v5}, Lcom/squareup/okhttp/Request$Builder;->OO0o〇〇(Ljava/lang/String;)Lcom/squareup/okhttp/Request$Builder;

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    const-string v0, "chunked"

    .line 52
    .line 53
    invoke-virtual {p1, v5, v0}, Lcom/squareup/okhttp/Request$Builder;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/Request$Builder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1, v4}, Lcom/squareup/okhttp/Request$Builder;->OO0o〇〇(Ljava/lang/String;)Lcom/squareup/okhttp/Request$Builder;

    .line 57
    .line 58
    .line 59
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/okhttp/Request$Builder;->〇〇888()Lcom/squareup/okhttp/Request;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    :cond_2
    move-object v2, p1

    .line 64
    new-instance p1, Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 65
    .line 66
    iget-object v1, p0, Lcom/squareup/okhttp/Call;->〇080:Lcom/squareup/okhttp/OkHttpClient;

    .line 67
    .line 68
    const/4 v3, 0x0

    .line 69
    const/4 v4, 0x0

    .line 70
    const/4 v6, 0x0

    .line 71
    const/4 v7, 0x0

    .line 72
    const/4 v8, 0x0

    .line 73
    const/4 v9, 0x0

    .line 74
    move-object v0, p1

    .line 75
    move v5, p2

    .line 76
    invoke-direct/range {v0 .. v9}, Lcom/squareup/okhttp/internal/http/HttpEngine;-><init>(Lcom/squareup/okhttp/OkHttpClient;Lcom/squareup/okhttp/Request;ZZZLcom/squareup/okhttp/Connection;Lcom/squareup/okhttp/internal/http/RouteSelector;Lcom/squareup/okhttp/internal/http/RetryableSink;Lcom/squareup/okhttp/Response;)V

    .line 77
    .line 78
    .line 79
    iput-object p1, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 80
    .line 81
    const/4 p1, 0x0

    .line 82
    :goto_1
    iget-boolean v0, p0, Lcom/squareup/okhttp/Call;->〇o〇:Z

    .line 83
    .line 84
    if-nez v0, :cond_9

    .line 85
    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 87
    .line 88
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->oo〇()V

    .line 89
    .line 90
    .line 91
    iget-object v0, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 92
    .line 93
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->O〇8O8〇008()V
    :try_end_0
    .catch Lcom/squareup/okhttp/internal/http/RequestException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/squareup/okhttp/internal/http/RouteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 97
    .line 98
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇O〇()Lcom/squareup/okhttp/Response;

    .line 99
    .line 100
    .line 101
    move-result-object v10

    .line 102
    iget-object v0, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 103
    .line 104
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->OO0o〇〇()Lcom/squareup/okhttp/Request;

    .line 105
    .line 106
    .line 107
    move-result-object v3

    .line 108
    if-nez v3, :cond_4

    .line 109
    .line 110
    if-nez p2, :cond_3

    .line 111
    .line 112
    iget-object p1, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 113
    .line 114
    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/HttpEngine;->o〇〇0〇()V

    .line 115
    .line 116
    .line 117
    :cond_3
    return-object v10

    .line 118
    :cond_4
    add-int/lit8 p1, p1, 0x1

    .line 119
    .line 120
    const/16 v0, 0x14

    .line 121
    .line 122
    if-gt p1, v0, :cond_6

    .line 123
    .line 124
    iget-object v0, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 125
    .line 126
    invoke-virtual {v3}, Lcom/squareup/okhttp/Request;->〇〇808〇()Ljava/net/URL;

    .line 127
    .line 128
    .line 129
    move-result-object v1

    .line 130
    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/HttpEngine;->OOO〇O0(Ljava/net/URL;)Z

    .line 131
    .line 132
    .line 133
    move-result v0

    .line 134
    if-nez v0, :cond_5

    .line 135
    .line 136
    iget-object v0, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 137
    .line 138
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->o〇〇0〇()V

    .line 139
    .line 140
    .line 141
    :cond_5
    iget-object v0, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 142
    .line 143
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->o〇0()Lcom/squareup/okhttp/Connection;

    .line 144
    .line 145
    .line 146
    move-result-object v7

    .line 147
    new-instance v0, Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 148
    .line 149
    iget-object v2, p0, Lcom/squareup/okhttp/Call;->〇080:Lcom/squareup/okhttp/OkHttpClient;

    .line 150
    .line 151
    const/4 v4, 0x0

    .line 152
    const/4 v5, 0x0

    .line 153
    const/4 v8, 0x0

    .line 154
    const/4 v9, 0x0

    .line 155
    move-object v1, v0

    .line 156
    move v6, p2

    .line 157
    invoke-direct/range {v1 .. v10}, Lcom/squareup/okhttp/internal/http/HttpEngine;-><init>(Lcom/squareup/okhttp/OkHttpClient;Lcom/squareup/okhttp/Request;ZZZLcom/squareup/okhttp/Connection;Lcom/squareup/okhttp/internal/http/RouteSelector;Lcom/squareup/okhttp/internal/http/RetryableSink;Lcom/squareup/okhttp/Response;)V

    .line 158
    .line 159
    .line 160
    iput-object v0, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 161
    .line 162
    goto :goto_1

    .line 163
    :cond_6
    new-instance p2, Ljava/net/ProtocolException;

    .line 164
    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    .line 166
    .line 167
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    .line 169
    .line 170
    const-string v1, "Too many follow-up requests: "

    .line 171
    .line 172
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    .line 174
    .line 175
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 179
    .line 180
    .line 181
    move-result-object p1

    .line 182
    invoke-direct {p2, p1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    throw p2

    .line 186
    :catch_0
    move-exception v0

    .line 187
    iget-object v1, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 188
    .line 189
    const/4 v2, 0x0

    .line 190
    invoke-virtual {v1, v0, v2}, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇0000OOO(Ljava/io/IOException;Lokio/Sink;)Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 191
    .line 192
    .line 193
    move-result-object v1

    .line 194
    if-eqz v1, :cond_7

    .line 195
    .line 196
    iput-object v1, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 197
    .line 198
    goto :goto_1

    .line 199
    :cond_7
    throw v0

    .line 200
    :catch_1
    move-exception v0

    .line 201
    iget-object v1, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 202
    .line 203
    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇oOO8O8(Lcom/squareup/okhttp/internal/http/RouteException;)Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 204
    .line 205
    .line 206
    move-result-object v1

    .line 207
    if-eqz v1, :cond_8

    .line 208
    .line 209
    iput-object v1, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 210
    .line 211
    goto/16 :goto_1

    .line 212
    .line 213
    :cond_8
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/RouteException;->getLastConnectException()Ljava/io/IOException;

    .line 214
    .line 215
    .line 216
    move-result-object p1

    .line 217
    throw p1

    .line 218
    :catch_2
    move-exception p1

    .line 219
    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/RequestException;->getCause()Ljava/io/IOException;

    .line 220
    .line 221
    .line 222
    move-result-object p1

    .line 223
    throw p1

    .line 224
    :cond_9
    iget-object p1, p0, Lcom/squareup/okhttp/Call;->Oo08:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 225
    .line 226
    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/HttpEngine;->o〇〇0〇()V

    .line 227
    .line 228
    .line 229
    new-instance p1, Ljava/io/IOException;

    .line 230
    .line 231
    const-string p2, "Canceled"

    .line 232
    .line 233
    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 234
    .line 235
    .line 236
    throw p1
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method o〇0(Lcom/squareup/okhttp/Callback;Z)V
    .locals 3

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/Call;->〇o00〇〇Oo:Z

    .line 3
    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/squareup/okhttp/Call;->〇o00〇〇Oo:Z

    .line 8
    .line 9
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    iget-object v0, p0, Lcom/squareup/okhttp/Call;->〇080:Lcom/squareup/okhttp/OkHttpClient;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/squareup/okhttp/OkHttpClient;->〇O8o08O()Lcom/squareup/okhttp/Dispatcher;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    new-instance v1, Lcom/squareup/okhttp/Call$AsyncCall;

    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    invoke-direct {v1, p0, p1, p2, v2}, Lcom/squareup/okhttp/Call$AsyncCall;-><init>(Lcom/squareup/okhttp/Call;Lcom/squareup/okhttp/Callback;ZLcom/squareup/okhttp/Call$1;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/Dispatcher;->〇080(Lcom/squareup/okhttp/Call$AsyncCall;)V

    .line 23
    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    :try_start_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 27
    .line 28
    const-string p2, "Already Executed"

    .line 29
    .line 30
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    throw p1

    .line 34
    :catchall_0
    move-exception p1

    .line 35
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 36
    throw p1
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public 〇〇888()Lcom/squareup/okhttp/Response;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/Call;->〇o00〇〇Oo:Z

    .line 3
    .line 4
    if-nez v0, :cond_1

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/squareup/okhttp/Call;->〇o00〇〇Oo:Z

    .line 8
    .line 9
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 10
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/Call;->〇080:Lcom/squareup/okhttp/OkHttpClient;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/squareup/okhttp/OkHttpClient;->〇O8o08O()Lcom/squareup/okhttp/Dispatcher;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {v0, p0}, Lcom/squareup/okhttp/Dispatcher;->〇o00〇〇Oo(Lcom/squareup/okhttp/Call;)V

    .line 17
    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, v0}, Lcom/squareup/okhttp/Call;->〇80〇808〇O(Z)Lcom/squareup/okhttp/Response;

    .line 21
    .line 22
    .line 23
    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 24
    if-eqz v0, :cond_0

    .line 25
    .line 26
    iget-object v1, p0, Lcom/squareup/okhttp/Call;->〇080:Lcom/squareup/okhttp/OkHttpClient;

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/squareup/okhttp/OkHttpClient;->〇O8o08O()Lcom/squareup/okhttp/Dispatcher;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-virtual {v1, p0}, Lcom/squareup/okhttp/Dispatcher;->O8(Lcom/squareup/okhttp/Call;)V

    .line 33
    .line 34
    .line 35
    return-object v0

    .line 36
    :cond_0
    :try_start_2
    new-instance v0, Ljava/io/IOException;

    .line 37
    .line 38
    const-string v1, "Canceled"

    .line 39
    .line 40
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 44
    :catchall_0
    move-exception v0

    .line 45
    iget-object v1, p0, Lcom/squareup/okhttp/Call;->〇080:Lcom/squareup/okhttp/OkHttpClient;

    .line 46
    .line 47
    invoke-virtual {v1}, Lcom/squareup/okhttp/OkHttpClient;->〇O8o08O()Lcom/squareup/okhttp/Dispatcher;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-virtual {v1, p0}, Lcom/squareup/okhttp/Dispatcher;->O8(Lcom/squareup/okhttp/Call;)V

    .line 52
    .line 53
    .line 54
    throw v0

    .line 55
    :cond_1
    :try_start_3
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 56
    .line 57
    const-string v1, "Already Executed"

    .line 58
    .line 59
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    throw v0

    .line 63
    :catchall_1
    move-exception v0

    .line 64
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 65
    throw v0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

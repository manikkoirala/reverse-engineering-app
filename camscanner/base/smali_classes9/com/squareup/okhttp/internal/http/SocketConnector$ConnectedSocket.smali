.class public Lcom/squareup/okhttp/internal/http/SocketConnector$ConnectedSocket;
.super Ljava/lang/Object;
.source "SocketConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/internal/http/SocketConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConnectedSocket"
.end annotation


# instance fields
.field public final O8:Lcom/squareup/okhttp/Handshake;

.field public final 〇080:Lcom/squareup/okhttp/Route;

.field public final 〇o00〇〇Oo:Ljava/net/Socket;

.field public final 〇o〇:Lcom/squareup/okhttp/Protocol;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/Route;Ljava/net/Socket;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/SocketConnector$ConnectedSocket;->〇080:Lcom/squareup/okhttp/Route;

    .line 3
    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/SocketConnector$ConnectedSocket;->〇o00〇〇Oo:Ljava/net/Socket;

    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/SocketConnector$ConnectedSocket;->〇o〇:Lcom/squareup/okhttp/Protocol;

    .line 5
    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/SocketConnector$ConnectedSocket;->O8:Lcom/squareup/okhttp/Handshake;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/okhttp/Route;Ljavax/net/ssl/SSLSocket;Lcom/squareup/okhttp/Protocol;Lcom/squareup/okhttp/Handshake;)V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/SocketConnector$ConnectedSocket;->〇080:Lcom/squareup/okhttp/Route;

    .line 8
    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/SocketConnector$ConnectedSocket;->〇o00〇〇Oo:Ljava/net/Socket;

    .line 9
    iput-object p3, p0, Lcom/squareup/okhttp/internal/http/SocketConnector$ConnectedSocket;->〇o〇:Lcom/squareup/okhttp/Protocol;

    .line 10
    iput-object p4, p0, Lcom/squareup/okhttp/internal/http/SocketConnector$ConnectedSocket;->O8:Lcom/squareup/okhttp/Handshake;

    return-void
.end method

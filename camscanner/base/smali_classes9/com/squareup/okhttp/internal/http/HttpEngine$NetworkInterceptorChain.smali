.class Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;
.super Ljava/lang/Object;
.source "HttpEngine.java"

# interfaces
.implements Lcom/squareup/okhttp/Interceptor$Chain;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/internal/http/HttpEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NetworkInterceptorChain"
.end annotation


# instance fields
.field final synthetic O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

.field private final 〇080:I

.field private final 〇o00〇〇Oo:Lcom/squareup/okhttp/Request;

.field private 〇o〇:I


# direct methods
.method constructor <init>(Lcom/squareup/okhttp/internal/http/HttpEngine;ILcom/squareup/okhttp/Request;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    iput p2, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇080:I

    .line 7
    .line 8
    iput-object p3, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇o00〇〇Oo:Lcom/squareup/okhttp/Request;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public 〇080(Lcom/squareup/okhttp/Request;)Lcom/squareup/okhttp/Response;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget v0, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇o〇:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    add-int/2addr v0, v1

    .line 5
    iput v0, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇o〇:I

    .line 6
    .line 7
    iget v0, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇080:I

    .line 8
    .line 9
    const-string v2, " must call proceed() exactly once"

    .line 10
    .line 11
    const-string v3, "network interceptor "

    .line 12
    .line 13
    if-lez v0, :cond_2

    .line 14
    .line 15
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 16
    .line 17
    iget-object v0, v0, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇080:Lcom/squareup/okhttp/OkHttpClient;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/squareup/okhttp/OkHttpClient;->O8ooOoo〇()Ljava/util/List;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iget v4, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇080:I

    .line 24
    .line 25
    sub-int/2addr v4, v1

    .line 26
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    check-cast v0, Lcom/squareup/okhttp/Interceptor;

    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇o00〇〇Oo()Lcom/squareup/okhttp/Connection;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    invoke-virtual {v4}, Lcom/squareup/okhttp/Connection;->oO80()Lcom/squareup/okhttp/Route;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    invoke-virtual {v4}, Lcom/squareup/okhttp/Route;->〇080()Lcom/squareup/okhttp/Address;

    .line 41
    .line 42
    .line 43
    move-result-object v4

    .line 44
    invoke-virtual {p1}, Lcom/squareup/okhttp/Request;->〇〇808〇()Ljava/net/URL;

    .line 45
    .line 46
    .line 47
    move-result-object v5

    .line 48
    invoke-virtual {v5}, Ljava/net/URL;->getHost()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v5

    .line 52
    invoke-virtual {v4}, Lcom/squareup/okhttp/Address;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v6

    .line 56
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    move-result v5

    .line 60
    if-eqz v5, :cond_1

    .line 61
    .line 62
    invoke-virtual {p1}, Lcom/squareup/okhttp/Request;->〇〇808〇()Ljava/net/URL;

    .line 63
    .line 64
    .line 65
    move-result-object v5

    .line 66
    invoke-static {v5}, Lcom/squareup/okhttp/internal/Util;->OO0o〇〇〇〇0(Ljava/net/URL;)I

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    invoke-virtual {v4}, Lcom/squareup/okhttp/Address;->〇8o8o〇()I

    .line 71
    .line 72
    .line 73
    move-result v4

    .line 74
    if-ne v5, v4, :cond_1

    .line 75
    .line 76
    iget v4, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇o〇:I

    .line 77
    .line 78
    if-gt v4, v1, :cond_0

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 82
    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    .line 84
    .line 85
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    throw p1

    .line 105
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 106
    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    .line 108
    .line 109
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    .line 111
    .line 112
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    const-string v0, " must retain the same host and port"

    .line 119
    .line 120
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    throw p1

    .line 131
    :cond_2
    :goto_0
    iget v0, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇080:I

    .line 132
    .line 133
    iget-object v4, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 134
    .line 135
    iget-object v4, v4, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇080:Lcom/squareup/okhttp/OkHttpClient;

    .line 136
    .line 137
    invoke-virtual {v4}, Lcom/squareup/okhttp/OkHttpClient;->O8ooOoo〇()Ljava/util/List;

    .line 138
    .line 139
    .line 140
    move-result-object v4

    .line 141
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 142
    .line 143
    .line 144
    move-result v4

    .line 145
    if-ge v0, v4, :cond_4

    .line 146
    .line 147
    new-instance v0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;

    .line 148
    .line 149
    iget-object v4, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 150
    .line 151
    iget v5, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇080:I

    .line 152
    .line 153
    add-int/2addr v5, v1

    .line 154
    invoke-direct {v0, v4, v5, p1}, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;-><init>(Lcom/squareup/okhttp/internal/http/HttpEngine;ILcom/squareup/okhttp/Request;)V

    .line 155
    .line 156
    .line 157
    iget-object p1, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 158
    .line 159
    iget-object p1, p1, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇080:Lcom/squareup/okhttp/OkHttpClient;

    .line 160
    .line 161
    invoke-virtual {p1}, Lcom/squareup/okhttp/OkHttpClient;->O8ooOoo〇()Ljava/util/List;

    .line 162
    .line 163
    .line 164
    move-result-object p1

    .line 165
    iget v4, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇080:I

    .line 166
    .line 167
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 168
    .line 169
    .line 170
    move-result-object p1

    .line 171
    check-cast p1, Lcom/squareup/okhttp/Interceptor;

    .line 172
    .line 173
    invoke-interface {p1, v0}, Lcom/squareup/okhttp/Interceptor;->〇080(Lcom/squareup/okhttp/Interceptor$Chain;)Lcom/squareup/okhttp/Response;

    .line 174
    .line 175
    .line 176
    move-result-object v4

    .line 177
    iget v0, v0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->〇o〇:I

    .line 178
    .line 179
    if-ne v0, v1, :cond_3

    .line 180
    .line 181
    return-object v4

    .line 182
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 183
    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    .line 185
    .line 186
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    .line 188
    .line 189
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object p1

    .line 202
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    throw v0

    .line 206
    :cond_4
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 207
    .line 208
    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇o00〇〇Oo(Lcom/squareup/okhttp/internal/http/HttpEngine;)Lcom/squareup/okhttp/internal/http/Transport;

    .line 209
    .line 210
    .line 211
    move-result-object v0

    .line 212
    invoke-interface {v0, p1}, Lcom/squareup/okhttp/internal/http/Transport;->o〇0(Lcom/squareup/okhttp/Request;)V

    .line 213
    .line 214
    .line 215
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 216
    .line 217
    invoke-static {v0, p1}, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇o〇(Lcom/squareup/okhttp/internal/http/HttpEngine;Lcom/squareup/okhttp/Request;)Lcom/squareup/okhttp/Request;

    .line 218
    .line 219
    .line 220
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 221
    .line 222
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->o〇O8〇〇o()Z

    .line 223
    .line 224
    .line 225
    move-result v0

    .line 226
    if-eqz v0, :cond_5

    .line 227
    .line 228
    invoke-virtual {p1}, Lcom/squareup/okhttp/Request;->o〇0()Lcom/squareup/okhttp/RequestBody;

    .line 229
    .line 230
    .line 231
    move-result-object v0

    .line 232
    if-eqz v0, :cond_5

    .line 233
    .line 234
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 235
    .line 236
    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇o00〇〇Oo(Lcom/squareup/okhttp/internal/http/HttpEngine;)Lcom/squareup/okhttp/internal/http/Transport;

    .line 237
    .line 238
    .line 239
    move-result-object v0

    .line 240
    invoke-virtual {p1}, Lcom/squareup/okhttp/Request;->o〇0()Lcom/squareup/okhttp/RequestBody;

    .line 241
    .line 242
    .line 243
    move-result-object v1

    .line 244
    invoke-virtual {v1}, Lcom/squareup/okhttp/RequestBody;->contentLength()J

    .line 245
    .line 246
    .line 247
    move-result-wide v1

    .line 248
    invoke-interface {v0, p1, v1, v2}, Lcom/squareup/okhttp/internal/http/Transport;->Oo08(Lcom/squareup/okhttp/Request;J)Lokio/Sink;

    .line 249
    .line 250
    .line 251
    move-result-object v0

    .line 252
    invoke-static {v0}, Lokio/Okio;->buffer(Lokio/Sink;)Lokio/BufferedSink;

    .line 253
    .line 254
    .line 255
    move-result-object v0

    .line 256
    invoke-virtual {p1}, Lcom/squareup/okhttp/Request;->o〇0()Lcom/squareup/okhttp/RequestBody;

    .line 257
    .line 258
    .line 259
    move-result-object p1

    .line 260
    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/RequestBody;->writeTo(Lokio/BufferedSink;)V

    .line 261
    .line 262
    .line 263
    invoke-interface {v0}, Lokio/Sink;->close()V

    .line 264
    .line 265
    .line 266
    :cond_5
    iget-object p1, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 267
    .line 268
    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/HttpEngine;->O8(Lcom/squareup/okhttp/internal/http/HttpEngine;)Lcom/squareup/okhttp/Response;

    .line 269
    .line 270
    .line 271
    move-result-object p1

    .line 272
    invoke-virtual {p1}, Lcom/squareup/okhttp/Response;->Oooo8o0〇()I

    .line 273
    .line 274
    .line 275
    move-result v0

    .line 276
    const/16 v1, 0xcc

    .line 277
    .line 278
    if-eq v0, v1, :cond_6

    .line 279
    .line 280
    const/16 v1, 0xcd

    .line 281
    .line 282
    if-ne v0, v1, :cond_7

    .line 283
    .line 284
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/okhttp/Response;->〇8o8o〇()Lcom/squareup/okhttp/ResponseBody;

    .line 285
    .line 286
    .line 287
    move-result-object v1

    .line 288
    invoke-virtual {v1}, Lcom/squareup/okhttp/ResponseBody;->〇8o8o〇()J

    .line 289
    .line 290
    .line 291
    move-result-wide v1

    .line 292
    const-wide/16 v3, 0x0

    .line 293
    .line 294
    cmp-long v5, v1, v3

    .line 295
    .line 296
    if-gtz v5, :cond_8

    .line 297
    .line 298
    :cond_7
    return-object p1

    .line 299
    :cond_8
    new-instance v1, Ljava/net/ProtocolException;

    .line 300
    .line 301
    new-instance v2, Ljava/lang/StringBuilder;

    .line 302
    .line 303
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 304
    .line 305
    .line 306
    const-string v3, "HTTP "

    .line 307
    .line 308
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    .line 310
    .line 311
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 312
    .line 313
    .line 314
    const-string v0, " had non-zero Content-Length: "

    .line 315
    .line 316
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    .line 318
    .line 319
    invoke-virtual {p1}, Lcom/squareup/okhttp/Response;->〇8o8o〇()Lcom/squareup/okhttp/ResponseBody;

    .line 320
    .line 321
    .line 322
    move-result-object p1

    .line 323
    invoke-virtual {p1}, Lcom/squareup/okhttp/ResponseBody;->〇8o8o〇()J

    .line 324
    .line 325
    .line 326
    move-result-wide v3

    .line 327
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 328
    .line 329
    .line 330
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 331
    .line 332
    .line 333
    move-result-object p1

    .line 334
    invoke-direct {v1, p1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    .line 335
    .line 336
    .line 337
    throw v1
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method public 〇o00〇〇Oo()Lcom/squareup/okhttp/Connection;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpEngine$NetworkInterceptorChain;->O8:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇080(Lcom/squareup/okhttp/internal/http/HttpEngine;)Lcom/squareup/okhttp/Connection;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

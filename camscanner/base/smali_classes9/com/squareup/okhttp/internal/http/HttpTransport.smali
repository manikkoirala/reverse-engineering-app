.class public final Lcom/squareup/okhttp/internal/http/HttpTransport;
.super Ljava/lang/Object;
.source "HttpTransport.java"

# interfaces
.implements Lcom/squareup/okhttp/internal/http/Transport;


# instance fields
.field private final 〇080:Lcom/squareup/okhttp/internal/http/HttpEngine;

.field private final 〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/internal/http/HttpEngine;Lcom/squareup/okhttp/internal/http/HttpConnection;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇080:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private OO0o〇〇〇〇0(Lcom/squareup/okhttp/Response;)Lokio/Source;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇〇8O0〇8(Lcom/squareup/okhttp/Response;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget-object p1, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 8
    .line 9
    const-wide/16 v0, 0x0

    .line 10
    .line 11
    invoke-virtual {p1, v0, v1}, Lcom/squareup/okhttp/internal/http/HttpConnection;->OoO8(J)Lokio/Source;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1

    .line 16
    :cond_0
    const-string v0, "Transfer-Encoding"

    .line 17
    .line 18
    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/Response;->〇O〇(Ljava/lang/String;)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-string v1, "chunked"

    .line 23
    .line 24
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    iget-object p1, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 31
    .line 32
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇080:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 33
    .line 34
    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/internal/http/HttpConnection;->〇〇8O0〇8(Lcom/squareup/okhttp/internal/http/HttpEngine;)Lokio/Source;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    return-object p1

    .line 39
    :cond_1
    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/OkHeaders;->Oo08(Lcom/squareup/okhttp/Response;)J

    .line 40
    .line 41
    .line 42
    move-result-wide v0

    .line 43
    const-wide/16 v2, -0x1

    .line 44
    .line 45
    cmp-long p1, v0, v2

    .line 46
    .line 47
    if-eqz p1, :cond_2

    .line 48
    .line 49
    iget-object p1, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 50
    .line 51
    invoke-virtual {p1, v0, v1}, Lcom/squareup/okhttp/internal/http/HttpConnection;->OoO8(J)Lokio/Source;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    return-object p1

    .line 56
    :cond_2
    iget-object p1, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/HttpConnection;->o800o8O()Lokio/Source;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    return-object p1
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method


# virtual methods
.method public O8(Lcom/squareup/okhttp/internal/http/HttpEngine;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/HttpConnection;->〇8o8o〇(Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public Oo08(Lcom/squareup/okhttp/Request;J)Lokio/Sink;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const-string v0, "Transfer-Encoding"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/Request;->oO80(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const-string v0, "chunked"

    .line 8
    .line 9
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    iget-object p1, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/HttpConnection;->〇O00()Lokio/Sink;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    return-object p1

    .line 22
    :cond_0
    const-wide/16 v0, -0x1

    .line 23
    .line 24
    cmp-long p1, p2, v0

    .line 25
    .line 26
    if-eqz p1, :cond_1

    .line 27
    .line 28
    iget-object p1, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 29
    .line 30
    invoke-virtual {p1, p2, p3}, Lcom/squareup/okhttp/internal/http/HttpConnection;->〇0〇O0088o(J)Lokio/Sink;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    return-object p1

    .line 35
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 36
    .line 37
    const-string p2, "Cannot stream a request body without chunked encoding or a known content length!"

    .line 38
    .line 39
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    throw p1
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public oO80()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇80〇808〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpConnection;->〇O888o0o()V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpConnection;->〇O8o08O()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
.end method

.method public o〇0(Lcom/squareup/okhttp/Request;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇080:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->o0ooO()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇080:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->Oooo8o0〇()Lcom/squareup/okhttp/Connection;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/squareup/okhttp/Connection;->oO80()Lcom/squareup/okhttp/Route;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lcom/squareup/okhttp/Route;->〇o00〇〇Oo()Ljava/net/Proxy;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇080:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 25
    .line 26
    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/HttpEngine;->Oooo8o0〇()Lcom/squareup/okhttp/Connection;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v1}, Lcom/squareup/okhttp/Connection;->〇〇888()Lcom/squareup/okhttp/Protocol;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-static {p1, v0, v1}, Lcom/squareup/okhttp/internal/http/RequestLine;->〇080(Lcom/squareup/okhttp/Request;Ljava/net/Proxy$Type;Lcom/squareup/okhttp/Protocol;)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 39
    .line 40
    invoke-virtual {p1}, Lcom/squareup/okhttp/Request;->〇80〇808〇O()Lcom/squareup/okhttp/Headers;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-virtual {v1, p1, v0}, Lcom/squareup/okhttp/internal/http/HttpConnection;->〇00(Lcom/squareup/okhttp/Headers;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public 〇080()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpConnection;->Oooo8o0〇()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇80〇808〇O()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇080:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇〇808〇()Lcom/squareup/okhttp/Request;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "Connection"

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/Request;->oO80(Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v2, "close"

    .line 14
    .line 15
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    const/4 v3, 0x0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    return v3

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇080:Lcom/squareup/okhttp/internal/http/HttpEngine;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpEngine;->〇O〇()Lcom/squareup/okhttp/Response;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/Response;->〇O〇(Ljava/lang/String;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    return v3

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpConnection;->〇〇808〇()Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-eqz v0, :cond_2

    .line 47
    .line 48
    return v3

    .line 49
    :cond_2
    const/4 v0, 0x1

    .line 50
    return v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public 〇o00〇〇Oo(Lcom/squareup/okhttp/internal/http/RetryableSink;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/HttpConnection;->O〇8O8〇008(Lcom/squareup/okhttp/internal/http/RetryableSink;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇o〇()Lcom/squareup/okhttp/Response$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpTransport;->〇o00〇〇Oo:Lcom/squareup/okhttp/internal/http/HttpConnection;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/HttpConnection;->〇oo〇()Lcom/squareup/okhttp/Response$Builder;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇〇888(Lcom/squareup/okhttp/Response;)Lcom/squareup/okhttp/ResponseBody;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/http/HttpTransport;->OO0o〇〇〇〇0(Lcom/squareup/okhttp/Response;)Lokio/Source;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/squareup/okhttp/internal/http/RealResponseBody;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/squareup/okhttp/Response;->〇〇8O0〇8()Lcom/squareup/okhttp/Headers;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    invoke-static {v0}, Lokio/Okio;->buffer(Lokio/Source;)Lokio/BufferedSource;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-direct {v1, p1, v0}, Lcom/squareup/okhttp/internal/http/RealResponseBody;-><init>(Lcom/squareup/okhttp/Headers;Lokio/BufferedSource;)V

    .line 16
    .line 17
    .line 18
    return-object v1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

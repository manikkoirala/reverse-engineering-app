.class final Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;
.super Ljava/lang/Object;
.source "Spdy3.java"

# interfaces
.implements Lcom/squareup/okhttp/internal/spdy/FrameReader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/internal/spdy/Spdy3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Reader"
.end annotation


# instance fields
.field private final OO:Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;

.field private final o0:Lokio/BufferedSource;

.field private final 〇OOo8〇0:Z


# direct methods
.method constructor <init>(Lokio/BufferedSource;Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 5
    .line 6
    new-instance v0, Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;

    .line 7
    .line 8
    invoke-direct {v0, p1}, Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;-><init>(Lokio/BufferedSource;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->OO:Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;

    .line 12
    .line 13
    iput-boolean p2, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->〇OOo8〇0:Z

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private OO0o〇〇(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 2
    .line 3
    invoke-interface {v0}, Lokio/BufferedSource;->readInt()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    mul-int/lit8 v1, v0, 0x8

    .line 8
    .line 9
    add-int/lit8 v1, v1, 0x4

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x1

    .line 13
    if-ne p3, v1, :cond_2

    .line 14
    .line 15
    new-instance p3, Lcom/squareup/okhttp/internal/spdy/Settings;

    .line 16
    .line 17
    invoke-direct {p3}, Lcom/squareup/okhttp/internal/spdy/Settings;-><init>()V

    .line 18
    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    :goto_0
    if-ge v1, v0, :cond_0

    .line 22
    .line 23
    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 24
    .line 25
    invoke-interface {v4}, Lokio/BufferedSource;->readInt()I

    .line 26
    .line 27
    .line 28
    move-result v4

    .line 29
    iget-object v5, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 30
    .line 31
    invoke-interface {v5}, Lokio/BufferedSource;->readInt()I

    .line 32
    .line 33
    .line 34
    move-result v5

    .line 35
    const/high16 v6, -0x1000000

    .line 36
    .line 37
    and-int/2addr v6, v4

    .line 38
    ushr-int/lit8 v6, v6, 0x18

    .line 39
    .line 40
    const v7, 0xffffff

    .line 41
    .line 42
    .line 43
    and-int/2addr v4, v7

    .line 44
    invoke-virtual {p3, v4, v6, v5}, Lcom/squareup/okhttp/internal/spdy/Settings;->〇8o8o〇(III)Lcom/squareup/okhttp/internal/spdy/Settings;

    .line 45
    .line 46
    .line 47
    add-int/lit8 v1, v1, 0x1

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_0
    and-int/2addr p2, v3

    .line 51
    if-eqz p2, :cond_1

    .line 52
    .line 53
    const/4 v2, 0x1

    .line 54
    :cond_1
    invoke-interface {p1, v2, p3}, Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;->〇O8o08O(ZLcom/squareup/okhttp/internal/spdy/Settings;)V

    .line 55
    .line 56
    .line 57
    return-void

    .line 58
    :cond_2
    const/4 p1, 0x2

    .line 59
    new-array p1, p1, [Ljava/lang/Object;

    .line 60
    .line 61
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    aput-object p2, p1, v2

    .line 66
    .line 67
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 68
    .line 69
    .line 70
    move-result-object p2

    .line 71
    aput-object p2, p1, v3

    .line 72
    .line 73
    const-string p2, "TYPE_SETTINGS length: %d != 4 + 8 * %d"

    .line 74
    .line 75
    invoke-static {p2, p1}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->Oo08(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    throw p1
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private static varargs Oo08(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/io/IOException;

    .line 2
    .line 3
    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    throw v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private oO80(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object p2, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 2
    .line 3
    invoke-interface {p2}, Lokio/BufferedSource;->readInt()I

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    const v0, 0x7fffffff

    .line 8
    .line 9
    .line 10
    and-int v4, p2, v0

    .line 11
    .line 12
    iget-object p2, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->OO:Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;

    .line 13
    .line 14
    add-int/lit8 p3, p3, -0x4

    .line 15
    .line 16
    invoke-virtual {p2, p3}, Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;->o〇0(I)Ljava/util/List;

    .line 17
    .line 18
    .line 19
    move-result-object v6

    .line 20
    const/4 v2, 0x0

    .line 21
    const/4 v3, 0x0

    .line 22
    const/4 v5, -0x1

    .line 23
    sget-object v7, Lcom/squareup/okhttp/internal/spdy/HeadersMode;->SPDY_HEADERS:Lcom/squareup/okhttp/internal/spdy/HeadersMode;

    .line 24
    .line 25
    move-object v1, p1

    .line 26
    invoke-interface/range {v1 .. v7}, Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;->OO0o〇〇〇〇0(ZZIILjava/util/List;Lcom/squareup/okhttp/internal/spdy/HeadersMode;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private 〇8o8o〇(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 p2, 0x4

    .line 2
    const/4 v0, 0x0

    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne p3, p2, :cond_2

    .line 5
    .line 6
    iget-object p2, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 7
    .line 8
    invoke-interface {p2}, Lokio/BufferedSource;->readInt()I

    .line 9
    .line 10
    .line 11
    move-result p2

    .line 12
    iget-boolean p3, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->〇OOo8〇0:Z

    .line 13
    .line 14
    and-int/lit8 v2, p2, 0x1

    .line 15
    .line 16
    if-ne v2, v1, :cond_0

    .line 17
    .line 18
    const/4 v2, 0x1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v2, 0x0

    .line 21
    :goto_0
    if-ne p3, v2, :cond_1

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    const/4 v1, 0x0

    .line 25
    :goto_1
    invoke-interface {p1, v1, p2, v0}, Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;->〇o00〇〇Oo(ZII)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_2
    new-array p1, v1, [Ljava/lang/Object;

    .line 30
    .line 31
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    aput-object p2, p1, v0

    .line 36
    .line 37
    const-string p2, "TYPE_PING length: %d != 4"

    .line 38
    .line 39
    invoke-static {p2, p1}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->Oo08(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    throw p1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private 〇O00(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/16 p2, 0x8

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    const/4 v1, 0x1

    .line 5
    if-ne p3, p2, :cond_1

    .line 6
    .line 7
    iget-object p2, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 8
    .line 9
    invoke-interface {p2}, Lokio/BufferedSource;->readInt()I

    .line 10
    .line 11
    .line 12
    move-result p2

    .line 13
    iget-object p3, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 14
    .line 15
    invoke-interface {p3}, Lokio/BufferedSource;->readInt()I

    .line 16
    .line 17
    .line 18
    move-result p3

    .line 19
    const v2, 0x7fffffff

    .line 20
    .line 21
    .line 22
    and-int/2addr p2, v2

    .line 23
    and-int/2addr p3, v2

    .line 24
    int-to-long v2, p3

    .line 25
    const-wide/16 v4, 0x0

    .line 26
    .line 27
    cmp-long p3, v2, v4

    .line 28
    .line 29
    if-eqz p3, :cond_0

    .line 30
    .line 31
    invoke-interface {p1, p2, v2, v3}, Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;->〇o〇(IJ)V

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_0
    new-array p1, v1, [Ljava/lang/Object;

    .line 36
    .line 37
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    aput-object p2, p1, v0

    .line 42
    .line 43
    const-string/jumbo p2, "windowSizeIncrement was 0"

    .line 44
    .line 45
    .line 46
    invoke-static {p2, p1}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->Oo08(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    throw p1

    .line 51
    :cond_1
    new-array p1, v1, [Ljava/lang/Object;

    .line 52
    .line 53
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 54
    .line 55
    .line 56
    move-result-object p2

    .line 57
    aput-object p2, p1, v0

    .line 58
    .line 59
    const-string p2, "TYPE_WINDOW_UPDATE length: %d != 8"

    .line 60
    .line 61
    invoke-static {p2, p1}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->Oo08(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    throw p1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private 〇O8o08O(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/16 p2, 0x8

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    const/4 v1, 0x1

    .line 5
    if-ne p3, p2, :cond_1

    .line 6
    .line 7
    iget-object p2, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 8
    .line 9
    invoke-interface {p2}, Lokio/BufferedSource;->readInt()I

    .line 10
    .line 11
    .line 12
    move-result p2

    .line 13
    const p3, 0x7fffffff

    .line 14
    .line 15
    .line 16
    and-int/2addr p2, p3

    .line 17
    iget-object p3, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 18
    .line 19
    invoke-interface {p3}, Lokio/BufferedSource;->readInt()I

    .line 20
    .line 21
    .line 22
    move-result p3

    .line 23
    invoke-static {p3}, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->fromSpdy3Rst(I)Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    if-eqz v2, :cond_0

    .line 28
    .line 29
    invoke-interface {p1, p2, v2}, Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;->o〇0(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :cond_0
    new-array p1, v1, [Ljava/lang/Object;

    .line 34
    .line 35
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 36
    .line 37
    .line 38
    move-result-object p2

    .line 39
    aput-object p2, p1, v0

    .line 40
    .line 41
    const-string p2, "TYPE_RST_STREAM unexpected error code: %d"

    .line 42
    .line 43
    invoke-static {p2, p1}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->Oo08(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    throw p1

    .line 48
    :cond_1
    new-array p1, v1, [Ljava/lang/Object;

    .line 49
    .line 50
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 51
    .line 52
    .line 53
    move-result-object p2

    .line 54
    aput-object p2, p1, v0

    .line 55
    .line 56
    const-string p2, "TYPE_RST_STREAM length: %d != 8"

    .line 57
    .line 58
    invoke-static {p2, p1}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->Oo08(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    throw p1
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private 〇O〇(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 2
    .line 3
    invoke-interface {v0}, Lokio/BufferedSource;->readInt()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 8
    .line 9
    invoke-interface {v1}, Lokio/BufferedSource;->readInt()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const v2, 0x7fffffff

    .line 14
    .line 15
    .line 16
    and-int v6, v0, v2

    .line 17
    .line 18
    and-int v7, v1, v2

    .line 19
    .line 20
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 21
    .line 22
    invoke-interface {v0}, Lokio/BufferedSource;->readShort()S

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->OO:Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;

    .line 26
    .line 27
    add-int/lit8 p3, p3, -0xa

    .line 28
    .line 29
    invoke-virtual {v0, p3}, Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;->o〇0(I)Ljava/util/List;

    .line 30
    .line 31
    .line 32
    move-result-object v8

    .line 33
    and-int/lit8 p3, p2, 0x1

    .line 34
    .line 35
    const/4 v0, 0x0

    .line 36
    const/4 v1, 0x1

    .line 37
    if-eqz p3, :cond_0

    .line 38
    .line 39
    const/4 v5, 0x1

    .line 40
    goto :goto_0

    .line 41
    :cond_0
    const/4 v5, 0x0

    .line 42
    :goto_0
    and-int/lit8 p2, p2, 0x2

    .line 43
    .line 44
    if-eqz p2, :cond_1

    .line 45
    .line 46
    const/4 v4, 0x1

    .line 47
    goto :goto_1

    .line 48
    :cond_1
    const/4 v4, 0x0

    .line 49
    :goto_1
    sget-object v9, Lcom/squareup/okhttp/internal/spdy/HeadersMode;->SPDY_SYN_STREAM:Lcom/squareup/okhttp/internal/spdy/HeadersMode;

    .line 50
    .line 51
    move-object v3, p1

    .line 52
    invoke-interface/range {v3 .. v9}, Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;->OO0o〇〇〇〇0(ZZIILjava/util/List;Lcom/squareup/okhttp/internal/spdy/HeadersMode;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private 〇〇808〇(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 2
    .line 3
    invoke-interface {v0}, Lokio/BufferedSource;->readInt()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const v1, 0x7fffffff

    .line 8
    .line 9
    .line 10
    and-int v5, v0, v1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->OO:Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;

    .line 13
    .line 14
    add-int/lit8 p3, p3, -0x4

    .line 15
    .line 16
    invoke-virtual {v0, p3}, Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;->o〇0(I)Ljava/util/List;

    .line 17
    .line 18
    .line 19
    move-result-object v7

    .line 20
    const/4 p3, 0x1

    .line 21
    and-int/2addr p2, p3

    .line 22
    if-eqz p2, :cond_0

    .line 23
    .line 24
    const/4 v4, 0x1

    .line 25
    goto :goto_0

    .line 26
    :cond_0
    const/4 p2, 0x0

    .line 27
    const/4 v4, 0x0

    .line 28
    :goto_0
    const/4 v3, 0x0

    .line 29
    const/4 v6, -0x1

    .line 30
    sget-object v8, Lcom/squareup/okhttp/internal/spdy/HeadersMode;->SPDY_REPLY:Lcom/squareup/okhttp/internal/spdy/HeadersMode;

    .line 31
    .line 32
    move-object v2, p1

    .line 33
    invoke-interface/range {v2 .. v8}, Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;->OO0o〇〇〇〇0(ZZIILjava/util/List;Lcom/squareup/okhttp/internal/spdy/HeadersMode;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private 〇〇888(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/16 p2, 0x8

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    const/4 v1, 0x1

    .line 5
    if-ne p3, p2, :cond_1

    .line 6
    .line 7
    iget-object p2, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 8
    .line 9
    invoke-interface {p2}, Lokio/BufferedSource;->readInt()I

    .line 10
    .line 11
    .line 12
    move-result p2

    .line 13
    const p3, 0x7fffffff

    .line 14
    .line 15
    .line 16
    and-int/2addr p2, p3

    .line 17
    iget-object p3, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 18
    .line 19
    invoke-interface {p3}, Lokio/BufferedSource;->readInt()I

    .line 20
    .line 21
    .line 22
    move-result p3

    .line 23
    invoke-static {p3}, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->fromSpdyGoAway(I)Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    if-eqz v2, :cond_0

    .line 28
    .line 29
    sget-object p3, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    .line 30
    .line 31
    invoke-interface {p1, p2, v2, p3}, Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;->〇8o8o〇(ILcom/squareup/okhttp/internal/spdy/ErrorCode;Lokio/ByteString;)V

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_0
    new-array p1, v1, [Ljava/lang/Object;

    .line 36
    .line 37
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    aput-object p2, p1, v0

    .line 42
    .line 43
    const-string p2, "TYPE_GOAWAY unexpected error code: %d"

    .line 44
    .line 45
    invoke-static {p2, p1}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->Oo08(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    throw p1

    .line 50
    :cond_1
    new-array p1, v1, [Ljava/lang/Object;

    .line 51
    .line 52
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 53
    .line 54
    .line 55
    move-result-object p2

    .line 56
    aput-object p2, p1, v0

    .line 57
    .line 58
    const-string p2, "TYPE_GOAWAY length: %d != 8"

    .line 59
    .line 60
    invoke-static {p2, p1}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->Oo08(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    throw p1
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public Oooo8o0〇()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->OO:Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/NameValueBlockReader;->〇o〇()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇0〇O0088o(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 3
    .line 4
    invoke-interface {v1}, Lokio/BufferedSource;->readInt()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 9
    .line 10
    invoke-interface {v2}, Lokio/BufferedSource;->readInt()I

    .line 11
    .line 12
    .line 13
    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    const/high16 v3, -0x80000000

    .line 15
    .line 16
    and-int/2addr v3, v1

    .line 17
    const/4 v4, 0x1

    .line 18
    if-eqz v3, :cond_0

    .line 19
    .line 20
    const/4 v3, 0x1

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v3, 0x0

    .line 23
    :goto_0
    const/high16 v5, -0x1000000

    .line 24
    .line 25
    and-int/2addr v5, v2

    .line 26
    ushr-int/lit8 v5, v5, 0x18

    .line 27
    .line 28
    const v6, 0xffffff

    .line 29
    .line 30
    .line 31
    and-int/2addr v2, v6

    .line 32
    if-eqz v3, :cond_2

    .line 33
    .line 34
    const/high16 v0, 0x7fff0000

    .line 35
    .line 36
    and-int/2addr v0, v1

    .line 37
    ushr-int/lit8 v0, v0, 0x10

    .line 38
    .line 39
    const v3, 0xffff

    .line 40
    .line 41
    .line 42
    and-int/2addr v1, v3

    .line 43
    const/4 v3, 0x3

    .line 44
    if-ne v0, v3, :cond_1

    .line 45
    .line 46
    packed-switch v1, :pswitch_data_0

    .line 47
    .line 48
    .line 49
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 50
    .line 51
    int-to-long v0, v2

    .line 52
    invoke-interface {p1, v0, v1}, Lokio/BufferedSource;->skip(J)V

    .line 53
    .line 54
    .line 55
    return v4

    .line 56
    :pswitch_1
    invoke-direct {p0, p1, v5, v2}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->〇O00(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V

    .line 57
    .line 58
    .line 59
    return v4

    .line 60
    :pswitch_2
    invoke-direct {p0, p1, v5, v2}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->oO80(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V

    .line 61
    .line 62
    .line 63
    return v4

    .line 64
    :pswitch_3
    invoke-direct {p0, p1, v5, v2}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->〇〇888(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V

    .line 65
    .line 66
    .line 67
    return v4

    .line 68
    :pswitch_4
    invoke-direct {p0, p1, v5, v2}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->〇8o8o〇(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V

    .line 69
    .line 70
    .line 71
    return v4

    .line 72
    :pswitch_5
    invoke-direct {p0, p1, v5, v2}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->OO0o〇〇(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V

    .line 73
    .line 74
    .line 75
    return v4

    .line 76
    :pswitch_6
    invoke-direct {p0, p1, v5, v2}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->〇O8o08O(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V

    .line 77
    .line 78
    .line 79
    return v4

    .line 80
    :pswitch_7
    invoke-direct {p0, p1, v5, v2}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->〇〇808〇(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V

    .line 81
    .line 82
    .line 83
    return v4

    .line 84
    :pswitch_8
    invoke-direct {p0, p1, v5, v2}, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->〇O〇(Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;II)V

    .line 85
    .line 86
    .line 87
    return v4

    .line 88
    :cond_1
    new-instance p1, Ljava/net/ProtocolException;

    .line 89
    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    .line 91
    .line 92
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    .line 94
    .line 95
    const-string/jumbo v2, "version != 3: "

    .line 96
    .line 97
    .line 98
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-direct {p1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    throw p1

    .line 112
    :cond_2
    const v3, 0x7fffffff

    .line 113
    .line 114
    .line 115
    and-int/2addr v1, v3

    .line 116
    and-int/lit8 v3, v5, 0x1

    .line 117
    .line 118
    if-eqz v3, :cond_3

    .line 119
    .line 120
    const/4 v0, 0x1

    .line 121
    :cond_3
    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/Spdy3$Reader;->o0:Lokio/BufferedSource;

    .line 122
    .line 123
    invoke-interface {p1, v0, v1, v3, v2}, Lcom/squareup/okhttp/internal/spdy/FrameReader$Handler;->oO80(ZILokio/BufferedSource;I)V

    .line 124
    .line 125
    .line 126
    return v4

    .line 127
    :catch_0
    return v0

    .line 128
    nop

    .line 129
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

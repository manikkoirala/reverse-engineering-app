.class final Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;
.super Ljava/lang/Object;
.source "Http2.java"

# interfaces
.implements Lokio/Source;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/okhttp/internal/spdy/Http2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ContinuationSource"
.end annotation


# instance fields
.field O8o08O8O:S

.field OO:B

.field private final o0:Lokio/BufferedSource;

.field o〇00O:I

.field 〇08O〇00〇o:I

.field 〇OOo8〇0:I


# direct methods
.method public constructor <init>(Lokio/BufferedSource;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o0:Lokio/BufferedSource;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private Oo08()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->〇08O〇00〇o:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o0:Lokio/BufferedSource;

    .line 4
    .line 5
    invoke-static {v1}, Lcom/squareup/okhttp/internal/spdy/Http2;->o〇0(Lokio/BufferedSource;)I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iput v1, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o〇00O:I

    .line 10
    .line 11
    iput v1, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->〇OOo8〇0:I

    .line 12
    .line 13
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o0:Lokio/BufferedSource;

    .line 14
    .line 15
    invoke-interface {v1}, Lokio/BufferedSource;->readByte()B

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    and-int/lit16 v1, v1, 0xff

    .line 20
    .line 21
    int-to-byte v1, v1

    .line 22
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o0:Lokio/BufferedSource;

    .line 23
    .line 24
    invoke-interface {v2}, Lokio/BufferedSource;->readByte()B

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    and-int/lit16 v2, v2, 0xff

    .line 29
    .line 30
    int-to-byte v2, v2

    .line 31
    iput-byte v2, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->OO:B

    .line 32
    .line 33
    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/Http2;->O8()Ljava/util/logging/Logger;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    .line 38
    .line 39
    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    const/4 v3, 0x1

    .line 44
    if-eqz v2, :cond_0

    .line 45
    .line 46
    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/Http2;->O8()Ljava/util/logging/Logger;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    iget v4, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->〇08O〇00〇o:I

    .line 51
    .line 52
    iget v5, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->〇OOo8〇0:I

    .line 53
    .line 54
    iget-byte v6, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->OO:B

    .line 55
    .line 56
    invoke-static {v3, v4, v5, v1, v6}, Lcom/squareup/okhttp/internal/spdy/Http2$FrameLogger;->〇o00〇〇Oo(ZIIBB)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v4

    .line 60
    invoke-virtual {v2, v4}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    :cond_0
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o0:Lokio/BufferedSource;

    .line 64
    .line 65
    invoke-interface {v2}, Lokio/BufferedSource;->readInt()I

    .line 66
    .line 67
    .line 68
    move-result v2

    .line 69
    const v4, 0x7fffffff

    .line 70
    .line 71
    .line 72
    and-int/2addr v2, v4

    .line 73
    iput v2, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->〇08O〇00〇o:I

    .line 74
    .line 75
    const/16 v4, 0x9

    .line 76
    .line 77
    const/4 v5, 0x0

    .line 78
    if-ne v1, v4, :cond_2

    .line 79
    .line 80
    if-ne v2, v0, :cond_1

    .line 81
    .line 82
    return-void

    .line 83
    :cond_1
    const-string v0, "TYPE_CONTINUATION streamId changed"

    .line 84
    .line 85
    new-array v1, v5, [Ljava/lang/Object;

    .line 86
    .line 87
    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/Http2;->Oo08(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    throw v0

    .line 92
    :cond_2
    new-array v0, v3, [Ljava/lang/Object;

    .line 93
    .line 94
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    aput-object v1, v0, v5

    .line 99
    .line 100
    const-string v1, "%s != TYPE_CONTINUATION"

    .line 101
    .line 102
    invoke-static {v1, v0}, Lcom/squareup/okhttp/internal/spdy/Http2;->Oo08(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    throw v0
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public read(Lokio/Buffer;J)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    :goto_0
    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o〇00O:I

    .line 2
    .line 3
    const-wide/16 v1, -0x1

    .line 4
    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o0:Lokio/BufferedSource;

    .line 8
    .line 9
    iget-short v3, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->O8o08O8O:S

    .line 10
    .line 11
    int-to-long v3, v3

    .line 12
    invoke-interface {v0, v3, v4}, Lokio/BufferedSource;->skip(J)V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    iput-short v0, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->O8o08O8O:S

    .line 17
    .line 18
    iget-byte v0, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->OO:B

    .line 19
    .line 20
    and-int/lit8 v0, v0, 0x4

    .line 21
    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    return-wide v1

    .line 25
    :cond_0
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->Oo08()V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o0:Lokio/BufferedSource;

    .line 30
    .line 31
    int-to-long v4, v0

    .line 32
    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    .line 33
    .line 34
    .line 35
    move-result-wide p2

    .line 36
    invoke-interface {v3, p1, p2, p3}, Lokio/Source;->read(Lokio/Buffer;J)J

    .line 37
    .line 38
    .line 39
    move-result-wide p1

    .line 40
    cmp-long p3, p1, v1

    .line 41
    .line 42
    if-nez p3, :cond_2

    .line 43
    .line 44
    return-wide v1

    .line 45
    :cond_2
    iget p3, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o〇00O:I

    .line 46
    .line 47
    int-to-long v0, p3

    .line 48
    sub-long/2addr v0, p1

    .line 49
    long-to-int p3, v0

    .line 50
    iput p3, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o〇00O:I

    .line 51
    .line 52
    return-wide p1
.end method

.method public timeout()Lokio/Timeout;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Http2$ContinuationSource;->o0:Lokio/BufferedSource;

    .line 2
    .line 3
    invoke-interface {v0}, Lokio/Source;->timeout()Lokio/Timeout;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

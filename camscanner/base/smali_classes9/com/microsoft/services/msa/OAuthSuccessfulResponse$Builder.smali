.class public Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
.super Ljava/lang/Object;
.source "OAuthSuccessfulResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/OAuthSuccessfulResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final accessToken:Ljava/lang/String;

.field private authenticationToken:Ljava/lang/String;

.field private expiresIn:I

.field private refreshToken:Ljava/lang/String;

.field private scope:Ljava/lang/String;

.field private final tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/microsoft/services/msa/OAuth$TokenType;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->expiresIn:I

    .line 6
    .line 7
    if-eqz p1, :cond_2

    .line 8
    .line 9
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    if-eqz p2, :cond_0

    .line 16
    .line 17
    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->accessToken:Ljava/lang/String;

    .line 18
    .line 19
    iput-object p2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;

    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    .line 23
    .line 24
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    .line 25
    .line 26
    .line 27
    throw p1

    .line 28
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    .line 29
    .line 30
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    .line 31
    .line 32
    .line 33
    throw p1

    .line 34
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    .line 35
    .line 36
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    .line 37
    .line 38
    .line 39
    throw p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic access$100(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->accessToken:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$200(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->authenticationToken:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$300(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Lcom/microsoft/services/msa/OAuth$TokenType;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$400(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->refreshToken:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$500(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->expiresIn:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$600(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->scope:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public authenticationToken(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->authenticationToken:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public build()Lcom/microsoft/services/msa/OAuthSuccessfulResponse;
    .locals 2

    .line 1
    new-instance v0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, v1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;-><init>(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;Lcom/microsoft/services/msa/OAuthSuccessfulResponse$1;)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public expiresIn(I)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->expiresIn:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public refreshToken(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->refreshToken:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public scope(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->scope:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

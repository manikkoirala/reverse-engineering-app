.class Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;
.super Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;
.source "LiveAuthClient.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthRequestObserver;
.implements Lcom/microsoft/services/msa/OAuthResponseVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/LiveAuthClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListenerCallerObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/services/msa/LiveAuthClient;


# direct methods
.method public constructor <init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    .line 2
    .line 3
    invoke-direct {p0, p2, p3}, Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;-><init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public onException(Lcom/microsoft/services/msa/LiveAuthException;)V
    .locals 3

    .line 1
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;->listener:Lcom/microsoft/services/msa/LiveAuthListener;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;->userState:Ljava/lang/Object;

    .line 6
    .line 7
    invoke-direct {v0, v1, v2, p1}, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;-><init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthException;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;->run()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public onResponse(Lcom/microsoft/services/msa/OAuthResponse;)V
    .locals 0

    .line 1
    invoke-interface {p1, p0}, Lcom/microsoft/services/msa/OAuthResponse;->accept(Lcom/microsoft/services/msa/OAuthResponseVisitor;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public visit(Lcom/microsoft/services/msa/OAuthErrorResponse;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthErrorResponse;->getError()Lcom/microsoft/services/msa/OAuth$ErrorType;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthErrorResponse;->getErrorDescription()Ljava/lang/String;

    move-result-object v1

    .line 3
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthErrorResponse;->getErrorUri()Ljava/lang/String;

    move-result-object p1

    .line 4
    new-instance v2, Lcom/microsoft/services/msa/LiveAuthException;

    invoke-direct {v2, v0, v1, p1}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;

    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;->listener:Lcom/microsoft/services/msa/LiveAuthListener;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;->userState:Ljava/lang/Object;

    invoke-direct {p1, v0, v1, v2}, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;-><init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthException;)V

    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;->run()V

    return-void
.end method

.method public visit(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V
    .locals 4

    .line 6
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-static {v0}, Lcom/microsoft/services/msa/LiveAuthClient;->access$000(Lcom/microsoft/services/msa/LiveAuthClient;)Lcom/microsoft/services/msa/LiveConnectSession;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/LiveConnectSession;->loadFromOAuthResponse(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V

    .line 7
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;

    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;->listener:Lcom/microsoft/services/msa/LiveAuthListener;

    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;->userState:Ljava/lang/Object;

    sget-object v2, Lcom/microsoft/services/msa/LiveStatus;->CONNECTED:Lcom/microsoft/services/msa/LiveStatus;

    iget-object v3, p0, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-static {v3}, Lcom/microsoft/services/msa/LiveAuthClient;->access$000(Lcom/microsoft/services/msa/LiveAuthClient;)Lcom/microsoft/services/msa/LiveConnectSession;

    move-result-object v3

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;-><init>(Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveStatus;Lcom/microsoft/services/msa/LiveConnectSession;)V

    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;->run()V

    return-void
.end method

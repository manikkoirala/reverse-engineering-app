.class abstract Lcom/microsoft/services/msa/TokenRequest;
.super Ljava/lang/Object;
.source "TokenRequest.java"


# static fields
.field private static final CONTENT_TYPE:Ljava/lang/String; = "application/x-www-form-urlencoded;charset=UTF-8"


# instance fields
.field protected final client:Lorg/apache/http/client/HttpClient;

.field protected final clientId:Ljava/lang/String;

.field protected final mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;


# direct methods
.method public constructor <init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_2

    .line 5
    .line 6
    if-eqz p2, :cond_1

    .line 7
    .line 8
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    iput-object p1, p0, Lcom/microsoft/services/msa/TokenRequest;->client:Lorg/apache/http/client/HttpClient;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/microsoft/services/msa/TokenRequest;->clientId:Ljava/lang/String;

    .line 17
    .line 18
    iput-object p3, p0, Lcom/microsoft/services/msa/TokenRequest;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    .line 22
    .line 23
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    .line 24
    .line 25
    .line 26
    throw p1

    .line 27
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    .line 28
    .line 29
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    .line 30
    .line 31
    .line 32
    throw p1

    .line 33
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    .line 34
    .line 35
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    .line 36
    .line 37
    .line 38
    throw p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method protected abstract constructBody(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation
.end method

.method public execute()Lcom/microsoft/services/msa/OAuthResponse;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/services/msa/LiveAuthException;
        }
    .end annotation

    .line 1
    const-string v0, "An error occured while communicating with the server during the operation. Please try again later."

    .line 2
    .line 3
    iget-object v1, p0, Lcom/microsoft/services/msa/TokenRequest;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/microsoft/services/msa/OAuthConfig;->getTokenUri()Landroid/net/Uri;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    .line 10
    .line 11
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    new-instance v1, Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .line 22
    .line 23
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    .line 24
    .line 25
    const-string v4, "client_id"

    .line 26
    .line 27
    iget-object v5, p0, Lcom/microsoft/services/msa/TokenRequest;->clientId:Ljava/lang/String;

    .line 28
    .line 29
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, v1}, Lcom/microsoft/services/msa/TokenRequest;->constructBody(Ljava/util/List;)V

    .line 36
    .line 37
    .line 38
    :try_start_0
    new-instance v3, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    .line 39
    .line 40
    const-string v4, "UTF-8"

    .line 41
    .line 42
    invoke-direct {v3, v1, v4}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    const-string v1, "application/x-www-form-urlencoded;charset=UTF-8"

    .line 46
    .line 47
    invoke-virtual {v3, v1}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;->setContentType(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_4

    .line 51
    .line 52
    .line 53
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/services/msa/TokenRequest;->client:Lorg/apache/http/client/HttpClient;

    .line 54
    .line 55
    invoke-interface {v1, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    .line 56
    .line 57
    .line 58
    move-result-object v1
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 59
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    :try_start_2
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 67
    :try_start_3
    new-instance v2, Lorg/json/JSONObject;

    .line 68
    .line 69
    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 70
    .line 71
    .line 72
    invoke-static {v2}, Lcom/microsoft/services/msa/OAuthErrorResponse;->validOAuthErrorResponse(Lorg/json/JSONObject;)Z

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    if-eqz v1, :cond_0

    .line 77
    .line 78
    invoke-static {v2}, Lcom/microsoft/services/msa/OAuthErrorResponse;->createFromJson(Lorg/json/JSONObject;)Lcom/microsoft/services/msa/OAuthErrorResponse;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    return-object v0

    .line 83
    :cond_0
    invoke-static {v2}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->validOAuthSuccessfulResponse(Lorg/json/JSONObject;)Z

    .line 84
    .line 85
    .line 86
    move-result v1

    .line 87
    if-eqz v1, :cond_1

    .line 88
    .line 89
    invoke-static {v2}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->createFromJson(Lorg/json/JSONObject;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    return-object v0

    .line 94
    :cond_1
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    .line 95
    .line 96
    invoke-direct {v1, v0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    throw v1

    .line 100
    :catch_0
    move-exception v1

    .line 101
    new-instance v2, Lcom/microsoft/services/msa/LiveAuthException;

    .line 102
    .line 103
    invoke-direct {v2, v0, v1}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 104
    .line 105
    .line 106
    throw v2

    .line 107
    :catch_1
    move-exception v1

    .line 108
    new-instance v2, Lcom/microsoft/services/msa/LiveAuthException;

    .line 109
    .line 110
    invoke-direct {v2, v0, v1}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 111
    .line 112
    .line 113
    throw v2

    .line 114
    :catch_2
    move-exception v1

    .line 115
    new-instance v2, Lcom/microsoft/services/msa/LiveAuthException;

    .line 116
    .line 117
    invoke-direct {v2, v0, v1}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 118
    .line 119
    .line 120
    throw v2

    .line 121
    :catch_3
    move-exception v1

    .line 122
    new-instance v2, Lcom/microsoft/services/msa/LiveAuthException;

    .line 123
    .line 124
    invoke-direct {v2, v0, v1}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 125
    .line 126
    .line 127
    throw v2

    .line 128
    :catch_4
    move-exception v0

    .line 129
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    .line 130
    .line 131
    const-string v2, "An error occured on the client during the operation."

    .line 132
    .line 133
    invoke-direct {v1, v2, v0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 134
    .line 135
    .line 136
    throw v1
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.class public Lcom/microsoft/services/msa/MicrosoftOAuthConfig;
.super Ljava/lang/Object;
.source "MicrosoftOAuthConfig.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthConfig;


# static fields
.field public static final HTTPS_LOGIN_LIVE_COM:Ljava/lang/String; = "https://login.live.com/"

.field private static sInstance:Lcom/microsoft/services/msa/MicrosoftOAuthConfig;


# instance fields
.field private mOAuthAuthorizeUri:Landroid/net/Uri;

.field private mOAuthDesktopUri:Landroid/net/Uri;

.field private mOAuthLogoutUri:Landroid/net/Uri;

.field private mOAuthTokenUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "https://login.live.com/oauth20_authorize.srf"

    .line 5
    .line 6
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthAuthorizeUri:Landroid/net/Uri;

    .line 11
    .line 12
    const-string v0, "https://login.live.com/oauth20_desktop.srf"

    .line 13
    .line 14
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iput-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthDesktopUri:Landroid/net/Uri;

    .line 19
    .line 20
    const-string v0, "https://login.live.com/oauth20_logout.srf"

    .line 21
    .line 22
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iput-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthLogoutUri:Landroid/net/Uri;

    .line 27
    .line 28
    const-string v0, "https://login.live.com/oauth20_token.srf"

    .line 29
    .line 30
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    iput-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthTokenUri:Landroid/net/Uri;

    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public static getInstance()Lcom/microsoft/services/msa/MicrosoftOAuthConfig;
    .locals 1

    .line 1
    sget-object v0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->sInstance:Lcom/microsoft/services/msa/MicrosoftOAuthConfig;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;-><init>()V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->sInstance:Lcom/microsoft/services/msa/MicrosoftOAuthConfig;

    .line 11
    .line 12
    :cond_0
    sget-object v0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->sInstance:Lcom/microsoft/services/msa/MicrosoftOAuthConfig;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public getAuthorizeUri()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthAuthorizeUri:Landroid/net/Uri;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getDesktopUri()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthDesktopUri:Landroid/net/Uri;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getLogoutUri()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthLogoutUri:Landroid/net/Uri;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getTokenUri()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->mOAuthTokenUri:Landroid/net/Uri;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

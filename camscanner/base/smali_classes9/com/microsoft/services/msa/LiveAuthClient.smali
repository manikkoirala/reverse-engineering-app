.class public Lcom/microsoft/services/msa/LiveAuthClient;
.super Ljava/lang/Object;
.source "LiveAuthClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;,
        Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;,
        Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;,
        Lcom/microsoft/services/msa/LiveAuthClient$AuthListenerCaller;,
        Lcom/microsoft/services/msa/LiveAuthClient$AuthErrorRunnable;,
        Lcom/microsoft/services/msa/LiveAuthClient$AuthCompleteRunnable;
    }
.end annotation


# static fields
.field private static final NULL_LISTENER:Lcom/microsoft/services/msa/LiveAuthListener;

.field private static final TAG:Ljava/lang/String; = "LiveAuthClient"


# instance fields
.field private final applicationContext:Landroid/content/Context;

.field private baseScopes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final clientId:Ljava/lang/String;

.field private hasPendingLoginRequest:Z

.field private httpClient:Lorg/apache/http/client/HttpClient;

.field private final mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

.field private final session:Lcom/microsoft/services/msa/LiveConnectSession;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthClient$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/microsoft/services/msa/LiveAuthClient$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/microsoft/services/msa/LiveAuthClient;->NULL_LISTENER:Lcom/microsoft/services/msa/LiveAuthListener;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/services/msa/LiveAuthClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Iterable;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/microsoft/services/msa/LiveAuthClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Iterable;Lcom/microsoft/services/msa/OAuthConfig;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Iterable;Lcom/microsoft/services/msa/OAuthConfig;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/services/msa/OAuthConfig;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->hasPendingLoginRequest:Z

    .line 4
    new-instance v1, Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-direct {v1, p0}, Lcom/microsoft/services/msa/LiveConnectSession;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;)V

    iput-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    const-string v1, "context"

    .line 5
    invoke-static {p1, v1}, Lcom/microsoft/services/msa/LiveConnectUtils;->assertNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "clientId"

    .line 6
    invoke-static {p2, v1}, Lcom/microsoft/services/msa/LiveConnectUtils;->assertNotNullOrEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->applicationContext:Landroid/content/Context;

    .line 8
    iput-object p2, p0, Lcom/microsoft/services/msa/LiveAuthClient;->clientId:Ljava/lang/String;

    if-nez p4, :cond_0

    .line 9
    invoke-static {}, Lcom/microsoft/services/msa/MicrosoftOAuthConfig;->getInstance()Lcom/microsoft/services/msa/MicrosoftOAuthConfig;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    goto :goto_0

    .line 10
    :cond_0
    iput-object p4, p0, Lcom/microsoft/services/msa/LiveAuthClient;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    :goto_0
    if-nez p3, :cond_1

    new-array p1, v0, [Ljava/lang/String;

    .line 11
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p3

    .line 12
    :cond_1
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    .line 13
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 14
    iget-object p3, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    invoke-interface {p3, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 15
    :cond_2
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    .line 16
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->getRefreshTokenFromPreferences()Ljava/lang/String;

    move-result-object v4

    .line 17
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    const-string p1, " "

    .line 18
    iget-object p2, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    invoke-static {p1, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    .line 19
    new-instance p1, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;

    iget-object v2, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    iget-object v3, p0, Lcom/microsoft/services/msa/LiveAuthClient;->clientId:Ljava/lang/String;

    iget-object v6, p0, Lcom/microsoft/services/msa/LiveAuthClient;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;-><init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V

    .line 20
    new-instance p2, Lcom/microsoft/services/msa/TokenRequestAsync;

    invoke-direct {p2, p1}, Lcom/microsoft/services/msa/TokenRequestAsync;-><init>(Lcom/microsoft/services/msa/TokenRequest;)V

    .line 21
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;

    const/4 p3, 0x0

    invoke-direct {p1, p0, p3}, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthClient$1;)V

    invoke-virtual {p2, p1}, Lcom/microsoft/services/msa/TokenRequestAsync;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    new-array p1, v0, [Ljava/lang/Void;

    .line 22
    invoke-virtual {p2, p1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_3
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/services/msa/LiveAuthClient;)Lcom/microsoft/services/msa/LiveConnectSession;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$100(Lcom/microsoft/services/msa/LiveAuthClient;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->clearRefreshTokenFromPreferences()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$200(Lcom/microsoft/services/msa/LiveAuthClient;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->applicationContext:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$402(Lcom/microsoft/services/msa/LiveAuthClient;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->hasPendingLoginRequest:Z

    .line 2
    .line 3
    return p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private clearRefreshTokenFromPreferences()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->getSharedPreferences()Landroid/content/SharedPreferences;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const-string v1, "refresh_token"

    .line 10
    .line 11
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 12
    .line 13
    .line 14
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    return v0
    .line 19
.end method

.method private getCookieKeysFromPreferences()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->getSharedPreferences()Landroid/content/SharedPreferences;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "cookies"

    .line 6
    .line 7
    const-string v2, ""

    .line 8
    .line 9
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, ","

    .line 14
    .line 15
    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    return-object v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method private getRefreshTokenFromPreferences()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->getSharedPreferences()Landroid/content/SharedPreferences;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "refresh_token"

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->applicationContext:Landroid/content/Context;

    .line 2
    .line 3
    const-string v1, "com.microsoft.live"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public getClientId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->clientId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getSession()Lcom/microsoft/services/msa/LiveConnectSession;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public login(Landroid/app/Activity;Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0, v0, p2}, Lcom/microsoft/services/msa/LiveAuthClient;->login(Landroid/app/Activity;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)V

    return-void
.end method

.method public login(Landroid/app/Activity;Ljava/lang/Iterable;Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/services/msa/LiveAuthListener;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    .line 2
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/services/msa/LiveAuthClient;->login(Landroid/app/Activity;Ljava/lang/Iterable;Ljava/lang/Object;Ljava/lang/String;Lcom/microsoft/services/msa/LiveAuthListener;)V

    return-void
.end method

.method public login(Landroid/app/Activity;Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Object;",
            "Lcom/microsoft/services/msa/LiveAuthListener;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    .line 3
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/services/msa/LiveAuthClient;->login(Landroid/app/Activity;Ljava/lang/Iterable;Ljava/lang/Object;Ljava/lang/String;Lcom/microsoft/services/msa/LiveAuthListener;)V

    return-void
.end method

.method public login(Landroid/app/Activity;Ljava/lang/Iterable;Ljava/lang/Object;Ljava/lang/String;Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Lcom/microsoft/services/msa/LiveAuthListener;",
            ")V"
        }
    .end annotation

    const-string v0, "activity"

    .line 4
    invoke-static {p1, v0}, Lcom/microsoft/services/msa/LiveConnectUtils;->assertNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p5, :cond_0

    .line 5
    sget-object p5, Lcom/microsoft/services/msa/LiveAuthClient;->NULL_LISTENER:Lcom/microsoft/services/msa/LiveAuthListener;

    .line 6
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->hasPendingLoginRequest:Z

    if-nez v0, :cond_3

    if-nez p2, :cond_1

    .line 7
    iget-object p2, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    if-nez p2, :cond_1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/String;

    .line 8
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    .line 9
    :cond_1
    invoke-virtual {p0, p2, p3, p5}, Lcom/microsoft/services/msa/LiveAuthClient;->loginSilent(Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    const-string v0, " "

    .line 10
    invoke-static {v0, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    .line 11
    new-instance p2, Lcom/microsoft/services/msa/AuthorizationRequest;

    iget-object v3, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    iget-object v4, p0, Lcom/microsoft/services/msa/LiveAuthClient;->clientId:Ljava/lang/String;

    iget-object v7, p0, Lcom/microsoft/services/msa/LiveAuthClient;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    move-object v1, p2

    move-object v2, p1

    move-object v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/services/msa/AuthorizationRequest;-><init>(Landroid/app/Activity;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V

    .line 12
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;

    invoke-direct {p1, p0, p5, p3}, Lcom/microsoft/services/msa/LiveAuthClient$ListenerCallerObserver;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;)V

    invoke-virtual {p2, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    .line 13
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;

    const/4 p3, 0x0

    invoke-direct {p1, p0, p3}, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthClient$1;)V

    invoke-virtual {p2, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    .line 14
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthClient$2;

    invoke-direct {p1, p0}, Lcom/microsoft/services/msa/LiveAuthClient$2;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;)V

    invoke-virtual {p2, p1}, Lcom/microsoft/services/msa/AuthorizationRequest;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    const/4 p1, 0x1

    .line 15
    iput-boolean p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->hasPendingLoginRequest:Z

    .line 16
    invoke-virtual {p2}, Lcom/microsoft/services/msa/AuthorizationRequest;->execute()V

    return-void

    .line 17
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Another login operation is already in progress."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public loginSilent(Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0, v0, p1}, Lcom/microsoft/services/msa/LiveAuthClient;->loginSilent(Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public loginSilent(Ljava/lang/Iterable;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/services/msa/LiveAuthListener;",
            ")",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0, p2}, Lcom/microsoft/services/msa/LiveAuthClient;->loginSilent(Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public loginSilent(Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Object;",
            "Lcom/microsoft/services/msa/LiveAuthListener;",
            ")",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .line 4
    iget-boolean v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->hasPendingLoginRequest:Z

    if-nez v0, :cond_4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 5
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->baseScopes:Ljava/util/Set;

    if-nez p1, :cond_0

    new-array p1, v0, [Ljava/lang/String;

    .line 6
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    :cond_0
    move-object v6, p1

    .line 7
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveConnectSession;->getRefreshToken()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 8
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->getRefreshTokenFromPreferences()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setRefreshToken(Ljava/lang/String;)V

    .line 9
    :cond_1
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveConnectSession;->isExpired()Z

    move-result p1

    const/4 v7, 0x1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {p1, v6}, Lcom/microsoft/services/msa/LiveConnectSession;->contains(Ljava/lang/Iterable;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v3, 0x1

    .line 10
    :goto_1
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveConnectSession;->getRefreshToken()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    .line 11
    new-instance v8, Lcom/microsoft/services/msa/LiveAuthClient$3;

    move-object v1, v8

    move-object v2, p0

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/services/msa/LiveAuthClient$3;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;ZLcom/microsoft/services/msa/LiveAuthListener;Ljava/lang/Object;Ljava/lang/Iterable;)V

    new-array p2, v0, [Ljava/lang/Void;

    invoke-virtual {v8, p2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    xor-int/2addr p1, v7

    .line 12
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    .line 13
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Another login operation is already in progress."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public loginSilent(Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x0

    .line 3
    invoke-virtual {p0, v0, p1, p2}, Lcom/microsoft/services/msa/LiveAuthClient;->loginSilent(Ljava/lang/Iterable;Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public logout(Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, v0, p1}, Lcom/microsoft/services/msa/LiveAuthClient;->logout(Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)V

    return-void
.end method

.method public logout(Ljava/lang/Object;Lcom/microsoft/services/msa/LiveAuthListener;)V
    .locals 3

    if-nez p2, :cond_0

    .line 2
    sget-object p2, Lcom/microsoft/services/msa/LiveAuthClient;->NULL_LISTENER:Lcom/microsoft/services/msa/LiveAuthListener;

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setAccessToken(Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setAuthenticationToken(Ljava/lang/String;)V

    .line 5
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setRefreshToken(Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setScopes(Ljava/lang/Iterable;)V

    .line 7
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0, v1}, Lcom/microsoft/services/msa/LiveConnectSession;->setTokenType(Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Lcom/microsoft/services/msa/LiveAuthClient;->clearRefreshTokenFromPreferences()Z

    .line 9
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient;->applicationContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v0

    .line 10
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v2

    .line 11
    invoke-virtual {v2, v1}, Landroid/webkit/CookieManager;->removeAllCookies(Landroid/webkit/ValueCallback;)V

    .line 12
    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 13
    sget-object v0, Lcom/microsoft/services/msa/LiveStatus;->UNKNOWN:Lcom/microsoft/services/msa/LiveStatus;

    invoke-interface {p2, v0, v1, p1}, Lcom/microsoft/services/msa/LiveAuthListener;->onAuthComplete(Lcom/microsoft/services/msa/LiveStatus;Lcom/microsoft/services/msa/LiveConnectSession;Ljava/lang/Object;)V

    return-void
.end method

.method setHttpClient(Lorg/apache/http/client/HttpClient;)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    .line 7
    .line 8
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    .line 9
    .line 10
    .line 11
    throw p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method tryRefresh(Ljava/lang/Iterable;)Ljava/lang/Boolean;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .line 1
    const-string v0, " "

    .line 2
    .line 3
    invoke-static {v0, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v5

    .line 7
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/microsoft/services/msa/LiveConnectSession;->getRefreshToken()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v4

    .line 13
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    if-eqz p1, :cond_0

    .line 18
    .line 19
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 20
    .line 21
    return-object p1

    .line 22
    :cond_0
    new-instance p1, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;

    .line 23
    .line 24
    iget-object v2, p0, Lcom/microsoft/services/msa/LiveAuthClient;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 25
    .line 26
    iget-object v3, p0, Lcom/microsoft/services/msa/LiveAuthClient;->clientId:Ljava/lang/String;

    .line 27
    .line 28
    iget-object v6, p0, Lcom/microsoft/services/msa/LiveAuthClient;->mOAuthConfig:Lcom/microsoft/services/msa/OAuthConfig;

    .line 29
    .line 30
    move-object v1, p1

    .line 31
    invoke-direct/range {v1 .. v6}, Lcom/microsoft/services/msa/RefreshAccessTokenRequest;-><init>(Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/services/msa/OAuthConfig;)V

    .line 32
    .line 33
    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/services/msa/TokenRequest;->execute()Lcom/microsoft/services/msa/OAuthResponse;

    .line 35
    .line 36
    .line 37
    move-result-object p1
    :try_end_0
    .catch Lcom/microsoft/services/msa/LiveAuthException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;

    .line 39
    .line 40
    iget-object v1, p0, Lcom/microsoft/services/msa/LiveAuthClient;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    .line 41
    .line 42
    invoke-direct {v0, v1}, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;-><init>(Lcom/microsoft/services/msa/LiveConnectSession;)V

    .line 43
    .line 44
    .line 45
    invoke-interface {p1, v0}, Lcom/microsoft/services/msa/OAuthResponse;->accept(Lcom/microsoft/services/msa/OAuthResponseVisitor;)V

    .line 46
    .line 47
    .line 48
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;

    .line 49
    .line 50
    const/4 v2, 0x0

    .line 51
    invoke-direct {v1, p0, v2}, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthClient$1;)V

    .line 52
    .line 53
    .line 54
    invoke-interface {p1, v1}, Lcom/microsoft/services/msa/OAuthResponse;->accept(Lcom/microsoft/services/msa/OAuthResponseVisitor;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0}, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->visitedSuccessfulResponse()Z

    .line 58
    .line 59
    .line 60
    move-result p1

    .line 61
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    return-object p1

    .line 66
    :catch_0
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 67
    .line 68
    return-object p1
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

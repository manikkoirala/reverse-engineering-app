.class Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;
.super Ljava/lang/Object;
.source "DefaultObservableOAuthRequest.java"

# interfaces
.implements Lcom/microsoft/services/msa/ObservableOAuthRequest;


# instance fields
.field private final observers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/microsoft/services/msa/OAuthRequestObserver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->observers:Ljava/util/List;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->observers:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public notifyObservers(Lcom/microsoft/services/msa/LiveAuthException;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/services/msa/OAuthRequestObserver;

    .line 2
    invoke-interface {v1, p1}, Lcom/microsoft/services/msa/OAuthRequestObserver;->onException(Lcom/microsoft/services/msa/LiveAuthException;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public notifyObservers(Lcom/microsoft/services/msa/OAuthResponse;)V
    .locals 2

    .line 3
    iget-object v0, p0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/services/msa/OAuthRequestObserver;

    .line 4
    invoke-interface {v1, p1}, Lcom/microsoft/services/msa/OAuthRequestObserver;->onResponse(Lcom/microsoft/services/msa/OAuthResponse;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->observers:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

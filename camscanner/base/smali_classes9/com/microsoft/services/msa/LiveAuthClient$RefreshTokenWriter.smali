.class Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;
.super Ljava/lang/Object;
.source "LiveAuthClient.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthRequestObserver;
.implements Lcom/microsoft/services/msa/OAuthResponseVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/LiveAuthClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RefreshTokenWriter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/services/msa/LiveAuthClient;


# direct methods
.method private constructor <init>(Lcom/microsoft/services/msa/LiveAuthClient;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/services/msa/LiveAuthClient;Lcom/microsoft/services/msa/LiveAuthClient$1;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;-><init>(Lcom/microsoft/services/msa/LiveAuthClient;)V

    return-void
.end method

.method private saveRefreshTokenToPreferences(Ljava/lang/String;)Z
    .locals 3

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    .line 8
    .line 9
    invoke-static {v0}, Lcom/microsoft/services/msa/LiveAuthClient;->access$200(Lcom/microsoft/services/msa/LiveAuthClient;)Landroid/content/Context;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "com.microsoft.live"

    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const-string v1, "refresh_token"

    .line 25
    .line 26
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 27
    .line 28
    .line 29
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    return p1

    .line 34
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    .line 35
    .line 36
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    .line 37
    .line 38
    .line 39
    throw p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method


# virtual methods
.method public onException(Lcom/microsoft/services/msa/LiveAuthException;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public onResponse(Lcom/microsoft/services/msa/OAuthResponse;)V
    .locals 0

    .line 1
    invoke-interface {p1, p0}, Lcom/microsoft/services/msa/OAuthResponse;->accept(Lcom/microsoft/services/msa/OAuthResponseVisitor;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public visit(Lcom/microsoft/services/msa/OAuthErrorResponse;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthErrorResponse;->getError()Lcom/microsoft/services/msa/OAuth$ErrorType;

    move-result-object p1

    sget-object v0, Lcom/microsoft/services/msa/OAuth$ErrorType;->INVALID_GRANT:Lcom/microsoft/services/msa/OAuth$ErrorType;

    if-ne p1, v0, :cond_0

    .line 2
    iget-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;->this$0:Lcom/microsoft/services/msa/LiveAuthClient;

    invoke-static {p1}, Lcom/microsoft/services/msa/LiveAuthClient;->access$100(Lcom/microsoft/services/msa/LiveAuthClient;)Z

    :cond_0
    return-void
.end method

.method public visit(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V
    .locals 1

    .line 3
    invoke-virtual {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->getRefreshToken()Ljava/lang/String;

    move-result-object p1

    .line 4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5
    invoke-direct {p0, p1}, Lcom/microsoft/services/msa/LiveAuthClient$RefreshTokenWriter;->saveRefreshTokenToPreferences(Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

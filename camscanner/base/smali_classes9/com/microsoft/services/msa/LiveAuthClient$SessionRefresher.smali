.class Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;
.super Ljava/lang/Object;
.source "LiveAuthClient.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthResponseVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/LiveAuthClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SessionRefresher"
.end annotation


# instance fields
.field private final session:Lcom/microsoft/services/msa/LiveConnectSession;

.field private visitedSuccessfulResponse:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/services/msa/LiveConnectSession;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    iput-boolean p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->visitedSuccessfulResponse:Z

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    .line 13
    .line 14
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    .line 15
    .line 16
    .line 17
    throw p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public visit(Lcom/microsoft/services/msa/OAuthErrorResponse;)V
    .locals 0

    const/4 p1, 0x0

    .line 1
    iput-boolean p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->visitedSuccessfulResponse:Z

    return-void
.end method

.method public visit(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->session:Lcom/microsoft/services/msa/LiveConnectSession;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/LiveConnectSession;->loadFromOAuthResponse(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V

    const/4 p1, 0x1

    .line 3
    iput-boolean p1, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->visitedSuccessfulResponse:Z

    return-void
.end method

.method public visitedSuccessfulResponse()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/microsoft/services/msa/LiveAuthClient$SessionRefresher;->visitedSuccessfulResponse:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

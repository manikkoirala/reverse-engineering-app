.class Lcom/microsoft/services/msa/OAuthSuccessfulResponse;
.super Ljava/lang/Object;
.source "OAuthSuccessfulResponse.java"

# interfaces
.implements Lcom/microsoft/services/msa/OAuthResponse;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;
    }
.end annotation


# static fields
.field private static final UNINITIALIZED:I = -0x1


# instance fields
.field private final accessToken:Ljava/lang/String;

.field private final authenticationToken:Ljava/lang/String;

.field private final expiresIn:I

.field private final refreshToken:Ljava/lang/String;

.field private final scope:Ljava/lang/String;

.field private final tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;


# direct methods
.method private constructor <init>(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$100(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->accessToken:Ljava/lang/String;

    .line 4
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$200(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->authenticationToken:Ljava/lang/String;

    .line 5
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$300(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Lcom/microsoft/services/msa/OAuth$TokenType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;

    .line 6
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$400(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->refreshToken:Ljava/lang/String;

    .line 7
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$500(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)I

    move-result v0

    iput v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->expiresIn:I

    .line 8
    invoke-static {p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->access$600(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->scope:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;Lcom/microsoft/services/msa/OAuthSuccessfulResponse$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;-><init>(Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;)V

    return-void
.end method

.method public static createFromFragment(Ljava/util/Map;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/services/msa/OAuthSuccessfulResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/services/msa/LiveAuthException;
        }
    .end annotation

    .line 1
    const-string v0, "An error occured while communicating with the server during the operation. Please try again later."

    .line 2
    .line 3
    const-string v1, "access_token"

    .line 4
    .line 5
    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    check-cast v1, Ljava/lang/String;

    .line 10
    .line 11
    const-string/jumbo v2, "token_type"

    .line 12
    .line 13
    .line 14
    invoke-interface {p0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    check-cast v2, Ljava/lang/String;

    .line 19
    .line 20
    if-eqz v1, :cond_4

    .line 21
    .line 22
    if-eqz v2, :cond_3

    .line 23
    .line 24
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-static {v2}, Lcom/microsoft/services/msa/OAuth$TokenType;->valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuth$TokenType;

    .line 29
    .line 30
    .line 31
    move-result-object v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 32
    new-instance v3, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    .line 33
    .line 34
    invoke-direct {v3, v1, v2}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;-><init>(Ljava/lang/String;Lcom/microsoft/services/msa/OAuth$TokenType;)V

    .line 35
    .line 36
    .line 37
    const-string v1, "authentication_token"

    .line 38
    .line 39
    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    check-cast v1, Ljava/lang/String;

    .line 44
    .line 45
    if-eqz v1, :cond_0

    .line 46
    .line 47
    invoke-virtual {v3, v1}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->authenticationToken(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    .line 48
    .line 49
    .line 50
    :cond_0
    const-string v1, "expires_in"

    .line 51
    .line 52
    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    check-cast v1, Ljava/lang/String;

    .line 57
    .line 58
    if-eqz v1, :cond_1

    .line 59
    .line 60
    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 61
    .line 62
    .line 63
    move-result v0
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 64
    invoke-virtual {v3, v0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->expiresIn(I)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :catch_0
    move-exception p0

    .line 69
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    .line 70
    .line 71
    invoke-direct {v1, v0, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 72
    .line 73
    .line 74
    throw v1

    .line 75
    :cond_1
    :goto_0
    const-string v0, "scope"

    .line 76
    .line 77
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object p0

    .line 81
    check-cast p0, Ljava/lang/String;

    .line 82
    .line 83
    if-eqz p0, :cond_2

    .line 84
    .line 85
    invoke-virtual {v3, p0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->scope(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    .line 86
    .line 87
    .line 88
    :cond_2
    invoke-virtual {v3}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->build()Lcom/microsoft/services/msa/OAuthSuccessfulResponse;

    .line 89
    .line 90
    .line 91
    move-result-object p0

    .line 92
    return-object p0

    .line 93
    :catch_1
    move-exception p0

    .line 94
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    .line 95
    .line 96
    invoke-direct {v1, v0, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 97
    .line 98
    .line 99
    throw v1

    .line 100
    :cond_3
    new-instance p0, Ljava/lang/AssertionError;

    .line 101
    .line 102
    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    .line 103
    .line 104
    .line 105
    throw p0

    .line 106
    :cond_4
    new-instance p0, Ljava/lang/AssertionError;

    .line 107
    .line 108
    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    .line 109
    .line 110
    .line 111
    throw p0
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method public static createFromJson(Lorg/json/JSONObject;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/services/msa/LiveAuthException;
        }
    .end annotation

    .line 1
    const-string v0, "An error occured while communicating with the server during the operation. Please try again later."

    .line 2
    .line 3
    invoke-static {p0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->validOAuthSuccessfulResponse(Lorg/json/JSONObject;)Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-eqz v1, :cond_4

    .line 8
    .line 9
    :try_start_0
    const-string v1, "access_token"

    .line 10
    .line 11
    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_7

    .line 15
    :try_start_1
    const-string/jumbo v2, "token_type"

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_6

    .line 22
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-static {v2}, Lcom/microsoft/services/msa/OAuth$TokenType;->valueOf(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuth$TokenType;

    .line 27
    .line 28
    .line 29
    move-result-object v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_4

    .line 30
    new-instance v2, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    .line 31
    .line 32
    invoke-direct {v2, v1, v0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;-><init>(Ljava/lang/String;Lcom/microsoft/services/msa/OAuth$TokenType;)V

    .line 33
    .line 34
    .line 35
    const-string v0, "authentication_token"

    .line 36
    .line 37
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    const-string v3, "An error occured on the client during the operation."

    .line 42
    .line 43
    if-eqz v1, :cond_0

    .line 44
    .line 45
    :try_start_3
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 49
    invoke-virtual {v2, v0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->authenticationToken(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :catch_0
    move-exception p0

    .line 54
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    .line 55
    .line 56
    invoke-direct {v0, v3, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 57
    .line 58
    .line 59
    throw v0

    .line 60
    :cond_0
    :goto_0
    const-string v0, "refresh_token"

    .line 61
    .line 62
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    if-eqz v1, :cond_1

    .line 67
    .line 68
    :try_start_4
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v0
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1

    .line 72
    invoke-virtual {v2, v0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->refreshToken(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    .line 73
    .line 74
    .line 75
    goto :goto_1

    .line 76
    :catch_1
    move-exception p0

    .line 77
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    .line 78
    .line 79
    invoke-direct {v0, v3, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 80
    .line 81
    .line 82
    throw v0

    .line 83
    :cond_1
    :goto_1
    const-string v0, "expires_in"

    .line 84
    .line 85
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    .line 86
    .line 87
    .line 88
    move-result v1

    .line 89
    if-eqz v1, :cond_2

    .line 90
    .line 91
    :try_start_5
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    .line 92
    .line 93
    .line 94
    move-result v0
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_2

    .line 95
    invoke-virtual {v2, v0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->expiresIn(I)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    .line 96
    .line 97
    .line 98
    goto :goto_2

    .line 99
    :catch_2
    move-exception p0

    .line 100
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    .line 101
    .line 102
    invoke-direct {v0, v3, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 103
    .line 104
    .line 105
    throw v0

    .line 106
    :cond_2
    :goto_2
    const-string v0, "scope"

    .line 107
    .line 108
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    .line 109
    .line 110
    .line 111
    move-result v1

    .line 112
    if-eqz v1, :cond_3

    .line 113
    .line 114
    :try_start_6
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object p0
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_3

    .line 118
    invoke-virtual {v2, p0}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->scope(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;

    .line 119
    .line 120
    .line 121
    goto :goto_3

    .line 122
    :catch_3
    move-exception p0

    .line 123
    new-instance v0, Lcom/microsoft/services/msa/LiveAuthException;

    .line 124
    .line 125
    invoke-direct {v0, v3, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 126
    .line 127
    .line 128
    throw v0

    .line 129
    :cond_3
    :goto_3
    invoke-virtual {v2}, Lcom/microsoft/services/msa/OAuthSuccessfulResponse$Builder;->build()Lcom/microsoft/services/msa/OAuthSuccessfulResponse;

    .line 130
    .line 131
    .line 132
    move-result-object p0

    .line 133
    return-object p0

    .line 134
    :catch_4
    move-exception p0

    .line 135
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    .line 136
    .line 137
    invoke-direct {v1, v0, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 138
    .line 139
    .line 140
    throw v1

    .line 141
    :catch_5
    move-exception p0

    .line 142
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    .line 143
    .line 144
    invoke-direct {v1, v0, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 145
    .line 146
    .line 147
    throw v1

    .line 148
    :catch_6
    move-exception p0

    .line 149
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    .line 150
    .line 151
    invoke-direct {v1, v0, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 152
    .line 153
    .line 154
    throw v1

    .line 155
    :catch_7
    move-exception p0

    .line 156
    new-instance v1, Lcom/microsoft/services/msa/LiveAuthException;

    .line 157
    .line 158
    invoke-direct {v1, v0, p0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 159
    .line 160
    .line 161
    throw v1

    .line 162
    :cond_4
    new-instance p0, Ljava/lang/AssertionError;

    .line 163
    .line 164
    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    .line 165
    .line 166
    .line 167
    throw p0
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method public static validOAuthSuccessfulResponse(Lorg/json/JSONObject;)Z
    .locals 1

    .line 1
    const-string v0, "access_token"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string/jumbo v0, "token_type"

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    .line 13
    .line 14
    .line 15
    move-result p0

    .line 16
    if-eqz p0, :cond_0

    .line 17
    .line 18
    const/4 p0, 0x1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 p0, 0x0

    .line 21
    :goto_0
    return p0
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public accept(Lcom/microsoft/services/msa/OAuthResponseVisitor;)V
    .locals 0

    .line 1
    invoke-interface {p1, p0}, Lcom/microsoft/services/msa/OAuthResponseVisitor;->visit(Lcom/microsoft/services/msa/OAuthSuccessfulResponse;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->accessToken:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getAuthenticationToken()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->authenticationToken:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getExpiresIn()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->expiresIn:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getRefreshToken()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->refreshToken:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getScope()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->scope:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getTokenType()Lcom/microsoft/services/msa/OAuth$TokenType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public hasAuthenticationToken()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->authenticationToken:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public hasExpiresIn()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->expiresIn:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-eq v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public hasRefreshToken()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->refreshToken:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public hasScope()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->scope:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    const/4 v0, 0x6

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->accessToken:Ljava/lang/String;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->authenticationToken:Ljava/lang/String;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->tokenType:Lcom/microsoft/services/msa/OAuth$TokenType;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    const/4 v1, 0x3

    .line 20
    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->refreshToken:Ljava/lang/String;

    .line 21
    .line 22
    aput-object v2, v0, v1

    .line 23
    .line 24
    iget v1, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->expiresIn:I

    .line 25
    .line 26
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    const/4 v2, 0x4

    .line 31
    aput-object v1, v0, v2

    .line 32
    .line 33
    const/4 v1, 0x5

    .line 34
    iget-object v2, p0, Lcom/microsoft/services/msa/OAuthSuccessfulResponse;->scope:Ljava/lang/String;

    .line 35
    .line 36
    aput-object v2, v0, v1

    .line 37
    .line 38
    const-string v1, "OAuthSuccessfulResponse [accessToken=%s, authenticationToken=%s, tokenType=%s, refreshToken=%s, expiresIn=%s, scope=%s]"

    .line 39
    .line 40
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

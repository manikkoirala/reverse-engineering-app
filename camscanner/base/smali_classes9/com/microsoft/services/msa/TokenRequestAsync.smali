.class Lcom/microsoft/services/msa/TokenRequestAsync;
.super Landroid/os/AsyncTask;
.source "TokenRequestAsync.java"

# interfaces
.implements Lcom/microsoft/services/msa/ObservableOAuthRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/microsoft/services/msa/ObservableOAuthRequest;"
    }
.end annotation


# instance fields
.field private exception:Lcom/microsoft/services/msa/LiveAuthException;

.field private final observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

.field private final request:Lcom/microsoft/services/msa/TokenRequest;

.field private response:Lcom/microsoft/services/msa/OAuthResponse;


# direct methods
.method public constructor <init>(Lcom/microsoft/services/msa/TokenRequest;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    new-instance v0, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    .line 12
    .line 13
    iput-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->request:Lcom/microsoft/services/msa/TokenRequest;

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    .line 17
    .line 18
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    .line 19
    .line 20
    .line 21
    throw p1
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->addObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/services/msa/TokenRequestAsync;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 0

    .line 2
    :try_start_0
    iget-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->request:Lcom/microsoft/services/msa/TokenRequest;

    invoke-virtual {p1}, Lcom/microsoft/services/msa/TokenRequest;->execute()Lcom/microsoft/services/msa/OAuthResponse;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->response:Lcom/microsoft/services/msa/OAuthResponse;
    :try_end_0
    .catch Lcom/microsoft/services/msa/LiveAuthException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 3
    iput-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->exception:Lcom/microsoft/services/msa/LiveAuthException;

    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/services/msa/TokenRequestAsync;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    .line 2
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 3
    iget-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->response:Lcom/microsoft/services/msa/OAuthResponse;

    if-eqz p1, :cond_0

    .line 4
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->notifyObservers(Lcom/microsoft/services/msa/OAuthResponse;)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->exception:Lcom/microsoft/services/msa/LiveAuthException;

    if-eqz p1, :cond_1

    .line 6
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->notifyObservers(Lcom/microsoft/services/msa/LiveAuthException;)V

    goto :goto_0

    .line 7
    :cond_1
    new-instance p1, Lcom/microsoft/services/msa/LiveAuthException;

    const-string v0, "An error occured on the client during the operation."

    invoke-direct {p1, v0}, Lcom/microsoft/services/msa/LiveAuthException;-><init>(Ljava/lang/String;)V

    .line 8
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->notifyObservers(Lcom/microsoft/services/msa/LiveAuthException;)V

    :goto_0
    return-void
.end method

.method public removeObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/TokenRequestAsync;->observerable:Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/microsoft/services/msa/DefaultObservableOAuthRequest;->removeObserver(Lcom/microsoft/services/msa/OAuthRequestObserver;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

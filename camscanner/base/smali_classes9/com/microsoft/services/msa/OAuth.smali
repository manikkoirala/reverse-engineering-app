.class final Lcom/microsoft/services/msa/OAuth;
.super Ljava/lang/Object;
.source "OAuth.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/services/msa/OAuth$TokenType;,
        Lcom/microsoft/services/msa/OAuth$ResponseType;,
        Lcom/microsoft/services/msa/OAuth$GrantType;,
        Lcom/microsoft/services/msa/OAuth$ErrorType;,
        Lcom/microsoft/services/msa/OAuth$DisplayType;
    }
.end annotation


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field public static final AUTHENTICATION_TOKEN:Ljava/lang/String; = "authentication_token"

.field public static final CLIENT_ID:Ljava/lang/String; = "client_id"

.field public static final CODE:Ljava/lang/String; = "code"

.field public static final DISPLAY:Ljava/lang/String; = "display"

.field public static final ERROR:Ljava/lang/String; = "error"

.field public static final ERROR_DESCRIPTION:Ljava/lang/String; = "error_description"

.field public static final ERROR_URI:Ljava/lang/String; = "error_uri"

.field public static final EXPIRES_IN:Ljava/lang/String; = "expires_in"

.field public static final GRANT_TYPE:Ljava/lang/String; = "grant_type"

.field public static final LOCALE:Ljava/lang/String; = "locale"

.field public static final LOGIN_HINT:Ljava/lang/String; = "login_hint"

.field public static final REDIRECT_URI:Ljava/lang/String; = "redirect_uri"

.field public static final REFRESH_TOKEN:Ljava/lang/String; = "refresh_token"

.field public static final RESPONSE_TYPE:Ljava/lang/String; = "response_type"

.field public static final SCOPE:Ljava/lang/String; = "scope"

.field public static final SCOPE_DELIMITER:Ljava/lang/String; = " "

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final THEME:Ljava/lang/String; = "theme"

.field public static final TOKEN_TYPE:Ljava/lang/String; = "token_type"

.field public static final USER_NAME:Ljava/lang/String; = "username"


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/lang/AssertionError;

    .line 5
    .line 6
    const-string v1, "Non-instantiable class"

    .line 7
    .line 8
    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    throw v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

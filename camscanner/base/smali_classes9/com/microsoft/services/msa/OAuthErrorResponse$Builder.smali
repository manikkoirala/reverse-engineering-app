.class public Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;
.super Ljava/lang/Object;
.source "OAuthErrorResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/services/msa/OAuthErrorResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final error:Lcom/microsoft/services/msa/OAuth$ErrorType;

.field private errorDescription:Ljava/lang/String;

.field private errorUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/services/msa/OAuth$ErrorType;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->error:Lcom/microsoft/services/msa/OAuth$ErrorType;

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    .line 10
    .line 11
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    .line 12
    .line 13
    .line 14
    throw p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$100(Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;)Lcom/microsoft/services/msa/OAuth$ErrorType;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->error:Lcom/microsoft/services/msa/OAuth$ErrorType;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$200(Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->errorDescription:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$300(Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->errorUri:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public build()Lcom/microsoft/services/msa/OAuthErrorResponse;
    .locals 2

    .line 1
    new-instance v0, Lcom/microsoft/services/msa/OAuthErrorResponse;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, v1}, Lcom/microsoft/services/msa/OAuthErrorResponse;-><init>(Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;Lcom/microsoft/services/msa/OAuthErrorResponse$1;)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public errorDescription(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->errorDescription:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public errorUri(Ljava/lang/String;)Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/services/msa/OAuthErrorResponse$Builder;->errorUri:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

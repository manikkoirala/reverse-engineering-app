.class public Lcom/microsoft/services/msa/LiveAuthException;
.super Ljava/lang/RuntimeException;
.source "LiveAuthException.java"


# static fields
.field private static final serialVersionUID:J = 0x2ebff25dba7416c8L


# instance fields
.field private final error:Ljava/lang/String;

.field private final errorUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    const-string p1, ""

    .line 2
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->error:Ljava/lang/String;

    .line 3
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->errorUri:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 7
    invoke-direct {p0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 8
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->error:Ljava/lang/String;

    .line 9
    iput-object p3, p0, Lcom/microsoft/services/msa/LiveAuthException;->errorUri:Ljava/lang/String;

    return-void

    .line 10
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 11
    invoke-direct {p0, p2, p4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    if-eqz p1, :cond_0

    .line 12
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->error:Ljava/lang/String;

    .line 13
    iput-object p3, p0, Lcom/microsoft/services/msa/LiveAuthException;->errorUri:Ljava/lang/String;

    return-void

    .line 14
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string p1, ""

    .line 5
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->error:Ljava/lang/String;

    .line 6
    iput-object p1, p0, Lcom/microsoft/services/msa/LiveAuthException;->errorUri:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getError()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthException;->error:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getErrorUri()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/services/msa/LiveAuthException;->errorUri:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

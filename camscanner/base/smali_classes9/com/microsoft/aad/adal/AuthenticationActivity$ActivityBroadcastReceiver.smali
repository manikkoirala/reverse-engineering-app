.class Lcom/microsoft/aad/adal/AuthenticationActivity$ActivityBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AuthenticationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/aad/adal/AuthenticationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActivityBroadcastReceiver"
.end annotation


# instance fields
.field private mWaitingRequestId:I

.field final synthetic this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;


# direct methods
.method private constructor <init>(Lcom/microsoft/aad/adal/AuthenticationActivity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$ActivityBroadcastReceiver;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 p1, -0x1

    .line 2
    iput p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$ActivityBroadcastReceiver;->mWaitingRequestId:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/aad/adal/AuthenticationActivity;Lcom/microsoft/aad/adal/AuthenticationActivity$1;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AuthenticationActivity$ActivityBroadcastReceiver;-><init>(Lcom/microsoft/aad/adal/AuthenticationActivity;)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/aad/adal/AuthenticationActivity$ActivityBroadcastReceiver;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$ActivityBroadcastReceiver;->mWaitingRequestId:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$102(Lcom/microsoft/aad/adal/AuthenticationActivity$ActivityBroadcastReceiver;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$ActivityBroadcastReceiver;->mWaitingRequestId:I

    .line 2
    .line 3
    return p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .line 1
    const-string p1, "ActivityBroadcastReceiver onReceive"

    .line 2
    .line 3
    const-string v0, "AuthenticationActivity:onReceive"

    .line 4
    .line 5
    invoke-static {v0, p1}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇O00(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const-string v1, "com.microsoft.aad.adal:BrowserCancel"

    .line 19
    .line 20
    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    if-eqz p1, :cond_0

    .line 25
    .line 26
    const-string p1, "ActivityBroadcastReceiver onReceive action is for cancelling Authentication Activity"

    .line 27
    .line 28
    invoke-static {v0, p1}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇O00(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const-string p1, "com.microsoft.aad.adal:RequestId"

    .line 32
    .line 33
    const/4 v1, 0x0

    .line 34
    invoke-virtual {p2, p1, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    iget p2, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$ActivityBroadcastReceiver;->mWaitingRequestId:I

    .line 39
    .line 40
    if-ne p1, p2, :cond_0

    .line 41
    .line 42
    const-string p1, "Waiting requestId is same and cancelling this activity"

    .line 43
    .line 44
    invoke-static {v0, p1}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇O00(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    iget-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$ActivityBroadcastReceiver;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationActivity;->finish()V

    .line 50
    .line 51
    .line 52
    :cond_0
    return-void
.end method

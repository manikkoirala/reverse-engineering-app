.class Lcom/microsoft/aad/adal/DefaultEvent;
.super Ljava/lang/Object;
.source "DefaultEvent.java"

# interfaces
.implements Lcom/microsoft/aad/adal/IEvents;


# static fields
.field private static final EVENT_LIST_SIZE:I = 0x1e

.field private static sApplicationName:Ljava/lang/String; = null

.field private static sApplicationVersion:Ljava/lang/String; = "NA"

.field private static sClientId:Ljava/lang/String; = "NA"

.field private static sDeviceId:Ljava/lang/String; = "NA"


# instance fields
.field private mDefaultEventCount:I

.field private final mEventList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mRequestId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    const/16 v1, 0x1e

    .line 7
    .line 8
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mEventList:Ljava/util/List;

    .line 12
    .line 13
    sget-object v1, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationName:Ljava/lang/String;

    .line 14
    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    const-string v2, "Microsoft.ADAL.application_name"

    .line 18
    .line 19
    invoke-virtual {p0, v2, v1}, Lcom/microsoft/aad/adal/DefaultEvent;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const-string v1, "Microsoft.ADAL.application_version"

    .line 23
    .line 24
    sget-object v2, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationVersion:Ljava/lang/String;

    .line 25
    .line 26
    invoke-virtual {p0, v1, v2}, Lcom/microsoft/aad/adal/DefaultEvent;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    const-string v1, "Microsoft.ADAL.client_id"

    .line 30
    .line 31
    sget-object v2, Lcom/microsoft/aad/adal/DefaultEvent;->sClientId:Ljava/lang/String;

    .line 32
    .line 33
    invoke-virtual {p0, v1, v2}, Lcom/microsoft/aad/adal/DefaultEvent;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    const-string v1, "Microsoft.ADAL.device_id"

    .line 37
    .line 38
    sget-object v2, Lcom/microsoft/aad/adal/DefaultEvent;->sDeviceId:Ljava/lang/String;

    .line 39
    .line 40
    invoke-virtual {p0, v1, v2}, Lcom/microsoft/aad/adal/DefaultEvent;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    iput v0, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mDefaultEventCount:I

    .line 48
    .line 49
    :cond_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method static isPrivacyCompliant(Ljava/lang/String;)Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/microsoft/aad/adal/Telemetry;->getAllowPii()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    sget-object v0, Lcom/microsoft/aad/adal/TelemetryUtils;->GDPR_FILTERED_FIELDS:Ljava/util/Set;

    .line 8
    .line 9
    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result p0

    .line 13
    if-nez p0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p0, 0x0

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    :goto_0
    const/4 p0, 0x1

    .line 19
    :goto_1
    return p0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public getDefaultEventCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mDefaultEventCount:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getEventList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mEventList:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getEvents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mEventList:Ljava/util/List;

    .line 2
    .line 3
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getTelemetryRequestId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mRequestId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public processEvent(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationName:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v0, "Microsoft.ADAL.application_name"

    .line 6
    .line 7
    invoke-static {v0}, Lcom/microsoft/aad/adal/DefaultEvent;->isPrivacyCompliant(Ljava/lang/String;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    sget-object v1, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationName:Ljava/lang/String;

    .line 14
    .line 15
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    :cond_0
    sget-object v0, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationVersion:Ljava/lang/String;

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    const-string v0, "Microsoft.ADAL.application_version"

    .line 23
    .line 24
    invoke-static {v0}, Lcom/microsoft/aad/adal/DefaultEvent;->isPrivacyCompliant(Ljava/lang/String;)Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_1

    .line 29
    .line 30
    sget-object v1, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationVersion:Ljava/lang/String;

    .line 31
    .line 32
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    :cond_1
    sget-object v0, Lcom/microsoft/aad/adal/DefaultEvent;->sClientId:Ljava/lang/String;

    .line 36
    .line 37
    if-eqz v0, :cond_2

    .line 38
    .line 39
    const-string v0, "Microsoft.ADAL.client_id"

    .line 40
    .line 41
    invoke-static {v0}, Lcom/microsoft/aad/adal/DefaultEvent;->isPrivacyCompliant(Ljava/lang/String;)Z

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    if-eqz v1, :cond_2

    .line 46
    .line 47
    sget-object v1, Lcom/microsoft/aad/adal/DefaultEvent;->sClientId:Ljava/lang/String;

    .line 48
    .line 49
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    :cond_2
    sget-object v0, Lcom/microsoft/aad/adal/DefaultEvent;->sDeviceId:Ljava/lang/String;

    .line 53
    .line 54
    if-eqz v0, :cond_3

    .line 55
    .line 56
    const-string v0, "Microsoft.ADAL.device_id"

    .line 57
    .line 58
    invoke-static {v0}, Lcom/microsoft/aad/adal/DefaultEvent;->isPrivacyCompliant(Ljava/lang/String;)Z

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    if-eqz v1, :cond_3

    .line 63
    .line 64
    sget-object v1, Lcom/microsoft/aad/adal/DefaultEvent;->sDeviceId:Ljava/lang/String;

    .line 65
    .line 66
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    .line 68
    .line 69
    :cond_3
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method setCorrelationId(Ljava/lang/String;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mEventList:Ljava/util/List;

    .line 2
    .line 3
    new-instance v1, Ljava/util/AbstractMap$SimpleEntry;

    .line 4
    .line 5
    const-string v2, "Microsoft.ADAL.correlation_id"

    .line 6
    .line 7
    invoke-direct {v1, v2, p1}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    const/4 p1, 0x0

    .line 11
    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 12
    .line 13
    .line 14
    iget p1, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mDefaultEventCount:I

    .line 15
    .line 16
    add-int/lit8 p1, p1, 0x1

    .line 17
    .line 18
    iput p1, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mDefaultEventCount:I

    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setDefaults(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HardwareIds"
        }
    .end annotation

    .line 1
    sput-object p2, Lcom/microsoft/aad/adal/DefaultEvent;->sClientId:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    sput-object p2, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationName:Ljava/lang/String;

    .line 8
    .line 9
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    .line 10
    .line 11
    .line 12
    move-result-object p2

    .line 13
    sget-object v0, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationName:Ljava/lang/String;

    .line 14
    .line 15
    const/4 v1, 0x0

    .line 16
    invoke-virtual {p2, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 17
    .line 18
    .line 19
    move-result-object p2

    .line 20
    iget-object p2, p2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 21
    .line 22
    sput-object p2, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationVersion:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :catch_0
    const-string p2, "NA"

    .line 26
    .line 27
    sput-object p2, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationVersion:Ljava/lang/String;

    .line 28
    .line 29
    :goto_0
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    const-string p2, "android_id"

    .line 34
    .line 35
    invoke-static {p1, p2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-static {p1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->〇o00〇〇Oo(Ljava/lang/String;)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    sput-object p1, Lcom/microsoft/aad/adal/DefaultEvent;->sDeviceId:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :catch_1
    const-string p1, ""

    .line 47
    .line 48
    sput-object p1, Lcom/microsoft/aad/adal/DefaultEvent;->sDeviceId:Ljava/lang/String;

    .line 49
    .line 50
    :goto_1
    iget p1, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mDefaultEventCount:I

    .line 51
    .line 52
    if-nez p1, :cond_0

    .line 53
    .line 54
    const-string p1, "Microsoft.ADAL.application_name"

    .line 55
    .line 56
    sget-object p2, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationName:Ljava/lang/String;

    .line 57
    .line 58
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/aad/adal/DefaultEvent;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    const-string p1, "Microsoft.ADAL.application_version"

    .line 62
    .line 63
    sget-object p2, Lcom/microsoft/aad/adal/DefaultEvent;->sApplicationVersion:Ljava/lang/String;

    .line 64
    .line 65
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/aad/adal/DefaultEvent;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    const-string p1, "Microsoft.ADAL.client_id"

    .line 69
    .line 70
    sget-object p2, Lcom/microsoft/aad/adal/DefaultEvent;->sClientId:Ljava/lang/String;

    .line 71
    .line 72
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/aad/adal/DefaultEvent;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    const-string p1, "Microsoft.ADAL.device_id"

    .line 76
    .line 77
    sget-object p2, Lcom/microsoft/aad/adal/DefaultEvent;->sDeviceId:Ljava/lang/String;

    .line 78
    .line 79
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/aad/adal/DefaultEvent;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    iget-object p1, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mEventList:Ljava/util/List;

    .line 83
    .line 84
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 85
    .line 86
    .line 87
    move-result p1

    .line 88
    iput p1, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mDefaultEventCount:I

    .line 89
    .line 90
    :cond_0
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_2

    .line 6
    .line 7
    if-eqz p2, :cond_1

    .line 8
    .line 9
    invoke-static {p1}, Lcom/microsoft/aad/adal/DefaultEvent;->isPrivacyCompliant(Ljava/lang/String;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mEventList:Ljava/util/List;

    .line 17
    .line 18
    new-instance v1, Ljava/util/AbstractMap$SimpleEntry;

    .line 19
    .line 20
    invoke-direct {v1, p1, p2}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    :cond_1
    :goto_0
    return-void

    .line 27
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 28
    .line 29
    const-string p2, "Telemetry setProperty on null name"

    .line 30
    .line 31
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    throw p1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method setRequestId(Ljava/lang/String;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mRequestId:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mEventList:Ljava/util/List;

    .line 4
    .line 5
    new-instance v1, Ljava/util/AbstractMap$SimpleEntry;

    .line 6
    .line 7
    const-string v2, "Microsoft.ADAL.request_id"

    .line 8
    .line 9
    invoke-direct {v1, v2, p1}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 10
    .line 11
    .line 12
    const/4 p1, 0x0

    .line 13
    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    iget p1, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mDefaultEventCount:I

    .line 17
    .line 18
    add-int/lit8 p1, p1, 0x1

    .line 19
    .line 20
    iput p1, p0, Lcom/microsoft/aad/adal/DefaultEvent;->mDefaultEventCount:I

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
.end method

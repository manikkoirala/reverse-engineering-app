.class Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;
.super Ljava/lang/Object;
.source "AcquireTokenSilentHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AcquireTokenSilentHandler"


# instance fields
.field private mAttemptedWithMRRT:Z

.field private final mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

.field private final mContext:Landroid/content/Context;

.field private mMrrtTokenCacheItem:Lcom/microsoft/aad/adal/TokenCacheItem;

.field private final mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

.field private mWebRequestHandler:Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method constructor <init>(Landroid/content/Context;Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/TokenCacheAccessor;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAttemptedWithMRRT:Z

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mWebRequestHandler:Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;

    .line 9
    .line 10
    if-eqz p1, :cond_1

    .line 11
    .line 12
    if-eqz p2, :cond_0

    .line 13
    .line 14
    iput-object p1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mContext:Landroid/content/Context;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 17
    .line 18
    iput-object p3, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 19
    .line 20
    new-instance p1, Lcom/microsoft/identity/common/adal/internal/net/WebRequestHandler;

    .line 21
    .line 22
    invoke-direct {p1}, Lcom/microsoft/identity/common/adal/internal/net/WebRequestHandler;-><init>()V

    .line 23
    .line 24
    .line 25
    iput-object p1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mWebRequestHandler:Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;

    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 29
    .line 30
    const-string p2, "authRequest"

    .line 31
    .line 32
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    throw p1

    .line 36
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 37
    .line 38
    const-string p2, "context"

    .line 39
    .line 40
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    throw p1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private acquireTokenWithCachedItem(Lcom/microsoft/aad/adal/TokenCacheItem;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/TokenCacheItem;->getRefreshToken()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance p1, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    sget-object v0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    const-string v0, ":acquireTokenWithCachedItem"

    .line 22
    .line 23
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLogInfo()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const-string v1, "Token cache item contains empty refresh token, cannot continue refresh token request"

    .line 37
    .line 38
    const/4 v2, 0x0

    .line 39
    invoke-static {p1, v1, v0, v2}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 40
    .line 41
    .line 42
    return-object v2

    .line 43
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/TokenCacheItem;->getRefreshToken()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {p0, v0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->acquireTokenWithRefreshToken(Ljava/lang/String;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    if-eqz v0, :cond_1

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationResult;->isExtendedLifeTimeToken()Z

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    if-nez v1, :cond_1

    .line 58
    .line 59
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 60
    .line 61
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 62
    .line 63
    invoke-virtual {v1, v2, v0, p1}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->updateCachedItemWithResult(Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/AuthenticationResult;Lcom/microsoft/aad/adal/TokenCacheItem;)V

    .line 64
    .line 65
    .line 66
    :cond_1
    return-object v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private isMRRTEntryExisted()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getClientId()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUserFromRequest()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v0, v1, v2}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->getMRRTItem(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 16
    .line 17
    .line 18
    move-result-object v0
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getRefreshToken()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-static {v0}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-nez v0, :cond_0

    .line 30
    .line 31
    const/4 v0, 0x1

    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 v0, 0x0

    .line 34
    :goto_0
    return v0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    new-instance v1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 37
    .line 38
    sget-object v2, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_AUTHORITY_IS_NOT_VALID_URL:Lcom/microsoft/aad/adal/ADALError;

    .line 39
    .line 40
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v3

    .line 44
    invoke-direct {v1, v2, v3, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 45
    .line 46
    .line 47
    throw v1
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method private isTokenRequestFailed(Lcom/microsoft/aad/adal/AuthenticationResult;)Z
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getErrorCode()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-static {p1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    const/4 p1, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private tryFRT(Ljava/lang/String;Lcom/microsoft/aad/adal/AuthenticationResult;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUserFromRequest()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, p1, v1}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->getFRTItem(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 10
    .line 11
    .line 12
    move-result-object p1
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 13
    const-string v0, ":tryFRT"

    .line 14
    .line 15
    if-nez p1, :cond_1

    .line 16
    .line 17
    iget-boolean p1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAttemptedWithMRRT:Z

    .line 18
    .line 19
    if-nez p1, :cond_0

    .line 20
    .line 21
    new-instance p1, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    sget-object p2, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 27
    .line 28
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    const-string p2, "FRT cache item does not exist, fall back to try MRRT."

    .line 39
    .line 40
    invoke-static {p1, p2}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->useMRRT()Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    return-object p1

    .line 48
    :cond_0
    return-object p2

    .line 49
    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    .line 50
    .line 51
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .line 53
    .line 54
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 55
    .line 56
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object p2

    .line 66
    const-string v0, "Send request to use FRT for new AT."

    .line 67
    .line 68
    invoke-static {p2, v0}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->acquireTokenWithCachedItem(Lcom/microsoft/aad/adal/TokenCacheItem;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->isTokenRequestFailed(Lcom/microsoft/aad/adal/AuthenticationResult;)Z

    .line 76
    .line 77
    .line 78
    move-result p2

    .line 79
    if-eqz p2, :cond_3

    .line 80
    .line 81
    iget-boolean p2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAttemptedWithMRRT:Z

    .line 82
    .line 83
    if-nez p2, :cond_3

    .line 84
    .line 85
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->useMRRT()Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 86
    .line 87
    .line 88
    move-result-object p2

    .line 89
    if-nez p2, :cond_2

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_2
    move-object p1, p2

    .line 93
    :cond_3
    :goto_0
    return-object p1

    .line 94
    :catch_0
    move-exception p1

    .line 95
    new-instance p2, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 96
    .line 97
    sget-object v0, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_AUTHORITY_IS_NOT_VALID_URL:Lcom/microsoft/aad/adal/ADALError;

    .line 98
    .line 99
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v1

    .line 103
    invoke-direct {p2, v0, v1, p1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 104
    .line 105
    .line 106
    throw p2
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method private tryMRRT()Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getClientId()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUserFromRequest()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v0, v1, v2}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->getMRRTItem(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iput-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mMrrtTokenCacheItem:Lcom/microsoft/aad/adal/TokenCacheItem;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    const-string v2, "1"

    .line 23
    .line 24
    const-string v3, ":tryMRRT"

    .line 25
    .line 26
    if-nez v0, :cond_0

    .line 27
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    sget-object v4, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 34
    .line 35
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    const-string v3, "MRRT token does not exist, try with FRT"

    .line 46
    .line 47
    invoke-static {v0, v3}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    invoke-direct {p0, v2, v1}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->tryFRT(Ljava/lang/String;Lcom/microsoft/aad/adal/AuthenticationResult;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    return-object v0

    .line 55
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/TokenCacheItem;->isFamilyToken()Z

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    if-eqz v0, :cond_1

    .line 60
    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    .line 62
    .line 63
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    .line 65
    .line 66
    sget-object v2, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 67
    .line 68
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    const-string v2, "MRRT item exists but it\'s also a FRT, try with FRT."

    .line 79
    .line 80
    invoke-static {v0, v2}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mMrrtTokenCacheItem:Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 84
    .line 85
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getFamilyClientId()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    invoke-direct {p0, v0, v1}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->tryFRT(Ljava/lang/String;Lcom/microsoft/aad/adal/AuthenticationResult;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    return-object v0

    .line 94
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->useMRRT()Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    invoke-direct {p0, v0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->isTokenRequestFailed(Lcom/microsoft/aad/adal/AuthenticationResult;)Z

    .line 99
    .line 100
    .line 101
    move-result v1

    .line 102
    if-eqz v1, :cond_3

    .line 103
    .line 104
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mMrrtTokenCacheItem:Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 105
    .line 106
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/TokenCacheItem;->getFamilyClientId()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v1

    .line 110
    invoke-static {v1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 111
    .line 112
    .line 113
    move-result v1

    .line 114
    if-eqz v1, :cond_2

    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_2
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mMrrtTokenCacheItem:Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 118
    .line 119
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/TokenCacheItem;->getFamilyClientId()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v2

    .line 123
    :goto_0
    invoke-direct {p0, v2, v0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->tryFRT(Ljava/lang/String;Lcom/microsoft/aad/adal/AuthenticationResult;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    :cond_3
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 128
    .line 129
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUserFromRequest()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v1

    .line 133
    invoke-static {v1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 134
    .line 135
    .line 136
    move-result v1

    .line 137
    if-eqz v1, :cond_5

    .line 138
    .line 139
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 140
    .line 141
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 142
    .line 143
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getClientId()Ljava/lang/String;

    .line 144
    .line 145
    .line 146
    move-result-object v2

    .line 147
    invoke-virtual {v1, v2}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->isMultipleMRRTsMatchingGivenApp(Ljava/lang/String;)Z

    .line 148
    .line 149
    .line 150
    move-result v1

    .line 151
    if-nez v1, :cond_4

    .line 152
    .line 153
    goto :goto_1

    .line 154
    :cond_4
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 155
    .line 156
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->AUTH_FAILED_USER_MISMATCH:Lcom/microsoft/aad/adal/ADALError;

    .line 157
    .line 158
    const-string v2, "No User provided and multiple MRRTs exist for the given client id"

    .line 159
    .line 160
    invoke-direct {v0, v1, v2}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    throw v0

    .line 164
    :cond_5
    :goto_1
    return-object v0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    new-instance v1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 167
    .line 168
    sget-object v2, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_AUTHORITY_IS_NOT_VALID_URL:Lcom/microsoft/aad/adal/ADALError;

    .line 169
    .line 170
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object v3

    .line 174
    invoke-direct {v1, v2, v3, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 175
    .line 176
    .line 177
    throw v1
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method private tryRT()Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getResource()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getClientId()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    iget-object v3, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 16
    .line 17
    invoke-virtual {v3}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUserFromRequest()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->getRegularRefreshTokenCacheItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 22
    .line 23
    .line 24
    move-result-object v0
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    const-string v1, ":tryRT"

    .line 26
    .line 27
    if-nez v0, :cond_0

    .line 28
    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    sget-object v2, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 35
    .line 36
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    const-string v1, "Regular token cache entry does not exist, try with MRRT."

    .line 47
    .line 48
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->tryMRRT()Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    return-object v0

    .line 56
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getIsMultiResourceRefreshToken()Z

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    if-nez v2, :cond_4

    .line 61
    .line 62
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->isMRRTEntryExisted()Z

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    if-eqz v2, :cond_1

    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_1
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 70
    .line 71
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUserFromRequest()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    invoke-static {v2}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    if-eqz v2, :cond_3

    .line 80
    .line 81
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 82
    .line 83
    iget-object v3, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 84
    .line 85
    invoke-virtual {v3}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getClientId()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v3

    .line 89
    iget-object v4, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 90
    .line 91
    invoke-virtual {v4}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getResource()Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v4

    .line 95
    invoke-virtual {v2, v3, v4}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->isMultipleRTsMatchingGivenAppAndResource(Ljava/lang/String;Ljava/lang/String;)Z

    .line 96
    .line 97
    .line 98
    move-result v2

    .line 99
    if-nez v2, :cond_2

    .line 100
    .line 101
    goto :goto_0

    .line 102
    :cond_2
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 103
    .line 104
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->AUTH_FAILED_USER_MISMATCH:Lcom/microsoft/aad/adal/ADALError;

    .line 105
    .line 106
    const-string v2, "Multiple refresh tokens exists for the given client id and resource"

    .line 107
    .line 108
    invoke-direct {v0, v1, v2}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    throw v0

    .line 112
    :cond_3
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    .line 113
    .line 114
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .line 116
    .line 117
    sget-object v3, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 118
    .line 119
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    const-string v2, "Send request to use regular RT for new AT."

    .line 130
    .line 131
    invoke-static {v1, v2}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    invoke-direct {p0, v0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->acquireTokenWithCachedItem(Lcom/microsoft/aad/adal/TokenCacheItem;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 135
    .line 136
    .line 137
    move-result-object v0

    .line 138
    return-object v0

    .line 139
    :cond_4
    :goto_1
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getIsMultiResourceRefreshToken()Z

    .line 140
    .line 141
    .line 142
    move-result v0

    .line 143
    if-eqz v0, :cond_5

    .line 144
    .line 145
    const-string v0, "Found RT and it\'s also a MRRT, retry with MRRT"

    .line 146
    .line 147
    goto :goto_2

    .line 148
    :cond_5
    const-string v0, "RT is found and there is a MRRT entry existed, try with MRRT"

    .line 149
    .line 150
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    .line 151
    .line 152
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    .line 154
    .line 155
    sget-object v3, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 156
    .line 157
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 164
    .line 165
    .line 166
    move-result-object v1

    .line 167
    invoke-static {v1, v0}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->tryMRRT()Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 171
    .line 172
    .line 173
    move-result-object v0

    .line 174
    return-object v0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    new-instance v1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 177
    .line 178
    sget-object v2, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_AUTHORITY_IS_NOT_VALID_URL:Lcom/microsoft/aad/adal/ADALError;

    .line 179
    .line 180
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 181
    .line 182
    .line 183
    move-result-object v3

    .line 184
    invoke-direct {v1, v2, v3, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 185
    .line 186
    .line 187
    throw v1
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method private useMRRT()Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    const-string v2, ":useMRRT"

    .line 12
    .line 13
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v3, "Send request to use MRRT for new AT."

    .line 21
    .line 22
    invoke-static {v0, v3}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAttemptedWithMRRT:Z

    .line 27
    .line 28
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mMrrtTokenCacheItem:Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 29
    .line 30
    if-nez v0, :cond_0

    .line 31
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    const-string v1, "MRRT does not exist, cannot proceed with MRRT for new AT."

    .line 48
    .line 49
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    const/4 v0, 0x0

    .line 53
    return-object v0

    .line 54
    :cond_0
    invoke-direct {p0, v0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->acquireTokenWithCachedItem(Lcom/microsoft/aad/adal/TokenCacheItem;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    return-object v0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method


# virtual methods
.method acquireTokenWithAssertion()Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    const-string v2, ":acquireTokenWithAssertion"

    .line 12
    .line 13
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iget-object v3, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 21
    .line 22
    invoke-virtual {v3}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLogInfo()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    const-string v4, "Try to get new access token with the provided assertion."

    .line 27
    .line 28
    const/4 v5, 0x0

    .line 29
    invoke-static {v0, v4, v3, v5}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mContext:Landroid/content/Context;

    .line 33
    .line 34
    invoke-static {v0}, Lcom/microsoft/aad/adal/HttpUtil;->throwIfNetworkNotAvailable(Landroid/content/Context;)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getSamlAssertion()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    iget-object v3, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 44
    .line 45
    invoke-virtual {v3}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getAssertionType()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    :try_start_0
    new-instance v4, Lcom/microsoft/identity/common/java/util/JWSBuilder;

    .line 50
    .line 51
    invoke-direct {v4}, Lcom/microsoft/identity/common/java/util/JWSBuilder;-><init>()V

    .line 52
    .line 53
    .line 54
    new-instance v6, Lcom/microsoft/aad/adal/Oauth2;

    .line 55
    .line 56
    iget-object v7, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 57
    .line 58
    iget-object v8, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mWebRequestHandler:Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;

    .line 59
    .line 60
    invoke-direct {v6, v7, v8, v4}, Lcom/microsoft/aad/adal/Oauth2;-><init>(Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;Lcom/microsoft/identity/common/java/util/JWSBuilder;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v6, v0, v3}, Lcom/microsoft/aad/adal/Oauth2;->refreshTokenUsingAssertion(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    if-eqz v0, :cond_0

    .line 68
    .line 69
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationResult;->getRefreshToken()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v3

    .line 73
    invoke-static {v3}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 74
    .line 75
    .line 76
    move-result v3

    .line 77
    if-eqz v3, :cond_0

    .line 78
    .line 79
    new-instance v3, Ljava/lang/StringBuilder;

    .line 80
    .line 81
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    const-string v3, "Refresh token is not returned or empty"

    .line 95
    .line 96
    invoke-static {v1, v3}, Lcom/microsoft/aad/adal/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/microsoft/aad/adal/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    .line 98
    .line 99
    :cond_0
    return-object v0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    goto :goto_0

    .line 102
    :catch_1
    move-exception v0

    .line 103
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 104
    .line 105
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .line 107
    .line 108
    sget-object v3, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 109
    .line 110
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    new-instance v2, Ljava/lang/StringBuilder;

    .line 121
    .line 122
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    .line 124
    .line 125
    const-string v3, "Request: "

    .line 126
    .line 127
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    iget-object v3, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 131
    .line 132
    invoke-virtual {v3}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLogInfo()Ljava/lang/String;

    .line 133
    .line 134
    .line 135
    move-result-object v3

    .line 136
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    const-string v3, " "

    .line 140
    .line 141
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-static {v0}, Lcom/microsoft/aad/adal/ExceptionExtensions;->getExceptionMessage(Ljava/lang/Exception;)Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v4

    .line 148
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    .line 153
    .line 154
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v3

    .line 158
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    .line 160
    .line 161
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v2

    .line 165
    sget-object v3, Lcom/microsoft/aad/adal/ADALError;->AUTH_FAILED_NO_TOKEN:Lcom/microsoft/aad/adal/ADALError;

    .line 166
    .line 167
    const-string v4, "Error in assertion for request."

    .line 168
    .line 169
    invoke-static {v1, v4, v2, v3, v5}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;Ljava/lang/Throwable;)V

    .line 170
    .line 171
    .line 172
    new-instance v1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 173
    .line 174
    invoke-static {v0}, Lcom/microsoft/aad/adal/ExceptionExtensions;->getExceptionMessage(Ljava/lang/Exception;)Ljava/lang/String;

    .line 175
    .line 176
    .line 177
    move-result-object v2

    .line 178
    new-instance v4, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 179
    .line 180
    sget-object v5, Lcom/microsoft/aad/adal/ADALError;->SERVER_ERROR:Lcom/microsoft/aad/adal/ADALError;

    .line 181
    .line 182
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 183
    .line 184
    .line 185
    move-result-object v6

    .line 186
    invoke-direct {v4, v5, v6, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 187
    .line 188
    .line 189
    invoke-direct {v1, v3, v2, v4}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 190
    .line 191
    .line 192
    throw v1
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method acquireTokenWithRefreshToken(Ljava/lang/String;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    const-string v0, "Request: "

    .line 2
    .line 3
    const-string v1, " "

    .line 4
    .line 5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    sget-object v3, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 11
    .line 12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    const-string v4, ":acquireTokenWithRefreshToken"

    .line 16
    .line 17
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    iget-object v5, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 25
    .line 26
    invoke-virtual {v5}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLogInfo()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v5

    .line 30
    const-string v6, "Try to get new access token with the found refresh token."

    .line 31
    .line 32
    const/4 v7, 0x0

    .line 33
    invoke-static {v2, v6, v5, v7}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 34
    .line 35
    .line 36
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mContext:Landroid/content/Context;

    .line 37
    .line 38
    invoke-static {v2}, Lcom/microsoft/aad/adal/HttpUtil;->throwIfNetworkNotAvailable(Landroid/content/Context;)V

    .line 39
    .line 40
    .line 41
    :try_start_0
    new-instance v2, Lcom/microsoft/identity/common/java/util/JWSBuilder;

    .line 42
    .line 43
    invoke-direct {v2}, Lcom/microsoft/identity/common/java/util/JWSBuilder;-><init>()V

    .line 44
    .line 45
    .line 46
    new-instance v5, Lcom/microsoft/aad/adal/Oauth2;

    .line 47
    .line 48
    iget-object v6, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 49
    .line 50
    iget-object v8, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mWebRequestHandler:Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;

    .line 51
    .line 52
    invoke-direct {v5, v6, v8, v2}, Lcom/microsoft/aad/adal/Oauth2;-><init>(Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;Lcom/microsoft/identity/common/java/util/JWSBuilder;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v5, p1}, Lcom/microsoft/aad/adal/Oauth2;->refreshToken(Ljava/lang/String;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    if-eqz v2, :cond_0

    .line 60
    .line 61
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/AuthenticationResult;->getRefreshToken()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v5

    .line 65
    invoke-static {v5}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    if-eqz v5, :cond_0

    .line 70
    .line 71
    new-instance v5, Ljava/lang/StringBuilder;

    .line 72
    .line 73
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v3

    .line 86
    const-string v5, "Refresh token is not returned or empty"

    .line 87
    .line 88
    invoke-static {v3, v5}, Lcom/microsoft/aad/adal/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v2, p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->setRefreshToken(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/aad/adal/ServerRespondingWithRetryableException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/microsoft/aad/adal/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    .line 93
    .line 94
    :cond_0
    return-object v2

    .line 95
    :catch_0
    move-exception p1

    .line 96
    goto :goto_0

    .line 97
    :catch_1
    move-exception p1

    .line 98
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    .line 99
    .line 100
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .line 102
    .line 103
    sget-object v3, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 104
    .line 105
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v2

    .line 115
    new-instance v3, Ljava/lang/StringBuilder;

    .line 116
    .line 117
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 124
    .line 125
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLogInfo()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    invoke-static {p1}, Lcom/microsoft/aad/adal/ExceptionExtensions;->getExceptionMessage(Ljava/lang/Exception;)Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    move-result-object v0

    .line 156
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->AUTH_FAILED_NO_TOKEN:Lcom/microsoft/aad/adal/ADALError;

    .line 157
    .line 158
    const-string v3, "Error in refresh token for request."

    .line 159
    .line 160
    invoke-static {v2, v3, v0, v1, v7}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;Ljava/lang/Throwable;)V

    .line 161
    .line 162
    .line 163
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 164
    .line 165
    invoke-static {p1}, Lcom/microsoft/aad/adal/ExceptionExtensions;->getExceptionMessage(Ljava/lang/Exception;)Ljava/lang/String;

    .line 166
    .line 167
    .line 168
    move-result-object v2

    .line 169
    new-instance v3, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 170
    .line 171
    sget-object v4, Lcom/microsoft/aad/adal/ADALError;->SERVER_ERROR:Lcom/microsoft/aad/adal/ADALError;

    .line 172
    .line 173
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 174
    .line 175
    .line 176
    move-result-object v5

    .line 177
    invoke-direct {v3, v4, v5, p1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 178
    .line 179
    .line 180
    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 181
    .line 182
    .line 183
    throw v0

    .line 184
    :catch_2
    move-exception p1

    .line 185
    new-instance v2, Ljava/lang/StringBuilder;

    .line 186
    .line 187
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    .line 189
    .line 190
    sget-object v3, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 191
    .line 192
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v2

    .line 202
    new-instance v5, Ljava/lang/StringBuilder;

    .line 203
    .line 204
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 205
    .line 206
    .line 207
    const-string v6, "The server is not responding after the retry with error code: "

    .line 208
    .line 209
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    .line 211
    .line 212
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationException;->getCode()Lcom/microsoft/aad/adal/ADALError;

    .line 213
    .line 214
    .line 215
    move-result-object v6

    .line 216
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 217
    .line 218
    .line 219
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 220
    .line 221
    .line 222
    move-result-object v5

    .line 223
    const-string v6, ""

    .line 224
    .line 225
    invoke-static {v2, v5, v6}, Lcom/microsoft/aad/adal/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 229
    .line 230
    iget-object v5, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 231
    .line 232
    invoke-virtual {v2, v5}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->getStaleToken(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 233
    .line 234
    .line 235
    move-result-object v2

    .line 236
    if-eqz v2, :cond_1

    .line 237
    .line 238
    invoke-static {v2}, Lcom/microsoft/aad/adal/AuthenticationResult;->createExtendedLifeTimeResult(Lcom/microsoft/aad/adal/TokenCacheItem;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 239
    .line 240
    .line 241
    move-result-object p1

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    .line 243
    .line 244
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 245
    .line 246
    .line 247
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    .line 249
    .line 250
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .line 252
    .line 253
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 254
    .line 255
    .line 256
    move-result-object v0

    .line 257
    const-string v1, "The result with stale access token is returned."

    .line 258
    .line 259
    invoke-static {v0, v1, v6}, Lcom/microsoft/aad/adal/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    .line 261
    .line 262
    return-object p1

    .line 263
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    .line 264
    .line 265
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 266
    .line 267
    .line 268
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    .line 270
    .line 271
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    .line 273
    .line 274
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 275
    .line 276
    .line 277
    move-result-object v2

    .line 278
    new-instance v3, Ljava/lang/StringBuilder;

    .line 279
    .line 280
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 281
    .line 282
    .line 283
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    .line 285
    .line 286
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 287
    .line 288
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLogInfo()Ljava/lang/String;

    .line 289
    .line 290
    .line 291
    move-result-object v0

    .line 292
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    .line 294
    .line 295
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    .line 297
    .line 298
    invoke-static {p1}, Lcom/microsoft/aad/adal/ExceptionExtensions;->getExceptionMessage(Ljava/lang/Exception;)Ljava/lang/String;

    .line 299
    .line 300
    .line 301
    move-result-object v0

    .line 302
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    .line 304
    .line 305
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    .line 307
    .line 308
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    .line 309
    .line 310
    .line 311
    move-result-object v0

    .line 312
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    .line 314
    .line 315
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 316
    .line 317
    .line 318
    move-result-object v0

    .line 319
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->AUTH_FAILED_NO_TOKEN:Lcom/microsoft/aad/adal/ADALError;

    .line 320
    .line 321
    const-string v3, "Error in refresh token for request. "

    .line 322
    .line 323
    invoke-static {v2, v3, v0, v1, v7}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;Ljava/lang/Throwable;)V

    .line 324
    .line 325
    .line 326
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 327
    .line 328
    invoke-static {p1}, Lcom/microsoft/aad/adal/ExceptionExtensions;->getExceptionMessage(Ljava/lang/Exception;)Ljava/lang/String;

    .line 329
    .line 330
    .line 331
    move-result-object v2

    .line 332
    new-instance v3, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 333
    .line 334
    sget-object v4, Lcom/microsoft/aad/adal/ADALError;->SERVER_ERROR:Lcom/microsoft/aad/adal/ADALError;

    .line 335
    .line 336
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationException;->getMessage()Ljava/lang/String;

    .line 337
    .line 338
    .line 339
    move-result-object v5

    .line 340
    invoke-direct {v3, v4, v5, p1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 341
    .line 342
    .line 343
    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 344
    .line 345
    .line 346
    throw v0
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method getAccessToken()Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getResource()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getClientId()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    iget-object v3, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 20
    .line 21
    invoke-virtual {v3}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUserFromRequest()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->getATFromCache(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const-string v1, ":getAccessToken"

    .line 30
    .line 31
    if-eqz v0, :cond_2

    .line 32
    .line 33
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 34
    .line 35
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getForceRefresh()Z

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    if-nez v2, :cond_2

    .line 40
    .line 41
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/AuthenticationRequest;->isClaimsChallengePresent()Z

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    if-eqz v2, :cond_1

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    .line 51
    .line 52
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .line 54
    .line 55
    sget-object v3, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 56
    .line 57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    const-string v2, "Return AT from cache."

    .line 68
    .line 69
    invoke-static {v1, v2}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    invoke-static {v0}, Lcom/microsoft/aad/adal/AuthenticationResult;->createResult(Lcom/microsoft/aad/adal/TokenCacheItem;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    return-object v0

    .line 77
    :cond_2
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 78
    .line 79
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .line 81
    .line 82
    sget-object v2, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 83
    .line 84
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    const-string v1, "No valid access token exists, try with refresh token."

    .line 95
    .line 96
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->tryRT()Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    return-object v0
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method getAccessTokenUsingAssertion()Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    const-string v0, "Access token fetched but unable to update token cache"

    .line 2
    .line 3
    const-string v1, ":getAccessTokenUsingAssertion"

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->acquireTokenWithAssertion()Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    if-eqz v2, :cond_0

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/AuthenticationResult;->getAccessToken()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v3

    .line 15
    invoke-static {v3}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    if-nez v3, :cond_0

    .line 20
    .line 21
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 22
    .line 23
    iget-object v4, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mAuthRequest:Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 24
    .line 25
    invoke-virtual {v3, v4, v2}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->updateTokenCache(Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/AuthenticationResult;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/microsoft/identity/common/java/exception/ClientException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :catch_0
    move-exception v2

    .line 30
    new-instance v3, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    sget-object v4, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 36
    .line 37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-static {v1, v0}, Lcom/microsoft/aad/adal/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    invoke-static {v2}, Lcom/microsoft/aad/adal/ADALError;->fromCommon(Lcom/microsoft/identity/common/java/exception/BaseException;)Lcom/microsoft/aad/adal/AuthenticationException;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    throw v0

    .line 55
    :catch_1
    move-exception v2

    .line 56
    new-instance v3, Ljava/lang/StringBuilder;

    .line 57
    .line 58
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .line 60
    .line 61
    sget-object v4, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->TAG:Ljava/lang/String;

    .line 62
    .line 63
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    invoke-static {v1, v0}, Lcom/microsoft/aad/adal/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 77
    .line 78
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_AUTHORITY_IS_NOT_VALID_URL:Lcom/microsoft/aad/adal/ADALError;

    .line 79
    .line 80
    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v3

    .line 84
    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 85
    .line 86
    .line 87
    throw v0

    .line 88
    :cond_0
    :goto_0
    return-object v2
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method setWebRequestHandler(Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->mWebRequestHandler:Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

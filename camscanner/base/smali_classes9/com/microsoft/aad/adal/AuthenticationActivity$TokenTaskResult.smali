.class Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;
.super Ljava/lang/Object;
.source "AuthenticationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/aad/adal/AuthenticationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TokenTaskResult"
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mTaskException:Ljava/lang/Exception;

.field private mTaskResult:Lcom/microsoft/aad/adal/AuthenticationResult;

.field final synthetic this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;


# direct methods
.method constructor <init>(Lcom/microsoft/aad/adal/AuthenticationActivity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$1800(Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;->mTaskResult:Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$1802(Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;Lcom/microsoft/aad/adal/AuthenticationResult;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;->mTaskResult:Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic access$1900(Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;)Ljava/lang/Exception;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;->mTaskException:Ljava/lang/Exception;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$1902(Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;->mTaskException:Ljava/lang/Exception;

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic access$2100(Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;->mAccountName:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$2102(Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTaskResult;->mAccountName:Ljava/lang/String;

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

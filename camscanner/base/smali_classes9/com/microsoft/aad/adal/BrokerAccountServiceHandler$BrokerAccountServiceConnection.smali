.class Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;
.super Ljava/lang/Object;
.source "BrokerAccountServiceHandler.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BrokerAccountServiceConnection"
.end annotation


# instance fields
.field private mBound:Z

.field private mBrokerAccountService:Lcom/microsoft/aad/adal/IBrokerAccountService;

.field private mEvent:Lcom/microsoft/aad/adal/BrokerEvent;

.field final synthetic this$0:Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;


# direct methods
.method private constructor <init>(Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->this$0:Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$1;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;-><init>(Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->mBound:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$602(Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->mBound:Z

    .line 2
    .line 3
    return p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public getBrokerAccountServiceProvider()Lcom/microsoft/aad/adal/IBrokerAccountService;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->mBrokerAccountService:Lcom/microsoft/aad/adal/IBrokerAccountService;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;->access$200()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const-string v0, "Broker Account service is connected."

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-static {p2}, Lcom/microsoft/aad/adal/IBrokerAccountService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/microsoft/aad/adal/IBrokerAccountService;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->mBrokerAccountService:Lcom/microsoft/aad/adal/IBrokerAccountService;

    .line 15
    .line 16
    const/4 p1, 0x1

    .line 17
    iput-boolean p1, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->mBound:Z

    .line 18
    .line 19
    iget-object p1, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->mEvent:Lcom/microsoft/aad/adal/BrokerEvent;

    .line 20
    .line 21
    if-eqz p1, :cond_0

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/BrokerEvent;->setBrokerAccountServiceConnected()V

    .line 24
    .line 25
    .line 26
    :cond_0
    iget-object p1, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->this$0:Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;

    .line 27
    .line 28
    invoke-static {p1}, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;->access$500(Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;)Ljava/util/concurrent/ConcurrentMap;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-interface {p1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    check-cast p1, Lcom/microsoft/aad/adal/CallbackExecutor;

    .line 37
    .line 38
    if-eqz p1, :cond_1

    .line 39
    .line 40
    invoke-virtual {p1, p0}, Lcom/microsoft/aad/adal/CallbackExecutor;->onSuccess(Ljava/lang/Object;)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    invoke-static {}, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;->access$200()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    const-string p2, "No callback is found."

    .line 49
    .line 50
    invoke-static {p1, p2}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    :goto_0
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;->access$200()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const-string v0, "Broker Account service is disconnected."

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 p1, 0x0

    .line 11
    iput-boolean p1, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->mBound:Z

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setTelemetryEvent(Lcom/microsoft/aad/adal/BrokerEvent;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->mEvent:Lcom/microsoft/aad/adal/BrokerEvent;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public unBindService(Landroid/content/Context;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/os/Handler;

    .line 2
    .line 3
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 8
    .line 9
    .line 10
    new-instance v1, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection$1;

    .line 11
    .line 12
    invoke-direct {v1, p0, p1}, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection$1;-><init>(Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;Landroid/content/Context;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

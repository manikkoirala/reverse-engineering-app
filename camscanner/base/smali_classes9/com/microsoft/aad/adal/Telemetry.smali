.class public final Lcom/microsoft/aad/adal/Telemetry;
.super Ljava/lang/Object;
.source "Telemetry.java"


# static fields
.field private static final INSTANCE:Lcom/microsoft/aad/adal/Telemetry;

.field private static final TAG:Ljava/lang/String; = "Telemetry"

.field private static sAllowPii:Z = false


# instance fields
.field private mDispatcher:Lcom/microsoft/aad/adal/DefaultDispatcher;

.field private final mEventTracking:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/microsoft/aad/adal/Telemetry;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/microsoft/aad/adal/Telemetry;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/microsoft/aad/adal/Telemetry;->INSTANCE:Lcom/microsoft/aad/adal/Telemetry;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/microsoft/aad/adal/Telemetry;->mDispatcher:Lcom/microsoft/aad/adal/DefaultDispatcher;

    .line 6
    .line 7
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/microsoft/aad/adal/Telemetry;->mEventTracking:Ljava/util/Map;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static getAllowPii()Z
    .locals 1

    .line 1
    sget-boolean v0, Lcom/microsoft/aad/adal/Telemetry;->sAllowPii:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static declared-synchronized getInstance()Lcom/microsoft/aad/adal/Telemetry;
    .locals 2

    .line 1
    const-class v0, Lcom/microsoft/aad/adal/Telemetry;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    sget-object v1, Lcom/microsoft/aad/adal/Telemetry;->INSTANCE:Lcom/microsoft/aad/adal/Telemetry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5
    .line 6
    monitor-exit v0

    .line 7
    return-object v1

    .line 8
    :catchall_0
    move-exception v1

    .line 9
    monitor-exit v0

    .line 10
    throw v1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method static registerNewRequest()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static setAllowPii(Z)V
    .locals 0

    .line 1
    sput-boolean p0, Lcom/microsoft/aad/adal/Telemetry;->sAllowPii:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method flush(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/Telemetry;->mDispatcher:Lcom/microsoft/aad/adal/DefaultDispatcher;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/microsoft/aad/adal/DefaultDispatcher;->flush(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public declared-synchronized registerDispatcher(Lcom/microsoft/aad/adal/IDispatcher;Z)V
    .locals 0

    .line 1
    monitor-enter p0

    .line 2
    if-eqz p2, :cond_0

    .line 3
    .line 4
    :try_start_0
    new-instance p2, Lcom/microsoft/aad/adal/AggregatedDispatcher;

    .line 5
    .line 6
    invoke-direct {p2, p1}, Lcom/microsoft/aad/adal/AggregatedDispatcher;-><init>(Lcom/microsoft/aad/adal/IDispatcher;)V

    .line 7
    .line 8
    .line 9
    iput-object p2, p0, Lcom/microsoft/aad/adal/Telemetry;->mDispatcher:Lcom/microsoft/aad/adal/DefaultDispatcher;

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    new-instance p2, Lcom/microsoft/aad/adal/DefaultDispatcher;

    .line 13
    .line 14
    invoke-direct {p2, p1}, Lcom/microsoft/aad/adal/DefaultDispatcher;-><init>(Lcom/microsoft/aad/adal/IDispatcher;)V

    .line 15
    .line 16
    .line 17
    iput-object p2, p0, Lcom/microsoft/aad/adal/Telemetry;->mDispatcher:Lcom/microsoft/aad/adal/DefaultDispatcher;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18
    .line 19
    :goto_0
    monitor-exit p0

    .line 20
    return-void

    .line 21
    :catchall_0
    move-exception p1

    .line 22
    monitor-exit p0

    .line 23
    throw p1
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method startEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/Telemetry;->mDispatcher:Lcom/microsoft/aad/adal/DefaultDispatcher;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/microsoft/aad/adal/Telemetry;->mEventTracking:Ljava/util/Map;

    .line 7
    .line 8
    new-instance v1, Ljava/util/AbstractMap$SimpleEntry;

    .line 9
    .line 10
    invoke-direct {v1, p1, p2}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 14
    .line 15
    .line 16
    move-result-wide p1

    .line 17
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method stopEvent(Ljava/lang/String;Lcom/microsoft/aad/adal/IEvents;Ljava/lang/String;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/Telemetry;->mDispatcher:Lcom/microsoft/aad/adal/DefaultDispatcher;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/microsoft/aad/adal/Telemetry;->mEventTracking:Ljava/util/Map;

    .line 7
    .line 8
    new-instance v1, Ljava/util/AbstractMap$SimpleEntry;

    .line 9
    .line 10
    invoke-direct {v1, p1, p3}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object p3

    .line 17
    check-cast p3, Ljava/lang/String;

    .line 18
    .line 19
    invoke-static {p3}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    sget-object p1, Lcom/microsoft/aad/adal/Telemetry;->TAG:Ljava/lang/String;

    .line 26
    .line 27
    const-string p2, ""

    .line 28
    .line 29
    const/4 p3, 0x0

    .line 30
    const-string v0, "Stop Event called without a corresponding start_event"

    .line 31
    .line 32
    invoke-static {p1, v0, p2, p3}, Lcom/microsoft/aad/adal/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 33
    .line 34
    .line 35
    return-void

    .line 36
    :cond_1
    invoke-static {p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 37
    .line 38
    .line 39
    move-result-wide v0

    .line 40
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 41
    .line 42
    .line 43
    move-result-wide v2

    .line 44
    sub-long v0, v2, v0

    .line 45
    .line 46
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    const-string v3, "Microsoft.ADAL.start_time"

    .line 51
    .line 52
    invoke-interface {p2, v3, p3}, Lcom/microsoft/aad/adal/IEvents;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    const-string p3, "Microsoft.ADAL.stop_time"

    .line 56
    .line 57
    invoke-interface {p2, p3, v2}, Lcom/microsoft/aad/adal/IEvents;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    const-string p3, "Microsoft.ADAL.response_time"

    .line 61
    .line 62
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-interface {p2, p3, v0}, Lcom/microsoft/aad/adal/IEvents;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    iget-object p3, p0, Lcom/microsoft/aad/adal/Telemetry;->mDispatcher:Lcom/microsoft/aad/adal/DefaultDispatcher;

    .line 70
    .line 71
    invoke-virtual {p3, p1, p2}, Lcom/microsoft/aad/adal/DefaultDispatcher;->receive(Ljava/lang/String;Lcom/microsoft/aad/adal/IEvents;)V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

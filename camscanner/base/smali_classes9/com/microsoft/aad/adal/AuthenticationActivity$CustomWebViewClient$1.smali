.class Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient$1;
.super Ljava/lang/Object;
.source "AuthenticationActivity.java"

# interfaces
.implements Landroid/security/KeyChainAliasCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->onReceivedClientCertRequest(Landroid/webkit/WebView;Landroid/webkit/ClientCertRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;

.field final synthetic val$request:Landroid/webkit/ClientCertRequest;


# direct methods
.method constructor <init>(Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;Landroid/webkit/ClientCertRequest;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient$1;->this$1:Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient$1;->val$request:Landroid/webkit/ClientCertRequest;

    .line 4
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public alias(Ljava/lang/String;)V
    .locals 3

    .line 1
    const-string v0, "AuthenticationActivity:onReceivedClientCertRequest"

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    const-string p1, "No certificate chosen by user, cancelling the TLS request."

    .line 6
    .line 7
    invoke-static {v0, p1}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇O00(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    iget-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient$1;->val$request:Landroid/webkit/ClientCertRequest;

    .line 11
    .line 12
    invoke-virtual {p1}, Landroid/webkit/ClientCertRequest;->cancel()V

    .line 13
    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient$1;->this$1:Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;

    .line 17
    .line 18
    iget-object v1, v1, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 19
    .line 20
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v1, p1}, Landroid/security/KeyChain;->getCertificateChain(Landroid/content/Context;Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    iget-object v2, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient$1;->this$1:Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/BasicWebViewClient;->getCallingContext()Landroid/content/Context;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    invoke-static {v2, p1}, Landroid/security/KeyChain;->getPrivateKey(Landroid/content/Context;Ljava/lang/String;)Ljava/security/PrivateKey;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    const-string v2, "Certificate is chosen by user, proceed with TLS request."

    .line 39
    .line 40
    invoke-static {v0, v2}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇O00(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    iget-object v2, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient$1;->val$request:Landroid/webkit/ClientCertRequest;

    .line 44
    .line 45
    invoke-virtual {v2, p1, v1}, Landroid/webkit/ClientCertRequest;->proceed(Ljava/security/PrivateKey;[Ljava/security/cert/X509Certificate;)V
    :try_end_0
    .catch Landroid/security/KeyChainException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .line 47
    .line 48
    return-void

    .line 49
    :catch_0
    move-exception p1

    .line 50
    const-string v1, "InterruptedException exception"

    .line 51
    .line 52
    invoke-static {v0, v1, p1}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :catch_1
    move-exception p1

    .line 57
    const-string v1, "Keychain exception"

    .line 58
    .line 59
    const/4 v2, 0x0

    .line 60
    invoke-static {v0, v1, v2}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    .line 62
    .line 63
    const-string v1, "Exception details:"

    .line 64
    .line 65
    invoke-static {v0, v1, p1}, Lcom/microsoft/identity/common/internal/logging/Logger;->Oo08(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 66
    .line 67
    .line 68
    :goto_0
    iget-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient$1;->val$request:Landroid/webkit/ClientCertRequest;

    .line 69
    .line 70
    invoke-virtual {p1}, Landroid/webkit/ClientCertRequest;->cancel()V

    .line 71
    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

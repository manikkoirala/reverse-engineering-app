.class public interface abstract Lcom/microsoft/aad/adal/ITokenStoreQuery;
.super Ljava/lang/Object;
.source "ITokenStoreQuery.java"


# virtual methods
.method public abstract clearTokensForUser(Ljava/lang/String;)V
.end method

.method public abstract getAll()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/microsoft/aad/adal/TokenCacheItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTokensAboutToExpire()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/microsoft/aad/adal/TokenCacheItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTokensForResource(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/microsoft/aad/adal/TokenCacheItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTokensForUser(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/microsoft/aad/adal/TokenCacheItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUniqueUsersWithTokenCache()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

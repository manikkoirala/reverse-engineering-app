.class final Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;
.super Ljava/lang/Object;
.source "InstanceDiscoveryMetadata.java"


# instance fields
.field private final mAliases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mIsValidated:Z

.field private final mPreferredCache:Ljava/lang/String;

.field private final mPreferredNetwork:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mAliases:Ljava/util/List;

    .line 14
    iput-object p1, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mPreferredNetwork:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mPreferredCache:Ljava/lang/String;

    const/4 p1, 0x1

    .line 16
    iput-boolean p1, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mIsValidated:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mAliases:Ljava/util/List;

    .line 8
    iput-object p1, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mPreferredNetwork:Ljava/lang/String;

    .line 9
    iput-object p2, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mPreferredCache:Ljava/lang/String;

    .line 10
    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/4 p1, 0x1

    .line 11
    iput-boolean p1, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mIsValidated:Z

    return-void
.end method

.method constructor <init>(Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mAliases:Ljava/util/List;

    .line 3
    iput-boolean p1, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mIsValidated:Z

    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mPreferredNetwork:Ljava/lang/String;

    .line 5
    iput-object p1, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mPreferredCache:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAliases()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mAliases:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getPreferredCache()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mPreferredCache:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getPreferredNetwork()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mPreferredNetwork:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public isValidated()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->mIsValidated:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

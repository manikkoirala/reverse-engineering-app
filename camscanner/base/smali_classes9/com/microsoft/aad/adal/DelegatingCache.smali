.class Lcom/microsoft/aad/adal/DelegatingCache;
.super Ljava/lang/Object;
.source "DelegatingCache.java"

# interfaces
.implements Lcom/microsoft/aad/adal/ITokenCacheStore;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDelegate:Lcom/microsoft/aad/adal/ITokenCacheStore;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/microsoft/aad/adal/ITokenCacheStore;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/aad/adal/ITokenCacheStore;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/microsoft/aad/adal/DelegatingCache;->mContext:Landroid/content/Context;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/microsoft/aad/adal/DelegatingCache;->mDelegate:Lcom/microsoft/aad/adal/ITokenCacheStore;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public contains(Ljava/lang/String;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DelegatingCache;->mDelegate:Lcom/microsoft/aad/adal/ITokenCacheStore;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/microsoft/aad/adal/ITokenCacheStore;->contains(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public getAll()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/microsoft/aad/adal/TokenCacheItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DelegatingCache;->mDelegate:Lcom/microsoft/aad/adal/ITokenCacheStore;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/microsoft/aad/adal/ITokenCacheStore;->getAll()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getDelegateCache()Lcom/microsoft/aad/adal/ITokenCacheStore;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DelegatingCache;->mDelegate:Lcom/microsoft/aad/adal/ITokenCacheStore;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getItem(Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DelegatingCache;->mDelegate:Lcom/microsoft/aad/adal/ITokenCacheStore;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/microsoft/aad/adal/ITokenCacheStore;->getItem(Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public removeAll()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DelegatingCache;->mDelegate:Lcom/microsoft/aad/adal/ITokenCacheStore;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/microsoft/aad/adal/ITokenCacheStore;->removeAll()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/microsoft/aad/adal/DelegatingCache;->mContext:Landroid/content/Context;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->getMsalOAuth2TokenCache(Landroid/content/Context;)Lcom/microsoft/identity/common/java/cache/MsalOAuth2TokenCache;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/microsoft/identity/common/java/cache/MsalOAuth2TokenCache;->〇o〇()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public removeItem(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DelegatingCache;->mDelegate:Lcom/microsoft/aad/adal/ITokenCacheStore;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/microsoft/aad/adal/ITokenCacheStore;->removeItem(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setItem(Ljava/lang/String;Lcom/microsoft/aad/adal/TokenCacheItem;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DelegatingCache;->mDelegate:Lcom/microsoft/aad/adal/ITokenCacheStore;

    .line 2
    .line 3
    invoke-interface {v0, p1, p2}, Lcom/microsoft/aad/adal/ITokenCacheStore;->setItem(Ljava/lang/String;Lcom/microsoft/aad/adal/TokenCacheItem;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

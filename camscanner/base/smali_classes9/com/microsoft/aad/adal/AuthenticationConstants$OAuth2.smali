.class public final Lcom/microsoft/aad/adal/AuthenticationConstants$OAuth2;
.super Ljava/lang/Object;
.source "AuthenticationConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/aad/adal/AuthenticationConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OAuth2"
.end annotation


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field static final ADAL_CLIENT_FAMILY_ID:Ljava/lang/String; = "foci"

.field public static final ASSERTION:Ljava/lang/String; = "assertion"

.field public static final AUTHORITY:Ljava/lang/String; = "authority"

.field public static final AUTHORIZATION_CODE:Ljava/lang/String; = "authorization_code"

.field static final CLAIMS:Ljava/lang/String; = "claims"

.field public static final CLIENT_ID:Ljava/lang/String; = "client_id"

.field static final CLOUD_INSTANCE_HOST_NAME:Ljava/lang/String; = "cloud_instance_host_name"

.field public static final CODE:Ljava/lang/String; = "code"

.field public static final ERROR:Ljava/lang/String; = "error"

.field public static final ERROR_CODES:Ljava/lang/String; = "error_codes"

.field public static final ERROR_DESCRIPTION:Ljava/lang/String; = "error_description"

.field public static final EXPIRES_IN:Ljava/lang/String; = "expires_in"

.field static final EXT_EXPIRES_IN:Ljava/lang/String; = "ext_expires_in"

.field public static final GRANT_TYPE:Ljava/lang/String; = "grant_type"

.field static final HAS_CHROME:Ljava/lang/String; = "haschrome"

.field public static final HTTP_RESPONSE_BODY:Ljava/lang/String; = "response_body"

.field public static final HTTP_RESPONSE_HEADER:Ljava/lang/String; = "response_headers"

.field public static final HTTP_STATUS_CODE:Ljava/lang/String; = "status_code"

.field static final ID_TOKEN:Ljava/lang/String; = "id_token"

.field static final ID_TOKEN_EMAIL:Ljava/lang/String; = "email"

.field static final ID_TOKEN_FAMILY_NAME:Ljava/lang/String; = "family_name"

.field static final ID_TOKEN_GIVEN_NAME:Ljava/lang/String; = "given_name"

.field static final ID_TOKEN_IDENTITY_PROVIDER:Ljava/lang/String; = "idp"

.field static final ID_TOKEN_OBJECT_ID:Ljava/lang/String; = "oid"

.field static final ID_TOKEN_PASSWORD_CHANGE_URL:Ljava/lang/String; = "pwd_url"

.field static final ID_TOKEN_PASSWORD_EXPIRATION:Ljava/lang/String; = "pwd_exp"

.field static final ID_TOKEN_SUBJECT:Ljava/lang/String; = "sub"

.field static final ID_TOKEN_TENANTID:Ljava/lang/String; = "tid"

.field static final ID_TOKEN_UNIQUE_NAME:Ljava/lang/String; = "unique_name"

.field static final ID_TOKEN_UPN:Ljava/lang/String; = "upn"

.field public static final MSID_OAUTH2_SAML11_BEARER_VALUE:Ljava/lang/String; = "urn:ietf:params:oauth:grant-type:saml1_1-bearer"

.field public static final MSID_OAUTH2_SAML2_BEARER_VALUE:Ljava/lang/String; = "urn:ietf:params:oauth:grant-type:saml2-bearer"

.field public static final REDIRECT_URI:Ljava/lang/String; = "redirect_uri"

.field public static final REFRESH_TOKEN:Ljava/lang/String; = "refresh_token"

.field public static final RESPONSE_TYPE:Ljava/lang/String; = "response_type"

.field public static final SCOPE:Ljava/lang/String; = "scope"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final TOKEN_TYPE:Ljava/lang/String; = "token_type"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

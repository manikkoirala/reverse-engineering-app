.class public Lcom/microsoft/aad/adal/AuthenticationException;
.super Ljava/lang/Exception;
.source "AuthenticationException.java"


# static fields
.field static final serialVersionUID:J = 0x1L


# instance fields
.field private mCliTelemErrorCode:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private mCliTelemSubErrorCode:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private mCode:Lcom/microsoft/aad/adal/ADALError;

.field private mHttpResponseBody:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHttpResponseHeaders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mRefreshTokenAge:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private mServiceStatusCode:I

.field private mSpeRing:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseBody:Ljava/util/HashMap;

    const/4 v1, -0x1

    .line 3
    iput v1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mServiceStatusCode:I

    .line 4
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseHeaders:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/aad/adal/ADALError;)V
    .locals 2

    .line 5
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseBody:Ljava/util/HashMap;

    const/4 v1, -0x1

    .line 7
    iput v1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mServiceStatusCode:I

    .line 8
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 9
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mCode:Lcom/microsoft/aad/adal/ADALError;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V
    .locals 1

    .line 10
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 11
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseBody:Ljava/util/HashMap;

    const/4 v0, -0x1

    .line 12
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mServiceStatusCode:I

    .line 13
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 14
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mCode:Lcom/microsoft/aad/adal/ADALError;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)V
    .locals 1

    .line 26
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 27
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseBody:Ljava/util/HashMap;

    const/4 v0, -0x1

    .line 28
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mServiceStatusCode:I

    .line 29
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 30
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mCode:Lcom/microsoft/aad/adal/ADALError;

    .line 31
    invoke-virtual {p0, p3}, Lcom/microsoft/aad/adal/AuthenticationException;->setHttpResponse(Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)V

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;Ljava/lang/Throwable;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2, p4}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 33
    invoke-virtual {p0, p3}, Lcom/microsoft/aad/adal/AuthenticationException;->setHttpResponse(Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)V

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 15
    invoke-direct {p0, p2, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 p2, 0x0

    .line 16
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseBody:Ljava/util/HashMap;

    const/4 v0, -0x1

    .line 17
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mServiceStatusCode:I

    .line 18
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 19
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mCode:Lcom/microsoft/aad/adal/ADALError;

    if-nez p3, :cond_0

    return-void

    .line 20
    :cond_0
    instance-of p1, p3, Lcom/microsoft/aad/adal/AuthenticationException;

    if-eqz p1, :cond_2

    .line 21
    check-cast p3, Lcom/microsoft/aad/adal/AuthenticationException;

    invoke-virtual {p3}, Lcom/microsoft/aad/adal/AuthenticationException;->getServiceStatusCode()I

    move-result p1

    iput p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mServiceStatusCode:I

    .line 22
    invoke-virtual {p3}, Lcom/microsoft/aad/adal/AuthenticationException;->getHttpResponseBody()Ljava/util/HashMap;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 23
    new-instance p1, Ljava/util/HashMap;

    invoke-virtual {p3}, Lcom/microsoft/aad/adal/AuthenticationException;->getHttpResponseBody()Ljava/util/HashMap;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseBody:Ljava/util/HashMap;

    .line 24
    :cond_1
    invoke-virtual {p3}, Lcom/microsoft/aad/adal/AuthenticationException;->getHttpResponseHeaders()Ljava/util/HashMap;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 25
    new-instance p1, Ljava/util/HashMap;

    invoke-virtual {p3}, Lcom/microsoft/aad/adal/AuthenticationException;->getHttpResponseHeaders()Ljava/util/HashMap;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseHeaders:Ljava/util/HashMap;

    :cond_2
    return-void
.end method


# virtual methods
.method getCliTelemErrorCode()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mCliTelemErrorCode:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getCliTelemSubErrorCode()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mCliTelemSubErrorCode:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getCode()Lcom/microsoft/aad/adal/ADALError;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mCode:Lcom/microsoft/aad/adal/ADALError;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getHttpResponseBody()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseBody:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getHttpResponseHeaders()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getLocalizedMessage(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-super {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-super {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mCode:Lcom/microsoft/aad/adal/ADALError;

    .line 17
    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {v0, p1}, Lcom/microsoft/aad/adal/ADALError;->getLocalizedDescription(Landroid/content/Context;)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    return-object p1

    .line 25
    :cond_1
    const/4 p1, 0x0

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/microsoft/aad/adal/AuthenticationException;->getLocalizedMessage(Landroid/content/Context;)Ljava/lang/String;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getRefreshTokenAge()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mRefreshTokenAge:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getServiceStatusCode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mServiceStatusCode:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getSpeRing()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mSpeRing:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method setCliTelemErrorCode(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mCliTelemErrorCode:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setCliTelemSubErrorCode(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mCliTelemSubErrorCode:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setHttpResponse(Lcom/microsoft/aad/adal/AuthenticationResult;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 9
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getHttpResponseBody()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseBody:Ljava/util/HashMap;

    .line 10
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getHttpResponseHeaders()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 11
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getServiceStatusCode()I

    move-result p1

    iput p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mServiceStatusCode:I

    :cond_0
    return-void
.end method

.method setHttpResponse(Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)V
    .locals 3

    if-eqz p1, :cond_1

    .line 1
    invoke-virtual {p1}, Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;->〇o〇()I

    move-result v0

    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mServiceStatusCode:I

    .line 2
    invoke-virtual {p1}, Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;->〇o00〇〇Oo()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    new-instance v0, Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;->〇o00〇〇Oo()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 4
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;->〇080()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 5
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-static {p1}, Lcom/microsoft/identity/common/adal/internal/util/HashMapExtensions;->〇080(Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)Ljava/util/HashMap;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseBody:Ljava/util/HashMap;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 6
    const-class v0, Lcom/microsoft/aad/adal/AuthenticationException;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 7
    invoke-static {p1}, Lcom/microsoft/aad/adal/ExceptionExtensions;->getExceptionMessage(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->SERVER_INVALID_JSON_RESPONSE:Lcom/microsoft/aad/adal/ADALError;

    const-string v2, "Json exception"

    .line 8
    invoke-static {v0, v2, p1, v1}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    :cond_1
    :goto_0
    return-void
.end method

.method setHttpResponseBody(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseBody:Ljava/util/HashMap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setHttpResponseHeaders(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setRefreshTokenAge(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mRefreshTokenAge:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setServiceStatusCode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mServiceStatusCode:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setSpeRing(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationException;->mSpeRing:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

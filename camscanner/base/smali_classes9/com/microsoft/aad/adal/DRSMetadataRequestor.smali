.class final Lcom/microsoft/aad/adal/DRSMetadataRequestor;
.super Lcom/microsoft/aad/adal/AbstractMetadataRequestor;
.source "DRSMetadataRequestor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/aad/adal/AbstractMetadataRequestor<",
        "Lcom/microsoft/aad/adal/DRSMetadata;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final CLOUD_RESOLVER_DOMAIN:Ljava/lang/String; = "windows.net/"

.field private static final DRS_URL_PREFIX:Ljava/lang/String; = "https://enterpriseregistration."

.field private static final TAG:Ljava/lang/String; = "DRSMetadataRequestor"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private requestCloud(Ljava/lang/String;)Lcom/microsoft/aad/adal/DRSMetadata;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->TAG:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "Requesting DRS discovery (cloud)"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :try_start_0
    sget-object v0, Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;->CLOUD:Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;

    .line 9
    .line 10
    invoke-direct {p0, v0, p1}, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->requestDrsDiscoveryInternal(Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;Ljava/lang/String;)Lcom/microsoft/aad/adal/DRSMetadata;

    .line 11
    .line 12
    .line 13
    move-result-object p1
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    return-object p1

    .line 15
    :catch_0
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 16
    .line 17
    sget-object v0, Lcom/microsoft/aad/adal/ADALError;->DRS_DISCOVERY_FAILED_UNKNOWN_HOST:Lcom/microsoft/aad/adal/ADALError;

    .line 18
    .line 19
    invoke-direct {p1, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;)V

    .line 20
    .line 21
    .line 22
    throw p1
    .line 23
    .line 24
    .line 25
.end method

.method private requestDrsDiscoveryInternal(Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;Ljava/lang/String;)Lcom/microsoft/aad/adal/DRSMetadata;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;,
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .line 1
    :try_start_0
    new-instance v0, Ljava/net/URL;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->buildRequestUrlByType(Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_2

    .line 8
    .line 9
    .line 10
    new-instance p1, Ljava/util/HashMap;

    .line 11
    .line 12
    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string p2, "Accept"

    .line 16
    .line 17
    const-string v1, "application/json"

    .line 18
    .line 19
    invoke-interface {p1, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;->getCorrelationId()Ljava/util/UUID;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    if-eqz p2, :cond_0

    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;->getCorrelationId()Ljava/util/UUID;

    .line 29
    .line 30
    .line 31
    move-result-object p2

    .line 32
    invoke-virtual {p2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    const-string v1, "client-request-id"

    .line 37
    .line 38
    invoke-interface {p1, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;->getWebrequestHandler()Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;

    .line 42
    .line 43
    .line 44
    move-result-object p2

    .line 45
    invoke-interface {p2, v0, p1}, Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;->〇o00〇〇Oo(Ljava/net/URL;Ljava/util/Map;)Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;->〇o〇()I

    .line 50
    .line 51
    .line 52
    move-result p2

    .line 53
    const/16 v0, 0xc8

    .line 54
    .line 55
    if-ne v0, p2, :cond_1

    .line 56
    .line 57
    invoke-virtual {p0, p1}, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->parseMetadata(Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)Lcom/microsoft/aad/adal/DRSMetadata;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    return-object p1

    .line 62
    :cond_1
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 63
    .line 64
    sget-object v0, Lcom/microsoft/aad/adal/ADALError;->DRS_FAILED_SERVER_ERROR:Lcom/microsoft/aad/adal/ADALError;

    .line 65
    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    .line 67
    .line 68
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .line 70
    .line 71
    const-string v2, "Unexpected error code: ["

    .line 72
    .line 73
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    const-string p2, "]"

    .line 80
    .line 81
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object p2

    .line 88
    invoke-direct {p1, v0, p2}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    throw p1
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 92
    :catch_0
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 93
    .line 94
    sget-object p2, Lcom/microsoft/aad/adal/ADALError;->IO_EXCEPTION:Lcom/microsoft/aad/adal/ADALError;

    .line 95
    .line 96
    invoke-direct {p1, p2}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;)V

    .line 97
    .line 98
    .line 99
    throw p1

    .line 100
    :catch_1
    move-exception p1

    .line 101
    throw p1

    .line 102
    :catch_2
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 103
    .line 104
    sget-object p2, Lcom/microsoft/aad/adal/ADALError;->DRS_METADATA_URL_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 105
    .line 106
    invoke-direct {p1, p2}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;)V

    .line 107
    .line 108
    .line 109
    throw p1
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method private requestOnPrem(Ljava/lang/String;)Lcom/microsoft/aad/adal/DRSMetadata;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;,
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->TAG:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "Requesting DRS discovery (on-prem)"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object v0, Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;->ON_PREM:Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;

    .line 9
    .line 10
    invoke-direct {p0, v0, p1}, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->requestDrsDiscoveryInternal(Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;Ljava/lang/String;)Lcom/microsoft/aad/adal/DRSMetadata;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method buildRequestUrlByType(Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    const-string v1, "https://enterpriseregistration."

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object v1, Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;->CLOUD:Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;

    .line 9
    .line 10
    if-ne v1, p1, :cond_0

    .line 11
    .line 12
    const-string/jumbo p1, "windows.net/"

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    sget-object v1, Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;->ON_PREM:Lcom/microsoft/aad/adal/DRSMetadataRequestor$Type;

    .line 23
    .line 24
    if-ne v1, p1, :cond_1

    .line 25
    .line 26
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    :cond_1
    :goto_0
    const-string p1, "/enrollmentserver/contract?api-version=1.0"

    .line 30
    .line 31
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    sget-object p2, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->TAG:Ljava/lang/String;

    .line 39
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    .line 41
    .line 42
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .line 44
    .line 45
    const-string v1, "URL: "

    .line 46
    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    const/4 v1, 0x0

    .line 58
    const-string v2, "Request will use DRS url. "

    .line 59
    .line 60
    invoke-static {p2, v2, v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 61
    .line 62
    .line 63
    return-object p1
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method parseMetadata(Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)Lcom/microsoft/aad/adal/DRSMetadata;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 2
    sget-object v0, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->TAG:Ljava/lang/String;

    const-string v1, "Parsing DRS metadata response"

    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;->parser()Lcom/google/gson/Gson;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;->〇080()Ljava/lang/String;

    move-result-object p1

    const-class v1, Lcom/microsoft/aad/adal/DRSMetadata;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->OO0o〇〇〇〇0(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/microsoft/aad/adal/DRSMetadata;
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 4
    :catch_0
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    sget-object v0, Lcom/microsoft/aad/adal/ADALError;->JSON_PARSE_ERROR:Lcom/microsoft/aad/adal/ADALError;

    invoke-direct {p1, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;)V

    throw p1
.end method

.method bridge synthetic parseMetadata(Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->parseMetadata(Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)Lcom/microsoft/aad/adal/DRSMetadata;

    move-result-object p1

    return-object p1
.end method

.method requestMetadata(Ljava/lang/String;)Lcom/microsoft/aad/adal/DRSMetadata;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 2
    :try_start_0
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->requestOnPrem(Ljava/lang/String;)Lcom/microsoft/aad/adal/DRSMetadata;

    move-result-object p1
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 3
    :catch_0
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->requestCloud(Ljava/lang/String;)Lcom/microsoft/aad/adal/DRSMetadata;

    move-result-object p1

    return-object p1
.end method

.method bridge synthetic requestMetadata(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/microsoft/aad/adal/DRSMetadataRequestor;->requestMetadata(Ljava/lang/String;)Lcom/microsoft/aad/adal/DRSMetadata;

    move-result-object p1

    return-object p1
.end method

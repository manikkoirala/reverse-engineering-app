.class Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;
.super Lcom/microsoft/aad/adal/BasicWebViewClient;
.source "AuthenticationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/aad/adal/AuthenticationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;


# direct methods
.method constructor <init>(Lcom/microsoft/aad/adal/AuthenticationActivity;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$300(Lcom/microsoft/aad/adal/AuthenticationActivity;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {p1}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$400(Lcom/microsoft/aad/adal/AuthenticationActivity;)Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-static {p1}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$500(Lcom/microsoft/aad/adal/AuthenticationActivity;)Lcom/microsoft/aad/adal/UIEvent;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/microsoft/aad/adal/BasicWebViewClient;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/UIEvent;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public cancelWebViewRequest(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$1300(Lcom/microsoft/aad/adal/AuthenticationActivity;Landroid/content/Intent;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public onReceivedClientCertRequest(Landroid/webkit/WebView;Landroid/webkit/ClientCertRequest;)V
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 1
    const-string p1, "Webview receives client TLS request."

    .line 2
    .line 3
    const-string v0, "AuthenticationActivity:onReceivedClientCertRequest"

    .line 4
    .line 5
    invoke-static {v0, p1}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇O00(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p2}, Landroid/webkit/ClientCertRequest;->getPrincipals()[Ljava/security/Principal;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    if-eqz p1, :cond_1

    .line 13
    .line 14
    array-length v1, p1

    .line 15
    const/4 v2, 0x0

    .line 16
    :goto_0
    if-ge v2, v1, :cond_1

    .line 17
    .line 18
    aget-object v3, p1, v2

    .line 19
    .line 20
    invoke-interface {v3}, Ljava/security/Principal;->getName()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    const-string v4, "CN=MS-Organization-Access"

    .line 25
    .line 26
    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    if-eqz v3, :cond_0

    .line 31
    .line 32
    const-string p1, "Cancelling the TLS request, not respond to TLS challenge triggered by device authentication."

    .line 33
    .line 34
    invoke-static {v0, p1}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇O00(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p2}, Landroid/webkit/ClientCertRequest;->cancel()V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    iget-object v3, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 45
    .line 46
    new-instance v4, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient$1;

    .line 47
    .line 48
    invoke-direct {v4, p0, p2}, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient$1;-><init>(Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;Landroid/webkit/ClientCertRequest;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p2}, Landroid/webkit/ClientCertRequest;->getKeyTypes()[Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v5

    .line 55
    invoke-virtual {p2}, Landroid/webkit/ClientCertRequest;->getPrincipals()[Ljava/security/Principal;

    .line 56
    .line 57
    .line 58
    move-result-object v6

    .line 59
    invoke-virtual {p2}, Landroid/webkit/ClientCertRequest;->getHost()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v7

    .line 63
    invoke-virtual {p2}, Landroid/webkit/ClientCertRequest;->getPort()I

    .line 64
    .line 65
    .line 66
    move-result v8

    .line 67
    const/4 v9, 0x0

    .line 68
    invoke-static/range {v3 .. v9}, Landroid/security/KeyChain;->choosePrivateKeyAlias(Landroid/app/Activity;Landroid/security/KeyChainAliasCallback;[Ljava/lang/String;[Ljava/security/Principal;Ljava/lang/String;ILjava/lang/String;)V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public postRunnable(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$200(Lcom/microsoft/aad/adal/AuthenticationActivity;)Landroid/webkit/WebView;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public prepareForBrokerResumeRequest()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$1400(Lcom/microsoft/aad/adal/AuthenticationActivity;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public processInvalidUrl(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$600(Lcom/microsoft/aad/adal/AuthenticationActivity;Landroid/content/Intent;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x0

    .line 13
    const-string v3, "AuthenticationActivity:processInvalidUrl"

    .line 14
    .line 15
    const/4 v4, 0x1

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    const-string v0, "msauth"

    .line 19
    .line 20
    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_0

    .line 25
    .line 26
    const-string v0, "The RedirectUri is not as expected."

    .line 27
    .line 28
    invoke-static {v3, v0, v2}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    .line 30
    .line 31
    const/4 v0, 0x2

    .line 32
    new-array v5, v0, [Ljava/lang/Object;

    .line 33
    .line 34
    aput-object p2, v5, v1

    .line 35
    .line 36
    iget-object v6, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 37
    .line 38
    invoke-static {v6}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$300(Lcom/microsoft/aad/adal/AuthenticationActivity;)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v6

    .line 42
    aput-object v6, v5, v4

    .line 43
    .line 44
    const-string v6, "Received %s and expected %s"

    .line 45
    .line 46
    invoke-static {v6, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v5

    .line 50
    invoke-static {v3, v5, v2}, Lcom/microsoft/identity/common/internal/logging/Logger;->Oo08(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    .line 52
    .line 53
    iget-object v2, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 54
    .line 55
    sget-object v3, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_REDIRECTURI_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 56
    .line 57
    new-array v0, v0, [Ljava/lang/Object;

    .line 58
    .line 59
    aput-object p2, v0, v1

    .line 60
    .line 61
    invoke-static {v2}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$300(Lcom/microsoft/aad/adal/AuthenticationActivity;)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    aput-object p2, v0, v4

    .line 66
    .line 67
    const-string p2, "The RedirectUri is not as expected. Received %s and expected %s"

    .line 68
    .line 69
    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object p2

    .line 73
    invoke-static {v2, v3, p2}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$1200(Lcom/microsoft/aad/adal/AuthenticationActivity;Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 77
    .line 78
    .line 79
    return v4

    .line 80
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 81
    .line 82
    invoke-virtual {p2, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v5

    .line 86
    const-string v6, "about:blank"

    .line 87
    .line 88
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 89
    .line 90
    .line 91
    move-result v5

    .line 92
    if-eqz v5, :cond_1

    .line 93
    .line 94
    const-string p1, "It is an blank page request"

    .line 95
    .line 96
    invoke-static {v3, p1}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇O00(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    return v4

    .line 100
    :cond_1
    invoke-virtual {p2, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p2

    .line 104
    const-string v0, "https://"

    .line 105
    .line 106
    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 107
    .line 108
    .line 109
    move-result p2

    .line 110
    if-nez p2, :cond_2

    .line 111
    .line 112
    const-string p2, "The webview was redirected to an unsafe URL."

    .line 113
    .line 114
    invoke-static {v3, p2, v2}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 115
    .line 116
    .line 117
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 118
    .line 119
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->WEBVIEW_REDIRECTURL_NOT_SSL_PROTECTED:Lcom/microsoft/aad/adal/ADALError;

    .line 120
    .line 121
    invoke-static {v0, v1, p2}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$1200(Lcom/microsoft/aad/adal/AuthenticationActivity;Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 125
    .line 126
    .line 127
    return v4

    .line 128
    :cond_2
    return v1
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public processRedirectUrl(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$600(Lcom/microsoft/aad/adal/AuthenticationActivity;Landroid/content/Intent;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const-string v1, "AuthenticationActivity:processRedirectUrl"

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "It is not a broker request"

    .line 16
    .line 17
    invoke-static {v1, v0}, Lcom/microsoft/identity/common/internal/logging/Logger;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    new-instance v0, Landroid/content/Intent;

    .line 21
    .line 22
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v1, "com.microsoft.aad.adal:BrowserFinalUrl"

    .line 26
    .line 27
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 28
    .line 29
    .line 30
    iget-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 31
    .line 32
    invoke-static {p2}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$400(Lcom/microsoft/aad/adal/AuthenticationActivity;)Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    const-string v1, "com.microsoft.aad.adal:BrowserRequestInfo"

    .line 37
    .line 38
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 39
    .line 40
    .line 41
    iget-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 42
    .line 43
    const/16 v1, 0x7d3

    .line 44
    .line 45
    invoke-static {p2, v1, v0}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$700(Lcom/microsoft/aad/adal/AuthenticationActivity;ILandroid/content/Intent;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    const-string v0, "It is a broker request"

    .line 53
    .line 54
    invoke-static {v1, v0}, Lcom/microsoft/identity/common/internal/logging/Logger;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 58
    .line 59
    const/4 v1, 0x1

    .line 60
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$800(Lcom/microsoft/aad/adal/AuthenticationActivity;Z)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 64
    .line 65
    .line 66
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTask;

    .line 67
    .line 68
    iget-object v3, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 69
    .line 70
    invoke-static {v3}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$900(Lcom/microsoft/aad/adal/AuthenticationActivity;)Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;

    .line 71
    .line 72
    .line 73
    move-result-object v4

    .line 74
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 75
    .line 76
    invoke-static {v0}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$400(Lcom/microsoft/aad/adal/AuthenticationActivity;)Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 77
    .line 78
    .line 79
    move-result-object v5

    .line 80
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 81
    .line 82
    invoke-static {v0}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$1000(Lcom/microsoft/aad/adal/AuthenticationActivity;)Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v6

    .line 86
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 87
    .line 88
    invoke-static {v0}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$1100(Lcom/microsoft/aad/adal/AuthenticationActivity;)I

    .line 89
    .line 90
    .line 91
    move-result v7

    .line 92
    move-object v2, p1

    .line 93
    invoke-direct/range {v2 .. v7}, Lcom/microsoft/aad/adal/AuthenticationActivity$TokenTask;-><init>(Lcom/microsoft/aad/adal/AuthenticationActivity;Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;Lcom/microsoft/aad/adal/AuthenticationRequest;Ljava/lang/String;I)V

    .line 94
    .line 95
    .line 96
    new-array v0, v1, [Ljava/lang/String;

    .line 97
    .line 98
    const/4 v1, 0x0

    .line 99
    aput-object p2, v0, v1

    .line 100
    .line 101
    invoke-virtual {p1, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 102
    .line 103
    .line 104
    :goto_0
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public sendResponse(ILandroid/content/Intent;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 2
    .line 3
    invoke-static {v0, p1, p2}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$700(Lcom/microsoft/aad/adal/AuthenticationActivity;ILandroid/content/Intent;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public setPKeyAuthStatus(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$1502(Lcom/microsoft/aad/adal/AuthenticationActivity;Z)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public showSpinner(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationActivity$CustomWebViewClient;->this$0:Lcom/microsoft/aad/adal/AuthenticationActivity;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/microsoft/aad/adal/AuthenticationActivity;->access$800(Lcom/microsoft/aad/adal/AuthenticationActivity;Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

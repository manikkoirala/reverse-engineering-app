.class final Lcom/microsoft/aad/adal/EventStrings;
.super Ljava/lang/Object;
.source "EventStrings.java"


# static fields
.field static final ACQUIRE_TOKEN_1:Ljava/lang/String; = "100"

.field static final ACQUIRE_TOKEN_10:Ljava/lang/String; = "120"

.field static final ACQUIRE_TOKEN_2:Ljava/lang/String; = "104"

.field static final ACQUIRE_TOKEN_3:Ljava/lang/String; = "108"

.field static final ACQUIRE_TOKEN_4:Ljava/lang/String; = "111"

.field static final ACQUIRE_TOKEN_5:Ljava/lang/String; = "115"

.field static final ACQUIRE_TOKEN_6:Ljava/lang/String; = "116"

.field static final ACQUIRE_TOKEN_7:Ljava/lang/String; = "117"

.field static final ACQUIRE_TOKEN_8:Ljava/lang/String; = "118"

.field static final ACQUIRE_TOKEN_9:Ljava/lang/String; = "119"

.field static final ACQUIRE_TOKEN_SILENT:Ljava/lang/String; = "2"

.field static final ACQUIRE_TOKEN_SILENT_ASYNC:Ljava/lang/String; = "3"

.field static final ACQUIRE_TOKEN_SILENT_ASYNC_CLAIMS_CHALLENGE:Ljava/lang/String; = "16"

.field static final ACQUIRE_TOKEN_SILENT_ASYNC_FORCE_REFRESH:Ljava/lang/String; = "14"

.field static final ACQUIRE_TOKEN_SILENT_SYNC:Ljava/lang/String; = "1"

.field static final ACQUIRE_TOKEN_SILENT_SYNC_CLAIMS_CHALLENGE:Ljava/lang/String; = "15"

.field static final ACQUIRE_TOKEN_SILENT_SYNC_FORCE_REFRESH:Ljava/lang/String; = "13"

.field static final ACQUIRE_TOKEN_WITH_REFRESH_TOKEN:Ljava/lang/String; = "4"

.field static final ACQUIRE_TOKEN_WITH_REFRESH_TOKEN_2:Ljava/lang/String; = "5"

.field static final ACQUIRE_TOKEN_WITH_SAML_ASSERTION:Ljava/lang/String; = "6"

.field static final API_DEPRECATED:Ljava/lang/String; = "Microsoft.ADAL.is_deprecated"

.field static final API_ERROR_CODE:Ljava/lang/String; = "Microsoft.ADAL.api_error_code"

.field static final API_EVENT:Ljava/lang/String; = "Microsoft.ADAL.api_event"

.field static final API_ID:Ljava/lang/String; = "Microsoft.ADAL.api_id"

.field static final APPLICATION_NAME:Ljava/lang/String; = "Microsoft.ADAL.application_name"

.field static final APPLICATION_VERSION:Ljava/lang/String; = "Microsoft.ADAL.application_version"

.field static final AUTHORITY_NAME:Ljava/lang/String; = "Microsoft.ADAL.authority"

.field static final AUTHORITY_TYPE:Ljava/lang/String; = "Microsoft.ADAL.authority_type"

.field static final AUTHORITY_TYPE_AAD:Ljava/lang/String; = "aad"

.field static final AUTHORITY_TYPE_ADFS:Ljava/lang/String; = "adfs"

.field static final AUTHORITY_VALIDATION:Ljava/lang/String; = "Microsoft.ADAL.authority_validation_status"

.field static final AUTHORITY_VALIDATION_EVENT:Ljava/lang/String; = "Microsoft.ADAL.authority_validation"

.field static final AUTHORITY_VALIDATION_FAILURE:Ljava/lang/String; = "no"

.field static final AUTHORITY_VALIDATION_NOT_DONE:Ljava/lang/String; = "not_done"

.field static final AUTHORITY_VALIDATION_SUCCESS:Ljava/lang/String; = "yes"

.field static final BROKER_ACCOUNT_SERVICE_BINDING_SUCCEED:Ljava/lang/String; = "Microsoft.ADAL.broker_account_service_binding_succeed"

.field static final BROKER_ACCOUNT_SERVICE_CONNECTED:Ljava/lang/String; = "Microsoft.ADAL.broker_account_service_connected"

.field static final BROKER_ACCOUNT_SERVICE_STARTS_BINDING:Ljava/lang/String; = "Microsoft.ADAL.broker_account_service_starts_binding"

.field static final BROKER_APP:Ljava/lang/String; = "Microsoft.ADAL.broker_app"

.field static final BROKER_APP_USED:Ljava/lang/String; = "Microsoft.ADAL.broker_app_used"

.field static final BROKER_EVENT:Ljava/lang/String; = "Microsoft.ADAL.broker_event"

.field static final BROKER_REQUEST_INTERACTIVE:Ljava/lang/String; = "Microsoft.ADAL.broker_request_interactive"

.field static final BROKER_REQUEST_SILENT:Ljava/lang/String; = "Microsoft.ADAL.broker_request_silent"

.field static final BROKER_VERSION:Ljava/lang/String; = "Microsoft.ADAL.broker_version"

.field static final CACHE_EVENT_COUNT:Ljava/lang/String; = "Microsoft.ADAL.cache_event_count"

.field static final CLIENT_ID:Ljava/lang/String; = "Microsoft.ADAL.client_id"

.field static final CORRELATION_ID:Ljava/lang/String; = "Microsoft.ADAL.correlation_id"

.field static final DEVICE_ID:Ljava/lang/String; = "Microsoft.ADAL.device_id"

.field static final EVENT_NAME:Ljava/lang/String; = "Microsoft.ADAL.event_name"

.field private static final EVENT_PREFIX:Ljava/lang/String; = "Microsoft.ADAL."

.field static final EXTENDED_EXPIRES_ON_SETTING:Ljava/lang/String; = "Microsoft.ADAL.extended_expires_on_setting"

.field static final HTTP_API_VERSION:Ljava/lang/String; = "Microsoft.ADAL.api_version"

.field static final HTTP_EVENT:Ljava/lang/String; = "Microsoft.ADAL.http_event"

.field static final HTTP_EVENT_COUNT:Ljava/lang/String; = "Microsoft.ADAL.http_event_count"

.field static final HTTP_METHOD:Ljava/lang/String; = "Microsoft.ADAL.method"

.field static final HTTP_METHOD_POST:Ljava/lang/String; = "Microsoft.ADAL.post"

.field static final HTTP_PATH:Ljava/lang/String; = "Microsoft.ADAL.http_path"

.field static final HTTP_QUERY_PARAMETERS:Ljava/lang/String; = "Microsoft.ADAL.query_params"

.field static final HTTP_RESPONSE_CODE:Ljava/lang/String; = "Microsoft.ADAL.response_code"

.field static final HTTP_USER_AGENT:Ljava/lang/String; = "Microsoft.ADAL.user_agent"

.field static final IDP_NAME:Ljava/lang/String; = "Microsoft.ADAL.idp"

.field static final LOGIN_HINT:Ljava/lang/String; = "Microsoft.ADAL.login_hint"

.field static final NTLM:Ljava/lang/String; = "Microsoft.ADAL.ntlm"

.field static final OAUTH_ERROR_CODE:Ljava/lang/String; = "Microsoft.ADAL.oauth_error_code"

.field static final PROMPT_BEHAVIOR:Ljava/lang/String; = "Microsoft.ADAL.prompt_behavior"

.field static final REDIRECT_COUNT:Ljava/lang/String; = "Microsoft.ADAL.redirect_count"

.field static final REQUEST_ID:Ljava/lang/String; = "Microsoft.ADAL.request_id"

.field static final REQUEST_ID_HEADER:Ljava/lang/String; = "Microsoft.ADAL.x_ms_request_id"

.field static final RESPONSE_TIME:Ljava/lang/String; = "Microsoft.ADAL.response_time"

.field static final SERVER_ERROR_CODE:Ljava/lang/String; = "Microsoft.ADAL.server_error_code"

.field static final SERVER_SUBERROR_CODE:Ljava/lang/String; = "Microsoft.ADAL.server_sub_error_code"

.field static final SPE_INFO:Ljava/lang/String; = "Microsoft.ADAL.spe_info"

.field static final START_TIME:Ljava/lang/String; = "Microsoft.ADAL.start_time"

.field static final STOP_TIME:Ljava/lang/String; = "Microsoft.ADAL.stop_time"

.field static final TENANT_ID:Ljava/lang/String; = "Microsoft.ADAL.tenant_id"

.field static final TOKEN_AGE:Ljava/lang/String; = "Microsoft.ADAL.rt_age"

.field static final TOKEN_CACHE_DELETE:Ljava/lang/String; = "Microsoft.ADAL.token_cache_delete"

.field static final TOKEN_CACHE_LOOKUP:Ljava/lang/String; = "Microsoft.ADAL.token_cache_lookup"

.field static final TOKEN_CACHE_WRITE:Ljava/lang/String; = "Microsoft.ADAL.token_cache_write"

.field static final TOKEN_TYPE:Ljava/lang/String; = "Microsoft.ADAL.token_type"

.field static final TOKEN_TYPE_FRT:Ljava/lang/String; = "Microsoft.ADAL.frt"

.field static final TOKEN_TYPE_IS_FRT:Ljava/lang/String; = "Microsoft.ADAL.is_frt"

.field static final TOKEN_TYPE_IS_MRRT:Ljava/lang/String; = "Microsoft.ADAL.is_mrrt"

.field static final TOKEN_TYPE_IS_RT:Ljava/lang/String; = "Microsoft.ADAL.is_rt"

.field static final TOKEN_TYPE_MRRT:Ljava/lang/String; = "Microsoft.ADAL.mrrt"

.field static final TOKEN_TYPE_RT:Ljava/lang/String; = "Microsoft.ADAL.rt"

.field static final UI_EVENT:Ljava/lang/String; = "Microsoft.ADAL.ui_event"

.field static final UI_EVENT_COUNT:Ljava/lang/String; = "Microsoft.ADAL.ui_event_count"

.field static final USER_CANCEL:Ljava/lang/String; = "Microsoft.ADAL.user_cancel"

.field static final USER_ID:Ljava/lang/String; = "Microsoft.ADAL.user_id"

.field static final WAS_SUCCESSFUL:Ljava/lang/String; = "Microsoft.ADAL.is_successful"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

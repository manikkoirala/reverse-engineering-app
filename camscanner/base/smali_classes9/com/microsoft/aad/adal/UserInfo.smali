.class public Lcom/microsoft/aad/adal/UserInfo;
.super Ljava/lang/Object;
.source "UserInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x79fcce71f9b9e9d0L


# instance fields
.field private mDisplayableId:Ljava/lang/String;

.field private mFamilyName:Ljava/lang/String;

.field private mGivenName:Ljava/lang/String;

.field private mIdentityProvider:Ljava/lang/String;

.field private transient mPasswordChangeUrl:Landroid/net/Uri;

.field private transient mPasswordExpiresOn:Ljava/util/Date;

.field private mUniqueId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/aad/adal/IdToken;)V
    .locals 6

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 11
    iput-object v0, p0, Lcom/microsoft/aad/adal/UserInfo;->mUniqueId:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/microsoft/aad/adal/UserInfo;->mDisplayableId:Ljava/lang/String;

    .line 13
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getObjectId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 14
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getObjectId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/aad/adal/UserInfo;->mUniqueId:Ljava/lang/String;

    goto :goto_0

    .line 15
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getSubject()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 16
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getSubject()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/aad/adal/UserInfo;->mUniqueId:Ljava/lang/String;

    .line 17
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getUpn()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 18
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getUpn()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/aad/adal/UserInfo;->mDisplayableId:Ljava/lang/String;

    goto :goto_1

    .line 19
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 20
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getEmail()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/aad/adal/UserInfo;->mDisplayableId:Ljava/lang/String;

    .line 21
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getGivenName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/aad/adal/UserInfo;->mGivenName:Ljava/lang/String;

    .line 22
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getFamilyName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/aad/adal/UserInfo;->mFamilyName:Ljava/lang/String;

    .line 23
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getIdentityProvider()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/aad/adal/UserInfo;->mIdentityProvider:Ljava/lang/String;

    .line 24
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getPasswordExpiration()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_4

    .line 25
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 26
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getPasswordExpiration()J

    move-result-wide v2

    long-to-int v3, v2

    const/16 v2, 0xd

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 27
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/aad/adal/UserInfo;->mPasswordExpiresOn:Ljava/util/Date;

    .line 28
    :cond_4
    iput-object v0, p0, Lcom/microsoft/aad/adal/UserInfo;->mPasswordChangeUrl:Landroid/net/Uri;

    .line 29
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getPasswordChangeUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 30
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/IdToken;->getPasswordChangeUrl()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/aad/adal/UserInfo;->mPasswordChangeUrl:Landroid/net/Uri;

    :cond_5
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/microsoft/aad/adal/UserInfo;->mDisplayableId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    iput-object p1, p0, Lcom/microsoft/aad/adal/UserInfo;->mUniqueId:Ljava/lang/String;

    .line 6
    iput-object p2, p0, Lcom/microsoft/aad/adal/UserInfo;->mGivenName:Ljava/lang/String;

    .line 7
    iput-object p3, p0, Lcom/microsoft/aad/adal/UserInfo;->mFamilyName:Ljava/lang/String;

    .line 8
    iput-object p4, p0, Lcom/microsoft/aad/adal/UserInfo;->mIdentityProvider:Ljava/lang/String;

    .line 9
    iput-object p5, p0, Lcom/microsoft/aad/adal/UserInfo;->mDisplayableId:Ljava/lang/String;

    return-void
.end method

.method static getUserInfoFromBrokerResult(Landroid/os/Bundle;)Lcom/microsoft/aad/adal/UserInfo;
    .locals 7

    .line 1
    const-string v0, "account.userinfo.userid"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v2

    .line 7
    const-string v0, "account.userinfo.given.name"

    .line 8
    .line 9
    invoke-virtual {p0, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    const-string v0, "account.userinfo.family.name"

    .line 14
    .line 15
    invoke-virtual {p0, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v4

    .line 19
    const-string v0, "account.userinfo.identity.provider"

    .line 20
    .line 21
    invoke-virtual {p0, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v5

    .line 25
    const-string v0, "account.userinfo.userid.displayable"

    .line 26
    .line 27
    invoke-virtual {p0, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v6

    .line 31
    new-instance p0, Lcom/microsoft/aad/adal/UserInfo;

    .line 32
    .line 33
    move-object v1, p0

    .line 34
    invoke-direct/range {v1 .. v6}, Lcom/microsoft/aad/adal/UserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    return-object p0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method


# virtual methods
.method public getDisplayableId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/UserInfo;->mDisplayableId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getFamilyName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/UserInfo;->mFamilyName:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getGivenName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/UserInfo;->mGivenName:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getIdentityProvider()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/UserInfo;->mIdentityProvider:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getPasswordChangeUrl()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/UserInfo;->mPasswordChangeUrl:Landroid/net/Uri;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getPasswordExpiresOn()Ljava/util/Date;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/UserInfo;->mPasswordExpiresOn:Ljava/util/Date;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/microsoft/identity/common/java/util/DateExtensions;->〇080(Ljava/util/Date;)Ljava/util/Date;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/UserInfo;->mUniqueId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method setDisplayableId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/UserInfo;->mDisplayableId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setFamilyName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/UserInfo;->mFamilyName:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setGivenName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/UserInfo;->mGivenName:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setIdentityProvider(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/UserInfo;->mIdentityProvider:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setPasswordChangeUrl(Landroid/net/Uri;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/UserInfo;->mPasswordChangeUrl:Landroid/net/Uri;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setPasswordExpiresOn(Ljava/util/Date;)V
    .locals 3

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/microsoft/aad/adal/UserInfo;->mPasswordExpiresOn:Ljava/util/Date;

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    new-instance v0, Ljava/util/Date;

    .line 8
    .line 9
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    .line 10
    .line 11
    .line 12
    move-result-wide v1

    .line 13
    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/microsoft/aad/adal/UserInfo;->mPasswordExpiresOn:Ljava/util/Date;

    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setUserId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/UserInfo;->mUniqueId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

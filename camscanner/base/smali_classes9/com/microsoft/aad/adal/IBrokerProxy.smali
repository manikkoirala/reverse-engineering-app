.class interface abstract Lcom/microsoft/aad/adal/IBrokerProxy;
.super Ljava/lang/Object;
.source "IBrokerProxy.java"


# virtual methods
.method public abstract canSwitchToBroker(Ljava/lang/String;)Lcom/microsoft/aad/adal/BrokerProxy$SwitchToBroker;
.end method

.method public abstract canUseLocalCache(Ljava/lang/String;)Z
.end method

.method public abstract getAuthTokenInBackground(Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/BrokerEvent;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation
.end method

.method public abstract getBrokerAppVersion(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation
.end method

.method public abstract getBrokerUsers()[Lcom/microsoft/aad/adal/UserInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;,
            Landroid/accounts/AuthenticatorException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getCurrentActiveBrokerPackageName()Ljava/lang/String;
.end method

.method public abstract getCurrentUser()Ljava/lang/String;
.end method

.method public abstract getIntentForBrokerActivity(Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/BrokerEvent;)Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation
.end method

.method public abstract removeAccounts()V
.end method

.method public abstract saveAccount(Ljava/lang/String;)V
.end method

.method public abstract verifyBrokerForSilentRequest(Lcom/microsoft/aad/adal/AuthenticationRequest;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation
.end method

.method public abstract verifyUser(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation
.end method

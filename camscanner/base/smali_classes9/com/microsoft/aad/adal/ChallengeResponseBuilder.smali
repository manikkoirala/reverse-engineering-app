.class Lcom/microsoft/aad/adal/ChallengeResponseBuilder;
.super Ljava/lang/Object;
.source "ChallengeResponseBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;,
        Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;,
        Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ChallengeResponseBuilder"


# instance fields
.field private final mJWSBuilder:Lcom/microsoft/identity/common/java/util/JWSBuilder;


# direct methods
.method constructor <init>(Lcom/microsoft/identity/common/java/util/JWSBuilder;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/microsoft/aad/adal/ChallengeResponseBuilder;->mJWSBuilder:Lcom/microsoft/identity/common/java/util/JWSBuilder;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private getChallengeRequest(Ljava/lang/String;)Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    new-instance v0, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;

    .line 8
    .line 9
    invoke-direct {v0, p0}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;-><init>(Lcom/microsoft/aad/adal/ChallengeResponseBuilder;)V

    .line 10
    .line 11
    .line 12
    invoke-static {p1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->o〇0(Ljava/lang/String;)Ljava/util/HashMap;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    const/4 v1, 0x1

    .line 17
    invoke-direct {p0, p1, v1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder;->validateChallengeRequest(Ljava/util/Map;Z)V

    .line 18
    .line 19
    .line 20
    sget-object v1, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->Nonce:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    check-cast v2, Ljava/lang/String;

    .line 31
    .line 32
    invoke-static {v0, v2}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$502(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/lang/String;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    invoke-static {v0}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$500(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-static {v2}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-eqz v2, :cond_0

    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 50
    .line 51
    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    check-cast v1, Ljava/lang/String;

    .line 60
    .line 61
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$502(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/lang/String;)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    :cond_0
    sget-object v1, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->CertAuthorities:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 65
    .line 66
    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    check-cast v1, Ljava/lang/String;

    .line 75
    .line 76
    new-instance v2, Ljava/lang/StringBuilder;

    .line 77
    .line 78
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    .line 80
    .line 81
    const-string v3, "Authorities: "

    .line 82
    .line 83
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    const/4 v3, 0x0

    .line 94
    const-string v4, "ChallengeResponseBuilder:getChallengeRequest"

    .line 95
    .line 96
    const-string v5, "Get cert authorities. "

    .line 97
    .line 98
    invoke-static {v4, v5, v2, v3}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 99
    .line 100
    .line 101
    const-string v2, ";"

    .line 102
    .line 103
    invoke-static {v1, v2}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->O8(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    .line 104
    .line 105
    .line 106
    move-result-object v1

    .line 107
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$702(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/util/List;)Ljava/util/List;

    .line 108
    .line 109
    .line 110
    sget-object v1, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->Version:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 111
    .line 112
    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v1

    .line 116
    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    check-cast v1, Ljava/lang/String;

    .line 121
    .line 122
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$402(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/lang/String;)Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    sget-object v1, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->SubmitUrl:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 126
    .line 127
    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v1

    .line 131
    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    .line 133
    .line 134
    move-result-object v1

    .line 135
    check-cast v1, Ljava/lang/String;

    .line 136
    .line 137
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$002(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/lang/String;)Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    sget-object v1, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->Context:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 141
    .line 142
    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    move-result-object p1

    .line 150
    check-cast p1, Ljava/lang/String;

    .line 151
    .line 152
    invoke-static {v0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$302(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/lang/String;)Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    return-object v0

    .line 156
    :cond_1
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationServerProtocolException;

    .line 157
    .line 158
    const-string v0, "redirectUri"

    .line 159
    .line 160
    invoke-direct {p1, v0}, Lcom/microsoft/aad/adal/AuthenticationServerProtocolException;-><init>(Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    throw p1
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method private getChallengeRequestFromHeader(Ljava/lang/String;)Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_8

    .line 6
    .line 7
    const-string v0, "PKeyAuth"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->〇〇888(Ljava/lang/String;Ljava/lang/String;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_7

    .line 14
    .line 15
    new-instance v0, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;

    .line 16
    .line 17
    invoke-direct {v0, p0}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;-><init>(Lcom/microsoft/aad/adal/ChallengeResponseBuilder;)V

    .line 18
    .line 19
    .line 20
    const/16 v1, 0x8

    .line 21
    .line 22
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    const/16 v1, 0x2c

    .line 27
    .line 28
    invoke-static {p1, v1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->〇8o8o〇(Ljava/lang/String;C)Ljava/util/ArrayList;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    new-instance v2, Ljava/util/HashMap;

    .line 33
    .line 34
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    const/4 v4, 0x0

    .line 46
    if-eqz v3, :cond_2

    .line 47
    .line 48
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    check-cast v3, Ljava/lang/String;

    .line 53
    .line 54
    const/16 v5, 0x3d

    .line 55
    .line 56
    invoke-static {v3, v5}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->〇8o8o〇(Ljava/lang/String;C)Ljava/util/ArrayList;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 61
    .line 62
    .line 63
    move-result v5

    .line 64
    const/4 v6, 0x2

    .line 65
    const/4 v7, 0x1

    .line 66
    if-ne v5, v6, :cond_0

    .line 67
    .line 68
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 69
    .line 70
    .line 71
    move-result-object v5

    .line 72
    check-cast v5, Ljava/lang/String;

    .line 73
    .line 74
    invoke-static {v5}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 75
    .line 76
    .line 77
    move-result v5

    .line 78
    if-nez v5, :cond_0

    .line 79
    .line 80
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 81
    .line 82
    .line 83
    move-result-object v5

    .line 84
    check-cast v5, Ljava/lang/String;

    .line 85
    .line 86
    invoke-static {v5}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 87
    .line 88
    .line 89
    move-result v5

    .line 90
    if-nez v5, :cond_0

    .line 91
    .line 92
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 93
    .line 94
    .line 95
    move-result-object v4

    .line 96
    check-cast v4, Ljava/lang/String;

    .line 97
    .line 98
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 99
    .line 100
    .line 101
    move-result-object v3

    .line 102
    check-cast v3, Ljava/lang/String;

    .line 103
    .line 104
    invoke-static {v4}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->〇O8o08O(Ljava/lang/String;)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v4

    .line 108
    invoke-static {v3}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->〇O8o08O(Ljava/lang/String;)Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v3

    .line 112
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v4

    .line 116
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v3

    .line 120
    invoke-static {v3}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->OO0o〇〇〇〇0(Ljava/lang/String;)Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v3

    .line 124
    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    .line 126
    .line 127
    goto :goto_0

    .line 128
    :cond_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 129
    .line 130
    .line 131
    move-result v5

    .line 132
    if-ne v5, v7, :cond_1

    .line 133
    .line 134
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 135
    .line 136
    .line 137
    move-result-object v5

    .line 138
    check-cast v5, Ljava/lang/String;

    .line 139
    .line 140
    invoke-static {v5}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 141
    .line 142
    .line 143
    move-result v5

    .line 144
    if-nez v5, :cond_1

    .line 145
    .line 146
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    move-result-object v3

    .line 150
    check-cast v3, Ljava/lang/String;

    .line 151
    .line 152
    invoke-static {v3}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->〇O8o08O(Ljava/lang/String;)Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    move-result-object v3

    .line 156
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v3

    .line 160
    const-string v4, ""

    .line 161
    .line 162
    invoke-static {v4}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->〇O8o08O(Ljava/lang/String;)Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v4

    .line 166
    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    .line 168
    .line 169
    goto/16 :goto_0

    .line 170
    .line 171
    :cond_1
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 172
    .line 173
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->DEVICE_CERTIFICATE_REQUEST_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 174
    .line 175
    invoke-direct {v0, v1, p1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    throw v0

    .line 179
    :cond_2
    invoke-direct {p0, v2, v4}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder;->validateChallengeRequest(Ljava/util/Map;Z)V

    .line 180
    .line 181
    .line 182
    sget-object p1, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->Nonce:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 183
    .line 184
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object v1

    .line 188
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    .line 190
    .line 191
    move-result-object v1

    .line 192
    check-cast v1, Ljava/lang/String;

    .line 193
    .line 194
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$502(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/lang/String;)Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    invoke-static {v0}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$500(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;)Ljava/lang/String;

    .line 198
    .line 199
    .line 200
    move-result-object v1

    .line 201
    invoke-static {v1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 202
    .line 203
    .line 204
    move-result v1

    .line 205
    if-eqz v1, :cond_3

    .line 206
    .line 207
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 208
    .line 209
    .line 210
    move-result-object p1

    .line 211
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 212
    .line 213
    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 214
    .line 215
    .line 216
    move-result-object p1

    .line 217
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    .line 219
    .line 220
    move-result-object p1

    .line 221
    check-cast p1, Ljava/lang/String;

    .line 222
    .line 223
    invoke-static {v0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$502(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/lang/String;)Ljava/lang/String;

    .line 224
    .line 225
    .line 226
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder;->isWorkplaceJoined()Z

    .line 227
    .line 228
    .line 229
    move-result p1

    .line 230
    const-string v1, "ChallengeResponseBuilder:getChallengeRequestFromHeader"

    .line 231
    .line 232
    if-nez p1, :cond_4

    .line 233
    .line 234
    const-string p1, "Device is not workplace joined. "

    .line 235
    .line 236
    invoke-static {v1, p1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    .line 238
    .line 239
    goto :goto_1

    .line 240
    :cond_4
    sget-object p1, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->CertThumbprint:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 241
    .line 242
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 243
    .line 244
    .line 245
    move-result-object v3

    .line 246
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    .line 248
    .line 249
    move-result-object v3

    .line 250
    check-cast v3, Ljava/lang/String;

    .line 251
    .line 252
    invoke-static {v3}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 253
    .line 254
    .line 255
    move-result v3

    .line 256
    if-nez v3, :cond_5

    .line 257
    .line 258
    const-string v3, "CertThumbprint exists in the device auth challenge."

    .line 259
    .line 260
    invoke-static {v1, v3}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    .line 262
    .line 263
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 264
    .line 265
    .line 266
    move-result-object p1

    .line 267
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    .line 269
    .line 270
    move-result-object p1

    .line 271
    check-cast p1, Ljava/lang/String;

    .line 272
    .line 273
    invoke-static {v0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$602(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/lang/String;)Ljava/lang/String;

    .line 274
    .line 275
    .line 276
    goto :goto_1

    .line 277
    :cond_5
    sget-object p1, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->CertAuthorities:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 278
    .line 279
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 280
    .line 281
    .line 282
    move-result-object v3

    .line 283
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 284
    .line 285
    .line 286
    move-result v3

    .line 287
    if-eqz v3, :cond_6

    .line 288
    .line 289
    const-string v3, "CertAuthorities exists in the device auth challenge."

    .line 290
    .line 291
    invoke-static {v1, v3}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    .line 293
    .line 294
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 295
    .line 296
    .line 297
    move-result-object p1

    .line 298
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    .line 300
    .line 301
    move-result-object p1

    .line 302
    check-cast p1, Ljava/lang/String;

    .line 303
    .line 304
    const-string v1, ";"

    .line 305
    .line 306
    invoke-static {p1, v1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->O8(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    .line 307
    .line 308
    .line 309
    move-result-object p1

    .line 310
    invoke-static {v0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$702(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/util/List;)Ljava/util/List;

    .line 311
    .line 312
    .line 313
    :goto_1
    sget-object p1, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->Version:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 314
    .line 315
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 316
    .line 317
    .line 318
    move-result-object p1

    .line 319
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    .line 321
    .line 322
    move-result-object p1

    .line 323
    check-cast p1, Ljava/lang/String;

    .line 324
    .line 325
    invoke-static {v0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$402(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/lang/String;)Ljava/lang/String;

    .line 326
    .line 327
    .line 328
    sget-object p1, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->Context:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 329
    .line 330
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 331
    .line 332
    .line 333
    move-result-object p1

    .line 334
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    .line 336
    .line 337
    move-result-object p1

    .line 338
    check-cast p1, Ljava/lang/String;

    .line 339
    .line 340
    invoke-static {v0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$302(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/lang/String;)Ljava/lang/String;

    .line 341
    .line 342
    .line 343
    return-object v0

    .line 344
    :cond_6
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 345
    .line 346
    sget-object v0, Lcom/microsoft/aad/adal/ADALError;->DEVICE_CERTIFICATE_REQUEST_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 347
    .line 348
    const-string v1, "Both certThumbprint and certauthorities are not present"

    .line 349
    .line 350
    invoke-direct {p1, v0, v1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 351
    .line 352
    .line 353
    throw p1

    .line 354
    :cond_7
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 355
    .line 356
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->DEVICE_CERTIFICATE_REQUEST_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 357
    .line 358
    invoke-direct {v0, v1, p1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 359
    .line 360
    .line 361
    throw v0

    .line 362
    :cond_8
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationServerProtocolException;

    .line 363
    .line 364
    const-string v0, "headerValue"

    .line 365
    .line 366
    invoke-direct {p1, v0}, Lcom/microsoft/aad/adal/AuthenticationServerProtocolException;-><init>(Ljava/lang/String;)V

    .line 367
    .line 368
    .line 369
    throw p1
.end method

.method private getDeviceCertResponse(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;)Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder;->getNoDeviceCertResponse(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;)Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private getNoDeviceCertResponse(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;)Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;
    .locals 4

    .line 1
    new-instance v0, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;-><init>(Lcom/microsoft/aad/adal/ChallengeResponseBuilder;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$000(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;->access$102(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;Ljava/lang/String;)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    const/4 v1, 0x3

    .line 14
    new-array v1, v1, [Ljava/lang/Object;

    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    const-string v3, "PKeyAuth"

    .line 18
    .line 19
    aput-object v3, v1, v2

    .line 20
    .line 21
    const/4 v2, 0x1

    .line 22
    invoke-static {p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$300(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    aput-object v3, v1, v2

    .line 27
    .line 28
    const/4 v2, 0x2

    .line 29
    invoke-static {p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$400(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    aput-object p1, v1, v2

    .line 34
    .line 35
    const-string p1, "%s Context=\"%s\",Version=\"%s\""

    .line 36
    .line 37
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-static {v0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;->access$202(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;Ljava/lang/String;)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private isWorkplaceJoined()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private validateChallengeRequest(Ljava/util/Map;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->Nonce:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_1

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 31
    .line 32
    sget-object p2, Lcom/microsoft/aad/adal/ADALError;->DEVICE_CERTIFICATE_REQUEST_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 33
    .line 34
    const-string v0, "Nonce"

    .line 35
    .line 36
    invoke-direct {p1, p2, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    throw p1

    .line 40
    :cond_1
    :goto_0
    sget-object v0, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->Version:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 41
    .line 42
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    if-eqz v0, :cond_7

    .line 51
    .line 52
    if-eqz p2, :cond_3

    .line 53
    .line 54
    sget-object v0, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->SubmitUrl:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 55
    .line 56
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    if-eqz v0, :cond_2

    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_2
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 68
    .line 69
    sget-object p2, Lcom/microsoft/aad/adal/ADALError;->DEVICE_CERTIFICATE_REQUEST_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 70
    .line 71
    const-string v0, "SubmitUrl"

    .line 72
    .line 73
    invoke-direct {p1, p2, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    throw p1

    .line 77
    :cond_3
    :goto_1
    sget-object v0, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->Context:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 78
    .line 79
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    move-result v0

    .line 87
    if-eqz v0, :cond_6

    .line 88
    .line 89
    if-eqz p2, :cond_5

    .line 90
    .line 91
    sget-object p2, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;->CertAuthorities:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$RequestField;

    .line 92
    .line 93
    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object p2

    .line 97
    invoke-interface {p1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 98
    .line 99
    .line 100
    move-result p1

    .line 101
    if-eqz p1, :cond_4

    .line 102
    .line 103
    goto :goto_2

    .line 104
    :cond_4
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 105
    .line 106
    sget-object p2, Lcom/microsoft/aad/adal/ADALError;->DEVICE_CERTIFICATE_REQUEST_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 107
    .line 108
    const-string v0, "CertAuthorities"

    .line 109
    .line 110
    invoke-direct {p1, p2, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    throw p1

    .line 114
    :cond_5
    :goto_2
    return-void

    .line 115
    :cond_6
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 116
    .line 117
    sget-object p2, Lcom/microsoft/aad/adal/ADALError;->DEVICE_CERTIFICATE_REQUEST_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 118
    .line 119
    const-string v0, "Context"

    .line 120
    .line 121
    invoke-direct {p1, p2, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    throw p1

    .line 125
    :cond_7
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 126
    .line 127
    sget-object p2, Lcom/microsoft/aad/adal/ADALError;->DEVICE_CERTIFICATE_REQUEST_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 128
    .line 129
    const-string v0, "Version"

    .line 130
    .line 131
    invoke-direct {p1, p2, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    throw p1
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method


# virtual methods
.method public getChallengeResponseFromHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder;->getChallengeRequestFromHeader(Ljava/lang/String;)Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-static {p1, p2}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;->access$002(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder;->getDeviceCertResponse(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;)Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public getChallengeResponseFromUri(Ljava/lang/String;)Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder;->getChallengeRequest(Ljava/lang/String;)Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder;->getDeviceCertResponse(Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeRequest;)Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

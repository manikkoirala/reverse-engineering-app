.class public interface abstract Lcom/microsoft/aad/adal/ITokenCacheStore;
.super Ljava/lang/Object;
.source "ITokenCacheStore.java"

# interfaces
.implements Ljava/io/Serializable;


# virtual methods
.method public abstract contains(Ljava/lang/String;)Z
.end method

.method public abstract getAll()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/microsoft/aad/adal/TokenCacheItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getItem(Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;
.end method

.method public abstract removeAll()V
.end method

.method public abstract removeItem(Ljava/lang/String;)V
.end method

.method public abstract setItem(Ljava/lang/String;Lcom/microsoft/aad/adal/TokenCacheItem;)V
.end method

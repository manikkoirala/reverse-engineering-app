.class public final Lcom/microsoft/aad/adal/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/aad/adal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f130493

.field public static final abc_action_bar_up_description:I = 0x7f130494

.field public static final abc_action_menu_overflow_description:I = 0x7f130495

.field public static final abc_action_mode_done:I = 0x7f130496

.field public static final abc_activity_chooser_view_see_all:I = 0x7f130497

.field public static final abc_activitychooserview_choose_application:I = 0x7f130498

.field public static final abc_capital_off:I = 0x7f130499

.field public static final abc_capital_on:I = 0x7f13049a

.field public static final abc_menu_alt_shortcut_label:I = 0x7f13049b

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f13049c

.field public static final abc_menu_delete_shortcut_label:I = 0x7f13049d

.field public static final abc_menu_enter_shortcut_label:I = 0x7f13049e

.field public static final abc_menu_function_shortcut_label:I = 0x7f13049f

.field public static final abc_menu_meta_shortcut_label:I = 0x7f1304a0

.field public static final abc_menu_shift_shortcut_label:I = 0x7f1304a1

.field public static final abc_menu_space_shortcut_label:I = 0x7f1304a2

.field public static final abc_menu_sym_shortcut_label:I = 0x7f1304a3

.field public static final abc_prepend_shortcut_label:I = 0x7f1304a4

.field public static final abc_search_hint:I = 0x7f1304a5

.field public static final abc_searchview_description_clear:I = 0x7f1304a6

.field public static final abc_searchview_description_query:I = 0x7f1304a7

.field public static final abc_searchview_description_search:I = 0x7f1304a8

.field public static final abc_searchview_description_submit:I = 0x7f1304a9

.field public static final abc_searchview_description_voice:I = 0x7f1304aa

.field public static final abc_shareactionprovider_share_with:I = 0x7f1304ab

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f1304ac

.field public static final abc_toolbar_collapse_description:I = 0x7f1304ad

.field public static final app_loading:I = 0x7f1304ec

.field public static final broker_processing:I = 0x7f13052a

.field public static final http_auth_dialog_cancel:I = 0x7f131d3e

.field public static final http_auth_dialog_login:I = 0x7f131d3f

.field public static final http_auth_dialog_password:I = 0x7f131d40

.field public static final http_auth_dialog_title:I = 0x7f131d41

.field public static final http_auth_dialog_username:I = 0x7f131d42

.field public static final search_menu_title:I = 0x7f131e6d

.field public static final smartcard_cert_dialog_negative_button:I = 0x7f131ea3

.field public static final smartcard_cert_dialog_positive_button:I = 0x7f131ea4

.field public static final smartcard_cert_dialog_title:I = 0x7f131ea5

.field public static final smartcard_early_unplug_dialog_message:I = 0x7f131ea6

.field public static final smartcard_early_unplug_dialog_title:I = 0x7f131ea7

.field public static final smartcard_error_dialog_positive_button:I = 0x7f131ea8

.field public static final smartcard_general_error_dialog_message:I = 0x7f131ea9

.field public static final smartcard_general_error_dialog_title:I = 0x7f131eaa

.field public static final smartcard_max_attempt_dialog_message:I = 0x7f131eab

.field public static final smartcard_max_attempt_dialog_title:I = 0x7f131eac

.field public static final smartcard_nfc_loading_dialog_message:I = 0x7f131ead

.field public static final smartcard_nfc_loading_dialog_title:I = 0x7f131eae

.field public static final smartcard_nfc_prompt_dialog_message:I = 0x7f131eaf

.field public static final smartcard_nfc_prompt_dialog_negative_button:I = 0x7f131eb0

.field public static final smartcard_nfc_prompt_dialog_title:I = 0x7f131eb1

.field public static final smartcard_nfc_reminder_dialog_message:I = 0x7f131eb2

.field public static final smartcard_nfc_reminder_dialog_positive_button:I = 0x7f131eb3

.field public static final smartcard_nfc_reminder_dialog_title:I = 0x7f131eb4

.field public static final smartcard_no_cert_dialog_message:I = 0x7f131eb5

.field public static final smartcard_no_cert_dialog_title:I = 0x7f131eb6

.field public static final smartcard_pin_dialog_error_message:I = 0x7f131eb7

.field public static final smartcard_pin_dialog_message:I = 0x7f131eb8

.field public static final smartcard_pin_dialog_negative_button:I = 0x7f131eb9

.field public static final smartcard_pin_dialog_positive_button:I = 0x7f131eba

.field public static final smartcard_pin_dialog_title:I = 0x7f131ebb

.field public static final smartcard_pin_layout_edittext_hint:I = 0x7f131ebc

.field public static final smartcard_prompt_dialog_message:I = 0x7f131ebd

.field public static final smartcard_prompt_dialog_negative_button:I = 0x7f131ebe

.field public static final smartcard_prompt_dialog_title:I = 0x7f131ebf

.field public static final status_bar_notification_info_overflow:I = 0x7f131eca

.field public static final user_choice_dialog_negative_button:I = 0x7f131f88

.field public static final user_choice_dialog_on_device_name:I = 0x7f131f89

.field public static final user_choice_dialog_positive_button:I = 0x7f131f8a

.field public static final user_choice_dialog_smartcard_name:I = 0x7f131f8b

.field public static final user_choice_dialog_title:I = 0x7f131f8c

.field public static final yubikit_otp_activity_title:I = 0x7f131fa7

.field public static final yubikit_otp_touch:I = 0x7f131fa8

.field public static final yubikit_prompt_activity_title:I = 0x7f131fa9

.field public static final yubikit_prompt_enable_nfc:I = 0x7f131faa

.field public static final yubikit_prompt_image_desc:I = 0x7f131fab

.field public static final yubikit_prompt_plug_in:I = 0x7f131fac

.field public static final yubikit_prompt_plug_in_or_tap:I = 0x7f131fad

.field public static final yubikit_prompt_remove:I = 0x7f131fae

.field public static final yubikit_prompt_uv:I = 0x7f131faf

.field public static final yubikit_prompt_wait:I = 0x7f131fb0


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

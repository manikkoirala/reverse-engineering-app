.class public Lcom/microsoft/aad/adal/FileTokenCacheStore;
.super Ljava/lang/Object;
.source "FileTokenCacheStore.java"

# interfaces
.implements Lcom/microsoft/aad/adal/ITokenCacheStore;


# static fields
.field private static final TAG:Ljava/lang/String; = "FileTokenCacheStore"

.field private static final serialVersionUID:J = -0x7286073f6b86917eL


# instance fields
.field private final mCacheLock:Ljava/lang/Object;

.field private final mFile:Ljava/io/File;

.field private final mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .line 1
    const-string v0, ":FileTokenCacheStore"

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/lang/Object;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object v1, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mCacheLock:Ljava/lang/Object;

    .line 12
    .line 13
    if-eqz p1, :cond_4

    .line 14
    .line 15
    invoke-static {p2}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-nez v1, :cond_3

    .line 20
    .line 21
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    const/4 v2, 0x0

    .line 26
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    if-eqz p1, :cond_2

    .line 31
    .line 32
    :try_start_0
    new-instance v1, Ljava/io/File;

    .line 33
    .line 34
    invoke-direct {v1, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iput-object v1, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mFile:Ljava/io/File;

    .line 38
    .line 39
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    new-instance p1, Ljava/lang/StringBuilder;

    .line 46
    .line 47
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 48
    .line 49
    .line 50
    sget-object p2, Lcom/microsoft/aad/adal/FileTokenCacheStore;->TAG:Ljava/lang/String;

    .line 51
    .line 52
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    const-string v2, "There is previous cache file to load cache. "

    .line 63
    .line 64
    invoke-static {p1, v2}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    new-instance p1, Ljava/io/FileInputStream;

    .line 68
    .line 69
    invoke-direct {p1, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 70
    .line 71
    .line 72
    new-instance v1, Ljava/io/ObjectInputStream;

    .line 73
    .line 74
    invoke-direct {v1, p1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    invoke-virtual {p1}, Ljava/io/FileInputStream;->close()V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    .line 85
    .line 86
    .line 87
    instance-of p1, v2, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 88
    .line 89
    if-eqz p1, :cond_0

    .line 90
    .line 91
    check-cast v2, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 92
    .line 93
    iput-object v2, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    .line 97
    .line 98
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .line 100
    .line 101
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    const-string p2, "Existing cache format is wrong. "

    .line 112
    .line 113
    const-string v1, ""

    .line 114
    .line 115
    sget-object v2, Lcom/microsoft/aad/adal/ADALError;->DEVICE_FILE_CACHE_FORMAT_IS_WRONG:Lcom/microsoft/aad/adal/ADALError;

    .line 116
    .line 117
    invoke-static {p1, p2, v1, v2}, Lcom/microsoft/aad/adal/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 118
    .line 119
    .line 120
    new-instance p1, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 121
    .line 122
    invoke-direct {p1}, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;-><init>()V

    .line 123
    .line 124
    .line 125
    iput-object p1, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 126
    .line 127
    goto :goto_0

    .line 128
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 129
    .line 130
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .line 132
    .line 133
    sget-object p2, Lcom/microsoft/aad/adal/FileTokenCacheStore;->TAG:Ljava/lang/String;

    .line 134
    .line 135
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object p1

    .line 145
    const-string p2, "There is not any previous cache file to load cache. "

    .line 146
    .line 147
    invoke-static {p1, p2}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    .line 149
    .line 150
    new-instance p1, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 151
    .line 152
    invoke-direct {p1}, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;-><init>()V

    .line 153
    .line 154
    .line 155
    iput-object p1, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    .line 157
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception p1

    .line 159
    goto :goto_1

    .line 160
    :catch_1
    move-exception p1

    .line 161
    :goto_1
    new-instance p2, Ljava/lang/StringBuilder;

    .line 162
    .line 163
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    .line 165
    .line 166
    sget-object v1, Lcom/microsoft/aad/adal/FileTokenCacheStore;->TAG:Ljava/lang/String;

    .line 167
    .line 168
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    .line 170
    .line 171
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 175
    .line 176
    .line 177
    move-result-object p2

    .line 178
    invoke-static {p1}, Lcom/microsoft/aad/adal/ExceptionExtensions;->getExceptionMessage(Ljava/lang/Exception;)Ljava/lang/String;

    .line 179
    .line 180
    .line 181
    move-result-object v0

    .line 182
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->DEVICE_FILE_CACHE_IS_NOT_LOADED_FROM_FILE:Lcom/microsoft/aad/adal/ADALError;

    .line 183
    .line 184
    const-string v2, "Exception during cache load. "

    .line 185
    .line 186
    invoke-static {p2, v2, v0, v1}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 187
    .line 188
    .line 189
    new-instance p2, Ljava/lang/IllegalStateException;

    .line 190
    .line 191
    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    .line 192
    .line 193
    .line 194
    throw p2

    .line 195
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 196
    .line 197
    const-string p2, "It could not access the Authorization cache directory"

    .line 198
    .line 199
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    throw p1

    .line 203
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 204
    .line 205
    const-string p2, "fileName"

    .line 206
    .line 207
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 208
    .line 209
    .line 210
    throw p1

    .line 211
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 212
    .line 213
    const-string p2, "context"

    .line 214
    .line 215
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 216
    .line 217
    .line 218
    throw p1
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method private writeToFile()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mCacheLock:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mFile:Ljava/io/File;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    iget-object v1, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    .line 13
    .line 14
    iget-object v2, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mFile:Ljava/io/File;

    .line 15
    .line 16
    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 17
    .line 18
    .line 19
    new-instance v2, Ljava/io/ObjectOutputStream;

    .line 20
    .line 21
    invoke-direct {v2, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 22
    .line 23
    .line 24
    iget-object v3, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 25
    .line 26
    invoke-virtual {v2, v3}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->flush()V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :catch_0
    move-exception v1

    .line 40
    :try_start_2
    sget-object v2, Lcom/microsoft/aad/adal/FileTokenCacheStore;->TAG:Ljava/lang/String;

    .line 41
    .line 42
    const-string v3, "Exception during cache flush"

    .line 43
    .line 44
    invoke-static {v1}, Lcom/microsoft/aad/adal/ExceptionExtensions;->getExceptionMessage(Ljava/lang/Exception;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    sget-object v4, Lcom/microsoft/aad/adal/ADALError;->DEVICE_FILE_CACHE_IS_NOT_WRITING_TO_FILE:Lcom/microsoft/aad/adal/ADALError;

    .line 49
    .line 50
    invoke-static {v2, v3, v1, v4}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 51
    .line 52
    .line 53
    :cond_0
    :goto_0
    monitor-exit v0

    .line 54
    return-void

    .line 55
    :catchall_0
    move-exception v1

    .line 56
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 57
    throw v1
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method


# virtual methods
.method public contains(Ljava/lang/String;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;->contains(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public getAll()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/microsoft/aad/adal/TokenCacheItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;->getAll()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getItem(Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;->getItem(Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public removeAll()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;->removeAll()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/microsoft/aad/adal/FileTokenCacheStore;->writeToFile()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public removeItem(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;->removeItem(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/microsoft/aad/adal/FileTokenCacheStore;->writeToFile()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setItem(Ljava/lang/String;Lcom/microsoft/aad/adal/TokenCacheItem;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/FileTokenCacheStore;->mInMemoryCache:Lcom/microsoft/aad/adal/MemoryTokenCacheStore;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/microsoft/aad/adal/MemoryTokenCacheStore;->setItem(Ljava/lang/String;Lcom/microsoft/aad/adal/TokenCacheItem;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/microsoft/aad/adal/FileTokenCacheStore;->writeToFile()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.class public final Lcom/microsoft/aad/adal/TokenCacheItemSerializationAdapater;
.super Ljava/lang/Object;
.source "TokenCacheItemSerializationAdapater.java"

# interfaces
.implements Lcom/google/gson/JsonDeserializer;
.implements Lcom/google/gson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonDeserializer<",
        "Lcom/microsoft/aad/adal/TokenCacheItem;",
        ">;",
        "Lcom/google/gson/JsonSerializer<",
        "Lcom/microsoft/aad/adal/TokenCacheItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TokenCacheItemSerializationAdapater"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private throwIfParameterMissing(Lcom/google/gson/JsonObject;Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p1, p2}, Lcom/google/gson/JsonObject;->〇O00(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance p1, Lcom/google/gson/JsonParseException;

    .line 9
    .line 10
    new-instance v0, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    sget-object v1, Lcom/microsoft/aad/adal/TokenCacheItemSerializationAdapater;->TAG:Ljava/lang/String;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v1, "Attribute "

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string p2, " is missing for deserialization."

    .line 29
    .line 30
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    invoke-direct {p1, p2}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    throw p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/microsoft/aad/adal/TokenCacheItem;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .line 2
    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->〇o〇()Lcom/google/gson/JsonObject;

    move-result-object p1

    const-string p2, "authority"

    .line 3
    invoke-direct {p0, p1, p2}, Lcom/microsoft/aad/adal/TokenCacheItemSerializationAdapater;->throwIfParameterMissing(Lcom/google/gson/JsonObject;Ljava/lang/String;)V

    const-string p3, "id_token"

    .line 4
    invoke-direct {p0, p1, p3}, Lcom/microsoft/aad/adal/TokenCacheItemSerializationAdapater;->throwIfParameterMissing(Lcom/google/gson/JsonObject;Ljava/lang/String;)V

    const-string v0, "foci"

    .line 5
    invoke-direct {p0, p1, v0}, Lcom/microsoft/aad/adal/TokenCacheItemSerializationAdapater;->throwIfParameterMissing(Lcom/google/gson/JsonObject;Ljava/lang/String;)V

    const-string v1, "refresh_token"

    .line 6
    invoke-direct {p0, p1, v1}, Lcom/microsoft/aad/adal/TokenCacheItemSerializationAdapater;->throwIfParameterMissing(Lcom/google/gson/JsonObject;Ljava/lang/String;)V

    .line 7
    invoke-virtual {p1, p3}, Lcom/google/gson/JsonObject;->Oooo8o0〇(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object p3

    invoke-virtual {p3}, Lcom/google/gson/JsonElement;->Oo08()Ljava/lang/String;

    move-result-object p3

    .line 8
    new-instance v2, Lcom/microsoft/aad/adal/TokenCacheItem;

    invoke-direct {v2}, Lcom/microsoft/aad/adal/TokenCacheItem;-><init>()V

    .line 9
    :try_start_0
    new-instance v3, Lcom/microsoft/aad/adal/IdToken;

    invoke-direct {v3, p3}, Lcom/microsoft/aad/adal/IdToken;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/aad/adal/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    new-instance v4, Lcom/microsoft/aad/adal/UserInfo;

    invoke-direct {v4, v3}, Lcom/microsoft/aad/adal/UserInfo;-><init>(Lcom/microsoft/aad/adal/IdToken;)V

    .line 11
    invoke-virtual {v2, v4}, Lcom/microsoft/aad/adal/TokenCacheItem;->setUserInfo(Lcom/microsoft/aad/adal/UserInfo;)V

    .line 12
    invoke-virtual {v3}, Lcom/microsoft/aad/adal/IdToken;->getTenantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/aad/adal/TokenCacheItem;->setTenantId(Ljava/lang/String;)V

    .line 13
    invoke-virtual {p1, p2}, Lcom/google/gson/JsonObject;->Oooo8o0〇(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/gson/JsonElement;->Oo08()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Lcom/microsoft/aad/adal/TokenCacheItem;->setAuthority(Ljava/lang/String;)V

    const/4 p2, 0x1

    .line 14
    invoke-virtual {v2, p2}, Lcom/microsoft/aad/adal/TokenCacheItem;->setIsMultiResourceRefreshToken(Z)V

    .line 15
    invoke-virtual {v2, p3}, Lcom/microsoft/aad/adal/TokenCacheItem;->setRawIdToken(Ljava/lang/String;)V

    .line 16
    invoke-virtual {p1, v0}, Lcom/google/gson/JsonObject;->Oooo8o0〇(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/gson/JsonElement;->Oo08()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Lcom/microsoft/aad/adal/TokenCacheItem;->setFamilyClientId(Ljava/lang/String;)V

    .line 17
    invoke-virtual {p1, v1}, Lcom/google/gson/JsonObject;->Oooo8o0〇(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->Oo08()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/microsoft/aad/adal/TokenCacheItem;->setRefreshToken(Ljava/lang/String;)V

    return-object v2

    :catch_0
    move-exception p1

    .line 18
    new-instance p2, Lcom/google/gson/JsonParseException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/microsoft/aad/adal/TokenCacheItemSerializationAdapater;->TAG:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ": Could not deserialize into a tokenCacheItem object"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Lcom/google/gson/JsonParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public bridge synthetic deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/aad/adal/TokenCacheItemSerializationAdapater;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/microsoft/aad/adal/TokenCacheItem;

    move-result-object p1

    return-object p1
.end method

.method public serialize(Lcom/microsoft/aad/adal/TokenCacheItem;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 1

    .line 2
    new-instance p2, Lcom/google/gson/JsonObject;

    invoke-direct {p2}, Lcom/google/gson/JsonObject;-><init>()V

    .line 3
    new-instance p3, Lcom/google/gson/JsonPrimitive;

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/TokenCacheItem;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p3, v0}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    const-string v0, "authority"

    invoke-virtual {p2, v0, p3}, Lcom/google/gson/JsonObject;->OO0o〇〇〇〇0(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 4
    new-instance p3, Lcom/google/gson/JsonPrimitive;

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/TokenCacheItem;->getRefreshToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p3, v0}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    const-string v0, "refresh_token"

    invoke-virtual {p2, v0, p3}, Lcom/google/gson/JsonObject;->OO0o〇〇〇〇0(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 5
    new-instance p3, Lcom/google/gson/JsonPrimitive;

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/TokenCacheItem;->getRawIdToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p3, v0}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    const-string v0, "id_token"

    invoke-virtual {p2, v0, p3}, Lcom/google/gson/JsonObject;->OO0o〇〇〇〇0(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 6
    new-instance p3, Lcom/google/gson/JsonPrimitive;

    invoke-virtual {p1}, Lcom/microsoft/aad/adal/TokenCacheItem;->getFamilyClientId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    const-string p1, "foci"

    invoke-virtual {p2, p1, p3}, Lcom/google/gson/JsonObject;->OO0o〇〇〇〇0(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    return-object p2
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 0

    .line 1
    check-cast p1, Lcom/microsoft/aad/adal/TokenCacheItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/aad/adal/TokenCacheItemSerializationAdapater;->serialize(Lcom/microsoft/aad/adal/TokenCacheItem;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;

    move-result-object p1

    return-object p1
.end method

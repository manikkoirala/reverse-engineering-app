.class Lcom/microsoft/aad/adal/BasicWebViewClient$3$1;
.super Ljava/lang/Object;
.source "BasicWebViewClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/aad/adal/BasicWebViewClient$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/aad/adal/BasicWebViewClient$3;

.field final synthetic val$challengeResponse:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;

.field final synthetic val$headers:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/microsoft/aad/adal/BasicWebViewClient$3;Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;Ljava/util/Map;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/BasicWebViewClient$3$1;->this$1:Lcom/microsoft/aad/adal/BasicWebViewClient$3;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/microsoft/aad/adal/BasicWebViewClient$3$1;->val$challengeResponse:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;

    .line 4
    .line 5
    iput-object p3, p0, Lcom/microsoft/aad/adal/BasicWebViewClient$3$1;->val$headers:Ljava/util/Map;

    .line 6
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/BasicWebViewClient$3$1;->val$challengeResponse:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;->getSubmitUrl()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "Respond to pkeyAuth challenge."

    .line 8
    .line 9
    const-string v2, "BasicWebViewClient:shouldOverrideUrlLoading"

    .line 10
    .line 11
    invoke-static {v2, v1}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇O00(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    new-instance v1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v3, "Challenge submit url:"

    .line 20
    .line 21
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    iget-object v3, p0, Lcom/microsoft/aad/adal/BasicWebViewClient$3$1;->val$challengeResponse:Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;

    .line 25
    .line 26
    invoke-virtual {v3}, Lcom/microsoft/aad/adal/ChallengeResponseBuilder$ChallengeResponse;->getSubmitUrl()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-static {v2, v1}, Lcom/microsoft/identity/common/internal/logging/Logger;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    iget-object v1, p0, Lcom/microsoft/aad/adal/BasicWebViewClient$3$1;->this$1:Lcom/microsoft/aad/adal/BasicWebViewClient$3;

    .line 41
    .line 42
    iget-object v1, v1, Lcom/microsoft/aad/adal/BasicWebViewClient$3;->val$view:Landroid/webkit/WebView;

    .line 43
    .line 44
    iget-object v2, p0, Lcom/microsoft/aad/adal/BasicWebViewClient$3$1;->val$headers:Ljava/util/Map;

    .line 45
    .line 46
    invoke-virtual {v1, v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

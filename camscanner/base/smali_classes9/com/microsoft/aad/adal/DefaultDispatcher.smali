.class Lcom/microsoft/aad/adal/DefaultDispatcher;
.super Ljava/lang/Object;
.source "DefaultDispatcher.java"


# instance fields
.field private final mDispatcher:Lcom/microsoft/aad/adal/IDispatcher;

.field private final mObjectsToBeDispatched:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/microsoft/aad/adal/IEvents;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/aad/adal/DefaultDispatcher;->mObjectsToBeDispatched:Ljava/util/Map;

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/microsoft/aad/adal/DefaultDispatcher;->mDispatcher:Lcom/microsoft/aad/adal/IDispatcher;

    return-void
.end method

.method constructor <init>(Lcom/microsoft/aad/adal/IDispatcher;)V
    .locals 1

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/aad/adal/DefaultDispatcher;->mObjectsToBeDispatched:Ljava/util/Map;

    .line 6
    iput-object p1, p0, Lcom/microsoft/aad/adal/DefaultDispatcher;->mDispatcher:Lcom/microsoft/aad/adal/IDispatcher;

    return-void
.end method


# virtual methods
.method declared-synchronized flush(Ljava/lang/String;)V
    .locals 0

    .line 1
    monitor-enter p0

    .line 2
    monitor-exit p0

    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method getDispatcher()Lcom/microsoft/aad/adal/IDispatcher;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DefaultDispatcher;->mDispatcher:Lcom/microsoft/aad/adal/IDispatcher;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getObjectsToBeDispatched()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/microsoft/aad/adal/IEvents;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/DefaultDispatcher;->mObjectsToBeDispatched:Ljava/util/Map;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method receive(Ljava/lang/String;Lcom/microsoft/aad/adal/IEvents;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/microsoft/aad/adal/DefaultDispatcher;->mDispatcher:Lcom/microsoft/aad/adal/IDispatcher;

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance p1, Ljava/util/HashMap;

    .line 7
    .line 8
    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-interface {p2}, Lcom/microsoft/aad/adal/IEvents;->getEvents()Ljava/util/List;

    .line 12
    .line 13
    .line 14
    move-result-object p2

    .line 15
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    check-cast v0, Ljava/util/Map$Entry;

    .line 30
    .line 31
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Ljava/lang/String;

    .line 36
    .line 37
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    check-cast v0, Ljava/lang/String;

    .line 42
    .line 43
    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    iget-object p2, p0, Lcom/microsoft/aad/adal/DefaultDispatcher;->mDispatcher:Lcom/microsoft/aad/adal/IDispatcher;

    .line 48
    .line 49
    invoke-interface {p2, p1}, Lcom/microsoft/aad/adal/IDispatcher;->dispatchEvent(Ljava/util/Map;)V

    .line 50
    .line 51
    .line 52
    return-void
.end method

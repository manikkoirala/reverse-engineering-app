.class Lcom/microsoft/aad/adal/AuthenticationRequest;
.super Ljava/lang/Object;
.source "AuthenticationRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;
    }
.end annotation


# static fields
.field private static final DELIM_NOT_FOUND:I = -0x1

.field private static final UPN_DOMAIN_SUFFIX_DELIM:Ljava/lang/String; = "@"

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mAppName:Ljava/lang/String;

.field private mAppVersion:Ljava/lang/String;

.field private mAssertionType:Ljava/lang/String;

.field private mAuthority:Ljava/lang/String;

.field private mBrokerAccountName:Ljava/lang/String;

.field private mClaimsChallenge:Ljava/lang/String;

.field private mClientCapabilities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mClientId:Ljava/lang/String;

.field private mCorrelationId:Ljava/util/UUID;

.field private mExtraQueryParamsAuthentication:Ljava/lang/String;

.field private mForceRefresh:Z

.field private mIdentifierType:Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;

.field private transient mInstanceDiscoveryMetadata:Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;

.field private mIsExtendedLifetimeEnabled:Z

.field private mLoginHint:Ljava/lang/String;

.field private mPrompt:Lcom/microsoft/aad/adal/PromptBehavior;

.field private mRedirectUri:Ljava/lang/String;

.field private mRequestId:I

.field private mResource:Ljava/lang/String;

.field private mSamlAssertion:Ljava/lang/String;

.field private mScope:Ljava/lang/String;

.field private mSilent:Z

.field private mSkipCache:Z

.field private mTelemetryRequestId:Ljava/lang/String;

.field private mUserId:Ljava/lang/String;

.field private mVersion:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRequestId:I

    const/4 v1, 0x0

    .line 3
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 4
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRedirectUri:Ljava/lang/String;

    .line 5
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mResource:Ljava/lang/String;

    .line 6
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mScope:Ljava/lang/String;

    .line 7
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 8
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 9
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 10
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 11
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSilent:Z

    .line 12
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mVersion:Ljava/lang/String;

    .line 13
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIsExtendedLifetimeEnabled:Z

    .line 14
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mForceRefresh:Z

    .line 15
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSkipCache:Z

    .line 16
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSamlAssertion:Ljava/lang/String;

    .line 17
    sget-object v0, Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;->NoUser:Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;

    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIdentifierType:Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/PromptBehavior;Ljava/lang/String;Ljava/util/UUID;ZLjava/lang/String;)V
    .locals 2

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 19
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRequestId:I

    const/4 v1, 0x0

    .line 20
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mScope:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 22
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSilent:Z

    .line 23
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mVersion:Ljava/lang/String;

    .line 24
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIsExtendedLifetimeEnabled:Z

    .line 25
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mForceRefresh:Z

    .line 26
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSkipCache:Z

    .line 27
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSamlAssertion:Ljava/lang/String;

    .line 28
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mResource:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRedirectUri:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 33
    iput-object p5, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 34
    iput-object p6, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mPrompt:Lcom/microsoft/aad/adal/PromptBehavior;

    .line 35
    iput-object p7, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mExtraQueryParamsAuthentication:Ljava/lang/String;

    .line 36
    iput-object p8, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mCorrelationId:Ljava/util/UUID;

    .line 37
    sget-object p1, Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;->NoUser:Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;

    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIdentifierType:Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;

    .line 38
    iput-boolean p9, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIsExtendedLifetimeEnabled:Z

    .line 39
    iput-object p10, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClaimsChallenge:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;ZZLjava/lang/String;)V
    .locals 11

    move-object v9, p0

    move-object v10, p2

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move/from16 v6, p8

    move/from16 v7, p9

    move-object/from16 v8, p10

    .line 124
    invoke-direct/range {v0 .. v8}, Lcom/microsoft/aad/adal/AuthenticationRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;ZZLjava/lang/String;)V

    move-object v0, p1

    .line 125
    iput-object v0, v9, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSamlAssertion:Ljava/lang/String;

    const-string/jumbo v0, "urn:ietf:params:oauth:grant-type:saml1_1-bearer"

    if-eq v10, v0, :cond_0

    const-string/jumbo v0, "urn:ietf:params:oauth:grant-type:saml2-bearer"

    if-eq v10, v0, :cond_0

    const/4 v0, 0x0

    .line 126
    iput-object v0, v9, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAssertionType:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object/from16 v0, p6

    .line 127
    iput-object v0, v9, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 128
    iput-object v10, v9, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAssertionType:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;Z)V
    .locals 2

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 41
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRequestId:I

    const/4 v1, 0x0

    .line 42
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mScope:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 44
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSilent:Z

    .line 45
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mVersion:Ljava/lang/String;

    .line 46
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mForceRefresh:Z

    .line 47
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSkipCache:Z

    .line 48
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSamlAssertion:Ljava/lang/String;

    .line 49
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mResource:Ljava/lang/String;

    .line 51
    iput-object p3, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRedirectUri:Ljava/lang/String;

    .line 53
    iput-object p5, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 54
    iput-object p5, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 55
    iput-object p6, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mCorrelationId:Ljava/util/UUID;

    .line 56
    iput-boolean p7, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIsExtendedLifetimeEnabled:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 58
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRequestId:I

    const/4 v1, 0x0

    .line 59
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mScope:Ljava/lang/String;

    .line 60
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 61
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSilent:Z

    .line 62
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mVersion:Ljava/lang/String;

    .line 63
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mForceRefresh:Z

    .line 64
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSkipCache:Z

    .line 65
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSamlAssertion:Ljava/lang/String;

    .line 66
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 67
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mResource:Ljava/lang/String;

    .line 68
    iput-object p3, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 69
    iput-object p4, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRedirectUri:Ljava/lang/String;

    .line 70
    iput-object p5, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 71
    iput-object p5, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 72
    iput-boolean p6, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIsExtendedLifetimeEnabled:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;Z)V
    .locals 2

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 90
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRequestId:I

    const/4 v1, 0x0

    .line 91
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRedirectUri:Ljava/lang/String;

    .line 92
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mScope:Ljava/lang/String;

    .line 93
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 94
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 95
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSilent:Z

    .line 96
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mVersion:Ljava/lang/String;

    .line 97
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mForceRefresh:Z

    .line 98
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSkipCache:Z

    .line 99
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSamlAssertion:Ljava/lang/String;

    .line 100
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 101
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mResource:Ljava/lang/String;

    .line 102
    iput-object p3, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 103
    iput-object p4, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 104
    iput-object p5, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mCorrelationId:Ljava/util/UUID;

    .line 105
    iput-boolean p6, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIsExtendedLifetimeEnabled:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;ZZLjava/lang/String;)V
    .locals 2

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 107
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRequestId:I

    const/4 v1, 0x0

    .line 108
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRedirectUri:Ljava/lang/String;

    .line 109
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mScope:Ljava/lang/String;

    .line 110
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 111
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 112
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSilent:Z

    .line 113
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mVersion:Ljava/lang/String;

    .line 114
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSkipCache:Z

    .line 115
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSamlAssertion:Ljava/lang/String;

    .line 116
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 117
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mResource:Ljava/lang/String;

    .line 118
    iput-object p3, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 119
    iput-object p4, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 120
    iput-object p5, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mCorrelationId:Ljava/util/UUID;

    .line 121
    iput-boolean p6, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIsExtendedLifetimeEnabled:Z

    .line 122
    iput-boolean p7, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mForceRefresh:Z

    .line 123
    iput-object p8, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClaimsChallenge:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;Z)V
    .locals 2

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 130
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRequestId:I

    const/4 v1, 0x0

    .line 131
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRedirectUri:Ljava/lang/String;

    .line 132
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mScope:Ljava/lang/String;

    .line 133
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 134
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 135
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 136
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSilent:Z

    .line 137
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mVersion:Ljava/lang/String;

    .line 138
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mForceRefresh:Z

    .line 139
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSkipCache:Z

    .line 140
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSamlAssertion:Ljava/lang/String;

    .line 141
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 142
    iput-object p3, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 143
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mResource:Ljava/lang/String;

    .line 144
    iput-object p4, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mCorrelationId:Ljava/util/UUID;

    .line 145
    iput-boolean p5, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIsExtendedLifetimeEnabled:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;ZLjava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/UUID;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 147
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRequestId:I

    const/4 v1, 0x0

    .line 148
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRedirectUri:Ljava/lang/String;

    .line 149
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 150
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 151
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 152
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSilent:Z

    .line 153
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mVersion:Ljava/lang/String;

    .line 154
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mForceRefresh:Z

    .line 155
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSkipCache:Z

    .line 156
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSamlAssertion:Ljava/lang/String;

    .line 157
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 158
    iput-object p3, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 159
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mResource:Ljava/lang/String;

    .line 160
    iput-object p4, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mCorrelationId:Ljava/util/UUID;

    .line 161
    iput-boolean p5, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIsExtendedLifetimeEnabled:Z

    .line 162
    iput-object p6, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mScope:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 74
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRequestId:I

    const/4 v1, 0x0

    .line 75
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRedirectUri:Ljava/lang/String;

    .line 76
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mScope:Ljava/lang/String;

    .line 77
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 78
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 79
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 80
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSilent:Z

    .line 81
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mVersion:Ljava/lang/String;

    .line 82
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mForceRefresh:Z

    .line 83
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSkipCache:Z

    .line 84
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSamlAssertion:Ljava/lang/String;

    .line 85
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 86
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mResource:Ljava/lang/String;

    .line 87
    iput-object p3, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 88
    iput-boolean p4, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIsExtendedLifetimeEnabled:Z

    return-void
.end method


# virtual methods
.method public getAppName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAppName:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAppVersion:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getAssertionType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAssertionType:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getAuthority()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getBrokerAccountName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getClaimsChallenge()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClaimsChallenge:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getClientCapabilities()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientCapabilities:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getCorrelationId()Ljava/util/UUID;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mCorrelationId:Ljava/util/UUID;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getExtraQueryParamsAuthentication()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mExtraQueryParamsAuthentication:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getForceRefresh()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mForceRefresh:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getInstanceDiscoveryMetadata()Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mInstanceDiscoveryMetadata:Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getIsExtendedLifetimeEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIsExtendedLifetimeEnabled:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getLogInfo()Ljava/lang/String;
    .locals 3

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    iget-object v2, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    iget-object v2, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const-string v1, "Request authority:%s clientid:%s"

    .line 15
    .line 16
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    return-object v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public getLoginHint()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getPrompt()Lcom/microsoft/aad/adal/PromptBehavior;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mPrompt:Lcom/microsoft/aad/adal/PromptBehavior;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getRedirectUri()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRedirectUri:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getRequestId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRequestId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getResource()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mResource:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getSamlAssertion()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSamlAssertion:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getScope()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mScope:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getSkipCache()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSkipCache:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getTelemetryRequestId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mTelemetryRequestId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getUpnSuffix()Ljava/lang/String;
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLoginHint()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    const-string v2, "@"

    .line 9
    .line 10
    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    const/4 v3, -0x1

    .line 15
    if-ne v3, v2, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 19
    .line 20
    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    move-object v1, v0

    .line 25
    :cond_1
    :goto_0
    return-object v1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method getUserFromRequest()Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;->LoginHint:Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIdentifierType:Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 8
    .line 9
    return-object v0

    .line 10
    :cond_0
    sget-object v0, Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;->UniqueId:Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;

    .line 11
    .line 12
    if-ne v0, v1, :cond_1

    .line 13
    .line 14
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 15
    .line 16
    return-object v0

    .line 17
    :cond_1
    const/4 v0, 0x0

    .line 18
    return-object v0
    .line 19
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getUserIdentifierType()Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIdentifierType:Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mVersion:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public isClaimsChallengePresent()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getClaimsChallenge()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    xor-int/lit8 v0, v0, 0x1

    .line 10
    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public isSilent()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSilent:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAppName:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setAppVersion(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAppVersion:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setAuthority(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mAuthority:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setBrokerAccountName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setClaimsChallenge(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClaimsChallenge:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setClientCapabilities(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientCapabilities:Ljava/util/List;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setClientId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mClientId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setCorrelationId(Ljava/util/UUID;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mCorrelationId:Ljava/util/UUID;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setExtraQueryParamsAuthentication(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mExtraQueryParamsAuthentication:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setForceRefresh(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mForceRefresh:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setInstanceDiscoveryMetadata(Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mInstanceDiscoveryMetadata:Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setLoginHint(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setPrompt(Lcom/microsoft/aad/adal/PromptBehavior;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mPrompt:Lcom/microsoft/aad/adal/PromptBehavior;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setRedirectUri(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRedirectUri:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setRequestId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mRequestId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setResource(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mResource:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setScope(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mScope:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSilent(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSilent:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setSkipCache(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mSkipCache:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setTelemetryRequestId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mTelemetryRequestId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mUserId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setUserIdentifierType(Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mIdentifierType:Lcom/microsoft/aad/adal/AuthenticationRequest$UserIdentifierType;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mLoginHint:Ljava/lang/String;

    .line 2
    .line 3
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mBrokerAccountName:Ljava/lang/String;

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationRequest;->mVersion:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

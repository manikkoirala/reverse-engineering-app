.class Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;
.super Ljava/lang/Object;
.source "AcquireTokenRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/aad/adal/AcquireTokenRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CallbackHandler"
.end annotation


# instance fields
.field private mCallback:Lcom/microsoft/aad/adal/AuthenticationCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/aad/adal/AuthenticationCallback<",
            "Lcom/microsoft/aad/adal/AuthenticationResult;",
            ">;"
        }
    .end annotation
.end field

.field private mRefHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;Lcom/microsoft/aad/adal/AuthenticationCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Lcom/microsoft/aad/adal/AuthenticationCallback<",
            "Lcom/microsoft/aad/adal/AuthenticationResult;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->mRefHandler:Landroid/os/Handler;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->mCallback:Lcom/microsoft/aad/adal/AuthenticationCallback;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic access$700(Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;)Lcom/microsoft/aad/adal/AuthenticationCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->mCallback:Lcom/microsoft/aad/adal/AuthenticationCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method getCallback()Lcom/microsoft/aad/adal/AuthenticationCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/aad/adal/AuthenticationCallback<",
            "Lcom/microsoft/aad/adal/AuthenticationResult;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->mCallback:Lcom/microsoft/aad/adal/AuthenticationCallback;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public onError(Lcom/microsoft/aad/adal/AuthenticationException;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->mCallback:Lcom/microsoft/aad/adal/AuthenticationCallback;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->mRefHandler:Landroid/os/Handler;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler$1;

    .line 10
    .line 11
    invoke-direct {v0, p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler$1;-><init>(Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/AuthenticationException;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-interface {v0, p1}, Lcom/microsoft/aad/adal/AuthenticationCallback;->onError(Ljava/lang/Exception;)V

    .line 19
    .line 20
    .line 21
    :cond_1
    :goto_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public onSuccess(Lcom/microsoft/aad/adal/AuthenticationResult;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->mCallback:Lcom/microsoft/aad/adal/AuthenticationCallback;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->mRefHandler:Landroid/os/Handler;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler$2;

    .line 10
    .line 11
    invoke-direct {v0, p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler$2;-><init>(Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/AuthenticationResult;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-interface {v0, p1}, Lcom/microsoft/aad/adal/AuthenticationCallback;->onSuccess(Ljava/lang/Object;)V

    .line 19
    .line 20
    .line 21
    :cond_1
    :goto_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.class public Lcom/microsoft/aad/adal/AuthenticationResult;
.super Ljava/lang/Object;
.source "AuthenticationResult.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1f220f817b95bab0L


# instance fields
.field protected mAccessToken:Ljava/lang/String;

.field protected mAuthority:Ljava/lang/String;

.field protected mCliTelemInfo:Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;

.field private mClientId:Ljava/lang/String;

.field private mClientInfo:Lcom/microsoft/identity/common/java/providers/microsoft/azureactivedirectory/ClientInfo;

.field private mCode:Ljava/lang/String;

.field protected mErrorCode:Ljava/lang/String;

.field protected mErrorCodes:Ljava/lang/String;

.field protected mErrorDescription:Ljava/lang/String;

.field private mExpiresIn:Ljava/lang/Long;

.field protected mExpiresOn:Ljava/util/Date;

.field protected mExtendedExpiresOn:Ljava/util/Date;

.field protected mFamilyClientId:Ljava/lang/String;

.field protected mHttpResponseBody:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mHttpResponseHeaders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field protected mIdToken:Ljava/lang/String;

.field protected mInitialRequest:Z

.field protected mIsExtendedLifeTimeToken:Z

.field protected mIsMultiResourceRefreshToken:Z

.field protected mRefreshToken:Ljava/lang/String;

.field private mResource:Ljava/lang/String;

.field private mResponseReceived:Ljava/lang/Long;

.field protected mServiceStatusCode:I

.field protected mStatus:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

.field protected mTenantId:Ljava/lang/String;

.field private mTokenType:Ljava/lang/String;

.field protected mUserInfo:Lcom/microsoft/aad/adal/UserInfo;


# direct methods
.method constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;->Failed:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mStatus:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIsExtendedLifeTimeToken:Z

    const/4 v0, 0x0

    .line 4
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseBody:Ljava/util/HashMap;

    const/4 v1, -0x1

    .line 5
    iput v1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mServiceStatusCode:I

    .line 6
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 7
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mCode:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    sget-object v0, Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;->Failed:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mStatus:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    const/4 v0, 0x0

    .line 10
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIsExtendedLifeTimeToken:Z

    const/4 v0, 0x0

    .line 11
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseBody:Ljava/util/HashMap;

    const/4 v1, -0x1

    .line 12
    iput v1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mServiceStatusCode:I

    .line 13
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 14
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mClientId:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mCode:Ljava/lang/String;

    .line 16
    sget-object p1, Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;->Succeeded:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mStatus:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    .line 17
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mAccessToken:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mRefreshToken:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    sget-object v0, Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;->Failed:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    const/4 v1, 0x0

    .line 38
    iput-boolean v1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIsExtendedLifeTimeToken:Z

    const/4 v1, 0x0

    .line 39
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseBody:Ljava/util/HashMap;

    const/4 v2, -0x1

    .line 40
    iput v2, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mServiceStatusCode:I

    .line 41
    iput-object v1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 42
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mErrorCode:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mErrorDescription:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mErrorCodes:Ljava/lang/String;

    .line 45
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mStatus:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;ZLcom/microsoft/aad/adal/UserInfo;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V
    .locals 2

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;->Failed:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mStatus:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    const/4 v0, 0x0

    .line 21
    iput-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIsExtendedLifeTimeToken:Z

    const/4 v0, 0x0

    .line 22
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseBody:Ljava/util/HashMap;

    const/4 v1, -0x1

    .line 23
    iput v1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mServiceStatusCode:I

    .line 24
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 25
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mCode:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mAccessToken:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mRefreshToken:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mExpiresOn:Ljava/util/Date;

    .line 29
    iput-boolean p4, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIsMultiResourceRefreshToken:Z

    .line 30
    sget-object p1, Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;->Succeeded:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mStatus:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    .line 31
    iput-object p5, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mUserInfo:Lcom/microsoft/aad/adal/UserInfo;

    .line 32
    iput-object p6, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mTenantId:Ljava/lang/String;

    .line 33
    iput-object p7, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIdToken:Ljava/lang/String;

    .line 34
    iput-object p8, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mExtendedExpiresOn:Ljava/util/Date;

    .line 35
    iput-object p9, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mClientId:Ljava/lang/String;

    return-void
.end method

.method static createExtendedLifeTimeResult(Lcom/microsoft/aad/adal/TokenCacheItem;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/microsoft/aad/adal/AuthenticationResult;->createResult(Lcom/microsoft/aad/adal/TokenCacheItem;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AuthenticationResult;->getExtendedExpiresOn()Ljava/util/Date;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p0, v0}, Lcom/microsoft/aad/adal/AuthenticationResult;->setExpiresOn(Ljava/util/Date;)V

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    invoke-virtual {p0, v0}, Lcom/microsoft/aad/adal/AuthenticationResult;->setIsExtendedLifeTimeToken(Z)V

    .line 14
    .line 15
    .line 16
    return-object p0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static createResult(Lcom/microsoft/aad/adal/TokenCacheItem;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 11

    .line 1
    if-nez p0, :cond_0

    .line 2
    .line 3
    new-instance p0, Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AuthenticationResult;-><init>()V

    .line 6
    .line 7
    .line 8
    sget-object v0, Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;->Failed:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    .line 9
    .line 10
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mStatus:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    .line 11
    .line 12
    return-object p0

    .line 13
    :cond_0
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getAccessToken()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getRefreshToken()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getExpiresOn()Ljava/util/Date;

    .line 24
    .line 25
    .line 26
    move-result-object v4

    .line 27
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getIsMultiResourceRefreshToken()Z

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getUserInfo()Lcom/microsoft/aad/adal/UserInfo;

    .line 32
    .line 33
    .line 34
    move-result-object v6

    .line 35
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getTenantId()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v7

    .line 39
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getRawIdToken()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v8

    .line 43
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getExtendedExpiresOn()Ljava/util/Date;

    .line 44
    .line 45
    .line 46
    move-result-object v9

    .line 47
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getClientId()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v10

    .line 51
    move-object v1, v0

    .line 52
    invoke-direct/range {v1 .. v10}, Lcom/microsoft/aad/adal/AuthenticationResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;ZLcom/microsoft/aad/adal/UserInfo;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    new-instance v1, Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;

    .line 56
    .line 57
    invoke-direct {v1}, Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;-><init>()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/TokenCacheItem;->getSpeRing()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p0

    .line 64
    invoke-virtual {v1, p0}, Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;->_setSpeRing(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v1}, Lcom/microsoft/aad/adal/AuthenticationResult;->setCliTelemInfo(Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;)V

    .line 68
    .line 69
    .line 70
    return-object v0
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method static createResultForInitialRequest(Ljava/lang/String;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 2

    .line 1
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/microsoft/aad/adal/AuthenticationResult;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    iput-boolean v1, v0, Lcom/microsoft/aad/adal/AuthenticationResult;->mInitialRequest:Z

    .line 8
    .line 9
    iput-object p0, v0, Lcom/microsoft/aad/adal/AuthenticationResult;->mClientId:Ljava/lang/String;

    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public createAuthorizationHeader()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "Bearer "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AuthenticationResult;->getAccessToken()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mAccessToken:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getAccessTokenType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mTokenType:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public final getAuthority()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mAuthority:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public final getCliTelemInfo()Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mCliTelemInfo:Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mClientId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getClientInfo()Lcom/microsoft/identity/common/java/providers/microsoft/azureactivedirectory/ClientInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mClientInfo:Lcom/microsoft/identity/common/java/providers/microsoft/azureactivedirectory/ClientInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getCode()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mCode:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getErrorCode()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mErrorCode:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getErrorCodes()[Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mErrorCodes:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v1, "[\\[\\]]"

    .line 6
    .line 7
    const-string v2, ""

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "([^,]),"

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    :goto_0
    return-object v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public getErrorDescription()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mErrorDescription:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getErrorLogInfo()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, " ErrorCode:"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AuthenticationResult;->getErrorCode()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public getExpiresIn()Ljava/lang/Long;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mExpiresIn:Ljava/lang/Long;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getExpiresOn()Ljava/util/Date;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mExpiresOn:Ljava/util/Date;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/microsoft/identity/common/java/util/DateExtensions;->〇080(Ljava/util/Date;)Ljava/util/Date;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method final getExtendedExpiresOn()Ljava/util/Date;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mExtendedExpiresOn:Ljava/util/Date;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method final getFamilyClientId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mFamilyClientId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getHttpResponseBody()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseBody:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getHttpResponseHeaders()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getIdToken()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIdToken:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getIsMultiResourceRefreshToken()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIsMultiResourceRefreshToken:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getRefreshToken()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mRefreshToken:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getResource()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mResource:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getResponseReceived()Ljava/lang/Long;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mResponseReceived:Ljava/lang/Long;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getServiceStatusCode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mServiceStatusCode:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getStatus()Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mStatus:Lcom/microsoft/aad/adal/AuthenticationResult$AuthenticationStatus;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getTenantId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mTenantId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getUserInfo()Lcom/microsoft/aad/adal/UserInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mUserInfo:Lcom/microsoft/aad/adal/UserInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public isExpired()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIsExtendedLifeTimeToken:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AuthenticationResult;->getExtendedExpiresOn()Ljava/util/Date;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Lcom/microsoft/aad/adal/TokenCacheItem;->isTokenExpired(Ljava/util/Date;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0

    .line 14
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/aad/adal/AuthenticationResult;->getExpiresOn()Ljava/util/Date;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-static {v0}, Lcom/microsoft/aad/adal/TokenCacheItem;->isTokenExpired(Ljava/util/Date;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public isExtendedLifeTimeToken()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIsExtendedLifeTimeToken:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method isInitialRequest()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mInitialRequest:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public final setAuthority(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/microsoft/aad/adal/StringExtensions;->isNullOrBlank(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mAuthority:Ljava/lang/String;

    .line 8
    .line 9
    :cond_0
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method final setCliTelemInfo(Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mCliTelemInfo:Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setClientId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mClientId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setClientInfo(Lcom/microsoft/identity/common/java/providers/microsoft/azureactivedirectory/ClientInfo;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mClientInfo:Lcom/microsoft/identity/common/java/providers/microsoft/azureactivedirectory/ClientInfo;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setCode(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mCode:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setExpiresIn(Ljava/lang/Long;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mExpiresIn:Ljava/lang/Long;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method final setExpiresOn(Ljava/util/Date;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mExpiresOn:Ljava/util/Date;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method final setExtendedExpiresOn(Ljava/util/Date;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mExtendedExpiresOn:Ljava/util/Date;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method final setFamilyClientId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mFamilyClientId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setHttpResponse(Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;->〇o〇()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iput v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mServiceStatusCode:I

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;->〇o00〇〇Oo()Ljava/util/Map;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    new-instance v0, Ljava/util/HashMap;

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;->〇o00〇〇Oo()Ljava/util/Map;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 25
    .line 26
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;->〇080()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    .line 33
    .line 34
    invoke-static {p1}, Lcom/microsoft/identity/common/adal/internal/util/HashMapExtensions;->〇080(Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)Ljava/util/HashMap;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 39
    .line 40
    .line 41
    iput-object v0, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseBody:Ljava/util/HashMap;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :catch_0
    move-exception p1

    .line 45
    const-class v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 46
    .line 47
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-static {p1}, Lcom/microsoft/aad/adal/ExceptionExtensions;->getExceptionMessage(Ljava/lang/Exception;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->SERVER_INVALID_JSON_RESPONSE:Lcom/microsoft/aad/adal/ADALError;

    .line 56
    .line 57
    const-string v2, "Json exception"

    .line 58
    .line 59
    invoke-static {v0, v2, p1, v1}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 60
    .line 61
    .line 62
    :cond_1
    :goto_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method setHttpResponseBody(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseBody:Ljava/util/HashMap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setHttpResponseHeaders(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mHttpResponseHeaders:Ljava/util/HashMap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setIdToken(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIdToken:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method final setIsExtendedLifeTimeToken(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mIsExtendedLifeTimeToken:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setRefreshToken(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mRefreshToken:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setResource(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mResource:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setResponseReceived(Ljava/lang/Long;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mResponseReceived:Ljava/lang/Long;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setServiceStatusCode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mServiceStatusCode:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setTenantId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mTenantId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setUserInfo(Lcom/microsoft/aad/adal/UserInfo;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AuthenticationResult;->mUserInfo:Lcom/microsoft/aad/adal/UserInfo;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

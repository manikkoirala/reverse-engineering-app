.class abstract Lcom/microsoft/aad/adal/AbstractMetadataRequestor;
.super Ljava/lang/Object;
.source "AbstractMetadataRequestor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MetadataType:",
        "Ljava/lang/Object;",
        "MetadataRequestOptions:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mCorrelationId:Ljava/util/UUID;

.field private mGson:Lcom/google/gson/Gson;

.field private final mWebrequestHandler:Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/microsoft/identity/common/adal/internal/net/WebRequestHandler;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/microsoft/identity/common/adal/internal/net/WebRequestHandler;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;->mWebrequestHandler:Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public final getCorrelationId()Ljava/util/UUID;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;->mCorrelationId:Ljava/util/UUID;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method getWebrequestHandler()Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;->mWebrequestHandler:Lcom/microsoft/identity/common/adal/internal/net/IWebRequestHandler;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method abstract parseMetadata(Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/identity/common/adal/internal/net/HttpWebResponse;",
            ")TMetadataType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method declared-synchronized parser()Lcom/google/gson/Gson;
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;->mGson:Lcom/google/gson/Gson;

    .line 3
    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    new-instance v0, Lcom/google/gson/Gson;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;->mGson:Lcom/google/gson/Gson;

    .line 12
    .line 13
    :cond_0
    iget-object v0, p0, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;->mGson:Lcom/google/gson/Gson;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 14
    .line 15
    monitor-exit p0

    .line 16
    return-object v0

    .line 17
    :catchall_0
    move-exception v0

    .line 18
    monitor-exit p0

    .line 19
    throw v0
.end method

.method abstract requestMetadata(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMetadataRequestOptions;)TMetadataType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public final setCorrelationId(Ljava/util/UUID;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/AbstractMetadataRequestor;->mCorrelationId:Ljava/util/UUID;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

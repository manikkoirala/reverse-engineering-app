.class Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection$1;
.super Ljava/lang/Object;
.source "BrokerAccountServiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->unBindService(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;Landroid/content/Context;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection$1;->this$1:Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection$1;->val$context:Landroid/content/Context;

    .line 4
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection$1;->this$1:Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->access$600(Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection$1;->val$context:Landroid/content/Context;

    .line 11
    .line 12
    iget-object v2, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection$1;->this$1:Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15
    .line 16
    .line 17
    :goto_0
    iget-object v1, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection$1;->this$1:Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;

    .line 18
    .line 19
    invoke-static {v1, v0}, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->access$602(Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;Z)Z

    .line 20
    .line 21
    .line 22
    goto :goto_2

    .line 23
    :catchall_0
    move-exception v1

    .line 24
    goto :goto_1

    .line 25
    :catch_0
    move-exception v1

    .line 26
    :try_start_1
    invoke-static {}, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler;->access$200()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    const-string v3, "Unbind threw IllegalArgumentException"

    .line 31
    .line 32
    const-string v4, ""

    .line 33
    .line 34
    const/4 v5, 0x0

    .line 35
    invoke-static {v2, v3, v4, v5, v1}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :goto_1
    iget-object v2, p0, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection$1;->this$1:Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;

    .line 40
    .line 41
    invoke-static {v2, v0}, Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;->access$602(Lcom/microsoft/aad/adal/BrokerAccountServiceHandler$BrokerAccountServiceConnection;Z)Z

    .line 42
    .line 43
    .line 44
    throw v1

    .line 45
    :cond_0
    :goto_2
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

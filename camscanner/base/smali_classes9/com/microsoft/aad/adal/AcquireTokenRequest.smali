.class Lcom/microsoft/aad/adal/AcquireTokenRequest;
.super Ljava/lang/Object;
.source "AcquireTokenRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;
    }
.end annotation


# static fields
.field private static final AUTHENTICATOR_LLT_VERSION_CODE:J = 0x8aL

.field private static final CP_LLT_VERSION_CODE:J = 0x2d0642L

.field private static final TAG:Ljava/lang/String; = "AcquireTokenRequest"

.field private static final THREAD_EXECUTOR:Ljava/util/concurrent/ExecutorService;

.field private static sHandler:Landroid/os/Handler;


# instance fields
.field private mAPIEvent:Lcom/microsoft/aad/adal/APIEvent;

.field private final mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

.field private final mBrokerProxy:Lcom/microsoft/aad/adal/IBrokerProxy;

.field private final mContext:Landroid/content/Context;

.field private mDiscovery:Lcom/microsoft/aad/adal/Discovery;

.field private mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sput-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->THREAD_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    sput-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->sHandler:Landroid/os/Handler;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method constructor <init>(Landroid/content/Context;Lcom/microsoft/aad/adal/AuthenticationContext;Lcom/microsoft/aad/adal/APIEvent;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mContext:Landroid/content/Context;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 7
    .line 8
    new-instance v0, Lcom/microsoft/aad/adal/Discovery;

    .line 9
    .line 10
    invoke-direct {v0, p1}, Lcom/microsoft/aad/adal/Discovery;-><init>(Landroid/content/Context;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mDiscovery:Lcom/microsoft/aad/adal/Discovery;

    .line 14
    .line 15
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationContext;->getCache()Lcom/microsoft/aad/adal/ITokenCacheStore;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    if-eqz p3, :cond_0

    .line 22
    .line 23
    new-instance v0, Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationContext;->getCache()Lcom/microsoft/aad/adal/ITokenCacheStore;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationContext;->getAuthority()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    invoke-virtual {p3}, Lcom/microsoft/aad/adal/DefaultEvent;->getTelemetryRequestId()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/aad/adal/TokenCacheAccessor;-><init>(Landroid/content/Context;Lcom/microsoft/aad/adal/ITokenCacheStore;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    iput-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 45
    .line 46
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationContext;->getValidateAuthority()Z

    .line 47
    .line 48
    .line 49
    move-result p2

    .line 50
    invoke-virtual {v0, p2}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->setValidateAuthorityHost(Z)V

    .line 51
    .line 52
    .line 53
    :cond_0
    new-instance p2, Lcom/microsoft/aad/adal/BrokerProxy;

    .line 54
    .line 55
    invoke-direct {p2, p1}, Lcom/microsoft/aad/adal/BrokerProxy;-><init>(Landroid/content/Context;)V

    .line 56
    .line 57
    .line 58
    iput-object p2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mBrokerProxy:Lcom/microsoft/aad/adal/IBrokerProxy;

    .line 59
    .line 60
    iput-object p3, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAPIEvent:Lcom/microsoft/aad/adal/APIEvent;

    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method static synthetic access$100(Lcom/microsoft/aad/adal/AcquireTokenRequest;Lcom/microsoft/aad/adal/AuthenticationRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->validateAcquireTokenRequest(Lcom/microsoft/aad/adal/AuthenticationRequest;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic access$200(Lcom/microsoft/aad/adal/AcquireTokenRequest;Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/IWindowComponent;ZLcom/microsoft/aad/adal/AuthenticationRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->performAcquireTokenRequest(Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/IWindowComponent;ZLcom/microsoft/aad/adal/AuthenticationRequest;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
.end method

.method static synthetic access$300(Lcom/microsoft/aad/adal/AcquireTokenRequest;)Lcom/microsoft/aad/adal/APIEvent;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAPIEvent:Lcom/microsoft/aad/adal/APIEvent;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$400(Lcom/microsoft/aad/adal/AcquireTokenRequest;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mContext:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$500(Lcom/microsoft/aad/adal/AcquireTokenRequest;)Lcom/microsoft/aad/adal/TokenCacheAccessor;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$600(Lcom/microsoft/aad/adal/AcquireTokenRequest;Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->waitingRequestOnError(Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
.end method

.method private acquireTokenInteractiveFlow(Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/IWindowComponent;ZLcom/microsoft/aad/adal/AuthenticationRequest;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    if-nez p2, :cond_1

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 7
    .line 8
    sget-object p2, Lcom/microsoft/aad/adal/ADALError;->AUTH_REFRESH_FAILED_PROMPT_NOT_ALLOWED:Lcom/microsoft/aad/adal/ADALError;

    .line 9
    .line 10
    new-instance p3, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p4}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLogInfo()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p4

    .line 19
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string p4, " Cannot launch webview, activity is null."

    .line 23
    .line 24
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p3

    .line 31
    invoke-direct {p1, p2, p3}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    throw p1

    .line 35
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mContext:Landroid/content/Context;

    .line 36
    .line 37
    invoke-static {v0}, Lcom/microsoft/aad/adal/HttpUtil;->throwIfNetworkNotAvailable(Landroid/content/Context;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->getCallback()Lcom/microsoft/aad/adal/AuthenticationCallback;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    invoke-virtual {p4, v0}, Lcom/microsoft/aad/adal/AuthenticationRequest;->setRequestId(I)V

    .line 49
    .line 50
    .line 51
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 52
    .line 53
    new-instance v2, Lcom/microsoft/aad/adal/AuthenticationRequestState;

    .line 54
    .line 55
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->getCallback()Lcom/microsoft/aad/adal/AuthenticationCallback;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    iget-object v4, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAPIEvent:Lcom/microsoft/aad/adal/APIEvent;

    .line 60
    .line 61
    invoke-direct {v2, v0, p4, v3, v4}, Lcom/microsoft/aad/adal/AuthenticationRequestState;-><init>(ILcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/AuthenticationCallback;Lcom/microsoft/aad/adal/APIEvent;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v1, v0, v2}, Lcom/microsoft/aad/adal/AuthenticationContext;->putWaitingRequest(ILcom/microsoft/aad/adal/AuthenticationRequestState;)V

    .line 65
    .line 66
    .line 67
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mBrokerProxy:Lcom/microsoft/aad/adal/IBrokerProxy;

    .line 68
    .line 69
    invoke-virtual {p4}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getAuthority()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    invoke-interface {v0, v1}, Lcom/microsoft/aad/adal/IBrokerProxy;->canSwitchToBroker(Ljava/lang/String;)Lcom/microsoft/aad/adal/BrokerProxy$SwitchToBroker;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    sget-object v1, Lcom/microsoft/aad/adal/BrokerProxy$SwitchToBroker;->CANNOT_SWITCH_TO_BROKER:Lcom/microsoft/aad/adal/BrokerProxy$SwitchToBroker;

    .line 78
    .line 79
    const-string v2, ":acquireTokenInteractiveFlow"

    .line 80
    .line 81
    const/4 v3, 0x0

    .line 82
    if-eq v0, v1, :cond_3

    .line 83
    .line 84
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mBrokerProxy:Lcom/microsoft/aad/adal/IBrokerProxy;

    .line 85
    .line 86
    invoke-virtual {p4}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLoginHint()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v4

    .line 90
    invoke-virtual {p4}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUserId()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v5

    .line 94
    invoke-interface {v1, v4, v5}, Lcom/microsoft/aad/adal/IBrokerProxy;->verifyUser(Ljava/lang/String;Ljava/lang/String;)Z

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    if-eqz v1, :cond_3

    .line 99
    .line 100
    sget-object p3, Lcom/microsoft/aad/adal/BrokerProxy$SwitchToBroker;->NEED_PERMISSIONS_TO_SWITCH_TO_BROKER:Lcom/microsoft/aad/adal/BrokerProxy$SwitchToBroker;

    .line 101
    .line 102
    if-eq v0, p3, :cond_2

    .line 103
    .line 104
    new-instance p3, Ljava/lang/StringBuilder;

    .line 105
    .line 106
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .line 108
    .line 109
    sget-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 110
    .line 111
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object p3

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    .line 122
    .line 123
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    .line 125
    .line 126
    const-string v1, ""

    .line 127
    .line 128
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->getCallback()Lcom/microsoft/aad/adal/AuthenticationCallback;

    .line 132
    .line 133
    .line 134
    move-result-object p1

    .line 135
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    .line 136
    .line 137
    .line 138
    move-result p1

    .line 139
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    const-string v0, "Launch activity for interactive authentication via broker with callback. "

    .line 147
    .line 148
    invoke-static {p3, v0, p1, v3}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 149
    .line 150
    .line 151
    new-instance p1, Lcom/microsoft/aad/adal/AcquireTokenWithBrokerRequest;

    .line 152
    .line 153
    iget-object p3, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mBrokerProxy:Lcom/microsoft/aad/adal/IBrokerProxy;

    .line 154
    .line 155
    invoke-direct {p1, p4, p3}, Lcom/microsoft/aad/adal/AcquireTokenWithBrokerRequest;-><init>(Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/IBrokerProxy;)V

    .line 156
    .line 157
    .line 158
    invoke-virtual {p1, p2}, Lcom/microsoft/aad/adal/AcquireTokenWithBrokerRequest;->acquireTokenWithBrokerInteractively(Lcom/microsoft/aad/adal/IWindowComponent;)V

    .line 159
    .line 160
    .line 161
    goto :goto_1

    .line 162
    :cond_2
    new-instance p1, Lcom/microsoft/aad/adal/UsageAuthenticationException;

    .line 163
    .line 164
    sget-object p2, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_BROKER_PERMISSIONS_MISSING:Lcom/microsoft/aad/adal/ADALError;

    .line 165
    .line 166
    const-string p3, "Broker related permissions are missing for GET_ACCOUNTS"

    .line 167
    .line 168
    invoke-direct {p1, p2, p3}, Lcom/microsoft/aad/adal/UsageAuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 169
    .line 170
    .line 171
    throw p1

    .line 172
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 173
    .line 174
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    .line 176
    .line 177
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 178
    .line 179
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    .line 181
    .line 182
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    .line 184
    .line 185
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v0

    .line 189
    new-instance v1, Ljava/lang/StringBuilder;

    .line 190
    .line 191
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    .line 193
    .line 194
    const-string v2, " Callback is: "

    .line 195
    .line 196
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    .line 198
    .line 199
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->getCallback()Lcom/microsoft/aad/adal/AuthenticationCallback;

    .line 200
    .line 201
    .line 202
    move-result-object p1

    .line 203
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    .line 204
    .line 205
    .line 206
    move-result p1

    .line 207
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 208
    .line 209
    .line 210
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 211
    .line 212
    .line 213
    move-result-object p1

    .line 214
    const-string v1, "Starting Authentication Activity for embedded flow. "

    .line 215
    .line 216
    invoke-static {v0, v1, p1, v3}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 217
    .line 218
    .line 219
    new-instance p1, Lcom/microsoft/aad/adal/AcquireTokenInteractiveRequest;

    .line 220
    .line 221
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mContext:Landroid/content/Context;

    .line 222
    .line 223
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 224
    .line 225
    invoke-direct {p1, v0, p4, v1}, Lcom/microsoft/aad/adal/AcquireTokenInteractiveRequest;-><init>(Landroid/content/Context;Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/TokenCacheAccessor;)V

    .line 226
    .line 227
    .line 228
    if-eqz p3, :cond_4

    .line 229
    .line 230
    new-instance v3, Lcom/microsoft/aad/adal/AuthenticationDialog;

    .line 231
    .line 232
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->getHandler()Landroid/os/Handler;

    .line 233
    .line 234
    .line 235
    move-result-object p3

    .line 236
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mContext:Landroid/content/Context;

    .line 237
    .line 238
    invoke-direct {v3, p3, v0, p0, p4}, Lcom/microsoft/aad/adal/AuthenticationDialog;-><init>(Landroid/os/Handler;Landroid/content/Context;Lcom/microsoft/aad/adal/AcquireTokenRequest;Lcom/microsoft/aad/adal/AuthenticationRequest;)V

    .line 239
    .line 240
    .line 241
    :cond_4
    invoke-virtual {p1, p2, v3}, Lcom/microsoft/aad/adal/AcquireTokenInteractiveRequest;->acquireToken(Lcom/microsoft/aad/adal/IWindowComponent;Lcom/microsoft/aad/adal/AuthenticationDialog;)V

    .line 242
    .line 243
    .line 244
    :goto_1
    return-void
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method private acquireTokenSilentFlow(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mBrokerProxy:Lcom/microsoft/aad/adal/IBrokerProxy;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/microsoft/aad/adal/IBrokerProxy;->verifyBrokerForSilentRequest(Lcom/microsoft/aad/adal/AuthenticationRequest;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getForceRefresh()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->isClaimsChallengePresent()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    :cond_0
    if-eqz v0, :cond_1

    .line 20
    .line 21
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->tryAcquireTokenSilentWithBroker(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    return-object p1

    .line 26
    :cond_1
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->tryAcquireTokenSilentLocally(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-direct {p0, v1}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->isAccessTokenReturned(Lcom/microsoft/aad/adal/AuthenticationResult;)Z

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    if-eqz v2, :cond_2

    .line 35
    .line 36
    return-object v1

    .line 37
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getSamlAssertion()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    if-eqz v2, :cond_4

    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getAssertionType()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    if-eqz v2, :cond_4

    .line 48
    .line 49
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->tryAcquireTokenSilentWithAssertion(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    invoke-direct {p0, v2}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->isAccessTokenReturned(Lcom/microsoft/aad/adal/AuthenticationResult;)Z

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    const-string v4, ":acquireTokenSilentFlow"

    .line 58
    .line 59
    if-eqz v3, :cond_3

    .line 60
    .line 61
    new-instance p1, Ljava/lang/StringBuilder;

    .line 62
    .line 63
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    .line 65
    .line 66
    sget-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 67
    .line 68
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    const-string v0, "Access token has been acquired using the saml assertion."

    .line 79
    .line 80
    invoke-static {p1, v0}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    return-object v2

    .line 84
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    .line 85
    .line 86
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .line 88
    .line 89
    sget-object v3, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 90
    .line 91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v2

    .line 101
    const-string v3, "Failed to acquire tokens using saml assertion."

    .line 102
    .line 103
    invoke-static {v2, v3}, Lcom/microsoft/aad/adal/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    :cond_4
    if-eqz v0, :cond_5

    .line 107
    .line 108
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->tryAcquireTokenSilentWithBroker(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    return-object p1

    .line 113
    :cond_5
    return-object v1
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method private addHttpInfoToException(Lcom/microsoft/aad/adal/AuthenticationResult;Lcom/microsoft/aad/adal/AuthenticationException;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    if-eqz p2, :cond_2

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getHttpResponseHeaders()Ljava/util/HashMap;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getHttpResponseHeaders()Ljava/util/HashMap;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {p2, v0}, Lcom/microsoft/aad/adal/AuthenticationException;->setHttpResponseHeaders(Ljava/util/HashMap;)V

    .line 16
    .line 17
    .line 18
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getHttpResponseBody()Ljava/util/HashMap;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getHttpResponseBody()Ljava/util/HashMap;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {p2, v0}, Lcom/microsoft/aad/adal/AuthenticationException;->setHttpResponseBody(Ljava/util/HashMap;)V

    .line 29
    .line 30
    .line 31
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getServiceStatusCode()I

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    invoke-virtual {p2, p1}, Lcom/microsoft/aad/adal/AuthenticationException;->setServiceStatusCode(I)V

    .line 36
    .line 37
    .line 38
    :cond_2
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private checkIfBrokerHasLltChanges()Z
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mContext:Landroid/content/Context;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const-wide v2, 0x7fffffffffffffffL

    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    :try_start_0
    const-string v4, "com.azure.authenticator"

    .line 14
    .line 15
    invoke-virtual {v0, v4, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 16
    .line 17
    .line 18
    move-result-object v4

    .line 19
    invoke-static {v4}, Landroidx/core/content/pm/PackageInfoCompat;->getLongVersionCode(Landroid/content/pm/PackageInfo;)J

    .line 20
    .line 21
    .line 22
    move-result-wide v4
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    goto :goto_0

    .line 24
    :catch_0
    move-wide v4, v2

    .line 25
    :goto_0
    :try_start_1
    const-string v6, "com.microsoft.windowsintune.companyportal"

    .line 26
    .line 27
    invoke-virtual {v0, v6, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 28
    .line 29
    .line 30
    move-result-object v6

    .line 31
    invoke-static {v6}, Landroidx/core/content/pm/PackageInfoCompat;->getLongVersionCode(Landroid/content/pm/PackageInfo;)J

    .line 32
    .line 33
    .line 34
    move-result-wide v2
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 35
    :catch_1
    :try_start_2
    const-string v6, "com.microsoft.identity.testuserapp"

    .line 36
    .line 37
    invoke-virtual {v0, v6, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-static {v0}, Landroidx/core/content/pm/PackageInfoCompat;->getLongVersionCode(Landroid/content/pm/PackageInfo;)J

    .line 42
    .line 43
    .line 44
    move-result-wide v6
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .line 45
    goto :goto_1

    .line 46
    :catch_2
    const-wide/32 v6, 0x7fffffff

    .line 47
    .line 48
    .line 49
    :goto_1
    const-wide/16 v8, 0x8a

    .line 50
    .line 51
    cmp-long v0, v4, v8

    .line 52
    .line 53
    if-ltz v0, :cond_0

    .line 54
    .line 55
    const-wide/32 v4, 0x2d0642

    .line 56
    .line 57
    .line 58
    cmp-long v0, v2, v4

    .line 59
    .line 60
    if-ltz v0, :cond_0

    .line 61
    .line 62
    cmp-long v0, v6, v4

    .line 63
    .line 64
    if-ltz v0, :cond_0

    .line 65
    .line 66
    const/4 v1, 0x1

    .line 67
    :cond_0
    return v1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method private declared-synchronized getHandler()Landroid/os/Handler;
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    sget-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->sHandler:Landroid/os/Handler;

    .line 3
    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    new-instance v0, Landroid/os/Handler;

    .line 7
    .line 8
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 13
    .line 14
    .line 15
    sput-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->sHandler:Landroid/os/Handler;

    .line 16
    .line 17
    :cond_0
    sget-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->sHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 18
    .line 19
    monitor-exit p0

    .line 20
    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    .line 22
    monitor-exit p0

    .line 23
    throw v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method private isAccessTokenReturned(Lcom/microsoft/aad/adal/AuthenticationResult;)Z
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationResult;->getAccessToken()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-static {p1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    const/4 p1, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private performAcquireTokenRequest(Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/IWindowComponent;ZLcom/microsoft/aad/adal/AuthenticationRequest;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p4}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->tryAcquireTokenSilent(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-direct {p0, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->isAccessTokenReturned(Lcom/microsoft/aad/adal/AuthenticationResult;)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    iget-object p2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAPIEvent:Lcom/microsoft/aad/adal/APIEvent;

    .line 12
    .line 13
    const/4 p3, 0x1

    .line 14
    const/4 v1, 0x0

    .line 15
    invoke-virtual {p2, p3, v1}, Lcom/microsoft/aad/adal/APIEvent;->setWasApiCallSuccessful(ZLjava/lang/Exception;)V

    .line 16
    .line 17
    .line 18
    iget-object p2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAPIEvent:Lcom/microsoft/aad/adal/APIEvent;

    .line 19
    .line 20
    invoke-virtual {p4}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getCorrelationId()Ljava/util/UUID;

    .line 21
    .line 22
    .line 23
    move-result-object p3

    .line 24
    invoke-virtual {p3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p3

    .line 28
    invoke-virtual {p2, p3}, Lcom/microsoft/aad/adal/DefaultEvent;->setCorrelationId(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-object p2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAPIEvent:Lcom/microsoft/aad/adal/APIEvent;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationResult;->getIdToken()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p3

    .line 37
    invoke-virtual {p2, p3}, Lcom/microsoft/aad/adal/APIEvent;->setIdToken(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    iget-object p2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAPIEvent:Lcom/microsoft/aad/adal/APIEvent;

    .line 41
    .line 42
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/APIEvent;->stopTelemetryAndFlush()V

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->onSuccess(Lcom/microsoft/aad/adal/AuthenticationResult;)V

    .line 46
    .line 47
    .line 48
    return-void

    .line 49
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 50
    .line 51
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .line 53
    .line 54
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 55
    .line 56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    const-string v1, ":performAcquireTokenRequest"

    .line 60
    .line 61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    const-string v1, "Trying to acquire token interactively."

    .line 69
    .line 70
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->acquireTokenInteractiveFlow(Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/IWindowComponent;ZLcom/microsoft/aad/adal/AuthenticationRequest;)V

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method private performAuthorityValidation(Lcom/microsoft/aad/adal/AuthenticationRequest;Ljava/net/URL;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/microsoft/aad/adal/Telemetry;->getInstance()Lcom/microsoft/aad/adal/Telemetry;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getTelemetryRequestId()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const-string v2, "Microsoft.ADAL.authority_validation"

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2}, Lcom/microsoft/aad/adal/Telemetry;->startEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    new-instance v0, Lcom/microsoft/aad/adal/APIEvent;

    .line 15
    .line 16
    invoke-direct {v0, v2}, Lcom/microsoft/aad/adal/APIEvent;-><init>(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getCorrelationId()Ljava/util/UUID;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Lcom/microsoft/aad/adal/DefaultEvent;->setCorrelationId(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getTelemetryRequestId()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Lcom/microsoft/aad/adal/DefaultEvent;->setRequestId(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/AuthenticationContext;->getValidateAuthority()Z

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    const-string v3, "not_done"

    .line 44
    .line 45
    if-eqz v1, :cond_2

    .line 46
    .line 47
    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUpnSuffix()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->isSilent()Z

    .line 52
    .line 53
    .line 54
    move-result v4

    .line 55
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getCorrelationId()Ljava/util/UUID;

    .line 56
    .line 57
    .line 58
    move-result-object v5

    .line 59
    invoke-direct {p0, p2, v1, v4, v5}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->validateAuthority(Ljava/net/URL;Ljava/lang/String;ZLjava/util/UUID;)V

    .line 60
    .line 61
    .line 62
    const-string/jumbo v1, "yes"

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, v1}, Lcom/microsoft/aad/adal/APIEvent;->setValidationStatus(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/aad/adal/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    .line 67
    .line 68
    invoke-static {}, Lcom/microsoft/aad/adal/Telemetry;->getInstance()Lcom/microsoft/aad/adal/Telemetry;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getTelemetryRequestId()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v3

    .line 76
    invoke-virtual {v1, v3, v0, v2}, Lcom/microsoft/aad/adal/Telemetry;->stopEvent(Ljava/lang/String;Lcom/microsoft/aad/adal/IEvents;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    goto/16 :goto_3

    .line 80
    .line 81
    :catchall_0
    move-exception p2

    .line 82
    goto :goto_1

    .line 83
    :catch_0
    move-exception p2

    .line 84
    :try_start_1
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationException;->getCode()Lcom/microsoft/aad/adal/ADALError;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    if-eqz v1, :cond_1

    .line 89
    .line 90
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationException;->getCode()Lcom/microsoft/aad/adal/ADALError;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    sget-object v4, Lcom/microsoft/aad/adal/ADALError;->DEVICE_CONNECTION_IS_NOT_AVAILABLE:Lcom/microsoft/aad/adal/ADALError;

    .line 95
    .line 96
    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    if-nez v1, :cond_0

    .line 101
    .line 102
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationException;->getCode()Lcom/microsoft/aad/adal/ADALError;

    .line 103
    .line 104
    .line 105
    move-result-object v1

    .line 106
    sget-object v4, Lcom/microsoft/aad/adal/ADALError;->NO_NETWORK_CONNECTION_POWER_OPTIMIZATION:Lcom/microsoft/aad/adal/ADALError;

    .line 107
    .line 108
    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 109
    .line 110
    .line 111
    move-result v1

    .line 112
    if-eqz v1, :cond_1

    .line 113
    .line 114
    :cond_0
    invoke-virtual {v0, v3}, Lcom/microsoft/aad/adal/APIEvent;->setValidationStatus(Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :cond_1
    const-string v1, "no"

    .line 119
    .line 120
    invoke-virtual {v0, v1}, Lcom/microsoft/aad/adal/APIEvent;->setValidationStatus(Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    :goto_0
    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    :goto_1
    invoke-static {}, Lcom/microsoft/aad/adal/Telemetry;->getInstance()Lcom/microsoft/aad/adal/Telemetry;

    .line 125
    .line 126
    .line 127
    move-result-object v1

    .line 128
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getTelemetryRequestId()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object p1

    .line 132
    invoke-virtual {v1, p1, v0, v2}, Lcom/microsoft/aad/adal/Telemetry;->stopEvent(Ljava/lang/String;Lcom/microsoft/aad/adal/IEvents;Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    throw p2

    .line 136
    :cond_2
    invoke-static {p2}, Lcom/microsoft/aad/adal/UrlExtensions;->isADFSAuthority(Ljava/net/URL;)Z

    .line 137
    .line 138
    .line 139
    move-result v1

    .line 140
    if-nez v1, :cond_3

    .line 141
    .line 142
    invoke-static {p2}, Lcom/microsoft/aad/adal/AuthorityValidationMetadataCache;->containsAuthorityHost(Ljava/net/URL;)Z

    .line 143
    .line 144
    .line 145
    move-result v1

    .line 146
    if-nez v1, :cond_3

    .line 147
    .line 148
    :try_start_2
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mDiscovery:Lcom/microsoft/aad/adal/Discovery;

    .line 149
    .line 150
    invoke-virtual {v1, p2}, Lcom/microsoft/aad/adal/Discovery;->validateAuthority(Ljava/net/URL;)V
    :try_end_2
    .catch Lcom/microsoft/aad/adal/AuthenticationException; {:try_start_2 .. :try_end_2} :catch_1

    .line 151
    .line 152
    .line 153
    goto :goto_2

    .line 154
    :catch_1
    invoke-virtual {p2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v1

    .line 158
    new-instance v4, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;

    .line 159
    .line 160
    const/4 v5, 0x0

    .line 161
    invoke-direct {v4, v5}, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;-><init>(Z)V

    .line 162
    .line 163
    .line 164
    invoke-static {v1, v4}, Lcom/microsoft/aad/adal/AuthorityValidationMetadataCache;->updateInstanceDiscoveryMap(Ljava/lang/String;Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;)V

    .line 165
    .line 166
    .line 167
    invoke-virtual {p2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    .line 168
    .line 169
    .line 170
    move-result-object v1

    .line 171
    new-instance v4, Lcom/microsoft/identity/common/java/providers/microsoft/azureactivedirectory/AzureActiveDirectoryCloud;

    .line 172
    .line 173
    invoke-direct {v4, v5}, Lcom/microsoft/identity/common/java/providers/microsoft/azureactivedirectory/AzureActiveDirectoryCloud;-><init>(Z)V

    .line 174
    .line 175
    .line 176
    invoke-static {v1, v4}, Lcom/microsoft/identity/common/java/providers/microsoft/azureactivedirectory/AzureActiveDirectory;->o〇0(Ljava/lang/String;Lcom/microsoft/identity/common/java/providers/microsoft/azureactivedirectory/AzureActiveDirectoryCloud;)V

    .line 177
    .line 178
    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    .line 180
    .line 181
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 182
    .line 183
    .line 184
    sget-object v4, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 185
    .line 186
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    .line 188
    .line 189
    const-string v4, ":performAuthorityValidation"

    .line 190
    .line 191
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    .line 193
    .line 194
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object v1

    .line 198
    const-string v4, "Fail to get authority validation metadata back. Ignore the failure since authority validation is turned off."

    .line 199
    .line 200
    invoke-static {v1, v4}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    .line 202
    .line 203
    :cond_3
    :goto_2
    invoke-virtual {v0, v3}, Lcom/microsoft/aad/adal/APIEvent;->setValidationStatus(Ljava/lang/String;)V

    .line 204
    .line 205
    .line 206
    invoke-static {}, Lcom/microsoft/aad/adal/Telemetry;->getInstance()Lcom/microsoft/aad/adal/Telemetry;

    .line 207
    .line 208
    .line 209
    move-result-object v1

    .line 210
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getTelemetryRequestId()Ljava/lang/String;

    .line 211
    .line 212
    .line 213
    move-result-object v3

    .line 214
    invoke-virtual {v1, v3, v0, v2}, Lcom/microsoft/aad/adal/Telemetry;->stopEvent(Ljava/lang/String;Lcom/microsoft/aad/adal/IEvents;Ljava/lang/String;)V

    .line 215
    .line 216
    .line 217
    :goto_3
    invoke-static {p2}, Lcom/microsoft/aad/adal/AuthorityValidationMetadataCache;->getCachedInstanceDiscoveryMetadata(Ljava/net/URL;)Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;

    .line 218
    .line 219
    .line 220
    move-result-object v0

    .line 221
    if-eqz v0, :cond_5

    .line 222
    .line 223
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->isValidated()Z

    .line 224
    .line 225
    .line 226
    move-result v1

    .line 227
    if-nez v1, :cond_4

    .line 228
    .line 229
    goto :goto_4

    .line 230
    :cond_4
    invoke-direct {p0, p2, p1, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->updatePreferredNetworkLocation(Ljava/net/URL;Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;)V

    .line 231
    .line 232
    .line 233
    :cond_5
    :goto_4
    return-void
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method private removeTokensForUser(Lcom/microsoft/aad/adal/AuthenticationRequest;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUserId()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-static {v0}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUserId()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    goto :goto_0

    .line 21
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLoginHint()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 26
    .line 27
    const-string v2, "1"

    .line 28
    .line 29
    invoke-virtual {v1, v2, v0}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->getFRTItem(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 30
    .line 31
    .line 32
    move-result-object v1
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1

    .line 33
    if-eqz v1, :cond_2

    .line 34
    .line 35
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getResource()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    invoke-virtual {v2, v1, v3}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->removeTokenCacheItem(Lcom/microsoft/aad/adal/TokenCacheItem;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getClientId()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-virtual {v1, v2, v0}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->getMRRTItem(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 55
    .line 56
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getResource()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getClientId()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v4

    .line 64
    invoke-virtual {v2, v3, v4, v0}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->getRegularRefreshTokenCacheItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/aad/adal/TokenCacheItem;

    .line 65
    .line 66
    .line 67
    move-result-object v0
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    .line 68
    if-eqz v1, :cond_3

    .line 69
    .line 70
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 71
    .line 72
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getResource()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    invoke-virtual {v0, v1, p1}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->removeTokenCacheItem(Lcom/microsoft/aad/adal/TokenCacheItem;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_3
    if-eqz v0, :cond_4

    .line 81
    .line 82
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 83
    .line 84
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getResource()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    invoke-virtual {v1, v0, p1}, Lcom/microsoft/aad/adal/TokenCacheAccessor;->removeTokenCacheItem(Lcom/microsoft/aad/adal/TokenCacheItem;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    goto :goto_1

    .line 92
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    .line 93
    .line 94
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .line 96
    .line 97
    sget-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 98
    .line 99
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    const-string v0, ":removeTokensForUser"

    .line 103
    .line 104
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    const-string v0, "No token items need to be deleted for the user."

    .line 112
    .line 113
    invoke-static {p1, v0}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    :goto_1
    return-void

    .line 117
    :catch_0
    move-exception p1

    .line 118
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 119
    .line 120
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_AUTHORITY_IS_NOT_VALID_URL:Lcom/microsoft/aad/adal/ADALError;

    .line 121
    .line 122
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v2

    .line 126
    invoke-direct {v0, v1, v2, p1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 127
    .line 128
    .line 129
    throw v0

    .line 130
    :catch_1
    move-exception p1

    .line 131
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 132
    .line 133
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_AUTHORITY_IS_NOT_VALID_URL:Lcom/microsoft/aad/adal/ADALError;

    .line 134
    .line 135
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v2

    .line 139
    invoke-direct {v0, v1, v2, p1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 140
    .line 141
    .line 142
    throw v0
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method private shouldTrySilentFlow(Lcom/microsoft/aad/adal/AuthenticationRequest;)Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->isClaimsChallengePresent()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->checkIfBrokerHasLltChanges()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x1

    .line 14
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->isSilent()Z

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    if-nez v2, :cond_2

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getPrompt()Lcom/microsoft/aad/adal/PromptBehavior;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    sget-object v0, Lcom/microsoft/aad/adal/PromptBehavior;->Auto:Lcom/microsoft/aad/adal/PromptBehavior;

    .line 27
    .line 28
    if-ne p1, v0, :cond_1

    .line 29
    .line 30
    goto :goto_1

    .line 31
    :cond_1
    const/4 v1, 0x0

    .line 32
    :cond_2
    :goto_1
    return v1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private tryAcquireTokenSilent(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->shouldTrySilentFlow(Lcom/microsoft/aad/adal/AuthenticationRequest;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v2, ":tryAcquireTokenSilent"

    .line 18
    .line 19
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v3, "Try to acquire token silently, return valid AT or use RT in the cache."

    .line 27
    .line 28
    invoke-static {v0, v3}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->acquireTokenSilentFlow(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-direct {p0, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->isAccessTokenReturned(Lcom/microsoft/aad/adal/AuthenticationResult;)Z

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-nez v3, :cond_1

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->isSilent()Z

    .line 42
    .line 43
    .line 44
    move-result v4

    .line 45
    if-eqz v4, :cond_1

    .line 46
    .line 47
    if-nez v0, :cond_0

    .line 48
    .line 49
    const-string v3, "No result returned from acquireTokenSilent"

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    .line 53
    .line 54
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 55
    .line 56
    .line 57
    const-string v4, " ErrorCode:"

    .line 58
    .line 59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationResult;->getErrorCode()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v4

    .line 66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v3

    .line 73
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    .line 74
    .line 75
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .line 77
    .line 78
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    new-instance v2, Ljava/lang/StringBuilder;

    .line 89
    .line 90
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .line 92
    .line 93
    const-string v4, "Prompt is not allowed and failed to get token. "

    .line 94
    .line 95
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v2

    .line 105
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLogInfo()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v4

    .line 109
    sget-object v5, Lcom/microsoft/aad/adal/ADALError;->AUTH_REFRESH_FAILED_PROMPT_NOT_ALLOWED:Lcom/microsoft/aad/adal/ADALError;

    .line 110
    .line 111
    invoke-static {v1, v2, v4, v5}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 112
    .line 113
    .line 114
    new-instance v1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 115
    .line 116
    new-instance v2, Ljava/lang/StringBuilder;

    .line 117
    .line 118
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    .line 120
    .line 121
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLogInfo()Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    const-string p1, " "

    .line 129
    .line 130
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object p1

    .line 140
    invoke-direct {v1, v5, p1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    invoke-direct {p0, v0, v1}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->addHttpInfoToException(Lcom/microsoft/aad/adal/AuthenticationResult;Lcom/microsoft/aad/adal/AuthenticationException;)V

    .line 144
    .line 145
    .line 146
    throw v1

    .line 147
    :cond_1
    if-eqz v3, :cond_3

    .line 148
    .line 149
    new-instance p1, Ljava/lang/StringBuilder;

    .line 150
    .line 151
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    .line 153
    .line 154
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    .line 156
    .line 157
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object p1

    .line 164
    const-string v1, "Token is successfully returned from silent flow. "

    .line 165
    .line 166
    invoke-static {p1, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    .line 168
    .line 169
    goto :goto_1

    .line 170
    :cond_2
    const/4 v0, 0x0

    .line 171
    :cond_3
    :goto_1
    return-object v0
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method private tryAcquireTokenSilentLocally(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    const-string v1, ":tryAcquireTokenSilentLocally"

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "Try to silently get token from local cache."

    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;

    .line 26
    .line 27
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mContext:Landroid/content/Context;

    .line 28
    .line 29
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 30
    .line 31
    invoke-direct {v0, v1, p1, v2}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;-><init>(Landroid/content/Context;Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/TokenCacheAccessor;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->getAccessToken()Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    return-object p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private tryAcquireTokenSilentWithAssertion(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    const-string v1, ":tryAcquireTokenSilentWithAssertion"

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "Try to silently get token using SAML Assertion."

    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;

    .line 26
    .line 27
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mContext:Landroid/content/Context;

    .line 28
    .line 29
    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mTokenCacheAccessor:Lcom/microsoft/aad/adal/TokenCacheAccessor;

    .line 30
    .line 31
    invoke-direct {v0, v1, p1, v2}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;-><init>(Landroid/content/Context;Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/TokenCacheAccessor;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AcquireTokenSilentHandler;->getAccessTokenUsingAssertion()Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    return-object p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private tryAcquireTokenSilentWithBroker(Lcom/microsoft/aad/adal/AuthenticationRequest;)Lcom/microsoft/aad/adal/AuthenticationResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    const-string v1, ":tryAcquireTokenSilentWithBroker"

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "Either could not get tokens from local cache or is force refresh request, switch to Broker for auth, clear tokens from local cache for the user."

    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->removeTokensForUser(Lcom/microsoft/aad/adal/AuthenticationRequest;)V

    .line 26
    .line 27
    .line 28
    new-instance v0, Lcom/microsoft/aad/adal/AcquireTokenWithBrokerRequest;

    .line 29
    .line 30
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mBrokerProxy:Lcom/microsoft/aad/adal/IBrokerProxy;

    .line 31
    .line 32
    invoke-direct {v0, p1, v1}, Lcom/microsoft/aad/adal/AcquireTokenWithBrokerRequest;-><init>(Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/IBrokerProxy;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AcquireTokenWithBrokerRequest;->acquireTokenWithBrokerSilent()Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    return-object p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private updatePreferredNetworkLocation(Ljava/net/URL;Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    if-eqz p3, :cond_1

    .line 2
    .line 3
    invoke-virtual {p3}, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->isValidated()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p3}, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->getPreferredNetwork()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-virtual {p1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-virtual {p3}, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->getPreferredNetwork()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-nez v0, :cond_1

    .line 29
    .line 30
    :try_start_0
    invoke-virtual {p3}, Lcom/microsoft/aad/adal/InstanceDiscoveryMetadata;->getPreferredNetwork()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p3

    .line 34
    invoke-static {p1, p3}, Lcom/microsoft/aad/adal/Discovery;->constructAuthorityUrl(Ljava/net/URL;Ljava/lang/String;)Ljava/net/URL;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-virtual {p2, p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->setAuthority(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :catch_0
    sget-object p1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 47
    .line 48
    const-string p2, "preferred network is invalid"

    .line 49
    .line 50
    const-string/jumbo p3, "use exactly the same authority url that is passed"

    .line 51
    .line 52
    .line 53
    invoke-static {p1, p2, p3}, Lcom/microsoft/aad/adal/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    :cond_1
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private validateAcquireTokenRequest(Lcom/microsoft/aad/adal/AuthenticationRequest;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getAuthority()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->Oo08(Ljava/lang/String;)Ljava/net/URL;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-direct {p0, p1, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->performAuthorityValidation(Lcom/microsoft/aad/adal/AuthenticationRequest;Ljava/net/URL;)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mBrokerProxy:Lcom/microsoft/aad/adal/IBrokerProxy;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getAuthority()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-interface {v0, v1}, Lcom/microsoft/aad/adal/IBrokerProxy;->canSwitchToBroker(Ljava/lang/String;)Lcom/microsoft/aad/adal/BrokerProxy$SwitchToBroker;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    sget-object v1, Lcom/microsoft/aad/adal/BrokerProxy$SwitchToBroker;->CANNOT_SWITCH_TO_BROKER:Lcom/microsoft/aad/adal/BrokerProxy$SwitchToBroker;

    .line 25
    .line 26
    if-eq v0, v1, :cond_1

    .line 27
    .line 28
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mBrokerProxy:Lcom/microsoft/aad/adal/IBrokerProxy;

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLoginHint()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getUserId()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v3

    .line 38
    invoke-interface {v1, v2, v3}, Lcom/microsoft/aad/adal/IBrokerProxy;->verifyUser(Ljava/lang/String;Ljava/lang/String;)Z

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-eqz v1, :cond_1

    .line 43
    .line 44
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->isSilent()Z

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    if-nez v1, :cond_1

    .line 49
    .line 50
    sget-object v1, Lcom/microsoft/aad/adal/BrokerProxy$SwitchToBroker;->NEED_PERMISSIONS_TO_SWITCH_TO_BROKER:Lcom/microsoft/aad/adal/BrokerProxy$SwitchToBroker;

    .line 51
    .line 52
    if-eq v0, v1, :cond_0

    .line 53
    .line 54
    invoke-direct {p0, p1}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->verifyBrokerRedirectUri(Lcom/microsoft/aad/adal/AuthenticationRequest;)V

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_0
    new-instance p1, Lcom/microsoft/aad/adal/UsageAuthenticationException;

    .line 59
    .line 60
    sget-object v0, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_BROKER_PERMISSIONS_MISSING:Lcom/microsoft/aad/adal/ADALError;

    .line 61
    .line 62
    const-string v1, "Broker related permissions are missing for GET_ACCOUNTS."

    .line 63
    .line 64
    invoke-direct {p1, v0, v1}, Lcom/microsoft/aad/adal/UsageAuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    throw p1

    .line 68
    :cond_1
    :goto_0
    return-void

    .line 69
    :cond_2
    new-instance p1, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 70
    .line 71
    sget-object v0, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_AUTHORITY_IS_NOT_VALID_URL:Lcom/microsoft/aad/adal/ADALError;

    .line 72
    .line 73
    invoke-direct {p1, v0}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;)V

    .line 74
    .line 75
    .line 76
    throw p1
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private validateAuthority(Ljava/net/URL;Ljava/lang/String;ZLjava/util/UUID;)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/AuthenticationException;
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/microsoft/aad/adal/UrlExtensions;->isADFSAuthority(Ljava/net/URL;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {p1}, Lcom/microsoft/aad/adal/AuthorityValidationMetadataCache;->isAuthorityValidated(Ljava/net/URL;)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_3

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/AuthenticationContext;->getIsAuthorityValidated()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    goto :goto_1

    .line 22
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    sget-object v2, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v3, ":validateAuthority"

    .line 33
    .line 34
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    const-string v4, "Start validating authority"

    .line 42
    .line 43
    invoke-static {v1, v4}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mDiscovery:Lcom/microsoft/aad/adal/Discovery;

    .line 47
    .line 48
    invoke-virtual {v1, p4}, Lcom/microsoft/aad/adal/Discovery;->setCorrelationId(Ljava/util/UUID;)V

    .line 49
    .line 50
    .line 51
    invoke-static {p1}, Lcom/microsoft/aad/adal/Discovery;->verifyAuthorityValidInstance(Ljava/net/URL;)V

    .line 52
    .line 53
    .line 54
    if-nez p3, :cond_1

    .line 55
    .line 56
    if-eqz v0, :cond_1

    .line 57
    .line 58
    if-eqz p2, :cond_1

    .line 59
    .line 60
    iget-object p3, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mDiscovery:Lcom/microsoft/aad/adal/Discovery;

    .line 61
    .line 62
    invoke-virtual {p3, p1, p2}, Lcom/microsoft/aad/adal/Discovery;->validateAuthorityADFS(Ljava/net/URL;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_1
    if-eqz p3, :cond_2

    .line 67
    .line 68
    invoke-static {p1}, Lcom/microsoft/aad/adal/UrlExtensions;->isADFSAuthority(Ljava/net/URL;)Z

    .line 69
    .line 70
    .line 71
    move-result p2

    .line 72
    if-eqz p2, :cond_2

    .line 73
    .line 74
    new-instance p2, Ljava/lang/StringBuilder;

    .line 75
    .line 76
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object p2

    .line 89
    const-string p3, "Silent request. Skipping AD FS authority validation"

    .line 90
    .line 91
    invoke-static {p2, p3}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    :cond_2
    iget-object p2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mDiscovery:Lcom/microsoft/aad/adal/Discovery;

    .line 95
    .line 96
    invoke-virtual {p2, p1}, Lcom/microsoft/aad/adal/Discovery;->validateAuthority(Ljava/net/URL;)V

    .line 97
    .line 98
    .line 99
    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    .line 100
    .line 101
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    const-string p2, "The passed in authority is valid."

    .line 115
    .line 116
    invoke-static {p1, p2}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    iget-object p1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 120
    .line 121
    const/4 p2, 0x1

    .line 122
    invoke-virtual {p1, p2}, Lcom/microsoft/aad/adal/AuthenticationContext;->setIsAuthorityValidated(Z)V

    .line 123
    .line 124
    .line 125
    :cond_3
    :goto_1
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method private verifyBrokerRedirectUri(Lcom/microsoft/aad/adal/AuthenticationRequest;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/aad/adal/UsageAuthenticationException;
        }
    .end annotation

    .line 1
    const-string v0, "UTF-8"

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getRedirectUri()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    iget-object v1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/AuthenticationContext;->getRedirectUriForBroker()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-static {p1}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    const-string v3, ":verifyBrokerRedirectUri"

    .line 18
    .line 19
    if-nez v2, :cond_4

    .line 20
    .line 21
    const-string/jumbo v2, "urn:ietf:wg:oauth:2.0:oob"

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-eqz v2, :cond_0

    .line 29
    .line 30
    new-instance p1, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    sget-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 36
    .line 37
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    const-string v0, "This is a broker redirectUri. Bypass the check."

    .line 48
    .line 49
    invoke-static {p1, v0}, Lcom/microsoft/identity/common/internal/logging/Logger;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    return-void

    .line 53
    :cond_0
    const-string v2, "msauth://"

    .line 54
    .line 55
    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    if-eqz v4, :cond_3

    .line 60
    .line 61
    new-instance v4, Lcom/microsoft/aad/adal/PackageHelper;

    .line 62
    .line 63
    iget-object v5, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mContext:Landroid/content/Context;

    .line 64
    .line 65
    invoke-direct {v4, v5}, Lcom/microsoft/aad/adal/PackageHelper;-><init>(Landroid/content/Context;)V

    .line 66
    .line 67
    .line 68
    :try_start_0
    iget-object v5, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mContext:Landroid/content/Context;

    .line 69
    .line 70
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    invoke-static {v5, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v5

    .line 78
    iget-object v6, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mContext:Landroid/content/Context;

    .line 79
    .line 80
    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v6

    .line 84
    invoke-virtual {v4, v6}, Lcom/microsoft/identity/common/internal/broker/PackageHelper;->getCurrentSignatureForPackage(Ljava/lang/String;)Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v4

    .line 88
    invoke-static {v4, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    new-instance v4, Ljava/lang/StringBuilder;

    .line 93
    .line 94
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    const-string v2, "/"

    .line 104
    .line 105
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v2

    .line 112
    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 113
    .line 114
    .line 115
    move-result v2

    .line 116
    const-string v4, " so the redirect uri is expected to be: "

    .line 117
    .line 118
    if-eqz v2, :cond_2

    .line 119
    .line 120
    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 121
    .line 122
    .line 123
    move-result p1

    .line 124
    if-eqz p1, :cond_1

    .line 125
    .line 126
    new-instance p1, Ljava/lang/StringBuilder;

    .line 127
    .line 128
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    .line 130
    .line 131
    sget-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 132
    .line 133
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object p1

    .line 143
    const-string v0, "The broker redirect URI is valid."

    .line 144
    .line 145
    invoke-static {p1, v0}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    .line 147
    .line 148
    return-void

    .line 149
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 150
    .line 151
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    .line 153
    .line 154
    const-string v2, "This apps signature is: "

    .line 155
    .line 156
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    .line 158
    .line 159
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    .line 164
    .line 165
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object p1

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    .line 173
    .line 174
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    .line 176
    .line 177
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 178
    .line 179
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    .line 181
    .line 182
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    .line 184
    .line 185
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v0

    .line 189
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_REDIRECTURI_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 190
    .line 191
    const-string v2, "The base64 url encoded signature component of the redirect uri does not match the expected value. "

    .line 192
    .line 193
    invoke-static {v0, v2, p1, v1}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 194
    .line 195
    .line 196
    new-instance p1, Lcom/microsoft/aad/adal/UsageAuthenticationException;

    .line 197
    .line 198
    const-string v0, "The base64 url encoded signature component of the redirect uri does not match the expected value."

    .line 199
    .line 200
    invoke-direct {p1, v1, v0}, Lcom/microsoft/aad/adal/UsageAuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 201
    .line 202
    .line 203
    throw p1

    .line 204
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    .line 205
    .line 206
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 207
    .line 208
    .line 209
    const-string v0, "This apps package name is: "

    .line 210
    .line 211
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    .line 216
    .line 217
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    .line 219
    .line 220
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    .line 222
    .line 223
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 224
    .line 225
    .line 226
    move-result-object p1

    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    .line 228
    .line 229
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 230
    .line 231
    .line 232
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 233
    .line 234
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    .line 236
    .line 237
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    .line 239
    .line 240
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 241
    .line 242
    .line 243
    move-result-object v0

    .line 244
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_REDIRECTURI_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 245
    .line 246
    const-string v2, "The base64 url encoded package name component of the redirect uri does not match the expected value. "

    .line 247
    .line 248
    invoke-static {v0, v2, p1, v1}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 249
    .line 250
    .line 251
    new-instance p1, Lcom/microsoft/aad/adal/UsageAuthenticationException;

    .line 252
    .line 253
    invoke-direct {p1, v1, v2}, Lcom/microsoft/aad/adal/UsageAuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 254
    .line 255
    .line 256
    throw p1

    .line 257
    :catch_0
    move-exception p1

    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    .line 259
    .line 260
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 261
    .line 262
    .line 263
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 264
    .line 265
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    .line 267
    .line 268
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    .line 270
    .line 271
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 272
    .line 273
    .line 274
    move-result-object v0

    .line 275
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->ENCODING_IS_NOT_SUPPORTED:Lcom/microsoft/aad/adal/ADALError;

    .line 276
    .line 277
    invoke-virtual {v1}, Lcom/microsoft/aad/adal/ADALError;->getDescription()Ljava/lang/String;

    .line 278
    .line 279
    .line 280
    move-result-object v2

    .line 281
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 282
    .line 283
    .line 284
    move-result-object v3

    .line 285
    invoke-static {v0, v2, v3, v1, p1}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;Ljava/lang/Throwable;)V

    .line 286
    .line 287
    .line 288
    new-instance v0, Lcom/microsoft/aad/adal/UsageAuthenticationException;

    .line 289
    .line 290
    const-string v2, "The verifying BrokerRedirectUri process failed because the base64 url encoding is not supported."

    .line 291
    .line 292
    invoke-direct {v0, v1, v2, p1}, Lcom/microsoft/aad/adal/UsageAuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 293
    .line 294
    .line 295
    throw v0

    .line 296
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    .line 297
    .line 298
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 299
    .line 300
    .line 301
    const-string v0, " The valid broker redirect URI prefix: msauth so the redirect uri is expected to be: "

    .line 302
    .line 303
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    .line 305
    .line 306
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    .line 308
    .line 309
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 310
    .line 311
    .line 312
    move-result-object p1

    .line 313
    new-instance v0, Ljava/lang/StringBuilder;

    .line 314
    .line 315
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 316
    .line 317
    .line 318
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 319
    .line 320
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    .line 322
    .line 323
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    .line 325
    .line 326
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 327
    .line 328
    .line 329
    move-result-object v0

    .line 330
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_REDIRECTURI_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 331
    .line 332
    const-string v2, "The prefix of the redirect uri does not match the expected value. "

    .line 333
    .line 334
    invoke-static {v0, v2, p1, v1}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 335
    .line 336
    .line 337
    new-instance p1, Lcom/microsoft/aad/adal/UsageAuthenticationException;

    .line 338
    .line 339
    const-string v0, "The prefix of the redirect uri does not match the expected value."

    .line 340
    .line 341
    invoke-direct {p1, v1, v0}, Lcom/microsoft/aad/adal/UsageAuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 342
    .line 343
    .line 344
    throw p1

    .line 345
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    .line 346
    .line 347
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 348
    .line 349
    .line 350
    sget-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 351
    .line 352
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    .line 354
    .line 355
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    .line 357
    .line 358
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 359
    .line 360
    .line 361
    move-result-object p1

    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    .line 363
    .line 364
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 365
    .line 366
    .line 367
    const-string v2, "The redirect uri is expected to be:"

    .line 368
    .line 369
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    .line 371
    .line 372
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    .line 374
    .line 375
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 376
    .line 377
    .line 378
    move-result-object v0

    .line 379
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->DEVELOPER_REDIRECTURI_INVALID:Lcom/microsoft/aad/adal/ADALError;

    .line 380
    .line 381
    const-string v2, "The redirectUri is null or blank. "

    .line 382
    .line 383
    invoke-static {p1, v2, v0, v1}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 384
    .line 385
    .line 386
    new-instance p1, Lcom/microsoft/aad/adal/UsageAuthenticationException;

    .line 387
    .line 388
    const-string v0, "The redirectUri is null or blank."

    .line 389
    .line 390
    invoke-direct {p1, v1, v0}, Lcom/microsoft/aad/adal/UsageAuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 391
    .line 392
    .line 393
    throw p1
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
.end method

.method private waitingRequestOnError(Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V
    .locals 3

    if-eqz p2, :cond_2

    .line 2
    :try_start_0
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getDelegate()Lcom/microsoft/aad/adal/AuthenticationCallback;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":waitingRequestOnError"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sending error to callback"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 4
    invoke-virtual {v2, p2}, Lcom/microsoft/aad/adal/AuthenticationContext;->getCorrelationInfoFromWaitingRequest(Lcom/microsoft/aad/adal/AuthenticationRequestState;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getAPIEvent()Lcom/microsoft/aad/adal/APIEvent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p4}, Lcom/microsoft/aad/adal/APIEvent;->setWasApiCallSuccessful(ZLjava/lang/Exception;)V

    .line 7
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getAPIEvent()Lcom/microsoft/aad/adal/APIEvent;

    move-result-object v0

    .line 8
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getRequest()Lcom/microsoft/aad/adal/AuthenticationRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getCorrelationId()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/microsoft/aad/adal/DefaultEvent;->setCorrelationId(Ljava/lang/String;)V

    .line 10
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getAPIEvent()Lcom/microsoft/aad/adal/APIEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/aad/adal/APIEvent;->stopTelemetryAndFlush()V

    if-eqz p1, :cond_0

    .line 11
    invoke-virtual {p1, p4}, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;->onError(Lcom/microsoft/aad/adal/AuthenticationException;)V

    goto :goto_0

    .line 12
    :cond_0
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getDelegate()Lcom/microsoft/aad/adal/AuthenticationCallback;

    move-result-object p1

    invoke-interface {p1, p4}, Lcom/microsoft/aad/adal/AuthenticationCallback;->onError(Ljava/lang/Exception;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    if-eqz p4, :cond_1

    .line 13
    iget-object p2, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    invoke-virtual {p2, p3}, Lcom/microsoft/aad/adal/AuthenticationContext;->removeWaitingRequest(I)V

    .line 14
    :cond_1
    throw p1

    :cond_2
    :goto_0
    if-eqz p4, :cond_3

    .line 15
    iget-object p1, p0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    invoke-virtual {p1, p3}, Lcom/microsoft/aad/adal/AuthenticationContext;->removeWaitingRequest(I)V

    :cond_3
    return-void
.end method

.method private waitingRequestOnError(Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->waitingRequestOnError(Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V

    return-void
.end method


# virtual methods
.method acquireToken(Lcom/microsoft/aad/adal/IWindowComponent;ZLcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/AuthenticationCallback;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/aad/adal/IWindowComponent;",
            "Z",
            "Lcom/microsoft/aad/adal/AuthenticationRequest;",
            "Lcom/microsoft/aad/adal/AuthenticationCallback<",
            "Lcom/microsoft/aad/adal/AuthenticationResult;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v3, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->getHandler()Landroid/os/Handler;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-direct {v3, v0, p4}, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;-><init>(Landroid/os/Handler;Lcom/microsoft/aad/adal/AuthenticationCallback;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p3}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getCorrelationId()Ljava/util/UUID;

    .line 11
    .line 12
    .line 13
    move-result-object p4

    .line 14
    invoke-static {p4}, Lcom/microsoft/aad/adal/Logger;->setCorrelationId(Ljava/util/UUID;)V

    .line 15
    .line 16
    .line 17
    new-instance p4, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    sget-object v0, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 23
    .line 24
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    const-string v0, ":acquireToken"

    .line 28
    .line 29
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p4

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v1, "Sending async task from thread:"

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-static {}, Landroid/os/Process;->myTid()I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-static {p4, v0}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    sget-object p4, Lcom/microsoft/aad/adal/AcquireTokenRequest;->THREAD_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    .line 61
    .line 62
    new-instance v6, Lcom/microsoft/aad/adal/AcquireTokenRequest$1;

    .line 63
    .line 64
    move-object v0, v6

    .line 65
    move-object v1, p0

    .line 66
    move-object v2, p3

    .line 67
    move-object v4, p1

    .line 68
    move v5, p2

    .line 69
    invoke-direct/range {v0 .. v5}, Lcom/microsoft/aad/adal/AcquireTokenRequest$1;-><init>(Lcom/microsoft/aad/adal/AcquireTokenRequest;Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;Lcom/microsoft/aad/adal/IWindowComponent;Z)V

    .line 70
    .line 71
    .line 72
    invoke-interface {p4, v6}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method onActivityResult(IILandroid/content/Intent;)V
    .locals 26

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    move/from16 v0, p2

    .line 4
    .line 5
    move-object/from16 v1, p3

    .line 6
    .line 7
    const/16 v2, 0x3e9

    .line 8
    .line 9
    move/from16 v3, p1

    .line 10
    .line 11
    if-ne v3, v2, :cond_e

    .line 12
    .line 13
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->getHandler()Landroid/os/Handler;

    .line 14
    .line 15
    .line 16
    const-string v2, ""

    .line 17
    .line 18
    const-string v3, ":onActivityResult"

    .line 19
    .line 20
    if-eqz v1, :cond_d

    .line 21
    .line 22
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 23
    .line 24
    .line 25
    move-result-object v4

    .line 26
    if-nez v4, :cond_0

    .line 27
    .line 28
    goto/16 :goto_1

    .line 29
    .line 30
    :cond_0
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    const-string v5, "com.microsoft.aad.adal:RequestId"

    .line 35
    .line 36
    invoke-virtual {v4, v5}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 37
    .line 38
    .line 39
    move-result v5

    .line 40
    :try_start_0
    iget-object v7, v6, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 41
    .line 42
    invoke-virtual {v7, v5}, Lcom/microsoft/aad/adal/AuthenticationContext;->getWaitingRequest(I)Lcom/microsoft/aad/adal/AuthenticationRequestState;

    .line 43
    .line 44
    .line 45
    move-result-object v7

    .line 46
    iget-object v8, v6, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 47
    .line 48
    invoke-virtual {v8, v5}, Lcom/microsoft/aad/adal/AuthenticationContext;->removeWaitingRequest(I)V

    .line 49
    .line 50
    .line 51
    new-instance v8, Ljava/lang/StringBuilder;

    .line 52
    .line 53
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .line 55
    .line 56
    sget-object v9, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 57
    .line 58
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v8

    .line 68
    new-instance v10, Ljava/lang/StringBuilder;

    .line 69
    .line 70
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    .line 72
    .line 73
    const-string v11, "Waiting request found. RequestId:"

    .line 74
    .line 75
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v10

    .line 85
    invoke-static {v8, v10}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/aad/adal/AuthenticationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    .line 87
    .line 88
    iget-object v8, v6, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mAuthContext:Lcom/microsoft/aad/adal/AuthenticationContext;

    .line 89
    .line 90
    invoke-virtual {v8, v7}, Lcom/microsoft/aad/adal/AuthenticationContext;->getCorrelationInfoFromWaitingRequest(Lcom/microsoft/aad/adal/AuthenticationRequestState;)Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v8

    .line 94
    const/16 v10, 0x7d4

    .line 95
    .line 96
    const/4 v11, 0x1

    .line 97
    const-string v12, "account.authority"

    .line 98
    .line 99
    const-string v13, "account.userinfo.tenantid"

    .line 100
    .line 101
    const-string v14, "account.name"

    .line 102
    .line 103
    const/4 v15, 0x0

    .line 104
    if-ne v0, v10, :cond_2

    .line 105
    .line 106
    const-string v0, "account.access.token"

    .line 107
    .line 108
    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v17

    .line 112
    invoke-virtual {v1, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    iget-object v2, v6, Lcom/microsoft/aad/adal/AcquireTokenRequest;->mBrokerProxy:Lcom/microsoft/aad/adal/IBrokerProxy;

    .line 117
    .line 118
    invoke-interface {v2, v0}, Lcom/microsoft/aad/adal/IBrokerProxy;->saveAccount(Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    const-string v0, "account.expiredate"

    .line 122
    .line 123
    const-wide/16 v2, 0x0

    .line 124
    .line 125
    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 126
    .line 127
    .line 128
    move-result-wide v2

    .line 129
    new-instance v0, Ljava/util/Date;

    .line 130
    .line 131
    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 132
    .line 133
    .line 134
    const-string v2, "account.idtoken"

    .line 135
    .line 136
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v23

    .line 140
    invoke-virtual {v1, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v22

    .line 144
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 145
    .line 146
    .line 147
    move-result-object v2

    .line 148
    invoke-static {v2}, Lcom/microsoft/aad/adal/UserInfo;->getUserInfoFromBrokerResult(Landroid/os/Bundle;)Lcom/microsoft/aad/adal/UserInfo;

    .line 149
    .line 150
    .line 151
    move-result-object v21

    .line 152
    const-string v2, "cliteleminfo.server_error"

    .line 153
    .line 154
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v2

    .line 158
    const-string v3, "cliteleminfo.server_suberror"

    .line 159
    .line 160
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object v3

    .line 164
    const-string v4, "cliteleminfo.rt_age"

    .line 165
    .line 166
    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 167
    .line 168
    .line 169
    move-result-object v4

    .line 170
    const-string v5, "cliteleminfo.spe_ring"

    .line 171
    .line 172
    invoke-virtual {v1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object v5

    .line 176
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getRequest()Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 177
    .line 178
    .line 179
    move-result-object v8

    .line 180
    if-eqz v8, :cond_1

    .line 181
    .line 182
    invoke-virtual {v8}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getClientId()Ljava/lang/String;

    .line 183
    .line 184
    .line 185
    move-result-object v8

    .line 186
    move-object/from16 v25, v8

    .line 187
    .line 188
    goto :goto_0

    .line 189
    :cond_1
    move-object/from16 v25, v15

    .line 190
    .line 191
    :goto_0
    new-instance v8, Lcom/microsoft/aad/adal/AuthenticationResult;

    .line 192
    .line 193
    const/16 v18, 0x0

    .line 194
    .line 195
    const/16 v20, 0x0

    .line 196
    .line 197
    const/16 v24, 0x0

    .line 198
    .line 199
    move-object/from16 v16, v8

    .line 200
    .line 201
    move-object/from16 v19, v0

    .line 202
    .line 203
    invoke-direct/range {v16 .. v25}, Lcom/microsoft/aad/adal/AuthenticationResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;ZLcom/microsoft/aad/adal/UserInfo;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V

    .line 204
    .line 205
    .line 206
    invoke-virtual {v1, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v0

    .line 210
    invoke-virtual {v8, v0}, Lcom/microsoft/aad/adal/AuthenticationResult;->setAuthority(Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    new-instance v0, Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;

    .line 214
    .line 215
    invoke-direct {v0}, Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;-><init>()V

    .line 216
    .line 217
    .line 218
    invoke-virtual {v0, v2}, Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;->_setServerErrorCode(Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    invoke-virtual {v0, v3}, Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;->_setServerSubErrorCode(Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    invoke-virtual {v0, v4}, Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;->_setRefreshTokenAge(Ljava/lang/String;)V

    .line 225
    .line 226
    .line 227
    invoke-virtual {v0, v5}, Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;->_setSpeRing(Ljava/lang/String;)V

    .line 228
    .line 229
    .line 230
    invoke-virtual {v8, v0}, Lcom/microsoft/aad/adal/AuthenticationResult;->setCliTelemInfo(Lcom/microsoft/aad/adal/TelemetryUtils$CliTelemInfo;)V

    .line 231
    .line 232
    .line 233
    invoke-virtual {v8}, Lcom/microsoft/aad/adal/AuthenticationResult;->getAccessToken()Ljava/lang/String;

    .line 234
    .line 235
    .line 236
    move-result-object v1

    .line 237
    if-eqz v1, :cond_e

    .line 238
    .line 239
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getAPIEvent()Lcom/microsoft/aad/adal/APIEvent;

    .line 240
    .line 241
    .line 242
    move-result-object v1

    .line 243
    invoke-virtual {v1, v11, v15}, Lcom/microsoft/aad/adal/APIEvent;->setWasApiCallSuccessful(ZLjava/lang/Exception;)V

    .line 244
    .line 245
    .line 246
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getAPIEvent()Lcom/microsoft/aad/adal/APIEvent;

    .line 247
    .line 248
    .line 249
    move-result-object v1

    .line 250
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getRequest()Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 251
    .line 252
    .line 253
    move-result-object v2

    .line 254
    invoke-virtual {v2}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getCorrelationId()Ljava/util/UUID;

    .line 255
    .line 256
    .line 257
    move-result-object v2

    .line 258
    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    .line 259
    .line 260
    .line 261
    move-result-object v2

    .line 262
    invoke-virtual {v1, v2}, Lcom/microsoft/aad/adal/DefaultEvent;->setCorrelationId(Ljava/lang/String;)V

    .line 263
    .line 264
    .line 265
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getAPIEvent()Lcom/microsoft/aad/adal/APIEvent;

    .line 266
    .line 267
    .line 268
    move-result-object v1

    .line 269
    invoke-virtual {v8}, Lcom/microsoft/aad/adal/AuthenticationResult;->getIdToken()Ljava/lang/String;

    .line 270
    .line 271
    .line 272
    move-result-object v2

    .line 273
    invoke-virtual {v1, v2}, Lcom/microsoft/aad/adal/APIEvent;->setIdToken(Ljava/lang/String;)V

    .line 274
    .line 275
    .line 276
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getAPIEvent()Lcom/microsoft/aad/adal/APIEvent;

    .line 277
    .line 278
    .line 279
    move-result-object v1

    .line 280
    invoke-virtual {v0}, Lcom/microsoft/identity/common/java/telemetry/CliTelemInfo;->getServerErrorCode()Ljava/lang/String;

    .line 281
    .line 282
    .line 283
    move-result-object v2

    .line 284
    invoke-virtual {v1, v2}, Lcom/microsoft/aad/adal/APIEvent;->setServerErrorCode(Ljava/lang/String;)V

    .line 285
    .line 286
    .line 287
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getAPIEvent()Lcom/microsoft/aad/adal/APIEvent;

    .line 288
    .line 289
    .line 290
    move-result-object v1

    .line 291
    invoke-virtual {v0}, Lcom/microsoft/identity/common/java/telemetry/CliTelemInfo;->getServerSubErrorCode()Ljava/lang/String;

    .line 292
    .line 293
    .line 294
    move-result-object v2

    .line 295
    invoke-virtual {v1, v2}, Lcom/microsoft/aad/adal/APIEvent;->setServerSubErrorCode(Ljava/lang/String;)V

    .line 296
    .line 297
    .line 298
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getAPIEvent()Lcom/microsoft/aad/adal/APIEvent;

    .line 299
    .line 300
    .line 301
    move-result-object v1

    .line 302
    invoke-virtual {v0}, Lcom/microsoft/identity/common/java/telemetry/CliTelemInfo;->getRefreshTokenAge()Ljava/lang/String;

    .line 303
    .line 304
    .line 305
    move-result-object v2

    .line 306
    invoke-virtual {v1, v2}, Lcom/microsoft/aad/adal/APIEvent;->setRefreshTokenAge(Ljava/lang/String;)V

    .line 307
    .line 308
    .line 309
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getAPIEvent()Lcom/microsoft/aad/adal/APIEvent;

    .line 310
    .line 311
    .line 312
    move-result-object v1

    .line 313
    invoke-virtual {v0}, Lcom/microsoft/identity/common/java/telemetry/CliTelemInfo;->getSpeRing()Ljava/lang/String;

    .line 314
    .line 315
    .line 316
    move-result-object v0

    .line 317
    invoke-virtual {v1, v0}, Lcom/microsoft/aad/adal/APIEvent;->setSpeRing(Ljava/lang/String;)V

    .line 318
    .line 319
    .line 320
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getAPIEvent()Lcom/microsoft/aad/adal/APIEvent;

    .line 321
    .line 322
    .line 323
    move-result-object v0

    .line 324
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/APIEvent;->stopTelemetryAndFlush()V

    .line 325
    .line 326
    .line 327
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getDelegate()Lcom/microsoft/aad/adal/AuthenticationCallback;

    .line 328
    .line 329
    .line 330
    move-result-object v0

    .line 331
    invoke-interface {v0, v8}, Lcom/microsoft/aad/adal/AuthenticationCallback;->onSuccess(Ljava/lang/Object;)V

    .line 332
    .line 333
    .line 334
    goto/16 :goto_2

    .line 335
    .line 336
    :cond_2
    const/16 v1, 0x7d1

    .line 337
    .line 338
    const-string v10, " "

    .line 339
    .line 340
    if-ne v0, v1, :cond_3

    .line 341
    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    .line 343
    .line 344
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 345
    .line 346
    .line 347
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    .line 349
    .line 350
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    .line 352
    .line 353
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 354
    .line 355
    .line 356
    move-result-object v0

    .line 357
    new-instance v1, Ljava/lang/StringBuilder;

    .line 358
    .line 359
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 360
    .line 361
    .line 362
    const-string v2, "User cancelled the flow. RequestId:"

    .line 363
    .line 364
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    .line 366
    .line 367
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 368
    .line 369
    .line 370
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    .line 372
    .line 373
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    .line 375
    .line 376
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 377
    .line 378
    .line 379
    move-result-object v1

    .line 380
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    .line 382
    .line 383
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationCancelError;

    .line 384
    .line 385
    new-instance v1, Ljava/lang/StringBuilder;

    .line 386
    .line 387
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 388
    .line 389
    .line 390
    const-string v2, "User cancelled the flow RequestId:"

    .line 391
    .line 392
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    .line 394
    .line 395
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 396
    .line 397
    .line 398
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    .line 400
    .line 401
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 402
    .line 403
    .line 404
    move-result-object v1

    .line 405
    invoke-direct {v0, v1}, Lcom/microsoft/aad/adal/AuthenticationCancelError;-><init>(Ljava/lang/String;)V

    .line 406
    .line 407
    .line 408
    invoke-direct {v6, v7, v5, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->waitingRequestOnError(Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V

    .line 409
    .line 410
    .line 411
    goto/16 :goto_2

    .line 412
    .line 413
    :cond_3
    const/16 v1, 0x7d6

    .line 414
    .line 415
    if-ne v0, v1, :cond_4

    .line 416
    .line 417
    new-instance v0, Ljava/lang/StringBuilder;

    .line 418
    .line 419
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 420
    .line 421
    .line 422
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    .line 424
    .line 425
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    .line 427
    .line 428
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 429
    .line 430
    .line 431
    move-result-object v0

    .line 432
    const-string v1, "Device needs to have broker installed, we expect the apps to call usback when the broker is installed"

    .line 433
    .line 434
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    .line 436
    .line 437
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 438
    .line 439
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->BROKER_APP_INSTALLATION_STARTED:Lcom/microsoft/aad/adal/ADALError;

    .line 440
    .line 441
    invoke-direct {v0, v1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;)V

    .line 442
    .line 443
    .line 444
    invoke-direct {v6, v7, v5, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->waitingRequestOnError(Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V

    .line 445
    .line 446
    .line 447
    goto/16 :goto_2

    .line 448
    .line 449
    :cond_4
    const/16 v1, 0x7d9

    .line 450
    .line 451
    const-string v11, "Device needs to be managed, we expect the apps to call usback when the device is managed"

    .line 452
    .line 453
    if-ne v0, v1, :cond_5

    .line 454
    .line 455
    new-instance v0, Ljava/lang/StringBuilder;

    .line 456
    .line 457
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 458
    .line 459
    .line 460
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 461
    .line 462
    .line 463
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 464
    .line 465
    .line 466
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 467
    .line 468
    .line 469
    move-result-object v0

    .line 470
    invoke-static {v0, v11}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    .line 472
    .line 473
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 474
    .line 475
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->MDM_REQUIRED:Lcom/microsoft/aad/adal/ADALError;

    .line 476
    .line 477
    invoke-direct {v0, v1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;)V

    .line 478
    .line 479
    .line 480
    invoke-direct {v6, v7, v5, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->waitingRequestOnError(Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V

    .line 481
    .line 482
    .line 483
    goto/16 :goto_2

    .line 484
    .line 485
    :cond_5
    const/16 v1, 0x7d5

    .line 486
    .line 487
    if-ne v0, v1, :cond_7

    .line 488
    .line 489
    const-string v0, "com.microsoft.aad.adal:AuthenticationException"

    .line 490
    .line 491
    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    .line 492
    .line 493
    .line 494
    move-result-object v0

    .line 495
    if-eqz v0, :cond_6

    .line 496
    .line 497
    instance-of v1, v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 498
    .line 499
    if-eqz v1, :cond_6

    .line 500
    .line 501
    check-cast v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 502
    .line 503
    new-instance v1, Ljava/lang/StringBuilder;

    .line 504
    .line 505
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 506
    .line 507
    .line 508
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    .line 510
    .line 511
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    .line 513
    .line 514
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 515
    .line 516
    .line 517
    move-result-object v1

    .line 518
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationException;->getMessage()Ljava/lang/String;

    .line 519
    .line 520
    .line 521
    move-result-object v2

    .line 522
    sget-object v3, Lcom/microsoft/aad/adal/ADALError;->WEBVIEW_RETURNED_AUTHENTICATION_EXCEPTION:Lcom/microsoft/aad/adal/ADALError;

    .line 523
    .line 524
    const-string v4, "Webview returned exception."

    .line 525
    .line 526
    invoke-static {v1, v4, v2, v3}, Lcom/microsoft/aad/adal/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 527
    .line 528
    .line 529
    invoke-direct {v6, v7, v5, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->waitingRequestOnError(Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V

    .line 530
    .line 531
    .line 532
    goto/16 :goto_2

    .line 533
    .line 534
    :cond_6
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 535
    .line 536
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->WEBVIEW_RETURNED_INVALID_AUTHENTICATION_EXCEPTION:Lcom/microsoft/aad/adal/ADALError;

    .line 537
    .line 538
    invoke-direct {v0, v1, v8}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 539
    .line 540
    .line 541
    invoke-direct {v6, v7, v5, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->waitingRequestOnError(Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V

    .line 542
    .line 543
    .line 544
    goto/16 :goto_2

    .line 545
    .line 546
    :cond_7
    const/16 v1, 0x7d2

    .line 547
    .line 548
    if-ne v0, v1, :cond_a

    .line 549
    .line 550
    const-string v0, "com.microsoft.aad.adal:BrowserErrorCode"

    .line 551
    .line 552
    invoke-virtual {v4, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 553
    .line 554
    .line 555
    move-result-object v0

    .line 556
    const-string v1, "com.microsoft.aad.adal:BrowserErrorMessage"

    .line 557
    .line 558
    invoke-virtual {v4, v1}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 559
    .line 560
    .line 561
    move-result-object v1

    .line 562
    new-instance v2, Ljava/lang/StringBuilder;

    .line 563
    .line 564
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 565
    .line 566
    .line 567
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 568
    .line 569
    .line 570
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 571
    .line 572
    .line 573
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 574
    .line 575
    .line 576
    move-result-object v2

    .line 577
    new-instance v15, Ljava/lang/StringBuilder;

    .line 578
    .line 579
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 580
    .line 581
    .line 582
    move-object/from16 p3, v11

    .line 583
    .line 584
    const-string v11, "Error info:"

    .line 585
    .line 586
    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 587
    .line 588
    .line 589
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 590
    .line 591
    .line 592
    const-string v11, " for requestId: "

    .line 593
    .line 594
    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 595
    .line 596
    .line 597
    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 598
    .line 599
    .line 600
    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 601
    .line 602
    .line 603
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 604
    .line 605
    .line 606
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 607
    .line 608
    .line 609
    move-result-object v10

    .line 610
    const/4 v11, 0x0

    .line 611
    invoke-static {v2, v10, v1, v11}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 612
    .line 613
    .line 614
    const/4 v2, 0x3

    .line 615
    new-array v2, v2, [Ljava/lang/Object;

    .line 616
    .line 617
    const/4 v10, 0x0

    .line 618
    aput-object v0, v2, v10

    .line 619
    .line 620
    const/4 v10, 0x1

    .line 621
    aput-object v1, v2, v10

    .line 622
    .line 623
    const/4 v1, 0x2

    .line 624
    aput-object v8, v2, v1

    .line 625
    .line 626
    const-string v1, "%s %s %s"

    .line 627
    .line 628
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 629
    .line 630
    .line 631
    move-result-object v1

    .line 632
    invoke-static {v0}, Lcom/microsoft/identity/common/adal/internal/util/StringExtensions;->oO80(Ljava/lang/String;)Z

    .line 633
    .line 634
    .line 635
    move-result v2

    .line 636
    if-nez v2, :cond_8

    .line 637
    .line 638
    sget-object v2, Lcom/microsoft/aad/adal/ADALError;->AUTH_FAILED_INTUNE_POLICY_REQUIRED:Lcom/microsoft/aad/adal/ADALError;

    .line 639
    .line 640
    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 641
    .line 642
    .line 643
    move-result-object v2

    .line 644
    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    .line 645
    .line 646
    .line 647
    move-result v2

    .line 648
    if-nez v2, :cond_8

    .line 649
    .line 650
    invoke-virtual {v4, v14}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 651
    .line 652
    .line 653
    move-result-object v17

    .line 654
    const-string v0, "account.userinfo.userid"

    .line 655
    .line 656
    invoke-virtual {v4, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 657
    .line 658
    .line 659
    move-result-object v18

    .line 660
    invoke-virtual {v4, v13}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 661
    .line 662
    .line 663
    move-result-object v19

    .line 664
    invoke-virtual {v4, v12}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 665
    .line 666
    .line 667
    move-result-object v20

    .line 668
    new-instance v0, Lcom/microsoft/aad/adal/IntuneAppProtectionPolicyRequiredException;

    .line 669
    .line 670
    move-object v15, v0

    .line 671
    move-object/from16 v16, v1

    .line 672
    .line 673
    invoke-direct/range {v15 .. v20}, Lcom/microsoft/aad/adal/IntuneAppProtectionPolicyRequiredException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    .line 675
    .line 676
    invoke-direct {v6, v7, v5, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->waitingRequestOnError(Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V

    .line 677
    .line 678
    .line 679
    goto/16 :goto_2

    .line 680
    .line 681
    :cond_8
    const-string v2, "device_needs_to_be_managed"

    .line 682
    .line 683
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 684
    .line 685
    .line 686
    move-result v0

    .line 687
    if-eqz v0, :cond_9

    .line 688
    .line 689
    new-instance v0, Ljava/lang/StringBuilder;

    .line 690
    .line 691
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 692
    .line 693
    .line 694
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    .line 696
    .line 697
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    .line 699
    .line 700
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 701
    .line 702
    .line 703
    move-result-object v0

    .line 704
    move-object/from16 v1, p3

    .line 705
    .line 706
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    .line 708
    .line 709
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 710
    .line 711
    sget-object v1, Lcom/microsoft/aad/adal/ADALError;->MDM_REQUIRED:Lcom/microsoft/aad/adal/ADALError;

    .line 712
    .line 713
    invoke-direct {v0, v1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;)V

    .line 714
    .line 715
    .line 716
    invoke-direct {v6, v7, v5, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->waitingRequestOnError(Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V

    .line 717
    .line 718
    .line 719
    goto/16 :goto_2

    .line 720
    .line 721
    :cond_9
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 722
    .line 723
    sget-object v2, Lcom/microsoft/aad/adal/ADALError;->SERVER_INVALID_REQUEST:Lcom/microsoft/aad/adal/ADALError;

    .line 724
    .line 725
    invoke-direct {v0, v2, v1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 726
    .line 727
    .line 728
    invoke-direct {v6, v7, v5, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->waitingRequestOnError(Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V

    .line 729
    .line 730
    .line 731
    goto/16 :goto_2

    .line 732
    .line 733
    :cond_a
    const/16 v1, 0x7d3

    .line 734
    .line 735
    if-ne v0, v1, :cond_e

    .line 736
    .line 737
    const-string v0, "com.microsoft.aad.adal:BrowserRequestInfo"

    .line 738
    .line 739
    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    .line 740
    .line 741
    .line 742
    move-result-object v0

    .line 743
    check-cast v0, Lcom/microsoft/aad/adal/AuthenticationRequest;

    .line 744
    .line 745
    const-string v1, "com.microsoft.aad.adal:BrowserFinalUrl"

    .line 746
    .line 747
    invoke-virtual {v4, v1, v2}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 748
    .line 749
    .line 750
    move-result-object v4

    .line 751
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    .line 752
    .line 753
    .line 754
    move-result v1

    .line 755
    if-eqz v1, :cond_c

    .line 756
    .line 757
    new-instance v1, Ljava/lang/StringBuilder;

    .line 758
    .line 759
    const-string v4, "Webview did not reach the redirectUrl. "

    .line 760
    .line 761
    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 762
    .line 763
    .line 764
    if-eqz v0, :cond_b

    .line 765
    .line 766
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getLogInfo()Ljava/lang/String;

    .line 767
    .line 768
    .line 769
    move-result-object v0

    .line 770
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 771
    .line 772
    .line 773
    :cond_b
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 774
    .line 775
    .line 776
    new-instance v0, Lcom/microsoft/aad/adal/AuthenticationException;

    .line 777
    .line 778
    sget-object v4, Lcom/microsoft/aad/adal/ADALError;->WEBVIEW_RETURNED_EMPTY_REDIRECT_URL:Lcom/microsoft/aad/adal/ADALError;

    .line 779
    .line 780
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 781
    .line 782
    .line 783
    move-result-object v1

    .line 784
    invoke-direct {v0, v4, v1}, Lcom/microsoft/aad/adal/AuthenticationException;-><init>(Lcom/microsoft/aad/adal/ADALError;Ljava/lang/String;)V

    .line 785
    .line 786
    .line 787
    new-instance v1, Ljava/lang/StringBuilder;

    .line 788
    .line 789
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 790
    .line 791
    .line 792
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793
    .line 794
    .line 795
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796
    .line 797
    .line 798
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 799
    .line 800
    .line 801
    move-result-object v1

    .line 802
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationException;->getMessage()Ljava/lang/String;

    .line 803
    .line 804
    .line 805
    move-result-object v3

    .line 806
    invoke-virtual {v0}, Lcom/microsoft/aad/adal/AuthenticationException;->getCode()Lcom/microsoft/aad/adal/ADALError;

    .line 807
    .line 808
    .line 809
    move-result-object v4

    .line 810
    invoke-static {v1, v2, v3, v4}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 811
    .line 812
    .line 813
    invoke-direct {v6, v7, v5, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->waitingRequestOnError(Lcom/microsoft/aad/adal/AuthenticationRequestState;ILcom/microsoft/aad/adal/AuthenticationException;)V

    .line 814
    .line 815
    .line 816
    goto :goto_2

    .line 817
    :cond_c
    new-instance v8, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;

    .line 818
    .line 819
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->getHandler()Landroid/os/Handler;

    .line 820
    .line 821
    .line 822
    move-result-object v0

    .line 823
    invoke-virtual {v7}, Lcom/microsoft/aad/adal/AuthenticationRequestState;->getDelegate()Lcom/microsoft/aad/adal/AuthenticationCallback;

    .line 824
    .line 825
    .line 826
    move-result-object v1

    .line 827
    invoke-direct {v8, v0, v1}, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;-><init>(Landroid/os/Handler;Lcom/microsoft/aad/adal/AuthenticationCallback;)V

    .line 828
    .line 829
    .line 830
    sget-object v9, Lcom/microsoft/aad/adal/AcquireTokenRequest;->THREAD_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    .line 831
    .line 832
    new-instance v10, Lcom/microsoft/aad/adal/AcquireTokenRequest$3;

    .line 833
    .line 834
    move-object v0, v10

    .line 835
    move-object/from16 v1, p0

    .line 836
    .line 837
    move-object v2, v7

    .line 838
    move-object v3, v4

    .line 839
    move-object v4, v8

    .line 840
    invoke-direct/range {v0 .. v5}, Lcom/microsoft/aad/adal/AcquireTokenRequest$3;-><init>(Lcom/microsoft/aad/adal/AcquireTokenRequest;Lcom/microsoft/aad/adal/AuthenticationRequestState;Ljava/lang/String;Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;I)V

    .line 841
    .line 842
    .line 843
    invoke-interface {v9, v10}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 844
    .line 845
    .line 846
    goto :goto_2

    .line 847
    :catch_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 848
    .line 849
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 850
    .line 851
    .line 852
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 853
    .line 854
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 855
    .line 856
    .line 857
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 858
    .line 859
    .line 860
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 861
    .line 862
    .line 863
    move-result-object v0

    .line 864
    new-instance v1, Ljava/lang/StringBuilder;

    .line 865
    .line 866
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 867
    .line 868
    .line 869
    const-string v3, "Failed to find waiting request. RequestId:"

    .line 870
    .line 871
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 872
    .line 873
    .line 874
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 875
    .line 876
    .line 877
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 878
    .line 879
    .line 880
    move-result-object v1

    .line 881
    sget-object v3, Lcom/microsoft/aad/adal/ADALError;->ON_ACTIVITY_RESULT_INTENT_NULL:Lcom/microsoft/aad/adal/ADALError;

    .line 882
    .line 883
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 884
    .line 885
    .line 886
    return-void

    .line 887
    :cond_d
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 888
    .line 889
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 890
    .line 891
    .line 892
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 893
    .line 894
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 895
    .line 896
    .line 897
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 898
    .line 899
    .line 900
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 901
    .line 902
    .line 903
    move-result-object v0

    .line 904
    const-string v1, "BROWSER_FLOW data is null."

    .line 905
    .line 906
    sget-object v3, Lcom/microsoft/aad/adal/ADALError;->ON_ACTIVITY_RESULT_INTENT_NULL:Lcom/microsoft/aad/adal/ADALError;

    .line 907
    .line 908
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/aad/adal/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/aad/adal/ADALError;)V

    .line 909
    .line 910
    .line 911
    :cond_e
    :goto_2
    return-void
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
.end method

.method refreshTokenWithoutCache(Ljava/lang/String;Lcom/microsoft/aad/adal/AuthenticationRequest;Lcom/microsoft/aad/adal/AuthenticationCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/aad/adal/AuthenticationRequest;",
            "Lcom/microsoft/aad/adal/AuthenticationCallback<",
            "Lcom/microsoft/aad/adal/AuthenticationResult;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Lcom/microsoft/aad/adal/AuthenticationRequest;->getCorrelationId()Ljava/util/UUID;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/microsoft/aad/adal/Logger;->setCorrelationId(Ljava/util/UUID;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    sget-object v1, Lcom/microsoft/aad/adal/AcquireTokenRequest;->TAG:Ljava/lang/String;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v1, ":refreshTokenWithoutCache"

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v1, "Refresh token without cache"

    .line 28
    .line 29
    invoke-static {v0, v1}, Lcom/microsoft/aad/adal/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    new-instance v0, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;

    .line 33
    .line 34
    invoke-direct {p0}, Lcom/microsoft/aad/adal/AcquireTokenRequest;->getHandler()Landroid/os/Handler;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-direct {v0, v1, p3}, Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;-><init>(Landroid/os/Handler;Lcom/microsoft/aad/adal/AuthenticationCallback;)V

    .line 39
    .line 40
    .line 41
    sget-object p3, Lcom/microsoft/aad/adal/AcquireTokenRequest;->THREAD_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    .line 42
    .line 43
    new-instance v1, Lcom/microsoft/aad/adal/AcquireTokenRequest$2;

    .line 44
    .line 45
    invoke-direct {v1, p0, p2, p1, v0}, Lcom/microsoft/aad/adal/AcquireTokenRequest$2;-><init>(Lcom/microsoft/aad/adal/AcquireTokenRequest;Lcom/microsoft/aad/adal/AuthenticationRequest;Ljava/lang/String;Lcom/microsoft/aad/adal/AcquireTokenRequest$CallbackHandler;)V

    .line 46
    .line 47
    .line 48
    invoke-interface {p3, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.class public Lcom/microsoft/identity/common/java/telemetry/Telemetry;
.super Ljava/lang/Object;
.source "Telemetry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/identity/common/java/telemetry/Telemetry$Builder;
    }
.end annotation


# static fields
.field private static volatile O8:Lcom/microsoft/identity/common/java/telemetry/Telemetry; = null

.field private static final 〇o〇:Ljava/lang/String; = "Telemetry"


# instance fields
.field private 〇080:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method protected constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->〇o00〇〇Oo:Z

    return-void
.end method

.method private constructor <init>(Lcom/microsoft/identity/common/java/telemetry/Telemetry$Builder;)V
    .locals 1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 4
    invoke-static {p1}, Lcom/microsoft/identity/common/java/telemetry/Telemetry$Builder;->〇080(Lcom/microsoft/identity/common/java/telemetry/Telemetry$Builder;)Lcom/microsoft/identity/common/java/telemetry/AbstractTelemetryContext;

    .line 5
    :cond_0
    sget-object p1, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->〇o〇:Ljava/lang/String;

    const-string v0, "Telemetry is disabled because the Telemetry context or configuration is null"

    invoke-static {p1, v0}, Lcom/microsoft/identity/common/java/logging/Logger;->〇00(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 6
    iput-boolean p1, p0, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->〇o00〇〇Oo:Z

    return-void
.end method

.method private declared-synchronized O8()Ljava/util/Queue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Queue<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->〇080:Ljava/util/Queue;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    .line 4
    monitor-exit p0

    .line 5
    return-object v0

    .line 6
    :catchall_0
    move-exception v0

    .line 7
    monitor-exit p0

    .line 8
    throw v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private static declared-synchronized Oo08(Lcom/microsoft/identity/common/java/telemetry/Telemetry$Builder;)Lcom/microsoft/identity/common/java/telemetry/Telemetry;
    .locals 2

    .line 1
    const-class v0, Lcom/microsoft/identity/common/java/telemetry/Telemetry;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    sget-object v1, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->O8:Lcom/microsoft/identity/common/java/telemetry/Telemetry;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    sget-object v1, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->O8:Lcom/microsoft/identity/common/java/telemetry/Telemetry;

    .line 9
    .line 10
    iget-boolean v1, v1, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->〇o00〇〇Oo:Z

    .line 11
    .line 12
    if-nez v1, :cond_1

    .line 13
    .line 14
    :cond_0
    new-instance v1, Lcom/microsoft/identity/common/java/telemetry/Telemetry;

    .line 15
    .line 16
    invoke-direct {v1, p0}, Lcom/microsoft/identity/common/java/telemetry/Telemetry;-><init>(Lcom/microsoft/identity/common/java/telemetry/Telemetry$Builder;)V

    .line 17
    .line 18
    .line 19
    sput-object v1, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->O8:Lcom/microsoft/identity/common/java/telemetry/Telemetry;

    .line 20
    .line 21
    :cond_1
    sget-object p0, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->O8:Lcom/microsoft/identity/common/java/telemetry/Telemetry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    .line 23
    monitor-exit v0

    .line 24
    return-object p0

    .line 25
    :catchall_0
    move-exception p0

    .line 26
    monitor-exit v0

    .line 27
    throw p0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method static synthetic 〇080(Lcom/microsoft/identity/common/java/telemetry/Telemetry$Builder;)Lcom/microsoft/identity/common/java/telemetry/Telemetry;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->Oo08(Lcom/microsoft/identity/common/java/telemetry/Telemetry$Builder;)Lcom/microsoft/identity/common/java/telemetry/Telemetry;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static 〇o00〇〇Oo(Lcom/microsoft/identity/common/java/telemetry/events/BaseEvent;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->〇o〇()Lcom/microsoft/identity/common/java/telemetry/Telemetry;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-boolean v0, v0, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->〇o00〇〇Oo:Z

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-static {}, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->〇o〇()Lcom/microsoft/identity/common/java/telemetry/Telemetry;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-direct {v0}, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->O8()Ljava/util/Queue;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {p0}, Lcom/microsoft/identity/common/java/telemetry/Properties;->〇080()Ljava/util/concurrent/ConcurrentHashMap;

    .line 18
    .line 19
    .line 20
    move-result-object p0

    .line 21
    invoke-interface {v0, p0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
.end method

.method public static declared-synchronized 〇o〇()Lcom/microsoft/identity/common/java/telemetry/Telemetry;
    .locals 2

    .line 1
    const-class v0, Lcom/microsoft/identity/common/java/telemetry/Telemetry;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    sget-object v1, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->O8:Lcom/microsoft/identity/common/java/telemetry/Telemetry;

    .line 5
    .line 6
    if-nez v1, :cond_0

    .line 7
    .line 8
    new-instance v1, Lcom/microsoft/identity/common/java/telemetry/Telemetry$Builder;

    .line 9
    .line 10
    invoke-direct {v1}, Lcom/microsoft/identity/common/java/telemetry/Telemetry$Builder;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/microsoft/identity/common/java/telemetry/Telemetry$Builder;->〇o00〇〇Oo()Lcom/microsoft/identity/common/java/telemetry/Telemetry;

    .line 14
    .line 15
    .line 16
    :cond_0
    sget-object v1, Lcom/microsoft/identity/common/java/telemetry/Telemetry;->O8:Lcom/microsoft/identity/common/java/telemetry/Telemetry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17
    .line 18
    monitor-exit v0

    .line 19
    return-object v1

    .line 20
    :catchall_0
    move-exception v1

    .line 21
    monitor-exit v0

    .line 22
    throw v1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

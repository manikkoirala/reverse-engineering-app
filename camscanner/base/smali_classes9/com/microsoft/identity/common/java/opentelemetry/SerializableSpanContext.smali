.class public Lcom/microsoft/identity/common/java/opentelemetry/SerializableSpanContext;
.super Ljava/lang/Object;
.source "SerializableSpanContext.java"

# interfaces
.implements Lio/opentelemetry/api/trace/SpanContext;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/identity/common/java/opentelemetry/SerializableSpanContext$SerializableSpanContextBuilder;
    }
.end annotation


# instance fields
.field private final mSpanId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "span_id"
    .end annotation

    .annotation build Llombok/NonNull;
    .end annotation
.end field

.field private final mTraceFlags:B
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "trace_flags"
    .end annotation
.end field

.field private final mTraceId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "trace_id"
    .end annotation

    .annotation build Llombok/NonNull;
    .end annotation
.end field


# direct methods
.method public static builder()Lcom/microsoft/identity/common/java/opentelemetry/SerializableSpanContext$SerializableSpanContextBuilder;
    .locals 1

    .line 1
    new-instance v0, Lcom/microsoft/identity/common/java/opentelemetry/SerializableSpanContext$SerializableSpanContextBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/microsoft/identity/common/java/opentelemetry/SerializableSpanContext$SerializableSpanContextBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public getSpanId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/identity/common/java/opentelemetry/SerializableSpanContext;->mSpanId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public synthetic getSpanIdBytes()[B
    .locals 1

    .line 1
    invoke-static {p0}, Lio/opentelemetry/api/trace/〇〇888;->〇080(Lio/opentelemetry/api/trace/SpanContext;)[B

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getTraceFlags()Lio/opentelemetry/api/trace/TraceFlags;
    .locals 1

    .line 1
    iget-byte v0, p0, Lcom/microsoft/identity/common/java/opentelemetry/SerializableSpanContext;->mTraceFlags:B

    .line 2
    .line 3
    invoke-static {v0}, Lio/opentelemetry/api/trace/oO80;->〇080(B)Lio/opentelemetry/api/trace/TraceFlags;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getTraceId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/identity/common/java/opentelemetry/SerializableSpanContext;->mTraceId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public synthetic getTraceIdBytes()[B
    .locals 1

    .line 1
    invoke-static {p0}, Lio/opentelemetry/api/trace/〇〇888;->〇o00〇〇Oo(Lio/opentelemetry/api/trace/SpanContext;)[B

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getTraceState()Lio/opentelemetry/api/trace/TraceState;
    .locals 1

    .line 1
    invoke-static {}, Lio/opentelemetry/api/trace/〇80〇808〇O;->〇o00〇〇Oo()Lio/opentelemetry/api/trace/TraceState;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public isRemote()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public synthetic isSampled()Z
    .locals 1

    .line 1
    invoke-static {p0}, Lio/opentelemetry/api/trace/〇〇888;->〇o〇(Lio/opentelemetry/api/trace/SpanContext;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public synthetic isValid()Z
    .locals 1

    .line 1
    invoke-static {p0}, Lio/opentelemetry/api/trace/〇〇888;->O8(Lio/opentelemetry/api/trace/SpanContext;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

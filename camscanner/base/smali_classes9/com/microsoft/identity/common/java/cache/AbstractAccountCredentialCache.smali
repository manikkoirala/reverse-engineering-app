.class public abstract Lcom/microsoft/identity/common/java/cache/AbstractAccountCredentialCache;
.super Ljava/lang/Object;
.source "AbstractAccountCredentialCache.java"

# interfaces
.implements Lcom/microsoft/identity/common/java/cache/IAccountCredentialCache;


# static fields
.field private static final 〇080:Ljava/lang/String; = "AbstractAccountCredentialCache"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method static oO80(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 7
    .param p0    # Ljava/lang/String;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param

    .line 1
    if-eqz p0, :cond_4

    .line 2
    .line 3
    if-eqz p1, :cond_3

    .line 4
    .line 5
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    const-string v0, "\\s+"

    .line 10
    .line 11
    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    new-instance v0, Ljava/util/HashSet;

    .line 24
    .line 25
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 26
    .line 27
    .line 28
    new-instance v1, Ljava/util/HashSet;

    .line 29
    .line 30
    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 31
    .line 32
    .line 33
    array-length v2, p0

    .line 34
    const/4 v3, 0x0

    .line 35
    const/4 v4, 0x0

    .line 36
    :goto_0
    if-ge v4, v2, :cond_0

    .line 37
    .line 38
    aget-object v5, p0, v4

    .line 39
    .line 40
    sget-object v6, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    .line 41
    .line 42
    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v5

    .line 46
    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    .line 48
    .line 49
    add-int/lit8 v4, v4, 0x1

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    array-length p0, p1

    .line 53
    :goto_1
    if-ge v3, p0, :cond_1

    .line 54
    .line 55
    aget-object v2, p1, v3

    .line 56
    .line 57
    sget-object v4, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    .line 58
    .line 59
    invoke-virtual {v2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    add-int/lit8 v3, v3, 0x1

    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_1
    if-eqz p2, :cond_2

    .line 70
    .line 71
    sget-object p0, Lcom/microsoft/identity/common/java/AuthenticationConstants;->O8:Ljava/util/Set;

    .line 72
    .line 73
    invoke-interface {v0, p0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 74
    .line 75
    .line 76
    invoke-interface {v1, p0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 77
    .line 78
    .line 79
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    .line 80
    .line 81
    .line 82
    move-result p0

    .line 83
    return p0

    .line 84
    :cond_3
    new-instance p0, Ljava/lang/NullPointerException;

    .line 85
    .line 86
    const-string p1, "credentialTarget is marked non-null but is null"

    .line 87
    .line 88
    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    throw p0

    .line 92
    :cond_4
    new-instance p0, Ljava/lang/NullPointerException;

    .line 93
    .line 94
    const-string/jumbo p1, "targetToMatch is marked non-null but is null"

    .line 95
    .line 96
    .line 97
    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    throw p0
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method protected o〇0(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/identity/common/java/dto/CredentialType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 19
    .param p1    # Ljava/util/List;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/microsoft/identity/common/java/dto/CredentialType;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .param p12    # Ljava/lang/String;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/microsoft/identity/common/java/dto/Credential;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/identity/common/java/dto/CredentialType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/microsoft/identity/common/java/dto/Credential;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p4

    move-object/from16 v1, p8

    move-object/from16 v2, p9

    move-object/from16 v3, p10

    if-eqz p1, :cond_2d

    .line 1
    invoke-static/range {p3 .. p3}, Lcom/microsoft/identity/common/java/util/StringUtil;->oO80(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    xor-int/2addr v4, v5

    .line 2
    invoke-static/range {p2 .. p2}, Lcom/microsoft/identity/common/java/util/StringUtil;->oO80(Ljava/lang/String;)Z

    move-result v6

    xor-int/2addr v6, v5

    .line 3
    invoke-static/range {p8 .. p8}, Lcom/microsoft/identity/common/java/util/StringUtil;->oO80(Ljava/lang/String;)Z

    move-result v7

    xor-int/2addr v7, v5

    .line 4
    invoke-static/range {p9 .. p9}, Lcom/microsoft/identity/common/java/util/StringUtil;->oO80(Ljava/lang/String;)Z

    move-result v8

    xor-int/2addr v8, v5

    .line 5
    invoke-static/range {p5 .. p5}, Lcom/microsoft/identity/common/java/util/StringUtil;->oO80(Ljava/lang/String;)Z

    move-result v9

    xor-int/2addr v9, v5

    .line 6
    invoke-static/range {p6 .. p6}, Lcom/microsoft/identity/common/java/util/StringUtil;->oO80(Ljava/lang/String;)Z

    move-result v10

    xor-int/2addr v10, v5

    .line 7
    invoke-static/range {p7 .. p7}, Lcom/microsoft/identity/common/java/util/StringUtil;->oO80(Ljava/lang/String;)Z

    move-result v11

    xor-int/2addr v11, v5

    if-eqz v0, :cond_0

    const/4 v13, 0x1

    goto :goto_0

    :cond_0
    const/4 v13, 0x0

    :goto_0
    if-eqz v13, :cond_1

    .line 8
    invoke-static/range {p10 .. p10}, Lcom/microsoft/identity/common/java/util/StringUtil;->oO80(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_1

    sget-object v14, Lcom/microsoft/identity/common/java/dto/CredentialType;->AccessToken_With_AuthScheme:Lcom/microsoft/identity/common/java/dto/CredentialType;

    if-ne v0, v14, :cond_1

    const/4 v14, 0x1

    goto :goto_1

    :cond_1
    const/4 v14, 0x0

    .line 9
    :goto_1
    invoke-static/range {p12 .. p12}, Lcom/microsoft/identity/common/java/util/StringUtil;->oO80(Ljava/lang/String;)Z

    move-result v15

    xor-int/2addr v15, v5

    .line 10
    invoke-static/range {p11 .. p11}, Lcom/microsoft/identity/common/java/util/StringUtil;->oO80(Ljava/lang/String;)Z

    move-result v16

    xor-int/lit8 v12, v16, 0x1

    .line 11
    sget-object v5, Lcom/microsoft/identity/common/java/cache/AbstractAccountCredentialCache;->〇080:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v17, v15

    const-string v15, "Credential lookup filtered by home_account_id? ["

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v15, "]"

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "Credential lookup filtered by realm? ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "Credential lookup filtered by target? ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "Credential lookup filtered by clientId? ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "Credential lookup filtered by applicationIdentifier? ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "Credential lookup filtered by mamEnrollmentIdentifier? ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "Credential lookup filtered by credential type? ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "Credential lookup filtered by auth scheme? ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "Credential lookup filtered by requested claims? ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/microsoft/identity/common/java/logging/Logger;->〇O888o0o(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 13
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/identity/common/java/dto/Credential;

    if-eqz v6, :cond_2

    .line 14
    invoke-virtual {v3}, Lcom/microsoft/identity/common/java/dto/Credential;->〇080()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v15, p2

    invoke-static {v15, v5}, Lcom/microsoft/identity/common/java/util/StringUtil;->Oo08(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    goto :goto_3

    :cond_2
    move-object/from16 v15, p2

    const/4 v5, 0x1

    :goto_3
    if-eqz v4, :cond_5

    if-eqz v5, :cond_3

    .line 15
    invoke-virtual {v3}, Lcom/microsoft/identity/common/java/dto/Credential;->Oo08()Ljava/lang/String;

    move-result-object v5

    move-object/from16 p1, v2

    move-object/from16 v2, p3

    invoke-static {v2, v5}, Lcom/microsoft/identity/common/java/util/StringUtil;->Oo08(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    goto :goto_4

    :cond_3
    move-object/from16 p1, v2

    move-object/from16 v2, p3

    :cond_4
    const/4 v5, 0x0

    goto :goto_4

    :cond_5
    move-object/from16 p1, v2

    move-object/from16 v2, p3

    :goto_4
    if-eqz v13, :cond_7

    if-eqz v5, :cond_6

    .line 16
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/microsoft/identity/common/java/dto/Credential;->〇〇8O0〇8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/microsoft/identity/common/java/util/StringUtil;->Oo08(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    :goto_5
    move v5, v2

    :cond_7
    if-eqz v9, :cond_a

    if-eqz v5, :cond_8

    .line 17
    invoke-virtual {v3}, Lcom/microsoft/identity/common/java/dto/Credential;->〇O00()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v5, p5

    invoke-static {v5, v2}, Lcom/microsoft/identity/common/java/util/StringUtil;->Oo08(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    goto :goto_6

    :cond_8
    move-object/from16 v5, p5

    :cond_9
    const/4 v2, 0x0

    :goto_6
    move v5, v2

    :cond_a
    if-eqz v10, :cond_d

    .line 18
    instance-of v2, v3, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v2, :cond_e

    .line 19
    move-object v2, v3

    check-cast v2, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v5, :cond_b

    .line 20
    invoke-virtual {v2}, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;->O8ooOoo〇()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v5, p6

    invoke-static {v5, v2}, Lcom/microsoft/identity/common/java/util/StringUtil;->Oo08(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    goto :goto_7

    :cond_b
    move-object/from16 v5, p6

    :cond_c
    const/4 v2, 0x0

    :goto_7
    move v5, v2

    :cond_d
    move/from16 v18, v4

    goto :goto_8

    .line 21
    :cond_e
    sget-object v2, Lcom/microsoft/identity/common/java/cache/AbstractAccountCredentialCache;->〇080:Ljava/lang/String;

    move/from16 v18, v4

    const-string v4, "Query specified applicationIdentifier match, but credential type does not have application identifier"

    invoke-static {v2, v4}, Lcom/microsoft/identity/common/java/logging/Logger;->〇O888o0o(Ljava/lang/String;Ljava/lang/String;)V

    :goto_8
    if-eqz v11, :cond_12

    .line 22
    instance-of v2, v3, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v2, :cond_11

    .line 23
    move-object v2, v3

    check-cast v2, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v5, :cond_f

    .line 24
    invoke-virtual {v2}, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;->o〇〇0〇()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v4, p7

    invoke-static {v4, v2}, Lcom/microsoft/identity/common/java/util/StringUtil;->Oo08(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const/4 v2, 0x1

    goto :goto_9

    :cond_f
    move-object/from16 v4, p7

    :cond_10
    const/4 v2, 0x0

    :goto_9
    move v5, v2

    goto :goto_a

    :cond_11
    move-object/from16 v4, p7

    .line 25
    sget-object v2, Lcom/microsoft/identity/common/java/cache/AbstractAccountCredentialCache;->〇080:Ljava/lang/String;

    const-string v4, "Query specified mamEnrollmentIdentifier match, but credential type does not have MAM enrollment identifier"

    invoke-static {v2, v4}, Lcom/microsoft/identity/common/java/logging/Logger;->〇O888o0o(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    :goto_a
    if-eqz v7, :cond_14

    .line 26
    instance-of v2, v3, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v2, :cond_14

    .line 27
    move-object v2, v3

    check-cast v2, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v5, :cond_13

    .line 28
    invoke-virtual {v2}, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;->〇O8o08O()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/identity/common/java/util/StringUtil;->Oo08(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    goto :goto_b

    :cond_13
    const/4 v2, 0x0

    :goto_b
    move v5, v2

    :cond_14
    if-eqz v7, :cond_16

    .line 29
    instance-of v2, v3, Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

    if-eqz v2, :cond_16

    .line 30
    move-object v2, v3

    check-cast v2, Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

    if-eqz v5, :cond_15

    .line 31
    invoke-virtual {v2}, Lcom/microsoft/identity/common/java/dto/IdTokenRecord;->〇O8o08O()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/identity/common/java/util/StringUtil;->Oo08(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    const/4 v2, 0x1

    goto :goto_c

    :cond_15
    const/4 v2, 0x0

    :goto_c
    move v5, v2

    :cond_16
    if-eqz v8, :cond_1d

    .line 32
    instance-of v2, v3, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v2, :cond_19

    .line 33
    move-object v2, v3

    check-cast v2, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v5, :cond_17

    .line 34
    invoke-virtual {v2}, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;->oo〇()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v4, p9

    const/4 v5, 0x1

    invoke-static {v4, v2, v5}, Lcom/microsoft/identity/common/java/cache/AbstractAccountCredentialCache;->oO80(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_18

    const/4 v5, 0x1

    goto :goto_e

    :cond_17
    move-object/from16 v4, p9

    :cond_18
    const/4 v5, 0x0

    goto :goto_e

    :cond_19
    move-object/from16 v4, p9

    .line 35
    instance-of v2, v3, Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;

    if-eqz v2, :cond_1c

    .line 36
    move-object v2, v3

    check-cast v2, Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;

    if-eqz v5, :cond_1a

    .line 37
    invoke-virtual {v2}, Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;->O〇8O8〇008()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    invoke-static {v4, v2, v5}, Lcom/microsoft/identity/common/java/cache/AbstractAccountCredentialCache;->oO80(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1b

    const/4 v2, 0x1

    goto :goto_d

    :cond_1a
    const/4 v5, 0x1

    :cond_1b
    const/4 v2, 0x0

    :goto_d
    move v5, v2

    goto :goto_e

    .line 38
    :cond_1c
    sget-object v2, Lcom/microsoft/identity/common/java/cache/AbstractAccountCredentialCache;->〇080:Ljava/lang/String;

    const-string v1, "Query specified target-match, but no target to match."

    invoke-static {v2, v1}, Lcom/microsoft/identity/common/java/logging/Logger;->〇O888o0o(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_e

    :cond_1d
    move-object/from16 v4, p9

    :goto_e
    if-eqz v14, :cond_23

    .line 39
    instance-of v1, v3, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v1, :cond_23

    .line 40
    move-object v1, v3

    check-cast v1, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    .line 41
    invoke-virtual {v1}, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;->O〇8O8〇008()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1e

    .line 42
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    :cond_1e
    const-string v2, "pop"

    .line 43
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    if-eqz v5, :cond_20

    const-string v1, "PoP_With_Client_Key"

    move-object/from16 v2, p10

    .line 44
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1f

    const-string v1, "PoP"

    .line 45
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    :cond_1f
    :goto_f
    const/4 v5, 0x1

    goto :goto_10

    :cond_20
    move-object/from16 v2, p10

    :cond_21
    const/4 v5, 0x0

    goto :goto_10

    :cond_22
    move-object/from16 v2, p10

    if-eqz v5, :cond_21

    .line 46
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    goto :goto_f

    :cond_23
    move-object/from16 v2, p10

    :goto_10
    if-eqz v17, :cond_26

    .line 47
    instance-of v1, v3, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v1, :cond_26

    .line 48
    move-object v1, v3

    check-cast v1, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v5, :cond_24

    .line 49
    invoke-virtual {v1}, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;->〇0000OOO()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v5, p12

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_25

    const/4 v1, 0x1

    goto :goto_11

    :cond_24
    move-object/from16 v5, p12

    :cond_25
    const/4 v1, 0x0

    :goto_11
    move v5, v1

    :cond_26
    if-eqz v12, :cond_2a

    .line 50
    instance-of v1, v3, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v1, :cond_29

    .line 51
    move-object v1, v3

    check-cast v1, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    if-eqz v5, :cond_27

    .line 52
    invoke-virtual {v1}, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;->OOO〇O0()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v5, p11

    invoke-static {v5, v1}, Lcom/microsoft/identity/common/java/util/StringUtil;->Oo08(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_28

    const/4 v1, 0x1

    goto :goto_12

    :cond_27
    move-object/from16 v5, p11

    :cond_28
    const/4 v1, 0x0

    :goto_12
    move v5, v1

    goto :goto_13

    .line 53
    :cond_29
    sget-object v1, Lcom/microsoft/identity/common/java/cache/AbstractAccountCredentialCache;->〇080:Ljava/lang/String;

    const-string v2, "Query specified requested_claims-match, but attempted to match with non-AT credential type."

    invoke-static {v1, v2}, Lcom/microsoft/identity/common/java/logging/Logger;->〇O888o0o(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2a
    :goto_13
    if-eqz v5, :cond_2b

    .line 54
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2b
    move-object/from16 v2, p1

    move-object/from16 v1, p8

    move/from16 v4, v18

    goto/16 :goto_2

    :cond_2c
    return-object v0

    .line 55
    :cond_2d
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "allCredentials is marked non-null but is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected 〇〇888(Ljava/lang/String;Lcom/microsoft/identity/common/java/dto/CredentialType;)Ljava/lang/Class;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/identity/common/java/dto/CredentialType;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/identity/common/java/dto/CredentialType;",
            ")",
            "Ljava/lang/Class<",
            "+",
            "Lcom/microsoft/identity/common/java/dto/Credential;",
            ">;"
        }
    .end annotation

    .annotation build Ledu/umd/cs/findbugs/annotations/Nullable;
    .end annotation

    .line 1
    if-eqz p2, :cond_1

    .line 2
    .line 3
    sget-object v0, Lcom/microsoft/identity/common/java/cache/AbstractAccountCredentialCache$1;->〇080:[I

    .line 4
    .line 5
    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    aget p2, v0, p2

    .line 10
    .line 11
    packed-switch p2, :pswitch_data_0

    .line 12
    .line 13
    .line 14
    sget-object p2, Lcom/microsoft/identity/common/java/cache/AbstractAccountCredentialCache;->〇080:Ljava/lang/String;

    .line 15
    .line 16
    const-string v0, "Could not match CredentialType to class. Did you forget to update this method with a new type?"

    .line 17
    .line 18
    invoke-static {p2, v0}, Lcom/microsoft/identity/common/java/logging/Logger;->〇00(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    if-eqz p1, :cond_0

    .line 22
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v1, "Sought key was: ["

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string p1, "]"

    .line 37
    .line 38
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-static {p2, p1}, Lcom/microsoft/identity/common/java/logging/Logger;->O8ooOoo〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :pswitch_0
    const-class p1, Lcom/microsoft/identity/common/java/dto/PrimaryRefreshTokenRecord;

    .line 50
    .line 51
    goto :goto_1

    .line 52
    :pswitch_1
    const-class p1, Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

    .line 53
    .line 54
    goto :goto_1

    .line 55
    :pswitch_2
    const-class p1, Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;

    .line 56
    .line 57
    goto :goto_1

    .line 58
    :pswitch_3
    const-class p1, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_0
    :goto_0
    const/4 p1, 0x0

    .line 62
    :goto_1
    return-object p1

    .line 63
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    .line 64
    .line 65
    const-string/jumbo p2, "targetType is marked non-null but is null"

    .line 66
    .line 67
    .line 68
    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    throw p1

    .line 72
    nop

    .line 73
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

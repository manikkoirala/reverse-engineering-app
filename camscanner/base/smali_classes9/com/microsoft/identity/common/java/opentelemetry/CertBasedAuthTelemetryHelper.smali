.class public Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;
.super Ljava/lang/Object;
.source "CertBasedAuthTelemetryHelper.java"

# interfaces
.implements Lcom/microsoft/identity/common/java/opentelemetry/ICertBasedAuthTelemetryHelper;


# instance fields
.field private final 〇080:Lio/opentelemetry/api/trace/Span;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/microsoft/identity/common/java/opentelemetry/SpanName;->CertBasedAuth:Lcom/microsoft/identity/common/java/opentelemetry/SpanName;

    .line 5
    .line 6
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-static {v0}, Lcom/microsoft/identity/common/java/opentelemetry/OTelUtility;->〇080(Ljava/lang/String;)Lio/opentelemetry/api/trace/Span;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iput-object v0, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public O8(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 2
    .line 3
    sget-object v1, Lcom/microsoft/identity/common/java/opentelemetry/AttributeName;->cert_based_auth_existing_piv_provider_present:Lcom/microsoft/identity/common/java/opentelemetry/AttributeName;

    .line 4
    .line 5
    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-interface {v0, v1, p1}, Lio/opentelemetry/api/trace/Span;->setAttribute(Ljava/lang/String;Z)Lio/opentelemetry/api/trace/Span;

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public Oo08(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 4
    .line 5
    sget-object v1, Lcom/microsoft/identity/common/java/opentelemetry/AttributeName;->cert_based_auth_challenge_handler:Lcom/microsoft/identity/common/java/opentelemetry/AttributeName;

    .line 6
    .line 7
    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-interface {v0, v1, p1}, Lio/opentelemetry/api/trace/Span;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Lio/opentelemetry/api/trace/Span;

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    .line 16
    .line 17
    const-string v0, "challengeHandlerName is marked non-null but is null"

    .line 18
    .line 19
    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    throw p1
    .line 23
    .line 24
    .line 25
.end method

.method public o〇0(Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthChoice;)V
    .locals 2
    .param p1    # Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthChoice;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    sget-object v0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper$1;->〇080:[I

    .line 4
    .line 5
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    aget p1, v0, p1

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    if-eq p1, v0, :cond_1

    .line 13
    .line 14
    const/4 v0, 0x2

    .line 15
    if-eq p1, v0, :cond_0

    .line 16
    .line 17
    iget-object p1, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 18
    .line 19
    sget-object v0, Lcom/microsoft/identity/common/java/opentelemetry/AttributeName;->cert_based_auth_user_choice:Lcom/microsoft/identity/common/java/opentelemetry/AttributeName;

    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v1, "N/A"

    .line 26
    .line 27
    invoke-interface {p1, v0, v1}, Lio/opentelemetry/api/trace/Span;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Lio/opentelemetry/api/trace/Span;

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    iget-object p1, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 32
    .line 33
    sget-object v0, Lcom/microsoft/identity/common/java/opentelemetry/AttributeName;->cert_based_auth_user_choice:Lcom/microsoft/identity/common/java/opentelemetry/AttributeName;

    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    const-string/jumbo v1, "smartcard"

    .line 40
    .line 41
    .line 42
    invoke-interface {p1, v0, v1}, Lio/opentelemetry/api/trace/Span;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Lio/opentelemetry/api/trace/Span;

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    iget-object p1, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 47
    .line 48
    sget-object v0, Lcom/microsoft/identity/common/java/opentelemetry/AttributeName;->cert_based_auth_user_choice:Lcom/microsoft/identity/common/java/opentelemetry/AttributeName;

    .line 49
    .line 50
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    const-string v1, "on-device"

    .line 55
    .line 56
    invoke-interface {p1, v0, v1}, Lio/opentelemetry/api/trace/Span;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Lio/opentelemetry/api/trace/Span;

    .line 57
    .line 58
    .line 59
    :goto_0
    return-void

    .line 60
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    .line 61
    .line 62
    const-string v0, "choice is marked non-null but is null"

    .line 63
    .line 64
    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    throw p1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public 〇080(Ljava/lang/Exception;)V
    .locals 1
    .param p1    # Ljava/lang/Exception;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .annotation build Ledu/umd/cs/findbugs/annotations/SuppressFBWarnings;
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lio/opentelemetry/api/trace/Span;->recordException(Ljava/lang/Throwable;)Lio/opentelemetry/api/trace/Span;

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 9
    .line 10
    sget-object v0, Lio/opentelemetry/api/trace/StatusCode;->ERROR:Lio/opentelemetry/api/trace/StatusCode;

    .line 11
    .line 12
    invoke-interface {p1, v0}, Lio/opentelemetry/api/trace/Span;->setStatus(Lio/opentelemetry/api/trace/StatusCode;)Lio/opentelemetry/api/trace/Span;

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 16
    .line 17
    invoke-interface {p1}, Lio/opentelemetry/api/trace/Span;->end()V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    .line 22
    .line 23
    const-string v0, "exception is marked non-null but is null"

    .line 24
    .line 25
    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    throw p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public 〇o00〇〇Oo()V
    .locals 2
    .annotation build Ledu/umd/cs/findbugs/annotations/SuppressFBWarnings;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 2
    .line 3
    sget-object v1, Lio/opentelemetry/api/trace/StatusCode;->OK:Lio/opentelemetry/api/trace/StatusCode;

    .line 4
    .line 5
    invoke-interface {v0, v1}, Lio/opentelemetry/api/trace/Span;->setStatus(Lio/opentelemetry/api/trace/StatusCode;)Lio/opentelemetry/api/trace/Span;

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 9
    .line 10
    invoke-interface {v0}, Lio/opentelemetry/api/trace/Span;->end()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇o〇(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .annotation build Ledu/umd/cs/findbugs/annotations/SuppressFBWarnings;
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 4
    .line 5
    sget-object v1, Lio/opentelemetry/api/trace/StatusCode;->ERROR:Lio/opentelemetry/api/trace/StatusCode;

    .line 6
    .line 7
    invoke-interface {v0, v1, p1}, Lio/opentelemetry/api/trace/Span;->setStatus(Lio/opentelemetry/api/trace/StatusCode;Ljava/lang/String;)Lio/opentelemetry/api/trace/Span;

    .line 8
    .line 9
    .line 10
    iget-object p1, p0, Lcom/microsoft/identity/common/java/opentelemetry/CertBasedAuthTelemetryHelper;->〇080:Lio/opentelemetry/api/trace/Span;

    .line 11
    .line 12
    invoke-interface {p1}, Lio/opentelemetry/api/trace/Span;->end()V

    .line 13
    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    .line 17
    .line 18
    const-string v0, "message is marked non-null but is null"

    .line 19
    .line 20
    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    throw p1
    .line 24
    .line 25
.end method

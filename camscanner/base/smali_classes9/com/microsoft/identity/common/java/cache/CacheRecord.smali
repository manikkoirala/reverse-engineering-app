.class public final Lcom/microsoft/identity/common/java/cache/CacheRecord;
.super Ljava/lang/Object;
.source "CacheRecord.java"

# interfaces
.implements Lcom/microsoft/identity/common/java/cache/ICacheRecord;


# instance fields
.field private final O8:Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

.field private final Oo08:Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

.field private final 〇080:Lcom/microsoft/identity/common/java/dto/AccountRecord;
    .annotation build Llombok/NonNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

.field private final 〇o〇:Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;


# virtual methods
.method public O8()Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o〇:Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public Oo08()Lcom/microsoft/identity/common/java/dto/IdTokenRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->Oo08:Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const/4 v1, 0x0

    .line 6
    if-eqz p1, :cond_a

    .line 7
    .line 8
    const-class v2, Lcom/microsoft/identity/common/java/cache/CacheRecord;

    .line 9
    .line 10
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 11
    .line 12
    .line 13
    move-result-object v3

    .line 14
    if-eq v2, v3, :cond_1

    .line 15
    .line 16
    goto :goto_4

    .line 17
    :cond_1
    check-cast p1, Lcom/microsoft/identity/common/java/cache/CacheRecord;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇080:Lcom/microsoft/identity/common/java/dto/AccountRecord;

    .line 20
    .line 21
    if-eqz v2, :cond_2

    .line 22
    .line 23
    iget-object v3, p1, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇080:Lcom/microsoft/identity/common/java/dto/AccountRecord;

    .line 24
    .line 25
    invoke-virtual {v2, v3}, Lcom/microsoft/identity/common/java/dto/AccountRecord;->equals(Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    if-nez v2, :cond_3

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_2
    iget-object v2, p1, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇080:Lcom/microsoft/identity/common/java/dto/AccountRecord;

    .line 33
    .line 34
    if-eqz v2, :cond_3

    .line 35
    .line 36
    :goto_0
    return v1

    .line 37
    :cond_3
    iget-object v2, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o00〇〇Oo:Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    .line 38
    .line 39
    if-eqz v2, :cond_4

    .line 40
    .line 41
    iget-object v3, p1, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o00〇〇Oo:Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    .line 42
    .line 43
    invoke-virtual {v2, v3}, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;->equals(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    if-nez v2, :cond_5

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_4
    iget-object v2, p1, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o00〇〇Oo:Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    .line 51
    .line 52
    if-eqz v2, :cond_5

    .line 53
    .line 54
    :goto_1
    return v1

    .line 55
    :cond_5
    iget-object v2, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o〇:Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;

    .line 56
    .line 57
    if-eqz v2, :cond_6

    .line 58
    .line 59
    iget-object v3, p1, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o〇:Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;

    .line 60
    .line 61
    invoke-virtual {v2, v3}, Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;->equals(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    if-nez v2, :cond_7

    .line 66
    .line 67
    goto :goto_2

    .line 68
    :cond_6
    iget-object v2, p1, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o〇:Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;

    .line 69
    .line 70
    if-eqz v2, :cond_7

    .line 71
    .line 72
    :goto_2
    return v1

    .line 73
    :cond_7
    iget-object v2, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->O8:Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

    .line 74
    .line 75
    iget-object p1, p1, Lcom/microsoft/identity/common/java/cache/CacheRecord;->O8:Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

    .line 76
    .line 77
    if-eqz v2, :cond_8

    .line 78
    .line 79
    invoke-virtual {v2, p1}, Lcom/microsoft/identity/common/java/dto/IdTokenRecord;->equals(Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    goto :goto_3

    .line 84
    :cond_8
    if-nez p1, :cond_9

    .line 85
    .line 86
    goto :goto_3

    .line 87
    :cond_9
    const/4 v0, 0x0

    .line 88
    :goto_3
    return v0

    .line 89
    :cond_a
    :goto_4
    return v1
    .line 90
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇080:Lcom/microsoft/identity/common/java/dto/AccountRecord;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/microsoft/identity/common/java/dto/AccountRecord;->hashCode()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 13
    .line 14
    iget-object v2, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o00〇〇Oo:Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    .line 15
    .line 16
    if-eqz v2, :cond_1

    .line 17
    .line 18
    invoke-virtual {v2}, Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;->hashCode()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    goto :goto_1

    .line 23
    :cond_1
    const/4 v2, 0x0

    .line 24
    :goto_1
    add-int/2addr v0, v2

    .line 25
    mul-int/lit8 v0, v0, 0x1f

    .line 26
    .line 27
    iget-object v2, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o〇:Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;

    .line 28
    .line 29
    if-eqz v2, :cond_2

    .line 30
    .line 31
    invoke-virtual {v2}, Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;->hashCode()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    goto :goto_2

    .line 36
    :cond_2
    const/4 v2, 0x0

    .line 37
    :goto_2
    add-int/2addr v0, v2

    .line 38
    mul-int/lit8 v0, v0, 0x1f

    .line 39
    .line 40
    iget-object v2, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->O8:Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

    .line 41
    .line 42
    if-eqz v2, :cond_3

    .line 43
    .line 44
    invoke-virtual {v2}, Lcom/microsoft/identity/common/java/dto/IdTokenRecord;->hashCode()I

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    :cond_3
    add-int/2addr v0, v1

    .line 49
    return v0
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "CacheRecord(mAccount="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o00〇〇Oo()Lcom/microsoft/identity/common/java/dto/AccountRecord;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v1, ", mAccessToken="

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇080()Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string v1, ", mRefreshToken="

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/microsoft/identity/common/java/cache/CacheRecord;->O8()Lcom/microsoft/identity/common/java/dto/RefreshTokenRecord;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    const-string v1, ", mIdToken="

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {p0}, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o〇()Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    const-string v1, ", mV1IdToken="

    .line 55
    .line 56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {p0}, Lcom/microsoft/identity/common/java/cache/CacheRecord;->Oo08()Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v1, ")"

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    return-object v0
.end method

.method public 〇080()Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇o00〇〇Oo:Lcom/microsoft/identity/common/java/dto/AccessTokenRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇o00〇〇Oo()Lcom/microsoft/identity/common/java/dto/AccountRecord;
    .locals 1
    .annotation build Llombok/NonNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->〇080:Lcom/microsoft/identity/common/java/dto/AccountRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇o〇()Lcom/microsoft/identity/common/java/dto/IdTokenRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/identity/common/java/cache/CacheRecord;->O8:Lcom/microsoft/identity/common/java/dto/IdTokenRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.class public final enum Lcom/microsoft/identity/common/adal/internal/ADALError;
.super Ljava/lang/Enum;
.source "ADALError.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/microsoft/identity/common/adal/internal/ADALError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ACTIVITY_REQUEST_INTENT_DATA_IS_NULL:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ANDROIDKEYSTORE_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ANDROIDKEYSTORE_KEYPAIR_GENERATOR_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum APP_PACKAGE_NAME_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ARGUMENT_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTHORIZATION_CODE_NOT_EXCHANGED_FOR_TOKEN:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_FAILED_BAD_STATE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_FAILED_CANCELLED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_FAILED_INTERNAL_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_FAILED_INTUNE_POLICY_REQUIRED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_FAILED_NO_RESOURCES:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_FAILED_NO_STATE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_FAILED_NO_TOKEN:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_FAILED_SERVER_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_FAILED_USER_MISMATCH:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_REFRESH_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum AUTH_REFRESH_FAILED_PROMPT_NOT_ALLOWED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROADCAST_CANCEL_NOT_SUCCESSFUL:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROADCAST_RECEIVER_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_ACCOUNT_DEVICE_REGISTRY_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_ACCOUNT_DOES_NOT_EXIST:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_ACCOUNT_FAILED_RESOLVED_INTERRUPT:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_ACCOUNT_SAVE_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_ACTIVITY_INFO_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_ACTIVITY_INVALID_REQUEST:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_ACTIVITY_IS_NOT_RESOLVED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_APP_INSTALLATION_STARTED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_APP_VERIFICATION_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_AUTHENTICATION_REQUEST_IS_NULL:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_AUTHENTICATOR_BAD_ARGUMENTS:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_AUTHENTICATOR_BAD_AUTHENTICATION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_AUTHENTICATOR_ERROR_GETAUTHTOKEN:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_AUTHENTICATOR_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_AUTHENTICATOR_IO_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_AUTHENTICATOR_NOT_RESPONDING:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_AUTHENTICATOR_OPERATION_CANCEL:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_AUTHENTICATOR_UNSUPPORTED_OPERATION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_BIND_SERVICE_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_PACKAGE_NAME_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_PRT_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_SIGNATURE_NOT_SAVED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_SINGLE_USER_EXPECTED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum BROKER_VERIFICATION_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum CALLBACK_IS_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum CERTIFICATE_ENCODING_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum CORRELATION_ID_FORMAT:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum CORRELATION_ID_NOT_MATCHING_REQUEST_RESPONSE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DATE_PARSING_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DECRYPTION_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_ACTIVITY_IS_NOT_RESOLVED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_ASYNC_TASK_REUSED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_AUTHORITY_CAN_NOT_BE_VALIDED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_AUTHORITY_IS_EMPTY:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_AUTHORITY_IS_NOT_VALID_INSTANCE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_AUTHORITY_IS_NOT_VALID_URL:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_BEARER_HEADER_MULTIPLE_ITEMS:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_BROKER_PERMISSIONS_MISSING:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_CALLING_ON_MAIN_THREAD:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_CONTEXT_IS_NOT_PROVIDED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_DIALOG_INFLATION_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_DIALOG_LAYOUT_INVALID:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_INTERNET_PERMISSION_MISSING:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_REDIRECTURI_INVALID:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVELOPER_RESOURCE_IS_EMPTY:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_ALGORITHM_PADDING_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_CACHE_IS_NOT_WORKING:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_CERTIFICATE_API_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_CERTIFICATE_REQUEST_INVALID:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_CERTIFICATE_RESPONSE_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_CERT_PROXY_UNINITIALIZED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_CHALLENGE_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_CONNECTION_IS_NOT_AVAILABLE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_FILE_CACHE_FORMAT_IS_WRONG:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_FILE_CACHE_IS_NOT_LOADED_FROM_FILE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_FILE_CACHE_IS_NOT_WRITING_TO_FILE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_INTERNET_IS_NOT_AVAILABLE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_NO_SUCH_ALGORITHM:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_PRNG_FIX_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DEVICE_SHARED_PREF_IS_NOT_AVAILABLE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DIGEST_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DISCOVERY_NOT_SUPPORTED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DRS_DISCOVERY_FAILED_UNKNOWN_HOST:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DRS_FAILED_SERVER_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum DRS_METADATA_URL_INVALID:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ENCODING_IS_NOT_SUPPORTED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ENCRYPTION_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ENCRYPTION_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ERROR_FAILED_SSL_HANDSHAKE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ERROR_SILENT_REQUEST:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ERROR_WEBVIEW:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum FAIL_TO_EXPORT:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum FAIL_TO_IMPORT:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum IDTOKEN_PARSING_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum INCOMPATIBLE_BLOB_VERSION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum INVALID_TOKEN_CACHE_ITEM:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum IO_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum JSON_PARSE_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum KEY_CHAIN_PRIVATE_KEY_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum MAPPING_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum MDM_REQUIRED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum NO_NETWORK_CONNECTION_POWER_OPTIMIZATION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ON_ACTIVITY_RESULT_CALLBACK_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum ON_ACTIVITY_RESULT_INTENT_NULL:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum PACKAGE_NAME_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum PARALLEL_UI_REQUESTS:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum RESOURCE_AUTHENTICATION_CHALLENGE_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum RESOURCE_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum SERVER_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum SERVER_INVALID_JSON_RESPONSE:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum SERVER_INVALID_REQUEST:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum SIGNATURE_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum SOCKET_TIMEOUT_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum TOKEN_CACHE_ITEM_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum WEBVIEW_REDIRECTURL_NOT_SSL_PROTECTED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum WEBVIEW_RETURNED_AUTHENTICATION_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum WEBVIEW_RETURNED_EMPTY_REDIRECT_URL:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum WEBVIEW_RETURNED_INVALID_AUTHENTICATION_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum X_MS_CLITELEM_MALFORMED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field public static final enum X_MS_CLITELEM_VERSION_UNRECOGNIZED:Lcom/microsoft/identity/common/adal/internal/ADALError;

.field private static final exceptionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/microsoft/identity/common/adal/internal/ADALError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDescription:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 123

    .line 1
    new-instance v0, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v1, "Authority validation returned an error"

    const-string v2, "DEVELOPER_AUTHORITY_CAN_NOT_BE_VALIDED"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_AUTHORITY_CAN_NOT_BE_VALIDED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 2
    new-instance v1, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v2, "Authority is not a valid instance"

    const-string v4, "DEVELOPER_AUTHORITY_IS_NOT_VALID_INSTANCE"

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5, v2}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_AUTHORITY_IS_NOT_VALID_INSTANCE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 3
    new-instance v2, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v4, "Authority url is not valid"

    const-string v6, "DEVELOPER_AUTHORITY_IS_NOT_VALID_URL"

    const/4 v7, 0x2

    invoke-direct {v2, v6, v7, v4}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_AUTHORITY_IS_NOT_VALID_URL:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 4
    new-instance v4, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v6, "Authority is empty"

    const-string v8, "DEVELOPER_AUTHORITY_IS_EMPTY"

    const/4 v9, 0x3

    invoke-direct {v4, v8, v9, v6}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_AUTHORITY_IS_EMPTY:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 5
    new-instance v6, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v8, "Async tasks can only be executed one time. They are not supposed to be reused."

    const-string v10, "DEVELOPER_ASYNC_TASK_REUSED"

    const/4 v11, 0x4

    invoke-direct {v6, v10, v11, v8}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v6, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_ASYNC_TASK_REUSED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 6
    new-instance v8, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v10, "Resource is empty"

    const-string v12, "DEVELOPER_RESOURCE_IS_EMPTY"

    const/4 v13, 0x5

    invoke-direct {v8, v12, v13, v10}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v8, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_RESOURCE_IS_EMPTY:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 7
    new-instance v10, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v12, "Context is not provided"

    const-string v14, "DEVELOPER_CONTEXT_IS_NOT_PROVIDED"

    const/4 v15, 0x6

    invoke-direct {v10, v14, v15, v12}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v10, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_CONTEXT_IS_NOT_PROVIDED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 8
    new-instance v12, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v14, "Key/value pair list contains redundant items in the header"

    const-string v15, "DEVELOPER_BEARER_HEADER_MULTIPLE_ITEMS"

    const/4 v13, 0x7

    invoke-direct {v12, v15, v13, v14}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v12, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_BEARER_HEADER_MULTIPLE_ITEMS:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 9
    new-instance v14, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "Active callback is not found"

    const-string v13, "CALLBACK_IS_NOT_FOUND"

    const/16 v11, 0x8

    invoke-direct {v14, v13, v11, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v14, Lcom/microsoft/identity/common/adal/internal/ADALError;->CALLBACK_IS_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 10
    new-instance v13, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "Activity is not resolved. Verify the activity name in your manifest file"

    const-string v11, "DEVELOPER_ACTIVITY_IS_NOT_RESOLVED"

    const/16 v9, 0x9

    invoke-direct {v13, v11, v9, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v13, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_ACTIVITY_IS_NOT_RESOLVED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 11
    new-instance v11, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "android.permission.INTERNET is not added to AndroidManifest file"

    const-string v9, "DEVELOPER_INTERNET_PERMISSION_MISSING"

    const/16 v7, 0xa

    invoke-direct {v11, v9, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_INTERNET_PERMISSION_MISSING:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 12
    new-instance v9, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "GET_ACCOUNTS, MANAGE_ACCOUNTS, USE_CREDENTIALS are not added to AndroidManifest file"

    const-string v7, "DEVELOPER_BROKER_PERMISSIONS_MISSING"

    const/16 v5, 0xb

    invoke-direct {v9, v7, v5, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_BROKER_PERMISSIONS_MISSING:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 13
    new-instance v7, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "Calling from main thread for background operation"

    const-string v5, "DEVELOPER_CALLING_ON_MAIN_THREAD"

    const/16 v3, 0xc

    invoke-direct {v7, v5, v3, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_CALLING_ON_MAIN_THREAD:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 14
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "dialog_authentication.xml file has invalid elements"

    const-string v3, "DEVELOPER_DIALOG_LAYOUT_INVALID"

    move-object/from16 v16, v7

    const/16 v7, 0xd

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_DIALOG_LAYOUT_INVALID:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 15
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "An error occur when attempting to inflate the authentication dialog"

    const-string v7, "DEVELOPER_DIALOG_INFLATION_ERROR"

    move-object/from16 v17, v5

    const/16 v5, 0xe

    invoke-direct {v3, v7, v5, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_DIALOG_INFLATION_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 16
    new-instance v7, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "Invalid request to server"

    const-string v5, "SERVER_INVALID_REQUEST"

    move-object/from16 v18, v3

    const/16 v3, 0xf

    invoke-direct {v7, v5, v3, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/microsoft/identity/common/adal/internal/ADALError;->SERVER_INVALID_REQUEST:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 17
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "Server returned an error"

    const-string v3, "SERVER_ERROR"

    move-object/from16 v19, v7

    const/16 v7, 0x10

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->SERVER_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 18
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "I/O exception"

    const-string v7, "IO_EXCEPTION"

    move-object/from16 v20, v5

    const/16 v5, 0x11

    invoke-direct {v3, v7, v5, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->IO_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 19
    new-instance v7, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "Socket timeout exception"

    const-string v5, "SOCKET_TIMEOUT_EXCEPTION"

    move-object/from16 v21, v3

    const/16 v3, 0x12

    invoke-direct {v7, v5, v3, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/microsoft/identity/common/adal/internal/ADALError;->SOCKET_TIMEOUT_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 20
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "Invalid argument"

    const-string v3, "ARGUMENT_EXCEPTION"

    move-object/from16 v22, v7

    const/16 v7, 0x13

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->ARGUMENT_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 21
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "Webview returned error for SSL"

    const-string v7, "ERROR_FAILED_SSL_HANDSHAKE"

    move-object/from16 v23, v5

    const/16 v5, 0x14

    invoke-direct {v3, v7, v5, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->ERROR_FAILED_SSL_HANDSHAKE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 22
    new-instance v7, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const-string v15, "Webview returned an error"

    const-string v5, "ERROR_WEBVIEW"

    move-object/from16 v24, v3

    const/16 v3, 0x15

    invoke-direct {v7, v5, v3, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/microsoft/identity/common/adal/internal/ADALError;->ERROR_WEBVIEW:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 23
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v15, 0x16

    const-string v3, "Request object is null"

    move-object/from16 v25, v7

    const-string v7, "ACTIVITY_REQUEST_INTENT_DATA_IS_NULL"

    invoke-direct {v5, v7, v15, v3}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->ACTIVITY_REQUEST_INTENT_DATA_IS_NULL:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 24
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x17

    const-string v15, "Broadcast receiver has an error"

    move-object/from16 v26, v5

    const-string v5, "BROADCAST_RECEIVER_ERROR"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROADCAST_RECEIVER_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 25
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x18

    const-string v15, "Authorization failed"

    move-object/from16 v27, v3

    const-string v3, "AUTH_FAILED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 26
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x19

    const-string v15, "Refresh token is failed and prompt is not allowed"

    move-object/from16 v28, v5

    const-string v5, "AUTH_REFRESH_FAILED_PROMPT_NOT_ALLOWED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_REFRESH_FAILED_PROMPT_NOT_ALLOWED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 27
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x1a

    const-string v15, "The Authorization Server returned an unrecognized response"

    move-object/from16 v29, v3

    const-string v3, "AUTH_FAILED_SERVER_ERROR"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_FAILED_SERVER_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 28
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x1b

    const-string v15, "The required resource bundle could not be loaded"

    move-object/from16 v30, v5

    const-string v5, "AUTH_FAILED_NO_RESOURCES"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_FAILED_NO_RESOURCES:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 29
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x1c

    const-string v15, "The authorization server response has incorrectly encoded state"

    move-object/from16 v31, v3

    const-string v3, "AUTH_FAILED_NO_STATE"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_FAILED_NO_STATE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 30
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x1d

    const-string v15, "The authorization server response has no encoded state"

    move-object/from16 v32, v5

    const-string v5, "AUTH_FAILED_BAD_STATE"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_FAILED_BAD_STATE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 31
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x1e

    const-string v15, "The requested access token could not be found"

    move-object/from16 v33, v3

    const-string v3, "AUTH_FAILED_NO_TOKEN"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_FAILED_NO_TOKEN:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 32
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x1f

    const-string v15, "The user cancelled the authorization request"

    move-object/from16 v34, v5

    const-string v5, "AUTH_FAILED_CANCELLED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_FAILED_CANCELLED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 33
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x20

    const-string v15, "Invalid parameters for authorization operation"

    move-object/from16 v35, v3

    const-string v3, "AUTH_FAILED_INTERNAL_ERROR"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_FAILED_INTERNAL_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 34
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x21

    const-string v15, "User returned by service does not match the one in the request"

    move-object/from16 v36, v5

    const-string v5, "AUTH_FAILED_USER_MISMATCH"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_FAILED_USER_MISMATCH:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 35
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x22

    const-string v15, "Intune App Protection Policy required"

    move-object/from16 v37, v3

    const-string v3, "AUTH_FAILED_INTUNE_POLICY_REQUIRED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_FAILED_INTUNE_POLICY_REQUIRED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 36
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x23

    const-string v15, "Internet permissions are not set for the app"

    move-object/from16 v38, v5

    const-string v5, "DEVICE_INTERNET_IS_NOT_AVAILABLE"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_INTERNET_IS_NOT_AVAILABLE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 37
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x24

    const-string v15, "Unable to access the network due to power optimizations"

    move-object/from16 v39, v3

    const-string v3, "NO_NETWORK_CONNECTION_POWER_OPTIMIZATION"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->NO_NETWORK_CONNECTION_POWER_OPTIMIZATION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 38
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x25

    const-string v15, "onActivityResult is called with null intent data"

    move-object/from16 v40, v5

    const-string v5, "ON_ACTIVITY_RESULT_INTENT_NULL"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->ON_ACTIVITY_RESULT_INTENT_NULL:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 39
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x26

    const-string v15, "onActivityResult is called, but callback is not found"

    move-object/from16 v41, v3

    const-string v3, "ON_ACTIVITY_RESULT_CALLBACK_NOT_FOUND"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->ON_ACTIVITY_RESULT_CALLBACK_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 40
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x27

    const-string v15, "Shared preferences are not available"

    move-object/from16 v42, v5

    const-string v5, "DEVICE_SHARED_PREF_IS_NOT_AVAILABLE"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_SHARED_PREF_IS_NOT_AVAILABLE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 41
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x28

    const-string v15, "Cache is not saving the changes."

    move-object/from16 v43, v3

    const-string v3, "DEVICE_CACHE_IS_NOT_WORKING"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_CACHE_IS_NOT_WORKING:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 42
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x29

    const-string v15, "Cache is not loaded from File"

    move-object/from16 v44, v5

    const-string v5, "DEVICE_FILE_CACHE_IS_NOT_LOADED_FROM_FILE"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_FILE_CACHE_IS_NOT_LOADED_FROM_FILE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 43
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x2a

    const-string v15, "FileCache could not write to the File"

    move-object/from16 v45, v3

    const-string v3, "DEVICE_FILE_CACHE_IS_NOT_WRITING_TO_FILE"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_FILE_CACHE_IS_NOT_WRITING_TO_FILE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 44
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x2b

    const-string v15, "Wrong cache file format"

    move-object/from16 v46, v5

    const-string v5, "DEVICE_FILE_CACHE_FORMAT_IS_WRONG"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_FILE_CACHE_FORMAT_IS_WRONG:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 45
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x2c

    const-string v15, "Connection is not available"

    move-object/from16 v47, v3

    const-string v3, "DEVICE_CONNECTION_IS_NOT_AVAILABLE"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_CONNECTION_IS_NOT_AVAILABLE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 46
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x2d

    const-string v15, "PRNG fixes are not applied"

    move-object/from16 v48, v5

    const-string v5, "DEVICE_PRNG_FIX_ERROR"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_PRNG_FIX_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 47
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x2e

    const-string v15, "Cannot parse IdToken"

    move-object/from16 v49, v3

    const-string v3, "IDTOKEN_PARSING_FAILURE"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->IDTOKEN_PARSING_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 48
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x2f

    const-string v15, "Cannot parse date"

    move-object/from16 v50, v5

    const-string v5, "DATE_PARSING_FAILURE"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DATE_PARSING_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 49
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x30

    const-string v15, "Authorization code not exchanged for token"

    move-object/from16 v51, v3

    const-string v3, "AUTHORIZATION_CODE_NOT_EXCHANGED_FOR_TOKEN"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTHORIZATION_CODE_NOT_EXCHANGED_FOR_TOKEN:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 50
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x31

    const-string v15, "Cancel message is not successfully delivered to broadcast receiver."

    move-object/from16 v52, v5

    const-string v5, "BROADCAST_CANCEL_NOT_SUCCESSFUL"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROADCAST_CANCEL_NOT_SUCCESSFUL:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 51
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x32

    const-string v15, "Correlationid is not in UUID format"

    move-object/from16 v53, v3

    const-string v3, "CORRELATION_ID_FORMAT"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->CORRELATION_ID_FORMAT:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 52
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x33

    const-string v15, "Correlationid provided in request is not matching the response"

    move-object/from16 v54, v5

    const-string v5, "CORRELATION_ID_NOT_MATCHING_REQUEST_RESPONSE"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->CORRELATION_ID_NOT_MATCHING_REQUEST_RESPONSE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 53
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x34

    const-string v15, "Encoding format is not supported"

    move-object/from16 v55, v3

    const-string v3, "ENCODING_IS_NOT_SUPPORTED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->ENCODING_IS_NOT_SUPPORTED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 54
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x35

    const-string v15, "Server returned invalid JSON response"

    move-object/from16 v56, v5

    const-string v5, "SERVER_INVALID_JSON_RESPONSE"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->SERVER_INVALID_JSON_RESPONSE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 55
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x36

    const-string v15, "Refresh token request failed"

    move-object/from16 v57, v3

    const-string v3, "AUTH_REFRESH_FAILED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->AUTH_REFRESH_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 56
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x37

    const-string v15, "Encryption failed"

    move-object/from16 v58, v5

    const-string v5, "ENCRYPTION_FAILED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->ENCRYPTION_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 57
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x38

    const-string v15, "Decryption failed"

    move-object/from16 v59, v3

    const-string v3, "DECRYPTION_FAILED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DECRYPTION_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 58
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x39

    const-string v15, "Failed to use AndroidKeyStore"

    move-object/from16 v60, v5

    const-string v5, "ANDROIDKEYSTORE_FAILED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->ANDROIDKEYSTORE_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 59
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x3a

    const-string v15, "Failed to use KeyPairGeneratorSpec"

    move-object/from16 v61, v3

    const-string v3, "ANDROIDKEYSTORE_KEYPAIR_GENERATOR_FAILED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->ANDROIDKEYSTORE_KEYPAIR_GENERATOR_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 60
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x3b

    const-string v15, "Authority validation is not supported for ADFS authority."

    move-object/from16 v62, v5

    const-string v5, "DISCOVERY_NOT_SUPPORTED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DISCOVERY_NOT_SUPPORTED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 61
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x3c

    const-string v15, "Broker is not installed in your system"

    move-object/from16 v63, v3

    const-string v3, "BROKER_PACKAGE_NAME_NOT_FOUND"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_PACKAGE_NAME_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 62
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x3d

    const-string v15, "Broker failed to get PRT"

    move-object/from16 v64, v5

    const-string v5, "BROKER_PRT_FAILED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_PRT_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 63
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x3e

    const-string v15, "Authenticator is not responding"

    move-object/from16 v65, v3

    const-string v3, "BROKER_AUTHENTICATOR_NOT_RESPONDING"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_AUTHENTICATOR_NOT_RESPONDING:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 64
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x3f

    const-string v15, "Operation was cancelled by the broker"

    move-object/from16 v66, v5

    const-string v5, "BROKER_AUTHENTICATOR_OPERATION_CANCEL"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_AUTHENTICATOR_OPERATION_CANCEL:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 65
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x40

    const-string v15, "Authenticator error"

    move-object/from16 v67, v3

    const-string v3, "BROKER_AUTHENTICATOR_ERROR_GETAUTHTOKEN"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_AUTHENTICATOR_ERROR_GETAUTHTOKEN:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 66
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x41

    const-string v15, "Invalid arguments for Authenticator request"

    move-object/from16 v68, v5

    const-string v5, "BROKER_AUTHENTICATOR_BAD_ARGUMENTS"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_AUTHENTICATOR_BAD_ARGUMENTS:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 67
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x42

    const-string v15, "Authentication request failed"

    move-object/from16 v69, v3

    const-string v3, "BROKER_AUTHENTICATOR_BAD_AUTHENTICATION"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_AUTHENTICATOR_BAD_AUTHENTICATION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 68
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x43

    const-string v15, "Authenticator is not supporting this operation"

    move-object/from16 v70, v5

    const-string v5, "BROKER_AUTHENTICATOR_UNSUPPORTED_OPERATION"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_AUTHENTICATOR_UNSUPPORTED_OPERATION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 69
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x44

    const-string v15, "Authenticator has IO Exception"

    move-object/from16 v71, v3

    const-string v3, "BROKER_AUTHENTICATOR_IO_EXCEPTION"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_AUTHENTICATOR_IO_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 70
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x45

    const-string v15, "Authenticator has an Exception"

    move-object/from16 v72, v5

    const-string v5, "BROKER_AUTHENTICATOR_EXCEPTION"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_AUTHENTICATOR_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 71
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x46

    const-string v15, "Signature could not be verified"

    move-object/from16 v73, v3

    const-string v3, "BROKER_VERIFICATION_FAILED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_VERIFICATION_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 72
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x47

    const-string v15, "Package name is not resolved"

    move-object/from16 v74, v5

    const-string v5, "PACKAGE_NAME_NOT_FOUND"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->PACKAGE_NAME_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 73
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x48

    const-string v15, "Error in generating hash with MessageDigest"

    move-object/from16 v75, v3

    const-string v3, "DIGEST_ERROR"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DIGEST_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 74
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x49

    const-string v15, "Authentication request is null"

    move-object/from16 v76, v5

    const-string v5, "BROKER_AUTHENTICATION_REQUEST_IS_NULL"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_AUTHENTICATION_REQUEST_IS_NULL:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 75
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x4a

    const-string v15, "Calling app could not be verified"

    move-object/from16 v77, v3

    const-string v3, "BROKER_APP_VERIFICATION_FAILED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_APP_VERIFICATION_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 76
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x4b

    const-string v15, "Activity information is not retrieved"

    move-object/from16 v78, v5

    const-string v5, "BROKER_ACTIVITY_INFO_NOT_FOUND"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_ACTIVITY_INFO_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 77
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x4c

    const-string v15, "Signature is not saved"

    move-object/from16 v79, v3

    const-string v3, "BROKER_SIGNATURE_NOT_SAVED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_SIGNATURE_NOT_SAVED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 78
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x4d

    const-string v15, "Device registration failed"

    move-object/from16 v80, v5

    const-string v5, "BROKER_ACCOUNT_DEVICE_REGISTRY_FAILURE"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_ACCOUNT_DEVICE_REGISTRY_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 79
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x4e

    const-string v15, "Token request after resolving web interrupt failed"

    move-object/from16 v81, v3

    const-string v3, "BROKER_ACCOUNT_FAILED_RESOLVED_INTERRUPT"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_ACCOUNT_FAILED_RESOLVED_INTERRUPT:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 80
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x4f

    const-string v15, "Device does not support the algorithm"

    move-object/from16 v82, v5

    const-string v5, "DEVICE_NO_SUCH_ALGORITHM"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_NO_SUCH_ALGORITHM:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 81
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x50

    const-string v15, "Requested padding is not available"

    move-object/from16 v83, v3

    const-string v3, "DEVICE_ALGORITHM_PADDING_EXCEPTION"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_ALGORITHM_PADDING_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 82
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x51

    const-string v15, "App package name is not found in the package manager"

    move-object/from16 v84, v5

    const-string v5, "APP_PACKAGE_NAME_NOT_FOUND"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->APP_PACKAGE_NAME_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 83
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x52

    const-string v15, "Encryption related error"

    move-object/from16 v85, v3

    const-string v3, "ENCRYPTION_ERROR"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->ENCRYPTION_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 84
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x53

    const-string v15, "Broker activity is not resolved"

    move-object/from16 v86, v5

    const-string v5, "BROKER_ACTIVITY_IS_NOT_RESOLVED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_ACTIVITY_IS_NOT_RESOLVED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 85
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x54

    const-string v15, "Invalid request parameters"

    move-object/from16 v87, v3

    const-string v3, "BROKER_ACTIVITY_INVALID_REQUEST"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_ACTIVITY_INVALID_REQUEST:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 86
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x55

    const-string v15, "Broker could not save the new account"

    move-object/from16 v88, v5

    const-string v5, "BROKER_ACCOUNT_SAVE_FAILED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_ACCOUNT_SAVE_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 87
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x56

    const-string v15, "Broker account does not exist"

    move-object/from16 v89, v3

    const-string v3, "BROKER_ACCOUNT_DOES_NOT_EXIST"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_ACCOUNT_DOES_NOT_EXIST:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 88
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x57

    const-string v15, "Single user is expected"

    move-object/from16 v90, v5

    const-string v5, "BROKER_SINGLE_USER_EXPECTED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_SINGLE_USER_EXPECTED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 89
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x58

    const-string v15, "Key Chain private key exception"

    move-object/from16 v91, v3

    const-string v3, "KEY_CHAIN_PRIVATE_KEY_EXCEPTION"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->KEY_CHAIN_PRIVATE_KEY_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 90
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x59

    const-string v15, "Signature exception"

    move-object/from16 v92, v5

    const-string v5, "SIGNATURE_EXCEPTION"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->SIGNATURE_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 91
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x5a

    const-string v15, "It is failed to create device certificate response"

    move-object/from16 v93, v3

    const-string v3, "DEVICE_CERTIFICATE_RESPONSE_FAILED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_CERTIFICATE_RESPONSE_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 92
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x5b

    const-string v15, "Webview returned Authentication Exception"

    move-object/from16 v94, v5

    const-string v5, "WEBVIEW_RETURNED_AUTHENTICATION_EXCEPTION"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->WEBVIEW_RETURNED_AUTHENTICATION_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 93
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x5c

    const-string v15, "Webview returned invalid or null Authentication Exception"

    move-object/from16 v95, v3

    const-string v3, "WEBVIEW_RETURNED_INVALID_AUTHENTICATION_EXCEPTION"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->WEBVIEW_RETURNED_INVALID_AUTHENTICATION_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 94
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x5d

    const-string v15, "Webview returned empty redirect url"

    move-object/from16 v96, v5

    const-string v5, "WEBVIEW_RETURNED_EMPTY_REDIRECT_URL"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->WEBVIEW_RETURNED_EMPTY_REDIRECT_URL:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 95
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x5e

    const-string v15, "The webview was redirected to an unsafe URL"

    move-object/from16 v97, v3

    const-string v3, "WEBVIEW_REDIRECTURL_NOT_SSL_PROTECTED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->WEBVIEW_REDIRECTURL_NOT_SSL_PROTECTED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 96
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x5f

    const-string v15, "Device certificate API has exception"

    move-object/from16 v98, v5

    const-string v5, "DEVICE_CERTIFICATE_API_EXCEPTION"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_CERTIFICATE_API_EXCEPTION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 97
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x60

    const-string v15, "Device certificate request is valid"

    move-object/from16 v99, v3

    const-string v3, "DEVICE_CERTIFICATE_REQUEST_INVALID"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_CERTIFICATE_REQUEST_INVALID:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 98
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x61

    const-string v15, "Resource is not found in your project. Please include resource files."

    move-object/from16 v100, v5

    const-string v5, "RESOURCE_NOT_FOUND"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->RESOURCE_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 99
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x62

    const-string v15, "Certificate encoding is not generated"

    move-object/from16 v101, v3

    const-string v3, "CERTIFICATE_ENCODING_ERROR"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->CERTIFICATE_ENCODING_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 100
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x63

    const-string v15, "Error in silent token request"

    move-object/from16 v102, v5

    const-string v5, "ERROR_SILENT_REQUEST"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->ERROR_SILENT_REQUEST:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 101
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x64

    const-string v15, "The redirectUri for broker is invalid"

    move-object/from16 v103, v3

    const-string v3, "DEVELOPER_REDIRECTURI_INVALID"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVELOPER_REDIRECTURI_INVALID:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 102
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x65

    const-string v15, "Device challenge failure"

    move-object/from16 v104, v5

    const-string v5, "DEVICE_CHALLENGE_FAILURE"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_CHALLENGE_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 103
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x66

    const-string v15, "Resource authentication challenge failure"

    move-object/from16 v105, v3

    const-string v3, "RESOURCE_AUTHENTICATION_CHALLENGE_FAILURE"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->RESOURCE_AUTHENTICATION_CHALLENGE_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 104
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x67

    const-string v15, "Invalid token cache item"

    move-object/from16 v106, v5

    const-string v5, "INVALID_TOKEN_CACHE_ITEM"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->INVALID_TOKEN_CACHE_ITEM:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 105
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x68

    const-string v15, "Fail to export"

    move-object/from16 v107, v3

    const-string v3, "FAIL_TO_EXPORT"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->FAIL_TO_EXPORT:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 106
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x69

    const-string v15, "Fail to import"

    move-object/from16 v108, v5

    const-string v5, "FAIL_TO_IMPORT"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->FAIL_TO_IMPORT:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 107
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x6a

    const-string v15, "Fail to deserialize because the blob version is incompatible"

    move-object/from16 v109, v3

    const-string v3, "INCOMPATIBLE_BLOB_VERSION"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->INCOMPATIBLE_BLOB_VERSION:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 108
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x6b

    const-string v15, "Token cache item is not found"

    move-object/from16 v110, v5

    const-string v5, "TOKEN_CACHE_ITEM_NOT_FOUND"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->TOKEN_CACHE_ITEM_NOT_FOUND:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 109
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x6c

    const-string v15, "Fail to parse JSON"

    move-object/from16 v111, v3

    const-string v3, "JSON_PARSE_ERROR"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->JSON_PARSE_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 110
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x6d

    const-string v15, "Malformed DRS metadata URL"

    move-object/from16 v112, v5

    const-string v5, "DRS_METADATA_URL_INVALID"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DRS_METADATA_URL_INVALID:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 111
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x6e

    const-string v15, "Enrollment server returned an unrecognized response"

    move-object/from16 v113, v3

    const-string v3, "DRS_FAILED_SERVER_ERROR"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DRS_FAILED_SERVER_ERROR:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 112
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x6f

    const-string v15, "DRS discovery failed: unknown host"

    move-object/from16 v114, v5

    const-string v5, "DRS_DISCOVERY_FAILED_UNKNOWN_HOST"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->DRS_DISCOVERY_FAILED_UNKNOWN_HOST:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 113
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x70

    const-string v15, "Broker app installation started"

    move-object/from16 v115, v3

    const-string v3, "BROKER_APP_INSTALLATION_STARTED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_APP_INSTALLATION_STARTED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 114
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x71

    const-string v15, "Parallel UI requests, cancelling and only one request will be allowed."

    move-object/from16 v116, v5

    const-string v5, "PARALLEL_UI_REQUESTS"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->PARALLEL_UI_REQUESTS:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 115
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x72

    const-string v15, "Unrecognized x-ms-clitelem header version"

    move-object/from16 v117, v3

    const-string v3, "X_MS_CLITELEM_VERSION_UNRECOGNIZED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->X_MS_CLITELEM_VERSION_UNRECOGNIZED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 116
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x73

    const-string v15, "Malformed x-ms-clitelem header"

    move-object/from16 v118, v5

    const-string v5, "X_MS_CLITELEM_MALFORMED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->X_MS_CLITELEM_MALFORMED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 117
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x74

    const-string v15, "WPJ Device Certificate Proxy class was not initialized."

    move-object/from16 v119, v3

    const-string v3, "DEVICE_CERT_PROXY_UNINITIALIZED"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->DEVICE_CERT_PROXY_UNINITIALIZED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 118
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x75

    const-string v15, "Failed to bind the service in broker app"

    move-object/from16 v120, v5

    const-string v5, "BROKER_BIND_SERVICE_FAILED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->BROKER_BIND_SERVICE_FAILED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 119
    new-instance v5, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x76

    const-string v15, "Common core returned an exception code that ADAL cannot parse"

    move-object/from16 v121, v3

    const-string v3, "MAPPING_FAILURE"

    invoke-direct {v5, v3, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->MAPPING_FAILURE:Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 120
    new-instance v3, Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v7, 0x77

    const-string v15, "Device needs to be managed to access the resource"

    move-object/from16 v122, v5

    const-string v5, "MDM_REQUIRED"

    invoke-direct {v3, v5, v7, v15}, Lcom/microsoft/identity/common/adal/internal/ADALError;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->MDM_REQUIRED:Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/16 v5, 0x78

    new-array v5, v5, [Lcom/microsoft/identity/common/adal/internal/ADALError;

    const/4 v7, 0x0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    aput-object v1, v5, v7

    const/4 v7, 0x2

    aput-object v2, v5, v7

    const/4 v7, 0x3

    aput-object v4, v5, v7

    const/4 v4, 0x4

    aput-object v6, v5, v4

    const/4 v4, 0x5

    aput-object v8, v5, v4

    const/4 v4, 0x6

    aput-object v10, v5, v4

    const/4 v4, 0x7

    aput-object v12, v5, v4

    const/16 v4, 0x8

    aput-object v14, v5, v4

    const/16 v4, 0x9

    aput-object v13, v5, v4

    const/16 v4, 0xa

    aput-object v11, v5, v4

    const/16 v4, 0xb

    aput-object v9, v5, v4

    const/16 v4, 0xc

    aput-object v16, v5, v4

    const/16 v4, 0xd

    aput-object v17, v5, v4

    const/16 v4, 0xe

    aput-object v18, v5, v4

    const/16 v4, 0xf

    aput-object v19, v5, v4

    const/16 v4, 0x10

    aput-object v20, v5, v4

    const/16 v4, 0x11

    aput-object v21, v5, v4

    const/16 v4, 0x12

    aput-object v22, v5, v4

    const/16 v4, 0x13

    aput-object v23, v5, v4

    const/16 v4, 0x14

    aput-object v24, v5, v4

    const/16 v4, 0x15

    aput-object v25, v5, v4

    const/16 v4, 0x16

    aput-object v26, v5, v4

    const/16 v4, 0x17

    aput-object v27, v5, v4

    const/16 v4, 0x18

    aput-object v28, v5, v4

    const/16 v4, 0x19

    aput-object v29, v5, v4

    const/16 v4, 0x1a

    aput-object v30, v5, v4

    const/16 v4, 0x1b

    aput-object v31, v5, v4

    const/16 v4, 0x1c

    aput-object v32, v5, v4

    const/16 v4, 0x1d

    aput-object v33, v5, v4

    const/16 v4, 0x1e

    aput-object v34, v5, v4

    const/16 v4, 0x1f

    aput-object v35, v5, v4

    const/16 v4, 0x20

    aput-object v36, v5, v4

    const/16 v4, 0x21

    aput-object v37, v5, v4

    const/16 v4, 0x22

    aput-object v38, v5, v4

    const/16 v4, 0x23

    aput-object v39, v5, v4

    const/16 v4, 0x24

    aput-object v40, v5, v4

    const/16 v4, 0x25

    aput-object v41, v5, v4

    const/16 v4, 0x26

    aput-object v42, v5, v4

    const/16 v4, 0x27

    aput-object v43, v5, v4

    const/16 v4, 0x28

    aput-object v44, v5, v4

    const/16 v4, 0x29

    aput-object v45, v5, v4

    const/16 v4, 0x2a

    aput-object v46, v5, v4

    const/16 v4, 0x2b

    aput-object v47, v5, v4

    const/16 v4, 0x2c

    aput-object v48, v5, v4

    const/16 v4, 0x2d

    aput-object v49, v5, v4

    const/16 v4, 0x2e

    aput-object v50, v5, v4

    const/16 v4, 0x2f

    aput-object v51, v5, v4

    const/16 v4, 0x30

    aput-object v52, v5, v4

    const/16 v4, 0x31

    aput-object v53, v5, v4

    const/16 v4, 0x32

    aput-object v54, v5, v4

    const/16 v4, 0x33

    aput-object v55, v5, v4

    const/16 v4, 0x34

    aput-object v56, v5, v4

    const/16 v4, 0x35

    aput-object v57, v5, v4

    const/16 v4, 0x36

    aput-object v58, v5, v4

    const/16 v4, 0x37

    aput-object v59, v5, v4

    const/16 v4, 0x38

    aput-object v60, v5, v4

    const/16 v4, 0x39

    aput-object v61, v5, v4

    const/16 v4, 0x3a

    aput-object v62, v5, v4

    const/16 v4, 0x3b

    aput-object v63, v5, v4

    const/16 v4, 0x3c

    aput-object v64, v5, v4

    const/16 v4, 0x3d

    aput-object v65, v5, v4

    const/16 v4, 0x3e

    aput-object v66, v5, v4

    const/16 v4, 0x3f

    aput-object v67, v5, v4

    const/16 v4, 0x40

    aput-object v68, v5, v4

    const/16 v4, 0x41

    aput-object v69, v5, v4

    const/16 v4, 0x42

    aput-object v70, v5, v4

    const/16 v4, 0x43

    aput-object v71, v5, v4

    const/16 v4, 0x44

    aput-object v72, v5, v4

    const/16 v4, 0x45

    aput-object v73, v5, v4

    const/16 v4, 0x46

    aput-object v74, v5, v4

    const/16 v4, 0x47

    aput-object v75, v5, v4

    const/16 v4, 0x48

    aput-object v76, v5, v4

    const/16 v4, 0x49

    aput-object v77, v5, v4

    const/16 v4, 0x4a

    aput-object v78, v5, v4

    const/16 v4, 0x4b

    aput-object v79, v5, v4

    const/16 v4, 0x4c

    aput-object v80, v5, v4

    const/16 v4, 0x4d

    aput-object v81, v5, v4

    const/16 v4, 0x4e

    aput-object v82, v5, v4

    const/16 v4, 0x4f

    aput-object v83, v5, v4

    const/16 v4, 0x50

    aput-object v84, v5, v4

    const/16 v4, 0x51

    aput-object v85, v5, v4

    const/16 v4, 0x52

    aput-object v86, v5, v4

    const/16 v4, 0x53

    aput-object v87, v5, v4

    const/16 v4, 0x54

    aput-object v88, v5, v4

    const/16 v4, 0x55

    aput-object v89, v5, v4

    const/16 v4, 0x56

    aput-object v90, v5, v4

    const/16 v4, 0x57

    aput-object v91, v5, v4

    const/16 v4, 0x58

    aput-object v92, v5, v4

    const/16 v4, 0x59

    aput-object v93, v5, v4

    const/16 v4, 0x5a

    aput-object v94, v5, v4

    const/16 v4, 0x5b

    aput-object v95, v5, v4

    const/16 v4, 0x5c

    aput-object v96, v5, v4

    const/16 v4, 0x5d

    aput-object v97, v5, v4

    const/16 v4, 0x5e

    aput-object v98, v5, v4

    const/16 v4, 0x5f

    aput-object v99, v5, v4

    const/16 v4, 0x60

    aput-object v100, v5, v4

    const/16 v4, 0x61

    aput-object v101, v5, v4

    const/16 v4, 0x62

    aput-object v102, v5, v4

    const/16 v4, 0x63

    aput-object v103, v5, v4

    const/16 v4, 0x64

    aput-object v104, v5, v4

    const/16 v4, 0x65

    aput-object v105, v5, v4

    const/16 v4, 0x66

    aput-object v106, v5, v4

    const/16 v4, 0x67

    aput-object v107, v5, v4

    const/16 v4, 0x68

    aput-object v108, v5, v4

    const/16 v4, 0x69

    aput-object v109, v5, v4

    const/16 v4, 0x6a

    aput-object v110, v5, v4

    const/16 v4, 0x6b

    aput-object v111, v5, v4

    const/16 v4, 0x6c

    aput-object v112, v5, v4

    const/16 v4, 0x6d

    aput-object v113, v5, v4

    const/16 v4, 0x6e

    aput-object v114, v5, v4

    const/16 v4, 0x6f

    aput-object v115, v5, v4

    const/16 v4, 0x70

    aput-object v116, v5, v4

    const/16 v4, 0x71

    aput-object v117, v5, v4

    const/16 v4, 0x72

    aput-object v118, v5, v4

    const/16 v4, 0x73

    aput-object v119, v5, v4

    const/16 v4, 0x74

    aput-object v120, v5, v4

    const/16 v4, 0x75

    aput-object v121, v5, v4

    const/16 v4, 0x76

    aput-object v122, v5, v4

    const/16 v4, 0x77

    aput-object v3, v5, v4

    .line 121
    sput-object v5, Lcom/microsoft/identity/common/adal/internal/ADALError;->$VALUES:[Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 122
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/microsoft/identity/common/adal/internal/ADALError;->exceptionMap:Ljava/util/Map;

    const-string v4, "android_keystore_failed"

    move-object/from16 v5, v61

    .line 123
    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "authority_url_not_valid"

    .line 124
    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "authority_validation_not_supported"

    .line 125
    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "decryption_error"

    move-object/from16 v1, v60

    .line 126
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "device_network_not_available"

    move-object/from16 v1, v48

    .line 127
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "device_network_not_available_doze_mode"

    move-object/from16 v1, v40

    .line 128
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "encryption_error"

    move-object/from16 v1, v86

    .line 129
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "invalid_jwt"

    move-object/from16 v1, v112

    .line 130
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "io_error"

    move-object/from16 v4, v21

    .line 131
    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "socket_timeout"

    move-object/from16 v4, v22

    .line 132
    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "json_parse_failure"

    .line 133
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "malformed_url"

    .line 134
    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "multiple_matching_tokens_detected"

    move-object/from16 v1, v37

    .line 135
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "user_mismatch"

    .line 136
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "no_such_algorithm"

    move-object/from16 v1, v83

    .line 137
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "unsupported_encoding"

    move-object/from16 v1, v56

    .line 138
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "access_denied"

    move-object/from16 v1, v29

    .line 139
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "invalid_instance"

    .line 140
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "invalid_request"

    move-object/from16 v1, v19

    .line 141
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "request_timeout"

    move-object/from16 v1, v20

    .line 142
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "service_not_available"

    .line 143
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "unknown_error"

    .line 144
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Package name is not resolved"

    move-object/from16 v1, v75

    .line 145
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Calling app could not be verified"

    move-object/from16 v1, v78

    .line 146
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "App package name is not found in the package manager"

    move-object/from16 v1, v85

    .line 147
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lcom/microsoft/identity/common/adal/internal/ADALError;->mDescription:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/identity/common/adal/internal/ADALError;
    .locals 1

    .line 1
    const-class v0, Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lcom/microsoft/identity/common/adal/internal/ADALError;
    .locals 1

    .line 1
    sget-object v0, Lcom/microsoft/identity/common/adal/internal/ADALError;->$VALUES:[Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/microsoft/identity/common/adal/internal/ADALError;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/microsoft/identity/common/adal/internal/ADALError;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/identity/common/adal/internal/ADALError;->mDescription:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getLocalizedDescription(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v1, Landroid/content/res/Resources;

    .line 12
    .line 13
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    invoke-direct {v1, v2, v3, v0}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const-string/jumbo v2, "string"

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-virtual {v1, v0, v2, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    return-object p1

    .line 48
    :cond_0
    iget-object p1, p0, Lcom/microsoft/identity/common/adal/internal/ADALError;->mDescription:Ljava/lang/String;

    .line 49
    .line 50
    return-object p1
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

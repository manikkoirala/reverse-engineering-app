.class public Lcom/microsoft/identity/common/internal/broker/BrokerResult;
.super Ljava/lang/Object;
.source "BrokerResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;
    }
.end annotation


# instance fields
.field private O8:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "home_account_id"
    .end annotation
.end field

.field private O8ooOoo〇:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "http_response_body"
    .end annotation
.end field

.field private OO0o〇〇:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "environment"
    .end annotation
.end field

.field private OO0o〇〇〇〇0:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "token_type"
    .end annotation
.end field

.field private final OOO〇O0:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "broker_exception_type"
    .end annotation
.end field

.field private Oo08:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "local_account_id"
    .end annotation
.end field

.field private OoO8:Z
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "success"
    .end annotation
.end field

.field private Oooo8o0〇:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tenant_id"
    .end annotation
.end field

.field private O〇8O8〇008:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "http_response_headers"
    .end annotation
.end field

.field private o800o8O:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "serviced_from_cache"
    .end annotation
.end field

.field private oO80:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "family_id"
    .end annotation
.end field

.field private oo88o8O:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "broker_error_message"
    .end annotation
.end field

.field private o〇0:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "username"
    .end annotation
.end field

.field private o〇O8〇〇o:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "oauth_sub_error"
    .end annotation
.end field

.field private final o〇〇0〇:Ljava/util/List;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tenant_profile_cache_records"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/microsoft/identity/common/java/cache/ICacheRecord;",
            ">;"
        }
    .end annotation
.end field

.field private 〇00:I
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "http_response_code"
    .end annotation
.end field

.field private 〇0000OOO:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cli_telem_suberror_code"
    .end annotation
.end field

.field private 〇080:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "access_token"
    .end annotation
.end field

.field private 〇0〇O0088o:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "refresh_token_age"
    .end annotation
.end field

.field private 〇80〇808〇O:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "scopes"
    .end annotation
.end field

.field private 〇8o8o〇:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "client_info"
    .end annotation
.end field

.field private 〇O00:J
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cached_at"
    .end annotation
.end field

.field private 〇O888o0o:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "broker_error_code"
    .end annotation
.end field

.field private 〇O8o08O:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "authority"
    .end annotation
.end field

.field private 〇O〇:J
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ext_expires_on"
    .end annotation
.end field

.field private 〇o00〇〇Oo:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_token"
    .end annotation
.end field

.field private 〇oOO8O8:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cli_telem_error_code"
    .end annotation
.end field

.field private 〇oo〇:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "correlation_id"
    .end annotation
.end field

.field private 〇o〇:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "refresh_token"
    .end annotation
.end field

.field private 〇〇808〇:J
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "expires_on"
    .end annotation
.end field

.field private 〇〇888:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "client_id"
    .end annotation
.end field

.field private 〇〇8O0〇8:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "spe_ring"
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)V
    .locals 2
    .param p1    # Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇080(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇080:Ljava/lang/String;

    .line 4
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇o00〇〇Oo(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇o00〇〇Oo:Ljava/lang/String;

    .line 5
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->OO0o〇〇(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇o〇:Ljava/lang/String;

    .line 6
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇oo〇(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->O8:Ljava/lang/String;

    .line 7
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->O〇8O8〇008(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->Oo08:Ljava/lang/String;

    .line 8
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->O8ooOoo〇(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->o〇0:Ljava/lang/String;

    .line 9
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇oOO8O8(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 10
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇0000OOO(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇〇888:Ljava/lang/String;

    .line 11
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->o〇〇0〇(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->oO80:Ljava/lang/String;

    .line 12
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->OOO〇O0(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇80〇808〇O:Ljava/lang/String;

    .line 13
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇o〇(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇8o8o〇:Ljava/lang/String;

    .line 14
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->O8(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇O8o08O:Ljava/lang/String;

    .line 15
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->Oo08(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->OO0o〇〇:Ljava/lang/String;

    .line 16
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->o〇0(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->Oooo8o0〇:Ljava/lang/String;

    .line 17
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇〇888(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇〇808〇:J

    .line 18
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->oO80(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇O〇:J

    .line 19
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇80〇808〇O(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇O00:J

    .line 20
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->OO0o〇〇〇〇0(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇〇8O0〇8:Ljava/lang/String;

    .line 21
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇8o8o〇(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇0〇O0088o:Ljava/lang/String;

    .line 22
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇O8o08O(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->OoO8:Z

    .line 23
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->Oooo8o0〇(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->o〇〇0〇:Ljava/util/List;

    .line 24
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇〇808〇(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->o800o8O:Z

    .line 25
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇O〇(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇O888o0o:Ljava/lang/String;

    .line 26
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇O00(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->oo88o8O:Ljava/lang/String;

    .line 27
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇〇8O0〇8(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇oo〇:Ljava/lang/String;

    .line 28
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇0〇O0088o(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->o〇O8〇〇o:Ljava/lang/String;

    .line 29
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->OoO8(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)I

    move-result v0

    iput v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇00:I

    .line 30
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->o800o8O(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->O8ooOoo〇:Ljava/lang/String;

    .line 31
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇O888o0o(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->O〇8O8〇008:Ljava/lang/String;

    .line 32
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->oo88o8O(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇oOO8O8:Ljava/lang/String;

    .line 33
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->o〇O8〇〇o(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->〇0000OOO:Ljava/lang/String;

    .line 34
    invoke-static {p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;->〇00(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/microsoft/identity/common/internal/broker/BrokerResult;->OOO〇O0:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;Lcom/microsoft/identity/common/internal/broker/BrokerResult$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/microsoft/identity/common/internal/broker/BrokerResult;-><init>(Lcom/microsoft/identity/common/internal/broker/BrokerResult$Builder;)V

    return-void
.end method

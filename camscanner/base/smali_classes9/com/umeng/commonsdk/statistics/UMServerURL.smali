.class public Lcom/umeng/commonsdk/statistics/UMServerURL;
.super Ljava/lang/Object;
.source "UMServerURL.java"


# static fields
.field public static DEFAULT_URL:Ljava/lang/String; = "https://ulogs.umeng.com"

.field public static OVERSEA_DEFAULT_URL:Ljava/lang/String; = "https://alogus.umeng.com"

.field public static OVERSEA_SECONDARY_URL:Ljava/lang/String; = "https://alogsus.umeng.com"

.field public static PATH_ANALYTICS:Ljava/lang/String; = "unify_logs"

.field public static PATH_INNER:Ljava/lang/String; = "unify_logs"

.field public static PATH_INNER_CRASH:Ljava/lang/String; = "pikachu"

.field public static PATH_PUSH_LAUNCH:Ljava/lang/String; = "umpx_push_launch"

.field public static PATH_PUSH_LOG:Ljava/lang/String; = "umpx_push_logs"

.field public static PATH_PUSH_REGIST:Ljava/lang/String; = "umpx_push_register"

.field public static PATH_SHARE:Ljava/lang/String; = "umpx_share"

.field public static SECONDARY_URL:Ljava/lang/String; = "https://ulogs.umengcloud.com"

.field public static SILENT_HEART_BEAT:Ljava/lang/String; = "heartbeat"

.field public static ZCFG_PATH:Ljava/lang/String; = "zcfg"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

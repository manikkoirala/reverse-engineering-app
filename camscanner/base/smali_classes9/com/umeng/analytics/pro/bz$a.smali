.class Lcom/umeng/analytics/pro/bz$a;
.super Lcom/umeng/analytics/pro/cy;
.source "TUnion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/umeng/analytics/pro/bz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/umeng/analytics/pro/cy<",
        "Lcom/umeng/analytics/pro/bz;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/umeng/analytics/pro/cy;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/umeng/analytics/pro/bz$1;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/umeng/analytics/pro/bz$a;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic a(Lcom/umeng/analytics/pro/co;Lcom/umeng/analytics/pro/bp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/umeng/analytics/pro/bv;
        }
    .end annotation

    .line 1
    check-cast p2, Lcom/umeng/analytics/pro/bz;

    invoke-virtual {p0, p1, p2}, Lcom/umeng/analytics/pro/bz$a;->b(Lcom/umeng/analytics/pro/co;Lcom/umeng/analytics/pro/bz;)V

    return-void
.end method

.method public a(Lcom/umeng/analytics/pro/co;Lcom/umeng/analytics/pro/bz;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/umeng/analytics/pro/bv;
        }
    .end annotation

    const/4 v0, 0x0

    .line 2
    iput-object v0, p2, Lcom/umeng/analytics/pro/bz;->b:Lcom/umeng/analytics/pro/bw;

    .line 3
    iput-object v0, p2, Lcom/umeng/analytics/pro/bz;->a:Ljava/lang/Object;

    .line 4
    invoke-virtual {p1}, Lcom/umeng/analytics/pro/co;->j()Lcom/umeng/analytics/pro/ct;

    .line 5
    invoke-virtual {p1}, Lcom/umeng/analytics/pro/co;->l()Lcom/umeng/analytics/pro/cj;

    move-result-object v0

    .line 6
    invoke-virtual {p2, p1, v0}, Lcom/umeng/analytics/pro/bz;->a(Lcom/umeng/analytics/pro/co;Lcom/umeng/analytics/pro/cj;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p2, Lcom/umeng/analytics/pro/bz;->a:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 7
    iget-short v0, v0, Lcom/umeng/analytics/pro/cj;->c:S

    invoke-virtual {p2, v0}, Lcom/umeng/analytics/pro/bz;->a(S)Lcom/umeng/analytics/pro/bw;

    move-result-object v0

    iput-object v0, p2, Lcom/umeng/analytics/pro/bz;->b:Lcom/umeng/analytics/pro/bw;

    .line 8
    :cond_0
    invoke-virtual {p1}, Lcom/umeng/analytics/pro/co;->m()V

    .line 9
    invoke-virtual {p1}, Lcom/umeng/analytics/pro/co;->l()Lcom/umeng/analytics/pro/cj;

    .line 10
    invoke-virtual {p1}, Lcom/umeng/analytics/pro/co;->k()V

    return-void
.end method

.method public synthetic b(Lcom/umeng/analytics/pro/co;Lcom/umeng/analytics/pro/bp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/umeng/analytics/pro/bv;
        }
    .end annotation

    .line 1
    check-cast p2, Lcom/umeng/analytics/pro/bz;

    invoke-virtual {p0, p1, p2}, Lcom/umeng/analytics/pro/bz$a;->a(Lcom/umeng/analytics/pro/co;Lcom/umeng/analytics/pro/bz;)V

    return-void
.end method

.method public b(Lcom/umeng/analytics/pro/co;Lcom/umeng/analytics/pro/bz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/umeng/analytics/pro/bv;
        }
    .end annotation

    .line 2
    invoke-virtual {p2}, Lcom/umeng/analytics/pro/bz;->a()Lcom/umeng/analytics/pro/bw;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/umeng/analytics/pro/bz;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p2}, Lcom/umeng/analytics/pro/bz;->d()Lcom/umeng/analytics/pro/ct;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/umeng/analytics/pro/co;->a(Lcom/umeng/analytics/pro/ct;)V

    .line 4
    iget-object v0, p2, Lcom/umeng/analytics/pro/bz;->b:Lcom/umeng/analytics/pro/bw;

    invoke-virtual {p2, v0}, Lcom/umeng/analytics/pro/bz;->c(Lcom/umeng/analytics/pro/bw;)Lcom/umeng/analytics/pro/cj;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/umeng/analytics/pro/co;->a(Lcom/umeng/analytics/pro/cj;)V

    .line 5
    invoke-virtual {p2, p1}, Lcom/umeng/analytics/pro/bz;->a(Lcom/umeng/analytics/pro/co;)V

    .line 6
    invoke-virtual {p1}, Lcom/umeng/analytics/pro/co;->c()V

    .line 7
    invoke-virtual {p1}, Lcom/umeng/analytics/pro/co;->d()V

    .line 8
    invoke-virtual {p1}, Lcom/umeng/analytics/pro/co;->b()V

    return-void

    .line 9
    :cond_0
    new-instance p1, Lcom/umeng/analytics/pro/cp;

    const-string p2, "Cannot write a TUnion with no set value!"

    invoke-direct {p1, p2}, Lcom/umeng/analytics/pro/cp;-><init>(Ljava/lang/String;)V

    throw p1
.end method

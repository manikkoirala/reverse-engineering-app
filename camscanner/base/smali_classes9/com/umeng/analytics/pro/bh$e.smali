.class public final enum Lcom/umeng/analytics/pro/bh$e;
.super Ljava/lang/Enum;
.source "UMEnvelope.java"

# interfaces
.implements Lcom/umeng/analytics/pro/bw;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/umeng/analytics/pro/bh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/umeng/analytics/pro/bh$e;",
        ">;",
        "Lcom/umeng/analytics/pro/bw;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/umeng/analytics/pro/bh$e;

.field public static final enum b:Lcom/umeng/analytics/pro/bh$e;

.field public static final enum c:Lcom/umeng/analytics/pro/bh$e;

.field public static final enum d:Lcom/umeng/analytics/pro/bh$e;

.field public static final enum e:Lcom/umeng/analytics/pro/bh$e;

.field public static final enum f:Lcom/umeng/analytics/pro/bh$e;

.field public static final enum g:Lcom/umeng/analytics/pro/bh$e;

.field public static final enum h:Lcom/umeng/analytics/pro/bh$e;

.field public static final enum i:Lcom/umeng/analytics/pro/bh$e;

.field public static final enum j:Lcom/umeng/analytics/pro/bh$e;

.field private static final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/umeng/analytics/pro/bh$e;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic n:[Lcom/umeng/analytics/pro/bh$e;


# instance fields
.field private final l:S

.field private final m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/umeng/analytics/pro/bh$e;

    .line 2
    .line 3
    const-string/jumbo v1, "version"

    .line 4
    .line 5
    .line 6
    const-string v2, "VERSION"

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x1

    .line 10
    invoke-direct {v0, v2, v3, v4, v1}, Lcom/umeng/analytics/pro/bh$e;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/umeng/analytics/pro/bh$e;->a:Lcom/umeng/analytics/pro/bh$e;

    .line 14
    .line 15
    new-instance v1, Lcom/umeng/analytics/pro/bh$e;

    .line 16
    .line 17
    const-string v2, "address"

    .line 18
    .line 19
    const-string v5, "ADDRESS"

    .line 20
    .line 21
    const/4 v6, 0x2

    .line 22
    invoke-direct {v1, v5, v4, v6, v2}, Lcom/umeng/analytics/pro/bh$e;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    .line 23
    .line 24
    .line 25
    sput-object v1, Lcom/umeng/analytics/pro/bh$e;->b:Lcom/umeng/analytics/pro/bh$e;

    .line 26
    .line 27
    new-instance v2, Lcom/umeng/analytics/pro/bh$e;

    .line 28
    .line 29
    const-string/jumbo v5, "signature"

    .line 30
    .line 31
    .line 32
    const-string v7, "SIGNATURE"

    .line 33
    .line 34
    const/4 v8, 0x3

    .line 35
    invoke-direct {v2, v7, v6, v8, v5}, Lcom/umeng/analytics/pro/bh$e;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    .line 36
    .line 37
    .line 38
    sput-object v2, Lcom/umeng/analytics/pro/bh$e;->c:Lcom/umeng/analytics/pro/bh$e;

    .line 39
    .line 40
    new-instance v5, Lcom/umeng/analytics/pro/bh$e;

    .line 41
    .line 42
    const-string v7, "serial_num"

    .line 43
    .line 44
    const-string v9, "SERIAL_NUM"

    .line 45
    .line 46
    const/4 v10, 0x4

    .line 47
    invoke-direct {v5, v9, v8, v10, v7}, Lcom/umeng/analytics/pro/bh$e;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    .line 48
    .line 49
    .line 50
    sput-object v5, Lcom/umeng/analytics/pro/bh$e;->d:Lcom/umeng/analytics/pro/bh$e;

    .line 51
    .line 52
    new-instance v7, Lcom/umeng/analytics/pro/bh$e;

    .line 53
    .line 54
    const-string/jumbo v9, "ts_secs"

    .line 55
    .line 56
    .line 57
    const-string v11, "TS_SECS"

    .line 58
    .line 59
    const/4 v12, 0x5

    .line 60
    invoke-direct {v7, v11, v10, v12, v9}, Lcom/umeng/analytics/pro/bh$e;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    .line 61
    .line 62
    .line 63
    sput-object v7, Lcom/umeng/analytics/pro/bh$e;->e:Lcom/umeng/analytics/pro/bh$e;

    .line 64
    .line 65
    new-instance v9, Lcom/umeng/analytics/pro/bh$e;

    .line 66
    .line 67
    const-string v11, "length"

    .line 68
    .line 69
    const-string v13, "LENGTH"

    .line 70
    .line 71
    const/4 v14, 0x6

    .line 72
    invoke-direct {v9, v13, v12, v14, v11}, Lcom/umeng/analytics/pro/bh$e;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    .line 73
    .line 74
    .line 75
    sput-object v9, Lcom/umeng/analytics/pro/bh$e;->f:Lcom/umeng/analytics/pro/bh$e;

    .line 76
    .line 77
    new-instance v11, Lcom/umeng/analytics/pro/bh$e;

    .line 78
    .line 79
    const-string v13, "entity"

    .line 80
    .line 81
    const-string v15, "ENTITY"

    .line 82
    .line 83
    const/4 v12, 0x7

    .line 84
    invoke-direct {v11, v15, v14, v12, v13}, Lcom/umeng/analytics/pro/bh$e;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    .line 85
    .line 86
    .line 87
    sput-object v11, Lcom/umeng/analytics/pro/bh$e;->g:Lcom/umeng/analytics/pro/bh$e;

    .line 88
    .line 89
    new-instance v13, Lcom/umeng/analytics/pro/bh$e;

    .line 90
    .line 91
    const-string v15, "guid"

    .line 92
    .line 93
    const-string v14, "GUID"

    .line 94
    .line 95
    const/16 v10, 0x8

    .line 96
    .line 97
    invoke-direct {v13, v14, v12, v10, v15}, Lcom/umeng/analytics/pro/bh$e;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    .line 98
    .line 99
    .line 100
    sput-object v13, Lcom/umeng/analytics/pro/bh$e;->h:Lcom/umeng/analytics/pro/bh$e;

    .line 101
    .line 102
    new-instance v14, Lcom/umeng/analytics/pro/bh$e;

    .line 103
    .line 104
    const-string v15, "checksum"

    .line 105
    .line 106
    const-string v12, "CHECKSUM"

    .line 107
    .line 108
    const/16 v8, 0x9

    .line 109
    .line 110
    invoke-direct {v14, v12, v10, v8, v15}, Lcom/umeng/analytics/pro/bh$e;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    .line 111
    .line 112
    .line 113
    sput-object v14, Lcom/umeng/analytics/pro/bh$e;->i:Lcom/umeng/analytics/pro/bh$e;

    .line 114
    .line 115
    new-instance v12, Lcom/umeng/analytics/pro/bh$e;

    .line 116
    .line 117
    const-string v15, "codex"

    .line 118
    .line 119
    const-string v10, "CODEX"

    .line 120
    .line 121
    const/16 v6, 0xa

    .line 122
    .line 123
    invoke-direct {v12, v10, v8, v6, v15}, Lcom/umeng/analytics/pro/bh$e;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    .line 124
    .line 125
    .line 126
    sput-object v12, Lcom/umeng/analytics/pro/bh$e;->j:Lcom/umeng/analytics/pro/bh$e;

    .line 127
    .line 128
    new-array v6, v6, [Lcom/umeng/analytics/pro/bh$e;

    .line 129
    .line 130
    aput-object v0, v6, v3

    .line 131
    .line 132
    aput-object v1, v6, v4

    .line 133
    .line 134
    const/4 v0, 0x2

    .line 135
    aput-object v2, v6, v0

    .line 136
    .line 137
    const/4 v0, 0x3

    .line 138
    aput-object v5, v6, v0

    .line 139
    .line 140
    const/4 v0, 0x4

    .line 141
    aput-object v7, v6, v0

    .line 142
    .line 143
    const/4 v0, 0x5

    .line 144
    aput-object v9, v6, v0

    .line 145
    .line 146
    const/4 v0, 0x6

    .line 147
    aput-object v11, v6, v0

    .line 148
    .line 149
    const/4 v0, 0x7

    .line 150
    aput-object v13, v6, v0

    .line 151
    .line 152
    const/16 v0, 0x8

    .line 153
    .line 154
    aput-object v14, v6, v0

    .line 155
    .line 156
    aput-object v12, v6, v8

    .line 157
    .line 158
    sput-object v6, Lcom/umeng/analytics/pro/bh$e;->n:[Lcom/umeng/analytics/pro/bh$e;

    .line 159
    .line 160
    new-instance v0, Ljava/util/HashMap;

    .line 161
    .line 162
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 163
    .line 164
    .line 165
    sput-object v0, Lcom/umeng/analytics/pro/bh$e;->k:Ljava/util/Map;

    .line 166
    .line 167
    const-class v0, Lcom/umeng/analytics/pro/bh$e;

    .line 168
    .line 169
    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    .line 170
    .line 171
    .line 172
    move-result-object v0

    .line 173
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 174
    .line 175
    .line 176
    move-result-object v0

    .line 177
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 178
    .line 179
    .line 180
    move-result v1

    .line 181
    if-eqz v1, :cond_0

    .line 182
    .line 183
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 184
    .line 185
    .line 186
    move-result-object v1

    .line 187
    check-cast v1, Lcom/umeng/analytics/pro/bh$e;

    .line 188
    .line 189
    sget-object v2, Lcom/umeng/analytics/pro/bh$e;->k:Ljava/util/Map;

    .line 190
    .line 191
    invoke-virtual {v1}, Lcom/umeng/analytics/pro/bh$e;->b()Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v3

    .line 195
    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    .line 197
    .line 198
    goto :goto_0

    .line 199
    :cond_0
    return-void
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method private constructor <init>(Ljava/lang/String;ISLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-short p3, p0, Lcom/umeng/analytics/pro/bh$e;->l:S

    .line 5
    .line 6
    iput-object p4, p0, Lcom/umeng/analytics/pro/bh$e;->m:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method public static a(I)Lcom/umeng/analytics/pro/bh$e;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 1
    :pswitch_0
    sget-object p0, Lcom/umeng/analytics/pro/bh$e;->j:Lcom/umeng/analytics/pro/bh$e;

    return-object p0

    .line 2
    :pswitch_1
    sget-object p0, Lcom/umeng/analytics/pro/bh$e;->i:Lcom/umeng/analytics/pro/bh$e;

    return-object p0

    .line 3
    :pswitch_2
    sget-object p0, Lcom/umeng/analytics/pro/bh$e;->h:Lcom/umeng/analytics/pro/bh$e;

    return-object p0

    .line 4
    :pswitch_3
    sget-object p0, Lcom/umeng/analytics/pro/bh$e;->g:Lcom/umeng/analytics/pro/bh$e;

    return-object p0

    .line 5
    :pswitch_4
    sget-object p0, Lcom/umeng/analytics/pro/bh$e;->f:Lcom/umeng/analytics/pro/bh$e;

    return-object p0

    .line 6
    :pswitch_5
    sget-object p0, Lcom/umeng/analytics/pro/bh$e;->e:Lcom/umeng/analytics/pro/bh$e;

    return-object p0

    .line 7
    :pswitch_6
    sget-object p0, Lcom/umeng/analytics/pro/bh$e;->d:Lcom/umeng/analytics/pro/bh$e;

    return-object p0

    .line 8
    :pswitch_7
    sget-object p0, Lcom/umeng/analytics/pro/bh$e;->c:Lcom/umeng/analytics/pro/bh$e;

    return-object p0

    .line 9
    :pswitch_8
    sget-object p0, Lcom/umeng/analytics/pro/bh$e;->b:Lcom/umeng/analytics/pro/bh$e;

    return-object p0

    .line 10
    :pswitch_9
    sget-object p0, Lcom/umeng/analytics/pro/bh$e;->a:Lcom/umeng/analytics/pro/bh$e;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Lcom/umeng/analytics/pro/bh$e;
    .locals 1

    .line 11
    sget-object v0, Lcom/umeng/analytics/pro/bh$e;->k:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/umeng/analytics/pro/bh$e;

    return-object p0
.end method

.method public static b(I)Lcom/umeng/analytics/pro/bh$e;
    .locals 3

    .line 1
    invoke-static {p0}, Lcom/umeng/analytics/pro/bh$e;->a(I)Lcom/umeng/analytics/pro/bh$e;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 2
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, " doesn\'t exist!"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/umeng/analytics/pro/bh$e;
    .locals 1

    .line 1
    const-class v0, Lcom/umeng/analytics/pro/bh$e;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/umeng/analytics/pro/bh$e;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lcom/umeng/analytics/pro/bh$e;
    .locals 1

    .line 1
    sget-object v0, Lcom/umeng/analytics/pro/bh$e;->n:[Lcom/umeng/analytics/pro/bh$e;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/umeng/analytics/pro/bh$e;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/umeng/analytics/pro/bh$e;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public a()S
    .locals 1

    .line 12
    iget-short v0, p0, Lcom/umeng/analytics/pro/bh$e;->l:S

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/umeng/analytics/pro/bh$e;->m:Ljava/lang/String;

    return-object v0
.end method

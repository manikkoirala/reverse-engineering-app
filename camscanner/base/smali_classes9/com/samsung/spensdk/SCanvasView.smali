.class public Lcom/samsung/spensdk/SCanvasView;
.super Lcom/samsung/spen/a/e/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/spensdk/SCanvasView$OnFileProcessingProgressListener;,
        Lcom/samsung/spensdk/SCanvasView$OnPlayCompleteListener;,
        Lcom/samsung/spensdk/SCanvasView$OnPlayProgressChangeListener;
    }
.end annotation


# instance fields
.field O008o8oo:Lcom/samsung/spensdk/applistener/SCanvasInitializeListener;

.field private O008oO0:Lcom/samsung/spen/a/h/c;

.field O00〇o00:Lcom/samsung/spensdk/applistener/FileProcessListener;

.field private O08o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private O0oO:Lcom/samsung/spen/a/a/b;

.field private O0〇8〇:Z

.field O0〇O80ooo:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

.field private O80:I

.field O8o:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

.field O8o0〇:Lcom/samsung/spensdk/applistener/AnimationProcessListener;

.field private O8〇:Landroid/view/ViewGroup;

.field OO00〇0o〇〇:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

.field private OO0〇O:Lcom/samsung/spen/a/e/c;

.field private OOO0o〇:F

.field private OO〇80oO〇:Z

.field private Oo0O〇8800:Landroid/content/Context;

.field private OoOO〇:I

.field OooO〇080:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

.field Oo〇〇〇〇:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

.field O〇00o08:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

.field O〇88:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

.field O〇O〇88O8O:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

.field O〇〇O:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

.field o00o0O〇〇o:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

.field o088〇〇:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

.field o0o〇〇〇8o:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

.field o0〇OO008O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

.field private o8oo0OOO:Z

.field o8〇O〇0O0〇:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

.field oO0o:Lcom/samsung/spensdk/applistener/FileProcessListener;

.field private oO8o〇08〇:Lcom/samsung/spen/a/g/a;

.field oOOO0:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

.field private oOO〇0o8〇:Landroid/graphics/drawable/Drawable;

.field private oo0O:I

.field private ooo008:I

.field private oooo800〇〇:Z

.field private oo〇O0o〇:Landroid/view/ViewGroup;

.field private o〇O80o8OO:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o〇Oo:Z

.field private o〇o8〇〇O:Lcom/samsung/samm/lib/a;

.field private o〇oO08〇o0:Z

.field o〇〇8〇〇:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

.field private 〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

.field 〇0o:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

.field private 〇0o88O:Z

.field private 〇0oO:F

.field private 〇0o〇o:Lcom/samsung/samm/common/SOptionSCanvas;

.field 〇0〇:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

.field 〇0〇8o〇:Lcom/samsung/sdraw/CanvasView$OnSettingViewShowListener;

.field private 〇0〇o8〇:Lcom/samsung/spen/a/b/b;

.field private 〇80O80O〇0:Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;

.field 〇88:Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;

.field 〇8o80O:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

.field 〇8〇8o00:Lcom/samsung/spensdk/applistener/SCanvasInitializeListener;

.field private 〇8〇o〇OoO8:Z

.field private 〇8〇〇8〇8:Ljava/lang/String;

.field 〇O8〇〇o8〇:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

.field 〇O〇〇〇:Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;

.field 〇o88〇O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

.field 〇oO〇:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

.field 〇〇08〇0oo0:Lcom/samsung/spensdk/applistener/AnimationProcessListener;

.field private 〇〇8o0OOOo:Lcom/samsung/spen/a/f/a;

.field private 〇〇O:Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/samsung/spen/a/e/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 2
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 4
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 5
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 6
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

    .line 7
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->oO8o〇08〇:Lcom/samsung/spen/a/g/a;

    .line 8
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->O0oO:Lcom/samsung/spen/a/a/b;

    .line 9
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇8o0OOOo:Lcom/samsung/spen/a/f/a;

    .line 10
    iput-object p0, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 11
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o〇o:Lcom/samsung/samm/common/SOptionSCanvas;

    const/4 v0, 0x0

    .line 12
    iput v0, p0, Lcom/samsung/spensdk/SCanvasView;->ooo008:I

    .line 13
    iput v0, p0, Lcom/samsung/spensdk/SCanvasView;->oo0O:I

    const/4 v1, 0x1

    .line 14
    iput-boolean v1, p0, Lcom/samsung/spensdk/SCanvasView;->oooo800〇〇:Z

    .line 15
    iput-boolean v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇8〇o〇OoO8:Z

    .line 16
    iput-boolean v0, p0, Lcom/samsung/spensdk/SCanvasView;->OO〇80oO〇:Z

    .line 17
    iput-boolean v0, p0, Lcom/samsung/spensdk/SCanvasView;->O0〇8〇:Z

    .line 18
    iput-boolean v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇oO08〇o0:Z

    .line 19
    iput-boolean v1, p0, Lcom/samsung/spensdk/SCanvasView;->o8oo0OOO:Z

    .line 20
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇8〇〇8〇8:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->O8〇:Landroid/view/ViewGroup;

    .line 22
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->oo〇O0o〇:Landroid/view/ViewGroup;

    .line 23
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/samsung/spensdk/SCanvasView;->O08o:Ljava/util/HashMap;

    .line 24
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/samsung/spensdk/SCanvasView;->o〇O80o8OO:Ljava/util/HashMap;

    .line 25
    iput-boolean v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o88O:Z

    .line 26
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$1;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$1;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇8〇8o00:Lcom/samsung/spensdk/applistener/SCanvasInitializeListener;

    .line 27
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$12;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$12;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O008o8oo:Lcom/samsung/spensdk/applistener/SCanvasInitializeListener;

    .line 28
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇80O80O〇0:Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;

    .line 29
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$16;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$16;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇O:Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;

    .line 30
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$17;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$17;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->oOOO0:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

    .line 31
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$18;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$18;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O〇00o08:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

    .line 32
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$19;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$19;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇o88〇O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

    .line 33
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$20;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$20;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->o0〇OO008O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

    .line 34
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$21;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$21;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O〇88:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

    .line 35
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$22;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$22;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->o〇〇8〇〇:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

    .line 36
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$2;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$2;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇O〇〇〇:Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;

    .line 37
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$3;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$3;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇oO〇:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

    .line 38
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$4;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$4;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O0〇O80ooo:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

    .line 39
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->o8〇O〇0O0〇:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

    .line 40
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$5;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$5;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

    .line 41
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇8o〇:Lcom/samsung/sdraw/CanvasView$OnSettingViewShowListener;

    .line 42
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇8o80O:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

    .line 43
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$6;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$6;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->OO00〇0o〇〇:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

    .line 44
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->o00o0O〇〇o:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

    .line 45
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$7;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$7;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O〇〇O:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

    .line 46
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

    .line 47
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$8;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$8;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->Oo〇〇〇〇:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

    .line 48
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇O8〇〇o8〇:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

    .line 49
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$9;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$9;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O8o:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

    .line 50
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->O00〇o00:Lcom/samsung/spensdk/applistener/FileProcessListener;

    .line 51
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$10;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$10;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->oO0o:Lcom/samsung/spensdk/applistener/FileProcessListener;

    .line 52
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇08〇0oo0:Lcom/samsung/spensdk/applistener/AnimationProcessListener;

    .line 53
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$11;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$11;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O8o0〇:Lcom/samsung/spensdk/applistener/AnimationProcessListener;

    .line 54
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->o088〇〇:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

    .line 55
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$13;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$13;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->o0o〇〇〇8o:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

    .line 56
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->O〇O〇88O8O:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

    .line 57
    new-instance v1, Lcom/samsung/spensdk/SCanvasView$14;

    invoke-direct {v1, p0}, Lcom/samsung/spensdk/SCanvasView$14;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->OooO〇080:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

    const/4 v1, 0x2

    .line 58
    iput v1, p0, Lcom/samsung/spensdk/SCanvasView;->OoOO〇:I

    const/4 v1, -0x1

    .line 59
    iput v1, p0, Lcom/samsung/spensdk/SCanvasView;->O80:I

    .line 60
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->oOO〇0o8〇:Landroid/graphics/drawable/Drawable;

    .line 61
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇88:Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;

    .line 62
    iput-boolean v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇Oo:Z

    const/4 p2, 0x0

    .line 63
    iput p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0oO:F

    .line 64
    iput p2, p0, Lcom/samsung/spensdk/SCanvasView;->OOO0o〇:F

    .line 65
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 66
    iget-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇O:Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;

    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setOnInitializeFinishListener(Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/spen/a/e/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    .line 68
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 69
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 70
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 71
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 72
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

    .line 73
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->oO8o〇08〇:Lcom/samsung/spen/a/g/a;

    .line 74
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->O0oO:Lcom/samsung/spen/a/a/b;

    .line 75
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇8o0OOOo:Lcom/samsung/spen/a/f/a;

    .line 76
    iput-object p0, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 77
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o〇o:Lcom/samsung/samm/common/SOptionSCanvas;

    const/4 p3, 0x0

    .line 78
    iput p3, p0, Lcom/samsung/spensdk/SCanvasView;->ooo008:I

    .line 79
    iput p3, p0, Lcom/samsung/spensdk/SCanvasView;->oo0O:I

    const/4 v0, 0x1

    .line 80
    iput-boolean v0, p0, Lcom/samsung/spensdk/SCanvasView;->oooo800〇〇:Z

    .line 81
    iput-boolean v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇8〇o〇OoO8:Z

    .line 82
    iput-boolean p3, p0, Lcom/samsung/spensdk/SCanvasView;->OO〇80oO〇:Z

    .line 83
    iput-boolean p3, p0, Lcom/samsung/spensdk/SCanvasView;->O0〇8〇:Z

    .line 84
    iput-boolean p3, p0, Lcom/samsung/spensdk/SCanvasView;->o〇oO08〇o0:Z

    .line 85
    iput-boolean v0, p0, Lcom/samsung/spensdk/SCanvasView;->o8oo0OOO:Z

    .line 86
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇8〇〇8〇8:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->O8〇:Landroid/view/ViewGroup;

    .line 88
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->oo〇O0o〇:Landroid/view/ViewGroup;

    .line 89
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O08o:Ljava/util/HashMap;

    .line 90
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->o〇O80o8OO:Ljava/util/HashMap;

    .line 91
    iput-boolean v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o88O:Z

    .line 92
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$1;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$1;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇8〇8o00:Lcom/samsung/spensdk/applistener/SCanvasInitializeListener;

    .line 93
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$12;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$12;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008o8oo:Lcom/samsung/spensdk/applistener/SCanvasInitializeListener;

    .line 94
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇80O80O〇0:Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;

    .line 95
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$16;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$16;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇O:Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;

    .line 96
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$17;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$17;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->oOOO0:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

    .line 97
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$18;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$18;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O〇00o08:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

    .line 98
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$19;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$19;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇o88〇O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

    .line 99
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$20;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$20;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o0〇OO008O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

    .line 100
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$21;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$21;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O〇88:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

    .line 101
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$22;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$22;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇〇8〇〇:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

    .line 102
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$2;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$2;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇O〇〇〇:Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;

    .line 103
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$3;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$3;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇oO〇:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

    .line 104
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$4;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$4;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O0〇O80ooo:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

    .line 105
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->o8〇O〇0O0〇:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

    .line 106
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$5;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$5;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

    .line 107
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇8o〇:Lcom/samsung/sdraw/CanvasView$OnSettingViewShowListener;

    .line 108
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇8o80O:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

    .line 109
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$6;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$6;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->OO00〇0o〇〇:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

    .line 110
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->o00o0O〇〇o:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

    .line 111
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$7;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$7;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O〇〇O:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

    .line 112
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

    .line 113
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$8;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$8;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->Oo〇〇〇〇:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

    .line 114
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇O8〇〇o8〇:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

    .line 115
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$9;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$9;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O8o:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

    .line 116
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->O00〇o00:Lcom/samsung/spensdk/applistener/FileProcessListener;

    .line 117
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$10;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$10;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->oO0o:Lcom/samsung/spensdk/applistener/FileProcessListener;

    .line 118
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇08〇0oo0:Lcom/samsung/spensdk/applistener/AnimationProcessListener;

    .line 119
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$11;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$11;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O8o0〇:Lcom/samsung/spensdk/applistener/AnimationProcessListener;

    .line 120
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->o088〇〇:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

    .line 121
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$13;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$13;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o0o〇〇〇8o:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

    .line 122
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->O〇O〇88O8O:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

    .line 123
    new-instance v0, Lcom/samsung/spensdk/SCanvasView$14;

    invoke-direct {v0, p0}, Lcom/samsung/spensdk/SCanvasView$14;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->OooO〇080:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

    const/4 v0, 0x2

    .line 124
    iput v0, p0, Lcom/samsung/spensdk/SCanvasView;->OoOO〇:I

    const/4 v0, -0x1

    .line 125
    iput v0, p0, Lcom/samsung/spensdk/SCanvasView;->O80:I

    .line 126
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->oOO〇0o8〇:Landroid/graphics/drawable/Drawable;

    .line 127
    iput-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇88:Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;

    .line 128
    iput-boolean p3, p0, Lcom/samsung/spensdk/SCanvasView;->o〇Oo:Z

    const/4 p2, 0x0

    .line 129
    iput p2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0oO:F

    .line 130
    iput p2, p0, Lcom/samsung/spensdk/SCanvasView;->OOO0o〇:F

    .line 131
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 132
    iget-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇O:Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;

    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setOnInitializeFinishListener(Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;)V

    return-void
.end method

.method static synthetic Oo0〇Ooo(Lcom/samsung/spensdk/SCanvasView;)Lcom/samsung/spen/a/h/c;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static getSAMMVersion()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/samsung/samm/lib/d;->〇o00〇〇Oo()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static getSDKVersion()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "2.2.5"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private o08oOO()Z
    .locals 6

    .line 1
    iget v0, p0, Lcom/samsung/spensdk/SCanvasView;->ooo008:I

    .line 2
    .line 3
    if-gtz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iput v0, p0, Lcom/samsung/spensdk/SCanvasView;->ooo008:I

    .line 10
    .line 11
    :cond_0
    iget v0, p0, Lcom/samsung/spensdk/SCanvasView;->oo0O:I

    .line 12
    .line 13
    if-gtz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iput v0, p0, Lcom/samsung/spensdk/SCanvasView;->oo0O:I

    .line 20
    .line 21
    :cond_1
    new-instance v0, Lcom/samsung/samm/common/SOptionSCanvas;

    .line 22
    .line 23
    invoke-direct {v0}, Lcom/samsung/samm/common/SOptionSCanvas;-><init>()V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o〇o:Lcom/samsung/samm/common/SOptionSCanvas;

    .line 27
    .line 28
    iget v0, p0, Lcom/samsung/spensdk/SCanvasView;->ooo008:I

    .line 29
    .line 30
    iget v1, p0, Lcom/samsung/spensdk/SCanvasView;->oo0O:I

    .line 31
    .line 32
    invoke-virtual {p0, v0, v1}, Lcom/samsung/spensdk/SCanvasView;->〇OO〇00〇0O(II)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    const/4 v1, 0x0

    .line 37
    if-nez v0, :cond_2

    .line 38
    .line 39
    return v1

    .line 40
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/spensdk/SCanvasView;->oO00〇o()Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-nez v0, :cond_3

    .line 45
    .line 46
    return v1

    .line 47
    :cond_3
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 48
    .line 49
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->r()V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 53
    .line 54
    iget-object v2, p0, Lcom/samsung/spensdk/SCanvasView;->oo〇O0o〇:Landroid/view/ViewGroup;

    .line 55
    .line 56
    iget-object v3, p0, Lcom/samsung/spensdk/SCanvasView;->O8〇:Landroid/view/ViewGroup;

    .line 57
    .line 58
    iget-object v4, p0, Lcom/samsung/spensdk/SCanvasView;->O08o:Ljava/util/HashMap;

    .line 59
    .line 60
    iget-object v5, p0, Lcom/samsung/spensdk/SCanvasView;->o〇O80o8OO:Ljava/util/HashMap;

    .line 61
    .line 62
    invoke-interface {v0, v2, v3, v4, v5}, Lcom/samsung/spen/a/h/c;->OO0o〇〇〇〇0(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Ljava/util/HashMap;Ljava/util/HashMap;)Z

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    if-eqz v0, :cond_5

    .line 67
    .line 68
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇8〇8o00:Lcom/samsung/spensdk/applistener/SCanvasInitializeListener;

    .line 69
    .line 70
    if-eqz v0, :cond_4

    .line 71
    .line 72
    invoke-interface {v0}, Lcom/samsung/spensdk/applistener/SCanvasInitializeListener;->onInitialized()V

    .line 73
    .line 74
    .line 75
    :cond_4
    const/4 v0, 0x1

    .line 76
    return v0

    .line 77
    :cond_5
    return v1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method static synthetic o0OoOOo0(Lcom/samsung/spensdk/SCanvasView;)Lcom/samsung/spensdk/SCanvasView$OnPlayProgressChangeListener;
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    const/4 p0, 0x0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic o8〇OO(Lcom/samsung/spensdk/SCanvasView;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/samsung/spensdk/SCanvasView;->o08oOO()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic oO〇8O8oOo(Lcom/samsung/spensdk/SCanvasView;)Lcom/samsung/spensdk/SCanvasView$OnFileProcessingProgressListener;
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    const/4 p0, 0x0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic ooO(Lcom/samsung/spensdk/SCanvasView;)Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spensdk/SCanvasView;->〇80O80O〇0:Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇0O〇O00O(Lcom/samsung/spensdk/SCanvasView;)Lcom/samsung/spensdk/SCanvasView$OnPlayCompleteListener;
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    const/4 p0, 0x0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇〇〇0o〇〇0(Lcom/samsung/spensdk/SCanvasView;)Lcom/samsung/spen/a/b/b;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public O08O0〇O()Z
    .locals 1

    .line 1
    const-string v0, "isUndoable"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->w()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method Ooo08(Ljava/lang/String;)Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0}, Lcom/samsung/spensdk/SCanvasView;->〇OO8ooO8〇(Ljava/lang/String;Z)Z

    .line 3
    .line 4
    .line 5
    move-result p1

    .line 6
    return p1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public getAnimationSpeed()I
    .locals 1

    .line 1
    const-string v0, "getAnimationSpeed"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x4

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O0oO:Lcom/samsung/spen/a/a/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/a/b;->g()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getAnimationState()I
    .locals 1

    .line 1
    const-string v0, "getAnimationState"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O0oO:Lcom/samsung/spen/a/a/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/a/b;->c()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getAppID()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getAppID"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    invoke-static {}, Lcom/samsung/samm/lib/d;->O8()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getAppIDName()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getAppIDName"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    invoke-static {}, Lcom/samsung/samm/lib/d;->Oo08()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getAppIDVerMajor()I
    .locals 1

    .line 1
    const-string v0, "getAppIDVerMajor"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    invoke-static {}, Lcom/samsung/samm/lib/d;->o〇0()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getAppIDVerMinor()I
    .locals 1

    .line 1
    const-string v0, "getAppIDVerMinor"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    invoke-static {}, Lcom/samsung/samm/lib/d;->〇〇888()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getAppIDVerPatchName()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getAppIDVerPatchName"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    invoke-static {}, Lcom/samsung/samm/lib/d;->oO80()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getAttachedFileNum()I
    .locals 1

    .line 1
    const-string v0, "getAttachedFileNum"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->J()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getAuthorEmail()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getAuthorEmail"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->n()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getAuthorImage()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    const-string v0, "getAuthorImage"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->o()Landroid/graphics/Bitmap;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getAuthorName()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getAuthorName"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->l()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getAuthorPhoneNum()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getAuthorPhoneNum"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->m()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getBGAudioFile()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getBGAudioFile"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->C()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getBGColor()I
    .locals 1

    .line 1
    const-string v0, "getBGColor"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->v()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getBGImagePath()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getBGImagePath"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->x()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getBGImagePathDecoded()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getBGImagePathDecoded"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->y()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getCanvasDrawable()Z
    .locals 1

    .line 1
    const-string v0, "getCanvasDrawable"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->h()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getCanvasMode()I
    .locals 1

    .line 1
    const-string v0, "getCanvasMode"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->g()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getCanvasPanEnable()Z
    .locals 1

    .line 1
    const-string v0, "getCanvasPanEnable"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->k()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getCanvasSupportPenOnly()Z
    .locals 1

    .line 1
    const-string v0, "getCanvasSupportPenOnly"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->i()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getCanvasZoomEnable()Z
    .locals 1

    .line 1
    const-string v0, "getCanvasZoomEnable"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->j()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getCanvasZoomScale()F
    .locals 1

    .line 1
    const-string v0, "getCanvasZoomScale"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->e()F

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getCheckPreference()I
    .locals 1

    .line 1
    const-string v0, "getCheckPreference"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->u()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getClearAllByListener()Z
    .locals 1

    .line 1
    const-string v0, "getClearAllByListener"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->t()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getClearImageBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    const-string v0, "getClearImageBitmap"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/d/b;->b()Landroid/graphics/Bitmap;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getClearImagePathDecoded()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getClearImagePathDecoded"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->z()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getClipboardSObjectList()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Lcom/samsung/samm/common/SObject;",
            ">;"
        }
    .end annotation

    .line 1
    const-string v0, "getClipboardSObjectList"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->oO8o〇08〇:Lcom/samsung/spen/a/g/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/g/a;->g()Ljava/util/LinkedList;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getClipboardSObjectListType()I
    .locals 1

    .line 1
    const-string v0, "getClipboardSObjectListType"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, -0x1

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->oO8o〇08〇:Lcom/samsung/spen/a/g/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/g/a;->h()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getCreateTime()J
    .locals 2

    .line 1
    const-string v0, "getCreateTime"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const-wide/16 v0, 0x0

    .line 10
    .line 11
    return-wide v0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 13
    .line 14
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->p()J

    .line 15
    .line 16
    .line 17
    move-result-wide v0

    .line 18
    return-wide v0
    .line 19
.end method

.method public getCustomBGAudioIndex()I
    .locals 1

    .line 1
    const-string v0, "getCustomBGAudioIndex"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, -0x1

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->B()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getCustomBGImageIndex()I
    .locals 1

    .line 1
    const-string v0, "getCustomBGImageIndex"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, -0x1

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->w()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getEnableSettingRestore()Z
    .locals 1

    .line 1
    const-string v0, "getEnableSettingRestore"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->s()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getGeoTagLatitude()I
    .locals 1

    .line 1
    const-string v0, "getGeoTagLatitude"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->r()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getGeoTagLongitude()I
    .locals 1

    .line 1
    const-string v0, "getGeoTagLongitude"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->s()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getHypertext()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getHypertext"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->q()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getLoadAppID()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getLoadAppID"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->f()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getLoadAppIDName()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getLoadAppIDName"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->g()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getLoadAppIDVerMajor()I
    .locals 1

    .line 1
    const-string v0, "getLoadAppIDVerMajor"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->h()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getLoadAppIDVerMinor()I
    .locals 1

    .line 1
    const-string v0, "getLoadAppIDVerMinor"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->i()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getLoadAppIDVerPatchName()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getLoadAppIDVerPatchName"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->j()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getMode()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getMode()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getObjectInfos()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Lcom/samsung/sdraw/ObjectInfo;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getObjectInfos()Ljava/util/LinkedList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getOption()Lcom/samsung/samm/common/SOptionSCanvas;
    .locals 2

    .line 1
    const-string v0, "getOption"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-object v1

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o〇o:Lcom/samsung/samm/common/SOptionSCanvas;

    .line 12
    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    return-object v1

    .line 16
    :cond_1
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 17
    .line 18
    invoke-interface {v1}, Lcom/samsung/samm/lib/a;->c()Lcom/samsung/samm/common/SOptionSAMM;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    iput-object v1, v0, Lcom/samsung/samm/common/SOptionSCanvas;->〇o00〇〇Oo:Lcom/samsung/samm/common/SOptionSAMM;

    .line 23
    .line 24
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o〇o:Lcom/samsung/samm/common/SOptionSCanvas;

    .line 25
    .line 26
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O0oO:Lcom/samsung/spen/a/a/b;

    .line 27
    .line 28
    invoke-interface {v1}, Lcom/samsung/spen/a/a/b;->a()Lcom/samsung/samm/common/SOptionPlay;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    iput-object v1, v0, Lcom/samsung/samm/common/SOptionSCanvas;->〇080:Lcom/samsung/samm/common/SOptionPlay;

    .line 33
    .line 34
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o〇o:Lcom/samsung/samm/common/SOptionSCanvas;

    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public getPanningMode()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    const-string v0, "getPanningMode"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->f()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getPenSettingInfo()Lcom/samsung/sdraw/PenSettingInfo;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getPenSettingInfo()Lcom/samsung/sdraw/PenSettingInfo;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getRemoveLongPressStroke()Z
    .locals 1

    .line 1
    const-string v0, "getRemoveLongPressStroke"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->q()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getSAMMObjectNum()I
    .locals 2

    .line 1
    const-string v0, "getSAMMObjectNum"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->oO8o〇08〇:Lcom/samsung/spen/a/g/a;

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/g/a;->a(Z)I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    return v0
    .line 19
.end method

.method public getSCanvasBitmapData()[B
    .locals 1

    .line 1
    const-string v0, "getSCanvasBitmapData"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/d/b;->d()[B

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getSelectedObjectType()Lcom/samsung/sdraw/CanvasView$ObjectType;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getSelectedObjectType()Lcom/samsung/sdraw/CanvasView$ObjectType;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getSelectedSObject()Lcom/samsung/samm/common/SObject;
    .locals 2

    .line 1
    const-string v0, "getSelectedSObject"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    const/4 v0, 0x1

    .line 12
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->oO8o〇08〇:Lcom/samsung/spen/a/g/a;

    .line 13
    .line 14
    invoke-interface {v1, v0}, Lcom/samsung/spen/a/g/a;->c(Z)Lcom/samsung/samm/common/SObject;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    return-object v0
    .line 19
.end method

.method public getSelectedSObjectRect()Landroid/graphics/RectF;
    .locals 1

    .line 1
    const-string v0, "getSelectedSObjectRect"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->p()Landroid/graphics/RectF;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getSelectedSObjectType()I
    .locals 1

    .line 1
    const-string v0, "getSelectedSObjectType"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->oO8o〇08〇:Lcom/samsung/spen/a/g/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/g/a;->e()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getSettingFillingInfo()Lcom/samsung/spen/settings/SettingFillingInfo;
    .locals 1

    .line 1
    const-string v0, "getSettingFillingInfo"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->d()Lcom/samsung/spen/settings/SettingFillingInfo;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getSettingStrokeInfo()Lcom/samsung/spen/settings/SettingStrokeInfo;
    .locals 1

    .line 1
    const-string v0, "getSettingStrokeInfo"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->b()Lcom/samsung/spen/settings/SettingStrokeInfo;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getSettingTextInfo()Lcom/samsung/spen/settings/SettingTextInfo;
    .locals 1

    .line 1
    const-string v0, "getSettingTextInfo"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->c()Lcom/samsung/spen/settings/SettingTextInfo;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getSettingViewFillingInfo()Lcom/samsung/spen/settings/SettingFillingInfo;
    .locals 1

    .line 1
    const-string v0, "getSettingViewFillingInfo"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/h/c;->d()Lcom/samsung/spen/settings/SettingFillingInfo;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getSettingViewStrokeInfo()Lcom/samsung/spen/settings/SettingStrokeInfo;
    .locals 1

    .line 1
    const-string v0, "getSettingViewStrokeInfo"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/h/c;->b()Lcom/samsung/spen/settings/SettingStrokeInfo;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getSettingViewTextInfo()Lcom/samsung/spen/settings/SettingTextInfo;
    .locals 1

    .line 1
    const-string v0, "getSettingViewTextInfo"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/h/c;->c()Lcom/samsung/spen/settings/SettingTextInfo;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getStrokeInfos()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Lcom/samsung/sdraw/StrokeInfo;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getStrokeInfos()Ljava/util/LinkedList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getTags()[Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getTags"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->t()[Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getTextInfos()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Lcom/samsung/sdraw/TextInfo;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getTextInfos()Ljava/util/LinkedList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getTextLongClickSelectOption()Z
    .locals 1

    .line 1
    const-string v0, "getTextLongClickSelectOption"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->o()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public getTextSettingInfo()Lcom/samsung/sdraw/TextSettingInfo;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getTextSettingInfo()Lcom/samsung/sdraw/TextSettingInfo;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "getTitle"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/samm/lib/a;->k()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
.end method

.method public getTouchEventDispatchMode()Z
    .locals 1

    .line 1
    const-string v0, "getTouchEventDispatchMode"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->m()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public o08O()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->o08O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o88O〇8(I)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->o88O〇8(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public oO00〇o()Z
    .locals 8

    .line 1
    const-string/jumbo v0, "updateCallbackFunctions"

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    return v0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O0oO:Lcom/samsung/spen/a/a/b;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O8o0〇:Lcom/samsung/spensdk/applistener/AnimationProcessListener;

    .line 15
    .line 16
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/a/b;->〇o〇(Lcom/samsung/spensdk/applistener/AnimationProcessListener;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

    .line 22
    .line 23
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/h/c;->Oo08(Lcom/samsung/spensdk/applistener/SettingViewShowListener;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 27
    .line 28
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->OO00〇0o〇〇:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

    .line 29
    .line 30
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/h/c;->〇80〇808〇O(Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 34
    .line 35
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O〇〇O:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

    .line 36
    .line 37
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/h/c;->〇8o8o〇(Lcom/samsung/spensdk/applistener/SettingTextChangeListener;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->Oo〇〇〇〇:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

    .line 43
    .line 44
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/h/c;->oO80(Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 48
    .line 49
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O8o:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

    .line 50
    .line 51
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/b/b;->Oooo8o0〇(Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 55
    .line 56
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->o〇〇8〇〇:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

    .line 57
    .line 58
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/b/b;->〇O8o08O(Lcom/samsung/spensdk/applistener/HistoryUpdateListener;)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 62
    .line 63
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O0〇O80ooo:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

    .line 64
    .line 65
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/b/b;->〇〇808〇(Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

    .line 69
    .line 70
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->oO0o:Lcom/samsung/spensdk/applistener/FileProcessListener;

    .line 71
    .line 72
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/d/b;->Oo08(Lcom/samsung/spensdk/applistener/FileProcessListener;)V

    .line 73
    .line 74
    .line 75
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 76
    .line 77
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->O〇00o08:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

    .line 78
    .line 79
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/e/c;->〇0000OOO(Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 83
    .line 84
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->o0〇OO008O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

    .line 85
    .line 86
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/e/c;->O08000(Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;)V

    .line 87
    .line 88
    .line 89
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 90
    .line 91
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->OooO〇080:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

    .line 92
    .line 93
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/e/c;->〇8o8o〇(Lcom/samsung/spensdk/applistener/SObjectUpdateListener;)V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 97
    .line 98
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->o0o〇〇〇8o:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

    .line 99
    .line 100
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/e/c;->O0(Lcom/samsung/spensdk/applistener/SObjectSelectListener;)V

    .line 101
    .line 102
    .line 103
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇8o0OOOo:Lcom/samsung/spen/a/f/a;

    .line 104
    .line 105
    const/4 v1, 0x0

    .line 106
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/f/a;->〇o00〇〇Oo(Lcom/samsung/spensdk/applistener/CustomSoundEffectSettingListener;)V

    .line 107
    .line 108
    .line 109
    iget-object v2, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 110
    .line 111
    const/4 v3, 0x0

    .line 112
    iget v4, p0, Lcom/samsung/spensdk/SCanvasView;->OoOO〇:I

    .line 113
    .line 114
    iget v5, p0, Lcom/samsung/spensdk/SCanvasView;->O80:I

    .line 115
    .line 116
    iget-object v6, p0, Lcom/samsung/spensdk/SCanvasView;->oOO〇0o8〇:Landroid/graphics/drawable/Drawable;

    .line 117
    .line 118
    const/4 v7, 0x0

    .line 119
    invoke-interface/range {v2 .. v7}, Lcom/samsung/spen/a/h/c;->〇080(Lcom/samsung/spensdk/applistener/SPenHoverListener;IILandroid/graphics/drawable/Drawable;Lcom/samsung/spensdk/applistener/CustomHoverPointerSettingListener;)V

    .line 120
    .line 121
    .line 122
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 123
    .line 124
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/h/c;->〇〇888(Lcom/samsung/spensdk/applistener/SPenTouchListener;)V

    .line 125
    .line 126
    .line 127
    const/4 v0, 0x1

    .line 128
    return v0
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 1
    iget-boolean v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇Oo:Z

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iput v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0oO:F

    .line 16
    .line 17
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    iput v0, p0, Lcom/samsung/spensdk/SCanvasView;->OOO0o〇:F

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v1, 0x2

    .line 25
    if-ne v0, v1, :cond_1

    .line 26
    .line 27
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    iget-object v2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 36
    .line 37
    iget v3, p0, Lcom/samsung/spensdk/SCanvasView;->〇0oO:F

    .line 38
    .line 39
    sub-float v3, v0, v3

    .line 40
    .line 41
    iget v4, p0, Lcom/samsung/spensdk/SCanvasView;->OOO0o〇:F

    .line 42
    .line 43
    sub-float v4, v1, v4

    .line 44
    .line 45
    const/4 v5, 0x0

    .line 46
    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/spen/a/b/b;->a(FFZ)Z

    .line 47
    .line 48
    .line 49
    iput v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0oO:F

    .line 50
    .line 51
    iput v1, p0, Lcom/samsung/spensdk/SCanvasView;->OOO0o〇:F

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_1
    const/4 v1, 0x1

    .line 55
    if-ne v0, v1, :cond_2

    .line 56
    .line 57
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 58
    .line 59
    const/4 v2, 0x0

    .line 60
    invoke-interface {v0, v2, v2, v1}, Lcom/samsung/spen/a/b/b;->a(FFZ)Z

    .line 61
    .line 62
    .line 63
    :cond_2
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    return p1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public o〇o〇Oo88(I)Z
    .locals 2

    .line 1
    const-string v0, "setBGColor"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return v1

    .line 11
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setBackgroundColor(I)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 15
    .line 16
    invoke-interface {v0, p1}, Lcom/samsung/samm/lib/a;->b(I)Z

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    if-nez p1, :cond_1

    .line 21
    .line 22
    return v1

    .line 23
    :cond_1
    const/4 p1, 0x1

    .line 24
    return p1
    .line 25
.end method

.method public setAnimationProcessListener(Lcom/samsung/spensdk/applistener/AnimationProcessListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇08〇0oo0:Lcom/samsung/spensdk/applistener/AnimationProcessListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .line 1
    const-string v0, "setBackgroundColor"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setBackgroundColor(I)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0, p1}, Lcom/samsung/spensdk/SCanvasView;->o〇o〇Oo88(I)Z

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setCanvasPanEnable(Z)V
    .locals 1

    .line 1
    const-string v0, "setCanvasPanEnable"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 11
    .line 12
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/b;->〇o00〇〇Oo(Z)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setCanvasZoomEnable(Z)V
    .locals 1

    .line 1
    const-string v0, "setCanvasZoomEnable"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 11
    .line 12
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/b;->〇080(Z)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setClearAllByListener(Z)V
    .locals 1

    .line 1
    const-string v0, "setClearAllByListener"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 11
    .line 12
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/b;->〇8o8o〇(Z)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setColorPickerColorChangeListener(Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇O8〇〇o8〇:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setColorPickerMode(Z)V
    .locals 1

    .line 1
    const-string v0, "setColorPickerMode"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 11
    .line 12
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/b;->〇o〇(Z)V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    invoke-interface {p1, v0}, Lcom/samsung/spen/a/h/c;->a(Z)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setCustomHoverPointerListener(Lcom/samsung/spensdk/applistener/CustomHoverPointerSettingListener;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇o00〇〇Oo()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 9
    .line 10
    invoke-static {p1}, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇080(Landroid/content/Context;)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    if-nez p1, :cond_1

    .line 15
    .line 16
    return-void

    .line 17
    :cond_1
    const/4 p1, -0x1

    .line 18
    iput p1, p0, Lcom/samsung/spensdk/SCanvasView;->O80:I

    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->oOO〇0o8〇:Landroid/graphics/drawable/Drawable;

    .line 22
    .line 23
    const-string v0, "setCustomHoverPointerListener"

    .line 24
    .line 25
    const/4 v1, 0x1

    .line 26
    invoke-virtual {p0, v0, v1}, Lcom/samsung/spensdk/SCanvasView;->〇OO8ooO8〇(Ljava/lang/String;Z)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-nez v0, :cond_2

    .line 31
    .line 32
    return-void

    .line 33
    :cond_2
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 34
    .line 35
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/h/c;->〇o00〇〇Oo(Lcom/samsung/spensdk/applistener/CustomHoverPointerSettingListener;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public setCustomSoundEffectSettingListener(Lcom/samsung/spensdk/applistener/CustomSoundEffectSettingListener;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setEnableSettingRestore(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/samsung/spensdk/SCanvasView;->o8oo0OOO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setEraserCursorVisible(Z)V
    .locals 1

    .line 1
    const-string v0, "setEraserCursorVisible"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 11
    .line 12
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/c;->b(Z)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setFileProcessListener(Lcom/samsung/spensdk/applistener/FileProcessListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->O00〇o00:Lcom/samsung/spensdk/applistener/FileProcessListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setHistoricalOperationSupport(Z)V
    .locals 1

    .line 1
    const-string v0, "setHistoricalOperationSupport"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 11
    .line 12
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/b;->oO80(Z)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setHistoryUpdateListener(Lcom/samsung/spensdk/applistener/HistoryUpdateListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->O〇88:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setMovingMode(Z)V
    .locals 1

    .line 1
    const-string/jumbo v0, "setMovingMode"

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/spensdk/SCanvasView;->o〇Oo:Z

    .line 12
    .line 13
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 14
    .line 15
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/b;->c(Z)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-eqz p1, :cond_1

    .line 20
    .line 21
    iget-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 22
    .line 23
    const/4 v0, 0x0

    .line 24
    invoke-interface {p1, v0}, Lcom/samsung/spen/a/h/c;->a(Z)V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public setMultiTouchCancel(Z)V
    .locals 1

    .line 1
    const-string/jumbo v0, "setMultiTouchCancel"

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/b;->o〇0(Z)Z

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setOnFileProcessingProgressListener(Lcom/samsung/spensdk/SCanvasView$OnFileProcessingProgressListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setOnHistoryChangeListener(Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇O〇〇〇:Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setOnInitializeFinishListener(Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇80O80O〇0:Lcom/samsung/sdraw/CanvasView$OnInitializeFinishListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setOnObjectListener(Lcom/samsung/sdraw/CanvasView$OnObjectListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setOnPlayCompleteListener(Lcom/samsung/spensdk/SCanvasView$OnPlayCompleteListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setOnPlayProgressChangeListener(Lcom/samsung/spensdk/SCanvasView$OnPlayProgressChangeListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setOnSettingViewShowListener(Lcom/samsung/sdraw/CanvasView$OnSettingViewShowListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇8o〇:Lcom/samsung/sdraw/CanvasView$OnSettingViewShowListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setPanningMode(Z)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    const-string/jumbo v0, "setPanningMode"

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/b;->c(Z)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    if-eqz p1, :cond_1

    .line 18
    .line 19
    iget-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    invoke-interface {p1, v0}, Lcom/samsung/spen/a/h/c;->a(Z)V

    .line 23
    .line 24
    .line 25
    :cond_1
    return-void
.end method

.method public setPenSettingInfo(Lcom/samsung/sdraw/PenSettingInfo;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setPenSettingInfo(Lcom/samsung/sdraw/PenSettingInfo;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setRemoveLongPressStroke(Z)V
    .locals 1

    .line 1
    const-string/jumbo v0, "setRemoveLongPressStroke"

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/b;->OO0o〇〇〇〇0(Z)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSCanvasHoverPointerStyle(I)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇o00〇〇Oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 9
    .line 10
    invoke-static {v0}, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇080(Landroid/content/Context;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    return-void

    .line 17
    :cond_1
    const/4 v0, 0x1

    .line 18
    if-eqz p1, :cond_2

    .line 19
    .line 20
    if-eq p1, v0, :cond_2

    .line 21
    .line 22
    const/4 v1, 0x2

    .line 23
    if-eq p1, v1, :cond_2

    .line 24
    .line 25
    const/4 v1, 0x3

    .line 26
    if-eq p1, v1, :cond_2

    .line 27
    .line 28
    return-void

    .line 29
    :cond_2
    iput p1, p0, Lcom/samsung/spensdk/SCanvasView;->OoOO〇:I

    .line 30
    .line 31
    const-string/jumbo p1, "setSCanvasHoverPointerStyle"

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0, p1, v0}, Lcom/samsung/spensdk/SCanvasView;->〇OO8ooO8〇(Ljava/lang/String;Z)Z

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    if-nez p1, :cond_3

    .line 39
    .line 40
    return-void

    .line 41
    :cond_3
    iget-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 42
    .line 43
    iget v0, p0, Lcom/samsung/spensdk/SCanvasView;->OoOO〇:I

    .line 44
    .line 45
    invoke-interface {p1, v0}, Lcom/samsung/spen/a/h/c;->c(I)V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public setSCanvasInitializeListener(Lcom/samsung/spensdk/applistener/SCanvasInitializeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇8〇8o00:Lcom/samsung/spensdk/applistener/SCanvasInitializeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSCanvasLongPressListener(Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇oO〇:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSCanvasMatrixChangeListener(Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->oOOO0:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSCanvasModeChangedListener(Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇o88〇O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSObjectSelectListener(Lcom/samsung/spensdk/applistener/SObjectSelectListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->o088〇〇:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSObjectUpdateListener(Lcom/samsung/spensdk/applistener/SObjectUpdateListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->O〇O〇88O8O:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSPenHoverListener(Lcom/samsung/spensdk/applistener/SPenHoverListener;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇o00〇〇Oo()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const-string/jumbo p1, "setSPenHoverListener"

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    invoke-virtual {p0, p1, v0}, Lcom/samsung/spensdk/SCanvasView;->〇OO8ooO8〇(Ljava/lang/String;Z)Z

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    if-nez p1, :cond_1

    .line 17
    .line 18
    return-void

    .line 19
    :cond_1
    iget-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    invoke-interface {p1, v0}, Lcom/samsung/spen/a/h/c;->O8(Lcom/samsung/spensdk/applistener/SPenHoverListener;)V

    .line 23
    .line 24
    .line 25
    return-void
.end method

.method public setSPenTouchListener(Lcom/samsung/spensdk/applistener/SPenTouchListener;)V
    .locals 1

    .line 1
    const-string/jumbo p1, "setSPenTouchListener"

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    invoke-virtual {p0, p1, v0}, Lcom/samsung/spensdk/SCanvasView;->〇OO8ooO8〇(Ljava/lang/String;Z)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    if-nez p1, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    iget-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 13
    .line 14
    if-nez p1, :cond_1

    .line 15
    .line 16
    return-void

    .line 17
    :cond_1
    const/4 v0, 0x0

    .line 18
    invoke-interface {p1, v0}, Lcom/samsung/spen/a/h/c;->〇〇888(Lcom/samsung/spensdk/applistener/SPenTouchListener;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSettingFillingChangeListener(Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSettingStrokeChangeListener(Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇8o80O:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSettingTextChangeListener(Lcom/samsung/spensdk/applistener/SettingTextChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->o00o0O〇〇o:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setSettingView(Lcom/samsung/sdraw/SettingView;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setSettingView(Lcom/samsung/sdraw/SettingView;)V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->O8〇:Landroid/view/ViewGroup;

    .line 5
    .line 6
    const-string/jumbo p1, "setSettingView"

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    invoke-virtual {p0, p1, v0}, Lcom/samsung/spensdk/SCanvasView;->〇OO8ooO8〇(Ljava/lang/String;Z)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    iget-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O8〇:Landroid/view/ViewGroup;

    .line 19
    .line 20
    invoke-interface {p1, v0}, Lcom/samsung/spen/a/e/g;->〇〇808〇(Landroid/view/ViewGroup;)Z

    .line 21
    .line 22
    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
.end method

.method public setSettingViewShowListener(Lcom/samsung/spensdk/applistener/SettingViewShowListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->o8〇O〇0O0〇:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setTempDirPath(Ljava/lang/String;)V
    .locals 1

    .line 1
    const-string/jumbo v0, "setTempDirPath"

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->〇8〇〇8〇8:Ljava/lang/String;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setTextLongClickSelectOption(Z)V
    .locals 1

    .line 1
    const-string/jumbo v0, "setTextLongClickSelectOption"

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/b;->〇80〇808〇O(Z)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setTextSettingInfo(Lcom/samsung/sdraw/TextSettingInfo;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setTextSettingInfo(Lcom/samsung/sdraw/TextSettingInfo;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setTouchEventDispatchMode(Z)V
    .locals 1

    .line 1
    const-string/jumbo v0, "setTouchEventDispatchMode"

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/b/b;->〇〇888(Z)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public setUseHistoricalEventForStroke(Z)V
    .locals 1

    .line 1
    const-string/jumbo v0, "setUseHistoricalEventForStroke"

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

    .line 12
    .line 13
    invoke-interface {v0, p1}, Lcom/samsung/spen/a/d/b;->b(Z)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇0O00oO()Z
    .locals 1

    .line 1
    const-string v0, "isRedoable"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 12
    .line 13
    invoke-interface {v0}, Lcom/samsung/spen/a/b/b;->x()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
.end method

.method 〇OO8ooO8〇(Ljava/lang/String;Z)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    if-eqz p2, :cond_0

    .line 6
    .line 7
    new-instance p2, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    const-string v0, "S-Canvas is not created yet. The function \""

    .line 10
    .line 11
    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string p1, "\" may not affect right now"

    .line 18
    .line 19
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    const-string v0, "S-Canvas is not created yet. Call \""

    .line 26
    .line 27
    invoke-direct {p2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string p1, "\" in onInitialized() of SCanvasInitializeListener or call it after onInitialized()"

    .line 34
    .line 35
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    :goto_0
    const/4 p1, 0x0

    .line 39
    return p1

    .line 40
    :cond_1
    const/4 p1, 0x1

    .line 41
    return p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method 〇OO〇00〇0O(II)Z
    .locals 9

    .line 1
    const/4 v0, 0x0

    .line 2
    if-lez p1, :cond_2

    .line 3
    .line 4
    if-gtz p2, :cond_0

    .line 5
    .line 6
    goto/16 :goto_0

    .line 7
    .line 8
    :cond_0
    new-instance v8, Lcom/samsung/samm/lib/d;

    .line 9
    .line 10
    iget-object v2, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 11
    .line 12
    iget-boolean v5, p0, Lcom/samsung/spensdk/SCanvasView;->oooo800〇〇:Z

    .line 13
    .line 14
    iget-boolean v6, p0, Lcom/samsung/spensdk/SCanvasView;->〇8〇o〇OoO8:Z

    .line 15
    .line 16
    iget-object v7, p0, Lcom/samsung/spensdk/SCanvasView;->〇8〇〇8〇8:Ljava/lang/String;

    .line 17
    .line 18
    move-object v1, v8

    .line 19
    move v3, p1

    .line 20
    move v4, p2

    .line 21
    invoke-direct/range {v1 .. v7}, Lcom/samsung/samm/lib/d;-><init>(Landroid/content/Context;IIZZLjava/lang/String;)V

    .line 22
    .line 23
    .line 24
    iput-object v8, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 25
    .line 26
    invoke-interface {v8}, Lcom/samsung/samm/lib/a;->b()Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-nez v1, :cond_1

    .line 31
    .line 32
    const/4 p1, 0x0

    .line 33
    iput-object p1, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 34
    .line 35
    return v0

    .line 36
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/spensdk/SCanvasView;->o〇oO08〇o0:Z

    .line 37
    .line 38
    invoke-virtual {p0, v0}, Lcom/samsung/sdraw/CanvasView;->setObjectSupportPenOnly(Z)V

    .line 39
    .line 40
    .line 41
    new-instance v0, Lcom/samsung/spen/a/g/c;

    .line 42
    .line 43
    invoke-direct {v0}, Lcom/samsung/spen/a/g/c;-><init>()V

    .line 44
    .line 45
    .line 46
    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->oO8o〇08〇:Lcom/samsung/spen/a/g/a;

    .line 47
    .line 48
    new-instance v0, Lcom/samsung/spen/a/f/b;

    .line 49
    .line 50
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 51
    .line 52
    invoke-direct {v0, v1}, Lcom/samsung/spen/a/f/b;-><init>(Landroid/content/Context;)V

    .line 53
    .line 54
    .line 55
    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇8o0OOOo:Lcom/samsung/spen/a/f/a;

    .line 56
    .line 57
    new-instance v0, Lcom/samsung/spen/a/a/a;

    .line 58
    .line 59
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 60
    .line 61
    invoke-direct {v0, v1, p1, p2}, Lcom/samsung/spen/a/a/a;-><init>(Landroid/content/Context;II)V

    .line 62
    .line 63
    .line 64
    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O0oO:Lcom/samsung/spen/a/a/b;

    .line 65
    .line 66
    new-instance v0, Lcom/samsung/spen/a/b/a;

    .line 67
    .line 68
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 69
    .line 70
    iget-boolean v2, p0, Lcom/samsung/spensdk/SCanvasView;->o8oo0OOO:Z

    .line 71
    .line 72
    invoke-direct {v0, v1, p1, p2, v2}, Lcom/samsung/spen/a/b/a;-><init>(Landroid/content/Context;IIZ)V

    .line 73
    .line 74
    .line 75
    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 76
    .line 77
    new-instance v0, Lcom/samsung/spen/a/d/a;

    .line 78
    .line 79
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 80
    .line 81
    invoke-direct {v0, v1, p1, p2}, Lcom/samsung/spen/a/d/a;-><init>(Landroid/content/Context;II)V

    .line 82
    .line 83
    .line 84
    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

    .line 85
    .line 86
    new-instance v0, Lcom/samsung/spen/a/h/d;

    .line 87
    .line 88
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->Oo0O〇8800:Landroid/content/Context;

    .line 89
    .line 90
    iget-boolean v2, p0, Lcom/samsung/spensdk/SCanvasView;->〇0o88O:Z

    .line 91
    .line 92
    invoke-direct {v0, v1, v2}, Lcom/samsung/spen/a/h/d;-><init>(Landroid/content/Context;Z)V

    .line 93
    .line 94
    .line 95
    iput-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 96
    .line 97
    iput-object p0, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 98
    .line 99
    invoke-interface {v0, p0}, Lcom/samsung/spen/a/h/c;->〇o〇(Lcom/samsung/spen/a/e/g;)V

    .line 100
    .line 101
    .line 102
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 103
    .line 104
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 105
    .line 106
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/h/c;->o〇0(Lcom/samsung/spen/a/b/d;)V

    .line 107
    .line 108
    .line 109
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O0oO:Lcom/samsung/spen/a/a/b;

    .line 110
    .line 111
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

    .line 112
    .line 113
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/a/b;->〇o00〇〇Oo(Lcom/samsung/spen/a/d/c;)V

    .line 114
    .line 115
    .line 116
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O0oO:Lcom/samsung/spen/a/a/b;

    .line 117
    .line 118
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇〇8o0OOOo:Lcom/samsung/spen/a/f/a;

    .line 119
    .line 120
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/a/b;->〇080(Lcom/samsung/spen/a/f/a;)V

    .line 121
    .line 122
    .line 123
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O0oO:Lcom/samsung/spen/a/a/b;

    .line 124
    .line 125
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 126
    .line 127
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/a/b;->O8(Lcom/samsung/spen/a/b/c;)V

    .line 128
    .line 129
    .line 130
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇0〇o8〇:Lcom/samsung/spen/a/b/b;

    .line 131
    .line 132
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 133
    .line 134
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/b/b;->Oo08(Lcom/samsung/spen/a/e/d;)V

    .line 135
    .line 136
    .line 137
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

    .line 138
    .line 139
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 140
    .line 141
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/d/b;->o〇0(Lcom/samsung/samm/lib/c;)V

    .line 142
    .line 143
    .line 144
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

    .line 145
    .line 146
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 147
    .line 148
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/d/b;->〇080(Lcom/samsung/spen/a/e/e;)V

    .line 149
    .line 150
    .line 151
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 152
    .line 153
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->oO8o〇08〇:Lcom/samsung/spen/a/g/a;

    .line 154
    .line 155
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/e/d;->〇0〇O0088o(Lcom/samsung/spen/a/g/a;)V

    .line 156
    .line 157
    .line 158
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->oO8o〇08〇:Lcom/samsung/spen/a/g/a;

    .line 159
    .line 160
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 161
    .line 162
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/g/a;->O8(Lcom/samsung/samm/lib/b;)V

    .line 163
    .line 164
    .line 165
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->oO8o〇08〇:Lcom/samsung/spen/a/g/a;

    .line 166
    .line 167
    iget-object v1, p0, Lcom/samsung/spensdk/SCanvasView;->OO0〇O:Lcom/samsung/spen/a/e/c;

    .line 168
    .line 169
    invoke-interface {v0, v1}, Lcom/samsung/spen/a/g/a;->〇080(Lcom/samsung/spen/a/e/e;)V

    .line 170
    .line 171
    .line 172
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->〇00〇〇〇o〇8:Lcom/samsung/spen/a/d/b;

    .line 173
    .line 174
    invoke-interface {v0, p1, p2}, Lcom/samsung/spen/a/d/b;->b(II)Z

    .line 175
    .line 176
    .line 177
    new-instance p1, Lcom/samsung/spensdk/SCanvasView$15;

    .line 178
    .line 179
    invoke-direct {p1, p0}, Lcom/samsung/spensdk/SCanvasView$15;-><init>(Lcom/samsung/spensdk/SCanvasView;)V

    .line 180
    .line 181
    .line 182
    iget-object p2, p0, Lcom/samsung/spensdk/SCanvasView;->o〇o8〇〇O:Lcom/samsung/samm/lib/a;

    .line 183
    .line 184
    invoke-interface {p2, p1}, Lcom/samsung/samm/lib/a;->〇080(Lcom/samsung/samm/lib/d$a;)V

    .line 185
    .line 186
    .line 187
    const/4 p1, 0x1

    .line 188
    return p1

    .line 189
    :cond_2
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 190
    .line 191
    const-string v2, "Invalid Canvas Size ("

    .line 192
    .line 193
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 194
    .line 195
    .line 196
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 197
    .line 198
    .line 199
    const-string p1, ", "

    .line 200
    .line 201
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    .line 203
    .line 204
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 205
    .line 206
    .line 207
    const-string p1, ")"

    .line 208
    .line 209
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    .line 211
    .line 212
    return v0
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public 〇〇o0o()V
    .locals 1

    .line 1
    const-string v0, "hideImm"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/samsung/spensdk/SCanvasView;->Ooo08(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/samsung/spensdk/SCanvasView;->O008oO0:Lcom/samsung/spen/a/h/c;

    .line 11
    .line 12
    invoke-interface {v0}, Lcom/samsung/spen/a/h/c;->e()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
.end method

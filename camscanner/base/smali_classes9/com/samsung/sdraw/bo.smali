.class Lcom/samsung/sdraw/bo;
.super Landroid/graphics/Paint;
.source "SourceFile"


# instance fields
.field protected 〇080:Z

.field protected 〇o00〇〇Oo:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroid/graphics/Paint;-><init>()V

    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 3
    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 4
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 5
    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 6
    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/sdraw/bo;)V
    .locals 1

    .line 7
    invoke-direct {p0}, Lcom/samsung/sdraw/bo;-><init>()V

    .line 8
    invoke-virtual {p0, p1}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 9
    iget-boolean v0, p1, Lcom/samsung/sdraw/bo;->〇080:Z

    iput-boolean v0, p0, Lcom/samsung/sdraw/bo;->〇080:Z

    .line 10
    iget-object p1, p1, Lcom/samsung/sdraw/bo;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/samsung/sdraw/bo;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method public setAlpha(I)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/bo;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/16 v0, 0xff

    .line 6
    .line 7
    invoke-super {p0, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Landroid/graphics/Paint;->getColor()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    int-to-float v1, v1

    .line 19
    const/high16 v2, 0x437f0000    # 255.0f

    .line 20
    .line 21
    div-float/2addr v1, v2

    .line 22
    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    int-to-float v3, v3

    .line 27
    div-float/2addr v3, v2

    .line 28
    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    int-to-float v0, v0

    .line 33
    div-float/2addr v0, v2

    .line 34
    int-to-float p1, p1

    .line 35
    div-float/2addr p1, v2

    .line 36
    const v2, 0x3f19999a    # 0.6f

    .line 37
    .line 38
    .line 39
    invoke-static {v2, p1}, Ljava/lang/Math;->min(FF)F

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    const v2, 0x3e4ccccd    # 0.2f

    .line 44
    .line 45
    .line 46
    invoke-static {v2, p1}, Ljava/lang/Math;->max(FF)F

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    new-instance v2, Landroid/graphics/ColorMatrixColorFilter;

    .line 51
    .line 52
    const/16 v4, 0x14

    .line 53
    .line 54
    new-array v4, v4, [F

    .line 55
    .line 56
    const/4 v5, 0x0

    .line 57
    aput v1, v4, v5

    .line 58
    .line 59
    const/4 v1, 0x1

    .line 60
    const/4 v5, 0x0

    .line 61
    aput v5, v4, v1

    .line 62
    .line 63
    const/4 v1, 0x2

    .line 64
    aput v5, v4, v1

    .line 65
    .line 66
    const/4 v1, 0x3

    .line 67
    aput v5, v4, v1

    .line 68
    .line 69
    const/4 v1, 0x4

    .line 70
    aput v5, v4, v1

    .line 71
    .line 72
    const/4 v1, 0x5

    .line 73
    aput v5, v4, v1

    .line 74
    .line 75
    const/4 v1, 0x6

    .line 76
    aput v3, v4, v1

    .line 77
    .line 78
    const/4 v1, 0x7

    .line 79
    aput v5, v4, v1

    .line 80
    .line 81
    const/16 v1, 0x8

    .line 82
    .line 83
    aput v5, v4, v1

    .line 84
    .line 85
    const/16 v1, 0x9

    .line 86
    .line 87
    aput v5, v4, v1

    .line 88
    .line 89
    const/16 v1, 0xa

    .line 90
    .line 91
    aput v5, v4, v1

    .line 92
    .line 93
    const/16 v1, 0xb

    .line 94
    .line 95
    aput v5, v4, v1

    .line 96
    .line 97
    const/16 v1, 0xc

    .line 98
    .line 99
    aput v0, v4, v1

    .line 100
    .line 101
    const/16 v0, 0xd

    .line 102
    .line 103
    aput v5, v4, v0

    .line 104
    .line 105
    const/16 v0, 0xe

    .line 106
    .line 107
    aput v5, v4, v0

    .line 108
    .line 109
    const/16 v0, 0xf

    .line 110
    .line 111
    aput v5, v4, v0

    .line 112
    .line 113
    const/16 v0, 0x10

    .line 114
    .line 115
    aput v5, v4, v0

    .line 116
    .line 117
    const/16 v0, 0x11

    .line 118
    .line 119
    aput v5, v4, v0

    .line 120
    .line 121
    const/16 v0, 0x12

    .line 122
    .line 123
    aput p1, v4, v0

    .line 124
    .line 125
    const/16 p1, 0x13

    .line 126
    .line 127
    aput v5, v4, p1

    .line 128
    .line 129
    invoke-direct {v2, v4}, Landroid/graphics/ColorMatrixColorFilter;-><init>([F)V

    .line 130
    .line 131
    .line 132
    invoke-virtual {p0, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 133
    .line 134
    .line 135
    goto :goto_0

    .line 136
    :cond_0
    invoke-super {p0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 137
    .line 138
    .line 139
    :goto_0
    return-void
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method public setColor(I)V
    .locals 6

    .line 1
    invoke-super {p0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/samsung/sdraw/bo;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    int-to-float v0, v0

    .line 13
    const/high16 v1, 0x437f0000    # 255.0f

    .line 14
    .line 15
    div-float/2addr v0, v1

    .line 16
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    int-to-float v2, v2

    .line 21
    div-float/2addr v2, v1

    .line 22
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    int-to-float v3, v3

    .line 27
    div-float/2addr v3, v1

    .line 28
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    int-to-float p1, p1

    .line 33
    div-float/2addr p1, v1

    .line 34
    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    .line 35
    .line 36
    const/16 v4, 0x14

    .line 37
    .line 38
    new-array v4, v4, [F

    .line 39
    .line 40
    const/4 v5, 0x0

    .line 41
    aput v0, v4, v5

    .line 42
    .line 43
    const/4 v0, 0x1

    .line 44
    const/4 v5, 0x0

    .line 45
    aput v5, v4, v0

    .line 46
    .line 47
    const/4 v0, 0x2

    .line 48
    aput v5, v4, v0

    .line 49
    .line 50
    const/4 v0, 0x3

    .line 51
    aput v5, v4, v0

    .line 52
    .line 53
    const/4 v0, 0x4

    .line 54
    aput v5, v4, v0

    .line 55
    .line 56
    const/4 v0, 0x5

    .line 57
    aput v5, v4, v0

    .line 58
    .line 59
    const/4 v0, 0x6

    .line 60
    aput v2, v4, v0

    .line 61
    .line 62
    const/4 v0, 0x7

    .line 63
    aput v5, v4, v0

    .line 64
    .line 65
    const/16 v0, 0x8

    .line 66
    .line 67
    aput v5, v4, v0

    .line 68
    .line 69
    const/16 v0, 0x9

    .line 70
    .line 71
    aput v5, v4, v0

    .line 72
    .line 73
    const/16 v0, 0xa

    .line 74
    .line 75
    aput v5, v4, v0

    .line 76
    .line 77
    const/16 v0, 0xb

    .line 78
    .line 79
    aput v5, v4, v0

    .line 80
    .line 81
    const/16 v0, 0xc

    .line 82
    .line 83
    aput v3, v4, v0

    .line 84
    .line 85
    const/16 v0, 0xd

    .line 86
    .line 87
    aput v5, v4, v0

    .line 88
    .line 89
    const/16 v0, 0xe

    .line 90
    .line 91
    aput v5, v4, v0

    .line 92
    .line 93
    const/16 v0, 0xf

    .line 94
    .line 95
    aput v5, v4, v0

    .line 96
    .line 97
    const/16 v0, 0x10

    .line 98
    .line 99
    aput v5, v4, v0

    .line 100
    .line 101
    const/16 v0, 0x11

    .line 102
    .line 103
    aput v5, v4, v0

    .line 104
    .line 105
    const/16 v0, 0x12

    .line 106
    .line 107
    aput p1, v4, v0

    .line 108
    .line 109
    const/16 p1, 0x13

    .line 110
    .line 111
    aput v5, v4, p1

    .line 112
    .line 113
    invoke-direct {v1, v4}, Landroid/graphics/ColorMatrixColorFilter;-><init>([F)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 117
    .line 118
    .line 119
    :cond_0
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method public 〇080()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/samsung/sdraw/bo;->〇080:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇o00〇〇Oo()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/bo;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

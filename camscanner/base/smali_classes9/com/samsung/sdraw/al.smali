.class Lcom/samsung/sdraw/al;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/sdraw/br;


# instance fields
.field private O8:Landroid/graphics/RectF;

.field private Oo08:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/samsung/sdraw/h;",
            ">;"
        }
    .end annotation
.end field

.field private oO80:I

.field private o〇0:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/samsung/sdraw/cd;",
            ">;"
        }
    .end annotation
.end field

.field private 〇080:Lcom/samsung/sdraw/StrokeSprite;

.field private 〇80〇808〇O:F

.field private 〇o00〇〇Oo:Lcom/samsung/sdraw/bo;

.field private 〇o〇:Landroid/graphics/Bitmap;

.field private 〇〇888:I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private O8(Lcom/samsung/sdraw/cd;)Landroid/graphics/RectF;
    .locals 4

    .line 1
    iget v0, p1, Lcom/samsung/sdraw/cd;->〇0O:F

    .line 2
    .line 3
    const/high16 v1, 0x40000000    # 2.0f

    .line 4
    .line 5
    mul-float v1, v1, v0

    .line 6
    .line 7
    float-to-int v1, v1

    .line 8
    new-instance v2, Landroid/graphics/RectF;

    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    int-to-float v1, v1

    .line 12
    invoke-direct {v2, v3, v3, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 13
    .line 14
    .line 15
    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 16
    .line 17
    sub-float/2addr v1, v0

    .line 18
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 19
    .line 20
    sub-float/2addr p1, v0

    .line 21
    invoke-virtual {v2, v1, p1}, Landroid/graphics/RectF;->offset(FF)V

    .line 22
    .line 23
    .line 24
    return-object v2
    .line 25
.end method

.method private Oo08(Landroid/graphics/Canvas;Lcom/samsung/sdraw/cd;)V
    .locals 11

    .line 1
    iget v0, p2, Lcom/samsung/sdraw/cd;->〇0O:F

    .line 2
    .line 3
    iget v1, p2, Landroid/graphics/PointF;->x:F

    .line 4
    .line 5
    iget p2, p2, Landroid/graphics/PointF;->y:F

    .line 6
    .line 7
    new-instance v2, Landroid/graphics/RectF;

    .line 8
    .line 9
    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 13
    .line 14
    .line 15
    move v2, v0

    .line 16
    :goto_0
    neg-float v3, v0

    .line 17
    cmpl-float v3, v2, v3

    .line 18
    .line 19
    if-gez v3, :cond_0

    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    mul-float v3, v0, v0

    .line 23
    .line 24
    mul-float v4, v2, v2

    .line 25
    .line 26
    sub-float/2addr v3, v4

    .line 27
    float-to-double v3, v3

    .line 28
    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    .line 29
    .line 30
    .line 31
    move-result-wide v3

    .line 32
    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    .line 33
    .line 34
    mul-double v3, v3, v5

    .line 35
    .line 36
    double-to-float v3, v3

    .line 37
    const/high16 v4, 0x40000000    # 2.0f

    .line 38
    .line 39
    div-float v4, v3, v4

    .line 40
    .line 41
    sub-float v6, v1, v4

    .line 42
    .line 43
    sub-float v7, p2, v2

    .line 44
    .line 45
    add-float v8, v6, v3

    .line 46
    .line 47
    const/high16 v3, 0x3f800000    # 1.0f

    .line 48
    .line 49
    add-float v9, v7, v3

    .line 50
    .line 51
    sget-object v10, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    .line 52
    .line 53
    move-object v5, p1

    .line 54
    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    .line 55
    .line 56
    .line 57
    sub-float/2addr v2, v3

    .line 58
    goto :goto_0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method private o〇0(Landroid/graphics/Canvas;Lcom/samsung/sdraw/cd;I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/al;->o〇0:Ljava/util/Vector;

    .line 2
    .line 3
    add-int/lit8 p3, p3, -0x1

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-static {v1, p3}, Ljava/lang/Math;->max(II)I

    .line 7
    .line 8
    .line 9
    move-result p3

    .line 10
    invoke-virtual {v0, p3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object p3

    .line 14
    check-cast p3, Lcom/samsung/sdraw/cd;

    .line 15
    .line 16
    iget v0, p3, Landroid/graphics/PointF;->x:F

    .line 17
    .line 18
    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 19
    .line 20
    iget v1, p2, Landroid/graphics/PointF;->x:F

    .line 21
    .line 22
    iget v2, p2, Landroid/graphics/PointF;->y:F

    .line 23
    .line 24
    invoke-static {v0, p3, v1, v2}, Lcom/samsung/sdraw/PointF;->〇080(FFFF)F

    .line 25
    .line 26
    .line 27
    move-result p3

    .line 28
    iput p3, p0, Lcom/samsung/sdraw/al;->〇80〇808〇O:F

    .line 29
    .line 30
    iget p3, p2, Landroid/graphics/PointF;->x:F

    .line 31
    .line 32
    iget v0, p2, Landroid/graphics/PointF;->y:F

    .line 33
    .line 34
    iget v1, p2, Lcom/samsung/sdraw/cd;->〇0O:F

    .line 35
    .line 36
    iget v2, p2, Lcom/samsung/sdraw/cd;->oOo〇8o008:F

    .line 37
    .line 38
    invoke-direct {p0, p3, v0, v1, v2}, Lcom/samsung/sdraw/al;->〇o〇(FFFF)Landroid/graphics/Matrix;

    .line 39
    .line 40
    .line 41
    move-result-object p3

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 43
    .line 44
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/samsung/sdraw/al;->Oo08(Landroid/graphics/Canvas;Lcom/samsung/sdraw/cd;)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/samsung/sdraw/al;->〇o00〇〇Oo:Lcom/samsung/sdraw/bo;

    .line 49
    .line 50
    iget p2, p2, Lcom/samsung/sdraw/cd;->oOo〇8o008:F

    .line 51
    .line 52
    const/high16 v1, 0x437f0000    # 255.0f

    .line 53
    .line 54
    mul-float p2, p2, v1

    .line 55
    .line 56
    float-to-int p2, p2

    .line 57
    invoke-virtual {v0, p2}, Lcom/samsung/sdraw/bo;->setAlpha(I)V

    .line 58
    .line 59
    .line 60
    iget-object p2, p0, Lcom/samsung/sdraw/al;->〇o〇:Landroid/graphics/Bitmap;

    .line 61
    .line 62
    iget-object v0, p0, Lcom/samsung/sdraw/al;->〇o00〇〇Oo:Lcom/samsung/sdraw/bo;

    .line 63
    .line 64
    invoke-virtual {p1, p2, p3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private 〇o〇(FFFF)Landroid/graphics/Matrix;
    .locals 0

    .line 1
    iget-object p3, p0, Lcom/samsung/sdraw/al;->〇o〇:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result p3

    .line 7
    int-to-float p3, p3

    .line 8
    neg-float p3, p3

    .line 9
    const/high16 p4, 0x3f000000    # 0.5f

    .line 10
    .line 11
    mul-float p3, p3, p4

    .line 12
    .line 13
    new-instance p4, Landroid/graphics/Matrix;

    .line 14
    .line 15
    invoke-direct {p4}, Landroid/graphics/Matrix;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p4, p3, p3}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 19
    .line 20
    .line 21
    iget p3, p0, Lcom/samsung/sdraw/al;->〇80〇808〇O:F

    .line 22
    .line 23
    invoke-virtual {p4, p3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 24
    .line 25
    .line 26
    invoke-virtual {p4, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 27
    .line 28
    .line 29
    return-object p4
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method private 〇〇888()Landroid/graphics/RectF;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/al;->O8:Landroid/graphics/RectF;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 5
    .line 6
    .line 7
    iget v0, p0, Lcom/samsung/sdraw/al;->〇〇888:I

    .line 8
    .line 9
    :goto_0
    iget v1, p0, Lcom/samsung/sdraw/al;->oO80:I

    .line 10
    .line 11
    if-lt v0, v1, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/samsung/sdraw/al;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iget-object v1, p0, Lcom/samsung/sdraw/al;->O8:Landroid/graphics/RectF;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/samsung/sdraw/al;->O8:Landroid/graphics/RectF;

    .line 25
    .line 26
    return-object v0

    .line 27
    :cond_0
    iget-object v1, p0, Lcom/samsung/sdraw/al;->o〇0:Ljava/util/Vector;

    .line 28
    .line 29
    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Lcom/samsung/sdraw/cd;

    .line 34
    .line 35
    iget-object v2, p0, Lcom/samsung/sdraw/al;->O8:Landroid/graphics/RectF;

    .line 36
    .line 37
    invoke-direct {p0, v1}, Lcom/samsung/sdraw/al;->O8(Lcom/samsung/sdraw/cd;)Landroid/graphics/RectF;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-virtual {v2, v1}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 42
    .line 43
    .line 44
    add-int/lit8 v0, v0, 0x1

    .line 45
    .line 46
    goto :goto_0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method


# virtual methods
.method public a(IZ)Landroid/graphics/RectF;
    .locals 0

    const/4 p2, -0x1

    if-eq p1, p2, :cond_1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 2
    :goto_0
    iput p1, p0, Lcom/samsung/sdraw/al;->〇〇888:I

    .line 3
    iget-object p1, p0, Lcom/samsung/sdraw/al;->o〇0:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    iput p1, p0, Lcom/samsung/sdraw/al;->oO80:I

    goto :goto_1

    .line 4
    :cond_1
    iget-object p1, p0, Lcom/samsung/sdraw/al;->o〇0:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    iput p1, p0, Lcom/samsung/sdraw/al;->oO80:I

    .line 5
    :goto_1
    invoke-direct {p0}, Lcom/samsung/sdraw/al;->〇〇888()Landroid/graphics/RectF;

    .line 6
    iget-object p1, p0, Lcom/samsung/sdraw/al;->O8:Landroid/graphics/RectF;

    return-object p1
.end method

.method public a()V
    .locals 0

    .line 1
    return-void
.end method

.method public 〇080(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 5

    .line 1
    iget-object p2, p0, Lcom/samsung/sdraw/al;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/samsung/sdraw/StrokeSprite;->〇8〇0〇o〇O()Z

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    const/4 v0, 0x0

    .line 8
    if-eqz p2, :cond_0

    .line 9
    .line 10
    iput v0, p0, Lcom/samsung/sdraw/al;->〇〇888:I

    .line 11
    .line 12
    iget-object p2, p0, Lcom/samsung/sdraw/al;->o〇0:Ljava/util/Vector;

    .line 13
    .line 14
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    iput p2, p0, Lcom/samsung/sdraw/al;->oO80:I

    .line 19
    .line 20
    :cond_0
    iget p2, p0, Lcom/samsung/sdraw/al;->〇〇888:I

    .line 21
    .line 22
    :goto_0
    iget v1, p0, Lcom/samsung/sdraw/al;->oO80:I

    .line 23
    .line 24
    if-lt p2, v1, :cond_4

    .line 25
    .line 26
    iget-object p2, p0, Lcom/samsung/sdraw/al;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 27
    .line 28
    invoke-virtual {p2}, Lcom/samsung/sdraw/StrokeSprite;->〇8〇0〇o〇O()Z

    .line 29
    .line 30
    .line 31
    move-result p2

    .line 32
    if-eqz p2, :cond_3

    .line 33
    .line 34
    iget-object p2, p0, Lcom/samsung/sdraw/al;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 35
    .line 36
    invoke-virtual {p2}, Lcom/samsung/sdraw/StrokeSprite;->〇oOO8O8()Z

    .line 37
    .line 38
    .line 39
    move-result p2

    .line 40
    if-eqz p2, :cond_3

    .line 41
    .line 42
    new-instance p2, Landroid/graphics/Paint;

    .line 43
    .line 44
    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    .line 45
    .line 46
    .line 47
    const/4 v1, 0x1

    .line 48
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 49
    .line 50
    .line 51
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 52
    .line 53
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 54
    .line 55
    .line 56
    const/high16 v1, 0x40800000    # 4.0f

    .line 57
    .line 58
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 59
    .line 60
    .line 61
    const v2, -0xff0100

    .line 62
    .line 63
    .line 64
    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    .line 66
    .line 67
    iget-object v2, p0, Lcom/samsung/sdraw/al;->o〇0:Ljava/util/Vector;

    .line 68
    .line 69
    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 74
    .line 75
    .line 76
    move-result v3

    .line 77
    if-nez v3, :cond_2

    .line 78
    .line 79
    const v2, -0xff01

    .line 80
    .line 81
    .line 82
    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 86
    .line 87
    .line 88
    :goto_2
    iget-object v1, p0, Lcom/samsung/sdraw/al;->Oo08:Ljava/util/Vector;

    .line 89
    .line 90
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    .line 91
    .line 92
    .line 93
    move-result v1

    .line 94
    if-lt v0, v1, :cond_1

    .line 95
    .line 96
    goto :goto_3

    .line 97
    :cond_1
    iget-object v1, p0, Lcom/samsung/sdraw/al;->Oo08:Ljava/util/Vector;

    .line 98
    .line 99
    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 100
    .line 101
    .line 102
    move-result-object v1

    .line 103
    check-cast v1, Lcom/samsung/sdraw/h;

    .line 104
    .line 105
    iget v2, v1, Lcom/samsung/sdraw/h;->〇080:F

    .line 106
    .line 107
    iget v1, v1, Lcom/samsung/sdraw/h;->〇o00〇〇Oo:F

    .line 108
    .line 109
    invoke-virtual {p1, v2, v1, p2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 110
    .line 111
    .line 112
    add-int/lit8 v0, v0, 0x1

    .line 113
    .line 114
    goto :goto_2

    .line 115
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 116
    .line 117
    .line 118
    move-result-object v3

    .line 119
    check-cast v3, Lcom/samsung/sdraw/cd;

    .line 120
    .line 121
    iget v4, v3, Landroid/graphics/PointF;->x:F

    .line 122
    .line 123
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 124
    .line 125
    invoke-virtual {p1, v4, v3, p2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 126
    .line 127
    .line 128
    goto :goto_1

    .line 129
    :cond_3
    :goto_3
    return-void

    .line 130
    :cond_4
    iget-object v1, p0, Lcom/samsung/sdraw/al;->o〇0:Ljava/util/Vector;

    .line 131
    .line 132
    invoke-virtual {v1, p2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    check-cast v1, Lcom/samsung/sdraw/cd;

    .line 137
    .line 138
    invoke-direct {p0, p1, v1, p2}, Lcom/samsung/sdraw/al;->o〇0(Landroid/graphics/Canvas;Lcom/samsung/sdraw/cd;I)V

    .line 139
    .line 140
    .line 141
    add-int/lit8 p2, p2, 0x1

    .line 142
    .line 143
    goto :goto_0
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public 〇o00〇〇Oo(Lcom/samsung/sdraw/StrokeSprite;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/samsung/sdraw/al;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/samsung/sdraw/StrokeSprite;->OOO〇O0()Lcom/samsung/sdraw/bo;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/samsung/sdraw/al;->〇o00〇〇Oo:Lcom/samsung/sdraw/bo;

    .line 8
    .line 9
    const/16 v1, 0xa0

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Lcom/samsung/sdraw/bo;->setAlpha(I)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/samsung/sdraw/StrokeSprite;->o0ooO()Ljava/util/Vector;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iput-object v0, p0, Lcom/samsung/sdraw/al;->Oo08:Ljava/util/Vector;

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/samsung/sdraw/StrokeSprite;->〇00〇8()Ljava/util/Vector;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    iput-object p1, p0, Lcom/samsung/sdraw/al;->o〇0:Ljava/util/Vector;

    .line 25
    .line 26
    iget-object p1, p0, Lcom/samsung/sdraw/al;->〇o00〇〇Oo:Lcom/samsung/sdraw/bo;

    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/samsung/sdraw/bo;->〇o00〇〇Oo()Landroid/graphics/Bitmap;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    iput-object p1, p0, Lcom/samsung/sdraw/al;->〇o〇:Landroid/graphics/Bitmap;

    .line 33
    .line 34
    new-instance p1, Landroid/graphics/RectF;

    .line 35
    .line 36
    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 37
    .line 38
    .line 39
    iput-object p1, p0, Lcom/samsung/sdraw/al;->O8:Landroid/graphics/RectF;

    .line 40
    .line 41
    const/4 p1, 0x0

    .line 42
    iput p1, p0, Lcom/samsung/sdraw/al;->〇80〇808〇O:F

    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

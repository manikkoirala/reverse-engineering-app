.class Lcom/samsung/sdraw/Setting;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static final O08000:Lcom/samsung/sdraw/StrokeSprite$InputMethod;

.field protected static final O〇O〇oO:[F

.field protected static final o8oO〇:I

.field protected static final oO:Lcom/samsung/sdraw/StrokeSprite$Type;

.field public static oO00OOO:I

.field protected static final o〇8oOO88:Ljava/lang/String;

.field protected static final o〇O:Landroid/text/Layout$Alignment;

.field protected static final 〇08O8o〇0:I

.field protected static final 〇8:Lcom/samsung/sdraw/StrokeSprite$ThicknessParameter;

.field protected static final 〇8〇0〇o〇O:[F


# instance fields
.field protected O8:[F

.field protected O8ooOoo〇:Ljava/lang/String;

.field private O8〇o:Landroid/text/Layout$Alignment;

.field protected OO0o〇〇:Landroid/graphics/PointF;

.field protected OO0o〇〇〇〇0:Lcom/samsung/sdraw/StrokeSprite$Type;

.field private OOO〇O0:I

.field protected Oo08:Landroid/graphics/Rect;

.field private Oo8Oo00oo:Z

.field protected OoO8:[F

.field protected Oooo8o0〇:Landroid/graphics/PointF;

.field protected O〇8O8〇008:I

.field private o0ooO:I

.field private o8:Z

.field protected o800o8O:[F

.field protected oO80:F

.field protected oo88o8O:[F

.field private oo〇:Ljava/lang/String;

.field protected o〇0:Landroid/graphics/Rect;

.field private o〇0OOo〇0:Z

.field private o〇8:Z

.field protected o〇O8〇〇o:I

.field private o〇〇0〇:I

.field protected 〇00:I

.field private 〇0000OOO:I

.field private 〇00〇8:Z

.field protected 〇080:[F

.field protected 〇0〇O0088o:[F

.field protected 〇80〇808〇O:I

.field protected 〇8o8o〇:Lcom/samsung/sdraw/StrokeSprite$ThicknessParameter;

.field protected 〇O00:Z

.field protected 〇O888o0o:[F

.field protected 〇O8o08O:Lcom/samsung/sdraw/StrokeSprite$InputMethod;

.field protected 〇O〇:Z

.field public 〇o:Z

.field protected 〇o00〇〇Oo:[F

.field private 〇oOO8O8:I

.field protected 〇oo〇:Ljava/lang/String;

.field protected 〇o〇:[F

.field private 〇〇0o:I

.field protected 〇〇808〇:F

.field protected 〇〇888:Landroid/view/View;

.field protected 〇〇8O0〇8:[F

.field private 〇〇〇0〇〇0:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/16 v0, 0xff

    .line 2
    .line 3
    const/16 v1, 0x13

    .line 4
    .line 5
    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    sput v0, Lcom/samsung/sdraw/Setting;->〇08O8o〇0:I

    .line 10
    .line 11
    sget-object v0, Lcom/samsung/sdraw/StrokeSprite$Type;->Solid:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 12
    .line 13
    sput-object v0, Lcom/samsung/sdraw/Setting;->oO:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 14
    .line 15
    sget-object v0, Lcom/samsung/sdraw/StrokeSprite$ThicknessParameter;->SpeedAndPressure:Lcom/samsung/sdraw/StrokeSprite$ThicknessParameter;

    .line 16
    .line 17
    sput-object v0, Lcom/samsung/sdraw/Setting;->〇8:Lcom/samsung/sdraw/StrokeSprite$ThicknessParameter;

    .line 18
    .line 19
    sget-object v0, Lcom/samsung/sdraw/StrokeSprite$InputMethod;->Hand:Lcom/samsung/sdraw/StrokeSprite$InputMethod;

    .line 20
    .line 21
    sput-object v0, Lcom/samsung/sdraw/Setting;->O08000:Lcom/samsung/sdraw/StrokeSprite$InputMethod;

    .line 22
    .line 23
    const/4 v0, 0x4

    .line 24
    new-array v1, v0, [F

    .line 25
    .line 26
    fill-array-data v1, :array_0

    .line 27
    .line 28
    .line 29
    sput-object v1, Lcom/samsung/sdraw/Setting;->〇8〇0〇o〇O:[F

    .line 30
    .line 31
    new-array v0, v0, [F

    .line 32
    .line 33
    fill-array-data v0, :array_1

    .line 34
    .line 35
    .line 36
    sput-object v0, Lcom/samsung/sdraw/Setting;->O〇O〇oO:[F

    .line 37
    .line 38
    sget v0, Lcom/samsung/sdraw/g;->o8〇OO0〇0o:I

    .line 39
    .line 40
    sput v0, Lcom/samsung/sdraw/Setting;->o8oO〇:I

    .line 41
    .line 42
    invoke-static {}, Lcom/samsung/sdraw/Setting;->o〇0OOo〇0()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    sput-object v0, Lcom/samsung/sdraw/Setting;->o〇8oOO88:Ljava/lang/String;

    .line 47
    .line 48
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 49
    .line 50
    sput-object v0, Lcom/samsung/sdraw/Setting;->o〇O:Landroid/text/Layout$Alignment;

    .line 51
    .line 52
    const/4 v0, 0x0

    .line 53
    sput v0, Lcom/samsung/sdraw/Setting;->oO00OOO:I

    .line 54
    .line 55
    return-void

    .line 56
    nop

    .line 57
    :array_0
    .array-data 4
        0x0
        0x40a00000    # 5.0f
        0x41b00000    # 22.0f
        0x42aa0000    # 85.0f
    .end array-data

    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    :array_1
    .array-data 4
        0x3d23d70a    # 0.04f
        0x3e23d70a    # 0.16f
        0x3e47ae14    # 0.195f
        0x3fa00000    # 1.25f
    .end array-data
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x14

    .line 5
    .line 6
    iput v0, p0, Lcom/samsung/sdraw/Setting;->〇0000OOO:I

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput v0, p0, Lcom/samsung/sdraw/Setting;->OOO〇O0:I

    .line 10
    .line 11
    sget-object v1, Lcom/samsung/sdraw/Setting;->o〇O:Landroid/text/Layout$Alignment;

    .line 12
    .line 13
    const/4 v2, 0x1

    .line 14
    iput-boolean v2, p0, Lcom/samsung/sdraw/Setting;->〇00〇8:Z

    .line 15
    .line 16
    iput-boolean v2, p0, Lcom/samsung/sdraw/Setting;->〇o:Z

    .line 17
    .line 18
    iput-boolean v0, p0, Lcom/samsung/sdraw/Setting;->o〇8:Z

    .line 19
    .line 20
    iput-boolean v0, p0, Lcom/samsung/sdraw/Setting;->o8:Z

    .line 21
    .line 22
    iput-boolean v0, p0, Lcom/samsung/sdraw/Setting;->Oo8Oo00oo:Z

    .line 23
    .line 24
    iput-boolean v2, p0, Lcom/samsung/sdraw/Setting;->〇〇〇0〇〇0:Z

    .line 25
    .line 26
    iput-boolean v2, p0, Lcom/samsung/sdraw/Setting;->o〇0OOo〇0:Z

    .line 27
    .line 28
    const/16 v3, 0x64

    .line 29
    .line 30
    iput v3, p0, Lcom/samsung/sdraw/Setting;->〇〇0o:I

    .line 31
    .line 32
    const/high16 v3, 0x41000000    # 8.0f

    .line 33
    .line 34
    iput v3, p0, Lcom/samsung/sdraw/Setting;->oO80:F

    .line 35
    .line 36
    sget v3, Lcom/samsung/sdraw/Setting;->〇08O8o〇0:I

    .line 37
    .line 38
    iput v3, p0, Lcom/samsung/sdraw/Setting;->〇80〇808〇O:I

    .line 39
    .line 40
    sget-object v4, Lcom/samsung/sdraw/Setting;->oO:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 41
    .line 42
    iput-object v4, p0, Lcom/samsung/sdraw/Setting;->OO0o〇〇〇〇0:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 43
    .line 44
    sget-object v4, Lcom/samsung/sdraw/Setting;->〇8:Lcom/samsung/sdraw/StrokeSprite$ThicknessParameter;

    .line 45
    .line 46
    iput-object v4, p0, Lcom/samsung/sdraw/Setting;->〇8o8o〇:Lcom/samsung/sdraw/StrokeSprite$ThicknessParameter;

    .line 47
    .line 48
    sget-object v4, Lcom/samsung/sdraw/Setting;->O08000:Lcom/samsung/sdraw/StrokeSprite$InputMethod;

    .line 49
    .line 50
    iput-object v4, p0, Lcom/samsung/sdraw/Setting;->〇O8o08O:Lcom/samsung/sdraw/StrokeSprite$InputMethod;

    .line 51
    .line 52
    const/16 v4, 0xa

    .line 53
    .line 54
    iput v4, p0, Lcom/samsung/sdraw/Setting;->〇oOO8O8:I

    .line 55
    .line 56
    sget v4, Lcom/samsung/sdraw/Setting;->o8oO〇:I

    .line 57
    .line 58
    iput v4, p0, Lcom/samsung/sdraw/Setting;->o〇〇0〇:I

    .line 59
    .line 60
    sget-object v4, Lcom/samsung/sdraw/Setting;->o〇8oOO88:Ljava/lang/String;

    .line 61
    .line 62
    iput-object v4, p0, Lcom/samsung/sdraw/Setting;->oo〇:Ljava/lang/String;

    .line 63
    .line 64
    iput-object v1, p0, Lcom/samsung/sdraw/Setting;->O8〇o:Landroid/text/Layout$Alignment;

    .line 65
    .line 66
    iput v3, p0, Lcom/samsung/sdraw/Setting;->o0ooO:I

    .line 67
    .line 68
    new-instance v1, Landroid/graphics/PointF;

    .line 69
    .line 70
    const/4 v3, 0x0

    .line 71
    invoke-direct {v1, v3, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 72
    .line 73
    .line 74
    iput-object v1, p0, Lcom/samsung/sdraw/Setting;->OO0o〇〇:Landroid/graphics/PointF;

    .line 75
    .line 76
    const/high16 v1, 0x3f800000    # 1.0f

    .line 77
    .line 78
    iput v1, p0, Lcom/samsung/sdraw/Setting;->〇〇808〇:F

    .line 79
    .line 80
    new-instance v1, Landroid/graphics/PointF;

    .line 81
    .line 82
    invoke-direct {v1, v3, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 83
    .line 84
    .line 85
    iput-object v1, p0, Lcom/samsung/sdraw/Setting;->Oooo8o0〇:Landroid/graphics/PointF;

    .line 86
    .line 87
    iput-boolean v0, p0, Lcom/samsung/sdraw/Setting;->〇O〇:Z

    .line 88
    .line 89
    if-eqz p1, :cond_0

    .line 90
    .line 91
    invoke-direct {p0, p1}, Lcom/samsung/sdraw/Setting;->〇o00〇〇Oo(Landroid/content/Context;)V

    .line 92
    .line 93
    .line 94
    :cond_0
    sget-object v1, Lcom/samsung/sdraw/Setting;->〇8〇0〇o〇O:[F

    .line 95
    .line 96
    iput-object v1, p0, Lcom/samsung/sdraw/Setting;->〇〇8O0〇8:[F

    .line 97
    .line 98
    iget-object v1, p0, Lcom/samsung/sdraw/Setting;->〇080:[F

    .line 99
    .line 100
    iput-object v1, p0, Lcom/samsung/sdraw/Setting;->〇0〇O0088o:[F

    .line 101
    .line 102
    sget-object v1, Lcom/samsung/sdraw/Setting;->O〇O〇oO:[F

    .line 103
    .line 104
    iput-object v1, p0, Lcom/samsung/sdraw/Setting;->o800o8O:[F

    .line 105
    .line 106
    iget-object v1, p0, Lcom/samsung/sdraw/Setting;->〇o00〇〇Oo:[F

    .line 107
    .line 108
    iput-object v1, p0, Lcom/samsung/sdraw/Setting;->〇O888o0o:[F

    .line 109
    .line 110
    iget-object v1, p0, Lcom/samsung/sdraw/Setting;->〇o〇:[F

    .line 111
    .line 112
    iput-object v1, p0, Lcom/samsung/sdraw/Setting;->OoO8:[F

    .line 113
    .line 114
    iget-object v1, p0, Lcom/samsung/sdraw/Setting;->O8:[F

    .line 115
    .line 116
    iput-object v1, p0, Lcom/samsung/sdraw/Setting;->oo88o8O:[F

    .line 117
    .line 118
    new-instance v1, Landroid/graphics/Rect;

    .line 119
    .line 120
    invoke-direct {v1, v0, v0, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 121
    .line 122
    .line 123
    invoke-virtual {p0, v1}, Lcom/samsung/sdraw/Setting;->〇08O8o〇0(Landroid/graphics/Rect;)V

    .line 124
    .line 125
    .line 126
    new-instance v1, Landroid/graphics/Rect;

    .line 127
    .line 128
    invoke-direct {v1, v0, v0, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 129
    .line 130
    .line 131
    invoke-virtual {p0, v1}, Lcom/samsung/sdraw/Setting;->〇8〇0〇o〇O(Landroid/graphics/Rect;)V

    .line 132
    .line 133
    .line 134
    const-string v0, "/mnt/sdcard/android/data/com.samsung.sdraw/cache"

    .line 135
    .line 136
    if-eqz p1, :cond_2

    .line 137
    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    .line 139
    .line 140
    .line 141
    move-result-object v1

    .line 142
    if-eqz v1, :cond_1

    .line 143
    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    .line 145
    .line 146
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object v1

    .line 150
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 151
    .line 152
    .line 153
    move-result-object v1

    .line 154
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    const-string v1, "/cache"

    .line 158
    .line 159
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v0

    .line 166
    iput-object v0, p0, Lcom/samsung/sdraw/Setting;->〇oo〇:Ljava/lang/String;

    .line 167
    .line 168
    goto :goto_0

    .line 169
    :cond_1
    iput-object v0, p0, Lcom/samsung/sdraw/Setting;->〇oo〇:Ljava/lang/String;

    .line 170
    .line 171
    goto :goto_0

    .line 172
    :cond_2
    iput-object v0, p0, Lcom/samsung/sdraw/Setting;->〇oo〇:Ljava/lang/String;

    .line 173
    .line 174
    :goto_0
    const/16 v0, 0x46

    .line 175
    .line 176
    iput v0, p0, Lcom/samsung/sdraw/Setting;->o〇O8〇〇o:I

    .line 177
    .line 178
    const/16 v0, 0x1e

    .line 179
    .line 180
    iput v0, p0, Lcom/samsung/sdraw/Setting;->〇00:I

    .line 181
    .line 182
    if-eqz p1, :cond_3

    .line 183
    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    .line 185
    .line 186
    const-string v1, "/mnt/sdcard/android/data/"

    .line 187
    .line 188
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 189
    .line 190
    .line 191
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object p1

    .line 195
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    const-string p1, "/serial"

    .line 199
    .line 200
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    .line 202
    .line 203
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 204
    .line 205
    .line 206
    move-result-object p1

    .line 207
    iput-object p1, p0, Lcom/samsung/sdraw/Setting;->O8ooOoo〇:Ljava/lang/String;

    .line 208
    .line 209
    :cond_3
    const/16 p1, 0x2710

    .line 210
    .line 211
    iput p1, p0, Lcom/samsung/sdraw/Setting;->O〇8O8〇008:I

    .line 212
    .line 213
    iput-boolean v2, p0, Lcom/samsung/sdraw/Setting;->〇O00:Z

    .line 214
    .line 215
    return-void
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method private static o〇0OOo〇0()Ljava/lang/String;
    .locals 6

    .line 1
    new-instance v0, Ljava/io/File;

    .line 2
    .line 3
    const-string v1, "/system/fonts/"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "Sans serif"

    .line 13
    .line 14
    if-eqz v0, :cond_2

    .line 15
    .line 16
    array-length v2, v0

    .line 17
    const/4 v3, 0x0

    .line 18
    :goto_0
    if-lt v3, v2, :cond_0

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_0
    aget-object v4, v0, v3

    .line 22
    .line 23
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v4

    .line 27
    const-string v5, "Sans.ttf"

    .line 28
    .line 29
    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    .line 30
    .line 31
    .line 32
    move-result v4

    .line 33
    if-eqz v4, :cond_1

    .line 34
    .line 35
    move-object v0, v1

    .line 36
    goto :goto_2

    .line 37
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_2
    :goto_1
    const/4 v0, 0x0

    .line 41
    :goto_2
    if-nez v0, :cond_3

    .line 42
    .line 43
    goto :goto_3

    .line 44
    :cond_3
    move-object v1, v0

    .line 45
    :goto_3
    return-object v1
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method private 〇o00〇〇Oo(Landroid/content/Context;)V
    .locals 1

    .line 1
    const/4 p1, 0x4

    .line 2
    new-array v0, p1, [F

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    iput-object v0, p0, Lcom/samsung/sdraw/Setting;->〇080:[F

    .line 8
    .line 9
    new-array v0, p1, [F

    .line 10
    .line 11
    fill-array-data v0, :array_1

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/samsung/sdraw/Setting;->〇o00〇〇Oo:[F

    .line 15
    .line 16
    new-array v0, p1, [F

    .line 17
    .line 18
    fill-array-data v0, :array_2

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/samsung/sdraw/Setting;->〇o〇:[F

    .line 22
    .line 23
    new-array p1, p1, [F

    .line 24
    .line 25
    fill-array-data p1, :array_3

    .line 26
    .line 27
    .line 28
    iput-object p1, p0, Lcom/samsung/sdraw/Setting;->O8:[F

    .line 29
    .line 30
    return-void

    .line 31
    :array_0
    .array-data 4
        0x0
        0x40866666    # 4.2f
        0x41180000    # 9.5f
        0x420c0000    # 35.0f
    .end array-data

    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    :array_1
    .array-data 4
        0x3e99999a    # 0.3f
        0x3ec28f5c    # 0.38f
        0x3eeb851f    # 0.46f
        0x3f0ccccd    # 0.55f
    .end array-data

    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    :array_2
    .array-data 4
        0x0
        0x40866666    # 4.2f
        0x41180000    # 9.5f
        0x420c0000    # 35.0f
    .end array-data

    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    :array_3
    .array-data 4
        0x3ecccccd    # 0.4f
        0x3f2e147b    # 0.68f
        0x3f428f5c    # 0.76f
        0x3f59999a    # 0.85f
    .end array-data
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method


# virtual methods
.method public O000(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/samsung/sdraw/Setting;->o〇〇0〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public O08000(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/samsung/sdraw/Setting;->〇0000OOO:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public O0o〇〇Oo(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/samsung/sdraw/Setting;->〇00〇8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method O8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/samsung/sdraw/Setting;->o〇8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public O8ooOoo〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->oo〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method O8〇o()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/samsung/sdraw/Setting;->o〇O8〇〇o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public OO0o〇〇()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->Oo08:Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method OO0o〇〇〇〇0()[F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->OO0o〇〇〇〇0:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 2
    .line 3
    sget-object v1, Lcom/samsung/sdraw/StrokeSprite$Type;->Zenbrush:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->oo88o8O:[F

    .line 8
    .line 9
    return-object v0

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->〇O888o0o:[F

    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public OOO(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/samsung/sdraw/Setting;->OOO〇O0:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public OOO〇O0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/samsung/sdraw/Setting;->〇〇〇0〇〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method Oo08()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/samsung/sdraw/Setting;->〇O〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method Oo8Oo00oo()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/samsung/sdraw/Setting;->o8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public OoO8()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->o〇0:Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public Ooo(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/samsung/sdraw/Setting;->o〇0OOo〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public Oooo8o0〇()Landroid/graphics/Rect;
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/samsung/sdraw/Setting;->Oo08:Landroid/graphics/Rect;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public O〇8O8〇008()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/samsung/sdraw/Setting;->o〇〇0〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public O〇O〇oO(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/samsung/sdraw/Setting;->〇80〇808〇O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method o0ooO()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/samsung/sdraw/Setting;->〇O00:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method o8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/samsung/sdraw/Setting;->o〇8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o800o8O()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/samsung/sdraw/Setting;->〇80〇808〇O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o8oO〇(Lcom/samsung/sdraw/StrokeSprite$ThicknessParameter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/sdraw/Setting;->〇8o8o〇:Lcom/samsung/sdraw/StrokeSprite$ThicknessParameter;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public oO(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/samsung/sdraw/Setting;->o0ooO:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public oO00OOO(Landroid/text/Layout$Alignment;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/sdraw/Setting;->O8〇o:Landroid/text/Layout$Alignment;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method oO80()[F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->OO0o〇〇〇〇0:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 2
    .line 3
    sget-object v1, Lcom/samsung/sdraw/StrokeSprite$Type;->Zenbrush:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->OoO8:[F

    .line 8
    .line 9
    return-object v0

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->〇0〇O0088o:[F

    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public oo88o8O()Lcom/samsung/sdraw/StrokeSprite$ThicknessParameter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->〇8o8o〇:Lcom/samsung/sdraw/StrokeSprite$ThicknessParameter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public ooo〇8oO(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/samsung/sdraw/Setting;->〇〇〇0〇〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public oo〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/samsung/sdraw/Setting;->〇00〇8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method o〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/samsung/sdraw/Setting;->o8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method o〇8()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->〇〇888:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o〇8oOO88(Lcom/samsung/sdraw/StrokeSprite$Type;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/sdraw/Setting;->OO0o〇〇〇〇0:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public o〇O(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/samsung/sdraw/Setting;->oO80:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public o〇O8〇〇o()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->OO0o〇〇〇〇0:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 2
    .line 3
    sget-object v1, Lcom/samsung/sdraw/StrokeSprite$Type;->Zenbrush:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    iget v0, p0, Lcom/samsung/sdraw/Setting;->oO80:F

    .line 8
    .line 9
    const v1, 0x3f8ccccd    # 1.1f

    .line 10
    .line 11
    .line 12
    mul-float v0, v0, v1

    .line 13
    .line 14
    return v0

    .line 15
    :cond_0
    iget v0, p0, Lcom/samsung/sdraw/Setting;->oO80:F

    .line 16
    .line 17
    return v0
    .line 18
    .line 19
.end method

.method public o〇〇0〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/samsung/sdraw/Setting;->OOO〇O0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇00()Landroid/text/Layout$Alignment;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->O8〇o:Landroid/text/Layout$Alignment;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇0000OOO()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/samsung/sdraw/Setting;->〇oOO8O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method 〇00〇8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/samsung/sdraw/Setting;->〇00:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method 〇080(I)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/samsung/sdraw/Setting;->〇oo〇:Ljava/lang/String;

    .line 4
    .line 5
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const-string v1, "/"

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    iput-object p1, p0, Lcom/samsung/sdraw/Setting;->〇oo〇:Ljava/lang/String;

    .line 29
    .line 30
    :try_start_0
    new-instance p1, Ljava/io/File;

    .line 31
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    iget-object v1, p0, Lcom/samsung/sdraw/Setting;->〇oo〇:Ljava/lang/String;

    .line 35
    .line 36
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    const-string v1, "/.nomedia"

    .line 44
    .line 45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    .line 57
    .line 58
    :catch_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public 〇08O8o〇0(Landroid/graphics/Rect;)V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/samsung/sdraw/Setting;->Oo08:Landroid/graphics/Rect;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇0〇O0088o()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->o〇0:Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇8(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/samsung/sdraw/Setting;->〇〇0o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇80(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/sdraw/Setting;->oo〇:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method 〇80〇808〇O()[F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->o800o8O:[F

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method 〇8o8o〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->O8ooOoo〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇8〇0〇o〇O(Landroid/graphics/Rect;)V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/samsung/sdraw/Setting;->o〇0:Landroid/graphics/Rect;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇O00()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/samsung/sdraw/Setting;->o0ooO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇O888o0o()Lcom/samsung/sdraw/StrokeSprite$InputMethod;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->〇O8o08O:Lcom/samsung/sdraw/StrokeSprite$InputMethod;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method 〇O8o08O()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->〇oo〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇O〇()Landroid/graphics/PointF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->Oooo8o0〇:Landroid/graphics/PointF;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇O〇80o08O(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/samsung/sdraw/Setting;->〇oOO8O8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method 〇o()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/samsung/sdraw/Setting;->O〇8O8〇008:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇oOO8O8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/samsung/sdraw/Setting;->o〇0OOo〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇oo〇()Lcom/samsung/sdraw/StrokeSprite$Type;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->OO0o〇〇〇〇0:Lcom/samsung/sdraw/StrokeSprite$Type;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method 〇o〇(Lcom/samsung/sdraw/StrokeSprite$InputMethod;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/sdraw/Setting;->〇O8o08O:Lcom/samsung/sdraw/StrokeSprite$InputMethod;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇〇0o()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->〇oo〇:Ljava/lang/String;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance v0, Ljava/io/File;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/samsung/sdraw/Setting;->〇oo〇:Ljava/lang/String;

    .line 9
    .line 10
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    if-eqz v1, :cond_3

    .line 18
    .line 19
    array-length v2, v1

    .line 20
    const/4 v3, 0x0

    .line 21
    :goto_0
    if-lt v3, v2, :cond_1

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    aget-object v4, v1, v3

    .line 25
    .line 26
    new-instance v5, Ljava/io/File;

    .line 27
    .line 28
    new-instance v6, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    iget-object v7, p0, Lcom/samsung/sdraw/Setting;->〇oo〇:Ljava/lang/String;

    .line 31
    .line 32
    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v7

    .line 36
    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v4

    .line 46
    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    .line 50
    .line 51
    .line 52
    move-result v4

    .line 53
    if-eqz v4, :cond_2

    .line 54
    .line 55
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    if-nez v4, :cond_2

    .line 60
    .line 61
    return-void

    .line 62
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_3
    :goto_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 66
    .line 67
    .line 68
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public 〇〇808〇()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->Oo08:Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method 〇〇888()[F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Setting;->〇〇8O0〇8:[F

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇〇8O0〇8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/samsung/sdraw/Setting;->〇〇0o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method 〇〇〇0〇〇0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/samsung/sdraw/Setting;->Oo8Oo00oo:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

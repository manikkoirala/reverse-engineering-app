.class Lcom/samsung/sdraw/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/sdraw/br;


# instance fields
.field private O8:Landroid/graphics/RectF;

.field private Oo08:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/samsung/sdraw/cd;",
            ">;"
        }
    .end annotation
.end field

.field private o〇0:I

.field private 〇080:Lcom/samsung/sdraw/StrokeSprite;

.field private 〇o00〇〇Oo:Landroid/graphics/Paint;

.field private 〇o〇:Landroid/graphics/Paint;

.field private 〇〇888:I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private O8(Landroid/graphics/Canvas;Lcom/samsung/sdraw/cd;I)V
    .locals 2

    .line 1
    iget-object p3, p0, Lcom/samsung/sdraw/e;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 2
    .line 3
    iget v0, p2, Lcom/samsung/sdraw/cd;->oOo〇8o008:F

    .line 4
    .line 5
    const/high16 v1, 0x437f0000    # 255.0f

    .line 6
    .line 7
    mul-float v0, v0, v1

    .line 8
    .line 9
    const/high16 v1, 0x43190000    # 153.0f

    .line 10
    .line 11
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/high16 v1, 0x42480000    # 50.0f

    .line 16
    .line 17
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    float-to-int v0, v0

    .line 22
    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 23
    .line 24
    .line 25
    iget p3, p2, Landroid/graphics/PointF;->x:F

    .line 26
    .line 27
    iget v0, p2, Landroid/graphics/PointF;->y:F

    .line 28
    .line 29
    iget p2, p2, Lcom/samsung/sdraw/cd;->〇0O:F

    .line 30
    .line 31
    iget-object v1, p0, Lcom/samsung/sdraw/e;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 32
    .line 33
    invoke-virtual {p1, p3, v0, p2, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private Oo08()Landroid/graphics/RectF;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/e;->O8:Landroid/graphics/RectF;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 5
    .line 6
    .line 7
    iget v0, p0, Lcom/samsung/sdraw/e;->o〇0:I

    .line 8
    .line 9
    :goto_0
    iget v1, p0, Lcom/samsung/sdraw/e;->〇〇888:I

    .line 10
    .line 11
    if-lt v0, v1, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/samsung/sdraw/e;->O8:Landroid/graphics/RectF;

    .line 14
    .line 15
    const/high16 v1, -0x40800000    # -1.0f

    .line 16
    .line 17
    invoke-virtual {v0, v1, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/samsung/sdraw/e;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/samsung/sdraw/e;->O8:Landroid/graphics/RectF;

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/samsung/sdraw/e;->O8:Landroid/graphics/RectF;

    .line 32
    .line 33
    return-object v0

    .line 34
    :cond_0
    iget-object v1, p0, Lcom/samsung/sdraw/e;->Oo08:Ljava/util/Vector;

    .line 35
    .line 36
    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    check-cast v1, Lcom/samsung/sdraw/cd;

    .line 41
    .line 42
    invoke-direct {p0, v1}, Lcom/samsung/sdraw/e;->〇o〇(Lcom/samsung/sdraw/cd;)Landroid/graphics/RectF;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    iget-object v2, p0, Lcom/samsung/sdraw/e;->O8:Landroid/graphics/RectF;

    .line 47
    .line 48
    invoke-virtual {v2, v1}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 49
    .line 50
    .line 51
    add-int/lit8 v0, v0, 0x1

    .line 52
    .line 53
    goto :goto_0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method private 〇o〇(Lcom/samsung/sdraw/cd;)Landroid/graphics/RectF;
    .locals 4

    .line 1
    iget v0, p1, Lcom/samsung/sdraw/cd;->〇0O:F

    .line 2
    .line 3
    const/high16 v1, 0x40000000    # 2.0f

    .line 4
    .line 5
    mul-float v1, v1, v0

    .line 6
    .line 7
    float-to-int v1, v1

    .line 8
    new-instance v2, Landroid/graphics/RectF;

    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    int-to-float v1, v1

    .line 12
    invoke-direct {v2, v3, v3, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 13
    .line 14
    .line 15
    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 16
    .line 17
    sub-float/2addr v1, v0

    .line 18
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 19
    .line 20
    sub-float/2addr p1, v0

    .line 21
    invoke-virtual {v2, v1, p1}, Landroid/graphics/RectF;->offset(FF)V

    .line 22
    .line 23
    .line 24
    return-object v2
    .line 25
.end method


# virtual methods
.method public a(IZ)Landroid/graphics/RectF;
    .locals 0

    const/4 p2, -0x1

    if-eq p1, p2, :cond_1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 2
    :goto_0
    iput p1, p0, Lcom/samsung/sdraw/e;->o〇0:I

    .line 3
    iget-object p1, p0, Lcom/samsung/sdraw/e;->Oo08:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    iput p1, p0, Lcom/samsung/sdraw/e;->〇〇888:I

    goto :goto_1

    .line 4
    :cond_1
    iget-object p1, p0, Lcom/samsung/sdraw/e;->Oo08:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    iput p1, p0, Lcom/samsung/sdraw/e;->〇〇888:I

    .line 5
    :goto_1
    invoke-direct {p0}, Lcom/samsung/sdraw/e;->Oo08()Landroid/graphics/RectF;

    .line 6
    iget-object p1, p0, Lcom/samsung/sdraw/e;->O8:Landroid/graphics/RectF;

    return-object p1
.end method

.method public a()V
    .locals 0

    .line 1
    return-void
.end method

.method public 〇080(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 1

    .line 1
    iget-object p2, p0, Lcom/samsung/sdraw/e;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/samsung/sdraw/StrokeSprite;->〇8〇0〇o〇O()Z

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    if-eqz p2, :cond_0

    .line 8
    .line 9
    const/4 p2, 0x0

    .line 10
    iput p2, p0, Lcom/samsung/sdraw/e;->o〇0:I

    .line 11
    .line 12
    iget-object p2, p0, Lcom/samsung/sdraw/e;->Oo08:Ljava/util/Vector;

    .line 13
    .line 14
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    iput p2, p0, Lcom/samsung/sdraw/e;->〇〇888:I

    .line 19
    .line 20
    :cond_0
    iget p2, p0, Lcom/samsung/sdraw/e;->o〇0:I

    .line 21
    .line 22
    :goto_0
    iget v0, p0, Lcom/samsung/sdraw/e;->〇〇888:I

    .line 23
    .line 24
    if-lt p2, v0, :cond_1

    .line 25
    .line 26
    return-void

    .line 27
    :cond_1
    iget-object v0, p0, Lcom/samsung/sdraw/e;->Oo08:Ljava/util/Vector;

    .line 28
    .line 29
    invoke-virtual {v0, p2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Lcom/samsung/sdraw/cd;

    .line 34
    .line 35
    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/sdraw/e;->O8(Landroid/graphics/Canvas;Lcom/samsung/sdraw/cd;I)V

    .line 36
    .line 37
    .line 38
    add-int/lit8 p2, p2, 0x1

    .line 39
    .line 40
    goto :goto_0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public 〇o00〇〇Oo(Lcom/samsung/sdraw/StrokeSprite;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/samsung/sdraw/e;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/samsung/sdraw/StrokeSprite;->OOO〇O0()Lcom/samsung/sdraw/bo;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/samsung/sdraw/e;->〇o〇:Landroid/graphics/Paint;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/samsung/sdraw/StrokeSprite;->〇00〇8()Ljava/util/Vector;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iput-object p1, p0, Lcom/samsung/sdraw/e;->Oo08:Ljava/util/Vector;

    .line 14
    .line 15
    new-instance p1, Landroid/graphics/RectF;

    .line 16
    .line 17
    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 18
    .line 19
    .line 20
    iput-object p1, p0, Lcom/samsung/sdraw/e;->O8:Landroid/graphics/RectF;

    .line 21
    .line 22
    new-instance p1, Landroid/graphics/Paint;

    .line 23
    .line 24
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 25
    .line 26
    .line 27
    iput-object p1, p0, Lcom/samsung/sdraw/e;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 28
    .line 29
    const/4 v0, 0x1

    .line 30
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/samsung/sdraw/e;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 36
    .line 37
    .line 38
    iget-object p1, p0, Lcom/samsung/sdraw/e;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 39
    .line 40
    iget-object v0, p0, Lcom/samsung/sdraw/e;->〇o〇:Landroid/graphics/Paint;

    .line 41
    .line 42
    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 47
    .line 48
    .line 49
    iget-object p1, p0, Lcom/samsung/sdraw/e;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 50
    .line 51
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 52
    .line 53
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 54
    .line 55
    .line 56
    iget-object p1, p0, Lcom/samsung/sdraw/e;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 57
    .line 58
    const/high16 v0, 0x3f800000    # 1.0f

    .line 59
    .line 60
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 61
    .line 62
    .line 63
    iget-object p1, p0, Lcom/samsung/sdraw/e;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 64
    .line 65
    iget-object v1, p0, Lcom/samsung/sdraw/e;->〇o〇:Landroid/graphics/Paint;

    .line 66
    .line 67
    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 72
    .line 73
    .line 74
    iget-object p1, p0, Lcom/samsung/sdraw/e;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 75
    .line 76
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    .line 77
    .line 78
    sget-object v2, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    .line 79
    .line 80
    invoke-direct {v1, v0, v2}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 84
    .line 85
    .line 86
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
.end method

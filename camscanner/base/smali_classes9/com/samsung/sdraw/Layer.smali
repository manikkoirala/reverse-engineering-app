.class Lcom/samsung/sdraw/Layer;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final oO80:Landroid/graphics/Matrix;


# instance fields
.field public O8:F

.field public Oo08:Landroid/graphics/Matrix;

.field public o〇0:Landroid/graphics/Matrix;

.field protected 〇080:Landroid/graphics/Canvas;

.field protected 〇o00〇〇Oo:Landroid/graphics/Bitmap;

.field public 〇o〇:Lcom/samsung/sdraw/PointF;

.field private 〇〇888:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/samsung/sdraw/Layer;->oO80:Landroid/graphics/Matrix;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public constructor <init>(II)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/samsung/sdraw/Layer;->〇〇888:Z

    .line 6
    .line 7
    new-instance v0, Lcom/samsung/sdraw/PointF;

    .line 8
    .line 9
    invoke-direct {v0}, Lcom/samsung/sdraw/PointF;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/samsung/sdraw/Layer;->〇o〇:Lcom/samsung/sdraw/PointF;

    .line 13
    .line 14
    const/high16 v0, 0x3f800000    # 1.0f

    .line 15
    .line 16
    iput v0, p0, Lcom/samsung/sdraw/Layer;->O8:F

    .line 17
    .line 18
    new-instance v0, Landroid/graphics/Matrix;

    .line 19
    .line 20
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/samsung/sdraw/Layer;->Oo08:Landroid/graphics/Matrix;

    .line 24
    .line 25
    new-instance v0, Landroid/graphics/Matrix;

    .line 26
    .line 27
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object v0, p0, Lcom/samsung/sdraw/Layer;->o〇0:Landroid/graphics/Matrix;

    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/samsung/sdraw/Layer;->〇80〇808〇O()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, p1, p2}, Lcom/samsung/sdraw/Layer;->oO80(II)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public O8(Landroid/graphics/Matrix;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Layer;->Oo08:Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/samsung/sdraw/Layer;->Oo08:Landroid/graphics/Matrix;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/samsung/sdraw/Layer;->o〇0:Landroid/graphics/Matrix;

    .line 9
    .line 10
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public OO0o〇〇〇〇0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/samsung/sdraw/Layer;->〇〇888:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public Oo08(Landroid/graphics/RectF;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 8
    .line 9
    .line 10
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 13
    .line 14
    .line 15
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    .line 16
    .line 17
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    .line 18
    .line 19
    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 23
    .line 24
    .line 25
    iget-object v1, p0, Lcom/samsung/sdraw/Layer;->〇080:Landroid/graphics/Canvas;

    .line 26
    .line 27
    invoke-virtual {v1, p1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method protected oO80(II)V
    .locals 1

    .line 1
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2
    .line 3
    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    iput-object p1, p0, Lcom/samsung/sdraw/Layer;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/samsung/sdraw/Layer;->〇〇888()V

    .line 10
    .line 11
    .line 12
    new-instance p1, Landroid/graphics/Canvas;

    .line 13
    .line 14
    invoke-direct {p1}, Landroid/graphics/Canvas;-><init>()V

    .line 15
    .line 16
    .line 17
    iput-object p1, p0, Lcom/samsung/sdraw/Layer;->〇080:Landroid/graphics/Canvas;

    .line 18
    .line 19
    iget-object p2, p0, Lcom/samsung/sdraw/Layer;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 20
    .line 21
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public o〇0(Lcom/samsung/sdraw/PointF;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/samsung/sdraw/PointF;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/samsung/sdraw/PointF;-><init>(Landroid/graphics/PointF;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/samsung/sdraw/Layer;->〇o〇:Lcom/samsung/sdraw/PointF;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/samsung/sdraw/Layer;->〇80〇808〇O()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇080()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Layer;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 6
    .line 7
    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/samsung/sdraw/Layer;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/samsung/sdraw/Layer;->〇080:Landroid/graphics/Canvas;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method protected 〇80〇808〇O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Layer;->Oo08:Landroid/graphics/Matrix;

    .line 2
    .line 3
    iget v1, p0, Lcom/samsung/sdraw/Layer;->O8:F

    .line 4
    .line 5
    invoke-virtual {v0, v1, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/samsung/sdraw/Layer;->Oo08:Landroid/graphics/Matrix;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/samsung/sdraw/Layer;->〇o〇:Lcom/samsung/sdraw/PointF;

    .line 11
    .line 12
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 13
    .line 14
    neg-float v2, v2

    .line 15
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 16
    .line 17
    neg-float v1, v1

    .line 18
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/samsung/sdraw/Layer;->Oo08:Landroid/graphics/Matrix;

    .line 22
    .line 23
    iget-object v1, p0, Lcom/samsung/sdraw/Layer;->o〇0:Landroid/graphics/Matrix;

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public 〇8o8o〇(Lcom/samsung/sdraw/AbstractSprite;Landroid/graphics/RectF;)V
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/samsung/sdraw/z;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractSprite;->〇8o8o〇()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/samsung/sdraw/Layer;->OO0o〇〇〇〇0()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_2

    .line 16
    .line 17
    :cond_0
    return-void

    .line 18
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractSprite;->〇8o8o〇()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_3

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/samsung/sdraw/Layer;->OO0o〇〇〇〇0()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_3

    .line 29
    .line 30
    invoke-virtual {p1, p2}, Lcom/samsung/sdraw/AbstractSprite;->o〇0(Landroid/graphics/RectF;)Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-nez v0, :cond_2

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_2
    iget-object v0, p0, Lcom/samsung/sdraw/Layer;->〇080:Landroid/graphics/Canvas;

    .line 38
    .line 39
    invoke-interface {p1, v0, p2}, Lcom/samsung/sdraw/as;->〇080(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    .line 40
    .line 41
    .line 42
    :cond_3
    :goto_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public 〇o00〇〇Oo(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/samsung/sdraw/Layer;->O8:F

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/samsung/sdraw/Layer;->〇80〇808〇O()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇o〇(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Layer;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 6
    .line 7
    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/samsung/sdraw/Layer;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 10
    .line 11
    invoke-virtual {p0, p1, p2}, Lcom/samsung/sdraw/Layer;->oO80(II)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public 〇〇888()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/Layer;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

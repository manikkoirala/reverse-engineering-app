.class Lcom/samsung/sdraw/SelectMode;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/sdraw/ModeState;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sdraw/SelectMode$HitDirection;,
        Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;
    }
.end annotation


# static fields
.field private static synthetic OOO:[I


# instance fields
.field private O000:Landroid/graphics/Bitmap;

.field private O08000:Landroid/graphics/Bitmap;

.field private O8:[Lcom/samsung/sdraw/PointF;

.field private O8ooOoo〇:I

.field private O8〇o:I

.field private OO0o〇〇:I

.field private OO0o〇〇〇〇0:Z

.field private OOO〇O0:I

.field private Oo08:Lcom/samsung/sdraw/PointF;

.field private Oo8Oo00oo:Z

.field private OoO8:I

.field private Ooo:Landroid/graphics/Bitmap;

.field private Oooo8o0〇:Z

.field private O〇8O8〇008:Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;

.field private O〇O〇oO:Landroid/graphics/Bitmap;

.field private o0ooO:Landroid/graphics/Rect;

.field private o8:Landroid/graphics/Rect;

.field private o800o8O:I

.field private o8oO〇:Landroid/graphics/Bitmap;

.field private oO:Landroid/graphics/Bitmap;

.field private oO00OOO:Landroid/graphics/Bitmap;

.field private oO80:Landroid/graphics/Bitmap;

.field private oo88o8O:[Lcom/samsung/sdraw/PointF;

.field private oo〇:I

.field private o〇0:Landroid/graphics/Bitmap;

.field private o〇0OOo〇0:Z

.field private o〇8:Landroid/graphics/Rect;

.field private o〇8oOO88:Landroid/graphics/Bitmap;

.field private o〇O:Landroid/graphics/Bitmap;

.field private o〇O8〇〇o:Z

.field private o〇〇0〇:I

.field private 〇00:Z

.field private 〇0000OOO:I

.field private 〇00〇8:I

.field private 〇080:Landroid/graphics/Paint;

.field private 〇08O8o〇0:Landroid/graphics/Bitmap;

.field private 〇0〇O0088o:I

.field private 〇8:Landroid/graphics/Bitmap;

.field private 〇80:Landroid/graphics/Bitmap;

.field private 〇80〇808〇O:Landroid/graphics/Bitmap;

.field protected 〇8o8o〇:Lcom/samsung/sdraw/SelectMode$HitDirection;

.field private 〇8〇0〇o〇O:Landroid/graphics/Bitmap;

.field private 〇O00:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/samsung/sdraw/PointF;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O888o0o:Lcom/samsung/sdraw/PointF;

.field private 〇O8o08O:I

.field private 〇O〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/samsung/sdraw/PointF;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O〇80o08O:Landroid/graphics/Bitmap;

.field private 〇o:I

.field public 〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

.field private 〇oOO8O8:I

.field private 〇oo〇:F

.field private 〇o〇:Z

.field private 〇〇0o:Z

.field private 〇〇808〇:Z

.field private 〇〇888:Landroid/graphics/Bitmap;

.field private 〇〇8O0〇8:I

.field private 〇〇〇0〇〇0:Z


# direct methods
.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇o〇:Z

    .line 9
    .line 10
    const/4 v2, 0x2

    .line 11
    new-array v3, v2, [Lcom/samsung/sdraw/PointF;

    .line 12
    .line 13
    new-instance v4, Lcom/samsung/sdraw/PointF;

    .line 14
    .line 15
    const/4 v5, 0x0

    .line 16
    invoke-direct {v4, v5, v5}, Lcom/samsung/sdraw/PointF;-><init>(FF)V

    .line 17
    .line 18
    .line 19
    aput-object v4, v3, v1

    .line 20
    .line 21
    new-instance v4, Lcom/samsung/sdraw/PointF;

    .line 22
    .line 23
    invoke-direct {v4, v5, v5}, Lcom/samsung/sdraw/PointF;-><init>(FF)V

    .line 24
    .line 25
    .line 26
    const/4 v6, 0x1

    .line 27
    aput-object v4, v3, v6

    .line 28
    .line 29
    iput-object v3, p0, Lcom/samsung/sdraw/SelectMode;->O8:[Lcom/samsung/sdraw/PointF;

    .line 30
    .line 31
    new-instance v3, Lcom/samsung/sdraw/PointF;

    .line 32
    .line 33
    invoke-direct {v3, v5, v5}, Lcom/samsung/sdraw/PointF;-><init>(FF)V

    .line 34
    .line 35
    .line 36
    iput-object v3, p0, Lcom/samsung/sdraw/SelectMode;->Oo08:Lcom/samsung/sdraw/PointF;

    .line 37
    .line 38
    const/16 v3, 0x9c

    .line 39
    .line 40
    iput v3, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 41
    .line 42
    iput v3, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 43
    .line 44
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->Oooo8o0〇:Z

    .line 45
    .line 46
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇808〇:Z

    .line 47
    .line 48
    new-instance v3, Ljava/util/ArrayList;

    .line 49
    .line 50
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .line 52
    .line 53
    iput-object v3, p0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 54
    .line 55
    new-instance v3, Ljava/util/ArrayList;

    .line 56
    .line 57
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 58
    .line 59
    .line 60
    iput-object v3, p0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 61
    .line 62
    iput v1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇8O0〇8:I

    .line 63
    .line 64
    const/16 v3, 0x310

    .line 65
    .line 66
    iput v3, p0, Lcom/samsung/sdraw/SelectMode;->〇0〇O0088o:I

    .line 67
    .line 68
    iput v1, p0, Lcom/samsung/sdraw/SelectMode;->OoO8:I

    .line 69
    .line 70
    const/16 v3, 0x468

    .line 71
    .line 72
    iput v3, p0, Lcom/samsung/sdraw/SelectMode;->o800o8O:I

    .line 73
    .line 74
    new-instance v3, Lcom/samsung/sdraw/PointF;

    .line 75
    .line 76
    invoke-direct {v3}, Lcom/samsung/sdraw/PointF;-><init>()V

    .line 77
    .line 78
    .line 79
    iput-object v3, p0, Lcom/samsung/sdraw/SelectMode;->〇O888o0o:Lcom/samsung/sdraw/PointF;

    .line 80
    .line 81
    const/4 v3, 0x4

    .line 82
    new-array v3, v3, [Lcom/samsung/sdraw/PointF;

    .line 83
    .line 84
    iput-object v3, p0, Lcom/samsung/sdraw/SelectMode;->oo88o8O:[Lcom/samsung/sdraw/PointF;

    .line 85
    .line 86
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o:Z

    .line 87
    .line 88
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇00:Z

    .line 89
    .line 90
    iput-object v0, p0, Lcom/samsung/sdraw/SelectMode;->O〇8O8〇008:Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;

    .line 91
    .line 92
    const/16 v0, 0x56

    .line 93
    .line 94
    iput v0, p0, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇:I

    .line 95
    .line 96
    const/16 v0, 0x7d

    .line 97
    .line 98
    iput v0, p0, Lcom/samsung/sdraw/SelectMode;->〇oOO8O8:I

    .line 99
    .line 100
    const/16 v0, 0x51

    .line 101
    .line 102
    iput v0, p0, Lcom/samsung/sdraw/SelectMode;->〇0000OOO:I

    .line 103
    .line 104
    const/16 v0, 0x5c

    .line 105
    .line 106
    iput v0, p0, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇:I

    .line 107
    .line 108
    const/16 v3, 0x44

    .line 109
    .line 110
    iput v3, p0, Lcom/samsung/sdraw/SelectMode;->OOO〇O0:I

    .line 111
    .line 112
    const/16 v3, 0x23

    .line 113
    .line 114
    iput v3, p0, Lcom/samsung/sdraw/SelectMode;->oo〇:I

    .line 115
    .line 116
    const/16 v3, 0x17

    .line 117
    .line 118
    iput v3, p0, Lcom/samsung/sdraw/SelectMode;->O8〇o:I

    .line 119
    .line 120
    mul-int/lit8 v0, v0, 0x2

    .line 121
    .line 122
    iput v0, p0, Lcom/samsung/sdraw/SelectMode;->〇00〇8:I

    .line 123
    .line 124
    const/16 v0, 0x2a

    .line 125
    .line 126
    iput v0, p0, Lcom/samsung/sdraw/SelectMode;->〇o:I

    .line 127
    .line 128
    new-instance v0, Landroid/graphics/Rect;

    .line 129
    .line 130
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 131
    .line 132
    .line 133
    iput-object v0, p0, Lcom/samsung/sdraw/SelectMode;->o0ooO:Landroid/graphics/Rect;

    .line 134
    .line 135
    new-instance v0, Landroid/graphics/Rect;

    .line 136
    .line 137
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 138
    .line 139
    .line 140
    iput-object v0, p0, Lcom/samsung/sdraw/SelectMode;->o〇8:Landroid/graphics/Rect;

    .line 141
    .line 142
    new-instance v0, Landroid/graphics/Rect;

    .line 143
    .line 144
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 145
    .line 146
    .line 147
    iput-object v0, p0, Lcom/samsung/sdraw/SelectMode;->o8:Landroid/graphics/Rect;

    .line 148
    .line 149
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->Oo8Oo00oo:Z

    .line 150
    .line 151
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇〇0〇〇0:Z

    .line 152
    .line 153
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->o〇0OOo〇0:Z

    .line 154
    .line 155
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇0o:Z

    .line 156
    .line 157
    new-instance v0, Landroid/graphics/Paint;

    .line 158
    .line 159
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 160
    .line 161
    .line 162
    iput-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇080:Landroid/graphics/Paint;

    .line 163
    .line 164
    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 165
    .line 166
    .line 167
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇080:Landroid/graphics/Paint;

    .line 168
    .line 169
    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setDither(Z)V

    .line 170
    .line 171
    .line 172
    return-void
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method private O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;
    .locals 5

    .line 1
    new-instance v0, Lcom/samsung/sdraw/PointF;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/samsung/sdraw/PointF;-><init>()V

    .line 4
    .line 5
    .line 6
    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 7
    .line 8
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    iget v2, p2, Landroid/graphics/PointF;->x:F

    .line 13
    .line 14
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    if-ne v1, v2, :cond_0

    .line 19
    .line 20
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 21
    .line 22
    iput p1, v0, Landroid/graphics/PointF;->x:F

    .line 23
    .line 24
    iget p1, p3, Landroid/graphics/PointF;->y:F

    .line 25
    .line 26
    iput p1, v0, Landroid/graphics/PointF;->y:F

    .line 27
    .line 28
    return-object v0

    .line 29
    :cond_0
    iget v1, p1, Landroid/graphics/PointF;->y:F

    .line 30
    .line 31
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    iget v2, p2, Landroid/graphics/PointF;->y:F

    .line 36
    .line 37
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-ne v1, v2, :cond_1

    .line 42
    .line 43
    iget p2, p3, Landroid/graphics/PointF;->x:F

    .line 44
    .line 45
    iput p2, v0, Landroid/graphics/PointF;->x:F

    .line 46
    .line 47
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 48
    .line 49
    iput p1, v0, Landroid/graphics/PointF;->y:F

    .line 50
    .line 51
    return-object v0

    .line 52
    :cond_1
    iget v1, p2, Landroid/graphics/PointF;->y:F

    .line 53
    .line 54
    iget v2, p1, Landroid/graphics/PointF;->y:F

    .line 55
    .line 56
    sub-float v3, v1, v2

    .line 57
    .line 58
    iget p2, p2, Landroid/graphics/PointF;->x:F

    .line 59
    .line 60
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 61
    .line 62
    sub-float v4, p2, p1

    .line 63
    .line 64
    div-float/2addr v3, v4

    .line 65
    mul-float v2, v2, p2

    .line 66
    .line 67
    mul-float v4, p1, v1

    .line 68
    .line 69
    sub-float/2addr v2, v4

    .line 70
    sub-float p1, p2, p1

    .line 71
    .line 72
    div-float/2addr v2, p1

    .line 73
    const/high16 p1, -0x40800000    # -1.0f

    .line 74
    .line 75
    div-float/2addr p1, v3

    .line 76
    mul-float p2, p2, v3

    .line 77
    .line 78
    sub-float/2addr p2, v1

    .line 79
    iget v1, p3, Landroid/graphics/PointF;->x:F

    .line 80
    .line 81
    mul-float v1, v1, p1

    .line 82
    .line 83
    sub-float/2addr p2, v1

    .line 84
    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 85
    .line 86
    add-float/2addr p2, p3

    .line 87
    sub-float p1, v3, p1

    .line 88
    .line 89
    div-float/2addr p2, p1

    .line 90
    mul-float v3, v3, p2

    .line 91
    .line 92
    add-float/2addr v3, v2

    .line 93
    iput p2, v0, Landroid/graphics/PointF;->x:F

    .line 94
    .line 95
    iput v3, v0, Landroid/graphics/PointF;->y:F

    .line 96
    .line 97
    return-object v0
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private O8〇o(Lcom/samsung/sdraw/AbstractModeContext;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/samsung/sdraw/ag;

    .line 2
    .line 3
    iget-object v1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇8o8o〇:Landroid/view/View;

    .line 4
    .line 5
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object p1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇8o8o〇:Landroid/view/View;

    .line 10
    .line 11
    check-cast p1, Lcom/samsung/sdraw/CanvasView;

    .line 12
    .line 13
    iget-object p1, p1, Lcom/samsung/sdraw/CanvasView;->oOo〇08〇:Ljava/lang/String;

    .line 14
    .line 15
    invoke-direct {v0, v1, p1}, Lcom/samsung/sdraw/ag;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇08O8o〇0:Landroid/graphics/Bitmap;

    .line 19
    .line 20
    if-nez p1, :cond_0

    .line 21
    .line 22
    const-string p1, "/ploating_popup_icon_delete_n.png"

    .line 23
    .line 24
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇08O8o〇0:Landroid/graphics/Bitmap;

    .line 29
    .line 30
    :cond_0
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->oO:Landroid/graphics/Bitmap;

    .line 31
    .line 32
    if-nez p1, :cond_1

    .line 33
    .line 34
    const-string p1, "/ploating_popup_icon_r1_n.png"

    .line 35
    .line 36
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->oO:Landroid/graphics/Bitmap;

    .line 41
    .line 42
    :cond_1
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇8:Landroid/graphics/Bitmap;

    .line 43
    .line 44
    if-nez p1, :cond_2

    .line 45
    .line 46
    const-string p1, "/ploating_popup_icon_r2_n.png"

    .line 47
    .line 48
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇8:Landroid/graphics/Bitmap;

    .line 53
    .line 54
    :cond_2
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O08000:Landroid/graphics/Bitmap;

    .line 55
    .line 56
    if-nez p1, :cond_3

    .line 57
    .line 58
    const-string p1, "/ploating_popup_icon_delete_d.png"

    .line 59
    .line 60
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O08000:Landroid/graphics/Bitmap;

    .line 65
    .line 66
    :cond_3
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇8〇0〇o〇O:Landroid/graphics/Bitmap;

    .line 67
    .line 68
    if-nez p1, :cond_4

    .line 69
    .line 70
    const-string p1, "/ploating_popup_icon_r1_d.png"

    .line 71
    .line 72
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇8〇0〇o〇O:Landroid/graphics/Bitmap;

    .line 77
    .line 78
    :cond_4
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O〇O〇oO:Landroid/graphics/Bitmap;

    .line 79
    .line 80
    if-nez p1, :cond_5

    .line 81
    .line 82
    const-string p1, "/ploating_popup_icon_r2_d.png"

    .line 83
    .line 84
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O〇O〇oO:Landroid/graphics/Bitmap;

    .line 89
    .line 90
    :cond_5
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇O:Landroid/graphics/Bitmap;

    .line 91
    .line 92
    if-nez p1, :cond_6

    .line 93
    .line 94
    const-string p1, "/ploating_popup_center_n.png"

    .line 95
    .line 96
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇O:Landroid/graphics/Bitmap;

    .line 101
    .line 102
    :cond_6
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o8oO〇:Landroid/graphics/Bitmap;

    .line 103
    .line 104
    if-nez p1, :cond_7

    .line 105
    .line 106
    const-string p1, "/ploating_popup_left_n.png"

    .line 107
    .line 108
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o8oO〇:Landroid/graphics/Bitmap;

    .line 113
    .line 114
    :cond_7
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇8oOO88:Landroid/graphics/Bitmap;

    .line 115
    .line 116
    if-nez p1, :cond_8

    .line 117
    .line 118
    const-string p1, "/ploating_popup_right_n.png"

    .line 119
    .line 120
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 121
    .line 122
    .line 123
    move-result-object p1

    .line 124
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇8oOO88:Landroid/graphics/Bitmap;

    .line 125
    .line 126
    :cond_8
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇80:Landroid/graphics/Bitmap;

    .line 127
    .line 128
    if-nez p1, :cond_9

    .line 129
    .line 130
    const-string p1, "/ploating_popup_center_p.png"

    .line 131
    .line 132
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 133
    .line 134
    .line 135
    move-result-object p1

    .line 136
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇80:Landroid/graphics/Bitmap;

    .line 137
    .line 138
    :cond_9
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->oO00OOO:Landroid/graphics/Bitmap;

    .line 139
    .line 140
    if-nez p1, :cond_a

    .line 141
    .line 142
    const-string p1, "/ploating_popup_left_p.png"

    .line 143
    .line 144
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 145
    .line 146
    .line 147
    move-result-object p1

    .line 148
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->oO00OOO:Landroid/graphics/Bitmap;

    .line 149
    .line 150
    :cond_a
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O000:Landroid/graphics/Bitmap;

    .line 151
    .line 152
    if-nez p1, :cond_b

    .line 153
    .line 154
    const-string p1, "/ploating_popup_right_p.png"

    .line 155
    .line 156
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 157
    .line 158
    .line 159
    move-result-object p1

    .line 160
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O000:Landroid/graphics/Bitmap;

    .line 161
    .line 162
    :cond_b
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->Ooo:Landroid/graphics/Bitmap;

    .line 163
    .line 164
    if-nez p1, :cond_c

    .line 165
    .line 166
    const-string p1, "/ploating_popup_picker_down.png"

    .line 167
    .line 168
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 169
    .line 170
    .line 171
    move-result-object p1

    .line 172
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->Ooo:Landroid/graphics/Bitmap;

    .line 173
    .line 174
    :cond_c
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇O〇80o08O:Landroid/graphics/Bitmap;

    .line 175
    .line 176
    if-nez p1, :cond_d

    .line 177
    .line 178
    const-string p1, "/ploating_popup_picker_up.png"

    .line 179
    .line 180
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 181
    .line 182
    .line 183
    move-result-object p1

    .line 184
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇O〇80o08O:Landroid/graphics/Bitmap;

    .line 185
    .line 186
    :cond_d
    return-void
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method private OO0o〇〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/Canvas;FFFLcom/samsung/sdraw/PointF;)V
    .locals 7

    .line 1
    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    invoke-virtual {p2}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    .line 5
    .line 6
    .line 7
    move-result-object p6

    .line 8
    const/16 v0, 0x9

    .line 9
    .line 10
    new-array v0, v0, [F

    .line 11
    .line 12
    invoke-virtual {p6, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 13
    .line 14
    .line 15
    const/4 p6, 0x0

    .line 16
    aget v0, v0, p6

    .line 17
    .line 18
    const/high16 v1, 0x3f800000    # 1.0f

    .line 19
    .line 20
    div-float/2addr v1, v0

    .line 21
    invoke-virtual {p2, v1, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 22
    .line 23
    .line 24
    mul-float p3, p3, v0

    .line 25
    .line 26
    mul-float p4, p4, v0

    .line 27
    .line 28
    mul-float p5, p5, v0

    .line 29
    .line 30
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇oOO8O8:I

    .line 31
    .line 32
    int-to-float v2, v1

    .line 33
    sub-float/2addr p3, v2

    .line 34
    iget v2, p0, Lcom/samsung/sdraw/SelectMode;->〇0000OOO:I

    .line 35
    .line 36
    int-to-float v3, v2

    .line 37
    add-float/2addr p3, v3

    .line 38
    iget v3, p0, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇:I

    .line 39
    .line 40
    int-to-float v3, v3

    .line 41
    cmpg-float v3, p3, v3

    .line 42
    .line 43
    if-gez v3, :cond_0

    .line 44
    .line 45
    int-to-float p3, v1

    .line 46
    const/high16 v1, 0x40000000    # 2.0f

    .line 47
    .line 48
    div-float/2addr p3, v1

    .line 49
    add-float/2addr p5, p3

    .line 50
    int-to-float p3, v2

    .line 51
    add-float/2addr p3, p5

    .line 52
    const/4 p5, 0x0

    .line 53
    goto :goto_0

    .line 54
    :cond_0
    const/4 p5, 0x1

    .line 55
    :goto_0
    iget-object v1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇8o8o〇:Landroid/view/View;

    .line 56
    .line 57
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    float-to-int p4, p4

    .line 62
    iget v2, p0, Lcom/samsung/sdraw/SelectMode;->〇00〇8:I

    .line 63
    .line 64
    div-int/lit8 v3, v2, 0x2

    .line 65
    .line 66
    sub-int/2addr p4, v3

    .line 67
    div-int/lit8 v3, v2, 0x4

    .line 68
    .line 69
    sub-int/2addr p4, v3

    .line 70
    add-int v3, p4, v2

    .line 71
    .line 72
    div-int/lit8 v4, v2, 0x2

    .line 73
    .line 74
    add-int/2addr v3, v4

    .line 75
    if-gez p4, :cond_1

    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_1
    int-to-float p6, v3

    .line 79
    int-to-float v1, v1

    .line 80
    mul-float v1, v1, v0

    .line 81
    .line 82
    cmpl-float p6, p6, v1

    .line 83
    .line 84
    if-lez p6, :cond_2

    .line 85
    .line 86
    float-to-int p4, v1

    .line 87
    sub-int/2addr p4, v2

    .line 88
    div-int/lit8 v2, v2, 0x2

    .line 89
    .line 90
    sub-int p6, p4, v2

    .line 91
    .line 92
    goto :goto_1

    .line 93
    :cond_2
    move p6, p4

    .line 94
    :goto_1
    invoke-direct {p0, p1}, Lcom/samsung/sdraw/SelectMode;->O8〇o(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 95
    .line 96
    .line 97
    iget p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇:I

    .line 98
    .line 99
    add-int p4, p6, p1

    .line 100
    .line 101
    float-to-int p3, p3

    .line 102
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇:I

    .line 103
    .line 104
    sub-int/2addr p3, v1

    .line 105
    div-int/lit8 v1, p1, 0x2

    .line 106
    .line 107
    add-int/2addr v1, p4

    .line 108
    iget v2, p0, Lcom/samsung/sdraw/SelectMode;->〇o:I

    .line 109
    .line 110
    div-int/lit8 v3, v2, 0x2

    .line 111
    .line 112
    sub-int/2addr v1, v3

    .line 113
    iget v3, p0, Lcom/samsung/sdraw/SelectMode;->OOO〇O0:I

    .line 114
    .line 115
    div-int/lit8 v4, v3, 0x2

    .line 116
    .line 117
    add-int/2addr v4, p3

    .line 118
    div-int/lit8 v2, v2, 0x2

    .line 119
    .line 120
    sub-int/2addr v4, v2

    .line 121
    div-int/lit8 v2, p1, 0x2

    .line 122
    .line 123
    add-int/2addr v2, p4

    .line 124
    iget v5, p0, Lcom/samsung/sdraw/SelectMode;->oo〇:I

    .line 125
    .line 126
    div-int/lit8 v5, v5, 0x2

    .line 127
    .line 128
    sub-int/2addr v2, v5

    .line 129
    iget-object v5, p0, Lcom/samsung/sdraw/SelectMode;->o0ooO:Landroid/graphics/Rect;

    .line 130
    .line 131
    add-int/2addr p1, p6

    .line 132
    add-int/2addr v3, p3

    .line 133
    invoke-virtual {v5, p6, p3, p1, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 134
    .line 135
    .line 136
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇8:Landroid/graphics/Rect;

    .line 137
    .line 138
    iget v3, p0, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇:I

    .line 139
    .line 140
    add-int/2addr v3, p4

    .line 141
    iget v5, p0, Lcom/samsung/sdraw/SelectMode;->OOO〇O0:I

    .line 142
    .line 143
    add-int/2addr v5, p3

    .line 144
    invoke-virtual {p1, p4, p3, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 145
    .line 146
    .line 147
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o8:Landroid/graphics/Rect;

    .line 148
    .line 149
    iget v3, p0, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇:I

    .line 150
    .line 151
    add-int v5, p4, v3

    .line 152
    .line 153
    mul-int/lit8 v3, v3, 0x2

    .line 154
    .line 155
    add-int/2addr v3, p4

    .line 156
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->OOO〇O0:I

    .line 157
    .line 158
    add-int/2addr v6, p3

    .line 159
    invoke-virtual {p1, v5, p3, v3, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 160
    .line 161
    .line 162
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o0ooO:Landroid/graphics/Rect;

    .line 163
    .line 164
    invoke-direct {p0, p1, v0}, Lcom/samsung/sdraw/SelectMode;->〇〇888(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    .line 165
    .line 166
    .line 167
    move-result-object p1

    .line 168
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o0ooO:Landroid/graphics/Rect;

    .line 169
    .line 170
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇8:Landroid/graphics/Rect;

    .line 171
    .line 172
    invoke-direct {p0, p1, v0}, Lcom/samsung/sdraw/SelectMode;->〇〇888(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    .line 173
    .line 174
    .line 175
    move-result-object p1

    .line 176
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇8:Landroid/graphics/Rect;

    .line 177
    .line 178
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o8:Landroid/graphics/Rect;

    .line 179
    .line 180
    invoke-direct {p0, p1, v0}, Lcom/samsung/sdraw/SelectMode;->〇〇888(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    .line 181
    .line 182
    .line 183
    move-result-object p1

    .line 184
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o8:Landroid/graphics/Rect;

    .line 185
    .line 186
    iget-boolean p1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇〇0〇〇0:Z

    .line 187
    .line 188
    const/4 v0, 0x0

    .line 189
    if-eqz p1, :cond_3

    .line 190
    .line 191
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->oO00OOO:Landroid/graphics/Bitmap;

    .line 192
    .line 193
    int-to-float p6, p6

    .line 194
    int-to-float v3, p3

    .line 195
    invoke-virtual {p2, p1, p6, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 196
    .line 197
    .line 198
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O08000:Landroid/graphics/Bitmap;

    .line 199
    .line 200
    iget p6, p0, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇:I

    .line 201
    .line 202
    sub-int p6, v1, p6

    .line 203
    .line 204
    int-to-float p6, p6

    .line 205
    int-to-float v3, v4

    .line 206
    invoke-virtual {p2, p1, p6, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 207
    .line 208
    .line 209
    goto :goto_2

    .line 210
    :cond_3
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o8oO〇:Landroid/graphics/Bitmap;

    .line 211
    .line 212
    int-to-float p6, p6

    .line 213
    int-to-float v3, p3

    .line 214
    invoke-virtual {p2, p1, p6, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 215
    .line 216
    .line 217
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇08O8o〇0:Landroid/graphics/Bitmap;

    .line 218
    .line 219
    iget p6, p0, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇:I

    .line 220
    .line 221
    sub-int p6, v1, p6

    .line 222
    .line 223
    int-to-float p6, p6

    .line 224
    int-to-float v3, v4

    .line 225
    invoke-virtual {p2, p1, p6, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 226
    .line 227
    .line 228
    :goto_2
    iget-boolean p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇0OOo〇0:Z

    .line 229
    .line 230
    if-eqz p1, :cond_4

    .line 231
    .line 232
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇80:Landroid/graphics/Bitmap;

    .line 233
    .line 234
    int-to-float p6, p4

    .line 235
    int-to-float v3, p3

    .line 236
    invoke-virtual {p2, p1, p6, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 237
    .line 238
    .line 239
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇8〇0〇o〇O:Landroid/graphics/Bitmap;

    .line 240
    .line 241
    int-to-float p6, v1

    .line 242
    int-to-float v3, v4

    .line 243
    invoke-virtual {p2, p1, p6, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 244
    .line 245
    .line 246
    goto :goto_3

    .line 247
    :cond_4
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇O:Landroid/graphics/Bitmap;

    .line 248
    .line 249
    int-to-float p6, p4

    .line 250
    int-to-float v3, p3

    .line 251
    invoke-virtual {p2, p1, p6, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 252
    .line 253
    .line 254
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->oO:Landroid/graphics/Bitmap;

    .line 255
    .line 256
    int-to-float p6, v1

    .line 257
    int-to-float v3, v4

    .line 258
    invoke-virtual {p2, p1, p6, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 259
    .line 260
    .line 261
    :goto_3
    iget-boolean p1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇0o:Z

    .line 262
    .line 263
    if-eqz p1, :cond_5

    .line 264
    .line 265
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O000:Landroid/graphics/Bitmap;

    .line 266
    .line 267
    iget p6, p0, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇:I

    .line 268
    .line 269
    add-int/2addr p6, p4

    .line 270
    int-to-float p6, p6

    .line 271
    int-to-float v3, p3

    .line 272
    invoke-virtual {p2, p1, p6, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 273
    .line 274
    .line 275
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O〇O〇oO:Landroid/graphics/Bitmap;

    .line 276
    .line 277
    iget p6, p0, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇:I

    .line 278
    .line 279
    add-int/2addr v1, p6

    .line 280
    int-to-float p6, v1

    .line 281
    int-to-float v1, v4

    .line 282
    invoke-virtual {p2, p1, p6, v1, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 283
    .line 284
    .line 285
    goto :goto_4

    .line 286
    :cond_5
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇8oOO88:Landroid/graphics/Bitmap;

    .line 287
    .line 288
    iget p6, p0, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇:I

    .line 289
    .line 290
    add-int/2addr p6, p4

    .line 291
    int-to-float p6, p6

    .line 292
    int-to-float v3, p3

    .line 293
    invoke-virtual {p2, p1, p6, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 294
    .line 295
    .line 296
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇8:Landroid/graphics/Bitmap;

    .line 297
    .line 298
    iget p6, p0, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇:I

    .line 299
    .line 300
    add-int/2addr v1, p6

    .line 301
    int-to-float p6, v1

    .line 302
    int-to-float v1, v4

    .line 303
    invoke-virtual {p2, p1, p6, v1, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 304
    .line 305
    .line 306
    :goto_4
    if-eqz p5, :cond_6

    .line 307
    .line 308
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->Ooo:Landroid/graphics/Bitmap;

    .line 309
    .line 310
    int-to-float p5, v2

    .line 311
    iget p6, p0, Lcom/samsung/sdraw/SelectMode;->OOO〇O0:I

    .line 312
    .line 313
    add-int/2addr p6, p3

    .line 314
    int-to-float p6, p6

    .line 315
    invoke-virtual {p2, p1, p5, p6, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 316
    .line 317
    .line 318
    goto :goto_5

    .line 319
    :cond_6
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇O〇80o08O:Landroid/graphics/Bitmap;

    .line 320
    .line 321
    int-to-float p5, v2

    .line 322
    iget p6, p0, Lcom/samsung/sdraw/SelectMode;->O8〇o:I

    .line 323
    .line 324
    sub-int p6, p3, p6

    .line 325
    .line 326
    int-to-float p6, p6

    .line 327
    invoke-virtual {p2, p1, p5, p6, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 328
    .line 329
    .line 330
    :goto_5
    new-instance p1, Landroid/graphics/Paint;

    .line 331
    .line 332
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 333
    .line 334
    .line 335
    const/high16 p5, -0x1000000

    .line 336
    .line 337
    invoke-virtual {p1, p5}, Landroid/graphics/Paint;->setColor(I)V

    .line 338
    .line 339
    .line 340
    int-to-float v4, p4

    .line 341
    int-to-float p5, p3

    .line 342
    iget p6, p0, Lcom/samsung/sdraw/SelectMode;->OOO〇O0:I

    .line 343
    .line 344
    add-int/2addr p6, p3

    .line 345
    int-to-float v5, p6

    .line 346
    move-object v1, p2

    .line 347
    move v2, v4

    .line 348
    move v3, p5

    .line 349
    move-object v6, p1

    .line 350
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 351
    .line 352
    .line 353
    iget p6, p0, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇:I

    .line 354
    .line 355
    add-int v0, p4, p6

    .line 356
    .line 357
    int-to-float v2, v0

    .line 358
    add-int/2addr p4, p6

    .line 359
    int-to-float v4, p4

    .line 360
    iget p4, p0, Lcom/samsung/sdraw/SelectMode;->OOO〇O0:I

    .line 361
    .line 362
    add-int/2addr p3, p4

    .line 363
    int-to-float v5, p3

    .line 364
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 365
    .line 366
    .line 367
    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    .line 368
    .line 369
    .line 370
    return-void
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
.end method

.method private OO0o〇〇〇〇0(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;
    .locals 3

    .line 1
    new-instance v0, Lcom/samsung/sdraw/PointF;

    .line 2
    .line 3
    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 4
    .line 5
    iget v2, p2, Landroid/graphics/PointF;->x:F

    .line 6
    .line 7
    add-float/2addr v1, v2

    .line 8
    const/high16 v2, 0x40000000    # 2.0f

    .line 9
    .line 10
    div-float/2addr v1, v2

    .line 11
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 12
    .line 13
    iget p2, p2, Landroid/graphics/PointF;->y:F

    .line 14
    .line 15
    add-float/2addr p1, p2

    .line 16
    div-float/2addr p1, v2

    .line 17
    invoke-direct {v0, v1, p1}, Lcom/samsung/sdraw/PointF;-><init>(FF)V

    .line 18
    .line 19
    .line 20
    return-object v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private OOO〇O0(Lcom/samsung/sdraw/AbstractModeContext;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v1, p0, Lcom/samsung/sdraw/SelectMode;->Oo08:Lcom/samsung/sdraw/PointF;

    .line 10
    .line 11
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 12
    .line 13
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 14
    .line 15
    invoke-virtual {v0, v2, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 16
    .line 17
    .line 18
    iget v1, v0, Landroid/graphics/RectF;->right:F

    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    const/high16 v3, 0x42c80000    # 100.0f

    .line 22
    .line 23
    cmpg-float v4, v1, v3

    .line 24
    .line 25
    if-gez v4, :cond_0

    .line 26
    .line 27
    iget-object v4, p0, Lcom/samsung/sdraw/SelectMode;->Oo08:Lcom/samsung/sdraw/PointF;

    .line 28
    .line 29
    sub-float v1, v3, v1

    .line 30
    .line 31
    invoke-virtual {v4, v1, v2}, Landroid/graphics/PointF;->offset(FF)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 36
    .line 37
    iget-object v4, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇〇888:Lcom/samsung/sdraw/Setting;

    .line 38
    .line 39
    invoke-virtual {v4}, Lcom/samsung/sdraw/Setting;->Oooo8o0〇()Landroid/graphics/Rect;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    iget v4, v4, Landroid/graphics/Rect;->right:I

    .line 44
    .line 45
    add-int/lit8 v4, v4, -0x64

    .line 46
    .line 47
    int-to-float v4, v4

    .line 48
    cmpl-float v1, v1, v4

    .line 49
    .line 50
    if-lez v1, :cond_1

    .line 51
    .line 52
    iget-object v1, p0, Lcom/samsung/sdraw/SelectMode;->Oo08:Lcom/samsung/sdraw/PointF;

    .line 53
    .line 54
    iget-object v4, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇〇888:Lcom/samsung/sdraw/Setting;

    .line 55
    .line 56
    invoke-virtual {v4}, Lcom/samsung/sdraw/Setting;->Oooo8o0〇()Landroid/graphics/Rect;

    .line 57
    .line 58
    .line 59
    move-result-object v4

    .line 60
    iget v4, v4, Landroid/graphics/Rect;->right:I

    .line 61
    .line 62
    add-int/lit8 v4, v4, -0x64

    .line 63
    .line 64
    int-to-float v4, v4

    .line 65
    iget v5, v0, Landroid/graphics/RectF;->left:F

    .line 66
    .line 67
    sub-float/2addr v4, v5

    .line 68
    invoke-virtual {v1, v4, v2}, Landroid/graphics/PointF;->offset(FF)V

    .line 69
    .line 70
    .line 71
    :cond_1
    :goto_0
    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 72
    .line 73
    cmpg-float v4, v1, v3

    .line 74
    .line 75
    if-gez v4, :cond_2

    .line 76
    .line 77
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->Oo08:Lcom/samsung/sdraw/PointF;

    .line 78
    .line 79
    sub-float/2addr v3, v1

    .line 80
    invoke-virtual {p1, v2, v3}, Landroid/graphics/PointF;->offset(FF)V

    .line 81
    .line 82
    .line 83
    goto :goto_1

    .line 84
    :cond_2
    iget v1, v0, Landroid/graphics/RectF;->top:F

    .line 85
    .line 86
    iget-object v3, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇〇888:Lcom/samsung/sdraw/Setting;

    .line 87
    .line 88
    invoke-virtual {v3}, Lcom/samsung/sdraw/Setting;->Oooo8o0〇()Landroid/graphics/Rect;

    .line 89
    .line 90
    .line 91
    move-result-object v3

    .line 92
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    .line 93
    .line 94
    add-int/lit8 v3, v3, -0x64

    .line 95
    .line 96
    int-to-float v3, v3

    .line 97
    cmpl-float v1, v1, v3

    .line 98
    .line 99
    if-lez v1, :cond_3

    .line 100
    .line 101
    iget-object v1, p0, Lcom/samsung/sdraw/SelectMode;->Oo08:Lcom/samsung/sdraw/PointF;

    .line 102
    .line 103
    iget-object p1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇〇888:Lcom/samsung/sdraw/Setting;

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/samsung/sdraw/Setting;->Oooo8o0〇()Landroid/graphics/Rect;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 110
    .line 111
    add-int/lit8 p1, p1, -0x64

    .line 112
    .line 113
    int-to-float p1, p1

    .line 114
    iget v0, v0, Landroid/graphics/RectF;->top:F

    .line 115
    .line 116
    sub-float/2addr p1, v0

    .line 117
    invoke-virtual {v1, v2, p1}, Landroid/graphics/PointF;->offset(FF)V

    .line 118
    .line 119
    .line 120
    :cond_3
    :goto_1
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method private OoO8(Lcom/samsung/sdraw/AbstractModeContext;Lcom/samsung/sdraw/SelectMode$HitDirection;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 2
    .line 3
    move-object v1, v0

    .line 4
    check-cast v1, Lcom/samsung/sdraw/z;

    .line 5
    .line 6
    sget-object v2, Lcom/samsung/sdraw/SelectMode$HitDirection;->DELETE:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 7
    .line 8
    const/4 v3, 0x1

    .line 9
    if-ne p2, v2, :cond_0

    .line 10
    .line 11
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 12
    .line 13
    invoke-virtual {p2, v0, v3}, Lcom/samsung/sdraw/AbstractStage;->O8ooOoo〇(Lcom/samsung/sdraw/AbstractSprite;Z)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇00〇8(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    sget-object v0, Lcom/samsung/sdraw/SelectMode$HitDirection;->ROTATE_LEFT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 24
    .line 25
    if-ne p2, v0, :cond_1

    .line 26
    .line 27
    iget p2, v1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 28
    .line 29
    const/16 v0, -0x5a

    .line 30
    .line 31
    invoke-virtual {v1, v0}, Lcom/samsung/sdraw/z;->〇00(I)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 35
    .line 36
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 37
    .line 38
    iget v1, v1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 39
    .line 40
    int-to-float v1, v1

    .line 41
    int-to-float p2, p2

    .line 42
    invoke-virtual {v0, v2, v1, p2, v3}, Lcom/samsung/sdraw/AbstractStage;->O8O〇(Lcom/samsung/sdraw/AbstractSprite;FFZ)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    iget p2, v1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 50
    .line 51
    const/16 v0, 0x5a

    .line 52
    .line 53
    invoke-virtual {v1, v0}, Lcom/samsung/sdraw/z;->〇00(I)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 57
    .line 58
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 59
    .line 60
    iget v1, v1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 61
    .line 62
    int-to-float v1, v1

    .line 63
    int-to-float p2, p2

    .line 64
    invoke-virtual {v0, v2, v1, p2, v3}, Lcom/samsung/sdraw/AbstractStage;->O8O〇(Lcom/samsung/sdraw/AbstractSprite;FFZ)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 68
    .line 69
    .line 70
    :goto_0
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method private O〇8O8〇008(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)D
    .locals 2

    .line 1
    iget v0, p2, Landroid/graphics/PointF;->y:F

    .line 2
    .line 3
    iget v1, p1, Landroid/graphics/PointF;->y:F

    .line 4
    .line 5
    sub-float/2addr v0, v1

    .line 6
    float-to-double v0, v0

    .line 7
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 8
    .line 9
    iget p2, p2, Landroid/graphics/PointF;->x:F

    .line 10
    .line 11
    sub-float/2addr p1, p2

    .line 12
    float-to-double p1, p1

    .line 13
    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->atan2(DD)D

    .line 14
    .line 15
    .line 16
    move-result-wide p1

    .line 17
    const-wide v0, 0x4066800000000000L    # 180.0

    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    mul-double p1, p1, v0

    .line 23
    .line 24
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    div-double/2addr p1, v0

    .line 30
    return-wide p1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private o800o8O(Lcom/samsung/sdraw/SelectMode$HitDirection;)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇〇〇0:Z

    .line 3
    .line 4
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇8o8o〇:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private oo88o8O([Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)V
    .locals 12

    .line 1
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 2
    .line 3
    check-cast p1, Lcom/samsung/sdraw/z;

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/samsung/sdraw/z;->oo〇()F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Lcom/samsung/sdraw/z;->OOO〇O0()F

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    div-float/2addr v0, v1

    .line 14
    invoke-static {}, Lcom/samsung/sdraw/SelectMode;->〇00()[I

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->〇8o8o〇:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 19
    .line 20
    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    aget v1, v1, v2

    .line 25
    .line 26
    const/4 v2, 0x3

    .line 27
    const/4 v3, 0x2

    .line 28
    const/4 v4, 0x1

    .line 29
    const/4 v5, 0x0

    .line 30
    packed-switch v1, :pswitch_data_0

    .line 31
    .line 32
    .line 33
    goto/16 :goto_1

    .line 34
    .line 35
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 40
    .line 41
    .line 42
    move-result-object v6

    .line 43
    invoke-direct {p0, p2, v1, v6}, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Z

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    if-nez v1, :cond_0

    .line 48
    .line 49
    return-void

    .line 50
    :cond_0
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 55
    .line 56
    .line 57
    move-result-object v6

    .line 58
    invoke-direct {p0, v1, v6, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 59
    .line 60
    .line 61
    move-result-object p2

    .line 62
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    invoke-static {v1, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 71
    .line 72
    int-to-float v6, v6

    .line 73
    cmpg-float v1, v1, v6

    .line 74
    .line 75
    if-gez v1, :cond_1

    .line 76
    .line 77
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    iget v1, v1, Landroid/graphics/PointF;->x:F

    .line 82
    .line 83
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 84
    .line 85
    int-to-double v6, v6

    .line 86
    iget v8, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 87
    .line 88
    int-to-double v8, v8

    .line 89
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 90
    .line 91
    .line 92
    move-result-wide v8

    .line 93
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    .line 94
    .line 95
    .line 96
    move-result-wide v8

    .line 97
    mul-double v6, v6, v8

    .line 98
    .line 99
    double-to-float v6, v6

    .line 100
    add-float/2addr v1, v6

    .line 101
    iput v1, p2, Landroid/graphics/PointF;->x:F

    .line 102
    .line 103
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 104
    .line 105
    .line 106
    move-result-object v1

    .line 107
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 108
    .line 109
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 110
    .line 111
    int-to-double v6, v6

    .line 112
    iget v8, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 113
    .line 114
    int-to-double v8, v8

    .line 115
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 116
    .line 117
    .line 118
    move-result-wide v8

    .line 119
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    .line 120
    .line 121
    .line 122
    move-result-wide v8

    .line 123
    mul-double v6, v6, v8

    .line 124
    .line 125
    double-to-float v6, v6

    .line 126
    add-float/2addr v1, v6

    .line 127
    iput v1, p2, Landroid/graphics/PointF;->y:F

    .line 128
    .line 129
    :cond_1
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 130
    .line 131
    .line 132
    move-result-object v1

    .line 133
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 134
    .line 135
    .line 136
    move-result-object v6

    .line 137
    invoke-static {v1, v6}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 138
    .line 139
    .line 140
    move-result v1

    .line 141
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 142
    .line 143
    .line 144
    move-result-object v6

    .line 145
    invoke-static {v6, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 146
    .line 147
    .line 148
    move-result v6

    .line 149
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 150
    .line 151
    .line 152
    move-result-object v7

    .line 153
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 154
    .line 155
    .line 156
    move-result-object v8

    .line 157
    invoke-direct {p0, v1, v6, v7, v8}, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O(FFLcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 158
    .line 159
    .line 160
    move-result-object v7

    .line 161
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 162
    .line 163
    .line 164
    move-result-object v8

    .line 165
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 166
    .line 167
    .line 168
    move-result-object v9

    .line 169
    invoke-direct {p0, v1, v6, v8, v9}, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O(FFLcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 170
    .line 171
    .line 172
    move-result-object v1

    .line 173
    invoke-direct {p0, v7, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 174
    .line 175
    .line 176
    move-result-object v1

    .line 177
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 178
    .line 179
    .line 180
    move-result-object v6

    .line 181
    invoke-static {v6, v7}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 182
    .line 183
    .line 184
    move-result v6

    .line 185
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 186
    .line 187
    int-to-float v8, v8

    .line 188
    cmpg-float v6, v6, v8

    .line 189
    .line 190
    if-gez v6, :cond_2

    .line 191
    .line 192
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 193
    .line 194
    .line 195
    move-result-object v6

    .line 196
    iget v6, v6, Landroid/graphics/PointF;->x:F

    .line 197
    .line 198
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 199
    .line 200
    int-to-double v8, v8

    .line 201
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 202
    .line 203
    add-int/lit8 v10, v10, -0x5a

    .line 204
    .line 205
    int-to-double v10, v10

    .line 206
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 207
    .line 208
    .line 209
    move-result-wide v10

    .line 210
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    .line 211
    .line 212
    .line 213
    move-result-wide v10

    .line 214
    mul-double v8, v8, v10

    .line 215
    .line 216
    double-to-float v8, v8

    .line 217
    sub-float/2addr v6, v8

    .line 218
    iput v6, v7, Landroid/graphics/PointF;->x:F

    .line 219
    .line 220
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 221
    .line 222
    .line 223
    move-result-object v6

    .line 224
    iget v6, v6, Landroid/graphics/PointF;->y:F

    .line 225
    .line 226
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 227
    .line 228
    int-to-double v8, v8

    .line 229
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 230
    .line 231
    add-int/lit8 v10, v10, -0x5a

    .line 232
    .line 233
    int-to-double v10, v10

    .line 234
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 235
    .line 236
    .line 237
    move-result-wide v10

    .line 238
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    .line 239
    .line 240
    .line 241
    move-result-wide v10

    .line 242
    mul-double v8, v8, v10

    .line 243
    .line 244
    double-to-float v8, v8

    .line 245
    sub-float/2addr v6, v8

    .line 246
    iput v6, v7, Landroid/graphics/PointF;->y:F

    .line 247
    .line 248
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 249
    .line 250
    .line 251
    move-result-object v6

    .line 252
    iget v6, v6, Landroid/graphics/PointF;->x:F

    .line 253
    .line 254
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 255
    .line 256
    int-to-float v8, v8

    .line 257
    div-float/2addr v8, v0

    .line 258
    float-to-double v8, v8

    .line 259
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 260
    .line 261
    int-to-double v10, v10

    .line 262
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 263
    .line 264
    .line 265
    move-result-wide v10

    .line 266
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    .line 267
    .line 268
    .line 269
    move-result-wide v10

    .line 270
    mul-double v8, v8, v10

    .line 271
    .line 272
    double-to-float v8, v8

    .line 273
    add-float/2addr v6, v8

    .line 274
    iput v6, p2, Landroid/graphics/PointF;->x:F

    .line 275
    .line 276
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 277
    .line 278
    .line 279
    move-result-object v5

    .line 280
    iget v5, v5, Landroid/graphics/PointF;->y:F

    .line 281
    .line 282
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 283
    .line 284
    int-to-float v6, v6

    .line 285
    div-float/2addr v6, v0

    .line 286
    float-to-double v8, v6

    .line 287
    iget v0, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 288
    .line 289
    int-to-double v10, v0

    .line 290
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 291
    .line 292
    .line 293
    move-result-wide v10

    .line 294
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    .line 295
    .line 296
    .line 297
    move-result-wide v10

    .line 298
    mul-double v8, v8, v10

    .line 299
    .line 300
    double-to-float v0, v8

    .line 301
    add-float/2addr v5, v0

    .line 302
    iput v5, p2, Landroid/graphics/PointF;->y:F

    .line 303
    .line 304
    iget v0, p2, Landroid/graphics/PointF;->x:F

    .line 305
    .line 306
    iget v5, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 307
    .line 308
    int-to-double v5, v5

    .line 309
    iget v8, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 310
    .line 311
    add-int/lit8 v8, v8, -0x5a

    .line 312
    .line 313
    int-to-double v8, v8

    .line 314
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 315
    .line 316
    .line 317
    move-result-wide v8

    .line 318
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    .line 319
    .line 320
    .line 321
    move-result-wide v8

    .line 322
    mul-double v5, v5, v8

    .line 323
    .line 324
    double-to-float v5, v5

    .line 325
    sub-float/2addr v0, v5

    .line 326
    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 327
    .line 328
    iget v0, p2, Landroid/graphics/PointF;->y:F

    .line 329
    .line 330
    iget v5, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 331
    .line 332
    int-to-double v5, v5

    .line 333
    iget v8, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 334
    .line 335
    add-int/lit8 v8, v8, -0x5a

    .line 336
    .line 337
    int-to-double v8, v8

    .line 338
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 339
    .line 340
    .line 341
    move-result-wide v8

    .line 342
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    .line 343
    .line 344
    .line 345
    move-result-wide v8

    .line 346
    mul-double v5, v5, v8

    .line 347
    .line 348
    double-to-float v5, v5

    .line 349
    sub-float/2addr v0, v5

    .line 350
    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 351
    .line 352
    :cond_2
    invoke-virtual {p1, v4, p2}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 353
    .line 354
    .line 355
    invoke-virtual {p1, v3, v7}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 356
    .line 357
    .line 358
    invoke-virtual {p1, v2, v1}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 359
    .line 360
    .line 361
    goto/16 :goto_1

    .line 362
    .line 363
    :pswitch_1
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 364
    .line 365
    .line 366
    move-result-object v1

    .line 367
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 368
    .line 369
    .line 370
    move-result-object v6

    .line 371
    invoke-direct {p0, p2, v1, v6}, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Z

    .line 372
    .line 373
    .line 374
    move-result v1

    .line 375
    if-nez v1, :cond_3

    .line 376
    .line 377
    return-void

    .line 378
    :cond_3
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 379
    .line 380
    .line 381
    move-result-object v1

    .line 382
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 383
    .line 384
    .line 385
    move-result-object v6

    .line 386
    invoke-direct {p0, v1, v6, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 387
    .line 388
    .line 389
    move-result-object p2

    .line 390
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 391
    .line 392
    .line 393
    move-result-object v1

    .line 394
    invoke-static {v1, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 395
    .line 396
    .line 397
    move-result v1

    .line 398
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 399
    .line 400
    int-to-float v6, v6

    .line 401
    cmpg-float v1, v1, v6

    .line 402
    .line 403
    if-gez v1, :cond_4

    .line 404
    .line 405
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 406
    .line 407
    .line 408
    move-result-object v1

    .line 409
    iget v1, v1, Landroid/graphics/PointF;->x:F

    .line 410
    .line 411
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 412
    .line 413
    int-to-double v6, v6

    .line 414
    iget v8, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 415
    .line 416
    int-to-double v8, v8

    .line 417
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 418
    .line 419
    .line 420
    move-result-wide v8

    .line 421
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    .line 422
    .line 423
    .line 424
    move-result-wide v8

    .line 425
    mul-double v6, v6, v8

    .line 426
    .line 427
    double-to-float v6, v6

    .line 428
    sub-float/2addr v1, v6

    .line 429
    iput v1, p2, Landroid/graphics/PointF;->x:F

    .line 430
    .line 431
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 432
    .line 433
    .line 434
    move-result-object v1

    .line 435
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 436
    .line 437
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 438
    .line 439
    int-to-double v6, v6

    .line 440
    iget v8, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 441
    .line 442
    int-to-double v8, v8

    .line 443
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 444
    .line 445
    .line 446
    move-result-wide v8

    .line 447
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    .line 448
    .line 449
    .line 450
    move-result-wide v8

    .line 451
    mul-double v6, v6, v8

    .line 452
    .line 453
    double-to-float v6, v6

    .line 454
    sub-float/2addr v1, v6

    .line 455
    iput v1, p2, Landroid/graphics/PointF;->y:F

    .line 456
    .line 457
    :cond_4
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 458
    .line 459
    .line 460
    move-result-object v1

    .line 461
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 462
    .line 463
    .line 464
    move-result-object v6

    .line 465
    invoke-static {v1, v6}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 466
    .line 467
    .line 468
    move-result v1

    .line 469
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 470
    .line 471
    .line 472
    move-result-object v6

    .line 473
    invoke-static {v6, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 474
    .line 475
    .line 476
    move-result v6

    .line 477
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 478
    .line 479
    .line 480
    move-result-object v7

    .line 481
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 482
    .line 483
    .line 484
    move-result-object v8

    .line 485
    invoke-direct {p0, v1, v6, v7, v8}, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O(FFLcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 486
    .line 487
    .line 488
    move-result-object v7

    .line 489
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 490
    .line 491
    .line 492
    move-result-object v8

    .line 493
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 494
    .line 495
    .line 496
    move-result-object v9

    .line 497
    invoke-direct {p0, v1, v6, v8, v9}, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O(FFLcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 498
    .line 499
    .line 500
    move-result-object v1

    .line 501
    invoke-direct {p0, v7, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 502
    .line 503
    .line 504
    move-result-object v1

    .line 505
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 506
    .line 507
    .line 508
    move-result-object v6

    .line 509
    invoke-static {v6, v7}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 510
    .line 511
    .line 512
    move-result v6

    .line 513
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 514
    .line 515
    int-to-float v8, v8

    .line 516
    cmpg-float v6, v6, v8

    .line 517
    .line 518
    if-gez v6, :cond_5

    .line 519
    .line 520
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 521
    .line 522
    .line 523
    move-result-object v6

    .line 524
    iget v6, v6, Landroid/graphics/PointF;->x:F

    .line 525
    .line 526
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 527
    .line 528
    int-to-double v8, v8

    .line 529
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 530
    .line 531
    add-int/lit8 v10, v10, -0x5a

    .line 532
    .line 533
    int-to-double v10, v10

    .line 534
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 535
    .line 536
    .line 537
    move-result-wide v10

    .line 538
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    .line 539
    .line 540
    .line 541
    move-result-wide v10

    .line 542
    mul-double v8, v8, v10

    .line 543
    .line 544
    double-to-float v8, v8

    .line 545
    sub-float/2addr v6, v8

    .line 546
    iput v6, v7, Landroid/graphics/PointF;->x:F

    .line 547
    .line 548
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 549
    .line 550
    .line 551
    move-result-object v6

    .line 552
    iget v6, v6, Landroid/graphics/PointF;->y:F

    .line 553
    .line 554
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 555
    .line 556
    int-to-double v8, v8

    .line 557
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 558
    .line 559
    add-int/lit8 v10, v10, -0x5a

    .line 560
    .line 561
    int-to-double v10, v10

    .line 562
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 563
    .line 564
    .line 565
    move-result-wide v10

    .line 566
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    .line 567
    .line 568
    .line 569
    move-result-wide v10

    .line 570
    mul-double v8, v8, v10

    .line 571
    .line 572
    double-to-float v8, v8

    .line 573
    sub-float/2addr v6, v8

    .line 574
    iput v6, v7, Landroid/graphics/PointF;->y:F

    .line 575
    .line 576
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 577
    .line 578
    .line 579
    move-result-object v6

    .line 580
    iget v6, v6, Landroid/graphics/PointF;->x:F

    .line 581
    .line 582
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 583
    .line 584
    int-to-float v8, v8

    .line 585
    div-float/2addr v8, v0

    .line 586
    float-to-double v8, v8

    .line 587
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 588
    .line 589
    int-to-double v10, v10

    .line 590
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 591
    .line 592
    .line 593
    move-result-wide v10

    .line 594
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    .line 595
    .line 596
    .line 597
    move-result-wide v10

    .line 598
    mul-double v8, v8, v10

    .line 599
    .line 600
    double-to-float v8, v8

    .line 601
    sub-float/2addr v6, v8

    .line 602
    iput v6, p2, Landroid/graphics/PointF;->x:F

    .line 603
    .line 604
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 605
    .line 606
    .line 607
    move-result-object v4

    .line 608
    iget v4, v4, Landroid/graphics/PointF;->y:F

    .line 609
    .line 610
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 611
    .line 612
    int-to-float v6, v6

    .line 613
    div-float/2addr v6, v0

    .line 614
    float-to-double v8, v6

    .line 615
    iget v0, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 616
    .line 617
    int-to-double v10, v0

    .line 618
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 619
    .line 620
    .line 621
    move-result-wide v10

    .line 622
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    .line 623
    .line 624
    .line 625
    move-result-wide v10

    .line 626
    mul-double v8, v8, v10

    .line 627
    .line 628
    double-to-float v0, v8

    .line 629
    sub-float/2addr v4, v0

    .line 630
    iput v4, p2, Landroid/graphics/PointF;->y:F

    .line 631
    .line 632
    iget v0, p2, Landroid/graphics/PointF;->x:F

    .line 633
    .line 634
    iget v4, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 635
    .line 636
    int-to-double v8, v4

    .line 637
    iget v4, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 638
    .line 639
    add-int/lit8 v4, v4, -0x5a

    .line 640
    .line 641
    int-to-double v10, v4

    .line 642
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 643
    .line 644
    .line 645
    move-result-wide v10

    .line 646
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    .line 647
    .line 648
    .line 649
    move-result-wide v10

    .line 650
    mul-double v8, v8, v10

    .line 651
    .line 652
    double-to-float v4, v8

    .line 653
    sub-float/2addr v0, v4

    .line 654
    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 655
    .line 656
    iget v0, p2, Landroid/graphics/PointF;->y:F

    .line 657
    .line 658
    iget v4, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 659
    .line 660
    int-to-double v8, v4

    .line 661
    iget v4, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 662
    .line 663
    add-int/lit8 v4, v4, -0x5a

    .line 664
    .line 665
    int-to-double v10, v4

    .line 666
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 667
    .line 668
    .line 669
    move-result-wide v10

    .line 670
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    .line 671
    .line 672
    .line 673
    move-result-wide v10

    .line 674
    mul-double v8, v8, v10

    .line 675
    .line 676
    double-to-float v4, v8

    .line 677
    sub-float/2addr v0, v4

    .line 678
    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 679
    .line 680
    :cond_5
    invoke-virtual {p1, v5, p2}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 681
    .line 682
    .line 683
    invoke-virtual {p1, v3, v1}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 684
    .line 685
    .line 686
    invoke-virtual {p1, v2, v7}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 687
    .line 688
    .line 689
    goto/16 :goto_1

    .line 690
    .line 691
    :pswitch_2
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 692
    .line 693
    .line 694
    move-result-object v1

    .line 695
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 696
    .line 697
    .line 698
    move-result-object v6

    .line 699
    invoke-direct {p0, p2, v1, v6}, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Z

    .line 700
    .line 701
    .line 702
    move-result v1

    .line 703
    if-nez v1, :cond_6

    .line 704
    .line 705
    return-void

    .line 706
    :cond_6
    iget v1, p2, Landroid/graphics/PointF;->x:F

    .line 707
    .line 708
    const/4 v6, 0x0

    .line 709
    cmpg-float v1, v1, v6

    .line 710
    .line 711
    if-lez v1, :cond_a

    .line 712
    .line 713
    iget v1, p2, Landroid/graphics/PointF;->y:F

    .line 714
    .line 715
    cmpg-float v1, v1, v6

    .line 716
    .line 717
    if-gtz v1, :cond_7

    .line 718
    .line 719
    goto/16 :goto_0

    .line 720
    .line 721
    :cond_7
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 722
    .line 723
    .line 724
    move-result-object v1

    .line 725
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 726
    .line 727
    .line 728
    move-result-object v6

    .line 729
    invoke-direct {p0, v1, v6, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 730
    .line 731
    .line 732
    move-result-object p2

    .line 733
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 734
    .line 735
    .line 736
    move-result-object v1

    .line 737
    invoke-static {v1, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 738
    .line 739
    .line 740
    move-result v1

    .line 741
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 742
    .line 743
    int-to-float v6, v6

    .line 744
    cmpg-float v1, v1, v6

    .line 745
    .line 746
    if-gez v1, :cond_8

    .line 747
    .line 748
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 749
    .line 750
    .line 751
    move-result-object v1

    .line 752
    iget v1, v1, Landroid/graphics/PointF;->x:F

    .line 753
    .line 754
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 755
    .line 756
    int-to-double v6, v6

    .line 757
    iget v8, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 758
    .line 759
    int-to-double v8, v8

    .line 760
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 761
    .line 762
    .line 763
    move-result-wide v8

    .line 764
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    .line 765
    .line 766
    .line 767
    move-result-wide v8

    .line 768
    mul-double v6, v6, v8

    .line 769
    .line 770
    double-to-float v6, v6

    .line 771
    add-float/2addr v1, v6

    .line 772
    iput v1, p2, Landroid/graphics/PointF;->x:F

    .line 773
    .line 774
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 775
    .line 776
    .line 777
    move-result-object v1

    .line 778
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 779
    .line 780
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 781
    .line 782
    int-to-double v6, v6

    .line 783
    iget v8, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 784
    .line 785
    int-to-double v8, v8

    .line 786
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 787
    .line 788
    .line 789
    move-result-wide v8

    .line 790
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    .line 791
    .line 792
    .line 793
    move-result-wide v8

    .line 794
    mul-double v6, v6, v8

    .line 795
    .line 796
    double-to-float v6, v6

    .line 797
    add-float/2addr v1, v6

    .line 798
    iput v1, p2, Landroid/graphics/PointF;->y:F

    .line 799
    .line 800
    :cond_8
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 801
    .line 802
    .line 803
    move-result-object v1

    .line 804
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 805
    .line 806
    .line 807
    move-result-object v6

    .line 808
    invoke-static {v1, v6}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 809
    .line 810
    .line 811
    move-result v1

    .line 812
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 813
    .line 814
    .line 815
    move-result-object v6

    .line 816
    invoke-static {v6, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 817
    .line 818
    .line 819
    move-result v6

    .line 820
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 821
    .line 822
    .line 823
    move-result-object v7

    .line 824
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 825
    .line 826
    .line 827
    move-result-object v8

    .line 828
    invoke-direct {p0, v1, v6, v7, v8}, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O(FFLcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 829
    .line 830
    .line 831
    move-result-object v7

    .line 832
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 833
    .line 834
    .line 835
    move-result-object v8

    .line 836
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 837
    .line 838
    .line 839
    move-result-object v9

    .line 840
    invoke-direct {p0, v1, v6, v8, v9}, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O(FFLcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 841
    .line 842
    .line 843
    move-result-object v1

    .line 844
    invoke-direct {p0, v7, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 845
    .line 846
    .line 847
    move-result-object v1

    .line 848
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 849
    .line 850
    .line 851
    move-result-object v6

    .line 852
    invoke-static {v6, v7}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 853
    .line 854
    .line 855
    move-result v6

    .line 856
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 857
    .line 858
    int-to-float v8, v8

    .line 859
    cmpg-float v6, v6, v8

    .line 860
    .line 861
    if-gez v6, :cond_9

    .line 862
    .line 863
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 864
    .line 865
    .line 866
    move-result-object v6

    .line 867
    iget v6, v6, Landroid/graphics/PointF;->x:F

    .line 868
    .line 869
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 870
    .line 871
    int-to-double v8, v8

    .line 872
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 873
    .line 874
    add-int/lit8 v10, v10, -0x5a

    .line 875
    .line 876
    int-to-double v10, v10

    .line 877
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 878
    .line 879
    .line 880
    move-result-wide v10

    .line 881
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    .line 882
    .line 883
    .line 884
    move-result-wide v10

    .line 885
    mul-double v8, v8, v10

    .line 886
    .line 887
    double-to-float v8, v8

    .line 888
    add-float/2addr v6, v8

    .line 889
    iput v6, v7, Landroid/graphics/PointF;->x:F

    .line 890
    .line 891
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 892
    .line 893
    .line 894
    move-result-object v6

    .line 895
    iget v6, v6, Landroid/graphics/PointF;->y:F

    .line 896
    .line 897
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 898
    .line 899
    int-to-double v8, v8

    .line 900
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 901
    .line 902
    add-int/lit8 v10, v10, -0x5a

    .line 903
    .line 904
    int-to-double v10, v10

    .line 905
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 906
    .line 907
    .line 908
    move-result-wide v10

    .line 909
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    .line 910
    .line 911
    .line 912
    move-result-wide v10

    .line 913
    mul-double v8, v8, v10

    .line 914
    .line 915
    double-to-float v8, v8

    .line 916
    add-float/2addr v6, v8

    .line 917
    iput v6, v7, Landroid/graphics/PointF;->y:F

    .line 918
    .line 919
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 920
    .line 921
    .line 922
    move-result-object v6

    .line 923
    iget v6, v6, Landroid/graphics/PointF;->x:F

    .line 924
    .line 925
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 926
    .line 927
    int-to-float v8, v8

    .line 928
    div-float/2addr v8, v0

    .line 929
    float-to-double v8, v8

    .line 930
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 931
    .line 932
    int-to-double v10, v10

    .line 933
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 934
    .line 935
    .line 936
    move-result-wide v10

    .line 937
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    .line 938
    .line 939
    .line 940
    move-result-wide v10

    .line 941
    mul-double v8, v8, v10

    .line 942
    .line 943
    double-to-float v8, v8

    .line 944
    add-float/2addr v6, v8

    .line 945
    iput v6, p2, Landroid/graphics/PointF;->x:F

    .line 946
    .line 947
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 948
    .line 949
    .line 950
    move-result-object v3

    .line 951
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 952
    .line 953
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 954
    .line 955
    int-to-float v6, v6

    .line 956
    div-float/2addr v6, v0

    .line 957
    float-to-double v8, v6

    .line 958
    iget v0, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 959
    .line 960
    int-to-double v10, v0

    .line 961
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 962
    .line 963
    .line 964
    move-result-wide v10

    .line 965
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    .line 966
    .line 967
    .line 968
    move-result-wide v10

    .line 969
    mul-double v8, v8, v10

    .line 970
    .line 971
    double-to-float v0, v8

    .line 972
    add-float/2addr v3, v0

    .line 973
    iput v3, p2, Landroid/graphics/PointF;->y:F

    .line 974
    .line 975
    iget v0, p2, Landroid/graphics/PointF;->x:F

    .line 976
    .line 977
    iget v3, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 978
    .line 979
    int-to-double v8, v3

    .line 980
    iget v3, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 981
    .line 982
    add-int/lit8 v3, v3, -0x5a

    .line 983
    .line 984
    int-to-double v10, v3

    .line 985
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 986
    .line 987
    .line 988
    move-result-wide v10

    .line 989
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    .line 990
    .line 991
    .line 992
    move-result-wide v10

    .line 993
    mul-double v8, v8, v10

    .line 994
    .line 995
    double-to-float v3, v8

    .line 996
    add-float/2addr v0, v3

    .line 997
    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 998
    .line 999
    iget v0, p2, Landroid/graphics/PointF;->y:F

    .line 1000
    .line 1001
    iget v3, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 1002
    .line 1003
    int-to-double v8, v3

    .line 1004
    iget v3, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1005
    .line 1006
    add-int/lit8 v3, v3, -0x5a

    .line 1007
    .line 1008
    int-to-double v10, v3

    .line 1009
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 1010
    .line 1011
    .line 1012
    move-result-wide v10

    .line 1013
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    .line 1014
    .line 1015
    .line 1016
    move-result-wide v10

    .line 1017
    mul-double v8, v8, v10

    .line 1018
    .line 1019
    double-to-float v3, v8

    .line 1020
    add-float/2addr v0, v3

    .line 1021
    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 1022
    .line 1023
    :cond_9
    invoke-virtual {p1, v5, v7}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1024
    .line 1025
    .line 1026
    invoke-virtual {p1, v4, v1}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1027
    .line 1028
    .line 1029
    invoke-virtual {p1, v2, p2}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1030
    .line 1031
    .line 1032
    goto/16 :goto_1

    .line 1033
    .line 1034
    :cond_a
    :goto_0
    return-void

    .line 1035
    :pswitch_3
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1036
    .line 1037
    .line 1038
    move-result-object v1

    .line 1039
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1040
    .line 1041
    .line 1042
    move-result-object v6

    .line 1043
    invoke-direct {p0, p2, v1, v6}, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Z

    .line 1044
    .line 1045
    .line 1046
    move-result v1

    .line 1047
    if-nez v1, :cond_b

    .line 1048
    .line 1049
    return-void

    .line 1050
    :cond_b
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1051
    .line 1052
    .line 1053
    move-result-object v1

    .line 1054
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1055
    .line 1056
    .line 1057
    move-result-object v6

    .line 1058
    invoke-direct {p0, v1, v6, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1059
    .line 1060
    .line 1061
    move-result-object p2

    .line 1062
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1063
    .line 1064
    .line 1065
    move-result-object v1

    .line 1066
    invoke-static {v1, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 1067
    .line 1068
    .line 1069
    move-result v1

    .line 1070
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1071
    .line 1072
    int-to-float v6, v6

    .line 1073
    cmpg-float v1, v1, v6

    .line 1074
    .line 1075
    if-gez v1, :cond_c

    .line 1076
    .line 1077
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1078
    .line 1079
    .line 1080
    move-result-object v1

    .line 1081
    iget v1, v1, Landroid/graphics/PointF;->x:F

    .line 1082
    .line 1083
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1084
    .line 1085
    int-to-double v6, v6

    .line 1086
    iget v8, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1087
    .line 1088
    int-to-double v8, v8

    .line 1089
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 1090
    .line 1091
    .line 1092
    move-result-wide v8

    .line 1093
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    .line 1094
    .line 1095
    .line 1096
    move-result-wide v8

    .line 1097
    mul-double v6, v6, v8

    .line 1098
    .line 1099
    double-to-float v6, v6

    .line 1100
    sub-float/2addr v1, v6

    .line 1101
    iput v1, p2, Landroid/graphics/PointF;->x:F

    .line 1102
    .line 1103
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1104
    .line 1105
    .line 1106
    move-result-object v1

    .line 1107
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 1108
    .line 1109
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1110
    .line 1111
    int-to-double v6, v6

    .line 1112
    iget v8, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1113
    .line 1114
    int-to-double v8, v8

    .line 1115
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 1116
    .line 1117
    .line 1118
    move-result-wide v8

    .line 1119
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    .line 1120
    .line 1121
    .line 1122
    move-result-wide v8

    .line 1123
    mul-double v6, v6, v8

    .line 1124
    .line 1125
    double-to-float v6, v6

    .line 1126
    sub-float/2addr v1, v6

    .line 1127
    iput v1, p2, Landroid/graphics/PointF;->y:F

    .line 1128
    .line 1129
    :cond_c
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1130
    .line 1131
    .line 1132
    move-result-object v1

    .line 1133
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1134
    .line 1135
    .line 1136
    move-result-object v6

    .line 1137
    invoke-static {v1, v6}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 1138
    .line 1139
    .line 1140
    move-result v1

    .line 1141
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1142
    .line 1143
    .line 1144
    move-result-object v6

    .line 1145
    invoke-static {v6, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 1146
    .line 1147
    .line 1148
    move-result v6

    .line 1149
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1150
    .line 1151
    .line 1152
    move-result-object v7

    .line 1153
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1154
    .line 1155
    .line 1156
    move-result-object v8

    .line 1157
    invoke-direct {p0, v1, v6, v7, v8}, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O(FFLcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1158
    .line 1159
    .line 1160
    move-result-object v7

    .line 1161
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1162
    .line 1163
    .line 1164
    move-result-object v8

    .line 1165
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1166
    .line 1167
    .line 1168
    move-result-object v9

    .line 1169
    invoke-direct {p0, v1, v6, v8, v9}, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O(FFLcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1170
    .line 1171
    .line 1172
    move-result-object v1

    .line 1173
    invoke-direct {p0, v7, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1174
    .line 1175
    .line 1176
    move-result-object v1

    .line 1177
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1178
    .line 1179
    .line 1180
    move-result-object v6

    .line 1181
    invoke-static {v6, v7}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 1182
    .line 1183
    .line 1184
    move-result v6

    .line 1185
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 1186
    .line 1187
    int-to-float v8, v8

    .line 1188
    cmpg-float v6, v6, v8

    .line 1189
    .line 1190
    if-gez v6, :cond_d

    .line 1191
    .line 1192
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1193
    .line 1194
    .line 1195
    move-result-object v6

    .line 1196
    iget v6, v6, Landroid/graphics/PointF;->x:F

    .line 1197
    .line 1198
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 1199
    .line 1200
    int-to-double v8, v8

    .line 1201
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1202
    .line 1203
    add-int/lit8 v10, v10, -0x5a

    .line 1204
    .line 1205
    int-to-double v10, v10

    .line 1206
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 1207
    .line 1208
    .line 1209
    move-result-wide v10

    .line 1210
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    .line 1211
    .line 1212
    .line 1213
    move-result-wide v10

    .line 1214
    mul-double v8, v8, v10

    .line 1215
    .line 1216
    double-to-float v8, v8

    .line 1217
    add-float/2addr v6, v8

    .line 1218
    iput v6, v7, Landroid/graphics/PointF;->x:F

    .line 1219
    .line 1220
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1221
    .line 1222
    .line 1223
    move-result-object v6

    .line 1224
    iget v6, v6, Landroid/graphics/PointF;->y:F

    .line 1225
    .line 1226
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 1227
    .line 1228
    int-to-double v8, v8

    .line 1229
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1230
    .line 1231
    add-int/lit8 v10, v10, -0x5a

    .line 1232
    .line 1233
    int-to-double v10, v10

    .line 1234
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 1235
    .line 1236
    .line 1237
    move-result-wide v10

    .line 1238
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    .line 1239
    .line 1240
    .line 1241
    move-result-wide v10

    .line 1242
    mul-double v8, v8, v10

    .line 1243
    .line 1244
    double-to-float v8, v8

    .line 1245
    add-float/2addr v6, v8

    .line 1246
    iput v6, v7, Landroid/graphics/PointF;->y:F

    .line 1247
    .line 1248
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1249
    .line 1250
    .line 1251
    move-result-object v6

    .line 1252
    iget v6, v6, Landroid/graphics/PointF;->x:F

    .line 1253
    .line 1254
    iget v8, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1255
    .line 1256
    int-to-float v8, v8

    .line 1257
    div-float/2addr v8, v0

    .line 1258
    float-to-double v8, v8

    .line 1259
    iget v10, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1260
    .line 1261
    int-to-double v10, v10

    .line 1262
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 1263
    .line 1264
    .line 1265
    move-result-wide v10

    .line 1266
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    .line 1267
    .line 1268
    .line 1269
    move-result-wide v10

    .line 1270
    mul-double v8, v8, v10

    .line 1271
    .line 1272
    double-to-float v8, v8

    .line 1273
    sub-float/2addr v6, v8

    .line 1274
    iput v6, p2, Landroid/graphics/PointF;->x:F

    .line 1275
    .line 1276
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1277
    .line 1278
    .line 1279
    move-result-object v2

    .line 1280
    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 1281
    .line 1282
    iget v6, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1283
    .line 1284
    int-to-float v6, v6

    .line 1285
    div-float/2addr v6, v0

    .line 1286
    float-to-double v8, v6

    .line 1287
    iget v0, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1288
    .line 1289
    int-to-double v10, v0

    .line 1290
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 1291
    .line 1292
    .line 1293
    move-result-wide v10

    .line 1294
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    .line 1295
    .line 1296
    .line 1297
    move-result-wide v10

    .line 1298
    mul-double v8, v8, v10

    .line 1299
    .line 1300
    double-to-float v0, v8

    .line 1301
    sub-float/2addr v2, v0

    .line 1302
    iput v2, p2, Landroid/graphics/PointF;->y:F

    .line 1303
    .line 1304
    iget v0, p2, Landroid/graphics/PointF;->x:F

    .line 1305
    .line 1306
    iget v2, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 1307
    .line 1308
    int-to-double v8, v2

    .line 1309
    iget v2, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1310
    .line 1311
    add-int/lit8 v2, v2, -0x5a

    .line 1312
    .line 1313
    int-to-double v10, v2

    .line 1314
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 1315
    .line 1316
    .line 1317
    move-result-wide v10

    .line 1318
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    .line 1319
    .line 1320
    .line 1321
    move-result-wide v10

    .line 1322
    mul-double v8, v8, v10

    .line 1323
    .line 1324
    double-to-float v2, v8

    .line 1325
    add-float/2addr v0, v2

    .line 1326
    iput v0, v1, Landroid/graphics/PointF;->x:F

    .line 1327
    .line 1328
    iget v0, p2, Landroid/graphics/PointF;->y:F

    .line 1329
    .line 1330
    iget v2, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 1331
    .line 1332
    int-to-double v8, v2

    .line 1333
    iget v2, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1334
    .line 1335
    add-int/lit8 v2, v2, -0x5a

    .line 1336
    .line 1337
    int-to-double v10, v2

    .line 1338
    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    .line 1339
    .line 1340
    .line 1341
    move-result-wide v10

    .line 1342
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    .line 1343
    .line 1344
    .line 1345
    move-result-wide v10

    .line 1346
    mul-double v8, v8, v10

    .line 1347
    .line 1348
    double-to-float v2, v8

    .line 1349
    add-float/2addr v0, v2

    .line 1350
    iput v0, v1, Landroid/graphics/PointF;->y:F

    .line 1351
    .line 1352
    :cond_d
    invoke-virtual {p1, v5, v1}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1353
    .line 1354
    .line 1355
    invoke-virtual {p1, v4, v7}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1356
    .line 1357
    .line 1358
    invoke-virtual {p1, v3, p2}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1359
    .line 1360
    .line 1361
    goto/16 :goto_1

    .line 1362
    .line 1363
    :pswitch_4
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1364
    .line 1365
    .line 1366
    move-result-object v0

    .line 1367
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1368
    .line 1369
    .line 1370
    move-result-object v1

    .line 1371
    invoke-direct {p0, p2, v0, v1}, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Z

    .line 1372
    .line 1373
    .line 1374
    move-result v0

    .line 1375
    if-nez v0, :cond_e

    .line 1376
    .line 1377
    return-void

    .line 1378
    :cond_e
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1379
    .line 1380
    .line 1381
    move-result-object v0

    .line 1382
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1383
    .line 1384
    .line 1385
    move-result-object v1

    .line 1386
    invoke-direct {p0, v0, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1387
    .line 1388
    .line 1389
    move-result-object p2

    .line 1390
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1391
    .line 1392
    .line 1393
    move-result-object v0

    .line 1394
    invoke-static {v0, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 1395
    .line 1396
    .line 1397
    move-result v0

    .line 1398
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1399
    .line 1400
    int-to-float v1, v1

    .line 1401
    cmpg-float v0, v0, v1

    .line 1402
    .line 1403
    if-gez v0, :cond_f

    .line 1404
    .line 1405
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1406
    .line 1407
    .line 1408
    move-result-object v0

    .line 1409
    iget v0, v0, Landroid/graphics/PointF;->x:F

    .line 1410
    .line 1411
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1412
    .line 1413
    int-to-double v6, v1

    .line 1414
    iget v1, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1415
    .line 1416
    add-int/lit8 v1, v1, -0x5a

    .line 1417
    .line 1418
    int-to-double v8, v1

    .line 1419
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 1420
    .line 1421
    .line 1422
    move-result-wide v8

    .line 1423
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    .line 1424
    .line 1425
    .line 1426
    move-result-wide v8

    .line 1427
    mul-double v6, v6, v8

    .line 1428
    .line 1429
    double-to-float v1, v6

    .line 1430
    sub-float/2addr v0, v1

    .line 1431
    iput v0, p2, Landroid/graphics/PointF;->x:F

    .line 1432
    .line 1433
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1434
    .line 1435
    .line 1436
    move-result-object v0

    .line 1437
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 1438
    .line 1439
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1440
    .line 1441
    int-to-double v5, v1

    .line 1442
    iget v1, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1443
    .line 1444
    add-int/lit8 v1, v1, -0x5a

    .line 1445
    .line 1446
    int-to-double v7, v1

    .line 1447
    invoke-static {v7, v8}, Ljava/lang/Math;->toRadians(D)D

    .line 1448
    .line 1449
    .line 1450
    move-result-wide v7

    .line 1451
    invoke-static {v7, v8}, Ljava/lang/Math;->sin(D)D

    .line 1452
    .line 1453
    .line 1454
    move-result-wide v7

    .line 1455
    mul-double v5, v5, v7

    .line 1456
    .line 1457
    double-to-float v1, v5

    .line 1458
    sub-float/2addr v0, v1

    .line 1459
    iput v0, p2, Landroid/graphics/PointF;->y:F

    .line 1460
    .line 1461
    :cond_f
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1462
    .line 1463
    .line 1464
    move-result-object v0

    .line 1465
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1466
    .line 1467
    .line 1468
    move-result-object v1

    .line 1469
    invoke-direct {p0, v0, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1470
    .line 1471
    .line 1472
    move-result-object v0

    .line 1473
    invoke-virtual {p1, v3, p2}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1474
    .line 1475
    .line 1476
    invoke-virtual {p1, v2, v0}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1477
    .line 1478
    .line 1479
    goto/16 :goto_1

    .line 1480
    .line 1481
    :pswitch_5
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1482
    .line 1483
    .line 1484
    move-result-object v0

    .line 1485
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1486
    .line 1487
    .line 1488
    move-result-object v1

    .line 1489
    invoke-direct {p0, p2, v0, v1}, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Z

    .line 1490
    .line 1491
    .line 1492
    move-result v0

    .line 1493
    if-nez v0, :cond_10

    .line 1494
    .line 1495
    return-void

    .line 1496
    :cond_10
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1497
    .line 1498
    .line 1499
    move-result-object v0

    .line 1500
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1501
    .line 1502
    .line 1503
    move-result-object v1

    .line 1504
    invoke-direct {p0, v0, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1505
    .line 1506
    .line 1507
    move-result-object p2

    .line 1508
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1509
    .line 1510
    .line 1511
    move-result-object v0

    .line 1512
    invoke-static {v0, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 1513
    .line 1514
    .line 1515
    move-result v0

    .line 1516
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1517
    .line 1518
    int-to-float v1, v1

    .line 1519
    cmpg-float v0, v0, v1

    .line 1520
    .line 1521
    if-gez v0, :cond_11

    .line 1522
    .line 1523
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1524
    .line 1525
    .line 1526
    move-result-object v0

    .line 1527
    iget v0, v0, Landroid/graphics/PointF;->x:F

    .line 1528
    .line 1529
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1530
    .line 1531
    int-to-double v6, v1

    .line 1532
    iget v1, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1533
    .line 1534
    int-to-double v8, v1

    .line 1535
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 1536
    .line 1537
    .line 1538
    move-result-wide v8

    .line 1539
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    .line 1540
    .line 1541
    .line 1542
    move-result-wide v8

    .line 1543
    mul-double v6, v6, v8

    .line 1544
    .line 1545
    double-to-float v1, v6

    .line 1546
    add-float/2addr v0, v1

    .line 1547
    iput v0, p2, Landroid/graphics/PointF;->x:F

    .line 1548
    .line 1549
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1550
    .line 1551
    .line 1552
    move-result-object v0

    .line 1553
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 1554
    .line 1555
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1556
    .line 1557
    int-to-double v5, v1

    .line 1558
    iget v1, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1559
    .line 1560
    int-to-double v7, v1

    .line 1561
    invoke-static {v7, v8}, Ljava/lang/Math;->toRadians(D)D

    .line 1562
    .line 1563
    .line 1564
    move-result-wide v7

    .line 1565
    invoke-static {v7, v8}, Ljava/lang/Math;->sin(D)D

    .line 1566
    .line 1567
    .line 1568
    move-result-wide v7

    .line 1569
    mul-double v5, v5, v7

    .line 1570
    .line 1571
    double-to-float v1, v5

    .line 1572
    add-float/2addr v0, v1

    .line 1573
    iput v0, p2, Landroid/graphics/PointF;->y:F

    .line 1574
    .line 1575
    :cond_11
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1576
    .line 1577
    .line 1578
    move-result-object v0

    .line 1579
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1580
    .line 1581
    .line 1582
    move-result-object v1

    .line 1583
    invoke-direct {p0, v0, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1584
    .line 1585
    .line 1586
    move-result-object v0

    .line 1587
    invoke-virtual {p1, v4, p2}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1588
    .line 1589
    .line 1590
    invoke-virtual {p1, v2, v0}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1591
    .line 1592
    .line 1593
    goto/16 :goto_1

    .line 1594
    .line 1595
    :pswitch_6
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1596
    .line 1597
    .line 1598
    move-result-object v0

    .line 1599
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1600
    .line 1601
    .line 1602
    move-result-object v1

    .line 1603
    invoke-direct {p0, p2, v0, v1}, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Z

    .line 1604
    .line 1605
    .line 1606
    move-result v0

    .line 1607
    if-nez v0, :cond_12

    .line 1608
    .line 1609
    return-void

    .line 1610
    :cond_12
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1611
    .line 1612
    .line 1613
    move-result-object v0

    .line 1614
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1615
    .line 1616
    .line 1617
    move-result-object v1

    .line 1618
    invoke-direct {p0, v0, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1619
    .line 1620
    .line 1621
    move-result-object p2

    .line 1622
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1623
    .line 1624
    .line 1625
    move-result-object v0

    .line 1626
    invoke-static {v0, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 1627
    .line 1628
    .line 1629
    move-result v0

    .line 1630
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1631
    .line 1632
    int-to-float v1, v1

    .line 1633
    cmpg-float v0, v0, v1

    .line 1634
    .line 1635
    if-gez v0, :cond_13

    .line 1636
    .line 1637
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1638
    .line 1639
    .line 1640
    move-result-object v0

    .line 1641
    iget v0, v0, Landroid/graphics/PointF;->x:F

    .line 1642
    .line 1643
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1644
    .line 1645
    int-to-double v6, v1

    .line 1646
    iget v1, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1647
    .line 1648
    add-int/lit8 v1, v1, -0x5a

    .line 1649
    .line 1650
    int-to-double v8, v1

    .line 1651
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 1652
    .line 1653
    .line 1654
    move-result-wide v8

    .line 1655
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    .line 1656
    .line 1657
    .line 1658
    move-result-wide v8

    .line 1659
    mul-double v6, v6, v8

    .line 1660
    .line 1661
    double-to-float v1, v6

    .line 1662
    add-float/2addr v0, v1

    .line 1663
    iput v0, p2, Landroid/graphics/PointF;->x:F

    .line 1664
    .line 1665
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1666
    .line 1667
    .line 1668
    move-result-object v0

    .line 1669
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 1670
    .line 1671
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1672
    .line 1673
    int-to-double v6, v1

    .line 1674
    iget v1, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1675
    .line 1676
    add-int/lit8 v1, v1, -0x5a

    .line 1677
    .line 1678
    int-to-double v8, v1

    .line 1679
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 1680
    .line 1681
    .line 1682
    move-result-wide v8

    .line 1683
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    .line 1684
    .line 1685
    .line 1686
    move-result-wide v8

    .line 1687
    mul-double v6, v6, v8

    .line 1688
    .line 1689
    double-to-float v1, v6

    .line 1690
    add-float/2addr v0, v1

    .line 1691
    iput v0, p2, Landroid/graphics/PointF;->y:F

    .line 1692
    .line 1693
    :cond_13
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1694
    .line 1695
    .line 1696
    move-result-object v0

    .line 1697
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1698
    .line 1699
    .line 1700
    move-result-object v1

    .line 1701
    invoke-direct {p0, v0, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1702
    .line 1703
    .line 1704
    move-result-object v0

    .line 1705
    invoke-virtual {p1, v5, p2}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1706
    .line 1707
    .line 1708
    invoke-virtual {p1, v4, v0}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1709
    .line 1710
    .line 1711
    goto :goto_1

    .line 1712
    :pswitch_7
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1713
    .line 1714
    .line 1715
    move-result-object v0

    .line 1716
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1717
    .line 1718
    .line 1719
    move-result-object v1

    .line 1720
    invoke-direct {p0, p2, v0, v1}, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Z

    .line 1721
    .line 1722
    .line 1723
    move-result v0

    .line 1724
    if-nez v0, :cond_14

    .line 1725
    .line 1726
    return-void

    .line 1727
    :cond_14
    invoke-virtual {p1, v5}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1728
    .line 1729
    .line 1730
    move-result-object v0

    .line 1731
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1732
    .line 1733
    .line 1734
    move-result-object v1

    .line 1735
    invoke-direct {p0, v0, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1736
    .line 1737
    .line 1738
    move-result-object p2

    .line 1739
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1740
    .line 1741
    .line 1742
    move-result-object v0

    .line 1743
    invoke-static {v0, p2}, Lcom/samsung/sdraw/PointF;->O8(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)F

    .line 1744
    .line 1745
    .line 1746
    move-result v0

    .line 1747
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1748
    .line 1749
    int-to-float v1, v1

    .line 1750
    cmpg-float v0, v0, v1

    .line 1751
    .line 1752
    if-gez v0, :cond_15

    .line 1753
    .line 1754
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1755
    .line 1756
    .line 1757
    move-result-object v0

    .line 1758
    iget v0, v0, Landroid/graphics/PointF;->x:F

    .line 1759
    .line 1760
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1761
    .line 1762
    int-to-double v6, v1

    .line 1763
    iget v1, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1764
    .line 1765
    int-to-double v8, v1

    .line 1766
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 1767
    .line 1768
    .line 1769
    move-result-wide v8

    .line 1770
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    .line 1771
    .line 1772
    .line 1773
    move-result-wide v8

    .line 1774
    mul-double v6, v6, v8

    .line 1775
    .line 1776
    double-to-float v1, v6

    .line 1777
    sub-float/2addr v0, v1

    .line 1778
    iput v0, p2, Landroid/graphics/PointF;->x:F

    .line 1779
    .line 1780
    invoke-virtual {p1, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1781
    .line 1782
    .line 1783
    move-result-object v0

    .line 1784
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 1785
    .line 1786
    iget v1, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 1787
    .line 1788
    int-to-double v6, v1

    .line 1789
    iget v1, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 1790
    .line 1791
    int-to-double v8, v1

    .line 1792
    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    .line 1793
    .line 1794
    .line 1795
    move-result-wide v8

    .line 1796
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    .line 1797
    .line 1798
    .line 1799
    move-result-wide v8

    .line 1800
    mul-double v6, v6, v8

    .line 1801
    .line 1802
    double-to-float v1, v6

    .line 1803
    sub-float/2addr v0, v1

    .line 1804
    iput v0, p2, Landroid/graphics/PointF;->y:F

    .line 1805
    .line 1806
    :cond_15
    invoke-virtual {p1, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1807
    .line 1808
    .line 1809
    move-result-object v0

    .line 1810
    invoke-virtual {p1, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 1811
    .line 1812
    .line 1813
    move-result-object v1

    .line 1814
    invoke-direct {p0, v0, v1, p2}, Lcom/samsung/sdraw/SelectMode;->O8ooOoo〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 1815
    .line 1816
    .line 1817
    move-result-object v0

    .line 1818
    invoke-virtual {p1, v5, p2}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1819
    .line 1820
    .line 1821
    invoke-virtual {p1, v3, v0}, Lcom/samsung/sdraw/z;->o800o8O(ILcom/samsung/sdraw/PointF;)V

    .line 1822
    .line 1823
    .line 1824
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private o〇O8〇〇o(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Z
    .locals 3

    .line 1
    iget v0, p3, Landroid/graphics/PointF;->x:F

    .line 2
    .line 3
    iget v1, p2, Landroid/graphics/PointF;->x:F

    .line 4
    .line 5
    sub-float/2addr v0, v1

    .line 6
    iget v2, p1, Landroid/graphics/PointF;->y:F

    .line 7
    .line 8
    iget p2, p2, Landroid/graphics/PointF;->y:F

    .line 9
    .line 10
    sub-float/2addr v2, p2

    .line 11
    mul-float v0, v0, v2

    .line 12
    .line 13
    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 14
    .line 15
    sub-float/2addr p3, p2

    .line 16
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 17
    .line 18
    sub-float/2addr p1, v1

    .line 19
    mul-float p3, p3, p1

    .line 20
    .line 21
    sub-float/2addr v0, p3

    .line 22
    const/4 p1, 0x0

    .line 23
    cmpl-float p1, v0, p1

    .line 24
    .line 25
    if-lez p1, :cond_0

    .line 26
    .line 27
    const/4 p1, 0x1

    .line 28
    return p1

    .line 29
    :cond_0
    const/4 p1, 0x0

    .line 30
    return p1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private o〇〇0〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)D
    .locals 3

    .line 1
    iget v0, p2, Landroid/graphics/PointF;->y:F

    .line 2
    .line 3
    iget v1, p1, Landroid/graphics/PointF;->y:F

    .line 4
    .line 5
    sub-float v2, v0, v1

    .line 6
    .line 7
    sub-float/2addr v0, v1

    .line 8
    mul-float v2, v2, v0

    .line 9
    .line 10
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 11
    .line 12
    iget p2, p2, Landroid/graphics/PointF;->x:F

    .line 13
    .line 14
    sub-float v0, p1, p2

    .line 15
    .line 16
    sub-float/2addr p1, p2

    .line 17
    mul-float v0, v0, p1

    .line 18
    .line 19
    add-float/2addr v2, v0

    .line 20
    float-to-double p1, v2

    .line 21
    return-wide p1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic 〇00()[I
    .locals 3

    .line 1
    sget-object v0, Lcom/samsung/sdraw/SelectMode;->OOO:[I

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    invoke-static {}, Lcom/samsung/sdraw/SelectMode$HitDirection;->values()[Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    array-length v0, v0

    .line 11
    new-array v0, v0, [I

    .line 12
    .line 13
    :try_start_0
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->BOTTOM:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 14
    .line 15
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    const/4 v2, 0x5

    .line 20
    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    .line 21
    .line 22
    :catch_0
    :try_start_1
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->DELETE:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    const/16 v2, 0xa

    .line 29
    .line 30
    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    .line 31
    .line 32
    :catch_1
    :try_start_2
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->INNER:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 33
    .line 34
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    const/4 v2, 0x1

    .line 39
    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    .line 40
    .line 41
    :catch_2
    :try_start_3
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->LEFT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 42
    .line 43
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    const/4 v2, 0x2

    .line 48
    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 49
    .line 50
    :catch_3
    :try_start_4
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->LEFT_BOTTOM:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 51
    .line 52
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    const/16 v2, 0x8

    .line 57
    .line 58
    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    .line 59
    .line 60
    :catch_4
    :try_start_5
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->LEFT_TOP:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 61
    .line 62
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    const/4 v2, 0x6

    .line 67
    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    .line 68
    .line 69
    :catch_5
    :try_start_6
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->RIGHT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 70
    .line 71
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    const/4 v2, 0x4

    .line 76
    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    .line 77
    .line 78
    :catch_6
    :try_start_7
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->RIGHT_BOTTOM:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 79
    .line 80
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    const/16 v2, 0x9

    .line 85
    .line 86
    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    .line 87
    .line 88
    :catch_7
    :try_start_8
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->RIGHT_TOP:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 89
    .line 90
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 91
    .line 92
    .line 93
    move-result v1

    .line 94
    const/4 v2, 0x7

    .line 95
    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    .line 96
    .line 97
    :catch_8
    :try_start_9
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->ROTATE_LEFT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 98
    .line 99
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    const/16 v2, 0xb

    .line 104
    .line 105
    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    .line 106
    .line 107
    :catch_9
    :try_start_a
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->ROTATE_RIGHT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 108
    .line 109
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    const/16 v2, 0xc

    .line 114
    .line 115
    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    .line 116
    .line 117
    :catch_a
    :try_start_b
    sget-object v1, Lcom/samsung/sdraw/SelectMode$HitDirection;->TOP:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 118
    .line 119
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 120
    .line 121
    .line 122
    move-result v1

    .line 123
    const/4 v2, 0x3

    .line 124
    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    .line 125
    .line 126
    :catch_b
    sput-object v0, Lcom/samsung/sdraw/SelectMode;->OOO:[I

    .line 127
    .line 128
    return-object v0
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method private 〇0000OOO(Lcom/samsung/sdraw/AbstractModeContext;Lcom/samsung/sdraw/PointF;)V
    .locals 4

    .line 1
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 2
    .line 3
    check-cast p2, Lcom/samsung/sdraw/z;

    .line 4
    .line 5
    invoke-virtual {p2}, Lcom/samsung/sdraw/z;->O8〇o()[Lcom/samsung/sdraw/PointF;

    .line 6
    .line 7
    .line 8
    move-result-object p2

    .line 9
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 12
    .line 13
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->oo88o8O:[Lcom/samsung/sdraw/PointF;

    .line 14
    .line 15
    const/4 v3, 0x1

    .line 16
    invoke-virtual {v0, v1, p2, v2, v3}, Lcom/samsung/sdraw/AbstractStage;->o88〇OO08〇(Lcom/samsung/sdraw/AbstractSprite;[Lcom/samsung/sdraw/PointF;[Lcom/samsung/sdraw/PointF;Z)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private 〇80〇808〇O(FFLcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;
    .locals 3

    .line 1
    new-instance v0, Lcom/samsung/sdraw/PointF;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/samsung/sdraw/PointF;-><init>()V

    .line 4
    .line 5
    .line 6
    sub-float v1, p1, p2

    .line 7
    .line 8
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    cmpl-float p1, p2, p1

    .line 13
    .line 14
    if-lez p1, :cond_0

    .line 15
    .line 16
    iget p1, p4, Landroid/graphics/PointF;->x:F

    .line 17
    .line 18
    mul-float p1, p1, p2

    .line 19
    .line 20
    iget v2, p3, Landroid/graphics/PointF;->x:F

    .line 21
    .line 22
    mul-float v2, v2, v1

    .line 23
    .line 24
    sub-float/2addr p1, v2

    .line 25
    sub-float v2, p2, v1

    .line 26
    .line 27
    div-float/2addr p1, v2

    .line 28
    iput p1, v0, Landroid/graphics/PointF;->x:F

    .line 29
    .line 30
    iget p1, p4, Landroid/graphics/PointF;->y:F

    .line 31
    .line 32
    mul-float p2, p2, p1

    .line 33
    .line 34
    iget p1, p3, Landroid/graphics/PointF;->y:F

    .line 35
    .line 36
    mul-float v1, v1, p1

    .line 37
    .line 38
    sub-float/2addr p2, v1

    .line 39
    div-float/2addr p2, v2

    .line 40
    iput p2, v0, Landroid/graphics/PointF;->y:F

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_0
    iget p1, p4, Landroid/graphics/PointF;->x:F

    .line 44
    .line 45
    mul-float p1, p1, p2

    .line 46
    .line 47
    iget v2, p3, Landroid/graphics/PointF;->x:F

    .line 48
    .line 49
    mul-float v2, v2, v1

    .line 50
    .line 51
    add-float/2addr p1, v2

    .line 52
    add-float v2, p2, v1

    .line 53
    .line 54
    div-float/2addr p1, v2

    .line 55
    iput p1, v0, Landroid/graphics/PointF;->x:F

    .line 56
    .line 57
    iget p1, p4, Landroid/graphics/PointF;->y:F

    .line 58
    .line 59
    mul-float p2, p2, p1

    .line 60
    .line 61
    iget p1, p3, Landroid/graphics/PointF;->y:F

    .line 62
    .line 63
    mul-float v1, v1, p1

    .line 64
    .line 65
    add-float/2addr p2, v1

    .line 66
    div-float/2addr p2, v2

    .line 67
    iput p2, v0, Landroid/graphics/PointF;->y:F

    .line 68
    .line 69
    :goto_0
    return-object v0
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method private 〇O00(Lcom/samsung/sdraw/AbstractModeContext;Landroid/view/MotionEvent;)V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/samsung/sdraw/SelectMode;->〇〇808〇:Z

    .line 3
    .line 4
    iget-object v1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 5
    .line 6
    new-instance v2, Lcom/samsung/sdraw/PointF;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getX(I)F

    .line 10
    .line 11
    .line 12
    move-result v4

    .line 13
    float-to-int v4, v4

    .line 14
    int-to-float v4, v4

    .line 15
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getY(I)F

    .line 16
    .line 17
    .line 18
    move-result v5

    .line 19
    float-to-int v5, v5

    .line 20
    int-to-float v5, v5

    .line 21
    invoke-direct {v2, v4, v5}, Lcom/samsung/sdraw/PointF;-><init>(FF)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, v2}, Lcom/samsung/sdraw/AbstractStage;->Ooo(Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    iget-object v2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 29
    .line 30
    new-instance v4, Lcom/samsung/sdraw/PointF;

    .line 31
    .line 32
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getX(I)F

    .line 33
    .line 34
    .line 35
    move-result v5

    .line 36
    float-to-int v5, v5

    .line 37
    int-to-float v5, v5

    .line 38
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getY(I)F

    .line 39
    .line 40
    .line 41
    move-result p2

    .line 42
    float-to-int p2, p2

    .line 43
    int-to-float p2, p2

    .line 44
    invoke-direct {v4, v5, p2}, Lcom/samsung/sdraw/PointF;-><init>(FF)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2, v4}, Lcom/samsung/sdraw/AbstractStage;->Ooo(Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 48
    .line 49
    .line 50
    move-result-object p2

    .line 51
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 57
    .line 58
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    .line 60
    .line 61
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 62
    .line 63
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    invoke-direct {p0, p1, p2, v3}, Lcom/samsung/sdraw/SelectMode;->〇O〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/RectF;Z)V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method private 〇O〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/RectF;Z)V
    .locals 19

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iget-object v2, v0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    const/4 v3, 0x2

    .line 16
    if-lt v1, v3, :cond_9

    .line 17
    .line 18
    if-ge v2, v3, :cond_0

    .line 19
    .line 20
    goto/16 :goto_2

    .line 21
    .line 22
    :cond_0
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    sub-int/2addr v2, v3

    .line 29
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Lcom/samsung/sdraw/PointF;

    .line 34
    .line 35
    iget-object v2, v0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 36
    .line 37
    iget-object v4, v0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 38
    .line 39
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 40
    .line 41
    .line 42
    move-result v4

    .line 43
    sub-int/2addr v4, v3

    .line 44
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    check-cast v2, Lcom/samsung/sdraw/PointF;

    .line 49
    .line 50
    invoke-direct {v0, v1, v2}, Lcom/samsung/sdraw/SelectMode;->O〇8O8〇008(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)D

    .line 51
    .line 52
    .line 53
    move-result-wide v1

    .line 54
    iget-object v4, v0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 55
    .line 56
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 57
    .line 58
    .line 59
    move-result v5

    .line 60
    const/4 v6, 0x1

    .line 61
    sub-int/2addr v5, v6

    .line 62
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object v4

    .line 66
    check-cast v4, Lcom/samsung/sdraw/PointF;

    .line 67
    .line 68
    iget-object v5, v0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 69
    .line 70
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    .line 71
    .line 72
    .line 73
    move-result v7

    .line 74
    sub-int/2addr v7, v6

    .line 75
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 76
    .line 77
    .line 78
    move-result-object v5

    .line 79
    check-cast v5, Lcom/samsung/sdraw/PointF;

    .line 80
    .line 81
    invoke-direct {v0, v4, v5}, Lcom/samsung/sdraw/SelectMode;->O〇8O8〇008(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)D

    .line 82
    .line 83
    .line 84
    move-result-wide v4

    .line 85
    iget-object v7, v0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 86
    .line 87
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    .line 88
    .line 89
    .line 90
    move-result v8

    .line 91
    sub-int/2addr v8, v3

    .line 92
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 93
    .line 94
    .line 95
    move-result-object v7

    .line 96
    check-cast v7, Lcom/samsung/sdraw/PointF;

    .line 97
    .line 98
    iget-object v8, v0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 99
    .line 100
    iget-object v9, v0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 101
    .line 102
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    .line 103
    .line 104
    .line 105
    move-result v9

    .line 106
    sub-int/2addr v9, v3

    .line 107
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 108
    .line 109
    .line 110
    move-result-object v3

    .line 111
    check-cast v3, Lcom/samsung/sdraw/PointF;

    .line 112
    .line 113
    invoke-direct {v0, v7, v3}, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)D

    .line 114
    .line 115
    .line 116
    move-result-wide v7

    .line 117
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    .line 118
    .line 119
    .line 120
    move-result-wide v7

    .line 121
    iget-object v3, v0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 122
    .line 123
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 124
    .line 125
    .line 126
    move-result v9

    .line 127
    sub-int/2addr v9, v6

    .line 128
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 129
    .line 130
    .line 131
    move-result-object v3

    .line 132
    check-cast v3, Lcom/samsung/sdraw/PointF;

    .line 133
    .line 134
    iget-object v9, v0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 135
    .line 136
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    .line 137
    .line 138
    .line 139
    move-result v10

    .line 140
    sub-int/2addr v10, v6

    .line 141
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 142
    .line 143
    .line 144
    move-result-object v9

    .line 145
    check-cast v9, Lcom/samsung/sdraw/PointF;

    .line 146
    .line 147
    invoke-direct {v0, v3, v9}, Lcom/samsung/sdraw/SelectMode;->o〇〇0〇(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)D

    .line 148
    .line 149
    .line 150
    move-result-wide v9

    .line 151
    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    .line 152
    .line 153
    .line 154
    move-result-wide v9

    .line 155
    const-wide/16 v11, 0x0

    .line 156
    .line 157
    const-wide v13, 0x4076800000000000L    # 360.0

    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    cmpg-double v3, v1, v11

    .line 163
    .line 164
    if-gez v3, :cond_1

    .line 165
    .line 166
    add-double/2addr v1, v13

    .line 167
    :cond_1
    cmpg-double v3, v4, v11

    .line 168
    .line 169
    if-gez v3, :cond_2

    .line 170
    .line 171
    add-double/2addr v4, v13

    .line 172
    :cond_2
    const-wide v15, 0x4056800000000000L    # 90.0

    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    const-wide v17, 0x4070e00000000000L    # 270.0

    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    cmpl-double v3, v1, v17

    .line 183
    .line 184
    if-lez v3, :cond_3

    .line 185
    .line 186
    cmpg-double v3, v4, v15

    .line 187
    .line 188
    if-gez v3, :cond_3

    .line 189
    .line 190
    add-double/2addr v4, v13

    .line 191
    goto :goto_0

    .line 192
    :cond_3
    cmpg-double v3, v1, v15

    .line 193
    .line 194
    if-gez v3, :cond_4

    .line 195
    .line 196
    cmpl-double v3, v4, v17

    .line 197
    .line 198
    if-lez v3, :cond_4

    .line 199
    .line 200
    sub-double/2addr v4, v13

    .line 201
    :cond_4
    :goto_0
    sub-double/2addr v4, v1

    .line 202
    sub-double/2addr v7, v9

    .line 203
    invoke-static {v7, v8}, Ljava/lang/Math;->abs(D)D

    .line 204
    .line 205
    .line 206
    move-result-wide v1

    .line 207
    const-wide/high16 v7, 0x403e000000000000L    # 30.0

    .line 208
    .line 209
    cmpg-double v3, v1, v7

    .line 210
    .line 211
    if-gtz v3, :cond_7

    .line 212
    .line 213
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    .line 214
    .line 215
    .line 216
    move-result-wide v1

    .line 217
    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    .line 218
    .line 219
    cmpl-double v3, v1, v7

    .line 220
    .line 221
    if-lez v3, :cond_7

    .line 222
    .line 223
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 224
    .line 225
    check-cast v1, Lcom/samsung/sdraw/z;

    .line 226
    .line 227
    if-nez v1, :cond_5

    .line 228
    .line 229
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 230
    .line 231
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 232
    .line 233
    .line 234
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 235
    .line 236
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 237
    .line 238
    .line 239
    return-void

    .line 240
    :cond_5
    cmpl-double v2, v4, v11

    .line 241
    .line 242
    if-lez v2, :cond_6

    .line 243
    .line 244
    const/4 v2, -0x3

    .line 245
    invoke-virtual {v1, v2}, Lcom/samsung/sdraw/z;->〇00(I)V

    .line 246
    .line 247
    .line 248
    goto :goto_1

    .line 249
    :cond_6
    const/4 v2, 0x3

    .line 250
    invoke-virtual {v1, v2}, Lcom/samsung/sdraw/z;->〇00(I)V

    .line 251
    .line 252
    .line 253
    :cond_7
    :goto_1
    move-object/from16 v1, p1

    .line 254
    .line 255
    if-eqz p3, :cond_8

    .line 256
    .line 257
    iget-object v2, v1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 258
    .line 259
    iget-object v3, v0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 260
    .line 261
    move-object v4, v3

    .line 262
    check-cast v4, Lcom/samsung/sdraw/z;

    .line 263
    .line 264
    iget v4, v4, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 265
    .line 266
    int-to-float v4, v4

    .line 267
    iget v5, v0, Lcom/samsung/sdraw/SelectMode;->〇oo〇:F

    .line 268
    .line 269
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/sdraw/AbstractStage;->O8O〇(Lcom/samsung/sdraw/AbstractSprite;FFZ)V

    .line 270
    .line 271
    .line 272
    :cond_8
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 273
    .line 274
    .line 275
    return-void

    .line 276
    :cond_9
    :goto_2
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 277
    .line 278
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 279
    .line 280
    .line 281
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 282
    .line 283
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 284
    .line 285
    .line 286
    return-void
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
.end method

.method private 〇oOO8O8(Lcom/samsung/sdraw/AbstractModeContext;)V
    .locals 2

    .line 1
    iget-object p1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractStage;->〇08O8o〇0()Ljava/util/LinkedList;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    const/4 p1, 0x0

    .line 18
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 19
    .line 20
    return-void

    .line 21
    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Lcom/samsung/sdraw/AbstractSprite;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSprite;->O8()Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-eqz v1, :cond_0

    .line 32
    .line 33
    instance-of v1, v0, Lcom/samsung/sdraw/z;

    .line 34
    .line 35
    if-eqz v1, :cond_0

    .line 36
    .line 37
    iput-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private 〇oo〇(Lcom/samsung/sdraw/AbstractModeContext;Lcom/samsung/sdraw/PointF;)Z
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSprite;->〇o00〇〇Oo()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->O〇8O8〇008:Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 12
    .line 13
    check-cast v2, Lcom/samsung/sdraw/z;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/samsung/sdraw/z;->〇oOO8O8()Lcom/samsung/sdraw/ImageInfo;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    invoke-interface {v0, v2, v1}, Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;->〇080(Lcom/samsung/sdraw/ObjectInfo;Z)V

    .line 20
    .line 21
    .line 22
    :cond_0
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 23
    .line 24
    check-cast v0, Lcom/samsung/sdraw/Stage;

    .line 25
    .line 26
    invoke-virtual {v0, p2}, Lcom/samsung/sdraw/Stage;->〇00O0O0(Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/AbstractSprite;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 31
    .line 32
    const/4 v2, 0x2

    .line 33
    invoke-virtual {v0, v2}, Lcom/samsung/sdraw/AbstractStage;->oo88o8O(I)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 37
    .line 38
    const/4 v3, 0x4

    .line 39
    invoke-virtual {v0, v3}, Lcom/samsung/sdraw/AbstractStage;->oo88o8O(I)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractStage;->o〇〇0〇()V

    .line 45
    .line 46
    .line 47
    const/4 v0, 0x3

    .line 48
    const-class v4, Lcom/samsung/sdraw/TextSprite;

    .line 49
    .line 50
    const-class v5, Lcom/samsung/sdraw/z;

    .line 51
    .line 52
    const-class v6, Lcom/samsung/sdraw/r;

    .line 53
    .line 54
    const-class v7, Lcom/samsung/sdraw/StrokeSprite;

    .line 55
    .line 56
    const/4 v8, 0x1

    .line 57
    if-eqz p2, :cond_c

    .line 58
    .line 59
    iput-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 60
    .line 61
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 62
    .line 63
    invoke-virtual {p2, v7, v6}, Lcom/samsung/sdraw/AbstractStage;->〇o〇(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    iget-object v9, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 68
    .line 69
    invoke-virtual {v9, v7, v6}, Lcom/samsung/sdraw/AbstractStage;->〇O00(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 70
    .line 71
    .line 72
    move-result-object v6

    .line 73
    iget-object v7, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 74
    .line 75
    invoke-virtual {v7, v5}, Lcom/samsung/sdraw/AbstractStage;->〇o00〇〇Oo(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 76
    .line 77
    .line 78
    move-result-object v7

    .line 79
    iget-object v9, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 80
    .line 81
    invoke-virtual {v9, v5}, Lcom/samsung/sdraw/AbstractStage;->〇O〇(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 82
    .line 83
    .line 84
    move-result-object v5

    .line 85
    iget-object v9, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 86
    .line 87
    invoke-virtual {v9, v4}, Lcom/samsung/sdraw/AbstractStage;->〇o00〇〇Oo(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 88
    .line 89
    .line 90
    move-result-object v9

    .line 91
    iget-object v10, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 92
    .line 93
    invoke-virtual {v10, v4}, Lcom/samsung/sdraw/AbstractStage;->〇O〇(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 94
    .line 95
    .line 96
    move-result-object v4

    .line 97
    iget-object v10, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 98
    .line 99
    invoke-virtual {v10, v8}, Lcom/samsung/sdraw/AbstractStage;->oo88o8O(I)V

    .line 100
    .line 101
    .line 102
    if-eqz v7, :cond_1

    .line 103
    .line 104
    iget-object v10, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 105
    .line 106
    iget-object v11, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 107
    .line 108
    invoke-virtual {v10, v2, v7, v11}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 109
    .line 110
    .line 111
    :cond_1
    if-eqz v9, :cond_2

    .line 112
    .line 113
    iget-object v7, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 114
    .line 115
    iget-object v10, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 116
    .line 117
    invoke-virtual {v7, v2, v9, v10}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 118
    .line 119
    .line 120
    :cond_2
    if-eqz p2, :cond_3

    .line 121
    .line 122
    iget-object v2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 123
    .line 124
    invoke-virtual {v2, v1, p2}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 125
    .line 126
    .line 127
    :cond_3
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 128
    .line 129
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractStage;->o〇0OOo〇0()I

    .line 130
    .line 131
    .line 132
    move-result p2

    .line 133
    if-nez p2, :cond_6

    .line 134
    .line 135
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 136
    .line 137
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractStage;->o0ooO()Z

    .line 138
    .line 139
    .line 140
    move-result p2

    .line 141
    if-nez p2, :cond_6

    .line 142
    .line 143
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 144
    .line 145
    iget-object v1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 146
    .line 147
    invoke-virtual {p2, v8, v1}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 148
    .line 149
    .line 150
    if-eqz v5, :cond_4

    .line 151
    .line 152
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 153
    .line 154
    iget-object v1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 155
    .line 156
    invoke-virtual {p2, v3, v5, v1}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 157
    .line 158
    .line 159
    :cond_4
    if-eqz v4, :cond_5

    .line 160
    .line 161
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 162
    .line 163
    iget-object v1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 164
    .line 165
    invoke-virtual {p2, v3, v4, v1}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 166
    .line 167
    .line 168
    :cond_5
    if-eqz v6, :cond_a

    .line 169
    .line 170
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 171
    .line 172
    invoke-virtual {p2, v0, v6}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 173
    .line 174
    .line 175
    goto :goto_0

    .line 176
    :cond_6
    if-eqz v5, :cond_7

    .line 177
    .line 178
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 179
    .line 180
    iget-object v1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 181
    .line 182
    invoke-virtual {p2, v3, v5, v1}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 183
    .line 184
    .line 185
    :cond_7
    if-eqz v4, :cond_8

    .line 186
    .line 187
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 188
    .line 189
    iget-object v1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 190
    .line 191
    invoke-virtual {p2, v3, v4, v1}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 192
    .line 193
    .line 194
    :cond_8
    if-eqz v6, :cond_9

    .line 195
    .line 196
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 197
    .line 198
    invoke-virtual {p2, v0, v6}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 199
    .line 200
    .line 201
    :cond_9
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 202
    .line 203
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 204
    .line 205
    invoke-virtual {p2, v8, v0}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 206
    .line 207
    .line 208
    :cond_a
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractModeContext;->〇O〇()V

    .line 209
    .line 210
    .line 211
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O〇8O8〇008:Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;

    .line 212
    .line 213
    if-eqz p1, :cond_b

    .line 214
    .line 215
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 216
    .line 217
    check-cast p2, Lcom/samsung/sdraw/z;

    .line 218
    .line 219
    invoke-virtual {p2}, Lcom/samsung/sdraw/z;->〇oOO8O8()Lcom/samsung/sdraw/ImageInfo;

    .line 220
    .line 221
    .line 222
    move-result-object p2

    .line 223
    invoke-interface {p1, p2, v8}, Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;->〇080(Lcom/samsung/sdraw/ObjectInfo;Z)V

    .line 224
    .line 225
    .line 226
    :cond_b
    const/4 v1, 0x1

    .line 227
    goto/16 :goto_2

    .line 228
    .line 229
    :cond_c
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 230
    .line 231
    invoke-virtual {p2, v7, v6}, Lcom/samsung/sdraw/AbstractStage;->〇o〇(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 232
    .line 233
    .line 234
    move-result-object p2

    .line 235
    iget-object v9, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 236
    .line 237
    invoke-virtual {v9, v7, v6}, Lcom/samsung/sdraw/AbstractStage;->〇O00(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 238
    .line 239
    .line 240
    move-result-object v6

    .line 241
    iget-object v7, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 242
    .line 243
    invoke-virtual {v7, v5}, Lcom/samsung/sdraw/AbstractStage;->〇o00〇〇Oo(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 244
    .line 245
    .line 246
    move-result-object v7

    .line 247
    iget-object v9, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 248
    .line 249
    invoke-virtual {v9, v5}, Lcom/samsung/sdraw/AbstractStage;->〇O〇(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 250
    .line 251
    .line 252
    move-result-object v5

    .line 253
    iget-object v9, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 254
    .line 255
    invoke-virtual {v9, v4}, Lcom/samsung/sdraw/AbstractStage;->〇o00〇〇Oo(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 256
    .line 257
    .line 258
    move-result-object v9

    .line 259
    iget-object v10, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 260
    .line 261
    invoke-virtual {v10, v4}, Lcom/samsung/sdraw/AbstractStage;->〇O〇(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 262
    .line 263
    .line 264
    move-result-object v4

    .line 265
    if-eqz v7, :cond_d

    .line 266
    .line 267
    iget-object v10, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 268
    .line 269
    iget-object v11, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 270
    .line 271
    invoke-virtual {v10, v2, v7, v11}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 272
    .line 273
    .line 274
    :cond_d
    if-eqz v9, :cond_e

    .line 275
    .line 276
    iget-object v7, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 277
    .line 278
    iget-object v10, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 279
    .line 280
    invoke-virtual {v7, v2, v9, v10}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 281
    .line 282
    .line 283
    :cond_e
    if-eqz p2, :cond_f

    .line 284
    .line 285
    iget-object v2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 286
    .line 287
    invoke-virtual {v2, v1, p2}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 288
    .line 289
    .line 290
    :cond_f
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 291
    .line 292
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractStage;->o〇0OOo〇0()I

    .line 293
    .line 294
    .line 295
    move-result p2

    .line 296
    if-nez p2, :cond_12

    .line 297
    .line 298
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 299
    .line 300
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractStage;->o0ooO()Z

    .line 301
    .line 302
    .line 303
    move-result p2

    .line 304
    if-nez p2, :cond_12

    .line 305
    .line 306
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 307
    .line 308
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 309
    .line 310
    invoke-virtual {p2, v8, v2}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 311
    .line 312
    .line 313
    if-eqz v5, :cond_10

    .line 314
    .line 315
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 316
    .line 317
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 318
    .line 319
    invoke-virtual {p2, v3, v5, v2}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 320
    .line 321
    .line 322
    :cond_10
    if-eqz v4, :cond_11

    .line 323
    .line 324
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 325
    .line 326
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 327
    .line 328
    invoke-virtual {p2, v3, v4, v2}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 329
    .line 330
    .line 331
    :cond_11
    if-eqz v6, :cond_16

    .line 332
    .line 333
    iget-object p1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 334
    .line 335
    invoke-virtual {p1, v0, v6}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 336
    .line 337
    .line 338
    goto :goto_1

    .line 339
    :cond_12
    if-eqz v5, :cond_13

    .line 340
    .line 341
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 342
    .line 343
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 344
    .line 345
    invoke-virtual {p2, v3, v5, v2}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 346
    .line 347
    .line 348
    :cond_13
    if-eqz v4, :cond_14

    .line 349
    .line 350
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 351
    .line 352
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 353
    .line 354
    invoke-virtual {p2, v3, v4, v2}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 355
    .line 356
    .line 357
    :cond_14
    if-eqz v6, :cond_15

    .line 358
    .line 359
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 360
    .line 361
    invoke-virtual {p2, v0, v6}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 362
    .line 363
    .line 364
    :cond_15
    iget-object p1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 365
    .line 366
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 367
    .line 368
    invoke-virtual {p1, v8, p2}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 369
    .line 370
    .line 371
    :cond_16
    :goto_1
    const/4 p1, 0x0

    .line 372
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 373
    .line 374
    :goto_2
    return v1
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
.end method

.method private 〇〇888(Landroid/graphics/Rect;F)Landroid/graphics/Rect;
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    .line 2
    .line 3
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    div-float/2addr v1, p2

    .line 7
    float-to-int v1, v1

    .line 8
    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 9
    .line 10
    int-to-float v2, v2

    .line 11
    div-float/2addr v2, p2

    .line 12
    float-to-int v2, v2

    .line 13
    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 14
    .line 15
    int-to-float v3, v3

    .line 16
    div-float/2addr v3, p2

    .line 17
    float-to-int v3, v3

    .line 18
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    .line 19
    .line 20
    int-to-float p1, p1

    .line 21
    div-float/2addr p1, p2

    .line 22
    float-to-int p1, p1

    .line 23
    invoke-direct {v0, v1, v2, v3, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 24
    .line 25
    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private 〇〇8O0〇8(Lcom/samsung/sdraw/AbstractModeContext;Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 6

    .line 1
    iget-object p3, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 2
    .line 3
    new-instance v0, Lcom/samsung/sdraw/PointF;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getX(I)F

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    float-to-int v2, v2

    .line 11
    int-to-float v2, v2

    .line 12
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getY(I)F

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    float-to-int v1, v1

    .line 17
    int-to-float v1, v1

    .line 18
    invoke-direct {v0, v2, v1}, Lcom/samsung/sdraw/PointF;-><init>(FF)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p3, v0}, Lcom/samsung/sdraw/AbstractStage;->Ooo(Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 22
    .line 23
    .line 24
    move-result-object p3

    .line 25
    iget-object p1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 26
    .line 27
    new-instance v0, Lcom/samsung/sdraw/PointF;

    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getX(I)F

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    float-to-int v2, v2

    .line 35
    int-to-float v2, v2

    .line 36
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getY(I)F

    .line 37
    .line 38
    .line 39
    move-result p2

    .line 40
    float-to-int p2, p2

    .line 41
    int-to-float p2, p2

    .line 42
    invoke-direct {v0, v2, p2}, Lcom/samsung/sdraw/PointF;-><init>(FF)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1, v0}, Lcom/samsung/sdraw/AbstractStage;->Ooo(Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 50
    .line 51
    new-instance v0, Landroid/graphics/RectF;

    .line 52
    .line 53
    iget v2, p1, Landroid/graphics/PointF;->x:F

    .line 54
    .line 55
    iget v3, p1, Landroid/graphics/PointF;->y:F

    .line 56
    .line 57
    const/high16 v4, 0x3f800000    # 1.0f

    .line 58
    .line 59
    add-float v5, v2, v4

    .line 60
    .line 61
    add-float/2addr v4, v3

    .line 62
    invoke-direct {v0, v2, v3, v5, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p2, v0}, Lcom/samsung/sdraw/AbstractSprite;->o〇0(Landroid/graphics/RectF;)Z

    .line 66
    .line 67
    .line 68
    move-result p2

    .line 69
    if-eqz p2, :cond_0

    .line 70
    .line 71
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇808〇:Z

    .line 72
    .line 73
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 74
    .line 75
    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    .line 77
    .line 78
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 79
    .line 80
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    .line 82
    .line 83
    :cond_0
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method public O8(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/Rect;)V
    .locals 0

    .line 1
    const/4 p2, 0x1

    .line 2
    iput-boolean p2, p0, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o:Z

    .line 3
    .line 4
    invoke-virtual {p0, p1, p2}, Lcom/samsung/sdraw/SelectMode;->〇o〇(Lcom/samsung/sdraw/AbstractModeContext;Z)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public Oo08(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/Canvas;)V
    .locals 16

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-object/from16 v8, p1

    .line 4
    .line 5
    move-object/from16 v9, p2

    .line 6
    .line 7
    iget-object v0, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    const/4 v1, 0x2

    .line 13
    invoke-virtual {v0, v1}, Lcom/samsung/sdraw/AbstractStage;->〇080(I)Landroid/graphics/Bitmap;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iget-object v2, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 18
    .line 19
    const/4 v10, 0x4

    .line 20
    invoke-virtual {v2, v10}, Lcom/samsung/sdraw/AbstractStage;->〇080(I)Landroid/graphics/Bitmap;

    .line 21
    .line 22
    .line 23
    move-result-object v11

    .line 24
    iget-object v2, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 25
    .line 26
    const/4 v3, 0x0

    .line 27
    invoke-virtual {v2, v3}, Lcom/samsung/sdraw/AbstractStage;->〇080(I)Landroid/graphics/Bitmap;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    iget-object v4, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 32
    .line 33
    const/4 v12, 0x3

    .line 34
    invoke-virtual {v4, v12}, Lcom/samsung/sdraw/AbstractStage;->〇080(I)Landroid/graphics/Bitmap;

    .line 35
    .line 36
    .line 37
    move-result-object v13

    .line 38
    iget-object v4, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 39
    .line 40
    const/4 v5, 0x1

    .line 41
    invoke-virtual {v4, v5}, Lcom/samsung/sdraw/AbstractStage;->〇080(I)Landroid/graphics/Bitmap;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sdraw/AbstractModeContext;->OO0o〇〇()Lcom/samsung/sdraw/PointF;

    .line 46
    .line 47
    .line 48
    move-result-object v6

    .line 49
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sdraw/AbstractModeContext;->Oooo8o0〇()F

    .line 50
    .line 51
    .line 52
    move-result v5

    .line 53
    const/4 v14, 0x0

    .line 54
    if-eqz v0, :cond_1

    .line 55
    .line 56
    iget-object v15, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 57
    .line 58
    invoke-virtual {v15, v1}, Lcom/samsung/sdraw/AbstractStage;->o〇O(I)Z

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    if-eqz v1, :cond_1

    .line 63
    .line 64
    iget-object v1, v7, Lcom/samsung/sdraw/SelectMode;->〇080:Landroid/graphics/Paint;

    .line 65
    .line 66
    invoke-virtual {v9, v0, v14, v14, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 67
    .line 68
    .line 69
    :cond_1
    if-eqz v2, :cond_2

    .line 70
    .line 71
    iget-object v0, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 72
    .line 73
    invoke-virtual {v0, v3}, Lcom/samsung/sdraw/AbstractStage;->o〇O(I)Z

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    if-eqz v0, :cond_2

    .line 78
    .line 79
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇080:Landroid/graphics/Paint;

    .line 80
    .line 81
    invoke-virtual {v9, v2, v14, v14, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 82
    .line 83
    .line 84
    :cond_2
    iget-object v0, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 85
    .line 86
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractStage;->o〇0OOo〇0()I

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    if-nez v0, :cond_6

    .line 91
    .line 92
    iget-object v0, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 93
    .line 94
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractStage;->o0ooO()Z

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    if-nez v0, :cond_6

    .line 99
    .line 100
    if-eqz v4, :cond_4

    .line 101
    .line 102
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇080:Landroid/graphics/Paint;

    .line 103
    .line 104
    invoke-virtual {v9, v4, v14, v14, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 105
    .line 106
    .line 107
    invoke-direct/range {p0 .. p1}, Lcom/samsung/sdraw/SelectMode;->〇oOO8O8(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 108
    .line 109
    .line 110
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 111
    .line 112
    if-nez v0, :cond_3

    .line 113
    .line 114
    return-void

    .line 115
    :cond_3
    invoke-interface {v0}, Lcom/samsung/sdraw/as;->a()Landroid/graphics/RectF;

    .line 116
    .line 117
    .line 118
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 119
    .line 120
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 121
    .line 122
    .line 123
    move-result-object v15

    .line 124
    move-object/from16 v0, p0

    .line 125
    .line 126
    move-object/from16 v1, p1

    .line 127
    .line 128
    move-object/from16 v2, p2

    .line 129
    .line 130
    move-object v3, v15

    .line 131
    move-object v4, v6

    .line 132
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/sdraw/SelectMode;->〇〇808〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/sdraw/PointF;F)V

    .line 133
    .line 134
    .line 135
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 136
    .line 137
    instance-of v0, v0, Lcom/samsung/sdraw/z;

    .line 138
    .line 139
    if-eqz v0, :cond_4

    .line 140
    .line 141
    const/4 v3, 0x0

    .line 142
    move-object/from16 v0, p0

    .line 143
    .line 144
    move-object/from16 v1, p1

    .line 145
    .line 146
    move-object/from16 v2, p2

    .line 147
    .line 148
    move-object v4, v15

    .line 149
    move-object v5, v6

    .line 150
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/sdraw/SelectMode;->Oooo8o0〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/RectF;Lcom/samsung/sdraw/PointF;)V

    .line 151
    .line 152
    .line 153
    iget v3, v15, Landroid/graphics/RectF;->top:F

    .line 154
    .line 155
    invoke-virtual {v15}, Landroid/graphics/RectF;->centerX()F

    .line 156
    .line 157
    .line 158
    move-result v4

    .line 159
    iget v5, v15, Landroid/graphics/RectF;->bottom:F

    .line 160
    .line 161
    invoke-direct/range {v0 .. v6}, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/Canvas;FFFLcom/samsung/sdraw/PointF;)V

    .line 162
    .line 163
    .line 164
    :cond_4
    if-eqz v11, :cond_5

    .line 165
    .line 166
    iget-object v0, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 167
    .line 168
    invoke-virtual {v0, v10}, Lcom/samsung/sdraw/AbstractStage;->o〇O(I)Z

    .line 169
    .line 170
    .line 171
    move-result v0

    .line 172
    if-eqz v0, :cond_5

    .line 173
    .line 174
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇080:Landroid/graphics/Paint;

    .line 175
    .line 176
    invoke-virtual {v9, v11, v14, v14, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 177
    .line 178
    .line 179
    :cond_5
    if-eqz v13, :cond_a

    .line 180
    .line 181
    iget-object v0, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 182
    .line 183
    invoke-virtual {v0, v12}, Lcom/samsung/sdraw/AbstractStage;->o〇O(I)Z

    .line 184
    .line 185
    .line 186
    move-result v0

    .line 187
    if-eqz v0, :cond_a

    .line 188
    .line 189
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇080:Landroid/graphics/Paint;

    .line 190
    .line 191
    invoke-virtual {v9, v13, v14, v14, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 192
    .line 193
    .line 194
    goto :goto_0

    .line 195
    :cond_6
    if-eqz v11, :cond_7

    .line 196
    .line 197
    iget-object v0, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 198
    .line 199
    invoke-virtual {v0, v10}, Lcom/samsung/sdraw/AbstractStage;->o〇O(I)Z

    .line 200
    .line 201
    .line 202
    move-result v0

    .line 203
    if-eqz v0, :cond_7

    .line 204
    .line 205
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇080:Landroid/graphics/Paint;

    .line 206
    .line 207
    invoke-virtual {v9, v11, v14, v14, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 208
    .line 209
    .line 210
    :cond_7
    if-eqz v13, :cond_8

    .line 211
    .line 212
    iget-object v0, v8, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 213
    .line 214
    invoke-virtual {v0, v12}, Lcom/samsung/sdraw/AbstractStage;->o〇O(I)Z

    .line 215
    .line 216
    .line 217
    move-result v0

    .line 218
    if-eqz v0, :cond_8

    .line 219
    .line 220
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇080:Landroid/graphics/Paint;

    .line 221
    .line 222
    invoke-virtual {v9, v13, v14, v14, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 223
    .line 224
    .line 225
    :cond_8
    if-eqz v4, :cond_a

    .line 226
    .line 227
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇080:Landroid/graphics/Paint;

    .line 228
    .line 229
    invoke-virtual {v9, v4, v14, v14, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 230
    .line 231
    .line 232
    invoke-direct/range {p0 .. p1}, Lcom/samsung/sdraw/SelectMode;->〇oOO8O8(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 233
    .line 234
    .line 235
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 236
    .line 237
    if-nez v0, :cond_9

    .line 238
    .line 239
    return-void

    .line 240
    :cond_9
    invoke-interface {v0}, Lcom/samsung/sdraw/as;->a()Landroid/graphics/RectF;

    .line 241
    .line 242
    .line 243
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 244
    .line 245
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 246
    .line 247
    .line 248
    move-result-object v10

    .line 249
    move-object/from16 v0, p0

    .line 250
    .line 251
    move-object/from16 v1, p1

    .line 252
    .line 253
    move-object/from16 v2, p2

    .line 254
    .line 255
    move-object v3, v10

    .line 256
    move-object v4, v6

    .line 257
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/sdraw/SelectMode;->〇〇808〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/sdraw/PointF;F)V

    .line 258
    .line 259
    .line 260
    iget-object v0, v7, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 261
    .line 262
    instance-of v0, v0, Lcom/samsung/sdraw/z;

    .line 263
    .line 264
    if-eqz v0, :cond_a

    .line 265
    .line 266
    const/4 v3, 0x0

    .line 267
    move-object/from16 v0, p0

    .line 268
    .line 269
    move-object/from16 v1, p1

    .line 270
    .line 271
    move-object/from16 v2, p2

    .line 272
    .line 273
    move-object v4, v10

    .line 274
    move-object v5, v6

    .line 275
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/sdraw/SelectMode;->Oooo8o0〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/RectF;Lcom/samsung/sdraw/PointF;)V

    .line 276
    .line 277
    .line 278
    iget v3, v10, Landroid/graphics/RectF;->top:F

    .line 279
    .line 280
    invoke-virtual {v10}, Landroid/graphics/RectF;->centerX()F

    .line 281
    .line 282
    .line 283
    move-result v4

    .line 284
    iget v5, v10, Landroid/graphics/RectF;->bottom:F

    .line 285
    .line 286
    invoke-direct/range {v0 .. v6}, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/Canvas;FFFLcom/samsung/sdraw/PointF;)V

    .line 287
    .line 288
    .line 289
    :cond_a
    :goto_0
    return-void
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
.end method

.method protected Oooo8o0〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/RectF;Lcom/samsung/sdraw/PointF;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p2

    .line 6
    .line 7
    move-object/from16 v3, p3

    .line 8
    .line 9
    move-object/from16 v4, p4

    .line 10
    .line 11
    new-instance v5, Lcom/samsung/sdraw/ag;

    .line 12
    .line 13
    iget-object v6, v1, Lcom/samsung/sdraw/AbstractModeContext;->〇8o8o〇:Landroid/view/View;

    .line 14
    .line 15
    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 16
    .line 17
    .line 18
    move-result-object v6

    .line 19
    iget-object v7, v1, Lcom/samsung/sdraw/AbstractModeContext;->〇8o8o〇:Landroid/view/View;

    .line 20
    .line 21
    check-cast v7, Lcom/samsung/sdraw/CanvasView;

    .line 22
    .line 23
    iget-object v7, v7, Lcom/samsung/sdraw/CanvasView;->oOo〇08〇:Ljava/lang/String;

    .line 24
    .line 25
    invoke-direct {v5, v6, v7}, Lcom/samsung/sdraw/ag;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iget-object v6, v0, Lcom/samsung/sdraw/SelectMode;->oO80:Landroid/graphics/Bitmap;

    .line 29
    .line 30
    const-string v7, "/camera_crop_holo_hdpi.png"

    .line 31
    .line 32
    if-nez v6, :cond_0

    .line 33
    .line 34
    invoke-virtual {v5, v7}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 35
    .line 36
    .line 37
    move-result-object v6

    .line 38
    iput-object v6, v0, Lcom/samsung/sdraw/SelectMode;->oO80:Landroid/graphics/Bitmap;

    .line 39
    .line 40
    :cond_0
    iget-object v6, v0, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O:Landroid/graphics/Bitmap;

    .line 41
    .line 42
    if-nez v6, :cond_1

    .line 43
    .line 44
    invoke-virtual {v5, v7}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 45
    .line 46
    .line 47
    move-result-object v6

    .line 48
    iput-object v6, v0, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O:Landroid/graphics/Bitmap;

    .line 49
    .line 50
    :cond_1
    iget-object v6, v0, Lcom/samsung/sdraw/SelectMode;->o〇0:Landroid/graphics/Bitmap;

    .line 51
    .line 52
    if-nez v6, :cond_2

    .line 53
    .line 54
    invoke-virtual {v5, v7}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 55
    .line 56
    .line 57
    move-result-object v6

    .line 58
    iput-object v6, v0, Lcom/samsung/sdraw/SelectMode;->o〇0:Landroid/graphics/Bitmap;

    .line 59
    .line 60
    :cond_2
    iget-object v6, v0, Lcom/samsung/sdraw/SelectMode;->〇〇888:Landroid/graphics/Bitmap;

    .line 61
    .line 62
    if-nez v6, :cond_3

    .line 63
    .line 64
    invoke-virtual {v5, v7}, Lcom/samsung/sdraw/ce;->〇80〇808〇O(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 65
    .line 66
    .line 67
    move-result-object v5

    .line 68
    iput-object v5, v0, Lcom/samsung/sdraw/SelectMode;->〇〇888:Landroid/graphics/Bitmap;

    .line 69
    .line 70
    :cond_3
    iget-object v5, v0, Lcom/samsung/sdraw/SelectMode;->oO80:Landroid/graphics/Bitmap;

    .line 71
    .line 72
    if-eqz v5, :cond_10

    .line 73
    .line 74
    iget-object v5, v0, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O:Landroid/graphics/Bitmap;

    .line 75
    .line 76
    if-eqz v5, :cond_10

    .line 77
    .line 78
    iget-object v5, v0, Lcom/samsung/sdraw/SelectMode;->o〇0:Landroid/graphics/Bitmap;

    .line 79
    .line 80
    if-eqz v5, :cond_10

    .line 81
    .line 82
    iget-object v5, v0, Lcom/samsung/sdraw/SelectMode;->〇〇888:Landroid/graphics/Bitmap;

    .line 83
    .line 84
    if-eqz v5, :cond_10

    .line 85
    .line 86
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Canvas;->save()I

    .line 87
    .line 88
    .line 89
    new-instance v5, Landroid/graphics/Matrix;

    .line 90
    .line 91
    iget-object v1, v1, Lcom/samsung/sdraw/AbstractModeContext;->〇O8o08O:Landroid/graphics/Matrix;

    .line 92
    .line 93
    invoke-direct {v5, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 94
    .line 95
    .line 96
    const/16 v1, 0x9

    .line 97
    .line 98
    new-array v6, v1, [F

    .line 99
    .line 100
    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->getValues([F)V

    .line 101
    .line 102
    .line 103
    const/4 v5, 0x0

    .line 104
    aget v7, v6, v5

    .line 105
    .line 106
    const/high16 v8, 0x3f800000    # 1.0f

    .line 107
    .line 108
    cmpl-float v9, v7, v8

    .line 109
    .line 110
    if-lez v9, :cond_4

    .line 111
    .line 112
    goto :goto_0

    .line 113
    :cond_4
    const/high16 v7, 0x3f800000    # 1.0f

    .line 114
    .line 115
    :goto_0
    invoke-static {}, Lcom/samsung/sdraw/SelectMode$HitDirection;->values()[Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 116
    .line 117
    .line 118
    move-result-object v9

    .line 119
    iget-object v10, v0, Lcom/samsung/sdraw/SelectMode;->o〇0:Landroid/graphics/Bitmap;

    .line 120
    .line 121
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    .line 122
    .line 123
    .line 124
    move-result v10

    .line 125
    const/4 v11, 0x2

    .line 126
    div-int/2addr v10, v11

    .line 127
    new-instance v12, Landroid/graphics/Rect;

    .line 128
    .line 129
    iget v13, v4, Landroid/graphics/RectF;->left:F

    .line 130
    .line 131
    mul-float v13, v13, v7

    .line 132
    .line 133
    float-to-int v13, v13

    .line 134
    iget v14, v4, Landroid/graphics/RectF;->top:F

    .line 135
    .line 136
    mul-float v14, v14, v7

    .line 137
    .line 138
    float-to-int v14, v14

    .line 139
    iget v15, v4, Landroid/graphics/RectF;->right:F

    .line 140
    .line 141
    mul-float v15, v15, v7

    .line 142
    .line 143
    float-to-int v15, v15

    .line 144
    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    .line 145
    .line 146
    mul-float v4, v4, v7

    .line 147
    .line 148
    float-to-int v4, v4

    .line 149
    invoke-direct {v12, v13, v14, v15, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 150
    .line 151
    .line 152
    iget-object v4, v0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 153
    .line 154
    instance-of v4, v4, Lcom/samsung/sdraw/z;

    .line 155
    .line 156
    if-eqz v4, :cond_f

    .line 157
    .line 158
    const/4 v4, 0x1

    .line 159
    const/4 v13, 0x1

    .line 160
    :goto_1
    if-lt v13, v1, :cond_5

    .line 161
    .line 162
    goto/16 :goto_3

    .line 163
    .line 164
    :cond_5
    aget-object v14, v9, v13

    .line 165
    .line 166
    invoke-virtual {v0, v14, v10, v12}, Lcom/samsung/sdraw/SelectMode;->oO80(Lcom/samsung/sdraw/SelectMode$HitDirection;ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 167
    .line 168
    .line 169
    move-result-object v14

    .line 170
    new-instance v15, Landroid/graphics/Matrix;

    .line 171
    .line 172
    invoke-direct {v15}, Landroid/graphics/Matrix;-><init>()V

    .line 173
    .line 174
    .line 175
    aget v16, v6, v5

    .line 176
    .line 177
    cmpl-float v16, v16, v8

    .line 178
    .line 179
    if-lez v16, :cond_6

    .line 180
    .line 181
    div-float v1, v8, v7

    .line 182
    .line 183
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    .line 184
    .line 185
    .line 186
    move-result v5

    .line 187
    int-to-float v5, v5

    .line 188
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    .line 189
    .line 190
    .line 191
    move-result v8

    .line 192
    int-to-float v8, v8

    .line 193
    invoke-virtual {v15, v1, v1, v5, v8}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 194
    .line 195
    .line 196
    :cond_6
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 197
    .line 198
    check-cast v1, Lcom/samsung/sdraw/z;

    .line 199
    .line 200
    iget v1, v1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 201
    .line 202
    int-to-float v1, v1

    .line 203
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerX()I

    .line 204
    .line 205
    .line 206
    move-result v5

    .line 207
    int-to-float v5, v5

    .line 208
    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    .line 209
    .line 210
    .line 211
    move-result v8

    .line 212
    int-to-float v8, v8

    .line 213
    invoke-virtual {v15, v1, v5, v8}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    .line 214
    .line 215
    .line 216
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Canvas;->save()I

    .line 217
    .line 218
    .line 219
    invoke-virtual {v2, v15}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 220
    .line 221
    .line 222
    if-ne v13, v4, :cond_7

    .line 223
    .line 224
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->o〇0:Landroid/graphics/Bitmap;

    .line 225
    .line 226
    iget v5, v14, Landroid/graphics/Rect;->left:I

    .line 227
    .line 228
    int-to-float v5, v5

    .line 229
    iget v8, v14, Landroid/graphics/Rect;->top:I

    .line 230
    .line 231
    int-to-float v8, v8

    .line 232
    invoke-virtual {v2, v1, v5, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 233
    .line 234
    .line 235
    goto :goto_2

    .line 236
    :cond_7
    if-ne v13, v11, :cond_8

    .line 237
    .line 238
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇〇888:Landroid/graphics/Bitmap;

    .line 239
    .line 240
    iget v5, v14, Landroid/graphics/Rect;->left:I

    .line 241
    .line 242
    int-to-float v5, v5

    .line 243
    iget v8, v14, Landroid/graphics/Rect;->top:I

    .line 244
    .line 245
    int-to-float v8, v8

    .line 246
    invoke-virtual {v2, v1, v5, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 247
    .line 248
    .line 249
    goto :goto_2

    .line 250
    :cond_8
    const/4 v1, 0x3

    .line 251
    if-ne v13, v1, :cond_9

    .line 252
    .line 253
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->o〇0:Landroid/graphics/Bitmap;

    .line 254
    .line 255
    iget v5, v14, Landroid/graphics/Rect;->left:I

    .line 256
    .line 257
    int-to-float v5, v5

    .line 258
    iget v8, v14, Landroid/graphics/Rect;->top:I

    .line 259
    .line 260
    int-to-float v8, v8

    .line 261
    invoke-virtual {v2, v1, v5, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 262
    .line 263
    .line 264
    goto :goto_2

    .line 265
    :cond_9
    const/4 v1, 0x4

    .line 266
    if-ne v13, v1, :cond_a

    .line 267
    .line 268
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇〇888:Landroid/graphics/Bitmap;

    .line 269
    .line 270
    iget v5, v14, Landroid/graphics/Rect;->left:I

    .line 271
    .line 272
    int-to-float v5, v5

    .line 273
    iget v8, v14, Landroid/graphics/Rect;->top:I

    .line 274
    .line 275
    int-to-float v8, v8

    .line 276
    invoke-virtual {v2, v1, v5, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 277
    .line 278
    .line 279
    goto :goto_2

    .line 280
    :cond_a
    const/4 v1, 0x5

    .line 281
    if-ne v13, v1, :cond_b

    .line 282
    .line 283
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O:Landroid/graphics/Bitmap;

    .line 284
    .line 285
    iget v5, v14, Landroid/graphics/Rect;->left:I

    .line 286
    .line 287
    int-to-float v5, v5

    .line 288
    iget v8, v14, Landroid/graphics/Rect;->top:I

    .line 289
    .line 290
    int-to-float v8, v8

    .line 291
    invoke-virtual {v2, v1, v5, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 292
    .line 293
    .line 294
    goto :goto_2

    .line 295
    :cond_b
    const/4 v1, 0x6

    .line 296
    if-ne v13, v1, :cond_c

    .line 297
    .line 298
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->oO80:Landroid/graphics/Bitmap;

    .line 299
    .line 300
    iget v5, v14, Landroid/graphics/Rect;->left:I

    .line 301
    .line 302
    int-to-float v5, v5

    .line 303
    iget v8, v14, Landroid/graphics/Rect;->top:I

    .line 304
    .line 305
    int-to-float v8, v8

    .line 306
    invoke-virtual {v2, v1, v5, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 307
    .line 308
    .line 309
    goto :goto_2

    .line 310
    :cond_c
    const/4 v1, 0x7

    .line 311
    if-ne v13, v1, :cond_d

    .line 312
    .line 313
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->oO80:Landroid/graphics/Bitmap;

    .line 314
    .line 315
    iget v5, v14, Landroid/graphics/Rect;->left:I

    .line 316
    .line 317
    int-to-float v5, v5

    .line 318
    iget v8, v14, Landroid/graphics/Rect;->top:I

    .line 319
    .line 320
    int-to-float v8, v8

    .line 321
    invoke-virtual {v2, v1, v5, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 322
    .line 323
    .line 324
    goto :goto_2

    .line 325
    :cond_d
    const/16 v1, 0x8

    .line 326
    .line 327
    if-ne v13, v1, :cond_e

    .line 328
    .line 329
    iget-object v1, v0, Lcom/samsung/sdraw/SelectMode;->〇80〇808〇O:Landroid/graphics/Bitmap;

    .line 330
    .line 331
    iget v5, v14, Landroid/graphics/Rect;->left:I

    .line 332
    .line 333
    int-to-float v5, v5

    .line 334
    iget v8, v14, Landroid/graphics/Rect;->top:I

    .line 335
    .line 336
    int-to-float v8, v8

    .line 337
    invoke-virtual {v2, v1, v5, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 338
    .line 339
    .line 340
    :cond_e
    :goto_2
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Canvas;->restore()V

    .line 341
    .line 342
    .line 343
    add-int/lit8 v13, v13, 0x1

    .line 344
    .line 345
    const/16 v1, 0x9

    .line 346
    .line 347
    const/4 v5, 0x0

    .line 348
    const/high16 v8, 0x3f800000    # 1.0f

    .line 349
    .line 350
    goto/16 :goto_1

    .line 351
    .line 352
    :cond_f
    :goto_3
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Canvas;->restore()V

    .line 353
    .line 354
    .line 355
    :cond_10
    return-void
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
.end method

.method protected oO80(Lcom/samsung/sdraw/SelectMode$HitDirection;ILandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-direct {v0, p3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 4
    .line 5
    .line 6
    iget-object p3, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 7
    .line 8
    check-cast p3, Lcom/samsung/sdraw/z;

    .line 9
    .line 10
    new-instance v1, Landroid/graphics/Matrix;

    .line 11
    .line 12
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 13
    .line 14
    .line 15
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 16
    .line 17
    move-object v3, v2

    .line 18
    check-cast v3, Lcom/samsung/sdraw/z;

    .line 19
    .line 20
    iget v3, v3, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 21
    .line 22
    int-to-float v3, v3

    .line 23
    invoke-virtual {v2}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    iget-object v4, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 32
    .line 33
    invoke-virtual {v4}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    .line 38
    .line 39
    .line 40
    move-result v4

    .line 41
    invoke-virtual {v1, v3, v2, v4}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    .line 42
    .line 43
    .line 44
    invoke-static {}, Lcom/samsung/sdraw/SelectMode;->〇00()[I

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    aget p1, v1, p1

    .line 53
    .line 54
    const/4 v1, 0x3

    .line 55
    const/4 v2, 0x1

    .line 56
    const/4 v3, 0x2

    .line 57
    const/4 v4, 0x0

    .line 58
    packed-switch p1, :pswitch_data_0

    .line 59
    .line 60
    .line 61
    goto/16 :goto_0

    .line 62
    .line 63
    :pswitch_0
    new-instance v0, Landroid/graphics/Rect;

    .line 64
    .line 65
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o8:Landroid/graphics/Rect;

    .line 66
    .line 67
    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 68
    .line 69
    .line 70
    goto/16 :goto_0

    .line 71
    .line 72
    :pswitch_1
    new-instance v0, Landroid/graphics/Rect;

    .line 73
    .line 74
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇8:Landroid/graphics/Rect;

    .line 75
    .line 76
    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 77
    .line 78
    .line 79
    goto/16 :goto_0

    .line 80
    .line 81
    :pswitch_2
    new-instance v0, Landroid/graphics/Rect;

    .line 82
    .line 83
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->o0ooO:Landroid/graphics/Rect;

    .line 84
    .line 85
    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 86
    .line 87
    .line 88
    goto/16 :goto_0

    .line 89
    .line 90
    :pswitch_3
    new-instance v0, Landroid/graphics/Rect;

    .line 91
    .line 92
    invoke-virtual {p3, v1}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 97
    .line 98
    float-to-int p1, p1

    .line 99
    sub-int/2addr p1, p2

    .line 100
    invoke-virtual {p3, v1}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 101
    .line 102
    .line 103
    move-result-object v2

    .line 104
    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 105
    .line 106
    float-to-int v2, v2

    .line 107
    sub-int/2addr v2, p2

    .line 108
    invoke-virtual {p3, v1}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 109
    .line 110
    .line 111
    move-result-object v3

    .line 112
    iget v3, v3, Landroid/graphics/PointF;->x:F

    .line 113
    .line 114
    float-to-int v3, v3

    .line 115
    add-int/2addr v3, p2

    .line 116
    invoke-virtual {p3, v1}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 117
    .line 118
    .line 119
    move-result-object p3

    .line 120
    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 121
    .line 122
    float-to-int p3, p3

    .line 123
    add-int/2addr p3, p2

    .line 124
    invoke-direct {v0, p1, v2, v3, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 125
    .line 126
    .line 127
    goto/16 :goto_0

    .line 128
    .line 129
    :pswitch_4
    new-instance v0, Landroid/graphics/Rect;

    .line 130
    .line 131
    invoke-virtual {p3, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 132
    .line 133
    .line 134
    move-result-object p1

    .line 135
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 136
    .line 137
    float-to-int p1, p1

    .line 138
    sub-int/2addr p1, p2

    .line 139
    invoke-virtual {p3, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 140
    .line 141
    .line 142
    move-result-object v1

    .line 143
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 144
    .line 145
    float-to-int v1, v1

    .line 146
    sub-int/2addr v1, p2

    .line 147
    invoke-virtual {p3, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 148
    .line 149
    .line 150
    move-result-object v2

    .line 151
    iget v2, v2, Landroid/graphics/PointF;->x:F

    .line 152
    .line 153
    float-to-int v2, v2

    .line 154
    add-int/2addr v2, p2

    .line 155
    invoke-virtual {p3, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 156
    .line 157
    .line 158
    move-result-object p3

    .line 159
    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 160
    .line 161
    float-to-int p3, p3

    .line 162
    add-int/2addr p3, p2

    .line 163
    invoke-direct {v0, p1, v1, v2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 164
    .line 165
    .line 166
    goto/16 :goto_0

    .line 167
    .line 168
    :pswitch_5
    new-instance v0, Landroid/graphics/Rect;

    .line 169
    .line 170
    invoke-virtual {p3, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 171
    .line 172
    .line 173
    move-result-object p1

    .line 174
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 175
    .line 176
    float-to-int p1, p1

    .line 177
    sub-int/2addr p1, p2

    .line 178
    invoke-virtual {p3, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 179
    .line 180
    .line 181
    move-result-object v1

    .line 182
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 183
    .line 184
    float-to-int v1, v1

    .line 185
    sub-int/2addr v1, p2

    .line 186
    invoke-virtual {p3, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 187
    .line 188
    .line 189
    move-result-object v3

    .line 190
    iget v3, v3, Landroid/graphics/PointF;->x:F

    .line 191
    .line 192
    float-to-int v3, v3

    .line 193
    add-int/2addr v3, p2

    .line 194
    invoke-virtual {p3, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 195
    .line 196
    .line 197
    move-result-object p3

    .line 198
    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 199
    .line 200
    float-to-int p3, p3

    .line 201
    add-int/2addr p3, p2

    .line 202
    invoke-direct {v0, p1, v1, v3, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 203
    .line 204
    .line 205
    goto/16 :goto_0

    .line 206
    .line 207
    :pswitch_6
    new-instance v0, Landroid/graphics/Rect;

    .line 208
    .line 209
    invoke-virtual {p3, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 210
    .line 211
    .line 212
    move-result-object p1

    .line 213
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 214
    .line 215
    float-to-int p1, p1

    .line 216
    sub-int/2addr p1, p2

    .line 217
    invoke-virtual {p3, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 218
    .line 219
    .line 220
    move-result-object v1

    .line 221
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 222
    .line 223
    float-to-int v1, v1

    .line 224
    sub-int/2addr v1, p2

    .line 225
    invoke-virtual {p3, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 226
    .line 227
    .line 228
    move-result-object v2

    .line 229
    iget v2, v2, Landroid/graphics/PointF;->x:F

    .line 230
    .line 231
    float-to-int v2, v2

    .line 232
    add-int/2addr v2, p2

    .line 233
    invoke-virtual {p3, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 234
    .line 235
    .line 236
    move-result-object p3

    .line 237
    iget p3, p3, Landroid/graphics/PointF;->y:F

    .line 238
    .line 239
    float-to-int p3, p3

    .line 240
    add-int/2addr p3, p2

    .line 241
    invoke-direct {v0, p1, v1, v2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 242
    .line 243
    .line 244
    goto/16 :goto_0

    .line 245
    .line 246
    :pswitch_7
    invoke-virtual {p3, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 247
    .line 248
    .line 249
    move-result-object p1

    .line 250
    invoke-virtual {p3, v1}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 251
    .line 252
    .line 253
    move-result-object p3

    .line 254
    invoke-direct {p0, p1, p3}, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇〇〇0(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 255
    .line 256
    .line 257
    move-result-object p1

    .line 258
    new-instance v0, Landroid/graphics/Rect;

    .line 259
    .line 260
    iget p3, p1, Landroid/graphics/PointF;->x:F

    .line 261
    .line 262
    float-to-int v1, p3

    .line 263
    sub-int/2addr v1, p2

    .line 264
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 265
    .line 266
    float-to-int v2, p1

    .line 267
    sub-int/2addr v2, p2

    .line 268
    float-to-int p3, p3

    .line 269
    add-int/2addr p3, p2

    .line 270
    float-to-int p1, p1

    .line 271
    add-int/2addr p1, p2

    .line 272
    invoke-direct {v0, v1, v2, p3, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 273
    .line 274
    .line 275
    goto :goto_0

    .line 276
    :pswitch_8
    invoke-virtual {p3, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 277
    .line 278
    .line 279
    move-result-object p1

    .line 280
    invoke-virtual {p3, v1}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 281
    .line 282
    .line 283
    move-result-object p3

    .line 284
    invoke-direct {p0, p1, p3}, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇〇〇0(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 285
    .line 286
    .line 287
    move-result-object p1

    .line 288
    new-instance v0, Landroid/graphics/Rect;

    .line 289
    .line 290
    iget p3, p1, Landroid/graphics/PointF;->x:F

    .line 291
    .line 292
    float-to-int v1, p3

    .line 293
    sub-int/2addr v1, p2

    .line 294
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 295
    .line 296
    float-to-int v2, p1

    .line 297
    sub-int/2addr v2, p2

    .line 298
    float-to-int p3, p3

    .line 299
    add-int/2addr p3, p2

    .line 300
    float-to-int p1, p1

    .line 301
    add-int/2addr p1, p2

    .line 302
    invoke-direct {v0, v1, v2, p3, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 303
    .line 304
    .line 305
    goto :goto_0

    .line 306
    :pswitch_9
    invoke-virtual {p3, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 307
    .line 308
    .line 309
    move-result-object p1

    .line 310
    invoke-virtual {p3, v2}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 311
    .line 312
    .line 313
    move-result-object p3

    .line 314
    invoke-direct {p0, p1, p3}, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇〇〇0(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 315
    .line 316
    .line 317
    move-result-object p1

    .line 318
    new-instance v0, Landroid/graphics/Rect;

    .line 319
    .line 320
    iget p3, p1, Landroid/graphics/PointF;->x:F

    .line 321
    .line 322
    float-to-int v1, p3

    .line 323
    sub-int/2addr v1, p2

    .line 324
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 325
    .line 326
    float-to-int v2, p1

    .line 327
    sub-int/2addr v2, p2

    .line 328
    float-to-int p3, p3

    .line 329
    add-int/2addr p3, p2

    .line 330
    float-to-int p1, p1

    .line 331
    add-int/2addr p1, p2

    .line 332
    invoke-direct {v0, v1, v2, p3, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 333
    .line 334
    .line 335
    goto :goto_0

    .line 336
    :pswitch_a
    invoke-virtual {p3, v4}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 337
    .line 338
    .line 339
    move-result-object p1

    .line 340
    invoke-virtual {p3, v3}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 341
    .line 342
    .line 343
    move-result-object p3

    .line 344
    invoke-direct {p0, p1, p3}, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇〇〇0(Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 345
    .line 346
    .line 347
    move-result-object p1

    .line 348
    new-instance v0, Landroid/graphics/Rect;

    .line 349
    .line 350
    iget p3, p1, Landroid/graphics/PointF;->x:F

    .line 351
    .line 352
    float-to-int v1, p3

    .line 353
    sub-int/2addr v1, p2

    .line 354
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 355
    .line 356
    float-to-int v2, p1

    .line 357
    sub-int/2addr v2, p2

    .line 358
    float-to-int p3, p3

    .line 359
    add-int/2addr p3, p2

    .line 360
    float-to-int p1, p1

    .line 361
    add-int/2addr p1, p2

    .line 362
    invoke-direct {v0, v1, v2, p3, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 363
    .line 364
    .line 365
    :goto_0
    return-object v0

    .line 366
    nop

    .line 367
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
.end method

.method public oo〇(Lcom/samsung/sdraw/AbstractModeContext;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public o〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/samsung/sdraw/SelectMode;->〇00:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇00〇8(Lcom/samsung/sdraw/AbstractModeContext;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->O〇8O8〇008:Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    check-cast p1, Lcom/samsung/sdraw/z;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/samsung/sdraw/z;->o〇O8〇〇o()Landroid/graphics/Bitmap;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O〇8O8〇008:Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;

    .line 18
    .line 19
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 20
    .line 21
    check-cast v0, Lcom/samsung/sdraw/z;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/samsung/sdraw/z;->〇oOO8O8()Lcom/samsung/sdraw/ImageInfo;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const/4 v1, 0x0

    .line 28
    invoke-interface {p1, v0, v1}, Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;->〇080(Lcom/samsung/sdraw/ObjectInfo;Z)V

    .line 29
    .line 30
    .line 31
    :cond_0
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractSprite;->〇o00〇〇Oo()V

    .line 34
    .line 35
    .line 36
    :cond_1
    const/4 p1, 0x0

    .line 37
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public 〇080(Lcom/samsung/sdraw/AbstractModeContext;Landroid/view/MotionEvent;)Z
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v2, Lcom/samsung/sdraw/PointF;

    .line 12
    .line 13
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 18
    .line 19
    .line 20
    move-result v4

    .line 21
    invoke-direct {v2, v3, v4}, Lcom/samsung/sdraw/PointF;-><init>(FF)V

    .line 22
    .line 23
    .line 24
    iget-object v3, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 25
    .line 26
    invoke-virtual {v3, v2}, Lcom/samsung/sdraw/AbstractStage;->Ooo(Lcom/samsung/sdraw/PointF;)Lcom/samsung/sdraw/PointF;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 35
    .line 36
    .line 37
    move-result v5

    .line 38
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 39
    .line 40
    .line 41
    move-result v6

    .line 42
    const/4 v7, 0x5

    .line 43
    const/4 v8, 0x1

    .line 44
    if-ne v6, v7, :cond_1

    .line 45
    .line 46
    if-le v5, v8, :cond_1

    .line 47
    .line 48
    iget-object v6, p0, Lcom/samsung/sdraw/SelectMode;->O8:[Lcom/samsung/sdraw/PointF;

    .line 49
    .line 50
    aget-object v6, v6, v8

    .line 51
    .line 52
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 53
    .line 54
    .line 55
    move-result v7

    .line 56
    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 57
    .line 58
    iget-object v6, p0, Lcom/samsung/sdraw/SelectMode;->O8:[Lcom/samsung/sdraw/PointF;

    .line 59
    .line 60
    aget-object v6, v6, v8

    .line 61
    .line 62
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 63
    .line 64
    .line 65
    move-result v7

    .line 66
    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 67
    .line 68
    :cond_1
    const/16 v6, 0x19

    .line 69
    .line 70
    if-eqz v4, :cond_14

    .line 71
    .line 72
    if-eq v4, v8, :cond_b

    .line 73
    .line 74
    const/4 v2, 0x2

    .line 75
    if-eq v4, v2, :cond_4

    .line 76
    .line 77
    const/4 v1, 0x3

    .line 78
    if-eq v4, v1, :cond_3

    .line 79
    .line 80
    const/16 v1, 0x105

    .line 81
    .line 82
    if-eq v4, v1, :cond_2

    .line 83
    .line 84
    goto/16 :goto_6

    .line 85
    .line 86
    :cond_2
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/sdraw/SelectMode;->〇〇8O0〇8(Lcom/samsung/sdraw/AbstractModeContext;Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 87
    .line 88
    .line 89
    goto/16 :goto_6

    .line 90
    .line 91
    :cond_3
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 92
    .line 93
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 94
    .line 95
    .line 96
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 97
    .line 98
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 99
    .line 100
    .line 101
    goto/16 :goto_6

    .line 102
    .line 103
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/sdraw/SelectMode;->Oo8Oo00oo:Z

    .line 104
    .line 105
    if-eqz v0, :cond_7

    .line 106
    .line 107
    iget-boolean v0, p0, Lcom/samsung/sdraw/SelectMode;->〇〇〇0〇〇0:Z

    .line 108
    .line 109
    if-eqz v0, :cond_5

    .line 110
    .line 111
    invoke-virtual {p0, v3, v6}, Lcom/samsung/sdraw/SelectMode;->〇8o8o〇(Lcom/samsung/sdraw/PointF;I)Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 112
    .line 113
    .line 114
    move-result-object v0

    .line 115
    iput-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇8o8o〇:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 116
    .line 117
    sget-object v2, Lcom/samsung/sdraw/SelectMode$HitDirection;->DELETE:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 118
    .line 119
    if-eq v0, v2, :cond_7

    .line 120
    .line 121
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇〇0〇〇0:Z

    .line 122
    .line 123
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 124
    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_5
    iget-boolean v0, p0, Lcom/samsung/sdraw/SelectMode;->o〇0OOo〇0:Z

    .line 128
    .line 129
    if-eqz v0, :cond_6

    .line 130
    .line 131
    invoke-virtual {p0, v3, v6}, Lcom/samsung/sdraw/SelectMode;->〇8o8o〇(Lcom/samsung/sdraw/PointF;I)Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    iput-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇8o8o〇:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 136
    .line 137
    sget-object v2, Lcom/samsung/sdraw/SelectMode$HitDirection;->ROTATE_LEFT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 138
    .line 139
    if-eq v0, v2, :cond_7

    .line 140
    .line 141
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->o〇0OOo〇0:Z

    .line 142
    .line 143
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 144
    .line 145
    .line 146
    goto :goto_0

    .line 147
    :cond_6
    iget-boolean v0, p0, Lcom/samsung/sdraw/SelectMode;->〇〇0o:Z

    .line 148
    .line 149
    if-eqz v0, :cond_7

    .line 150
    .line 151
    invoke-virtual {p0, v3, v6}, Lcom/samsung/sdraw/SelectMode;->〇8o8o〇(Lcom/samsung/sdraw/PointF;I)Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    iput-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇8o8o〇:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 156
    .line 157
    sget-object v2, Lcom/samsung/sdraw/SelectMode$HitDirection;->ROTATE_RIGHT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 158
    .line 159
    if-eq v0, v2, :cond_7

    .line 160
    .line 161
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 162
    .line 163
    .line 164
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇0o:Z

    .line 165
    .line 166
    :cond_7
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/sdraw/SelectMode;->〇o〇:Z

    .line 167
    .line 168
    if-eqz v0, :cond_1d

    .line 169
    .line 170
    if-le v5, v8, :cond_8

    .line 171
    .line 172
    invoke-direct {p0, p1, p2}, Lcom/samsung/sdraw/SelectMode;->〇O00(Lcom/samsung/sdraw/AbstractModeContext;Landroid/view/MotionEvent;)V

    .line 173
    .line 174
    .line 175
    goto/16 :goto_6

    .line 176
    .line 177
    :cond_8
    iget-boolean p2, p0, Lcom/samsung/sdraw/SelectMode;->〇〇808〇:Z

    .line 178
    .line 179
    if-eqz p2, :cond_9

    .line 180
    .line 181
    return v8

    .line 182
    :cond_9
    iget-boolean p2, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇〇〇0:Z

    .line 183
    .line 184
    if-eqz p2, :cond_a

    .line 185
    .line 186
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 187
    .line 188
    check-cast p2, Lcom/samsung/sdraw/z;

    .line 189
    .line 190
    invoke-virtual {p2}, Lcom/samsung/sdraw/z;->O8〇o()[Lcom/samsung/sdraw/PointF;

    .line 191
    .line 192
    .line 193
    move-result-object p2

    .line 194
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇〇888:Lcom/samsung/sdraw/Setting;

    .line 195
    .line 196
    invoke-virtual {v0}, Lcom/samsung/sdraw/Setting;->〇〇8O0〇8()I

    .line 197
    .line 198
    .line 199
    move-result v0

    .line 200
    iput v0, p0, Lcom/samsung/sdraw/SelectMode;->〇O8o08O:I

    .line 201
    .line 202
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇〇888:Lcom/samsung/sdraw/Setting;

    .line 203
    .line 204
    invoke-virtual {v0}, Lcom/samsung/sdraw/Setting;->〇〇8O0〇8()I

    .line 205
    .line 206
    .line 207
    move-result v0

    .line 208
    iput v0, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇:I

    .line 209
    .line 210
    invoke-direct {p0, p2, v3}, Lcom/samsung/sdraw/SelectMode;->oo88o8O([Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;)V

    .line 211
    .line 212
    .line 213
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 214
    .line 215
    .line 216
    goto/16 :goto_6

    .line 217
    .line 218
    :cond_a
    iput-boolean v8, p0, Lcom/samsung/sdraw/SelectMode;->Oooo8o0〇:Z

    .line 219
    .line 220
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->Oo08:Lcom/samsung/sdraw/PointF;

    .line 221
    .line 222
    iget v0, v3, Landroid/graphics/PointF;->x:F

    .line 223
    .line 224
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->O8:[Lcom/samsung/sdraw/PointF;

    .line 225
    .line 226
    aget-object v2, v2, v1

    .line 227
    .line 228
    iget v4, v2, Landroid/graphics/PointF;->x:F

    .line 229
    .line 230
    sub-float/2addr v0, v4

    .line 231
    iput v0, p2, Landroid/graphics/PointF;->x:F

    .line 232
    .line 233
    iget v4, v3, Landroid/graphics/PointF;->y:F

    .line 234
    .line 235
    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 236
    .line 237
    sub-float/2addr v4, v2

    .line 238
    iput v4, p2, Landroid/graphics/PointF;->y:F

    .line 239
    .line 240
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractModeContext;->Oooo8o0〇()F

    .line 241
    .line 242
    .line 243
    move-result v2

    .line 244
    div-float/2addr v0, v2

    .line 245
    iput v0, p2, Landroid/graphics/PointF;->x:F

    .line 246
    .line 247
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->Oo08:Lcom/samsung/sdraw/PointF;

    .line 248
    .line 249
    iget v0, p2, Landroid/graphics/PointF;->y:F

    .line 250
    .line 251
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractModeContext;->Oooo8o0〇()F

    .line 252
    .line 253
    .line 254
    move-result v2

    .line 255
    div-float/2addr v0, v2

    .line 256
    iput v0, p2, Landroid/graphics/PointF;->y:F

    .line 257
    .line 258
    invoke-direct {p0, p1}, Lcom/samsung/sdraw/SelectMode;->OOO〇O0(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 259
    .line 260
    .line 261
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 262
    .line 263
    check-cast p2, Lcom/samsung/sdraw/z;

    .line 264
    .line 265
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->Oo08:Lcom/samsung/sdraw/PointF;

    .line 266
    .line 267
    iget v2, v0, Landroid/graphics/PointF;->x:F

    .line 268
    .line 269
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 270
    .line 271
    invoke-virtual {p2, v2, v0}, Lcom/samsung/sdraw/z;->〇〇8O0〇8(FF)V

    .line 272
    .line 273
    .line 274
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 275
    .line 276
    .line 277
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O8:[Lcom/samsung/sdraw/PointF;

    .line 278
    .line 279
    aget-object p1, p1, v1

    .line 280
    .line 281
    iget p2, v3, Landroid/graphics/PointF;->x:F

    .line 282
    .line 283
    iput p2, p1, Landroid/graphics/PointF;->x:F

    .line 284
    .line 285
    iget p2, v3, Landroid/graphics/PointF;->y:F

    .line 286
    .line 287
    iput p2, p1, Landroid/graphics/PointF;->y:F

    .line 288
    .line 289
    goto/16 :goto_6

    .line 290
    .line 291
    :cond_b
    iget-boolean p2, p0, Lcom/samsung/sdraw/SelectMode;->〇〇808〇:Z

    .line 292
    .line 293
    if-eqz p2, :cond_d

    .line 294
    .line 295
    invoke-direct {p0, p1, v0, v8}, Lcom/samsung/sdraw/SelectMode;->〇O〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/RectF;Z)V

    .line 296
    .line 297
    .line 298
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇O〇:Ljava/util/ArrayList;

    .line 299
    .line 300
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 301
    .line 302
    .line 303
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇O00:Ljava/util/ArrayList;

    .line 304
    .line 305
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 306
    .line 307
    .line 308
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇808〇:Z

    .line 309
    .line 310
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇080:Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;

    .line 311
    .line 312
    if-eqz p2, :cond_c

    .line 313
    .line 314
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 315
    .line 316
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractStage;->o8oO〇()Z

    .line 317
    .line 318
    .line 319
    move-result v0

    .line 320
    iget-object p1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 321
    .line 322
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractStage;->O〇O〇oO()Z

    .line 323
    .line 324
    .line 325
    move-result p1

    .line 326
    invoke-interface {p2, v0, p1}, Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;->〇080(ZZ)V

    .line 327
    .line 328
    .line 329
    :cond_c
    return v8

    .line 330
    :cond_d
    iget-boolean p2, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇〇〇0:Z

    .line 331
    .line 332
    if-eqz p2, :cond_e

    .line 333
    .line 334
    invoke-direct {p0, p1, v3}, Lcom/samsung/sdraw/SelectMode;->〇0000OOO(Lcom/samsung/sdraw/AbstractModeContext;Lcom/samsung/sdraw/PointF;)V

    .line 335
    .line 336
    .line 337
    goto :goto_1

    .line 338
    :cond_e
    iget-boolean p2, p0, Lcom/samsung/sdraw/SelectMode;->Oooo8o0〇:Z

    .line 339
    .line 340
    if-eqz p2, :cond_f

    .line 341
    .line 342
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 343
    .line 344
    iget-object v0, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 345
    .line 346
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->Oo08:Lcom/samsung/sdraw/PointF;

    .line 347
    .line 348
    iget-object v3, p0, Lcom/samsung/sdraw/SelectMode;->〇O888o0o:Lcom/samsung/sdraw/PointF;

    .line 349
    .line 350
    invoke-virtual {p2, v0, v2, v3, v8}, Lcom/samsung/sdraw/AbstractStage;->ooo〇8oO(Lcom/samsung/sdraw/AbstractSprite;Lcom/samsung/sdraw/PointF;Lcom/samsung/sdraw/PointF;Z)V

    .line 351
    .line 352
    .line 353
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 354
    .line 355
    .line 356
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->Oooo8o0〇:Z

    .line 357
    .line 358
    :cond_f
    :goto_1
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇o〇:Z

    .line 359
    .line 360
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->OO0o〇〇〇〇0:Z

    .line 361
    .line 362
    iget-boolean p2, p0, Lcom/samsung/sdraw/SelectMode;->Oo8Oo00oo:Z

    .line 363
    .line 364
    if-eqz p2, :cond_13

    .line 365
    .line 366
    iget-boolean p2, p0, Lcom/samsung/sdraw/SelectMode;->〇〇〇0〇〇0:Z

    .line 367
    .line 368
    if-eqz p2, :cond_10

    .line 369
    .line 370
    sget-object p2, Lcom/samsung/sdraw/SelectMode$HitDirection;->DELETE:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 371
    .line 372
    invoke-direct {p0, p1, p2}, Lcom/samsung/sdraw/SelectMode;->OoO8(Lcom/samsung/sdraw/AbstractModeContext;Lcom/samsung/sdraw/SelectMode$HitDirection;)V

    .line 373
    .line 374
    .line 375
    goto :goto_2

    .line 376
    :cond_10
    iget-boolean p2, p0, Lcom/samsung/sdraw/SelectMode;->o〇0OOo〇0:Z

    .line 377
    .line 378
    if-eqz p2, :cond_11

    .line 379
    .line 380
    sget-object p2, Lcom/samsung/sdraw/SelectMode$HitDirection;->ROTATE_LEFT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 381
    .line 382
    invoke-direct {p0, p1, p2}, Lcom/samsung/sdraw/SelectMode;->OoO8(Lcom/samsung/sdraw/AbstractModeContext;Lcom/samsung/sdraw/SelectMode$HitDirection;)V

    .line 383
    .line 384
    .line 385
    goto :goto_2

    .line 386
    :cond_11
    iget-boolean p2, p0, Lcom/samsung/sdraw/SelectMode;->〇〇0o:Z

    .line 387
    .line 388
    if-eqz p2, :cond_12

    .line 389
    .line 390
    sget-object p2, Lcom/samsung/sdraw/SelectMode$HitDirection;->ROTATE_RIGHT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 391
    .line 392
    invoke-direct {p0, p1, p2}, Lcom/samsung/sdraw/SelectMode;->OoO8(Lcom/samsung/sdraw/AbstractModeContext;Lcom/samsung/sdraw/SelectMode$HitDirection;)V

    .line 393
    .line 394
    .line 395
    :cond_12
    :goto_2
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->Oo8Oo00oo:Z

    .line 396
    .line 397
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇〇0〇〇0:Z

    .line 398
    .line 399
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->o〇0OOo〇0:Z

    .line 400
    .line 401
    iput-boolean v1, p0, Lcom/samsung/sdraw/SelectMode;->〇〇0o:Z

    .line 402
    .line 403
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 404
    .line 405
    .line 406
    :cond_13
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇080:Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;

    .line 407
    .line 408
    if-eqz p2, :cond_1d

    .line 409
    .line 410
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 411
    .line 412
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractStage;->o8oO〇()Z

    .line 413
    .line 414
    .line 415
    move-result v0

    .line 416
    iget-object p1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 417
    .line 418
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractStage;->O〇O〇oO()Z

    .line 419
    .line 420
    .line 421
    move-result p1

    .line 422
    invoke-interface {p2, v0, p1}, Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;->〇080(ZZ)V

    .line 423
    .line 424
    .line 425
    goto/16 :goto_6

    .line 426
    .line 427
    :cond_14
    iget-object v4, p0, Lcom/samsung/sdraw/SelectMode;->O8:[Lcom/samsung/sdraw/PointF;

    .line 428
    .line 429
    aget-object v4, v4, v1

    .line 430
    .line 431
    iget v5, v3, Landroid/graphics/PointF;->x:F

    .line 432
    .line 433
    iput v5, v4, Landroid/graphics/PointF;->x:F

    .line 434
    .line 435
    iget v5, v3, Landroid/graphics/PointF;->y:F

    .line 436
    .line 437
    iput v5, v4, Landroid/graphics/PointF;->y:F

    .line 438
    .line 439
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 440
    .line 441
    .line 442
    move-result v4

    .line 443
    if-le v4, v8, :cond_15

    .line 444
    .line 445
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/sdraw/SelectMode;->〇〇8O0〇8(Lcom/samsung/sdraw/AbstractModeContext;Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    .line 446
    .line 447
    .line 448
    goto :goto_5

    .line 449
    :cond_15
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇〇888:Lcom/samsung/sdraw/Setting;

    .line 450
    .line 451
    invoke-virtual {p2}, Lcom/samsung/sdraw/Setting;->〇O888o0o()Lcom/samsung/sdraw/StrokeSprite$InputMethod;

    .line 452
    .line 453
    .line 454
    move-result-object p2

    .line 455
    sget-object v0, Lcom/samsung/sdraw/StrokeSprite$InputMethod;->Tablet:Lcom/samsung/sdraw/StrokeSprite$InputMethod;

    .line 456
    .line 457
    if-ne p2, v0, :cond_16

    .line 458
    .line 459
    invoke-virtual {p0, v3, v6}, Lcom/samsung/sdraw/SelectMode;->〇8o8o〇(Lcom/samsung/sdraw/PointF;I)Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 460
    .line 461
    .line 462
    move-result-object p2

    .line 463
    goto :goto_3

    .line 464
    :cond_16
    const/16 p2, 0x1e

    .line 465
    .line 466
    invoke-virtual {p0, v3, p2}, Lcom/samsung/sdraw/SelectMode;->〇8o8o〇(Lcom/samsung/sdraw/PointF;I)Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 467
    .line 468
    .line 469
    move-result-object p2

    .line 470
    :goto_3
    sget-object v0, Lcom/samsung/sdraw/SelectMode$HitDirection;->DELETE:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 471
    .line 472
    if-eq p2, v0, :cond_1a

    .line 473
    .line 474
    sget-object v4, Lcom/samsung/sdraw/SelectMode$HitDirection;->ROTATE_RIGHT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 475
    .line 476
    if-eq p2, v4, :cond_1a

    .line 477
    .line 478
    sget-object v4, Lcom/samsung/sdraw/SelectMode$HitDirection;->ROTATE_LEFT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 479
    .line 480
    if-ne p2, v4, :cond_17

    .line 481
    .line 482
    goto :goto_4

    .line 483
    :cond_17
    sget-object v0, Lcom/samsung/sdraw/SelectMode$HitDirection;->INNER:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 484
    .line 485
    if-eq p2, v0, :cond_18

    .line 486
    .line 487
    invoke-direct {p0, p2}, Lcom/samsung/sdraw/SelectMode;->o800o8O(Lcom/samsung/sdraw/SelectMode$HitDirection;)V

    .line 488
    .line 489
    .line 490
    iput-boolean v8, p0, Lcom/samsung/sdraw/SelectMode;->〇o〇:Z

    .line 491
    .line 492
    goto :goto_5

    .line 493
    :cond_18
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 494
    .line 495
    new-instance v0, Landroid/graphics/RectF;

    .line 496
    .line 497
    iget v4, v3, Landroid/graphics/PointF;->x:F

    .line 498
    .line 499
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 500
    .line 501
    const/high16 v5, 0x3f800000    # 1.0f

    .line 502
    .line 503
    add-float v6, v4, v5

    .line 504
    .line 505
    add-float/2addr v5, v3

    .line 506
    invoke-direct {v0, v4, v3, v6, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 507
    .line 508
    .line 509
    invoke-virtual {p2, v0}, Lcom/samsung/sdraw/AbstractSprite;->o〇0(Landroid/graphics/RectF;)Z

    .line 510
    .line 511
    .line 512
    move-result p2

    .line 513
    if-eqz p2, :cond_19

    .line 514
    .line 515
    iput-boolean v8, p0, Lcom/samsung/sdraw/SelectMode;->〇o〇:Z

    .line 516
    .line 517
    goto :goto_5

    .line 518
    :cond_19
    invoke-direct {p0, p1, v2}, Lcom/samsung/sdraw/SelectMode;->〇oo〇(Lcom/samsung/sdraw/AbstractModeContext;Lcom/samsung/sdraw/PointF;)Z

    .line 519
    .line 520
    .line 521
    move-result p1

    .line 522
    iput-boolean p1, p0, Lcom/samsung/sdraw/SelectMode;->〇o〇:Z

    .line 523
    .line 524
    goto :goto_5

    .line 525
    :cond_1a
    :goto_4
    iput-boolean v8, p0, Lcom/samsung/sdraw/SelectMode;->Oo8Oo00oo:Z

    .line 526
    .line 527
    if-ne p2, v0, :cond_1b

    .line 528
    .line 529
    iput-boolean v8, p0, Lcom/samsung/sdraw/SelectMode;->〇〇〇0〇〇0:Z

    .line 530
    .line 531
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 532
    .line 533
    .line 534
    goto :goto_5

    .line 535
    :cond_1b
    sget-object v0, Lcom/samsung/sdraw/SelectMode$HitDirection;->ROTATE_LEFT:Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 536
    .line 537
    if-ne p2, v0, :cond_1c

    .line 538
    .line 539
    iput-boolean v8, p0, Lcom/samsung/sdraw/SelectMode;->o〇0OOo〇0:Z

    .line 540
    .line 541
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 542
    .line 543
    .line 544
    goto :goto_5

    .line 545
    :cond_1c
    iput-boolean v8, p0, Lcom/samsung/sdraw/SelectMode;->〇〇0o:Z

    .line 546
    .line 547
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 548
    .line 549
    .line 550
    :goto_5
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 551
    .line 552
    instance-of p2, p1, Lcom/samsung/sdraw/z;

    .line 553
    .line 554
    if-eqz p2, :cond_1d

    .line 555
    .line 556
    check-cast p1, Lcom/samsung/sdraw/z;

    .line 557
    .line 558
    new-instance p2, Lcom/samsung/sdraw/PointF;

    .line 559
    .line 560
    invoke-virtual {p1, v1}, Lcom/samsung/sdraw/z;->〇0000OOO(I)Lcom/samsung/sdraw/PointF;

    .line 561
    .line 562
    .line 563
    move-result-object v0

    .line 564
    invoke-direct {p2, v0}, Lcom/samsung/sdraw/PointF;-><init>(Landroid/graphics/PointF;)V

    .line 565
    .line 566
    .line 567
    iput-object p2, p0, Lcom/samsung/sdraw/SelectMode;->〇O888o0o:Lcom/samsung/sdraw/PointF;

    .line 568
    .line 569
    invoke-virtual {p1}, Lcom/samsung/sdraw/z;->O8〇o()[Lcom/samsung/sdraw/PointF;

    .line 570
    .line 571
    .line 572
    move-result-object p2

    .line 573
    iput-object p2, p0, Lcom/samsung/sdraw/SelectMode;->oo88o8O:[Lcom/samsung/sdraw/PointF;

    .line 574
    .line 575
    iget p1, p1, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 576
    .line 577
    int-to-float p1, p1

    .line 578
    iput p1, p0, Lcom/samsung/sdraw/SelectMode;->〇oo〇:F

    .line 579
    .line 580
    :cond_1d
    :goto_6
    return v8
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
.end method

.method public 〇0〇O0088o(Lcom/samsung/sdraw/AbstractModeContext;Lcom/samsung/sdraw/AbstractSprite;)V
    .locals 11

    .line 1
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->oo〇(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 2
    .line 3
    .line 4
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 5
    .line 6
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractStage;->o〇O8〇〇o()V

    .line 7
    .line 8
    .line 9
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 10
    .line 11
    const-class v0, Lcom/samsung/sdraw/StrokeSprite;

    .line 12
    .line 13
    const-class v1, Lcom/samsung/sdraw/r;

    .line 14
    .line 15
    invoke-virtual {p2, v0, v1}, Lcom/samsung/sdraw/AbstractStage;->〇o〇(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    iget-object v2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 20
    .line 21
    invoke-virtual {v2, v0, v1}, Lcom/samsung/sdraw/AbstractStage;->〇O00(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iget-object v1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 26
    .line 27
    const-class v2, Lcom/samsung/sdraw/TextSprite;

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Lcom/samsung/sdraw/AbstractStage;->〇o00〇〇Oo(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    iget-object v3, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 34
    .line 35
    invoke-virtual {v3, v2}, Lcom/samsung/sdraw/AbstractStage;->〇O〇(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    iget-object v3, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 40
    .line 41
    const-class v4, Lcom/samsung/sdraw/z;

    .line 42
    .line 43
    invoke-virtual {v3, v4}, Lcom/samsung/sdraw/AbstractStage;->〇o00〇〇Oo(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    iget-object v5, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 48
    .line 49
    invoke-virtual {v5, v4}, Lcom/samsung/sdraw/AbstractStage;->〇O〇(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 50
    .line 51
    .line 52
    move-result-object v4

    .line 53
    iget-object v5, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 54
    .line 55
    const/4 v6, 0x0

    .line 56
    invoke-virtual {v5, v6}, Lcom/samsung/sdraw/AbstractStage;->O0o〇〇Oo(I)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v3}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 60
    .line 61
    .line 62
    move-result-object v5

    .line 63
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 64
    .line 65
    .line 66
    move-result v7

    .line 67
    if-nez v7, :cond_14

    .line 68
    .line 69
    if-eqz v1, :cond_1

    .line 70
    .line 71
    iget-object v5, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 72
    .line 73
    const/4 v7, 0x2

    .line 74
    invoke-virtual {v5, v7, v1}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 75
    .line 76
    .line 77
    :cond_1
    if-eqz p2, :cond_2

    .line 78
    .line 79
    iget-object v1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 80
    .line 81
    invoke-virtual {v1, v6, p2}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 82
    .line 83
    .line 84
    :cond_2
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 85
    .line 86
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractStage;->o〇0OOo〇0()I

    .line 87
    .line 88
    .line 89
    move-result p2

    .line 90
    const/4 v7, 0x4

    .line 91
    const/4 v8, 0x1

    .line 92
    const/4 v9, 0x3

    .line 93
    if-nez p2, :cond_b

    .line 94
    .line 95
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 96
    .line 97
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractStage;->o0ooO()Z

    .line 98
    .line 99
    .line 100
    move-result p2

    .line 101
    if-nez p2, :cond_b

    .line 102
    .line 103
    invoke-virtual {v3}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 104
    .line 105
    .line 106
    move-result-object p2

    .line 107
    :cond_3
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 108
    .line 109
    .line 110
    move-result v1

    .line 111
    if-nez v1, :cond_a

    .line 112
    .line 113
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 114
    .line 115
    invoke-virtual {p2, v9}, Lcom/samsung/sdraw/AbstractStage;->O0o〇〇Oo(I)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {v4}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 119
    .line 120
    .line 121
    move-result-object v1

    .line 122
    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 123
    .line 124
    .line 125
    move-result p2

    .line 126
    if-nez p2, :cond_9

    .line 127
    .line 128
    if-eqz v2, :cond_5

    .line 129
    .line 130
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 131
    .line 132
    invoke-virtual {p2, v7, v2}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 133
    .line 134
    .line 135
    :cond_5
    if-eqz v0, :cond_6

    .line 136
    .line 137
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 138
    .line 139
    invoke-virtual {p2, v9, v0}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 140
    .line 141
    .line 142
    :cond_6
    invoke-virtual {v4}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 143
    .line 144
    .line 145
    move-result-object p2

    .line 146
    :cond_7
    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 147
    .line 148
    .line 149
    move-result v0

    .line 150
    if-nez v0, :cond_8

    .line 151
    .line 152
    goto :goto_7

    .line 153
    :cond_8
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 154
    .line 155
    .line 156
    move-result-object v0

    .line 157
    check-cast v0, Lcom/samsung/sdraw/AbstractSprite;

    .line 158
    .line 159
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSprite;->O8()Z

    .line 160
    .line 161
    .line 162
    move-result v1

    .line 163
    if-eqz v1, :cond_7

    .line 164
    .line 165
    iget-object v1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 166
    .line 167
    invoke-virtual {v1, v8, v0}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 168
    .line 169
    .line 170
    goto :goto_3

    .line 171
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 172
    .line 173
    .line 174
    move-result-object p2

    .line 175
    check-cast p2, Lcom/samsung/sdraw/AbstractSprite;

    .line 176
    .line 177
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractSprite;->O8()Z

    .line 178
    .line 179
    .line 180
    move-result v3

    .line 181
    if-nez v3, :cond_4

    .line 182
    .line 183
    iget-object v3, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 184
    .line 185
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractSprite;->〇80〇808〇O()I

    .line 186
    .line 187
    .line 188
    move-result v5

    .line 189
    invoke-virtual {v3, v5, p2}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 190
    .line 191
    .line 192
    goto :goto_2

    .line 193
    :cond_a
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 194
    .line 195
    .line 196
    move-result-object v1

    .line 197
    check-cast v1, Lcom/samsung/sdraw/AbstractSprite;

    .line 198
    .line 199
    invoke-virtual {v1}, Lcom/samsung/sdraw/AbstractSprite;->O8()Z

    .line 200
    .line 201
    .line 202
    move-result v3

    .line 203
    if-eqz v3, :cond_3

    .line 204
    .line 205
    iget-object v3, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 206
    .line 207
    invoke-virtual {v3, v8, v1}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 208
    .line 209
    .line 210
    goto :goto_1

    .line 211
    :cond_b
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 212
    .line 213
    invoke-virtual {p2, v9}, Lcom/samsung/sdraw/AbstractStage;->O0o〇〇Oo(I)V

    .line 214
    .line 215
    .line 216
    invoke-virtual {v4}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 217
    .line 218
    .line 219
    move-result-object v10

    .line 220
    :cond_c
    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    .line 221
    .line 222
    .line 223
    move-result p2

    .line 224
    if-nez p2, :cond_13

    .line 225
    .line 226
    if-eqz v2, :cond_d

    .line 227
    .line 228
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 229
    .line 230
    invoke-virtual {p2, v7, v2}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 231
    .line 232
    .line 233
    :cond_d
    if-eqz v0, :cond_e

    .line 234
    .line 235
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 236
    .line 237
    invoke-virtual {p2, v9, v0}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 238
    .line 239
    .line 240
    :cond_e
    invoke-virtual {v4}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 241
    .line 242
    .line 243
    move-result-object p2

    .line 244
    :cond_f
    :goto_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 245
    .line 246
    .line 247
    move-result v0

    .line 248
    if-nez v0, :cond_12

    .line 249
    .line 250
    invoke-virtual {v3}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 251
    .line 252
    .line 253
    move-result-object v0

    .line 254
    :cond_10
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 255
    .line 256
    .line 257
    move-result p2

    .line 258
    if-nez p2, :cond_11

    .line 259
    .line 260
    :goto_7
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractModeContext;->〇O〇()V

    .line 261
    .line 262
    .line 263
    return-void

    .line 264
    :cond_11
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 265
    .line 266
    .line 267
    move-result-object p2

    .line 268
    check-cast p2, Lcom/samsung/sdraw/AbstractSprite;

    .line 269
    .line 270
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractSprite;->O8()Z

    .line 271
    .line 272
    .line 273
    move-result v1

    .line 274
    if-eqz v1, :cond_10

    .line 275
    .line 276
    iget-object v1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 277
    .line 278
    invoke-virtual {v1, v8, p2}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 279
    .line 280
    .line 281
    goto :goto_6

    .line 282
    :cond_12
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 283
    .line 284
    .line 285
    move-result-object v0

    .line 286
    check-cast v0, Lcom/samsung/sdraw/AbstractSprite;

    .line 287
    .line 288
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSprite;->O8()Z

    .line 289
    .line 290
    .line 291
    move-result v1

    .line 292
    if-eqz v1, :cond_f

    .line 293
    .line 294
    iget-object v1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 295
    .line 296
    invoke-virtual {v1, v8, v0}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 297
    .line 298
    .line 299
    goto :goto_5

    .line 300
    :cond_13
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 301
    .line 302
    .line 303
    move-result-object p2

    .line 304
    check-cast p2, Lcom/samsung/sdraw/AbstractSprite;

    .line 305
    .line 306
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractSprite;->O8()Z

    .line 307
    .line 308
    .line 309
    move-result v1

    .line 310
    if-nez v1, :cond_c

    .line 311
    .line 312
    iget-object v1, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 313
    .line 314
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractSprite;->〇80〇808〇O()I

    .line 315
    .line 316
    .line 317
    move-result v5

    .line 318
    invoke-virtual {v1, v5, p2}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 319
    .line 320
    .line 321
    goto :goto_4

    .line 322
    :cond_14
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 323
    .line 324
    .line 325
    move-result-object v7

    .line 326
    check-cast v7, Lcom/samsung/sdraw/AbstractSprite;

    .line 327
    .line 328
    invoke-virtual {v7}, Lcom/samsung/sdraw/AbstractSprite;->O8()Z

    .line 329
    .line 330
    .line 331
    move-result v8

    .line 332
    if-nez v8, :cond_0

    .line 333
    .line 334
    iget-object v8, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 335
    .line 336
    invoke-virtual {v7}, Lcom/samsung/sdraw/AbstractSprite;->〇80〇808〇O()I

    .line 337
    .line 338
    .line 339
    move-result v9

    .line 340
    invoke-virtual {v8, v9, v7}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 341
    .line 342
    .line 343
    goto/16 :goto_0
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
.end method

.method protected 〇8o8o〇(Lcom/samsung/sdraw/PointF;I)Lcom/samsung/sdraw/SelectMode$HitDirection;
    .locals 7

    .line 1
    invoke-static {}, Lcom/samsung/sdraw/SelectMode$HitDirection;->values()[Lcom/samsung/sdraw/SelectMode$HitDirection;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 6
    .line 7
    instance-of v1, v1, Lcom/samsung/sdraw/z;

    .line 8
    .line 9
    if-eqz v1, :cond_2

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    :goto_0
    array-length v2, v0

    .line 13
    if-lt v1, v2, :cond_0

    .line 14
    .line 15
    goto :goto_1

    .line 16
    :cond_0
    new-instance v2, Landroid/graphics/Rect;

    .line 17
    .line 18
    iget-object v3, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 19
    .line 20
    invoke-virtual {v3}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    iget v3, v3, Landroid/graphics/RectF;->left:F

    .line 25
    .line 26
    float-to-int v3, v3

    .line 27
    iget-object v4, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 28
    .line 29
    invoke-virtual {v4}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    iget v4, v4, Landroid/graphics/RectF;->top:F

    .line 34
    .line 35
    float-to-int v4, v4

    .line 36
    iget-object v5, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 37
    .line 38
    invoke-virtual {v5}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 39
    .line 40
    .line 41
    move-result-object v5

    .line 42
    iget v5, v5, Landroid/graphics/RectF;->right:F

    .line 43
    .line 44
    float-to-int v5, v5

    .line 45
    iget-object v6, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 46
    .line 47
    invoke-virtual {v6}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 48
    .line 49
    .line 50
    move-result-object v6

    .line 51
    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    .line 52
    .line 53
    float-to-int v6, v6

    .line 54
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 55
    .line 56
    .line 57
    aget-object v3, v0, v1

    .line 58
    .line 59
    invoke-virtual {p0, v3, p2, v2}, Lcom/samsung/sdraw/SelectMode;->oO80(Lcom/samsung/sdraw/SelectMode$HitDirection;ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    iget v3, p1, Landroid/graphics/PointF;->x:F

    .line 64
    .line 65
    float-to-int v3, v3

    .line 66
    iget v4, p1, Landroid/graphics/PointF;->y:F

    .line 67
    .line 68
    float-to-int v4, v4

    .line 69
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    .line 70
    .line 71
    .line 72
    move-result v2

    .line 73
    if-eqz v2, :cond_1

    .line 74
    .line 75
    aget-object p1, v0, v1

    .line 76
    .line 77
    return-object p1

    .line 78
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_2
    :goto_1
    const/4 p1, 0x0

    .line 82
    aget-object p1, v0, p1

    .line 83
    .line 84
    return-object p1
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public 〇O888o0o(Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/sdraw/SelectMode;->O〇8O8〇008:Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇O8o08O(Lcom/samsung/sdraw/AbstractModeContext;)V
    .locals 3

    .line 1
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Lcom/samsung/sdraw/AbstractStage;->oo88o8O(I)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 8
    .line 9
    iget-object v2, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractModeContext;->〇O〇()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇o00〇〇Oo(Lcom/samsung/sdraw/AbstractModeContext;)I
    .locals 0

    .line 1
    const/4 p1, 0x2

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇o〇(Lcom/samsung/sdraw/AbstractModeContext;Z)V
    .locals 12

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p2, :cond_e

    .line 3
    .line 4
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 5
    .line 6
    invoke-virtual {p2}, Lcom/samsung/sdraw/AbstractStage;->o〇O8〇〇o()V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇oOO8O8(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 10
    .line 11
    .line 12
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 13
    .line 14
    const-class v1, Lcom/samsung/sdraw/StrokeSprite;

    .line 15
    .line 16
    const-class v2, Lcom/samsung/sdraw/r;

    .line 17
    .line 18
    invoke-virtual {p2, v1, v2}, Lcom/samsung/sdraw/AbstractStage;->〇o〇(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 19
    .line 20
    .line 21
    move-result-object p2

    .line 22
    iget-object v3, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 23
    .line 24
    invoke-virtual {v3, v1, v2}, Lcom/samsung/sdraw/AbstractStage;->〇O00(Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    iget-object v2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 29
    .line 30
    const-class v3, Lcom/samsung/sdraw/z;

    .line 31
    .line 32
    invoke-virtual {v2, v3}, Lcom/samsung/sdraw/AbstractStage;->〇o00〇〇Oo(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    iget-object v4, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 37
    .line 38
    invoke-virtual {v4, v3}, Lcom/samsung/sdraw/AbstractStage;->〇O〇(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    iget-object v4, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 43
    .line 44
    const-class v5, Lcom/samsung/sdraw/TextSprite;

    .line 45
    .line 46
    invoke-virtual {v4, v5}, Lcom/samsung/sdraw/AbstractStage;->〇o00〇〇Oo(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 47
    .line 48
    .line 49
    move-result-object v4

    .line 50
    iget-object v6, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 51
    .line 52
    invoke-virtual {v6, v5}, Lcom/samsung/sdraw/AbstractStage;->〇O〇(Ljava/lang/Class;)Ljava/util/LinkedList;

    .line 53
    .line 54
    .line 55
    move-result-object v5

    .line 56
    iget-object v6, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 57
    .line 58
    const/4 v7, 0x0

    .line 59
    invoke-virtual {v6, v7}, Lcom/samsung/sdraw/AbstractStage;->〇〇808〇(I)Landroid/graphics/Canvas;

    .line 60
    .line 61
    .line 62
    move-result-object v6

    .line 63
    iget-object v8, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 64
    .line 65
    iget-object v9, v8, Lcom/samsung/sdraw/AbstractStage;->〇〇888:Landroid/graphics/Bitmap;

    .line 66
    .line 67
    const/4 v10, 0x0

    .line 68
    const/4 v11, 0x0

    .line 69
    if-eqz v9, :cond_0

    .line 70
    .line 71
    invoke-virtual {v8, v7}, Lcom/samsung/sdraw/AbstractStage;->o〇O(I)Z

    .line 72
    .line 73
    .line 74
    move-result v8

    .line 75
    if-eqz v8, :cond_0

    .line 76
    .line 77
    iget-object v8, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 78
    .line 79
    iget-object v8, v8, Lcom/samsung/sdraw/AbstractStage;->〇〇888:Landroid/graphics/Bitmap;

    .line 80
    .line 81
    invoke-virtual {v6, v8, v11, v11, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 82
    .line 83
    .line 84
    :cond_0
    const/4 v6, 0x2

    .line 85
    if-eqz v2, :cond_1

    .line 86
    .line 87
    iget-object v8, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 88
    .line 89
    iget-object v9, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 90
    .line 91
    invoke-virtual {v8, v6, v2, v9}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 92
    .line 93
    .line 94
    :cond_1
    if-eqz v4, :cond_2

    .line 95
    .line 96
    iget-object v2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 97
    .line 98
    iget-object v8, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 99
    .line 100
    invoke-virtual {v2, v6, v4, v8}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 101
    .line 102
    .line 103
    :cond_2
    if-eqz p2, :cond_3

    .line 104
    .line 105
    iget-object v2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 106
    .line 107
    invoke-virtual {v2, v7, p2}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 108
    .line 109
    .line 110
    :cond_3
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 111
    .line 112
    const/4 v2, 0x3

    .line 113
    invoke-virtual {p2, v2}, Lcom/samsung/sdraw/AbstractStage;->〇〇808〇(I)Landroid/graphics/Canvas;

    .line 114
    .line 115
    .line 116
    move-result-object p2

    .line 117
    iget-object v4, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 118
    .line 119
    invoke-virtual {v4}, Lcom/samsung/sdraw/AbstractStage;->o〇0OOo〇0()I

    .line 120
    .line 121
    .line 122
    move-result v4

    .line 123
    const/4 v6, 0x4

    .line 124
    if-nez v4, :cond_7

    .line 125
    .line 126
    iget-object v4, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 127
    .line 128
    invoke-virtual {v4}, Lcom/samsung/sdraw/AbstractStage;->o0ooO()Z

    .line 129
    .line 130
    .line 131
    move-result v4

    .line 132
    if-nez v4, :cond_7

    .line 133
    .line 134
    iget-object v4, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 135
    .line 136
    iget-object v8, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 137
    .line 138
    invoke-virtual {v4, v0, v8}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 139
    .line 140
    .line 141
    iget-object v4, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 142
    .line 143
    iget-object v8, v4, Lcom/samsung/sdraw/AbstractStage;->oO80:Landroid/graphics/Bitmap;

    .line 144
    .line 145
    if-eqz v8, :cond_4

    .line 146
    .line 147
    invoke-virtual {v4, v2}, Lcom/samsung/sdraw/AbstractStage;->o〇O(I)Z

    .line 148
    .line 149
    .line 150
    move-result v4

    .line 151
    if-eqz v4, :cond_4

    .line 152
    .line 153
    iget-object v4, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 154
    .line 155
    iget-object v4, v4, Lcom/samsung/sdraw/AbstractStage;->oO80:Landroid/graphics/Bitmap;

    .line 156
    .line 157
    invoke-virtual {p2, v4, v11, v11, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 158
    .line 159
    .line 160
    :cond_4
    if-eqz v3, :cond_5

    .line 161
    .line 162
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 163
    .line 164
    iget-object v4, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 165
    .line 166
    invoke-virtual {p2, v6, v3, v4}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 167
    .line 168
    .line 169
    :cond_5
    if-eqz v5, :cond_6

    .line 170
    .line 171
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 172
    .line 173
    iget-object v3, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 174
    .line 175
    invoke-virtual {p2, v6, v5, v3}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 176
    .line 177
    .line 178
    :cond_6
    if-eqz v1, :cond_c

    .line 179
    .line 180
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 181
    .line 182
    invoke-virtual {p2, v2, v1}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 183
    .line 184
    .line 185
    goto :goto_0

    .line 186
    :cond_7
    iget-object v4, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 187
    .line 188
    iget-object v8, v4, Lcom/samsung/sdraw/AbstractStage;->oO80:Landroid/graphics/Bitmap;

    .line 189
    .line 190
    if-eqz v8, :cond_8

    .line 191
    .line 192
    invoke-virtual {v4, v2}, Lcom/samsung/sdraw/AbstractStage;->o〇O(I)Z

    .line 193
    .line 194
    .line 195
    move-result v4

    .line 196
    if-eqz v4, :cond_8

    .line 197
    .line 198
    iget-object v4, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 199
    .line 200
    iget-object v4, v4, Lcom/samsung/sdraw/AbstractStage;->oO80:Landroid/graphics/Bitmap;

    .line 201
    .line 202
    invoke-virtual {p2, v4, v11, v11, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 203
    .line 204
    .line 205
    :cond_8
    if-eqz v3, :cond_9

    .line 206
    .line 207
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 208
    .line 209
    iget-object v4, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 210
    .line 211
    invoke-virtual {p2, v6, v3, v4}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 212
    .line 213
    .line 214
    :cond_9
    if-eqz v5, :cond_a

    .line 215
    .line 216
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 217
    .line 218
    iget-object v3, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 219
    .line 220
    invoke-virtual {p2, v6, v5, v3}, Lcom/samsung/sdraw/AbstractStage;->oO80(ILjava/util/LinkedList;Lcom/samsung/sdraw/AbstractSprite;)V

    .line 221
    .line 222
    .line 223
    :cond_a
    if-eqz v1, :cond_b

    .line 224
    .line 225
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 226
    .line 227
    invoke-virtual {p2, v2, v1}, Lcom/samsung/sdraw/AbstractStage;->〇〇888(ILjava/util/LinkedList;)V

    .line 228
    .line 229
    .line 230
    :cond_b
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 231
    .line 232
    iget-object v1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 233
    .line 234
    invoke-virtual {p2, v0, v1}, Lcom/samsung/sdraw/AbstractStage;->OO8oO0o〇(ILcom/samsung/sdraw/AbstractSprite;)V

    .line 235
    .line 236
    .line 237
    :cond_c
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractModeContext;->〇O〇()V

    .line 238
    .line 239
    .line 240
    iget-boolean p1, p0, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o:Z

    .line 241
    .line 242
    if-nez p1, :cond_d

    .line 243
    .line 244
    iget-object p1, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 245
    .line 246
    if-eqz p1, :cond_d

    .line 247
    .line 248
    iget-object p2, p0, Lcom/samsung/sdraw/SelectMode;->O〇8O8〇008:Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;

    .line 249
    .line 250
    if-eqz p2, :cond_d

    .line 251
    .line 252
    check-cast p1, Lcom/samsung/sdraw/z;

    .line 253
    .line 254
    invoke-virtual {p1}, Lcom/samsung/sdraw/z;->〇oOO8O8()Lcom/samsung/sdraw/ImageInfo;

    .line 255
    .line 256
    .line 257
    move-result-object p1

    .line 258
    invoke-interface {p2, p1, v0}, Lcom/samsung/sdraw/SelectMode$OnImageSelectedListener;->〇080(Lcom/samsung/sdraw/ObjectInfo;Z)V

    .line 259
    .line 260
    .line 261
    :cond_d
    iput-boolean v7, p0, Lcom/samsung/sdraw/SelectMode;->o〇O8〇〇o:Z

    .line 262
    .line 263
    goto :goto_1

    .line 264
    :cond_e
    iget-object p2, p1, Lcom/samsung/sdraw/AbstractModeContext;->〇80〇808〇O:Lcom/samsung/sdraw/AbstractStage;

    .line 265
    .line 266
    invoke-virtual {p2, v0}, Lcom/samsung/sdraw/AbstractStage;->oo88o8O(I)V

    .line 267
    .line 268
    .line 269
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/SelectMode;->〇00〇8(Lcom/samsung/sdraw/AbstractModeContext;)V

    .line 270
    .line 271
    .line 272
    :goto_1
    return-void
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
.end method

.method protected 〇〇808〇(Lcom/samsung/sdraw/AbstractModeContext;Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/sdraw/PointF;F)V
    .locals 1

    .line 1
    new-instance p1, Landroid/graphics/RectF;

    .line 2
    .line 3
    iget-object p3, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 4
    .line 5
    check-cast p3, Lcom/samsung/sdraw/z;

    .line 6
    .line 7
    invoke-virtual {p3}, Lcom/samsung/sdraw/z;->o〇〇0〇()Landroid/graphics/RectF;

    .line 8
    .line 9
    .line 10
    move-result-object p3

    .line 11
    invoke-direct {p1, p3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    .line 15
    .line 16
    .line 17
    new-instance p3, Landroid/graphics/Matrix;

    .line 18
    .line 19
    invoke-direct {p3}, Landroid/graphics/Matrix;-><init>()V

    .line 20
    .line 21
    .line 22
    iget-object p4, p0, Lcom/samsung/sdraw/SelectMode;->〇o00〇〇Oo:Lcom/samsung/sdraw/AbstractSprite;

    .line 23
    .line 24
    check-cast p4, Lcom/samsung/sdraw/z;

    .line 25
    .line 26
    iget p4, p4, Lcom/samsung/sdraw/z;->〇〇808〇:I

    .line 27
    .line 28
    int-to-float p4, p4

    .line 29
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    .line 30
    .line 31
    .line 32
    move-result p5

    .line 33
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    invoke-virtual {p3, p4, p5, v0}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    .line 38
    .line 39
    .line 40
    invoke-virtual {p2, p3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 41
    .line 42
    .line 43
    new-instance p3, Landroid/graphics/Paint;

    .line 44
    .line 45
    invoke-direct {p3}, Landroid/graphics/Paint;-><init>()V

    .line 46
    .line 47
    .line 48
    sget-object p4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 49
    .line 50
    invoke-virtual {p3, p4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 51
    .line 52
    .line 53
    const p4, -0xfe7604

    .line 54
    .line 55
    .line 56
    invoke-virtual {p3, p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 57
    .line 58
    .line 59
    const/high16 p4, 0x40000000    # 2.0f

    .line 60
    .line 61
    invoke-virtual {p3, p4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p2, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
.end method

.class Lcom/samsung/sdraw/ch;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/sdraw/br;


# static fields
.field private static OO0o〇〇〇〇0:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Integer;",
            "Ljava/util/Vector<",
            "Landroid/graphics/Region;",
            ">;>;"
        }
    .end annotation
.end field

.field private static 〇80〇808〇O:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private O8:Landroid/graphics/RectF;

.field private Oo08:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/samsung/sdraw/cd;",
            ">;"
        }
    .end annotation
.end field

.field oO80:Lcom/samsung/sdraw/ae;

.field private o〇0:I

.field private 〇080:Lcom/samsung/sdraw/StrokeSprite;

.field private 〇o00〇〇Oo:Landroid/graphics/Paint;

.field private 〇o〇:Lcom/samsung/sdraw/bo;

.field private 〇〇888:I


# direct methods
.method constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/samsung/sdraw/ae;

    .line 5
    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 7
    .line 8
    .line 9
    move-result-wide v1

    .line 10
    invoke-direct {v0, v1, v2}, Lcom/samsung/sdraw/ae;-><init>(J)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/samsung/sdraw/ch;->oO80:Lcom/samsung/sdraw/ae;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private O8(I)V
    .locals 20

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    sget-object v1, Lcom/samsung/sdraw/ch;->OO0o〇〇〇〇0:Ljava/util/LinkedHashMap;

    .line 4
    .line 5
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-virtual {v1, v2}, Ljava/util/AbstractMap;->containsKey(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    new-instance v1, Ljava/util/Vector;

    .line 17
    .line 18
    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 19
    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    :goto_0
    const/4 v3, 0x5

    .line 23
    if-lt v2, v3, :cond_1

    .line 24
    .line 25
    sget-object v2, Lcom/samsung/sdraw/ch;->OO0o〇〇〇〇0:Ljava/util/LinkedHashMap;

    .line 26
    .line 27
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    invoke-virtual {v2, v3, v1}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_1
    move/from16 v3, p1

    .line 36
    .line 37
    int-to-float v4, v3

    .line 38
    new-instance v11, Landroid/graphics/Region;

    .line 39
    .line 40
    invoke-direct {v11}, Landroid/graphics/Region;-><init>()V

    .line 41
    .line 42
    .line 43
    move v12, v4

    .line 44
    :goto_1
    neg-float v13, v4

    .line 45
    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    .line 46
    .line 47
    const/high16 v16, 0x3f800000    # 1.0f

    .line 48
    .line 49
    const/high16 v17, 0x40000000    # 2.0f

    .line 50
    .line 51
    cmpl-float v5, v12, v13

    .line 52
    .line 53
    if-gez v5, :cond_3

    .line 54
    .line 55
    move v12, v4

    .line 56
    :goto_2
    cmpl-float v5, v12, v13

    .line 57
    .line 58
    if-gez v5, :cond_2

    .line 59
    .line 60
    invoke-virtual {v1, v11}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    add-int/lit8 v2, v2, 0x1

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_2
    mul-float v5, v4, v4

    .line 67
    .line 68
    mul-float v6, v12, v12

    .line 69
    .line 70
    sub-float/2addr v5, v6

    .line 71
    float-to-double v5, v5

    .line 72
    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    .line 73
    .line 74
    .line 75
    move-result-wide v5

    .line 76
    mul-double v5, v5, v14

    .line 77
    .line 78
    double-to-float v5, v5

    .line 79
    sub-float v6, v4, v12

    .line 80
    .line 81
    div-float v7, v5, v17

    .line 82
    .line 83
    sub-float v7, v4, v7

    .line 84
    .line 85
    iget-object v8, v0, Lcom/samsung/sdraw/ch;->oO80:Lcom/samsung/sdraw/ae;

    .line 86
    .line 87
    invoke-virtual {v8}, Lcom/samsung/sdraw/ae;->〇o00〇〇Oo()F

    .line 88
    .line 89
    .line 90
    move-result v8

    .line 91
    mul-float v8, v8, v17

    .line 92
    .line 93
    float-to-int v9, v6

    .line 94
    sub-float v10, v7, v8

    .line 95
    .line 96
    float-to-int v10, v10

    .line 97
    add-float v6, v6, v16

    .line 98
    .line 99
    float-to-int v6, v6

    .line 100
    add-float/2addr v7, v5

    .line 101
    add-float/2addr v7, v8

    .line 102
    float-to-int v8, v7

    .line 103
    sget-object v18, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    .line 104
    .line 105
    move-object v5, v11

    .line 106
    move/from16 v19, v6

    .line 107
    .line 108
    move v6, v9

    .line 109
    move v7, v10

    .line 110
    move v9, v8

    .line 111
    move/from16 v8, v19

    .line 112
    .line 113
    move-object/from16 v10, v18

    .line 114
    .line 115
    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    .line 116
    .line 117
    .line 118
    sub-float v12, v12, v16

    .line 119
    .line 120
    goto :goto_2

    .line 121
    :cond_3
    mul-float v5, v4, v4

    .line 122
    .line 123
    mul-float v6, v12, v12

    .line 124
    .line 125
    sub-float/2addr v5, v6

    .line 126
    float-to-double v5, v5

    .line 127
    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    .line 128
    .line 129
    .line 130
    move-result-wide v5

    .line 131
    mul-double v5, v5, v14

    .line 132
    .line 133
    double-to-float v5, v5

    .line 134
    div-float v6, v5, v17

    .line 135
    .line 136
    sub-float v6, v4, v6

    .line 137
    .line 138
    sub-float v7, v4, v12

    .line 139
    .line 140
    iget-object v8, v0, Lcom/samsung/sdraw/ch;->oO80:Lcom/samsung/sdraw/ae;

    .line 141
    .line 142
    invoke-virtual {v8}, Lcom/samsung/sdraw/ae;->〇o00〇〇Oo()F

    .line 143
    .line 144
    .line 145
    move-result v8

    .line 146
    mul-float v8, v8, v17

    .line 147
    .line 148
    sub-float v9, v6, v8

    .line 149
    .line 150
    float-to-int v9, v9

    .line 151
    float-to-int v10, v7

    .line 152
    add-float/2addr v6, v5

    .line 153
    add-float/2addr v6, v8

    .line 154
    float-to-int v8, v6

    .line 155
    add-float v7, v7, v16

    .line 156
    .line 157
    float-to-int v13, v7

    .line 158
    sget-object v14, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    .line 159
    .line 160
    move-object v5, v11

    .line 161
    move v6, v9

    .line 162
    move v7, v10

    .line 163
    move v9, v13

    .line 164
    move-object v10, v14

    .line 165
    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    .line 166
    .line 167
    .line 168
    sub-float v12, v12, v16

    .line 169
    .line 170
    goto :goto_1
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method private Oo08(Landroid/graphics/Canvas;Lcom/samsung/sdraw/cd;)V
    .locals 5

    .line 1
    iget v0, p2, Lcom/samsung/sdraw/cd;->〇0O:F

    .line 2
    .line 3
    float-to-int v0, v0

    .line 4
    invoke-direct {p0, v0}, Lcom/samsung/sdraw/ch;->O8(I)V

    .line 5
    .line 6
    .line 7
    new-instance v1, Ljava/util/Random;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x5

    .line 13
    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    sget-object v2, Lcom/samsung/sdraw/ch;->OO0o〇〇〇〇0:Ljava/util/LinkedHashMap;

    .line 22
    .line 23
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    invoke-virtual {v2, v3}, Ljava/util/AbstractMap;->containsKey(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-eqz v2, :cond_0

    .line 32
    .line 33
    new-instance v2, Landroid/graphics/Region;

    .line 34
    .line 35
    sget-object v3, Lcom/samsung/sdraw/ch;->OO0o〇〇〇〇0:Ljava/util/LinkedHashMap;

    .line 36
    .line 37
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    check-cast v3, Ljava/util/Vector;

    .line 46
    .line 47
    invoke-virtual {v3, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    check-cast v1, Landroid/graphics/Region;

    .line 52
    .line 53
    invoke-direct {v2, v1}, Landroid/graphics/Region;-><init>(Landroid/graphics/Region;)V

    .line 54
    .line 55
    .line 56
    iget v1, p2, Landroid/graphics/PointF;->x:F

    .line 57
    .line 58
    float-to-int v1, v1

    .line 59
    sub-int/2addr v1, v0

    .line 60
    iget p2, p2, Landroid/graphics/PointF;->y:F

    .line 61
    .line 62
    float-to-int p2, p2

    .line 63
    sub-int/2addr p2, v0

    .line 64
    invoke-virtual {v2, v1, p2}, Landroid/graphics/Region;->translate(II)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->clipRegion(Landroid/graphics/Region;)Z

    .line 68
    .line 69
    .line 70
    :cond_0
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method private o〇0(Landroid/graphics/Canvas;Lcom/samsung/sdraw/cd;I)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1, p2}, Lcom/samsung/sdraw/ch;->Oo08(Landroid/graphics/Canvas;Lcom/samsung/sdraw/cd;)V

    .line 5
    .line 6
    .line 7
    iget-object p3, p0, Lcom/samsung/sdraw/ch;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 8
    .line 9
    iget v0, p2, Lcom/samsung/sdraw/cd;->oOo〇8o008:F

    .line 10
    .line 11
    const/high16 v1, 0x437f0000    # 255.0f

    .line 12
    .line 13
    mul-float v0, v0, v1

    .line 14
    .line 15
    const/high16 v1, 0x42f00000    # 120.0f

    .line 16
    .line 17
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    const/high16 v1, 0x42480000    # 50.0f

    .line 22
    .line 23
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    float-to-int v0, v0

    .line 28
    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 29
    .line 30
    .line 31
    iget p3, p2, Landroid/graphics/PointF;->x:F

    .line 32
    .line 33
    iget v0, p2, Landroid/graphics/PointF;->y:F

    .line 34
    .line 35
    iget p2, p2, Lcom/samsung/sdraw/cd;->〇0O:F

    .line 36
    .line 37
    iget-object v1, p0, Lcom/samsung/sdraw/ch;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 38
    .line 39
    invoke-virtual {p1, p3, v0, p2, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method private 〇o〇(Lcom/samsung/sdraw/cd;)Landroid/graphics/RectF;
    .locals 4

    .line 1
    iget v0, p1, Lcom/samsung/sdraw/cd;->〇0O:F

    .line 2
    .line 3
    const/high16 v1, 0x40000000    # 2.0f

    .line 4
    .line 5
    mul-float v1, v1, v0

    .line 6
    .line 7
    float-to-int v1, v1

    .line 8
    new-instance v2, Landroid/graphics/RectF;

    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    int-to-float v1, v1

    .line 12
    invoke-direct {v2, v3, v3, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 13
    .line 14
    .line 15
    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 16
    .line 17
    sub-float/2addr v1, v0

    .line 18
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 19
    .line 20
    sub-float/2addr p1, v0

    .line 21
    invoke-virtual {v2, v1, p1}, Landroid/graphics/RectF;->offset(FF)V

    .line 22
    .line 23
    .line 24
    return-object v2
    .line 25
.end method

.method private 〇〇888()Landroid/graphics/RectF;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/samsung/sdraw/ch;->O8:Landroid/graphics/RectF;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 5
    .line 6
    .line 7
    iget v0, p0, Lcom/samsung/sdraw/ch;->o〇0:I

    .line 8
    .line 9
    :goto_0
    iget v1, p0, Lcom/samsung/sdraw/ch;->〇〇888:I

    .line 10
    .line 11
    if-lt v0, v1, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/samsung/sdraw/ch;->O8:Landroid/graphics/RectF;

    .line 14
    .line 15
    const/high16 v1, -0x40800000    # -1.0f

    .line 16
    .line 17
    invoke-virtual {v0, v1, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/samsung/sdraw/ch;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSprite;->oO80()Landroid/graphics/RectF;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/samsung/sdraw/ch;->O8:Landroid/graphics/RectF;

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/samsung/sdraw/ch;->O8:Landroid/graphics/RectF;

    .line 32
    .line 33
    return-object v0

    .line 34
    :cond_0
    iget-object v1, p0, Lcom/samsung/sdraw/ch;->Oo08:Ljava/util/Vector;

    .line 35
    .line 36
    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    check-cast v1, Lcom/samsung/sdraw/cd;

    .line 41
    .line 42
    iget-object v2, p0, Lcom/samsung/sdraw/ch;->O8:Landroid/graphics/RectF;

    .line 43
    .line 44
    invoke-direct {p0, v1}, Lcom/samsung/sdraw/ch;->〇o〇(Lcom/samsung/sdraw/cd;)Landroid/graphics/RectF;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {v2, v1}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    .line 49
    .line 50
    .line 51
    add-int/lit8 v0, v0, 0x1

    .line 52
    .line 53
    goto :goto_0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method


# virtual methods
.method public a(IZ)Landroid/graphics/RectF;
    .locals 0

    const/4 p2, -0x1

    if-eq p1, p2, :cond_1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 2
    :goto_0
    iput p1, p0, Lcom/samsung/sdraw/ch;->o〇0:I

    .line 3
    iget-object p1, p0, Lcom/samsung/sdraw/ch;->Oo08:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    iput p1, p0, Lcom/samsung/sdraw/ch;->〇〇888:I

    goto :goto_1

    .line 4
    :cond_1
    iget-object p1, p0, Lcom/samsung/sdraw/ch;->Oo08:Ljava/util/Vector;

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result p1

    iput p1, p0, Lcom/samsung/sdraw/ch;->〇〇888:I

    .line 5
    :goto_1
    invoke-direct {p0}, Lcom/samsung/sdraw/ch;->〇〇888()Landroid/graphics/RectF;

    .line 6
    iget-object p1, p0, Lcom/samsung/sdraw/ch;->O8:Landroid/graphics/RectF;

    return-object p1
.end method

.method public a()V
    .locals 0

    .line 1
    return-void
.end method

.method public 〇080(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 5

    .line 1
    iget-object p2, p0, Lcom/samsung/sdraw/ch;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/samsung/sdraw/StrokeSprite;->〇8〇0〇o〇O()Z

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    const/4 v0, 0x0

    .line 8
    if-eqz p2, :cond_0

    .line 9
    .line 10
    iput v0, p0, Lcom/samsung/sdraw/ch;->o〇0:I

    .line 11
    .line 12
    iget-object p2, p0, Lcom/samsung/sdraw/ch;->Oo08:Ljava/util/Vector;

    .line 13
    .line 14
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    iput p2, p0, Lcom/samsung/sdraw/ch;->〇〇888:I

    .line 19
    .line 20
    :cond_0
    iget p2, p0, Lcom/samsung/sdraw/ch;->o〇0:I

    .line 21
    .line 22
    :goto_0
    iget v1, p0, Lcom/samsung/sdraw/ch;->〇〇888:I

    .line 23
    .line 24
    if-lt p2, v1, :cond_4

    .line 25
    .line 26
    iget-object p2, p0, Lcom/samsung/sdraw/ch;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 27
    .line 28
    invoke-virtual {p2}, Lcom/samsung/sdraw/StrokeSprite;->〇8〇0〇o〇O()Z

    .line 29
    .line 30
    .line 31
    move-result p2

    .line 32
    if-eqz p2, :cond_3

    .line 33
    .line 34
    iget-object p2, p0, Lcom/samsung/sdraw/ch;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 35
    .line 36
    invoke-virtual {p2}, Lcom/samsung/sdraw/StrokeSprite;->〇oOO8O8()Z

    .line 37
    .line 38
    .line 39
    move-result p2

    .line 40
    if-eqz p2, :cond_3

    .line 41
    .line 42
    new-instance p2, Landroid/graphics/Paint;

    .line 43
    .line 44
    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    .line 45
    .line 46
    .line 47
    const/4 v1, 0x1

    .line 48
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 49
    .line 50
    .line 51
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 52
    .line 53
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 54
    .line 55
    .line 56
    const/high16 v1, 0x40800000    # 4.0f

    .line 57
    .line 58
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 59
    .line 60
    .line 61
    const v2, -0xff0100

    .line 62
    .line 63
    .line 64
    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    .line 66
    .line 67
    iget-object v2, p0, Lcom/samsung/sdraw/ch;->Oo08:Ljava/util/Vector;

    .line 68
    .line 69
    invoke-virtual {v2}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 74
    .line 75
    .line 76
    move-result v3

    .line 77
    if-nez v3, :cond_2

    .line 78
    .line 79
    const v2, -0xff01

    .line 80
    .line 81
    .line 82
    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 86
    .line 87
    .line 88
    :goto_2
    iget-object v1, p0, Lcom/samsung/sdraw/ch;->Oo08:Ljava/util/Vector;

    .line 89
    .line 90
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    .line 91
    .line 92
    .line 93
    move-result v1

    .line 94
    if-lt v0, v1, :cond_1

    .line 95
    .line 96
    goto :goto_3

    .line 97
    :cond_1
    iget-object v1, p0, Lcom/samsung/sdraw/ch;->Oo08:Ljava/util/Vector;

    .line 98
    .line 99
    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 100
    .line 101
    .line 102
    move-result-object v1

    .line 103
    check-cast v1, Lcom/samsung/sdraw/cd;

    .line 104
    .line 105
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 106
    .line 107
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 108
    .line 109
    invoke-virtual {p1, v2, v1, p2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 110
    .line 111
    .line 112
    add-int/lit8 v0, v0, 0x1

    .line 113
    .line 114
    goto :goto_2

    .line 115
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 116
    .line 117
    .line 118
    move-result-object v3

    .line 119
    check-cast v3, Lcom/samsung/sdraw/cd;

    .line 120
    .line 121
    iget v4, v3, Landroid/graphics/PointF;->x:F

    .line 122
    .line 123
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 124
    .line 125
    invoke-virtual {p1, v4, v3, p2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 126
    .line 127
    .line 128
    goto :goto_1

    .line 129
    :cond_3
    :goto_3
    return-void

    .line 130
    :cond_4
    iget-object v1, p0, Lcom/samsung/sdraw/ch;->Oo08:Ljava/util/Vector;

    .line 131
    .line 132
    invoke-virtual {v1, p2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    check-cast v1, Lcom/samsung/sdraw/cd;

    .line 137
    .line 138
    invoke-direct {p0, p1, v1, p2}, Lcom/samsung/sdraw/ch;->o〇0(Landroid/graphics/Canvas;Lcom/samsung/sdraw/cd;I)V

    .line 139
    .line 140
    .line 141
    add-int/lit8 p2, p2, 0x1

    .line 142
    .line 143
    goto :goto_0
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public 〇o00〇〇Oo(Lcom/samsung/sdraw/StrokeSprite;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/samsung/sdraw/ch;->〇080:Lcom/samsung/sdraw/StrokeSprite;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/samsung/sdraw/StrokeSprite;->OOO〇O0()Lcom/samsung/sdraw/bo;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/samsung/sdraw/ch;->〇o〇:Lcom/samsung/sdraw/bo;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/samsung/sdraw/StrokeSprite;->〇00〇8()Ljava/util/Vector;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iput-object p1, p0, Lcom/samsung/sdraw/ch;->Oo08:Ljava/util/Vector;

    .line 14
    .line 15
    new-instance p1, Landroid/graphics/Paint;

    .line 16
    .line 17
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 18
    .line 19
    .line 20
    iput-object p1, p0, Lcom/samsung/sdraw/ch;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 24
    .line 25
    .line 26
    iget-object p1, p0, Lcom/samsung/sdraw/ch;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/samsung/sdraw/ch;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 32
    .line 33
    iget-object v0, p0, Lcom/samsung/sdraw/ch;->〇o〇:Lcom/samsung/sdraw/bo;

    .line 34
    .line 35
    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 40
    .line 41
    .line 42
    iget-object p1, p0, Lcom/samsung/sdraw/ch;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 43
    .line 44
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 45
    .line 46
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 47
    .line 48
    .line 49
    iget-object p1, p0, Lcom/samsung/sdraw/ch;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 50
    .line 51
    const/high16 v0, 0x3f800000    # 1.0f

    .line 52
    .line 53
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 54
    .line 55
    .line 56
    iget-object p1, p0, Lcom/samsung/sdraw/ch;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 57
    .line 58
    iget-object v1, p0, Lcom/samsung/sdraw/ch;->〇o〇:Lcom/samsung/sdraw/bo;

    .line 59
    .line 60
    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 65
    .line 66
    .line 67
    iget-object p1, p0, Lcom/samsung/sdraw/ch;->〇o00〇〇Oo:Landroid/graphics/Paint;

    .line 68
    .line 69
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    .line 70
    .line 71
    sget-object v2, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    .line 72
    .line 73
    invoke-direct {v1, v0, v2}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 77
    .line 78
    .line 79
    sget-object p1, Lcom/samsung/sdraw/ch;->〇80〇808〇O:Ljava/util/LinkedHashMap;

    .line 80
    .line 81
    if-nez p1, :cond_0

    .line 82
    .line 83
    new-instance p1, Ljava/util/LinkedHashMap;

    .line 84
    .line 85
    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 86
    .line 87
    .line 88
    sput-object p1, Lcom/samsung/sdraw/ch;->〇80〇808〇O:Ljava/util/LinkedHashMap;

    .line 89
    .line 90
    :cond_0
    sget-object p1, Lcom/samsung/sdraw/ch;->OO0o〇〇〇〇0:Ljava/util/LinkedHashMap;

    .line 91
    .line 92
    if-nez p1, :cond_1

    .line 93
    .line 94
    new-instance p1, Ljava/util/LinkedHashMap;

    .line 95
    .line 96
    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 97
    .line 98
    .line 99
    sput-object p1, Lcom/samsung/sdraw/ch;->OO0o〇〇〇〇0:Ljava/util/LinkedHashMap;

    .line 100
    .line 101
    :cond_1
    new-instance p1, Landroid/graphics/RectF;

    .line 102
    .line 103
    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 104
    .line 105
    .line 106
    iput-object p1, p0, Lcom/samsung/sdraw/ch;->O8:Landroid/graphics/RectF;

    .line 107
    .line 108
    return-void
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

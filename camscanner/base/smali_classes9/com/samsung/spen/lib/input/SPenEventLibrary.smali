.class public Lcom/samsung/spen/lib/input/SPenEventLibrary;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field 〇080:Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇080:Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static 〇080(Landroid/content/Context;)Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇o00〇〇Oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    if-nez p0, :cond_1

    .line 10
    .line 11
    return v1

    .line 12
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    .line 13
    .line 14
    .line 15
    move-result-object p0

    .line 16
    if-eqz p0, :cond_2

    .line 17
    .line 18
    const-string v0, "com.sec.feature.hovering_ui"

    .line 19
    .line 20
    invoke-virtual {p0, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    .line 21
    .line 22
    .line 23
    move-result p0

    .line 24
    return p0

    .line 25
    :cond_2
    return v1
.end method

.method public static 〇o00〇〇Oo()Z
    .locals 2

    .line 1
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "4."

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public O8(Landroid/content/Context;)Z
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return p1

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇080:Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;

    .line 6
    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 10
    .line 11
    .line 12
    :cond_1
    const/4 p1, 0x0

    .line 13
    iput-object p1, p0, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇080:Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;

    .line 14
    .line 15
    const/4 p1, 0x1

    .line 16
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇o〇(Landroid/content/Context;Lcom/samsung/spensdk/applistener/SPenDetachmentListener;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    if-nez p2, :cond_1

    .line 6
    .line 7
    return v0

    .line 8
    :cond_1
    iget-object v0, p0, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇080:Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;

    .line 9
    .line 10
    if-nez v0, :cond_2

    .line 11
    .line 12
    new-instance v0, Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;

    .line 13
    .line 14
    invoke-direct {v0}, Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;-><init>()V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇080:Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;

    .line 18
    .line 19
    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    .line 20
    .line 21
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v1, "com.samsung.pen.INSERT"

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object v1, p0, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇080:Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;

    .line 30
    .line 31
    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇080:Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;

    .line 35
    .line 36
    invoke-virtual {p1, p2}, Lcom/samsung/spen/lib/input/core/SPenDetachmentReceiver;->〇080(Lcom/samsung/spensdk/applistener/SPenDetachmentListener;)V

    .line 37
    .line 38
    .line 39
    const/4 p1, 0x1

    .line 40
    return p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

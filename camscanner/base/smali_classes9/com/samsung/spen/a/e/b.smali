.class public Lcom/samsung/spen/a/e/b;
.super Lcom/samsung/sdraw/CanvasView;
.source "SourceFile"

# interfaces
.implements Lcom/samsung/spen/a/e/c;


# instance fields
.field private O888Oo:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

.field private OO〇000:Z

.field O〇8:Lcom/samsung/sdraw/CanvasView$OnObjectListener;

.field private O〇8O0O80〇:Lcom/samsung/sdraw/CanvasView$OnDropperColorChangeListener;

.field O〇O800oo:Lcom/samsung/sdraw/CanvasView$OnSettingViewShowListener;

.field O〇oo8O80:Lcom/samsung/sdraw/AbstractSettingView$OnSettingChangedListener;

.field private o8o0:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

.field private oO88〇0O8O:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

.field private oo8:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

.field private oo88:Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;

.field private oo8〇〇:Lcom/samsung/sdraw/SettingView;

.field private oooO8〇00:Lcom/samsung/sdraw/CanvasView$OnLongPressListener;

.field private ooooo0O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

.field private 〇0888:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

.field private 〇80O:Lcom/samsung/sdraw/CanvasView$OnCanvasMatrixChangeListener;

.field private 〇8O:Lcom/samsung/spen/a/g/b;

.field private 〇8oo0oO0:Lcom/samsung/sdraw/CanvasView$OnModeChangedListener;

.field private 〇8ooOO:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

.field private 〇8ooo:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

.field private 〇8oo〇〇oO:Z

.field private 〇8〇0O〇:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

.field private 〇o8〇8:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

.field private 〇〇0:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/samsung/sdraw/CanvasView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    .line 2
    iput-boolean p1, p0, Lcom/samsung/spen/a/e/b;->〇8oo〇〇oO:Z

    .line 3
    iput-boolean p1, p0, Lcom/samsung/spen/a/e/b;->OO〇000:Z

    const/4 p1, 0x0

    .line 4
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 5
    new-instance p1, Lcom/samsung/spen/a/e/b$1;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$1;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇80O:Lcom/samsung/sdraw/CanvasView$OnCanvasMatrixChangeListener;

    .line 6
    new-instance p1, Lcom/samsung/spen/a/e/b$12;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$12;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8oo0oO0:Lcom/samsung/sdraw/CanvasView$OnModeChangedListener;

    .line 7
    new-instance p1, Lcom/samsung/spen/a/e/b$15;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$15;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->O〇O800oo:Lcom/samsung/sdraw/CanvasView$OnSettingViewShowListener;

    .line 8
    new-instance p1, Lcom/samsung/spen/a/e/b$16;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$16;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oooO8〇00:Lcom/samsung/sdraw/CanvasView$OnLongPressListener;

    .line 9
    new-instance p1, Lcom/samsung/spen/a/e/b$17;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$17;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->O〇8O0O80〇:Lcom/samsung/sdraw/CanvasView$OnDropperColorChangeListener;

    .line 10
    new-instance p1, Lcom/samsung/spen/a/e/b$18;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$18;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oo88:Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;

    .line 11
    new-instance p1, Lcom/samsung/spen/a/e/b$19;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$19;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->O〇8:Lcom/samsung/sdraw/CanvasView$OnObjectListener;

    .line 12
    new-instance p1, Lcom/samsung/spen/a/e/b$20;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$20;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->O〇oo8O80:Lcom/samsung/sdraw/AbstractSettingView$OnSettingChangedListener;

    .line 13
    new-instance p1, Lcom/samsung/spen/a/e/b$21;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$21;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oo8:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

    .line 14
    new-instance p1, Lcom/samsung/spen/a/e/b$2;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$2;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->ooooo0O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

    .line 15
    new-instance p1, Lcom/samsung/spen/a/e/b$3;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$3;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇0888:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

    .line 16
    new-instance p1, Lcom/samsung/spen/a/e/b$4;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$4;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8〇0O〇:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

    .line 17
    new-instance p1, Lcom/samsung/spen/a/e/b$5;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$5;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->o8o0:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

    .line 18
    new-instance p1, Lcom/samsung/spen/a/e/b$6;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$6;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇o8〇8:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

    .line 19
    new-instance p1, Lcom/samsung/spen/a/e/b$7;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$7;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇〇0:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

    .line 20
    new-instance p1, Lcom/samsung/spen/a/e/b$8;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$8;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->O888Oo:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

    .line 21
    new-instance p1, Lcom/samsung/spen/a/e/b$9;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$9;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8ooo:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

    .line 22
    new-instance p1, Lcom/samsung/spen/a/e/b$10;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$10;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oO88〇0O8O:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

    .line 23
    new-instance p1, Lcom/samsung/spen/a/e/b$11;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$11;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8ooOO:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

    .line 24
    new-instance p1, Lcom/samsung/spen/a/e/b$13;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$13;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8O:Lcom/samsung/spen/a/g/b;

    .line 25
    invoke-virtual {p0}, Lcom/samsung/spen/a/e/b;->O〇08oOOO0()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/sdraw/CanvasView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x1

    .line 27
    iput-boolean p1, p0, Lcom/samsung/spen/a/e/b;->〇8oo〇〇oO:Z

    .line 28
    iput-boolean p1, p0, Lcom/samsung/spen/a/e/b;->OO〇000:Z

    const/4 p1, 0x0

    .line 29
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 30
    new-instance p1, Lcom/samsung/spen/a/e/b$1;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$1;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇80O:Lcom/samsung/sdraw/CanvasView$OnCanvasMatrixChangeListener;

    .line 31
    new-instance p1, Lcom/samsung/spen/a/e/b$12;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$12;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8oo0oO0:Lcom/samsung/sdraw/CanvasView$OnModeChangedListener;

    .line 32
    new-instance p1, Lcom/samsung/spen/a/e/b$15;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$15;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->O〇O800oo:Lcom/samsung/sdraw/CanvasView$OnSettingViewShowListener;

    .line 33
    new-instance p1, Lcom/samsung/spen/a/e/b$16;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$16;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oooO8〇00:Lcom/samsung/sdraw/CanvasView$OnLongPressListener;

    .line 34
    new-instance p1, Lcom/samsung/spen/a/e/b$17;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$17;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->O〇8O0O80〇:Lcom/samsung/sdraw/CanvasView$OnDropperColorChangeListener;

    .line 35
    new-instance p1, Lcom/samsung/spen/a/e/b$18;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$18;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oo88:Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;

    .line 36
    new-instance p1, Lcom/samsung/spen/a/e/b$19;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$19;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->O〇8:Lcom/samsung/sdraw/CanvasView$OnObjectListener;

    .line 37
    new-instance p1, Lcom/samsung/spen/a/e/b$20;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$20;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->O〇oo8O80:Lcom/samsung/sdraw/AbstractSettingView$OnSettingChangedListener;

    .line 38
    new-instance p1, Lcom/samsung/spen/a/e/b$21;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$21;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oo8:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

    .line 39
    new-instance p1, Lcom/samsung/spen/a/e/b$2;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$2;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->ooooo0O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

    .line 40
    new-instance p1, Lcom/samsung/spen/a/e/b$3;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$3;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇0888:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

    .line 41
    new-instance p1, Lcom/samsung/spen/a/e/b$4;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$4;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8〇0O〇:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

    .line 42
    new-instance p1, Lcom/samsung/spen/a/e/b$5;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$5;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->o8o0:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

    .line 43
    new-instance p1, Lcom/samsung/spen/a/e/b$6;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$6;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇o8〇8:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

    .line 44
    new-instance p1, Lcom/samsung/spen/a/e/b$7;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$7;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇〇0:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

    .line 45
    new-instance p1, Lcom/samsung/spen/a/e/b$8;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$8;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->O888Oo:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

    .line 46
    new-instance p1, Lcom/samsung/spen/a/e/b$9;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$9;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8ooo:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

    .line 47
    new-instance p1, Lcom/samsung/spen/a/e/b$10;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$10;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oO88〇0O8O:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

    .line 48
    new-instance p1, Lcom/samsung/spen/a/e/b$11;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$11;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8ooOO:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

    .line 49
    new-instance p1, Lcom/samsung/spen/a/e/b$13;

    invoke-direct {p1, p0}, Lcom/samsung/spen/a/e/b$13;-><init>(Lcom/samsung/spen/a/e/b;)V

    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8O:Lcom/samsung/spen/a/g/b;

    .line 50
    invoke-virtual {p0}, Lcom/samsung/spen/a/e/b;->O〇08oOOO0()V

    return-void
.end method

.method static synthetic O88O(Lcom/samsung/spen/a/e/b;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/samsung/spen/a/e/b;->OO〇000:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic Oo80(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spensdk/applistener/SettingTextChangeListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->oO88〇0O8O:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic O〇o88o08〇(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->〇8ooOO:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private o08oOO()Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    invoke-static {}, Lcom/samsung/spen/lib/input/core/a;->〇o00〇〇Oo()Lcom/samsung/spen/lib/input/core/a;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    const/4 v2, 0x1

    .line 7
    invoke-virtual {v1, v2}, Lcom/samsung/spen/lib/input/core/a;->〇o〇(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    .line 9
    .line 10
    return v2

    .line 11
    :catch_0
    move-exception v1

    .line 12
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 13
    .line 14
    .line 15
    return v0

    .line 16
    :catch_1
    move-exception v1

    .line 17
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 18
    .line 19
    .line 20
    return v0

    .line 21
    :catch_2
    move-exception v1

    .line 22
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 23
    .line 24
    .line 25
    return v0

    .line 26
    :catch_3
    move-exception v1

    .line 27
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 28
    .line 29
    .line 30
    return v0

    .line 31
    :catch_4
    move-exception v1

    .line 32
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 33
    .line 34
    .line 35
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method static synthetic o8o(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spensdk/applistener/SObjectUpdateListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->〇0888:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic o8oOOo(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->o8o0:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic o8〇OO0〇0o(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->ooooo0O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic oOO〇〇(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spen/a/g/b;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->〇8O:Lcom/samsung/spen/a/g/b;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic oOo0(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->oo8:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic oo8ooo8O(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spensdk/applistener/SObjectSelectListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->〇8〇0O〇:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic o〇oO(Lcom/samsung/spen/a/e/b;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/samsung/spen/a/e/b;->〇8oo〇〇oO:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private o〇〇0〇88()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/samsung/spen/a/e/b;->O〇oo8O80:Lcom/samsung/sdraw/AbstractSettingView$OnSettingChangedListener;

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Lcom/samsung/sdraw/AbstractSettingView;->setOnSettingChangedListener(Lcom/samsung/sdraw/AbstractSettingView$OnSettingChangedListener;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-static {v0}, Lcom/samsung/spen/lib/input/SPenEventLibrary;->〇080(Landroid/content/Context;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 22
    .line 23
    new-instance v1, Lcom/samsung/spen/a/e/b$14;

    .line 24
    .line 25
    invoke-direct {v1, p0}, Lcom/samsung/spen/a/e/b$14;-><init>(Lcom/samsung/spen/a/e/b;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 29
    .line 30
    .line 31
    :cond_1
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method static synthetic 〇00O0(Lcom/samsung/spen/a/e/b;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/samsung/spen/a/e/b;->o08oOO()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇08〇o0O(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->〇8ooo:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇8〇oO〇〇8o(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spensdk/applistener/SettingViewShowListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->O888Oo:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇O〇〇O8(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spensdk/applistener/HistoryUpdateListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->〇o8〇8:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇o0O(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spen/engine/signature/b;
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2
    .line 3
    .line 4
    const/4 p0, 0x0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇〇08O(Lcom/samsung/spen/a/e/b;)Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/samsung/spen/a/e/b;->〇〇0:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic 〇〇o〇(Lcom/samsung/spen/a/e/b;)Lcom/samsung/sdraw/PenSettingInfo;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/samsung/sdraw/CanvasView;->getPenSettingViewInfo()Lcom/samsung/sdraw/PenSettingInfo;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public O0(Lcom/samsung/spensdk/applistener/SObjectSelectListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8〇0O〇:Lcom/samsung/spensdk/applistener/SObjectSelectListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public O000()Lcom/samsung/spen/settings/SettingFillingInfo;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/samsung/sdraw/CanvasView;->getFillingSettingInfo()Lcom/samsung/sdraw/FillingSettingInfo;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return-object v0

    .line 9
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/sdraw/FillingSettingInfo;->〇〇888()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    new-instance v1, Lcom/samsung/spen/settings/SettingFillingInfo;

    .line 14
    .line 15
    invoke-direct {v1}, Lcom/samsung/spen/settings/SettingFillingInfo;-><init>()V

    .line 16
    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    invoke-virtual {v1, v2}, Lcom/samsung/spen/settings/SettingFillingInfo;->〇o〇(I)Z

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v0}, Lcom/samsung/spen/settings/SettingFillingInfo;->〇o00〇〇Oo(I)V

    .line 23
    .line 24
    .line 25
    return-object v1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public O08000(Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->ooooo0O:Lcom/samsung/spensdk/applistener/SCanvasModeChangedListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public O0O(Lcom/samsung/samm/common/SObject;Z)Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/samsung/spen/a/e/b;->〇8O:Lcom/samsung/spen/a/g/b;

    .line 6
    .line 7
    invoke-interface {v1, p1}, Lcom/samsung/spen/a/g/b;->〇o00〇〇Oo(Lcom/samsung/samm/common/SObject;)I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, -0x1

    .line 12
    if-ne v1, v2, :cond_1

    .line 13
    .line 14
    return v0

    .line 15
    :cond_1
    invoke-static {p1, v1}, Lcom/samsung/spen/a/e/a;->O8(Lcom/samsung/samm/common/SObject;I)Lcom/samsung/sdraw/ObjectInfo;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    if-nez p1, :cond_2

    .line 20
    .line 21
    return v0

    .line 22
    :cond_2
    instance-of v1, p1, Lcom/samsung/sdraw/StrokeInfo;

    .line 23
    .line 24
    if-eqz v1, :cond_3

    .line 25
    .line 26
    check-cast p1, Lcom/samsung/sdraw/StrokeInfo;

    .line 27
    .line 28
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/CanvasView;->〇008〇o0〇〇(Lcom/samsung/sdraw/StrokeInfo;)Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    return p1

    .line 33
    :cond_3
    instance-of v1, p1, Lcom/samsung/sdraw/ImageInfo;

    .line 34
    .line 35
    if-eqz v1, :cond_4

    .line 36
    .line 37
    check-cast p1, Lcom/samsung/sdraw/ImageInfo;

    .line 38
    .line 39
    invoke-virtual {p0, p1, p2}, Lcom/samsung/sdraw/CanvasView;->〇080O0(Lcom/samsung/sdraw/ImageInfo;Z)Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    return p1

    .line 44
    :cond_4
    instance-of v1, p1, Lcom/samsung/sdraw/TextInfo;

    .line 45
    .line 46
    if-eqz v1, :cond_5

    .line 47
    .line 48
    check-cast p1, Lcom/samsung/sdraw/TextInfo;

    .line 49
    .line 50
    invoke-virtual {p0, p1, p2}, Lcom/samsung/sdraw/CanvasView;->Oo〇(Lcom/samsung/sdraw/TextInfo;Z)Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    return p1

    .line 55
    :cond_5
    instance-of p2, p1, Lcom/samsung/sdraw/FillColorInfo;

    .line 56
    .line 57
    if-eqz p2, :cond_6

    .line 58
    .line 59
    check-cast p1, Lcom/samsung/sdraw/FillColorInfo;

    .line 60
    .line 61
    invoke-virtual {p0, p1}, Lcom/samsung/sdraw/CanvasView;->OoOOo8(Lcom/samsung/sdraw/FillColorInfo;)Z

    .line 62
    .line 63
    .line 64
    move-result p1

    .line 65
    return p1

    .line 66
    :cond_6
    return v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method public O0O8OO088(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setRestoreEnable(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public O0o〇〇Oo()Landroid/view/ViewGroup;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public O8()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getPanningMode()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public O8O〇(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/HashMap;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/samsung/sdraw/SettingView;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3}, Lcom/samsung/sdraw/SettingView;-><init>(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/HashMap;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/samsung/spen/a/e/b;->o〇〇0〇88()V

    .line 9
    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public O8〇o(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setUsingHistoricalEventForStroke(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public OO0o〇〇(IFI)Z
    .locals 3

    .line 1
    new-instance v0, Lcom/samsung/sdraw/PenSettingInfo;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/samsung/sdraw/PenSettingInfo;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {p1}, Lcom/samsung/spen/a/e/a;->OO0o〇〇(I)I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/PenSettingInfo;->〇O888o0o(I)V

    .line 11
    .line 12
    .line 13
    float-to-int p2, p2

    .line 14
    const/4 v1, 0x1

    .line 15
    if-gtz p2, :cond_0

    .line 16
    .line 17
    const/4 p2, 0x1

    .line 18
    :cond_0
    const/4 v2, 0x4

    .line 19
    if-ne p1, v2, :cond_2

    .line 20
    .line 21
    const/16 v2, 0x45

    .line 22
    .line 23
    if-le p2, v2, :cond_1

    .line 24
    .line 25
    const/16 p2, 0x45

    .line 26
    .line 27
    :cond_1
    invoke-virtual {v0, p2}, Lcom/samsung/sdraw/PenSettingInfo;->oo88o8O(I)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, p2}, Lcom/samsung/sdraw/PenSettingInfo;->〇O00(I)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_2
    const/16 v2, 0x48

    .line 35
    .line 36
    if-le p2, v2, :cond_3

    .line 37
    .line 38
    const/16 p2, 0x48

    .line 39
    .line 40
    :cond_3
    invoke-virtual {v0, p1, p2}, Lcom/samsung/sdraw/PenSettingInfo;->〇oo〇(II)V

    .line 41
    .line 42
    .line 43
    :goto_0
    shr-int/lit8 p2, p3, 0x18

    .line 44
    .line 45
    and-int/lit16 p2, p2, 0xff

    .line 46
    .line 47
    invoke-virtual {v0, p2}, Lcom/samsung/sdraw/PenSettingInfo;->〇〇8O0〇8(I)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0, p1, p2}, Lcom/samsung/sdraw/PenSettingInfo;->〇0〇O0088o(II)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, p3}, Lcom/samsung/sdraw/PenSettingInfo;->OoO8(I)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, p1, p3}, Lcom/samsung/sdraw/PenSettingInfo;->o800o8O(II)V

    .line 57
    .line 58
    .line 59
    invoke-super {p0, v0}, Lcom/samsung/sdraw/CanvasView;->setPenSettingInfo(Lcom/samsung/sdraw/PenSettingInfo;)V

    .line 60
    .line 61
    .line 62
    return v1
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public OO0o〇〇〇〇0()Lcom/samsung/spen/settings/SettingStrokeInfo;
    .locals 5

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getPenSettingViewInfo()Lcom/samsung/sdraw/PenSettingInfo;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return-object v0

    .line 9
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/sdraw/PenSettingInfo;->Oooo8o0〇()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {v0}, Lcom/samsung/sdraw/PenSettingInfo;->〇O8o08O()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    invoke-virtual {v0}, Lcom/samsung/sdraw/PenSettingInfo;->〇80〇808〇O()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    shl-int/lit8 v3, v3, 0x18

    .line 22
    .line 23
    const v4, 0xffffff

    .line 24
    .line 25
    .line 26
    and-int/2addr v2, v4

    .line 27
    or-int/2addr v2, v3

    .line 28
    const/4 v3, 0x4

    .line 29
    if-ne v1, v3, :cond_1

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/samsung/sdraw/PenSettingInfo;->oO80()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    goto :goto_0

    .line 36
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/sdraw/PenSettingInfo;->〇〇808〇()I

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    :goto_0
    invoke-static {v1}, Lcom/samsung/spen/a/e/a;->Oooo8o0〇(I)I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    new-instance v3, Lcom/samsung/spen/settings/SettingStrokeInfo;

    .line 45
    .line 46
    invoke-direct {v3}, Lcom/samsung/spen/settings/SettingStrokeInfo;-><init>()V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v3, v1}, Lcom/samsung/spen/settings/SettingStrokeInfo;->Oo08(I)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v3, v2}, Lcom/samsung/spen/settings/SettingStrokeInfo;->O8(I)V

    .line 53
    .line 54
    .line 55
    int-to-float v0, v0

    .line 56
    invoke-virtual {v3, v0}, Lcom/samsung/spen/settings/SettingStrokeInfo;->o〇0(F)V

    .line 57
    .line 58
    .line 59
    return-object v3
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public OO8oO0o〇(IFFFIJJ)Z
    .locals 10

    .line 1
    invoke-static {p5}, Lcom/samsung/spen/a/e/a;->〇O〇(I)I

    .line 2
    .line 3
    .line 4
    move-result v5

    .line 5
    move-object v0, p0

    .line 6
    move v1, p1

    .line 7
    move v2, p2

    .line 8
    move v3, p3

    .line 9
    move v4, p4

    .line 10
    move-wide/from16 v6, p6

    .line 11
    .line 12
    move-wide/from16 v8, p8

    .line 13
    .line 14
    invoke-super/range {v0 .. v9}, Lcom/samsung/sdraw/CanvasView;->Ooo8(IFFFIJJ)V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
.end method

.method public OOO(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setPanEnable(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method OO〇00〇8oO()V
    .locals 4

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getSelectedObjectInfos()Ljava/util/LinkedList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-nez v1, :cond_2

    .line 17
    .line 18
    return-void

    .line 19
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    check-cast v1, Lcom/samsung/sdraw/ObjectInfo;

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/samsung/sdraw/ObjectInfo;->〇080()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    if-gez v2, :cond_5

    .line 30
    .line 31
    invoke-static {v1}, Lcom/samsung/spen/a/e/a;->〇o〇(Lcom/samsung/sdraw/ObjectInfo;)Lcom/samsung/samm/common/SObject;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    if-nez v2, :cond_3

    .line 36
    .line 37
    return-void

    .line 38
    :cond_3
    iget-object v3, p0, Lcom/samsung/spen/a/e/b;->〇8O:Lcom/samsung/spen/a/g/b;

    .line 39
    .line 40
    invoke-interface {v3, v2}, Lcom/samsung/spen/a/g/b;->〇o〇(Lcom/samsung/samm/common/SObject;)I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    if-gez v2, :cond_4

    .line 45
    .line 46
    return-void

    .line 47
    :cond_4
    invoke-virtual {v1, v2}, Lcom/samsung/sdraw/ObjectInfo;->〇o〇(I)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_5
    iget-object v3, p0, Lcom/samsung/spen/a/e/b;->〇8O:Lcom/samsung/spen/a/g/b;

    .line 52
    .line 53
    invoke-interface {v3, v2}, Lcom/samsung/spen/a/g/b;->a(I)Lcom/samsung/samm/common/SObject;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    if-nez v2, :cond_6

    .line 58
    .line 59
    return-void

    .line 60
    :cond_6
    invoke-static {v1, v2}, Lcom/samsung/spen/a/e/a;->〇〇888(Lcom/samsung/sdraw/ObjectInfo;Lcom/samsung/samm/common/SObject;)Z

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    if-nez v1, :cond_1

    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public Oo08(Z)Z
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setMultiTouchCancel(Z)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x1

    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public Oo8Oo00oo()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/samsung/spen/a/e/b;->OO〇00〇8oO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public OoO8(Ljava/lang/Object;)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    check-cast p1, Landroid/view/View$OnHoverListener;

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 6
    .line 7
    .line 8
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setOnSelectedTextViewHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 14
    .line 15
    .line 16
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setOnSelectedTextViewHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 17
    .line 18
    .line 19
    :goto_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public Ooo(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setHistoricalOperationSupport(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public Oo〇O()Lcom/samsung/spen/settings/SettingTextInfo;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/samsung/sdraw/CanvasView;->getTextSettingViewInfo()Lcom/samsung/sdraw/TextSettingInfo;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return-object v0

    .line 9
    :cond_0
    new-instance v1, Lcom/samsung/spen/settings/SettingTextInfo;

    .line 10
    .line 11
    invoke-direct {v1}, Lcom/samsung/spen/settings/SettingTextInfo;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/samsung/sdraw/TextSettingInfo;->OO0o〇〇〇〇0()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    invoke-static {v2}, Lcom/samsung/spen/a/e/a;->〇0〇O0088o(I)I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    invoke-virtual {v1, v2}, Lcom/samsung/spen/settings/SettingTextInfo;->O8(I)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/samsung/sdraw/TextSettingInfo;->〇〇888()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    invoke-virtual {v1, v2}, Lcom/samsung/spen/settings/SettingTextInfo;->〇080(I)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/samsung/sdraw/TextSettingInfo;->〇80〇808〇O()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    int-to-float v2, v2

    .line 37
    invoke-virtual {v1, v2}, Lcom/samsung/spen/settings/SettingTextInfo;->〇o〇(F)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/samsung/sdraw/TextSettingInfo;->oO80()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {v1, v0}, Lcom/samsung/spen/settings/SettingTextInfo;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-object v1
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public O〇08oOOO0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->〇80O:Lcom/samsung/sdraw/CanvasView$OnCanvasMatrixChangeListener;

    .line 2
    .line 3
    invoke-super {p0, v0}, Lcom/samsung/sdraw/CanvasView;->setOnCanvasMatrixChangeListener(Lcom/samsung/sdraw/CanvasView$OnCanvasMatrixChangeListener;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->〇8oo0oO0:Lcom/samsung/sdraw/CanvasView$OnModeChangedListener;

    .line 7
    .line 8
    invoke-super {p0, v0}, Lcom/samsung/sdraw/CanvasView;->setOnModeChangedListener(Lcom/samsung/sdraw/CanvasView$OnModeChangedListener;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->O〇8:Lcom/samsung/sdraw/CanvasView$OnObjectListener;

    .line 12
    .line 13
    invoke-super {p0, v0}, Lcom/samsung/sdraw/CanvasView;->setOnObjectListener(Lcom/samsung/sdraw/CanvasView$OnObjectListener;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->O〇O800oo:Lcom/samsung/sdraw/CanvasView$OnSettingViewShowListener;

    .line 17
    .line 18
    invoke-super {p0, v0}, Lcom/samsung/sdraw/CanvasView;->setOnSettingViewShowListener(Lcom/samsung/sdraw/CanvasView$OnSettingViewShowListener;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->O〇8O0O80〇:Lcom/samsung/sdraw/CanvasView$OnDropperColorChangeListener;

    .line 22
    .line 23
    invoke-super {p0, v0}, Lcom/samsung/sdraw/CanvasView;->setOnDropperColorChangeListener(Lcom/samsung/sdraw/CanvasView$OnDropperColorChangeListener;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->oo88:Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;

    .line 27
    .line 28
    invoke-super {p0, v0}, Lcom/samsung/sdraw/CanvasView;->setOnHistoryChangeListener(Lcom/samsung/sdraw/CanvasView$OnHistoryChangeListener;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->oooO8〇00:Lcom/samsung/sdraw/CanvasView$OnLongPressListener;

    .line 32
    .line 33
    invoke-super {p0, v0}, Lcom/samsung/sdraw/CanvasView;->setOnLongPressListener(Lcom/samsung/sdraw/CanvasView$OnLongPressListener;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public O〇8O8〇008(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setRemoveLongPressStroke(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public O〇O〇oO(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setTextLongClickSelectOption(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public getCanvasViewObjectNum()I
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getObjectInfos()Ljava/util/LinkedList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return v0

    .line 9
    :cond_0
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o0O0(Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8ooOO:Lcom/samsung/spensdk/applistener/SettingFillingChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public o0ooO()Lcom/samsung/spen/settings/SettingFillingInfo;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/samsung/sdraw/CanvasView;->getFillingSettingViewInfo()Lcom/samsung/sdraw/FillingSettingInfo;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return-object v0

    .line 9
    :cond_0
    new-instance v1, Lcom/samsung/spen/settings/SettingFillingInfo;

    .line 10
    .line 11
    invoke-direct {v1}, Lcom/samsung/spen/settings/SettingFillingInfo;-><init>()V

    .line 12
    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-virtual {v1, v2}, Lcom/samsung/spen/settings/SettingFillingInfo;->〇o〇(I)Z

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/samsung/sdraw/FillingSettingInfo;->〇〇888()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    invoke-virtual {v1, v0}, Lcom/samsung/spen/settings/SettingFillingInfo;->〇o00〇〇Oo(I)V

    .line 23
    .line 24
    .line 25
    return-object v1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public o8(Lcom/samsung/spensdk/applistener/HistoryUpdateListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇o8〇8:Lcom/samsung/spensdk/applistener/HistoryUpdateListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public o800o8O()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getTouchEventDispatchMode()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o88〇OO08〇()Lcom/samsung/spen/settings/SettingTextInfo;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/samsung/sdraw/CanvasView;->getTextSettingInfo()Lcom/samsung/sdraw/TextSettingInfo;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return-object v0

    .line 9
    :cond_0
    new-instance v1, Lcom/samsung/spen/settings/SettingTextInfo;

    .line 10
    .line 11
    invoke-direct {v1}, Lcom/samsung/spen/settings/SettingTextInfo;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/samsung/sdraw/TextSettingInfo;->OO0o〇〇〇〇0()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    invoke-static {v2}, Lcom/samsung/spen/a/e/a;->〇0〇O0088o(I)I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    invoke-virtual {v1, v2}, Lcom/samsung/spen/settings/SettingTextInfo;->O8(I)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/samsung/sdraw/TextSettingInfo;->〇〇888()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    invoke-virtual {v1, v2}, Lcom/samsung/spen/settings/SettingTextInfo;->〇080(I)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/samsung/sdraw/TextSettingInfo;->〇80〇808〇O()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    int-to-float v2, v2

    .line 37
    invoke-virtual {v1, v2}, Lcom/samsung/spen/settings/SettingTextInfo;->〇o〇(F)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/samsung/sdraw/TextSettingInfo;->oO80()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {v1, v0}, Lcom/samsung/spen/settings/SettingTextInfo;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-object v1
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public o8O〇()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getTextLongClickSelectOption()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o8oO〇(Z)Z
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setPanningMode(Z)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x1

    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public oO(FFZ)Z
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/sdraw/CanvasView;->〇OO0(FFZ)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x1

    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public oO00OOO(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setEraserCursorVisible(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public oO80(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setZoomEnable(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public oo88o8O(Ljava/lang/Object;)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    check-cast p1, Landroid/view/View$OnTouchListener;

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 6
    .line 7
    .line 8
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setOnSelectedTextViewTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 14
    .line 15
    .line 16
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setOnSelectedTextViewTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 17
    .line 18
    .line 19
    :goto_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public ooo0〇〇O(Lcom/samsung/samm/common/SObject;)Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const-string v1, "SObjectIDKey"

    .line 6
    .line 7
    const/4 v2, -0x1

    .line 8
    invoke-virtual {p1, v1, v2}, Lcom/samsung/samm/common/SObject;->〇80〇808〇O(Ljava/lang/String;I)I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-ne v1, v2, :cond_1

    .line 13
    .line 14
    return v0

    .line 15
    :cond_1
    invoke-static {p1, v1}, Lcom/samsung/spen/a/e/a;->O8(Lcom/samsung/samm/common/SObject;I)Lcom/samsung/sdraw/ObjectInfo;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    if-nez p1, :cond_2

    .line 20
    .line 21
    return v0

    .line 22
    :cond_2
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->o88O8(Lcom/samsung/sdraw/ObjectInfo;)Z

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    return p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public ooo〇8oO(Lcom/samsung/spensdk/applistener/SettingViewShowListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->O888Oo:Lcom/samsung/spensdk/applistener/SettingViewShowListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public oo〇(Lcom/samsung/spensdk/applistener/SettingTextChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oO88〇0O8O:Lcom/samsung/spensdk/applistener/SettingTextChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public o〇0()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->〇〇o0o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o〇0OOo〇0(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setDropperMode(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public o〇8(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setTouchEventDispatchMode(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public o〇8oOO88()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getPanEnable()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o〇O(IZ)Z
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/samsung/spen/a/h/d;->〇〇808〇(I)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    return p1

    .line 9
    :cond_0
    const/4 v0, 0x1

    .line 10
    if-eqz p2, :cond_1

    .line 11
    .line 12
    invoke-static {p1}, Lcom/samsung/spen/a/e/a;->〇8o8o〇(I)I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    iget-object p2, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 17
    .line 18
    invoke-virtual {p2, p1}, Lcom/samsung/sdraw/AbstractSettingView;->o〇8〇(I)V

    .line 19
    .line 20
    .line 21
    return v0

    .line 22
    :cond_1
    iget-object p1, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/samsung/sdraw/AbstractSettingView;->OOO()V

    .line 25
    .line 26
    .line 27
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public o〇O8〇〇o()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getDropperMode()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public o〇〇0〇(Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->o8o0:Lcom/samsung/spensdk/applistener/ColorPickerColorChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇0()F
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getScale()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇00(Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8ooo:Lcom/samsung/spensdk/applistener/SettingStrokeChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇0000OOO(Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oo8:Lcom/samsung/spensdk/applistener/SCanvasMatrixChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇00〇8()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->〇0O00oO()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇080()I
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getMode()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {v0}, Lcom/samsung/spen/a/e/a;->OO0o〇〇〇〇0(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇08O8o〇0(Lcom/samsung/samm/common/SObject;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/samsung/spen/a/e/b;->ooo0〇〇O(Lcom/samsung/samm/common/SObject;)Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇0〇O0088o(Lcom/samsung/spen/a/g/a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇8O:Lcom/samsung/spen/a/g/b;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇8(I)Z
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eq p1, v0, :cond_0

    .line 3
    .line 4
    const/4 v1, 0x2

    .line 5
    if-eq p1, v1, :cond_0

    .line 6
    .line 7
    const/16 v1, 0xa

    .line 8
    .line 9
    if-eq p1, v1, :cond_0

    .line 10
    .line 11
    const/16 v1, 0xb

    .line 12
    .line 13
    if-eq p1, v1, :cond_0

    .line 14
    .line 15
    const/16 v1, 0xc

    .line 16
    .line 17
    if-eq p1, v1, :cond_0

    .line 18
    .line 19
    const/16 v1, 0xd

    .line 20
    .line 21
    if-eq p1, v1, :cond_0

    .line 22
    .line 23
    const/16 v1, 0xe

    .line 24
    .line 25
    if-eq p1, v1, :cond_0

    .line 26
    .line 27
    const/4 p1, 0x0

    .line 28
    return p1

    .line 29
    :cond_0
    invoke-static {p1}, Lcom/samsung/spen/a/e/a;->〇080(I)I

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->o88O〇8(I)V

    .line 34
    .line 35
    .line 36
    return v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public 〇80(II)Z
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/samsung/sdraw/CanvasView;->o08o〇0(II)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x1

    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public 〇8o8o〇(Lcom/samsung/spensdk/applistener/SObjectUpdateListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇0888:Lcom/samsung/spensdk/applistener/SObjectUpdateListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇8〇0〇o〇O(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/samsung/sdraw/AbstractSettingView;->setCustomClearAll(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇O00(Z)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setDrawingUpdatable(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇O888o0o()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getRestoreEnable()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇O8o08O()[B
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getData()[B

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇O〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/samsung/sdraw/AbstractSettingView;->getCustomClearAll()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇O〇80o08O()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getObjectSupportPenOnly()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇o(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    check-cast p1, Landroid/view/View$OnHoverListener;

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 6
    .line 7
    .line 8
    check-cast p2, Landroid/view/View$OnHoverListener;

    .line 9
    .line 10
    invoke-super {p0, p2}, Lcom/samsung/sdraw/CanvasView;->setOnSelectedTextViewHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 p1, 0x0

    .line 15
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 16
    .line 17
    .line 18
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setOnSelectedTextViewHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 19
    .line 20
    .line 21
    :goto_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public 〇o00〇〇Oo()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getZoomEnable()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇o0O0O8(Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->〇〇0:Lcom/samsung/spensdk/applistener/SCanvasLongPressListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇oOO8O8()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getRemoveLongPressStroke()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇oo〇(Lcom/samsung/samm/common/SObject;Z)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/samsung/spen/a/e/b;->O0O(Lcom/samsung/samm/common/SObject;Z)Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public 〇o〇()Landroid/graphics/RectF;
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getSelectedObjectBounds()Landroid/graphics/RectF;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇〇808〇(Landroid/view/ViewGroup;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/samsung/sdraw/SettingView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/samsung/sdraw/SettingView;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 8
    .line 9
    invoke-super {p0, p1}, Lcom/samsung/sdraw/CanvasView;->setSettingView(Lcom/samsung/sdraw/SettingView;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/samsung/spen/a/e/b;->o〇〇0〇88()V

    .line 13
    .line 14
    .line 15
    const/4 p1, 0x1

    .line 16
    return p1

    .line 17
    :cond_0
    const/4 p1, 0x0

    .line 18
    return p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇〇888(II)Z
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/samsung/spen/a/h/d;->〇〇808〇(I)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    invoke-static {p2}, Lcom/samsung/spen/a/e/a;->〇O8o08O(I)I

    .line 10
    .line 11
    .line 12
    move-result p2

    .line 13
    const/4 v0, 0x1

    .line 14
    if-ne p1, v0, :cond_1

    .line 15
    .line 16
    iget-object p1, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 17
    .line 18
    invoke-virtual {p1, p2}, Lcom/samsung/sdraw/AbstractSettingView;->Oo0oO〇O〇O(I)Z

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_1
    const/4 v2, 0x2

    .line 23
    if-ne p1, v2, :cond_2

    .line 24
    .line 25
    iget-object p1, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 26
    .line 27
    invoke-virtual {p1, p2}, Lcom/samsung/sdraw/AbstractSettingView;->O0o〇O0〇(I)Z

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_2
    const/4 v2, 0x3

    .line 32
    if-ne p1, v2, :cond_3

    .line 33
    .line 34
    iget-object p1, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 35
    .line 36
    invoke-virtual {p1, p2}, Lcom/samsung/sdraw/AbstractSettingView;->O0oO008(I)Z

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_3
    const/4 v2, 0x4

    .line 41
    if-ne p1, v2, :cond_4

    .line 42
    .line 43
    iget-object p1, p0, Lcom/samsung/spen/a/e/b;->oo8〇〇:Lcom/samsung/sdraw/SettingView;

    .line 44
    .line 45
    invoke-virtual {p1, p2}, Lcom/samsung/sdraw/AbstractSettingView;->o0O〇8o0O(I)Z

    .line 46
    .line 47
    .line 48
    :goto_0
    return v0

    .line 49
    :cond_4
    return v1
    .line 50
    .line 51
    .line 52
.end method

.method public 〇〇8O0〇8()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->O08O0〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public 〇〇o8()Lcom/samsung/spen/settings/SettingStrokeInfo;
    .locals 5

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getPenSettingInfo()Lcom/samsung/sdraw/PenSettingInfo;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return-object v0

    .line 9
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/sdraw/PenSettingInfo;->Oooo8o0〇()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {v0}, Lcom/samsung/sdraw/PenSettingInfo;->〇O8o08O()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    invoke-virtual {v0}, Lcom/samsung/sdraw/PenSettingInfo;->〇80〇808〇O()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    shl-int/lit8 v3, v3, 0x18

    .line 22
    .line 23
    const v4, 0xffffff

    .line 24
    .line 25
    .line 26
    and-int/2addr v2, v4

    .line 27
    or-int/2addr v2, v3

    .line 28
    const/4 v3, 0x4

    .line 29
    if-ne v1, v3, :cond_1

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/samsung/sdraw/PenSettingInfo;->oO80()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    goto :goto_0

    .line 36
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/sdraw/PenSettingInfo;->〇〇808〇()I

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    :goto_0
    invoke-static {v1}, Lcom/samsung/spen/a/e/a;->Oooo8o0〇(I)I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    new-instance v3, Lcom/samsung/spen/settings/SettingStrokeInfo;

    .line 45
    .line 46
    invoke-direct {v3}, Lcom/samsung/spen/settings/SettingStrokeInfo;-><init>()V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v3, v1}, Lcom/samsung/spen/settings/SettingStrokeInfo;->Oo08(I)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v3, v2}, Lcom/samsung/spen/settings/SettingStrokeInfo;->O8(I)V

    .line 53
    .line 54
    .line 55
    int-to-float v0, v0

    .line 56
    invoke-virtual {v3, v0}, Lcom/samsung/spen/settings/SettingStrokeInfo;->o〇0(F)V

    .line 57
    .line 58
    .line 59
    return-object v3
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public 〇〇〇0〇〇0()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/samsung/sdraw/CanvasView;->getDrawable()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

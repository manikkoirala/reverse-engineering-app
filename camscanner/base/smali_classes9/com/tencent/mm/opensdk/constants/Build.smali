.class public final Lcom/tencent/mm/opensdk/constants/Build;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final CHECK_TOKEN_SDK_INT:I = 0x25010600

.field public static final CHOOSE_INVOICE_TILE_SUPPORT_SDK_INT:I = 0x25010000

.field public static final EMOJI_SUPPORTED_SDK_INT:I = 0x21030001

.field public static final FAVORITE_SUPPPORTED_SDK_INT:I = 0x22000001

.field public static final INVOICE_AUTH_INSERT_SDK_INT:I = 0x25010400

.field public static final LAUNCH_MINIPROGRAM_SUPPORTED_SDK_INT:I = 0x25000008

.field public static final LAUNCH_MINIPROGRAM_WITH_TOKEN_SUPPORTED_SDK_INT:I = 0x25050700

.field public static final MESSAGE_ACTION_SUPPPORTED_SDK_INT:I = 0x22010003

.field public static final MINIPROGRAM_SUPPORTED_SDK_INT:I = 0x25000001

.field public static final MIN_SDK_INT:I = 0x21010001

.field public static final MUSIC_DATA_URL_SUPPORTED_SDK_INT:I = 0x21040001

.field public static final NON_TAX_PAY_SDK_INT:I = 0x25010400

.field public static final OFFLINE_PAY_SDK_INT:I = 0x25010500

.field public static final OPENID_SUPPORTED_SDK_INT:I = 0x22000001

.field public static final OPEN_BUSINESS_VIEW_SDK_INT:I = 0x25020500

.field public static final OPEN_BUSINESS_WEBVIEW_SDK_INT:I = 0x25010600

.field public static final PAY_INSURANCE_SDK_INT:I = 0x25010400

.field public static final PAY_SUPPORTED_SDK_INT:I = 0x22000001

.field public static final SCAN_QRCODE_AUTH_SUPPORTED_SDK_INT:I = 0x23010001

.field public static final SDK_INT:I = 0x26060600

.field public static final SDK_VERSION_NAME:Ljava/lang/String; = "android 6.6.6"

.field public static final SEND_25M_IMAGE_SDK_INT:I = 0x25020400

.field public static final SEND_AUTH_SCOPE_SNSAPI_WXAAPP_INFO_SUPPORTED_SDK_INT:I = 0x25050700

.field public static final SEND_BUSINESS_CARD_SDK_INT:I = 0x25020500

.field public static final SEND_TO_SPECIFIED_CONTACT_SDK_INT:I = 0x25010600

.field public static final SUBSCRIBE_MESSAGE_SUPPORTED_SDK_INT:I = 0x25000006

.field public static final SUBSCRIBE_MINI_PROGRAM_MSG_SUPPORTED_SDK_INT:I = 0x25010500

.field public static final SUPPORTED_JOINT_PAY:I = 0x25040900

.field public static final SUPPORTED_PRELOAD_MINI_PROGRAM:I = 0x25050500

.field public static final SUPPORTED_SEND_WX_WEWORK_OBJECT:I = 0x25030400

.field public static final TIMELINE_SUPPORTED_SDK_INT:I = 0x21020001

.field public static final VIDEO_FILE_SUPPORTED_SDK_INT:I = 0x25000004

.field public static final WEISHI_MINIPROGRAM_SUPPORTED_SDK_INT:I = 0x25030100


# direct methods
.method private constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/lang/RuntimeException;

    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-class v2, Lcom/tencent/mm/opensdk/constants/Build;

    .line 12
    .line 13
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v2, " should not be instantiated"

    .line 21
    .line 22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    throw v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public static getMajorVersion()I
    .locals 1

    .line 1
    const/4 v0, 0x6

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static getMinorVersion()I
    .locals 1

    .line 1
    const/4 v0, 0x6

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

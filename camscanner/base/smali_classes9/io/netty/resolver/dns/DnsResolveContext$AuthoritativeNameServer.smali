.class final Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;
.super Ljava/lang/Object;
.source "DnsResolveContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/netty/resolver/dns/DnsResolveContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AuthoritativeNameServer"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private address:Ljava/net/InetSocketAddress;

.field private final domainName:Ljava/lang/String;

.field private final dots:I

.field final isCopy:Z

.field next:Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;

.field final nsName:Ljava/lang/String;

.field private ttl:J


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method constructor <init>(IJLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->dots:I

    .line 3
    iput-wide p2, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->ttl:J

    .line 4
    iput-object p5, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->nsName:Ljava/lang/String;

    .line 5
    iput-object p4, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->domainName:Ljava/lang/String;

    const/4 p1, 0x0

    .line 6
    iput-boolean p1, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->isCopy:Z

    return-void
.end method

.method constructor <init>(Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;)V
    .locals 2

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iget v0, p1, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->dots:I

    iput v0, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->dots:I

    .line 9
    iget-wide v0, p1, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->ttl:J

    iput-wide v0, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->ttl:J

    .line 10
    iget-object v0, p1, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->nsName:Ljava/lang/String;

    iput-object v0, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->nsName:Ljava/lang/String;

    .line 11
    iget-object p1, p1, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->domainName:Ljava/lang/String;

    iput-object p1, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->domainName:Ljava/lang/String;

    const/4 p1, 0x1

    .line 12
    iput-boolean p1, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->isCopy:Z

    return-void
.end method

.method static synthetic access$1000(Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;)Ljava/net/InetSocketAddress;
    .locals 0

    .line 1
    iget-object p0, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->address:Ljava/net/InetSocketAddress;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$1002(Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;Ljava/net/InetSocketAddress;)Ljava/net/InetSocketAddress;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->address:Ljava/net/InetSocketAddress;

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method static synthetic access$1100(Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->domainName:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$1200(Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->ttl:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method static synthetic access$900(Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;)I
    .locals 0

    .line 1
    iget p0, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->dots:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method isRootServer()Z
    .locals 2

    .line 1
    iget v0, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->dots:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method update(Ljava/net/InetSocketAddress;)V
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    .line 3
    invoke-virtual {p0, p1, v0, v1}, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->update(Ljava/net/InetSocketAddress;J)V

    return-void
.end method

.method update(Ljava/net/InetSocketAddress;J)V
    .locals 2

    .line 1
    iput-object p1, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->address:Ljava/net/InetSocketAddress;

    .line 2
    iget-wide v0, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->ttl:J

    invoke-static {v0, v1, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    iput-wide p1, p0, Lio/netty/resolver/dns/DnsResolveContext$AuthoritativeNameServer;->ttl:J

    return-void
.end method

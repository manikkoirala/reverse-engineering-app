.class final Lio/netty/resolver/dns/UnixResolverOptions$Builder;
.super Ljava/lang/Object;
.source "UnixResolverOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/netty/resolver/dns/UnixResolverOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private attempts:I

.field private ndots:I

.field private timeout:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 3
    iput v0, p0, Lio/netty/resolver/dns/UnixResolverOptions$Builder;->ndots:I

    const/4 v0, 0x5

    .line 4
    iput v0, p0, Lio/netty/resolver/dns/UnixResolverOptions$Builder;->timeout:I

    const/16 v0, 0x10

    .line 5
    iput v0, p0, Lio/netty/resolver/dns/UnixResolverOptions$Builder;->attempts:I

    return-void
.end method

.method synthetic constructor <init>(Lio/netty/resolver/dns/UnixResolverOptions$1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lio/netty/resolver/dns/UnixResolverOptions$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method build()Lio/netty/resolver/dns/UnixResolverOptions;
    .locals 4

    .line 1
    new-instance v0, Lio/netty/resolver/dns/UnixResolverOptions;

    .line 2
    .line 3
    iget v1, p0, Lio/netty/resolver/dns/UnixResolverOptions$Builder;->ndots:I

    .line 4
    .line 5
    iget v2, p0, Lio/netty/resolver/dns/UnixResolverOptions$Builder;->timeout:I

    .line 6
    .line 7
    iget v3, p0, Lio/netty/resolver/dns/UnixResolverOptions$Builder;->attempts:I

    .line 8
    .line 9
    invoke-direct {v0, v1, v2, v3}, Lio/netty/resolver/dns/UnixResolverOptions;-><init>(III)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method setAttempts(I)V
    .locals 0

    .line 1
    iput p1, p0, Lio/netty/resolver/dns/UnixResolverOptions$Builder;->attempts:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setNdots(I)V
    .locals 0

    .line 1
    iput p1, p0, Lio/netty/resolver/dns/UnixResolverOptions$Builder;->ndots:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method setTimeout(I)V
    .locals 0

    .line 1
    iput p1, p0, Lio/netty/resolver/dns/UnixResolverOptions$Builder;->timeout:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

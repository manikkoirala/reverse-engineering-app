.class abstract Lio/netty/buffer/SizeClasses;
.super Ljava/lang/Object;
.source "SizeClasses.java"

# interfaces
.implements Lio/netty/buffer/SizeClassesMetric;


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final INDEX_IDX:I = 0x0

.field private static final LOG2DELTA_IDX:I = 0x2

.field private static final LOG2GROUP_IDX:I = 0x1

.field private static final LOG2_DELTA_LOOKUP_IDX:I = 0x6

.field private static final LOG2_MAX_LOOKUP_SIZE:I = 0xc

.field static final LOG2_QUANTUM:I = 0x4

.field private static final LOG2_SIZE_CLASS_GROUP:I = 0x2

.field private static final NDELTA_IDX:I = 0x3

.field private static final PAGESIZE_IDX:I = 0x4

.field private static final SUBPAGE_IDX:I = 0x5

.field private static final no:B = 0x0t

.field private static final yes:B = 0x1t


# instance fields
.field protected final chunkSize:I

.field protected final directMemoryCacheAlignment:I

.field private lookupMaxSize:I

.field nPSizes:I

.field final nSizes:I

.field nSubpages:I

.field private final pageIdx2sizeTab:[I

.field protected final pageShifts:I

.field protected final pageSize:I

.field private final size2idxTab:[I

.field private final sizeClasses:[[S

.field private final sizeIdx2sizeTab:[I

.field smallMaxSizeIdx:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method protected constructor <init>(IIII)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lio/netty/buffer/SizeClasses;->pageSize:I

    .line 5
    .line 6
    iput p2, p0, Lio/netty/buffer/SizeClasses;->pageShifts:I

    .line 7
    .line 8
    iput p3, p0, Lio/netty/buffer/SizeClasses;->chunkSize:I

    .line 9
    .line 10
    iput p4, p0, Lio/netty/buffer/SizeClasses;->directMemoryCacheAlignment:I

    .line 11
    .line 12
    invoke-static {p3}, Lio/netty/buffer/PoolThreadCache;->log2(I)I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    const/4 p2, 0x1

    .line 17
    add-int/2addr p1, p2

    .line 18
    add-int/lit8 p1, p1, -0x4

    .line 19
    .line 20
    const/4 p3, 0x2

    .line 21
    shl-int/2addr p1, p3

    .line 22
    new-array p3, p3, [I

    .line 23
    .line 24
    const/4 p4, 0x7

    .line 25
    aput p4, p3, p2

    .line 26
    .line 27
    const/4 p2, 0x0

    .line 28
    aput p1, p3, p2

    .line 29
    .line 30
    sget-object p1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    .line 31
    .line 32
    invoke-static {p1, p3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    check-cast p1, [[S

    .line 37
    .line 38
    iput-object p1, p0, Lio/netty/buffer/SizeClasses;->sizeClasses:[[S

    .line 39
    .line 40
    invoke-direct {p0}, Lio/netty/buffer/SizeClasses;->sizeClasses()I

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    iput p1, p0, Lio/netty/buffer/SizeClasses;->nSizes:I

    .line 45
    .line 46
    new-array p1, p1, [I

    .line 47
    .line 48
    iput-object p1, p0, Lio/netty/buffer/SizeClasses;->sizeIdx2sizeTab:[I

    .line 49
    .line 50
    iget p2, p0, Lio/netty/buffer/SizeClasses;->nPSizes:I

    .line 51
    .line 52
    new-array p2, p2, [I

    .line 53
    .line 54
    iput-object p2, p0, Lio/netty/buffer/SizeClasses;->pageIdx2sizeTab:[I

    .line 55
    .line 56
    invoke-direct {p0, p1, p2}, Lio/netty/buffer/SizeClasses;->idx2SizeTab([I[I)V

    .line 57
    .line 58
    .line 59
    iget p1, p0, Lio/netty/buffer/SizeClasses;->lookupMaxSize:I

    .line 60
    .line 61
    shr-int/lit8 p1, p1, 0x4

    .line 62
    .line 63
    new-array p1, p1, [I

    .line 64
    .line 65
    iput-object p1, p0, Lio/netty/buffer/SizeClasses;->size2idxTab:[I

    .line 66
    .line 67
    invoke-direct {p0, p1}, Lio/netty/buffer/SizeClasses;->size2idxTab([I)V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method private alignSize(I)I
    .locals 2

    .line 1
    iget v0, p0, Lio/netty/buffer/SizeClasses;->directMemoryCacheAlignment:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, -0x1

    .line 4
    .line 5
    and-int/2addr v1, p1

    .line 6
    if-nez v1, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    add-int/2addr p1, v0

    .line 10
    sub-int/2addr p1, v1

    .line 11
    :goto_0
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private idx2SizeTab([I[I)V
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    iget v2, p0, Lio/netty/buffer/SizeClasses;->nSizes:I

    .line 4
    .line 5
    if-ge v0, v2, :cond_1

    .line 6
    .line 7
    iget-object v2, p0, Lio/netty/buffer/SizeClasses;->sizeClasses:[[S

    .line 8
    .line 9
    aget-object v2, v2, v0

    .line 10
    .line 11
    const/4 v3, 0x1

    .line 12
    aget-short v4, v2, v3

    .line 13
    .line 14
    const/4 v5, 0x2

    .line 15
    aget-short v5, v2, v5

    .line 16
    .line 17
    const/4 v6, 0x3

    .line 18
    aget-short v6, v2, v6

    .line 19
    .line 20
    shl-int v4, v3, v4

    .line 21
    .line 22
    shl-int v5, v6, v5

    .line 23
    .line 24
    add-int/2addr v4, v5

    .line 25
    aput v4, p1, v0

    .line 26
    .line 27
    const/4 v5, 0x4

    .line 28
    aget-short v2, v2, v5

    .line 29
    .line 30
    if-ne v2, v3, :cond_0

    .line 31
    .line 32
    add-int/lit8 v2, v1, 0x1

    .line 33
    .line 34
    aput v4, p2, v1

    .line 35
    .line 36
    move v1, v2

    .line 37
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private static normalizeSizeCompute(I)I
    .locals 3

    .line 1
    shl-int/lit8 v0, p0, 0x1

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    sub-int/2addr v0, v1

    .line 5
    invoke-static {v0}, Lio/netty/buffer/PoolThreadCache;->log2(I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v2, 0x7

    .line 10
    if-ge v0, v2, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x4

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    add-int/lit8 v0, v0, -0x2

    .line 15
    .line 16
    sub-int/2addr v0, v1

    .line 17
    :goto_0
    shl-int v0, v1, v0

    .line 18
    .line 19
    sub-int/2addr v0, v1

    .line 20
    add-int/2addr p0, v0

    .line 21
    not-int v0, v0

    .line 22
    and-int/2addr p0, v0

    .line 23
    return p0
    .line 24
    .line 25
.end method

.method private pages2pageIdxCompute(IZ)I
    .locals 5

    .line 1
    iget v0, p0, Lio/netty/buffer/SizeClasses;->pageShifts:I

    .line 2
    .line 3
    shl-int v0, p1, v0

    .line 4
    .line 5
    iget v1, p0, Lio/netty/buffer/SizeClasses;->chunkSize:I

    .line 6
    .line 7
    if-le v0, v1, :cond_0

    .line 8
    .line 9
    iget p1, p0, Lio/netty/buffer/SizeClasses;->nPSizes:I

    .line 10
    .line 11
    return p1

    .line 12
    :cond_0
    shl-int/lit8 v1, v0, 0x1

    .line 13
    .line 14
    add-int/lit8 v1, v1, -0x1

    .line 15
    .line 16
    invoke-static {v1}, Lio/netty/buffer/PoolThreadCache;->log2(I)I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    iget v2, p0, Lio/netty/buffer/SizeClasses;->pageShifts:I

    .line 21
    .line 22
    add-int/lit8 v3, v2, 0x2

    .line 23
    .line 24
    if-ge v1, v3, :cond_1

    .line 25
    .line 26
    const/4 v3, 0x0

    .line 27
    goto :goto_0

    .line 28
    :cond_1
    add-int/lit8 v3, v2, 0x2

    .line 29
    .line 30
    sub-int v3, v1, v3

    .line 31
    .line 32
    :goto_0
    shl-int/lit8 v3, v3, 0x2

    .line 33
    .line 34
    add-int/lit8 v4, v2, 0x2

    .line 35
    .line 36
    add-int/lit8 v4, v4, 0x1

    .line 37
    .line 38
    if-ge v1, v4, :cond_2

    .line 39
    .line 40
    move v1, v2

    .line 41
    goto :goto_1

    .line 42
    :cond_2
    add-int/lit8 v1, v1, -0x2

    .line 43
    .line 44
    add-int/lit8 v1, v1, -0x1

    .line 45
    .line 46
    :goto_1
    const/4 v4, -0x1

    .line 47
    shl-int/2addr v4, v1

    .line 48
    add-int/lit8 v0, v0, -0x1

    .line 49
    .line 50
    and-int/2addr v0, v4

    .line 51
    shr-int/2addr v0, v1

    .line 52
    and-int/lit8 v0, v0, 0x3

    .line 53
    .line 54
    add-int/2addr v3, v0

    .line 55
    if-eqz p2, :cond_3

    .line 56
    .line 57
    iget-object p2, p0, Lio/netty/buffer/SizeClasses;->pageIdx2sizeTab:[I

    .line 58
    .line 59
    aget p2, p2, v3

    .line 60
    .line 61
    shl-int/2addr p1, v2

    .line 62
    if-le p2, p1, :cond_3

    .line 63
    .line 64
    add-int/lit8 v3, v3, -0x1

    .line 65
    .line 66
    :cond_3
    return v3
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

.method private size2idxTab([I)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    :goto_0
    iget v3, p0, Lio/netty/buffer/SizeClasses;->lookupMaxSize:I

    .line 5
    .line 6
    if-gt v0, v3, :cond_1

    .line 7
    .line 8
    iget-object v3, p0, Lio/netty/buffer/SizeClasses;->sizeClasses:[[S

    .line 9
    .line 10
    aget-object v3, v3, v1

    .line 11
    .line 12
    const/4 v4, 0x2

    .line 13
    aget-short v3, v3, v4

    .line 14
    .line 15
    add-int/lit8 v3, v3, -0x4

    .line 16
    .line 17
    const/4 v4, 0x1

    .line 18
    shl-int v3, v4, v3

    .line 19
    .line 20
    :goto_1
    iget v4, p0, Lio/netty/buffer/SizeClasses;->lookupMaxSize:I

    .line 21
    .line 22
    if-gt v0, v4, :cond_0

    .line 23
    .line 24
    add-int/lit8 v4, v3, -0x1

    .line 25
    .line 26
    if-lez v3, :cond_0

    .line 27
    .line 28
    add-int/lit8 v0, v2, 0x1

    .line 29
    .line 30
    aput v1, p1, v2

    .line 31
    .line 32
    add-int/lit8 v2, v0, 0x1

    .line 33
    .line 34
    shl-int/lit8 v2, v2, 0x4

    .line 35
    .line 36
    move v3, v4

    .line 37
    move v5, v2

    .line 38
    move v2, v0

    .line 39
    move v0, v5

    .line 40
    goto :goto_1

    .line 41
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private sizeClass(IIII)I
    .locals 8

    .line 1
    iget v0, p0, Lio/netty/buffer/SizeClasses;->pageShifts:I

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-lt p3, v0, :cond_0

    .line 6
    .line 7
    :goto_0
    const/4 v0, 0x1

    .line 8
    goto :goto_1

    .line 9
    :cond_0
    shl-int v0, v2, v0

    .line 10
    .line 11
    shl-int v3, v2, p2

    .line 12
    .line 13
    shl-int v4, v2, p3

    .line 14
    .line 15
    mul-int v4, v4, p4

    .line 16
    .line 17
    add-int/2addr v3, v4

    .line 18
    div-int v4, v3, v0

    .line 19
    .line 20
    mul-int v4, v4, v0

    .line 21
    .line 22
    if-ne v3, v4, :cond_1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    const/4 v0, 0x0

    .line 26
    :goto_1
    if-nez p4, :cond_2

    .line 27
    .line 28
    const/4 v3, 0x0

    .line 29
    goto :goto_2

    .line 30
    :cond_2
    invoke-static {p4}, Lio/netty/buffer/PoolThreadCache;->log2(I)I

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    :goto_2
    shl-int v4, v2, v3

    .line 35
    .line 36
    if-ge v4, p4, :cond_3

    .line 37
    .line 38
    const/4 v4, 0x1

    .line 39
    goto :goto_3

    .line 40
    :cond_3
    const/4 v4, 0x0

    .line 41
    :goto_3
    add-int/2addr v3, p3

    .line 42
    if-ne v3, p2, :cond_4

    .line 43
    .line 44
    add-int/lit8 v3, p2, 0x1

    .line 45
    .line 46
    goto :goto_4

    .line 47
    :cond_4
    move v3, p2

    .line 48
    :goto_4
    if-ne v3, p2, :cond_5

    .line 49
    .line 50
    const/4 v4, 0x1

    .line 51
    :cond_5
    iget v5, p0, Lio/netty/buffer/SizeClasses;->pageShifts:I

    .line 52
    .line 53
    const/4 v6, 0x2

    .line 54
    add-int/2addr v5, v6

    .line 55
    if-ge v3, v5, :cond_6

    .line 56
    .line 57
    const/4 v5, 0x1

    .line 58
    goto :goto_5

    .line 59
    :cond_6
    const/4 v5, 0x0

    .line 60
    :goto_5
    const/16 v7, 0xc

    .line 61
    .line 62
    if-lt v3, v7, :cond_8

    .line 63
    .line 64
    if-ne v3, v7, :cond_7

    .line 65
    .line 66
    if-nez v4, :cond_7

    .line 67
    .line 68
    goto :goto_6

    .line 69
    :cond_7
    const/4 v3, 0x0

    .line 70
    goto :goto_7

    .line 71
    :cond_8
    :goto_6
    move v3, p3

    .line 72
    :goto_7
    const/4 v4, 0x7

    .line 73
    new-array v4, v4, [S

    .line 74
    .line 75
    int-to-short v7, p1

    .line 76
    aput-short v7, v4, v1

    .line 77
    .line 78
    int-to-short v1, p2

    .line 79
    aput-short v1, v4, v2

    .line 80
    .line 81
    int-to-short v1, p3

    .line 82
    aput-short v1, v4, v6

    .line 83
    .line 84
    const/4 v1, 0x3

    .line 85
    int-to-short v6, p4

    .line 86
    aput-short v6, v4, v1

    .line 87
    .line 88
    const/4 v1, 0x4

    .line 89
    aput-short v0, v4, v1

    .line 90
    .line 91
    const/4 v1, 0x5

    .line 92
    aput-short v5, v4, v1

    .line 93
    .line 94
    int-to-short v1, v3

    .line 95
    const/4 v3, 0x6

    .line 96
    aput-short v1, v4, v3

    .line 97
    .line 98
    iget-object v3, p0, Lio/netty/buffer/SizeClasses;->sizeClasses:[[S

    .line 99
    .line 100
    aput-object v4, v3, p1

    .line 101
    .line 102
    shl-int p2, v2, p2

    .line 103
    .line 104
    shl-int p3, p4, p3

    .line 105
    .line 106
    add-int/2addr p2, p3

    .line 107
    if-ne v0, v2, :cond_9

    .line 108
    .line 109
    iget p3, p0, Lio/netty/buffer/SizeClasses;->nPSizes:I

    .line 110
    .line 111
    add-int/2addr p3, v2

    .line 112
    iput p3, p0, Lio/netty/buffer/SizeClasses;->nPSizes:I

    .line 113
    .line 114
    :cond_9
    if-ne v5, v2, :cond_a

    .line 115
    .line 116
    iget p3, p0, Lio/netty/buffer/SizeClasses;->nSubpages:I

    .line 117
    .line 118
    add-int/2addr p3, v2

    .line 119
    iput p3, p0, Lio/netty/buffer/SizeClasses;->nSubpages:I

    .line 120
    .line 121
    iput p1, p0, Lio/netty/buffer/SizeClasses;->smallMaxSizeIdx:I

    .line 122
    .line 123
    :cond_a
    if-eqz v1, :cond_b

    .line 124
    .line 125
    iput p2, p0, Lio/netty/buffer/SizeClasses;->lookupMaxSize:I

    .line 126
    .line 127
    :cond_b
    return p2
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
.end method

.method private sizeClasses()I
    .locals 8

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    :goto_0
    const/4 v3, 0x4

    .line 5
    if-ge v0, v3, :cond_0

    .line 6
    .line 7
    add-int/lit8 v2, v1, 0x1

    .line 8
    .line 9
    add-int/lit8 v4, v0, 0x1

    .line 10
    .line 11
    invoke-direct {p0, v1, v3, v3, v0}, Lio/netty/buffer/SizeClasses;->sizeClass(IIII)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    move v1, v2

    .line 16
    move v2, v0

    .line 17
    move v0, v4

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x6

    .line 20
    const/4 v4, 0x4

    .line 21
    :goto_1
    iget v5, p0, Lio/netty/buffer/SizeClasses;->chunkSize:I

    .line 22
    .line 23
    if-ge v2, v5, :cond_2

    .line 24
    .line 25
    const/4 v5, 0x1

    .line 26
    :goto_2
    if-gt v5, v3, :cond_1

    .line 27
    .line 28
    iget v6, p0, Lio/netty/buffer/SizeClasses;->chunkSize:I

    .line 29
    .line 30
    if-ge v2, v6, :cond_1

    .line 31
    .line 32
    add-int/lit8 v2, v1, 0x1

    .line 33
    .line 34
    add-int/lit8 v6, v5, 0x1

    .line 35
    .line 36
    invoke-direct {p0, v1, v0, v4, v5}, Lio/netty/buffer/SizeClasses;->sizeClass(IIII)I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    move v5, v6

    .line 41
    move v7, v2

    .line 42
    move v2, v1

    .line 43
    move v1, v7

    .line 44
    goto :goto_2

    .line 45
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 46
    .line 47
    add-int/lit8 v4, v4, 0x1

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_2
    return v1
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method


# virtual methods
.method public normalizeSize(I)I
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lio/netty/buffer/SizeClasses;->sizeIdx2sizeTab:[I

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    aget p1, p1, v0

    .line 7
    .line 8
    return p1

    .line 9
    :cond_0
    iget v0, p0, Lio/netty/buffer/SizeClasses;->directMemoryCacheAlignment:I

    .line 10
    .line 11
    if-lez v0, :cond_1

    .line 12
    .line 13
    invoke-direct {p0, p1}, Lio/netty/buffer/SizeClasses;->alignSize(I)I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    :cond_1
    iget v0, p0, Lio/netty/buffer/SizeClasses;->lookupMaxSize:I

    .line 18
    .line 19
    if-gt p1, v0, :cond_2

    .line 20
    .line 21
    iget-object v0, p0, Lio/netty/buffer/SizeClasses;->sizeIdx2sizeTab:[I

    .line 22
    .line 23
    iget-object v1, p0, Lio/netty/buffer/SizeClasses;->size2idxTab:[I

    .line 24
    .line 25
    add-int/lit8 p1, p1, -0x1

    .line 26
    .line 27
    shr-int/lit8 p1, p1, 0x4

    .line 28
    .line 29
    aget p1, v1, p1

    .line 30
    .line 31
    aget p1, v0, p1

    .line 32
    .line 33
    return p1

    .line 34
    :cond_2
    invoke-static {p1}, Lio/netty/buffer/SizeClasses;->normalizeSizeCompute(I)I

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    return p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public pageIdx2size(I)J
    .locals 2

    .line 1
    iget-object v0, p0, Lio/netty/buffer/SizeClasses;->pageIdx2sizeTab:[I

    .line 2
    .line 3
    aget p1, v0, p1

    .line 4
    .line 5
    int-to-long v0, p1

    .line 6
    return-wide v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public pageIdx2sizeCompute(I)J
    .locals 5

    .line 1
    shr-int/lit8 v0, p1, 0x2

    .line 2
    .line 3
    and-int/lit8 p1, p1, 0x3

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    const-wide/16 v2, 0x0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iget v2, p0, Lio/netty/buffer/SizeClasses;->pageShifts:I

    .line 12
    .line 13
    add-int/lit8 v2, v2, 0x2

    .line 14
    .line 15
    sub-int/2addr v2, v1

    .line 16
    const-wide/16 v3, 0x1

    .line 17
    .line 18
    shl-long v2, v3, v2

    .line 19
    .line 20
    shl-long/2addr v2, v0

    .line 21
    :goto_0
    if-nez v0, :cond_1

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    :cond_1
    iget v4, p0, Lio/netty/buffer/SizeClasses;->pageShifts:I

    .line 25
    .line 26
    add-int/2addr v0, v4

    .line 27
    sub-int/2addr v0, v1

    .line 28
    add-int/2addr p1, v1

    .line 29
    shl-int/2addr p1, v0

    .line 30
    int-to-long v0, p1

    .line 31
    add-long/2addr v2, v0

    .line 32
    return-wide v2
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public pages2pageIdx(I)I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, v0}, Lio/netty/buffer/SizeClasses;->pages2pageIdxCompute(IZ)I

    .line 3
    .line 4
    .line 5
    move-result p1

    .line 6
    return p1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public pages2pageIdxFloor(I)I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, p1, v0}, Lio/netty/buffer/SizeClasses;->pages2pageIdxCompute(IZ)I

    .line 3
    .line 4
    .line 5
    move-result p1

    .line 6
    return p1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public size2SizeIdx(I)I
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    iget v1, p0, Lio/netty/buffer/SizeClasses;->chunkSize:I

    .line 6
    .line 7
    if-le p1, v1, :cond_1

    .line 8
    .line 9
    iget p1, p0, Lio/netty/buffer/SizeClasses;->nSizes:I

    .line 10
    .line 11
    return p1

    .line 12
    :cond_1
    iget v1, p0, Lio/netty/buffer/SizeClasses;->directMemoryCacheAlignment:I

    .line 13
    .line 14
    if-lez v1, :cond_2

    .line 15
    .line 16
    invoke-direct {p0, p1}, Lio/netty/buffer/SizeClasses;->alignSize(I)I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    :cond_2
    iget v1, p0, Lio/netty/buffer/SizeClasses;->lookupMaxSize:I

    .line 21
    .line 22
    const/4 v2, 0x4

    .line 23
    if-gt p1, v1, :cond_3

    .line 24
    .line 25
    iget-object v0, p0, Lio/netty/buffer/SizeClasses;->size2idxTab:[I

    .line 26
    .line 27
    add-int/lit8 p1, p1, -0x1

    .line 28
    .line 29
    shr-int/2addr p1, v2

    .line 30
    aget p1, v0, p1

    .line 31
    .line 32
    return p1

    .line 33
    :cond_3
    shl-int/lit8 v1, p1, 0x1

    .line 34
    .line 35
    add-int/lit8 v1, v1, -0x1

    .line 36
    .line 37
    invoke-static {v1}, Lio/netty/buffer/PoolThreadCache;->log2(I)I

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    const/4 v3, 0x7

    .line 42
    if-ge v1, v3, :cond_4

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_4
    add-int/lit8 v0, v1, -0x6

    .line 46
    .line 47
    :goto_0
    shl-int/lit8 v0, v0, 0x2

    .line 48
    .line 49
    if-ge v1, v3, :cond_5

    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_5
    add-int/lit8 v1, v1, -0x2

    .line 53
    .line 54
    add-int/lit8 v2, v1, -0x1

    .line 55
    .line 56
    :goto_1
    const/4 v1, -0x1

    .line 57
    shl-int/2addr v1, v2

    .line 58
    add-int/lit8 p1, p1, -0x1

    .line 59
    .line 60
    and-int/2addr p1, v1

    .line 61
    shr-int/2addr p1, v2

    .line 62
    and-int/lit8 p1, p1, 0x3

    .line 63
    .line 64
    add-int/2addr v0, p1

    .line 65
    return v0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public sizeIdx2size(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lio/netty/buffer/SizeClasses;->sizeIdx2sizeTab:[I

    .line 2
    .line 3
    aget p1, v0, p1

    .line 4
    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public sizeIdx2sizeCompute(I)I
    .locals 3

    .line 1
    shr-int/lit8 v0, p1, 0x2

    .line 2
    .line 3
    and-int/lit8 p1, p1, 0x3

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/16 v1, 0x20

    .line 10
    .line 11
    shl-int/2addr v1, v0

    .line 12
    :goto_0
    const/4 v2, 0x1

    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    :cond_1
    add-int/lit8 v0, v0, 0x4

    .line 17
    .line 18
    sub-int/2addr v0, v2

    .line 19
    add-int/2addr p1, v2

    .line 20
    shl-int/2addr p1, v0

    .line 21
    add-int/2addr v1, p1

    .line 22
    return v1
    .line 23
    .line 24
    .line 25
.end method

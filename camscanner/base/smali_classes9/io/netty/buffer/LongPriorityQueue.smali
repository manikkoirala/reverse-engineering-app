.class final Lio/netty/buffer/LongPriorityQueue;
.super Ljava/lang/Object;
.source "LongPriorityQueue.java"


# static fields
.field public static final NO_VALUE:I = -0x1


# instance fields
.field private array:[J

.field private size:I


# direct methods
.method constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x9

    .line 5
    .line 6
    new-array v0, v0, [J

    .line 7
    .line 8
    iput-object v0, p0, Lio/netty/buffer/LongPriorityQueue;->array:[J

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private lift(I)V
    .locals 2

    .line 1
    :goto_0
    const/4 v0, 0x1

    .line 2
    if-le p1, v0, :cond_0

    .line 3
    .line 4
    shr-int/lit8 v0, p1, 0x1

    .line 5
    .line 6
    invoke-direct {p0, v0, p1}, Lio/netty/buffer/LongPriorityQueue;->subord(II)Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-direct {p0, p1, v0}, Lio/netty/buffer/LongPriorityQueue;->swap(II)V

    .line 13
    .line 14
    .line 15
    move p1, v0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private sink(I)V
    .locals 3

    .line 1
    :goto_0
    shl-int/lit8 v0, p1, 0x1

    .line 2
    .line 3
    iget v1, p0, Lio/netty/buffer/LongPriorityQueue;->size:I

    .line 4
    .line 5
    if-gt v0, v1, :cond_2

    .line 6
    .line 7
    if-ge v0, v1, :cond_0

    .line 8
    .line 9
    add-int/lit8 v1, v0, 0x1

    .line 10
    .line 11
    invoke-direct {p0, v0, v1}, Lio/netty/buffer/LongPriorityQueue;->subord(II)Z

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    if-eqz v2, :cond_0

    .line 16
    .line 17
    move v0, v1

    .line 18
    :cond_0
    invoke-direct {p0, p1, v0}, Lio/netty/buffer/LongPriorityQueue;->subord(II)Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-nez v1, :cond_1

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_1
    invoke-direct {p0, p1, v0}, Lio/netty/buffer/LongPriorityQueue;->swap(II)V

    .line 26
    .line 27
    .line 28
    move p1, v0

    .line 29
    goto :goto_0

    .line 30
    :cond_2
    :goto_1
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private subord(II)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lio/netty/buffer/LongPriorityQueue;->array:[J

    .line 2
    .line 3
    aget-wide v1, v0, p1

    .line 4
    .line 5
    aget-wide p1, v0, p2

    .line 6
    .line 7
    cmp-long v0, v1, p1

    .line 8
    .line 9
    if-lez v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 p1, 0x0

    .line 14
    :goto_0
    return p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method private swap(II)V
    .locals 5

    .line 1
    iget-object v0, p0, Lio/netty/buffer/LongPriorityQueue;->array:[J

    .line 2
    .line 3
    aget-wide v1, v0, p1

    .line 4
    .line 5
    aget-wide v3, v0, p2

    .line 6
    .line 7
    aput-wide v3, v0, p1

    .line 8
    .line 9
    aput-wide v1, v0, p2

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public isEmpty()Z
    .locals 1

    .line 1
    iget v0, p0, Lio/netty/buffer/LongPriorityQueue;->size:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public offer(J)V
    .locals 3

    .line 1
    const-wide/16 v0, -0x1

    .line 2
    .line 3
    cmp-long v2, p1, v0

    .line 4
    .line 5
    if-eqz v2, :cond_1

    .line 6
    .line 7
    iget v0, p0, Lio/netty/buffer/LongPriorityQueue;->size:I

    .line 8
    .line 9
    add-int/lit8 v0, v0, 0x1

    .line 10
    .line 11
    iput v0, p0, Lio/netty/buffer/LongPriorityQueue;->size:I

    .line 12
    .line 13
    iget-object v1, p0, Lio/netty/buffer/LongPriorityQueue;->array:[J

    .line 14
    .line 15
    array-length v2, v1

    .line 16
    if-ne v0, v2, :cond_0

    .line 17
    .line 18
    array-length v0, v1

    .line 19
    add-int/lit8 v0, v0, -0x1

    .line 20
    .line 21
    mul-int/lit8 v0, v0, 0x2

    .line 22
    .line 23
    add-int/lit8 v0, v0, 0x1

    .line 24
    .line 25
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([JI)[J

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    iput-object v0, p0, Lio/netty/buffer/LongPriorityQueue;->array:[J

    .line 30
    .line 31
    :cond_0
    iget-object v0, p0, Lio/netty/buffer/LongPriorityQueue;->array:[J

    .line 32
    .line 33
    iget v1, p0, Lio/netty/buffer/LongPriorityQueue;->size:I

    .line 34
    .line 35
    aput-wide p1, v0, v1

    .line 36
    .line 37
    invoke-direct {p0, v1}, Lio/netty/buffer/LongPriorityQueue;->lift(I)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 42
    .line 43
    const-string p2, "The NO_VALUE (-1) cannot be added to the queue."

    .line 44
    .line 45
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    throw p1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public peek()J
    .locals 3

    .line 1
    iget v0, p0, Lio/netty/buffer/LongPriorityQueue;->size:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-wide/16 v0, -0x1

    .line 6
    .line 7
    return-wide v0

    .line 8
    :cond_0
    iget-object v0, p0, Lio/netty/buffer/LongPriorityQueue;->array:[J

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    aget-wide v1, v0, v1

    .line 12
    .line 13
    return-wide v1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public poll()J
    .locals 7

    .line 1
    iget v0, p0, Lio/netty/buffer/LongPriorityQueue;->size:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-wide/16 v0, -0x1

    .line 6
    .line 7
    return-wide v0

    .line 8
    :cond_0
    iget-object v1, p0, Lio/netty/buffer/LongPriorityQueue;->array:[J

    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    aget-wide v3, v1, v2

    .line 12
    .line 13
    aget-wide v5, v1, v0

    .line 14
    .line 15
    aput-wide v5, v1, v2

    .line 16
    .line 17
    const-wide/16 v5, 0x0

    .line 18
    .line 19
    aput-wide v5, v1, v0

    .line 20
    .line 21
    sub-int/2addr v0, v2

    .line 22
    iput v0, p0, Lio/netty/buffer/LongPriorityQueue;->size:I

    .line 23
    .line 24
    invoke-direct {p0, v2}, Lio/netty/buffer/LongPriorityQueue;->sink(I)V

    .line 25
    .line 26
    .line 27
    return-wide v3
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public remove(J)V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    :goto_0
    iget v1, p0, Lio/netty/buffer/LongPriorityQueue;->size:I

    .line 3
    .line 4
    if-gt v0, v1, :cond_1

    .line 5
    .line 6
    iget-object v2, p0, Lio/netty/buffer/LongPriorityQueue;->array:[J

    .line 7
    .line 8
    aget-wide v3, v2, v0

    .line 9
    .line 10
    cmp-long v5, v3, p1

    .line 11
    .line 12
    if-nez v5, :cond_0

    .line 13
    .line 14
    add-int/lit8 p1, v1, -0x1

    .line 15
    .line 16
    iput p1, p0, Lio/netty/buffer/LongPriorityQueue;->size:I

    .line 17
    .line 18
    aget-wide p1, v2, v1

    .line 19
    .line 20
    aput-wide p1, v2, v0

    .line 21
    .line 22
    invoke-direct {p0, v0}, Lio/netty/buffer/LongPriorityQueue;->lift(I)V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0, v0}, Lio/netty/buffer/LongPriorityQueue;->sink(I)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

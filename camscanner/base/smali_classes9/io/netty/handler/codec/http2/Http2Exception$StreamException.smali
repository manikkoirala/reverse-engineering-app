.class public Lio/netty/handler/codec/http2/Http2Exception$StreamException;
.super Lio/netty/handler/codec/http2/Http2Exception;
.source "Http2Exception.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/netty/handler/codec/http2/Http2Exception;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StreamException"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x85c69964ce66d40L


# instance fields
.field private final streamId:I


# direct methods
.method constructor <init>(ILio/netty/handler/codec/http2/Http2Error;Ljava/lang/String;)V
    .locals 1

    .line 1
    sget-object v0, Lio/netty/handler/codec/http2/Http2Exception$ShutdownHint;->NO_SHUTDOWN:Lio/netty/handler/codec/http2/Http2Exception$ShutdownHint;

    invoke-direct {p0, p2, p3, v0}, Lio/netty/handler/codec/http2/Http2Exception;-><init>(Lio/netty/handler/codec/http2/Http2Error;Ljava/lang/String;Lio/netty/handler/codec/http2/Http2Exception$ShutdownHint;)V

    .line 2
    iput p1, p0, Lio/netty/handler/codec/http2/Http2Exception$StreamException;->streamId:I

    return-void
.end method

.method constructor <init>(ILio/netty/handler/codec/http2/Http2Error;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 3
    sget-object v0, Lio/netty/handler/codec/http2/Http2Exception$ShutdownHint;->NO_SHUTDOWN:Lio/netty/handler/codec/http2/Http2Exception$ShutdownHint;

    invoke-direct {p0, p2, p3, p4, v0}, Lio/netty/handler/codec/http2/Http2Exception;-><init>(Lio/netty/handler/codec/http2/Http2Error;Ljava/lang/String;Ljava/lang/Throwable;Lio/netty/handler/codec/http2/Http2Exception$ShutdownHint;)V

    .line 4
    iput p1, p0, Lio/netty/handler/codec/http2/Http2Exception$StreamException;->streamId:I

    return-void
.end method


# virtual methods
.method public streamId()I
    .locals 1

    .line 1
    iget v0, p0, Lio/netty/handler/codec/http2/Http2Exception$StreamException;->streamId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

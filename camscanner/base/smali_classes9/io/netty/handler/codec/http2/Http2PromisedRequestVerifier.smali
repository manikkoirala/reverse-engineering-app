.class public interface abstract Lio/netty/handler/codec/http2/Http2PromisedRequestVerifier;
.super Ljava/lang/Object;
.source "Http2PromisedRequestVerifier.java"


# static fields
.field public static final ALWAYS_VERIFY:Lio/netty/handler/codec/http2/Http2PromisedRequestVerifier;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lio/netty/handler/codec/http2/Http2PromisedRequestVerifier$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lio/netty/handler/codec/http2/Http2PromisedRequestVerifier$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lio/netty/handler/codec/http2/Http2PromisedRequestVerifier;->ALWAYS_VERIFY:Lio/netty/handler/codec/http2/Http2PromisedRequestVerifier;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public abstract isAuthoritative(Lio/netty/channel/ChannelHandlerContext;Lio/netty/handler/codec/http2/Http2Headers;)Z
.end method

.method public abstract isCacheable(Lio/netty/handler/codec/http2/Http2Headers;)Z
.end method

.method public abstract isSafe(Lio/netty/handler/codec/http2/Http2Headers;)Z
.end method

.class final Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;
.super Ljava/lang/Object;
.source "Bzip2MTFAndRLE2StageEncoder.java"


# instance fields
.field private alphabetSize:I

.field private final bwtBlock:[I

.field private final bwtLength:I

.field private final bwtValuesPresent:[Z

.field private final mtfBlock:[C

.field private mtfLength:I

.field private final mtfSymbolFrequencies:[I


# direct methods
.method constructor <init>([II[Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x102

    .line 5
    .line 6
    new-array v0, v0, [I

    .line 7
    .line 8
    iput-object v0, p0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->mtfSymbolFrequencies:[I

    .line 9
    .line 10
    iput-object p1, p0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->bwtBlock:[I

    .line 11
    .line 12
    iput p2, p0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->bwtLength:I

    .line 13
    .line 14
    iput-object p3, p0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->bwtValuesPresent:[Z

    .line 15
    .line 16
    add-int/lit8 p2, p2, 0x1

    .line 17
    .line 18
    new-array p1, p2, [C

    .line 19
    .line 20
    iput-object p1, p0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->mtfBlock:[C

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method


# virtual methods
.method encode()V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget v1, v0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->bwtLength:I

    .line 4
    .line 5
    iget-object v2, v0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->bwtValuesPresent:[Z

    .line 6
    .line 7
    iget-object v3, v0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->bwtBlock:[I

    .line 8
    .line 9
    iget-object v4, v0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->mtfBlock:[C

    .line 10
    .line 11
    iget-object v5, v0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->mtfSymbolFrequencies:[I

    .line 12
    .line 13
    const/16 v6, 0x100

    .line 14
    .line 15
    new-array v7, v6, [B

    .line 16
    .line 17
    new-instance v8, Lio/netty/handler/codec/compression/Bzip2MoveToFrontTable;

    .line 18
    .line 19
    invoke-direct {v8}, Lio/netty/handler/codec/compression/Bzip2MoveToFrontTable;-><init>()V

    .line 20
    .line 21
    .line 22
    const/4 v9, 0x0

    .line 23
    const/4 v10, 0x0

    .line 24
    const/4 v11, 0x0

    .line 25
    :goto_0
    if-ge v10, v6, :cond_1

    .line 26
    .line 27
    aget-boolean v12, v2, v10

    .line 28
    .line 29
    if-eqz v12, :cond_0

    .line 30
    .line 31
    add-int/lit8 v12, v11, 0x1

    .line 32
    .line 33
    int-to-byte v11, v11

    .line 34
    aput-byte v11, v7, v10

    .line 35
    .line 36
    move v11, v12

    .line 37
    :cond_0
    add-int/lit8 v10, v10, 0x1

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    const/4 v2, 0x1

    .line 41
    add-int/2addr v11, v2

    .line 42
    const/4 v6, 0x0

    .line 43
    const/4 v10, 0x0

    .line 44
    const/4 v12, 0x0

    .line 45
    const/4 v13, 0x0

    .line 46
    const/4 v14, 0x0

    .line 47
    :goto_1
    if-ge v6, v1, :cond_6

    .line 48
    .line 49
    aget v15, v3, v6

    .line 50
    .line 51
    and-int/lit16 v15, v15, 0xff

    .line 52
    .line 53
    aget-byte v15, v7, v15

    .line 54
    .line 55
    invoke-virtual {v8, v15}, Lio/netty/handler/codec/compression/Bzip2MoveToFrontTable;->valueToFront(B)I

    .line 56
    .line 57
    .line 58
    move-result v15

    .line 59
    if-nez v15, :cond_2

    .line 60
    .line 61
    add-int/lit8 v10, v10, 0x1

    .line 62
    .line 63
    goto :goto_5

    .line 64
    :cond_2
    if-lez v10, :cond_5

    .line 65
    .line 66
    add-int/lit8 v10, v10, -0x1

    .line 67
    .line 68
    :goto_2
    and-int/lit8 v16, v10, 0x1

    .line 69
    .line 70
    if-nez v16, :cond_3

    .line 71
    .line 72
    add-int/lit8 v16, v12, 0x1

    .line 73
    .line 74
    aput-char v9, v4, v12

    .line 75
    .line 76
    add-int/lit8 v13, v13, 0x1

    .line 77
    .line 78
    goto :goto_3

    .line 79
    :cond_3
    add-int/lit8 v16, v12, 0x1

    .line 80
    .line 81
    aput-char v2, v4, v12

    .line 82
    .line 83
    add-int/lit8 v14, v14, 0x1

    .line 84
    .line 85
    :goto_3
    move/from16 v12, v16

    .line 86
    .line 87
    if-gt v10, v2, :cond_4

    .line 88
    .line 89
    const/4 v10, 0x0

    .line 90
    goto :goto_4

    .line 91
    :cond_4
    add-int/lit8 v10, v10, -0x2

    .line 92
    .line 93
    ushr-int/2addr v10, v2

    .line 94
    goto :goto_2

    .line 95
    :cond_5
    :goto_4
    add-int/lit8 v16, v12, 0x1

    .line 96
    .line 97
    add-int/lit8 v15, v15, 0x1

    .line 98
    .line 99
    int-to-char v9, v15

    .line 100
    aput-char v9, v4, v12

    .line 101
    .line 102
    aget v9, v5, v15

    .line 103
    .line 104
    add-int/2addr v9, v2

    .line 105
    aput v9, v5, v15

    .line 106
    .line 107
    move/from16 v12, v16

    .line 108
    .line 109
    :goto_5
    add-int/lit8 v6, v6, 0x1

    .line 110
    .line 111
    const/4 v9, 0x0

    .line 112
    goto :goto_1

    .line 113
    :cond_6
    if-lez v10, :cond_9

    .line 114
    .line 115
    add-int/lit8 v10, v10, -0x1

    .line 116
    .line 117
    :goto_6
    and-int/lit8 v1, v10, 0x1

    .line 118
    .line 119
    if-nez v1, :cond_7

    .line 120
    .line 121
    add-int/lit8 v1, v12, 0x1

    .line 122
    .line 123
    const/4 v3, 0x0

    .line 124
    aput-char v3, v4, v12

    .line 125
    .line 126
    add-int/lit8 v13, v13, 0x1

    .line 127
    .line 128
    goto :goto_7

    .line 129
    :cond_7
    add-int/lit8 v1, v12, 0x1

    .line 130
    .line 131
    aput-char v2, v4, v12

    .line 132
    .line 133
    add-int/lit8 v14, v14, 0x1

    .line 134
    .line 135
    :goto_7
    move v12, v1

    .line 136
    if-gt v10, v2, :cond_8

    .line 137
    .line 138
    goto :goto_8

    .line 139
    :cond_8
    add-int/lit8 v10, v10, -0x2

    .line 140
    .line 141
    ushr-int/2addr v10, v2

    .line 142
    goto :goto_6

    .line 143
    :cond_9
    :goto_8
    int-to-char v1, v11

    .line 144
    aput-char v1, v4, v12

    .line 145
    .line 146
    aget v1, v5, v11

    .line 147
    .line 148
    add-int/2addr v1, v2

    .line 149
    aput v1, v5, v11

    .line 150
    .line 151
    const/4 v1, 0x0

    .line 152
    aget v3, v5, v1

    .line 153
    .line 154
    add-int/2addr v3, v13

    .line 155
    aput v3, v5, v1

    .line 156
    .line 157
    aget v1, v5, v2

    .line 158
    .line 159
    add-int/2addr v1, v14

    .line 160
    aput v1, v5, v2

    .line 161
    .line 162
    add-int/2addr v12, v2

    .line 163
    iput v12, v0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->mtfLength:I

    .line 164
    .line 165
    add-int/2addr v11, v2

    .line 166
    iput v11, v0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->alphabetSize:I

    .line 167
    .line 168
    return-void
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method mtfAlphabetSize()I
    .locals 1

    .line 1
    iget v0, p0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->alphabetSize:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method mtfBlock()[C
    .locals 1

    .line 1
    iget-object v0, p0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->mtfBlock:[C

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method mtfLength()I
    .locals 1

    .line 1
    iget v0, p0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->mtfLength:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method mtfSymbolFrequencies()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lio/netty/handler/codec/compression/Bzip2MTFAndRLE2StageEncoder;->mtfSymbolFrequencies:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

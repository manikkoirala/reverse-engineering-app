.class public final Lio/netty/handler/codec/http/cookie/ClientCookieDecoder;
.super Lio/netty/handler/codec/http/cookie/CookieDecoder;
.source "ClientCookieDecoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/netty/handler/codec/http/cookie/ClientCookieDecoder$CookieBuilder;
    }
.end annotation


# static fields
.field public static final LAX:Lio/netty/handler/codec/http/cookie/ClientCookieDecoder;

.field public static final STRICT:Lio/netty/handler/codec/http/cookie/ClientCookieDecoder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lio/netty/handler/codec/http/cookie/ClientCookieDecoder;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {v0, v1}, Lio/netty/handler/codec/http/cookie/ClientCookieDecoder;-><init>(Z)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lio/netty/handler/codec/http/cookie/ClientCookieDecoder;->STRICT:Lio/netty/handler/codec/http/cookie/ClientCookieDecoder;

    .line 8
    .line 9
    new-instance v0, Lio/netty/handler/codec/http/cookie/ClientCookieDecoder;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-direct {v0, v1}, Lio/netty/handler/codec/http/cookie/ClientCookieDecoder;-><init>(Z)V

    .line 13
    .line 14
    .line 15
    sput-object v0, Lio/netty/handler/codec/http/cookie/ClientCookieDecoder;->LAX:Lio/netty/handler/codec/http/cookie/ClientCookieDecoder;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
.end method

.method private constructor <init>(Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lio/netty/handler/codec/http/cookie/CookieDecoder;-><init>(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public decode(Ljava/lang/String;)Lio/netty/handler/codec/http/cookie/Cookie;
    .locals 11

    .line 1
    const-string v0, "header"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lio/netty/util/internal/ObjectUtil;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/String;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, 0x0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    return-object v1

    .line 17
    :cond_0
    const/4 v2, 0x0

    .line 18
    move-object v3, v1

    .line 19
    const/4 v5, 0x0

    .line 20
    :goto_0
    if-ne v5, v0, :cond_1

    .line 21
    .line 22
    goto :goto_1

    .line 23
    :cond_1
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    const/16 v6, 0x2c

    .line 28
    .line 29
    if-ne v4, v6, :cond_3

    .line 30
    .line 31
    :goto_1
    if-eqz v3, :cond_2

    .line 32
    .line 33
    invoke-virtual {v3}, Lio/netty/handler/codec/http/cookie/ClientCookieDecoder$CookieBuilder;->cookie()Lio/netty/handler/codec/http/cookie/Cookie;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    :cond_2
    return-object v1

    .line 38
    :cond_3
    const/16 v7, 0x9

    .line 39
    .line 40
    if-eq v4, v7, :cond_d

    .line 41
    .line 42
    const/16 v7, 0xa

    .line 43
    .line 44
    if-eq v4, v7, :cond_d

    .line 45
    .line 46
    const/16 v7, 0xb

    .line 47
    .line 48
    if-eq v4, v7, :cond_d

    .line 49
    .line 50
    const/16 v7, 0xc

    .line 51
    .line 52
    if-eq v4, v7, :cond_d

    .line 53
    .line 54
    const/16 v7, 0xd

    .line 55
    .line 56
    if-eq v4, v7, :cond_d

    .line 57
    .line 58
    const/16 v7, 0x20

    .line 59
    .line 60
    if-eq v4, v7, :cond_d

    .line 61
    .line 62
    const/16 v7, 0x3b

    .line 63
    .line 64
    if-ne v4, v7, :cond_4

    .line 65
    .line 66
    goto/16 :goto_6

    .line 67
    .line 68
    :cond_4
    move v4, v5

    .line 69
    :cond_5
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    .line 70
    .line 71
    .line 72
    move-result v8

    .line 73
    const/4 v9, -0x1

    .line 74
    if-ne v8, v7, :cond_6

    .line 75
    .line 76
    move v7, v4

    .line 77
    move v9, v7

    .line 78
    :goto_2
    const/4 v4, -0x1

    .line 79
    const/4 v8, -0x1

    .line 80
    goto :goto_4

    .line 81
    :cond_6
    const/16 v10, 0x3d

    .line 82
    .line 83
    if-ne v8, v10, :cond_9

    .line 84
    .line 85
    add-int/lit8 v8, v4, 0x1

    .line 86
    .line 87
    if-ne v8, v0, :cond_7

    .line 88
    .line 89
    move v7, v4

    .line 90
    move v9, v8

    .line 91
    const/4 v4, 0x0

    .line 92
    const/4 v8, 0x0

    .line 93
    goto :goto_4

    .line 94
    :cond_7
    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->indexOf(II)I

    .line 95
    .line 96
    .line 97
    move-result v7

    .line 98
    if-lez v7, :cond_8

    .line 99
    .line 100
    move v9, v7

    .line 101
    goto :goto_3

    .line 102
    :cond_8
    move v9, v0

    .line 103
    :goto_3
    move v7, v4

    .line 104
    move v4, v9

    .line 105
    goto :goto_4

    .line 106
    :cond_9
    add-int/lit8 v4, v4, 0x1

    .line 107
    .line 108
    if-ne v4, v0, :cond_5

    .line 109
    .line 110
    move v7, v0

    .line 111
    move v9, v4

    .line 112
    goto :goto_2

    .line 113
    :goto_4
    if-lez v4, :cond_a

    .line 114
    .line 115
    add-int/lit8 v10, v4, -0x1

    .line 116
    .line 117
    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    .line 118
    .line 119
    .line 120
    move-result v10

    .line 121
    if-ne v10, v6, :cond_a

    .line 122
    .line 123
    add-int/lit8 v4, v4, -0x1

    .line 124
    .line 125
    :cond_a
    move v10, v4

    .line 126
    if-nez v3, :cond_c

    .line 127
    .line 128
    move-object v3, p0

    .line 129
    move-object v4, p1

    .line 130
    move v6, v7

    .line 131
    move v7, v8

    .line 132
    move v8, v10

    .line 133
    invoke-virtual/range {v3 .. v8}, Lio/netty/handler/codec/http/cookie/CookieDecoder;->initCookie(Ljava/lang/String;IIII)Lio/netty/handler/codec/http/cookie/DefaultCookie;

    .line 134
    .line 135
    .line 136
    move-result-object v3

    .line 137
    if-nez v3, :cond_b

    .line 138
    .line 139
    return-object v1

    .line 140
    :cond_b
    new-instance v4, Lio/netty/handler/codec/http/cookie/ClientCookieDecoder$CookieBuilder;

    .line 141
    .line 142
    invoke-direct {v4, v3, p1}, Lio/netty/handler/codec/http/cookie/ClientCookieDecoder$CookieBuilder;-><init>(Lio/netty/handler/codec/http/cookie/DefaultCookie;Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    move-object v3, v4

    .line 146
    goto :goto_5

    .line 147
    :cond_c
    invoke-virtual {v3, v5, v7, v8, v10}, Lio/netty/handler/codec/http/cookie/ClientCookieDecoder$CookieBuilder;->appendAttribute(IIII)V

    .line 148
    .line 149
    .line 150
    :goto_5
    move v5, v9

    .line 151
    goto/16 :goto_0

    .line 152
    .line 153
    :cond_d
    :goto_6
    add-int/lit8 v5, v5, 0x1

    .line 154
    .line 155
    goto/16 :goto_0
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

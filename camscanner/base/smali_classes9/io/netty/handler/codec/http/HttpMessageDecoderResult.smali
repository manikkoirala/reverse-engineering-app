.class public final Lio/netty/handler/codec/http/HttpMessageDecoderResult;
.super Lio/netty/handler/codec/DecoderResult;
.source "HttpMessageDecoderResult.java"


# instance fields
.field private final headerSize:I

.field private final initialLineLength:I


# direct methods
.method constructor <init>(II)V
    .locals 1

    .line 1
    sget-object v0, Lio/netty/handler/codec/DecoderResult;->SIGNAL_SUCCESS:Lio/netty/util/Signal;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lio/netty/handler/codec/DecoderResult;-><init>(Ljava/lang/Throwable;)V

    .line 4
    .line 5
    .line 6
    iput p1, p0, Lio/netty/handler/codec/http/HttpMessageDecoderResult;->initialLineLength:I

    .line 7
    .line 8
    iput p2, p0, Lio/netty/handler/codec/http/HttpMessageDecoderResult;->headerSize:I

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method


# virtual methods
.method public headerSize()I
    .locals 1

    .line 1
    iget v0, p0, Lio/netty/handler/codec/http/HttpMessageDecoderResult;->headerSize:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public initialLineLength()I
    .locals 1

    .line 1
    iget v0, p0, Lio/netty/handler/codec/http/HttpMessageDecoderResult;->initialLineLength:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public totalSize()I
    .locals 2

    .line 1
    iget v0, p0, Lio/netty/handler/codec/http/HttpMessageDecoderResult;->initialLineLength:I

    .line 2
    .line 3
    iget v1, p0, Lio/netty/handler/codec/http/HttpMessageDecoderResult;->headerSize:I

    .line 4
    .line 5
    add-int/2addr v0, v1

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

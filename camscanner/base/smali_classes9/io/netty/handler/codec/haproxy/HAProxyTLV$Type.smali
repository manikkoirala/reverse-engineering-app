.class public final enum Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;
.super Ljava/lang/Enum;
.source "HAProxyTLV.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/netty/handler/codec/haproxy/HAProxyTLV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

.field public static final enum OTHER:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

.field public static final enum PP2_TYPE_ALPN:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

.field public static final enum PP2_TYPE_AUTHORITY:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

.field public static final enum PP2_TYPE_NETNS:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

.field public static final enum PP2_TYPE_SSL:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

.field public static final enum PP2_TYPE_SSL_CN:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

.field public static final enum PP2_TYPE_SSL_VERSION:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 1
    new-instance v0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 2
    .line 3
    const-string v1, "PP2_TYPE_ALPN"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_ALPN:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 10
    .line 11
    new-instance v1, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 12
    .line 13
    const-string v3, "PP2_TYPE_AUTHORITY"

    .line 14
    .line 15
    const/4 v4, 0x1

    .line 16
    invoke-direct {v1, v3, v4}, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;-><init>(Ljava/lang/String;I)V

    .line 17
    .line 18
    .line 19
    sput-object v1, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_AUTHORITY:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 20
    .line 21
    new-instance v3, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 22
    .line 23
    const-string v5, "PP2_TYPE_SSL"

    .line 24
    .line 25
    const/4 v6, 0x2

    .line 26
    invoke-direct {v3, v5, v6}, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sput-object v3, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_SSL:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 30
    .line 31
    new-instance v5, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 32
    .line 33
    const-string v7, "PP2_TYPE_SSL_VERSION"

    .line 34
    .line 35
    const/4 v8, 0x3

    .line 36
    invoke-direct {v5, v7, v8}, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;-><init>(Ljava/lang/String;I)V

    .line 37
    .line 38
    .line 39
    sput-object v5, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_SSL_VERSION:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 40
    .line 41
    new-instance v7, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 42
    .line 43
    const-string v9, "PP2_TYPE_SSL_CN"

    .line 44
    .line 45
    const/4 v10, 0x4

    .line 46
    invoke-direct {v7, v9, v10}, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;-><init>(Ljava/lang/String;I)V

    .line 47
    .line 48
    .line 49
    sput-object v7, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_SSL_CN:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 50
    .line 51
    new-instance v9, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 52
    .line 53
    const-string v11, "PP2_TYPE_NETNS"

    .line 54
    .line 55
    const/4 v12, 0x5

    .line 56
    invoke-direct {v9, v11, v12}, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;-><init>(Ljava/lang/String;I)V

    .line 57
    .line 58
    .line 59
    sput-object v9, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_NETNS:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 60
    .line 61
    new-instance v11, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 62
    .line 63
    const-string v13, "OTHER"

    .line 64
    .line 65
    const/4 v14, 0x6

    .line 66
    invoke-direct {v11, v13, v14}, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;-><init>(Ljava/lang/String;I)V

    .line 67
    .line 68
    .line 69
    sput-object v11, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->OTHER:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 70
    .line 71
    const/4 v13, 0x7

    .line 72
    new-array v13, v13, [Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 73
    .line 74
    aput-object v0, v13, v2

    .line 75
    .line 76
    aput-object v1, v13, v4

    .line 77
    .line 78
    aput-object v3, v13, v6

    .line 79
    .line 80
    aput-object v5, v13, v8

    .line 81
    .line 82
    aput-object v7, v13, v10

    .line 83
    .line 84
    aput-object v9, v13, v12

    .line 85
    .line 86
    aput-object v11, v13, v14

    .line 87
    .line 88
    sput-object v13, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->$VALUES:[Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 89
    .line 90
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static byteValueForType(Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;)B
    .locals 3

    .line 1
    sget-object v0, Lio/netty/handler/codec/haproxy/HAProxyTLV$1;->$SwitchMap$io$netty$handler$codec$haproxy$HAProxyTLV$Type:[I

    .line 2
    .line 3
    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    aget v0, v0, v1

    .line 8
    .line 9
    packed-switch v0, :pswitch_data_0

    .line 10
    .line 11
    .line 12
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 13
    .line 14
    new-instance v1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string/jumbo v2, "unknown type: "

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object p0

    .line 32
    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    throw v0

    .line 36
    :pswitch_0
    const/16 p0, 0x30

    .line 37
    .line 38
    return p0

    .line 39
    :pswitch_1
    const/16 p0, 0x22

    .line 40
    .line 41
    return p0

    .line 42
    :pswitch_2
    const/16 p0, 0x21

    .line 43
    .line 44
    return p0

    .line 45
    :pswitch_3
    const/16 p0, 0x20

    .line 46
    .line 47
    return p0

    .line 48
    :pswitch_4
    const/4 p0, 0x2

    .line 49
    return p0

    .line 50
    :pswitch_5
    const/4 p0, 0x1

    .line 51
    return p0

    .line 52
    nop

    .line 53
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public static typeForByteValue(B)Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eq p0, v0, :cond_2

    .line 3
    .line 4
    const/4 v0, 0x2

    .line 5
    if-eq p0, v0, :cond_1

    .line 6
    .line 7
    const/16 v0, 0x30

    .line 8
    .line 9
    if-eq p0, v0, :cond_0

    .line 10
    .line 11
    packed-switch p0, :pswitch_data_0

    .line 12
    .line 13
    .line 14
    sget-object p0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->OTHER:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 15
    .line 16
    return-object p0

    .line 17
    :pswitch_0
    sget-object p0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_SSL_CN:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 18
    .line 19
    return-object p0

    .line 20
    :pswitch_1
    sget-object p0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_SSL_VERSION:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 21
    .line 22
    return-object p0

    .line 23
    :pswitch_2
    sget-object p0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_SSL:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 24
    .line 25
    return-object p0

    .line 26
    :cond_0
    sget-object p0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_NETNS:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 27
    .line 28
    return-object p0

    .line 29
    :cond_1
    sget-object p0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_AUTHORITY:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 30
    .line 31
    return-object p0

    .line 32
    :cond_2
    sget-object p0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->PP2_TYPE_ALPN:Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 33
    .line 34
    return-object p0

    .line 35
    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public static valueOf(Ljava/lang/String;)Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;
    .locals 1

    .line 1
    const-class v0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;
    .locals 1

    .line 1
    sget-object v0, Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->$VALUES:[Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lio/netty/handler/codec/haproxy/HAProxyTLV$Type;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

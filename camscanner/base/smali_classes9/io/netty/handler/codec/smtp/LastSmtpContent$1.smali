.class final Lio/netty/handler/codec/smtp/LastSmtpContent$1;
.super Ljava/lang/Object;
.source "LastSmtpContent.java"

# interfaces
.implements Lio/netty/handler/codec/smtp/LastSmtpContent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/netty/handler/codec/smtp/LastSmtpContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public content()Lio/netty/buffer/ByteBuf;
    .locals 1

    .line 1
    sget-object v0, Lio/netty/buffer/Unpooled;->EMPTY_BUFFER:Lio/netty/buffer/ByteBuf;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public bridge synthetic copy()Lio/netty/buffer/ByteBufHolder;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->copy()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public copy()Lio/netty/handler/codec/smtp/LastSmtpContent;
    .locals 0

    .line 1
    return-object p0
.end method

.method public bridge synthetic copy()Lio/netty/handler/codec/smtp/SmtpContent;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->copy()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic duplicate()Lio/netty/buffer/ByteBufHolder;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->duplicate()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public duplicate()Lio/netty/handler/codec/smtp/LastSmtpContent;
    .locals 0

    .line 1
    return-object p0
.end method

.method public bridge synthetic duplicate()Lio/netty/handler/codec/smtp/SmtpContent;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->duplicate()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public refCnt()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public release()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    return v0
.end method

.method public release(I)Z
    .locals 0

    .line 2
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic replace(Lio/netty/buffer/ByteBuf;)Lio/netty/buffer/ByteBufHolder;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->replace(Lio/netty/buffer/ByteBuf;)Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object p1

    return-object p1
.end method

.method public replace(Lio/netty/buffer/ByteBuf;)Lio/netty/handler/codec/smtp/LastSmtpContent;
    .locals 1

    .line 3
    new-instance v0, Lio/netty/handler/codec/smtp/DefaultLastSmtpContent;

    invoke-direct {v0, p1}, Lio/netty/handler/codec/smtp/DefaultLastSmtpContent;-><init>(Lio/netty/buffer/ByteBuf;)V

    return-object v0
.end method

.method public bridge synthetic replace(Lio/netty/buffer/ByteBuf;)Lio/netty/handler/codec/smtp/SmtpContent;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->replace(Lio/netty/buffer/ByteBuf;)Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic retain()Lio/netty/buffer/ByteBufHolder;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->retain()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic retain(I)Lio/netty/buffer/ByteBufHolder;
    .locals 0

    .line 4
    invoke-virtual {p0, p1}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->retain(I)Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object p1

    return-object p1
.end method

.method public retain()Lio/netty/handler/codec/smtp/LastSmtpContent;
    .locals 0

    .line 1
    return-object p0
.end method

.method public retain(I)Lio/netty/handler/codec/smtp/LastSmtpContent;
    .locals 0

    .line 2
    return-object p0
.end method

.method public bridge synthetic retain()Lio/netty/handler/codec/smtp/SmtpContent;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->retain()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic retain(I)Lio/netty/handler/codec/smtp/SmtpContent;
    .locals 0

    .line 6
    invoke-virtual {p0, p1}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->retain(I)Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic retain()Lio/netty/util/ReferenceCounted;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->retain()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic retain(I)Lio/netty/util/ReferenceCounted;
    .locals 0

    .line 8
    invoke-virtual {p0, p1}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->retain(I)Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic retainedDuplicate()Lio/netty/buffer/ByteBufHolder;
    .locals 1

    .line 2
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->retainedDuplicate()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public retainedDuplicate()Lio/netty/handler/codec/smtp/LastSmtpContent;
    .locals 0

    .line 1
    return-object p0
.end method

.method public bridge synthetic retainedDuplicate()Lio/netty/handler/codec/smtp/SmtpContent;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->retainedDuplicate()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic touch()Lio/netty/buffer/ByteBufHolder;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->touch()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic touch(Ljava/lang/Object;)Lio/netty/buffer/ByteBufHolder;
    .locals 0

    .line 4
    invoke-virtual {p0, p1}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->touch(Ljava/lang/Object;)Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object p1

    return-object p1
.end method

.method public touch()Lio/netty/handler/codec/smtp/LastSmtpContent;
    .locals 0

    .line 1
    return-object p0
.end method

.method public touch(Ljava/lang/Object;)Lio/netty/handler/codec/smtp/LastSmtpContent;
    .locals 0

    .line 2
    return-object p0
.end method

.method public bridge synthetic touch()Lio/netty/handler/codec/smtp/SmtpContent;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->touch()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic touch(Ljava/lang/Object;)Lio/netty/handler/codec/smtp/SmtpContent;
    .locals 0

    .line 6
    invoke-virtual {p0, p1}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->touch(Ljava/lang/Object;)Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic touch()Lio/netty/util/ReferenceCounted;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->touch()Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic touch(Ljava/lang/Object;)Lio/netty/util/ReferenceCounted;
    .locals 0

    .line 8
    invoke-virtual {p0, p1}, Lio/netty/handler/codec/smtp/LastSmtpContent$1;->touch(Ljava/lang/Object;)Lio/netty/handler/codec/smtp/LastSmtpContent;

    move-result-object p1

    return-object p1
.end method

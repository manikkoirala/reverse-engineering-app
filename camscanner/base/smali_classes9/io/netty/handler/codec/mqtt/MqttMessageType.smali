.class public final enum Lio/netty/handler/codec/mqtt/MqttMessageType;
.super Ljava/lang/Enum;
.source "MqttMessageType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/netty/handler/codec/mqtt/MqttMessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum AUTH:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum CONNACK:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum CONNECT:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum DISCONNECT:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum PINGREQ:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum PINGRESP:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum PUBACK:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum PUBCOMP:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum PUBLISH:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum PUBREC:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum PUBREL:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum SUBACK:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum SUBSCRIBE:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum UNSUBACK:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field public static final enum UNSUBSCRIBE:Lio/netty/handler/codec/mqtt/MqttMessageType;

.field private static final VALUES:[Lio/netty/handler/codec/mqtt/MqttMessageType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 2
    .line 3
    const-string v1, "CONNECT"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x1

    .line 7
    invoke-direct {v0, v1, v2, v3}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lio/netty/handler/codec/mqtt/MqttMessageType;->CONNECT:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 11
    .line 12
    new-instance v1, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 13
    .line 14
    const-string v4, "CONNACK"

    .line 15
    .line 16
    const/4 v5, 0x2

    .line 17
    invoke-direct {v1, v4, v3, v5}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 18
    .line 19
    .line 20
    sput-object v1, Lio/netty/handler/codec/mqtt/MqttMessageType;->CONNACK:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 21
    .line 22
    new-instance v4, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 23
    .line 24
    const-string v6, "PUBLISH"

    .line 25
    .line 26
    const/4 v7, 0x3

    .line 27
    invoke-direct {v4, v6, v5, v7}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 28
    .line 29
    .line 30
    sput-object v4, Lio/netty/handler/codec/mqtt/MqttMessageType;->PUBLISH:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 31
    .line 32
    new-instance v6, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 33
    .line 34
    const-string v8, "PUBACK"

    .line 35
    .line 36
    const/4 v9, 0x4

    .line 37
    invoke-direct {v6, v8, v7, v9}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 38
    .line 39
    .line 40
    sput-object v6, Lio/netty/handler/codec/mqtt/MqttMessageType;->PUBACK:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 41
    .line 42
    new-instance v8, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 43
    .line 44
    const-string v10, "PUBREC"

    .line 45
    .line 46
    const/4 v11, 0x5

    .line 47
    invoke-direct {v8, v10, v9, v11}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 48
    .line 49
    .line 50
    sput-object v8, Lio/netty/handler/codec/mqtt/MqttMessageType;->PUBREC:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 51
    .line 52
    new-instance v10, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 53
    .line 54
    const-string v12, "PUBREL"

    .line 55
    .line 56
    const/4 v13, 0x6

    .line 57
    invoke-direct {v10, v12, v11, v13}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 58
    .line 59
    .line 60
    sput-object v10, Lio/netty/handler/codec/mqtt/MqttMessageType;->PUBREL:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 61
    .line 62
    new-instance v12, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 63
    .line 64
    const-string v14, "PUBCOMP"

    .line 65
    .line 66
    const/4 v15, 0x7

    .line 67
    invoke-direct {v12, v14, v13, v15}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 68
    .line 69
    .line 70
    sput-object v12, Lio/netty/handler/codec/mqtt/MqttMessageType;->PUBCOMP:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 71
    .line 72
    new-instance v14, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 73
    .line 74
    const-string v13, "SUBSCRIBE"

    .line 75
    .line 76
    const/16 v11, 0x8

    .line 77
    .line 78
    invoke-direct {v14, v13, v15, v11}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 79
    .line 80
    .line 81
    sput-object v14, Lio/netty/handler/codec/mqtt/MqttMessageType;->SUBSCRIBE:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 82
    .line 83
    new-instance v13, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 84
    .line 85
    const-string v15, "SUBACK"

    .line 86
    .line 87
    const/16 v9, 0x9

    .line 88
    .line 89
    invoke-direct {v13, v15, v11, v9}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 90
    .line 91
    .line 92
    sput-object v13, Lio/netty/handler/codec/mqtt/MqttMessageType;->SUBACK:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 93
    .line 94
    new-instance v15, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 95
    .line 96
    const-string v11, "UNSUBSCRIBE"

    .line 97
    .line 98
    const/16 v7, 0xa

    .line 99
    .line 100
    invoke-direct {v15, v11, v9, v7}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 101
    .line 102
    .line 103
    sput-object v15, Lio/netty/handler/codec/mqtt/MqttMessageType;->UNSUBSCRIBE:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 104
    .line 105
    new-instance v11, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 106
    .line 107
    const-string v9, "UNSUBACK"

    .line 108
    .line 109
    const/16 v5, 0xb

    .line 110
    .line 111
    invoke-direct {v11, v9, v7, v5}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 112
    .line 113
    .line 114
    sput-object v11, Lio/netty/handler/codec/mqtt/MqttMessageType;->UNSUBACK:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 115
    .line 116
    new-instance v9, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 117
    .line 118
    const-string v7, "PINGREQ"

    .line 119
    .line 120
    const/16 v3, 0xc

    .line 121
    .line 122
    invoke-direct {v9, v7, v5, v3}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 123
    .line 124
    .line 125
    sput-object v9, Lio/netty/handler/codec/mqtt/MqttMessageType;->PINGREQ:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 126
    .line 127
    new-instance v7, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 128
    .line 129
    const-string v5, "PINGRESP"

    .line 130
    .line 131
    const/16 v2, 0xd

    .line 132
    .line 133
    invoke-direct {v7, v5, v3, v2}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 134
    .line 135
    .line 136
    sput-object v7, Lio/netty/handler/codec/mqtt/MqttMessageType;->PINGRESP:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 137
    .line 138
    new-instance v5, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 139
    .line 140
    const-string v3, "DISCONNECT"

    .line 141
    .line 142
    move-object/from16 v16, v7

    .line 143
    .line 144
    const/16 v7, 0xe

    .line 145
    .line 146
    invoke-direct {v5, v3, v2, v7}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 147
    .line 148
    .line 149
    sput-object v5, Lio/netty/handler/codec/mqtt/MqttMessageType;->DISCONNECT:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 150
    .line 151
    new-instance v3, Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 152
    .line 153
    const-string v2, "AUTH"

    .line 154
    .line 155
    move-object/from16 v17, v5

    .line 156
    .line 157
    const/16 v5, 0xf

    .line 158
    .line 159
    invoke-direct {v3, v2, v7, v5}, Lio/netty/handler/codec/mqtt/MqttMessageType;-><init>(Ljava/lang/String;II)V

    .line 160
    .line 161
    .line 162
    sput-object v3, Lio/netty/handler/codec/mqtt/MqttMessageType;->AUTH:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 163
    .line 164
    new-array v2, v5, [Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 165
    .line 166
    const/4 v5, 0x0

    .line 167
    aput-object v0, v2, v5

    .line 168
    .line 169
    const/4 v0, 0x1

    .line 170
    aput-object v1, v2, v0

    .line 171
    .line 172
    const/4 v0, 0x2

    .line 173
    aput-object v4, v2, v0

    .line 174
    .line 175
    const/4 v0, 0x3

    .line 176
    aput-object v6, v2, v0

    .line 177
    .line 178
    const/4 v0, 0x4

    .line 179
    aput-object v8, v2, v0

    .line 180
    .line 181
    const/4 v0, 0x5

    .line 182
    aput-object v10, v2, v0

    .line 183
    .line 184
    const/4 v0, 0x6

    .line 185
    aput-object v12, v2, v0

    .line 186
    .line 187
    const/4 v0, 0x7

    .line 188
    aput-object v14, v2, v0

    .line 189
    .line 190
    const/16 v0, 0x8

    .line 191
    .line 192
    aput-object v13, v2, v0

    .line 193
    .line 194
    const/16 v0, 0x9

    .line 195
    .line 196
    aput-object v15, v2, v0

    .line 197
    .line 198
    const/16 v0, 0xa

    .line 199
    .line 200
    aput-object v11, v2, v0

    .line 201
    .line 202
    const/16 v0, 0xb

    .line 203
    .line 204
    aput-object v9, v2, v0

    .line 205
    .line 206
    const/16 v0, 0xc

    .line 207
    .line 208
    aput-object v16, v2, v0

    .line 209
    .line 210
    const/16 v0, 0xd

    .line 211
    .line 212
    aput-object v17, v2, v0

    .line 213
    .line 214
    aput-object v3, v2, v7

    .line 215
    .line 216
    sput-object v2, Lio/netty/handler/codec/mqtt/MqttMessageType;->$VALUES:[Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 217
    .line 218
    invoke-static {}, Lio/netty/handler/codec/mqtt/MqttMessageType;->values()[Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 219
    .line 220
    .line 221
    move-result-object v0

    .line 222
    array-length v1, v0

    .line 223
    const/4 v2, 0x1

    .line 224
    add-int/2addr v1, v2

    .line 225
    new-array v1, v1, [Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 226
    .line 227
    sput-object v1, Lio/netty/handler/codec/mqtt/MqttMessageType;->VALUES:[Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 228
    .line 229
    array-length v1, v0

    .line 230
    const/4 v2, 0x0

    .line 231
    :goto_0
    if-ge v2, v1, :cond_1

    .line 232
    .line 233
    aget-object v3, v0, v2

    .line 234
    .line 235
    iget v4, v3, Lio/netty/handler/codec/mqtt/MqttMessageType;->value:I

    .line 236
    .line 237
    sget-object v5, Lio/netty/handler/codec/mqtt/MqttMessageType;->VALUES:[Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 238
    .line 239
    aget-object v6, v5, v4

    .line 240
    .line 241
    if-nez v6, :cond_0

    .line 242
    .line 243
    aput-object v3, v5, v4

    .line 244
    .line 245
    add-int/lit8 v2, v2, 0x1

    .line 246
    .line 247
    goto :goto_0

    .line 248
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    .line 249
    .line 250
    new-instance v1, Ljava/lang/StringBuilder;

    .line 251
    .line 252
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 253
    .line 254
    .line 255
    const-string/jumbo v2, "value already in use: "

    .line 256
    .line 257
    .line 258
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    .line 260
    .line 261
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 262
    .line 263
    .line 264
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 265
    .line 266
    .line 267
    move-result-object v1

    .line 268
    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    .line 269
    .line 270
    .line 271
    throw v0

    .line 272
    :cond_1
    return-void
    .line 273
    .line 274
    .line 275
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lio/netty/handler/codec/mqtt/MqttMessageType;->value:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public static valueOf(I)Lio/netty/handler/codec/mqtt/MqttMessageType;
    .locals 3

    if-lez p0, :cond_0

    .line 2
    sget-object v0, Lio/netty/handler/codec/mqtt/MqttMessageType;->VALUES:[Lio/netty/handler/codec/mqtt/MqttMessageType;

    array-length v1, v0

    if-ge p0, v1, :cond_0

    .line 3
    aget-object p0, v0, p0

    return-object p0

    .line 4
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown message type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lio/netty/handler/codec/mqtt/MqttMessageType;
    .locals 1

    .line 1
    const-class v0, Lio/netty/handler/codec/mqtt/MqttMessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lio/netty/handler/codec/mqtt/MqttMessageType;

    return-object p0
.end method

.method public static values()[Lio/netty/handler/codec/mqtt/MqttMessageType;
    .locals 1

    .line 1
    sget-object v0, Lio/netty/handler/codec/mqtt/MqttMessageType;->$VALUES:[Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lio/netty/handler/codec/mqtt/MqttMessageType;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public value()I
    .locals 1

    .line 1
    iget v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageType;->value:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

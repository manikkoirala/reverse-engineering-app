.class public final Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;
.super Ljava/lang/Object;
.source "MqttMessageBuilders.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/netty/handler/codec/mqtt/MqttMessageBuilders;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConnAckBuilder"
.end annotation


# instance fields
.field private properties:Lio/netty/handler/codec/mqtt/MqttProperties;

.field private propsBuilder:Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckPropertiesBuilder;

.field private returnCode:Lio/netty/handler/codec/mqtt/MqttConnectReturnCode;

.field private sessionPresent:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    sget-object v0, Lio/netty/handler/codec/mqtt/MqttProperties;->NO_PROPERTIES:Lio/netty/handler/codec/mqtt/MqttProperties;

    iput-object v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->properties:Lio/netty/handler/codec/mqtt/MqttProperties;

    return-void
.end method

.method synthetic constructor <init>(Lio/netty/handler/codec/mqtt/MqttMessageBuilders$1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lio/netty/handler/codec/mqtt/MqttConnAckMessage;
    .locals 7

    .line 1
    iget-object v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->propsBuilder:Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckPropertiesBuilder;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckPropertiesBuilder;->build()Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->properties:Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 10
    .line 11
    :cond_0
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttFixedHeader;

    .line 12
    .line 13
    sget-object v2, Lio/netty/handler/codec/mqtt/MqttMessageType;->CONNACK:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    sget-object v4, Lio/netty/handler/codec/mqtt/MqttQoS;->AT_MOST_ONCE:Lio/netty/handler/codec/mqtt/MqttQoS;

    .line 17
    .line 18
    const/4 v5, 0x0

    .line 19
    const/4 v6, 0x0

    .line 20
    move-object v1, v0

    .line 21
    invoke-direct/range {v1 .. v6}, Lio/netty/handler/codec/mqtt/MqttFixedHeader;-><init>(Lio/netty/handler/codec/mqtt/MqttMessageType;ZLio/netty/handler/codec/mqtt/MqttQoS;ZI)V

    .line 22
    .line 23
    .line 24
    new-instance v1, Lio/netty/handler/codec/mqtt/MqttConnAckVariableHeader;

    .line 25
    .line 26
    iget-object v2, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->returnCode:Lio/netty/handler/codec/mqtt/MqttConnectReturnCode;

    .line 27
    .line 28
    iget-boolean v3, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->sessionPresent:Z

    .line 29
    .line 30
    iget-object v4, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->properties:Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 31
    .line 32
    invoke-direct {v1, v2, v3, v4}, Lio/netty/handler/codec/mqtt/MqttConnAckVariableHeader;-><init>(Lio/netty/handler/codec/mqtt/MqttConnectReturnCode;ZLio/netty/handler/codec/mqtt/MqttProperties;)V

    .line 33
    .line 34
    .line 35
    new-instance v2, Lio/netty/handler/codec/mqtt/MqttConnAckMessage;

    .line 36
    .line 37
    invoke-direct {v2, v0, v1}, Lio/netty/handler/codec/mqtt/MqttConnAckMessage;-><init>(Lio/netty/handler/codec/mqtt/MqttFixedHeader;Lio/netty/handler/codec/mqtt/MqttConnAckVariableHeader;)V

    .line 38
    .line 39
    .line 40
    return-object v2
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public properties(Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PropertiesInitializer;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PropertiesInitializer<",
            "Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckPropertiesBuilder;",
            ">;)",
            "Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->propsBuilder:Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckPropertiesBuilder;

    if-nez v0, :cond_0

    .line 3
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckPropertiesBuilder;

    invoke-direct {v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckPropertiesBuilder;-><init>()V

    iput-object v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->propsBuilder:Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckPropertiesBuilder;

    .line 4
    :cond_0
    iget-object v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->propsBuilder:Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckPropertiesBuilder;

    invoke-interface {p1, v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PropertiesInitializer;->apply(Ljava/lang/Object;)V

    return-object p0
.end method

.method public properties(Lio/netty/handler/codec/mqtt/MqttProperties;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->properties:Lio/netty/handler/codec/mqtt/MqttProperties;

    return-object p0
.end method

.method public returnCode(Lio/netty/handler/codec/mqtt/MqttConnectReturnCode;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->returnCode:Lio/netty/handler/codec/mqtt/MqttConnectReturnCode;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public sessionPresent(Z)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;->sessionPresent:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

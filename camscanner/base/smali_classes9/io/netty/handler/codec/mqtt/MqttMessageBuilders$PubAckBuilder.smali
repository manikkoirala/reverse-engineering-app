.class public final Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;
.super Ljava/lang/Object;
.source "MqttMessageBuilders.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/netty/handler/codec/mqtt/MqttMessageBuilders;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PubAckBuilder"
.end annotation


# instance fields
.field private packetId:I

.field private properties:Lio/netty/handler/codec/mqtt/MqttProperties;

.field private reasonCode:B


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public build()Lio/netty/handler/codec/mqtt/MqttMessage;
    .locals 7

    .line 1
    new-instance v6, Lio/netty/handler/codec/mqtt/MqttFixedHeader;

    .line 2
    .line 3
    sget-object v1, Lio/netty/handler/codec/mqtt/MqttMessageType;->PUBACK:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    sget-object v3, Lio/netty/handler/codec/mqtt/MqttQoS;->AT_MOST_ONCE:Lio/netty/handler/codec/mqtt/MqttQoS;

    .line 7
    .line 8
    const/4 v4, 0x0

    .line 9
    const/4 v5, 0x0

    .line 10
    move-object v0, v6

    .line 11
    invoke-direct/range {v0 .. v5}, Lio/netty/handler/codec/mqtt/MqttFixedHeader;-><init>(Lio/netty/handler/codec/mqtt/MqttMessageType;ZLio/netty/handler/codec/mqtt/MqttQoS;ZI)V

    .line 12
    .line 13
    .line 14
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttPubReplyMessageVariableHeader;

    .line 15
    .line 16
    iget v1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;->packetId:I

    .line 17
    .line 18
    iget-byte v2, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;->reasonCode:B

    .line 19
    .line 20
    iget-object v3, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;->properties:Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 21
    .line 22
    invoke-direct {v0, v1, v2, v3}, Lio/netty/handler/codec/mqtt/MqttPubReplyMessageVariableHeader;-><init>(IBLio/netty/handler/codec/mqtt/MqttProperties;)V

    .line 23
    .line 24
    .line 25
    new-instance v1, Lio/netty/handler/codec/mqtt/MqttMessage;

    .line 26
    .line 27
    invoke-direct {v1, v6, v0}, Lio/netty/handler/codec/mqtt/MqttMessage;-><init>(Lio/netty/handler/codec/mqtt/MqttFixedHeader;Ljava/lang/Object;)V

    .line 28
    .line 29
    .line 30
    return-object v1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method public packetId(I)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;
    .locals 0

    .line 1
    iput p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;->packetId:I

    return-object p0
.end method

.method public packetId(S)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const v0, 0xffff

    and-int/2addr p1, v0

    .line 2
    invoke-virtual {p0, p1}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;->packetId(I)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;

    move-result-object p1

    return-object p1
.end method

.method public properties(Lio/netty/handler/codec/mqtt/MqttProperties;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;->properties:Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public reasonCode(B)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;
    .locals 0

    .line 1
    iput-byte p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;->reasonCode:B

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

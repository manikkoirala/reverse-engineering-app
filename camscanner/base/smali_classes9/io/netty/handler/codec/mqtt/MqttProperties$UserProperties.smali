.class public final Lio/netty/handler/codec/mqtt/MqttProperties$UserProperties;
.super Lio/netty/handler/codec/mqtt/MqttProperties$MqttProperty;
.source "MqttProperties.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/netty/handler/codec/mqtt/MqttProperties;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserProperties"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/netty/handler/codec/mqtt/MqttProperties$MqttProperty<",
        "Ljava/util/List<",
        "Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    sget-object v0, Lio/netty/handler/codec/mqtt/MqttProperties$MqttPropertyType;->USER_PROPERTY:Lio/netty/handler/codec/mqtt/MqttProperties$MqttPropertyType;

    invoke-static {v0}, Lio/netty/handler/codec/mqtt/MqttProperties$MqttPropertyType;->access$000(Lio/netty/handler/codec/mqtt/MqttProperties$MqttPropertyType;)I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v0, v1}, Lio/netty/handler/codec/mqtt/MqttProperties$MqttProperty;-><init>(ILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;",
            ">;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Lio/netty/handler/codec/mqtt/MqttProperties$UserProperties;-><init>()V

    .line 3
    iget-object v0, p0, Lio/netty/handler/codec/mqtt/MqttProperties$MqttProperty;->value:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method static synthetic access$100(Ljava/util/Collection;)Lio/netty/handler/codec/mqtt/MqttProperties$UserProperties;
    .locals 0

    .line 1
    invoke-static {p0}, Lio/netty/handler/codec/mqtt/MqttProperties$UserProperties;->fromUserPropertyCollection(Ljava/util/Collection;)Lio/netty/handler/codec/mqtt/MqttProperties$UserProperties;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private static fromUserPropertyCollection(Ljava/util/Collection;)Lio/netty/handler/codec/mqtt/MqttProperties$UserProperties;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lio/netty/handler/codec/mqtt/MqttProperties$UserProperty;",
            ">;)",
            "Lio/netty/handler/codec/mqtt/MqttProperties$UserProperties;"
        }
    .end annotation

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttProperties$UserProperties;

    .line 2
    .line 3
    invoke-direct {v0}, Lio/netty/handler/codec/mqtt/MqttProperties$UserProperties;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Lio/netty/handler/codec/mqtt/MqttProperties$UserProperty;

    .line 21
    .line 22
    new-instance v2, Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;

    .line 23
    .line 24
    iget-object v1, v1, Lio/netty/handler/codec/mqtt/MqttProperties$MqttProperty;->value:Ljava/lang/Object;

    .line 25
    .line 26
    move-object v3, v1

    .line 27
    check-cast v3, Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;

    .line 28
    .line 29
    iget-object v3, v3, Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;->key:Ljava/lang/String;

    .line 30
    .line 31
    check-cast v1, Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;

    .line 32
    .line 33
    iget-object v1, v1, Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;->value:Ljava/lang/String;

    .line 34
    .line 35
    invoke-direct {v2, v3, v1}, Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v2}, Lio/netty/handler/codec/mqtt/MqttProperties$UserProperties;->add(Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    return-object v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method


# virtual methods
.method public add(Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lio/netty/handler/codec/mqtt/MqttProperties$MqttProperty;->value:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public add(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lio/netty/handler/codec/mqtt/MqttProperties$MqttProperty;->value:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    new-instance v1, Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;

    invoke-direct {v1, p1, p2}, Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    const-string v1, "UserProperties("

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lio/netty/handler/codec/mqtt/MqttProperties$MqttProperty;->value:Ljava/lang/Object;

    .line 9
    .line 10
    check-cast v1, Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    const/4 v2, 0x1

    .line 17
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    if-eqz v3, :cond_1

    .line 22
    .line 23
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    check-cast v3, Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;

    .line 28
    .line 29
    if-nez v2, :cond_0

    .line 30
    .line 31
    const-string v2, ", "

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    iget-object v4, v3, Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;->key:Ljava/lang/String;

    .line 42
    .line 43
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v4, "->"

    .line 47
    .line 48
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    iget-object v3, v3, Lio/netty/handler/codec/mqtt/MqttProperties$StringPair;->value:Ljava/lang/String;

    .line 52
    .line 53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    const/4 v2, 0x0

    .line 64
    goto :goto_0

    .line 65
    :cond_1
    const-string v1, ")"

    .line 66
    .line 67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    return-object v0
    .line 75
.end method

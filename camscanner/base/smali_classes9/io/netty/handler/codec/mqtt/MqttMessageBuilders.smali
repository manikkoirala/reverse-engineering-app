.class public final Lio/netty/handler/codec/mqtt/MqttMessageBuilders;
.super Ljava/lang/Object;
.source "MqttMessageBuilders.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$AuthBuilder;,
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$DisconnectBuilder;,
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$UnsubAckBuilder;,
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$SubAckBuilder;,
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;,
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckPropertiesBuilder;,
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;,
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PropertiesInitializer;,
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$UnsubscribeBuilder;,
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$SubscribeBuilder;,
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;,
        Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PublishBuilder;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static auth()Lio/netty/handler/codec/mqtt/MqttMessageBuilders$AuthBuilder;
    .locals 1

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$AuthBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$AuthBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static connAck()Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;
    .locals 2

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnAckBuilder;-><init>(Lio/netty/handler/codec/mqtt/MqttMessageBuilders$1;)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static connect()Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 1

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static disconnect()Lio/netty/handler/codec/mqtt/MqttMessageBuilders$DisconnectBuilder;
    .locals 1

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$DisconnectBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$DisconnectBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static pubAck()Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;
    .locals 1

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PubAckBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static publish()Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PublishBuilder;
    .locals 1

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PublishBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$PublishBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static subAck()Lio/netty/handler/codec/mqtt/MqttMessageBuilders$SubAckBuilder;
    .locals 1

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$SubAckBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$SubAckBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static subscribe()Lio/netty/handler/codec/mqtt/MqttMessageBuilders$SubscribeBuilder;
    .locals 1

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$SubscribeBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$SubscribeBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static unsubAck()Lio/netty/handler/codec/mqtt/MqttMessageBuilders$UnsubAckBuilder;
    .locals 1

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$UnsubAckBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$UnsubAckBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public static unsubscribe()Lio/netty/handler/codec/mqtt/MqttMessageBuilders$UnsubscribeBuilder;
    .locals 1

    .line 1
    new-instance v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$UnsubscribeBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$UnsubscribeBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

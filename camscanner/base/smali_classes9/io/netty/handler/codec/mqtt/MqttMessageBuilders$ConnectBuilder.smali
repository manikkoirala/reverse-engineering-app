.class public final Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
.super Ljava/lang/Object;
.source "MqttMessageBuilders.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/netty/handler/codec/mqtt/MqttMessageBuilders;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConnectBuilder"
.end annotation


# instance fields
.field private cleanSession:Z

.field private clientId:Ljava/lang/String;

.field private hasPassword:Z

.field private hasUser:Z

.field private keepAliveSecs:I

.field private password:[B

.field private properties:Lio/netty/handler/codec/mqtt/MqttProperties;

.field private username:Ljava/lang/String;

.field private version:Lio/netty/handler/codec/mqtt/MqttVersion;

.field private willFlag:Z

.field private willMessage:[B

.field private willProperties:Lio/netty/handler/codec/mqtt/MqttProperties;

.field private willQos:Lio/netty/handler/codec/mqtt/MqttQoS;

.field private willRetain:Z

.field private willTopic:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lio/netty/handler/codec/mqtt/MqttVersion;->MQTT_3_1_1:Lio/netty/handler/codec/mqtt/MqttVersion;

    .line 5
    .line 6
    iput-object v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->version:Lio/netty/handler/codec/mqtt/MqttVersion;

    .line 7
    .line 8
    sget-object v0, Lio/netty/handler/codec/mqtt/MqttProperties;->NO_PROPERTIES:Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 9
    .line 10
    iput-object v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willProperties:Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 11
    .line 12
    sget-object v1, Lio/netty/handler/codec/mqtt/MqttQoS;->AT_MOST_ONCE:Lio/netty/handler/codec/mqtt/MqttQoS;

    .line 13
    .line 14
    iput-object v1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willQos:Lio/netty/handler/codec/mqtt/MqttQoS;

    .line 15
    .line 16
    iput-object v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->properties:Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 17
    .line 18
    return-void
    .line 19
.end method


# virtual methods
.method public build()Lio/netty/handler/codec/mqtt/MqttConnectMessage;
    .locals 23

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    new-instance v7, Lio/netty/handler/codec/mqtt/MqttFixedHeader;

    .line 4
    .line 5
    sget-object v2, Lio/netty/handler/codec/mqtt/MqttMessageType;->CONNECT:Lio/netty/handler/codec/mqtt/MqttMessageType;

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    sget-object v4, Lio/netty/handler/codec/mqtt/MqttQoS;->AT_MOST_ONCE:Lio/netty/handler/codec/mqtt/MqttQoS;

    .line 9
    .line 10
    const/4 v5, 0x0

    .line 11
    const/4 v6, 0x0

    .line 12
    move-object v1, v7

    .line 13
    invoke-direct/range {v1 .. v6}, Lio/netty/handler/codec/mqtt/MqttFixedHeader;-><init>(Lio/netty/handler/codec/mqtt/MqttMessageType;ZLio/netty/handler/codec/mqtt/MqttQoS;ZI)V

    .line 14
    .line 15
    .line 16
    new-instance v1, Lio/netty/handler/codec/mqtt/MqttConnectVariableHeader;

    .line 17
    .line 18
    iget-object v2, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->version:Lio/netty/handler/codec/mqtt/MqttVersion;

    .line 19
    .line 20
    invoke-virtual {v2}, Lio/netty/handler/codec/mqtt/MqttVersion;->protocolName()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v9

    .line 24
    iget-object v2, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->version:Lio/netty/handler/codec/mqtt/MqttVersion;

    .line 25
    .line 26
    invoke-virtual {v2}, Lio/netty/handler/codec/mqtt/MqttVersion;->protocolLevel()B

    .line 27
    .line 28
    .line 29
    move-result v10

    .line 30
    iget-boolean v11, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->hasUser:Z

    .line 31
    .line 32
    iget-boolean v12, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->hasPassword:Z

    .line 33
    .line 34
    iget-boolean v13, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willRetain:Z

    .line 35
    .line 36
    iget-object v2, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willQos:Lio/netty/handler/codec/mqtt/MqttQoS;

    .line 37
    .line 38
    invoke-virtual {v2}, Lio/netty/handler/codec/mqtt/MqttQoS;->value()I

    .line 39
    .line 40
    .line 41
    move-result v14

    .line 42
    iget-boolean v15, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willFlag:Z

    .line 43
    .line 44
    iget-boolean v2, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->cleanSession:Z

    .line 45
    .line 46
    iget v3, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->keepAliveSecs:I

    .line 47
    .line 48
    iget-object v4, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->properties:Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 49
    .line 50
    move-object v8, v1

    .line 51
    move/from16 v16, v2

    .line 52
    .line 53
    move/from16 v17, v3

    .line 54
    .line 55
    move-object/from16 v18, v4

    .line 56
    .line 57
    invoke-direct/range {v8 .. v18}, Lio/netty/handler/codec/mqtt/MqttConnectVariableHeader;-><init>(Ljava/lang/String;IZZZIZZILio/netty/handler/codec/mqtt/MqttProperties;)V

    .line 58
    .line 59
    .line 60
    new-instance v2, Lio/netty/handler/codec/mqtt/MqttConnectPayload;

    .line 61
    .line 62
    iget-object v3, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->clientId:Ljava/lang/String;

    .line 63
    .line 64
    iget-object v4, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willProperties:Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 65
    .line 66
    iget-object v5, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willTopic:Ljava/lang/String;

    .line 67
    .line 68
    iget-object v6, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willMessage:[B

    .line 69
    .line 70
    iget-object v8, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->username:Ljava/lang/String;

    .line 71
    .line 72
    iget-object v9, v0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->password:[B

    .line 73
    .line 74
    move-object/from16 v16, v2

    .line 75
    .line 76
    move-object/from16 v17, v3

    .line 77
    .line 78
    move-object/from16 v18, v4

    .line 79
    .line 80
    move-object/from16 v19, v5

    .line 81
    .line 82
    move-object/from16 v20, v6

    .line 83
    .line 84
    move-object/from16 v21, v8

    .line 85
    .line 86
    move-object/from16 v22, v9

    .line 87
    .line 88
    invoke-direct/range {v16 .. v22}, Lio/netty/handler/codec/mqtt/MqttConnectPayload;-><init>(Ljava/lang/String;Lio/netty/handler/codec/mqtt/MqttProperties;Ljava/lang/String;[BLjava/lang/String;[B)V

    .line 89
    .line 90
    .line 91
    new-instance v3, Lio/netty/handler/codec/mqtt/MqttConnectMessage;

    .line 92
    .line 93
    invoke-direct {v3, v7, v1, v2}, Lio/netty/handler/codec/mqtt/MqttConnectMessage;-><init>(Lio/netty/handler/codec/mqtt/MqttFixedHeader;Lio/netty/handler/codec/mqtt/MqttConnectVariableHeader;Lio/netty/handler/codec/mqtt/MqttConnectPayload;)V

    .line 94
    .line 95
    .line 96
    return-object v3
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method public cleanSession(Z)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->cleanSession:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public clientId(Ljava/lang/String;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->clientId:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public hasPassword(Z)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->hasPassword:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public hasUser(Z)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->hasUser:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public keepAlive(I)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->keepAliveSecs:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public password(Ljava/lang/String;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 1
    :cond_0
    sget-object v0, Lio/netty/util/CharsetUtil;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    :goto_0
    invoke-virtual {p0, p1}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->password([B)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;

    return-object p0
.end method

.method public password([B)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 2
    :goto_0
    iput-boolean v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->hasPassword:Z

    .line 3
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->password:[B

    return-object p0
.end method

.method public properties(Lio/netty/handler/codec/mqtt/MqttProperties;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->properties:Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public protocolVersion(Lio/netty/handler/codec/mqtt/MqttVersion;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->version:Lio/netty/handler/codec/mqtt/MqttVersion;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public username(Ljava/lang/String;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    goto :goto_0

    .line 5
    :cond_0
    const/4 v0, 0x0

    .line 6
    :goto_0
    iput-boolean v0, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->hasUser:Z

    .line 7
    .line 8
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->username:Ljava/lang/String;

    .line 9
    .line 10
    return-object p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public willFlag(Z)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willFlag:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public willMessage(Ljava/lang/String;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 1
    :cond_0
    sget-object v0, Lio/netty/util/CharsetUtil;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    :goto_0
    invoke-virtual {p0, p1}, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willMessage([B)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;

    return-object p0
.end method

.method public willMessage([B)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 2
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willMessage:[B

    return-object p0
.end method

.method public willProperties(Lio/netty/handler/codec/mqtt/MqttProperties;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willProperties:Lio/netty/handler/codec/mqtt/MqttProperties;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public willQoS(Lio/netty/handler/codec/mqtt/MqttQoS;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willQos:Lio/netty/handler/codec/mqtt/MqttQoS;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public willRetain(Z)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willRetain:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public willTopic(Ljava/lang/String;)Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/netty/handler/codec/mqtt/MqttMessageBuilders$ConnectBuilder;->willTopic:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

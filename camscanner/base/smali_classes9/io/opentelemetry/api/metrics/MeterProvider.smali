.class public interface abstract Lio/opentelemetry/api/metrics/MeterProvider;
.super Ljava/lang/Object;
.source "MeterProvider.java"


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# virtual methods
.method public abstract get(Ljava/lang/String;)Lio/opentelemetry/api/metrics/Meter;
.end method

.method public abstract meterBuilder(Ljava/lang/String;)Lio/opentelemetry/api/metrics/MeterBuilder;
.end method

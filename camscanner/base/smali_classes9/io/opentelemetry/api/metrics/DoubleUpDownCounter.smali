.class public interface abstract Lio/opentelemetry/api/metrics/DoubleUpDownCounter;
.super Ljava/lang/Object;
.source "DoubleUpDownCounter.java"


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# virtual methods
.method public abstract add(D)V
.end method

.method public abstract add(DLio/opentelemetry/api/common/Attributes;)V
.end method

.method public abstract add(DLio/opentelemetry/api/common/Attributes;Lio/opentelemetry/context/Context;)V
.end method

.class public interface abstract Lio/opentelemetry/api/common/AttributeKey;
.super Ljava/lang/Object;
.source "AttributeKey.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# virtual methods
.method public abstract getKey()Ljava/lang/String;
.end method

.method public abstract getType()Lio/opentelemetry/api/common/AttributeType;
.end method

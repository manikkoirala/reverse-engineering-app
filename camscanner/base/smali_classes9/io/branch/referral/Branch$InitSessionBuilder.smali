.class public Lio/branch/referral/Branch$InitSessionBuilder;
.super Ljava/lang/Object;
.source "Branch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/branch/referral/Branch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InitSessionBuilder"
.end annotation


# instance fields
.field private O8:Landroid/net/Uri;

.field private Oo08:Ljava/lang/Boolean;

.field private o〇0:Z

.field private 〇080:Lio/branch/referral/Branch$BranchReferralInitListener;

.field private 〇o00〇〇Oo:Z

.field private 〇o〇:I


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .locals 3

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {}, Lio/branch/referral/Branch;->〇8〇0〇o〇O()Lio/branch/referral/Branch;

    move-result-object v0

    if-eqz p1, :cond_1

    .line 4
    invoke-virtual {v0}, Lio/branch/referral/Branch;->〇〇0o()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5
    invoke-virtual {v0}, Lio/branch/referral/Branch;->〇〇0o()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 6
    :cond_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lio/branch/referral/Branch;->〇O〇:Ljava/lang/ref/WeakReference;

    :cond_1
    return-void
.end method

.method synthetic constructor <init>(Landroid/app/Activity;Lio/branch/referral/Branch$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lio/branch/referral/Branch$InitSessionBuilder;-><init>(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method public O8(Lio/branch/referral/Branch$BranchReferralInitListener;)Lio/branch/referral/Branch$InitSessionBuilder;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/branch/referral/Branch$InitSessionBuilder;->〇080:Lio/branch/referral/Branch$BranchReferralInitListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public Oo08(Landroid/net/Uri;)Lio/branch/referral/Branch$InitSessionBuilder;
    .locals 0

    .line 1
    iput-object p1, p0, Lio/branch/referral/Branch$InitSessionBuilder;->O8:Landroid/net/Uri;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇080()V
    .locals 6

    .line 1
    invoke-static {}, Lio/branch/referral/Branch;->〇8〇0〇o〇O()Lio/branch/referral/Branch;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string v0, "Branch is not setup properly, make sure to call getAutoInstance in your application class or declare BranchApp in your manifest."

    .line 8
    .line 9
    invoke-static {v0}, Lio/branch/referral/PrefHelper;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iget-object v1, p0, Lio/branch/referral/Branch$InitSessionBuilder;->Oo08:Ljava/lang/Boolean;

    .line 14
    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    invoke-static {v1}, Lio/branch/referral/Branch;->o800o8O(Z)V

    .line 22
    .line 23
    .line 24
    :cond_1
    invoke-virtual {v0}, Lio/branch/referral/Branch;->〇〇0o()Landroid/app/Activity;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    const/4 v2, 0x0

    .line 29
    if-eqz v1, :cond_2

    .line 30
    .line 31
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    goto :goto_0

    .line 36
    :cond_2
    move-object v3, v2

    .line 37
    :goto_0
    if-eqz v1, :cond_3

    .line 38
    .line 39
    if-eqz v3, :cond_3

    .line 40
    .line 41
    invoke-static {v1}, Landroidx/core/app/ActivityCompat;->getReferrer(Landroid/app/Activity;)Landroid/net/Uri;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    if-eqz v4, :cond_3

    .line 46
    .line 47
    invoke-static {v1}, Lio/branch/referral/PrefHelper;->〇oOO8O8(Landroid/content/Context;)Lio/branch/referral/PrefHelper;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    invoke-static {v1}, Landroidx/core/app/ActivityCompat;->getReferrer(Landroid/app/Activity;)Landroid/net/Uri;

    .line 52
    .line 53
    .line 54
    move-result-object v5

    .line 55
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v5

    .line 59
    invoke-virtual {v4, v5}, Lio/branch/referral/PrefHelper;->OOO8o〇〇(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    :cond_3
    iget-object v4, p0, Lio/branch/referral/Branch$InitSessionBuilder;->O8:Landroid/net/Uri;

    .line 63
    .line 64
    if-eqz v4, :cond_4

    .line 65
    .line 66
    invoke-static {v0, v4, v1}, Lio/branch/referral/Branch;->〇O8o08O(Lio/branch/referral/Branch;Landroid/net/Uri;Landroid/app/Activity;)V

    .line 67
    .line 68
    .line 69
    goto :goto_2

    .line 70
    :cond_4
    iget-boolean v4, p0, Lio/branch/referral/Branch$InitSessionBuilder;->o〇0:Z

    .line 71
    .line 72
    if-eqz v4, :cond_6

    .line 73
    .line 74
    invoke-virtual {v0, v3}, Lio/branch/referral/Branch;->o8O〇(Landroid/content/Intent;)Z

    .line 75
    .line 76
    .line 77
    move-result v4

    .line 78
    if-eqz v4, :cond_6

    .line 79
    .line 80
    if-eqz v3, :cond_5

    .line 81
    .line 82
    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 83
    .line 84
    .line 85
    move-result-object v3

    .line 86
    goto :goto_1

    .line 87
    :cond_5
    move-object v3, v2

    .line 88
    :goto_1
    invoke-static {v0, v3, v1}, Lio/branch/referral/Branch;->〇O8o08O(Lio/branch/referral/Branch;Landroid/net/Uri;Landroid/app/Activity;)V

    .line 89
    .line 90
    .line 91
    goto :goto_2

    .line 92
    :cond_6
    iget-boolean v1, p0, Lio/branch/referral/Branch$InitSessionBuilder;->o〇0:Z

    .line 93
    .line 94
    if-eqz v1, :cond_8

    .line 95
    .line 96
    iget-object v0, p0, Lio/branch/referral/Branch$InitSessionBuilder;->〇080:Lio/branch/referral/Branch$BranchReferralInitListener;

    .line 97
    .line 98
    if-eqz v0, :cond_7

    .line 99
    .line 100
    new-instance v1, Lio/branch/referral/BranchError;

    .line 101
    .line 102
    const-string v3, ""

    .line 103
    .line 104
    const/16 v4, -0x77

    .line 105
    .line 106
    invoke-direct {v1, v3, v4}, Lio/branch/referral/BranchError;-><init>(Ljava/lang/String;I)V

    .line 107
    .line 108
    .line 109
    invoke-interface {v0, v2, v1}, Lio/branch/referral/Branch$BranchReferralInitListener;->〇080(Lorg/json/JSONObject;Lio/branch/referral/BranchError;)V

    .line 110
    .line 111
    .line 112
    :cond_7
    return-void

    .line 113
    :cond_8
    :goto_2
    invoke-static {v0}, Lio/branch/referral/Branch;->OO0o〇〇(Lio/branch/referral/Branch;)Z

    .line 114
    .line 115
    .line 116
    move-result v1

    .line 117
    if-eqz v1, :cond_a

    .line 118
    .line 119
    const/4 v1, 0x0

    .line 120
    invoke-static {v0, v1}, Lio/branch/referral/Branch;->Oooo8o0〇(Lio/branch/referral/Branch;Z)Z

    .line 121
    .line 122
    .line 123
    iget-object v1, p0, Lio/branch/referral/Branch$InitSessionBuilder;->〇080:Lio/branch/referral/Branch$BranchReferralInitListener;

    .line 124
    .line 125
    if-eqz v1, :cond_9

    .line 126
    .line 127
    invoke-virtual {v0}, Lio/branch/referral/Branch;->O〇O〇oO()Lorg/json/JSONObject;

    .line 128
    .line 129
    .line 130
    move-result-object v3

    .line 131
    invoke-interface {v1, v3, v2}, Lio/branch/referral/Branch$BranchReferralInitListener;->〇080(Lorg/json/JSONObject;Lio/branch/referral/BranchError;)V

    .line 132
    .line 133
    .line 134
    :cond_9
    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->InstantDeepLinkSession:Lio/branch/referral/Defines$Jsonkey;

    .line 135
    .line 136
    invoke-virtual {v1}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v1

    .line 140
    const-string/jumbo v3, "true"

    .line 141
    .line 142
    .line 143
    invoke-virtual {v0, v1, v3}, Lio/branch/referral/Branch;->〇O〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    invoke-virtual {v0}, Lio/branch/referral/Branch;->〇O888o0o()V

    .line 147
    .line 148
    .line 149
    iput-object v2, p0, Lio/branch/referral/Branch$InitSessionBuilder;->〇080:Lio/branch/referral/Branch$BranchReferralInitListener;

    .line 150
    .line 151
    :cond_a
    iget v1, p0, Lio/branch/referral/Branch$InitSessionBuilder;->〇o〇:I

    .line 152
    .line 153
    if-lez v1, :cond_b

    .line 154
    .line 155
    const/4 v1, 0x1

    .line 156
    invoke-static {v1}, Lio/branch/referral/Branch;->O8〇o(Z)V

    .line 157
    .line 158
    .line 159
    :cond_b
    iget-object v1, p0, Lio/branch/referral/Branch$InitSessionBuilder;->〇080:Lio/branch/referral/Branch$BranchReferralInitListener;

    .line 160
    .line 161
    iget-boolean v2, p0, Lio/branch/referral/Branch$InitSessionBuilder;->〇o00〇〇Oo:Z

    .line 162
    .line 163
    invoke-virtual {v0, v1, v2}, Lio/branch/referral/Branch;->O08000(Lio/branch/referral/Branch$BranchReferralInitListener;Z)Lio/branch/referral/ServerRequestInitSession;

    .line 164
    .line 165
    .line 166
    move-result-object v1

    .line 167
    iget v2, p0, Lio/branch/referral/Branch$InitSessionBuilder;->〇o〇:I

    .line 168
    .line 169
    invoke-static {v0, v1, v2}, Lio/branch/referral/Branch;->〇〇808〇(Lio/branch/referral/Branch;Lio/branch/referral/ServerRequestInitSession;I)V

    .line 170
    .line 171
    .line 172
    return-void
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method 〇o00〇〇Oo(Z)Lio/branch/referral/Branch$InitSessionBuilder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lio/branch/referral/Branch$InitSessionBuilder;->〇o00〇〇Oo:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public 〇o〇()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lio/branch/referral/Branch$InitSessionBuilder;->o〇0:Z

    .line 3
    .line 4
    invoke-virtual {p0}, Lio/branch/referral/Branch$InitSessionBuilder;->〇080()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

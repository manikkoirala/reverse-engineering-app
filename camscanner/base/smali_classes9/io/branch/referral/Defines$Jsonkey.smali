.class public final enum Lio/branch/referral/Defines$Jsonkey;
.super Ljava/lang/Enum;
.source "Defines.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/branch/referral/Defines$Jsonkey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AAID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum APILevel:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AdType:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AddressCity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AddressCountry:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AddressPostalCode:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AddressRegion:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AddressStreet:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AdvertisingIDs:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Affiliation:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Amount:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AndroidAppLinkURL:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AndroidDeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AndroidID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AndroidPushIdentifier:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AndroidPushNotificationKey:Lio/branch/referral/Defines$Jsonkey;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum AppIdentifier:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AppLinks:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum AppVersion:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum App_Store:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BackgroundColor:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BeginAfterID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchData:Lio/branch/referral/Defines$Jsonkey;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum BranchIdentity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchKey:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchLinkUsed:Lio/branch/referral/Defines$Jsonkey;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum BranchViewAction:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchViewData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchViewHtml:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchViewID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchViewNumOfUse:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum BranchViewUrl:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Branch_Instrumentation:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Branch_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Brand:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Bucket:Lio/branch/referral/Defines$Jsonkey;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum CPUType:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CalculationType:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CanonicalIdentifier:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CanonicalUrl:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CenterLogo:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ClickedReferrerTimeStamp:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Clicked_Branch_Link:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CodeColor:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CommerceData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Condition:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ConnectionType:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentActionView:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentAnalyticsMode:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentDesc:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentEvents:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentExpiryTime:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentImgUrl:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentItems:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentKeyWords:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentNavPath:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentPath:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentSchema:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentTitle:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ContentType:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Country:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Coupon:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CreationSource:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CreationTimestamp:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Currency:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CustomData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum CustomerEventAlias:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Data:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Debug:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum DeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum DefaultBucket:Lio/branch/referral/Defines$Jsonkey;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum Description:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum DeveloperIdentity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum DeviceBuildId:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum DeviceCarrier:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Direction:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum DisableAdNetworkCallouts:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Environment:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Event:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum EventData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Expiration:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum External_Intent_Extra:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum External_Intent_URI:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum FaceBookAppLinkChecked:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum FireAdId:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum FirstInstallTime:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum GoogleAdvertisingID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum GooglePlayInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum GoogleSearchInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Google_Play_Store:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum HardwareID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum HardwareIDType:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum HardwareIDTypeRandom:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum HardwareIDTypeVendor:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Huawei_App_Gallery:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Identity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ImageCaptions:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ImageFormat:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum InitialReferrer:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum InstallBeginTimeStamp:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum InstallMetadata:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Instant:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum InstantApp:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum InstantDeepLinkSession:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum IsFirstSession:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum IsFullAppConv:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum IsHardwareIDReal:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LATDAttributionWindow:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LATVal:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Language:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LastUpdateTime:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Last_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Latitude:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Length:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LimitedAdTracking:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Link:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LinkClickID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LinkIdentifier:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LocalIP:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Locale:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum LocallyIndexable:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Location:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Longitude:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Margin:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Metadata:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Model:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Name:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum NativeApp:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum OS:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum OSVersionAndroid:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum OpenAdvertisingID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum OriginalInstallTime:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Params:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum PartnerData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Path:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum PlayAutoInstalls:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum PluginName:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum PluginVersion:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Prefix:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum PreviousUpdateTime:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Price:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum PriceCurrency:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ProductBrand:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ProductCategory:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ProductName:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ProductVariant:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum PublicallyIndexable:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum QRCodeBranchKey:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum QRCodeData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum QRCodeResponseString:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum QRCodeSettings:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum QRCodeTag:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Quantity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Queue_Wait_Time:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum RandomizedBundleToken:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum RandomizedDeviceToken:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Rating:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum RatingAverage:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum RatingCount:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum RatingMax:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ReferralCode:Lio/branch/referral/Defines$Jsonkey;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum ReferralLink:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ReferrerExtraGclidParam:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ReferrerGclid:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ReferringBranchIdentity:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ReferringData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ReferringLink:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Revenue:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SDK:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SKU:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Samsung_Galaxy_Store:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ScreenDpi:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ScreenHeight:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ScreenWidth:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SdkVersion:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SearchQuery:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SessionID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ShareError:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum SharedLink:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Shipping:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Tax:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Total:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum TrackingDisabled:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum TransactionID:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Type:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum UIMode:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum URIScheme:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum UTMCampaign:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum UTMMedium:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum UnidentifiedDevice:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Unique:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Update:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum UserAgent:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum UserData:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum ViewList:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum WiFi:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Width:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum Xiaomi_Get_Apps:Lio/branch/referral/Defines$Jsonkey;

.field public static final enum limitFacebookTracking:Lio/branch/referral/Defines$Jsonkey;


# instance fields
.field private final key:Ljava/lang/String;


# direct methods
.method private static synthetic $values()[Lio/branch/referral/Defines$Jsonkey;
    .locals 3

    .line 1
    const/16 v0, 0xc8

    .line 2
    .line 3
    new-array v0, v0, [Lio/branch/referral/Defines$Jsonkey;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->RandomizedBundleToken:Lio/branch/referral/Defines$Jsonkey;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Identity:Lio/branch/referral/Defines$Jsonkey;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->RandomizedDeviceToken:Lio/branch/referral/Defines$Jsonkey;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->SessionID:Lio/branch/referral/Defines$Jsonkey;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->LinkClickID:Lio/branch/referral/Defines$Jsonkey;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->GoogleSearchInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->GooglePlayInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ClickedReferrerTimeStamp:Lio/branch/referral/Defines$Jsonkey;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ReferrerExtraGclidParam:Lio/branch/referral/Defines$Jsonkey;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    const/16 v1, 0x9

    .line 52
    .line 53
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ReferrerGclid:Lio/branch/referral/Defines$Jsonkey;

    .line 54
    .line 55
    aput-object v2, v0, v1

    .line 56
    .line 57
    const/16 v1, 0xa

    .line 58
    .line 59
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->InstallBeginTimeStamp:Lio/branch/referral/Defines$Jsonkey;

    .line 60
    .line 61
    aput-object v2, v0, v1

    .line 62
    .line 63
    const/16 v1, 0xb

    .line 64
    .line 65
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->FaceBookAppLinkChecked:Lio/branch/referral/Defines$Jsonkey;

    .line 66
    .line 67
    aput-object v2, v0, v1

    .line 68
    .line 69
    const/16 v1, 0xc

    .line 70
    .line 71
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BranchLinkUsed:Lio/branch/referral/Defines$Jsonkey;

    .line 72
    .line 73
    aput-object v2, v0, v1

    .line 74
    .line 75
    const/16 v1, 0xd

    .line 76
    .line 77
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ReferringBranchIdentity:Lio/branch/referral/Defines$Jsonkey;

    .line 78
    .line 79
    aput-object v2, v0, v1

    .line 80
    .line 81
    const/16 v1, 0xe

    .line 82
    .line 83
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BranchIdentity:Lio/branch/referral/Defines$Jsonkey;

    .line 84
    .line 85
    aput-object v2, v0, v1

    .line 86
    .line 87
    const/16 v1, 0xf

    .line 88
    .line 89
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BranchKey:Lio/branch/referral/Defines$Jsonkey;

    .line 90
    .line 91
    aput-object v2, v0, v1

    .line 92
    .line 93
    const/16 v1, 0x10

    .line 94
    .line 95
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BranchData:Lio/branch/referral/Defines$Jsonkey;

    .line 96
    .line 97
    aput-object v2, v0, v1

    .line 98
    .line 99
    const/16 v1, 0x11

    .line 100
    .line 101
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->PlayAutoInstalls:Lio/branch/referral/Defines$Jsonkey;

    .line 102
    .line 103
    aput-object v2, v0, v1

    .line 104
    .line 105
    const/16 v1, 0x12

    .line 106
    .line 107
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->UTMCampaign:Lio/branch/referral/Defines$Jsonkey;

    .line 108
    .line 109
    aput-object v2, v0, v1

    .line 110
    .line 111
    const/16 v1, 0x13

    .line 112
    .line 113
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->UTMMedium:Lio/branch/referral/Defines$Jsonkey;

    .line 114
    .line 115
    aput-object v2, v0, v1

    .line 116
    .line 117
    const/16 v1, 0x14

    .line 118
    .line 119
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->InitialReferrer:Lio/branch/referral/Defines$Jsonkey;

    .line 120
    .line 121
    aput-object v2, v0, v1

    .line 122
    .line 123
    const/16 v1, 0x15

    .line 124
    .line 125
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Bucket:Lio/branch/referral/Defines$Jsonkey;

    .line 126
    .line 127
    aput-object v2, v0, v1

    .line 128
    .line 129
    const/16 v1, 0x16

    .line 130
    .line 131
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->DefaultBucket:Lio/branch/referral/Defines$Jsonkey;

    .line 132
    .line 133
    aput-object v2, v0, v1

    .line 134
    .line 135
    const/16 v1, 0x17

    .line 136
    .line 137
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Amount:Lio/branch/referral/Defines$Jsonkey;

    .line 138
    .line 139
    aput-object v2, v0, v1

    .line 140
    .line 141
    const/16 v1, 0x18

    .line 142
    .line 143
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->CalculationType:Lio/branch/referral/Defines$Jsonkey;

    .line 144
    .line 145
    aput-object v2, v0, v1

    .line 146
    .line 147
    const/16 v1, 0x19

    .line 148
    .line 149
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Location:Lio/branch/referral/Defines$Jsonkey;

    .line 150
    .line 151
    aput-object v2, v0, v1

    .line 152
    .line 153
    const/16 v1, 0x1a

    .line 154
    .line 155
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Type:Lio/branch/referral/Defines$Jsonkey;

    .line 156
    .line 157
    aput-object v2, v0, v1

    .line 158
    .line 159
    const/16 v1, 0x1b

    .line 160
    .line 161
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->CreationSource:Lio/branch/referral/Defines$Jsonkey;

    .line 162
    .line 163
    aput-object v2, v0, v1

    .line 164
    .line 165
    const/16 v1, 0x1c

    .line 166
    .line 167
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Prefix:Lio/branch/referral/Defines$Jsonkey;

    .line 168
    .line 169
    aput-object v2, v0, v1

    .line 170
    .line 171
    const/16 v1, 0x1d

    .line 172
    .line 173
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Expiration:Lio/branch/referral/Defines$Jsonkey;

    .line 174
    .line 175
    aput-object v2, v0, v1

    .line 176
    .line 177
    const/16 v1, 0x1e

    .line 178
    .line 179
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Event:Lio/branch/referral/Defines$Jsonkey;

    .line 180
    .line 181
    aput-object v2, v0, v1

    .line 182
    .line 183
    const/16 v1, 0x1f

    .line 184
    .line 185
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Metadata:Lio/branch/referral/Defines$Jsonkey;

    .line 186
    .line 187
    aput-object v2, v0, v1

    .line 188
    .line 189
    const/16 v1, 0x20

    .line 190
    .line 191
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->CommerceData:Lio/branch/referral/Defines$Jsonkey;

    .line 192
    .line 193
    aput-object v2, v0, v1

    .line 194
    .line 195
    const/16 v1, 0x21

    .line 196
    .line 197
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ReferralCode:Lio/branch/referral/Defines$Jsonkey;

    .line 198
    .line 199
    aput-object v2, v0, v1

    .line 200
    .line 201
    const/16 v1, 0x22

    .line 202
    .line 203
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Total:Lio/branch/referral/Defines$Jsonkey;

    .line 204
    .line 205
    aput-object v2, v0, v1

    .line 206
    .line 207
    const/16 v1, 0x23

    .line 208
    .line 209
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Unique:Lio/branch/referral/Defines$Jsonkey;

    .line 210
    .line 211
    aput-object v2, v0, v1

    .line 212
    .line 213
    const/16 v1, 0x24

    .line 214
    .line 215
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Length:Lio/branch/referral/Defines$Jsonkey;

    .line 216
    .line 217
    aput-object v2, v0, v1

    .line 218
    .line 219
    const/16 v1, 0x25

    .line 220
    .line 221
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Direction:Lio/branch/referral/Defines$Jsonkey;

    .line 222
    .line 223
    aput-object v2, v0, v1

    .line 224
    .line 225
    const/16 v1, 0x26

    .line 226
    .line 227
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BeginAfterID:Lio/branch/referral/Defines$Jsonkey;

    .line 228
    .line 229
    aput-object v2, v0, v1

    .line 230
    .line 231
    const/16 v1, 0x27

    .line 232
    .line 233
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Link:Lio/branch/referral/Defines$Jsonkey;

    .line 234
    .line 235
    aput-object v2, v0, v1

    .line 236
    .line 237
    const/16 v1, 0x28

    .line 238
    .line 239
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ReferringData:Lio/branch/referral/Defines$Jsonkey;

    .line 240
    .line 241
    aput-object v2, v0, v1

    .line 242
    .line 243
    const/16 v1, 0x29

    .line 244
    .line 245
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ReferringLink:Lio/branch/referral/Defines$Jsonkey;

    .line 246
    .line 247
    aput-object v2, v0, v1

    .line 248
    .line 249
    const/16 v1, 0x2a

    .line 250
    .line 251
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->IsFullAppConv:Lio/branch/referral/Defines$Jsonkey;

    .line 252
    .line 253
    aput-object v2, v0, v1

    .line 254
    .line 255
    const/16 v1, 0x2b

    .line 256
    .line 257
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Data:Lio/branch/referral/Defines$Jsonkey;

    .line 258
    .line 259
    aput-object v2, v0, v1

    .line 260
    .line 261
    const/16 v1, 0x2c

    .line 262
    .line 263
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->OS:Lio/branch/referral/Defines$Jsonkey;

    .line 264
    .line 265
    aput-object v2, v0, v1

    .line 266
    .line 267
    const/16 v1, 0x2d

    .line 268
    .line 269
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->HardwareID:Lio/branch/referral/Defines$Jsonkey;

    .line 270
    .line 271
    aput-object v2, v0, v1

    .line 272
    .line 273
    const/16 v1, 0x2e

    .line 274
    .line 275
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AndroidID:Lio/branch/referral/Defines$Jsonkey;

    .line 276
    .line 277
    aput-object v2, v0, v1

    .line 278
    .line 279
    const/16 v1, 0x2f

    .line 280
    .line 281
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->UnidentifiedDevice:Lio/branch/referral/Defines$Jsonkey;

    .line 282
    .line 283
    aput-object v2, v0, v1

    .line 284
    .line 285
    const/16 v1, 0x30

    .line 286
    .line 287
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->HardwareIDType:Lio/branch/referral/Defines$Jsonkey;

    .line 288
    .line 289
    aput-object v2, v0, v1

    .line 290
    .line 291
    const/16 v1, 0x31

    .line 292
    .line 293
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->HardwareIDTypeVendor:Lio/branch/referral/Defines$Jsonkey;

    .line 294
    .line 295
    aput-object v2, v0, v1

    .line 296
    .line 297
    const/16 v1, 0x32

    .line 298
    .line 299
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->HardwareIDTypeRandom:Lio/branch/referral/Defines$Jsonkey;

    .line 300
    .line 301
    aput-object v2, v0, v1

    .line 302
    .line 303
    const/16 v1, 0x33

    .line 304
    .line 305
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->IsHardwareIDReal:Lio/branch/referral/Defines$Jsonkey;

    .line 306
    .line 307
    aput-object v2, v0, v1

    .line 308
    .line 309
    const/16 v1, 0x34

    .line 310
    .line 311
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AppVersion:Lio/branch/referral/Defines$Jsonkey;

    .line 312
    .line 313
    aput-object v2, v0, v1

    .line 314
    .line 315
    const/16 v1, 0x35

    .line 316
    .line 317
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->APILevel:Lio/branch/referral/Defines$Jsonkey;

    .line 318
    .line 319
    aput-object v2, v0, v1

    .line 320
    .line 321
    const/16 v1, 0x36

    .line 322
    .line 323
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Country:Lio/branch/referral/Defines$Jsonkey;

    .line 324
    .line 325
    aput-object v2, v0, v1

    .line 326
    .line 327
    const/16 v1, 0x37

    .line 328
    .line 329
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Language:Lio/branch/referral/Defines$Jsonkey;

    .line 330
    .line 331
    aput-object v2, v0, v1

    .line 332
    .line 333
    const/16 v1, 0x38

    .line 334
    .line 335
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Update:Lio/branch/referral/Defines$Jsonkey;

    .line 336
    .line 337
    aput-object v2, v0, v1

    .line 338
    .line 339
    const/16 v1, 0x39

    .line 340
    .line 341
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->OriginalInstallTime:Lio/branch/referral/Defines$Jsonkey;

    .line 342
    .line 343
    aput-object v2, v0, v1

    .line 344
    .line 345
    const/16 v1, 0x3a

    .line 346
    .line 347
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->FirstInstallTime:Lio/branch/referral/Defines$Jsonkey;

    .line 348
    .line 349
    aput-object v2, v0, v1

    .line 350
    .line 351
    const/16 v1, 0x3b

    .line 352
    .line 353
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->LastUpdateTime:Lio/branch/referral/Defines$Jsonkey;

    .line 354
    .line 355
    aput-object v2, v0, v1

    .line 356
    .line 357
    const/16 v1, 0x3c

    .line 358
    .line 359
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->PreviousUpdateTime:Lio/branch/referral/Defines$Jsonkey;

    .line 360
    .line 361
    aput-object v2, v0, v1

    .line 362
    .line 363
    const/16 v1, 0x3d

    .line 364
    .line 365
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->URIScheme:Lio/branch/referral/Defines$Jsonkey;

    .line 366
    .line 367
    aput-object v2, v0, v1

    .line 368
    .line 369
    const/16 v1, 0x3e

    .line 370
    .line 371
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AppLinks:Lio/branch/referral/Defines$Jsonkey;

    .line 372
    .line 373
    aput-object v2, v0, v1

    .line 374
    .line 375
    const/16 v1, 0x3f

    .line 376
    .line 377
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AppIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 378
    .line 379
    aput-object v2, v0, v1

    .line 380
    .line 381
    const/16 v1, 0x40

    .line 382
    .line 383
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->LinkIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 384
    .line 385
    aput-object v2, v0, v1

    .line 386
    .line 387
    const/16 v1, 0x41

    .line 388
    .line 389
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->GoogleAdvertisingID:Lio/branch/referral/Defines$Jsonkey;

    .line 390
    .line 391
    aput-object v2, v0, v1

    .line 392
    .line 393
    const/16 v1, 0x42

    .line 394
    .line 395
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AAID:Lio/branch/referral/Defines$Jsonkey;

    .line 396
    .line 397
    aput-object v2, v0, v1

    .line 398
    .line 399
    const/16 v1, 0x43

    .line 400
    .line 401
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->FireAdId:Lio/branch/referral/Defines$Jsonkey;

    .line 402
    .line 403
    aput-object v2, v0, v1

    .line 404
    .line 405
    const/16 v1, 0x44

    .line 406
    .line 407
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->OpenAdvertisingID:Lio/branch/referral/Defines$Jsonkey;

    .line 408
    .line 409
    aput-object v2, v0, v1

    .line 410
    .line 411
    const/16 v1, 0x45

    .line 412
    .line 413
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->LATVal:Lio/branch/referral/Defines$Jsonkey;

    .line 414
    .line 415
    aput-object v2, v0, v1

    .line 416
    .line 417
    const/16 v1, 0x46

    .line 418
    .line 419
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->LimitedAdTracking:Lio/branch/referral/Defines$Jsonkey;

    .line 420
    .line 421
    aput-object v2, v0, v1

    .line 422
    .line 423
    const/16 v1, 0x47

    .line 424
    .line 425
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->limitFacebookTracking:Lio/branch/referral/Defines$Jsonkey;

    .line 426
    .line 427
    aput-object v2, v0, v1

    .line 428
    .line 429
    const/16 v1, 0x48

    .line 430
    .line 431
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Debug:Lio/branch/referral/Defines$Jsonkey;

    .line 432
    .line 433
    aput-object v2, v0, v1

    .line 434
    .line 435
    const/16 v1, 0x49

    .line 436
    .line 437
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Brand:Lio/branch/referral/Defines$Jsonkey;

    .line 438
    .line 439
    aput-object v2, v0, v1

    .line 440
    .line 441
    const/16 v1, 0x4a

    .line 442
    .line 443
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Model:Lio/branch/referral/Defines$Jsonkey;

    .line 444
    .line 445
    aput-object v2, v0, v1

    .line 446
    .line 447
    const/16 v1, 0x4b

    .line 448
    .line 449
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ScreenDpi:Lio/branch/referral/Defines$Jsonkey;

    .line 450
    .line 451
    aput-object v2, v0, v1

    .line 452
    .line 453
    const/16 v1, 0x4c

    .line 454
    .line 455
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ScreenHeight:Lio/branch/referral/Defines$Jsonkey;

    .line 456
    .line 457
    aput-object v2, v0, v1

    .line 458
    .line 459
    const/16 v1, 0x4d

    .line 460
    .line 461
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ScreenWidth:Lio/branch/referral/Defines$Jsonkey;

    .line 462
    .line 463
    aput-object v2, v0, v1

    .line 464
    .line 465
    const/16 v1, 0x4e

    .line 466
    .line 467
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->WiFi:Lio/branch/referral/Defines$Jsonkey;

    .line 468
    .line 469
    aput-object v2, v0, v1

    .line 470
    .line 471
    const/16 v1, 0x4f

    .line 472
    .line 473
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->LocalIP:Lio/branch/referral/Defines$Jsonkey;

    .line 474
    .line 475
    aput-object v2, v0, v1

    .line 476
    .line 477
    const/16 v1, 0x50

    .line 478
    .line 479
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->UserData:Lio/branch/referral/Defines$Jsonkey;

    .line 480
    .line 481
    aput-object v2, v0, v1

    .line 482
    .line 483
    const/16 v1, 0x51

    .line 484
    .line 485
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AdvertisingIDs:Lio/branch/referral/Defines$Jsonkey;

    .line 486
    .line 487
    aput-object v2, v0, v1

    .line 488
    .line 489
    const/16 v1, 0x52

    .line 490
    .line 491
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->DeveloperIdentity:Lio/branch/referral/Defines$Jsonkey;

    .line 492
    .line 493
    aput-object v2, v0, v1

    .line 494
    .line 495
    const/16 v1, 0x53

    .line 496
    .line 497
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->UserAgent:Lio/branch/referral/Defines$Jsonkey;

    .line 498
    .line 499
    aput-object v2, v0, v1

    .line 500
    .line 501
    const/16 v1, 0x54

    .line 502
    .line 503
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->SDK:Lio/branch/referral/Defines$Jsonkey;

    .line 504
    .line 505
    aput-object v2, v0, v1

    .line 506
    .line 507
    const/16 v1, 0x55

    .line 508
    .line 509
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->SdkVersion:Lio/branch/referral/Defines$Jsonkey;

    .line 510
    .line 511
    aput-object v2, v0, v1

    .line 512
    .line 513
    const/16 v1, 0x56

    .line 514
    .line 515
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->UIMode:Lio/branch/referral/Defines$Jsonkey;

    .line 516
    .line 517
    aput-object v2, v0, v1

    .line 518
    .line 519
    const/16 v1, 0x57

    .line 520
    .line 521
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->InstallMetadata:Lio/branch/referral/Defines$Jsonkey;

    .line 522
    .line 523
    aput-object v2, v0, v1

    .line 524
    .line 525
    const/16 v1, 0x58

    .line 526
    .line 527
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->LATDAttributionWindow:Lio/branch/referral/Defines$Jsonkey;

    .line 528
    .line 529
    aput-object v2, v0, v1

    .line 530
    .line 531
    const/16 v1, 0x59

    .line 532
    .line 533
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Clicked_Branch_Link:Lio/branch/referral/Defines$Jsonkey;

    .line 534
    .line 535
    aput-object v2, v0, v1

    .line 536
    .line 537
    const/16 v1, 0x5a

    .line 538
    .line 539
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->IsFirstSession:Lio/branch/referral/Defines$Jsonkey;

    .line 540
    .line 541
    aput-object v2, v0, v1

    .line 542
    .line 543
    const/16 v1, 0x5b

    .line 544
    .line 545
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AndroidDeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

    .line 546
    .line 547
    aput-object v2, v0, v1

    .line 548
    .line 549
    const/16 v1, 0x5c

    .line 550
    .line 551
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->DeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

    .line 552
    .line 553
    aput-object v2, v0, v1

    .line 554
    .line 555
    const/16 v1, 0x5d

    .line 556
    .line 557
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AndroidAppLinkURL:Lio/branch/referral/Defines$Jsonkey;

    .line 558
    .line 559
    aput-object v2, v0, v1

    .line 560
    .line 561
    const/16 v1, 0x5e

    .line 562
    .line 563
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AndroidPushNotificationKey:Lio/branch/referral/Defines$Jsonkey;

    .line 564
    .line 565
    aput-object v2, v0, v1

    .line 566
    .line 567
    const/16 v1, 0x5f

    .line 568
    .line 569
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AndroidPushIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 570
    .line 571
    aput-object v2, v0, v1

    .line 572
    .line 573
    const/16 v1, 0x60

    .line 574
    .line 575
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->CanonicalIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 576
    .line 577
    aput-object v2, v0, v1

    .line 578
    .line 579
    const/16 v1, 0x61

    .line 580
    .line 581
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentTitle:Lio/branch/referral/Defines$Jsonkey;

    .line 582
    .line 583
    aput-object v2, v0, v1

    .line 584
    .line 585
    const/16 v1, 0x62

    .line 586
    .line 587
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentDesc:Lio/branch/referral/Defines$Jsonkey;

    .line 588
    .line 589
    aput-object v2, v0, v1

    .line 590
    .line 591
    const/16 v1, 0x63

    .line 592
    .line 593
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentImgUrl:Lio/branch/referral/Defines$Jsonkey;

    .line 594
    .line 595
    aput-object v2, v0, v1

    .line 596
    .line 597
    const/16 v1, 0x64

    .line 598
    .line 599
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->CanonicalUrl:Lio/branch/referral/Defines$Jsonkey;

    .line 600
    .line 601
    aput-object v2, v0, v1

    .line 602
    .line 603
    const/16 v1, 0x65

    .line 604
    .line 605
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentType:Lio/branch/referral/Defines$Jsonkey;

    .line 606
    .line 607
    aput-object v2, v0, v1

    .line 608
    .line 609
    const/16 v1, 0x66

    .line 610
    .line 611
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->PublicallyIndexable:Lio/branch/referral/Defines$Jsonkey;

    .line 612
    .line 613
    aput-object v2, v0, v1

    .line 614
    .line 615
    const/16 v1, 0x67

    .line 616
    .line 617
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->LocallyIndexable:Lio/branch/referral/Defines$Jsonkey;

    .line 618
    .line 619
    aput-object v2, v0, v1

    .line 620
    .line 621
    const/16 v1, 0x68

    .line 622
    .line 623
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentKeyWords:Lio/branch/referral/Defines$Jsonkey;

    .line 624
    .line 625
    aput-object v2, v0, v1

    .line 626
    .line 627
    const/16 v1, 0x69

    .line 628
    .line 629
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentExpiryTime:Lio/branch/referral/Defines$Jsonkey;

    .line 630
    .line 631
    aput-object v2, v0, v1

    .line 632
    .line 633
    const/16 v1, 0x6a

    .line 634
    .line 635
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Params:Lio/branch/referral/Defines$Jsonkey;

    .line 636
    .line 637
    aput-object v2, v0, v1

    .line 638
    .line 639
    const/16 v1, 0x6b

    .line 640
    .line 641
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->SharedLink:Lio/branch/referral/Defines$Jsonkey;

    .line 642
    .line 643
    aput-object v2, v0, v1

    .line 644
    .line 645
    const/16 v1, 0x6c

    .line 646
    .line 647
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ShareError:Lio/branch/referral/Defines$Jsonkey;

    .line 648
    .line 649
    aput-object v2, v0, v1

    .line 650
    .line 651
    const/16 v1, 0x6d

    .line 652
    .line 653
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->External_Intent_URI:Lio/branch/referral/Defines$Jsonkey;

    .line 654
    .line 655
    aput-object v2, v0, v1

    .line 656
    .line 657
    const/16 v1, 0x6e

    .line 658
    .line 659
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->External_Intent_Extra:Lio/branch/referral/Defines$Jsonkey;

    .line 660
    .line 661
    aput-object v2, v0, v1

    .line 662
    .line 663
    const/16 v1, 0x6f

    .line 664
    .line 665
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Last_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

    .line 666
    .line 667
    aput-object v2, v0, v1

    .line 668
    .line 669
    const/16 v1, 0x70

    .line 670
    .line 671
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Branch_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

    .line 672
    .line 673
    aput-object v2, v0, v1

    .line 674
    .line 675
    const/16 v1, 0x71

    .line 676
    .line 677
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Branch_Instrumentation:Lio/branch/referral/Defines$Jsonkey;

    .line 678
    .line 679
    aput-object v2, v0, v1

    .line 680
    .line 681
    const/16 v1, 0x72

    .line 682
    .line 683
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Queue_Wait_Time:Lio/branch/referral/Defines$Jsonkey;

    .line 684
    .line 685
    aput-object v2, v0, v1

    .line 686
    .line 687
    const/16 v1, 0x73

    .line 688
    .line 689
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->InstantDeepLinkSession:Lio/branch/referral/Defines$Jsonkey;

    .line 690
    .line 691
    aput-object v2, v0, v1

    .line 692
    .line 693
    const/16 v1, 0x74

    .line 694
    .line 695
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BranchViewData:Lio/branch/referral/Defines$Jsonkey;

    .line 696
    .line 697
    aput-object v2, v0, v1

    .line 698
    .line 699
    const/16 v1, 0x75

    .line 700
    .line 701
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BranchViewID:Lio/branch/referral/Defines$Jsonkey;

    .line 702
    .line 703
    aput-object v2, v0, v1

    .line 704
    .line 705
    const/16 v1, 0x76

    .line 706
    .line 707
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BranchViewAction:Lio/branch/referral/Defines$Jsonkey;

    .line 708
    .line 709
    aput-object v2, v0, v1

    .line 710
    .line 711
    const/16 v1, 0x77

    .line 712
    .line 713
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BranchViewNumOfUse:Lio/branch/referral/Defines$Jsonkey;

    .line 714
    .line 715
    aput-object v2, v0, v1

    .line 716
    .line 717
    const/16 v1, 0x78

    .line 718
    .line 719
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BranchViewUrl:Lio/branch/referral/Defines$Jsonkey;

    .line 720
    .line 721
    aput-object v2, v0, v1

    .line 722
    .line 723
    const/16 v1, 0x79

    .line 724
    .line 725
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BranchViewHtml:Lio/branch/referral/Defines$Jsonkey;

    .line 726
    .line 727
    aput-object v2, v0, v1

    .line 728
    .line 729
    const/16 v1, 0x7a

    .line 730
    .line 731
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Path:Lio/branch/referral/Defines$Jsonkey;

    .line 732
    .line 733
    aput-object v2, v0, v1

    .line 734
    .line 735
    const/16 v1, 0x7b

    .line 736
    .line 737
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ViewList:Lio/branch/referral/Defines$Jsonkey;

    .line 738
    .line 739
    aput-object v2, v0, v1

    .line 740
    .line 741
    const/16 v1, 0x7c

    .line 742
    .line 743
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentActionView:Lio/branch/referral/Defines$Jsonkey;

    .line 744
    .line 745
    aput-object v2, v0, v1

    .line 746
    .line 747
    const/16 v1, 0x7d

    .line 748
    .line 749
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentPath:Lio/branch/referral/Defines$Jsonkey;

    .line 750
    .line 751
    aput-object v2, v0, v1

    .line 752
    .line 753
    const/16 v1, 0x7e

    .line 754
    .line 755
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentNavPath:Lio/branch/referral/Defines$Jsonkey;

    .line 756
    .line 757
    aput-object v2, v0, v1

    .line 758
    .line 759
    const/16 v1, 0x7f

    .line 760
    .line 761
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ReferralLink:Lio/branch/referral/Defines$Jsonkey;

    .line 762
    .line 763
    aput-object v2, v0, v1

    .line 764
    .line 765
    const/16 v1, 0x80

    .line 766
    .line 767
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentData:Lio/branch/referral/Defines$Jsonkey;

    .line 768
    .line 769
    aput-object v2, v0, v1

    .line 770
    .line 771
    const/16 v1, 0x81

    .line 772
    .line 773
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentEvents:Lio/branch/referral/Defines$Jsonkey;

    .line 774
    .line 775
    aput-object v2, v0, v1

    .line 776
    .line 777
    const/16 v1, 0x82

    .line 778
    .line 779
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentAnalyticsMode:Lio/branch/referral/Defines$Jsonkey;

    .line 780
    .line 781
    aput-object v2, v0, v1

    .line 782
    .line 783
    const/16 v1, 0x83

    .line 784
    .line 785
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Environment:Lio/branch/referral/Defines$Jsonkey;

    .line 786
    .line 787
    aput-object v2, v0, v1

    .line 788
    .line 789
    const/16 v1, 0x84

    .line 790
    .line 791
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->InstantApp:Lio/branch/referral/Defines$Jsonkey;

    .line 792
    .line 793
    aput-object v2, v0, v1

    .line 794
    .line 795
    const/16 v1, 0x85

    .line 796
    .line 797
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->NativeApp:Lio/branch/referral/Defines$Jsonkey;

    .line 798
    .line 799
    aput-object v2, v0, v1

    .line 800
    .line 801
    const/16 v1, 0x86

    .line 802
    .line 803
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->CustomerEventAlias:Lio/branch/referral/Defines$Jsonkey;

    .line 804
    .line 805
    aput-object v2, v0, v1

    .line 806
    .line 807
    const/16 v1, 0x87

    .line 808
    .line 809
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->TransactionID:Lio/branch/referral/Defines$Jsonkey;

    .line 810
    .line 811
    aput-object v2, v0, v1

    .line 812
    .line 813
    const/16 v1, 0x88

    .line 814
    .line 815
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Currency:Lio/branch/referral/Defines$Jsonkey;

    .line 816
    .line 817
    aput-object v2, v0, v1

    .line 818
    .line 819
    const/16 v1, 0x89

    .line 820
    .line 821
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Revenue:Lio/branch/referral/Defines$Jsonkey;

    .line 822
    .line 823
    aput-object v2, v0, v1

    .line 824
    .line 825
    const/16 v1, 0x8a

    .line 826
    .line 827
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Shipping:Lio/branch/referral/Defines$Jsonkey;

    .line 828
    .line 829
    aput-object v2, v0, v1

    .line 830
    .line 831
    const/16 v1, 0x8b

    .line 832
    .line 833
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Tax:Lio/branch/referral/Defines$Jsonkey;

    .line 834
    .line 835
    aput-object v2, v0, v1

    .line 836
    .line 837
    const/16 v1, 0x8c

    .line 838
    .line 839
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Coupon:Lio/branch/referral/Defines$Jsonkey;

    .line 840
    .line 841
    aput-object v2, v0, v1

    .line 842
    .line 843
    const/16 v1, 0x8d

    .line 844
    .line 845
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Affiliation:Lio/branch/referral/Defines$Jsonkey;

    .line 846
    .line 847
    aput-object v2, v0, v1

    .line 848
    .line 849
    const/16 v1, 0x8e

    .line 850
    .line 851
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Description:Lio/branch/referral/Defines$Jsonkey;

    .line 852
    .line 853
    aput-object v2, v0, v1

    .line 854
    .line 855
    const/16 v1, 0x8f

    .line 856
    .line 857
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->SearchQuery:Lio/branch/referral/Defines$Jsonkey;

    .line 858
    .line 859
    aput-object v2, v0, v1

    .line 860
    .line 861
    const/16 v1, 0x90

    .line 862
    .line 863
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AdType:Lio/branch/referral/Defines$Jsonkey;

    .line 864
    .line 865
    aput-object v2, v0, v1

    .line 866
    .line 867
    const/16 v1, 0x91

    .line 868
    .line 869
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->CPUType:Lio/branch/referral/Defines$Jsonkey;

    .line 870
    .line 871
    aput-object v2, v0, v1

    .line 872
    .line 873
    const/16 v1, 0x92

    .line 874
    .line 875
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->DeviceBuildId:Lio/branch/referral/Defines$Jsonkey;

    .line 876
    .line 877
    aput-object v2, v0, v1

    .line 878
    .line 879
    const/16 v1, 0x93

    .line 880
    .line 881
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Locale:Lio/branch/referral/Defines$Jsonkey;

    .line 882
    .line 883
    aput-object v2, v0, v1

    .line 884
    .line 885
    const/16 v1, 0x94

    .line 886
    .line 887
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ConnectionType:Lio/branch/referral/Defines$Jsonkey;

    .line 888
    .line 889
    aput-object v2, v0, v1

    .line 890
    .line 891
    const/16 v1, 0x95

    .line 892
    .line 893
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->DeviceCarrier:Lio/branch/referral/Defines$Jsonkey;

    .line 894
    .line 895
    aput-object v2, v0, v1

    .line 896
    .line 897
    const/16 v1, 0x96

    .line 898
    .line 899
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->PluginName:Lio/branch/referral/Defines$Jsonkey;

    .line 900
    .line 901
    aput-object v2, v0, v1

    .line 902
    .line 903
    const/16 v1, 0x97

    .line 904
    .line 905
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->PluginVersion:Lio/branch/referral/Defines$Jsonkey;

    .line 906
    .line 907
    aput-object v2, v0, v1

    .line 908
    .line 909
    const/16 v1, 0x98

    .line 910
    .line 911
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->OSVersionAndroid:Lio/branch/referral/Defines$Jsonkey;

    .line 912
    .line 913
    aput-object v2, v0, v1

    .line 914
    .line 915
    const/16 v1, 0x99

    .line 916
    .line 917
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Name:Lio/branch/referral/Defines$Jsonkey;

    .line 918
    .line 919
    aput-object v2, v0, v1

    .line 920
    .line 921
    const/16 v1, 0x9a

    .line 922
    .line 923
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->CustomData:Lio/branch/referral/Defines$Jsonkey;

    .line 924
    .line 925
    aput-object v2, v0, v1

    .line 926
    .line 927
    const/16 v1, 0x9b

    .line 928
    .line 929
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->EventData:Lio/branch/referral/Defines$Jsonkey;

    .line 930
    .line 931
    aput-object v2, v0, v1

    .line 932
    .line 933
    const/16 v1, 0x9c

    .line 934
    .line 935
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentItems:Lio/branch/referral/Defines$Jsonkey;

    .line 936
    .line 937
    aput-object v2, v0, v1

    .line 938
    .line 939
    const/16 v1, 0x9d

    .line 940
    .line 941
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ContentSchema:Lio/branch/referral/Defines$Jsonkey;

    .line 942
    .line 943
    aput-object v2, v0, v1

    .line 944
    .line 945
    const/16 v1, 0x9e

    .line 946
    .line 947
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Price:Lio/branch/referral/Defines$Jsonkey;

    .line 948
    .line 949
    aput-object v2, v0, v1

    .line 950
    .line 951
    const/16 v1, 0x9f

    .line 952
    .line 953
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->PriceCurrency:Lio/branch/referral/Defines$Jsonkey;

    .line 954
    .line 955
    aput-object v2, v0, v1

    .line 956
    .line 957
    const/16 v1, 0xa0

    .line 958
    .line 959
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Quantity:Lio/branch/referral/Defines$Jsonkey;

    .line 960
    .line 961
    aput-object v2, v0, v1

    .line 962
    .line 963
    const/16 v1, 0xa1

    .line 964
    .line 965
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->SKU:Lio/branch/referral/Defines$Jsonkey;

    .line 966
    .line 967
    aput-object v2, v0, v1

    .line 968
    .line 969
    const/16 v1, 0xa2

    .line 970
    .line 971
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ProductName:Lio/branch/referral/Defines$Jsonkey;

    .line 972
    .line 973
    aput-object v2, v0, v1

    .line 974
    .line 975
    const/16 v1, 0xa3

    .line 976
    .line 977
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ProductBrand:Lio/branch/referral/Defines$Jsonkey;

    .line 978
    .line 979
    aput-object v2, v0, v1

    .line 980
    .line 981
    const/16 v1, 0xa4

    .line 982
    .line 983
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ProductCategory:Lio/branch/referral/Defines$Jsonkey;

    .line 984
    .line 985
    aput-object v2, v0, v1

    .line 986
    .line 987
    const/16 v1, 0xa5

    .line 988
    .line 989
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ProductVariant:Lio/branch/referral/Defines$Jsonkey;

    .line 990
    .line 991
    aput-object v2, v0, v1

    .line 992
    .line 993
    const/16 v1, 0xa6

    .line 994
    .line 995
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Rating:Lio/branch/referral/Defines$Jsonkey;

    .line 996
    .line 997
    aput-object v2, v0, v1

    .line 998
    .line 999
    const/16 v1, 0xa7

    .line 1000
    .line 1001
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->RatingAverage:Lio/branch/referral/Defines$Jsonkey;

    .line 1002
    .line 1003
    aput-object v2, v0, v1

    .line 1004
    .line 1005
    const/16 v1, 0xa8

    .line 1006
    .line 1007
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->RatingCount:Lio/branch/referral/Defines$Jsonkey;

    .line 1008
    .line 1009
    aput-object v2, v0, v1

    .line 1010
    .line 1011
    const/16 v1, 0xa9

    .line 1012
    .line 1013
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->RatingMax:Lio/branch/referral/Defines$Jsonkey;

    .line 1014
    .line 1015
    aput-object v2, v0, v1

    .line 1016
    .line 1017
    const/16 v1, 0xaa

    .line 1018
    .line 1019
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AddressStreet:Lio/branch/referral/Defines$Jsonkey;

    .line 1020
    .line 1021
    aput-object v2, v0, v1

    .line 1022
    .line 1023
    const/16 v1, 0xab

    .line 1024
    .line 1025
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AddressCity:Lio/branch/referral/Defines$Jsonkey;

    .line 1026
    .line 1027
    aput-object v2, v0, v1

    .line 1028
    .line 1029
    const/16 v1, 0xac

    .line 1030
    .line 1031
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AddressRegion:Lio/branch/referral/Defines$Jsonkey;

    .line 1032
    .line 1033
    aput-object v2, v0, v1

    .line 1034
    .line 1035
    const/16 v1, 0xad

    .line 1036
    .line 1037
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AddressCountry:Lio/branch/referral/Defines$Jsonkey;

    .line 1038
    .line 1039
    aput-object v2, v0, v1

    .line 1040
    .line 1041
    const/16 v1, 0xae

    .line 1042
    .line 1043
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->AddressPostalCode:Lio/branch/referral/Defines$Jsonkey;

    .line 1044
    .line 1045
    aput-object v2, v0, v1

    .line 1046
    .line 1047
    const/16 v1, 0xaf

    .line 1048
    .line 1049
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Latitude:Lio/branch/referral/Defines$Jsonkey;

    .line 1050
    .line 1051
    aput-object v2, v0, v1

    .line 1052
    .line 1053
    const/16 v1, 0xb0

    .line 1054
    .line 1055
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Longitude:Lio/branch/referral/Defines$Jsonkey;

    .line 1056
    .line 1057
    aput-object v2, v0, v1

    .line 1058
    .line 1059
    const/16 v1, 0xb1

    .line 1060
    .line 1061
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ImageCaptions:Lio/branch/referral/Defines$Jsonkey;

    .line 1062
    .line 1063
    aput-object v2, v0, v1

    .line 1064
    .line 1065
    const/16 v1, 0xb2

    .line 1066
    .line 1067
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Condition:Lio/branch/referral/Defines$Jsonkey;

    .line 1068
    .line 1069
    aput-object v2, v0, v1

    .line 1070
    .line 1071
    const/16 v1, 0xb3

    .line 1072
    .line 1073
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->CreationTimestamp:Lio/branch/referral/Defines$Jsonkey;

    .line 1074
    .line 1075
    aput-object v2, v0, v1

    .line 1076
    .line 1077
    const/16 v1, 0xb4

    .line 1078
    .line 1079
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->TrackingDisabled:Lio/branch/referral/Defines$Jsonkey;

    .line 1080
    .line 1081
    aput-object v2, v0, v1

    .line 1082
    .line 1083
    const/16 v1, 0xb5

    .line 1084
    .line 1085
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->DisableAdNetworkCallouts:Lio/branch/referral/Defines$Jsonkey;

    .line 1086
    .line 1087
    aput-object v2, v0, v1

    .line 1088
    .line 1089
    const/16 v1, 0xb6

    .line 1090
    .line 1091
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->PartnerData:Lio/branch/referral/Defines$Jsonkey;

    .line 1092
    .line 1093
    aput-object v2, v0, v1

    .line 1094
    .line 1095
    const/16 v1, 0xb7

    .line 1096
    .line 1097
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Instant:Lio/branch/referral/Defines$Jsonkey;

    .line 1098
    .line 1099
    aput-object v2, v0, v1

    .line 1100
    .line 1101
    const/16 v1, 0xb8

    .line 1102
    .line 1103
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->QRCodeTag:Lio/branch/referral/Defines$Jsonkey;

    .line 1104
    .line 1105
    aput-object v2, v0, v1

    .line 1106
    .line 1107
    const/16 v1, 0xb9

    .line 1108
    .line 1109
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->CodeColor:Lio/branch/referral/Defines$Jsonkey;

    .line 1110
    .line 1111
    aput-object v2, v0, v1

    .line 1112
    .line 1113
    const/16 v1, 0xba

    .line 1114
    .line 1115
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->BackgroundColor:Lio/branch/referral/Defines$Jsonkey;

    .line 1116
    .line 1117
    aput-object v2, v0, v1

    .line 1118
    .line 1119
    const/16 v1, 0xbb

    .line 1120
    .line 1121
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Width:Lio/branch/referral/Defines$Jsonkey;

    .line 1122
    .line 1123
    aput-object v2, v0, v1

    .line 1124
    .line 1125
    const/16 v1, 0xbc

    .line 1126
    .line 1127
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Margin:Lio/branch/referral/Defines$Jsonkey;

    .line 1128
    .line 1129
    aput-object v2, v0, v1

    .line 1130
    .line 1131
    const/16 v1, 0xbd

    .line 1132
    .line 1133
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->ImageFormat:Lio/branch/referral/Defines$Jsonkey;

    .line 1134
    .line 1135
    aput-object v2, v0, v1

    .line 1136
    .line 1137
    const/16 v1, 0xbe

    .line 1138
    .line 1139
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->CenterLogo:Lio/branch/referral/Defines$Jsonkey;

    .line 1140
    .line 1141
    aput-object v2, v0, v1

    .line 1142
    .line 1143
    const/16 v1, 0xbf

    .line 1144
    .line 1145
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->QRCodeSettings:Lio/branch/referral/Defines$Jsonkey;

    .line 1146
    .line 1147
    aput-object v2, v0, v1

    .line 1148
    .line 1149
    const/16 v1, 0xc0

    .line 1150
    .line 1151
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->QRCodeData:Lio/branch/referral/Defines$Jsonkey;

    .line 1152
    .line 1153
    aput-object v2, v0, v1

    .line 1154
    .line 1155
    const/16 v1, 0xc1

    .line 1156
    .line 1157
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->QRCodeBranchKey:Lio/branch/referral/Defines$Jsonkey;

    .line 1158
    .line 1159
    aput-object v2, v0, v1

    .line 1160
    .line 1161
    const/16 v1, 0xc2

    .line 1162
    .line 1163
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->QRCodeResponseString:Lio/branch/referral/Defines$Jsonkey;

    .line 1164
    .line 1165
    aput-object v2, v0, v1

    .line 1166
    .line 1167
    const/16 v1, 0xc3

    .line 1168
    .line 1169
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->App_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 1170
    .line 1171
    aput-object v2, v0, v1

    .line 1172
    .line 1173
    const/16 v1, 0xc4

    .line 1174
    .line 1175
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Google_Play_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 1176
    .line 1177
    aput-object v2, v0, v1

    .line 1178
    .line 1179
    const/16 v1, 0xc5

    .line 1180
    .line 1181
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Huawei_App_Gallery:Lio/branch/referral/Defines$Jsonkey;

    .line 1182
    .line 1183
    aput-object v2, v0, v1

    .line 1184
    .line 1185
    const/16 v1, 0xc6

    .line 1186
    .line 1187
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Samsung_Galaxy_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 1188
    .line 1189
    aput-object v2, v0, v1

    .line 1190
    .line 1191
    const/16 v1, 0xc7

    .line 1192
    .line 1193
    sget-object v2, Lio/branch/referral/Defines$Jsonkey;->Xiaomi_Get_Apps:Lio/branch/referral/Defines$Jsonkey;

    .line 1194
    .line 1195
    aput-object v2, v0, v1

    .line 1196
    .line 1197
    return-object v0
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
    .line 2087
    .line 2088
    .line 2089
    .line 2090
    .line 2091
    .line 2092
    .line 2093
    .line 2094
    .line 2095
    .line 2096
    .line 2097
    .line 2098
    .line 2099
    .line 2100
    .line 2101
    .line 2102
    .line 2103
    .line 2104
    .line 2105
    .line 2106
    .line 2107
    .line 2108
    .line 2109
    .line 2110
    .line 2111
    .line 2112
    .line 2113
    .line 2114
    .line 2115
    .line 2116
    .line 2117
    .line 2118
    .line 2119
    .line 2120
    .line 2121
    .line 2122
    .line 2123
    .line 2124
    .line 2125
    .line 2126
    .line 2127
    .line 2128
    .line 2129
    .line 2130
    .line 2131
    .line 2132
    .line 2133
    .line 2134
    .line 2135
    .line 2136
    .line 2137
    .line 2138
    .line 2139
    .line 2140
    .line 2141
    .line 2142
    .line 2143
    .line 2144
    .line 2145
    .line 2146
    .line 2147
    .line 2148
    .line 2149
    .line 2150
    .line 2151
    .line 2152
    .line 2153
    .line 2154
    .line 2155
    .line 2156
    .line 2157
    .line 2158
    .line 2159
    .line 2160
    .line 2161
    .line 2162
    .line 2163
    .line 2164
    .line 2165
    .line 2166
    .line 2167
    .line 2168
    .line 2169
    .line 2170
    .line 2171
    .line 2172
    .line 2173
    .line 2174
    .line 2175
    .line 2176
    .line 2177
    .line 2178
    .line 2179
    .line 2180
    .line 2181
    .line 2182
    .line 2183
    .line 2184
    .line 2185
    .line 2186
    .line 2187
    .line 2188
    .line 2189
    .line 2190
    .line 2191
    .line 2192
    .line 2193
    .line 2194
    .line 2195
    .line 2196
    .line 2197
    .line 2198
    .line 2199
    .line 2200
    .line 2201
    .line 2202
    .line 2203
    .line 2204
    .line 2205
    .line 2206
    .line 2207
    .line 2208
    .line 2209
    .line 2210
    .line 2211
    .line 2212
    .line 2213
    .line 2214
    .line 2215
    .line 2216
    .line 2217
    .line 2218
    .line 2219
    .line 2220
    .line 2221
    .line 2222
    .line 2223
    .line 2224
    .line 2225
    .line 2226
    .line 2227
    .line 2228
    .line 2229
    .line 2230
    .line 2231
    .line 2232
    .line 2233
    .line 2234
    .line 2235
    .line 2236
    .line 2237
    .line 2238
    .line 2239
    .line 2240
    .line 2241
    .line 2242
    .line 2243
    .line 2244
    .line 2245
    .line 2246
    .line 2247
    .line 2248
    .line 2249
    .line 2250
    .line 2251
    .line 2252
    .line 2253
    .line 2254
    .line 2255
    .line 2256
    .line 2257
    .line 2258
    .line 2259
    .line 2260
    .line 2261
    .line 2262
    .line 2263
    .line 2264
    .line 2265
    .line 2266
    .line 2267
    .line 2268
    .line 2269
.end method

.method static constructor <clinit>()V
    .locals 6

    .line 1
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v1, 0x0

    const-string v2, "randomized_bundle_token"

    const-string v3, "RandomizedBundleToken"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->RandomizedBundleToken:Lio/branch/referral/Defines$Jsonkey;

    .line 2
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v1, 0x1

    const-string v2, "identity"

    const-string v3, "Identity"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Identity:Lio/branch/referral/Defines$Jsonkey;

    .line 3
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v1, 0x2

    const-string v2, "randomized_device_token"

    const-string v3, "RandomizedDeviceToken"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->RandomizedDeviceToken:Lio/branch/referral/Defines$Jsonkey;

    .line 4
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v1, 0x3

    const-string v2, "session_id"

    const-string v3, "SessionID"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SessionID:Lio/branch/referral/Defines$Jsonkey;

    .line 5
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v1, 0x4

    const-string v2, "link_click_id"

    const-string v3, "LinkClickID"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LinkClickID:Lio/branch/referral/Defines$Jsonkey;

    .line 6
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v1, 0x5

    const-string v2, "google_search_install_referrer"

    const-string v3, "GoogleSearchInstallReferrer"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->GoogleSearchInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

    .line 7
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v1, 0x6

    const-string v2, "install_referrer_extras"

    const-string v3, "GooglePlayInstallReferrer"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->GooglePlayInstallReferrer:Lio/branch/referral/Defines$Jsonkey;

    .line 8
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/4 v1, 0x7

    const-string v2, "clicked_referrer_ts"

    const-string v3, "ClickedReferrerTimeStamp"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ClickedReferrerTimeStamp:Lio/branch/referral/Defines$Jsonkey;

    .line 9
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x8

    const-string v2, "gclid"

    const-string v3, "ReferrerExtraGclidParam"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferrerExtraGclidParam:Lio/branch/referral/Defines$Jsonkey;

    .line 10
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x9

    const-string v2, "referrer_gclid"

    const-string v3, "ReferrerGclid"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferrerGclid:Lio/branch/referral/Defines$Jsonkey;

    .line 11
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xa

    const-string v2, "install_begin_ts"

    const-string v3, "InstallBeginTimeStamp"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->InstallBeginTimeStamp:Lio/branch/referral/Defines$Jsonkey;

    .line 12
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xb

    const-string v2, "facebook_app_link_checked"

    const-string v3, "FaceBookAppLinkChecked"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->FaceBookAppLinkChecked:Lio/branch/referral/Defines$Jsonkey;

    .line 13
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xc

    const-string v2, "branch_used"

    const-string v3, "BranchLinkUsed"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchLinkUsed:Lio/branch/referral/Defines$Jsonkey;

    .line 14
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xd

    const-string v2, "referring_branch_identity"

    const-string v3, "ReferringBranchIdentity"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferringBranchIdentity:Lio/branch/referral/Defines$Jsonkey;

    .line 15
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xe

    const-string v2, "branch_identity"

    const-string v3, "BranchIdentity"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchIdentity:Lio/branch/referral/Defines$Jsonkey;

    .line 16
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v1, "BranchKey"

    const/16 v2, 0xf

    const-string v3, "branch_key"

    invoke-direct {v0, v1, v2, v3}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchKey:Lio/branch/referral/Defines$Jsonkey;

    .line 17
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x10

    const-string v2, "branch_data"

    const-string v4, "BranchData"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchData:Lio/branch/referral/Defines$Jsonkey;

    .line 18
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x11

    const-string v2, "play-auto-installs"

    const-string v4, "PlayAutoInstalls"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->PlayAutoInstalls:Lio/branch/referral/Defines$Jsonkey;

    .line 19
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x12

    const-string/jumbo v2, "utm_campaign"

    const-string v4, "UTMCampaign"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->UTMCampaign:Lio/branch/referral/Defines$Jsonkey;

    .line 20
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x13

    const-string/jumbo v2, "utm_medium"

    const-string v4, "UTMMedium"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->UTMMedium:Lio/branch/referral/Defines$Jsonkey;

    .line 21
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x14

    const-string v2, "initial_referrer"

    const-string v4, "InitialReferrer"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->InitialReferrer:Lio/branch/referral/Defines$Jsonkey;

    .line 22
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x15

    const-string v2, "bucket"

    const-string v4, "Bucket"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Bucket:Lio/branch/referral/Defines$Jsonkey;

    .line 23
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x16

    const-string v2, "default"

    const-string v4, "DefaultBucket"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->DefaultBucket:Lio/branch/referral/Defines$Jsonkey;

    .line 24
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x17

    const-string v2, "amount"

    const-string v4, "Amount"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Amount:Lio/branch/referral/Defines$Jsonkey;

    .line 25
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x18

    const-string v2, "calculation_type"

    const-string v4, "CalculationType"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CalculationType:Lio/branch/referral/Defines$Jsonkey;

    .line 26
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x19

    const-string v2, "location"

    const-string v4, "Location"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Location:Lio/branch/referral/Defines$Jsonkey;

    .line 27
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x1a

    const-string/jumbo v2, "type"

    const-string v4, "Type"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Type:Lio/branch/referral/Defines$Jsonkey;

    .line 28
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x1b

    const-string v2, "creation_source"

    const-string v4, "CreationSource"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CreationSource:Lio/branch/referral/Defines$Jsonkey;

    .line 29
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x1c

    const-string v2, "prefix"

    const-string v4, "Prefix"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Prefix:Lio/branch/referral/Defines$Jsonkey;

    .line 30
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x1d

    const-string v2, "expiration"

    const-string v4, "Expiration"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Expiration:Lio/branch/referral/Defines$Jsonkey;

    .line 31
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x1e

    const-string v2, "event"

    const-string v4, "Event"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Event:Lio/branch/referral/Defines$Jsonkey;

    .line 32
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x1f

    const-string v2, "metadata"

    const-string v4, "Metadata"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Metadata:Lio/branch/referral/Defines$Jsonkey;

    .line 33
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x20

    const-string v2, "commerce_data"

    const-string v4, "CommerceData"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CommerceData:Lio/branch/referral/Defines$Jsonkey;

    .line 34
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x21

    const-string v2, "referral_code"

    const-string v4, "ReferralCode"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferralCode:Lio/branch/referral/Defines$Jsonkey;

    .line 35
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x22

    const-string/jumbo v2, "total"

    const-string v4, "Total"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Total:Lio/branch/referral/Defines$Jsonkey;

    .line 36
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x23

    const-string/jumbo v2, "unique"

    const-string v4, "Unique"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Unique:Lio/branch/referral/Defines$Jsonkey;

    .line 37
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x24

    const-string v2, "length"

    const-string v4, "Length"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Length:Lio/branch/referral/Defines$Jsonkey;

    .line 38
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x25

    const-string v2, "direction"

    const-string v4, "Direction"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Direction:Lio/branch/referral/Defines$Jsonkey;

    .line 39
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x26

    const-string v2, "begin_after_id"

    const-string v4, "BeginAfterID"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BeginAfterID:Lio/branch/referral/Defines$Jsonkey;

    .line 40
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x27

    const-string v2, "link"

    const-string v4, "Link"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Link:Lio/branch/referral/Defines$Jsonkey;

    .line 41
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x28

    const-string v2, "referring_data"

    const-string v4, "ReferringData"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferringData:Lio/branch/referral/Defines$Jsonkey;

    .line 42
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x29

    const-string v2, "referring_link"

    const-string v4, "ReferringLink"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferringLink:Lio/branch/referral/Defines$Jsonkey;

    .line 43
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x2a

    const-string v2, "is_full_app_conversion"

    const-string v4, "IsFullAppConv"

    invoke-direct {v0, v4, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->IsFullAppConv:Lio/branch/referral/Defines$Jsonkey;

    .line 44
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v1, "Data"

    const/16 v2, 0x2b

    const-string v4, "data"

    invoke-direct {v0, v1, v2, v4}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Data:Lio/branch/referral/Defines$Jsonkey;

    .line 45
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x2c

    const-string v2, "os"

    const-string v5, "OS"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->OS:Lio/branch/referral/Defines$Jsonkey;

    .line 46
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x2d

    const-string v2, "hardware_id"

    const-string v5, "HardwareID"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->HardwareID:Lio/branch/referral/Defines$Jsonkey;

    .line 47
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x2e

    const-string v2, "android_id"

    const-string v5, "AndroidID"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AndroidID:Lio/branch/referral/Defines$Jsonkey;

    .line 48
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x2f

    const-string/jumbo v2, "unidentified_device"

    const-string v5, "UnidentifiedDevice"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->UnidentifiedDevice:Lio/branch/referral/Defines$Jsonkey;

    .line 49
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x30

    const-string v2, "hardware_id_type"

    const-string v5, "HardwareIDType"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->HardwareIDType:Lio/branch/referral/Defines$Jsonkey;

    .line 50
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x31

    const-string/jumbo v2, "vendor_id"

    const-string v5, "HardwareIDTypeVendor"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->HardwareIDTypeVendor:Lio/branch/referral/Defines$Jsonkey;

    .line 51
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x32

    const-string v2, "random"

    const-string v5, "HardwareIDTypeRandom"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->HardwareIDTypeRandom:Lio/branch/referral/Defines$Jsonkey;

    .line 52
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x33

    const-string v2, "is_hardware_id_real"

    const-string v5, "IsHardwareIDReal"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->IsHardwareIDReal:Lio/branch/referral/Defines$Jsonkey;

    .line 53
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x34

    const-string v2, "app_version"

    const-string v5, "AppVersion"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AppVersion:Lio/branch/referral/Defines$Jsonkey;

    .line 54
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x35

    const-string v2, "os_version"

    const-string v5, "APILevel"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->APILevel:Lio/branch/referral/Defines$Jsonkey;

    .line 55
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x36

    const-string v2, "country"

    const-string v5, "Country"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Country:Lio/branch/referral/Defines$Jsonkey;

    .line 56
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x37

    const-string v2, "language"

    const-string v5, "Language"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Language:Lio/branch/referral/Defines$Jsonkey;

    .line 57
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x38

    const-string/jumbo v2, "update"

    const-string v5, "Update"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Update:Lio/branch/referral/Defines$Jsonkey;

    .line 58
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x39

    const-string v2, "first_install_time"

    const-string v5, "OriginalInstallTime"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->OriginalInstallTime:Lio/branch/referral/Defines$Jsonkey;

    .line 59
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x3a

    const-string v2, "latest_install_time"

    const-string v5, "FirstInstallTime"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->FirstInstallTime:Lio/branch/referral/Defines$Jsonkey;

    .line 60
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x3b

    const-string v2, "latest_update_time"

    const-string v5, "LastUpdateTime"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LastUpdateTime:Lio/branch/referral/Defines$Jsonkey;

    .line 61
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x3c

    const-string v2, "previous_update_time"

    const-string v5, "PreviousUpdateTime"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->PreviousUpdateTime:Lio/branch/referral/Defines$Jsonkey;

    .line 62
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x3d

    const-string/jumbo v2, "uri_scheme"

    const-string v5, "URIScheme"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->URIScheme:Lio/branch/referral/Defines$Jsonkey;

    .line 63
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x3e

    const-string v2, "app_links"

    const-string v5, "AppLinks"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AppLinks:Lio/branch/referral/Defines$Jsonkey;

    .line 64
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x3f

    const-string v2, "app_identifier"

    const-string v5, "AppIdentifier"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AppIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 65
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x40

    const-string v2, "link_identifier"

    const-string v5, "LinkIdentifier"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LinkIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 66
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x41

    const-string v2, "google_advertising_id"

    const-string v5, "GoogleAdvertisingID"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->GoogleAdvertisingID:Lio/branch/referral/Defines$Jsonkey;

    .line 67
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x42

    const-string v2, "aaid"

    const-string v5, "AAID"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AAID:Lio/branch/referral/Defines$Jsonkey;

    .line 68
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x43

    const-string v2, "fire_ad_id"

    const-string v5, "FireAdId"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->FireAdId:Lio/branch/referral/Defines$Jsonkey;

    .line 69
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x44

    const-string v2, "oaid"

    const-string v5, "OpenAdvertisingID"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->OpenAdvertisingID:Lio/branch/referral/Defines$Jsonkey;

    .line 70
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x45

    const-string v2, "lat_val"

    const-string v5, "LATVal"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LATVal:Lio/branch/referral/Defines$Jsonkey;

    .line 71
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x46

    const-string v2, "limit_ad_tracking"

    const-string v5, "LimitedAdTracking"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LimitedAdTracking:Lio/branch/referral/Defines$Jsonkey;

    .line 72
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x47

    const-string v2, "limit_facebook_tracking"

    const-string v5, "limitFacebookTracking"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->limitFacebookTracking:Lio/branch/referral/Defines$Jsonkey;

    .line 73
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x48

    const-string v2, "debug"

    const-string v5, "Debug"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Debug:Lio/branch/referral/Defines$Jsonkey;

    .line 74
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x49

    const-string v2, "brand"

    const-string v5, "Brand"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Brand:Lio/branch/referral/Defines$Jsonkey;

    .line 75
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x4a

    const-string v2, "model"

    const-string v5, "Model"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Model:Lio/branch/referral/Defines$Jsonkey;

    .line 76
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x4b

    const-string v2, "screen_dpi"

    const-string v5, "ScreenDpi"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ScreenDpi:Lio/branch/referral/Defines$Jsonkey;

    .line 77
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x4c

    const-string v2, "screen_height"

    const-string v5, "ScreenHeight"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ScreenHeight:Lio/branch/referral/Defines$Jsonkey;

    .line 78
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x4d

    const-string v2, "screen_width"

    const-string v5, "ScreenWidth"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ScreenWidth:Lio/branch/referral/Defines$Jsonkey;

    .line 79
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x4e

    const-string/jumbo v2, "wifi"

    const-string v5, "WiFi"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->WiFi:Lio/branch/referral/Defines$Jsonkey;

    .line 80
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x4f

    const-string v2, "local_ip"

    const-string v5, "LocalIP"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LocalIP:Lio/branch/referral/Defines$Jsonkey;

    .line 81
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x50

    const-string/jumbo v2, "user_data"

    const-string v5, "UserData"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->UserData:Lio/branch/referral/Defines$Jsonkey;

    .line 82
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x51

    const-string v2, "advertising_ids"

    const-string v5, "AdvertisingIDs"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AdvertisingIDs:Lio/branch/referral/Defines$Jsonkey;

    .line 83
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x52

    const-string v2, "developer_identity"

    const-string v5, "DeveloperIdentity"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->DeveloperIdentity:Lio/branch/referral/Defines$Jsonkey;

    .line 84
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x53

    const-string/jumbo v2, "user_agent"

    const-string v5, "UserAgent"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->UserAgent:Lio/branch/referral/Defines$Jsonkey;

    .line 85
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x54

    const-string v2, "sdk"

    const-string v5, "SDK"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SDK:Lio/branch/referral/Defines$Jsonkey;

    .line 86
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x55

    const-string v2, "sdk_version"

    const-string v5, "SdkVersion"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SdkVersion:Lio/branch/referral/Defines$Jsonkey;

    .line 87
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x56

    const-string/jumbo v2, "ui_mode"

    const-string v5, "UIMode"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->UIMode:Lio/branch/referral/Defines$Jsonkey;

    .line 88
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x57

    const-string v2, "install_metadata"

    const-string v5, "InstallMetadata"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->InstallMetadata:Lio/branch/referral/Defines$Jsonkey;

    .line 89
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x58

    const-string v2, "attribution_window"

    const-string v5, "LATDAttributionWindow"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LATDAttributionWindow:Lio/branch/referral/Defines$Jsonkey;

    .line 90
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x59

    const-string v2, "+clicked_branch_link"

    const-string v5, "Clicked_Branch_Link"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Clicked_Branch_Link:Lio/branch/referral/Defines$Jsonkey;

    .line 91
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x5a

    const-string v2, "+is_first_session"

    const-string v5, "IsFirstSession"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->IsFirstSession:Lio/branch/referral/Defines$Jsonkey;

    .line 92
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x5b

    const-string v2, "$android_deeplink_path"

    const-string v5, "AndroidDeepLinkPath"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AndroidDeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

    .line 93
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x5c

    const-string v2, "$deeplink_path"

    const-string v5, "DeepLinkPath"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->DeepLinkPath:Lio/branch/referral/Defines$Jsonkey;

    .line 94
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x5d

    const-string v2, "android_app_link_url"

    const-string v5, "AndroidAppLinkURL"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AndroidAppLinkURL:Lio/branch/referral/Defines$Jsonkey;

    .line 95
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x5e

    const-string v2, "branch"

    const-string v5, "AndroidPushNotificationKey"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AndroidPushNotificationKey:Lio/branch/referral/Defines$Jsonkey;

    .line 96
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x5f

    const-string v2, "push_identifier"

    const-string v5, "AndroidPushIdentifier"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AndroidPushIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 97
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x60

    const-string v2, "$canonical_identifier"

    const-string v5, "CanonicalIdentifier"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CanonicalIdentifier:Lio/branch/referral/Defines$Jsonkey;

    .line 98
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x61

    const-string v2, "$og_title"

    const-string v5, "ContentTitle"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentTitle:Lio/branch/referral/Defines$Jsonkey;

    .line 99
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x62

    const-string v2, "$og_description"

    const-string v5, "ContentDesc"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentDesc:Lio/branch/referral/Defines$Jsonkey;

    .line 100
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x63

    const-string v2, "$og_image_url"

    const-string v5, "ContentImgUrl"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentImgUrl:Lio/branch/referral/Defines$Jsonkey;

    .line 101
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x64

    const-string v2, "$canonical_url"

    const-string v5, "CanonicalUrl"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CanonicalUrl:Lio/branch/referral/Defines$Jsonkey;

    .line 102
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x65

    const-string v2, "$content_type"

    const-string v5, "ContentType"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentType:Lio/branch/referral/Defines$Jsonkey;

    .line 103
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x66

    const-string v2, "$publicly_indexable"

    const-string v5, "PublicallyIndexable"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->PublicallyIndexable:Lio/branch/referral/Defines$Jsonkey;

    .line 104
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x67

    const-string v2, "$locally_indexable"

    const-string v5, "LocallyIndexable"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->LocallyIndexable:Lio/branch/referral/Defines$Jsonkey;

    .line 105
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x68

    const-string v2, "$keywords"

    const-string v5, "ContentKeyWords"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentKeyWords:Lio/branch/referral/Defines$Jsonkey;

    .line 106
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x69

    const-string v2, "$exp_date"

    const-string v5, "ContentExpiryTime"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentExpiryTime:Lio/branch/referral/Defines$Jsonkey;

    .line 107
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x6a

    const-string v2, "params"

    const-string v5, "Params"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Params:Lio/branch/referral/Defines$Jsonkey;

    .line 108
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x6b

    const-string v2, "$shared_link"

    const-string v5, "SharedLink"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SharedLink:Lio/branch/referral/Defines$Jsonkey;

    .line 109
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x6c

    const-string v2, "$share_error"

    const-string v5, "ShareError"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ShareError:Lio/branch/referral/Defines$Jsonkey;

    .line 110
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x6d

    const-string v2, "external_intent_uri"

    const-string v5, "External_Intent_URI"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->External_Intent_URI:Lio/branch/referral/Defines$Jsonkey;

    .line 111
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x6e

    const-string v2, "external_intent_extra"

    const-string v5, "External_Intent_Extra"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->External_Intent_Extra:Lio/branch/referral/Defines$Jsonkey;

    .line 112
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x6f

    const-string v2, "lrtt"

    const-string v5, "Last_Round_Trip_Time"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Last_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

    .line 113
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x70

    const-string v2, "brtt"

    const-string v5, "Branch_Round_Trip_Time"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Branch_Round_Trip_Time:Lio/branch/referral/Defines$Jsonkey;

    .line 114
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x71

    const-string v2, "instrumentation"

    const-string v5, "Branch_Instrumentation"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Branch_Instrumentation:Lio/branch/referral/Defines$Jsonkey;

    .line 115
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x72

    const-string v2, "qwt"

    const-string v5, "Queue_Wait_Time"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Queue_Wait_Time:Lio/branch/referral/Defines$Jsonkey;

    .line 116
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x73

    const-string v2, "instant_dl_session"

    const-string v5, "InstantDeepLinkSession"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->InstantDeepLinkSession:Lio/branch/referral/Defines$Jsonkey;

    .line 117
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x74

    const-string v2, "branch_view_data"

    const-string v5, "BranchViewData"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewData:Lio/branch/referral/Defines$Jsonkey;

    .line 118
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x75

    const-string v2, "id"

    const-string v5, "BranchViewID"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewID:Lio/branch/referral/Defines$Jsonkey;

    .line 119
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x76

    const-string v2, "action"

    const-string v5, "BranchViewAction"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewAction:Lio/branch/referral/Defines$Jsonkey;

    .line 120
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x77

    const-string v2, "number_of_use"

    const-string v5, "BranchViewNumOfUse"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewNumOfUse:Lio/branch/referral/Defines$Jsonkey;

    .line 121
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x78

    const-string/jumbo v2, "url"

    const-string v5, "BranchViewUrl"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewUrl:Lio/branch/referral/Defines$Jsonkey;

    .line 122
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x79

    const-string v2, "html"

    const-string v5, "BranchViewHtml"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BranchViewHtml:Lio/branch/referral/Defines$Jsonkey;

    .line 123
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x7a

    const-string v2, "path"

    const-string v5, "Path"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Path:Lio/branch/referral/Defines$Jsonkey;

    .line 124
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x7b

    const-string/jumbo v2, "view_list"

    const-string v5, "ViewList"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ViewList:Lio/branch/referral/Defines$Jsonkey;

    .line 125
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x7c

    const-string/jumbo v2, "view"

    const-string v5, "ContentActionView"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentActionView:Lio/branch/referral/Defines$Jsonkey;

    .line 126
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x7d

    const-string v2, "content_path"

    const-string v5, "ContentPath"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentPath:Lio/branch/referral/Defines$Jsonkey;

    .line 127
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x7e

    const-string v2, "content_nav_path"

    const-string v5, "ContentNavPath"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentNavPath:Lio/branch/referral/Defines$Jsonkey;

    .line 128
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x7f

    const-string v2, "referral_link"

    const-string v5, "ReferralLink"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ReferralLink:Lio/branch/referral/Defines$Jsonkey;

    .line 129
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x80

    const-string v2, "content_data"

    const-string v5, "ContentData"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentData:Lio/branch/referral/Defines$Jsonkey;

    .line 130
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x81

    const-string v2, "events"

    const-string v5, "ContentEvents"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentEvents:Lio/branch/referral/Defines$Jsonkey;

    .line 131
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x82

    const-string v2, "content_analytics_mode"

    const-string v5, "ContentAnalyticsMode"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentAnalyticsMode:Lio/branch/referral/Defines$Jsonkey;

    .line 132
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x83

    const-string v2, "environment"

    const-string v5, "Environment"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Environment:Lio/branch/referral/Defines$Jsonkey;

    .line 133
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x84

    const-string v2, "INSTANT_APP"

    const-string v5, "InstantApp"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->InstantApp:Lio/branch/referral/Defines$Jsonkey;

    .line 134
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x85

    const-string v2, "FULL_APP"

    const-string v5, "NativeApp"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->NativeApp:Lio/branch/referral/Defines$Jsonkey;

    .line 135
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x86

    const-string v2, "customer_event_alias"

    const-string v5, "CustomerEventAlias"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CustomerEventAlias:Lio/branch/referral/Defines$Jsonkey;

    .line 136
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x87

    const-string/jumbo v2, "transaction_id"

    const-string v5, "TransactionID"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->TransactionID:Lio/branch/referral/Defines$Jsonkey;

    .line 137
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x88

    const-string v2, "currency"

    const-string v5, "Currency"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Currency:Lio/branch/referral/Defines$Jsonkey;

    .line 138
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x89

    const-string v2, "revenue"

    const-string v5, "Revenue"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Revenue:Lio/branch/referral/Defines$Jsonkey;

    .line 139
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x8a

    const-string/jumbo v2, "shipping"

    const-string v5, "Shipping"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Shipping:Lio/branch/referral/Defines$Jsonkey;

    .line 140
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x8b

    const-string/jumbo v2, "tax"

    const-string v5, "Tax"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Tax:Lio/branch/referral/Defines$Jsonkey;

    .line 141
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x8c

    const-string v2, "coupon"

    const-string v5, "Coupon"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Coupon:Lio/branch/referral/Defines$Jsonkey;

    .line 142
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x8d

    const-string v2, "affiliation"

    const-string v5, "Affiliation"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Affiliation:Lio/branch/referral/Defines$Jsonkey;

    .line 143
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x8e

    const-string v2, "description"

    const-string v5, "Description"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Description:Lio/branch/referral/Defines$Jsonkey;

    .line 144
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x8f

    const-string v2, "search_query"

    const-string v5, "SearchQuery"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SearchQuery:Lio/branch/referral/Defines$Jsonkey;

    .line 145
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x90

    const-string v2, "ad_type"

    const-string v5, "AdType"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AdType:Lio/branch/referral/Defines$Jsonkey;

    .line 146
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x91

    const-string v2, "cpu_type"

    const-string v5, "CPUType"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CPUType:Lio/branch/referral/Defines$Jsonkey;

    .line 147
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x92

    const-string v2, "build"

    const-string v5, "DeviceBuildId"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->DeviceBuildId:Lio/branch/referral/Defines$Jsonkey;

    .line 148
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x93

    const-string v2, "locale"

    const-string v5, "Locale"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Locale:Lio/branch/referral/Defines$Jsonkey;

    .line 149
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x94

    const-string v2, "connection_type"

    const-string v5, "ConnectionType"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ConnectionType:Lio/branch/referral/Defines$Jsonkey;

    .line 150
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x95

    const-string v2, "device_carrier"

    const-string v5, "DeviceCarrier"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->DeviceCarrier:Lio/branch/referral/Defines$Jsonkey;

    .line 151
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x96

    const-string v2, "plugin_name"

    const-string v5, "PluginName"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->PluginName:Lio/branch/referral/Defines$Jsonkey;

    .line 152
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x97

    const-string v2, "plugin_version"

    const-string v5, "PluginVersion"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->PluginVersion:Lio/branch/referral/Defines$Jsonkey;

    .line 153
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x98

    const-string v2, "os_version_android"

    const-string v5, "OSVersionAndroid"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->OSVersionAndroid:Lio/branch/referral/Defines$Jsonkey;

    .line 154
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x99

    const-string v2, "name"

    const-string v5, "Name"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Name:Lio/branch/referral/Defines$Jsonkey;

    .line 155
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x9a

    const-string v2, "custom_data"

    const-string v5, "CustomData"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CustomData:Lio/branch/referral/Defines$Jsonkey;

    .line 156
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x9b

    const-string v2, "event_data"

    const-string v5, "EventData"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->EventData:Lio/branch/referral/Defines$Jsonkey;

    .line 157
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x9c

    const-string v2, "content_items"

    const-string v5, "ContentItems"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentItems:Lio/branch/referral/Defines$Jsonkey;

    .line 158
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x9d

    const-string v2, "$content_schema"

    const-string v5, "ContentSchema"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ContentSchema:Lio/branch/referral/Defines$Jsonkey;

    .line 159
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x9e

    const-string v2, "$price"

    const-string v5, "Price"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Price:Lio/branch/referral/Defines$Jsonkey;

    .line 160
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0x9f

    const-string v2, "$currency"

    const-string v5, "PriceCurrency"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->PriceCurrency:Lio/branch/referral/Defines$Jsonkey;

    .line 161
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xa0

    const-string v2, "$quantity"

    const-string v5, "Quantity"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Quantity:Lio/branch/referral/Defines$Jsonkey;

    .line 162
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xa1

    const-string v2, "$sku"

    const-string v5, "SKU"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->SKU:Lio/branch/referral/Defines$Jsonkey;

    .line 163
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xa2

    const-string v2, "$product_name"

    const-string v5, "ProductName"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ProductName:Lio/branch/referral/Defines$Jsonkey;

    .line 164
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xa3

    const-string v2, "$product_brand"

    const-string v5, "ProductBrand"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ProductBrand:Lio/branch/referral/Defines$Jsonkey;

    .line 165
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xa4

    const-string v2, "$product_category"

    const-string v5, "ProductCategory"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ProductCategory:Lio/branch/referral/Defines$Jsonkey;

    .line 166
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xa5

    const-string v2, "$product_variant"

    const-string v5, "ProductVariant"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ProductVariant:Lio/branch/referral/Defines$Jsonkey;

    .line 167
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xa6

    const-string v2, "$rating"

    const-string v5, "Rating"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Rating:Lio/branch/referral/Defines$Jsonkey;

    .line 168
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xa7

    const-string v2, "$rating_average"

    const-string v5, "RatingAverage"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->RatingAverage:Lio/branch/referral/Defines$Jsonkey;

    .line 169
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xa8

    const-string v2, "$rating_count"

    const-string v5, "RatingCount"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->RatingCount:Lio/branch/referral/Defines$Jsonkey;

    .line 170
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xa9

    const-string v2, "$rating_max"

    const-string v5, "RatingMax"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->RatingMax:Lio/branch/referral/Defines$Jsonkey;

    .line 171
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xaa

    const-string v2, "$address_street"

    const-string v5, "AddressStreet"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AddressStreet:Lio/branch/referral/Defines$Jsonkey;

    .line 172
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xab

    const-string v2, "$address_city"

    const-string v5, "AddressCity"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AddressCity:Lio/branch/referral/Defines$Jsonkey;

    .line 173
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xac

    const-string v2, "$address_region"

    const-string v5, "AddressRegion"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AddressRegion:Lio/branch/referral/Defines$Jsonkey;

    .line 174
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xad

    const-string v2, "$address_country"

    const-string v5, "AddressCountry"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AddressCountry:Lio/branch/referral/Defines$Jsonkey;

    .line 175
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xae

    const-string v2, "$address_postal_code"

    const-string v5, "AddressPostalCode"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->AddressPostalCode:Lio/branch/referral/Defines$Jsonkey;

    .line 176
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xaf

    const-string v2, "$latitude"

    const-string v5, "Latitude"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Latitude:Lio/branch/referral/Defines$Jsonkey;

    .line 177
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xb0

    const-string v2, "$longitude"

    const-string v5, "Longitude"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Longitude:Lio/branch/referral/Defines$Jsonkey;

    .line 178
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xb1

    const-string v2, "$image_captions"

    const-string v5, "ImageCaptions"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ImageCaptions:Lio/branch/referral/Defines$Jsonkey;

    .line 179
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xb2

    const-string v2, "$condition"

    const-string v5, "Condition"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Condition:Lio/branch/referral/Defines$Jsonkey;

    .line 180
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xb3

    const-string v2, "$creation_timestamp"

    const-string v5, "CreationTimestamp"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CreationTimestamp:Lio/branch/referral/Defines$Jsonkey;

    .line 181
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xb4

    const-string/jumbo v2, "tracking_disabled"

    const-string v5, "TrackingDisabled"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->TrackingDisabled:Lio/branch/referral/Defines$Jsonkey;

    .line 182
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xb5

    const-string v2, "disable_ad_network_callouts"

    const-string v5, "DisableAdNetworkCallouts"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->DisableAdNetworkCallouts:Lio/branch/referral/Defines$Jsonkey;

    .line 183
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xb6

    const-string v2, "partner_data"

    const-string v5, "PartnerData"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->PartnerData:Lio/branch/referral/Defines$Jsonkey;

    .line 184
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xb7

    const-string v2, "instant"

    const-string v5, "Instant"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Instant:Lio/branch/referral/Defines$Jsonkey;

    .line 185
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xb8

    const-string v2, "qr-code"

    const-string v5, "QRCodeTag"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->QRCodeTag:Lio/branch/referral/Defines$Jsonkey;

    .line 186
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xb9

    const-string v2, "code_color"

    const-string v5, "CodeColor"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CodeColor:Lio/branch/referral/Defines$Jsonkey;

    .line 187
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xba

    const-string v2, "background_color"

    const-string v5, "BackgroundColor"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->BackgroundColor:Lio/branch/referral/Defines$Jsonkey;

    .line 188
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xbb

    const-string/jumbo v2, "width"

    const-string v5, "Width"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Width:Lio/branch/referral/Defines$Jsonkey;

    .line 189
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xbc

    const-string v2, "margin"

    const-string v5, "Margin"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Margin:Lio/branch/referral/Defines$Jsonkey;

    .line 190
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xbd

    const-string v2, "image_format"

    const-string v5, "ImageFormat"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->ImageFormat:Lio/branch/referral/Defines$Jsonkey;

    .line 191
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xbe

    const-string v2, "center_logo_url"

    const-string v5, "CenterLogo"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->CenterLogo:Lio/branch/referral/Defines$Jsonkey;

    .line 192
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xbf

    const-string v2, "qr_code_settings"

    const-string v5, "QRCodeSettings"

    invoke-direct {v0, v5, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->QRCodeSettings:Lio/branch/referral/Defines$Jsonkey;

    .line 193
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v1, "QRCodeData"

    const/16 v2, 0xc0

    invoke-direct {v0, v1, v2, v4}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->QRCodeData:Lio/branch/referral/Defines$Jsonkey;

    .line 194
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const-string v1, "QRCodeBranchKey"

    const/16 v2, 0xc1

    invoke-direct {v0, v1, v2, v3}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->QRCodeBranchKey:Lio/branch/referral/Defines$Jsonkey;

    .line 195
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xc2

    const-string v2, "QRCodeString"

    const-string v3, "QRCodeResponseString"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->QRCodeResponseString:Lio/branch/referral/Defines$Jsonkey;

    .line 196
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xc3

    const-string v2, "app_store"

    const-string v3, "App_Store"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->App_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 197
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xc4

    const-string v2, "PlayStore"

    const-string v3, "Google_Play_Store"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Google_Play_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 198
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xc5

    const-string v2, "AppGallery"

    const-string v3, "Huawei_App_Gallery"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Huawei_App_Gallery:Lio/branch/referral/Defines$Jsonkey;

    .line 199
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xc6

    const-string v2, "GalaxyStore"

    const-string v3, "Samsung_Galaxy_Store"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Samsung_Galaxy_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 200
    new-instance v0, Lio/branch/referral/Defines$Jsonkey;

    const/16 v1, 0xc7

    const-string v2, "GetApps"

    const-string v3, "Xiaomi_Get_Apps"

    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/Defines$Jsonkey;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->Xiaomi_Get_Apps:Lio/branch/referral/Defines$Jsonkey;

    .line 201
    invoke-static {}, Lio/branch/referral/Defines$Jsonkey;->$values()[Lio/branch/referral/Defines$Jsonkey;

    move-result-object v0

    sput-object v0, Lio/branch/referral/Defines$Jsonkey;->$VALUES:[Lio/branch/referral/Defines$Jsonkey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lio/branch/referral/Defines$Jsonkey;->key:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public static valueOf(Ljava/lang/String;)Lio/branch/referral/Defines$Jsonkey;
    .locals 1

    .line 1
    const-class v0, Lio/branch/referral/Defines$Jsonkey;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lio/branch/referral/Defines$Jsonkey;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lio/branch/referral/Defines$Jsonkey;
    .locals 1

    .line 1
    sget-object v0, Lio/branch/referral/Defines$Jsonkey;->$VALUES:[Lio/branch/referral/Defines$Jsonkey;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lio/branch/referral/Defines$Jsonkey;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lio/branch/referral/Defines$Jsonkey;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lio/branch/referral/Defines$Jsonkey;->key:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lio/branch/referral/Defines$Jsonkey;->key:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

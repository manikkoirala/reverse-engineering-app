.class public final Lio/branch/referral/R$styleable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/branch/referral/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AppDataSearch:[I

.field public static final ColorStateListItem:[I

.field public static final ColorStateListItem_alpha:I = 0x3

.field public static final ColorStateListItem_android_alpha:I = 0x1

.field public static final ColorStateListItem_android_color:I = 0x0

.field public static final ColorStateListItem_android_lStar:I = 0x2

.field public static final ColorStateListItem_lStar:I = 0x4

.field public static final CoordinatorLayout:[I

.field public static final CoordinatorLayout_Layout:[I

.field public static final CoordinatorLayout_Layout_android_layout_gravity:I = 0x0

.field public static final CoordinatorLayout_Layout_layout_anchor:I = 0x1

.field public static final CoordinatorLayout_Layout_layout_anchorGravity:I = 0x2

.field public static final CoordinatorLayout_Layout_layout_behavior:I = 0x3

.field public static final CoordinatorLayout_Layout_layout_dodgeInsetEdges:I = 0x4

.field public static final CoordinatorLayout_Layout_layout_insetEdge:I = 0x5

.field public static final CoordinatorLayout_Layout_layout_keyline:I = 0x6

.field public static final CoordinatorLayout_keylines:I = 0x0

.field public static final CoordinatorLayout_statusBarBackground:I = 0x1

.field public static final Corpus:[I

.field public static final Corpus_contentProviderUri:I = 0x0

.field public static final Corpus_corpusId:I = 0x1

.field public static final Corpus_corpusVersion:I = 0x2

.field public static final Corpus_documentMaxAgeSecs:I = 0x3

.field public static final Corpus_perAccountTemplate:I = 0x4

.field public static final Corpus_schemaOrgType:I = 0x5

.field public static final Corpus_semanticallySearchable:I = 0x6

.field public static final Corpus_trimmable:I = 0x7

.field public static final FeatureParam:[I

.field public static final FeatureParam_paramName:I = 0x0

.field public static final FeatureParam_paramValue:I = 0x1

.field public static final FontFamily:[I

.field public static final FontFamilyFont:[I

.field public static final FontFamilyFont_android_font:I = 0x0

.field public static final FontFamilyFont_android_fontStyle:I = 0x2

.field public static final FontFamilyFont_android_fontVariationSettings:I = 0x4

.field public static final FontFamilyFont_android_fontWeight:I = 0x1

.field public static final FontFamilyFont_android_ttcIndex:I = 0x3

.field public static final FontFamilyFont_font:I = 0x5

.field public static final FontFamilyFont_fontStyle:I = 0x6

.field public static final FontFamilyFont_fontVariationSettings:I = 0x7

.field public static final FontFamilyFont_fontWeight:I = 0x8

.field public static final FontFamilyFont_ttcIndex:I = 0x9

.field public static final FontFamily_fontProviderAuthority:I = 0x0

.field public static final FontFamily_fontProviderCerts:I = 0x1

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x2

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x3

.field public static final FontFamily_fontProviderPackage:I = 0x4

.field public static final FontFamily_fontProviderQuery:I = 0x5

.field public static final FontFamily_fontProviderSystemFontFamily:I = 0x6

.field public static final GlobalSearch:[I

.field public static final GlobalSearchCorpus:[I

.field public static final GlobalSearchCorpus_allowShortcuts:I = 0x0

.field public static final GlobalSearchSection:[I

.field public static final GlobalSearchSection_sectionContent:I = 0x0

.field public static final GlobalSearchSection_sectionType:I = 0x1

.field public static final GlobalSearch_defaultIntentAction:I = 0x0

.field public static final GlobalSearch_defaultIntentActivity:I = 0x1

.field public static final GlobalSearch_defaultIntentData:I = 0x2

.field public static final GlobalSearch_searchEnabled:I = 0x3

.field public static final GlobalSearch_searchLabel:I = 0x4

.field public static final GlobalSearch_settingsDescription:I = 0x5

.field public static final GradientColor:[I

.field public static final GradientColorItem:[I

.field public static final GradientColorItem_android_color:I = 0x0

.field public static final GradientColorItem_android_offset:I = 0x1

.field public static final GradientColor_android_centerColor:I = 0x7

.field public static final GradientColor_android_centerX:I = 0x3

.field public static final GradientColor_android_centerY:I = 0x4

.field public static final GradientColor_android_endColor:I = 0x1

.field public static final GradientColor_android_endX:I = 0xa

.field public static final GradientColor_android_endY:I = 0xb

.field public static final GradientColor_android_gradientRadius:I = 0x5

.field public static final GradientColor_android_startColor:I = 0x0

.field public static final GradientColor_android_startX:I = 0x8

.field public static final GradientColor_android_startY:I = 0x9

.field public static final GradientColor_android_tileMode:I = 0x6

.field public static final GradientColor_android_type:I = 0x2

.field public static final IMECorpus:[I

.field public static final IMECorpus_inputEnabled:I = 0x0

.field public static final IMECorpus_sourceClass:I = 0x1

.field public static final IMECorpus_toAddressesSection:I = 0x2

.field public static final IMECorpus_userInputSection:I = 0x3

.field public static final IMECorpus_userInputTag:I = 0x4

.field public static final IMECorpus_userInputValue:I = 0x5

.field public static final LoadingImageView:[I

.field public static final LoadingImageView_circleCrop:I = 0x0

.field public static final LoadingImageView_imageAspectRatio:I = 0x1

.field public static final LoadingImageView_imageAspectRatioAdjust:I = 0x2

.field public static final Section:[I

.field public static final SectionFeature:[I

.field public static final SectionFeature_featureType:I = 0x0

.field public static final Section_indexPrefixes:I = 0x0

.field public static final Section_noIndex:I = 0x1

.field public static final Section_schemaOrgProperty:I = 0x2

.field public static final Section_sectionFormat:I = 0x3

.field public static final Section_sectionId:I = 0x4

.field public static final Section_sectionWeight:I = 0x5

.field public static final Section_subsectionSeparator:I = 0x6

.field public static final SignInButton:[I

.field public static final SignInButton_buttonSize:I = 0x0

.field public static final SignInButton_colorScheme:I = 0x1

.field public static final SignInButton_scopeUris:I = 0x2


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    new-array v1, v0, [I

    .line 3
    .line 4
    sput-object v1, Lio/branch/referral/R$styleable;->AppDataSearch:[I

    .line 5
    .line 6
    const/4 v1, 0x5

    .line 7
    new-array v1, v1, [I

    .line 8
    .line 9
    fill-array-data v1, :array_0

    .line 10
    .line 11
    .line 12
    sput-object v1, Lio/branch/referral/R$styleable;->ColorStateListItem:[I

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    new-array v2, v1, [I

    .line 16
    .line 17
    fill-array-data v2, :array_1

    .line 18
    .line 19
    .line 20
    sput-object v2, Lio/branch/referral/R$styleable;->CoordinatorLayout:[I

    .line 21
    .line 22
    const/4 v2, 0x7

    .line 23
    new-array v3, v2, [I

    .line 24
    .line 25
    fill-array-data v3, :array_2

    .line 26
    .line 27
    .line 28
    sput-object v3, Lio/branch/referral/R$styleable;->CoordinatorLayout_Layout:[I

    .line 29
    .line 30
    const/16 v3, 0x8

    .line 31
    .line 32
    new-array v3, v3, [I

    .line 33
    .line 34
    fill-array-data v3, :array_3

    .line 35
    .line 36
    .line 37
    sput-object v3, Lio/branch/referral/R$styleable;->Corpus:[I

    .line 38
    .line 39
    new-array v3, v1, [I

    .line 40
    .line 41
    fill-array-data v3, :array_4

    .line 42
    .line 43
    .line 44
    sput-object v3, Lio/branch/referral/R$styleable;->FeatureParam:[I

    .line 45
    .line 46
    new-array v3, v2, [I

    .line 47
    .line 48
    fill-array-data v3, :array_5

    .line 49
    .line 50
    .line 51
    sput-object v3, Lio/branch/referral/R$styleable;->FontFamily:[I

    .line 52
    .line 53
    const/16 v3, 0xa

    .line 54
    .line 55
    new-array v3, v3, [I

    .line 56
    .line 57
    fill-array-data v3, :array_6

    .line 58
    .line 59
    .line 60
    sput-object v3, Lio/branch/referral/R$styleable;->FontFamilyFont:[I

    .line 61
    .line 62
    const/4 v3, 0x6

    .line 63
    new-array v4, v3, [I

    .line 64
    .line 65
    fill-array-data v4, :array_7

    .line 66
    .line 67
    .line 68
    sput-object v4, Lio/branch/referral/R$styleable;->GlobalSearch:[I

    .line 69
    .line 70
    const/4 v4, 0x1

    .line 71
    new-array v5, v4, [I

    .line 72
    .line 73
    const v6, 0x7f040059

    .line 74
    .line 75
    .line 76
    aput v6, v5, v0

    .line 77
    .line 78
    sput-object v5, Lio/branch/referral/R$styleable;->GlobalSearchCorpus:[I

    .line 79
    .line 80
    new-array v5, v1, [I

    .line 81
    .line 82
    fill-array-data v5, :array_8

    .line 83
    .line 84
    .line 85
    sput-object v5, Lio/branch/referral/R$styleable;->GlobalSearchSection:[I

    .line 86
    .line 87
    const/16 v5, 0xc

    .line 88
    .line 89
    new-array v5, v5, [I

    .line 90
    .line 91
    fill-array-data v5, :array_9

    .line 92
    .line 93
    .line 94
    sput-object v5, Lio/branch/referral/R$styleable;->GradientColor:[I

    .line 95
    .line 96
    new-array v1, v1, [I

    .line 97
    .line 98
    fill-array-data v1, :array_a

    .line 99
    .line 100
    .line 101
    sput-object v1, Lio/branch/referral/R$styleable;->GradientColorItem:[I

    .line 102
    .line 103
    new-array v1, v3, [I

    .line 104
    .line 105
    fill-array-data v1, :array_b

    .line 106
    .line 107
    .line 108
    sput-object v1, Lio/branch/referral/R$styleable;->IMECorpus:[I

    .line 109
    .line 110
    const/4 v1, 0x3

    .line 111
    new-array v3, v1, [I

    .line 112
    .line 113
    fill-array-data v3, :array_c

    .line 114
    .line 115
    .line 116
    sput-object v3, Lio/branch/referral/R$styleable;->LoadingImageView:[I

    .line 117
    .line 118
    new-array v2, v2, [I

    .line 119
    .line 120
    fill-array-data v2, :array_d

    .line 121
    .line 122
    .line 123
    sput-object v2, Lio/branch/referral/R$styleable;->Section:[I

    .line 124
    .line 125
    new-array v2, v4, [I

    .line 126
    .line 127
    const v3, 0x7f040297

    .line 128
    .line 129
    .line 130
    aput v3, v2, v0

    .line 131
    .line 132
    sput-object v2, Lio/branch/referral/R$styleable;->SectionFeature:[I

    .line 133
    .line 134
    new-array v0, v1, [I

    .line 135
    .line 136
    fill-array-data v0, :array_e

    .line 137
    .line 138
    .line 139
    sput-object v0, Lio/branch/referral/R$styleable;->SignInButton:[I

    .line 140
    .line 141
    return-void

    .line 142
    nop

    .line 143
    :array_0
    .array-data 4
        0x10101a5
        0x101031f
        0x1010647
        0x7f04005b
        0x7f04036e
    .end array-data

    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    :array_1
    .array-data 4
        0x7f04036d
        0x7f04059a
    .end array-data

    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    :array_2
    .array-data 4
        0x10100b3
        0x7f040379
        0x7f04037a
        0x7f04037b
        0x7f0403ac
        0x7f0403b9
        0x7f0403ba
    .end array-data

    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    :array_3
    .array-data 4
        0x7f0401af
        0x7f0401c0
        0x7f0401c1
        0x7f040208
        0x7f0404c9
        0x7f04051c
        0x7f040536
        0x7f040696
    .end array-data

    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    :array_4
    .array-data 4
        0x7f0404c0
        0x7f0404c1
    .end array-data

    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    :array_5
    .array-data 4
        0x7f0402c9
        0x7f0402ca
        0x7f0402cb
        0x7f0402cc
        0x7f0402cd
        0x7f0402ce
        0x7f0402cf
    .end array-data

    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    :array_6
    .array-data 4
        0x1010532
        0x1010533
        0x101053f
        0x101056f
        0x1010570
        0x7f0402c7
        0x7f0402d0
        0x7f0402d1
        0x7f0402d2
        0x7f040697
    .end array-data

    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    :array_7
    .array-data 4
        0x7f0401e6
        0x7f0401e7
        0x7f0401e8
        0x7f040522
        0x7f040525
        0x7f040538
    .end array-data

    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    :array_8
    .array-data 4
        0x7f040528
        0x7f04052b
    .end array-data

    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    :array_9
    .array-data 4
        0x101019d
        0x101019e
        0x10101a1
        0x10101a2
        0x10101a3
        0x10101a4
        0x1010201
        0x101020b
        0x1010510
        0x1010511
        0x1010512
        0x1010513
    .end array-data

    :array_a
    .array-data 4
        0x10101a5
        0x1010514
    .end array-data

    :array_b
    .array-data 4
        0x7f040328
        0x7f040579
        0x7f040670
        0x7f0406a4
        0x7f0406a5
        0x7f0406a6
    .end array-data

    :array_c
    .array-data 4
        0x7f040123
        0x7f04030a
        0x7f04030b
    .end array-data

    :array_d
    .array-data 4
        0x7f04031e
        0x7f0404a1
        0x7f04051b
        0x7f040529
        0x7f04052a
        0x7f04052c
        0x7f0405a8
    .end array-data

    :array_e
    .array-data 4
        0x7f0400e5
        0x7f040173
        0x7f04051d
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

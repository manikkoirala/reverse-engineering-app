.class public Lio/branch/referral/StoreReferrerUtils;
.super Ljava/lang/Object;
.source "StoreReferrerUtils.java"


# direct methods
.method public static 〇080()Ljava/lang/String;
    .locals 7

    .line 1
    const-wide/16 v0, 0x0

    .line 2
    .line 3
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Lio/branch/referral/StoreReferrerGooglePlayStore;->o〇0:Ljava/lang/Long;

    .line 8
    .line 9
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 10
    .line 11
    .line 12
    move-result-wide v1

    .line 13
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 14
    .line 15
    .line 16
    move-result-wide v3

    .line 17
    cmp-long v5, v1, v3

    .line 18
    .line 19
    if-lez v5, :cond_0

    .line 20
    .line 21
    sget-object v0, Lio/branch/referral/StoreReferrerGooglePlayStore;->o〇0:Ljava/lang/Long;

    .line 22
    .line 23
    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Google_Play_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 24
    .line 25
    invoke-virtual {v1}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const-string v1, ""

    .line 31
    .line 32
    :goto_0
    sget-wide v2, Lio/branch/referral/StoreReferrerHuaweiAppGallery;->o〇0:J

    .line 33
    .line 34
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 35
    .line 36
    .line 37
    move-result-wide v4

    .line 38
    cmp-long v6, v2, v4

    .line 39
    .line 40
    if-lez v6, :cond_1

    .line 41
    .line 42
    sget-wide v0, Lio/branch/referral/StoreReferrerHuaweiAppGallery;->o〇0:J

    .line 43
    .line 44
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Huawei_App_Gallery:Lio/branch/referral/Defines$Jsonkey;

    .line 49
    .line 50
    invoke-virtual {v1}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    :cond_1
    sget-object v2, Lio/branch/referral/StoreReferrerSamsungGalaxyStore;->o〇0:Ljava/lang/Long;

    .line 55
    .line 56
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 57
    .line 58
    .line 59
    move-result-wide v2

    .line 60
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 61
    .line 62
    .line 63
    move-result-wide v4

    .line 64
    cmp-long v6, v2, v4

    .line 65
    .line 66
    if-lez v6, :cond_2

    .line 67
    .line 68
    sget-object v0, Lio/branch/referral/StoreReferrerSamsungGalaxyStore;->o〇0:Ljava/lang/Long;

    .line 69
    .line 70
    sget-object v1, Lio/branch/referral/Defines$Jsonkey;->Samsung_Galaxy_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 71
    .line 72
    invoke-virtual {v1}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    :cond_2
    sget-object v2, Lio/branch/referral/StoreReferrerXiaomiGetApps;->o〇0:Ljava/lang/Long;

    .line 77
    .line 78
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 79
    .line 80
    .line 81
    move-result-wide v2

    .line 82
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 83
    .line 84
    .line 85
    move-result-wide v4

    .line 86
    cmp-long v0, v2, v4

    .line 87
    .line 88
    if-lez v0, :cond_3

    .line 89
    .line 90
    sget-object v0, Lio/branch/referral/Defines$Jsonkey;->Xiaomi_Get_Apps:Lio/branch/referral/Defines$Jsonkey;

    .line 91
    .line 92
    invoke-virtual {v0}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    if-eqz v0, :cond_7

    .line 101
    .line 102
    sget-object v0, Lio/branch/referral/StoreReferrerGooglePlayStore;->〇〇888:Ljava/lang/String;

    .line 103
    .line 104
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    if-nez v0, :cond_4

    .line 109
    .line 110
    sget-object v0, Lio/branch/referral/Defines$Jsonkey;->Google_Play_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 111
    .line 112
    invoke-virtual {v0}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v1

    .line 116
    :cond_4
    sget-object v0, Lio/branch/referral/StoreReferrerHuaweiAppGallery;->〇〇888:Ljava/lang/String;

    .line 117
    .line 118
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    if-nez v0, :cond_5

    .line 123
    .line 124
    sget-object v0, Lio/branch/referral/Defines$Jsonkey;->Huawei_App_Gallery:Lio/branch/referral/Defines$Jsonkey;

    .line 125
    .line 126
    invoke-virtual {v0}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v1

    .line 130
    :cond_5
    sget-object v0, Lio/branch/referral/StoreReferrerSamsungGalaxyStore;->〇〇888:Ljava/lang/String;

    .line 131
    .line 132
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    if-nez v0, :cond_6

    .line 137
    .line 138
    sget-object v0, Lio/branch/referral/Defines$Jsonkey;->Samsung_Galaxy_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 139
    .line 140
    invoke-virtual {v0}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    move-object v1, v0

    .line 145
    :cond_6
    sget-object v0, Lio/branch/referral/StoreReferrerXiaomiGetApps;->〇〇888:Ljava/lang/String;

    .line 146
    .line 147
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 148
    .line 149
    .line 150
    move-result v0

    .line 151
    if-nez v0, :cond_7

    .line 152
    .line 153
    sget-object v0, Lio/branch/referral/Defines$Jsonkey;->Xiaomi_Get_Apps:Lio/branch/referral/Defines$Jsonkey;

    .line 154
    .line 155
    invoke-virtual {v0}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v1

    .line 159
    :cond_7
    return-object v1
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method public static 〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    .line 1
    sget-object v0, Lio/branch/referral/Defines$Jsonkey;->Google_Play_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 2
    .line 3
    invoke-virtual {v0}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    sget-object v2, Lio/branch/referral/StoreReferrerGooglePlayStore;->〇〇888:Ljava/lang/String;

    .line 14
    .line 15
    sget-object v0, Lio/branch/referral/StoreReferrerGooglePlayStore;->Oo08:Ljava/lang/Long;

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 18
    .line 19
    .line 20
    move-result-wide v3

    .line 21
    sget-object v0, Lio/branch/referral/StoreReferrerGooglePlayStore;->o〇0:Ljava/lang/Long;

    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 24
    .line 25
    .line 26
    move-result-wide v5

    .line 27
    move-object v1, p0

    .line 28
    move-object v7, p1

    .line 29
    invoke-static/range {v1 .. v7}, Lio/branch/referral/AppStoreReferrer;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;JJLjava/lang/String;)V

    .line 30
    .line 31
    .line 32
    :cond_0
    sget-object v0, Lio/branch/referral/Defines$Jsonkey;->Huawei_App_Gallery:Lio/branch/referral/Defines$Jsonkey;

    .line 33
    .line 34
    invoke-virtual {v0}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-eqz v0, :cond_1

    .line 43
    .line 44
    sget-object v2, Lio/branch/referral/StoreReferrerHuaweiAppGallery;->〇〇888:Ljava/lang/String;

    .line 45
    .line 46
    sget-wide v3, Lio/branch/referral/StoreReferrerHuaweiAppGallery;->Oo08:J

    .line 47
    .line 48
    sget-wide v5, Lio/branch/referral/StoreReferrerHuaweiAppGallery;->o〇0:J

    .line 49
    .line 50
    move-object v1, p0

    .line 51
    move-object v7, p1

    .line 52
    invoke-static/range {v1 .. v7}, Lio/branch/referral/AppStoreReferrer;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;JJLjava/lang/String;)V

    .line 53
    .line 54
    .line 55
    :cond_1
    sget-object v0, Lio/branch/referral/Defines$Jsonkey;->Samsung_Galaxy_Store:Lio/branch/referral/Defines$Jsonkey;

    .line 56
    .line 57
    invoke-virtual {v0}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    if-eqz v0, :cond_2

    .line 66
    .line 67
    sget-object v2, Lio/branch/referral/StoreReferrerSamsungGalaxyStore;->〇〇888:Ljava/lang/String;

    .line 68
    .line 69
    sget-object v0, Lio/branch/referral/StoreReferrerSamsungGalaxyStore;->Oo08:Ljava/lang/Long;

    .line 70
    .line 71
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 72
    .line 73
    .line 74
    move-result-wide v3

    .line 75
    sget-object v0, Lio/branch/referral/StoreReferrerSamsungGalaxyStore;->o〇0:Ljava/lang/Long;

    .line 76
    .line 77
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 78
    .line 79
    .line 80
    move-result-wide v5

    .line 81
    move-object v1, p0

    .line 82
    move-object v7, p1

    .line 83
    invoke-static/range {v1 .. v7}, Lio/branch/referral/AppStoreReferrer;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;JJLjava/lang/String;)V

    .line 84
    .line 85
    .line 86
    :cond_2
    sget-object v0, Lio/branch/referral/Defines$Jsonkey;->Xiaomi_Get_Apps:Lio/branch/referral/Defines$Jsonkey;

    .line 87
    .line 88
    invoke-virtual {v0}, Lio/branch/referral/Defines$Jsonkey;->getKey()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 93
    .line 94
    .line 95
    move-result v0

    .line 96
    if-eqz v0, :cond_3

    .line 97
    .line 98
    sget-object v2, Lio/branch/referral/StoreReferrerXiaomiGetApps;->〇〇888:Ljava/lang/String;

    .line 99
    .line 100
    sget-object v0, Lio/branch/referral/StoreReferrerXiaomiGetApps;->Oo08:Ljava/lang/Long;

    .line 101
    .line 102
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 103
    .line 104
    .line 105
    move-result-wide v3

    .line 106
    sget-object v0, Lio/branch/referral/StoreReferrerXiaomiGetApps;->o〇0:Ljava/lang/Long;

    .line 107
    .line 108
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 109
    .line 110
    .line 111
    move-result-wide v5

    .line 112
    move-object v1, p0

    .line 113
    move-object v7, p1

    .line 114
    invoke-static/range {v1 .. v7}, Lio/branch/referral/AppStoreReferrer;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;JJLjava/lang/String;)V

    .line 115
    .line 116
    .line 117
    :cond_3
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
.end method

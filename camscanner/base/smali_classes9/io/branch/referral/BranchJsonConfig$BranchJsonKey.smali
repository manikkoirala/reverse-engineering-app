.class public final enum Lio/branch/referral/BranchJsonConfig$BranchJsonKey;
.super Ljava/lang/Enum;
.source "BranchJsonConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/branch/referral/BranchJsonConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BranchJsonKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/branch/referral/BranchJsonConfig$BranchJsonKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

.field public static final enum branchKey:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

.field public static final enum enableFacebookLinkCheck:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

.field public static final enum enableLogging:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

.field public static final enum liveKey:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

.field public static final enum testKey:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

.field public static final enum useTestInstance:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;


# direct methods
.method private static synthetic $values()[Lio/branch/referral/BranchJsonConfig$BranchJsonKey;
    .locals 3

    .line 1
    const/4 v0, 0x6

    .line 2
    new-array v0, v0, [Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->branchKey:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->testKey:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->liveKey:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    const/4 v1, 0x3

    .line 20
    sget-object v2, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->useTestInstance:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 21
    .line 22
    aput-object v2, v0, v1

    .line 23
    .line 24
    const/4 v1, 0x4

    .line 25
    sget-object v2, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->enableFacebookLinkCheck:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 26
    .line 27
    aput-object v2, v0, v1

    .line 28
    .line 29
    const/4 v1, 0x5

    .line 30
    sget-object v2, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->enableLogging:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 31
    .line 32
    aput-object v2, v0, v1

    .line 33
    .line 34
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 2
    .line 3
    const-string v1, "branchKey"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->branchKey:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 10
    .line 11
    new-instance v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 12
    .line 13
    const-string/jumbo v1, "testKey"

    .line 14
    .line 15
    .line 16
    const/4 v2, 0x1

    .line 17
    invoke-direct {v0, v1, v2}, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;-><init>(Ljava/lang/String;I)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->testKey:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 21
    .line 22
    new-instance v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 23
    .line 24
    const-string v1, "liveKey"

    .line 25
    .line 26
    const/4 v2, 0x2

    .line 27
    invoke-direct {v0, v1, v2}, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;-><init>(Ljava/lang/String;I)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->liveKey:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 31
    .line 32
    new-instance v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 33
    .line 34
    const-string/jumbo v1, "useTestInstance"

    .line 35
    .line 36
    .line 37
    const/4 v2, 0x3

    .line 38
    invoke-direct {v0, v1, v2}, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;-><init>(Ljava/lang/String;I)V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->useTestInstance:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 42
    .line 43
    new-instance v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 44
    .line 45
    const-string v1, "enableFacebookLinkCheck"

    .line 46
    .line 47
    const/4 v2, 0x4

    .line 48
    invoke-direct {v0, v1, v2}, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;-><init>(Ljava/lang/String;I)V

    .line 49
    .line 50
    .line 51
    sput-object v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->enableFacebookLinkCheck:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 52
    .line 53
    new-instance v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 54
    .line 55
    const-string v1, "enableLogging"

    .line 56
    .line 57
    const/4 v2, 0x5

    .line 58
    invoke-direct {v0, v1, v2}, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;-><init>(Ljava/lang/String;I)V

    .line 59
    .line 60
    .line 61
    sput-object v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->enableLogging:Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 62
    .line 63
    invoke-static {}, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->$values()[Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    sput-object v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->$VALUES:[Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static valueOf(Ljava/lang/String;)Lio/branch/referral/BranchJsonConfig$BranchJsonKey;
    .locals 1

    .line 1
    const-class v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lio/branch/referral/BranchJsonConfig$BranchJsonKey;
    .locals 1

    .line 1
    sget-object v0, Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->$VALUES:[Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lio/branch/referral/BranchJsonConfig$BranchJsonKey;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lio/branch/referral/BranchJsonConfig$BranchJsonKey;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

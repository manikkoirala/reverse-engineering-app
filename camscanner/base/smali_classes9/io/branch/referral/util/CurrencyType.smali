.class public final enum Lio/branch/referral/util/CurrencyType;
.super Ljava/lang/Enum;
.source "CurrencyType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/branch/referral/util/CurrencyType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/branch/referral/util/CurrencyType;

.field public static final enum AED:Lio/branch/referral/util/CurrencyType;

.field public static final enum AFN:Lio/branch/referral/util/CurrencyType;

.field public static final enum ALL:Lio/branch/referral/util/CurrencyType;

.field public static final enum AMD:Lio/branch/referral/util/CurrencyType;

.field public static final enum ANG:Lio/branch/referral/util/CurrencyType;

.field public static final enum AOA:Lio/branch/referral/util/CurrencyType;

.field public static final enum ARS:Lio/branch/referral/util/CurrencyType;

.field public static final enum AUD:Lio/branch/referral/util/CurrencyType;

.field public static final enum AWG:Lio/branch/referral/util/CurrencyType;

.field public static final enum AZN:Lio/branch/referral/util/CurrencyType;

.field public static final enum BAM:Lio/branch/referral/util/CurrencyType;

.field public static final enum BBD:Lio/branch/referral/util/CurrencyType;

.field public static final enum BDT:Lio/branch/referral/util/CurrencyType;

.field public static final enum BGN:Lio/branch/referral/util/CurrencyType;

.field public static final enum BHD:Lio/branch/referral/util/CurrencyType;

.field public static final enum BIF:Lio/branch/referral/util/CurrencyType;

.field public static final enum BMD:Lio/branch/referral/util/CurrencyType;

.field public static final enum BND:Lio/branch/referral/util/CurrencyType;

.field public static final enum BOB:Lio/branch/referral/util/CurrencyType;

.field public static final enum BOV:Lio/branch/referral/util/CurrencyType;

.field public static final enum BRL:Lio/branch/referral/util/CurrencyType;

.field public static final enum BSD:Lio/branch/referral/util/CurrencyType;

.field public static final enum BTN:Lio/branch/referral/util/CurrencyType;

.field public static final enum BWP:Lio/branch/referral/util/CurrencyType;

.field public static final enum BYN:Lio/branch/referral/util/CurrencyType;

.field public static final enum BYR:Lio/branch/referral/util/CurrencyType;

.field public static final enum BZD:Lio/branch/referral/util/CurrencyType;

.field public static final enum CAD:Lio/branch/referral/util/CurrencyType;

.field public static final enum CDF:Lio/branch/referral/util/CurrencyType;

.field public static final enum CHE:Lio/branch/referral/util/CurrencyType;

.field public static final enum CHF:Lio/branch/referral/util/CurrencyType;

.field public static final enum CHW:Lio/branch/referral/util/CurrencyType;

.field public static final enum CLF:Lio/branch/referral/util/CurrencyType;

.field public static final enum CLP:Lio/branch/referral/util/CurrencyType;

.field public static final enum CNY:Lio/branch/referral/util/CurrencyType;

.field public static final enum COP:Lio/branch/referral/util/CurrencyType;

.field public static final enum COU:Lio/branch/referral/util/CurrencyType;

.field public static final enum CRC:Lio/branch/referral/util/CurrencyType;

.field public static final enum CUC:Lio/branch/referral/util/CurrencyType;

.field public static final enum CUP:Lio/branch/referral/util/CurrencyType;

.field public static final enum CVE:Lio/branch/referral/util/CurrencyType;

.field public static final enum CZK:Lio/branch/referral/util/CurrencyType;

.field public static final enum DJF:Lio/branch/referral/util/CurrencyType;

.field public static final enum DKK:Lio/branch/referral/util/CurrencyType;

.field public static final enum DOP:Lio/branch/referral/util/CurrencyType;

.field public static final enum DZD:Lio/branch/referral/util/CurrencyType;

.field public static final enum EGP:Lio/branch/referral/util/CurrencyType;

.field public static final enum ERN:Lio/branch/referral/util/CurrencyType;

.field public static final enum ETB:Lio/branch/referral/util/CurrencyType;

.field public static final enum EUR:Lio/branch/referral/util/CurrencyType;

.field public static final enum FJD:Lio/branch/referral/util/CurrencyType;

.field public static final enum FKP:Lio/branch/referral/util/CurrencyType;

.field public static final enum GBP:Lio/branch/referral/util/CurrencyType;

.field public static final enum GEL:Lio/branch/referral/util/CurrencyType;

.field public static final enum GHS:Lio/branch/referral/util/CurrencyType;

.field public static final enum GIP:Lio/branch/referral/util/CurrencyType;

.field public static final enum GMD:Lio/branch/referral/util/CurrencyType;

.field public static final enum GNF:Lio/branch/referral/util/CurrencyType;

.field public static final enum GTQ:Lio/branch/referral/util/CurrencyType;

.field public static final enum GYD:Lio/branch/referral/util/CurrencyType;

.field public static final enum HKD:Lio/branch/referral/util/CurrencyType;

.field public static final enum HNL:Lio/branch/referral/util/CurrencyType;

.field public static final enum HRK:Lio/branch/referral/util/CurrencyType;

.field public static final enum HTG:Lio/branch/referral/util/CurrencyType;

.field public static final enum HUF:Lio/branch/referral/util/CurrencyType;

.field public static final enum IDR:Lio/branch/referral/util/CurrencyType;

.field public static final enum ILS:Lio/branch/referral/util/CurrencyType;

.field public static final enum INR:Lio/branch/referral/util/CurrencyType;

.field public static final enum IQD:Lio/branch/referral/util/CurrencyType;

.field public static final enum IRR:Lio/branch/referral/util/CurrencyType;

.field public static final enum ISK:Lio/branch/referral/util/CurrencyType;

.field public static final enum JMD:Lio/branch/referral/util/CurrencyType;

.field public static final enum JOD:Lio/branch/referral/util/CurrencyType;

.field public static final enum JPY:Lio/branch/referral/util/CurrencyType;

.field public static final enum KES:Lio/branch/referral/util/CurrencyType;

.field public static final enum KGS:Lio/branch/referral/util/CurrencyType;

.field public static final enum KHR:Lio/branch/referral/util/CurrencyType;

.field public static final enum KMF:Lio/branch/referral/util/CurrencyType;

.field public static final enum KPW:Lio/branch/referral/util/CurrencyType;

.field public static final enum KRW:Lio/branch/referral/util/CurrencyType;

.field public static final enum KWD:Lio/branch/referral/util/CurrencyType;

.field public static final enum KYD:Lio/branch/referral/util/CurrencyType;

.field public static final enum KZT:Lio/branch/referral/util/CurrencyType;

.field public static final enum LAK:Lio/branch/referral/util/CurrencyType;

.field public static final enum LBP:Lio/branch/referral/util/CurrencyType;

.field public static final enum LKR:Lio/branch/referral/util/CurrencyType;

.field public static final enum LRD:Lio/branch/referral/util/CurrencyType;

.field public static final enum LSL:Lio/branch/referral/util/CurrencyType;

.field public static final enum LYD:Lio/branch/referral/util/CurrencyType;

.field public static final enum MAD:Lio/branch/referral/util/CurrencyType;

.field public static final enum MDL:Lio/branch/referral/util/CurrencyType;

.field public static final enum MGA:Lio/branch/referral/util/CurrencyType;

.field public static final enum MKD:Lio/branch/referral/util/CurrencyType;

.field public static final enum MMK:Lio/branch/referral/util/CurrencyType;

.field public static final enum MNT:Lio/branch/referral/util/CurrencyType;

.field public static final enum MOP:Lio/branch/referral/util/CurrencyType;

.field public static final enum MRO:Lio/branch/referral/util/CurrencyType;

.field public static final enum MUR:Lio/branch/referral/util/CurrencyType;

.field public static final enum MVR:Lio/branch/referral/util/CurrencyType;

.field public static final enum MWK:Lio/branch/referral/util/CurrencyType;

.field public static final enum MXN:Lio/branch/referral/util/CurrencyType;

.field public static final enum MXV:Lio/branch/referral/util/CurrencyType;

.field public static final enum MYR:Lio/branch/referral/util/CurrencyType;

.field public static final enum MZN:Lio/branch/referral/util/CurrencyType;

.field public static final enum NAD:Lio/branch/referral/util/CurrencyType;

.field public static final enum NGN:Lio/branch/referral/util/CurrencyType;

.field public static final enum NIO:Lio/branch/referral/util/CurrencyType;

.field public static final enum NOK:Lio/branch/referral/util/CurrencyType;

.field public static final enum NPR:Lio/branch/referral/util/CurrencyType;

.field public static final enum NZD:Lio/branch/referral/util/CurrencyType;

.field public static final enum OMR:Lio/branch/referral/util/CurrencyType;

.field public static final enum PAB:Lio/branch/referral/util/CurrencyType;

.field public static final enum PEN:Lio/branch/referral/util/CurrencyType;

.field public static final enum PGK:Lio/branch/referral/util/CurrencyType;

.field public static final enum PHP:Lio/branch/referral/util/CurrencyType;

.field public static final enum PKR:Lio/branch/referral/util/CurrencyType;

.field public static final enum PLN:Lio/branch/referral/util/CurrencyType;

.field public static final enum PYG:Lio/branch/referral/util/CurrencyType;

.field public static final enum QAR:Lio/branch/referral/util/CurrencyType;

.field public static final enum RON:Lio/branch/referral/util/CurrencyType;

.field public static final enum RSD:Lio/branch/referral/util/CurrencyType;

.field public static final enum RUB:Lio/branch/referral/util/CurrencyType;

.field public static final enum RWF:Lio/branch/referral/util/CurrencyType;

.field public static final enum SAR:Lio/branch/referral/util/CurrencyType;

.field public static final enum SBD:Lio/branch/referral/util/CurrencyType;

.field public static final enum SCR:Lio/branch/referral/util/CurrencyType;

.field public static final enum SDG:Lio/branch/referral/util/CurrencyType;

.field public static final enum SEK:Lio/branch/referral/util/CurrencyType;

.field public static final enum SGD:Lio/branch/referral/util/CurrencyType;

.field public static final enum SHP:Lio/branch/referral/util/CurrencyType;

.field public static final enum SLL:Lio/branch/referral/util/CurrencyType;

.field public static final enum SOS:Lio/branch/referral/util/CurrencyType;

.field public static final enum SRD:Lio/branch/referral/util/CurrencyType;

.field public static final enum SSP:Lio/branch/referral/util/CurrencyType;

.field public static final enum STD:Lio/branch/referral/util/CurrencyType;

.field public static final enum SYP:Lio/branch/referral/util/CurrencyType;

.field public static final enum SZL:Lio/branch/referral/util/CurrencyType;

.field public static final enum THB:Lio/branch/referral/util/CurrencyType;

.field public static final enum TJS:Lio/branch/referral/util/CurrencyType;

.field public static final enum TMT:Lio/branch/referral/util/CurrencyType;

.field public static final enum TND:Lio/branch/referral/util/CurrencyType;

.field public static final enum TOP:Lio/branch/referral/util/CurrencyType;

.field public static final enum TRY:Lio/branch/referral/util/CurrencyType;

.field public static final enum TTD:Lio/branch/referral/util/CurrencyType;

.field public static final enum TWD:Lio/branch/referral/util/CurrencyType;

.field public static final enum TZS:Lio/branch/referral/util/CurrencyType;

.field public static final enum UAH:Lio/branch/referral/util/CurrencyType;

.field public static final enum UGX:Lio/branch/referral/util/CurrencyType;

.field public static final enum USD:Lio/branch/referral/util/CurrencyType;

.field public static final enum USN:Lio/branch/referral/util/CurrencyType;

.field public static final enum UYI:Lio/branch/referral/util/CurrencyType;

.field public static final enum UYU:Lio/branch/referral/util/CurrencyType;

.field public static final enum UZS:Lio/branch/referral/util/CurrencyType;

.field public static final enum VEF:Lio/branch/referral/util/CurrencyType;

.field public static final enum VND:Lio/branch/referral/util/CurrencyType;

.field public static final enum VUV:Lio/branch/referral/util/CurrencyType;

.field public static final enum WST:Lio/branch/referral/util/CurrencyType;

.field public static final enum XAF:Lio/branch/referral/util/CurrencyType;

.field public static final enum XAG:Lio/branch/referral/util/CurrencyType;

.field public static final enum XAU:Lio/branch/referral/util/CurrencyType;

.field public static final enum XBA:Lio/branch/referral/util/CurrencyType;

.field public static final enum XBB:Lio/branch/referral/util/CurrencyType;

.field public static final enum XBC:Lio/branch/referral/util/CurrencyType;

.field public static final enum XBD:Lio/branch/referral/util/CurrencyType;

.field public static final enum XCD:Lio/branch/referral/util/CurrencyType;

.field public static final enum XDR:Lio/branch/referral/util/CurrencyType;

.field public static final enum XFU:Lio/branch/referral/util/CurrencyType;

.field public static final enum XOF:Lio/branch/referral/util/CurrencyType;

.field public static final enum XPD:Lio/branch/referral/util/CurrencyType;

.field public static final enum XPF:Lio/branch/referral/util/CurrencyType;

.field public static final enum XPT:Lio/branch/referral/util/CurrencyType;

.field public static final enum XSU:Lio/branch/referral/util/CurrencyType;

.field public static final enum XTS:Lio/branch/referral/util/CurrencyType;

.field public static final enum XUA:Lio/branch/referral/util/CurrencyType;

.field public static final enum XXX:Lio/branch/referral/util/CurrencyType;

.field public static final enum YER:Lio/branch/referral/util/CurrencyType;

.field public static final enum ZAR:Lio/branch/referral/util/CurrencyType;

.field public static final enum ZMW:Lio/branch/referral/util/CurrencyType;


# instance fields
.field private iso4217Code:Ljava/lang/String;


# direct methods
.method private static synthetic $values()[Lio/branch/referral/util/CurrencyType;
    .locals 3

    .line 1
    const/16 v0, 0xb2

    .line 2
    .line 3
    new-array v0, v0, [Lio/branch/referral/util/CurrencyType;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lio/branch/referral/util/CurrencyType;->AED:Lio/branch/referral/util/CurrencyType;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lio/branch/referral/util/CurrencyType;->AFN:Lio/branch/referral/util/CurrencyType;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lio/branch/referral/util/CurrencyType;->ALL:Lio/branch/referral/util/CurrencyType;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lio/branch/referral/util/CurrencyType;->AMD:Lio/branch/referral/util/CurrencyType;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lio/branch/referral/util/CurrencyType;->ANG:Lio/branch/referral/util/CurrencyType;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lio/branch/referral/util/CurrencyType;->AOA:Lio/branch/referral/util/CurrencyType;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lio/branch/referral/util/CurrencyType;->ARS:Lio/branch/referral/util/CurrencyType;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lio/branch/referral/util/CurrencyType;->AUD:Lio/branch/referral/util/CurrencyType;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lio/branch/referral/util/CurrencyType;->AWG:Lio/branch/referral/util/CurrencyType;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    const/16 v1, 0x9

    .line 52
    .line 53
    sget-object v2, Lio/branch/referral/util/CurrencyType;->AZN:Lio/branch/referral/util/CurrencyType;

    .line 54
    .line 55
    aput-object v2, v0, v1

    .line 56
    .line 57
    const/16 v1, 0xa

    .line 58
    .line 59
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BAM:Lio/branch/referral/util/CurrencyType;

    .line 60
    .line 61
    aput-object v2, v0, v1

    .line 62
    .line 63
    const/16 v1, 0xb

    .line 64
    .line 65
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BBD:Lio/branch/referral/util/CurrencyType;

    .line 66
    .line 67
    aput-object v2, v0, v1

    .line 68
    .line 69
    const/16 v1, 0xc

    .line 70
    .line 71
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BDT:Lio/branch/referral/util/CurrencyType;

    .line 72
    .line 73
    aput-object v2, v0, v1

    .line 74
    .line 75
    const/16 v1, 0xd

    .line 76
    .line 77
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BGN:Lio/branch/referral/util/CurrencyType;

    .line 78
    .line 79
    aput-object v2, v0, v1

    .line 80
    .line 81
    const/16 v1, 0xe

    .line 82
    .line 83
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BHD:Lio/branch/referral/util/CurrencyType;

    .line 84
    .line 85
    aput-object v2, v0, v1

    .line 86
    .line 87
    const/16 v1, 0xf

    .line 88
    .line 89
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BIF:Lio/branch/referral/util/CurrencyType;

    .line 90
    .line 91
    aput-object v2, v0, v1

    .line 92
    .line 93
    const/16 v1, 0x10

    .line 94
    .line 95
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BMD:Lio/branch/referral/util/CurrencyType;

    .line 96
    .line 97
    aput-object v2, v0, v1

    .line 98
    .line 99
    const/16 v1, 0x11

    .line 100
    .line 101
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BND:Lio/branch/referral/util/CurrencyType;

    .line 102
    .line 103
    aput-object v2, v0, v1

    .line 104
    .line 105
    const/16 v1, 0x12

    .line 106
    .line 107
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BOB:Lio/branch/referral/util/CurrencyType;

    .line 108
    .line 109
    aput-object v2, v0, v1

    .line 110
    .line 111
    const/16 v1, 0x13

    .line 112
    .line 113
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BOV:Lio/branch/referral/util/CurrencyType;

    .line 114
    .line 115
    aput-object v2, v0, v1

    .line 116
    .line 117
    const/16 v1, 0x14

    .line 118
    .line 119
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BRL:Lio/branch/referral/util/CurrencyType;

    .line 120
    .line 121
    aput-object v2, v0, v1

    .line 122
    .line 123
    const/16 v1, 0x15

    .line 124
    .line 125
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BSD:Lio/branch/referral/util/CurrencyType;

    .line 126
    .line 127
    aput-object v2, v0, v1

    .line 128
    .line 129
    const/16 v1, 0x16

    .line 130
    .line 131
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BTN:Lio/branch/referral/util/CurrencyType;

    .line 132
    .line 133
    aput-object v2, v0, v1

    .line 134
    .line 135
    const/16 v1, 0x17

    .line 136
    .line 137
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BWP:Lio/branch/referral/util/CurrencyType;

    .line 138
    .line 139
    aput-object v2, v0, v1

    .line 140
    .line 141
    const/16 v1, 0x18

    .line 142
    .line 143
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BYN:Lio/branch/referral/util/CurrencyType;

    .line 144
    .line 145
    aput-object v2, v0, v1

    .line 146
    .line 147
    const/16 v1, 0x19

    .line 148
    .line 149
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BYR:Lio/branch/referral/util/CurrencyType;

    .line 150
    .line 151
    aput-object v2, v0, v1

    .line 152
    .line 153
    const/16 v1, 0x1a

    .line 154
    .line 155
    sget-object v2, Lio/branch/referral/util/CurrencyType;->BZD:Lio/branch/referral/util/CurrencyType;

    .line 156
    .line 157
    aput-object v2, v0, v1

    .line 158
    .line 159
    const/16 v1, 0x1b

    .line 160
    .line 161
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CAD:Lio/branch/referral/util/CurrencyType;

    .line 162
    .line 163
    aput-object v2, v0, v1

    .line 164
    .line 165
    const/16 v1, 0x1c

    .line 166
    .line 167
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CDF:Lio/branch/referral/util/CurrencyType;

    .line 168
    .line 169
    aput-object v2, v0, v1

    .line 170
    .line 171
    const/16 v1, 0x1d

    .line 172
    .line 173
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CHE:Lio/branch/referral/util/CurrencyType;

    .line 174
    .line 175
    aput-object v2, v0, v1

    .line 176
    .line 177
    const/16 v1, 0x1e

    .line 178
    .line 179
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CHF:Lio/branch/referral/util/CurrencyType;

    .line 180
    .line 181
    aput-object v2, v0, v1

    .line 182
    .line 183
    const/16 v1, 0x1f

    .line 184
    .line 185
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CHW:Lio/branch/referral/util/CurrencyType;

    .line 186
    .line 187
    aput-object v2, v0, v1

    .line 188
    .line 189
    const/16 v1, 0x20

    .line 190
    .line 191
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CLF:Lio/branch/referral/util/CurrencyType;

    .line 192
    .line 193
    aput-object v2, v0, v1

    .line 194
    .line 195
    const/16 v1, 0x21

    .line 196
    .line 197
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CLP:Lio/branch/referral/util/CurrencyType;

    .line 198
    .line 199
    aput-object v2, v0, v1

    .line 200
    .line 201
    const/16 v1, 0x22

    .line 202
    .line 203
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CNY:Lio/branch/referral/util/CurrencyType;

    .line 204
    .line 205
    aput-object v2, v0, v1

    .line 206
    .line 207
    const/16 v1, 0x23

    .line 208
    .line 209
    sget-object v2, Lio/branch/referral/util/CurrencyType;->COP:Lio/branch/referral/util/CurrencyType;

    .line 210
    .line 211
    aput-object v2, v0, v1

    .line 212
    .line 213
    const/16 v1, 0x24

    .line 214
    .line 215
    sget-object v2, Lio/branch/referral/util/CurrencyType;->COU:Lio/branch/referral/util/CurrencyType;

    .line 216
    .line 217
    aput-object v2, v0, v1

    .line 218
    .line 219
    const/16 v1, 0x25

    .line 220
    .line 221
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CRC:Lio/branch/referral/util/CurrencyType;

    .line 222
    .line 223
    aput-object v2, v0, v1

    .line 224
    .line 225
    const/16 v1, 0x26

    .line 226
    .line 227
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CUC:Lio/branch/referral/util/CurrencyType;

    .line 228
    .line 229
    aput-object v2, v0, v1

    .line 230
    .line 231
    const/16 v1, 0x27

    .line 232
    .line 233
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CUP:Lio/branch/referral/util/CurrencyType;

    .line 234
    .line 235
    aput-object v2, v0, v1

    .line 236
    .line 237
    const/16 v1, 0x28

    .line 238
    .line 239
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CVE:Lio/branch/referral/util/CurrencyType;

    .line 240
    .line 241
    aput-object v2, v0, v1

    .line 242
    .line 243
    const/16 v1, 0x29

    .line 244
    .line 245
    sget-object v2, Lio/branch/referral/util/CurrencyType;->CZK:Lio/branch/referral/util/CurrencyType;

    .line 246
    .line 247
    aput-object v2, v0, v1

    .line 248
    .line 249
    const/16 v1, 0x2a

    .line 250
    .line 251
    sget-object v2, Lio/branch/referral/util/CurrencyType;->DJF:Lio/branch/referral/util/CurrencyType;

    .line 252
    .line 253
    aput-object v2, v0, v1

    .line 254
    .line 255
    const/16 v1, 0x2b

    .line 256
    .line 257
    sget-object v2, Lio/branch/referral/util/CurrencyType;->DKK:Lio/branch/referral/util/CurrencyType;

    .line 258
    .line 259
    aput-object v2, v0, v1

    .line 260
    .line 261
    const/16 v1, 0x2c

    .line 262
    .line 263
    sget-object v2, Lio/branch/referral/util/CurrencyType;->DOP:Lio/branch/referral/util/CurrencyType;

    .line 264
    .line 265
    aput-object v2, v0, v1

    .line 266
    .line 267
    const/16 v1, 0x2d

    .line 268
    .line 269
    sget-object v2, Lio/branch/referral/util/CurrencyType;->DZD:Lio/branch/referral/util/CurrencyType;

    .line 270
    .line 271
    aput-object v2, v0, v1

    .line 272
    .line 273
    const/16 v1, 0x2e

    .line 274
    .line 275
    sget-object v2, Lio/branch/referral/util/CurrencyType;->EGP:Lio/branch/referral/util/CurrencyType;

    .line 276
    .line 277
    aput-object v2, v0, v1

    .line 278
    .line 279
    const/16 v1, 0x2f

    .line 280
    .line 281
    sget-object v2, Lio/branch/referral/util/CurrencyType;->ERN:Lio/branch/referral/util/CurrencyType;

    .line 282
    .line 283
    aput-object v2, v0, v1

    .line 284
    .line 285
    const/16 v1, 0x30

    .line 286
    .line 287
    sget-object v2, Lio/branch/referral/util/CurrencyType;->ETB:Lio/branch/referral/util/CurrencyType;

    .line 288
    .line 289
    aput-object v2, v0, v1

    .line 290
    .line 291
    const/16 v1, 0x31

    .line 292
    .line 293
    sget-object v2, Lio/branch/referral/util/CurrencyType;->EUR:Lio/branch/referral/util/CurrencyType;

    .line 294
    .line 295
    aput-object v2, v0, v1

    .line 296
    .line 297
    const/16 v1, 0x32

    .line 298
    .line 299
    sget-object v2, Lio/branch/referral/util/CurrencyType;->FJD:Lio/branch/referral/util/CurrencyType;

    .line 300
    .line 301
    aput-object v2, v0, v1

    .line 302
    .line 303
    const/16 v1, 0x33

    .line 304
    .line 305
    sget-object v2, Lio/branch/referral/util/CurrencyType;->FKP:Lio/branch/referral/util/CurrencyType;

    .line 306
    .line 307
    aput-object v2, v0, v1

    .line 308
    .line 309
    const/16 v1, 0x34

    .line 310
    .line 311
    sget-object v2, Lio/branch/referral/util/CurrencyType;->GBP:Lio/branch/referral/util/CurrencyType;

    .line 312
    .line 313
    aput-object v2, v0, v1

    .line 314
    .line 315
    const/16 v1, 0x35

    .line 316
    .line 317
    sget-object v2, Lio/branch/referral/util/CurrencyType;->GEL:Lio/branch/referral/util/CurrencyType;

    .line 318
    .line 319
    aput-object v2, v0, v1

    .line 320
    .line 321
    const/16 v1, 0x36

    .line 322
    .line 323
    sget-object v2, Lio/branch/referral/util/CurrencyType;->GHS:Lio/branch/referral/util/CurrencyType;

    .line 324
    .line 325
    aput-object v2, v0, v1

    .line 326
    .line 327
    const/16 v1, 0x37

    .line 328
    .line 329
    sget-object v2, Lio/branch/referral/util/CurrencyType;->GIP:Lio/branch/referral/util/CurrencyType;

    .line 330
    .line 331
    aput-object v2, v0, v1

    .line 332
    .line 333
    const/16 v1, 0x38

    .line 334
    .line 335
    sget-object v2, Lio/branch/referral/util/CurrencyType;->GMD:Lio/branch/referral/util/CurrencyType;

    .line 336
    .line 337
    aput-object v2, v0, v1

    .line 338
    .line 339
    const/16 v1, 0x39

    .line 340
    .line 341
    sget-object v2, Lio/branch/referral/util/CurrencyType;->GNF:Lio/branch/referral/util/CurrencyType;

    .line 342
    .line 343
    aput-object v2, v0, v1

    .line 344
    .line 345
    const/16 v1, 0x3a

    .line 346
    .line 347
    sget-object v2, Lio/branch/referral/util/CurrencyType;->GTQ:Lio/branch/referral/util/CurrencyType;

    .line 348
    .line 349
    aput-object v2, v0, v1

    .line 350
    .line 351
    const/16 v1, 0x3b

    .line 352
    .line 353
    sget-object v2, Lio/branch/referral/util/CurrencyType;->GYD:Lio/branch/referral/util/CurrencyType;

    .line 354
    .line 355
    aput-object v2, v0, v1

    .line 356
    .line 357
    const/16 v1, 0x3c

    .line 358
    .line 359
    sget-object v2, Lio/branch/referral/util/CurrencyType;->HKD:Lio/branch/referral/util/CurrencyType;

    .line 360
    .line 361
    aput-object v2, v0, v1

    .line 362
    .line 363
    const/16 v1, 0x3d

    .line 364
    .line 365
    sget-object v2, Lio/branch/referral/util/CurrencyType;->HNL:Lio/branch/referral/util/CurrencyType;

    .line 366
    .line 367
    aput-object v2, v0, v1

    .line 368
    .line 369
    const/16 v1, 0x3e

    .line 370
    .line 371
    sget-object v2, Lio/branch/referral/util/CurrencyType;->HRK:Lio/branch/referral/util/CurrencyType;

    .line 372
    .line 373
    aput-object v2, v0, v1

    .line 374
    .line 375
    const/16 v1, 0x3f

    .line 376
    .line 377
    sget-object v2, Lio/branch/referral/util/CurrencyType;->HTG:Lio/branch/referral/util/CurrencyType;

    .line 378
    .line 379
    aput-object v2, v0, v1

    .line 380
    .line 381
    const/16 v1, 0x40

    .line 382
    .line 383
    sget-object v2, Lio/branch/referral/util/CurrencyType;->HUF:Lio/branch/referral/util/CurrencyType;

    .line 384
    .line 385
    aput-object v2, v0, v1

    .line 386
    .line 387
    const/16 v1, 0x41

    .line 388
    .line 389
    sget-object v2, Lio/branch/referral/util/CurrencyType;->IDR:Lio/branch/referral/util/CurrencyType;

    .line 390
    .line 391
    aput-object v2, v0, v1

    .line 392
    .line 393
    const/16 v1, 0x42

    .line 394
    .line 395
    sget-object v2, Lio/branch/referral/util/CurrencyType;->ILS:Lio/branch/referral/util/CurrencyType;

    .line 396
    .line 397
    aput-object v2, v0, v1

    .line 398
    .line 399
    const/16 v1, 0x43

    .line 400
    .line 401
    sget-object v2, Lio/branch/referral/util/CurrencyType;->INR:Lio/branch/referral/util/CurrencyType;

    .line 402
    .line 403
    aput-object v2, v0, v1

    .line 404
    .line 405
    const/16 v1, 0x44

    .line 406
    .line 407
    sget-object v2, Lio/branch/referral/util/CurrencyType;->IQD:Lio/branch/referral/util/CurrencyType;

    .line 408
    .line 409
    aput-object v2, v0, v1

    .line 410
    .line 411
    const/16 v1, 0x45

    .line 412
    .line 413
    sget-object v2, Lio/branch/referral/util/CurrencyType;->IRR:Lio/branch/referral/util/CurrencyType;

    .line 414
    .line 415
    aput-object v2, v0, v1

    .line 416
    .line 417
    const/16 v1, 0x46

    .line 418
    .line 419
    sget-object v2, Lio/branch/referral/util/CurrencyType;->ISK:Lio/branch/referral/util/CurrencyType;

    .line 420
    .line 421
    aput-object v2, v0, v1

    .line 422
    .line 423
    const/16 v1, 0x47

    .line 424
    .line 425
    sget-object v2, Lio/branch/referral/util/CurrencyType;->JMD:Lio/branch/referral/util/CurrencyType;

    .line 426
    .line 427
    aput-object v2, v0, v1

    .line 428
    .line 429
    const/16 v1, 0x48

    .line 430
    .line 431
    sget-object v2, Lio/branch/referral/util/CurrencyType;->JOD:Lio/branch/referral/util/CurrencyType;

    .line 432
    .line 433
    aput-object v2, v0, v1

    .line 434
    .line 435
    const/16 v1, 0x49

    .line 436
    .line 437
    sget-object v2, Lio/branch/referral/util/CurrencyType;->JPY:Lio/branch/referral/util/CurrencyType;

    .line 438
    .line 439
    aput-object v2, v0, v1

    .line 440
    .line 441
    const/16 v1, 0x4a

    .line 442
    .line 443
    sget-object v2, Lio/branch/referral/util/CurrencyType;->KES:Lio/branch/referral/util/CurrencyType;

    .line 444
    .line 445
    aput-object v2, v0, v1

    .line 446
    .line 447
    const/16 v1, 0x4b

    .line 448
    .line 449
    sget-object v2, Lio/branch/referral/util/CurrencyType;->KGS:Lio/branch/referral/util/CurrencyType;

    .line 450
    .line 451
    aput-object v2, v0, v1

    .line 452
    .line 453
    const/16 v1, 0x4c

    .line 454
    .line 455
    sget-object v2, Lio/branch/referral/util/CurrencyType;->KHR:Lio/branch/referral/util/CurrencyType;

    .line 456
    .line 457
    aput-object v2, v0, v1

    .line 458
    .line 459
    const/16 v1, 0x4d

    .line 460
    .line 461
    sget-object v2, Lio/branch/referral/util/CurrencyType;->KMF:Lio/branch/referral/util/CurrencyType;

    .line 462
    .line 463
    aput-object v2, v0, v1

    .line 464
    .line 465
    const/16 v1, 0x4e

    .line 466
    .line 467
    sget-object v2, Lio/branch/referral/util/CurrencyType;->KPW:Lio/branch/referral/util/CurrencyType;

    .line 468
    .line 469
    aput-object v2, v0, v1

    .line 470
    .line 471
    const/16 v1, 0x4f

    .line 472
    .line 473
    sget-object v2, Lio/branch/referral/util/CurrencyType;->KRW:Lio/branch/referral/util/CurrencyType;

    .line 474
    .line 475
    aput-object v2, v0, v1

    .line 476
    .line 477
    const/16 v1, 0x50

    .line 478
    .line 479
    sget-object v2, Lio/branch/referral/util/CurrencyType;->KWD:Lio/branch/referral/util/CurrencyType;

    .line 480
    .line 481
    aput-object v2, v0, v1

    .line 482
    .line 483
    const/16 v1, 0x51

    .line 484
    .line 485
    sget-object v2, Lio/branch/referral/util/CurrencyType;->KYD:Lio/branch/referral/util/CurrencyType;

    .line 486
    .line 487
    aput-object v2, v0, v1

    .line 488
    .line 489
    const/16 v1, 0x52

    .line 490
    .line 491
    sget-object v2, Lio/branch/referral/util/CurrencyType;->KZT:Lio/branch/referral/util/CurrencyType;

    .line 492
    .line 493
    aput-object v2, v0, v1

    .line 494
    .line 495
    const/16 v1, 0x53

    .line 496
    .line 497
    sget-object v2, Lio/branch/referral/util/CurrencyType;->LAK:Lio/branch/referral/util/CurrencyType;

    .line 498
    .line 499
    aput-object v2, v0, v1

    .line 500
    .line 501
    const/16 v1, 0x54

    .line 502
    .line 503
    sget-object v2, Lio/branch/referral/util/CurrencyType;->LBP:Lio/branch/referral/util/CurrencyType;

    .line 504
    .line 505
    aput-object v2, v0, v1

    .line 506
    .line 507
    const/16 v1, 0x55

    .line 508
    .line 509
    sget-object v2, Lio/branch/referral/util/CurrencyType;->LKR:Lio/branch/referral/util/CurrencyType;

    .line 510
    .line 511
    aput-object v2, v0, v1

    .line 512
    .line 513
    const/16 v1, 0x56

    .line 514
    .line 515
    sget-object v2, Lio/branch/referral/util/CurrencyType;->LRD:Lio/branch/referral/util/CurrencyType;

    .line 516
    .line 517
    aput-object v2, v0, v1

    .line 518
    .line 519
    const/16 v1, 0x57

    .line 520
    .line 521
    sget-object v2, Lio/branch/referral/util/CurrencyType;->LSL:Lio/branch/referral/util/CurrencyType;

    .line 522
    .line 523
    aput-object v2, v0, v1

    .line 524
    .line 525
    const/16 v1, 0x58

    .line 526
    .line 527
    sget-object v2, Lio/branch/referral/util/CurrencyType;->LYD:Lio/branch/referral/util/CurrencyType;

    .line 528
    .line 529
    aput-object v2, v0, v1

    .line 530
    .line 531
    const/16 v1, 0x59

    .line 532
    .line 533
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MAD:Lio/branch/referral/util/CurrencyType;

    .line 534
    .line 535
    aput-object v2, v0, v1

    .line 536
    .line 537
    const/16 v1, 0x5a

    .line 538
    .line 539
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MDL:Lio/branch/referral/util/CurrencyType;

    .line 540
    .line 541
    aput-object v2, v0, v1

    .line 542
    .line 543
    const/16 v1, 0x5b

    .line 544
    .line 545
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MGA:Lio/branch/referral/util/CurrencyType;

    .line 546
    .line 547
    aput-object v2, v0, v1

    .line 548
    .line 549
    const/16 v1, 0x5c

    .line 550
    .line 551
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MKD:Lio/branch/referral/util/CurrencyType;

    .line 552
    .line 553
    aput-object v2, v0, v1

    .line 554
    .line 555
    const/16 v1, 0x5d

    .line 556
    .line 557
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MMK:Lio/branch/referral/util/CurrencyType;

    .line 558
    .line 559
    aput-object v2, v0, v1

    .line 560
    .line 561
    const/16 v1, 0x5e

    .line 562
    .line 563
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MNT:Lio/branch/referral/util/CurrencyType;

    .line 564
    .line 565
    aput-object v2, v0, v1

    .line 566
    .line 567
    const/16 v1, 0x5f

    .line 568
    .line 569
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MOP:Lio/branch/referral/util/CurrencyType;

    .line 570
    .line 571
    aput-object v2, v0, v1

    .line 572
    .line 573
    const/16 v1, 0x60

    .line 574
    .line 575
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MRO:Lio/branch/referral/util/CurrencyType;

    .line 576
    .line 577
    aput-object v2, v0, v1

    .line 578
    .line 579
    const/16 v1, 0x61

    .line 580
    .line 581
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MUR:Lio/branch/referral/util/CurrencyType;

    .line 582
    .line 583
    aput-object v2, v0, v1

    .line 584
    .line 585
    const/16 v1, 0x62

    .line 586
    .line 587
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MVR:Lio/branch/referral/util/CurrencyType;

    .line 588
    .line 589
    aput-object v2, v0, v1

    .line 590
    .line 591
    const/16 v1, 0x63

    .line 592
    .line 593
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MWK:Lio/branch/referral/util/CurrencyType;

    .line 594
    .line 595
    aput-object v2, v0, v1

    .line 596
    .line 597
    const/16 v1, 0x64

    .line 598
    .line 599
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MXN:Lio/branch/referral/util/CurrencyType;

    .line 600
    .line 601
    aput-object v2, v0, v1

    .line 602
    .line 603
    const/16 v1, 0x65

    .line 604
    .line 605
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MXV:Lio/branch/referral/util/CurrencyType;

    .line 606
    .line 607
    aput-object v2, v0, v1

    .line 608
    .line 609
    const/16 v1, 0x66

    .line 610
    .line 611
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MYR:Lio/branch/referral/util/CurrencyType;

    .line 612
    .line 613
    aput-object v2, v0, v1

    .line 614
    .line 615
    const/16 v1, 0x67

    .line 616
    .line 617
    sget-object v2, Lio/branch/referral/util/CurrencyType;->MZN:Lio/branch/referral/util/CurrencyType;

    .line 618
    .line 619
    aput-object v2, v0, v1

    .line 620
    .line 621
    const/16 v1, 0x68

    .line 622
    .line 623
    sget-object v2, Lio/branch/referral/util/CurrencyType;->NAD:Lio/branch/referral/util/CurrencyType;

    .line 624
    .line 625
    aput-object v2, v0, v1

    .line 626
    .line 627
    const/16 v1, 0x69

    .line 628
    .line 629
    sget-object v2, Lio/branch/referral/util/CurrencyType;->NGN:Lio/branch/referral/util/CurrencyType;

    .line 630
    .line 631
    aput-object v2, v0, v1

    .line 632
    .line 633
    const/16 v1, 0x6a

    .line 634
    .line 635
    sget-object v2, Lio/branch/referral/util/CurrencyType;->NIO:Lio/branch/referral/util/CurrencyType;

    .line 636
    .line 637
    aput-object v2, v0, v1

    .line 638
    .line 639
    const/16 v1, 0x6b

    .line 640
    .line 641
    sget-object v2, Lio/branch/referral/util/CurrencyType;->NOK:Lio/branch/referral/util/CurrencyType;

    .line 642
    .line 643
    aput-object v2, v0, v1

    .line 644
    .line 645
    const/16 v1, 0x6c

    .line 646
    .line 647
    sget-object v2, Lio/branch/referral/util/CurrencyType;->NPR:Lio/branch/referral/util/CurrencyType;

    .line 648
    .line 649
    aput-object v2, v0, v1

    .line 650
    .line 651
    const/16 v1, 0x6d

    .line 652
    .line 653
    sget-object v2, Lio/branch/referral/util/CurrencyType;->NZD:Lio/branch/referral/util/CurrencyType;

    .line 654
    .line 655
    aput-object v2, v0, v1

    .line 656
    .line 657
    const/16 v1, 0x6e

    .line 658
    .line 659
    sget-object v2, Lio/branch/referral/util/CurrencyType;->OMR:Lio/branch/referral/util/CurrencyType;

    .line 660
    .line 661
    aput-object v2, v0, v1

    .line 662
    .line 663
    const/16 v1, 0x6f

    .line 664
    .line 665
    sget-object v2, Lio/branch/referral/util/CurrencyType;->PAB:Lio/branch/referral/util/CurrencyType;

    .line 666
    .line 667
    aput-object v2, v0, v1

    .line 668
    .line 669
    const/16 v1, 0x70

    .line 670
    .line 671
    sget-object v2, Lio/branch/referral/util/CurrencyType;->PEN:Lio/branch/referral/util/CurrencyType;

    .line 672
    .line 673
    aput-object v2, v0, v1

    .line 674
    .line 675
    const/16 v1, 0x71

    .line 676
    .line 677
    sget-object v2, Lio/branch/referral/util/CurrencyType;->PGK:Lio/branch/referral/util/CurrencyType;

    .line 678
    .line 679
    aput-object v2, v0, v1

    .line 680
    .line 681
    const/16 v1, 0x72

    .line 682
    .line 683
    sget-object v2, Lio/branch/referral/util/CurrencyType;->PHP:Lio/branch/referral/util/CurrencyType;

    .line 684
    .line 685
    aput-object v2, v0, v1

    .line 686
    .line 687
    const/16 v1, 0x73

    .line 688
    .line 689
    sget-object v2, Lio/branch/referral/util/CurrencyType;->PKR:Lio/branch/referral/util/CurrencyType;

    .line 690
    .line 691
    aput-object v2, v0, v1

    .line 692
    .line 693
    const/16 v1, 0x74

    .line 694
    .line 695
    sget-object v2, Lio/branch/referral/util/CurrencyType;->PLN:Lio/branch/referral/util/CurrencyType;

    .line 696
    .line 697
    aput-object v2, v0, v1

    .line 698
    .line 699
    const/16 v1, 0x75

    .line 700
    .line 701
    sget-object v2, Lio/branch/referral/util/CurrencyType;->PYG:Lio/branch/referral/util/CurrencyType;

    .line 702
    .line 703
    aput-object v2, v0, v1

    .line 704
    .line 705
    const/16 v1, 0x76

    .line 706
    .line 707
    sget-object v2, Lio/branch/referral/util/CurrencyType;->QAR:Lio/branch/referral/util/CurrencyType;

    .line 708
    .line 709
    aput-object v2, v0, v1

    .line 710
    .line 711
    const/16 v1, 0x77

    .line 712
    .line 713
    sget-object v2, Lio/branch/referral/util/CurrencyType;->RON:Lio/branch/referral/util/CurrencyType;

    .line 714
    .line 715
    aput-object v2, v0, v1

    .line 716
    .line 717
    const/16 v1, 0x78

    .line 718
    .line 719
    sget-object v2, Lio/branch/referral/util/CurrencyType;->RSD:Lio/branch/referral/util/CurrencyType;

    .line 720
    .line 721
    aput-object v2, v0, v1

    .line 722
    .line 723
    const/16 v1, 0x79

    .line 724
    .line 725
    sget-object v2, Lio/branch/referral/util/CurrencyType;->RUB:Lio/branch/referral/util/CurrencyType;

    .line 726
    .line 727
    aput-object v2, v0, v1

    .line 728
    .line 729
    const/16 v1, 0x7a

    .line 730
    .line 731
    sget-object v2, Lio/branch/referral/util/CurrencyType;->RWF:Lio/branch/referral/util/CurrencyType;

    .line 732
    .line 733
    aput-object v2, v0, v1

    .line 734
    .line 735
    const/16 v1, 0x7b

    .line 736
    .line 737
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SAR:Lio/branch/referral/util/CurrencyType;

    .line 738
    .line 739
    aput-object v2, v0, v1

    .line 740
    .line 741
    const/16 v1, 0x7c

    .line 742
    .line 743
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SBD:Lio/branch/referral/util/CurrencyType;

    .line 744
    .line 745
    aput-object v2, v0, v1

    .line 746
    .line 747
    const/16 v1, 0x7d

    .line 748
    .line 749
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SCR:Lio/branch/referral/util/CurrencyType;

    .line 750
    .line 751
    aput-object v2, v0, v1

    .line 752
    .line 753
    const/16 v1, 0x7e

    .line 754
    .line 755
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SDG:Lio/branch/referral/util/CurrencyType;

    .line 756
    .line 757
    aput-object v2, v0, v1

    .line 758
    .line 759
    const/16 v1, 0x7f

    .line 760
    .line 761
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SEK:Lio/branch/referral/util/CurrencyType;

    .line 762
    .line 763
    aput-object v2, v0, v1

    .line 764
    .line 765
    const/16 v1, 0x80

    .line 766
    .line 767
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SGD:Lio/branch/referral/util/CurrencyType;

    .line 768
    .line 769
    aput-object v2, v0, v1

    .line 770
    .line 771
    const/16 v1, 0x81

    .line 772
    .line 773
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SHP:Lio/branch/referral/util/CurrencyType;

    .line 774
    .line 775
    aput-object v2, v0, v1

    .line 776
    .line 777
    const/16 v1, 0x82

    .line 778
    .line 779
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SLL:Lio/branch/referral/util/CurrencyType;

    .line 780
    .line 781
    aput-object v2, v0, v1

    .line 782
    .line 783
    const/16 v1, 0x83

    .line 784
    .line 785
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SOS:Lio/branch/referral/util/CurrencyType;

    .line 786
    .line 787
    aput-object v2, v0, v1

    .line 788
    .line 789
    const/16 v1, 0x84

    .line 790
    .line 791
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SRD:Lio/branch/referral/util/CurrencyType;

    .line 792
    .line 793
    aput-object v2, v0, v1

    .line 794
    .line 795
    const/16 v1, 0x85

    .line 796
    .line 797
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SSP:Lio/branch/referral/util/CurrencyType;

    .line 798
    .line 799
    aput-object v2, v0, v1

    .line 800
    .line 801
    const/16 v1, 0x86

    .line 802
    .line 803
    sget-object v2, Lio/branch/referral/util/CurrencyType;->STD:Lio/branch/referral/util/CurrencyType;

    .line 804
    .line 805
    aput-object v2, v0, v1

    .line 806
    .line 807
    const/16 v1, 0x87

    .line 808
    .line 809
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SYP:Lio/branch/referral/util/CurrencyType;

    .line 810
    .line 811
    aput-object v2, v0, v1

    .line 812
    .line 813
    const/16 v1, 0x88

    .line 814
    .line 815
    sget-object v2, Lio/branch/referral/util/CurrencyType;->SZL:Lio/branch/referral/util/CurrencyType;

    .line 816
    .line 817
    aput-object v2, v0, v1

    .line 818
    .line 819
    const/16 v1, 0x89

    .line 820
    .line 821
    sget-object v2, Lio/branch/referral/util/CurrencyType;->THB:Lio/branch/referral/util/CurrencyType;

    .line 822
    .line 823
    aput-object v2, v0, v1

    .line 824
    .line 825
    const/16 v1, 0x8a

    .line 826
    .line 827
    sget-object v2, Lio/branch/referral/util/CurrencyType;->TJS:Lio/branch/referral/util/CurrencyType;

    .line 828
    .line 829
    aput-object v2, v0, v1

    .line 830
    .line 831
    const/16 v1, 0x8b

    .line 832
    .line 833
    sget-object v2, Lio/branch/referral/util/CurrencyType;->TMT:Lio/branch/referral/util/CurrencyType;

    .line 834
    .line 835
    aput-object v2, v0, v1

    .line 836
    .line 837
    const/16 v1, 0x8c

    .line 838
    .line 839
    sget-object v2, Lio/branch/referral/util/CurrencyType;->TND:Lio/branch/referral/util/CurrencyType;

    .line 840
    .line 841
    aput-object v2, v0, v1

    .line 842
    .line 843
    const/16 v1, 0x8d

    .line 844
    .line 845
    sget-object v2, Lio/branch/referral/util/CurrencyType;->TOP:Lio/branch/referral/util/CurrencyType;

    .line 846
    .line 847
    aput-object v2, v0, v1

    .line 848
    .line 849
    const/16 v1, 0x8e

    .line 850
    .line 851
    sget-object v2, Lio/branch/referral/util/CurrencyType;->TRY:Lio/branch/referral/util/CurrencyType;

    .line 852
    .line 853
    aput-object v2, v0, v1

    .line 854
    .line 855
    const/16 v1, 0x8f

    .line 856
    .line 857
    sget-object v2, Lio/branch/referral/util/CurrencyType;->TTD:Lio/branch/referral/util/CurrencyType;

    .line 858
    .line 859
    aput-object v2, v0, v1

    .line 860
    .line 861
    const/16 v1, 0x90

    .line 862
    .line 863
    sget-object v2, Lio/branch/referral/util/CurrencyType;->TWD:Lio/branch/referral/util/CurrencyType;

    .line 864
    .line 865
    aput-object v2, v0, v1

    .line 866
    .line 867
    const/16 v1, 0x91

    .line 868
    .line 869
    sget-object v2, Lio/branch/referral/util/CurrencyType;->TZS:Lio/branch/referral/util/CurrencyType;

    .line 870
    .line 871
    aput-object v2, v0, v1

    .line 872
    .line 873
    const/16 v1, 0x92

    .line 874
    .line 875
    sget-object v2, Lio/branch/referral/util/CurrencyType;->UAH:Lio/branch/referral/util/CurrencyType;

    .line 876
    .line 877
    aput-object v2, v0, v1

    .line 878
    .line 879
    const/16 v1, 0x93

    .line 880
    .line 881
    sget-object v2, Lio/branch/referral/util/CurrencyType;->UGX:Lio/branch/referral/util/CurrencyType;

    .line 882
    .line 883
    aput-object v2, v0, v1

    .line 884
    .line 885
    const/16 v1, 0x94

    .line 886
    .line 887
    sget-object v2, Lio/branch/referral/util/CurrencyType;->USD:Lio/branch/referral/util/CurrencyType;

    .line 888
    .line 889
    aput-object v2, v0, v1

    .line 890
    .line 891
    const/16 v1, 0x95

    .line 892
    .line 893
    sget-object v2, Lio/branch/referral/util/CurrencyType;->USN:Lio/branch/referral/util/CurrencyType;

    .line 894
    .line 895
    aput-object v2, v0, v1

    .line 896
    .line 897
    const/16 v1, 0x96

    .line 898
    .line 899
    sget-object v2, Lio/branch/referral/util/CurrencyType;->UYI:Lio/branch/referral/util/CurrencyType;

    .line 900
    .line 901
    aput-object v2, v0, v1

    .line 902
    .line 903
    const/16 v1, 0x97

    .line 904
    .line 905
    sget-object v2, Lio/branch/referral/util/CurrencyType;->UYU:Lio/branch/referral/util/CurrencyType;

    .line 906
    .line 907
    aput-object v2, v0, v1

    .line 908
    .line 909
    const/16 v1, 0x98

    .line 910
    .line 911
    sget-object v2, Lio/branch/referral/util/CurrencyType;->UZS:Lio/branch/referral/util/CurrencyType;

    .line 912
    .line 913
    aput-object v2, v0, v1

    .line 914
    .line 915
    const/16 v1, 0x99

    .line 916
    .line 917
    sget-object v2, Lio/branch/referral/util/CurrencyType;->VEF:Lio/branch/referral/util/CurrencyType;

    .line 918
    .line 919
    aput-object v2, v0, v1

    .line 920
    .line 921
    const/16 v1, 0x9a

    .line 922
    .line 923
    sget-object v2, Lio/branch/referral/util/CurrencyType;->VND:Lio/branch/referral/util/CurrencyType;

    .line 924
    .line 925
    aput-object v2, v0, v1

    .line 926
    .line 927
    const/16 v1, 0x9b

    .line 928
    .line 929
    sget-object v2, Lio/branch/referral/util/CurrencyType;->VUV:Lio/branch/referral/util/CurrencyType;

    .line 930
    .line 931
    aput-object v2, v0, v1

    .line 932
    .line 933
    const/16 v1, 0x9c

    .line 934
    .line 935
    sget-object v2, Lio/branch/referral/util/CurrencyType;->WST:Lio/branch/referral/util/CurrencyType;

    .line 936
    .line 937
    aput-object v2, v0, v1

    .line 938
    .line 939
    const/16 v1, 0x9d

    .line 940
    .line 941
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XAF:Lio/branch/referral/util/CurrencyType;

    .line 942
    .line 943
    aput-object v2, v0, v1

    .line 944
    .line 945
    const/16 v1, 0x9e

    .line 946
    .line 947
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XAG:Lio/branch/referral/util/CurrencyType;

    .line 948
    .line 949
    aput-object v2, v0, v1

    .line 950
    .line 951
    const/16 v1, 0x9f

    .line 952
    .line 953
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XAU:Lio/branch/referral/util/CurrencyType;

    .line 954
    .line 955
    aput-object v2, v0, v1

    .line 956
    .line 957
    const/16 v1, 0xa0

    .line 958
    .line 959
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XBA:Lio/branch/referral/util/CurrencyType;

    .line 960
    .line 961
    aput-object v2, v0, v1

    .line 962
    .line 963
    const/16 v1, 0xa1

    .line 964
    .line 965
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XBB:Lio/branch/referral/util/CurrencyType;

    .line 966
    .line 967
    aput-object v2, v0, v1

    .line 968
    .line 969
    const/16 v1, 0xa2

    .line 970
    .line 971
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XBC:Lio/branch/referral/util/CurrencyType;

    .line 972
    .line 973
    aput-object v2, v0, v1

    .line 974
    .line 975
    const/16 v1, 0xa3

    .line 976
    .line 977
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XBD:Lio/branch/referral/util/CurrencyType;

    .line 978
    .line 979
    aput-object v2, v0, v1

    .line 980
    .line 981
    const/16 v1, 0xa4

    .line 982
    .line 983
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XCD:Lio/branch/referral/util/CurrencyType;

    .line 984
    .line 985
    aput-object v2, v0, v1

    .line 986
    .line 987
    const/16 v1, 0xa5

    .line 988
    .line 989
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XDR:Lio/branch/referral/util/CurrencyType;

    .line 990
    .line 991
    aput-object v2, v0, v1

    .line 992
    .line 993
    const/16 v1, 0xa6

    .line 994
    .line 995
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XFU:Lio/branch/referral/util/CurrencyType;

    .line 996
    .line 997
    aput-object v2, v0, v1

    .line 998
    .line 999
    const/16 v1, 0xa7

    .line 1000
    .line 1001
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XOF:Lio/branch/referral/util/CurrencyType;

    .line 1002
    .line 1003
    aput-object v2, v0, v1

    .line 1004
    .line 1005
    const/16 v1, 0xa8

    .line 1006
    .line 1007
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XPD:Lio/branch/referral/util/CurrencyType;

    .line 1008
    .line 1009
    aput-object v2, v0, v1

    .line 1010
    .line 1011
    const/16 v1, 0xa9

    .line 1012
    .line 1013
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XPF:Lio/branch/referral/util/CurrencyType;

    .line 1014
    .line 1015
    aput-object v2, v0, v1

    .line 1016
    .line 1017
    const/16 v1, 0xaa

    .line 1018
    .line 1019
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XPT:Lio/branch/referral/util/CurrencyType;

    .line 1020
    .line 1021
    aput-object v2, v0, v1

    .line 1022
    .line 1023
    const/16 v1, 0xab

    .line 1024
    .line 1025
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XSU:Lio/branch/referral/util/CurrencyType;

    .line 1026
    .line 1027
    aput-object v2, v0, v1

    .line 1028
    .line 1029
    const/16 v1, 0xac

    .line 1030
    .line 1031
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XTS:Lio/branch/referral/util/CurrencyType;

    .line 1032
    .line 1033
    aput-object v2, v0, v1

    .line 1034
    .line 1035
    const/16 v1, 0xad

    .line 1036
    .line 1037
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XUA:Lio/branch/referral/util/CurrencyType;

    .line 1038
    .line 1039
    aput-object v2, v0, v1

    .line 1040
    .line 1041
    const/16 v1, 0xae

    .line 1042
    .line 1043
    sget-object v2, Lio/branch/referral/util/CurrencyType;->XXX:Lio/branch/referral/util/CurrencyType;

    .line 1044
    .line 1045
    aput-object v2, v0, v1

    .line 1046
    .line 1047
    const/16 v1, 0xaf

    .line 1048
    .line 1049
    sget-object v2, Lio/branch/referral/util/CurrencyType;->YER:Lio/branch/referral/util/CurrencyType;

    .line 1050
    .line 1051
    aput-object v2, v0, v1

    .line 1052
    .line 1053
    const/16 v1, 0xb0

    .line 1054
    .line 1055
    sget-object v2, Lio/branch/referral/util/CurrencyType;->ZAR:Lio/branch/referral/util/CurrencyType;

    .line 1056
    .line 1057
    aput-object v2, v0, v1

    .line 1058
    .line 1059
    const/16 v1, 0xb1

    .line 1060
    .line 1061
    sget-object v2, Lio/branch/referral/util/CurrencyType;->ZMW:Lio/branch/referral/util/CurrencyType;

    .line 1062
    .line 1063
    aput-object v2, v0, v1

    .line 1064
    .line 1065
    return-object v0
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
    .line 1600
    .line 1601
    .line 1602
    .line 1603
    .line 1604
    .line 1605
    .line 1606
    .line 1607
    .line 1608
    .line 1609
    .line 1610
    .line 1611
    .line 1612
    .line 1613
    .line 1614
    .line 1615
    .line 1616
    .line 1617
    .line 1618
    .line 1619
    .line 1620
    .line 1621
    .line 1622
    .line 1623
    .line 1624
    .line 1625
    .line 1626
    .line 1627
    .line 1628
    .line 1629
    .line 1630
    .line 1631
    .line 1632
    .line 1633
    .line 1634
    .line 1635
    .line 1636
    .line 1637
    .line 1638
    .line 1639
    .line 1640
    .line 1641
    .line 1642
    .line 1643
    .line 1644
    .line 1645
    .line 1646
    .line 1647
    .line 1648
    .line 1649
    .line 1650
    .line 1651
    .line 1652
    .line 1653
    .line 1654
    .line 1655
    .line 1656
    .line 1657
    .line 1658
    .line 1659
    .line 1660
    .line 1661
    .line 1662
    .line 1663
    .line 1664
    .line 1665
    .line 1666
    .line 1667
    .line 1668
    .line 1669
    .line 1670
    .line 1671
    .line 1672
    .line 1673
    .line 1674
    .line 1675
    .line 1676
    .line 1677
    .line 1678
    .line 1679
    .line 1680
    .line 1681
    .line 1682
    .line 1683
    .line 1684
    .line 1685
    .line 1686
    .line 1687
    .line 1688
    .line 1689
    .line 1690
    .line 1691
    .line 1692
    .line 1693
    .line 1694
    .line 1695
    .line 1696
    .line 1697
    .line 1698
    .line 1699
    .line 1700
    .line 1701
    .line 1702
    .line 1703
    .line 1704
    .line 1705
    .line 1706
    .line 1707
    .line 1708
    .line 1709
    .line 1710
    .line 1711
    .line 1712
    .line 1713
    .line 1714
    .line 1715
    .line 1716
    .line 1717
    .line 1718
    .line 1719
    .line 1720
    .line 1721
    .line 1722
    .line 1723
    .line 1724
    .line 1725
    .line 1726
    .line 1727
    .line 1728
    .line 1729
    .line 1730
    .line 1731
    .line 1732
    .line 1733
    .line 1734
    .line 1735
    .line 1736
    .line 1737
    .line 1738
    .line 1739
    .line 1740
    .line 1741
    .line 1742
    .line 1743
    .line 1744
    .line 1745
    .line 1746
    .line 1747
    .line 1748
    .line 1749
    .line 1750
    .line 1751
    .line 1752
    .line 1753
    .line 1754
    .line 1755
    .line 1756
    .line 1757
    .line 1758
    .line 1759
    .line 1760
    .line 1761
    .line 1762
    .line 1763
    .line 1764
    .line 1765
    .line 1766
    .line 1767
    .line 1768
    .line 1769
    .line 1770
    .line 1771
    .line 1772
    .line 1773
    .line 1774
    .line 1775
    .line 1776
    .line 1777
    .line 1778
    .line 1779
    .line 1780
    .line 1781
    .line 1782
    .line 1783
    .line 1784
    .line 1785
    .line 1786
    .line 1787
    .line 1788
    .line 1789
    .line 1790
    .line 1791
    .line 1792
    .line 1793
    .line 1794
    .line 1795
    .line 1796
    .line 1797
    .line 1798
    .line 1799
    .line 1800
    .line 1801
    .line 1802
    .line 1803
    .line 1804
    .line 1805
    .line 1806
    .line 1807
    .line 1808
    .line 1809
    .line 1810
    .line 1811
    .line 1812
    .line 1813
    .line 1814
    .line 1815
    .line 1816
    .line 1817
    .line 1818
    .line 1819
    .line 1820
    .line 1821
    .line 1822
    .line 1823
    .line 1824
    .line 1825
    .line 1826
    .line 1827
    .line 1828
    .line 1829
    .line 1830
    .line 1831
    .line 1832
    .line 1833
    .line 1834
    .line 1835
    .line 1836
    .line 1837
    .line 1838
    .line 1839
    .line 1840
    .line 1841
    .line 1842
    .line 1843
    .line 1844
    .line 1845
    .line 1846
    .line 1847
    .line 1848
    .line 1849
    .line 1850
    .line 1851
    .line 1852
    .line 1853
    .line 1854
    .line 1855
    .line 1856
    .line 1857
    .line 1858
    .line 1859
    .line 1860
    .line 1861
    .line 1862
    .line 1863
    .line 1864
    .line 1865
    .line 1866
    .line 1867
    .line 1868
    .line 1869
    .line 1870
    .line 1871
    .line 1872
    .line 1873
    .line 1874
    .line 1875
    .line 1876
    .line 1877
    .line 1878
    .line 1879
    .line 1880
    .line 1881
    .line 1882
    .line 1883
    .line 1884
    .line 1885
    .line 1886
    .line 1887
    .line 1888
    .line 1889
    .line 1890
    .line 1891
    .line 1892
    .line 1893
    .line 1894
    .line 1895
    .line 1896
    .line 1897
    .line 1898
    .line 1899
    .line 1900
    .line 1901
    .line 1902
    .line 1903
    .line 1904
    .line 1905
    .line 1906
    .line 1907
    .line 1908
    .line 1909
    .line 1910
    .line 1911
    .line 1912
    .line 1913
    .line 1914
    .line 1915
    .line 1916
    .line 1917
    .line 1918
    .line 1919
    .line 1920
    .line 1921
    .line 1922
    .line 1923
    .line 1924
    .line 1925
    .line 1926
    .line 1927
    .line 1928
    .line 1929
    .line 1930
    .line 1931
    .line 1932
    .line 1933
    .line 1934
    .line 1935
    .line 1936
    .line 1937
    .line 1938
    .line 1939
    .line 1940
    .line 1941
    .line 1942
    .line 1943
    .line 1944
    .line 1945
    .line 1946
    .line 1947
    .line 1948
    .line 1949
    .line 1950
    .line 1951
    .line 1952
    .line 1953
    .line 1954
    .line 1955
    .line 1956
    .line 1957
    .line 1958
    .line 1959
    .line 1960
    .line 1961
    .line 1962
    .line 1963
    .line 1964
    .line 1965
    .line 1966
    .line 1967
    .line 1968
    .line 1969
    .line 1970
    .line 1971
    .line 1972
    .line 1973
    .line 1974
    .line 1975
    .line 1976
    .line 1977
    .line 1978
    .line 1979
    .line 1980
    .line 1981
    .line 1982
    .line 1983
    .line 1984
    .line 1985
    .line 1986
    .line 1987
    .line 1988
    .line 1989
    .line 1990
    .line 1991
    .line 1992
    .line 1993
    .line 1994
    .line 1995
    .line 1996
    .line 1997
    .line 1998
    .line 1999
    .line 2000
    .line 2001
    .line 2002
    .line 2003
    .line 2004
    .line 2005
    .line 2006
    .line 2007
    .line 2008
    .line 2009
    .line 2010
    .line 2011
    .line 2012
    .line 2013
    .line 2014
    .line 2015
    .line 2016
    .line 2017
    .line 2018
    .line 2019
    .line 2020
    .line 2021
    .line 2022
    .line 2023
    .line 2024
    .line 2025
    .line 2026
    .line 2027
    .line 2028
    .line 2029
    .line 2030
    .line 2031
    .line 2032
    .line 2033
    .line 2034
    .line 2035
    .line 2036
    .line 2037
    .line 2038
    .line 2039
    .line 2040
    .line 2041
    .line 2042
    .line 2043
    .line 2044
    .line 2045
    .line 2046
    .line 2047
    .line 2048
    .line 2049
    .line 2050
    .line 2051
    .line 2052
    .line 2053
    .line 2054
    .line 2055
    .line 2056
    .line 2057
    .line 2058
    .line 2059
    .line 2060
    .line 2061
    .line 2062
    .line 2063
    .line 2064
    .line 2065
    .line 2066
    .line 2067
    .line 2068
    .line 2069
    .line 2070
    .line 2071
    .line 2072
    .line 2073
    .line 2074
    .line 2075
    .line 2076
    .line 2077
    .line 2078
    .line 2079
    .line 2080
    .line 2081
    .line 2082
    .line 2083
    .line 2084
    .line 2085
    .line 2086
    .line 2087
    .line 2088
    .line 2089
    .line 2090
    .line 2091
    .line 2092
    .line 2093
    .line 2094
    .line 2095
    .line 2096
    .line 2097
    .line 2098
    .line 2099
    .line 2100
    .line 2101
    .line 2102
    .line 2103
    .line 2104
    .line 2105
    .line 2106
    .line 2107
    .line 2108
    .line 2109
    .line 2110
    .line 2111
    .line 2112
    .line 2113
    .line 2114
    .line 2115
    .line 2116
    .line 2117
    .line 2118
    .line 2119
    .line 2120
    .line 2121
    .line 2122
    .line 2123
    .line 2124
    .line 2125
    .line 2126
    .line 2127
    .line 2128
    .line 2129
    .line 2130
    .line 2131
    .line 2132
    .line 2133
    .line 2134
    .line 2135
    .line 2136
    .line 2137
    .line 2138
    .line 2139
    .line 2140
    .line 2141
    .line 2142
    .line 2143
    .line 2144
    .line 2145
    .line 2146
    .line 2147
    .line 2148
    .line 2149
    .line 2150
    .line 2151
    .line 2152
    .line 2153
    .line 2154
    .line 2155
    .line 2156
    .line 2157
    .line 2158
    .line 2159
    .line 2160
    .line 2161
    .line 2162
    .line 2163
    .line 2164
    .line 2165
    .line 2166
    .line 2167
    .line 2168
    .line 2169
    .line 2170
    .line 2171
    .line 2172
    .line 2173
    .line 2174
    .line 2175
    .line 2176
    .line 2177
    .line 2178
    .line 2179
    .line 2180
    .line 2181
    .line 2182
    .line 2183
    .line 2184
    .line 2185
    .line 2186
    .line 2187
    .line 2188
    .line 2189
    .line 2190
    .line 2191
    .line 2192
    .line 2193
    .line 2194
    .line 2195
    .line 2196
    .line 2197
    .line 2198
    .line 2199
    .line 2200
    .line 2201
    .line 2202
    .line 2203
    .line 2204
    .line 2205
    .line 2206
    .line 2207
    .line 2208
    .line 2209
    .line 2210
    .line 2211
    .line 2212
    .line 2213
    .line 2214
    .line 2215
    .line 2216
    .line 2217
    .line 2218
    .line 2219
    .line 2220
    .line 2221
    .line 2222
    .line 2223
    .line 2224
    .line 2225
    .line 2226
    .line 2227
    .line 2228
    .line 2229
    .line 2230
    .line 2231
    .line 2232
    .line 2233
    .line 2234
    .line 2235
    .line 2236
    .line 2237
    .line 2238
    .line 2239
    .line 2240
    .line 2241
    .line 2242
    .line 2243
    .line 2244
    .line 2245
    .line 2246
    .line 2247
    .line 2248
    .line 2249
    .line 2250
    .line 2251
    .line 2252
    .line 2253
    .line 2254
    .line 2255
    .line 2256
    .line 2257
    .line 2258
    .line 2259
    .line 2260
    .line 2261
    .line 2262
    .line 2263
    .line 2264
    .line 2265
    .line 2266
    .line 2267
    .line 2268
    .line 2269
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2
    .line 3
    const-string v1, "AED"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lio/branch/referral/util/CurrencyType;->AED:Lio/branch/referral/util/CurrencyType;

    .line 10
    .line 11
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 12
    .line 13
    const-string v1, "AFN"

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lio/branch/referral/util/CurrencyType;->AFN:Lio/branch/referral/util/CurrencyType;

    .line 20
    .line 21
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 22
    .line 23
    const-string v1, "ALL"

    .line 24
    .line 25
    const/4 v2, 0x2

    .line 26
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lio/branch/referral/util/CurrencyType;->ALL:Lio/branch/referral/util/CurrencyType;

    .line 30
    .line 31
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 32
    .line 33
    const-string v1, "AMD"

    .line 34
    .line 35
    const/4 v2, 0x3

    .line 36
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lio/branch/referral/util/CurrencyType;->AMD:Lio/branch/referral/util/CurrencyType;

    .line 40
    .line 41
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 42
    .line 43
    const-string v1, "ANG"

    .line 44
    .line 45
    const/4 v2, 0x4

    .line 46
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 47
    .line 48
    .line 49
    sput-object v0, Lio/branch/referral/util/CurrencyType;->ANG:Lio/branch/referral/util/CurrencyType;

    .line 50
    .line 51
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 52
    .line 53
    const-string v1, "AOA"

    .line 54
    .line 55
    const/4 v2, 0x5

    .line 56
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 57
    .line 58
    .line 59
    sput-object v0, Lio/branch/referral/util/CurrencyType;->AOA:Lio/branch/referral/util/CurrencyType;

    .line 60
    .line 61
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 62
    .line 63
    const-string v1, "ARS"

    .line 64
    .line 65
    const/4 v2, 0x6

    .line 66
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 67
    .line 68
    .line 69
    sput-object v0, Lio/branch/referral/util/CurrencyType;->ARS:Lio/branch/referral/util/CurrencyType;

    .line 70
    .line 71
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 72
    .line 73
    const-string v1, "AUD"

    .line 74
    .line 75
    const/4 v2, 0x7

    .line 76
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 77
    .line 78
    .line 79
    sput-object v0, Lio/branch/referral/util/CurrencyType;->AUD:Lio/branch/referral/util/CurrencyType;

    .line 80
    .line 81
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 82
    .line 83
    const-string v1, "AWG"

    .line 84
    .line 85
    const/16 v2, 0x8

    .line 86
    .line 87
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 88
    .line 89
    .line 90
    sput-object v0, Lio/branch/referral/util/CurrencyType;->AWG:Lio/branch/referral/util/CurrencyType;

    .line 91
    .line 92
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 93
    .line 94
    const-string v1, "AZN"

    .line 95
    .line 96
    const/16 v2, 0x9

    .line 97
    .line 98
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 99
    .line 100
    .line 101
    sput-object v0, Lio/branch/referral/util/CurrencyType;->AZN:Lio/branch/referral/util/CurrencyType;

    .line 102
    .line 103
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 104
    .line 105
    const-string v1, "BAM"

    .line 106
    .line 107
    const/16 v2, 0xa

    .line 108
    .line 109
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 110
    .line 111
    .line 112
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BAM:Lio/branch/referral/util/CurrencyType;

    .line 113
    .line 114
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 115
    .line 116
    const-string v1, "BBD"

    .line 117
    .line 118
    const/16 v2, 0xb

    .line 119
    .line 120
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 121
    .line 122
    .line 123
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BBD:Lio/branch/referral/util/CurrencyType;

    .line 124
    .line 125
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 126
    .line 127
    const-string v1, "BDT"

    .line 128
    .line 129
    const/16 v2, 0xc

    .line 130
    .line 131
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 132
    .line 133
    .line 134
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BDT:Lio/branch/referral/util/CurrencyType;

    .line 135
    .line 136
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 137
    .line 138
    const-string v1, "BGN"

    .line 139
    .line 140
    const/16 v2, 0xd

    .line 141
    .line 142
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 143
    .line 144
    .line 145
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BGN:Lio/branch/referral/util/CurrencyType;

    .line 146
    .line 147
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 148
    .line 149
    const-string v1, "BHD"

    .line 150
    .line 151
    const/16 v2, 0xe

    .line 152
    .line 153
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 154
    .line 155
    .line 156
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BHD:Lio/branch/referral/util/CurrencyType;

    .line 157
    .line 158
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 159
    .line 160
    const-string v1, "BIF"

    .line 161
    .line 162
    const/16 v2, 0xf

    .line 163
    .line 164
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 165
    .line 166
    .line 167
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BIF:Lio/branch/referral/util/CurrencyType;

    .line 168
    .line 169
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 170
    .line 171
    const-string v1, "BMD"

    .line 172
    .line 173
    const/16 v2, 0x10

    .line 174
    .line 175
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 176
    .line 177
    .line 178
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BMD:Lio/branch/referral/util/CurrencyType;

    .line 179
    .line 180
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 181
    .line 182
    const-string v1, "BND"

    .line 183
    .line 184
    const/16 v2, 0x11

    .line 185
    .line 186
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 187
    .line 188
    .line 189
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BND:Lio/branch/referral/util/CurrencyType;

    .line 190
    .line 191
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 192
    .line 193
    const-string v1, "BOB"

    .line 194
    .line 195
    const/16 v2, 0x12

    .line 196
    .line 197
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 198
    .line 199
    .line 200
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BOB:Lio/branch/referral/util/CurrencyType;

    .line 201
    .line 202
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 203
    .line 204
    const-string v1, "BOV"

    .line 205
    .line 206
    const/16 v2, 0x13

    .line 207
    .line 208
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 209
    .line 210
    .line 211
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BOV:Lio/branch/referral/util/CurrencyType;

    .line 212
    .line 213
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 214
    .line 215
    const-string v1, "BRL"

    .line 216
    .line 217
    const/16 v2, 0x14

    .line 218
    .line 219
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 220
    .line 221
    .line 222
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BRL:Lio/branch/referral/util/CurrencyType;

    .line 223
    .line 224
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 225
    .line 226
    const-string v1, "BSD"

    .line 227
    .line 228
    const/16 v2, 0x15

    .line 229
    .line 230
    invoke-direct {v0, v1, v2, v1}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 231
    .line 232
    .line 233
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BSD:Lio/branch/referral/util/CurrencyType;

    .line 234
    .line 235
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 236
    .line 237
    const/16 v1, 0x16

    .line 238
    .line 239
    const-string v2, "BTN"

    .line 240
    .line 241
    const-string v3, "BTN"

    .line 242
    .line 243
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 244
    .line 245
    .line 246
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BTN:Lio/branch/referral/util/CurrencyType;

    .line 247
    .line 248
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 249
    .line 250
    const/16 v1, 0x17

    .line 251
    .line 252
    const-string v2, "BWP"

    .line 253
    .line 254
    const-string v3, "BWP"

    .line 255
    .line 256
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 257
    .line 258
    .line 259
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BWP:Lio/branch/referral/util/CurrencyType;

    .line 260
    .line 261
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 262
    .line 263
    const/16 v1, 0x18

    .line 264
    .line 265
    const-string v2, "BYN"

    .line 266
    .line 267
    const-string v3, "BYN"

    .line 268
    .line 269
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 270
    .line 271
    .line 272
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BYN:Lio/branch/referral/util/CurrencyType;

    .line 273
    .line 274
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 275
    .line 276
    const/16 v1, 0x19

    .line 277
    .line 278
    const-string v2, "BYR"

    .line 279
    .line 280
    const-string v3, "BYR"

    .line 281
    .line 282
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 283
    .line 284
    .line 285
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BYR:Lio/branch/referral/util/CurrencyType;

    .line 286
    .line 287
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 288
    .line 289
    const/16 v1, 0x1a

    .line 290
    .line 291
    const-string v2, "BZD"

    .line 292
    .line 293
    const-string v3, "BZD"

    .line 294
    .line 295
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 296
    .line 297
    .line 298
    sput-object v0, Lio/branch/referral/util/CurrencyType;->BZD:Lio/branch/referral/util/CurrencyType;

    .line 299
    .line 300
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 301
    .line 302
    const/16 v1, 0x1b

    .line 303
    .line 304
    const-string v2, "CAD"

    .line 305
    .line 306
    const-string v3, "CAD"

    .line 307
    .line 308
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 309
    .line 310
    .line 311
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CAD:Lio/branch/referral/util/CurrencyType;

    .line 312
    .line 313
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 314
    .line 315
    const/16 v1, 0x1c

    .line 316
    .line 317
    const-string v2, "CDF"

    .line 318
    .line 319
    const-string v3, "CDF"

    .line 320
    .line 321
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 322
    .line 323
    .line 324
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CDF:Lio/branch/referral/util/CurrencyType;

    .line 325
    .line 326
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 327
    .line 328
    const/16 v1, 0x1d

    .line 329
    .line 330
    const-string v2, "CHE"

    .line 331
    .line 332
    const-string v3, "CHE"

    .line 333
    .line 334
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 335
    .line 336
    .line 337
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CHE:Lio/branch/referral/util/CurrencyType;

    .line 338
    .line 339
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 340
    .line 341
    const/16 v1, 0x1e

    .line 342
    .line 343
    const-string v2, "CHF"

    .line 344
    .line 345
    const-string v3, "CHF"

    .line 346
    .line 347
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 348
    .line 349
    .line 350
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CHF:Lio/branch/referral/util/CurrencyType;

    .line 351
    .line 352
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 353
    .line 354
    const/16 v1, 0x1f

    .line 355
    .line 356
    const-string v2, "CHW"

    .line 357
    .line 358
    const-string v3, "CHW"

    .line 359
    .line 360
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 361
    .line 362
    .line 363
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CHW:Lio/branch/referral/util/CurrencyType;

    .line 364
    .line 365
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 366
    .line 367
    const/16 v1, 0x20

    .line 368
    .line 369
    const-string v2, "CLF"

    .line 370
    .line 371
    const-string v3, "CLF"

    .line 372
    .line 373
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 374
    .line 375
    .line 376
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CLF:Lio/branch/referral/util/CurrencyType;

    .line 377
    .line 378
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 379
    .line 380
    const/16 v1, 0x21

    .line 381
    .line 382
    const-string v2, "CLP"

    .line 383
    .line 384
    const-string v3, "CLP"

    .line 385
    .line 386
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 387
    .line 388
    .line 389
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CLP:Lio/branch/referral/util/CurrencyType;

    .line 390
    .line 391
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 392
    .line 393
    const/16 v1, 0x22

    .line 394
    .line 395
    const-string v2, "CNY"

    .line 396
    .line 397
    const-string v3, "CNY"

    .line 398
    .line 399
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 400
    .line 401
    .line 402
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CNY:Lio/branch/referral/util/CurrencyType;

    .line 403
    .line 404
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 405
    .line 406
    const/16 v1, 0x23

    .line 407
    .line 408
    const-string v2, "COP"

    .line 409
    .line 410
    const-string v3, "COP"

    .line 411
    .line 412
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 413
    .line 414
    .line 415
    sput-object v0, Lio/branch/referral/util/CurrencyType;->COP:Lio/branch/referral/util/CurrencyType;

    .line 416
    .line 417
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 418
    .line 419
    const/16 v1, 0x24

    .line 420
    .line 421
    const-string v2, "COU"

    .line 422
    .line 423
    const-string v3, "COU"

    .line 424
    .line 425
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 426
    .line 427
    .line 428
    sput-object v0, Lio/branch/referral/util/CurrencyType;->COU:Lio/branch/referral/util/CurrencyType;

    .line 429
    .line 430
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 431
    .line 432
    const/16 v1, 0x25

    .line 433
    .line 434
    const-string v2, "CRC"

    .line 435
    .line 436
    const-string v3, "CRC"

    .line 437
    .line 438
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 439
    .line 440
    .line 441
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CRC:Lio/branch/referral/util/CurrencyType;

    .line 442
    .line 443
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 444
    .line 445
    const/16 v1, 0x26

    .line 446
    .line 447
    const-string v2, "CUC"

    .line 448
    .line 449
    const-string v3, "CUC"

    .line 450
    .line 451
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 452
    .line 453
    .line 454
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CUC:Lio/branch/referral/util/CurrencyType;

    .line 455
    .line 456
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 457
    .line 458
    const/16 v1, 0x27

    .line 459
    .line 460
    const-string v2, "CUP"

    .line 461
    .line 462
    const-string v3, "CUP"

    .line 463
    .line 464
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 465
    .line 466
    .line 467
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CUP:Lio/branch/referral/util/CurrencyType;

    .line 468
    .line 469
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 470
    .line 471
    const/16 v1, 0x28

    .line 472
    .line 473
    const-string v2, "CVE"

    .line 474
    .line 475
    const-string v3, "CVE"

    .line 476
    .line 477
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 478
    .line 479
    .line 480
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CVE:Lio/branch/referral/util/CurrencyType;

    .line 481
    .line 482
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 483
    .line 484
    const/16 v1, 0x29

    .line 485
    .line 486
    const-string v2, "CZK"

    .line 487
    .line 488
    const-string v3, "CZK"

    .line 489
    .line 490
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 491
    .line 492
    .line 493
    sput-object v0, Lio/branch/referral/util/CurrencyType;->CZK:Lio/branch/referral/util/CurrencyType;

    .line 494
    .line 495
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 496
    .line 497
    const/16 v1, 0x2a

    .line 498
    .line 499
    const-string v2, "DJF"

    .line 500
    .line 501
    const-string v3, "DJF"

    .line 502
    .line 503
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 504
    .line 505
    .line 506
    sput-object v0, Lio/branch/referral/util/CurrencyType;->DJF:Lio/branch/referral/util/CurrencyType;

    .line 507
    .line 508
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 509
    .line 510
    const/16 v1, 0x2b

    .line 511
    .line 512
    const-string v2, "DKK"

    .line 513
    .line 514
    const-string v3, "DKK"

    .line 515
    .line 516
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 517
    .line 518
    .line 519
    sput-object v0, Lio/branch/referral/util/CurrencyType;->DKK:Lio/branch/referral/util/CurrencyType;

    .line 520
    .line 521
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 522
    .line 523
    const/16 v1, 0x2c

    .line 524
    .line 525
    const-string v2, "DOP"

    .line 526
    .line 527
    const-string v3, "DOP"

    .line 528
    .line 529
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 530
    .line 531
    .line 532
    sput-object v0, Lio/branch/referral/util/CurrencyType;->DOP:Lio/branch/referral/util/CurrencyType;

    .line 533
    .line 534
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 535
    .line 536
    const/16 v1, 0x2d

    .line 537
    .line 538
    const-string v2, "DZD"

    .line 539
    .line 540
    const-string v3, "DZD"

    .line 541
    .line 542
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 543
    .line 544
    .line 545
    sput-object v0, Lio/branch/referral/util/CurrencyType;->DZD:Lio/branch/referral/util/CurrencyType;

    .line 546
    .line 547
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 548
    .line 549
    const/16 v1, 0x2e

    .line 550
    .line 551
    const-string v2, "EGP"

    .line 552
    .line 553
    const-string v3, "EGP"

    .line 554
    .line 555
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 556
    .line 557
    .line 558
    sput-object v0, Lio/branch/referral/util/CurrencyType;->EGP:Lio/branch/referral/util/CurrencyType;

    .line 559
    .line 560
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 561
    .line 562
    const/16 v1, 0x2f

    .line 563
    .line 564
    const-string v2, "ERN"

    .line 565
    .line 566
    const-string v3, "ERN"

    .line 567
    .line 568
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 569
    .line 570
    .line 571
    sput-object v0, Lio/branch/referral/util/CurrencyType;->ERN:Lio/branch/referral/util/CurrencyType;

    .line 572
    .line 573
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 574
    .line 575
    const/16 v1, 0x30

    .line 576
    .line 577
    const-string v2, "ETB"

    .line 578
    .line 579
    const-string v3, "ETB"

    .line 580
    .line 581
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 582
    .line 583
    .line 584
    sput-object v0, Lio/branch/referral/util/CurrencyType;->ETB:Lio/branch/referral/util/CurrencyType;

    .line 585
    .line 586
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 587
    .line 588
    const/16 v1, 0x31

    .line 589
    .line 590
    const-string v2, "EUR"

    .line 591
    .line 592
    const-string v3, "EUR"

    .line 593
    .line 594
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 595
    .line 596
    .line 597
    sput-object v0, Lio/branch/referral/util/CurrencyType;->EUR:Lio/branch/referral/util/CurrencyType;

    .line 598
    .line 599
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 600
    .line 601
    const/16 v1, 0x32

    .line 602
    .line 603
    const-string v2, "FJD"

    .line 604
    .line 605
    const-string v3, "FJD"

    .line 606
    .line 607
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 608
    .line 609
    .line 610
    sput-object v0, Lio/branch/referral/util/CurrencyType;->FJD:Lio/branch/referral/util/CurrencyType;

    .line 611
    .line 612
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 613
    .line 614
    const/16 v1, 0x33

    .line 615
    .line 616
    const-string v2, "FKP"

    .line 617
    .line 618
    const-string v3, "FKP"

    .line 619
    .line 620
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 621
    .line 622
    .line 623
    sput-object v0, Lio/branch/referral/util/CurrencyType;->FKP:Lio/branch/referral/util/CurrencyType;

    .line 624
    .line 625
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 626
    .line 627
    const/16 v1, 0x34

    .line 628
    .line 629
    const-string v2, "GBP"

    .line 630
    .line 631
    const-string v3, "GBP"

    .line 632
    .line 633
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 634
    .line 635
    .line 636
    sput-object v0, Lio/branch/referral/util/CurrencyType;->GBP:Lio/branch/referral/util/CurrencyType;

    .line 637
    .line 638
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 639
    .line 640
    const/16 v1, 0x35

    .line 641
    .line 642
    const-string v2, "GEL"

    .line 643
    .line 644
    const-string v3, "GEL"

    .line 645
    .line 646
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 647
    .line 648
    .line 649
    sput-object v0, Lio/branch/referral/util/CurrencyType;->GEL:Lio/branch/referral/util/CurrencyType;

    .line 650
    .line 651
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 652
    .line 653
    const/16 v1, 0x36

    .line 654
    .line 655
    const-string v2, "GHS"

    .line 656
    .line 657
    const-string v3, "GHS"

    .line 658
    .line 659
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 660
    .line 661
    .line 662
    sput-object v0, Lio/branch/referral/util/CurrencyType;->GHS:Lio/branch/referral/util/CurrencyType;

    .line 663
    .line 664
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 665
    .line 666
    const/16 v1, 0x37

    .line 667
    .line 668
    const-string v2, "GIP"

    .line 669
    .line 670
    const-string v3, "GIP"

    .line 671
    .line 672
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 673
    .line 674
    .line 675
    sput-object v0, Lio/branch/referral/util/CurrencyType;->GIP:Lio/branch/referral/util/CurrencyType;

    .line 676
    .line 677
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 678
    .line 679
    const/16 v1, 0x38

    .line 680
    .line 681
    const-string v2, "GMD"

    .line 682
    .line 683
    const-string v3, "GMD"

    .line 684
    .line 685
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 686
    .line 687
    .line 688
    sput-object v0, Lio/branch/referral/util/CurrencyType;->GMD:Lio/branch/referral/util/CurrencyType;

    .line 689
    .line 690
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 691
    .line 692
    const/16 v1, 0x39

    .line 693
    .line 694
    const-string v2, "GNF"

    .line 695
    .line 696
    const-string v3, "GNF"

    .line 697
    .line 698
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 699
    .line 700
    .line 701
    sput-object v0, Lio/branch/referral/util/CurrencyType;->GNF:Lio/branch/referral/util/CurrencyType;

    .line 702
    .line 703
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 704
    .line 705
    const/16 v1, 0x3a

    .line 706
    .line 707
    const-string v2, "GTQ"

    .line 708
    .line 709
    const-string v3, "GTQ"

    .line 710
    .line 711
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 712
    .line 713
    .line 714
    sput-object v0, Lio/branch/referral/util/CurrencyType;->GTQ:Lio/branch/referral/util/CurrencyType;

    .line 715
    .line 716
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 717
    .line 718
    const/16 v1, 0x3b

    .line 719
    .line 720
    const-string v2, "GYD"

    .line 721
    .line 722
    const-string v3, "GYD"

    .line 723
    .line 724
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 725
    .line 726
    .line 727
    sput-object v0, Lio/branch/referral/util/CurrencyType;->GYD:Lio/branch/referral/util/CurrencyType;

    .line 728
    .line 729
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 730
    .line 731
    const/16 v1, 0x3c

    .line 732
    .line 733
    const-string v2, "HKD"

    .line 734
    .line 735
    const-string v3, "HKD"

    .line 736
    .line 737
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 738
    .line 739
    .line 740
    sput-object v0, Lio/branch/referral/util/CurrencyType;->HKD:Lio/branch/referral/util/CurrencyType;

    .line 741
    .line 742
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 743
    .line 744
    const/16 v1, 0x3d

    .line 745
    .line 746
    const-string v2, "HNL"

    .line 747
    .line 748
    const-string v3, "HNL"

    .line 749
    .line 750
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 751
    .line 752
    .line 753
    sput-object v0, Lio/branch/referral/util/CurrencyType;->HNL:Lio/branch/referral/util/CurrencyType;

    .line 754
    .line 755
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 756
    .line 757
    const/16 v1, 0x3e

    .line 758
    .line 759
    const-string v2, "HRK"

    .line 760
    .line 761
    const-string v3, "HRK"

    .line 762
    .line 763
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 764
    .line 765
    .line 766
    sput-object v0, Lio/branch/referral/util/CurrencyType;->HRK:Lio/branch/referral/util/CurrencyType;

    .line 767
    .line 768
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 769
    .line 770
    const/16 v1, 0x3f

    .line 771
    .line 772
    const-string v2, "HTG"

    .line 773
    .line 774
    const-string v3, "HTG"

    .line 775
    .line 776
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 777
    .line 778
    .line 779
    sput-object v0, Lio/branch/referral/util/CurrencyType;->HTG:Lio/branch/referral/util/CurrencyType;

    .line 780
    .line 781
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 782
    .line 783
    const/16 v1, 0x40

    .line 784
    .line 785
    const-string v2, "HUF"

    .line 786
    .line 787
    const-string v3, "HUF"

    .line 788
    .line 789
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 790
    .line 791
    .line 792
    sput-object v0, Lio/branch/referral/util/CurrencyType;->HUF:Lio/branch/referral/util/CurrencyType;

    .line 793
    .line 794
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 795
    .line 796
    const/16 v1, 0x41

    .line 797
    .line 798
    const-string v2, "IDR"

    .line 799
    .line 800
    const-string v3, "IDR"

    .line 801
    .line 802
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 803
    .line 804
    .line 805
    sput-object v0, Lio/branch/referral/util/CurrencyType;->IDR:Lio/branch/referral/util/CurrencyType;

    .line 806
    .line 807
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 808
    .line 809
    const/16 v1, 0x42

    .line 810
    .line 811
    const-string v2, "ILS"

    .line 812
    .line 813
    const-string v3, "ILS"

    .line 814
    .line 815
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 816
    .line 817
    .line 818
    sput-object v0, Lio/branch/referral/util/CurrencyType;->ILS:Lio/branch/referral/util/CurrencyType;

    .line 819
    .line 820
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 821
    .line 822
    const/16 v1, 0x43

    .line 823
    .line 824
    const-string v2, "INR"

    .line 825
    .line 826
    const-string v3, "INR"

    .line 827
    .line 828
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 829
    .line 830
    .line 831
    sput-object v0, Lio/branch/referral/util/CurrencyType;->INR:Lio/branch/referral/util/CurrencyType;

    .line 832
    .line 833
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 834
    .line 835
    const/16 v1, 0x44

    .line 836
    .line 837
    const-string v2, "IQD"

    .line 838
    .line 839
    const-string v3, "IQD"

    .line 840
    .line 841
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 842
    .line 843
    .line 844
    sput-object v0, Lio/branch/referral/util/CurrencyType;->IQD:Lio/branch/referral/util/CurrencyType;

    .line 845
    .line 846
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 847
    .line 848
    const/16 v1, 0x45

    .line 849
    .line 850
    const-string v2, "IRR"

    .line 851
    .line 852
    const-string v3, "IRR"

    .line 853
    .line 854
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 855
    .line 856
    .line 857
    sput-object v0, Lio/branch/referral/util/CurrencyType;->IRR:Lio/branch/referral/util/CurrencyType;

    .line 858
    .line 859
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 860
    .line 861
    const/16 v1, 0x46

    .line 862
    .line 863
    const-string v2, "ISK"

    .line 864
    .line 865
    const-string v3, "ISK"

    .line 866
    .line 867
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 868
    .line 869
    .line 870
    sput-object v0, Lio/branch/referral/util/CurrencyType;->ISK:Lio/branch/referral/util/CurrencyType;

    .line 871
    .line 872
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 873
    .line 874
    const/16 v1, 0x47

    .line 875
    .line 876
    const-string v2, "JMD"

    .line 877
    .line 878
    const-string v3, "JMD"

    .line 879
    .line 880
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 881
    .line 882
    .line 883
    sput-object v0, Lio/branch/referral/util/CurrencyType;->JMD:Lio/branch/referral/util/CurrencyType;

    .line 884
    .line 885
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 886
    .line 887
    const/16 v1, 0x48

    .line 888
    .line 889
    const-string v2, "JOD"

    .line 890
    .line 891
    const-string v3, "JOD"

    .line 892
    .line 893
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 894
    .line 895
    .line 896
    sput-object v0, Lio/branch/referral/util/CurrencyType;->JOD:Lio/branch/referral/util/CurrencyType;

    .line 897
    .line 898
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 899
    .line 900
    const/16 v1, 0x49

    .line 901
    .line 902
    const-string v2, "JPY"

    .line 903
    .line 904
    const-string v3, "JPY"

    .line 905
    .line 906
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 907
    .line 908
    .line 909
    sput-object v0, Lio/branch/referral/util/CurrencyType;->JPY:Lio/branch/referral/util/CurrencyType;

    .line 910
    .line 911
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 912
    .line 913
    const/16 v1, 0x4a

    .line 914
    .line 915
    const-string v2, "KES"

    .line 916
    .line 917
    const-string v3, "KES"

    .line 918
    .line 919
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 920
    .line 921
    .line 922
    sput-object v0, Lio/branch/referral/util/CurrencyType;->KES:Lio/branch/referral/util/CurrencyType;

    .line 923
    .line 924
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 925
    .line 926
    const/16 v1, 0x4b

    .line 927
    .line 928
    const-string v2, "KGS"

    .line 929
    .line 930
    const-string v3, "KGS"

    .line 931
    .line 932
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 933
    .line 934
    .line 935
    sput-object v0, Lio/branch/referral/util/CurrencyType;->KGS:Lio/branch/referral/util/CurrencyType;

    .line 936
    .line 937
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 938
    .line 939
    const/16 v1, 0x4c

    .line 940
    .line 941
    const-string v2, "KHR"

    .line 942
    .line 943
    const-string v3, "KHR"

    .line 944
    .line 945
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 946
    .line 947
    .line 948
    sput-object v0, Lio/branch/referral/util/CurrencyType;->KHR:Lio/branch/referral/util/CurrencyType;

    .line 949
    .line 950
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 951
    .line 952
    const/16 v1, 0x4d

    .line 953
    .line 954
    const-string v2, "KMF"

    .line 955
    .line 956
    const-string v3, "KMF"

    .line 957
    .line 958
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 959
    .line 960
    .line 961
    sput-object v0, Lio/branch/referral/util/CurrencyType;->KMF:Lio/branch/referral/util/CurrencyType;

    .line 962
    .line 963
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 964
    .line 965
    const/16 v1, 0x4e

    .line 966
    .line 967
    const-string v2, "KPW"

    .line 968
    .line 969
    const-string v3, "KPW"

    .line 970
    .line 971
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 972
    .line 973
    .line 974
    sput-object v0, Lio/branch/referral/util/CurrencyType;->KPW:Lio/branch/referral/util/CurrencyType;

    .line 975
    .line 976
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 977
    .line 978
    const/16 v1, 0x4f

    .line 979
    .line 980
    const-string v2, "KRW"

    .line 981
    .line 982
    const-string v3, "KRW"

    .line 983
    .line 984
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 985
    .line 986
    .line 987
    sput-object v0, Lio/branch/referral/util/CurrencyType;->KRW:Lio/branch/referral/util/CurrencyType;

    .line 988
    .line 989
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 990
    .line 991
    const/16 v1, 0x50

    .line 992
    .line 993
    const-string v2, "KWD"

    .line 994
    .line 995
    const-string v3, "KWD"

    .line 996
    .line 997
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 998
    .line 999
    .line 1000
    sput-object v0, Lio/branch/referral/util/CurrencyType;->KWD:Lio/branch/referral/util/CurrencyType;

    .line 1001
    .line 1002
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1003
    .line 1004
    const/16 v1, 0x51

    .line 1005
    .line 1006
    const-string v2, "KYD"

    .line 1007
    .line 1008
    const-string v3, "KYD"

    .line 1009
    .line 1010
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1011
    .line 1012
    .line 1013
    sput-object v0, Lio/branch/referral/util/CurrencyType;->KYD:Lio/branch/referral/util/CurrencyType;

    .line 1014
    .line 1015
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1016
    .line 1017
    const/16 v1, 0x52

    .line 1018
    .line 1019
    const-string v2, "KZT"

    .line 1020
    .line 1021
    const-string v3, "KZT"

    .line 1022
    .line 1023
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1024
    .line 1025
    .line 1026
    sput-object v0, Lio/branch/referral/util/CurrencyType;->KZT:Lio/branch/referral/util/CurrencyType;

    .line 1027
    .line 1028
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1029
    .line 1030
    const/16 v1, 0x53

    .line 1031
    .line 1032
    const-string v2, "LAK"

    .line 1033
    .line 1034
    const-string v3, "LAK"

    .line 1035
    .line 1036
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1037
    .line 1038
    .line 1039
    sput-object v0, Lio/branch/referral/util/CurrencyType;->LAK:Lio/branch/referral/util/CurrencyType;

    .line 1040
    .line 1041
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1042
    .line 1043
    const/16 v1, 0x54

    .line 1044
    .line 1045
    const-string v2, "LBP"

    .line 1046
    .line 1047
    const-string v3, "LBP"

    .line 1048
    .line 1049
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1050
    .line 1051
    .line 1052
    sput-object v0, Lio/branch/referral/util/CurrencyType;->LBP:Lio/branch/referral/util/CurrencyType;

    .line 1053
    .line 1054
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1055
    .line 1056
    const/16 v1, 0x55

    .line 1057
    .line 1058
    const-string v2, "LKR"

    .line 1059
    .line 1060
    const-string v3, "LKR"

    .line 1061
    .line 1062
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1063
    .line 1064
    .line 1065
    sput-object v0, Lio/branch/referral/util/CurrencyType;->LKR:Lio/branch/referral/util/CurrencyType;

    .line 1066
    .line 1067
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1068
    .line 1069
    const/16 v1, 0x56

    .line 1070
    .line 1071
    const-string v2, "LRD"

    .line 1072
    .line 1073
    const-string v3, "LRD"

    .line 1074
    .line 1075
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1076
    .line 1077
    .line 1078
    sput-object v0, Lio/branch/referral/util/CurrencyType;->LRD:Lio/branch/referral/util/CurrencyType;

    .line 1079
    .line 1080
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1081
    .line 1082
    const/16 v1, 0x57

    .line 1083
    .line 1084
    const-string v2, "LSL"

    .line 1085
    .line 1086
    const-string v3, "LSL"

    .line 1087
    .line 1088
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1089
    .line 1090
    .line 1091
    sput-object v0, Lio/branch/referral/util/CurrencyType;->LSL:Lio/branch/referral/util/CurrencyType;

    .line 1092
    .line 1093
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1094
    .line 1095
    const/16 v1, 0x58

    .line 1096
    .line 1097
    const-string v2, "LYD"

    .line 1098
    .line 1099
    const-string v3, "LYD"

    .line 1100
    .line 1101
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1102
    .line 1103
    .line 1104
    sput-object v0, Lio/branch/referral/util/CurrencyType;->LYD:Lio/branch/referral/util/CurrencyType;

    .line 1105
    .line 1106
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1107
    .line 1108
    const/16 v1, 0x59

    .line 1109
    .line 1110
    const-string v2, "MAD"

    .line 1111
    .line 1112
    const-string v3, "MAD"

    .line 1113
    .line 1114
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1115
    .line 1116
    .line 1117
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MAD:Lio/branch/referral/util/CurrencyType;

    .line 1118
    .line 1119
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1120
    .line 1121
    const/16 v1, 0x5a

    .line 1122
    .line 1123
    const-string v2, "MDL"

    .line 1124
    .line 1125
    const-string v3, "MDL"

    .line 1126
    .line 1127
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1128
    .line 1129
    .line 1130
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MDL:Lio/branch/referral/util/CurrencyType;

    .line 1131
    .line 1132
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1133
    .line 1134
    const/16 v1, 0x5b

    .line 1135
    .line 1136
    const-string v2, "MGA"

    .line 1137
    .line 1138
    const-string v3, "MGA"

    .line 1139
    .line 1140
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1141
    .line 1142
    .line 1143
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MGA:Lio/branch/referral/util/CurrencyType;

    .line 1144
    .line 1145
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1146
    .line 1147
    const/16 v1, 0x5c

    .line 1148
    .line 1149
    const-string v2, "MKD"

    .line 1150
    .line 1151
    const-string v3, "MKD"

    .line 1152
    .line 1153
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1154
    .line 1155
    .line 1156
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MKD:Lio/branch/referral/util/CurrencyType;

    .line 1157
    .line 1158
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1159
    .line 1160
    const/16 v1, 0x5d

    .line 1161
    .line 1162
    const-string v2, "MMK"

    .line 1163
    .line 1164
    const-string v3, "MMK"

    .line 1165
    .line 1166
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1167
    .line 1168
    .line 1169
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MMK:Lio/branch/referral/util/CurrencyType;

    .line 1170
    .line 1171
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1172
    .line 1173
    const/16 v1, 0x5e

    .line 1174
    .line 1175
    const-string v2, "MNT"

    .line 1176
    .line 1177
    const-string v3, "MNT"

    .line 1178
    .line 1179
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1180
    .line 1181
    .line 1182
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MNT:Lio/branch/referral/util/CurrencyType;

    .line 1183
    .line 1184
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1185
    .line 1186
    const/16 v1, 0x5f

    .line 1187
    .line 1188
    const-string v2, "MOP"

    .line 1189
    .line 1190
    const-string v3, "MOP"

    .line 1191
    .line 1192
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1193
    .line 1194
    .line 1195
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MOP:Lio/branch/referral/util/CurrencyType;

    .line 1196
    .line 1197
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1198
    .line 1199
    const/16 v1, 0x60

    .line 1200
    .line 1201
    const-string v2, "MRO"

    .line 1202
    .line 1203
    const-string v3, "MRO"

    .line 1204
    .line 1205
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1206
    .line 1207
    .line 1208
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MRO:Lio/branch/referral/util/CurrencyType;

    .line 1209
    .line 1210
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1211
    .line 1212
    const/16 v1, 0x61

    .line 1213
    .line 1214
    const-string v2, "MUR"

    .line 1215
    .line 1216
    const-string v3, "MUR"

    .line 1217
    .line 1218
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1219
    .line 1220
    .line 1221
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MUR:Lio/branch/referral/util/CurrencyType;

    .line 1222
    .line 1223
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1224
    .line 1225
    const/16 v1, 0x62

    .line 1226
    .line 1227
    const-string v2, "MVR"

    .line 1228
    .line 1229
    const-string v3, "MVR"

    .line 1230
    .line 1231
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1232
    .line 1233
    .line 1234
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MVR:Lio/branch/referral/util/CurrencyType;

    .line 1235
    .line 1236
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1237
    .line 1238
    const/16 v1, 0x63

    .line 1239
    .line 1240
    const-string v2, "MWK"

    .line 1241
    .line 1242
    const-string v3, "MWK"

    .line 1243
    .line 1244
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1245
    .line 1246
    .line 1247
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MWK:Lio/branch/referral/util/CurrencyType;

    .line 1248
    .line 1249
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1250
    .line 1251
    const/16 v1, 0x64

    .line 1252
    .line 1253
    const-string v2, "MXN"

    .line 1254
    .line 1255
    const-string v3, "MXN"

    .line 1256
    .line 1257
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1258
    .line 1259
    .line 1260
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MXN:Lio/branch/referral/util/CurrencyType;

    .line 1261
    .line 1262
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1263
    .line 1264
    const/16 v1, 0x65

    .line 1265
    .line 1266
    const-string v2, "MXV"

    .line 1267
    .line 1268
    const-string v3, "MXV"

    .line 1269
    .line 1270
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1271
    .line 1272
    .line 1273
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MXV:Lio/branch/referral/util/CurrencyType;

    .line 1274
    .line 1275
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1276
    .line 1277
    const/16 v1, 0x66

    .line 1278
    .line 1279
    const-string v2, "MYR"

    .line 1280
    .line 1281
    const-string v3, "MYR"

    .line 1282
    .line 1283
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1284
    .line 1285
    .line 1286
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MYR:Lio/branch/referral/util/CurrencyType;

    .line 1287
    .line 1288
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1289
    .line 1290
    const/16 v1, 0x67

    .line 1291
    .line 1292
    const-string v2, "MZN"

    .line 1293
    .line 1294
    const-string v3, "MZN"

    .line 1295
    .line 1296
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1297
    .line 1298
    .line 1299
    sput-object v0, Lio/branch/referral/util/CurrencyType;->MZN:Lio/branch/referral/util/CurrencyType;

    .line 1300
    .line 1301
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1302
    .line 1303
    const/16 v1, 0x68

    .line 1304
    .line 1305
    const-string v2, "NAD"

    .line 1306
    .line 1307
    const-string v3, "NAD"

    .line 1308
    .line 1309
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1310
    .line 1311
    .line 1312
    sput-object v0, Lio/branch/referral/util/CurrencyType;->NAD:Lio/branch/referral/util/CurrencyType;

    .line 1313
    .line 1314
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1315
    .line 1316
    const/16 v1, 0x69

    .line 1317
    .line 1318
    const-string v2, "NGN"

    .line 1319
    .line 1320
    const-string v3, "NGN"

    .line 1321
    .line 1322
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1323
    .line 1324
    .line 1325
    sput-object v0, Lio/branch/referral/util/CurrencyType;->NGN:Lio/branch/referral/util/CurrencyType;

    .line 1326
    .line 1327
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1328
    .line 1329
    const/16 v1, 0x6a

    .line 1330
    .line 1331
    const-string v2, "NIO"

    .line 1332
    .line 1333
    const-string v3, "NIO"

    .line 1334
    .line 1335
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1336
    .line 1337
    .line 1338
    sput-object v0, Lio/branch/referral/util/CurrencyType;->NIO:Lio/branch/referral/util/CurrencyType;

    .line 1339
    .line 1340
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1341
    .line 1342
    const/16 v1, 0x6b

    .line 1343
    .line 1344
    const-string v2, "NOK"

    .line 1345
    .line 1346
    const-string v3, "NOK"

    .line 1347
    .line 1348
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1349
    .line 1350
    .line 1351
    sput-object v0, Lio/branch/referral/util/CurrencyType;->NOK:Lio/branch/referral/util/CurrencyType;

    .line 1352
    .line 1353
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1354
    .line 1355
    const/16 v1, 0x6c

    .line 1356
    .line 1357
    const-string v2, "NPR"

    .line 1358
    .line 1359
    const-string v3, "NPR"

    .line 1360
    .line 1361
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1362
    .line 1363
    .line 1364
    sput-object v0, Lio/branch/referral/util/CurrencyType;->NPR:Lio/branch/referral/util/CurrencyType;

    .line 1365
    .line 1366
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1367
    .line 1368
    const/16 v1, 0x6d

    .line 1369
    .line 1370
    const-string v2, "NZD"

    .line 1371
    .line 1372
    const-string v3, "NZD"

    .line 1373
    .line 1374
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1375
    .line 1376
    .line 1377
    sput-object v0, Lio/branch/referral/util/CurrencyType;->NZD:Lio/branch/referral/util/CurrencyType;

    .line 1378
    .line 1379
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1380
    .line 1381
    const/16 v1, 0x6e

    .line 1382
    .line 1383
    const-string v2, "OMR"

    .line 1384
    .line 1385
    const-string v3, "OMR"

    .line 1386
    .line 1387
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1388
    .line 1389
    .line 1390
    sput-object v0, Lio/branch/referral/util/CurrencyType;->OMR:Lio/branch/referral/util/CurrencyType;

    .line 1391
    .line 1392
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1393
    .line 1394
    const/16 v1, 0x6f

    .line 1395
    .line 1396
    const-string v2, "PAB"

    .line 1397
    .line 1398
    const-string v3, "PAB"

    .line 1399
    .line 1400
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1401
    .line 1402
    .line 1403
    sput-object v0, Lio/branch/referral/util/CurrencyType;->PAB:Lio/branch/referral/util/CurrencyType;

    .line 1404
    .line 1405
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1406
    .line 1407
    const/16 v1, 0x70

    .line 1408
    .line 1409
    const-string v2, "PEN"

    .line 1410
    .line 1411
    const-string v3, "PEN"

    .line 1412
    .line 1413
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1414
    .line 1415
    .line 1416
    sput-object v0, Lio/branch/referral/util/CurrencyType;->PEN:Lio/branch/referral/util/CurrencyType;

    .line 1417
    .line 1418
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1419
    .line 1420
    const/16 v1, 0x71

    .line 1421
    .line 1422
    const-string v2, "PGK"

    .line 1423
    .line 1424
    const-string v3, "PGK"

    .line 1425
    .line 1426
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1427
    .line 1428
    .line 1429
    sput-object v0, Lio/branch/referral/util/CurrencyType;->PGK:Lio/branch/referral/util/CurrencyType;

    .line 1430
    .line 1431
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1432
    .line 1433
    const/16 v1, 0x72

    .line 1434
    .line 1435
    const-string v2, "PHP"

    .line 1436
    .line 1437
    const-string v3, "PHP"

    .line 1438
    .line 1439
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1440
    .line 1441
    .line 1442
    sput-object v0, Lio/branch/referral/util/CurrencyType;->PHP:Lio/branch/referral/util/CurrencyType;

    .line 1443
    .line 1444
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1445
    .line 1446
    const/16 v1, 0x73

    .line 1447
    .line 1448
    const-string v2, "PKR"

    .line 1449
    .line 1450
    const-string v3, "PKR"

    .line 1451
    .line 1452
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1453
    .line 1454
    .line 1455
    sput-object v0, Lio/branch/referral/util/CurrencyType;->PKR:Lio/branch/referral/util/CurrencyType;

    .line 1456
    .line 1457
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1458
    .line 1459
    const/16 v1, 0x74

    .line 1460
    .line 1461
    const-string v2, "PLN"

    .line 1462
    .line 1463
    const-string v3, "PLN"

    .line 1464
    .line 1465
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1466
    .line 1467
    .line 1468
    sput-object v0, Lio/branch/referral/util/CurrencyType;->PLN:Lio/branch/referral/util/CurrencyType;

    .line 1469
    .line 1470
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1471
    .line 1472
    const/16 v1, 0x75

    .line 1473
    .line 1474
    const-string v2, "PYG"

    .line 1475
    .line 1476
    const-string v3, "PYG"

    .line 1477
    .line 1478
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1479
    .line 1480
    .line 1481
    sput-object v0, Lio/branch/referral/util/CurrencyType;->PYG:Lio/branch/referral/util/CurrencyType;

    .line 1482
    .line 1483
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1484
    .line 1485
    const/16 v1, 0x76

    .line 1486
    .line 1487
    const-string v2, "QAR"

    .line 1488
    .line 1489
    const-string v3, "QAR"

    .line 1490
    .line 1491
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1492
    .line 1493
    .line 1494
    sput-object v0, Lio/branch/referral/util/CurrencyType;->QAR:Lio/branch/referral/util/CurrencyType;

    .line 1495
    .line 1496
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1497
    .line 1498
    const/16 v1, 0x77

    .line 1499
    .line 1500
    const-string v2, "RON"

    .line 1501
    .line 1502
    const-string v3, "RON"

    .line 1503
    .line 1504
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1505
    .line 1506
    .line 1507
    sput-object v0, Lio/branch/referral/util/CurrencyType;->RON:Lio/branch/referral/util/CurrencyType;

    .line 1508
    .line 1509
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1510
    .line 1511
    const/16 v1, 0x78

    .line 1512
    .line 1513
    const-string v2, "RSD"

    .line 1514
    .line 1515
    const-string v3, "RSD"

    .line 1516
    .line 1517
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1518
    .line 1519
    .line 1520
    sput-object v0, Lio/branch/referral/util/CurrencyType;->RSD:Lio/branch/referral/util/CurrencyType;

    .line 1521
    .line 1522
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1523
    .line 1524
    const/16 v1, 0x79

    .line 1525
    .line 1526
    const-string v2, "RUB"

    .line 1527
    .line 1528
    const-string v3, "RUB"

    .line 1529
    .line 1530
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1531
    .line 1532
    .line 1533
    sput-object v0, Lio/branch/referral/util/CurrencyType;->RUB:Lio/branch/referral/util/CurrencyType;

    .line 1534
    .line 1535
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1536
    .line 1537
    const/16 v1, 0x7a

    .line 1538
    .line 1539
    const-string v2, "RWF"

    .line 1540
    .line 1541
    const-string v3, "RWF"

    .line 1542
    .line 1543
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1544
    .line 1545
    .line 1546
    sput-object v0, Lio/branch/referral/util/CurrencyType;->RWF:Lio/branch/referral/util/CurrencyType;

    .line 1547
    .line 1548
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1549
    .line 1550
    const/16 v1, 0x7b

    .line 1551
    .line 1552
    const-string v2, "SAR"

    .line 1553
    .line 1554
    const-string v3, "SAR"

    .line 1555
    .line 1556
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1557
    .line 1558
    .line 1559
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SAR:Lio/branch/referral/util/CurrencyType;

    .line 1560
    .line 1561
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1562
    .line 1563
    const/16 v1, 0x7c

    .line 1564
    .line 1565
    const-string v2, "SBD"

    .line 1566
    .line 1567
    const-string v3, "SBD"

    .line 1568
    .line 1569
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1570
    .line 1571
    .line 1572
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SBD:Lio/branch/referral/util/CurrencyType;

    .line 1573
    .line 1574
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1575
    .line 1576
    const/16 v1, 0x7d

    .line 1577
    .line 1578
    const-string v2, "SCR"

    .line 1579
    .line 1580
    const-string v3, "SCR"

    .line 1581
    .line 1582
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1583
    .line 1584
    .line 1585
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SCR:Lio/branch/referral/util/CurrencyType;

    .line 1586
    .line 1587
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1588
    .line 1589
    const/16 v1, 0x7e

    .line 1590
    .line 1591
    const-string v2, "SDG"

    .line 1592
    .line 1593
    const-string v3, "SDG"

    .line 1594
    .line 1595
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1596
    .line 1597
    .line 1598
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SDG:Lio/branch/referral/util/CurrencyType;

    .line 1599
    .line 1600
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1601
    .line 1602
    const/16 v1, 0x7f

    .line 1603
    .line 1604
    const-string v2, "SEK"

    .line 1605
    .line 1606
    const-string v3, "SEK"

    .line 1607
    .line 1608
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1609
    .line 1610
    .line 1611
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SEK:Lio/branch/referral/util/CurrencyType;

    .line 1612
    .line 1613
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1614
    .line 1615
    const/16 v1, 0x80

    .line 1616
    .line 1617
    const-string v2, "SGD"

    .line 1618
    .line 1619
    const-string v3, "SGD"

    .line 1620
    .line 1621
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1622
    .line 1623
    .line 1624
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SGD:Lio/branch/referral/util/CurrencyType;

    .line 1625
    .line 1626
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1627
    .line 1628
    const/16 v1, 0x81

    .line 1629
    .line 1630
    const-string v2, "SHP"

    .line 1631
    .line 1632
    const-string v3, "SHP"

    .line 1633
    .line 1634
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1635
    .line 1636
    .line 1637
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SHP:Lio/branch/referral/util/CurrencyType;

    .line 1638
    .line 1639
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1640
    .line 1641
    const/16 v1, 0x82

    .line 1642
    .line 1643
    const-string v2, "SLL"

    .line 1644
    .line 1645
    const-string v3, "SLL"

    .line 1646
    .line 1647
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1648
    .line 1649
    .line 1650
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SLL:Lio/branch/referral/util/CurrencyType;

    .line 1651
    .line 1652
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1653
    .line 1654
    const/16 v1, 0x83

    .line 1655
    .line 1656
    const-string v2, "SOS"

    .line 1657
    .line 1658
    const-string v3, "SOS"

    .line 1659
    .line 1660
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1661
    .line 1662
    .line 1663
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SOS:Lio/branch/referral/util/CurrencyType;

    .line 1664
    .line 1665
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1666
    .line 1667
    const/16 v1, 0x84

    .line 1668
    .line 1669
    const-string v2, "SRD"

    .line 1670
    .line 1671
    const-string v3, "SRD"

    .line 1672
    .line 1673
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1674
    .line 1675
    .line 1676
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SRD:Lio/branch/referral/util/CurrencyType;

    .line 1677
    .line 1678
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1679
    .line 1680
    const/16 v1, 0x85

    .line 1681
    .line 1682
    const-string v2, "SSP"

    .line 1683
    .line 1684
    const-string v3, "SSP"

    .line 1685
    .line 1686
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1687
    .line 1688
    .line 1689
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SSP:Lio/branch/referral/util/CurrencyType;

    .line 1690
    .line 1691
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1692
    .line 1693
    const/16 v1, 0x86

    .line 1694
    .line 1695
    const-string v2, "STD"

    .line 1696
    .line 1697
    const-string v3, "STD"

    .line 1698
    .line 1699
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1700
    .line 1701
    .line 1702
    sput-object v0, Lio/branch/referral/util/CurrencyType;->STD:Lio/branch/referral/util/CurrencyType;

    .line 1703
    .line 1704
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1705
    .line 1706
    const/16 v1, 0x87

    .line 1707
    .line 1708
    const-string v2, "SYP"

    .line 1709
    .line 1710
    const-string v3, "SYP"

    .line 1711
    .line 1712
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1713
    .line 1714
    .line 1715
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SYP:Lio/branch/referral/util/CurrencyType;

    .line 1716
    .line 1717
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1718
    .line 1719
    const/16 v1, 0x88

    .line 1720
    .line 1721
    const-string v2, "SZL"

    .line 1722
    .line 1723
    const-string v3, "SZL"

    .line 1724
    .line 1725
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1726
    .line 1727
    .line 1728
    sput-object v0, Lio/branch/referral/util/CurrencyType;->SZL:Lio/branch/referral/util/CurrencyType;

    .line 1729
    .line 1730
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1731
    .line 1732
    const/16 v1, 0x89

    .line 1733
    .line 1734
    const-string v2, "THB"

    .line 1735
    .line 1736
    const-string v3, "THB"

    .line 1737
    .line 1738
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1739
    .line 1740
    .line 1741
    sput-object v0, Lio/branch/referral/util/CurrencyType;->THB:Lio/branch/referral/util/CurrencyType;

    .line 1742
    .line 1743
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1744
    .line 1745
    const/16 v1, 0x8a

    .line 1746
    .line 1747
    const-string v2, "TJS"

    .line 1748
    .line 1749
    const-string v3, "TJS"

    .line 1750
    .line 1751
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1752
    .line 1753
    .line 1754
    sput-object v0, Lio/branch/referral/util/CurrencyType;->TJS:Lio/branch/referral/util/CurrencyType;

    .line 1755
    .line 1756
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1757
    .line 1758
    const/16 v1, 0x8b

    .line 1759
    .line 1760
    const-string v2, "TMT"

    .line 1761
    .line 1762
    const-string v3, "TMT"

    .line 1763
    .line 1764
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1765
    .line 1766
    .line 1767
    sput-object v0, Lio/branch/referral/util/CurrencyType;->TMT:Lio/branch/referral/util/CurrencyType;

    .line 1768
    .line 1769
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1770
    .line 1771
    const/16 v1, 0x8c

    .line 1772
    .line 1773
    const-string v2, "TND"

    .line 1774
    .line 1775
    const-string v3, "TND"

    .line 1776
    .line 1777
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1778
    .line 1779
    .line 1780
    sput-object v0, Lio/branch/referral/util/CurrencyType;->TND:Lio/branch/referral/util/CurrencyType;

    .line 1781
    .line 1782
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1783
    .line 1784
    const/16 v1, 0x8d

    .line 1785
    .line 1786
    const-string v2, "TOP"

    .line 1787
    .line 1788
    const-string v3, "TOP"

    .line 1789
    .line 1790
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1791
    .line 1792
    .line 1793
    sput-object v0, Lio/branch/referral/util/CurrencyType;->TOP:Lio/branch/referral/util/CurrencyType;

    .line 1794
    .line 1795
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1796
    .line 1797
    const/16 v1, 0x8e

    .line 1798
    .line 1799
    const-string v2, "TRY"

    .line 1800
    .line 1801
    const-string v3, "TRY"

    .line 1802
    .line 1803
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1804
    .line 1805
    .line 1806
    sput-object v0, Lio/branch/referral/util/CurrencyType;->TRY:Lio/branch/referral/util/CurrencyType;

    .line 1807
    .line 1808
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1809
    .line 1810
    const/16 v1, 0x8f

    .line 1811
    .line 1812
    const-string v2, "TTD"

    .line 1813
    .line 1814
    const-string v3, "TTD"

    .line 1815
    .line 1816
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1817
    .line 1818
    .line 1819
    sput-object v0, Lio/branch/referral/util/CurrencyType;->TTD:Lio/branch/referral/util/CurrencyType;

    .line 1820
    .line 1821
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1822
    .line 1823
    const/16 v1, 0x90

    .line 1824
    .line 1825
    const-string v2, "TWD"

    .line 1826
    .line 1827
    const-string v3, "TWD"

    .line 1828
    .line 1829
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1830
    .line 1831
    .line 1832
    sput-object v0, Lio/branch/referral/util/CurrencyType;->TWD:Lio/branch/referral/util/CurrencyType;

    .line 1833
    .line 1834
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1835
    .line 1836
    const/16 v1, 0x91

    .line 1837
    .line 1838
    const-string v2, "TZS"

    .line 1839
    .line 1840
    const-string v3, "TZS"

    .line 1841
    .line 1842
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1843
    .line 1844
    .line 1845
    sput-object v0, Lio/branch/referral/util/CurrencyType;->TZS:Lio/branch/referral/util/CurrencyType;

    .line 1846
    .line 1847
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1848
    .line 1849
    const/16 v1, 0x92

    .line 1850
    .line 1851
    const-string v2, "UAH"

    .line 1852
    .line 1853
    const-string v3, "UAH"

    .line 1854
    .line 1855
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1856
    .line 1857
    .line 1858
    sput-object v0, Lio/branch/referral/util/CurrencyType;->UAH:Lio/branch/referral/util/CurrencyType;

    .line 1859
    .line 1860
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1861
    .line 1862
    const/16 v1, 0x93

    .line 1863
    .line 1864
    const-string v2, "UGX"

    .line 1865
    .line 1866
    const-string v3, "UGX"

    .line 1867
    .line 1868
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1869
    .line 1870
    .line 1871
    sput-object v0, Lio/branch/referral/util/CurrencyType;->UGX:Lio/branch/referral/util/CurrencyType;

    .line 1872
    .line 1873
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1874
    .line 1875
    const/16 v1, 0x94

    .line 1876
    .line 1877
    const-string v2, "USD"

    .line 1878
    .line 1879
    const-string v3, "USD"

    .line 1880
    .line 1881
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1882
    .line 1883
    .line 1884
    sput-object v0, Lio/branch/referral/util/CurrencyType;->USD:Lio/branch/referral/util/CurrencyType;

    .line 1885
    .line 1886
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1887
    .line 1888
    const/16 v1, 0x95

    .line 1889
    .line 1890
    const-string v2, "USN"

    .line 1891
    .line 1892
    const-string v3, "USN"

    .line 1893
    .line 1894
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1895
    .line 1896
    .line 1897
    sput-object v0, Lio/branch/referral/util/CurrencyType;->USN:Lio/branch/referral/util/CurrencyType;

    .line 1898
    .line 1899
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1900
    .line 1901
    const/16 v1, 0x96

    .line 1902
    .line 1903
    const-string v2, "UYI"

    .line 1904
    .line 1905
    const-string v3, "UYI"

    .line 1906
    .line 1907
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1908
    .line 1909
    .line 1910
    sput-object v0, Lio/branch/referral/util/CurrencyType;->UYI:Lio/branch/referral/util/CurrencyType;

    .line 1911
    .line 1912
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1913
    .line 1914
    const/16 v1, 0x97

    .line 1915
    .line 1916
    const-string v2, "UYU"

    .line 1917
    .line 1918
    const-string v3, "UYU"

    .line 1919
    .line 1920
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1921
    .line 1922
    .line 1923
    sput-object v0, Lio/branch/referral/util/CurrencyType;->UYU:Lio/branch/referral/util/CurrencyType;

    .line 1924
    .line 1925
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1926
    .line 1927
    const/16 v1, 0x98

    .line 1928
    .line 1929
    const-string v2, "UZS"

    .line 1930
    .line 1931
    const-string v3, "UZS"

    .line 1932
    .line 1933
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1934
    .line 1935
    .line 1936
    sput-object v0, Lio/branch/referral/util/CurrencyType;->UZS:Lio/branch/referral/util/CurrencyType;

    .line 1937
    .line 1938
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1939
    .line 1940
    const/16 v1, 0x99

    .line 1941
    .line 1942
    const-string v2, "VEF"

    .line 1943
    .line 1944
    const-string v3, "VEF"

    .line 1945
    .line 1946
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1947
    .line 1948
    .line 1949
    sput-object v0, Lio/branch/referral/util/CurrencyType;->VEF:Lio/branch/referral/util/CurrencyType;

    .line 1950
    .line 1951
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1952
    .line 1953
    const/16 v1, 0x9a

    .line 1954
    .line 1955
    const-string v2, "VND"

    .line 1956
    .line 1957
    const-string v3, "VND"

    .line 1958
    .line 1959
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1960
    .line 1961
    .line 1962
    sput-object v0, Lio/branch/referral/util/CurrencyType;->VND:Lio/branch/referral/util/CurrencyType;

    .line 1963
    .line 1964
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1965
    .line 1966
    const/16 v1, 0x9b

    .line 1967
    .line 1968
    const-string v2, "VUV"

    .line 1969
    .line 1970
    const-string v3, "VUV"

    .line 1971
    .line 1972
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1973
    .line 1974
    .line 1975
    sput-object v0, Lio/branch/referral/util/CurrencyType;->VUV:Lio/branch/referral/util/CurrencyType;

    .line 1976
    .line 1977
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1978
    .line 1979
    const/16 v1, 0x9c

    .line 1980
    .line 1981
    const-string v2, "WST"

    .line 1982
    .line 1983
    const-string v3, "WST"

    .line 1984
    .line 1985
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1986
    .line 1987
    .line 1988
    sput-object v0, Lio/branch/referral/util/CurrencyType;->WST:Lio/branch/referral/util/CurrencyType;

    .line 1989
    .line 1990
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 1991
    .line 1992
    const/16 v1, 0x9d

    .line 1993
    .line 1994
    const-string v2, "XAF"

    .line 1995
    .line 1996
    const-string v3, "XAF"

    .line 1997
    .line 1998
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1999
    .line 2000
    .line 2001
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XAF:Lio/branch/referral/util/CurrencyType;

    .line 2002
    .line 2003
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2004
    .line 2005
    const/16 v1, 0x9e

    .line 2006
    .line 2007
    const-string v2, "XAG"

    .line 2008
    .line 2009
    const-string v3, "XAG"

    .line 2010
    .line 2011
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2012
    .line 2013
    .line 2014
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XAG:Lio/branch/referral/util/CurrencyType;

    .line 2015
    .line 2016
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2017
    .line 2018
    const/16 v1, 0x9f

    .line 2019
    .line 2020
    const-string v2, "XAU"

    .line 2021
    .line 2022
    const-string v3, "XAU"

    .line 2023
    .line 2024
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2025
    .line 2026
    .line 2027
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XAU:Lio/branch/referral/util/CurrencyType;

    .line 2028
    .line 2029
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2030
    .line 2031
    const/16 v1, 0xa0

    .line 2032
    .line 2033
    const-string v2, "XBA"

    .line 2034
    .line 2035
    const-string v3, "XBA"

    .line 2036
    .line 2037
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2038
    .line 2039
    .line 2040
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XBA:Lio/branch/referral/util/CurrencyType;

    .line 2041
    .line 2042
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2043
    .line 2044
    const/16 v1, 0xa1

    .line 2045
    .line 2046
    const-string v2, "XBB"

    .line 2047
    .line 2048
    const-string v3, "XBB"

    .line 2049
    .line 2050
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2051
    .line 2052
    .line 2053
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XBB:Lio/branch/referral/util/CurrencyType;

    .line 2054
    .line 2055
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2056
    .line 2057
    const/16 v1, 0xa2

    .line 2058
    .line 2059
    const-string v2, "XBC"

    .line 2060
    .line 2061
    const-string v3, "XBC"

    .line 2062
    .line 2063
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2064
    .line 2065
    .line 2066
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XBC:Lio/branch/referral/util/CurrencyType;

    .line 2067
    .line 2068
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2069
    .line 2070
    const/16 v1, 0xa3

    .line 2071
    .line 2072
    const-string v2, "XBD"

    .line 2073
    .line 2074
    const-string v3, "XBD"

    .line 2075
    .line 2076
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2077
    .line 2078
    .line 2079
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XBD:Lio/branch/referral/util/CurrencyType;

    .line 2080
    .line 2081
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2082
    .line 2083
    const/16 v1, 0xa4

    .line 2084
    .line 2085
    const-string v2, "XCD"

    .line 2086
    .line 2087
    const-string v3, "XCD"

    .line 2088
    .line 2089
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2090
    .line 2091
    .line 2092
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XCD:Lio/branch/referral/util/CurrencyType;

    .line 2093
    .line 2094
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2095
    .line 2096
    const/16 v1, 0xa5

    .line 2097
    .line 2098
    const-string v2, "XDR"

    .line 2099
    .line 2100
    const-string v3, "XDR"

    .line 2101
    .line 2102
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2103
    .line 2104
    .line 2105
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XDR:Lio/branch/referral/util/CurrencyType;

    .line 2106
    .line 2107
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2108
    .line 2109
    const/16 v1, 0xa6

    .line 2110
    .line 2111
    const-string v2, "XFU"

    .line 2112
    .line 2113
    const-string v3, "XFU"

    .line 2114
    .line 2115
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2116
    .line 2117
    .line 2118
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XFU:Lio/branch/referral/util/CurrencyType;

    .line 2119
    .line 2120
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2121
    .line 2122
    const/16 v1, 0xa7

    .line 2123
    .line 2124
    const-string v2, "XOF"

    .line 2125
    .line 2126
    const-string v3, "XOF"

    .line 2127
    .line 2128
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2129
    .line 2130
    .line 2131
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XOF:Lio/branch/referral/util/CurrencyType;

    .line 2132
    .line 2133
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2134
    .line 2135
    const/16 v1, 0xa8

    .line 2136
    .line 2137
    const-string v2, "XPD"

    .line 2138
    .line 2139
    const-string v3, "XPD"

    .line 2140
    .line 2141
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2142
    .line 2143
    .line 2144
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XPD:Lio/branch/referral/util/CurrencyType;

    .line 2145
    .line 2146
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2147
    .line 2148
    const/16 v1, 0xa9

    .line 2149
    .line 2150
    const-string v2, "XPF"

    .line 2151
    .line 2152
    const-string v3, "XPF"

    .line 2153
    .line 2154
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2155
    .line 2156
    .line 2157
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XPF:Lio/branch/referral/util/CurrencyType;

    .line 2158
    .line 2159
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2160
    .line 2161
    const/16 v1, 0xaa

    .line 2162
    .line 2163
    const-string v2, "XPT"

    .line 2164
    .line 2165
    const-string v3, "XPT"

    .line 2166
    .line 2167
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2168
    .line 2169
    .line 2170
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XPT:Lio/branch/referral/util/CurrencyType;

    .line 2171
    .line 2172
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2173
    .line 2174
    const/16 v1, 0xab

    .line 2175
    .line 2176
    const-string v2, "XSU"

    .line 2177
    .line 2178
    const-string v3, "XSU"

    .line 2179
    .line 2180
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2181
    .line 2182
    .line 2183
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XSU:Lio/branch/referral/util/CurrencyType;

    .line 2184
    .line 2185
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2186
    .line 2187
    const/16 v1, 0xac

    .line 2188
    .line 2189
    const-string v2, "XTS"

    .line 2190
    .line 2191
    const-string v3, "XTS"

    .line 2192
    .line 2193
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2194
    .line 2195
    .line 2196
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XTS:Lio/branch/referral/util/CurrencyType;

    .line 2197
    .line 2198
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2199
    .line 2200
    const/16 v1, 0xad

    .line 2201
    .line 2202
    const-string v2, "XUA"

    .line 2203
    .line 2204
    const-string v3, "XUA"

    .line 2205
    .line 2206
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2207
    .line 2208
    .line 2209
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XUA:Lio/branch/referral/util/CurrencyType;

    .line 2210
    .line 2211
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2212
    .line 2213
    const/16 v1, 0xae

    .line 2214
    .line 2215
    const-string v2, "XXX"

    .line 2216
    .line 2217
    const-string v3, "XXX"

    .line 2218
    .line 2219
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2220
    .line 2221
    .line 2222
    sput-object v0, Lio/branch/referral/util/CurrencyType;->XXX:Lio/branch/referral/util/CurrencyType;

    .line 2223
    .line 2224
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2225
    .line 2226
    const/16 v1, 0xaf

    .line 2227
    .line 2228
    const-string v2, "YER"

    .line 2229
    .line 2230
    const-string v3, "YER"

    .line 2231
    .line 2232
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2233
    .line 2234
    .line 2235
    sput-object v0, Lio/branch/referral/util/CurrencyType;->YER:Lio/branch/referral/util/CurrencyType;

    .line 2236
    .line 2237
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2238
    .line 2239
    const/16 v1, 0xb0

    .line 2240
    .line 2241
    const-string v2, "ZAR"

    .line 2242
    .line 2243
    const-string v3, "ZAR"

    .line 2244
    .line 2245
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2246
    .line 2247
    .line 2248
    sput-object v0, Lio/branch/referral/util/CurrencyType;->ZAR:Lio/branch/referral/util/CurrencyType;

    .line 2249
    .line 2250
    new-instance v0, Lio/branch/referral/util/CurrencyType;

    .line 2251
    .line 2252
    const/16 v1, 0xb1

    .line 2253
    .line 2254
    const-string v2, "ZMW"

    .line 2255
    .line 2256
    const-string v3, "ZMW"

    .line 2257
    .line 2258
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/CurrencyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 2259
    .line 2260
    .line 2261
    sput-object v0, Lio/branch/referral/util/CurrencyType;->ZMW:Lio/branch/referral/util/CurrencyType;

    .line 2262
    .line 2263
    invoke-static {}, Lio/branch/referral/util/CurrencyType;->$values()[Lio/branch/referral/util/CurrencyType;

    .line 2264
    .line 2265
    .line 2266
    move-result-object v0

    .line 2267
    sput-object v0, Lio/branch/referral/util/CurrencyType;->$VALUES:[Lio/branch/referral/util/CurrencyType;

    .line 2268
    .line 2269
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lio/branch/referral/util/CurrencyType;->iso4217Code:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public static getValue(Ljava/lang/String;)Lio/branch/referral/util/CurrencyType;
    .locals 5

    .line 1
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-static {}, Lio/branch/referral/util/CurrencyType;->values()[Lio/branch/referral/util/CurrencyType;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    array-length v1, v0

    .line 12
    const/4 v2, 0x0

    .line 13
    :goto_0
    if-ge v2, v1, :cond_1

    .line 14
    .line 15
    aget-object v3, v0, v2

    .line 16
    .line 17
    iget-object v4, v3, Lio/branch/referral/util/CurrencyType;->iso4217Code:Ljava/lang/String;

    .line 18
    .line 19
    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    if-eqz v4, :cond_0

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const/4 v3, 0x0

    .line 30
    :goto_1
    return-object v3
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public static valueOf(Ljava/lang/String;)Lio/branch/referral/util/CurrencyType;
    .locals 1

    .line 1
    const-class v0, Lio/branch/referral/util/CurrencyType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lio/branch/referral/util/CurrencyType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lio/branch/referral/util/CurrencyType;
    .locals 1

    .line 1
    sget-object v0, Lio/branch/referral/util/CurrencyType;->$VALUES:[Lio/branch/referral/util/CurrencyType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lio/branch/referral/util/CurrencyType;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lio/branch/referral/util/CurrencyType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lio/branch/referral/util/CurrencyType;->iso4217Code:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

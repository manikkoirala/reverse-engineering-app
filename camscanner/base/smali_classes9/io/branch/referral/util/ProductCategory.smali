.class public final enum Lio/branch/referral/util/ProductCategory;
.super Ljava/lang/Enum;
.source "ProductCategory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/branch/referral/util/ProductCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/branch/referral/util/ProductCategory;

.field public static final enum ANIMALS_AND_PET_SUPPLIES:Lio/branch/referral/util/ProductCategory;

.field public static final enum APPAREL_AND_ACCESSORIES:Lio/branch/referral/util/ProductCategory;

.field public static final enum ARTS_AND_ENTERTAINMENT:Lio/branch/referral/util/ProductCategory;

.field public static final enum BABY_AND_TODDLER:Lio/branch/referral/util/ProductCategory;

.field public static final enum BUSINESS_AND_INDUSTRIAL:Lio/branch/referral/util/ProductCategory;

.field public static final enum CAMERAS_AND_OPTICS:Lio/branch/referral/util/ProductCategory;

.field public static final enum ELECTRONICS:Lio/branch/referral/util/ProductCategory;

.field public static final enum FOOD_BEVERAGES_AND_TOBACCO:Lio/branch/referral/util/ProductCategory;

.field public static final enum FURNITURE:Lio/branch/referral/util/ProductCategory;

.field public static final enum HARDWARE:Lio/branch/referral/util/ProductCategory;

.field public static final enum HEALTH_AND_BEAUTY:Lio/branch/referral/util/ProductCategory;

.field public static final enum HOME_AND_GARDEN:Lio/branch/referral/util/ProductCategory;

.field public static final enum LUGGAGE_AND_BAGS:Lio/branch/referral/util/ProductCategory;

.field public static final enum MATURE:Lio/branch/referral/util/ProductCategory;

.field public static final enum MEDIA:Lio/branch/referral/util/ProductCategory;

.field public static final enum OFFICE_SUPPLIES:Lio/branch/referral/util/ProductCategory;

.field public static final enum RELIGIOUS_AND_CEREMONIAL:Lio/branch/referral/util/ProductCategory;

.field public static final enum SOFTWARE:Lio/branch/referral/util/ProductCategory;

.field public static final enum SPORTING_GOODS:Lio/branch/referral/util/ProductCategory;

.field public static final enum TOYS_AND_GAMES:Lio/branch/referral/util/ProductCategory;

.field public static final enum VEHICLES_AND_PARTS:Lio/branch/referral/util/ProductCategory;


# instance fields
.field private name:Ljava/lang/String;


# direct methods
.method private static synthetic $values()[Lio/branch/referral/util/ProductCategory;
    .locals 3

    .line 1
    const/16 v0, 0x15

    .line 2
    .line 3
    new-array v0, v0, [Lio/branch/referral/util/ProductCategory;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lio/branch/referral/util/ProductCategory;->ANIMALS_AND_PET_SUPPLIES:Lio/branch/referral/util/ProductCategory;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lio/branch/referral/util/ProductCategory;->APPAREL_AND_ACCESSORIES:Lio/branch/referral/util/ProductCategory;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lio/branch/referral/util/ProductCategory;->ARTS_AND_ENTERTAINMENT:Lio/branch/referral/util/ProductCategory;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lio/branch/referral/util/ProductCategory;->BABY_AND_TODDLER:Lio/branch/referral/util/ProductCategory;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lio/branch/referral/util/ProductCategory;->BUSINESS_AND_INDUSTRIAL:Lio/branch/referral/util/ProductCategory;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lio/branch/referral/util/ProductCategory;->CAMERAS_AND_OPTICS:Lio/branch/referral/util/ProductCategory;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lio/branch/referral/util/ProductCategory;->ELECTRONICS:Lio/branch/referral/util/ProductCategory;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lio/branch/referral/util/ProductCategory;->FOOD_BEVERAGES_AND_TOBACCO:Lio/branch/referral/util/ProductCategory;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lio/branch/referral/util/ProductCategory;->FURNITURE:Lio/branch/referral/util/ProductCategory;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    const/16 v1, 0x9

    .line 52
    .line 53
    sget-object v2, Lio/branch/referral/util/ProductCategory;->HARDWARE:Lio/branch/referral/util/ProductCategory;

    .line 54
    .line 55
    aput-object v2, v0, v1

    .line 56
    .line 57
    const/16 v1, 0xa

    .line 58
    .line 59
    sget-object v2, Lio/branch/referral/util/ProductCategory;->HEALTH_AND_BEAUTY:Lio/branch/referral/util/ProductCategory;

    .line 60
    .line 61
    aput-object v2, v0, v1

    .line 62
    .line 63
    const/16 v1, 0xb

    .line 64
    .line 65
    sget-object v2, Lio/branch/referral/util/ProductCategory;->HOME_AND_GARDEN:Lio/branch/referral/util/ProductCategory;

    .line 66
    .line 67
    aput-object v2, v0, v1

    .line 68
    .line 69
    const/16 v1, 0xc

    .line 70
    .line 71
    sget-object v2, Lio/branch/referral/util/ProductCategory;->LUGGAGE_AND_BAGS:Lio/branch/referral/util/ProductCategory;

    .line 72
    .line 73
    aput-object v2, v0, v1

    .line 74
    .line 75
    const/16 v1, 0xd

    .line 76
    .line 77
    sget-object v2, Lio/branch/referral/util/ProductCategory;->MATURE:Lio/branch/referral/util/ProductCategory;

    .line 78
    .line 79
    aput-object v2, v0, v1

    .line 80
    .line 81
    const/16 v1, 0xe

    .line 82
    .line 83
    sget-object v2, Lio/branch/referral/util/ProductCategory;->MEDIA:Lio/branch/referral/util/ProductCategory;

    .line 84
    .line 85
    aput-object v2, v0, v1

    .line 86
    .line 87
    const/16 v1, 0xf

    .line 88
    .line 89
    sget-object v2, Lio/branch/referral/util/ProductCategory;->OFFICE_SUPPLIES:Lio/branch/referral/util/ProductCategory;

    .line 90
    .line 91
    aput-object v2, v0, v1

    .line 92
    .line 93
    const/16 v1, 0x10

    .line 94
    .line 95
    sget-object v2, Lio/branch/referral/util/ProductCategory;->RELIGIOUS_AND_CEREMONIAL:Lio/branch/referral/util/ProductCategory;

    .line 96
    .line 97
    aput-object v2, v0, v1

    .line 98
    .line 99
    const/16 v1, 0x11

    .line 100
    .line 101
    sget-object v2, Lio/branch/referral/util/ProductCategory;->SOFTWARE:Lio/branch/referral/util/ProductCategory;

    .line 102
    .line 103
    aput-object v2, v0, v1

    .line 104
    .line 105
    const/16 v1, 0x12

    .line 106
    .line 107
    sget-object v2, Lio/branch/referral/util/ProductCategory;->SPORTING_GOODS:Lio/branch/referral/util/ProductCategory;

    .line 108
    .line 109
    aput-object v2, v0, v1

    .line 110
    .line 111
    const/16 v1, 0x13

    .line 112
    .line 113
    sget-object v2, Lio/branch/referral/util/ProductCategory;->TOYS_AND_GAMES:Lio/branch/referral/util/ProductCategory;

    .line 114
    .line 115
    aput-object v2, v0, v1

    .line 116
    .line 117
    const/16 v1, 0x14

    .line 118
    .line 119
    sget-object v2, Lio/branch/referral/util/ProductCategory;->VEHICLES_AND_PARTS:Lio/branch/referral/util/ProductCategory;

    .line 120
    .line 121
    aput-object v2, v0, v1

    .line 122
    .line 123
    return-object v0
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "Animals & Pet Supplies"

    .line 5
    .line 6
    const-string v3, "ANIMALS_AND_PET_SUPPLIES"

    .line 7
    .line 8
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sput-object v0, Lio/branch/referral/util/ProductCategory;->ANIMALS_AND_PET_SUPPLIES:Lio/branch/referral/util/ProductCategory;

    .line 12
    .line 13
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    const-string v2, "Apparel & Accessories"

    .line 17
    .line 18
    const-string v3, "APPAREL_AND_ACCESSORIES"

    .line 19
    .line 20
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sput-object v0, Lio/branch/referral/util/ProductCategory;->APPAREL_AND_ACCESSORIES:Lio/branch/referral/util/ProductCategory;

    .line 24
    .line 25
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 26
    .line 27
    const/4 v1, 0x2

    .line 28
    const-string v2, "Arts & Entertainment"

    .line 29
    .line 30
    const-string v3, "ARTS_AND_ENTERTAINMENT"

    .line 31
    .line 32
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 33
    .line 34
    .line 35
    sput-object v0, Lio/branch/referral/util/ProductCategory;->ARTS_AND_ENTERTAINMENT:Lio/branch/referral/util/ProductCategory;

    .line 36
    .line 37
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 38
    .line 39
    const/4 v1, 0x3

    .line 40
    const-string v2, "Baby & Toddler"

    .line 41
    .line 42
    const-string v3, "BABY_AND_TODDLER"

    .line 43
    .line 44
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 45
    .line 46
    .line 47
    sput-object v0, Lio/branch/referral/util/ProductCategory;->BABY_AND_TODDLER:Lio/branch/referral/util/ProductCategory;

    .line 48
    .line 49
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 50
    .line 51
    const/4 v1, 0x4

    .line 52
    const-string v2, "Business & Industrial"

    .line 53
    .line 54
    const-string v3, "BUSINESS_AND_INDUSTRIAL"

    .line 55
    .line 56
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 57
    .line 58
    .line 59
    sput-object v0, Lio/branch/referral/util/ProductCategory;->BUSINESS_AND_INDUSTRIAL:Lio/branch/referral/util/ProductCategory;

    .line 60
    .line 61
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 62
    .line 63
    const/4 v1, 0x5

    .line 64
    const-string v2, "Cameras & Optics"

    .line 65
    .line 66
    const-string v3, "CAMERAS_AND_OPTICS"

    .line 67
    .line 68
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 69
    .line 70
    .line 71
    sput-object v0, Lio/branch/referral/util/ProductCategory;->CAMERAS_AND_OPTICS:Lio/branch/referral/util/ProductCategory;

    .line 72
    .line 73
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 74
    .line 75
    const/4 v1, 0x6

    .line 76
    const-string v2, "Electronics"

    .line 77
    .line 78
    const-string v3, "ELECTRONICS"

    .line 79
    .line 80
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 81
    .line 82
    .line 83
    sput-object v0, Lio/branch/referral/util/ProductCategory;->ELECTRONICS:Lio/branch/referral/util/ProductCategory;

    .line 84
    .line 85
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 86
    .line 87
    const/4 v1, 0x7

    .line 88
    const-string v2, "Food, Beverages & Tobacco"

    .line 89
    .line 90
    const-string v3, "FOOD_BEVERAGES_AND_TOBACCO"

    .line 91
    .line 92
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 93
    .line 94
    .line 95
    sput-object v0, Lio/branch/referral/util/ProductCategory;->FOOD_BEVERAGES_AND_TOBACCO:Lio/branch/referral/util/ProductCategory;

    .line 96
    .line 97
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 98
    .line 99
    const/16 v1, 0x8

    .line 100
    .line 101
    const-string v2, "Furniture"

    .line 102
    .line 103
    const-string v3, "FURNITURE"

    .line 104
    .line 105
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 106
    .line 107
    .line 108
    sput-object v0, Lio/branch/referral/util/ProductCategory;->FURNITURE:Lio/branch/referral/util/ProductCategory;

    .line 109
    .line 110
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 111
    .line 112
    const/16 v1, 0x9

    .line 113
    .line 114
    const-string v2, "Hardware"

    .line 115
    .line 116
    const-string v3, "HARDWARE"

    .line 117
    .line 118
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 119
    .line 120
    .line 121
    sput-object v0, Lio/branch/referral/util/ProductCategory;->HARDWARE:Lio/branch/referral/util/ProductCategory;

    .line 122
    .line 123
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 124
    .line 125
    const/16 v1, 0xa

    .line 126
    .line 127
    const-string v2, "Health & Beauty"

    .line 128
    .line 129
    const-string v3, "HEALTH_AND_BEAUTY"

    .line 130
    .line 131
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 132
    .line 133
    .line 134
    sput-object v0, Lio/branch/referral/util/ProductCategory;->HEALTH_AND_BEAUTY:Lio/branch/referral/util/ProductCategory;

    .line 135
    .line 136
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 137
    .line 138
    const/16 v1, 0xb

    .line 139
    .line 140
    const-string v2, "Home & Garden"

    .line 141
    .line 142
    const-string v3, "HOME_AND_GARDEN"

    .line 143
    .line 144
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 145
    .line 146
    .line 147
    sput-object v0, Lio/branch/referral/util/ProductCategory;->HOME_AND_GARDEN:Lio/branch/referral/util/ProductCategory;

    .line 148
    .line 149
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 150
    .line 151
    const/16 v1, 0xc

    .line 152
    .line 153
    const-string v2, "Luggage & Bags"

    .line 154
    .line 155
    const-string v3, "LUGGAGE_AND_BAGS"

    .line 156
    .line 157
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 158
    .line 159
    .line 160
    sput-object v0, Lio/branch/referral/util/ProductCategory;->LUGGAGE_AND_BAGS:Lio/branch/referral/util/ProductCategory;

    .line 161
    .line 162
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 163
    .line 164
    const/16 v1, 0xd

    .line 165
    .line 166
    const-string v2, "Mature"

    .line 167
    .line 168
    const-string v3, "MATURE"

    .line 169
    .line 170
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 171
    .line 172
    .line 173
    sput-object v0, Lio/branch/referral/util/ProductCategory;->MATURE:Lio/branch/referral/util/ProductCategory;

    .line 174
    .line 175
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 176
    .line 177
    const/16 v1, 0xe

    .line 178
    .line 179
    const-string v2, "Media"

    .line 180
    .line 181
    const-string v3, "MEDIA"

    .line 182
    .line 183
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 184
    .line 185
    .line 186
    sput-object v0, Lio/branch/referral/util/ProductCategory;->MEDIA:Lio/branch/referral/util/ProductCategory;

    .line 187
    .line 188
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 189
    .line 190
    const/16 v1, 0xf

    .line 191
    .line 192
    const-string v2, "Office Supplies"

    .line 193
    .line 194
    const-string v3, "OFFICE_SUPPLIES"

    .line 195
    .line 196
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 197
    .line 198
    .line 199
    sput-object v0, Lio/branch/referral/util/ProductCategory;->OFFICE_SUPPLIES:Lio/branch/referral/util/ProductCategory;

    .line 200
    .line 201
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 202
    .line 203
    const/16 v1, 0x10

    .line 204
    .line 205
    const-string v2, "Religious & Ceremonial"

    .line 206
    .line 207
    const-string v3, "RELIGIOUS_AND_CEREMONIAL"

    .line 208
    .line 209
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 210
    .line 211
    .line 212
    sput-object v0, Lio/branch/referral/util/ProductCategory;->RELIGIOUS_AND_CEREMONIAL:Lio/branch/referral/util/ProductCategory;

    .line 213
    .line 214
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 215
    .line 216
    const/16 v1, 0x11

    .line 217
    .line 218
    const-string v2, "Software"

    .line 219
    .line 220
    const-string v3, "SOFTWARE"

    .line 221
    .line 222
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 223
    .line 224
    .line 225
    sput-object v0, Lio/branch/referral/util/ProductCategory;->SOFTWARE:Lio/branch/referral/util/ProductCategory;

    .line 226
    .line 227
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 228
    .line 229
    const/16 v1, 0x12

    .line 230
    .line 231
    const-string v2, "Sporting Goods"

    .line 232
    .line 233
    const-string v3, "SPORTING_GOODS"

    .line 234
    .line 235
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 236
    .line 237
    .line 238
    sput-object v0, Lio/branch/referral/util/ProductCategory;->SPORTING_GOODS:Lio/branch/referral/util/ProductCategory;

    .line 239
    .line 240
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 241
    .line 242
    const/16 v1, 0x13

    .line 243
    .line 244
    const-string v2, "Toys & Games"

    .line 245
    .line 246
    const-string v3, "TOYS_AND_GAMES"

    .line 247
    .line 248
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 249
    .line 250
    .line 251
    sput-object v0, Lio/branch/referral/util/ProductCategory;->TOYS_AND_GAMES:Lio/branch/referral/util/ProductCategory;

    .line 252
    .line 253
    new-instance v0, Lio/branch/referral/util/ProductCategory;

    .line 254
    .line 255
    const/16 v1, 0x14

    .line 256
    .line 257
    const-string v2, "Vehicles & Parts"

    .line 258
    .line 259
    const-string v3, "VEHICLES_AND_PARTS"

    .line 260
    .line 261
    invoke-direct {v0, v3, v1, v2}, Lio/branch/referral/util/ProductCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 262
    .line 263
    .line 264
    sput-object v0, Lio/branch/referral/util/ProductCategory;->VEHICLES_AND_PARTS:Lio/branch/referral/util/ProductCategory;

    .line 265
    .line 266
    invoke-static {}, Lio/branch/referral/util/ProductCategory;->$values()[Lio/branch/referral/util/ProductCategory;

    .line 267
    .line 268
    .line 269
    move-result-object v0

    .line 270
    sput-object v0, Lio/branch/referral/util/ProductCategory;->$VALUES:[Lio/branch/referral/util/ProductCategory;

    .line 271
    .line 272
    return-void
    .line 273
    .line 274
    .line 275
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lio/branch/referral/util/ProductCategory;->name:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
.end method

.method public static getValue(Ljava/lang/String;)Lio/branch/referral/util/ProductCategory;
    .locals 5

    .line 1
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-static {}, Lio/branch/referral/util/ProductCategory;->values()[Lio/branch/referral/util/ProductCategory;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    array-length v1, v0

    .line 12
    const/4 v2, 0x0

    .line 13
    :goto_0
    if-ge v2, v1, :cond_1

    .line 14
    .line 15
    aget-object v3, v0, v2

    .line 16
    .line 17
    iget-object v4, v3, Lio/branch/referral/util/ProductCategory;->name:Ljava/lang/String;

    .line 18
    .line 19
    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    if-eqz v4, :cond_0

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const/4 v3, 0x0

    .line 30
    :goto_1
    return-object v3
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public static valueOf(Ljava/lang/String;)Lio/branch/referral/util/ProductCategory;
    .locals 1

    .line 1
    const-class v0, Lio/branch/referral/util/ProductCategory;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lio/branch/referral/util/ProductCategory;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lio/branch/referral/util/ProductCategory;
    .locals 1

    .line 1
    sget-object v0, Lio/branch/referral/util/ProductCategory;->$VALUES:[Lio/branch/referral/util/ProductCategory;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lio/branch/referral/util/ProductCategory;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lio/branch/referral/util/ProductCategory;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lio/branch/referral/util/ProductCategory;->name:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

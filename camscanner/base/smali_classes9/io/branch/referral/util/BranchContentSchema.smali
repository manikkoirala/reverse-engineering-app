.class public final enum Lio/branch/referral/util/BranchContentSchema;
.super Ljava/lang/Enum;
.source "BranchContentSchema.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/branch/referral/util/BranchContentSchema;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_AUCTION:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_BUSINESS:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_OTHER:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_PRODUCT:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_RESTAURANT:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_SERVICE:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_TRAVEL_FLIGHT:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_TRAVEL_HOTEL:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum COMMERCE_TRAVEL_OTHER:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum GAME_STATE:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum MEDIA_IMAGE:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum MEDIA_MIXED:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum MEDIA_MUSIC:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum MEDIA_OTHER:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum MEDIA_VIDEO:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum OTHER:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_ARTICLE:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_BLOG:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_OTHER:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_RECIPE:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_REVIEW:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_SEARCH_RESULTS:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_STORY:Lio/branch/referral/util/BranchContentSchema;

.field public static final enum TEXT_TECHNICAL_DOC:Lio/branch/referral/util/BranchContentSchema;


# direct methods
.method private static synthetic $values()[Lio/branch/referral/util/BranchContentSchema;
    .locals 3

    .line 1
    const/16 v0, 0x18

    .line 2
    .line 3
    new-array v0, v0, [Lio/branch/referral/util/BranchContentSchema;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_AUCTION:Lio/branch/referral/util/BranchContentSchema;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_BUSINESS:Lio/branch/referral/util/BranchContentSchema;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_PRODUCT:Lio/branch/referral/util/BranchContentSchema;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_RESTAURANT:Lio/branch/referral/util/BranchContentSchema;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_SERVICE:Lio/branch/referral/util/BranchContentSchema;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_FLIGHT:Lio/branch/referral/util/BranchContentSchema;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_HOTEL:Lio/branch/referral/util/BranchContentSchema;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    const/16 v1, 0x9

    .line 52
    .line 53
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->GAME_STATE:Lio/branch/referral/util/BranchContentSchema;

    .line 54
    .line 55
    aput-object v2, v0, v1

    .line 56
    .line 57
    const/16 v1, 0xa

    .line 58
    .line 59
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->MEDIA_IMAGE:Lio/branch/referral/util/BranchContentSchema;

    .line 60
    .line 61
    aput-object v2, v0, v1

    .line 62
    .line 63
    const/16 v1, 0xb

    .line 64
    .line 65
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->MEDIA_MIXED:Lio/branch/referral/util/BranchContentSchema;

    .line 66
    .line 67
    aput-object v2, v0, v1

    .line 68
    .line 69
    const/16 v1, 0xc

    .line 70
    .line 71
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->MEDIA_MUSIC:Lio/branch/referral/util/BranchContentSchema;

    .line 72
    .line 73
    aput-object v2, v0, v1

    .line 74
    .line 75
    const/16 v1, 0xd

    .line 76
    .line 77
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->MEDIA_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 78
    .line 79
    aput-object v2, v0, v1

    .line 80
    .line 81
    const/16 v1, 0xe

    .line 82
    .line 83
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->MEDIA_VIDEO:Lio/branch/referral/util/BranchContentSchema;

    .line 84
    .line 85
    aput-object v2, v0, v1

    .line 86
    .line 87
    const/16 v1, 0xf

    .line 88
    .line 89
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 90
    .line 91
    aput-object v2, v0, v1

    .line 92
    .line 93
    const/16 v1, 0x10

    .line 94
    .line 95
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->TEXT_ARTICLE:Lio/branch/referral/util/BranchContentSchema;

    .line 96
    .line 97
    aput-object v2, v0, v1

    .line 98
    .line 99
    const/16 v1, 0x11

    .line 100
    .line 101
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->TEXT_BLOG:Lio/branch/referral/util/BranchContentSchema;

    .line 102
    .line 103
    aput-object v2, v0, v1

    .line 104
    .line 105
    const/16 v1, 0x12

    .line 106
    .line 107
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->TEXT_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 108
    .line 109
    aput-object v2, v0, v1

    .line 110
    .line 111
    const/16 v1, 0x13

    .line 112
    .line 113
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->TEXT_RECIPE:Lio/branch/referral/util/BranchContentSchema;

    .line 114
    .line 115
    aput-object v2, v0, v1

    .line 116
    .line 117
    const/16 v1, 0x14

    .line 118
    .line 119
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->TEXT_REVIEW:Lio/branch/referral/util/BranchContentSchema;

    .line 120
    .line 121
    aput-object v2, v0, v1

    .line 122
    .line 123
    const/16 v1, 0x15

    .line 124
    .line 125
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->TEXT_SEARCH_RESULTS:Lio/branch/referral/util/BranchContentSchema;

    .line 126
    .line 127
    aput-object v2, v0, v1

    .line 128
    .line 129
    const/16 v1, 0x16

    .line 130
    .line 131
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->TEXT_STORY:Lio/branch/referral/util/BranchContentSchema;

    .line 132
    .line 133
    aput-object v2, v0, v1

    .line 134
    .line 135
    const/16 v1, 0x17

    .line 136
    .line 137
    sget-object v2, Lio/branch/referral/util/BranchContentSchema;->TEXT_TECHNICAL_DOC:Lio/branch/referral/util/BranchContentSchema;

    .line 138
    .line 139
    aput-object v2, v0, v1

    .line 140
    .line 141
    return-object v0
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 2
    .line 3
    const-string v1, "COMMERCE_AUCTION"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_AUCTION:Lio/branch/referral/util/BranchContentSchema;

    .line 10
    .line 11
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 12
    .line 13
    const-string v1, "COMMERCE_BUSINESS"

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_BUSINESS:Lio/branch/referral/util/BranchContentSchema;

    .line 20
    .line 21
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 22
    .line 23
    const-string v1, "COMMERCE_OTHER"

    .line 24
    .line 25
    const/4 v2, 0x2

    .line 26
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 30
    .line 31
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 32
    .line 33
    const-string v1, "COMMERCE_PRODUCT"

    .line 34
    .line 35
    const/4 v2, 0x3

    .line 36
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_PRODUCT:Lio/branch/referral/util/BranchContentSchema;

    .line 40
    .line 41
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 42
    .line 43
    const-string v1, "COMMERCE_RESTAURANT"

    .line 44
    .line 45
    const/4 v2, 0x4

    .line 46
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 47
    .line 48
    .line 49
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_RESTAURANT:Lio/branch/referral/util/BranchContentSchema;

    .line 50
    .line 51
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 52
    .line 53
    const-string v1, "COMMERCE_SERVICE"

    .line 54
    .line 55
    const/4 v2, 0x5

    .line 56
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 57
    .line 58
    .line 59
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_SERVICE:Lio/branch/referral/util/BranchContentSchema;

    .line 60
    .line 61
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 62
    .line 63
    const-string v1, "COMMERCE_TRAVEL_FLIGHT"

    .line 64
    .line 65
    const/4 v2, 0x6

    .line 66
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 67
    .line 68
    .line 69
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_FLIGHT:Lio/branch/referral/util/BranchContentSchema;

    .line 70
    .line 71
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 72
    .line 73
    const-string v1, "COMMERCE_TRAVEL_HOTEL"

    .line 74
    .line 75
    const/4 v2, 0x7

    .line 76
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 77
    .line 78
    .line 79
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_HOTEL:Lio/branch/referral/util/BranchContentSchema;

    .line 80
    .line 81
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 82
    .line 83
    const-string v1, "COMMERCE_TRAVEL_OTHER"

    .line 84
    .line 85
    const/16 v2, 0x8

    .line 86
    .line 87
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 88
    .line 89
    .line 90
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->COMMERCE_TRAVEL_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 91
    .line 92
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 93
    .line 94
    const-string v1, "GAME_STATE"

    .line 95
    .line 96
    const/16 v2, 0x9

    .line 97
    .line 98
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 99
    .line 100
    .line 101
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->GAME_STATE:Lio/branch/referral/util/BranchContentSchema;

    .line 102
    .line 103
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 104
    .line 105
    const-string v1, "MEDIA_IMAGE"

    .line 106
    .line 107
    const/16 v2, 0xa

    .line 108
    .line 109
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 110
    .line 111
    .line 112
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->MEDIA_IMAGE:Lio/branch/referral/util/BranchContentSchema;

    .line 113
    .line 114
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 115
    .line 116
    const-string v1, "MEDIA_MIXED"

    .line 117
    .line 118
    const/16 v2, 0xb

    .line 119
    .line 120
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 121
    .line 122
    .line 123
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->MEDIA_MIXED:Lio/branch/referral/util/BranchContentSchema;

    .line 124
    .line 125
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 126
    .line 127
    const-string v1, "MEDIA_MUSIC"

    .line 128
    .line 129
    const/16 v2, 0xc

    .line 130
    .line 131
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 132
    .line 133
    .line 134
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->MEDIA_MUSIC:Lio/branch/referral/util/BranchContentSchema;

    .line 135
    .line 136
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 137
    .line 138
    const-string v1, "MEDIA_OTHER"

    .line 139
    .line 140
    const/16 v2, 0xd

    .line 141
    .line 142
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 143
    .line 144
    .line 145
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->MEDIA_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 146
    .line 147
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 148
    .line 149
    const-string v1, "MEDIA_VIDEO"

    .line 150
    .line 151
    const/16 v2, 0xe

    .line 152
    .line 153
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 154
    .line 155
    .line 156
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->MEDIA_VIDEO:Lio/branch/referral/util/BranchContentSchema;

    .line 157
    .line 158
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 159
    .line 160
    const-string v1, "OTHER"

    .line 161
    .line 162
    const/16 v2, 0xf

    .line 163
    .line 164
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 165
    .line 166
    .line 167
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 168
    .line 169
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 170
    .line 171
    const-string v1, "TEXT_ARTICLE"

    .line 172
    .line 173
    const/16 v2, 0x10

    .line 174
    .line 175
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 176
    .line 177
    .line 178
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_ARTICLE:Lio/branch/referral/util/BranchContentSchema;

    .line 179
    .line 180
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 181
    .line 182
    const-string v1, "TEXT_BLOG"

    .line 183
    .line 184
    const/16 v2, 0x11

    .line 185
    .line 186
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 187
    .line 188
    .line 189
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_BLOG:Lio/branch/referral/util/BranchContentSchema;

    .line 190
    .line 191
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 192
    .line 193
    const-string v1, "TEXT_OTHER"

    .line 194
    .line 195
    const/16 v2, 0x12

    .line 196
    .line 197
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 198
    .line 199
    .line 200
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_OTHER:Lio/branch/referral/util/BranchContentSchema;

    .line 201
    .line 202
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 203
    .line 204
    const-string v1, "TEXT_RECIPE"

    .line 205
    .line 206
    const/16 v2, 0x13

    .line 207
    .line 208
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 209
    .line 210
    .line 211
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_RECIPE:Lio/branch/referral/util/BranchContentSchema;

    .line 212
    .line 213
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 214
    .line 215
    const-string v1, "TEXT_REVIEW"

    .line 216
    .line 217
    const/16 v2, 0x14

    .line 218
    .line 219
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 220
    .line 221
    .line 222
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_REVIEW:Lio/branch/referral/util/BranchContentSchema;

    .line 223
    .line 224
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 225
    .line 226
    const-string v1, "TEXT_SEARCH_RESULTS"

    .line 227
    .line 228
    const/16 v2, 0x15

    .line 229
    .line 230
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 231
    .line 232
    .line 233
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_SEARCH_RESULTS:Lio/branch/referral/util/BranchContentSchema;

    .line 234
    .line 235
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 236
    .line 237
    const-string v1, "TEXT_STORY"

    .line 238
    .line 239
    const/16 v2, 0x16

    .line 240
    .line 241
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 242
    .line 243
    .line 244
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_STORY:Lio/branch/referral/util/BranchContentSchema;

    .line 245
    .line 246
    new-instance v0, Lio/branch/referral/util/BranchContentSchema;

    .line 247
    .line 248
    const-string v1, "TEXT_TECHNICAL_DOC"

    .line 249
    .line 250
    const/16 v2, 0x17

    .line 251
    .line 252
    invoke-direct {v0, v1, v2}, Lio/branch/referral/util/BranchContentSchema;-><init>(Ljava/lang/String;I)V

    .line 253
    .line 254
    .line 255
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->TEXT_TECHNICAL_DOC:Lio/branch/referral/util/BranchContentSchema;

    .line 256
    .line 257
    invoke-static {}, Lio/branch/referral/util/BranchContentSchema;->$values()[Lio/branch/referral/util/BranchContentSchema;

    .line 258
    .line 259
    .line 260
    move-result-object v0

    .line 261
    sput-object v0, Lio/branch/referral/util/BranchContentSchema;->$VALUES:[Lio/branch/referral/util/BranchContentSchema;

    .line 262
    .line 263
    return-void
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static getValue(Ljava/lang/String;)Lio/branch/referral/util/BranchContentSchema;
    .locals 5

    .line 1
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-static {}, Lio/branch/referral/util/BranchContentSchema;->values()[Lio/branch/referral/util/BranchContentSchema;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    array-length v1, v0

    .line 12
    const/4 v2, 0x0

    .line 13
    :goto_0
    if-ge v2, v1, :cond_1

    .line 14
    .line 15
    aget-object v3, v0, v2

    .line 16
    .line 17
    invoke-virtual {v3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v4

    .line 21
    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 22
    .line 23
    .line 24
    move-result v4

    .line 25
    if-eqz v4, :cond_0

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    const/4 v3, 0x0

    .line 32
    :goto_1
    return-object v3
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method public static valueOf(Ljava/lang/String;)Lio/branch/referral/util/BranchContentSchema;
    .locals 1

    .line 1
    const-class v0, Lio/branch/referral/util/BranchContentSchema;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lio/branch/referral/util/BranchContentSchema;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method public static values()[Lio/branch/referral/util/BranchContentSchema;
    .locals 1

    .line 1
    sget-object v0, Lio/branch/referral/util/BranchContentSchema;->$VALUES:[Lio/branch/referral/util/BranchContentSchema;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lio/branch/referral/util/BranchContentSchema;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lio/branch/referral/util/BranchContentSchema;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

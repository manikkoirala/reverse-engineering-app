.class public Lio/branch/referral/validators/IntegrationValidator;
.super Ljava/lang/Object;
.source "IntegrationValidator.java"

# interfaces
.implements Lio/branch/referral/validators/ServerRequestGetAppConfig$IGetAppConfigEvents;


# static fields
.field private static 〇o〇:Lio/branch/referral/validators/IntegrationValidator;


# instance fields
.field private final 〇080:Lio/branch/referral/validators/BranchIntegrationModel;

.field private final 〇o00〇〇Oo:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "BranchSDK_Doctor"

    .line 5
    .line 6
    iput-object v0, p0, Lio/branch/referral/validators/IntegrationValidator;->〇o00〇〇Oo:Ljava/lang/String;

    .line 7
    .line 8
    new-instance v0, Lio/branch/referral/validators/BranchIntegrationModel;

    .line 9
    .line 10
    invoke-direct {v0, p1}, Lio/branch/referral/validators/BranchIntegrationModel;-><init>(Landroid/content/Context;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private O8(Lorg/json/JSONObject;)V
    .locals 7

    .line 1
    const-string v0, "3. Verifying application package name"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 7
    .line 8
    iget-object v0, v0, Lio/branch/referral/validators/BranchIntegrationModel;->Oo08:Ljava/lang/String;

    .line 9
    .line 10
    const-string v1, "android_package_name"

    .line 11
    .line 12
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    const-string v1, "https://help.branch.io/developers-hub/docs/android-basic-integration#section-configure-branch-dashboard"

    .line 21
    .line 22
    if-nez v0, :cond_0

    .line 23
    .line 24
    const-string p1, "Incorrect package name in Branch dashboard. Please correct your package name in dashboard -> Configuration page."

    .line 25
    .line 26
    invoke-direct {p0, p1, v1}, Lio/branch/referral/validators/IntegrationValidator;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return-void

    .line 30
    :cond_0
    invoke-direct {p0}, Lio/branch/referral/validators/IntegrationValidator;->o〇0()V

    .line 31
    .line 32
    .line 33
    const-string v0, "4. Checking Android Manifest for URI based deep link config"

    .line 34
    .line 35
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 39
    .line 40
    iget-object v0, v0, Lio/branch/referral/validators/BranchIntegrationModel;->〇080:Lorg/json/JSONObject;

    .line 41
    .line 42
    const-string v2, "android_uri_scheme"

    .line 43
    .line 44
    const-string v3, "https://help.branch.io/developers-hub/docs/android-basic-integration#section-configure-app"

    .line 45
    .line 46
    const/4 v4, 0x0

    .line 47
    const/4 v5, 0x1

    .line 48
    if-eqz v0, :cond_2

    .line 49
    .line 50
    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-nez v0, :cond_1

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_1
    invoke-direct {p0}, Lio/branch/referral/validators/IntegrationValidator;->o〇0()V

    .line 58
    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_2
    :goto_0
    iget-object v0, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 62
    .line 63
    iget-boolean v0, v0, Lio/branch/referral/validators/BranchIntegrationModel;->o〇0:Z

    .line 64
    .line 65
    if-nez v0, :cond_e

    .line 66
    .line 67
    const-string v0, "- Skipping. Unable to verify the deep link config. Failed to read the Android Manifest"

    .line 68
    .line 69
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    :goto_1
    const-string v0, "5. Verifying URI based deep link config with Branch dash board."

    .line 73
    .line 74
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 82
    .line 83
    .line 84
    move-result v2

    .line 85
    if-eqz v2, :cond_3

    .line 86
    .line 87
    const-string p1, "Uri Scheme to open your app is not specified in Branch dashboard. Please add URI scheme in Branch dashboard."

    .line 88
    .line 89
    invoke-direct {p0, p1, v1}, Lio/branch/referral/validators/IntegrationValidator;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    return-void

    .line 93
    :cond_3
    invoke-direct {p0}, Lio/branch/referral/validators/IntegrationValidator;->o〇0()V

    .line 94
    .line 95
    .line 96
    const-string v2, "6. Verifying intent for receiving URI scheme."

    .line 97
    .line 98
    invoke-direct {p0, v2}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇o〇(Ljava/lang/String;)Z

    .line 102
    .line 103
    .line 104
    move-result v2

    .line 105
    const-string v6, "- Skipping. Unable to verify intent for receiving URI scheme. Failed to read the Android Manifest"

    .line 106
    .line 107
    if-nez v2, :cond_5

    .line 108
    .line 109
    iget-object v2, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 110
    .line 111
    iget-boolean v2, v2, Lio/branch/referral/validators/BranchIntegrationModel;->o〇0:Z

    .line 112
    .line 113
    if-nez v2, :cond_4

    .line 114
    .line 115
    invoke-direct {p0, v6}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    goto :goto_2

    .line 119
    :cond_4
    new-array p1, v5, [Ljava/lang/Object;

    .line 120
    .line 121
    aput-object v0, p1, v4

    .line 122
    .line 123
    const-string v0, "Uri scheme \'%s\' specified in Branch dashboard doesn\'t match with the deep link intent in manifest file"

    .line 124
    .line 125
    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object p1

    .line 129
    invoke-direct {p0, p1, v1}, Lio/branch/referral/validators/IntegrationValidator;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    return-void

    .line 133
    :cond_5
    invoke-direct {p0}, Lio/branch/referral/validators/IntegrationValidator;->o〇0()V

    .line 134
    .line 135
    .line 136
    :goto_2
    const-string v0, "7. Checking AndroidManifest for AppLink config."

    .line 137
    .line 138
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    iget-object v0, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 142
    .line 143
    iget-object v0, v0, Lio/branch/referral/validators/BranchIntegrationModel;->O8:Ljava/util/List;

    .line 144
    .line 145
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 146
    .line 147
    .line 148
    move-result v0

    .line 149
    if-eqz v0, :cond_7

    .line 150
    .line 151
    iget-object v0, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 152
    .line 153
    iget-boolean v0, v0, Lio/branch/referral/validators/BranchIntegrationModel;->o〇0:Z

    .line 154
    .line 155
    if-nez v0, :cond_6

    .line 156
    .line 157
    invoke-direct {p0, v6}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    goto :goto_3

    .line 161
    :cond_6
    const-string p1, "Could not find any App Link hosts to support Android AppLinks. Please add intent filter for handling AppLinks in your Android Manifest file"

    .line 162
    .line 163
    const-string v0, "https://help.branch.io/using-branch/docs/android-app-links#section-add-intent-filter-to-manifest"

    .line 164
    .line 165
    invoke-direct {p0, p1, v0}, Lio/branch/referral/validators/IntegrationValidator;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    return-void

    .line 169
    :cond_7
    invoke-direct {p0}, Lio/branch/referral/validators/IntegrationValidator;->o〇0()V

    .line 170
    .line 171
    .line 172
    :goto_3
    const-string v0, "8. Verifying any supported custom link domains."

    .line 173
    .line 174
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    const-string/jumbo v0, "short_url_domain"

    .line 178
    .line 179
    .line 180
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 181
    .line 182
    .line 183
    move-result-object v0

    .line 184
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 185
    .line 186
    .line 187
    move-result v1

    .line 188
    if-nez v1, :cond_9

    .line 189
    .line 190
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇o00〇〇Oo(Ljava/lang/String;)Z

    .line 191
    .line 192
    .line 193
    move-result v1

    .line 194
    if-nez v1, :cond_9

    .line 195
    .line 196
    iget-object v1, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 197
    .line 198
    iget-boolean v1, v1, Lio/branch/referral/validators/BranchIntegrationModel;->o〇0:Z

    .line 199
    .line 200
    if-nez v1, :cond_8

    .line 201
    .line 202
    const-string v0, "- Skipping. Unable to verify supported custom link domains. Failed to read the Android Manifest"

    .line 203
    .line 204
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    goto :goto_4

    .line 208
    :cond_8
    new-array p1, v5, [Ljava/lang/Object;

    .line 209
    .line 210
    aput-object v0, p1, v4

    .line 211
    .line 212
    const-string v0, "Could not find intent filter to support custom link domain \'%s\'. Please add intent filter for handling custom link domain in your Android Manifest file "

    .line 213
    .line 214
    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 215
    .line 216
    .line 217
    move-result-object p1

    .line 218
    invoke-direct {p0, p1, v3}, Lio/branch/referral/validators/IntegrationValidator;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    return-void

    .line 222
    :cond_9
    invoke-direct {p0}, Lio/branch/referral/validators/IntegrationValidator;->o〇0()V

    .line 223
    .line 224
    .line 225
    :goto_4
    const-string v0, "9. Verifying default link domains integrations."

    .line 226
    .line 227
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 228
    .line 229
    .line 230
    const-string v0, "default_short_url_domain"

    .line 231
    .line 232
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object v0

    .line 236
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 237
    .line 238
    .line 239
    move-result v1

    .line 240
    if-nez v1, :cond_b

    .line 241
    .line 242
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇o00〇〇Oo(Ljava/lang/String;)Z

    .line 243
    .line 244
    .line 245
    move-result v1

    .line 246
    if-nez v1, :cond_b

    .line 247
    .line 248
    iget-object v1, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 249
    .line 250
    iget-boolean v1, v1, Lio/branch/referral/validators/BranchIntegrationModel;->o〇0:Z

    .line 251
    .line 252
    if-nez v1, :cond_a

    .line 253
    .line 254
    const-string v0, "- Skipping. Unable to verify default link domains. Failed to read the Android Manifest"

    .line 255
    .line 256
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 257
    .line 258
    .line 259
    goto :goto_5

    .line 260
    :cond_a
    new-array p1, v5, [Ljava/lang/Object;

    .line 261
    .line 262
    aput-object v0, p1, v4

    .line 263
    .line 264
    const-string v0, "Could not find intent filter to support Branch default link domain \'%s\'. Please add intent filter for handling custom link domain in your Android Manifest file "

    .line 265
    .line 266
    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 267
    .line 268
    .line 269
    move-result-object p1

    .line 270
    invoke-direct {p0, p1, v3}, Lio/branch/referral/validators/IntegrationValidator;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    .line 272
    .line 273
    return-void

    .line 274
    :cond_b
    invoke-direct {p0}, Lio/branch/referral/validators/IntegrationValidator;->o〇0()V

    .line 275
    .line 276
    .line 277
    :goto_5
    const-string v0, "10. Verifying alternate link domains integrations."

    .line 278
    .line 279
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 280
    .line 281
    .line 282
    const-string v0, "alternate_short_url_domain"

    .line 283
    .line 284
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 285
    .line 286
    .line 287
    move-result-object p1

    .line 288
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 289
    .line 290
    .line 291
    move-result v0

    .line 292
    if-nez v0, :cond_d

    .line 293
    .line 294
    invoke-direct {p0, p1}, Lio/branch/referral/validators/IntegrationValidator;->〇o00〇〇Oo(Ljava/lang/String;)Z

    .line 295
    .line 296
    .line 297
    move-result v0

    .line 298
    if-nez v0, :cond_d

    .line 299
    .line 300
    iget-object v0, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 301
    .line 302
    iget-boolean v0, v0, Lio/branch/referral/validators/BranchIntegrationModel;->o〇0:Z

    .line 303
    .line 304
    if-nez v0, :cond_c

    .line 305
    .line 306
    const-string p1, "- Skipping.Unable to verify alternate link domains. Failed to read the Android Manifest"

    .line 307
    .line 308
    invoke-direct {p0, p1}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 309
    .line 310
    .line 311
    goto :goto_6

    .line 312
    :cond_c
    new-array v0, v5, [Ljava/lang/Object;

    .line 313
    .line 314
    aput-object p1, v0, v4

    .line 315
    .line 316
    const-string p1, "Could not find intent filter to support alternate link domain \'%s\'. Please add intent filter for handling custom link domain in your Android Manifest file "

    .line 317
    .line 318
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 319
    .line 320
    .line 321
    move-result-object p1

    .line 322
    invoke-direct {p0, p1, v3}, Lio/branch/referral/validators/IntegrationValidator;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .line 324
    .line 325
    return-void

    .line 326
    :cond_d
    invoke-direct {p0}, Lio/branch/referral/validators/IntegrationValidator;->o〇0()V

    .line 327
    .line 328
    .line 329
    :goto_6
    invoke-direct {p0}, Lio/branch/referral/validators/IntegrationValidator;->o〇0()V

    .line 330
    .line 331
    .line 332
    return-void

    .line 333
    :cond_e
    new-array v0, v5, [Ljava/lang/Object;

    .line 334
    .line 335
    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 336
    .line 337
    .line 338
    move-result-object p1

    .line 339
    aput-object p1, v0, v4

    .line 340
    .line 341
    const-string p1, "No intent found for opening the app through uri Scheme \'%s\'.Please add the intent with URI scheme to your Android manifest."

    .line 342
    .line 343
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 344
    .line 345
    .line 346
    move-result-object p1

    .line 347
    invoke-direct {p0, p1, v3}, Lio/branch/referral/validators/IntegrationValidator;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    .line 349
    .line 350
    return-void
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method private Oo08(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "** ERROR ** : "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string p1, "\nPlease follow the link for more info "

    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
.end method

.method public static oO80(Landroid/content/Context;)V
    .locals 1

    .line 1
    sget-object v0, Lio/branch/referral/validators/IntegrationValidator;->〇o〇:Lio/branch/referral/validators/IntegrationValidator;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lio/branch/referral/validators/IntegrationValidator;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lio/branch/referral/validators/IntegrationValidator;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lio/branch/referral/validators/IntegrationValidator;->〇o〇:Lio/branch/referral/validators/IntegrationValidator;

    .line 11
    .line 12
    :cond_0
    sget-object v0, Lio/branch/referral/validators/IntegrationValidator;->〇o〇:Lio/branch/referral/validators/IntegrationValidator;

    .line 13
    .line 14
    invoke-direct {v0, p0}, Lio/branch/referral/validators/IntegrationValidator;->〇80〇808〇O(Landroid/content/Context;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.method private o〇0()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
.end method

.method private 〇80〇808〇O(Landroid/content/Context;)V
    .locals 2

    .line 1
    const-string v0, "\n\n------------------- Initiating Branch integration verification ---------------------------"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "1. Verifying Branch instance creation"

    .line 7
    .line 8
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lio/branch/referral/Branch;->〇8〇0〇o〇O()Lio/branch/referral/Branch;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    const-string p1, "Branch is not initialised from your Application class. Please add `Branch.getAutoInstance(this);` to your Application#onCreate() method."

    .line 18
    .line 19
    const-string v0, "https://help.branch.io/developers-hub/docs/android-basic-integration#section-load-branch"

    .line 20
    .line 21
    invoke-direct {p0, p1, v0}, Lio/branch/referral/validators/IntegrationValidator;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    return-void

    .line 25
    :cond_0
    invoke-direct {p0}, Lio/branch/referral/validators/IntegrationValidator;->o〇0()V

    .line 26
    .line 27
    .line 28
    const-string v0, "2. Checking Branch keys"

    .line 29
    .line 30
    invoke-direct {p0, v0}, Lio/branch/referral/validators/IntegrationValidator;->〇〇888(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-static {p1}, Lio/branch/referral/BranchUtil;->o〇0(Landroid/content/Context;)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-eqz v0, :cond_1

    .line 42
    .line 43
    const-string p1, "Unable to read Branch keys from your application. Did you forget to add Branch keys in your application?."

    .line 44
    .line 45
    const-string v0, "https://help.branch.io/developers-hub/docs/android-basic-integration#section-configure-app"

    .line 46
    .line 47
    invoke-direct {p0, p1, v0}, Lio/branch/referral/validators/IntegrationValidator;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    return-void

    .line 51
    :cond_1
    invoke-direct {p0}, Lio/branch/referral/validators/IntegrationValidator;->o〇0()V

    .line 52
    .line 53
    .line 54
    invoke-static {}, Lio/branch/referral/Branch;->〇8〇0〇o〇O()Lio/branch/referral/Branch;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    new-instance v1, Lio/branch/referral/validators/ServerRequestGetAppConfig;

    .line 59
    .line 60
    invoke-direct {v1, p1, p0}, Lio/branch/referral/validators/ServerRequestGetAppConfig;-><init>(Landroid/content/Context;Lio/branch/referral/validators/ServerRequestGetAppConfig$IGetAppConfigEvents;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v1}, Lio/branch/referral/Branch;->〇O〇80o08O(Lio/branch/referral/ServerRequest;)V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private 〇o00〇〇Oo(Ljava/lang/String;)Z
    .locals 2

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 8
    .line 9
    iget-object v0, v0, Lio/branch/referral/validators/BranchIntegrationModel;->O8:Ljava/util/List;

    .line 10
    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Ljava/lang/String;

    .line 28
    .line 29
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-eqz v1, :cond_0

    .line 34
    .line 35
    const/4 p1, 0x1

    .line 36
    goto :goto_0

    .line 37
    :cond_1
    const/4 p1, 0x0

    .line 38
    :goto_0
    return p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
.end method

.method private 〇o〇(Ljava/lang/String;)Z
    .locals 8

    .line 1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    const-string p1, "open"

    .line 20
    .line 21
    :cond_0
    iget-object v1, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 22
    .line 23
    iget-object v1, v1, Lio/branch/referral/validators/BranchIntegrationModel;->〇080:Lorg/json/JSONObject;

    .line 24
    .line 25
    const/4 v2, 0x0

    .line 26
    if-eqz v1, :cond_5

    .line 27
    .line 28
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const/4 v3, 0x0

    .line 33
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    if-eqz v4, :cond_4

    .line 38
    .line 39
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    check-cast v4, Ljava/lang/String;

    .line 44
    .line 45
    if-eqz v0, :cond_1

    .line 46
    .line 47
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    move-result v5

    .line 51
    if-eqz v5, :cond_1

    .line 52
    .line 53
    iget-object v5, p0, Lio/branch/referral/validators/IntegrationValidator;->〇080:Lio/branch/referral/validators/BranchIntegrationModel;

    .line 54
    .line 55
    iget-object v5, v5, Lio/branch/referral/validators/BranchIntegrationModel;->〇080:Lorg/json/JSONObject;

    .line 56
    .line 57
    invoke-virtual {v5, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    .line 58
    .line 59
    .line 60
    move-result-object v4

    .line 61
    const/4 v5, 0x1

    .line 62
    if-eqz v4, :cond_3

    .line 63
    .line 64
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    .line 65
    .line 66
    .line 67
    move-result v6

    .line 68
    if-lez v6, :cond_3

    .line 69
    .line 70
    const/4 v6, 0x0

    .line 71
    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    .line 72
    .line 73
    .line 74
    move-result v7

    .line 75
    if-ge v6, v7, :cond_1

    .line 76
    .line 77
    if-eqz p1, :cond_2

    .line 78
    .line 79
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v7

    .line 83
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    move-result v7

    .line 87
    if-eqz v7, :cond_2

    .line 88
    .line 89
    const/4 v3, 0x1

    .line 90
    goto :goto_0

    .line 91
    :cond_2
    add-int/lit8 v6, v6, 0x1

    .line 92
    .line 93
    goto :goto_1

    .line 94
    :cond_3
    const/4 v2, 0x1

    .line 95
    goto :goto_2

    .line 96
    :cond_4
    move v2, v3

    .line 97
    :cond_5
    :goto_2
    return v2
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
.end method

.method private 〇〇888(Ljava/lang/String;)V
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7
    .line 8
    .line 9
    const-string p1, " ... "

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method


# virtual methods
.method public 〇080(Lorg/json/JSONObject;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-direct {p0, p1}, Lio/branch/referral/validators/IntegrationValidator;->O8(Lorg/json/JSONObject;)V

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const-string p1, "Unable to read Dashboard config. Please confirm that your Branch key is properly added to the manifest. Please fix your Dashboard settings."

    .line 8
    .line 9
    const-string v0, "https://branch.app.link/link-settings-page"

    .line 10
    .line 11
    invoke-direct {p0, p1, v0}, Lio/branch/referral/validators/IntegrationValidator;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    :goto_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
.end method

.class public final Lio/branch/referral/R$attr;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/branch/referral/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final allowShortcuts:I = 0x7f040059

.field public static final alpha:I = 0x7f04005b

.field public static final buttonSize:I = 0x7f0400e5

.field public static final circleCrop:I = 0x7f040123

.field public static final colorScheme:I = 0x7f040173

.field public static final contentProviderUri:I = 0x7f0401af

.field public static final coordinatorLayoutStyle:I = 0x7f0401b3

.field public static final corpusId:I = 0x7f0401c0

.field public static final corpusVersion:I = 0x7f0401c1

.field public static final defaultIntentAction:I = 0x7f0401e6

.field public static final defaultIntentActivity:I = 0x7f0401e7

.field public static final defaultIntentData:I = 0x7f0401e8

.field public static final documentMaxAgeSecs:I = 0x7f040208

.field public static final featureType:I = 0x7f040297

.field public static final font:I = 0x7f0402c7

.field public static final fontProviderAuthority:I = 0x7f0402c9

.field public static final fontProviderCerts:I = 0x7f0402ca

.field public static final fontProviderFetchStrategy:I = 0x7f0402cb

.field public static final fontProviderFetchTimeout:I = 0x7f0402cc

.field public static final fontProviderPackage:I = 0x7f0402cd

.field public static final fontProviderQuery:I = 0x7f0402ce

.field public static final fontStyle:I = 0x7f0402d0

.field public static final fontVariationSettings:I = 0x7f0402d1

.field public static final fontWeight:I = 0x7f0402d2

.field public static final imageAspectRatio:I = 0x7f04030a

.field public static final imageAspectRatioAdjust:I = 0x7f04030b

.field public static final indexPrefixes:I = 0x7f04031e

.field public static final inputEnabled:I = 0x7f040328

.field public static final keylines:I = 0x7f04036d

.field public static final layout_anchor:I = 0x7f040379

.field public static final layout_anchorGravity:I = 0x7f04037a

.field public static final layout_behavior:I = 0x7f04037b

.field public static final layout_dodgeInsetEdges:I = 0x7f0403ac

.field public static final layout_insetEdge:I = 0x7f0403b9

.field public static final layout_keyline:I = 0x7f0403ba

.field public static final noIndex:I = 0x7f0404a1

.field public static final paramName:I = 0x7f0404c0

.field public static final paramValue:I = 0x7f0404c1

.field public static final perAccountTemplate:I = 0x7f0404c9

.field public static final schemaOrgProperty:I = 0x7f04051b

.field public static final schemaOrgType:I = 0x7f04051c

.field public static final scopeUris:I = 0x7f04051d

.field public static final searchEnabled:I = 0x7f040522

.field public static final searchLabel:I = 0x7f040525

.field public static final sectionContent:I = 0x7f040528

.field public static final sectionFormat:I = 0x7f040529

.field public static final sectionId:I = 0x7f04052a

.field public static final sectionType:I = 0x7f04052b

.field public static final sectionWeight:I = 0x7f04052c

.field public static final semanticallySearchable:I = 0x7f040536

.field public static final settingsDescription:I = 0x7f040538

.field public static final sourceClass:I = 0x7f040579

.field public static final statusBarBackground:I = 0x7f04059a

.field public static final subsectionSeparator:I = 0x7f0405a8

.field public static final toAddressesSection:I = 0x7f040670

.field public static final trimmable:I = 0x7f040696

.field public static final ttcIndex:I = 0x7f040697

.field public static final userInputSection:I = 0x7f0406a4

.field public static final userInputTag:I = 0x7f0406a5

.field public static final userInputValue:I = 0x7f0406a6


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

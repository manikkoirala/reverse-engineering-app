.class public Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;
.super Ljava/lang/Object;
.source "LoaderCallbackManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;
    }
.end annotation


# instance fields
.field private final O8:I

.field private Oo08:Landroid/database/Cursor;

.field private final 〇080:Landroidx/loader/app/LoaderManager;

.field private 〇o00〇〇Oo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/loader/app/LoaderManager$LoaderCallbacks<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;


# direct methods
.method public constructor <init>(Landroidx/loader/app/LoaderManager;Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->Oo08:Landroid/database/Cursor;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇080:Landroidx/loader/app/LoaderManager;

    .line 8
    .line 9
    iput-object p2, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

    .line 10
    .line 11
    iput p3, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->O8:I

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private O8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇o00〇〇Oo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$1;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$1;-><init>(Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇o00〇〇Oo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private Oo08(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->Oo08:Landroid/database/Cursor;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "LoaderCallbackManager"

    .line 5
    .line 6
    if-ne p1, v0, :cond_0

    .line 7
    .line 8
    const-string p1, "swapCursor newCursor == lastCursor"

    .line 9
    .line 10
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-object v1

    .line 14
    :cond_0
    const-string v0, "swapCursor newCursor != lastCursor"

    .line 15
    .line 16
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->Oo08:Landroid/database/Cursor;

    .line 20
    .line 21
    iput-object p1, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->Oo08:Landroid/database/Cursor;

    .line 22
    .line 23
    if-nez p1, :cond_1

    .line 24
    .line 25
    const-string p1, "lastCursor == null"

    .line 26
    .line 27
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

    .line 31
    .line 32
    invoke-interface {p1, v1}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;->〇o00〇〇Oo(Landroid/database/Cursor;)V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const/4 v1, -0x1

    .line 37
    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 38
    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->Oo08:Landroid/database/Cursor;

    .line 43
    .line 44
    invoke-interface {p1, v1}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;->〇o00〇〇Oo(Landroid/database/Cursor;)V

    .line 45
    .line 46
    .line 47
    :goto_0
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;)Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇o〇(Landroid/database/Cursor;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇o〇(Landroid/database/Cursor;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->Oo08(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public o〇0()V
    .locals 4

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇o00〇〇Oo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->O8()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇080:Landroidx/loader/app/LoaderManager;

    .line 10
    .line 11
    iget v2, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->O8:I

    .line 12
    .line 13
    iget-object v3, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇o00〇〇Oo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 14
    .line 15
    invoke-virtual {v0, v2, v1, v3}, Landroidx/loader/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->〇080:Landroidx/loader/app/LoaderManager;

    .line 20
    .line 21
    iget v3, p0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->O8:I

    .line 22
    .line 23
    invoke-virtual {v2, v3, v1, v0}, Landroidx/loader/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :catch_0
    move-exception v0

    .line 28
    const-string v1, "LoaderCallbackManager"

    .line 29
    .line 30
    const-string v2, "updateLoader"

    .line 31
    .line 32
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 33
    .line 34
    .line 35
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

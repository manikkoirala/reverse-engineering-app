.class public Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "CertificateOcrResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$ItemList;,
        Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$CardOcr;
    }
.end annotation


# instance fields
.field public balance:I

.field public card_ocr:Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$CardOcr;

.field public error_code:I

.field public points:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 3
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    .line 4
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;->parse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;->parse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public static createCertificate(Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$CardOcr;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    if-eqz p0, :cond_5

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$CardOcr;->item_list:[Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$ItemList;

    .line 6
    .line 7
    if-eqz v1, :cond_5

    .line 8
    .line 9
    array-length v1, v1

    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    goto :goto_2

    .line 13
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    .line 14
    .line 15
    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 16
    .line 17
    .line 18
    iget-object p0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$CardOcr;->item_list:[Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$ItemList;

    .line 19
    .line 20
    array-length v2, p0

    .line 21
    const/4 v3, 0x0

    .line 22
    :goto_0
    if-ge v3, v2, :cond_3

    .line 23
    .line 24
    aget-object v4, p0, v3

    .line 25
    .line 26
    iget-object v5, v4, Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$ItemList;->key:Ljava/lang/String;

    .line 27
    .line 28
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 29
    .line 30
    .line 31
    move-result v5

    .line 32
    if-eqz v5, :cond_1

    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_1
    iget-object v5, v4, Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$ItemList;->value:Ljava/lang/String;

    .line 36
    .line 37
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 38
    .line 39
    .line 40
    move-result v5

    .line 41
    if-eqz v5, :cond_2

    .line 42
    .line 43
    goto :goto_1

    .line 44
    :cond_2
    iget-object v5, v4, Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$ItemList;->key:Ljava/lang/String;

    .line 45
    .line 46
    iget-object v4, v4, Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$ItemList;->value:Ljava/lang/String;

    .line 47
    .line 48
    invoke-virtual {v1, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 49
    .line 50
    .line 51
    :goto_1
    add-int/lit8 v3, v3, 0x1

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_3
    invoke-virtual {v1}, Lorg/json/JSONObject;->length()I

    .line 55
    .line 56
    .line 57
    move-result p0

    .line 58
    if-nez p0, :cond_4

    .line 59
    .line 60
    return-object v0

    .line 61
    :cond_4
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object p0

    .line 65
    return-object p0

    .line 66
    :cond_5
    :goto_2
    return-object v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

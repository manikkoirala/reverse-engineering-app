.class public Lcom/intsig/camscanner/certificate_package/datamode/CNTravelData;
.super Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;
.source "CNTravelData.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private engine_no:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateHolderNumberAnnotation;
    .end annotation

    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x2
        stringResId = 0x7f13074f
    .end annotation
.end field

.field private issue_date:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x5
        stringResId = 0x7f130754
    .end annotation
.end field

.field private model:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x3
        stringResId = 0x7f130738
    .end annotation
.end field

.field private owner:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateHolderNameAnnotation;
    .end annotation

    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x0
        stringResId = 0x7f13074c
    .end annotation
.end field

.field private plate_no:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x1
        stringResId = 0x7f13075c
    .end annotation
.end field

.field private register_date:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x4
        stringResId = 0x7f130753
    .end annotation
.end field

.field private vin:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x6
        stringResId = 0x7f130755
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public getCertiType()I
    .locals 1

    .line 1
    const/16 v0, 0x72

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/certificate_package/datamode/CNTravelData;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public preProcessing()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNTravelData;->engine_no:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNTravelData;->engine_no:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNTravelData;->engine_no:Ljava/lang/String;

    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

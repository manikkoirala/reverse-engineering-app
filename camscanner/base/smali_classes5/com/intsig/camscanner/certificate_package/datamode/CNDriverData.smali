.class public Lcom/intsig/camscanner/certificate_package/datamode/CNDriverData;
.super Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;
.source "CNDriverData.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private drive_type:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x2
        stringResId = 0x7f130752
    .end annotation
.end field

.field private driving_license_main_number:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateHolderNumberAnnotation;
    .end annotation

    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x1
        stringResId = 0x7f13073e
    .end annotation
.end field

.field private file_number:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x5
        stringResId = 0x7f130749
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateHolderNameAnnotation;
    .end annotation

    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x0
        stringResId = 0x7f13074c
    .end annotation
.end field

.field private valid_period:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x4
        stringResId = 0x7f130751
    .end annotation
.end field

.field private valid_period_from:Ljava/lang/String;
    .annotation runtime Lcom/intsig/camscanner/certificate_package/util/CertificateItemAnnotation;
        order = 0x3
        stringResId = 0x7f130740
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;-><init>()V

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;-><init>()V

    .line 3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNDriverData;->driving_license_main_number:Ljava/lang/String;

    .line 4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNDriverData;->name:Ljava/lang/String;

    .line 5
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNDriverData;->valid_period:Ljava/lang/String;

    .line 6
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNDriverData;->drive_type:Ljava/lang/String;

    .line 7
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNDriverData;->valid_period_from:Ljava/lang/String;

    .line 8
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNDriverData;->file_number:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCertiType()I
    .locals 1

    .line 1
    const/16 v0, 0x71

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/certificate_package/datamode/CNDriverData;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public preProcessing()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNDriverData;->driving_license_main_number:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNDriverData;->driving_license_main_number:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/datamode/CNDriverData;->driving_license_main_number:Ljava/lang/String;

    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.class public Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult$ItemList;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "CertificateOcrResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/certificate_package/datamode/CertificateOcrResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ItemList"
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public key:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 3
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    .line 4
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/intsig/tianshu/base/BaseJsonObj;->parse(Lorg/json/JSONObject;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>()V

    .line 2
    invoke-virtual {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;->parse(Lorg/json/JSONObject;)V

    return-void
.end method

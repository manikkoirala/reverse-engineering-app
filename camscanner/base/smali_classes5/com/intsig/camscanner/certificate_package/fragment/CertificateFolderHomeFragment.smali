.class public Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;
.super Landroidx/fragment/app/Fragment;
.source "CertificateFolderHomeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter$OnItemClickEventListener;


# static fields
.field private static final oOO〇〇:Ljava/lang/String; = "CertificateFolderHomeFragment"


# instance fields
.field private final O0O:I

.field private O88O:Lcom/intsig/camscanner/tools/MaskDialogTipsClient;

.field private O8o08O8O:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

.field private OO:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;

.field private OO〇00〇8oO:Ljava/util/concurrent/ExecutorService;

.field private o0:Landroid/view/View;

.field private final o8oOOo:I

.field private o8〇OO0〇0o:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

.field private oOo0:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderDiffCallback;

.field private oOo〇8o008:Landroid/os/Handler;

.field private final ooo0〇〇O:I

.field private o〇00O:Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;

.field private 〇080OO8〇0:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

.field private 〇08O〇00〇o:Landroid/view/View;

.field private 〇0O:Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;

.field private 〇8〇oO〇〇8o:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

.field private 〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

.field private final 〇O〇〇O8:I

.field private 〇o0O:I

.field private final 〇〇08O:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇0O:Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;

    .line 10
    .line 11
    new-instance v0, Landroid/os/Handler;

    .line 12
    .line 13
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    new-instance v2, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment$1;

    .line 18
    .line 19
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment$1;-><init>(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;)V

    .line 20
    .line 21
    .line 22
    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOo〇8o008:Landroid/os/Handler;

    .line 26
    .line 27
    new-instance v0, Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderDiffCallback;

    .line 28
    .line 29
    invoke-direct {v0}, Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderDiffCallback;-><init>()V

    .line 30
    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOo0:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderDiffCallback;

    .line 33
    .line 34
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->OO〇00〇8oO:Ljava/util/concurrent/ExecutorService;

    .line 39
    .line 40
    new-instance v0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment$2;

    .line 41
    .line 42
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment$2;-><init>(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;)V

    .line 43
    .line 44
    .line 45
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

    .line 46
    .line 47
    new-instance v0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment$3;

    .line 48
    .line 49
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment$3;-><init>(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;)V

    .line 50
    .line 51
    .line 52
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

    .line 53
    .line 54
    const/4 v0, 0x0

    .line 55
    iput v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->ooo0〇〇O:I

    .line 56
    .line 57
    const/4 v0, 0x1

    .line 58
    iput v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇〇08O:I

    .line 59
    .line 60
    const/4 v0, 0x2

    .line 61
    iput v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->O0O:I

    .line 62
    .line 63
    const/4 v0, 0x3

    .line 64
    iput v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o8oOOo:I

    .line 65
    .line 66
    const/4 v0, 0x4

    .line 67
    iput v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇O〇〇O8:I

    .line 68
    .line 69
    const/4 v0, 0x0

    .line 70
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->O88O:Lcom/intsig/camscanner/tools/MaskDialogTipsClient;

    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method static bridge synthetic O0〇0()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private Ooo8o()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    if-nez v0, :cond_1

    .line 19
    .line 20
    sget-object v0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 21
    .line 22
    const-string v1, "intent == null"

    .line 23
    .line 24
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_1
    const-string v1, "extra_certificate_home_parameter"

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    check-cast v0, Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;

    .line 35
    .line 36
    if-nez v0, :cond_2

    .line 37
    .line 38
    sget-object v0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 39
    .line 40
    const-string v1, "tempParameter == null"

    .line 41
    .line 42
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    return-void

    .line 46
    :cond_2
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇0O:Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;

    .line 47
    .line 48
    return-void

    .line 49
    :cond_3
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 50
    .line 51
    const-string v1, "activity == null || activity.isFinishing()"

    .line 52
    .line 53
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method static bridge synthetic o00〇88〇08(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇088O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private o80ooO(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter$CertificateDocItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "refreshData"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->OO:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;

    .line 9
    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;

    .line 13
    .line 14
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;-><init>(Landroid/content/Context;)V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->OO:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;

    .line 22
    .line 23
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;->o800o8O(Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter$OnItemClickEventListener;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->OO:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;

    .line 27
    .line 28
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;->〇O888o0o(Ljava/util/List;)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->OO:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 36
    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇08O〇00〇o:Landroid/view/View;

    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->OO:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;->〇0〇O0088o()Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    .line 48
    const/4 v0, 0x0

    .line 49
    goto :goto_0

    .line 50
    :cond_0
    const/16 v0, 0x8

    .line 51
    .line 52
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 53
    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->OO〇00〇8oO:Ljava/util/concurrent/ExecutorService;

    .line 57
    .line 58
    new-instance v1, L〇8O0O808〇/〇80〇808〇O;

    .line 59
    .line 60
    invoke-direct {v1, p0, p1}, L〇8O0O808〇/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;Ljava/util/List;)V

    .line 61
    .line 62
    .line 63
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 64
    .line 65
    .line 66
    :goto_1
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method static bridge synthetic o880(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇08O〇00〇o:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private synthetic oOoO8OO〇(Ljava/util/ArrayList;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    check-cast p1, Lcom/intsig/menu/MenuItem;

    .line 6
    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/menu/MenuItem;->〇〇888()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇0ooOOo(I)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public static synthetic oOo〇08〇(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇0〇0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic oO〇oo(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;)Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->OO:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic oooO888(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;)Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇0O:Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private o〇0〇o()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v2, 0x1

    .line 8
    const/4 v3, 0x0

    .line 9
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 13
    .line 14
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o〇O8OO(Ljava/util/List;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->OO:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;->〇O00()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOo0:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderDiffCallback;

    .line 8
    .line 9
    invoke-virtual {v1, v0, p1}, Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderDiffCallback;->〇080(Ljava/util/List;Ljava/util/List;)V

    .line 10
    .line 11
    .line 12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 13
    .line 14
    .line 15
    move-result-wide v0

    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOo0:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderDiffCallback;

    .line 17
    .line 18
    const/4 v3, 0x1

    .line 19
    invoke-static {v2, v3}, Landroidx/recyclerview/widget/DiffUtil;->calculateDiff(Landroidx/recyclerview/widget/DiffUtil$Callback;Z)Landroidx/recyclerview/widget/DiffUtil$DiffResult;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    sget-object v3, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 24
    .line 25
    new-instance v4, Ljava/lang/StringBuilder;

    .line 26
    .line 27
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .line 29
    .line 30
    const-string v5, "diffResult cost time="

    .line 31
    .line 32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 36
    .line 37
    .line 38
    move-result-wide v5

    .line 39
    sub-long/2addr v5, v0

    .line 40
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->OO:Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;

    .line 51
    .line 52
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter;->〇O888o0o(Ljava/util/List;)V

    .line 53
    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOo〇8o008:Landroid/os/Handler;

    .line 56
    .line 57
    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    const/16 v0, 0x65

    .line 62
    .line 63
    iput v0, p1, Landroid/os/Message;->what:I

    .line 64
    .line 65
    iput-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 66
    .line 67
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOo〇8o008:Landroid/os/Handler;

    .line 68
    .line 69
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private 〇088O()V
    .locals 10

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-wide/16 v1, -0x2

    .line 6
    .line 7
    iget-object v3, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇0O:Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;

    .line 8
    .line 9
    invoke-virtual {v3}, Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;->〇080()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    const/4 v4, 0x0

    .line 14
    sget-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 15
    .line 16
    const/4 v6, 0x0

    .line 17
    const/4 v7, 0x0

    .line 18
    sget-object v8, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_CERTIFICATION:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 19
    .line 20
    const/4 v9, 0x0

    .line 21
    invoke-static/range {v0 .. v9}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->〇080(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/capture/CaptureMode;ZLjava/lang/String;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Z)Landroid/content/Intent;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v1, "extra_entrance"

    .line 26
    .line 27
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_IDCARD_FOLDER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 28
    .line 29
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 30
    .line 31
    .line 32
    const-string v1, "extra_certificate_capture_type"

    .line 33
    .line 34
    iget v2, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇o0O:I

    .line 35
    .line 36
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private 〇0ooOOo(I)V
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇8〇OOoooo(I)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_4

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    if-eq p1, v0, :cond_3

    .line 8
    .line 9
    const/4 v0, 0x2

    .line 10
    if-eq p1, v0, :cond_2

    .line 11
    .line 12
    const/4 v0, 0x3

    .line 13
    if-eq p1, v0, :cond_1

    .line 14
    .line 15
    const/4 v0, 0x4

    .line 16
    if-ne p1, v0, :cond_0

    .line 17
    .line 18
    sget-object p1, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 19
    .line 20
    const-string v0, "menuClick MENU_BANK_CARD"

    .line 21
    .line 22
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/16 p1, 0x3f1

    .line 26
    .line 27
    iput p1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇o0O:I

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 31
    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v2, " Illegal menuId="

    .line 38
    .line 39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    throw v0

    .line 53
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 54
    .line 55
    const-string v0, "menuClick MENU_TRAVEL"

    .line 56
    .line 57
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    const/16 p1, 0x3ed

    .line 61
    .line 62
    iput p1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇o0O:I

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 66
    .line 67
    const-string v0, "menuClick MENU_DRIVER"

    .line 68
    .line 69
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    const/16 p1, 0x3ec

    .line 73
    .line 74
    iput p1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇o0O:I

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 78
    .line 79
    const-string v0, "menuClick MENU_PASSPORT"

    .line 80
    .line 81
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    const/16 p1, 0x3ea

    .line 85
    .line 86
    iput p1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇o0O:I

    .line 87
    .line 88
    goto :goto_0

    .line 89
    :cond_4
    sget-object p1, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 90
    .line 91
    const-string v0, "menuClick MENU_CN_ID_CARD"

    .line 92
    .line 93
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    const/16 p1, 0x3e9

    .line 97
    .line 98
    iput p1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇o0O:I

    .line 99
    .line 100
    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    new-instance v0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment$4;

    .line 105
    .line 106
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment$4;-><init>(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;)V

    .line 107
    .line 108
    .line 109
    invoke-static {p1, v0}, Lcom/intsig/util/PermissionUtil;->O8(Landroid/content/Context;Lcom/intsig/permission/PermissionCallback;)V

    .line 110
    .line 111
    .line 112
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private static synthetic 〇0〇0(Landroid/view/View;)V
    .locals 0

    .line 1
    const/4 p0, 0x0

    .line 2
    invoke-static {p0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOo〇88(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇80O8o8O〇(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;Ljava/util/ArrayList;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOoO8OO〇(Ljava/util/ArrayList;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private 〇8〇80o()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o0:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a0350

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o0:Landroid/view/View;

    .line 15
    .line 16
    const v1, 0x7f0a0b67

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o0:Landroid/view/View;

    .line 27
    .line 28
    const v1, 0x7f0a0bef

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇08O〇00〇o:Landroid/view/View;

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o0:Landroid/view/View;

    .line 38
    .line 39
    const v1, 0x7f0a0e63

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    check-cast v0, Landroid/widget/ProgressBar;

    .line 47
    .line 48
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o〇0〇o()V

    .line 49
    .line 50
    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o0:Landroid/view/View;

    .line 52
    .line 53
    const v2, 0x7f0a0ea1

    .line 54
    .line 55
    .line 56
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    check-cast v1, Lcom/intsig/camscanner/view/AbstractPullToSyncView;

    .line 61
    .line 62
    new-instance v2, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;

    .line 63
    .line 64
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 65
    .line 66
    .line 67
    move-result-object v3

    .line 68
    invoke-direct {v2, p0, v3, v1}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;-><init>(Ljava/lang/Object;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setIHeaderViewStrategy(Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;)V

    .line 72
    .line 73
    .line 74
    new-instance v2, Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;

    .line 75
    .line 76
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 77
    .line 78
    .line 79
    move-result-object v3

    .line 80
    invoke-direct {v2, v3, v1, v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/view/AbstractPullToSyncView;Landroid/widget/ProgressBar;)V

    .line 81
    .line 82
    .line 83
    iput-object v2, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o〇00O:Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;

    .line 84
    .line 85
    invoke-virtual {v2}, Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;->〇〇808〇()V

    .line 86
    .line 87
    .line 88
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇o〇88〇8()V

    .line 89
    .line 90
    .line 91
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private 〇8〇OOoooo(I)V
    .locals 4

    .line 1
    if-eqz p1, :cond_4

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    if-eq p1, v0, :cond_3

    .line 5
    .line 6
    const/4 v0, 0x2

    .line 7
    if-eq p1, v0, :cond_2

    .line 8
    .line 9
    const/4 v0, 0x3

    .line 10
    if-eq p1, v0, :cond_1

    .line 11
    .line 12
    const/4 v0, 0x4

    .line 13
    if-eq p1, v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const-string v0, "bank_card"

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_1
    const-string v0, "china_car"

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_2
    const-string v0, "china_driver"

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_3
    const-string v0, "passport"

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_4
    const-string v0, "idcard"

    .line 30
    .line 31
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    if-eqz v1, :cond_5

    .line 36
    .line 37
    sget-object v1, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 38
    .line 39
    new-instance v2, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string v3, "actionLog menuId="

    .line 45
    .line 46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    const-string p1, " certificateType is empty"

    .line 53
    .line 54
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    :cond_5
    new-instance p1, Lorg/json/JSONObject;

    .line 65
    .line 66
    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    .line 67
    .line 68
    .line 69
    :try_start_0
    const-string v1, "type"

    .line 70
    .line 71
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 72
    .line 73
    .line 74
    const-string v0, "CSCertificateBag"

    .line 75
    .line 76
    const-string v1, "select_certificate"

    .line 77
    .line 78
    invoke-static {v0, v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .line 80
    .line 81
    goto :goto_1

    .line 82
    :catch_0
    move-exception p1

    .line 83
    sget-object v0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 84
    .line 85
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 86
    .line 87
    .line 88
    :goto_1
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o〇O8OO(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇o08()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇o0〇0O〇o()Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-nez v1, :cond_1

    .line 19
    .line 20
    return-void

    .line 21
    :cond_1
    new-instance v1, Lorg/json/JSONObject;

    .line 22
    .line 23
    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 24
    .line 25
    .line 26
    :try_start_0
    const-string v2, "from_part"

    .line 27
    .line 28
    const-string v3, "cs_certificate_bag"

    .line 29
    .line 30
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 31
    .line 32
    .line 33
    const-string v2, "CSCertificateGuide"

    .line 34
    .line 35
    invoke-static {v2, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :catch_0
    move-exception v1

    .line 40
    sget-object v2, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 41
    .line 42
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    .line 44
    .line 45
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->O88O:Lcom/intsig/camscanner/tools/MaskDialogTipsClient;

    .line 46
    .line 47
    if-nez v1, :cond_2

    .line 48
    .line 49
    invoke-static {v0}, Lcom/intsig/camscanner/tools/MaskDialogTipsClient;->o〇0(Landroid/app/Activity;)Lcom/intsig/camscanner/tools/MaskDialogTipsClient;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    iput-object v1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->O88O:Lcom/intsig/camscanner/tools/MaskDialogTipsClient;

    .line 54
    .line 55
    :cond_2
    const/4 v1, 0x4

    .line 56
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    new-instance v2, Lcom/intsig/camscanner/tools/MaskDialogTipsClient$MaskDialogTipsClientParameter;

    .line 61
    .line 62
    const v3, 0x7f130743

    .line 63
    .line 64
    .line 65
    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v3

    .line 69
    const v4, 0x7f130019

    .line 70
    .line 71
    .line 72
    invoke-virtual {p0, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v4

    .line 76
    int-to-float v1, v1

    .line 77
    invoke-direct {v2, v3, v4, v1}, Lcom/intsig/camscanner/tools/MaskDialogTipsClient$MaskDialogTipsClientParameter;-><init>(Ljava/lang/String;Ljava/lang/String;F)V

    .line 78
    .line 79
    .line 80
    iget-object v1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->O88O:Lcom/intsig/camscanner/tools/MaskDialogTipsClient;

    .line 81
    .line 82
    iget-object v3, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o0:Landroid/view/View;

    .line 83
    .line 84
    const v4, 0x7f0a0b67

    .line 85
    .line 86
    .line 87
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 88
    .line 89
    .line 90
    move-result-object v3

    .line 91
    new-instance v4, L〇8O0O808〇/oO80;

    .line 92
    .line 93
    invoke-direct {v4}, L〇8O0O808〇/oO80;-><init>()V

    .line 94
    .line 95
    .line 96
    invoke-virtual {v1, v0, v3, v2, v4}, Lcom/intsig/camscanner/tools/MaskDialogTipsClient;->〇〇888(Landroid/content/Context;Landroid/view/View;Lcom/intsig/camscanner/tools/MaskDialogTipsClient$MaskDialogTipsClientParameter;Landroid/view/View$OnClickListener;)V

    .line 97
    .line 98
    .line 99
    :cond_3
    :goto_1
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private 〇o〇88〇8()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 2
    .line 3
    invoke-static {p0}, Landroidx/loader/app/LoaderManager;->getInstance(Landroidx/lifecycle/LifecycleOwner;)Landroidx/loader/app/LoaderManager;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v2, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

    .line 8
    .line 9
    const/16 v3, 0x1770

    .line 10
    .line 11
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;-><init>(Landroidx/loader/app/LoaderManager;Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;I)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->O8o08O8O:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 15
    .line 16
    new-instance v0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 17
    .line 18
    invoke-static {p0}, Landroidx/loader/app/LoaderManager;->getInstance(Landroidx/lifecycle/LifecycleOwner;)Landroidx/loader/app/LoaderManager;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

    .line 23
    .line 24
    const/16 v3, 0x1771

    .line 25
    .line 26
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;-><init>(Landroidx/loader/app/LoaderManager;Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;I)V

    .line 27
    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇080OO8〇0:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇〇o0〇8(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o80ooO(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇〇〇00()V
    .locals 4

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 7
    .line 8
    const v2, 0x7f1306e8

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const/4 v3, 0x0

    .line 16
    invoke-direct {v1, v3, v2}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 23
    .line 24
    const v2, 0x7f130107

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    const/4 v3, 0x1

    .line 32
    invoke-direct {v1, v3, v2}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 39
    .line 40
    const v2, 0x7f1300fc

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    const/4 v3, 0x2

    .line 48
    invoke-direct {v1, v3, v2}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 55
    .line 56
    const v2, 0x7f130dc5

    .line 57
    .line 58
    .line 59
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    const/4 v3, 0x3

    .line 64
    invoke-direct {v1, v3, v2}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    .line 69
    .line 70
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 71
    .line 72
    const v2, 0x7f130dba

    .line 73
    .line 74
    .line 75
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    const/4 v3, 0x4

    .line 80
    invoke-direct {v1, v3, v2}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    new-instance v1, Lcom/intsig/app/AlertBottomDialog;

    .line 87
    .line 88
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    const v3, 0x7f140004

    .line 93
    .line 94
    .line 95
    invoke-direct {v1, v2, v3}, Lcom/intsig/app/AlertBottomDialog;-><init>(Landroid/content/Context;I)V

    .line 96
    .line 97
    .line 98
    const v2, 0x7f130741

    .line 99
    .line 100
    .line 101
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v2

    .line 105
    invoke-virtual {v1, v2, v0}, Lcom/intsig/app/AlertBottomDialog;->〇o00〇〇Oo(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 106
    .line 107
    .line 108
    new-instance v2, L〇8O0O808〇/〇〇888;

    .line 109
    .line 110
    invoke-direct {v2, p0, v0}, L〇8O0O808〇/〇〇888;-><init>(Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;Ljava/util/ArrayList;)V

    .line 111
    .line 112
    .line 113
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertBottomDialog;->O8(Landroid/content/DialogInterface$OnClickListener;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v1}, Lcom/intsig/app/AlertBottomDialog;->show()V

    .line 117
    .line 118
    .line 119
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method


# virtual methods
.method public OoO8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇0O:Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/certificate_package/intent_parameter/CertificateHomeIntentParameter;->〇o00〇〇Oo()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/MainPageRoute;->Oo08(Landroid/content/Context;)Landroid/content/Intent;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 20
    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    return v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oOo0(Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter$CertificateDocItem;Landroid/view/View;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_0
    const-string v1, "CSCertificateBag"

    .line 15
    .line 16
    const-string v2, "click_certificate"

    .line 17
    .line 18
    invoke-static {v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-wide v1, p1, Lcom/intsig/camscanner/certificate_package/adapter/CertificateFolderHomeAdapter$CertificateDocItem;->〇080:J

    .line 22
    .line 23
    const/4 p1, 0x1

    .line 24
    const/4 v3, 0x0

    .line 25
    invoke-static {v0, v1, v2, p1, v3}, Lcom/intsig/camscanner/certificate_package/activity/CertificateDetailActivity;->O0〇(Landroid/content/Context;JZZ)Landroid/content/Intent;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    if-eqz p2, :cond_1

    .line 30
    .line 31
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 32
    .line 33
    sget v2, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O:I

    .line 34
    .line 35
    if-lt v1, v2, :cond_1

    .line 36
    .line 37
    const-string v1, "nubia"

    .line 38
    .line 39
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 40
    .line 41
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    if-nez v1, :cond_1

    .line 46
    .line 47
    const v1, 0x7f0a0702

    .line 48
    .line 49
    .line 50
    :try_start_0
    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 51
    .line 52
    .line 53
    move-result-object p2

    .line 54
    const v1, 0x7f131ee7

    .line 55
    .line 56
    .line 57
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-static {v0, p2, v1}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/app/ActivityOptions;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    invoke-virtual {p2}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    .line 66
    .line 67
    .line 68
    move-result-object p2

    .line 69
    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :catch_0
    move-exception p2

    .line 74
    sget-object v0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 75
    .line 76
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 84
    .line 85
    .line 86
    :goto_0
    return-void

    .line 87
    :cond_2
    :goto_1
    sget-object p1, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 88
    .line 89
    const-string p2, "activity == null || activity.isFinishing()"

    .line 90
    .line 91
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const v0, 0x7f0a0b67

    .line 6
    .line 7
    .line 8
    if-ne p1, v0, :cond_0

    .line 9
    .line 10
    sget-object p1, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 11
    .line 12
    const-string v0, "add_cetificate"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const-string p1, "CSCertificateBag"

    .line 18
    .line 19
    const-string v0, "add_certificate"

    .line 20
    .line 21
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇〇〇00()V

    .line 25
    .line 26
    .line 27
    :cond_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    sget-object p3, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "onCreateView"

    .line 4
    .line 5
    invoke-static {p3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const p3, 0x7f0d02aa

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o0:Landroid/view/View;

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->Ooo8o()V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇8〇80o()V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o0:Landroid/view/View;

    .line 25
    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public onPause()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->oOO〇〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onPause()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o〇00O:Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;->〇O00()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public onResume()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->o〇00O:Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;->〇8o8o〇()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->O8o08O8O:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->o〇0()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇080OO8〇0:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->o〇0()V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/fragment/CertificateFolderHomeFragment;->〇o08()V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

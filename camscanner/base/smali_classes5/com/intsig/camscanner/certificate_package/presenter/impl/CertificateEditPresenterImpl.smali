.class public Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;
.super Ljava/lang/Object;
.source "CertificateEditPresenterImpl.java"

# interfaces
.implements Lcom/intsig/camscanner/certificate_package/presenter/ICertificateEditPresenter;


# instance fields
.field private final O8:I

.field private Oo08:Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;

.field private o〇0:Landroid/os/Handler;

.field private final 〇080:Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;

.field private 〇o00〇〇Oo:J

.field private final 〇o〇:I

.field private 〇〇888:I


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x3e8

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇o〇:I

    .line 7
    .line 8
    const/16 v0, 0x7d0

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->O8:I

    .line 11
    .line 12
    new-instance v0, Landroid/os/Handler;

    .line 13
    .line 14
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    new-instance v2, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl$1;

    .line 19
    .line 20
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl$1;-><init>(Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->o〇0:Landroid/os/Handler;

    .line 27
    .line 28
    const/4 v0, -0x2

    .line 29
    iput v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇〇888:I

    .line 30
    .line 31
    iput-object p1, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇080:Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;

    .line 32
    .line 33
    return-void
    .line 34
.end method

.method private OO0o〇〇(Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    if-nez p1, :cond_0

    .line 7
    .line 8
    return-object v0

    .line 9
    :cond_0
    const-string v1, "_data"

    .line 10
    .line 11
    const-string v2, "raw_data"

    .line 12
    .line 13
    const-string v3, "thumb_data"

    .line 14
    .line 15
    filled-new-array {v3, v1, v2}, [Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v6

    .line 19
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 20
    .line 21
    .line 22
    move-result-object v4

    .line 23
    iget-wide v1, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇o00〇〇Oo:J

    .line 24
    .line 25
    invoke-static {v1, v2}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 26
    .line 27
    .line 28
    move-result-object v5

    .line 29
    const/4 v7, 0x0

    .line 30
    const/4 v8, 0x0

    .line 31
    const-string v9, "page_num ASC"

    .line 32
    .line 33
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    if-eqz p1, :cond_5

    .line 38
    .line 39
    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    if-eqz v1, :cond_4

    .line 44
    .line 45
    const/4 v1, 0x0

    .line 46
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    if-eqz v2, :cond_2

    .line 55
    .line 56
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_2
    const/4 v1, 0x1

    .line 65
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 70
    .line 71
    .line 72
    move-result v2

    .line 73
    if-eqz v2, :cond_3

    .line 74
    .line 75
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_3
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    if-eqz v2, :cond_1

    .line 92
    .line 93
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    .line 99
    .line 100
    goto :goto_0

    .line 101
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 102
    .line 103
    .line 104
    :cond_5
    return-object v0
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private synthetic OO0o〇〇〇〇0()V
    .locals 5

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇080:Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;

    .line 6
    .line 7
    invoke-interface {v2}, Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;->getCurrentActivity()Landroid/app/Activity;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    iget-wide v3, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇o00〇〇Oo:J

    .line 12
    .line 13
    invoke-static {v2, v3, v4}, Lcom/intsig/camscanner/certificate_package/util/CertificateDBUtil;->o〇0(Landroid/content/Context;J)Lcom/intsig/camscanner/certificate_package/datamode/CertificateCursorData;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v2}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateCursorData;->O8()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    invoke-static {v3}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateUiDataEnum;->getCertificateUiDataEnum(I)Lcom/intsig/camscanner/certificate_package/datamode/CertificateUiDataEnum;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    invoke-virtual {v2}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateCursorData;->〇080()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-static {v3, v2}, Lcom/intsig/camscanner/certificate_package/factory/CertificateDataFactory;->〇o00〇〇Oo(Lcom/intsig/camscanner/certificate_package/datamode/CertificateUiDataEnum;Ljava/lang/String;)Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    iput-object v2, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->Oo08:Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;

    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->o〇0:Landroid/os/Handler;

    .line 36
    .line 37
    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    const/16 v3, 0x7d0

    .line 42
    .line 43
    iput v3, v2, Landroid/os/Message;->what:I

    .line 44
    .line 45
    iget-object v3, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->Oo08:Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;

    .line 46
    .line 47
    iput-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 48
    .line 49
    iget-object v3, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->o〇0:Landroid/os/Handler;

    .line 50
    .line 51
    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 52
    .line 53
    .line 54
    new-instance v2, Ljava/lang/StringBuilder;

    .line 55
    .line 56
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .line 58
    .line 59
    const-string v3, "loadCertificateInfo cost time="

    .line 60
    .line 61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 65
    .line 66
    .line 67
    move-result-wide v3

    .line 68
    sub-long/2addr v3, v0

    .line 69
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    const-string v1, "CertificateEditPresenterImpl"

    .line 77
    .line 78
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇O8o08O(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->OO0o〇〇〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;)Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇080:Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private synthetic 〇8o8o〇()V
    .locals 5

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇080:Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;

    .line 6
    .line 7
    invoke-interface {v2}, Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;->getCurrentActivity()Landroid/app/Activity;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->OO0o〇〇(Landroid/content/Context;)Ljava/util/List;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    if-eqz v2, :cond_1

    .line 16
    .line 17
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    if-nez v3, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    iget-object v3, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->o〇0:Landroid/os/Handler;

    .line 25
    .line 26
    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    const/16 v4, 0x3e8

    .line 31
    .line 32
    iput v4, v3, Landroid/os/Message;->what:I

    .line 33
    .line 34
    iput-object v2, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 35
    .line 36
    iget-object v2, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->o〇0:Landroid/os/Handler;

    .line 37
    .line 38
    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 39
    .line 40
    .line 41
    new-instance v2, Ljava/lang/StringBuilder;

    .line 42
    .line 43
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    .line 45
    .line 46
    const-string v3, "loadImage cost time="

    .line 47
    .line 48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 52
    .line 53
    .line 54
    move-result-wide v3

    .line 55
    sub-long/2addr v3, v0

    .line 56
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    const-string v1, "CertificateEditPresenterImpl"

    .line 64
    .line 65
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    :cond_1
    :goto_0
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private synthetic 〇O8o08O(Landroid/content/Context;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->Oo08:Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;->isAllFieldNullValue()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->Oo08:Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;->resetAllStringField()V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->Oo08:Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;->preProcessing()V

    .line 20
    .line 21
    .line 22
    :goto_0
    iget-wide v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇o00〇〇Oo:J

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->Oo08:Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;->toJsonString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    const/4 v3, -0x1

    .line 31
    invoke-static {p1, v0, v1, v2, v3}, Lcom/intsig/camscanner/Client/CertificateOcrClient;->o〇0(Landroid/content/Context;JLjava/lang/String;I)V

    .line 32
    .line 33
    .line 34
    :cond_1
    return-void
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇8o8o〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public O8()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, LOOo88OOo/〇〇888;

    .line 6
    .line 7
    invoke-direct {v1, p0}, LOOo88OOo/〇〇888;-><init>(Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo08()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇080:Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;->getCurrentActivity()Landroid/app/Activity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    new-instance v2, LOOo88OOo/oO80;

    .line 25
    .line 26
    invoke-direct {v2, p0, v0}, LOOo88OOo/oO80;-><init>(Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;Landroid/content/Context;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1, v2}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 30
    .line 31
    .line 32
    :cond_1
    :goto_0
    return-void
    .line 33
.end method

.method Oooo8o0〇(Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇〇888:I

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;->getCertiType()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-eq v0, v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;->getCertiType()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇〇888:I

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇080:Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;->getCertificateItemList()Ljava/util/List;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;->〇〇〇0o〇〇0(Ljava/util/List;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇080:Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;->getCertificateItemList()Ljava/util/List;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/certificate_package/iview/ICertificateEditView;->O0o(Ljava/util/List;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
.end method

.method public 〇080(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->Oo08:Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/certificate_package/datamode/CertificateBaseData;->updateStringField(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇o00〇〇Oo()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, LOOo88OOo/o〇0;

    .line 6
    .line 7
    invoke-direct {v1, p0}, LOOo88OOo/o〇0;-><init>(Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o〇(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/certificate_package/presenter/impl/CertificateEditPresenterImpl;->〇o00〇〇Oo:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

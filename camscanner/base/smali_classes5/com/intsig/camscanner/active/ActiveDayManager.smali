.class public final Lcom/intsig/camscanner/active/ActiveDayManager;
.super Ljava/lang/Object;
.source "ActiveDayManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/active/ActiveDayManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇o00〇〇Oo:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final 〇o〇:Lcom/intsig/camscanner/delegate/sp/ObjectSharedPreferencesDelegate;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "activeDayData"

    .line 7
    .line 8
    const-string v3, "getActiveDayData()Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/active/ActiveDayManager;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->Oo08(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/active/ActiveDayManager;->〇o00〇〇Oo:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/active/ActiveDayManager;

    .line 25
    .line 26
    invoke-direct {v0}, Lcom/intsig/camscanner/active/ActiveDayManager;-><init>()V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/intsig/camscanner/active/ActiveDayManager;->〇080:Lcom/intsig/camscanner/active/ActiveDayManager;

    .line 30
    .line 31
    new-instance v0, Lcom/intsig/camscanner/delegate/sp/SpConfig$Builder;

    .line 32
    .line 33
    const/4 v1, 0x0

    .line 34
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/delegate/sp/SpConfig$Builder;-><init>(Ljava/lang/Object;)V

    .line 35
    .line 36
    .line 37
    const-string v1, "KEY_ACTIVE_DAY_DATA_641"

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/delegate/sp/SpConfig$Builder;->〇o〇(Ljava/lang/String;)Lcom/intsig/camscanner/delegate/sp/SpConfig$Builder;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/delegate/sp/SpConfig$Builder;->O8(Z)Lcom/intsig/camscanner/delegate/sp/SpConfig$Builder;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/delegate/sp/SpConfig$Builder;->〇o00〇〇Oo(Z)Lcom/intsig/camscanner/delegate/sp/SpConfig$Builder;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/delegate/sp/SpConfig$Builder;->〇080()Lcom/intsig/camscanner/delegate/sp/SpConfig;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    new-instance v1, Lcom/intsig/camscanner/delegate/sp/ObjectSharedPreferencesDelegate;

    .line 56
    .line 57
    const-class v2, Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;

    .line 58
    .line 59
    invoke-direct {v1, v0, v2}, Lcom/intsig/camscanner/delegate/sp/ObjectSharedPreferencesDelegate;-><init>(Lcom/intsig/camscanner/delegate/sp/SpConfig;Ljava/lang/Class;)V

    .line 60
    .line 61
    .line 62
    sput-object v1, Lcom/intsig/camscanner/active/ActiveDayManager;->〇o〇:Lcom/intsig/camscanner/delegate/sp/ObjectSharedPreferencesDelegate;

    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final O8()V
    .locals 9

    .line 1
    sget-object v0, Lcom/intsig/camscanner/active/ActiveDayManager;->〇080:Lcom/intsig/camscanner/active/ActiveDayManager;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/active/ActiveDayManager;->〇080()Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "ActiveDayManager"

    .line 8
    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    const-string v3, "recordActiveDayData, recordedData == null"

    .line 12
    .line 13
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {v0}, Lcom/intsig/camscanner/active/ActiveDayManager;->〇o〇()Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;

    .line 17
    .line 18
    .line 19
    move-result-object v3

    .line 20
    if-eqz v3, :cond_0

    .line 21
    .line 22
    const-string v1, "recordActiveDayData, apply old"

    .line 23
    .line 24
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-direct {v0, v3}, Lcom/intsig/camscanner/active/ActiveDayManager;->Oo08(Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;)V

    .line 28
    .line 29
    .line 30
    move-object v1, v3

    .line 31
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v4, "recordActiveDayData, record: "

    .line 37
    .line 38
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    const/4 v3, 0x1

    .line 52
    if-nez v1, :cond_1

    .line 53
    .line 54
    new-instance v1, Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;

    .line 55
    .line 56
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 57
    .line 58
    .line 59
    move-result-wide v4

    .line 60
    invoke-direct {v1, v4, v5, v3}, Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;-><init>(JI)V

    .line 61
    .line 62
    .line 63
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/active/ActiveDayManager;->Oo08(Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;->getLastActiveTimeMs()J

    .line 68
    .line 69
    .line 70
    move-result-wide v4

    .line 71
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 72
    .line 73
    .line 74
    move-result-wide v6

    .line 75
    cmp-long v8, v6, v4

    .line 76
    .line 77
    if-lez v8, :cond_2

    .line 78
    .line 79
    invoke-static {v4, v5, v6, v7}, Lcom/intsig/utils/DateTimeUtil;->〇8o8o〇(JJ)Z

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    if-nez v4, :cond_2

    .line 84
    .line 85
    const-string v4, "recordActiveDayData, active day + 1"

    .line 86
    .line 87
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    new-instance v2, Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;

    .line 91
    .line 92
    invoke-virtual {v1}, Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;->getActiveDays()I

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    add-int/2addr v1, v3

    .line 97
    invoke-direct {v2, v6, v7, v1}, Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;-><init>(JI)V

    .line 98
    .line 99
    .line 100
    invoke-direct {v0, v2}, Lcom/intsig/camscanner/active/ActiveDayManager;->Oo08(Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;)V

    .line 101
    .line 102
    .line 103
    :cond_2
    :goto_0
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final Oo08(Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/active/ActiveDayManager;->〇o〇:Lcom/intsig/camscanner/delegate/sp/ObjectSharedPreferencesDelegate;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/active/ActiveDayManager;->〇o00〇〇Oo:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1, p1}, Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;->〇080(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇080()Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/active/ActiveDayManager;->〇o〇:Lcom/intsig/camscanner/delegate/sp/ObjectSharedPreferencesDelegate;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/active/ActiveDayManager;->〇o00〇〇Oo:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;->〇o00〇〇Oo(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final 〇o00〇〇Oo()I
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/active/ActiveDayManager;->〇080:Lcom/intsig/camscanner/active/ActiveDayManager;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/active/ActiveDayManager;->〇080()Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;->getActiveDays()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x1

    .line 15
    :goto_0
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o〇()Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;
    .locals 4

    .line 1
    const-string v0, "readOtherActiveDayData"

    .line 2
    .line 3
    const-string v1, "ActiveDayManager"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0o〇O0〇()Lcom/intsig/camscanner/purchase/looperdialog/DialogActiveDayDataBean;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    new-instance v2, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v3, "readOtherActiveDayData, last: "

    .line 20
    .line 21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    new-instance v1, Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;

    .line 35
    .line 36
    iget-wide v2, v0, Lcom/intsig/camscanner/purchase/looperdialog/DialogActiveDayDataBean;->lastLoginTime:J

    .line 37
    .line 38
    iget v0, v0, Lcom/intsig/camscanner/purchase/looperdialog/DialogActiveDayDataBean;->loginDays:I

    .line 39
    .line 40
    add-int/lit8 v0, v0, 0x1

    .line 41
    .line 42
    invoke-direct {v1, v2, v3, v0}, Lcom/intsig/camscanner/active/ActiveDayManager$ActiveDayData;-><init>(JI)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    const/4 v1, 0x0

    .line 47
    :goto_0
    return-object v1
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.class public Lcom/intsig/camscanner/CaptureIntentActivity;
.super Landroidx/appcompat/app/AppCompatActivity;
.source "CaptureIntentActivity.java"


# annotations
.annotation build Lcom/alibaba/android/arouter/facade/annotation/Route;
    path = "/capture/camera"
.end annotation


# instance fields
.field public autoArchiveId:Ljava/lang/String;
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "capture_scene_auto_archive_id"
    .end annotation
.end field

.field public cameraAdFromPart:Ljava/lang/String;
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "camera_ad_from_part"
    .end annotation
.end field

.field public captureFunctionEntrance:Ljava/lang/String;
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "capture_function_entrance"
    .end annotation
.end field

.field public captureIsShowGuide:Z
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "capture_is_show_guide"
    .end annotation
.end field

.field public captureSceneJson:Ljava/lang/String;
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "capture_scene_json"
    .end annotation
.end field

.field public certificateType:Ljava/lang/String;
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "certificate_type"
    .end annotation
.end field

.field public csInternal:Z
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "cs_internal"
    .end annotation
.end field

.field public docTitle:Ljava/lang/String;
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "capture_doc_title"
    .end annotation
.end field

.field public eduLottery:Ljava/lang/String;
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "edu_lottery"
    .end annotation
.end field

.field public invoiceRetake:Z
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "key_is_retake"
    .end annotation
.end field

.field public mode:Ljava/lang/String;
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "mode"
    .end annotation
.end field

.field private o0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public onlyOne:Z
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "capture_only_one_mode"
    .end annotation
.end field

.field public parentSyncId:Ljava/lang/String;
    .annotation build Lcom/alibaba/android/arouter/facade/annotation/Autowired;
        name = "capture_dir_sync_id"
    .end annotation
.end field

.field private 〇OOo8〇0:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->o0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 7
    .line 8
    const/4 v0, -0x1

    .line 9
    iput v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->〇OOo8〇0:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-virtual {p1, p0}, Lcom/intsig/router/CSRouter;->Oo08(Ljava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    iget-boolean p1, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->csInternal:Z

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    const/4 v1, 0x1

    .line 15
    if-nez p1, :cond_0

    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    const/16 v2, 0x400

    .line 22
    .line 23
    invoke-virtual {p1, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {p1, v0}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-static {p1, v1}, Lcom/intsig/utils/SystemUiUtil;->o〇0(Landroid/view/Window;Z)V

    .line 38
    .line 39
    .line 40
    const p1, 0x7f0d0718

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->setContentView(I)V

    .line 44
    .line 45
    .line 46
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 47
    .line 48
    iget-object v2, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->captureFunctionEntrance:Ljava/lang/String;

    .line 49
    .line 50
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    if-nez v2, :cond_1

    .line 55
    .line 56
    :try_start_0
    iget-object v2, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->captureFunctionEntrance:Ljava/lang/String;

    .line 57
    .line 58
    invoke-static {v2}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    iput-object v2, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->o0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :catch_0
    move-exception v2

    .line 66
    const-string v3, "CaptureIntentActivity"

    .line 67
    .line 68
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    .line 70
    .line 71
    :cond_1
    :goto_0
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 72
    .line 73
    iget-object v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->captureSceneJson:Ljava/lang/String;

    .line 74
    .line 75
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 76
    .line 77
    .line 78
    move-result v3

    .line 79
    if-nez v3, :cond_2

    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->captureSceneJson:Ljava/lang/String;

    .line 82
    .line 83
    invoke-static {v0}, Lcom/intsig/camscanner/capture/scene/CaptureSceneDataExtKt;->〇o〇(Ljava/lang/String;)Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    if-eqz v0, :cond_2

    .line 88
    .line 89
    sget-object v3, Lcom/intsig/camscanner/capture/scene/AutoArchiveUtil;->〇080:Lcom/intsig/camscanner/capture/scene/AutoArchiveUtil;

    .line 90
    .line 91
    iget-object v4, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->autoArchiveId:Ljava/lang/String;

    .line 92
    .line 93
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/capture/scene/AutoArchiveUtil;->〇o〇(Ljava/lang/String;)Lcom/intsig/camscanner/capture/scene/AutoArchiveDirData;

    .line 94
    .line 95
    .line 96
    move-result-object v3

    .line 97
    if-eqz v3, :cond_2

    .line 98
    .line 99
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setAutoArchive(Z)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setArchiveDirData(Lcom/intsig/camscanner/capture/scene/AutoArchiveDirData;)V

    .line 103
    .line 104
    .line 105
    :cond_2
    iget-object v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->mode:Ljava/lang/String;

    .line 106
    .line 107
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 108
    .line 109
    .line 110
    move-result v3

    .line 111
    if-nez v3, :cond_20

    .line 112
    .line 113
    iget-object v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->mode:Ljava/lang/String;

    .line 114
    .line 115
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    .line 116
    .line 117
    .line 118
    move-result v4

    .line 119
    const/16 v5, 0x31

    .line 120
    .line 121
    if-eq v4, v5, :cond_8

    .line 122
    .line 123
    const/16 v5, 0x32

    .line 124
    .line 125
    if-eq v4, v5, :cond_7

    .line 126
    .line 127
    const/16 v5, 0x35

    .line 128
    .line 129
    if-eq v4, v5, :cond_6

    .line 130
    .line 131
    const/16 v5, 0x65d

    .line 132
    .line 133
    if-eq v4, v5, :cond_5

    .line 134
    .line 135
    const/16 v5, 0x37

    .line 136
    .line 137
    if-eq v4, v5, :cond_4

    .line 138
    .line 139
    const/16 v5, 0x38

    .line 140
    .line 141
    if-eq v4, v5, :cond_3

    .line 142
    .line 143
    packed-switch v4, :pswitch_data_0

    .line 144
    .line 145
    .line 146
    packed-switch v4, :pswitch_data_1

    .line 147
    .line 148
    .line 149
    packed-switch v4, :pswitch_data_2

    .line 150
    .line 151
    .line 152
    goto/16 :goto_1

    .line 153
    .line 154
    :pswitch_0
    const-string v4, "29"

    .line 155
    .line 156
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 157
    .line 158
    .line 159
    move-result v3

    .line 160
    if-eqz v3, :cond_9

    .line 161
    .line 162
    const/16 v3, 0x18

    .line 163
    .line 164
    goto/16 :goto_2

    .line 165
    .line 166
    :pswitch_1
    const-string v4, "28"

    .line 167
    .line 168
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 169
    .line 170
    .line 171
    move-result v3

    .line 172
    if-eqz v3, :cond_9

    .line 173
    .line 174
    const/16 v3, 0x16

    .line 175
    .line 176
    goto/16 :goto_2

    .line 177
    .line 178
    :pswitch_2
    const-string v4, "27"

    .line 179
    .line 180
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 181
    .line 182
    .line 183
    move-result v3

    .line 184
    if-eqz v3, :cond_9

    .line 185
    .line 186
    const/16 v3, 0x15

    .line 187
    .line 188
    goto/16 :goto_2

    .line 189
    .line 190
    :pswitch_3
    const-string v4, "26"

    .line 191
    .line 192
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 193
    .line 194
    .line 195
    move-result v3

    .line 196
    if-eqz v3, :cond_9

    .line 197
    .line 198
    const/16 v3, 0x14

    .line 199
    .line 200
    goto/16 :goto_2

    .line 201
    .line 202
    :pswitch_4
    const-string v4, "25"

    .line 203
    .line 204
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 205
    .line 206
    .line 207
    move-result v3

    .line 208
    if-eqz v3, :cond_9

    .line 209
    .line 210
    const/16 v3, 0x13

    .line 211
    .line 212
    goto/16 :goto_2

    .line 213
    .line 214
    :pswitch_5
    const-string v4, "24"

    .line 215
    .line 216
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 217
    .line 218
    .line 219
    move-result v3

    .line 220
    if-eqz v3, :cond_9

    .line 221
    .line 222
    const/16 v3, 0x12

    .line 223
    .line 224
    goto/16 :goto_2

    .line 225
    .line 226
    :pswitch_6
    const-string v4, "23"

    .line 227
    .line 228
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 229
    .line 230
    .line 231
    move-result v3

    .line 232
    if-eqz v3, :cond_9

    .line 233
    .line 234
    const/16 v3, 0x11

    .line 235
    .line 236
    goto/16 :goto_2

    .line 237
    .line 238
    :pswitch_7
    const-string v4, "22"

    .line 239
    .line 240
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 241
    .line 242
    .line 243
    move-result v3

    .line 244
    if-eqz v3, :cond_9

    .line 245
    .line 246
    const/16 v3, 0x10

    .line 247
    .line 248
    goto/16 :goto_2

    .line 249
    .line 250
    :pswitch_8
    const-string v4, "21"

    .line 251
    .line 252
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 253
    .line 254
    .line 255
    move-result v3

    .line 256
    if-eqz v3, :cond_9

    .line 257
    .line 258
    const/16 v3, 0xf

    .line 259
    .line 260
    goto/16 :goto_2

    .line 261
    .line 262
    :pswitch_9
    const-string v4, "20"

    .line 263
    .line 264
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 265
    .line 266
    .line 267
    move-result v3

    .line 268
    if-eqz v3, :cond_9

    .line 269
    .line 270
    const/16 v3, 0xe

    .line 271
    .line 272
    goto/16 :goto_2

    .line 273
    .line 274
    :pswitch_a
    const-string v4, "19"

    .line 275
    .line 276
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 277
    .line 278
    .line 279
    move-result v3

    .line 280
    if-eqz v3, :cond_9

    .line 281
    .line 282
    const/16 v3, 0xd

    .line 283
    .line 284
    goto/16 :goto_2

    .line 285
    .line 286
    :pswitch_b
    const-string v4, "18"

    .line 287
    .line 288
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 289
    .line 290
    .line 291
    move-result v3

    .line 292
    if-eqz v3, :cond_9

    .line 293
    .line 294
    const/16 v3, 0xc

    .line 295
    .line 296
    goto/16 :goto_2

    .line 297
    .line 298
    :pswitch_c
    const-string v4, "17"

    .line 299
    .line 300
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 301
    .line 302
    .line 303
    move-result v3

    .line 304
    if-eqz v3, :cond_9

    .line 305
    .line 306
    const/16 v3, 0xb

    .line 307
    .line 308
    goto/16 :goto_2

    .line 309
    .line 310
    :pswitch_d
    const-string v4, "16"

    .line 311
    .line 312
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 313
    .line 314
    .line 315
    move-result v3

    .line 316
    if-eqz v3, :cond_9

    .line 317
    .line 318
    const/16 v3, 0xa

    .line 319
    .line 320
    goto/16 :goto_2

    .line 321
    .line 322
    :pswitch_e
    const-string v4, "13"

    .line 323
    .line 324
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 325
    .line 326
    .line 327
    move-result v3

    .line 328
    if-eqz v3, :cond_9

    .line 329
    .line 330
    const/16 v3, 0x9

    .line 331
    .line 332
    goto :goto_2

    .line 333
    :pswitch_f
    const-string v4, "12"

    .line 334
    .line 335
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 336
    .line 337
    .line 338
    move-result v3

    .line 339
    if-eqz v3, :cond_9

    .line 340
    .line 341
    const/16 v3, 0x8

    .line 342
    .line 343
    goto :goto_2

    .line 344
    :pswitch_10
    const-string v4, "11"

    .line 345
    .line 346
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 347
    .line 348
    .line 349
    move-result v3

    .line 350
    if-eqz v3, :cond_9

    .line 351
    .line 352
    const/4 v3, 0x7

    .line 353
    goto :goto_2

    .line 354
    :pswitch_11
    const-string v4, "10"

    .line 355
    .line 356
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 357
    .line 358
    .line 359
    move-result v3

    .line 360
    if-eqz v3, :cond_9

    .line 361
    .line 362
    const/4 v3, 0x6

    .line 363
    goto :goto_2

    .line 364
    :cond_3
    const-string v4, "8"

    .line 365
    .line 366
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 367
    .line 368
    .line 369
    move-result v3

    .line 370
    if-eqz v3, :cond_9

    .line 371
    .line 372
    const/4 v3, 0x5

    .line 373
    goto :goto_2

    .line 374
    :cond_4
    const-string v4, "7"

    .line 375
    .line 376
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 377
    .line 378
    .line 379
    move-result v3

    .line 380
    if-eqz v3, :cond_9

    .line 381
    .line 382
    const/4 v3, 0x4

    .line 383
    goto :goto_2

    .line 384
    :cond_5
    const-string v4, "30"

    .line 385
    .line 386
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 387
    .line 388
    .line 389
    move-result v3

    .line 390
    if-eqz v3, :cond_9

    .line 391
    .line 392
    const/16 v3, 0x17

    .line 393
    .line 394
    goto :goto_2

    .line 395
    :cond_6
    const-string v4, "5"

    .line 396
    .line 397
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 398
    .line 399
    .line 400
    move-result v3

    .line 401
    if-eqz v3, :cond_9

    .line 402
    .line 403
    const/4 v3, 0x3

    .line 404
    goto :goto_2

    .line 405
    :cond_7
    const-string v4, "2"

    .line 406
    .line 407
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 408
    .line 409
    .line 410
    move-result v3

    .line 411
    if-eqz v3, :cond_9

    .line 412
    .line 413
    const/4 v3, 0x2

    .line 414
    goto :goto_2

    .line 415
    :cond_8
    const-string v4, "1"

    .line 416
    .line 417
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 418
    .line 419
    .line 420
    move-result v3

    .line 421
    if-eqz v3, :cond_9

    .line 422
    .line 423
    const/4 v3, 0x1

    .line 424
    goto :goto_2

    .line 425
    :cond_9
    :goto_1
    const/4 v3, -0x1

    .line 426
    :goto_2
    packed-switch v3, :pswitch_data_3

    .line 427
    .line 428
    .line 429
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_SINGLE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 430
    .line 431
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 432
    .line 433
    if-eqz v3, :cond_1f

    .line 434
    .line 435
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_NORMAL_SINGLE:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 436
    .line 437
    goto/16 :goto_4

    .line 438
    .line 439
    :pswitch_12
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_WORKBENCH:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 440
    .line 441
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 442
    .line 443
    if-eqz v3, :cond_a

    .line 444
    .line 445
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_NORMAL_WORKBENCH:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 446
    .line 447
    :cond_a
    if-eqz v0, :cond_20

    .line 448
    .line 449
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 450
    .line 451
    .line 452
    move-result-object v3

    .line 453
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 454
    .line 455
    .line 456
    goto/16 :goto_5

    .line 457
    .line 458
    :pswitch_13
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->INVOICE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 459
    .line 460
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 461
    .line 462
    if-eqz v3, :cond_b

    .line 463
    .line 464
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_INVOICE:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 465
    .line 466
    :cond_b
    if-eqz v0, :cond_20

    .line 467
    .line 468
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 469
    .line 470
    .line 471
    move-result-object v3

    .line 472
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 473
    .line 474
    .line 475
    goto/16 :goto_5

    .line 476
    .line 477
    :pswitch_14
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->COUNT_NUMBER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 478
    .line 479
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 480
    .line 481
    if-eqz v3, :cond_c

    .line 482
    .line 483
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_COUNT_NUMBER:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 484
    .line 485
    :cond_c
    if-eqz v0, :cond_20

    .line 486
    .line 487
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 488
    .line 489
    .line 490
    move-result-object v3

    .line 491
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 492
    .line 493
    .line 494
    goto/16 :goto_5

    .line 495
    .line 496
    :pswitch_15
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->WHITE_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 497
    .line 498
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_WHITE_BOARD:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 499
    .line 500
    if-eqz v0, :cond_20

    .line 501
    .line 502
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 503
    .line 504
    .line 505
    move-result-object v3

    .line 506
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 507
    .line 508
    .line 509
    goto/16 :goto_5

    .line 510
    .line 511
    :pswitch_16
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->WRITING_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 512
    .line 513
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_WRITING_BOARD:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 514
    .line 515
    if-eqz v0, :cond_20

    .line 516
    .line 517
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 518
    .line 519
    .line 520
    move-result-object v3

    .line 521
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 522
    .line 523
    .line 524
    goto/16 :goto_5

    .line 525
    .line 526
    :pswitch_17
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->CAPTURE_SIGNATURE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 527
    .line 528
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 529
    .line 530
    if-eqz v3, :cond_d

    .line 531
    .line 532
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_CAPTURE_SIGNATURE:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 533
    .line 534
    :cond_d
    if-eqz v0, :cond_20

    .line 535
    .line 536
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 537
    .line 538
    .line 539
    move-result-object v3

    .line 540
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 541
    .line 542
    .line 543
    goto/16 :goto_5

    .line 544
    .line 545
    :pswitch_18
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->SMART_ERASE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 546
    .line 547
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 548
    .line 549
    if-eqz v3, :cond_e

    .line 550
    .line 551
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_SMART_ERASE:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 552
    .line 553
    :cond_e
    if-eqz v0, :cond_20

    .line 554
    .line 555
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 556
    .line 557
    .line 558
    move-result-object v3

    .line 559
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 560
    .line 561
    .line 562
    goto/16 :goto_5

    .line 563
    .line 564
    :pswitch_19
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->GREET_CARD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 565
    .line 566
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_GREET_CARD:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 567
    .line 568
    if-eqz v0, :cond_20

    .line 569
    .line 570
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 571
    .line 572
    .line 573
    move-result-object v3

    .line 574
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 575
    .line 576
    .line 577
    goto/16 :goto_5

    .line 578
    .line 579
    :pswitch_1a
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->E_EVIDENCE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 580
    .line 581
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 582
    .line 583
    if-eqz v3, :cond_f

    .line 584
    .line 585
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_EEVIDENCE:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 586
    .line 587
    :cond_f
    if-eqz v0, :cond_20

    .line 588
    .line 589
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 590
    .line 591
    .line 592
    move-result-object v3

    .line 593
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 594
    .line 595
    .line 596
    goto/16 :goto_5

    .line 597
    .line 598
    :pswitch_1b
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC_LEGACY:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 599
    .line 600
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 601
    .line 602
    if-eqz v3, :cond_10

    .line 603
    .line 604
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_TOPIC:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 605
    .line 606
    :cond_10
    if-eqz v0, :cond_20

    .line 607
    .line 608
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 609
    .line 610
    .line 611
    move-result-object v3

    .line 612
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 613
    .line 614
    .line 615
    goto/16 :goto_5

    .line 616
    .line 617
    :pswitch_1c
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC_PAPER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 618
    .line 619
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 620
    .line 621
    if-eqz v3, :cond_11

    .line 622
    .line 623
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_TOPIC_PAPER:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 624
    .line 625
    :cond_11
    if-eqz v0, :cond_20

    .line 626
    .line 627
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 628
    .line 629
    .line 630
    move-result-object v3

    .line 631
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 632
    .line 633
    .line 634
    goto/16 :goto_5

    .line 635
    .line 636
    :pswitch_1d
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->IMAGE_RESTORE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 637
    .line 638
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 639
    .line 640
    if-eqz v3, :cond_12

    .line 641
    .line 642
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_IMAGE_RESTORE:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 643
    .line 644
    :cond_12
    if-eqz v0, :cond_20

    .line 645
    .line 646
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 647
    .line 648
    .line 649
    move-result-object v3

    .line 650
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 651
    .line 652
    .line 653
    goto/16 :goto_5

    .line 654
    .line 655
    :pswitch_1e
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 656
    .line 657
    .line 658
    move-result-object v3

    .line 659
    invoke-virtual {v3}, Lcom/intsig/tsapp/sync/AppConfigJson;->forbidNcnnLibForBookScene()Z

    .line 660
    .line 661
    .line 662
    move-result v3

    .line 663
    if-eqz v3, :cond_13

    .line 664
    .line 665
    goto/16 :goto_5

    .line 666
    .line 667
    :cond_13
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->BOOK_SPLITTER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 668
    .line 669
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 670
    .line 671
    if-eqz v3, :cond_14

    .line 672
    .line 673
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_BOOK:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 674
    .line 675
    :cond_14
    if-eqz v0, :cond_20

    .line 676
    .line 677
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 678
    .line 679
    .line 680
    move-result-object v3

    .line 681
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 682
    .line 683
    .line 684
    goto/16 :goto_5

    .line 685
    .line 686
    :pswitch_1f
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->MODEL_MORE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 687
    .line 688
    if-eqz v0, :cond_20

    .line 689
    .line 690
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 691
    .line 692
    .line 693
    move-result-object v3

    .line 694
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 695
    .line 696
    .line 697
    goto/16 :goto_5

    .line 698
    .line 699
    :pswitch_20
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->PPT:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 700
    .line 701
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 702
    .line 703
    if-eqz v3, :cond_15

    .line 704
    .line 705
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_PPT:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 706
    .line 707
    :cond_15
    if-eqz v0, :cond_20

    .line 708
    .line 709
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 710
    .line 711
    .line 712
    move-result-object v3

    .line 713
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 714
    .line 715
    .line 716
    goto/16 :goto_5

    .line 717
    .line 718
    :pswitch_21
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE_PHOTO:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 719
    .line 720
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 721
    .line 722
    if-eqz v3, :cond_16

    .line 723
    .line 724
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_CERTIFICATE_PHOTO:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 725
    .line 726
    :cond_16
    if-eqz v0, :cond_20

    .line 727
    .line 728
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 729
    .line 730
    .line 731
    move-result-object v3

    .line 732
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 733
    .line 734
    .line 735
    goto/16 :goto_5

    .line 736
    .line 737
    :pswitch_22
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->DOC_TO_EXCEL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 738
    .line 739
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 740
    .line 741
    if-eqz v3, :cond_17

    .line 742
    .line 743
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_DOC_TO_EXCEL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 744
    .line 745
    :cond_17
    if-eqz v0, :cond_20

    .line 746
    .line 747
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 748
    .line 749
    .line 750
    move-result-object v3

    .line 751
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 752
    .line 753
    .line 754
    goto/16 :goto_5

    .line 755
    .line 756
    :pswitch_23
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->DOC_TO_WORD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 757
    .line 758
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 759
    .line 760
    if-eqz v3, :cond_18

    .line 761
    .line 762
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_DOC_TO_WORD:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 763
    .line 764
    :cond_18
    if-eqz v0, :cond_20

    .line 765
    .line 766
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 767
    .line 768
    .line 769
    move-result-object v3

    .line 770
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 771
    .line 772
    .line 773
    goto/16 :goto_5

    .line 774
    .line 775
    :pswitch_24
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->OCR:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 776
    .line 777
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 778
    .line 779
    if-eqz v3, :cond_19

    .line 780
    .line 781
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_OCR:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 782
    .line 783
    :cond_19
    if-eqz v0, :cond_20

    .line 784
    .line 785
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 786
    .line 787
    .line 788
    move-result-object v3

    .line 789
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 790
    .line 791
    .line 792
    goto/16 :goto_5

    .line 793
    .line 794
    :pswitch_25
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 795
    .line 796
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 797
    .line 798
    if-eqz v3, :cond_1a

    .line 799
    .line 800
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_TOPIC:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 801
    .line 802
    :cond_1a
    if-eqz v0, :cond_20

    .line 803
    .line 804
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 805
    .line 806
    .line 807
    move-result-object v3

    .line 808
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 809
    .line 810
    .line 811
    goto :goto_5

    .line 812
    :pswitch_26
    sget-object p1, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇080:Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;

    .line 813
    .line 814
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇〇888()Z

    .line 815
    .line 816
    .line 817
    move-result p1

    .line 818
    if-eqz p1, :cond_1b

    .line 819
    .line 820
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->BARCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 821
    .line 822
    goto :goto_3

    .line 823
    :cond_1b
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->QRCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 824
    .line 825
    :goto_3
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_QR_CODE_ONLY:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 826
    .line 827
    if-eqz v0, :cond_20

    .line 828
    .line 829
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 830
    .line 831
    .line 832
    move-result-object v3

    .line 833
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 834
    .line 835
    .line 836
    goto :goto_5

    .line 837
    :pswitch_27
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 838
    .line 839
    iget-object v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->certificateType:Ljava/lang/String;

    .line 840
    .line 841
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 842
    .line 843
    .line 844
    move-result v3

    .line 845
    if-nez v3, :cond_1c

    .line 846
    .line 847
    iget-object v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->certificateType:Ljava/lang/String;

    .line 848
    .line 849
    invoke-static {v3}, Lcom/intsig/utils/ext/StringExtKt;->〇〇888(Ljava/lang/String;)I

    .line 850
    .line 851
    .line 852
    move-result v3

    .line 853
    iput v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->〇OOo8〇0:I

    .line 854
    .line 855
    if-lez v3, :cond_1c

    .line 856
    .line 857
    iput-boolean v1, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 858
    .line 859
    :cond_1c
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 860
    .line 861
    if-eqz v3, :cond_1d

    .line 862
    .line 863
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_CERTIFICATION:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 864
    .line 865
    :cond_1d
    if-eqz v0, :cond_20

    .line 866
    .line 867
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 868
    .line 869
    .line 870
    move-result-object v3

    .line 871
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 872
    .line 873
    .line 874
    goto :goto_5

    .line 875
    :pswitch_28
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_MULTI:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 876
    .line 877
    iget-boolean v3, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->onlyOne:Z

    .line 878
    .line 879
    if-eqz v3, :cond_1e

    .line 880
    .line 881
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_NORMAL_MULTI:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 882
    .line 883
    :cond_1e
    if-eqz v0, :cond_20

    .line 884
    .line 885
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 886
    .line 887
    .line 888
    move-result-object v3

    .line 889
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 890
    .line 891
    .line 892
    goto :goto_5

    .line 893
    :cond_1f
    :goto_4
    if-eqz v0, :cond_20

    .line 894
    .line 895
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 896
    .line 897
    .line 898
    move-result-object v3

    .line 899
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->setCaptureModeName(Ljava/lang/String;)V

    .line 900
    .line 901
    .line 902
    :cond_20
    :goto_5
    if-eqz v0, :cond_21

    .line 903
    .line 904
    invoke-static {v0, v1}, Lcom/intsig/camscanner/capture/scene/CaptureSceneDataExtKt;->Oo08(Lcom/intsig/camscanner/capture/scene/CaptureSceneData;Z)Ljava/lang/String;

    .line 905
    .line 906
    .line 907
    move-result-object v0

    .line 908
    iput-object v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->captureSceneJson:Ljava/lang/String;

    .line 909
    .line 910
    :cond_21
    new-instance v0, Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 911
    .line 912
    invoke-direct {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 913
    .line 914
    .line 915
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇o(Landroid/app/Activity;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 916
    .line 917
    .line 918
    move-result-object v0

    .line 919
    iget-object v1, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->o0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 920
    .line 921
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 922
    .line 923
    .line 924
    move-result-object v0

    .line 925
    const-wide/16 v3, 0x0

    .line 926
    .line 927
    invoke-virtual {v0, v3, v4}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇80〇808〇O(J)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 928
    .line 929
    .line 930
    move-result-object v0

    .line 931
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 932
    .line 933
    .line 934
    move-result-object p1

    .line 935
    iget-object v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->captureSceneJson:Ljava/lang/String;

    .line 936
    .line 937
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->oO80(Ljava/lang/String;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 938
    .line 939
    .line 940
    move-result-object p1

    .line 941
    iget-boolean v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->captureIsShowGuide:Z

    .line 942
    .line 943
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->o〇0(Z)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 944
    .line 945
    .line 946
    move-result-object p1

    .line 947
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 948
    .line 949
    .line 950
    move-result-object p1

    .line 951
    new-instance v0, Lcom/intsig/camscanner/CaptureIntentActivity$1;

    .line 952
    .line 953
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/CaptureIntentActivity$1;-><init>(Lcom/intsig/camscanner/CaptureIntentActivity;)V

    .line 954
    .line 955
    .line 956
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇0000OOO(Lcom/intsig/camscanner/app/StartCameraBuilder$onStartCameraCallback;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 957
    .line 958
    .line 959
    move-result-object p1

    .line 960
    iget-object v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->docTitle:Ljava/lang/String;

    .line 961
    .line 962
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OOO〇O0(Ljava/lang/String;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 963
    .line 964
    .line 965
    move-result-object p1

    .line 966
    iget v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->〇OOo8〇0:I

    .line 967
    .line 968
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇〇〇0(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 969
    .line 970
    .line 971
    move-result-object p1

    .line 972
    iget-object v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->parentSyncId:Ljava/lang/String;

    .line 973
    .line 974
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇00(Ljava/lang/String;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 975
    .line 976
    .line 977
    move-result-object p1

    .line 978
    iget-object v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->eduLottery:Ljava/lang/String;

    .line 979
    .line 980
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇oo〇(Ljava/lang/String;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 981
    .line 982
    .line 983
    move-result-object p1

    .line 984
    iget-boolean v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->invoiceRetake:Z

    .line 985
    .line 986
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇8O0〇8(Z)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 987
    .line 988
    .line 989
    move-result-object p1

    .line 990
    iget-object v0, p0, Lcom/intsig/camscanner/CaptureIntentActivity;->cameraAdFromPart:Ljava/lang/String;

    .line 991
    .line 992
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->o〇〇0〇(Ljava/lang/String;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 993
    .line 994
    .line 995
    move-result-object p1

    .line 996
    invoke-virtual {p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    .line 997
    .line 998
    .line 999
    return-void

    .line 1000
    nop

    .line 1001
    :pswitch_data_0
    .packed-switch 0x61f
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
    .end packed-switch

    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    :pswitch_data_1
    .packed-switch 0x625
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch

    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    :pswitch_data_2
    .packed-switch 0x63e
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
    .end packed-switch
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
.end method

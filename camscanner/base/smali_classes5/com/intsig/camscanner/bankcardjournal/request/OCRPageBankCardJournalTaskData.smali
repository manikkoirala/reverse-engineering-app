.class public final Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;
.super Lcom/intsig/camscanner/tsapp/request/RequestTaskData;
.source "OCRPageBankCardJournalTaskData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData$OnBankCardJournalCompleteListener;,
        Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇〇888:Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Ljava/lang/String;

.field private final Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇0:Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData$OnBankCardJournalCompleteListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->〇〇888:Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;J)V
    .locals 7
    .param p2    # Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "bankCardJournalPageData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p3

    .line 3
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData;-><init>(JIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->O8:Ljava/lang/String;

    .line 5
    iput-object p2, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;JILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p3

    .line 2
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;J)V

    return-void
.end method


# virtual methods
.method public final O8(Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData$OnBankCardJournalCompleteListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->o〇0:Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData$OnBankCardJournalCompleteListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;

    .line 6
    .line 7
    if-eqz v1, :cond_1

    .line 8
    .line 9
    move-object v1, p1

    .line 10
    check-cast v1, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;

    .line 11
    .line 12
    iget-object v2, v1, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇o〇()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    iget-object v3, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 19
    .line 20
    invoke-virtual {v3}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇o〇()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-eqz v2, :cond_1

    .line 29
    .line 30
    iget-object v1, v1, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->O8:Ljava/lang/String;

    .line 31
    .line 32
    iget-object v2, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->O8:Ljava/lang/String;

    .line 33
    .line 34
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-eqz v1, :cond_1

    .line 39
    .line 40
    return v0

    .line 41
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    return p1
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->O8:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇o〇()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    add-int/2addr v0, v1

    .line 24
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇080(Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataCallback;)V
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇o〇()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇o00〇〇Oo()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-static {v0}, Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;->oO80(Ljava/lang/String;)Ljava/io/File;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    const/4 v4, 0x1

    .line 22
    const/4 v11, 0x0

    .line 23
    if-eqz v3, :cond_1

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇080()Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    if-nez v0, :cond_7

    .line 32
    .line 33
    invoke-static {v2}, Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;->〇8o8o〇(Ljava/io/File;)Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    if-nez v0, :cond_0

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 40
    .line 41
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->o〇0(I)V

    .line 42
    .line 43
    .line 44
    goto/16 :goto_4

    .line 45
    .line 46
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 47
    .line 48
    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->o〇0(I)V

    .line 49
    .line 50
    .line 51
    goto/16 :goto_4

    .line 52
    .line 53
    :cond_1
    invoke-static {}, Lcom/intsig/util/NetworkUtils;->O8()Z

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    const/4 v5, 0x0

    .line 58
    if-eqz v3, :cond_5

    .line 59
    .line 60
    if-eqz v1, :cond_3

    .line 61
    .line 62
    invoke-static {v1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 63
    .line 64
    .line 65
    move-result v3

    .line 66
    if-eqz v3, :cond_2

    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_2
    const/4 v3, 0x0

    .line 70
    goto :goto_1

    .line 71
    :cond_3
    :goto_0
    const/4 v3, 0x1

    .line 72
    :goto_1
    if-eqz v3, :cond_4

    .line 73
    .line 74
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 75
    .line 76
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    invoke-static {v1, v0}, Lcom/intsig/camscanner/db/dao/ImageDao;->Oo08(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    if-eqz v1, :cond_5

    .line 85
    .line 86
    iget-object v3, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->O8:Ljava/lang/String;

    .line 87
    .line 88
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    invoke-static {v3, v0, v1, v2}, Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;->〇080(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    if-eqz v1, :cond_5

    .line 97
    .line 98
    move-object v12, v1

    .line 99
    goto :goto_2

    .line 100
    :cond_4
    iget-object v3, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->O8:Ljava/lang/String;

    .line 101
    .line 102
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v2

    .line 106
    invoke-static {v3, v0, v1, v2}, Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;->〇080(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;

    .line 107
    .line 108
    .line 109
    move-result-object v5

    .line 110
    :cond_5
    move-object v12, v5

    .line 111
    :goto_2
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 112
    .line 113
    invoke-virtual {v1, v12}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->Oo08(Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;)V

    .line 114
    .line 115
    .line 116
    if-nez v12, :cond_6

    .line 117
    .line 118
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 119
    .line 120
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->o〇0(I)V

    .line 121
    .line 122
    .line 123
    goto :goto_3

    .line 124
    :cond_6
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->Oo08:Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 125
    .line 126
    invoke-virtual {v1, v11}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->o〇0(I)V

    .line 127
    .line 128
    .line 129
    sget-object v1, Lcom/intsig/camscanner/db/dao/BankCardJournalDao;->〇080:Lcom/intsig/camscanner/db/dao/BankCardJournalDao;

    .line 130
    .line 131
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 132
    .line 133
    invoke-virtual {v2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 134
    .line 135
    .line 136
    move-result-object v2

    .line 137
    invoke-virtual {v12}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getUpload_time()J

    .line 138
    .line 139
    .line 140
    move-result-wide v3

    .line 141
    invoke-virtual {v12}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getStart_trade_data()Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v6

    .line 145
    invoke-virtual {v12}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getEnd_trade_data()Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v7

    .line 149
    invoke-virtual {v12}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getIncome()F

    .line 150
    .line 151
    .line 152
    move-result v8

    .line 153
    invoke-virtual {v12}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getExpenses()F

    .line 154
    .line 155
    .line 156
    move-result v9

    .line 157
    invoke-virtual {v12}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getTitle()Ljava/lang/String;

    .line 158
    .line 159
    .line 160
    move-result-object v10

    .line 161
    move-object v5, v0

    .line 162
    invoke-virtual/range {v1 .. v10}, Lcom/intsig/camscanner/db/dao/BankCardJournalDao;->Oo08(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;FFLjava/lang/String;)Z

    .line 163
    .line 164
    .line 165
    move-result v1

    .line 166
    new-instance v2, Ljava/lang/StringBuilder;

    .line 167
    .line 168
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    .line 170
    .line 171
    const-string v3, "updatePageRecord pageSyncId:"

    .line 172
    .line 173
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    .line 175
    .line 176
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    .line 178
    .line 179
    const-string v3, ", result:"

    .line 180
    .line 181
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    .line 183
    .line 184
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 185
    .line 186
    .line 187
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object v1

    .line 191
    const-string v2, "DownloadPageBankCardJournalTaskData"

    .line 192
    .line 193
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    .line 195
    .line 196
    :goto_3
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;->o〇0:Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData$OnBankCardJournalCompleteListener;

    .line 197
    .line 198
    if-eqz v1, :cond_7

    .line 199
    .line 200
    invoke-interface {v1, v0, v12, v11}, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData$OnBankCardJournalCompleteListener;->〇080(Ljava/lang/String;Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;I)V

    .line 201
    .line 202
    .line 203
    :cond_7
    :goto_4
    if-eqz p1, :cond_8

    .line 204
    .line 205
    const/4 v0, 0x0

    .line 206
    invoke-interface {p1, v0, v0}, Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataCallback;->〇o〇(FF)V

    .line 207
    .line 208
    .line 209
    :cond_8
    return-void
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

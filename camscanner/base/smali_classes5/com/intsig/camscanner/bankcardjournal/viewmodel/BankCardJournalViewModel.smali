.class public final Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "BankCardJournalViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o〇00O:Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final OO:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:Z

.field private 〇08O〇00〇o:I

.field private 〇OOo8〇0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->o〇00O:Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇〇0〇〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    const-class v0, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageListHolder;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/singleton/Singleton;->〇080(Ljava/lang/Class;)Lcom/intsig/singleton/Singleton;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "null cannot be cast to non-null type com.intsig.camscanner.bankcardjournal.model.BankCardJournalPageListHolder"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    check-cast v0, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageListHolder;

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/singleton/SingletonDataHolder;->〇o00〇〇Oo(Z)Ljava/util/List;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 25
    .line 26
    check-cast v0, Ljava/util/Collection;

    .line 27
    .line 28
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method


# virtual methods
.method public final O8ooOoo〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O8〇o()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final Oooo8o0〇()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O〇O〇oO()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 8
    .line 9
    iput v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o0ooO()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->o0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final oO()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-lt v0, v1, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    return v0

    .line 13
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    add-int/2addr v0, v1

    .line 17
    iput v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 18
    .line 19
    return v1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected onCleared()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/lifecycle/ViewModel;->onCleared()V

    .line 2
    .line 3
    .line 4
    const-string v0, "BankCardJournalViewModel"

    .line 5
    .line 6
    const-string v1, "onCleared"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final oo88o8O()Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/bankcardjournal/model/BankBaseItemData;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 6
    .line 7
    .line 8
    iget v2, v0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 9
    .line 10
    iget-object v3, v0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v3

    .line 16
    if-lt v2, v3, :cond_0

    .line 17
    .line 18
    return-object v1

    .line 19
    :cond_0
    iget-object v2, v0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 20
    .line 21
    iget v3, v0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 22
    .line 23
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    check-cast v2, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 28
    .line 29
    invoke-virtual {v2}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇080()Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    if-nez v2, :cond_1

    .line 34
    .line 35
    return-object v1

    .line 36
    :cond_1
    new-instance v9, Lcom/intsig/camscanner/bankcardjournal/model/BankGeneralItemData;

    .line 37
    .line 38
    invoke-virtual {v2}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getCount()I

    .line 39
    .line 40
    .line 41
    move-result v4

    .line 42
    invoke-virtual {v2}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getIncome()F

    .line 43
    .line 44
    .line 45
    move-result v5

    .line 46
    invoke-virtual {v2}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getExpenses()F

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    invoke-virtual {v2}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getSelf_account()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v7

    .line 54
    invoke-virtual {v2}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getSelf_name()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v8

    .line 58
    move-object v3, v9

    .line 59
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/bankcardjournal/model/BankGeneralItemData;-><init>(IFFLjava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    .line 64
    .line 65
    invoke-virtual {v2}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getTrade_list()Ljava/util/List;

    .line 66
    .line 67
    .line 68
    move-result-object v3

    .line 69
    if-eqz v3, :cond_2

    .line 70
    .line 71
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    goto :goto_0

    .line 76
    :cond_2
    const/4 v3, 0x0

    .line 77
    :goto_0
    invoke-virtual {v2}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getTrade_list()Ljava/util/List;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    if-eqz v2, :cond_5

    .line 82
    .line 83
    check-cast v2, Ljava/lang/Iterable;

    .line 84
    .line 85
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    const/4 v5, 0x0

    .line 90
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 91
    .line 92
    .line 93
    move-result v6

    .line 94
    if-eqz v6, :cond_5

    .line 95
    .line 96
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 97
    .line 98
    .line 99
    move-result-object v6

    .line 100
    add-int/lit8 v7, v5, 0x1

    .line 101
    .line 102
    if-gez v5, :cond_3

    .line 103
    .line 104
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 105
    .line 106
    .line 107
    :cond_3
    check-cast v6, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalPageItemData;

    .line 108
    .line 109
    new-instance v15, Lcom/intsig/camscanner/bankcardjournal/model/BankContentItemData;

    .line 110
    .line 111
    invoke-virtual {v6}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalPageItemData;->getOther_name()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v9

    .line 115
    invoke-virtual {v6}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalPageItemData;->getTrade_data()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v10

    .line 119
    invoke-virtual {v6}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalPageItemData;->getSummary()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v11

    .line 123
    invoke-virtual {v6}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalPageItemData;->getTrade_amount()Ljava/lang/Float;

    .line 124
    .line 125
    .line 126
    move-result-object v12

    .line 127
    invoke-virtual {v6}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalPageItemData;->getRest_amount()Ljava/lang/Float;

    .line 128
    .line 129
    .line 130
    move-result-object v13

    .line 131
    invoke-virtual {v6}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalPageItemData;->getErr_type()Ljava/lang/Integer;

    .line 132
    .line 133
    .line 134
    move-result-object v14

    .line 135
    invoke-virtual {v6}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalPageItemData;->getPosition()Ljava/util/List;

    .line 136
    .line 137
    .line 138
    move-result-object v6

    .line 139
    const/4 v8, 0x1

    .line 140
    add-int/lit8 v4, v3, -0x1

    .line 141
    .line 142
    if-eq v5, v4, :cond_4

    .line 143
    .line 144
    const/16 v16, 0x1

    .line 145
    .line 146
    goto :goto_2

    .line 147
    :cond_4
    const/16 v16, 0x0

    .line 148
    .line 149
    :goto_2
    const/16 v17, 0x0

    .line 150
    .line 151
    const/16 v18, 0x100

    .line 152
    .line 153
    const/16 v19, 0x0

    .line 154
    .line 155
    move-object v8, v15

    .line 156
    move-object v4, v15

    .line 157
    move-object v15, v6

    .line 158
    invoke-direct/range {v8 .. v19}, Lcom/intsig/camscanner/bankcardjournal/model/BankContentItemData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/util/List;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 159
    .line 160
    .line 161
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    .line 163
    .line 164
    move v5, v7

    .line 165
    goto :goto_1

    .line 166
    :cond_5
    return-object v1
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final oo〇()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o〇0OOo〇0(Landroid/content/Intent;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇〇〇0〇〇0()V

    .line 2
    .line 3
    .line 4
    if-nez p1, :cond_0

    .line 5
    .line 6
    const-string p1, "BankCardJournalViewModel"

    .line 7
    .line 8
    const-string v0, "loadData intent == null"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    const-string v0, "key_extra_first_show_position"

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    iput v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 22
    .line 23
    const-string v0, "extra_doc_id"

    .line 24
    .line 25
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇OOo8〇0:Ljava/lang/String;

    .line 30
    .line 31
    const-string v0, "KEY_EXTRA_SHOW_DONE"

    .line 32
    .line 33
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    iput-boolean p1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->o0:Z

    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final o〇8oOO88(Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;)V
    .locals 4
    .param p1    # Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "bankCardJournalPageData"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇OOo8〇0:Ljava/lang/String;

    .line 9
    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 11
    .line 12
    .line 13
    move-result-wide v2

    .line 14
    invoke-direct {v0, v1, p1, v2, v3}, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;J)V

    .line 15
    .line 16
    .line 17
    sget-object p1, Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;->〇080:Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;->〇80〇808〇O()Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    const/4 v2, 0x0

    .line 24
    invoke-virtual {v1, v0, v2, v2}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->OoO8(Lcom/intsig/camscanner/tsapp/request/RequestTaskData;ZZ)V

    .line 25
    .line 26
    .line 27
    sget-object v0, Lcom/intsig/camscanner/bankcardjournal/request/BankCardJournalDownloadClient;->〇8o8o〇:Lcom/intsig/camscanner/bankcardjournal/request/BankCardJournalDownloadClient$Companion;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/bankcardjournal/request/BankCardJournalDownloadClient$Companion;->〇080()Lcom/intsig/camscanner/bankcardjournal/request/BankCardJournalDownloadClient;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;->〇80〇808〇O()Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/tsapp/request/RequestTaskClient;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/tsapp/request/RequestTask;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final o〇O(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇00()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 7
    .line 8
    iget v2, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 9
    .line 10
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    check-cast v1, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 15
    .line 16
    invoke-virtual {v1}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇o〇()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-static {v1, v0}, Lcom/intsig/camscanner/db/dao/ImageDao;->o〇8oOO88(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    return-object v0
.end method

.method public final 〇80〇808〇O()Z
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    if-lt v0, v1, :cond_0

    .line 11
    .line 12
    return v2

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 16
    .line 17
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->O8()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-nez v0, :cond_1

    .line 28
    .line 29
    const/4 v2, 0x1

    .line 30
    :cond_1
    return v2
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇8o8o〇()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 7
    .line 8
    check-cast v1, Ljava/lang/Iterable;

    .line 9
    .line 10
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    if-eqz v2, :cond_0

    .line 19
    .line 20
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    check-cast v2, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇o〇()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 35
    .line 36
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-static {v1, v0}, Lcom/intsig/camscanner/db/dao/ImageDao;->o〇8oOO88(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final 〇O00()Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-lt v0, v1, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    return-object v0

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 16
    .line 17
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇080()Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇oo〇()I
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    if-lt v0, v1, :cond_0

    .line 11
    .line 12
    return v2

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->OO:Ljava/util/List;

    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/camscanner/bankcardjournal/viewmodel/BankCardJournalViewModel;->〇08O〇00〇o:I

    .line 16
    .line 17
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇080()Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-nez v0, :cond_1

    .line 28
    .line 29
    return v2

    .line 30
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;->getTrade_list()Ljava/util/List;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    if-eqz v0, :cond_5

    .line 35
    .line 36
    check-cast v0, Ljava/lang/Iterable;

    .line 37
    .line 38
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    const/4 v1, 0x0

    .line 43
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 44
    .line 45
    .line 46
    move-result v3

    .line 47
    if-eqz v3, :cond_4

    .line 48
    .line 49
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    check-cast v3, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalPageItemData;

    .line 54
    .line 55
    invoke-virtual {v3}, Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalPageItemData;->getErr_type()Ljava/lang/Integer;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    if-eqz v3, :cond_3

    .line 60
    .line 61
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    goto :goto_1

    .line 66
    :cond_3
    const/4 v3, 0x0

    .line 67
    :goto_1
    if-lez v3, :cond_2

    .line 68
    .line 69
    add-int/lit8 v1, v1, 0x1

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_4
    move v2, v1

    .line 73
    :cond_5
    return v2
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

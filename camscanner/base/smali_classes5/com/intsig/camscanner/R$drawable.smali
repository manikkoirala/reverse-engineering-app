.class public final Lcom/intsig/camscanner/R$drawable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final a_key_login_cs_logo:I = 0x7f08004d

.field public static final a_lable_channel_ocr:I = 0x7f08004e

.field public static final ab_baground_cs:I = 0x7f08004f

.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f080050

.field public static final abc_action_bar_item_background_material:I = 0x7f080051

.field public static final abc_btn_borderless_material:I = 0x7f080052

.field public static final abc_btn_check_material:I = 0x7f080053

.field public static final abc_btn_check_material_anim:I = 0x7f080054

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f080055

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f080056

.field public static final abc_btn_colored_material:I = 0x7f080057

.field public static final abc_btn_default_mtrl_shape:I = 0x7f080058

.field public static final abc_btn_radio_material:I = 0x7f080059

.field public static final abc_btn_radio_material_anim:I = 0x7f08005a

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f08005b

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f08005c

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f08005d

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f08005e

.field public static final abc_cab_background_internal_bg:I = 0x7f08005f

.field public static final abc_cab_background_top_material:I = 0x7f080060

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f080061

.field public static final abc_control_background_material:I = 0x7f080062

.field public static final abc_dialog_material_background:I = 0x7f080063

.field public static final abc_edit_text_material:I = 0x7f080064

.field public static final abc_ic_ab_back_material:I = 0x7f080065

.field public static final abc_ic_arrow_drop_right_black_24dp:I = 0x7f080066

.field public static final abc_ic_clear_material:I = 0x7f080067

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f080068

.field public static final abc_ic_go_search_api_material:I = 0x7f080069

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f08006a

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f08006b

.field public static final abc_ic_menu_overflow_material:I = 0x7f08006c

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f08006d

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f08006e

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f08006f

.field public static final abc_ic_search_api_material:I = 0x7f080070

.field public static final abc_ic_voice_search_api_material:I = 0x7f080071

.field public static final abc_item_background_holo_dark:I = 0x7f080072

.field public static final abc_item_background_holo_light:I = 0x7f080073

.field public static final abc_list_divider_material:I = 0x7f080074

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f080075

.field public static final abc_list_focused_holo:I = 0x7f080076

.field public static final abc_list_longpressed_holo:I = 0x7f080077

.field public static final abc_list_pressed_holo_dark:I = 0x7f080078

.field public static final abc_list_pressed_holo_light:I = 0x7f080079

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f08007a

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f08007b

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f08007c

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f08007d

.field public static final abc_list_selector_holo_dark:I = 0x7f08007e

.field public static final abc_list_selector_holo_light:I = 0x7f08007f

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f080080

.field public static final abc_popup_background_mtrl_mult:I = 0x7f080081

.field public static final abc_ratingbar_indicator_material:I = 0x7f080082

.field public static final abc_ratingbar_material:I = 0x7f080083

.field public static final abc_ratingbar_small_material:I = 0x7f080084

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f080085

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f080086

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f080087

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f080088

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f080089

.field public static final abc_seekbar_thumb_material:I = 0x7f08008a

.field public static final abc_seekbar_tick_mark_material:I = 0x7f08008b

.field public static final abc_seekbar_track_material:I = 0x7f08008c

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f08008d

.field public static final abc_spinner_textfield_background_material:I = 0x7f08008e

.field public static final abc_star_black_48dp:I = 0x7f08008f

.field public static final abc_star_half_black_48dp:I = 0x7f080090

.field public static final abc_switch_thumb_material:I = 0x7f080091

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f080092

.field public static final abc_tab_indicator_material:I = 0x7f080093

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f080094

.field public static final abc_text_cursor_material:I = 0x7f080095

.field public static final abc_text_select_handle_left_mtrl:I = 0x7f080096

.field public static final abc_text_select_handle_middle_mtrl:I = 0x7f080097

.field public static final abc_text_select_handle_right_mtrl:I = 0x7f080098

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f080099

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f08009a

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f08009b

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f08009c

.field public static final abc_textfield_search_material:I = 0x7f08009d

.field public static final abc_vector_test:I = 0x7f08009e

.field public static final account_common_btn_bg:I = 0x7f08009f

.field public static final ad_action_bg:I = 0x7f0800a0

.field public static final ad_action_btn_bg_selector:I = 0x7f0800a1

.field public static final ad_action_btn_normal:I = 0x7f0800a2

.field public static final ad_action_btn_pressed:I = 0x7f0800a3

.field public static final ad_applaunch_btn_bg:I = 0x7f0800a4

.field public static final ad_close_icon:I = 0x7f0800a5

.field public static final ad_doc_bg_stroke:I = 0x7f0800a6

.field public static final ad_move_mask:I = 0x7f0800a7

.field public static final ad_tag_bg:I = 0x7f0800a8

.field public static final ad_tag_bg_lb_rt:I = 0x7f0800a9

.field public static final ad_tag_bg_rt:I = 0x7f0800aa

.field public static final admob_close_button_black_circle_white_cross:I = 0x7f0800ab

.field public static final admob_close_button_white_circle_black_cross:I = 0x7f0800ac

.field public static final ai_entrance:I = 0x7f0800ad

.field public static final ai_entrance_circle:I = 0x7f0800ae

.field public static final ai_entrance_fold:I = 0x7f0800af

.field public static final ai_entrance_shadow_left:I = 0x7f0800b0

.field public static final ai_entrance_shadow_middle:I = 0x7f0800b1

.field public static final ai_entrance_shadow_right:I = 0x7f0800b2

.field public static final al_exo_controls_fastforward:I = 0x7f0800b3

.field public static final al_exo_controls_fullscreen_enter:I = 0x7f0800b4

.field public static final al_exo_controls_fullscreen_exit:I = 0x7f0800b5

.field public static final al_exo_controls_next:I = 0x7f0800b6

.field public static final al_exo_controls_pause:I = 0x7f0800b7

.field public static final al_exo_controls_play:I = 0x7f0800b8

.field public static final al_exo_controls_previous:I = 0x7f0800b9

.field public static final al_exo_controls_repeat_all:I = 0x7f0800ba

.field public static final al_exo_controls_repeat_off:I = 0x7f0800bb

.field public static final al_exo_controls_repeat_one:I = 0x7f0800bc

.field public static final al_exo_controls_rewind:I = 0x7f0800bd

.field public static final al_exo_controls_shuffle_off:I = 0x7f0800be

.field public static final al_exo_controls_shuffle_on:I = 0x7f0800bf

.field public static final al_exo_controls_vr:I = 0x7f0800c0

.field public static final al_exo_notification_fastforward:I = 0x7f0800c1

.field public static final al_exo_notification_next:I = 0x7f0800c2

.field public static final al_exo_notification_pause:I = 0x7f0800c3

.field public static final al_exo_notification_play:I = 0x7f0800c4

.field public static final al_exo_notification_previous:I = 0x7f0800c5

.field public static final al_exo_notification_rewind:I = 0x7f0800c6

.field public static final al_exo_notification_small_icon:I = 0x7f0800c7

.field public static final al_exo_notification_stop:I = 0x7f0800c8

.field public static final al_exo_styled_controls_audiotrack:I = 0x7f0800c9

.field public static final al_exo_styled_controls_check:I = 0x7f0800ca

.field public static final al_exo_styled_controls_fastforward:I = 0x7f0800cb

.field public static final al_exo_styled_controls_fullscreen_enter:I = 0x7f0800cc

.field public static final al_exo_styled_controls_fullscreen_exit:I = 0x7f0800cd

.field public static final al_exo_styled_controls_next:I = 0x7f0800ce

.field public static final al_exo_styled_controls_overflow_hide:I = 0x7f0800cf

.field public static final al_exo_styled_controls_overflow_show:I = 0x7f0800d0

.field public static final al_exo_styled_controls_pause:I = 0x7f0800d1

.field public static final al_exo_styled_controls_play:I = 0x7f0800d2

.field public static final al_exo_styled_controls_previous:I = 0x7f0800d3

.field public static final al_exo_styled_controls_repeat_all:I = 0x7f0800d4

.field public static final al_exo_styled_controls_repeat_off:I = 0x7f0800d5

.field public static final al_exo_styled_controls_repeat_one:I = 0x7f0800d6

.field public static final al_exo_styled_controls_rewind:I = 0x7f0800d7

.field public static final al_exo_styled_controls_settings:I = 0x7f0800d8

.field public static final al_exo_styled_controls_shuffle_off:I = 0x7f0800d9

.field public static final al_exo_styled_controls_shuffle_on:I = 0x7f0800da

.field public static final al_exo_styled_controls_speed:I = 0x7f0800db

.field public static final al_exo_styled_controls_subtitle_off:I = 0x7f0800dc

.field public static final al_exo_styled_controls_subtitle_on:I = 0x7f0800dd

.field public static final al_exo_styled_controls_vr:I = 0x7f0800de

.field public static final app_splash:I = 0x7f0800df

.field public static final applaunch_bg:I = 0x7f0800e0

.field public static final applovin_consent_flow_gdpr_flow_switch_thumb:I = 0x7f0800e1

.field public static final applovin_consent_flow_gdpr_flow_switch_track:I = 0x7f0800e2

.field public static final applovin_consent_flow_gdpr_positive_button_background:I = 0x7f0800e3

.field public static final applovin_consent_flow_gdpr_rounded_background:I = 0x7f0800e4

.field public static final applovin_creative_debugger_report_ad_rounded_button:I = 0x7f0800e5

.field public static final applovin_exo_edit_mode_logo:I = 0x7f0800e6

.field public static final applovin_exo_ic_audiotrack:I = 0x7f0800e7

.field public static final applovin_exo_ic_check:I = 0x7f0800e8

.field public static final applovin_exo_ic_chevron_left:I = 0x7f0800e9

.field public static final applovin_exo_ic_chevron_right:I = 0x7f0800ea

.field public static final applovin_exo_ic_default_album_image:I = 0x7f0800eb

.field public static final applovin_exo_ic_forward:I = 0x7f0800ec

.field public static final applovin_exo_ic_fullscreen_enter:I = 0x7f0800ed

.field public static final applovin_exo_ic_fullscreen_exit:I = 0x7f0800ee

.field public static final applovin_exo_ic_pause_circle_filled:I = 0x7f0800ef

.field public static final applovin_exo_ic_play_circle_filled:I = 0x7f0800f0

.field public static final applovin_exo_ic_rewind:I = 0x7f0800f1

.field public static final applovin_exo_ic_settings:I = 0x7f0800f2

.field public static final applovin_exo_ic_skip_next:I = 0x7f0800f3

.field public static final applovin_exo_ic_skip_previous:I = 0x7f0800f4

.field public static final applovin_exo_ic_speed:I = 0x7f0800f5

.field public static final applovin_exo_ic_subtitle_off:I = 0x7f0800f6

.field public static final applovin_exo_ic_subtitle_on:I = 0x7f0800f7

.field public static final applovin_exo_icon_circular_play:I = 0x7f0800f8

.field public static final applovin_exo_icon_fastforward:I = 0x7f0800f9

.field public static final applovin_exo_icon_fullscreen_enter:I = 0x7f0800fa

.field public static final applovin_exo_icon_fullscreen_exit:I = 0x7f0800fb

.field public static final applovin_exo_icon_next:I = 0x7f0800fc

.field public static final applovin_exo_icon_pause:I = 0x7f0800fd

.field public static final applovin_exo_icon_play:I = 0x7f0800fe

.field public static final applovin_exo_icon_previous:I = 0x7f0800ff

.field public static final applovin_exo_icon_repeat_all:I = 0x7f080100

.field public static final applovin_exo_icon_repeat_off:I = 0x7f080101

.field public static final applovin_exo_icon_repeat_one:I = 0x7f080102

.field public static final applovin_exo_icon_rewind:I = 0x7f080103

.field public static final applovin_exo_icon_shuffle_off:I = 0x7f080104

.field public static final applovin_exo_icon_shuffle_on:I = 0x7f080105

.field public static final applovin_exo_icon_stop:I = 0x7f080106

.field public static final applovin_exo_icon_vr:I = 0x7f080107

.field public static final applovin_exo_rounded_rectangle:I = 0x7f080108

.field public static final applovin_ic_baseline_add_circle_outline:I = 0x7f080109

.field public static final applovin_ic_check_mark_bordered:I = 0x7f08010a

.field public static final applovin_ic_check_mark_borderless:I = 0x7f08010b

.field public static final applovin_ic_disclosure_arrow:I = 0x7f08010c

.field public static final applovin_ic_mediation_adcolony:I = 0x7f08010d

.field public static final applovin_ic_mediation_admob:I = 0x7f08010e

.field public static final applovin_ic_mediation_amazon_marketplace:I = 0x7f08010f

.field public static final applovin_ic_mediation_applovin:I = 0x7f080110

.field public static final applovin_ic_mediation_bidmachine:I = 0x7f080111

.field public static final applovin_ic_mediation_chartboost:I = 0x7f080112

.field public static final applovin_ic_mediation_criteo:I = 0x7f080113

.field public static final applovin_ic_mediation_facebook:I = 0x7f080114

.field public static final applovin_ic_mediation_fyber:I = 0x7f080115

.field public static final applovin_ic_mediation_google_ad_manager:I = 0x7f080116

.field public static final applovin_ic_mediation_hyprmx:I = 0x7f080117

.field public static final applovin_ic_mediation_inmobi:I = 0x7f080118

.field public static final applovin_ic_mediation_ironsource:I = 0x7f080119

.field public static final applovin_ic_mediation_line:I = 0x7f08011a

.field public static final applovin_ic_mediation_maio:I = 0x7f08011b

.field public static final applovin_ic_mediation_mintegral:I = 0x7f08011c

.field public static final applovin_ic_mediation_mobilefuse:I = 0x7f08011d

.field public static final applovin_ic_mediation_mytarget:I = 0x7f08011e

.field public static final applovin_ic_mediation_nend:I = 0x7f08011f

.field public static final applovin_ic_mediation_ogury_presage:I = 0x7f080120

.field public static final applovin_ic_mediation_pangle:I = 0x7f080121

.field public static final applovin_ic_mediation_placeholder:I = 0x7f080122

.field public static final applovin_ic_mediation_smaato:I = 0x7f080123

.field public static final applovin_ic_mediation_tapjoy:I = 0x7f080124

.field public static final applovin_ic_mediation_tiktok:I = 0x7f080125

.field public static final applovin_ic_mediation_unity:I = 0x7f080126

.field public static final applovin_ic_mediation_verve:I = 0x7f080127

.field public static final applovin_ic_mediation_vungle:I = 0x7f080128

.field public static final applovin_ic_mediation_yandex:I = 0x7f080129

.field public static final applovin_ic_mute_to_unmute:I = 0x7f08012a

.field public static final applovin_ic_pause_icon:I = 0x7f08012b

.field public static final applovin_ic_play_icon:I = 0x7f08012c

.field public static final applovin_ic_privacy_icon:I = 0x7f08012d

.field public static final applovin_ic_privacy_icon_layered_list:I = 0x7f08012e

.field public static final applovin_ic_replay_icon:I = 0x7f08012f

.field public static final applovin_ic_unmute_to_mute:I = 0x7f080130

.field public static final applovin_ic_warning:I = 0x7f080131

.field public static final applovin_ic_warning_outline:I = 0x7f080132

.field public static final applovin_ic_white_small:I = 0x7f080133

.field public static final applovin_ic_x_mark:I = 0x7f080134

.field public static final applovin_rounded_black_background:I = 0x7f080135

.field public static final applovin_rounded_button:I = 0x7f080136

.field public static final applovin_rounded_text_view_border:I = 0x7f080137

.field public static final arrow_pagelist_tip:I = 0x7f080138

.field public static final avd_hide_password:I = 0x7f080139

.field public static final avd_show_password:I = 0x7f08013a

.field public static final baidu_toutiao_back:I = 0x7f08013b

.field public static final banner_10g_cloud:I = 0x7f08013c

.field public static final banner_360_324:I = 0x7f08013d

.field public static final banner_ad:I = 0x7f08013e

.field public static final banner_ad_285_285px:I = 0x7f08013f

.field public static final banner_autograph_region_294_212px:I = 0x7f080140

.field public static final banner_blank_doc_160px:I = 0x7f080141

.field public static final banner_blank_folder_160px:I = 0x7f080142

.field public static final banner_blank_move_160px:I = 0x7f080143

.field public static final banner_blank_search_160px:I = 0x7f080144

.field public static final banner_buy_vip_again:I = 0x7f080145

.field public static final banner_car_card:I = 0x7f080146

.field public static final banner_cloud_ocr:I = 0x7f080147

.field public static final banner_collage_285_285px:I = 0x7f080148

.field public static final banner_driver_license:I = 0x7f080149

.field public static final banner_edu_popup_375_80:I = 0x7f08014a

.field public static final banner_encrypted_link:I = 0x7f08014b

.field public static final banner_ex_bankcard:I = 0x7f08014c

.field public static final banner_ex_car_2:I = 0x7f08014d

.field public static final banner_ex_fangchan:I = 0x7f08014e

.field public static final banner_ex_hukoubo:I = 0x7f08014f

.field public static final banner_ex_hukoubo_2:I = 0x7f080150

.field public static final banner_ex_idcard:I = 0x7f080151

.field public static final banner_ex_pass:I = 0x7f080152

.field public static final banner_ex_pass_2:I = 0x7f080153

.field public static final banner_ex_yingyezhizhao:I = 0x7f080154

.field public static final banner_excel:I = 0x7f080155

.field public static final banner_folder:I = 0x7f080156

.field public static final banner_greetingcard_album:I = 0x7f080157

.field public static final banner_icon:I = 0x7f080158

.field public static final banner_id_card_m:I = 0x7f080159

.field public static final banner_id_china:I = 0x7f08015a

.field public static final banner_id_other:I = 0x7f08015b

.field public static final banner_idcard_285_285px:I = 0x7f08015c

.field public static final banner_image_quality:I = 0x7f08015d

.field public static final banner_more_285_285px:I = 0x7f08015e

.field public static final banner_ocr_285_285px:I = 0x7f08015f

.field public static final banner_ocr_proofread:I = 0x7f080160

.field public static final banner_ocr_share:I = 0x7f080161

.field public static final banner_pdf_285_285px:I = 0x7f080162

.field public static final banner_pdf_jpg:I = 0x7f080163

.field public static final banner_ppt:I = 0x7f080164

.field public static final banner_puzzle:I = 0x7f080165

.field public static final banner_repairphotos:I = 0x7f080166

.field public static final banner_resolution:I = 0x7f080167

.field public static final banner_score:I = 0x7f080168

.field public static final banner_team_folder_done:I = 0x7f080169

.field public static final banner_translation:I = 0x7f08016a

.field public static final banner_unlimited_scans_285_285px:I = 0x7f08016b

.field public static final banner_upload:I = 0x7f08016c

.field public static final banner_upload_cloud:I = 0x7f08016d

.field public static final banner_vip:I = 0x7f08016e

.field public static final banner_vip_book:I = 0x7f08016f

.field public static final banner_vip_compress:I = 0x7f080170

.field public static final banner_vip_encryption:I = 0x7f080171

.field public static final banner_vip_encryption_pdf_ad:I = 0x7f080172

.field public static final banner_vip_enjoy_pc:I = 0x7f080173

.field public static final banner_vip_enjoy_pdf:I = 0x7f080174

.field public static final banner_vip_excel:I = 0x7f080175

.field public static final banner_vip_id:I = 0x7f080176

.field public static final banner_vip_invoice:I = 0x7f080177

.field public static final banner_vip_long_img:I = 0x7f080178

.field public static final banner_vip_more:I = 0x7f080179

.field public static final banner_vip_pc:I = 0x7f08017a

.field public static final banner_vip_pdf:I = 0x7f08017b

.field public static final banner_vip_pdf_word:I = 0x7f08017c

.field public static final banner_vip_seal:I = 0x7f08017d

.field public static final banner_vip_sign:I = 0x7f08017e

.field public static final banner_vip_signature:I = 0x7f08017f

.field public static final banner_vip_signatures:I = 0x7f080180

.field public static final banner_vip_word:I = 0x7f080181

.field public static final banner_word_285_285px:I = 0x7f080182

.field public static final base_bg_light_black:I = 0x7f080183

.field public static final bg0_corner_4_stroke_border_1:I = 0x7f080184

.field public static final bg_000000_40_corner_4:I = 0x7f080185

.field public static final bg_000000_bottom_corner_4:I = 0x7f080186

.field public static final bg_000000_corner_4:I = 0x7f080187

.field public static final bg_0dacd5_corner4:I = 0x7f080188

.field public static final bg_1919bcaa_corner_8:I = 0x7f080189

.field public static final bg_19bcaa_1dp:I = 0x7f08018a

.field public static final bg_19bcaa_corner_2:I = 0x7f08018b

.field public static final bg_19bcaa_corner_38:I = 0x7f08018c

.field public static final bg_19bcaa_corner_4:I = 0x7f08018d

.field public static final bg_19bcaa_corner_4_bottom:I = 0x7f08018e

.field public static final bg_19bcaa_corner_4_transport:I = 0x7f08018f

.field public static final bg_19bcaa_corner_8:I = 0x7f080190

.field public static final bg_19bcaa_corner_8_bottom:I = 0x7f080191

.field public static final bg_19bcaa_solid_corner_27dp:I = 0x7f080192

.field public static final bg_19bcaa_solid_corner_40dp:I = 0x7f080193

.field public static final bg_1a19bcaa_round_corner_4:I = 0x7f080194

.field public static final bg_1a19bcaa_round_corner_4_stroke_1_19bcaa:I = 0x7f080195

.field public static final bg_1a19bcaa_round_corner_8:I = 0x7f080196

.field public static final bg_1a19bcaa_round_corner_8_stroke_1_19bcaa:I = 0x7f080197

.field public static final bg_212121_corner_20:I = 0x7f080198

.field public static final bg_33000000_corner2:I = 0x7f080199

.field public static final bg_33ffffff_corner_16:I = 0x7f08019a

.field public static final bg_33ffffff_corner_8:I = 0x7f08019b

.field public static final bg_40_000000_corner_16:I = 0x7f08019c

.field public static final bg_40_black_corner_4_bottom:I = 0x7f08019d

.field public static final bg_4d000000_corner19:I = 0x7f08019e

.field public static final bg_4d000000_corner_14:I = 0x7f08019f

.field public static final bg_4d000000_corner_28:I = 0x7f0801a0

.field public static final bg_4d212121_corner_4:I = 0x7f0801a1

.field public static final bg_5a5a5a_corner_16_border_6:I = 0x7f0801a2

.field public static final bg_66000000_corner_2:I = 0x7f0801a3

.field public static final bg_66000000_corner_4:I = 0x7f0801a4

.field public static final bg_66ffffff_corner_12:I = 0x7f0801a5

.field public static final bg_80212121_corner_20:I = 0x7f0801a6

.field public static final bg_99000000_corner_2:I = 0x7f0801a7

.field public static final bg_99000000_corner_20:I = 0x7f0801a8

.field public static final bg_99000000_corner_8:I = 0x7f0801a9

.field public static final bg_99000000_solid_corner_27dp:I = 0x7f0801aa

.field public static final bg_995a5a5a_corner_14:I = 0x7f0801ab

.field public static final bg_9c9c9c_corner_16:I = 0x7f0801ac

.field public static final bg_ad:I = 0x7f0801ad

.field public static final bg_ad_free_card_s_124_74px_blue:I = 0x7f0801ae

.field public static final bg_add_certificate:I = 0x7f0801af

.field public static final bg_alert_dialog_common:I = 0x7f0801b0

.field public static final bg_back_to_main:I = 0x7f0801b1

.field public static final bg_bank_card_web_finder_guide:I = 0x7f0801b2

.field public static final bg_bank_card_web_finder_scan_qrcode:I = 0x7f0801b3

.field public static final bg_bg0_border1:I = 0x7f0801b4

.field public static final bg_bg0_corner_12:I = 0x7f0801b5

.field public static final bg_black_round_corner_4:I = 0x7f0801b6

.field public static final bg_blue_round_corner_15dp_solid_blue_pressed:I = 0x7f0801b7

.field public static final bg_blue_round_corner_15dp_solid_white_normal_1dp:I = 0x7f0801b8

.field public static final bg_blue_round_corner_alpha:I = 0x7f0801b9

.field public static final bg_blue_round_corner_dialog_normal:I = 0x7f0801ba

.field public static final bg_blue_round_corner_normal:I = 0x7f0801bb

.field public static final bg_blue_round_corner_pressed:I = 0x7f0801bc

.field public static final bg_blue_round_corner_solid_blue_pressed:I = 0x7f0801bd

.field public static final bg_blue_round_corner_solid_gray_pressed:I = 0x7f0801be

.field public static final bg_blue_round_corner_solid_white_normal:I = 0x7f0801bf

.field public static final bg_blue_round_corner_solid_white_normal_1dp:I = 0x7f0801c0

.field public static final bg_blue_round_corner_solid_white_transparent:I = 0x7f0801c1

.field public static final bg_blue_round_corner_stroke_blue:I = 0x7f0801c2

.field public static final bg_blue_white_round_corner:I = 0x7f0801c3

.field public static final bg_bottom_bar_word_list:I = 0x7f0801c4

.field public static final bg_brand_corner_24:I = 0x7f0801c5

.field public static final bg_brand_corner_4:I = 0x7f0801c6

.field public static final bg_btn_19bc9c:I = 0x7f0801c7

.field public static final bg_btn_19bc9c_corner2:I = 0x7f0801c8

.field public static final bg_btn_19bc9c_corner6:I = 0x7f0801c9

.field public static final bg_btn_19bc9c_stroke_21dp:I = 0x7f0801ca

.field public static final bg_btn_19bcaa_corner2:I = 0x7f0801cb

.field public static final bg_btn_4019bcaa_corner2:I = 0x7f0801cc

.field public static final bg_btn_coupon_style:I = 0x7f0801cd

.field public static final bg_btn_ecf0f1:I = 0x7f0801ce

.field public static final bg_btn_f47070:I = 0x7f0801cf

.field public static final bg_btn_ff6161:I = 0x7f0801d0

.field public static final bg_btn_gradient_fdeecc:I = 0x7f0801d1

.field public static final bg_btn_gray:I = 0x7f0801d2

.field public static final bg_btn_green_text:I = 0x7f0801d3

.field public static final bg_btn_hollow_19bc9d:I = 0x7f0801d4

.field public static final bg_btn_hollow_normal:I = 0x7f0801d5

.field public static final bg_btn_red:I = 0x7f0801d6

.field public static final bg_btn_selector:I = 0x7f0801d7

.field public static final bg_button_green_corner_4:I = 0x7f0801d8

.field public static final bg_call_to_action_normal:I = 0x7f0801d9

.field public static final bg_call_to_action_pressed:I = 0x7f0801da

.field public static final bg_call_to_action_selector:I = 0x7f0801db

.field public static final bg_cam_exam_bottom:I = 0x7f0801dc

.field public static final bg_cam_exam_btn:I = 0x7f0801dd

.field public static final bg_cam_exam_download_app:I = 0x7f0801de

.field public static final bg_cam_exam_guide_toolbox:I = 0x7f0801df

.field public static final bg_cam_exam_top:I = 0x7f0801e0

.field public static final bg_cam_exam_txt_left:I = 0x7f0801e1

.field public static final bg_cam_exam_txt_right:I = 0x7f0801e2

.field public static final bg_camcard_ad:I = 0x7f0801e3

.field public static final bg_camcard_ad_pressed:I = 0x7f0801e4

.field public static final bg_camera_guid_hint:I = 0x7f0801e5

.field public static final bg_cancel_btn:I = 0x7f0801e6

.field public static final bg_cancel_btn_gray:I = 0x7f0801e7

.field public static final bg_capture_excel_guide:I = 0x7f0801e8

.field public static final bg_capture_number:I = 0x7f0801e9

.field public static final bg_capture_tips:I = 0x7f0801ea

.field public static final bg_capture_word_guide:I = 0x7f0801eb

.field public static final bg_card_c_120_70px:I = 0x7f0801ec

.field public static final bg_card_vip_floating_92_76:I = 0x7f0801ed

.field public static final bg_cb_handwrite_convert:I = 0x7f0801ee

.field public static final bg_cb_show_password:I = 0x7f0801ef

.field public static final bg_cc000000_corner27:I = 0x7f0801f0

.field public static final bg_cc000000_corner_4:I = 0x7f0801f1

.field public static final bg_cc19bc9c_corner2:I = 0x7f0801f2

.field public static final bg_certificate_a4:I = 0x7f0801f3

.field public static final bg_certificate_menu_mode:I = 0x7f0801f4

.field public static final bg_certificate_normal:I = 0x7f0801f5

.field public static final bg_certificate_tips:I = 0x7f0801f6

.field public static final bg_certificate_top:I = 0x7f0801f7

.field public static final bg_change_icon:I = 0x7f0801f8

.field public static final bg_char_num_remain:I = 0x7f0801f9

.field public static final bg_check_dialog:I = 0x7f0801fa

.field public static final bg_checkbox_gray:I = 0x7f0801fb

.field public static final bg_checkbox_reward_choose:I = 0x7f0801fc

.field public static final bg_checkbox_round_blue:I = 0x7f0801fd

.field public static final bg_checkbox_select_all:I = 0x7f0801fe

.field public static final bg_circle_cs_color_bg1:I = 0x7f0801ff

.field public static final bg_circle_ff7255:I = 0x7f080200

.field public static final bg_cloud_disk_file_pdf:I = 0x7f080201

.field public static final bg_cn_annual_premium:I = 0x7f080202

.field public static final bg_cn_annual_premium_success_1_year:I = 0x7f080203

.field public static final bg_cn_annual_premium_success_1_yuan:I = 0x7f080204

.field public static final bg_cn_annual_premium_success_btn:I = 0x7f080205

.field public static final bg_collage:I = 0x7f080206

.field public static final bg_color_brand_corner_32:I = 0x7f080207

.field public static final bg_color_picker_normal:I = 0x7f080208

.field public static final bg_color_picker_white:I = 0x7f080209

.field public static final bg_colorbg0_border0_corner8:I = 0x7f08020a

.field public static final bg_complete_button:I = 0x7f08020b

.field public static final bg_corner2_border1:I = 0x7f08020c

.field public static final bg_corner_10_ffffff:I = 0x7f08020d

.field public static final bg_corner_2_190dacd5:I = 0x7f08020e

.field public static final bg_corner_2_1919bc51:I = 0x7f08020f

.field public static final bg_corner_2_194580f2:I = 0x7f080210

.field public static final bg_corner_2_198150f8:I = 0x7f080211

.field public static final bg_corner_2_66000000:I = 0x7f080212

.field public static final bg_corner_2_bg1:I = 0x7f080213

.field public static final bg_corner_2_e8e8e8:I = 0x7f080214

.field public static final bg_corner_2_f1f1f1:I = 0x7f080215

.field public static final bg_corner_3_c9dae3:I = 0x7f080216

.field public static final bg_corner_3_ffffff:I = 0x7f080217

.field public static final bg_corner_4_19bc51:I = 0x7f080218

.field public static final bg_corner_4_484849:I = 0x7f080219

.field public static final bg_corner_4_548bf5:I = 0x7f08021a

.field public static final bg_corner_8_bg1:I = 0x7f08021b

.field public static final bg_corner_8_e4bc6e:I = 0x7f08021c

.field public static final bg_corner_8_e5257258:I = 0x7f08021d

.field public static final bg_corner_8_f7f7f7:I = 0x7f08021e

.field public static final bg_corner_8_ffffff:I = 0x7f08021f

.field public static final bg_corner_9_767680:I = 0x7f080220

.field public static final bg_corner_green_solid_green_stroke_green:I = 0x7f080221

.field public static final bg_count_down_purchase_normal:I = 0x7f080222

.field public static final bg_count_down_purchase_red:I = 0x7f080223

.field public static final bg_cs_ai_tip:I = 0x7f080224

.field public static final bg_cs_ai_titles:I = 0x7f080225

.field public static final bg_cs_border_2_conner_4_stroke_1:I = 0x7f080226

.field public static final bg_cs_border_2_conner_5_stroke_1:I = 0x7f080227

.field public static final bg_cs_color_auxiliary_2_stroke_conner_11:I = 0x7f080228

.field public static final bg_cs_color_bg_0_stroke_cs_color_text_2:I = 0x7f080229

.field public static final bg_cs_color_bg_1_1_corner_4:I = 0x7f08022a

.field public static final bg_cs_color_bg_3_corner_4:I = 0x7f08022b

.field public static final bg_cs_color_brand_conner_10_stroke_1:I = 0x7f08022c

.field public static final bg_cs_color_fill_0_corner_2:I = 0x7f08022d

.field public static final bg_cs_color_fill_0_corner_4:I = 0x7f08022e

.field public static final bg_csgrey5_top_corner_8:I = 0x7f08022f

.field public static final bg_csl_purchase_new_style2_left:I = 0x7f080230

.field public static final bg_csl_purchase_new_style2_right:I = 0x7f080231

.field public static final bg_custom_snack_bar:I = 0x7f080232

.field public static final bg_dafaf6_conner_4:I = 0x7f080233

.field public static final bg_danger_corner4:I = 0x7f080234

.field public static final bg_danger_round_corner_normal:I = 0x7f080235

.field public static final bg_dash_f1f1f1_4dp:I = 0x7f080236

.field public static final bg_dcdcdc_corner_26:I = 0x7f080237

.field public static final bg_dcdcdc_corner_4:I = 0x7f080238

.field public static final bg_dcdcdc_corner_8:I = 0x7f080239

.field public static final bg_dialog_coupon:I = 0x7f08023a

.field public static final bg_dialog_exemption:I = 0x7f08023b

.field public static final bg_dialog_vip_activity_purchase_cancel_text:I = 0x7f08023c

.field public static final bg_dialog_vip_activity_purchase_text:I = 0x7f08023d

.field public static final bg_discount:I = 0x7f08023e

.field public static final bg_discount_bottom:I = 0x7f08023f

.field public static final bg_discount_clock:I = 0x7f080240

.field public static final bg_discount_decorate:I = 0x7f080241

.field public static final bg_discount_purchase_v2:I = 0x7f080242

.field public static final bg_divider_light_gray1:I = 0x7f080243

.field public static final bg_doc_gride_no_select_md:I = 0x7f080244

.field public static final bg_doc_gride_stroke:I = 0x7f080245

.field public static final bg_doc_image_stroke:I = 0x7f080246

.field public static final bg_doc_image_stroke_new:I = 0x7f080247

.field public static final bg_doc_image_stroke_pdf:I = 0x7f080248

.field public static final bg_doc_upload:I = 0x7f080249

.field public static final bg_document_capture_guide:I = 0x7f08024a

.field public static final bg_document_more_thumb_image:I = 0x7f08024b

.field public static final bg_document_more_title:I = 0x7f08024c

.field public static final bg_e7e7e7_round_corner_8:I = 0x7f08024d

.field public static final bg_edu_ad:I = 0x7f08024e

.field public static final bg_edu_ad_cover:I = 0x7f08024f

.field public static final bg_enhance_menu:I = 0x7f080250

.field public static final bg_enhance_menu_item_selected:I = 0x7f080251

.field public static final bg_enhance_mode_list:I = 0x7f080252

.field public static final bg_enhance_mode_list_new:I = 0x7f080253

.field public static final bg_explanation_green:I = 0x7f080254

.field public static final bg_f1f1f1_corner_12_top:I = 0x7f080255

.field public static final bg_f1f1f1_corner_2:I = 0x7f080256

.field public static final bg_f1f1f1_corner_3:I = 0x7f080257

.field public static final bg_f1f1f1_stroke_24dp:I = 0x7f080258

.field public static final bg_f4f4f4_corner_6:I = 0x7f080259

.field public static final bg_f6f7f8_top_corner_4:I = 0x7f08025a

.field public static final bg_f7f7f7_border_1_corner_8:I = 0x7f08025b

.field public static final bg_f7f7f7_corner_10:I = 0x7f08025c

.field public static final bg_f7f7f7_corner_2:I = 0x7f08025d

.field public static final bg_f7f7f7_corner_4:I = 0x7f08025e

.field public static final bg_f7f7f7_ffffff_gradient:I = 0x7f08025f

.field public static final bg_f7f7f7_gradient:I = 0x7f080260

.field public static final bg_f7f7f7_round_corner:I = 0x7f080261

.field public static final bg_f7f7f7_round_corner_4:I = 0x7f080262

.field public static final bg_f7f7f7_stroke_24dp:I = 0x7f080263

.field public static final bg_facebook_guid:I = 0x7f080264

.field public static final bg_ff000000_corner_12:I = 0x7f080265

.field public static final bg_ff7d1f_20_corner:I = 0x7f080266

.field public static final bg_ff8227_corner_4:I = 0x7f080267

.field public static final bg_ff8c3e_ff6748_round_corner_100:I = 0x7f080268

.field public static final bg_ff8c3e_ff6748_round_corner_4:I = 0x7f080269

.field public static final bg_ff9312_corner_4:I = 0x7f08026a

.field public static final bg_ffdea1_corner_4:I = 0x7f08026b

.field public static final bg_fff0dd_corner_2:I = 0x7f08026c

.field public static final bg_fff5e3_border_ff8227_corner_4:I = 0x7f08026d

.field public static final bg_fff5e3_corner_4:I = 0x7f08026e

.field public static final bg_ffffff_10_corner_6:I = 0x7f08026f

.field public static final bg_ffffff_corner_4:I = 0x7f080270

.field public static final bg_ffffff_corner_4_bg_0_stroke_border1:I = 0x7f080271

.field public static final bg_ffffff_corner_4_stroke_19bcaa:I = 0x7f080272

.field public static final bg_ffffff_corner_4_stroke_border_1:I = 0x7f080273

.field public static final bg_ffffff_corner_4_stroke_brand:I = 0x7f080274

.field public static final bg_ffffff_round_corner_8:I = 0x7f080275

.field public static final bg_ffffff_stroke_corner_25dp:I = 0x7f080276

.field public static final bg_ffffff_stroke_f1f1f1_corner_4:I = 0x7f080277

.field public static final bg_ffffff_top_corner_12:I = 0x7f080278

.field public static final bg_ffffff_top_corner_8:I = 0x7f080279

.field public static final bg_filter_page:I = 0x7f08027a

.field public static final bg_gallery_enhance_menu:I = 0x7f08027b

.field public static final bg_gallery_enhance_menu_new:I = 0x7f08027c

.field public static final bg_gallery_folder:I = 0x7f08027d

.field public static final bg_gallery_number_guide:I = 0x7f08027e

.field public static final bg_gallery_select_all:I = 0x7f08027f

.field public static final bg_go_visit:I = 0x7f080280

.field public static final bg_gold_round_corner:I = 0x7f080281

.field public static final bg_gp_annual_premium:I = 0x7f080282

.field public static final bg_gp_annual_premium_success_discount:I = 0x7f080283

.field public static final bg_gp_annual_premium_success_free_trial:I = 0x7f080284

.field public static final bg_gp_first_premium_border:I = 0x7f080285

.field public static final bg_gp_solid_f7f7f7_ffffff:I = 0x7f080286

.field public static final bg_gp_solid_ff6748_gradient_corner5:I = 0x7f080287

.field public static final bg_gp_solid_ff6748_gradient_corner8:I = 0x7f080288

.field public static final bg_gp_solid_ff873f_stroke_1:I = 0x7f080289

.field public static final bg_gp_solid_ffc207:I = 0x7f08028a

.field public static final bg_gp_solid_ffe54b_ffbe00:I = 0x7f08028b

.field public static final bg_gp_stroke_ff7255:I = 0x7f08028c

.field public static final bg_gradient_16223d:I = 0x7f08028d

.field public static final bg_gradient_1c1c1e:I = 0x7f08028e

.field public static final bg_gradient_edbe73_f9dca8:I = 0x7f08028f

.field public static final bg_gradient_fcfcfc:I = 0x7f080290

.field public static final bg_gray_border:I = 0x7f080291

.field public static final bg_gray_cancel_btn:I = 0x7f080292

.field public static final bg_gray_document:I = 0x7f080293

.field public static final bg_gray_rectangle_round_corner:I = 0x7f080294

.field public static final bg_gray_round_corner:I = 0x7f080295

.field public static final bg_gray_round_corner_disable:I = 0x7f080296

.field public static final bg_gray_round_corner_new:I = 0x7f080297

.field public static final bg_gray_round_corner_normal:I = 0x7f080298

.field public static final bg_gray_round_corner_solid_white_normal:I = 0x7f080299

.field public static final bg_gray_round_corner_solid_white_screenshot:I = 0x7f08029a

.field public static final bg_gray_round_up_corner_6:I = 0x7f08029b

.field public static final bg_green_round_corner_100:I = 0x7f08029c

.field public static final bg_green_round_corner_6:I = 0x7f08029d

.field public static final bg_grey_enable_green_normal:I = 0x7f08029e

.field public static final bg_grey_round_alpha:I = 0x7f08029f

.field public static final bg_grey_round_corner_disable:I = 0x7f0802a0

.field public static final bg_grey_round_corner_solid_white:I = 0x7f0802a1

.field public static final bg_guid_use_direct:I = 0x7f0802a2

.field public static final bg_guide_btn:I = 0x7f0802a3

.field public static final bg_guide_buy_selected_round_corner_solid_normal:I = 0x7f0802a4

.field public static final bg_guide_buy_selected_round_corner_solid_normal_2:I = 0x7f0802a5

.field public static final bg_guide_buy_unselected_round_corner_solid_normal:I = 0x7f0802a6

.field public static final bg_guide_buy_unselected_round_corner_solid_normal_2:I = 0x7f0802a7

.field public static final bg_guide_gp_purchase_price_white:I = 0x7f0802a8

.field public static final bg_guide_gp_purchase_price_white_2:I = 0x7f0802a9

.field public static final bg_horizontal_gradient_blue:I = 0x7f0802aa

.field public static final bg_horizontal_gradient_bottom_radius_blue:I = 0x7f0802ab

.field public static final bg_horizontal_gradient_gray:I = 0x7f0802ac

.field public static final bg_horizontal_gradient_radius_blue:I = 0x7f0802ad

.field public static final bg_hot_funtion_content_dark:I = 0x7f0802ae

.field public static final bg_hot_funtion_content_light:I = 0x7f0802af

.field public static final bg_ic_search_all_hint:I = 0x7f0802b0

.field public static final bg_id_feature_corner_selected_button:I = 0x7f0802b1

.field public static final bg_id_tips:I = 0x7f0802b2

.field public static final bg_image_scan_next:I = 0x7f0802b3

.field public static final bg_image_scan_pre:I = 0x7f0802b4

.field public static final bg_image_upload:I = 0x7f0802b5

.field public static final bg_index:I = 0x7f0802b6

.field public static final bg_item_new_purchase_11_rv:I = 0x7f0802b7

.field public static final bg_item_new_purchase_rv:I = 0x7f0802b8

.field public static final bg_language_ocr:I = 0x7f0802b9

.field public static final bg_last_share_tip:I = 0x7f0802ba

.field public static final bg_last_share_tip_dd8989:I = 0x7f0802bb

.field public static final bg_last_share_tip_new:I = 0x7f0802bc

.field public static final bg_light_black:I = 0x7f0802bd

.field public static final bg_light_green_round_corner_disable:I = 0x7f0802be

.field public static final bg_link_share_unlogin:I = 0x7f0802bf

.field public static final bg_log_main_banner:I = 0x7f0802c0

.field public static final bg_login_for_compliance_cb_show_pwd:I = 0x7f0802c1

.field public static final bg_login_for_compliance_protocol:I = 0x7f0802c2

.field public static final bg_look_detail:I = 0x7f0802c3

.field public static final bg_looper_top:I = 0x7f0802c4

.field public static final bg_lr_edit_bar:I = 0x7f0802c5

.field public static final bg_lr_table_edit_bar:I = 0x7f0802c6

.field public static final bg_me_page_account_edu_tips:I = 0x7f0802c7

.field public static final bg_me_page_header_gold:I = 0x7f0802c8

.field public static final bg_more:I = 0x7f0802c9

.field public static final bg_more_document_tag:I = 0x7f0802ca

.field public static final bg_more_document_tag_default:I = 0x7f0802cb

.field public static final bg_more_title_bar:I = 0x7f0802cc

.field public static final bg_msg_numbers:I = 0x7f0802cd

.field public static final bg_multi_capture_add:I = 0x7f0802ce

.field public static final bg_negative_pop_gold_vip_extra:I = 0x7f0802cf

.field public static final bg_negative_pop_premium_vip_extra:I = 0x7f0802d0

.field public static final bg_ocr_image_type_guide:I = 0x7f0802d1

.field public static final bg_ocr_next_page:I = 0x7f0802d2

.field public static final bg_ocr_pre_item:I = 0x7f0802d3

.field public static final bg_ocr_previous_page:I = 0x7f0802d4

.field public static final bg_ocr_try_num_left:I = 0x7f0802d5

.field public static final bg_offline_dialog:I = 0x7f0802d6

.field public static final bg_one_login_btn:I = 0x7f0802d7

.field public static final bg_only_read_camera_function:I = 0x7f0802d8

.field public static final bg_only_read_camera_top:I = 0x7f0802d9

.field public static final bg_only_read_image_scanner_bottom:I = 0x7f0802da

.field public static final bg_only_read_image_scanner_middle:I = 0x7f0802db

.field public static final bg_only_read_img_scancomplete:I = 0x7f0802dc

.field public static final bg_only_read_top:I = 0x7f0802dd

.field public static final bg_orange_bottom_right_round:I = 0x7f0802de

.field public static final bg_orange_bottom_round:I = 0x7f0802df

.field public static final bg_orange_round:I = 0x7f0802e0

.field public static final bg_orange_round_corner_4:I = 0x7f0802e1

.field public static final bg_pad_board:I = 0x7f0802e2

.field public static final bg_pad_paper:I = 0x7f0802e3

.field public static final bg_page_item_stroke:I = 0x7f0802e4

.field public static final bg_page_select:I = 0x7f0802e5

.field public static final bg_page_stroke:I = 0x7f0802e6

.field public static final bg_page_title:I = 0x7f0802e7

.field public static final bg_paging_seal_paging:I = 0x7f0802e8

.field public static final bg_paper_property_select_dialog:I = 0x7f0802e9

.field public static final bg_password_identify_result_dialog:I = 0x7f0802ea

.field public static final bg_pb_capture_scale:I = 0x7f0802eb

.field public static final bg_pink_ff7255_round_corner_100:I = 0x7f0802ec

.field public static final bg_pink_ff7255_round_corner_5:I = 0x7f0802ed

.field public static final bg_pink_ff7255_round_corner_9:I = 0x7f0802ee

.field public static final bg_print_cs_recommend:I = 0x7f0802ef

.field public static final bg_print_feedback:I = 0x7f0802f0

.field public static final bg_printer_property:I = 0x7f0802f1

.field public static final bg_provider_share_dir:I = 0x7f0802f2

.field public static final bg_qr_code_out:I = 0x7f0802f3

.field public static final bg_question_dialog:I = 0x7f0802f4

.field public static final bg_question_dialog_f7f7f7:I = 0x7f0802f5

.field public static final bg_question_other:I = 0x7f0802f6

.field public static final bg_question_tag:I = 0x7f0802f7

.field public static final bg_question_tag_select:I = 0x7f0802f8

.field public static final bg_radio_selector:I = 0x7f0802f9

.field public static final bg_radio_tab_checked:I = 0x7f0802fa

.field public static final bg_radius_commen_dark_btn:I = 0x7f0802fb

.field public static final bg_recommend_tip:I = 0x7f0802fc

.field public static final bg_rectangle_f7f7f7_dash_dcdcdc_4dp:I = 0x7f0802fd

.field public static final bg_red_number:I = 0x7f0802fe

.field public static final bg_red_number_ff7255:I = 0x7f0802ff

.field public static final bg_register_page_bottom:I = 0x7f080300

.field public static final bg_rejoin:I = 0x7f080301

.field public static final bg_rejoin_eight_btn:I = 0x7f080302

.field public static final bg_rejoin_eight_tip:I = 0x7f080303

.field public static final bg_rejoin_eight_tip_bottom:I = 0x7f080304

.field public static final bg_rejoin_eight_vips:I = 0x7f080305

.field public static final bg_rejoin_item_top:I = 0x7f080306

.field public static final bg_rejoin_item_top_1:I = 0x7f080307

.field public static final bg_rejoin_recycler:I = 0x7f080308

.field public static final bg_rejoin_vip:I = 0x7f080309

.field public static final bg_remove_watermark:I = 0x7f08030a

.field public static final bg_review_state:I = 0x7f08030b

.field public static final bg_reward_result_1:I = 0x7f08030c

.field public static final bg_reward_result_2:I = 0x7f08030d

.field public static final bg_reward_video_btn:I = 0x7f08030e

.field public static final bg_reward_video_btn_no_select:I = 0x7f08030f

.field public static final bg_reward_video_btn_selected:I = 0x7f080310

.field public static final bg_reward_video_normal:I = 0x7f080311

.field public static final bg_round_corner_right_bottom:I = 0x7f080312

.field public static final bg_round_orange:I = 0x7f080313

.field public static final bg_scan_done_chat:I = 0x7f080314

.field public static final bg_search_all_vip_function:I = 0x7f080315

.field public static final bg_send_to_pc_content:I = 0x7f080316

.field public static final bg_send_to_pc_header:I = 0x7f080317

.field public static final bg_shape_cs_color_bg_0_top_8dp:I = 0x7f080318

.field public static final bg_shape_rect_cs_color_bg_0_14dp:I = 0x7f080319

.field public static final bg_shape_rect_cs_color_bg_0_4dp:I = 0x7f08031a

.field public static final bg_shape_rect_cs_color_bg_0_8dp:I = 0x7f08031b

.field public static final bg_share_doc_preview_pages:I = 0x7f08031c

.field public static final bg_share_image_water_mart_title_line:I = 0x7f08031d

.field public static final bg_share_link_pdf_card_item:I = 0x7f08031e

.field public static final bg_share_panel_tab_left:I = 0x7f08031f

.field public static final bg_share_top_image:I = 0x7f080320

.field public static final bg_share_water_mark:I = 0x7f080321

.field public static final bg_sheet_button_selected:I = 0x7f080322

.field public static final bg_sheet_button_unselected:I = 0x7f080323

.field public static final bg_shutter_btn:I = 0x7f080324

.field public static final bg_shutter_refactor:I = 0x7f080325

.field public static final bg_sign_tab_shadow:I = 0x7f080326

.field public static final bg_signature_bottom_mask:I = 0x7f080327

.field public static final bg_signature_editor_layout:I = 0x7f080328

.field public static final bg_signature_paging_seal_shadow:I = 0x7f080329

.field public static final bg_signature_tab_guide:I = 0x7f08032a

.field public static final bg_signature_tab_mask:I = 0x7f08032b

.field public static final bg_signature_thumb_item:I = 0x7f08032c

.field public static final bg_skip_round_4:I = 0x7f08032d

.field public static final bg_smart_erase_seek:I = 0x7f08032e

.field public static final bg_snack_bar:I = 0x7f08032f

.field public static final bg_solid_ffffff_f7f7f7:I = 0x7f080330

.field public static final bg_solidcc000_stroke99dcdcdc_corner4:I = 0x7f080331

.field public static final bg_start_page:I = 0x7f080332

.field public static final bg_stroke_4_19bc51:I = 0x7f080333

.field public static final bg_stroke_4_548bf5:I = 0x7f080334

.field public static final bg_stroke_4dffffff_corner_4dp:I = 0x7f080335

.field public static final bg_system_message_state:I = 0x7f080336

.field public static final bg_tabhost_green:I = 0x7f080337

.field public static final bg_tabhost_white:I = 0x7f080338

.field public static final bg_tagedit:I = 0x7f080339

.field public static final bg_tagedit_new:I = 0x7f08033a

.field public static final bg_tagtextview_gray:I = 0x7f08033b

.field public static final bg_tagtextview_prepare_del:I = 0x7f08033c

.field public static final bg_test_country_tips:I = 0x7f08033d

.field public static final bg_texture2:I = 0x7f08033e

.field public static final bg_thumb_gallery:I = 0x7f08033f

.field public static final bg_timer_alert_dialog:I = 0x7f080340

.field public static final bg_timing_pop_up_window:I = 0x7f080341

.field public static final bg_title_image_text_button:I = 0x7f080342

.field public static final bg_top_areq8:I = 0x7f080343

.field public static final bg_top_round:I = 0x7f080344

.field public static final bg_top_round_12:I = 0x7f080345

.field public static final bg_top_round_12_color_f1f1f1:I = 0x7f080346

.field public static final bg_top_round_12_color_f7f7f7:I = 0x7f080347

.field public static final bg_top_round_12_cs_color_bg_1:I = 0x7f080348

.field public static final bg_top_round_dfedf9_ffffff:I = 0x7f080349

.field public static final bg_topic_paper_how_to_guide:I = 0x7f08034a

.field public static final bg_translate_limit_free:I = 0x7f08034b

.field public static final bg_transparent_corner4_stroke_border_1dp_dcdcdc:I = 0x7f08034c

.field public static final bg_transparent_corner4_strokecolor0:I = 0x7f08034d

.field public static final bg_transparent_round_corner_12:I = 0x7f08034e

.field public static final bg_upgrade_dialog:I = 0x7f08034f

.field public static final bg_upgrade_dialog_up_radiu_6:I = 0x7f080350

.field public static final bg_verify_code_blank:I = 0x7f080351

.field public static final bg_verify_code_next:I = 0x7f080352

.field public static final bg_vip_account_tips_top_white:I = 0x7f080353

.field public static final bg_vip_card_s_124_74px:I = 0x7f080354

.field public static final bg_vip_month_purchase_success_dialog:I = 0x7f080355

.field public static final bg_vipacount_tips:I = 0x7f080356

.field public static final bg_visit_normal:I = 0x7f080357

.field public static final bg_visit_press:I = 0x7f080358

.field public static final bg_watermark_item_background:I = 0x7f080359

.field public static final bg_watermark_item_background_unselected:I = 0x7f08035a

.field public static final bg_wheel_select_eeeef0_4dp:I = 0x7f08035b

.field public static final bg_white01_top_corner_4:I = 0x7f08035c

.field public static final bg_white01_top_corner_8:I = 0x7f08035d

.field public static final bg_white_border1_corner24:I = 0x7f08035e

.field public static final bg_white_border2_corner4:I = 0x7f08035f

.field public static final bg_white_bottom_corner_4:I = 0x7f080360

.field public static final bg_white_bottom_corner_6:I = 0x7f080361

.field public static final bg_white_cancel_btn:I = 0x7f080362

.field public static final bg_white_circle_stoke_1dp:I = 0x7f080363

.field public static final bg_white_corner21:I = 0x7f080364

.field public static final bg_white_corner_29:I = 0x7f080365

.field public static final bg_white_round_corner_2dp:I = 0x7f080366

.field public static final bg_white_round_corner_5:I = 0x7f080367

.field public static final bg_white_round_corner_6:I = 0x7f080368

.field public static final bg_white_round_corner_normal:I = 0x7f080369

.field public static final bg_white_stroke:I = 0x7f08036a

.field public static final bg_white_stroke_f1f1f1_corner_4:I = 0x7f08036b

.field public static final bg_white_top_corner_12:I = 0x7f08036c

.field public static final bg_white_top_corner_16:I = 0x7f08036d

.field public static final bg_white_top_corner_4:I = 0x7f08036e

.field public static final bg_white_top_corner_6:I = 0x7f08036f

.field public static final bg_white_top_corner_8:I = 0x7f080370

.field public static final bg_white_top_corner_8_bg_1:I = 0x7f080371

.field public static final bg_white_top_corner_8_no_drak:I = 0x7f080372

.field public static final bg_word_content_op:I = 0x7f080373

.field public static final bind_send_ver_code_btn_bg:I = 0x7f080374

.field public static final blue_btn_bg:I = 0x7f080375

.field public static final blue_tag:I = 0x7f080376

.field public static final bm_img_preview_horizonal_anim:I = 0x7f080377

.field public static final brightness_high:I = 0x7f080378

.field public static final brvah_sample_footer_loading:I = 0x7f080379

.field public static final brvah_sample_footer_loading_progress:I = 0x7f08037a

.field public static final btn_alert_dialog:I = 0x7f08037b

.field public static final btn_bg_a25624:I = 0x7f08037c

.field public static final btn_bg_white:I = 0x7f08037d

.field public static final btn_check_alert_dialog:I = 0x7f08037e

.field public static final btn_checkbox_checked_mtrl:I = 0x7f08037f

.field public static final btn_checkbox_checked_to_unchecked_mtrl_animation:I = 0x7f080380

.field public static final btn_checkbox_unchecked_mtrl:I = 0x7f080381

.field public static final btn_checkbox_unchecked_to_checked_mtrl_animation:I = 0x7f080382

.field public static final btn_gray:I = 0x7f080383

.field public static final btn_gray_stroke:I = 0x7f080384

.field public static final btn_gray_stroke_r22:I = 0x7f080385

.field public static final btn_guide_skip_bg:I = 0x7f080386

.field public static final btn_guide_skip_bg_corner:I = 0x7f080387

.field public static final btn_ic_facebook:I = 0x7f080388

.field public static final btn_ic_flashalways:I = 0x7f080389

.field public static final btn_ic_flashauto:I = 0x7f08038a

.field public static final btn_ic_flashoff:I = 0x7f08038b

.field public static final btn_ic_flashon:I = 0x7f08038c

.field public static final btn_ic_share_item_gallery:I = 0x7f08038d

.field public static final btn_ic_share_item_sns:I = 0x7f08038e

.field public static final btn_ic_twitter:I = 0x7f08038f

.field public static final btn_ic_twitter_disable:I = 0x7f080390

.field public static final btn_ic_wechat:I = 0x7f080391

.field public static final btn_ic_wechat_moments:I = 0x7f080392

.field public static final btn_ic_weibo:I = 0x7f080393

.field public static final btn_image_share:I = 0x7f080394

.field public static final btn_radio_18:I = 0x7f080395

.field public static final btn_radio_alert_dialog:I = 0x7f080396

.field public static final btn_radio_capture_dialog:I = 0x7f080397

.field public static final btn_radio_capture_off:I = 0x7f080398

.field public static final btn_radio_off_mtrl:I = 0x7f080399

.field public static final btn_radio_off_to_on_mtrl_animation:I = 0x7f08039a

.field public static final btn_radio_on_mtrl:I = 0x7f08039b

.field public static final btn_radio_on_to_off_mtrl_animation:I = 0x7f08039c

.field public static final btn_share_twitter:I = 0x7f08039d

.field public static final bubble_bg_d59b45_edcc8b:I = 0x7f08039e

.field public static final bubble_bg_d59b45_edcc8b_corner_4:I = 0x7f08039f

.field public static final bubble_bg_fdeecc_ffdca7:I = 0x7f0803a0

.field public static final button_19bcaa_corner_8:I = 0x7f0803a1

.field public static final button_home_banner1_button:I = 0x7f0803a2

.field public static final button_home_banner1_timelimit:I = 0x7f0803a3

.field public static final button_sale_guide:I = 0x7f0803a4

.field public static final buy_vip_sale:I = 0x7f0803a5

.field public static final cam_scanner_bg_gold_invalid_2x:I = 0x7f0803a6

.field public static final capture_guide2:I = 0x7f0803a7

.field public static final capture_guide2_zh:I = 0x7f0803a8

.field public static final cb_handwrite_enable_color:I = 0x7f0803a9

.field public static final cb_security_mark_black:I = 0x7f0803aa

.field public static final cb_security_mark_blue:I = 0x7f0803ab

.field public static final cb_security_mark_green:I = 0x7f0803ac

.field public static final cb_security_mark_orange:I = 0x7f0803ad

.field public static final cb_security_mark_red:I = 0x7f0803ae

.field public static final cb_security_mark_white:I = 0x7f0803af

.field public static final cd_btn_check_off:I = 0x7f0803b0

.field public static final cd_btn_check_on:I = 0x7f0803b1

.field public static final cd_btn_radio_off:I = 0x7f0803b2

.field public static final cd_btn_radio_on:I = 0x7f0803b3

.field public static final cd_select_selected:I = 0x7f0803b4

.field public static final cd_select_unselected:I = 0x7f0803b5

.field public static final certificate_mode:I = 0x7f0803b6

.field public static final checkbox_green_white:I = 0x7f0803b7

.field public static final close:I = 0x7f0803b8

.field public static final close_bg_alpha:I = 0x7f0803b9

.field public static final close_discount_pop:I = 0x7f0803ba

.field public static final close_sdk_btn:I = 0x7f0803bb

.field public static final cloud_ocr:I = 0x7f0803bc

.field public static final cn_1:I = 0x7f0803bd

.field public static final cn_2:I = 0x7f0803be

.field public static final cn_3:I = 0x7f0803bf

.field public static final cn_4_black_white:I = 0x7f0803c0

.field public static final com_facebook_auth_dialog_background:I = 0x7f0803c1

.field public static final com_facebook_auth_dialog_cancel_background:I = 0x7f0803c2

.field public static final com_facebook_auth_dialog_header_background:I = 0x7f0803c3

.field public static final com_facebook_button_background:I = 0x7f0803c4

.field public static final com_facebook_button_icon:I = 0x7f0803c5

.field public static final com_facebook_button_like_background:I = 0x7f0803c6

.field public static final com_facebook_button_like_icon_selected:I = 0x7f0803c7

.field public static final com_facebook_button_send_background:I = 0x7f0803c8

.field public static final com_facebook_button_send_icon_blue:I = 0x7f0803c9

.field public static final com_facebook_button_send_icon_white:I = 0x7f0803ca

.field public static final com_facebook_close:I = 0x7f0803cb

.field public static final com_facebook_favicon_blue:I = 0x7f0803cc

.field public static final com_facebook_profile_picture_blank_portrait:I = 0x7f0803cd

.field public static final com_facebook_profile_picture_blank_square:I = 0x7f0803ce

.field public static final com_facebook_send_button_icon:I = 0x7f0803cf

.field public static final com_facebook_tooltip_black_background:I = 0x7f0803d0

.field public static final com_facebook_tooltip_black_bottomnub:I = 0x7f0803d1

.field public static final com_facebook_tooltip_black_topnub:I = 0x7f0803d2

.field public static final com_facebook_tooltip_black_xout:I = 0x7f0803d3

.field public static final com_facebook_tooltip_blue_background:I = 0x7f0803d4

.field public static final com_facebook_tooltip_blue_bottomnub:I = 0x7f0803d5

.field public static final com_facebook_tooltip_blue_topnub:I = 0x7f0803d6

.field public static final com_facebook_tooltip_blue_xout:I = 0x7f0803d7

.field public static final comm_bg_dialog_loading:I = 0x7f0803d8

.field public static final common_full_open_on_phone:I = 0x7f0803d9

.field public static final common_google_signin_btn_icon_dark:I = 0x7f0803da

.field public static final common_google_signin_btn_icon_dark_focused:I = 0x7f0803db

.field public static final common_google_signin_btn_icon_dark_normal:I = 0x7f0803dc

.field public static final common_google_signin_btn_icon_dark_normal_background:I = 0x7f0803dd

.field public static final common_google_signin_btn_icon_disabled:I = 0x7f0803de

.field public static final common_google_signin_btn_icon_light:I = 0x7f0803df

.field public static final common_google_signin_btn_icon_light_focused:I = 0x7f0803e0

.field public static final common_google_signin_btn_icon_light_normal:I = 0x7f0803e1

.field public static final common_google_signin_btn_icon_light_normal_background:I = 0x7f0803e2

.field public static final common_google_signin_btn_text_dark:I = 0x7f0803e3

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0803e4

.field public static final common_google_signin_btn_text_dark_normal:I = 0x7f0803e5

.field public static final common_google_signin_btn_text_dark_normal_background:I = 0x7f0803e6

.field public static final common_google_signin_btn_text_disabled:I = 0x7f0803e7

.field public static final common_google_signin_btn_text_light:I = 0x7f0803e8

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0803e9

.field public static final common_google_signin_btn_text_light_normal:I = 0x7f0803ea

.field public static final common_google_signin_btn_text_light_normal_background:I = 0x7f0803eb

.field public static final composite_picture:I = 0x7f0803ec

.field public static final coom_bg_dialog_loading:I = 0x7f0803ed

.field public static final corner_1_19bcaa:I = 0x7f0803ee

.field public static final corner_27_19bcaa:I = 0x7f0803ef

.field public static final corner_32_36e2cf_19bcaa:I = 0x7f0803f0

.field public static final corner_4_19bcaa:I = 0x7f0803f1

.field public static final corner_4_ff7255:I = 0x7f0803f2

.field public static final corner_8_f7f7f7:I = 0x7f0803f3

.field public static final countdown_number_zero_2x:I = 0x7f0803f4

.field public static final coupon_background:I = 0x7f0803f5

.field public static final cs_color_bg_0_conner_8:I = 0x7f0803f6

.field public static final cs_color_bg_0_conner_bottom_8:I = 0x7f0803f7

.field public static final cs_color_bg_0_conner_top_12:I = 0x7f0803f8

.field public static final cs_color_bg_0_conner_top_8:I = 0x7f0803f9

.field public static final cs_color_bg_2_conner_12:I = 0x7f0803fa

.field public static final cs_color_bg_2_conner_5:I = 0x7f0803fb

.field public static final cs_launcher_300_200:I = 0x7f0803fc

.field public static final cs_launcher_300_200_router:I = 0x7f0803fd

.field public static final cs_loading_icon:I = 0x7f0803fe

.field public static final cs_logo_for_renovation_poster:I = 0x7f0803ff

.field public static final cs_seek_track:I = 0x7f080400

.field public static final cslogo:I = 0x7f080401

.field public static final cursor_color:I = 0x7f080402

.field public static final custom_gallery_tips_bg:I = 0x7f080403

.field public static final dafault_indexBar_background:I = 0x7f080404

.field public static final dash_divider_horizontal:I = 0x7f080405

.field public static final dash_divider_vertical:I = 0x7f080406

.field public static final db_file:I = 0x7f080407

.field public static final deal_loading:I = 0x7f080408

.field public static final default_doc_thumb:I = 0x7f080409

.field public static final default_page_thumb:I = 0x7f08040a

.field public static final default_scroll_handle_bottom:I = 0x7f08040b

.field public static final default_scroll_handle_left:I = 0x7f08040c

.field public static final default_scroll_handle_right:I = 0x7f08040d

.field public static final default_scroll_handle_top:I = 0x7f08040e

.field public static final demo_en_s:I = 0x7f08040f

.field public static final design_fab_background:I = 0x7f080410

.field public static final design_ic_visibility:I = 0x7f080411

.field public static final design_ic_visibility_off:I = 0x7f080412

.field public static final design_password_eye:I = 0x7f080413

.field public static final design_snackbar_background:I = 0x7f080414

.field public static final dialog_water_mark_guide:I = 0x7f080415

.field public static final dialog_water_mark_guide_i_know:I = 0x7f080416

.field public static final dialog_water_mark_guide_white:I = 0x7f080417

.field public static final divider_border0_1dp:I = 0x7f080418

.field public static final divier_gray_dfdfdf_1dp:I = 0x7f080419

.field public static final divier_transparent_12dp:I = 0x7f08041a

.field public static final divier_transparent_16dp:I = 0x7f08041b

.field public static final divier_transparent_20dp:I = 0x7f08041c

.field public static final doc_import_more_44:I = 0x7f08041d

.field public static final doc_import_phone_36px:I = 0x7f08041e

.field public static final doc_import_wechat_44:I = 0x7f08041f

.field public static final doc_item_frame:I = 0x7f080420

.field public static final doc_list_divider:I = 0x7f080421

.field public static final dock_btn:I = 0x7f080422

.field public static final document_capture_guide:I = 0x7f080423

.field public static final doodle_bg_dropper_done:I = 0x7f080424

.field public static final doodle_bg_free:I = 0x7f080425

.field public static final doodle_btn_done:I = 0x7f080426

.field public static final doodle_ic_back:I = 0x7f080427

.field public static final doodle_ic_cancel:I = 0x7f080428

.field public static final doodle_ic_complete:I = 0x7f080429

.field public static final doodle_ic_help:I = 0x7f08042a

.field public static final doodle_ic_redo:I = 0x7f08042b

.field public static final doodle_ic_save:I = 0x7f08042c

.field public static final doodle_ic_text_delete:I = 0x7f08042d

.field public static final doodle_ic_text_move:I = 0x7f08042e

.field public static final doodle_ic_touch_complete:I = 0x7f08042f

.field public static final doodle_ic_undo:I = 0x7f080430

.field public static final doodle_item_background:I = 0x7f080431

.field public static final doodle_item_background_borderless:I = 0x7f080432

.field public static final doodle_mode_indicator:I = 0x7f080433

.field public static final doodle_paint_brush:I = 0x7f080434

.field public static final doodle_pen_size_indicator:I = 0x7f080435

.field public static final doodle_rectangle:I = 0x7f080436

.field public static final doodle_rubber:I = 0x7f080437

.field public static final doodle_take_color:I = 0x7f080438

.field public static final doodle_take_color_bg:I = 0x7f080439

.field public static final dot_background:I = 0x7f08043a

.field public static final dot_gray_20:I = 0x7f08043b

.field public static final dot_purchase:I = 0x7f08043c

.field public static final dot_purchase_round:I = 0x7f08043d

.field public static final dot_purchase_round2:I = 0x7f08043e

.field public static final dot_purchase_round_d59845:I = 0x7f08043f

.field public static final dot_select:I = 0x7f080440

.field public static final dot_select_golden_e8bc72:I = 0x7f080441

.field public static final dot_select_gray_dark:I = 0x7f080442

.field public static final dot_select_red_ff6748:I = 0x7f080443

.field public static final dot_select_white_drop_cnl:I = 0x7f080444

.field public static final dot_share_app_select:I = 0x7f080445

.field public static final dot_share_app_unselect:I = 0x7f080446

.field public static final dot_unselect:I = 0x7f080447

.field public static final dot_unselect_gray_9c9c9c:I = 0x7f080448

.field public static final dot_unselect_gray_light:I = 0x7f080449

.field public static final dot_unselect_white_drop_cnl:I = 0x7f08044a

.field public static final dotted_line:I = 0x7f08044b

.field public static final dotted_line_color_cs_brand:I = 0x7f08044c

.field public static final dotted_line_ffc8b1:I = 0x7f08044d

.field public static final dragpoint:I = 0x7f08044e

.field public static final dragpoint_10:I = 0x7f08044f

.field public static final drawable_blue_btn_normal:I = 0x7f080450

.field public static final drawable_blue_btn_pressed:I = 0x7f080451

.field public static final drawable_blue_progress:I = 0x7f080452

.field public static final drawable_blue_progress_bg_2:I = 0x7f080453

.field public static final drawable_danger_progress:I = 0x7f080454

.field public static final drawable_warning_progress:I = 0x7f080455

.field public static final drawable_warning_progress_bg_2:I = 0x7f080456

.field public static final drawer_menu_item_bg:I = 0x7f080457

.field public static final edittext_cursor_drawable:I = 0x7f080458

.field public static final edittext_cursor_drawable_special:I = 0x7f080459

.field public static final edittext_underline_red:I = 0x7f08045a

.field public static final empty_transition_ac_bg:I = 0x7f08045b

.field public static final enhance_anim_line:I = 0x7f08045c

.field public static final esign_contact_empty:I = 0x7f08045d

.field public static final esign_empty:I = 0x7f08045e

.field public static final esign_line_weight_1:I = 0x7f08045f

.field public static final esign_line_weight_2:I = 0x7f080460

.field public static final flame:I = 0x7f080461

.field public static final focus_marker_fill:I = 0x7f080462

.field public static final focus_marker_outline:I = 0x7f080463

.field public static final frame_19bc9c:I = 0x7f080464

.field public static final galaxy_flush_bg:I = 0x7f080465

.field public static final galaxy_flush_line:I = 0x7f080466

.field public static final gif_intergal_reward:I = 0x7f080467

.field public static final gold_light_effect_2x:I = 0x7f080468

.field public static final google_play:I = 0x7f080469

.field public static final googleg_disabled_color_18:I = 0x7f08046a

.field public static final googleg_standard_color_18:I = 0x7f08046b

.field public static final gp_guide_bg_old_women:I = 0x7f08046c

.field public static final gradient_bg0_to_bg2_top_corner_12:I = 0x7f08046d

.field public static final gray_common_btn_bg:I = 0x7f08046e

.field public static final green_common_btn_bg:I = 0x7f08046f

.field public static final green_common_btn_bg_disable:I = 0x7f080470

.field public static final green_common_btn_new_bg:I = 0x7f080471

.field public static final greeting_card_12_jpg:I = 0x7f080472

.field public static final greeting_card_12_ori:I = 0x7f080473

.field public static final greeting_card_12_thumbnail:I = 0x7f080474

.field public static final greeting_card_12_transparent:I = 0x7f080475

.field public static final guide_3d_01:I = 0x7f080476

.field public static final guide_3d_01_2:I = 0x7f080477

.field public static final guide_3d_02:I = 0x7f080478

.field public static final guide_3d_02_2:I = 0x7f080479

.field public static final guide_3d_03:I = 0x7f08047a

.field public static final guide_3d_04_2:I = 0x7f08047b

.field public static final guide_3d_05:I = 0x7f08047c

.field public static final guide_3d_05_2:I = 0x7f08047d

.field public static final guide_3d_06:I = 0x7f08047e

.field public static final guide_3d_06_2:I = 0x7f08047f

.field public static final guide_bottom_shape:I = 0x7f080480

.field public static final guide_bottom_shape_black:I = 0x7f080481

.field public static final guide_bottom_shape_cn:I = 0x7f080482

.field public static final guide_bottom_shape_cn_ff7255:I = 0x7f080483

.field public static final guide_bottom_shape_gp:I = 0x7f080484

.field public static final guide_bottom_shape_gp_new:I = 0x7f080485

.field public static final guide_cn_clear_sharp:I = 0x7f080486

.field public static final guide_cn_efficient_convenient:I = 0x7f080487

.field public static final guide_cn_high_fidelity:I = 0x7f080488

.field public static final guide_cn_login:I = 0x7f080489

.field public static final guide_cn_mobile_office:I = 0x7f08048a

.field public static final guide_cn_ocr:I = 0x7f08048b

.field public static final guide_cn_tidy_storage:I = 0x7f08048c

.field public static final guide_down_black_10:I = 0x7f08048d

.field public static final guide_pic_1:I = 0x7f08048e

.field public static final guide_pic_2:I = 0x7f08048f

.field public static final guide_pic_3:I = 0x7f080490

.field public static final guide_pic_4:I = 0x7f080491

.field public static final guide_pic_5:I = 0x7f080492

.field public static final guide_up_black_10:I = 0x7f080493

.field public static final holo_checkbox_bg:I = 0x7f080494

.field public static final holo_common_btn_bg:I = 0x7f080495

.field public static final holo_common_btn_bg_disable:I = 0x7f080496

.field public static final holo_common_transition:I = 0x7f080497

.field public static final holo_pdf_import_btn_bg:I = 0x7f080498

.field public static final holo_pdf_import_btn_bg_new:I = 0x7f080499

.field public static final home_arrow_24px:I = 0x7f08049a

.field public static final home_nav_plugin:I = 0x7f08049b

.field public static final home_nav_search:I = 0x7f08049c

.field public static final home_select_selected:I = 0x7f08049d

.field public static final home_select_unselected:I = 0x7f08049e

.field public static final home_toolbar_delete:I = 0x7f08049f

.field public static final home_toolbar_lock:I = 0x7f0804a0

.field public static final home_toolbar_photo:I = 0x7f0804a1

.field public static final home_toolbar_share:I = 0x7f0804a2

.field public static final home_toolbar_spinner:I = 0x7f0804a3

.field public static final home_toolbar_tag:I = 0x7f0804a4

.field public static final ic_10_36px:I = 0x7f0804a5

.field public static final ic_11_36px:I = 0x7f0804a6

.field public static final ic_12_36px:I = 0x7f0804a7

.field public static final ic_1_2_60px:I = 0x7f0804a8

.field public static final ic_1_2_60px_new:I = 0x7f0804a9

.field public static final ic_1_2_60px_new_un:I = 0x7f0804aa

.field public static final ic_1_36px:I = 0x7f0804ab

.field public static final ic_2_1_60px:I = 0x7f0804ac

.field public static final ic_2_1_60px_new:I = 0x7f0804ad

.field public static final ic_2_1_60px_new_un:I = 0x7f0804ae

.field public static final ic_2_2_60px:I = 0x7f0804af

.field public static final ic_2_2_60px_new:I = 0x7f0804b0

.field public static final ic_2_2_60px_new_un:I = 0x7f0804b1

.field public static final ic_2_36px:I = 0x7f0804b2

.field public static final ic_2_3_60px:I = 0x7f0804b3

.field public static final ic_2_3_60px_new:I = 0x7f0804b4

.field public static final ic_2_3_60px_new_un:I = 0x7f0804b5

.field public static final ic_3_1_60px:I = 0x7f0804b6

.field public static final ic_3_1_60px_new:I = 0x7f0804b7

.field public static final ic_3_1_60px_new_un:I = 0x7f0804b8

.field public static final ic_3_2_60px:I = 0x7f0804b9

.field public static final ic_3_2_60px_new:I = 0x7f0804ba

.field public static final ic_3_2_60px_new_un:I = 0x7f0804bb

.field public static final ic_3_36px:I = 0x7f0804bc

.field public static final ic_3_3_60px:I = 0x7f0804bd

.field public static final ic_3_3_60px_new:I = 0x7f0804be

.field public static final ic_3_3_60px_new_un:I = 0x7f0804bf

.field public static final ic_4_36px:I = 0x7f0804c0

.field public static final ic_5_36px:I = 0x7f0804c1

.field public static final ic_6_36px:I = 0x7f0804c2

.field public static final ic_7_36px:I = 0x7f0804c3

.field public static final ic_8888:I = 0x7f0804c4

.field public static final ic_8888_refactor:I = 0x7f0804c5

.field public static final ic_8_1_60px:I = 0x7f0804c6

.field public static final ic_8_1_60px_new:I = 0x7f0804c7

.field public static final ic_8_1_60px_new_un:I = 0x7f0804c8

.field public static final ic_8_36px:I = 0x7f0804c9

.field public static final ic_9_36px:I = 0x7f0804ca

.field public static final ic_a3:I = 0x7f0804cb

.field public static final ic_a4:I = 0x7f0804cc

.field public static final ic_a5:I = 0x7f0804cd

.field public static final ic_a_b:I = 0x7f0804ce

.field public static final ic_access_rights_40px:I = 0x7f0804cf

.field public static final ic_account_edu_tips:I = 0x7f0804d0

.field public static final ic_account_issue:I = 0x7f0804d1

.field public static final ic_account_line_24px:I = 0x7f0804d2

.field public static final ic_activated_close:I = 0x7f0804d3

.field public static final ic_ad_arrow_24px:I = 0x7f0804d4

.field public static final ic_ad_close:I = 0x7f0804d5

.field public static final ic_add_autograph_page_24px:I = 0x7f0804d6

.field public static final ic_add_black_12:I = 0x7f0804d7

.field public static final ic_add_count:I = 0x7f0804d8

.field public static final ic_add_doc_24px:I = 0x7f0804d9

.field public static final ic_add_font:I = 0x7f0804da

.field public static final ic_add_id_card:I = 0x7f0804db

.field public static final ic_add_img:I = 0x7f0804dc

.field public static final ic_add_line_24px:I = 0x7f0804dd

.field public static final ic_add_one_page:I = 0x7f0804de

.field public static final ic_add_signature:I = 0x7f0804df

.field public static final ic_add_signature_new:I = 0x7f0804e0

.field public static final ic_add_signer:I = 0x7f0804e1

.field public static final ic_add_text_24px:I = 0x7f0804e2

.field public static final ic_add_widget_24px:I = 0x7f0804e3

.field public static final ic_addfile_line_24px:I = 0x7f0804e4

.field public static final ic_addhome_line_24px:I = 0x7f0804e5

.field public static final ic_adjust_24px:I = 0x7f0804e6

.field public static final ic_administrator_pitchon_24px:I = 0x7f0804e7

.field public static final ic_administrator_rule_24px:I = 0x7f0804e8

.field public static final ic_after_scan_premium_doc:I = 0x7f0804e9

.field public static final ic_alarm_26:I = 0x7f0804ea

.field public static final ic_album:I = 0x7f0804eb

.field public static final ic_album_capture:I = 0x7f0804ec

.field public static final ic_alipay_logo:I = 0x7f0804ed

.field public static final ic_animation_arrow:I = 0x7f0804ee

.field public static final ic_annot_del:I = 0x7f0804ef

.field public static final ic_annot_style:I = 0x7f0804f0

.field public static final ic_annotation_line_24px:I = 0x7f0804f1

.field public static final ic_annual_premium_arrow_right:I = 0x7f0804f2

.field public static final ic_annual_premium_vip:I = 0x7f0804f3

.field public static final ic_annual_premium_vip_update:I = 0x7f0804f4

.field public static final ic_anti_counterfert_line_24px:I = 0x7f0804f5

.field public static final ic_anti_theft:I = 0x7f0804f6

.field public static final ic_app_file_green_24_24:I = 0x7f0804f7

.field public static final ic_app_information_random_0:I = 0x7f0804f8

.field public static final ic_app_information_random_1:I = 0x7f0804f9

.field public static final ic_app_information_random_2:I = 0x7f0804fa

.field public static final ic_app_information_random_3:I = 0x7f0804fb

.field public static final ic_app_information_random_4:I = 0x7f0804fc

.field public static final ic_app_information_random_5:I = 0x7f0804fd

.field public static final ic_apply_old_24:I = 0x7f0804fe

.field public static final ic_apply_to_all_selected:I = 0x7f0804ff

.field public static final ic_apply_to_all_unselected:I = 0x7f080500

.field public static final ic_archive_grid_forder_image:I = 0x7f080501

.field public static final ic_archive_list_forder_image:I = 0x7f080502

.field public static final ic_area_free_bg:I = 0x7f080503

.field public static final ic_area_free_share_arrow:I = 0x7f080504

.field public static final ic_arrange1_line_24px:I = 0x7f080505

.field public static final ic_arrange2_line_24px:I = 0x7f080506

.field public static final ic_arrange3_line_24px:I = 0x7f080507

.field public static final ic_arrow2:I = 0x7f080508

.field public static final ic_arrow_12px:I = 0x7f080509

.field public static final ic_arrow_139d90:I = 0x7f08050a

.field public static final ic_arrow_16:I = 0x7f08050b

.field public static final ic_arrow_20px:I = 0x7f08050c

.field public static final ic_arrow_back_black_24:I = 0x7f08050d

.field public static final ic_arrow_black_20:I = 0x7f08050e

.field public static final ic_arrow_down:I = 0x7f08050f

.field public static final ic_arrow_down_16:I = 0x7f080510

.field public static final ic_arrow_down_24:I = 0x7f080511

.field public static final ic_arrow_down_gray_16x16:I = 0x7f080512

.field public static final ic_arrow_esign_10_10:I = 0x7f080513

.field public static final ic_arrow_for_top:I = 0x7f080514

.field public static final ic_arrow_icp_12px:I = 0x7f080515

.field public static final ic_arrow_left:I = 0x7f080516

.field public static final ic_arrow_left_24px:I = 0x7f080517

.field public static final ic_arrow_left_round:I = 0x7f080518

.field public static final ic_arrow_more:I = 0x7f080519

.field public static final ic_arrow_occupation:I = 0x7f08051a

.field public static final ic_arrow_right:I = 0x7f08051b

.field public static final ic_arrow_right_10_10:I = 0x7f08051c

.field public static final ic_arrow_right_12_12:I = 0x7f08051d

.field public static final ic_arrow_right_20_20:I = 0x7f08051e

.field public static final ic_arrow_right_24_24:I = 0x7f08051f

.field public static final ic_arrow_right_30_30:I = 0x7f080520

.field public static final ic_arrow_right_5_10:I = 0x7f080521

.field public static final ic_arrow_right_6_11:I = 0x7f080522

.field public static final ic_arrow_right_gray_3_6:I = 0x7f080523

.field public static final ic_arrow_right_round:I = 0x7f080524

.field public static final ic_arrow_right_white_3_6:I = 0x7f080525

.field public static final ic_arrow_state_10:I = 0x7f080526

.field public static final ic_arrow_top:I = 0x7f080527

.field public static final ic_arrow_up:I = 0x7f080528

.field public static final ic_arrow_white_18_14:I = 0x7f080529

.field public static final ic_arrowhead_left:I = 0x7f08052a

.field public static final ic_article_screenshot:I = 0x7f08052b

.field public static final ic_autograp_album_20px:I = 0x7f08052c

.field public static final ic_autograp_album_24px:I = 0x7f08052d

.field public static final ic_autograph_camera_20px:I = 0x7f08052e

.field public static final ic_autograph_camera_24px:I = 0x7f08052f

.field public static final ic_b4:I = 0x7f080530

.field public static final ic_b5:I = 0x7f080531

.field public static final ic_b_a:I = 0x7f080532

.field public static final ic_backup_bg_in_import_page:I = 0x7f080533

.field public static final ic_backup_cloud:I = 0x7f080534

.field public static final ic_backup_instructions:I = 0x7f080535

.field public static final ic_backup_intro1:I = 0x7f080536

.field public static final ic_backup_intro2:I = 0x7f080537

.field public static final ic_backup_intro3:I = 0x7f080538

.field public static final ic_backup_intro4:I = 0x7f080539

.field public static final ic_backup_main_head:I = 0x7f08053a

.field public static final ic_backup_mobile_in_import_page:I = 0x7f08053b

.field public static final ic_backup_mobile_in_import_page_gp:I = 0x7f08053c

.field public static final ic_backup_phone:I = 0x7f08053d

.field public static final ic_backup_status_backupping:I = 0x7f08053e

.field public static final ic_backup_status_completed:I = 0x7f08053f

.field public static final ic_backup_status_full_cloud_space:I = 0x7f080540

.field public static final ic_backup_status_no_net:I = 0x7f080541

.field public static final ic_backup_status_searching:I = 0x7f080542

.field public static final ic_backup_ticket:I = 0x7f080543

.field public static final ic_backward:I = 0x7f080544

.field public static final ic_backward_24:I = 0x7f080545

.field public static final ic_bank_capture:I = 0x7f080546

.field public static final ic_bank_card:I = 0x7f080547

.field public static final ic_bank_card_web_finder_click:I = 0x7f080548

.field public static final ic_bank_card_web_finder_line:I = 0x7f080549

.field public static final ic_bank_consult:I = 0x7f08054a

.field public static final ic_bank_delete:I = 0x7f08054b

.field public static final ic_bank_journal_merge:I = 0x7f08054c

.field public static final ic_bank_journal_recognizing_bg:I = 0x7f08054d

.field public static final ic_bank_journal_un_recognize_bg:I = 0x7f08054e

.field public static final ic_bankcard:I = 0x7f08054f

.field public static final ic_bankcard_refactor:I = 0x7f080550

.field public static final ic_bankcard_s:I = 0x7f080551

.field public static final ic_banner_cloud_ocr:I = 0x7f080552

.field public static final ic_banner_topic:I = 0x7f080553

.field public static final ic_bar_code_16_16:I = 0x7f080554

.field public static final ic_barcode_scan:I = 0x7f080555

.field public static final ic_batch:I = 0x7f080556

.field public static final ic_batch_24:I = 0x7f080557

.field public static final ic_batch_selected:I = 0x7f080558

.field public static final ic_batch_unselected:I = 0x7f080559

.field public static final ic_beta:I = 0x7f08055a

.field public static final ic_beta2_5213:I = 0x7f08055b

.field public static final ic_beta_24_12:I = 0x7f08055c

.field public static final ic_beta_with_corner:I = 0x7f08055d

.field public static final ic_bg_backup_main:I = 0x7f08055e

.field public static final ic_bg_gray_document:I = 0x7f08055f

.field public static final ic_bg_hd_pdf:I = 0x7f080560

.field public static final ic_bg_long_img:I = 0x7f080561

.field public static final ic_bg_main_home_gift_box:I = 0x7f080562

.field public static final ic_black_close:I = 0x7f080563

.field public static final ic_black_close_24px:I = 0x7f080564

.field public static final ic_black_close_32px:I = 0x7f080565

.field public static final ic_black_close_44:I = 0x7f080566

.field public static final ic_black_more:I = 0x7f080567

.field public static final ic_black_open:I = 0x7f080568

.field public static final ic_blank_ic_card_folder:I = 0x7f080569

.field public static final ic_blank_page:I = 0x7f08056a

.field public static final ic_blank_pdf:I = 0x7f08056b

.field public static final ic_bluetooth:I = 0x7f08056c

.field public static final ic_boldline_line_24px:I = 0x7f08056d

.field public static final ic_booklet:I = 0x7f08056e

.field public static final ic_box_selected:I = 0x7f08056f

.field public static final ic_box_unselected:I = 0x7f080570

.field public static final ic_brush:I = 0x7f080571

.field public static final ic_brush_24px:I = 0x7f080572

.field public static final ic_btn_collaborate_resend:I = 0x7f080573

.field public static final ic_btn_collaborate_resend_normal:I = 0x7f080574

.field public static final ic_btn_collaborate_resend_pressed:I = 0x7f080575

.field public static final ic_btn_radio_off_18:I = 0x7f080576

.field public static final ic_btn_radio_on_18:I = 0x7f080577

.field public static final ic_btn_vip:I = 0x7f080578

.field public static final ic_bubble_close:I = 0x7f080579

.field public static final ic_bubble_logo_cspdf:I = 0x7f08057a

.field public static final ic_bubble_owl_close_ocr:I = 0x7f08057b

.field public static final ic_bubble_tips_16px:I = 0x7f08057c

.field public static final ic_bubble_tips_16px_dark:I = 0x7f08057d

.field public static final ic_buy_20px:I = 0x7f08057e

.field public static final ic_buy_arrow_red_left:I = 0x7f08057f

.field public static final ic_buy_arrow_red_right:I = 0x7f080580

.field public static final ic_cache_finish:I = 0x7f080581

.field public static final ic_cam_exam:I = 0x7f080582

.field public static final ic_cam_exam_calculate:I = 0x7f080583

.field public static final ic_cam_exam_checked:I = 0x7f080584

.field public static final ic_cam_exam_diversion_ope:I = 0x7f080585

.field public static final ic_cam_exam_download_app:I = 0x7f080586

.field public static final ic_cam_exam_download_right:I = 0x7f080587

.field public static final ic_cam_exam_free:I = 0x7f080588

.field public static final ic_cam_exam_guide_toolbox_close:I = 0x7f080589

.field public static final ic_cam_exam_learn_new:I = 0x7f08058a

.field public static final ic_cam_exam_learning_tool:I = 0x7f08058b

.field public static final ic_cam_exam_more_tools:I = 0x7f08058c

.field public static final ic_cam_exam_one_three:I = 0x7f08058d

.field public static final ic_cam_exam_pages:I = 0x7f08058e

.field public static final ic_cam_exam_popup_close_32px:I = 0x7f08058f

.field public static final ic_cam_exam_unchecked:I = 0x7f080590

.field public static final ic_cam_exam_work_check:I = 0x7f080591

.field public static final ic_camcard_ad:I = 0x7f080592

.field public static final ic_camera:I = 0x7f080593

.field public static final ic_camera_20px:I = 0x7f080594

.field public static final ic_camera_24px:I = 0x7f080595

.field public static final ic_camera_40px:I = 0x7f080596

.field public static final ic_camera_auto_24px:I = 0x7f080597

.field public static final ic_camera_auto_off_24px:I = 0x7f080598

.field public static final ic_camera_back:I = 0x7f080599

.field public static final ic_camera_filter:I = 0x7f08059a

.field public static final ic_camera_filter_green:I = 0x7f08059b

.field public static final ic_camera_hd:I = 0x7f08059c

.field public static final ic_camera_line_24px:I = 0x7f08059d

.field public static final ic_camera_more:I = 0x7f08059e

.field public static final ic_camera_more_green:I = 0x7f08059f

.field public static final ic_camera_more_qc_code:I = 0x7f0805a0

.field public static final ic_camera_more_test_evidence:I = 0x7f0805a1

.field public static final ic_camera_more_test_greeting_card:I = 0x7f0805a2

.field public static final ic_camera_multi_add:I = 0x7f0805a3

.field public static final ic_camera_next:I = 0x7f0805a4

.field public static final ic_cancel:I = 0x7f0805a5

.field public static final ic_cancel_16_16:I = 0x7f0805a6

.field public static final ic_cancell_all_selected:I = 0x7f0805a7

.field public static final ic_cancellation:I = 0x7f0805a8

.field public static final ic_capture_add:I = 0x7f0805a9

.field public static final ic_capture_bank_card_journal:I = 0x7f0805aa

.field public static final ic_capture_bank_card_journal_guide_banner:I = 0x7f0805ab

.field public static final ic_capture_bank_card_journal_tips:I = 0x7f0805ac

.field public static final ic_capture_book:I = 0x7f0805ad

.field public static final ic_capture_book_tips:I = 0x7f0805ae

.field public static final ic_capture_capure:I = 0x7f0805af

.field public static final ic_capture_certificate:I = 0x7f0805b0

.field public static final ic_capture_certificate_photo:I = 0x7f0805b1

.field public static final ic_capture_certificate_photo_tips:I = 0x7f0805b2

.field public static final ic_capture_certificate_tips:I = 0x7f0805b3

.field public static final ic_capture_child_mode_excel:I = 0x7f0805b4

.field public static final ic_capture_child_mode_ocr:I = 0x7f0805b5

.field public static final ic_capture_child_mode_word:I = 0x7f0805b6

.field public static final ic_capture_confirm_back:I = 0x7f0805b7

.field public static final ic_capture_confirm_cancel:I = 0x7f0805b8

.field public static final ic_capture_count:I = 0x7f0805b9

.field public static final ic_capture_count_tips:I = 0x7f0805ba

.field public static final ic_capture_crop:I = 0x7f0805bb

.field public static final ic_capture_e_evidence:I = 0x7f0805bc

.field public static final ic_capture_e_evidence_tips:I = 0x7f0805bd

.field public static final ic_capture_excel:I = 0x7f0805be

.field public static final ic_capture_excel_tips:I = 0x7f0805bf

.field public static final ic_capture_greet_card:I = 0x7f0805c0

.field public static final ic_capture_image_restore:I = 0x7f0805c1

.field public static final ic_capture_image_restore_tips:I = 0x7f0805c2

.field public static final ic_capture_import:I = 0x7f0805c3

.field public static final ic_capture_invoice_menu:I = 0x7f0805c4

.field public static final ic_capture_invoice_tips:I = 0x7f0805c5

.field public static final ic_capture_load:I = 0x7f0805c6

.field public static final ic_capture_magnetic:I = 0x7f0805c7

.field public static final ic_capture_menu:I = 0x7f0805c8

.field public static final ic_capture_menu_certificate:I = 0x7f0805c9

.field public static final ic_capture_menu_driver:I = 0x7f0805ca

.field public static final ic_capture_menu_id:I = 0x7f0805cb

.field public static final ic_capture_menu_passport:I = 0x7f0805cc

.field public static final ic_capture_menu_passport_gp:I = 0x7f0805cd

.field public static final ic_capture_menu_residence_booklet:I = 0x7f0805ce

.field public static final ic_capture_mode_new:I = 0x7f0805cf

.field public static final ic_capture_mode_select_bar:I = 0x7f0805d0

.field public static final ic_capture_multi:I = 0x7f0805d1

.field public static final ic_capture_multi_tips:I = 0x7f0805d2

.field public static final ic_capture_next_page:I = 0x7f0805d3

.field public static final ic_capture_ocr:I = 0x7f0805d4

.field public static final ic_capture_ocr_tips:I = 0x7f0805d5

.field public static final ic_capture_ppt:I = 0x7f0805d6

.field public static final ic_capture_ppt_tips:I = 0x7f0805d7

.field public static final ic_capture_progress:I = 0x7f0805d8

.field public static final ic_capture_qr:I = 0x7f0805d9

.field public static final ic_capture_qr_tips:I = 0x7f0805da

.field public static final ic_capture_refactor_child_bg:I = 0x7f0805db

.field public static final ic_capture_shutter:I = 0x7f0805dc

.field public static final ic_capture_shutter_disable:I = 0x7f0805dd

.field public static final ic_capture_signature:I = 0x7f0805de

.field public static final ic_capture_signature_tips:I = 0x7f0805df

.field public static final ic_capture_single:I = 0x7f0805e0

.field public static final ic_capture_single_tips:I = 0x7f0805e1

.field public static final ic_capture_single_topic:I = 0x7f0805e2

.field public static final ic_capture_single_topic_tips:I = 0x7f0805e3

.field public static final ic_capture_smart_erase_tips:I = 0x7f0805e4

.field public static final ic_capture_smart_eraser:I = 0x7f0805e5

.field public static final ic_capture_topic_paper:I = 0x7f0805e6

.field public static final ic_capture_topic_paper_tips:I = 0x7f0805e7

.field public static final ic_capture_translate:I = 0x7f0805e8

.field public static final ic_capture_translate_tips:I = 0x7f0805e9

.field public static final ic_capture_word:I = 0x7f0805ea

.field public static final ic_capture_word_tips:I = 0x7f0805eb

.field public static final ic_car_license:I = 0x7f0805ec

.field public static final ic_card_arrow:I = 0x7f0805ed

.field public static final ic_carlicense:I = 0x7f0805ee

.field public static final ic_cashback_60px:I = 0x7f0805ef

.field public static final ic_cb_close:I = 0x7f0805f0

.field public static final ic_cb_disable:I = 0x7f0805f1

.field public static final ic_cb_open:I = 0x7f0805f2

.field public static final ic_cc_banner:I = 0x7f0805f3

.field public static final ic_cc_banner_hide:I = 0x7f0805f4

.field public static final ic_cerificate_vip:I = 0x7f0805f5

.field public static final ic_certi_detail_id_card:I = 0x7f0805f6

.field public static final ic_certificate_bank_card_1_portrait:I = 0x7f0805f7

.field public static final ic_certificate_bank_card_2_portrait:I = 0x7f0805f8

.field public static final ic_certificate_car_license_1:I = 0x7f0805f9

.field public static final ic_certificate_car_license_1_portrait:I = 0x7f0805fa

.field public static final ic_certificate_car_license_2:I = 0x7f0805fb

.field public static final ic_certificate_car_license_2_portrait:I = 0x7f0805fc

.field public static final ic_certificate_car_license_3:I = 0x7f0805fd

.field public static final ic_certificate_car_license_3_portrait:I = 0x7f0805fe

.field public static final ic_certificate_driver_license_1:I = 0x7f0805ff

.field public static final ic_certificate_driver_license_1_portrait:I = 0x7f080600

.field public static final ic_certificate_driver_license_2:I = 0x7f080601

.field public static final ic_certificate_driver_license_2_portrait:I = 0x7f080602

.field public static final ic_certificate_general_portraitr:I = 0x7f080603

.field public static final ic_certificate_photo_cover:I = 0x7f080604

.field public static final ic_certificate_photo_tips:I = 0x7f080605

.field public static final ic_certificate_preview_bg_line_refactor:I = 0x7f080606

.field public static final ic_certificate_preview_bg_refactor:I = 0x7f080607

.field public static final ic_certificate_two_page:I = 0x7f080608

.field public static final ic_certification_grid_forder_image:I = 0x7f080609

.field public static final ic_certification_list_forder_image:I = 0x7f08060a

.field public static final ic_certification_more:I = 0x7f08060b

.field public static final ic_check1_14px:I = 0x7f08060c

.field public static final ic_check1_18px:I = 0x7f08060d

.field public static final ic_check2_14px:I = 0x7f08060e

.field public static final ic_check_24px:I = 0x7f08060f

.field public static final ic_check_line_24px:I = 0x7f080610

.field public static final ic_checkbox_off:I = 0x7f080611

.field public static final ic_checkbox_on:I = 0x7f080612

.field public static final ic_checked_12dp:I = 0x7f080613

.field public static final ic_checkout_dialog_close:I = 0x7f080614

.field public static final ic_checkout_dialog_cs_logo:I = 0x7f080615

.field public static final ic_checkout_pay_way_no_selected:I = 0x7f080616

.field public static final ic_checkout_pay_way_selected:I = 0x7f080617

.field public static final ic_china_mobile_pay:I = 0x7f080618

.field public static final ic_choice_line_20px:I = 0x7f080619

.field public static final ic_choice_line_24px:I = 0x7f08061a

.field public static final ic_choose_language:I = 0x7f08061b

.field public static final ic_chrome_28_28:I = 0x7f08061c

.field public static final ic_circle_add:I = 0x7f08061d

.field public static final ic_circle_progress:I = 0x7f08061e

.field public static final ic_circle_search:I = 0x7f08061f

.field public static final ic_circle_white:I = 0x7f080620

.field public static final ic_clear_black_24:I = 0x7f080621

.field public static final ic_clear_watermark:I = 0x7f080622

.field public static final ic_clear_watermark_disable:I = 0x7f080623

.field public static final ic_clear_white:I = 0x7f080624

.field public static final ic_clip:I = 0x7f080625

.field public static final ic_clock_44_44:I = 0x7f080626

.field public static final ic_clock_black_24dp:I = 0x7f080627

.field public static final ic_close_16:I = 0x7f080628

.field public static final ic_close_16px:I = 0x7f080629

.field public static final ic_close_20px:I = 0x7f08062a

.field public static final ic_close_212121_32:I = 0x7f08062b

.field public static final ic_close_24px:I = 0x7f08062c

.field public static final ic_close_32_pagelist_guide:I = 0x7f08062d

.field public static final ic_close_32px:I = 0x7f08062e

.field public static final ic_close_5a5a5a:I = 0x7f08062f

.field public static final ic_close_dialog_36_36_white:I = 0x7f080630

.field public static final ic_close_discount_pop:I = 0x7f080631

.field public static final ic_close_drop_cnl_32_32:I = 0x7f080632

.field public static final ic_close_drop_cnl_drak_32_32:I = 0x7f080633

.field public static final ic_close_esign_24px:I = 0x7f080634

.field public static final ic_close_gray_bg_black:I = 0x7f080635

.field public static final ic_close_gray_circle:I = 0x7f080636

.field public static final ic_close_gray_circle_1c1c1e:I = 0x7f080637

.field public static final ic_close_gray_dark_circle:I = 0x7f080638

.field public static final ic_close_line_24px:I = 0x7f080639

.field public static final ic_close_new:I = 0x7f08063a

.field public static final ic_close_req8:I = 0x7f08063b

.field public static final ic_close_transparent_60x60:I = 0x7f08063c

.field public static final ic_close_white_10:I = 0x7f08063d

.field public static final ic_close_white_24px:I = 0x7f08063e

.field public static final ic_close_white_sd:I = 0x7f08063f

.field public static final ic_cloud:I = 0x7f080640

.field public static final ic_cloud_disk_dropbox:I = 0x7f080641

.field public static final ic_cloud_disk_dropbox_thumb:I = 0x7f080642

.field public static final ic_cloud_disk_file_pdf:I = 0x7f080643

.field public static final ic_cloud_disk_folder:I = 0x7f080644

.field public static final ic_cloud_disk_google_drive:I = 0x7f080645

.field public static final ic_cloud_disk_google_drive_thumb:I = 0x7f080646

.field public static final ic_cloud_disk_one_drive_thumb:I = 0x7f080647

.field public static final ic_cloud_disk_onedrive:I = 0x7f080648

.field public static final ic_cloud_disk_onedrive_new:I = 0x7f080649

.field public static final ic_cloud_full:I = 0x7f08064a

.field public static final ic_cloud_full_2:I = 0x7f08064b

.field public static final ic_cloud_place_full:I = 0x7f08064c

.field public static final ic_cloud_place_insufficient:I = 0x7f08064d

.field public static final ic_cloud_place_plenty:I = 0x7f08064e

.field public static final ic_cloud_req4:I = 0x7f08064f

.field public static final ic_cloud_service_auth_bg:I = 0x7f080650

.field public static final ic_cloud_service_auth_left:I = 0x7f080651

.field public static final ic_cloud_service_auth_right:I = 0x7f080652

.field public static final ic_cloud_storage_upload_24x24:I = 0x7f080653

.field public static final ic_cloud_sync_1gb_44_44:I = 0x7f080654

.field public static final ic_cloud_sync_44_44:I = 0x7f080655

.field public static final ic_cloud_sync_auto_backup_44_44:I = 0x7f080656

.field public static final ic_cloud_sync_close:I = 0x7f080657

.field public static final ic_cloud_sync_cloud_store_44_44:I = 0x7f080658

.field public static final ic_cloud_sync_diff_device_44_44:I = 0x7f080659

.field public static final ic_cloud_sync_exam_erase_44_44:I = 0x7f08065a

.field public static final ic_cloud_sync_photo_recovery_44_44:I = 0x7f08065b

.field public static final ic_cloud_sync_share_dir_44_44:I = 0x7f08065c

.field public static final ic_cloud_sync_smart_erase_44_44:I = 0x7f08065d

.field public static final ic_cn_annual_premium_discount:I = 0x7f08065e

.field public static final ic_cn_annual_premium_eight:I = 0x7f08065f

.field public static final ic_cn_annual_premium_eighteen:I = 0x7f080660

.field public static final ic_cn_annual_premium_eleven:I = 0x7f080661

.field public static final ic_cn_annual_premium_fifteen:I = 0x7f080662

.field public static final ic_cn_annual_premium_five:I = 0x7f080663

.field public static final ic_cn_annual_premium_four:I = 0x7f080664

.field public static final ic_cn_annual_premium_fourteen:I = 0x7f080665

.field public static final ic_cn_annual_premium_free_trial:I = 0x7f080666

.field public static final ic_cn_annual_premium_main_discount:I = 0x7f080667

.field public static final ic_cn_annual_premium_main_free_trail:I = 0x7f080668

.field public static final ic_cn_annual_premium_nine:I = 0x7f080669

.field public static final ic_cn_annual_premium_ninteen:I = 0x7f08066a

.field public static final ic_cn_annual_premium_one:I = 0x7f08066b

.field public static final ic_cn_annual_premium_seven:I = 0x7f08066c

.field public static final ic_cn_annual_premium_seventeen:I = 0x7f08066d

.field public static final ic_cn_annual_premium_six:I = 0x7f08066e

.field public static final ic_cn_annual_premium_sixteen:I = 0x7f08066f

.field public static final ic_cn_annual_premium_ten:I = 0x7f080670

.field public static final ic_cn_annual_premium_thirteen:I = 0x7f080671

.field public static final ic_cn_annual_premium_three:I = 0x7f080672

.field public static final ic_cn_annual_premium_twenty:I = 0x7f080673

.field public static final ic_cn_annual_premium_twleve:I = 0x7f080674

.field public static final ic_cn_annual_premium_two:I = 0x7f080675

.field public static final ic_cn_driver_back:I = 0x7f080676

.field public static final ic_cn_driver_front:I = 0x7f080677

.field public static final ic_cn_driver_license:I = 0x7f080678

.field public static final ic_cn_driving_license:I = 0x7f080679

.field public static final ic_cn_id_card:I = 0x7f08067a

.field public static final ic_cn_renew_recall_bg:I = 0x7f08067b

.field public static final ic_cn_renew_recall_btn:I = 0x7f08067c

.field public static final ic_cn_renew_recall_flower:I = 0x7f08067d

.field public static final ic_cn_renew_recall_flower2:I = 0x7f08067e

.field public static final ic_cn_renew_recall_gift:I = 0x7f08067f

.field public static final ic_cn_renew_recall_gray_light:I = 0x7f080680

.field public static final ic_cn_renew_recall_label:I = 0x7f080681

.field public static final ic_cn_renew_recall_page_list_bg:I = 0x7f080682

.field public static final ic_cn_renew_recall_seven_day_premium:I = 0x7f080683

.field public static final ic_cn_renew_recall_strong_line:I = 0x7f080684

.field public static final ic_cn_renew_recall_strong_line2:I = 0x7f080685

.field public static final ic_cn_renewal_dialog_price_circle:I = 0x7f080686

.field public static final ic_cn_unscribe_scaffold_bg:I = 0x7f080687

.field public static final ic_cn_unscribe_scaffold_buy:I = 0x7f080688

.field public static final ic_cn_unsubscribe_scaffold_main_vip_icon:I = 0x7f080689

.field public static final ic_cn_unsubscribe_scaffold_title:I = 0x7f08068a

.field public static final ic_code_verify_bg_3:I = 0x7f08068b

.field public static final ic_collage:I = 0x7f08068c

.field public static final ic_collarorate_doc_update:I = 0x7f08068d

.field public static final ic_color:I = 0x7f08068e

.field public static final ic_color_green:I = 0x7f08068f

.field public static final ic_color_list:I = 0x7f080690

.field public static final ic_color_select_black:I = 0x7f080691

.field public static final ic_color_select_blue:I = 0x7f080692

.field public static final ic_color_select_checked_black:I = 0x7f080693

.field public static final ic_color_select_checked_blue:I = 0x7f080694

.field public static final ic_color_select_checked_green:I = 0x7f080695

.field public static final ic_color_select_checked_orange:I = 0x7f080696

.field public static final ic_color_select_checked_red:I = 0x7f080697

.field public static final ic_color_select_checked_white:I = 0x7f080698

.field public static final ic_color_select_green:I = 0x7f080699

.field public static final ic_color_select_green_white:I = 0x7f08069a

.field public static final ic_color_select_orange:I = 0x7f08069b

.field public static final ic_color_select_red:I = 0x7f08069c

.field public static final ic_color_select_white:I = 0x7f08069d

.field public static final ic_color_select_white_trans:I = 0x7f08069e

.field public static final ic_comm_redo:I = 0x7f08069f

.field public static final ic_comm_save:I = 0x7f0806a0

.field public static final ic_comm_undo:I = 0x7f0806a1

.field public static final ic_comment_24px:I = 0x7f0806a2

.field public static final ic_comment_arrow_down:I = 0x7f0806a3

.field public static final ic_comment_arrow_up:I = 0x7f0806a4

.field public static final ic_common_arrow:I = 0x7f0806a5

.field public static final ic_common_back:I = 0x7f0806a6

.field public static final ic_common_close:I = 0x7f0806a7

.field public static final ic_common_close_212121:I = 0x7f0806a8

.field public static final ic_common_close_24px:I = 0x7f0806a9

.field public static final ic_common_close_874d33:I = 0x7f0806aa

.field public static final ic_common_close_gray:I = 0x7f0806ab

.field public static final ic_common_close_gray_bg:I = 0x7f0806ac

.field public static final ic_common_close_white:I = 0x7f0806ad

.field public static final ic_common_tag:I = 0x7f0806ae

.field public static final ic_company_license:I = 0x7f0806af

.field public static final ic_completed_req4:I = 0x7f0806b0

.field public static final ic_composite_red_40:I = 0x7f0806b1

.field public static final ic_computer_line_24px:I = 0x7f0806b2

.field public static final ic_convert:I = 0x7f0806b3

.field public static final ic_convert_pdf:I = 0x7f0806b4

.field public static final ic_coordinates:I = 0x7f0806b5

.field public static final ic_copy:I = 0x7f0806b6

.field public static final ic_copy_24px_dark:I = 0x7f0806b7

.field public static final ic_copy_24px_light:I = 0x7f0806b8

.field public static final ic_copy_line_24px:I = 0x7f0806b9

.field public static final ic_copy_line_white_24px:I = 0x7f0806ba

.field public static final ic_copy_white:I = 0x7f0806bb

.field public static final ic_corner:I = 0x7f0806bc

.field public static final ic_count_category_normal:I = 0x7f0806bd

.field public static final ic_count_category_stick:I = 0x7f0806be

.field public static final ic_count_category_tube:I = 0x7f0806bf

.field public static final ic_count_category_wood:I = 0x7f0806c0

.field public static final ic_count_loading:I = 0x7f0806c1

.field public static final ic_count_select_point:I = 0x7f0806c2

.field public static final ic_course:I = 0x7f0806c3

.field public static final ic_create_dir:I = 0x7f0806c4

.field public static final ic_create_folder_white:I = 0x7f0806c5

.field public static final ic_crop_line_24px:I = 0x7f0806c6

.field public static final ic_crop_maxedge:I = 0x7f0806c7

.field public static final ic_crop_sn:I = 0x7f0806c8

.field public static final ic_crop_toolbar_done:I = 0x7f0806c9

.field public static final ic_crying_face_36px:I = 0x7f0806ca

.field public static final ic_cs_app_icon:I = 0x7f0806cb

.field public static final ic_cs_bluetooth:I = 0x7f0806cc

.field public static final ic_cs_jpg:I = 0x7f0806cd

.field public static final ic_cs_list_bottom_add_image:I = 0x7f0806ce

.field public static final ic_cs_list_btm_process_pic:I = 0x7f0806cf

.field public static final ic_cs_list_fab_take_photo:I = 0x7f0806d0

.field public static final ic_cs_logo:I = 0x7f0806d1

.field public static final ic_cs_logo_12_12:I = 0x7f0806d2

.field public static final ic_cs_logo_20:I = 0x7f0806d3

.field public static final ic_cs_pc:I = 0x7f0806d4

.field public static final ic_cs_pdf:I = 0x7f0806d5

.field public static final ic_cs_pdf_vip_buy_success_banner:I = 0x7f0806d6

.field public static final ic_cs_pdf_vip_done:I = 0x7f0806d7

.field public static final ic_cs_printer_new:I = 0x7f0806d8

.field public static final ic_cs_save:I = 0x7f0806d9

.field public static final ic_cs_text_chinese:I = 0x7f0806da

.field public static final ic_cs_text_english:I = 0x7f0806db

.field public static final ic_cs_word:I = 0x7f0806dc

.field public static final ic_cslogo_60:I = 0x7f0806dd

.field public static final ic_customer_information:I = 0x7f0806de

.field public static final ic_cutting_20:I = 0x7f0806df

.field public static final ic_cutting_24px:I = 0x7f0806e0

.field public static final ic_cycle_pop_close:I = 0x7f0806e1

.field public static final ic_dark_cancel_50:I = 0x7f0806e2

.field public static final ic_daub_line_24px:I = 0x7f0806e3

.field public static final ic_decrease:I = 0x7f0806e4

.field public static final ic_deepclean_alert:I = 0x7f0806e5

.field public static final ic_deepclean_cache:I = 0x7f0806e6

.field public static final ic_default_img_64_64:I = 0x7f0806e7

.field public static final ic_delete_12:I = 0x7f0806e8

.field public static final ic_delete_16px_red:I = 0x7f0806e9

.field public static final ic_delete_18:I = 0x7f0806ea

.field public static final ic_delete_bar:I = 0x7f0806eb

.field public static final ic_delete_circle_gray:I = 0x7f0806ec

.field public static final ic_delete_count:I = 0x7f0806ed

.field public static final ic_delete_esign_contact:I = 0x7f0806ee

.field public static final ic_delete_history_line_24px:I = 0x7f0806ef

.field public static final ic_delete_img:I = 0x7f0806f0

.field public static final ic_delete_line_24px:I = 0x7f0806f1

.field public static final ic_delete_line_24px_red:I = 0x7f0806f2

.field public static final ic_deleteline:I = 0x7f0806f3

.field public static final ic_detail_req4:I = 0x7f0806f4

.field public static final ic_dialog_more_close:I = 0x7f0806f5

.field public static final ic_digital_add:I = 0x7f0806f6

.field public static final ic_digital_remove:I = 0x7f0806f7

.field public static final ic_discount_purchase_doc:I = 0x7f0806f8

.field public static final ic_discount_purchase_pdf:I = 0x7f0806f9

.field public static final ic_discount_purchase_watermark:I = 0x7f0806fa

.field public static final ic_dissatisfied:I = 0x7f0806fb

.field public static final ic_doc_composite:I = 0x7f0806fc

.field public static final ic_doc_email_to_me:I = 0x7f0806fd

.field public static final ic_doc_email_to_me_white:I = 0x7f0806fe

.field public static final ic_doc_example:I = 0x7f0806ff

.field public static final ic_doc_excel:I = 0x7f080700

.field public static final ic_doc_excel_40px:I = 0x7f080701

.field public static final ic_doc_export:I = 0x7f080702

.field public static final ic_doc_import:I = 0x7f080703

.field public static final ic_doc_import_header_collapse:I = 0x7f080704

.field public static final ic_doc_import_header_expand:I = 0x7f080705

.field public static final ic_doc_import_header_image:I = 0x7f080706

.field public static final ic_doc_import_search:I = 0x7f080707

.field public static final ic_doc_import_type_excel:I = 0x7f080708

.field public static final ic_doc_import_type_pdf:I = 0x7f080709

.field public static final ic_doc_import_type_ppt:I = 0x7f08070a

.field public static final ic_doc_import_type_word:I = 0x7f08070b

.field public static final ic_doc_long_img:I = 0x7f08070c

.field public static final ic_doc_long_img_upgrade:I = 0x7f08070d

.field public static final ic_doc_manul_sort:I = 0x7f08070e

.field public static final ic_doc_more_20px:I = 0x7f08070f

.field public static final ic_doc_move:I = 0x7f080710

.field public static final ic_doc_navi_more:I = 0x7f080711

.field public static final ic_doc_note:I = 0x7f080712

.field public static final ic_doc_note_normal:I = 0x7f080713

.field public static final ic_doc_note_selected:I = 0x7f080714

.field public static final ic_doc_ocr_light_20:I = 0x7f080715

.field public static final ic_doc_pdf_40px:I = 0x7f080716

.field public static final ic_doc_pdf_40px_upgrade:I = 0x7f080717

.field public static final ic_doc_ppt:I = 0x7f080718

.field public static final ic_doc_ppt_40px:I = 0x7f080719

.field public static final ic_doc_rec_permission:I = 0x7f08071a

.field public static final ic_doc_rename:I = 0x7f08071b

.field public static final ic_doc_rename_white:I = 0x7f08071c

.field public static final ic_doc_save_to_gallery:I = 0x7f08071d

.field public static final ic_doc_search_empty:I = 0x7f08071e

.field public static final ic_doc_upgrade_excel:I = 0x7f08071f

.field public static final ic_doc_upgrade_long_img:I = 0x7f080720

.field public static final ic_doc_upgrade_pdf_40px:I = 0x7f080721

.field public static final ic_doc_upgrade_ppt:I = 0x7f080722

.field public static final ic_doc_upgrade_word:I = 0x7f080723

.field public static final ic_doc_view_pdf:I = 0x7f080724

.field public static final ic_doc_view_sort:I = 0x7f080725

.field public static final ic_doc_word:I = 0x7f080726

.field public static final ic_doc_word_40px:I = 0x7f080727

.field public static final ic_docedit_deletepage:I = 0x7f080728

.field public static final ic_docjson_shortcut:I = 0x7f080729

.field public static final ic_docset_line_24px:I = 0x7f08072a

.field public static final ic_doctor_pitchon_24px:I = 0x7f08072b

.field public static final ic_doctor_rule_24px:I = 0x7f08072c

.field public static final ic_doctype_doc:I = 0x7f08072d

.field public static final ic_doctype_excel:I = 0x7f08072e

.field public static final ic_doctype_excel_mark:I = 0x7f08072f

.field public static final ic_doctype_word:I = 0x7f080730

.field public static final ic_doctype_word_mark:I = 0x7f080731

.field public static final ic_document_kingkong_generate_wrong_list:I = 0x7f080732

.field public static final ic_document_kingkong_import_pic:I = 0x7f080733

.field public static final ic_document_kingkong_print_doc:I = 0x7f080734

.field public static final ic_document_kingkong_process_pic:I = 0x7f080735

.field public static final ic_document_kingkong_puzzle:I = 0x7f080736

.field public static final ic_document_kingkong_save_to_album:I = 0x7f080737

.field public static final ic_document_kingkong_share_dir:I = 0x7f080738

.field public static final ic_document_kingkong_switch_word:I = 0x7f080739

.field public static final ic_document_more_delete_doc:I = 0x7f08073a

.field public static final ic_document_shortcut:I = 0x7f08073b

.field public static final ic_done_20px:I = 0x7f08073c

.field public static final ic_done_24px:I = 0x7f08073d

.field public static final ic_done_36:I = 0x7f08073e

.field public static final ic_done_60_60:I = 0x7f08073f

.field public static final ic_done_gray_16px:I = 0x7f080740

.field public static final ic_done_line_24px:I = 0x7f080741

.field public static final ic_done_share:I = 0x7f080742

.field public static final ic_done_svip_16px:I = 0x7f080743

.field public static final ic_done_yellow_20px:I = 0x7f080744

.field public static final ic_doodle_smudge:I = 0x7f080745

.field public static final ic_dot_qr_share:I = 0x7f080746

.field public static final ic_down_arrow:I = 0x7f080747

.field public static final ic_download:I = 0x7f080748

.field public static final ic_downloadlocal_line_24px:I = 0x7f080749

.field public static final ic_drag:I = 0x7f08074a

.field public static final ic_dragdown:I = 0x7f08074b

.field public static final ic_drive_liscence:I = 0x7f08074c

.field public static final ic_drivelicense:I = 0x7f08074d

.field public static final ic_driver_license:I = 0x7f08074e

.field public static final ic_driving_licence:I = 0x7f08074f

.field public static final ic_drop_close_white:I = 0x7f080750

.field public static final ic_edit_cspdf:I = 0x7f080751

.field public static final ic_edit_esign_contact:I = 0x7f080752

.field public static final ic_edit_idcard:I = 0x7f080753

.field public static final ic_edit_pic:I = 0x7f080754

.field public static final ic_edit_text:I = 0x7f080755

.field public static final ic_edit_title:I = 0x7f080756

.field public static final ic_editor_24px:I = 0x7f080757

.field public static final ic_editor_24px_dark:I = 0x7f080758

.field public static final ic_editor_24px_light:I = 0x7f080759

.field public static final ic_editor_line_24px:I = 0x7f08075a

.field public static final ic_edu_about_large_header_1:I = 0x7f08075b

.field public static final ic_edu_ad_24px:I = 0x7f08075c

.field public static final ic_edu_ad_2_24px:I = 0x7f08075d

.field public static final ic_edu_add_folder_24px:I = 0x7f08075e

.field public static final ic_edu_cloud_24px:I = 0x7f08075f

.field public static final ic_edu_icon_24px:I = 0x7f080760

.field public static final ic_edu_ocr_24px:I = 0x7f080761

.field public static final ic_encryption_line_24px:I = 0x7f080762

.field public static final ic_enter_16px:I = 0x7f080763

.field public static final ic_equals_18_9:I = 0x7f080764

.field public static final ic_eraser:I = 0x7f080765

.field public static final ic_eraser_green:I = 0x7f080766

.field public static final ic_esc:I = 0x7f080767

.field public static final ic_esc_24_gray:I = 0x7f080768

.field public static final ic_esc_24px:I = 0x7f080769

.field public static final ic_esc_36px:I = 0x7f08076a

.field public static final ic_esc_red:I = 0x7f08076b

.field public static final ic_esign_adjust_bg:I = 0x7f08076c

.field public static final ic_esign_checked_18:I = 0x7f08076d

.field public static final ic_esign_circle_16_16:I = 0x7f08076e

.field public static final ic_esign_close:I = 0x7f08076f

.field public static final ic_esign_detail_20:I = 0x7f080770

.field public static final ic_esign_home_share:I = 0x7f080771

.field public static final ic_esign_import_wechat_40:I = 0x7f080772

.field public static final ic_esign_invite_phone:I = 0x7f080773

.field public static final ic_esign_manage_20:I = 0x7f080774

.field public static final ic_esign_me_help:I = 0x7f080775

.field public static final ic_esign_me_kefu:I = 0x7f080776

.field public static final ic_esign_me_message:I = 0x7f080777

.field public static final ic_esign_modify_name:I = 0x7f080778

.field public static final ic_esign_unchecked_18:I = 0x7f080779

.field public static final ic_evidence:I = 0x7f08077a

.field public static final ic_evidence_list:I = 0x7f08077b

.field public static final ic_excel:I = 0x7f08077c

.field public static final ic_excel_24px:I = 0x7f08077d

.field public static final ic_excel_line_24px:I = 0x7f08077e

.field public static final ic_excel_record:I = 0x7f08077f

.field public static final ic_excel_record_24px:I = 0x7f080780

.field public static final ic_excel_red_40:I = 0x7f080781

.field public static final ic_excel_share:I = 0x7f080782

.field public static final ic_excel_thumb:I = 0x7f080783

.field public static final ic_exclamation_point:I = 0x7f080784

.field public static final ic_exclamationpoint:I = 0x7f080785

.field public static final ic_experience_next:I = 0x7f080786

.field public static final ic_export_all_docs:I = 0x7f080787

.field public static final ic_export_copy:I = 0x7f080788

.field public static final ic_export_line_24px:I = 0x7f080789

.field public static final ic_export_line_white_24px:I = 0x7f08078a

.field public static final ic_export_txt:I = 0x7f08078b

.field public static final ic_export_word:I = 0x7f08078c

.field public static final ic_export_word_white:I = 0x7f08078d

.field public static final ic_external_import_doc_menu_item:I = 0x7f08078e

.field public static final ic_extract_line_24px:I = 0x7f08078f

.field public static final ic_eye_close:I = 0x7f080790

.field public static final ic_eye_open:I = 0x7f080791

.field public static final ic_fab_import_pic:I = 0x7f080792

.field public static final ic_facebook_44:I = 0x7f080793

.field public static final ic_fake_pdf:I = 0x7f080794

.field public static final ic_fax_circle_44:I = 0x7f080795

.field public static final ic_fax_white:I = 0x7f080796

.field public static final ic_feed_back_chevron_right:I = 0x7f080797

.field public static final ic_feishu:I = 0x7f080798

.field public static final ic_file_ad_arrow:I = 0x7f080799

.field public static final ic_file_checked_20px:I = 0x7f08079a

.field public static final ic_file_checked_square_20px:I = 0x7f08079b

.field public static final ic_file_counter_14px:I = 0x7f08079c

.field public static final ic_file_counts_14px:I = 0x7f08079d

.field public static final ic_file_line_24px:I = 0x7f08079e

.field public static final ic_file_more_20px:I = 0x7f08079f

.field public static final ic_file_naming_24x24:I = 0x7f0807a0

.field public static final ic_file_number_14px:I = 0x7f0807a1

.field public static final ic_file_square_unchecked_20px:I = 0x7f0807a2

.field public static final ic_file_unchecked_20px:I = 0x7f0807a3

.field public static final ic_file_unchecked_20px_large:I = 0x7f0807a4

.field public static final ic_filecounter_16px:I = 0x7f0807a5

.field public static final ic_filethinbody_line_24px:I = 0x7f0807a6

.field public static final ic_filter:I = 0x7f0807a7

.field public static final ic_filter_24px:I = 0x7f0807a8

.field public static final ic_filter_24px_g:I = 0x7f0807a9

.field public static final ic_filter_24px_refactor:I = 0x7f0807aa

.field public static final ic_filter_adjustment:I = 0x7f0807ab

.field public static final ic_filter_bw2_5213:I = 0x7f0807ac

.field public static final ic_filter_bw_5213:I = 0x7f0807ad

.field public static final ic_filter_gray_mode_5213:I = 0x7f0807ae

.field public static final ic_filter_lighten_5213:I = 0x7f0807af

.field public static final ic_filter_line_24px:I = 0x7f0807b0

.field public static final ic_filter_magic_color_5213:I = 0x7f0807b1

.field public static final ic_filter_mirror_24px:I = 0x7f0807b2

.field public static final ic_filter_original_5213:I = 0x7f0807b3

.field public static final ic_filter_remove_5213:I = 0x7f0807b4

.field public static final ic_filter_select_logo:I = 0x7f0807b5

.field public static final ic_filter_style_black_white:I = 0x7f0807b6

.field public static final ic_filter_style_board:I = 0x7f0807b7

.field public static final ic_filter_style_echo:I = 0x7f0807b8

.field public static final ic_filter_style_enhance:I = 0x7f0807b9

.field public static final ic_filter_style_grey:I = 0x7f0807ba

.field public static final ic_filter_style_invert:I = 0x7f0807bb

.field public static final ic_filter_style_lighten:I = 0x7f0807bc

.field public static final ic_filter_style_magic:I = 0x7f0807bd

.field public static final ic_filter_style_magic_pro:I = 0x7f0807be

.field public static final ic_filter_style_no_shadow:I = 0x7f0807bf

.field public static final ic_filter_style_origin:I = 0x7f0807c0

.field public static final ic_finance_pitchon_24px:I = 0x7f0807c1

.field public static final ic_finance_rule_24px:I = 0x7f0807c2

.field public static final ic_fire:I = 0x7f0807c3

.field public static final ic_first_premium_1d:I = 0x7f0807c4

.field public static final ic_first_premium_2d:I = 0x7f0807c5

.field public static final ic_first_premium_3d:I = 0x7f0807c6

.field public static final ic_first_premium_certificate:I = 0x7f0807c7

.field public static final ic_first_premium_expired:I = 0x7f0807c8

.field public static final ic_first_premium_word:I = 0x7f0807c9

.field public static final ic_first_premium_word_banner:I = 0x7f0807ca

.field public static final ic_first_time_enter_android:I = 0x7f0807cb

.field public static final ic_flash_auto:I = 0x7f0807cc

.field public static final ic_flash_off:I = 0x7f0807cd

.field public static final ic_flash_on:I = 0x7f0807ce

.field public static final ic_flash_torch:I = 0x7f0807cf

.field public static final ic_flashlight_off:I = 0x7f0807d0

.field public static final ic_flashlight_on:I = 0x7f0807d1

.field public static final ic_focus_default:I = 0x7f0807d2

.field public static final ic_focus_fail:I = 0x7f0807d3

.field public static final ic_focus_success:I = 0x7f0807d4

.field public static final ic_folder_backup_18_18:I = 0x7f0807d5

.field public static final ic_folder_backup_80px:I = 0x7f0807d6

.field public static final ic_folder_briefcase_18_18:I = 0x7f0807d7

.field public static final ic_folder_briefcase_80_80:I = 0x7f0807d8

.field public static final ic_folder_briefcase_80_80_new:I = 0x7f0807d9

.field public static final ic_folder_change_76px:I = 0x7f0807da

.field public static final ic_folder_cloudoff_18_18:I = 0x7f0807db

.field public static final ic_folder_collect_18_18:I = 0x7f0807dc

.field public static final ic_folder_conversion_18_18:I = 0x7f0807dd

.field public static final ic_folder_delete:I = 0x7f0807de

.field public static final ic_folder_doc_explore_list:I = 0x7f0807df

.field public static final ic_folder_dox_explore_grid:I = 0x7f0807e0

.field public static final ic_folder_esign_72:I = 0x7f0807e1

.field public static final ic_folder_file_76px:I = 0x7f0807e2

.field public static final ic_folder_growthrecord_18_18:I = 0x7f0807e3

.field public static final ic_folder_growthrecord_80_80:I = 0x7f0807e4

.field public static final ic_folder_growthrecord_80_80_new:I = 0x7f0807e5

.field public static final ic_folder_homestorage_18_18:I = 0x7f0807e6

.field public static final ic_folder_homestorage_80_80:I = 0x7f0807e7

.field public static final ic_folder_homestorage_80_80_new:I = 0x7f0807e8

.field public static final ic_folder_idcard_76px:I = 0x7f0807e9

.field public static final ic_folder_idcard_gp_80_80:I = 0x7f0807ea

.field public static final ic_folder_idcard_gp_80_80_new:I = 0x7f0807eb

.field public static final ic_folder_idcards2_80:I = 0x7f0807ec

.field public static final ic_folder_idcards_18_18:I = 0x7f0807ed

.field public static final ic_folder_idcards_80_80:I = 0x7f0807ee

.field public static final ic_folder_idcards_80_80_new:I = 0x7f0807ef

.field public static final ic_folder_ideas_18_18:I = 0x7f0807f0

.field public static final ic_folder_ideas_80_80:I = 0x7f0807f1

.field public static final ic_folder_ideas_80_80_new:I = 0x7f0807f2

.field public static final ic_folder_material_gp_80_80:I = 0x7f0807f3

.field public static final ic_folder_material_gp_80_80_new:I = 0x7f0807f4

.field public static final ic_folder_mokcup_18_18:I = 0x7f0807f5

.field public static final ic_folder_mokcup_80_80:I = 0x7f0807f6

.field public static final ic_folder_moments_gp_80_80:I = 0x7f0807f7

.field public static final ic_folder_moments_gp_80_80_new:I = 0x7f0807f8

.field public static final ic_folder_newsign_18_18:I = 0x7f0807f9

.field public static final ic_folder_nocloud_76px:I = 0x7f0807fa

.field public static final ic_folder_normal:I = 0x7f0807fb

.field public static final ic_folder_normal_76px:I = 0x7f0807fc

.field public static final ic_folder_normal_new:I = 0x7f0807fd

.field public static final ic_folder_normal_share_new:I = 0x7f0807fe

.field public static final ic_folder_normal_share_new_hd:I = 0x7f0807ff

.field public static final ic_folder_path:I = 0x7f080800

.field public static final ic_folder_share_18_18:I = 0x7f080801

.field public static final ic_folder_share_76px:I = 0x7f080802

.field public static final ic_folder_shortcut:I = 0x7f080803

.field public static final ic_folder_team_18_18:I = 0x7f080804

.field public static final ic_folder_team_76px:I = 0x7f080805

.field public static final ic_folder_template:I = 0x7f080806

.field public static final ic_folder_template_new:I = 0x7f080807

.field public static final ic_foldercounter_16px:I = 0x7f080808

.field public static final ic_foldercounter_upgrade_16px:I = 0x7f080809

.field public static final ic_font_size:I = 0x7f08080a

.field public static final ic_forder_addto_desk:I = 0x7f08080b

.field public static final ic_forward:I = 0x7f08080c

.field public static final ic_forward_24:I = 0x7f08080d

.field public static final ic_free_ad_1:I = 0x7f08080e

.field public static final ic_free_ad_2:I = 0x7f08080f

.field public static final ic_free_ad_3:I = 0x7f080810

.field public static final ic_free_ad_blue_arrow:I = 0x7f080811

.field public static final ic_free_eng_signature_actionbar_right:I = 0x7f080812

.field public static final ic_free_share_tag:I = 0x7f080813

.field public static final ic_free_signature_actionibar_right:I = 0x7f080814

.field public static final ic_fullrange_line_24px:I = 0x7f080815

.field public static final ic_gallery_circle_44:I = 0x7f080816

.field public static final ic_gallery_default:I = 0x7f080817

.field public static final ic_gallery_folder:I = 0x7f080818

.field public static final ic_gallery_green_24_24:I = 0x7f080819

.field public static final ic_gallery_import_24px:I = 0x7f08081a

.field public static final ic_gallery_share:I = 0x7f08081b

.field public static final ic_gcard_guide_1:I = 0x7f08081c

.field public static final ic_gcard_guide_2:I = 0x7f08081d

.field public static final ic_gcard_guide_3:I = 0x7f08081e

.field public static final ic_gcard_guide_4:I = 0x7f08081f

.field public static final ic_gcard_guide_4_en:I = 0x7f080820

.field public static final ic_gift_card_share_img_1:I = 0x7f080821

.field public static final ic_gift_card_share_img_2:I = 0x7f080822

.field public static final ic_gift_card_share_img_3:I = 0x7f080823

.field public static final ic_go_24px:I = 0x7f080824

.field public static final ic_go_areq22:I = 0x7f080825

.field public static final ic_gold_coin_32:I = 0x7f080826

.field public static final ic_gold_premium_no_ad:I = 0x7f080827

.field public static final ic_gold_premium_priviedge_crown:I = 0x7f080828

.field public static final ic_gold_premium_shooting_mode:I = 0x7f080829

.field public static final ic_gold_premium_txt:I = 0x7f08082a

.field public static final ic_golden_collage_112x112:I = 0x7f08082b

.field public static final ic_golden_doc_112x112:I = 0x7f08082c

.field public static final ic_golden_id_mode_scan_112x112:I = 0x7f08082d

.field public static final ic_golden_no_ads_112x112:I = 0x7f08082e

.field public static final ic_golden_pdf_112x112:I = 0x7f08082f

.field public static final ic_golden_remove_watermark_112x112:I = 0x7f080830

.field public static final ic_golden_vip_big_120px:I = 0x7f080831

.field public static final ic_goldvip_get_bg:I = 0x7f080832

.field public static final ic_goldvip_gift_cloud:I = 0x7f080833

.field public static final ic_goldvip_gift_pdf:I = 0x7f080834

.field public static final ic_goldvip_gift_pdf_benefits:I = 0x7f080835

.field public static final ic_goldvip_gift_pdf_gray:I = 0x7f080836

.field public static final ic_goldvip_gift_triangle:I = 0x7f080837

.field public static final ic_goldvip_gift_vip:I = 0x7f080838

.field public static final ic_government_pitchon_24px:I = 0x7f080839

.field public static final ic_government_rule_24px:I = 0x7f08083a

.field public static final ic_gp_annual_premium_ad:I = 0x7f08083b

.field public static final ic_gp_annual_premium_cloud:I = 0x7f08083c

.field public static final ic_gp_annual_premium_hot:I = 0x7f08083d

.field public static final ic_gp_annual_premium_main_discount:I = 0x7f08083e

.field public static final ic_gp_annual_premium_main_free_trail:I = 0x7f08083f

.field public static final ic_gp_annual_premium_success_btn:I = 0x7f080840

.field public static final ic_gp_annual_premium_vip_update:I = 0x7f080841

.field public static final ic_gp_annual_premium_word:I = 0x7f080842

.field public static final ic_gp_annual_premium_yes:I = 0x7f080843

.field public static final ic_gp_card_detail_arrow_right:I = 0x7f080844

.field public static final ic_gp_first_premium_crown:I = 0x7f080845

.field public static final ic_gp_first_premium_gift_back_1:I = 0x7f080846

.field public static final ic_gp_first_premium_gift_back_2:I = 0x7f080847

.field public static final ic_gp_first_premium_gift_back_3:I = 0x7f080848

.field public static final ic_gp_first_premium_label_end:I = 0x7f080849

.field public static final ic_gp_first_premium_label_start:I = 0x7f08084a

.field public static final ic_gp_first_premium_rights_1_1:I = 0x7f08084b

.field public static final ic_gp_first_premium_rights_1_2:I = 0x7f08084c

.field public static final ic_gp_first_premium_rights_2_1:I = 0x7f08084d

.field public static final ic_gp_first_premium_rights_2_2:I = 0x7f08084e

.field public static final ic_gp_first_premium_rights_3_1:I = 0x7f08084f

.field public static final ic_gp_first_premium_rights_3_2:I = 0x7f080850

.field public static final ic_gp_first_premium_success_hook:I = 0x7f080851

.field public static final ic_gp_first_premium_top_texture_large:I = 0x7f080852

.field public static final ic_gp_first_premium_top_texture_small:I = 0x7f080853

.field public static final ic_gp_guide_pay_type_selected:I = 0x7f080854

.field public static final ic_gp_guide_pay_type_unselected:I = 0x7f080855

.field public static final ic_gp_negative_purchase_tag:I = 0x7f080856

.field public static final ic_gpay_48_28px:I = 0x7f080857

.field public static final ic_gps:I = 0x7f080858

.field public static final ic_gray_i_13_13:I = 0x7f080859

.field public static final ic_gray_unchecked_20:I = 0x7f08085a

.field public static final ic_green_frame:I = 0x7f08085b

.field public static final ic_green_right_arrow:I = 0x7f08085c

.field public static final ic_green_select:I = 0x7f08085d

.field public static final ic_grey_warning:I = 0x7f08085e

.field public static final ic_grid_forder_image:I = 0x7f08085f

.field public static final ic_grid_teamfolder:I = 0x7f080860

.field public static final ic_group_461:I = 0x7f080861

.field public static final ic_group_add:I = 0x7f080862

.field public static final ic_group_reduce:I = 0x7f080863

.field public static final ic_guide_adorn_orange_left_15_3px:I = 0x7f080864

.field public static final ic_guide_adorn_orange_right_15_3px:I = 0x7f080865

.field public static final ic_guide_anniversary_font_250_28px:I = 0x7f080866

.field public static final ic_guide_arrow:I = 0x7f080867

.field public static final ic_guide_bank_static:I = 0x7f080868

.field public static final ic_guide_book_static:I = 0x7f080869

.field public static final ic_guide_capture:I = 0x7f08086a

.field public static final ic_guide_close_64:I = 0x7f08086b

.field public static final ic_guide_count_static:I = 0x7f08086c

.field public static final ic_guide_next:I = 0x7f08086d

.field public static final ic_guide_next_24:I = 0x7f08086e

.field public static final ic_guide_no_ads_32:I = 0x7f08086f

.field public static final ic_guide_paper_static:I = 0x7f080870

.field public static final ic_guide_ppt_static:I = 0x7f080871

.field public static final ic_guide_signature_32:I = 0x7f080872

.field public static final ic_guide_signature_static:I = 0x7f080873

.field public static final ic_guide_smart_eraser:I = 0x7f080874

.field public static final ic_guide_unlimited_documents_32:I = 0x7f080875

.field public static final ic_guide_word:I = 0x7f080876

.field public static final ic_guide_writing_pad_static:I = 0x7f080877

.field public static final ic_hand_44px:I = 0x7f080878

.field public static final ic_hand_60_60:I = 0x7f080879

.field public static final ic_hands:I = 0x7f08087a

.field public static final ic_hardware_24:I = 0x7f08087b

.field public static final ic_hd_24px_refactor:I = 0x7f08087c

.field public static final ic_header_add_folder:I = 0x7f08087d

.field public static final ic_header_choice:I = 0x7f08087e

.field public static final ic_header_default_32:I = 0x7f08087f

.field public static final ic_header_grid:I = 0x7f080880

.field public static final ic_header_qr_code_scan:I = 0x7f080881

.field public static final ic_header_sort:I = 0x7f080882

.field public static final ic_headicon_local1:I = 0x7f080883

.field public static final ic_headicon_local2:I = 0x7f080884

.field public static final ic_headicon_local3:I = 0x7f080885

.field public static final ic_headicon_local4:I = 0x7f080886

.field public static final ic_headicon_local5:I = 0x7f080887

.field public static final ic_hear_cnl_apps_shop:I = 0x7f080888

.field public static final ic_hear_cnl_bilibili:I = 0x7f080889

.field public static final ic_hear_cnl_douyin:I = 0x7f08088a

.field public static final ic_hear_cnl_elec_shift:I = 0x7f08088b

.field public static final ic_hear_cnl_gongzhonghao:I = 0x7f08088c

.field public static final ic_hear_cnl_iqiyi:I = 0x7f08088d

.field public static final ic_hear_cnl_kuaishou:I = 0x7f08088e

.field public static final ic_hear_cnl_logo:I = 0x7f08088f

.field public static final ic_hear_cnl_mangguo:I = 0x7f080890

.field public static final ic_hear_cnl_subway:I = 0x7f080891

.field public static final ic_hear_cnl_weibo:I = 0x7f080892

.field public static final ic_hear_cnl_xiaohongshu:I = 0x7f080893

.field public static final ic_hear_cnl_yes:I = 0x7f080894

.field public static final ic_hear_cnl_youku:I = 0x7f080895

.field public static final ic_help_member:I = 0x7f080896

.field public static final ic_help_tips_20:I = 0x7f080897

.field public static final ic_help_white:I = 0x7f080898

.field public static final ic_helpservice_line_24px:I = 0x7f080899

.field public static final ic_highline:I = 0x7f08089a

.field public static final ic_hms_48_28px:I = 0x7f08089b

.field public static final ic_home_adicon1:I = 0x7f08089c

.field public static final ic_home_adicon2_bg:I = 0x7f08089d

.field public static final ic_home_adicon2_buton:I = 0x7f08089e

.field public static final ic_home_comment:I = 0x7f08089f

.field public static final ic_home_invent:I = 0x7f0808a0

.field public static final ic_home_nav_invite:I = 0x7f0808a1

.field public static final ic_home_nav_weixin:I = 0x7f0808a2

.field public static final ic_home_navi_done:I = 0x7f0808a3

.field public static final ic_home_nodoc:I = 0x7f0808a4

.field public static final ic_home_search_hint:I = 0x7f0808a5

.field public static final ic_home_select:I = 0x7f0808a6

.field public static final ic_home_vip_normal_1:I = 0x7f0808a7

.field public static final ic_home_vip_normal_2_free:I = 0x7f0808a8

.field public static final ic_home_vip_normal_2_other:I = 0x7f0808a9

.field public static final ic_home_vip_normal_2_pay:I = 0x7f0808aa

.field public static final ic_home_vip_normal_3:I = 0x7f0808ab

.field public static final ic_home_vip_normal_4_free:I = 0x7f0808ac

.field public static final ic_home_vip_normal_4_other:I = 0x7f0808ad

.field public static final ic_home_vip_normal_4_pay:I = 0x7f0808ae

.field public static final ic_home_vip_oversea_1:I = 0x7f0808af

.field public static final ic_home_vip_oversea_2_free:I = 0x7f0808b0

.field public static final ic_home_vip_oversea_2_pay:I = 0x7f0808b1

.field public static final ic_homead_img_52px:I = 0x7f0808b2

.field public static final ic_hot:I = 0x7f0808b3

.field public static final ic_hot_events_44px:I = 0x7f0808b4

.field public static final ic_hot_new:I = 0x7f0808b5

.field public static final ic_hot_purchase_item:I = 0x7f0808b6

.field public static final ic_house_property:I = 0x7f0808b7

.field public static final ic_house_property_pingtu:I = 0x7f0808b8

.field public static final ic_how:I = 0x7f0808b9

.field public static final ic_hr_pitchon_24px:I = 0x7f0808ba

.field public static final ic_hr_rule_24px:I = 0x7f0808bb

.field public static final ic_hukoubo_2_50px:I = 0x7f0808bc

.field public static final ic_ic_card_24px:I = 0x7f0808bd

.field public static final ic_icon_add_round_rect:I = 0x7f0808be

.field public static final ic_icon_album_circle:I = 0x7f0808bf

.field public static final ic_icon_capture_book_revert:I = 0x7f0808c0

.field public static final ic_icon_capture_checkbox_check:I = 0x7f0808c1

.field public static final ic_icon_capture_checkbox_uncheck:I = 0x7f0808c2

.field public static final ic_icon_capture_checkbox_uncheck_cccccc:I = 0x7f0808c3

.field public static final ic_icon_capture_setting_vip:I = 0x7f0808c4

.field public static final ic_icon_close_fore_white_bg_gray:I = 0x7f0808c5

.field public static final ic_icon_close_gray_bg_black_cross:I = 0x7f0808c6

.field public static final ic_icon_compare:I = 0x7f0808c7

.field public static final ic_icon_config_gray:I = 0x7f0808c8

.field public static final ic_icon_cross:I = 0x7f0808c9

.field public static final ic_icon_cross_c4c4c4:I = 0x7f0808ca

.field public static final ic_icon_data_security:I = 0x7f0808cb

.field public static final ic_icon_delete_new:I = 0x7f0808cc

.field public static final ic_icon_down_white:I = 0x7f0808cd

.field public static final ic_icon_expand_arrow_down:I = 0x7f0808ce

.field public static final ic_icon_extract_word:I = 0x7f0808cf

.field public static final ic_icon_free:I = 0x7f0808d0

.field public static final ic_icon_gift_award_result_bg:I = 0x7f0808d1

.field public static final ic_icon_gift_box:I = 0x7f0808d2

.field public static final ic_icon_home:I = 0x7f0808d3

.field public static final ic_icon_hot:I = 0x7f0808d4

.field public static final ic_icon_info_exclamation:I = 0x7f0808d5

.field public static final ic_icon_limited_free:I = 0x7f0808d6

.field public static final ic_icon_print_doc:I = 0x7f0808d7

.field public static final ic_icon_recapture:I = 0x7f0808d8

.field public static final ic_icon_recommend:I = 0x7f0808d9

.field public static final ic_icon_retry:I = 0x7f0808da

.field public static final ic_icon_school_season_award_2_time_to_word:I = 0x7f0808db

.field public static final ic_icon_school_season_award_7_day_premium:I = 0x7f0808dc

.field public static final ic_icon_school_season_award_iqiyi:I = 0x7f0808dd

.field public static final ic_icon_school_season_award_life_long_premium:I = 0x7f0808de

.field public static final ic_icon_school_season_award_printer:I = 0x7f0808df

.field public static final ic_icon_setting_refactor:I = 0x7f0808e0

.field public static final ic_icon_share_dir_king_kong:I = 0x7f0808e1

.field public static final ic_icon_tiny_folder:I = 0x7f0808e2

.field public static final ic_icon_topic_set:I = 0x7f0808e3

.field public static final ic_icon_triangle_bottom_19bcaa:I = 0x7f0808e4

.field public static final ic_icon_up_white:I = 0x7f0808e5

.field public static final ic_id_card:I = 0x7f0808e6

.field public static final ic_id_share_activity:I = 0x7f0808e7

.field public static final ic_id_tips_front:I = 0x7f0808e8

.field public static final ic_idcard_car_license_60px:I = 0x7f0808e9

.field public static final ic_idcard_drive_license_60px:I = 0x7f0808ea

.field public static final ic_idcard_drive_license_60px_2:I = 0x7f0808eb

.field public static final ic_idcard_emblem:I = 0x7f0808ec

.field public static final ic_idcard_emblem_refactor:I = 0x7f0808ed

.field public static final ic_idcard_emblem_refactor_portrait:I = 0x7f0808ee

.field public static final ic_idcard_firm_60px:I = 0x7f0808ef

.field public static final ic_idcard_head:I = 0x7f0808f0

.field public static final ic_idcard_head_refactor:I = 0x7f0808f1

.field public static final ic_idcard_head_refactor_portrait:I = 0x7f0808f2

.field public static final ic_idcard_hkb_60px:I = 0x7f0808f3

.field public static final ic_idcard_hkb_60px_2:I = 0x7f0808f4

.field public static final ic_idcard_ida_50:I = 0x7f0808f5

.field public static final ic_idcard_idcard_ch_60px:I = 0x7f0808f6

.field public static final ic_idcard_idcard_en_60px:I = 0x7f0808f7

.field public static final ic_idcard_more:I = 0x7f0808f8

.field public static final ic_idcard_passport_60px:I = 0x7f0808f9

.field public static final ic_idcard_property_60px:I = 0x7f0808fa

.field public static final ic_idcard_title_60px:I = 0x7f0808fb

.field public static final ic_idcard_visa_60px:I = 0x7f0808fc

.field public static final ic_identifyscope_line_24px:I = 0x7f0808fd

.field public static final ic_idmode_req1:I = 0x7f0808fe

.field public static final ic_image_break:I = 0x7f0808ff

.field public static final ic_image_default:I = 0x7f080900

.field public static final ic_image_file_compress:I = 0x7f080901

.field public static final ic_image_line_24px:I = 0x7f080902

.field public static final ic_image_quality_high_line:I = 0x7f080903

.field public static final ic_image_share:I = 0x7f080904

.field public static final ic_imagepageview_download:I = 0x7f080905

.field public static final ic_img_cloud_max:I = 0x7f080906

.field public static final ic_img_merge:I = 0x7f080907

.field public static final ic_img_wifi_broken:I = 0x7f080908

.field public static final ic_imgpage_ocr_small:I = 0x7f080909

.field public static final ic_imgpage_toolbar_mark:I = 0x7f08090a

.field public static final ic_imgpage_toolbar_note:I = 0x7f08090b

.field public static final ic_imgpage_toolbar_ocr:I = 0x7f08090c

.field public static final ic_imgpage_toolbar_rotete:I = 0x7f08090d

.field public static final ic_import:I = 0x7f08090e

.field public static final ic_import_backup:I = 0x7f08090f

.field public static final ic_import_doc:I = 0x7f080910

.field public static final ic_import_doc_pad:I = 0x7f080911

.field public static final ic_import_export:I = 0x7f080912

.field public static final ic_import_export_2:I = 0x7f080913

.field public static final ic_import_img:I = 0x7f080914

.field public static final ic_import_office_file_guide:I = 0x7f080915

.field public static final ic_import_wx_pic_from_gallery:I = 0x7f080916

.field public static final ic_importfile_line_24px:I = 0x7f080917

.field public static final ic_importfolder_line_24px:I = 0x7f080918

.field public static final ic_increase:I = 0x7f080919

.field public static final ic_index_tools_aiscan_36_36:I = 0x7f08091a

.field public static final ic_index_tools_aiscan_44px:I = 0x7f08091b

.field public static final ic_index_tools_aiscan_48_40:I = 0x7f08091c

.field public static final ic_index_tools_excel_44px:I = 0x7f08091d

.field public static final ic_index_tools_idcard_44px:I = 0x7f08091e

.field public static final ic_index_tools_img_44px:I = 0x7f08091f

.field public static final ic_index_tools_import_44px:I = 0x7f080920

.field public static final ic_index_tools_more_44px:I = 0x7f080921

.field public static final ic_index_tools_ocr_44px:I = 0x7f080922

.field public static final ic_index_tools_oldphoto_44px:I = 0x7f080923

.field public static final ic_index_tools_pdf_44px:I = 0x7f080924

.field public static final ic_index_tools_photo_44px:I = 0x7f080925

.field public static final ic_index_tools_scan_44px:I = 0x7f080926

.field public static final ic_index_tools_signature_44px:I = 0x7f080927

.field public static final ic_index_tools_test_paper:I = 0x7f080928

.field public static final ic_index_tools_test_paper_kingkong:I = 0x7f080929

.field public static final ic_index_tools_watermark_44px:I = 0x7f08092a

.field public static final ic_index_tools_word_44px:I = 0x7f08092b

.field public static final ic_information_40px:I = 0x7f08092c

.field public static final ic_information_tips:I = 0x7f08092d

.field public static final ic_innovation_lab:I = 0x7f08092e

.field public static final ic_input_clear_18px:I = 0x7f08092f

.field public static final ic_integral_home:I = 0x7f080930

.field public static final ic_integral_me:I = 0x7f080931

.field public static final ic_invoice_capture:I = 0x7f080932

.field public static final ic_invoice_fail_intro:I = 0x7f080933

.field public static final ic_invoice_guide_new_static:I = 0x7f080934

.field public static final ic_invoice_retry:I = 0x7f080935

.field public static final ic_jz_24px:I = 0x7f080936

.field public static final ic_jz_24px_pressed:I = 0x7f080937

.field public static final ic_kakaotalk:I = 0x7f080938

.field public static final ic_keyboard_arrow_right:I = 0x7f080939

.field public static final ic_keyboard_black_24dp:I = 0x7f08093a

.field public static final ic_label_document_recovery:I = 0x7f08093b

.field public static final ic_label_line_24px:I = 0x7f08093c

.field public static final ic_label_spdf_20px:I = 0x7f08093d

.field public static final ic_language:I = 0x7f08093e

.field public static final ic_lasso:I = 0x7f08093f

.field public static final ic_lasso_green:I = 0x7f080940

.field public static final ic_last_share_triangle_down:I = 0x7f080941

.field public static final ic_last_share_triangle_left:I = 0x7f080942

.field public static final ic_last_share_triangle_left_red:I = 0x7f080943

.field public static final ic_last_share_triangle_up:I = 0x7f080944

.field public static final ic_launcher_background:I = 0x7f080945

.field public static final ic_launcher_cs_background:I = 0x7f080946

.field public static final ic_launcher_cs_background_vip:I = 0x7f080947

.field public static final ic_launcher_cs_foreground:I = 0x7f080948

.field public static final ic_launcher_foreground:I = 0x7f080949

.field public static final ic_law_pitchon_24px:I = 0x7f08094a

.field public static final ic_law_rule_24px:I = 0x7f08094b

.field public static final ic_left_arrow:I = 0x7f08094c

.field public static final ic_left_arrow_72:I = 0x7f08094d

.field public static final ic_left_rotate_line_24px:I = 0x7f08094e

.field public static final ic_lianzi:I = 0x7f08094f

.field public static final ic_light_375_375:I = 0x7f080950

.field public static final ic_line:I = 0x7f080951

.field public static final ic_line_wechat:I = 0x7f080952

.field public static final ic_list_forder_image:I = 0x7f080953

.field public static final ic_list_forder_more:I = 0x7f080954

.field public static final ic_list_mode_24px:I = 0x7f080955

.field public static final ic_list_teamfolder:I = 0x7f080956

.field public static final ic_little_i_16x16:I = 0x7f080957

.field public static final ic_load_white_36px:I = 0x7f080958

.field public static final ic_loading_18:I = 0x7f080959

.field public static final ic_loading_blank:I = 0x7f08095a

.field public static final ic_loading_green:I = 0x7f08095b

.field public static final ic_loading_pull_release:I = 0x7f08095c

.field public static final ic_loading_purchase_info:I = 0x7f08095d

.field public static final ic_loading_tips_16px:I = 0x7f08095e

.field public static final ic_local_doc_folder_arrow:I = 0x7f08095f

.field public static final ic_lock:I = 0x7f080960

.field public static final ic_login_bottom_dialog_back:I = 0x7f080961

.field public static final ic_login_email_half_screen:I = 0x7f080962

.field public static final ic_login_email_login_bottom:I = 0x7f080963

.field public static final ic_login_email_me_page:I = 0x7f080964

.field public static final ic_login_email_me_page_cn:I = 0x7f080965

.field public static final ic_login_google:I = 0x7f080966

.field public static final ic_login_google_bg:I = 0x7f080967

.field public static final ic_login_guide_bubble_intro:I = 0x7f080968

.field public static final ic_login_head_icon:I = 0x7f080969

.field public static final ic_login_head_last_login:I = 0x7f08096a

.field public static final ic_login_mail:I = 0x7f08096b

.field public static final ic_login_mailphone:I = 0x7f08096c

.field public static final ic_login_main_home_bubble_bg:I = 0x7f08096d

.field public static final ic_login_main_home_bubble_bg_last_login:I = 0x7f08096e

.field public static final ic_login_main_selected:I = 0x7f08096f

.field public static final ic_login_main_unselected:I = 0x7f080970

.field public static final ic_login_mobile:I = 0x7f080971

.field public static final ic_login_mobile_half_screen:I = 0x7f080972

.field public static final ic_login_mobile_me_page:I = 0x7f080973

.field public static final ic_login_mobile_me_page_cn:I = 0x7f080974

.field public static final ic_login_opt_bg_last_login:I = 0x7f080975

.field public static final ic_login_opt_bg_last_login_vip:I = 0x7f080976

.field public static final ic_login_protocol_dialog_close:I = 0x7f080977

.field public static final ic_login_protocol_select:I = 0x7f080978

.field public static final ic_login_protocol_unselect:I = 0x7f080979

.field public static final ic_login_tips_for_licence:I = 0x7f08097a

.field public static final ic_login_ways_back:I = 0x7f08097b

.field public static final ic_login_ways_bg:I = 0x7f08097c

.field public static final ic_login_ways_email:I = 0x7f08097d

.field public static final ic_login_ways_google_logo:I = 0x7f08097e

.field public static final ic_login_ways_logo:I = 0x7f08097f

.field public static final ic_login_ways_phone:I = 0x7f080980

.field public static final ic_login_ways_text:I = 0x7f080981

.field public static final ic_login_wechat:I = 0x7f080982

.field public static final ic_login_wechat_me_page:I = 0x7f080983

.field public static final ic_logo_google:I = 0x7f080984

.field public static final ic_logo_google_play:I = 0x7f080985

.field public static final ic_logo_gray:I = 0x7f080986

.field public static final ic_long_img_24px:I = 0x7f080987

.field public static final ic_long_img_24px_out:I = 0x7f080988

.field public static final ic_long_img_40px:I = 0x7f080989

.field public static final ic_long_press_hint:I = 0x7f08098a

.field public static final ic_longimage_line_24px:I = 0x7f08098b

.field public static final ic_looper_excel:I = 0x7f08098c

.field public static final ic_looper_id:I = 0x7f08098d

.field public static final ic_looper_id_mode:I = 0x7f08098e

.field public static final ic_looper_sign:I = 0x7f08098f

.field public static final ic_looper_test_paper:I = 0x7f080990

.field public static final ic_looper_to_word:I = 0x7f080991

.field public static final ic_looper_word:I = 0x7f080992

.field public static final ic_m3_chip_check:I = 0x7f080993

.field public static final ic_m3_chip_checked_circle:I = 0x7f080994

.field public static final ic_m3_chip_close:I = 0x7f080995

.field public static final ic_mailme_line_24px:I = 0x7f080996

.field public static final ic_main_fab_photo:I = 0x7f080997

.field public static final ic_map_bg:I = 0x7f080998

.field public static final ic_market_pitchon_24px:I = 0x7f080999

.field public static final ic_market_rule_24px:I = 0x7f08099a

.field public static final ic_markup_line_24px:I = 0x7f08099b

.field public static final ic_me_account_line:I = 0x7f08099c

.field public static final ic_me_card_arrow_right_24px:I = 0x7f08099d

.field public static final ic_me_card_cloud_36px:I = 0x7f08099e

.field public static final ic_me_card_enterprise_36px:I = 0x7f08099f

.field public static final ic_me_card_invoice_36px:I = 0x7f0809a0

.field public static final ic_me_card_point_36px:I = 0x7f0809a1

.field public static final ic_me_card_task_36px:I = 0x7f0809a2

.field public static final ic_me_card_team_36px:I = 0x7f0809a3

.field public static final ic_me_cloud_add_12:I = 0x7f0809a4

.field public static final ic_me_cloud_add_vip_12:I = 0x7f0809a5

.field public static final ic_me_file_line:I = 0x7f0809a6

.field public static final ic_me_free_vip_card_1:I = 0x7f0809a7

.field public static final ic_me_gift_14px:I = 0x7f0809a8

.field public static final ic_me_hardware:I = 0x7f0809a9

.field public static final ic_me_head_logged_56px:I = 0x7f0809aa

.field public static final ic_me_head_logged_gp_premium_56px:I = 0x7f0809ab

.field public static final ic_me_head_logged_vip_56px:I = 0x7f0809ac

.field public static final ic_me_head_nologged_56px:I = 0x7f0809ad

.field public static final ic_me_head_nologged_vip_56px:I = 0x7f0809ae

.field public static final ic_me_help_service_line:I = 0x7f0809af

.field public static final ic_me_invite_edu_user:I = 0x7f0809b0

.field public static final ic_me_no_vip_14px:I = 0x7f0809b1

.field public static final ic_me_page_area_free_vip:I = 0x7f0809b2

.field public static final ic_me_page_avatar_level_1:I = 0x7f0809b3

.field public static final ic_me_page_avatar_level_2:I = 0x7f0809b4

.field public static final ic_me_page_avatar_level_3:I = 0x7f0809b5

.field public static final ic_me_page_avatar_level_4:I = 0x7f0809b6

.field public static final ic_me_page_avatar_level_5:I = 0x7f0809b7

.field public static final ic_me_page_avatar_level_6:I = 0x7f0809b8

.field public static final ic_me_page_avatar_level_7:I = 0x7f0809b9

.field public static final ic_me_page_login_opt_bg:I = 0x7f0809ba

.field public static final ic_me_page_message:I = 0x7f0809bb

.field public static final ic_me_page_o_vip_anti_theft:I = 0x7f0809bc

.field public static final ic_me_page_o_vip_certificates:I = 0x7f0809bd

.field public static final ic_me_page_o_vip_excel:I = 0x7f0809be

.field public static final ic_me_page_o_vip_ppt:I = 0x7f0809bf

.field public static final ic_me_page_o_vip_rewater:I = 0x7f0809c0

.field public static final ic_me_page_o_vip_word:I = 0x7f0809c1

.field public static final ic_me_page_scan:I = 0x7f0809c2

.field public static final ic_me_page_state_o_vip:I = 0x7f0809c3

.field public static final ic_me_page_vip_card_level_1:I = 0x7f0809c4

.field public static final ic_me_page_vip_card_level_2:I = 0x7f0809c5

.field public static final ic_me_page_vip_card_level_3:I = 0x7f0809c6

.field public static final ic_me_page_vip_card_level_4:I = 0x7f0809c7

.field public static final ic_me_page_vip_card_level_5:I = 0x7f0809c8

.field public static final ic_me_page_vip_card_level_6:I = 0x7f0809c9

.field public static final ic_me_page_vip_card_level_7:I = 0x7f0809ca

.field public static final ic_me_page_vip_card_next:I = 0x7f0809cb

.field public static final ic_me_page_vip_login_opt_bg:I = 0x7f0809cc

.field public static final ic_me_page_vip_right_moire:I = 0x7f0809cd

.field public static final ic_me_page_vip_right_sale:I = 0x7f0809ce

.field public static final ic_me_page_vip_right_vip:I = 0x7f0809cf

.field public static final ic_me_page_vip_right_word:I = 0x7f0809d0

.field public static final ic_me_prompt_line:I = 0x7f0809d1

.field public static final ic_me_recommend_line:I = 0x7f0809d2

.field public static final ic_me_scan_text_line:I = 0x7f0809d3

.field public static final ic_me_set_line:I = 0x7f0809d4

.field public static final ic_me_state_edu_14px:I = 0x7f0809d5

.field public static final ic_me_state_ordinary_14px:I = 0x7f0809d6

.field public static final ic_me_state_plus_14px:I = 0x7f0809d7

.field public static final ic_me_state_svip_14px:I = 0x7f0809d8

.field public static final ic_me_state_vip_14px:I = 0x7f0809d9

.field public static final ic_me_student_rights:I = 0x7f0809da

.field public static final ic_me_subscription_line:I = 0x7f0809db

.field public static final ic_me_svip_14px:I = 0x7f0809dc

.field public static final ic_me_synchronous_line:I = 0x7f0809dd

.field public static final ic_meeting_minutes:I = 0x7f0809de

.field public static final ic_menu_erase:I = 0x7f0809df

.field public static final ic_menu_grid_mode:I = 0x7f0809e0

.field public static final ic_menu_hand_write:I = 0x7f0809e1

.field public static final ic_menu_import_images:I = 0x7f0809e2

.field public static final ic_menu_list_mode:I = 0x7f0809e3

.field public static final ic_menu_mail:I = 0x7f0809e4

.field public static final ic_menu_order:I = 0x7f0809e5

.field public static final ic_menu_order_auto:I = 0x7f0809e6

.field public static final ic_menu_pdf:I = 0x7f0809e7

.field public static final ic_menu_pintu:I = 0x7f0809e8

.field public static final ic_menu_rename:I = 0x7f0809e9

.field public static final ic_menu_select:I = 0x7f0809ea

.field public static final ic_menu_select_new:I = 0x7f0809eb

.field public static final ic_menu_shopping:I = 0x7f0809ec

.field public static final ic_menu_sort_way:I = 0x7f0809ed

.field public static final ic_menu_tag:I = 0x7f0809ee

.field public static final ic_menus_more_44:I = 0x7f0809ef

.field public static final ic_menus_pyq_44:I = 0x7f0809f0

.field public static final ic_menus_qq_44:I = 0x7f0809f1

.field public static final ic_menus_wx_44:I = 0x7f0809f2

.field public static final ic_menus_wx_44_new:I = 0x7f0809f3

.field public static final ic_merge_line_24px:I = 0x7f0809f4

.field public static final ic_messenger:I = 0x7f0809f5

.field public static final ic_messenger_lite:I = 0x7f0809f6

.field public static final ic_minus_10_2:I = 0x7f0809f7

.field public static final ic_minus_18_4:I = 0x7f0809f8

.field public static final ic_mode_cardpack_24:I = 0x7f0809f9

.field public static final ic_mode_cardpack_select_24:I = 0x7f0809fa

.field public static final ic_mode_list_24:I = 0x7f0809fb

.field public static final ic_mode_list_select_24:I = 0x7f0809fc

.field public static final ic_mode_thumbnails_24:I = 0x7f0809fd

.field public static final ic_mode_thumbnails_select_24:I = 0x7f0809fe

.field public static final ic_mode_timeline_24:I = 0x7f0809ff

.field public static final ic_mode_timeline_select_24:I = 0x7f080a00

.field public static final ic_moire_24px_off:I = 0x7f080a01

.field public static final ic_moire_24px_off_dark:I = 0x7f080a02

.field public static final ic_more:I = 0x7f080a03

.field public static final ic_more_black:I = 0x7f080a04

.field public static final ic_more_clrcle_44:I = 0x7f080a05

.field public static final ic_more_help_24px:I = 0x7f080a06

.field public static final ic_more_horizontal_gray_24:I = 0x7f080a07

.field public static final ic_more_jpg:I = 0x7f080a08

.field public static final ic_more_line_24px:I = 0x7f080a09

.field public static final ic_more_pdf_encryption_32px:I = 0x7f080a0a

.field public static final ic_more_recognize:I = 0x7f080a0b

.field public static final ic_more_seal:I = 0x7f080a0c

.field public static final ic_more_web_24px:I = 0x7f080a0d

.field public static final ic_more_word:I = 0x7f080a0e

.field public static final ic_move:I = 0x7f080a0f

.field public static final ic_move_exist_documents:I = 0x7f080a10

.field public static final ic_move_line_24px:I = 0x7f080a11

.field public static final ic_move_other_in_24px:I = 0x7f080a12

.field public static final ic_mtrl_checked_circle:I = 0x7f080a13

.field public static final ic_mtrl_chip_checked_black:I = 0x7f080a14

.field public static final ic_mtrl_chip_checked_circle:I = 0x7f080a15

.field public static final ic_mtrl_chip_close_circle:I = 0x7f080a16

.field public static final ic_my_account_cs_pdf_vip_icon:I = 0x7f080a17

.field public static final ic_my_account_pdf_more:I = 0x7f080a18

.field public static final ic_navigation_left:I = 0x7f080a19

.field public static final ic_navigationbar_back_24:I = 0x7f080a1a

.field public static final ic_new_81x42:I = 0x7f080a1b

.field public static final ic_new_main:I = 0x7f080a1c

.field public static final ic_new_pdf:I = 0x7f080a1d

.field public static final ic_new_web:I = 0x7f080a1e

.field public static final ic_newfolder_line_20px:I = 0x7f080a1f

.field public static final ic_newfolder_line_24px:I = 0x7f080a20

.field public static final ic_newfolder_req5:I = 0x7f080a21

.field public static final ic_newscan_close:I = 0x7f080a22

.field public static final ic_newsign_camera:I = 0x7f080a23

.field public static final ic_newsign_import_camera:I = 0x7f080a24

.field public static final ic_newsign_import_file:I = 0x7f080a25

.field public static final ic_newsign_import_image:I = 0x7f080a26

.field public static final ic_newsign_sign_24:I = 0x7f080a27

.field public static final ic_newsign_tab:I = 0x7f080a28

.field public static final ic_next:I = 0x7f080a29

.field public static final ic_next_step:I = 0x7f080a2a

.field public static final ic_no:I = 0x7f080a2b

.field public static final ic_no_prompt_selected_circle:I = 0x7f080a2c

.field public static final ic_no_prompt_selected_retangle:I = 0x7f080a2d

.field public static final ic_no_prompt_unselected_circle:I = 0x7f080a2e

.field public static final ic_no_prompt_unselected_retangle:I = 0x7f080a2f

.field public static final ic_no_task_drawable:I = 0x7f080a30

.field public static final ic_none_doc_empty_tip_arrow_left:I = 0x7f080a31

.field public static final ic_none_doc_empty_tip_arrow_right:I = 0x7f080a32

.field public static final ic_nonvip:I = 0x7f080a33

.field public static final ic_normal_premium_collage_112x112:I = 0x7f080a34

.field public static final ic_normal_premium_doc_112x112:I = 0x7f080a35

.field public static final ic_normal_premium_id_mode_scan_112x112:I = 0x7f080a36

.field public static final ic_normal_premium_no_ads_112x112:I = 0x7f080a37

.field public static final ic_normal_premium_no_watermark_112x112:I = 0x7f080a38

.field public static final ic_normal_premium_privileges_112x112:I = 0x7f080a39

.field public static final ic_not_selected_gold:I = 0x7f080a3a

.field public static final ic_note_line_24px:I = 0x7f080a3b

.field public static final ic_o_vip_upgrade_agree_check:I = 0x7f080a3c

.field public static final ic_o_vip_upgrade_agree_normal:I = 0x7f080a3d

.field public static final ic_o_vip_upgrade_pay_type_ali:I = 0x7f080a3e

.field public static final ic_o_vip_upgrade_pay_type_wx:I = 0x7f080a3f

.field public static final ic_ocr_10times:I = 0x7f080a40

.field public static final ic_ocr_20px:I = 0x7f080a41

.field public static final ic_ocr_24:I = 0x7f080a42

.field public static final ic_ocr_24px_dark:I = 0x7f080a43

.field public static final ic_ocr_24px_light:I = 0x7f080a44

.field public static final ic_ocr_4times:I = 0x7f080a45

.field public static final ic_ocr_512px:I = 0x7f080a46

.field public static final ic_ocr_again:I = 0x7f080a47

.field public static final ic_ocr_batch_20px:I = 0x7f080a48

.field public static final ic_ocr_copy:I = 0x7f080a49

.field public static final ic_ocr_done:I = 0x7f080a4a

.field public static final ic_ocr_line_24px:I = 0x7f080a4b

.field public static final ic_ocr_line_24px_white:I = 0x7f080a4c

.field public static final ic_ocr_nav_arrows_left_disable:I = 0x7f080a4d

.field public static final ic_ocr_nav_arrows_left_normal:I = 0x7f080a4e

.field public static final ic_ocr_nav_arrows_right_disable:I = 0x7f080a4f

.field public static final ic_ocr_nav_arrows_right_normal:I = 0x7f080a50

.field public static final ic_ocr_question_24px:I = 0x7f080a51

.field public static final ic_ocr_result_slider:I = 0x7f080a52

.field public static final ic_ocr_tab_export_light_24:I = 0x7f080a53

.field public static final ic_ocr_txt_export:I = 0x7f080a54

.field public static final ic_ocr_word:I = 0x7f080a55

.field public static final ic_offline_gride_forder_image:I = 0x7f080a56

.field public static final ic_offline_list_forder_image:I = 0x7f080a57

.field public static final ic_oldmembers_ocr_36px:I = 0x7f080a58

.field public static final ic_oldmembers_pdf_36px:I = 0x7f080a59

.field public static final ic_oldmembers_scanidcand_36px:I = 0x7f080a5a

.field public static final ic_oldmembers_toword_36px:I = 0x7f080a5b

.field public static final ic_onetry_pop_close:I = 0x7f080a5c

.field public static final ic_onetry_pop_msg:I = 0x7f080a5d

.field public static final ic_only_read_bottom_bar_1:I = 0x7f080a5e

.field public static final ic_only_read_bottom_bar_2:I = 0x7f080a5f

.field public static final ic_only_read_bottom_bar_3:I = 0x7f080a60

.field public static final ic_only_read_bottom_bar_4:I = 0x7f080a61

.field public static final ic_only_read_bottom_bubble:I = 0x7f080a62

.field public static final ic_only_read_first_recent:I = 0x7f080a63

.field public static final ic_only_read_four_below:I = 0x7f080a64

.field public static final ic_only_read_four_top:I = 0x7f080a65

.field public static final ic_only_read_no_doc:I = 0x7f080a66

.field public static final ic_only_read_second_top:I = 0x7f080a67

.field public static final ic_only_read_third_below:I = 0x7f080a68

.field public static final ic_only_read_third_top:I = 0x7f080a69

.field public static final ic_open_on_cs_pdf:I = 0x7f080a6a

.field public static final ic_other_pitchon_24px:I = 0x7f080a6b

.field public static final ic_other_rule_24px:I = 0x7f080a6c

.field public static final ic_outside_logo_cspdf:I = 0x7f080a6d

.field public static final ic_package_delivery_close:I = 0x7f080a6e

.field public static final ic_pad_camera:I = 0x7f080a6f

.field public static final ic_pad_camera_tip:I = 0x7f080a70

.field public static final ic_pad_tool:I = 0x7f080a71

.field public static final ic_page_2:I = 0x7f080a72

.field public static final ic_page_add:I = 0x7f080a73

.field public static final ic_page_delete:I = 0x7f080a74

.field public static final ic_page_list_immersive_quit:I = 0x7f080a75

.field public static final ic_page_num_left_arrow:I = 0x7f080a76

.field public static final ic_page_ratake_24px:I = 0x7f080a77

.field public static final ic_page_rename:I = 0x7f080a78

.field public static final ic_page_retake:I = 0x7f080a79

.field public static final ic_page_save_gallery:I = 0x7f080a7a

.field public static final ic_page_split:I = 0x7f080a7b

.field public static final ic_page_title_rename_20px:I = 0x7f080a7c

.field public static final ic_page_to_word:I = 0x7f080a7d

.field public static final ic_page_word_scan_24px:I = 0x7f080a7e

.field public static final ic_pagelist_checkbox_unselect:I = 0x7f080a7f

.field public static final ic_pagelist_viewmode_big_24:I = 0x7f080a80

.field public static final ic_pagelist_viewmode_thumb_24:I = 0x7f080a81

.field public static final ic_pagelist_word_20px:I = 0x7f080a82

.field public static final ic_paper_empty:I = 0x7f080a83

.field public static final ic_paper_page_24px:I = 0x7f080a84

.field public static final ic_passport:I = 0x7f080a85

.field public static final ic_passport_capture_line:I = 0x7f080a86

.field public static final ic_passport_cn_capture_line:I = 0x7f080a87

.field public static final ic_passport_icon:I = 0x7f080a88

.field public static final ic_password_20px:I = 0x7f080a89

.field public static final ic_password_close:I = 0x7f080a8a

.field public static final ic_password_line_24px:I = 0x7f080a8b

.field public static final ic_password_open:I = 0x7f080a8c

.field public static final ic_pay_close:I = 0x7f080a8d

.field public static final ic_pay_hd:I = 0x7f080a8e

.field public static final ic_pay_id:I = 0x7f080a8f

.field public static final ic_pay_text:I = 0x7f080a90

.field public static final ic_pay_unlimited:I = 0x7f080a91

.field public static final ic_pay_way_ali:I = 0x7f080a92

.field public static final ic_pay_way_wx:I = 0x7f080a93

.field public static final ic_pc_10_14:I = 0x7f080a94

.field public static final ic_pc_14_20:I = 0x7f080a95

.field public static final ic_pc_16_20:I = 0x7f080a96

.field public static final ic_pc_16_22:I = 0x7f080a97

.field public static final ic_pc_big_one_inch:I = 0x7f080a98

.field public static final ic_pc_card:I = 0x7f080a99

.field public static final ic_pc_id_card:I = 0x7f080a9a

.field public static final ic_pc_ocr_24px:I = 0x7f080a9b

.field public static final ic_pc_one_inch:I = 0x7f080a9c

.field public static final ic_pc_passport:I = 0x7f080a9d

.field public static final ic_pc_size:I = 0x7f080a9e

.field public static final ic_pc_small_one_inch:I = 0x7f080a9f

.field public static final ic_pc_two_inch:I = 0x7f080aa0

.field public static final ic_pc_usa_passport:I = 0x7f080aa1

.field public static final ic_pdf_40px:I = 0x7f080aa2

.field public static final ic_pdf_app_icon:I = 0x7f080aa3

.field public static final ic_pdf_autograph:I = 0x7f080aa4

.field public static final ic_pdf_batch_line_24px:I = 0x7f080aa5

.field public static final ic_pdf_box:I = 0x7f080aa6

.field public static final ic_pdf_compress:I = 0x7f080aa7

.field public static final ic_pdf_compression:I = 0x7f080aa8

.field public static final ic_pdf_editing_image_edit_24px:I = 0x7f080aa9

.field public static final ic_pdf_encrypt:I = 0x7f080aaa

.field public static final ic_pdf_encryption:I = 0x7f080aab

.field public static final ic_pdf_encryption_24px:I = 0x7f080aac

.field public static final ic_pdf_esc_bg_gray:I = 0x7f080aad

.field public static final ic_pdf_for_share:I = 0x7f080aae

.field public static final ic_pdf_for_share_mini_program:I = 0x7f080aaf

.field public static final ic_pdf_gallery_back:I = 0x7f080ab0

.field public static final ic_pdf_gallery_search:I = 0x7f080ab1

.field public static final ic_pdf_import_tips:I = 0x7f080ab2

.field public static final ic_pdf_line_24px:I = 0x7f080ab3

.field public static final ic_pdf_local_76:I = 0x7f080ab4

.field public static final ic_pdf_phone_24px:I = 0x7f080ab5

.field public static final ic_pdf_preview_backup_tips:I = 0x7f080ab6

.field public static final ic_pdf_share:I = 0x7f080ab7

.field public static final ic_pdf_signature:I = 0x7f080ab8

.field public static final ic_pdf_tips_arrows_12:I = 0x7f080ab9

.field public static final ic_pdf_tips_close_20:I = 0x7f080aba

.field public static final ic_pdf_tips_enc:I = 0x7f080abb

.field public static final ic_pdf_tips_enc1_104_72:I = 0x7f080abc

.field public static final ic_pdf_tool_24px:I = 0x7f080abd

.field public static final ic_pdf_transfer_doc:I = 0x7f080abe

.field public static final ic_pdf_watermark_edit_new1_brazil:I = 0x7f080abf

.field public static final ic_pdf_watermark_edit_new1_cn:I = 0x7f080ac0

.field public static final ic_pdf_watermark_edit_new1_en:I = 0x7f080ac1

.field public static final ic_pdf_watermark_edit_new2_brazil:I = 0x7f080ac2

.field public static final ic_pdf_watermark_edit_new2_cn:I = 0x7f080ac3

.field public static final ic_pdf_watermark_edit_new2_en:I = 0x7f080ac4

.field public static final ic_pdf_watermark_export_new1_brazil:I = 0x7f080ac5

.field public static final ic_pdf_watermark_export_new1_cn:I = 0x7f080ac6

.field public static final ic_pdf_watermark_export_new1_en:I = 0x7f080ac7

.field public static final ic_pdf_watermark_export_new2_brazil:I = 0x7f080ac8

.field public static final ic_pdf_watermark_export_new2_cn:I = 0x7f080ac9

.field public static final ic_pdf_watermark_export_new2_en:I = 0x7f080aca

.field public static final ic_pdf_watermark_export_new3_cn:I = 0x7f080acb

.field public static final ic_pdf_watermark_share_free_cn:I = 0x7f080acc

.field public static final ic_pdf_watermark_share_free_en:I = 0x7f080acd

.field public static final ic_pdf_watermark_share_free_in:I = 0x7f080ace

.field public static final ic_pdf_watermark_share_indonesia:I = 0x7f080acf

.field public static final ic_pdf_watermark_share_new1_brazil:I = 0x7f080ad0

.field public static final ic_pdf_watermark_share_new1_cn:I = 0x7f080ad1

.field public static final ic_pdf_watermark_share_new1_en:I = 0x7f080ad2

.field public static final ic_pdf_watermark_share_new2_brazil:I = 0x7f080ad3

.field public static final ic_pdf_watermark_share_new2_cn:I = 0x7f080ad4

.field public static final ic_pdf_watermark_share_new2_en:I = 0x7f080ad5

.field public static final ic_pdf_watermark_share_new3_cn:I = 0x7f080ad6

.field public static final ic_pdf_watermark_share_watermark_qrcode:I = 0x7f080ad7

.field public static final ic_pdf_watermark_share_watermark_qrcode_large:I = 0x7f080ad8

.field public static final ic_pdf_word:I = 0x7f080ad9

.field public static final ic_pdftllos_fill_froms_44px:I = 0x7f080ada

.field public static final ic_pdftools_cswatermark_44px3x:I = 0x7f080adb

.field public static final ic_pdftools_cutting_44px3x:I = 0x7f080adc

.field public static final ic_pen_24:I = 0x7f080add

.field public static final ic_permission_camera:I = 0x7f080ade

.field public static final ic_permission_location:I = 0x7f080adf

.field public static final ic_permission_storage:I = 0x7f080ae0

.field public static final ic_permission_telephone:I = 0x7f080ae1

.field public static final ic_phone:I = 0x7f080ae2

.field public static final ic_phone_file_green_24_24:I = 0x7f080ae3

.field public static final ic_photo_for_ocr:I = 0x7f080ae4

.field public static final ic_photo_ida_20_30:I = 0x7f080ae5

.field public static final ic_picture_puzzle_20px:I = 0x7f080ae6

.field public static final ic_plus_10_10:I = 0x7f080ae7

.field public static final ic_plus_28:I = 0x7f080ae8

.field public static final ic_point_down:I = 0x7f080ae9

.field public static final ic_point_mid:I = 0x7f080aea

.field public static final ic_point_top:I = 0x7f080aeb

.field public static final ic_pop_ad_36px:I = 0x7f080aec

.field public static final ic_pop_bug_arrow_red_24px:I = 0x7f080aed

.field public static final ic_pop_close_24px:I = 0x7f080aee

.field public static final ic_pop_idcard_36px:I = 0x7f080aef

.field public static final ic_pop_left_15_10px:I = 0x7f080af0

.field public static final ic_pop_ocr_36px:I = 0x7f080af1

.field public static final ic_pop_off_144_144px:I = 0x7f080af2

.field public static final ic_pop_pdf_36px:I = 0x7f080af3

.field public static final ic_pop_pre_10g_32px:I = 0x7f080af4

.field public static final ic_pop_pre_ad_32px:I = 0x7f080af5

.field public static final ic_pop_pre_ocr_32px:I = 0x7f080af6

.field public static final ic_pop_pre_pdf_32px:I = 0x7f080af7

.field public static final ic_pop_pre_scan_32px:I = 0x7f080af8

.field public static final ic_pop_right_15_10px:I = 0x7f080af9

.field public static final ic_pop_ticket_282_132px:I = 0x7f080afa

.field public static final ic_popup_and:I = 0x7f080afb

.field public static final ic_popup_bg_green:I = 0x7f080afc

.field public static final ic_popup_close_24px:I = 0x7f080afd

.field public static final ic_popup_close_36px:I = 0x7f080afe

.field public static final ic_popup_close_member:I = 0x7f080aff

.field public static final ic_popup_discount_200_100px:I = 0x7f080b00

.field public static final ic_popup_id_32px:I = 0x7f080b01

.field public static final ic_popup_infiniteshare_32px:I = 0x7f080b02

.field public static final ic_popup_ocr_32px:I = 0x7f080b03

.field public static final ic_popup_pdf_32px:I = 0x7f080b04

.field public static final ic_popup_removeads_32px:I = 0x7f080b05

.field public static final ic_popup_removewatermark_32px:I = 0x7f080b06

.field public static final ic_popup_seal_32px:I = 0x7f080b07

.field public static final ic_popup_snow_1:I = 0x7f080b08

.field public static final ic_popup_snow_2:I = 0x7f080b09

.field public static final ic_popup_stars:I = 0x7f080b0a

.field public static final ic_popup_toword_32px:I = 0x7f080b0b

.field public static final ic_popup_vip:I = 0x7f080b0c

.field public static final ic_position_40px:I = 0x7f080b0d

.field public static final ic_post_pay_guide_complete:I = 0x7f080b0e

.field public static final ic_post_pay_guide_gift:I = 0x7f080b0f

.field public static final ic_ppt:I = 0x7f080b10

.field public static final ic_ppt_40dp:I = 0x7f080b11

.field public static final ic_ppt_doc:I = 0x7f080b12

.field public static final ic_ppt_thumb:I = 0x7f080b13

.field public static final ic_premium_arrow_down:I = 0x7f080b14

.field public static final ic_premiumvip_get_bg:I = 0x7f080b15

.field public static final ic_print_20:I = 0x7f080b16

.field public static final ic_print_cs_20px:I = 0x7f080b17

.field public static final ic_print_filter_bw:I = 0x7f080b18

.field public static final ic_print_filter_bw2:I = 0x7f080b19

.field public static final ic_print_filter_gray:I = 0x7f080b1a

.field public static final ic_print_filter_lighten:I = 0x7f080b1b

.field public static final ic_print_filter_magic:I = 0x7f080b1c

.field public static final ic_print_filter_original:I = 0x7f080b1d

.field public static final ic_print_guide:I = 0x7f080b1e

.field public static final ic_print_other_20px:I = 0x7f080b1f

.field public static final ic_print_paper:I = 0x7f080b20

.field public static final ic_print_white_24px:I = 0x7f080b21

.field public static final ic_printer:I = 0x7f080b22

.field public static final ic_printer_24px:I = 0x7f080b23

.field public static final ic_printer_32px:I = 0x7f080b24

.field public static final ic_printer_paper:I = 0x7f080b25

.field public static final ic_printer_photo:I = 0x7f080b26

.field public static final ic_printer_scan:I = 0x7f080b27

.field public static final ic_privilege_ad_vip_48px:I = 0x7f080b28

.field public static final ic_privilege_addfolder_vip_48px:I = 0x7f080b29

.field public static final ic_privilege_form_vip_48px:I = 0x7f080b2a

.field public static final ic_privilege_hdscan_vip_48px:I = 0x7f080b2b

.field public static final ic_privilege_idcard_vip_48px:I = 0x7f080b2c

.field public static final ic_privilege_imgedit_vip_48px:I = 0x7f080b2d

.field public static final ic_privilege_imgmerge_vip_48px:I = 0x7f080b2e

.field public static final ic_privilege_ocr_vip_48px:I = 0x7f080b2f

.field public static final ic_privilege_pdfcompress_vip_48px:I = 0x7f080b30

.field public static final ic_privilege_pdfextract_vip_48px:I = 0x7f080b31

.field public static final ic_privilege_pdflock_vip_48px:I = 0x7f080b32

.field public static final ic_privilege_pdfmerge_vip_48px:I = 0x7f080b33

.field public static final ic_privilege_pdfsign_vip_48px:I = 0x7f080b34

.field public static final ic_privilege_pdfwatermark_vip_48px:I = 0x7f080b35

.field public static final ic_privilege_scanbook_vip_48px:I = 0x7f080b36

.field public static final ic_privilege_scanidcard_vip_48px:I = 0x7f080b37

.field public static final ic_privilege_toppt_vip_48px:I = 0x7f080b38

.field public static final ic_privilege_toword_vip_48px:I = 0x7f080b39

.field public static final ic_privilege_translate_vip_48px:I = 0x7f080b3a

.field public static final ic_privilege_wrongtopic_vip_48px:I = 0x7f080b3b

.field public static final ic_progress_36px:I = 0x7f080b3c

.field public static final ic_progress_white:I = 0x7f080b3d

.field public static final ic_progress_white_32px:I = 0x7f080b3e

.field public static final ic_prompt_line_24px:I = 0x7f080b3f

.field public static final ic_proof_1:I = 0x7f080b40

.field public static final ic_proof_2:I = 0x7f080b41

.field public static final ic_proof_3:I = 0x7f080b42

.field public static final ic_proof_4:I = 0x7f080b43

.field public static final ic_proof_line_24px:I = 0x7f080b44

.field public static final ic_ps_detect_placeholder_48_48:I = 0x7f080b45

.field public static final ic_purchased:I = 0x7f080b46

.field public static final ic_pwd_eye_close:I = 0x7f080b47

.field public static final ic_pwd_eye_open:I = 0x7f080b48

.field public static final ic_qingquan:I = 0x7f080b49

.field public static final ic_qq_circle_44:I = 0x7f080b4a

.field public static final ic_qr_code:I = 0x7f080b4b

.field public static final ic_qr_code_16_16:I = 0x7f080b4c

.field public static final ic_qr_code_amazon:I = 0x7f080b4d

.field public static final ic_qr_code_ch:I = 0x7f080b4e

.field public static final ic_qr_code_ch_3:I = 0x7f080b4f

.field public static final ic_qr_code_en:I = 0x7f080b50

.field public static final ic_qr_code_en_3:I = 0x7f080b51

.field public static final ic_qr_code_expand:I = 0x7f080b52

.field public static final ic_qr_code_for_page:I = 0x7f080b53

.field public static final ic_qr_code_google:I = 0x7f080b54

.field public static final ic_qr_code_thumb:I = 0x7f080b55

.field public static final ic_question:I = 0x7f080b56

.field public static final ic_question_9c9c9c:I = 0x7f080b57

.field public static final ic_question_mark:I = 0x7f080b58

.field public static final ic_questionnaire_done_40px:I = 0x7f080b59

.field public static final ic_quick_login_close:I = 0x7f080b5a

.field public static final ic_re_take:I = 0x7f080b5b

.field public static final ic_recipt_a4_130_156px_ch:I = 0x7f080b5c

.field public static final ic_recipt_a4_130_156px_en:I = 0x7f080b5d

.field public static final ic_recommend_checked_20px:I = 0x7f080b5e

.field public static final ic_recommend_line_24px:I = 0x7f080b5f

.field public static final ic_recommend_unchecked_20px:I = 0x7f080b60

.field public static final ic_record_book_summary:I = 0x7f080b61

.field public static final ic_record_material:I = 0x7f080b62

.field public static final ic_recovery:I = 0x7f080b63

.field public static final ic_recovery_24px:I = 0x7f080b64

.field public static final ic_rectangle_business:I = 0x7f080b65

.field public static final ic_recycle:I = 0x7f080b66

.field public static final ic_recycle_scroll_thumb:I = 0x7f080b67

.field public static final ic_red_close:I = 0x7f080b68

.field public static final ic_red_dot_8:I = 0x7f080b69

.field public static final ic_red_marker_54_36px:I = 0x7f080b6a

.field public static final ic_red_open:I = 0x7f080b6b

.field public static final ic_red_subscript_40_24px:I = 0x7f080b6c

.field public static final ic_red_subscript_54_36px:I = 0x7f080b6d

.field public static final ic_red_warning:I = 0x7f080b6e

.field public static final ic_redo:I = 0x7f080b6f

.field public static final ic_refer_20px:I = 0x7f080b70

.field public static final ic_refresh:I = 0x7f080b71

.field public static final ic_refresh_12x12_blue:I = 0x7f080b72

.field public static final ic_refresh_48px:I = 0x7f080b73

.field public static final ic_register_guide:I = 0x7f080b74

.field public static final ic_rejoin_benefit_ad:I = 0x7f080b75

.field public static final ic_rejoin_benefit_composite:I = 0x7f080b76

.field public static final ic_rejoin_benefit_doc:I = 0x7f080b77

.field public static final ic_rejoin_benefit_excel:I = 0x7f080b78

.field public static final ic_rejoin_benefit_id:I = 0x7f080b79

.field public static final ic_rejoin_benefit_ocr:I = 0x7f080b7a

.field public static final ic_rejoin_benefit_pdf:I = 0x7f080b7b

.field public static final ic_rejoin_benefit_word:I = 0x7f080b7c

.field public static final ic_rejoin_eight_benefit_get:I = 0x7f080b7d

.field public static final ic_rejoin_eight_card:I = 0x7f080b7e

.field public static final ic_rejoin_eight_certificate:I = 0x7f080b7f

.field public static final ic_rejoin_eight_excel:I = 0x7f080b80

.field public static final ic_rejoin_eight_lock:I = 0x7f080b81

.field public static final ic_rejoin_eight_ocr:I = 0x7f080b82

.field public static final ic_rejoin_eight_ppt:I = 0x7f080b83

.field public static final ic_rejoin_eight_translate:I = 0x7f080b84

.field public static final ic_rejoin_eight_vip:I = 0x7f080b85

.field public static final ic_rejoin_eight_word:I = 0x7f080b86

.field public static final ic_rename_12_12:I = 0x7f080b87

.field public static final ic_rename_20px_green:I = 0x7f080b88

.field public static final ic_rename_line_24px:I = 0x7f080b89

.field public static final ic_rename_line_new_24px:I = 0x7f080b8a

.field public static final ic_reopen:I = 0x7f080b8b

.field public static final ic_repairphotos_24px:I = 0x7f080b8c

.field public static final ic_reset:I = 0x7f080b8d

.field public static final ic_resize_bar:I = 0x7f080b8e

.field public static final ic_retake:I = 0x7f080b8f

.field public static final ic_retake_invoice:I = 0x7f080b90

.field public static final ic_retake_picture:I = 0x7f080b91

.field public static final ic_return_24px:I = 0x7f080b92

.field public static final ic_return_line_24px:I = 0x7f080b93

.field public static final ic_return_line_white:I = 0x7f080b94

.field public static final ic_revise_seal:I = 0x7f080b95

.field public static final ic_revision_a:I = 0x7f080b96

.field public static final ic_revision_pen:I = 0x7f080b97

.field public static final ic_revision_signature:I = 0x7f080b98

.field public static final ic_revision_watermark:I = 0x7f080b99

.field public static final ic_reward_link:I = 0x7f080b9a

.field public static final ic_reward_sina_weibo:I = 0x7f080b9b

.field public static final ic_reward_topic:I = 0x7f080b9c

.field public static final ic_reward_transfer_word:I = 0x7f080b9d

.field public static final ic_reward_video:I = 0x7f080b9e

.field public static final ic_reward_vip:I = 0x7f080b9f

.field public static final ic_right_arrow:I = 0x7f080ba0

.field public static final ic_right_arrow_72:I = 0x7f080ba1

.field public static final ic_right_arrow_black:I = 0x7f080ba2

.field public static final ic_rightarrow_black_12:I = 0x7f080ba3

.field public static final ic_rights_cloud:I = 0x7f080ba4

.field public static final ic_rights_cloud_expired:I = 0x7f080ba5

.field public static final ic_rights_premiumvip:I = 0x7f080ba6

.field public static final ic_rights_vip:I = 0x7f080ba7

.field public static final ic_rights_vip_expired:I = 0x7f080ba8

.field public static final ic_rotate_24px:I = 0x7f080ba9

.field public static final ic_rotate_areq16:I = 0x7f080baa

.field public static final ic_rotate_bar:I = 0x7f080bab

.field public static final ic_rotate_left:I = 0x7f080bac

.field public static final ic_rotate_line_24px:I = 0x7f080bad

.field public static final ic_rotate_white:I = 0x7f080bae

.field public static final ic_round_rect:I = 0x7f080baf

.field public static final ic_save_line_24px:I = 0x7f080bb0

.field public static final ic_sc_create_folder_24:I = 0x7f080bb1

.field public static final ic_sc_multi_select_24:I = 0x7f080bb2

.field public static final ic_sc_viewmode_big_20:I = 0x7f080bb3

.field public static final ic_sc_viewmode_list_20:I = 0x7f080bb4

.field public static final ic_sc_viewmode_thumb_20:I = 0x7f080bb5

.field public static final ic_scan_bag_24px:I = 0x7f080bb6

.field public static final ic_scan_done:I = 0x7f080bb7

.field public static final ic_scan_done_arrow_right_blue:I = 0x7f080bb8

.field public static final ic_scan_done_task_monday_tag:I = 0x7f080bb9

.field public static final ic_scan_done_task_status_disable:I = 0x7f080bba

.field public static final ic_scan_done_task_status_normal:I = 0x7f080bbb

.field public static final ic_scan_done_task_union:I = 0x7f080bbc

.field public static final ic_scan_done_task_vip:I = 0x7f080bbd

.field public static final ic_scan_done_task_vip_disable:I = 0x7f080bbe

.field public static final ic_scan_done_task_vip_normal:I = 0x7f080bbf

.field public static final ic_scan_done_task_vip_received:I = 0x7f080bc0

.field public static final ic_scan_done_task_week_tag:I = 0x7f080bc1

.field public static final ic_scan_done_vip_month_receive_mask:I = 0x7f080bc2

.field public static final ic_scanagain_line_24px:I = 0x7f080bc3

.field public static final ic_scandone_air:I = 0x7f080bc4

.field public static final ic_scandone_email:I = 0x7f080bc5

.field public static final ic_scandone_idcard:I = 0x7f080bc6

.field public static final ic_scandone_img_24px:I = 0x7f080bc7

.field public static final ic_scandone_law:I = 0x7f080bc8

.field public static final ic_scandone_modify:I = 0x7f080bc9

.field public static final ic_scandone_more_dark_44:I = 0x7f080bca

.field public static final ic_scandone_more_light_44:I = 0x7f080bcb

.field public static final ic_scandone_no_synchronize:I = 0x7f080bcc

.field public static final ic_scandone_ocr:I = 0x7f080bcd

.field public static final ic_scandone_pdf:I = 0x7f080bce

.field public static final ic_scandone_pdf_dark_44:I = 0x7f080bcf

.field public static final ic_scandone_pdf_light_44:I = 0x7f080bd0

.field public static final ic_scandone_scan_24px:I = 0x7f080bd1

.field public static final ic_scandone_word:I = 0x7f080bd2

.field public static final ic_scandone_word_44:I = 0x7f080bd3

.field public static final ic_scanqr_line_24px:I = 0x7f080bd4

.field public static final ic_scantext_line_24px:I = 0x7f080bd5

.field public static final ic_screenshot_share_album:I = 0x7f080bd6

.field public static final ic_screenshot_share_pdf:I = 0x7f080bd7

.field public static final ic_screenshot_share_photo:I = 0x7f080bd8

.field public static final ic_screenshot_share_word:I = 0x7f080bd9

.field public static final ic_scroll_bar:I = 0x7f080bda

.field public static final ic_search:I = 0x7f080bdb

.field public static final ic_search_36px:I = 0x7f080bdc

.field public static final ic_search_all_hint:I = 0x7f080bdd

.field public static final ic_search_back:I = 0x7f080bde

.field public static final ic_search_black:I = 0x7f080bdf

.field public static final ic_search_black_24:I = 0x7f080be0

.field public static final ic_search_cancel:I = 0x7f080be1

.field public static final ic_search_clear:I = 0x7f080be2

.field public static final ic_search_close:I = 0x7f080be3

.field public static final ic_search_edittext_foucs:I = 0x7f080be4

.field public static final ic_search_edittext_normal:I = 0x7f080be5

.field public static final ic_search_gray:I = 0x7f080be6

.field public static final ic_search_history:I = 0x7f080be7

.field public static final ic_search_history_clear:I = 0x7f080be8

.field public static final ic_search_history_delete:I = 0x7f080be9

.field public static final ic_search_line_24px:I = 0x7f080bea

.field public static final ic_search_print:I = 0x7f080beb

.field public static final ic_search_referral_0:I = 0x7f080bec

.field public static final ic_search_referral_1:I = 0x7f080bed

.field public static final ic_search_referral_2:I = 0x7f080bee

.field public static final ic_search_referral_more:I = 0x7f080bef

.field public static final ic_search_small:I = 0x7f080bf0

.field public static final ic_select_all:I = 0x7f080bf1

.field public static final ic_select_black:I = 0x7f080bf2

.field public static final ic_select_color_brand:I = 0x7f080bf3

.field public static final ic_select_green:I = 0x7f080bf4

.field public static final ic_select_white:I = 0x7f080bf5

.field public static final ic_selected:I = 0x7f080bf6

.field public static final ic_selected_gold:I = 0x7f080bf7

.field public static final ic_sendtopc:I = 0x7f080bf8

.field public static final ic_sendtopc_areq22:I = 0x7f080bf9

.field public static final ic_sendtopc_areq22_red:I = 0x7f080bfa

.field public static final ic_sensorball:I = 0x7f080bfb

.field public static final ic_set_line_24px:I = 0x7f080bfc

.field public static final ic_setting_16x16:I = 0x7f080bfd

.field public static final ic_seve_40px:I = 0x7f080bfe

.field public static final ic_share:I = 0x7f080bff

.field public static final ic_share_24px_dark:I = 0x7f080c00

.field public static final ic_share_24px_light:I = 0x7f080c01

.field public static final ic_share_adr_video:I = 0x7f080c02

.field public static final ic_share_album_20px:I = 0x7f080c03

.field public static final ic_share_avatar:I = 0x7f080c04

.field public static final ic_share_copy_link:I = 0x7f080c05

.field public static final ic_share_copy_link_44:I = 0x7f080c06

.field public static final ic_share_copy_link_52:I = 0x7f080c07

.field public static final ic_share_copy_link_blue:I = 0x7f080c08

.field public static final ic_share_dingtalk:I = 0x7f080c09

.field public static final ic_share_dir_24px:I = 0x7f080c0a

.field public static final ic_share_dir_detail_24:I = 0x7f080c0b

.field public static final ic_share_dir_guide_dialog_class:I = 0x7f080c0c

.field public static final ic_share_dir_guide_dialog_normal:I = 0x7f080c0d

.field public static final ic_share_dir_guide_dialog_work:I = 0x7f080c0e

.field public static final ic_share_dir_new:I = 0x7f080c0f

.field public static final ic_share_dir_three:I = 0x7f080c10

.field public static final ic_share_dir_use_tip:I = 0x7f080c11

.field public static final ic_share_done_64px:I = 0x7f080c12

.field public static final ic_share_email:I = 0x7f080c13

.field public static final ic_share_email_24px:I = 0x7f080c14

.field public static final ic_share_email_24px_dark:I = 0x7f080c15

.field public static final ic_share_email_24x24:I = 0x7f080c16

.field public static final ic_share_email_52:I = 0x7f080c17

.field public static final ic_share_excel:I = 0x7f080c18

.field public static final ic_share_facebook:I = 0x7f080c19

.field public static final ic_share_facebook_44:I = 0x7f080c1a

.field public static final ic_share_facebook_52:I = 0x7f080c1b

.field public static final ic_share_feishu_52:I = 0x7f080c1c

.field public static final ic_share_googleplus:I = 0x7f080c1d

.field public static final ic_share_image_water_mark_top:I = 0x7f080c1e

.field public static final ic_share_img_56px:I = 0x7f080c1f

.field public static final ic_share_jpg:I = 0x7f080c20

.field public static final ic_share_jpg_album:I = 0x7f080c21

.field public static final ic_share_kakaotalk_52:I = 0x7f080c22

.field public static final ic_share_label_new:I = 0x7f080c23

.field public static final ic_share_line_24px:I = 0x7f080c24

.field public static final ic_share_link:I = 0x7f080c25

.field public static final ic_share_link_24px:I = 0x7f080c26

.field public static final ic_share_link_24px_dark:I = 0x7f080c27

.field public static final ic_share_link_pdf_card_title_more:I = 0x7f080c28

.field public static final ic_share_lock:I = 0x7f080c29

.field public static final ic_share_long_56px:I = 0x7f080c2a

.field public static final ic_share_longimage_new:I = 0x7f080c2b

.field public static final ic_share_mail:I = 0x7f080c2c

.field public static final ic_share_messenger_52:I = 0x7f080c2d

.field public static final ic_share_more:I = 0x7f080c2e

.field public static final ic_share_more_52:I = 0x7f080c2f

.field public static final ic_share_more_white:I = 0x7f080c30

.field public static final ic_share_ocr:I = 0x7f080c31

.field public static final ic_share_pdf:I = 0x7f080c32

.field public static final ic_share_pdf_56px:I = 0x7f080c33

.field public static final ic_share_pdf_link:I = 0x7f080c34

.field public static final ic_share_pdf_link_out:I = 0x7f080c35

.field public static final ic_share_pdf_new:I = 0x7f080c36

.field public static final ic_share_photo_link:I = 0x7f080c37

.field public static final ic_share_photo_link_out:I = 0x7f080c38

.field public static final ic_share_pop_hint:I = 0x7f080c39

.field public static final ic_share_pop_vip:I = 0x7f080c3a

.field public static final ic_share_printer_20:I = 0x7f080c3b

.field public static final ic_share_printer_24px_dark:I = 0x7f080c3c

.field public static final ic_share_qq:I = 0x7f080c3d

.field public static final ic_share_qq_52:I = 0x7f080c3e

.field public static final ic_share_qq_638:I = 0x7f080c3f

.field public static final ic_share_save_dcim:I = 0x7f080c40

.field public static final ic_share_send_to:I = 0x7f080c41

.field public static final ic_share_send_to_pc_24:I = 0x7f080c42

.field public static final ic_share_send_to_pc_52:I = 0x7f080c43

.field public static final ic_share_separated_pdf_link:I = 0x7f080c44

.field public static final ic_share_separated_pdf_link_out:I = 0x7f080c45

.field public static final ic_share_sms:I = 0x7f080c46

.field public static final ic_share_text_link:I = 0x7f080c47

.field public static final ic_share_text_link_out:I = 0x7f080c48

.field public static final ic_share_transfer_doc:I = 0x7f080c49

.field public static final ic_share_twitter:I = 0x7f080c4a

.field public static final ic_share_twitter_44:I = 0x7f080c4b

.field public static final ic_share_twitter_52:I = 0x7f080c4c

.field public static final ic_share_txt_20px:I = 0x7f080c4d

.field public static final ic_share_type_select:I = 0x7f080c4e

.field public static final ic_share_vk:I = 0x7f080c4f

.field public static final ic_share_vk_52:I = 0x7f080c50

.field public static final ic_share_wechat:I = 0x7f080c51

.field public static final ic_share_wechat_52:I = 0x7f080c52

.field public static final ic_share_wechat_638:I = 0x7f080c53

.field public static final ic_share_wechat_moments:I = 0x7f080c54

.field public static final ic_share_weibo:I = 0x7f080c55

.field public static final ic_share_wework:I = 0x7f080c56

.field public static final ic_share_whatsapp:I = 0x7f080c57

.field public static final ic_share_whatsapp_52:I = 0x7f080c58

.field public static final ic_share_word:I = 0x7f080c59

.field public static final ic_share_word_20px:I = 0x7f080c5a

.field public static final ic_share_word_56px:I = 0x7f080c5b

.field public static final ic_share_word_link:I = 0x7f080c5c

.field public static final ic_share_word_link_out:I = 0x7f080c5d

.field public static final ic_share_word_new:I = 0x7f080c5e

.field public static final ic_share_word_ocr:I = 0x7f080c5f

.field public static final ic_shoot_auto:I = 0x7f080c60

.field public static final ic_shoot_auto_refactor:I = 0x7f080c61

.field public static final ic_shoot_auto_small:I = 0x7f080c62

.field public static final ic_shoot_auto_small_48:I = 0x7f080c63

.field public static final ic_shoot_horizontal:I = 0x7f080c64

.field public static final ic_shoot_horizontal_refactor:I = 0x7f080c65

.field public static final ic_shoot_horizontal_small:I = 0x7f080c66

.field public static final ic_shoot_horizontal_small_48:I = 0x7f080c67

.field public static final ic_shoot_vertical:I = 0x7f080c68

.field public static final ic_shoot_vertical_refactor:I = 0x7f080c69

.field public static final ic_shoot_vertical_small:I = 0x7f080c6a

.field public static final ic_shoot_vertical_small_48:I = 0x7f080c6b

.field public static final ic_shootagain_line_24px:I = 0x7f080c6c

.field public static final ic_short_capture_to_word:I = 0x7f080c6d

.field public static final ic_short_multi_capture:I = 0x7f080c6e

.field public static final ic_short_single_capture:I = 0x7f080c6f

.field public static final ic_shortcut_scan:I = 0x7f080c70

.field public static final ic_show_ori:I = 0x7f080c71

.field public static final ic_show_trans:I = 0x7f080c72

.field public static final ic_sidecut_auto:I = 0x7f080c73

.field public static final ic_sidecut_manual:I = 0x7f080c74

.field public static final ic_sigature_line_24px:I = 0x7f080c75

.field public static final ic_sign_close:I = 0x7f080c76

.field public static final ic_sign_done:I = 0x7f080c77

.field public static final ic_sign_in_wechat:I = 0x7f080c78

.field public static final ic_sign_open:I = 0x7f080c79

.field public static final ic_sign_phone_cloud:I = 0x7f080c7a

.field public static final ic_sign_phone_equipment:I = 0x7f080c7b

.field public static final ic_sign_phone_message:I = 0x7f080c7c

.field public static final ic_sign_red_40:I = 0x7f080c7d

.field public static final ic_signature_24px_light:I = 0x7f080c7e

.field public static final ic_signature_date_24:I = 0x7f080c7f

.field public static final ic_signature_done:I = 0x7f080c80

.field public static final ic_signature_normal_24:I = 0x7f080c81

.field public static final ic_signature_spanning_seal_24:I = 0x7f080c82

.field public static final ic_signature_tab_guide_arrow:I = 0x7f080c83

.field public static final ic_signature_tmpl_delete:I = 0x7f080c84

.field public static final ic_signature_tmpl_delete_new:I = 0x7f080c85

.field public static final ic_single_doc:I = 0x7f080c86

.field public static final ic_single_page_selected:I = 0x7f080c87

.field public static final ic_single_page_unselected:I = 0x7f080c88

.field public static final ic_sizea3_line_24px:I = 0x7f080c89

.field public static final ic_sizea4_line_24px:I = 0x7f080c8a

.field public static final ic_sizea5_line_24px:I = 0x7f080c8b

.field public static final ic_sizeb4_line_24px:I = 0x7f080c8c

.field public static final ic_sizeb5_line_24px:I = 0x7f080c8d

.field public static final ic_skip_18px:I = 0x7f080c8e

.field public static final ic_smart_erase_close:I = 0x7f080c8f

.field public static final ic_smart_erase_crop_all:I = 0x7f080c90

.field public static final ic_smart_erase_guide:I = 0x7f080c91

.field public static final ic_smart_erase_placeholder_48_48:I = 0x7f080c92

.field public static final ic_smart_erase_scale:I = 0x7f080c93

.field public static final ic_smear_thickness:I = 0x7f080c94

.field public static final ic_smiling_face_36px:I = 0x7f080c95

.field public static final ic_sort1_line_24px:I = 0x7f080c96

.field public static final ic_sort_arrow_down:I = 0x7f080c97

.field public static final ic_sort_arrow_up:I = 0x7f080c98

.field public static final ic_sort_line_20px:I = 0x7f080c99

.field public static final ic_sort_line_24px:I = 0x7f080c9a

.field public static final ic_spdf_24px:I = 0x7f080c9b

.field public static final ic_squiggly:I = 0x7f080c9c

.field public static final ic_star:I = 0x7f080c9d

.field public static final ic_star_12_12:I = 0x7f080c9e

.field public static final ic_star_68px_12px:I = 0x7f080c9f

.field public static final ic_star_bright:I = 0x7f080ca0

.field public static final ic_star_gray:I = 0x7f080ca1

.field public static final ic_star_ring:I = 0x7f080ca2

.field public static final ic_star_yellow_100_16:I = 0x7f080ca3

.field public static final ic_stroke_bold:I = 0x7f080ca4

.field public static final ic_stroke_light:I = 0x7f080ca5

.field public static final ic_student_pitchon_24px:I = 0x7f080ca6

.field public static final ic_student_rule_24px:I = 0x7f080ca7

.field public static final ic_super_filter:I = 0x7f080ca8

.field public static final ic_super_vip:I = 0x7f080ca9

.field public static final ic_svg_nav_down_close:I = 0x7f080caa

.field public static final ic_svip_guide_done_2x:I = 0x7f080cab

.field public static final ic_svip_passive_pop_close:I = 0x7f080cac

.field public static final ic_svip_passive_pop_close_dark:I = 0x7f080cad

.field public static final ic_switch_sync_setting:I = 0x7f080cae

.field public static final ic_sync_arrow_down:I = 0x7f080caf

.field public static final ic_sync_fail_20:I = 0x7f080cb0

.field public static final ic_sync_open2:I = 0x7f080cb1

.field public static final ic_sync_syncing_20:I = 0x7f080cb2

.field public static final ic_sync_syncing_upgrade_20:I = 0x7f080cb3

.field public static final ic_sync_waiting_20:I = 0x7f080cb4

.field public static final ic_synchronous_line_24px:I = 0x7f080cb5

.field public static final ic_sys_print_default:I = 0x7f080cb6

.field public static final ic_system_message_44px:I = 0x7f080cb7

.field public static final ic_tab_doc_highlight_24px:I = 0x7f080cb8

.field public static final ic_tab_doc_normal_24px:I = 0x7f080cb9

.field public static final ic_tab_home_highlight_24px:I = 0x7f080cba

.field public static final ic_tab_home_normal_24px:I = 0x7f080cbb

.field public static final ic_tab_me_highlight_24px:I = 0x7f080cbc

.field public static final ic_tab_me_normal_24px:I = 0x7f080cbd

.field public static final ic_tab_scan_24px:I = 0x7f080cbe

.field public static final ic_tab_tools_highlight_24px:I = 0x7f080cbf

.field public static final ic_tab_tools_normal_24px:I = 0x7f080cc0

.field public static final ic_table:I = 0x7f080cc1

.field public static final ic_tag_free_video:I = 0x7f080cc2

.field public static final ic_tag_hd:I = 0x7f080cc3

.field public static final ic_tag_manager_add:I = 0x7f080cc4

.field public static final ic_tags_delete:I = 0x7f080cc5

.field public static final ic_take_photo_green_24_24:I = 0x7f080cc6

.field public static final ic_teacher_pitchon_24px:I = 0x7f080cc7

.field public static final ic_teacher_rule_24px:I = 0x7f080cc8

.field public static final ic_team_add_24px:I = 0x7f080cc9

.field public static final ic_team_folder_24px:I = 0x7f080cca

.field public static final ic_team_information_44px:I = 0x7f080ccb

.field public static final ic_team_introduce:I = 0x7f080ccc

.field public static final ic_team_people_24px:I = 0x7f080ccd

.field public static final ic_team_upgrade_24px:I = 0x7f080cce

.field public static final ic_template_line_24px:I = 0x7f080ccf

.field public static final ic_template_white:I = 0x7f080cd0

.field public static final ic_thinline_line_24px:I = 0x7f080cd1

.field public static final ic_thumb_state_lock_32:I = 0x7f080cd2

.field public static final ic_thumb_state_lock_new_32:I = 0x7f080cd3

.field public static final ic_thumb_state_unlock_32:I = 0x7f080cd4

.field public static final ic_tick_green:I = 0x7f080cd5

.field public static final ic_time:I = 0x7f080cd6

.field public static final ic_time_line_demo_tag:I = 0x7f080cd7

.field public static final ic_time_line_demo_tag_gp:I = 0x7f080cd8

.field public static final ic_time_line_more_copy:I = 0x7f080cd9

.field public static final ic_time_line_more_share:I = 0x7f080cda

.field public static final ic_time_line_new_doc:I = 0x7f080cdb

.field public static final ic_tips:I = 0x7f080cdc

.field public static final ic_tips_24px:I = 0x7f080cdd

.field public static final ic_tips_28px:I = 0x7f080cde

.field public static final ic_tips_bar_code:I = 0x7f080cdf

.field public static final ic_tips_doc_page_sort:I = 0x7f080ce0

.field public static final ic_tips_down_imagepage:I = 0x7f080ce1

.field public static final ic_tips_enhance_square:I = 0x7f080ce2

.field public static final ic_tips_highenhance:I = 0x7f080ce3

.field public static final ic_tips_link:I = 0x7f080ce4

.field public static final ic_tips_qrcode:I = 0x7f080ce5

.field public static final ic_tips_un:I = 0x7f080ce6

.field public static final ic_to_word_24px:I = 0x7f080ce7

.field public static final ic_tool_add_16_16:I = 0x7f080ce8

.field public static final ic_tool_box_36_36:I = 0x7f080ce9

.field public static final ic_tool_box_48_40:I = 0x7f080cea

.field public static final ic_tool_cancel_20_20:I = 0x7f080ceb

.field public static final ic_tool_logo_cspdf:I = 0x7f080cec

.field public static final ic_tool_page_from_gallery_wechat_clicked:I = 0x7f080ced

.field public static final ic_tool_save_gallery:I = 0x7f080cee

.field public static final ic_toolbar_print_doc:I = 0x7f080cef

.field public static final ic_toolbar_print_doc_for_more:I = 0x7f080cf0

.field public static final ic_toolbar_puzzle_24px:I = 0x7f080cf1

.field public static final ic_toolbar_upload_24px:I = 0x7f080cf2

.field public static final ic_tools_ad_close_16px:I = 0x7f080cf3

.field public static final ic_tools_ad_programs_16px:I = 0x7f080cf4

.field public static final ic_tools_backup:I = 0x7f080cf5

.field public static final ic_tools_bank_card_journal:I = 0x7f080cf6

.field public static final ic_tools_buy_device:I = 0x7f080cf7

.field public static final ic_tools_count_number:I = 0x7f080cf8

.field public static final ic_tools_from_gallery_36_36:I = 0x7f080cf9

.field public static final ic_tools_from_gallery_44px:I = 0x7f080cfa

.field public static final ic_tools_from_gallery_48_40:I = 0x7f080cfb

.field public static final ic_tools_from_gallery_wechat:I = 0x7f080cfc

.field public static final ic_tools_from_gallery_wechat_clicked:I = 0x7f080cfd

.field public static final ic_tools_id_photo_36_36:I = 0x7f080cfe

.field public static final ic_tools_id_photo_44px:I = 0x7f080cff

.field public static final ic_tools_id_photo_48_40:I = 0x7f080d00

.field public static final ic_tools_idcard_104px:I = 0x7f080d01

.field public static final ic_tools_idcard_36_36:I = 0x7f080d02

.field public static final ic_tools_idcard_44px:I = 0x7f080d03

.field public static final ic_tools_idcard_48_40:I = 0x7f080d04

.field public static final ic_tools_invoice:I = 0x7f080d05

.field public static final ic_tools_ocr_36_36:I = 0x7f080d06

.field public static final ic_tools_ocr_44px:I = 0x7f080d07

.field public static final ic_tools_ocr_48_40:I = 0x7f080d08

.field public static final ic_tools_oldphoto_36_36:I = 0x7f080d09

.field public static final ic_tools_oldphoto_44px:I = 0x7f080d0a

.field public static final ic_tools_oldphoto_48_40:I = 0x7f080d0b

.field public static final ic_tools_pdfadjust_36_36:I = 0x7f080d0c

.field public static final ic_tools_pdfadjust_44px:I = 0x7f080d0d

.field public static final ic_tools_pdfadjust_48_40:I = 0x7f080d0e

.field public static final ic_tools_pdfautograph_36_36:I = 0x7f080d0f

.field public static final ic_tools_pdfautograph_44px:I = 0x7f080d10

.field public static final ic_tools_pdfautograph_48_40:I = 0x7f080d11

.field public static final ic_tools_pdfencryption_36_36:I = 0x7f080d12

.field public static final ic_tools_pdfencryption_44px:I = 0x7f080d13

.field public static final ic_tools_pdfencryption_48_40:I = 0x7f080d14

.field public static final ic_tools_pdfextract_36_36:I = 0x7f080d15

.field public static final ic_tools_pdfextract_44px:I = 0x7f080d16

.field public static final ic_tools_pdfextract_48_40:I = 0x7f080d17

.field public static final ic_tools_pdfimport_36_36:I = 0x7f080d18

.field public static final ic_tools_pdfimport_44px:I = 0x7f080d19

.field public static final ic_tools_pdfimport_48_40:I = 0x7f080d1a

.field public static final ic_tools_pdfmerge_36_36:I = 0x7f080d1b

.field public static final ic_tools_pdfmerge_44px:I = 0x7f080d1c

.field public static final ic_tools_pdfmerge_48_40:I = 0x7f080d1d

.field public static final ic_tools_pdfwatermark_36_36:I = 0x7f080d1e

.field public static final ic_tools_pdfwatermark_44px:I = 0x7f080d1f

.field public static final ic_tools_pdfwatermark_48_40:I = 0x7f080d20

.field public static final ic_tools_printer:I = 0x7f080d21

.field public static final ic_tools_qrcode_44px:I = 0x7f080d22

.field public static final ic_tools_scan_translate:I = 0x7f080d23

.field public static final ic_tools_scan_wrongtopic_36_36:I = 0x7f080d24

.field public static final ic_tools_scan_wrongtopic_44px:I = 0x7f080d25

.field public static final ic_tools_scan_wrongtopic_48_40:I = 0x7f080d26

.field public static final ic_tools_scanbook_44px:I = 0x7f080d27

.field public static final ic_tools_scanbook_44px_kingkong:I = 0x7f080d28

.field public static final ic_tools_scanexcel_44px:I = 0x7f080d29

.field public static final ic_tools_scanppt_36_36:I = 0x7f080d2a

.field public static final ic_tools_scanppt_44px:I = 0x7f080d2b

.field public static final ic_tools_scanppt_48_40:I = 0x7f080d2c

.field public static final ic_tools_smart_erase:I = 0x7f080d2d

.field public static final ic_tools_toexcel_36_36:I = 0x7f080d2e

.field public static final ic_tools_toexcel_44px:I = 0x7f080d2f

.field public static final ic_tools_toexcel_48_40:I = 0x7f080d30

.field public static final ic_tools_toimage_36_36:I = 0x7f080d31

.field public static final ic_tools_toimage_44px:I = 0x7f080d32

.field public static final ic_tools_toimage_48_40:I = 0x7f080d33

.field public static final ic_tools_tolongimage_36_36:I = 0x7f080d34

.field public static final ic_tools_tolongimage_44px:I = 0x7f080d35

.field public static final ic_tools_tolongimage_48_40:I = 0x7f080d36

.field public static final ic_tools_toppt_36_36:I = 0x7f080d37

.field public static final ic_tools_toppt_44px:I = 0x7f080d38

.field public static final ic_tools_toppt_48_40:I = 0x7f080d39

.field public static final ic_tools_toword_36_36:I = 0x7f080d3a

.field public static final ic_tools_toword_44px:I = 0x7f080d3b

.field public static final ic_tools_toword_48_40:I = 0x7f080d3c

.field public static final ic_tools_workflow:I = 0x7f080d3d

.field public static final ic_tools_workflow2:I = 0x7f080d3e

.field public static final ic_top_nav_esc_24px:I = 0x7f080d3f

.field public static final ic_top_tab_cloud_24px:I = 0x7f080d40

.field public static final ic_topic_set_guide_ch:I = 0x7f080d41

.field public static final ic_trans_edit_image:I = 0x7f080d42

.field public static final ic_trans_lang_select_loading:I = 0x7f080d43

.field public static final ic_trans_new:I = 0x7f080d44

.field public static final ic_trans_new_white:I = 0x7f080d45

.field public static final ic_translate_44px:I = 0x7f080d46

.field public static final ic_translate_copy_grey:I = 0x7f080d47

.field public static final ic_translate_lan_exchange:I = 0x7f080d48

.field public static final ic_translate_lan_triangle:I = 0x7f080d49

.field public static final ic_translate_lang_select:I = 0x7f080d4a

.field public static final ic_translate_line_24px:I = 0x7f080d4b

.field public static final ic_translate_line_white_24px:I = 0x7f080d4c

.field public static final ic_translate_re_translate:I = 0x7f080d4d

.field public static final ic_translate_to_trans:I = 0x7f080d4e

.field public static final ic_translation:I = 0x7f080d4f

.field public static final ic_trial_renew_7d:I = 0x7f080d50

.field public static final ic_trial_renew_act_desc:I = 0x7f080d51

.field public static final ic_trial_renew_act_desc_new:I = 0x7f080d52

.field public static final ic_trial_renew_certificate_banner:I = 0x7f080d53

.field public static final ic_trial_renew_received:I = 0x7f080d54

.field public static final ic_trial_renew_top_banner:I = 0x7f080d55

.field public static final ic_trial_renew_top_title:I = 0x7f080d56

.field public static final ic_triangle_green:I = 0x7f080d57

.field public static final ic_triangle_green_18px:I = 0x7f080d58

.field public static final ic_turn_right_white_24px:I = 0x7f080d59

.field public static final ic_turnleft_line_24px:I = 0x7f080d5a

.field public static final ic_turnright_line_24px:I = 0x7f080d5b

.field public static final ic_turntable:I = 0x7f080d5c

.field public static final ic_txt_20px:I = 0x7f080d5d

.field public static final ic_txt_line_24px:I = 0x7f080d5e

.field public static final ic_un_change_color:I = 0x7f080d5f

.field public static final ic_unchecked_12dp:I = 0x7f080d60

.field public static final ic_undergraduate_pitchon_24px:I = 0x7f080d61

.field public static final ic_undergraduate_rule_24px:I = 0x7f080d62

.field public static final ic_underline:I = 0x7f080d63

.field public static final ic_undo:I = 0x7f080d64

.field public static final ic_unionpay_logo:I = 0x7f080d65

.field public static final ic_unknown:I = 0x7f080d66

.field public static final ic_up_arrow_green_18px:I = 0x7f080d67

.field public static final ic_upload:I = 0x7f080d68

.field public static final ic_upload_baiducloud:I = 0x7f080d69

.field public static final ic_upload_box:I = 0x7f080d6a

.field public static final ic_upload_box_24x24:I = 0x7f080d6b

.field public static final ic_upload_dropbox:I = 0x7f080d6c

.field public static final ic_upload_dropbox_24x24:I = 0x7f080d6d

.field public static final ic_upload_evernote:I = 0x7f080d6e

.field public static final ic_upload_evernote_24x24:I = 0x7f080d6f

.field public static final ic_upload_fail:I = 0x7f080d70

.field public static final ic_upload_googledrive:I = 0x7f080d71

.field public static final ic_upload_googledrive_24x24:I = 0x7f080d72

.field public static final ic_upload_line_24px:I = 0x7f080d73

.field public static final ic_upload_member:I = 0x7f080d74

.field public static final ic_upload_onedrive:I = 0x7f080d75

.field public static final ic_upload_onedrive_24x24:I = 0x7f080d76

.field public static final ic_upload_onenote:I = 0x7f080d77

.field public static final ic_upload_onenote_24x24:I = 0x7f080d78

.field public static final ic_upload_third_netdisk_24x24:I = 0x7f080d79

.field public static final ic_upward:I = 0x7f080d7a

.field public static final ic_user_back_extract_text:I = 0x7f080d7b

.field public static final ic_user_back_watermark:I = 0x7f080d7c

.field public static final ic_user_member:I = 0x7f080d7d

.field public static final ic_v48_ic_chose:I = 0x7f080d7e

.field public static final ic_verify:I = 0x7f080d7f

.field public static final ic_verify_done:I = 0x7f080d80

.field public static final ic_verify_screenshot:I = 0x7f080d81

.field public static final ic_viber:I = 0x7f080d82

.field public static final ic_view_cs_pdf:I = 0x7f080d83

.field public static final ic_view_list:I = 0x7f080d84

.field public static final ic_vip:I = 0x7f080d85

.field public static final ic_vip_10:I = 0x7f080d86

.field public static final ic_vip_10g:I = 0x7f080d87

.field public static final ic_vip_120_90px:I = 0x7f080d88

.field public static final ic_vip_16_32:I = 0x7f080d89

.field public static final ic_vip_18px:I = 0x7f080d8a

.field public static final ic_vip_20:I = 0x7f080d8b

.field public static final ic_vip_24px:I = 0x7f080d8c

.field public static final ic_vip_agree_choice:I = 0x7f080d8d

.field public static final ic_vip_agree_choice_normal:I = 0x7f080d8e

.field public static final ic_vip_agree_choice_normal_yellow:I = 0x7f080d8f

.field public static final ic_vip_agree_unchoice:I = 0x7f080d90

.field public static final ic_vip_blue_14px:I = 0x7f080d91

.field public static final ic_vip_book:I = 0x7f080d92

.field public static final ic_vip_buy_pop_close_agree:I = 0x7f080d93

.field public static final ic_vip_clouds:I = 0x7f080d94

.field public static final ic_vip_coin_50:I = 0x7f080d95

.field public static final ic_vip_collage:I = 0x7f080d96

.field public static final ic_vip_discount_44px:I = 0x7f080d97

.field public static final ic_vip_encryption:I = 0x7f080d98

.field public static final ic_vip_excel:I = 0x7f080d99

.field public static final ic_vip_folder_local:I = 0x7f080d9a

.field public static final ic_vip_folder_unlimited:I = 0x7f080d9b

.field public static final ic_vip_fullguide_close:I = 0x7f080d9c

.field public static final ic_vip_golden:I = 0x7f080d9d

.field public static final ic_vip_hd:I = 0x7f080d9e

.field public static final ic_vip_icon:I = 0x7f080d9f

.field public static final ic_vip_icon_139d90:I = 0x7f080da0

.field public static final ic_vip_icon_rejoin_benefit:I = 0x7f080da1

.field public static final ic_vip_id_photo:I = 0x7f080da2

.field public static final ic_vip_idmode:I = 0x7f080da3

.field public static final ic_vip_level_1:I = 0x7f080da4

.field public static final ic_vip_level_2:I = 0x7f080da5

.field public static final ic_vip_level_3:I = 0x7f080da6

.field public static final ic_vip_level_4:I = 0x7f080da7

.field public static final ic_vip_level_5:I = 0x7f080da8

.field public static final ic_vip_level_6:I = 0x7f080da9

.field public static final ic_vip_level_7:I = 0x7f080daa

.field public static final ic_vip_level_upgrade_close:I = 0x7f080dab

.field public static final ic_vip_month_id:I = 0x7f080dac

.field public static final ic_vip_month_ocr:I = 0x7f080dad

.field public static final ic_vip_month_pdf:I = 0x7f080dae

.field public static final ic_vip_month_promotion_close:I = 0x7f080daf

.field public static final ic_vip_month_promotion_discount_title_line_left:I = 0x7f080db0

.field public static final ic_vip_month_promotion_discount_title_line_right:I = 0x7f080db1

.field public static final ic_vip_month_promotion_func_arrow:I = 0x7f080db2

.field public static final ic_vip_month_promotion_lottie_default:I = 0x7f080db3

.field public static final ic_vip_month_promotion_trophy:I = 0x7f080db4

.field public static final ic_vip_month_scan_level_1:I = 0x7f080db5

.field public static final ic_vip_month_scan_level_2:I = 0x7f080db6

.field public static final ic_vip_month_scan_level_3:I = 0x7f080db7

.field public static final ic_vip_month_scan_level_4:I = 0x7f080db8

.field public static final ic_vip_month_scan_level_5:I = 0x7f080db9

.field public static final ic_vip_month_tips_bg:I = 0x7f080dba

.field public static final ic_vip_month_word:I = 0x7f080dbb

.field public static final ic_vip_noad:I = 0x7f080dbc

.field public static final ic_vip_ocr:I = 0x7f080dbd

.field public static final ic_vip_ocr_batch:I = 0x7f080dbe

.field public static final ic_vip_ocr_cloud:I = 0x7f080dbf

.field public static final ic_vip_ocrexport:I = 0x7f080dc0

.field public static final ic_vip_oninterest__share_copy:I = 0x7f080dc1

.field public static final ic_vip_oninterest__share_msg:I = 0x7f080dc2

.field public static final ic_vip_pdf:I = 0x7f080dc3

.field public static final ic_vip_pdf_download:I = 0x7f080dc4

.field public static final ic_vip_pdf_upload:I = 0x7f080dc5

.field public static final ic_vip_question_book:I = 0x7f080dc6

.field public static final ic_vip_share_encrypt:I = 0x7f080dc7

.field public static final ic_vip_signature:I = 0x7f080dc8

.field public static final ic_vip_svg:I = 0x7f080dc9

.field public static final ic_vip_translation:I = 0x7f080dca

.field public static final ic_vip_upgrade_gift_point:I = 0x7f080dcb

.field public static final ic_vip_upgrade_gift_qxb_vip:I = 0x7f080dcc

.field public static final ic_vip_upgrade_gift_received:I = 0x7f080dcd

.field public static final ic_vip_upgrade_gift_vip:I = 0x7f080dce

.field public static final ic_vip_upgrade_right_folder:I = 0x7f080dcf

.field public static final ic_vip_upgrade_right_id:I = 0x7f080dd0

.field public static final ic_vip_upgrade_right_moire:I = 0x7f080dd1

.field public static final ic_vip_upgrade_right_old_photo:I = 0x7f080dd2

.field public static final ic_vip_upgrade_right_sales:I = 0x7f080dd3

.field public static final ic_vip_upgrade_right_translate:I = 0x7f080dd4

.field public static final ic_vip_upgrade_right_word:I = 0x7f080dd5

.field public static final ic_vippop_vip_32px:I = 0x7f080dd6

.field public static final ic_vk_circle:I = 0x7f080dd7

.field public static final ic_wachat_fail:I = 0x7f080dd8

.field public static final ic_wachat_success:I = 0x7f080dd9

.field public static final ic_wait_uploading:I = 0x7f080dda

.field public static final ic_wallet_bankcard:I = 0x7f080ddb

.field public static final ic_warm:I = 0x7f080ddc

.field public static final ic_warning:I = 0x7f080ddd

.field public static final ic_warning_16_16:I = 0x7f080dde

.field public static final ic_watermark:I = 0x7f080ddf

.field public static final ic_watermark_add:I = 0x7f080de0

.field public static final ic_watermark_line_24px:I = 0x7f080de1

.field public static final ic_watermark_pop_close:I = 0x7f080de2

.field public static final ic_watermark_pop_video:I = 0x7f080de3

.field public static final ic_watermark_pop_video_no:I = 0x7f080de4

.field public static final ic_watermark_red_40:I = 0x7f080de5

.field public static final ic_watermark_remove:I = 0x7f080de6

.field public static final ic_web_ocr:I = 0x7f080de7

.field public static final ic_web_ocr2:I = 0x7f080de8

.field public static final ic_web_search:I = 0x7f080de9

.field public static final ic_website_areq22:I = 0x7f080dea

.field public static final ic_wechat:I = 0x7f080deb

.field public static final ic_wechat_24:I = 0x7f080dec

.field public static final ic_wechat_24_green:I = 0x7f080ded

.field public static final ic_wechat_44:I = 0x7f080dee

.field public static final ic_wechat_circle_44:I = 0x7f080def

.field public static final ic_wechat_doc_green_24_24:I = 0x7f080df0

.field public static final ic_wechat_pic_green_24_24:I = 0x7f080df1

.field public static final ic_wechat_timeline:I = 0x7f080df2

.field public static final ic_wechat_timeline_circle_44:I = 0x7f080df3

.field public static final ic_weixinpay_logo:I = 0x7f080df4

.field public static final ic_whatsapp:I = 0x7f080df5

.field public static final ic_whatsapp_24:I = 0x7f080df6

.field public static final ic_whatsapp_24_green:I = 0x7f080df7

.field public static final ic_whatsapp_busniess:I = 0x7f080df8

.field public static final ic_whatsapp_circle_44:I = 0x7f080df9

.field public static final ic_wheat_left_32_64:I = 0x7f080dfa

.field public static final ic_wheat_right_32_64:I = 0x7f080dfb

.field public static final ic_white_board_static:I = 0x7f080dfc

.field public static final ic_white_close:I = 0x7f080dfd

.field public static final ic_white_pad:I = 0x7f080dfe

.field public static final ic_white_pad_2:I = 0x7f080dff

.field public static final ic_white_pad_tip:I = 0x7f080e00

.field public static final ic_white_select:I = 0x7f080e01

.field public static final ic_widget_capture:I = 0x7f080e02

.field public static final ic_widget_capture_main_search:I = 0x7f080e03

.field public static final ic_widget_capture_multi_1_1:I = 0x7f080e04

.field public static final ic_widget_capture_multi_2_2:I = 0x7f080e05

.field public static final ic_widget_capture_qrcode_1_1:I = 0x7f080e06

.field public static final ic_widget_capture_qrcode_2_2:I = 0x7f080e07

.field public static final ic_widget_capture_single_1_1:I = 0x7f080e08

.field public static final ic_widget_capture_single_2_2:I = 0x7f080e09

.field public static final ic_widget_data_empty:I = 0x7f080e0a

.field public static final ic_widget_recent_doc_preview:I = 0x7f080e0b

.field public static final ic_widget_search_certificate:I = 0x7f080e0c

.field public static final ic_widget_search_multi:I = 0x7f080e0d

.field public static final ic_widget_search_ocr:I = 0x7f080e0e

.field public static final ic_widget_search_search:I = 0x7f080e0f

.field public static final ic_widget_search_single:I = 0x7f080e10

.field public static final ic_wifi:I = 0x7f080e11

.field public static final ic_wifi_20px:I = 0x7f080e12

.field public static final ic_withdraw:I = 0x7f080e13

.field public static final ic_withdraw_30:I = 0x7f080e14

.field public static final ic_word_20px:I = 0x7f080e15

.field public static final ic_word_24px:I = 0x7f080e16

.field public static final ic_word_complete:I = 0x7f080e17

.field public static final ic_word_convert_touch:I = 0x7f080e18

.field public static final ic_word_cs_list_bottom:I = 0x7f080e19

.field public static final ic_word_error_refresh:I = 0x7f080e1a

.field public static final ic_word_line_24px:I = 0x7f080e1b

.field public static final ic_word_list_item_add_page:I = 0x7f080e1c

.field public static final ic_word_list_item_re_identify:I = 0x7f080e1d

.field public static final ic_word_mark:I = 0x7f080e1e

.field public static final ic_word_mark_round_rect:I = 0x7f080e1f

.field public static final ic_word_recovery_24px:I = 0x7f080e20

.field public static final ic_word_red_40:I = 0x7f080e21

.field public static final ic_word_req3:I = 0x7f080e22

.field public static final ic_word_thumb:I = 0x7f080e23

.field public static final ic_word_withdraw_24px:I = 0x7f080e24

.field public static final ic_work_contract:I = 0x7f080e25

.field public static final ic_worker_pitchon_24px:I = 0x7f080e26

.field public static final ic_worker_rule_24px:I = 0x7f080e27

.field public static final ic_wrong_question_book:I = 0x7f080e28

.field public static final ic_x_gray_16px:I = 0x7f080e29

.field public static final ic_xmas_qipao_bg:I = 0x7f080e2a

.field public static final ic_yes:I = 0x7f080e2b

.field public static final ic_yinhao_down:I = 0x7f080e2c

.field public static final ic_yinhao_up:I = 0x7f080e2d

.field public static final ic_zalo:I = 0x7f080e2e

.field public static final ic_zd_10g:I = 0x7f080e2f

.field public static final ic_zd_ad:I = 0x7f080e30

.field public static final ic_zd_camera:I = 0x7f080e31

.field public static final ic_zd_ocr:I = 0x7f080e32

.field public static final ic_zd_pdf:I = 0x7f080e33

.field public static final icon:I = 0x7f080e34

.field public static final icon_add:I = 0x7f080e35

.field public static final icon_add_image:I = 0x7f080e36

.field public static final icon_ai_circle:I = 0x7f080e37

.field public static final icon_all_in_one_pdf:I = 0x7f080e38

.field public static final icon_back:I = 0x7f080e39

.field public static final icon_card_capture:I = 0x7f080e3a

.field public static final icon_card_manage:I = 0x7f080e3b

.field public static final icon_click_ocr:I = 0x7f080e3c

.field public static final icon_close_equity:I = 0x7f080e3d

.field public static final icon_continue_capture:I = 0x7f080e3e

.field public static final icon_convert_long_img_fail:I = 0x7f080e3f

.field public static final icon_convert_long_img_success:I = 0x7f080e40

.field public static final icon_convert_pdf_fail:I = 0x7f080e41

.field public static final icon_convert_pdf_success:I = 0x7f080e42

.field public static final icon_copy_link_new:I = 0x7f080e43

.field public static final icon_discount_purchase_clock:I = 0x7f080e44

.field public static final icon_discount_toword:I = 0x7f080e45

.field public static final icon_drive_license:I = 0x7f080e46

.field public static final icon_edit_clear:I = 0x7f080e47

.field public static final icon_esgin_import_system_files:I = 0x7f080e48

.field public static final icon_esign_brand:I = 0x7f080e49

.field public static final icon_extract_image_by_page_24px:I = 0x7f080e4a

.field public static final icon_family_register:I = 0x7f080e4b

.field public static final icon_file_scan_capture:I = 0x7f080e4c

.field public static final icon_file_uncheck_20px:I = 0x7f080e4d

.field public static final icon_foreign_other_card:I = 0x7f080e4e

.field public static final icon_home_listview:I = 0x7f080e4f

.field public static final icon_home_sort:I = 0x7f080e50

.field public static final icon_home_thumbview:I = 0x7f080e51

.field public static final icon_html_collect:I = 0x7f080e52

.field public static final icon_html_to_img:I = 0x7f080e53

.field public static final icon_html_to_pdf:I = 0x7f080e54

.field public static final icon_id_card:I = 0x7f080e55

.field public static final icon_item_select:I = 0x7f080e56

.field public static final icon_jiaobiao:I = 0x7f080e57

.field public static final icon_limit_enhance:I = 0x7f080e58

.field public static final icon_limit_enhance_light:I = 0x7f080e59

.field public static final icon_long_img_20px:I = 0x7f080e5a

.field public static final icon_looper_discount_purchase_clock:I = 0x7f080e5b

.field public static final icon_lr_para_close:I = 0x7f080e5c

.field public static final icon_more_black24px:I = 0x7f080e5d

.field public static final icon_more_horizontal:I = 0x7f080e5e

.field public static final icon_new:I = 0x7f080e5f

.field public static final icon_noti:I = 0x7f080e60

.field public static final icon_ocr_edit_result_save:I = 0x7f080e61

.field public static final icon_other_card:I = 0x7f080e62

.field public static final icon_page_list_capture_bar:I = 0x7f080e63

.field public static final icon_paper_capture:I = 0x7f080e64

.field public static final icon_passport_card:I = 0x7f080e65

.field public static final icon_pdf_cs_logo_cn:I = 0x7f080e66

.field public static final icon_pdf_cs_logo_en:I = 0x7f080e67

.field public static final icon_personal_info:I = 0x7f080e68

.field public static final icon_premium_red_round:I = 0x7f080e69

.field public static final icon_printer_buy_title:I = 0x7f080e6a

.field public static final icon_rectangle_590:I = 0x7f080e6b

.field public static final icon_rectangle_591:I = 0x7f080e6c

.field public static final icon_remove_pdf_water:I = 0x7f080e6d

.field public static final icon_right_arrow_20:I = 0x7f080e6e

.field public static final icon_save_success:I = 0x7f080e6f

.field public static final icon_scene_banner_function_next:I = 0x7f080e70

.field public static final icon_sliding_block_horizontal:I = 0x7f080e71

.field public static final icon_study_help_single_capture:I = 0x7f080e72

.field public static final icon_style3_selected:I = 0x7f080e73

.field public static final icon_tab_indicator:I = 0x7f080e74

.field public static final icon_type_all_doc:I = 0x7f080e75

.field public static final icon_type_camera:I = 0x7f080e76

.field public static final icon_type_excel:I = 0x7f080e77

.field public static final icon_type_pdf:I = 0x7f080e78

.field public static final icon_type_ppt:I = 0x7f080e79

.field public static final icon_type_upgrade_excel:I = 0x7f080e7a

.field public static final icon_type_upgrade_word:I = 0x7f080e7b

.field public static final icon_type_word:I = 0x7f080e7c

.field public static final icon_vehicle_license:I = 0x7f080e7d

.field public static final icon_view_excel:I = 0x7f080e7e

.field public static final icon_view_file:I = 0x7f080e7f

.field public static final icon_view_ppt:I = 0x7f080e80

.field public static final icon_view_word:I = 0x7f080e81

.field public static final icon_widget_add:I = 0x7f080e82

.field public static final icon_widget_set:I = 0x7f080e83

.field public static final icon_xin_chun_xiao_shi:I = 0x7f080e84

.field public static final im_paopao:I = 0x7f080e85

.field public static final im_shine:I = 0x7f080e86

.field public static final image_bankcard:I = 0x7f080e87

.field public static final image_collage_en:I = 0x7f080e88

.field public static final image_drive:I = 0x7f080e89

.field public static final image_edit_guide_star:I = 0x7f080e8a

.field public static final image_edit_guide_text_underline:I = 0x7f080e8b

.field public static final image_id:I = 0x7f080e8c

.field public static final image_id1s:I = 0x7f080e8d

.field public static final image_idcard:I = 0x7f080e8e

.field public static final image_idmode:I = 0x7f080e8f

.field public static final image_idmode_general:I = 0x7f080e90

.field public static final image_mission_idmode:I = 0x7f080e91

.field public static final image_mission_ocr:I = 0x7f080e92

.field public static final image_ocr_style_cn:I = 0x7f080e93

.field public static final image_ocr_style_en:I = 0x7f080e94

.field public static final image_pdftoword_ch:I = 0x7f080e95

.field public static final image_pdftoword_en:I = 0x7f080e96

.field public static final image_quality_tips_icon:I = 0x7f080e97

.field public static final image_recolor:I = 0x7f080e98

.field public static final image_sendtopc_areq22:I = 0x7f080e99

.field public static final image_signature_ch:I = 0x7f080e9a

.field public static final image_signature_en:I = 0x7f080e9b

.field public static final image_vehicle:I = 0x7f080e9c

.field public static final img1_375_358:I = 0x7f080e9d

.field public static final img1_after:I = 0x7f080e9e

.field public static final img2_375_358:I = 0x7f080e9f

.field public static final img2_before:I = 0x7f080ea0

.field public static final img3_375_600:I = 0x7f080ea1

.field public static final img3_after:I = 0x7f080ea2

.field public static final img_ac_1_img:I = 0x7f080ea3

.field public static final img_ac_2_img:I = 0x7f080ea4

.field public static final img_ac_ban:I = 0x7f080ea5

.field public static final img_ac_ban_1:I = 0x7f080ea6

.field public static final img_ac_ban_first_year1:I = 0x7f080ea7

.field public static final img_ac_ban_first_year2:I = 0x7f080ea8

.field public static final img_ac_img:I = 0x7f080ea9

.field public static final img_ac_img_2:I = 0x7f080eaa

.field public static final img_all_doc_permission_request_banner:I = 0x7f080eab

.field public static final img_allinall:I = 0x7f080eac

.field public static final img_android_home_activity_clock:I = 0x7f080ead

.field public static final img_android_home_close_24px:I = 0x7f080eae

.field public static final img_benefit_gift:I = 0x7f080eaf

.field public static final img_book_long_525:I = 0x7f080eb0

.field public static final img_btn_fristlabel_1:I = 0x7f080eb1

.field public static final img_btn_fristlabel_gray:I = 0x7f080eb2

.field public static final img_cloudspace_lock:I = 0x7f080eb3

.field public static final img_commodity_paper_48px:I = 0x7f080eb4

.field public static final img_commodity_printer_48px:I = 0x7f080eb5

.field public static final img_csgold1day_activation:I = 0x7f080eb6

.field public static final img_csgold1day_noactivation:I = 0x7f080eb7

.field public static final img_csgold1day_receive:I = 0x7f080eb8

.field public static final img_csgold3days_activation:I = 0x7f080eb9

.field public static final img_csgold3days_noactivation:I = 0x7f080eba

.field public static final img_csgold3days_receive:I = 0x7f080ebb

.field public static final img_csgold5days_activation:I = 0x7f080ebc

.field public static final img_csgold5days_noactivation:I = 0x7f080ebd

.field public static final img_csgold5days_receive:I = 0x7f080ebe

.field public static final img_csgold7days_activation:I = 0x7f080ebf

.field public static final img_csgold7days_noactivation:I = 0x7f080ec0

.field public static final img_csgold7days_receive:I = 0x7f080ec1

.field public static final img_csgoldcsicon_activation:I = 0x7f080ec2

.field public static final img_csgoldcsicon_noactivation:I = 0x7f080ec3

.field public static final img_csgoldcsicon_receive:I = 0x7f080ec4

.field public static final img_day7_rewardbg_choice:I = 0x7f080ec5

.field public static final img_day7_rewardbg_unchoice:I = 0x7f080ec6

.field public static final img_doc_first_280_140:I = 0x7f080ec7

.field public static final img_doc_first_banner_bg:I = 0x7f080ec8

.field public static final img_doc_first_lineleft_33_4:I = 0x7f080ec9

.field public static final img_doc_first_lineright_33_4:I = 0x7f080eca

.field public static final img_doc_import_empty:I = 0x7f080ecb

.field public static final img_doc_import_permission_arrow:I = 0x7f080ecc

.field public static final img_doc_import_permission_setting:I = 0x7f080ecd

.field public static final img_download_pop_cspdf:I = 0x7f080ece

.field public static final img_emptystatus_billing_60_60:I = 0x7f080ecf

.field public static final img_emptystatus_books_60_60:I = 0x7f080ed0

.field public static final img_emptystatus_certificates_60_60:I = 0x7f080ed1

.field public static final img_emptystatus_contract_60_60:I = 0x7f080ed2

.field public static final img_emptystatus_idcards_60_60:I = 0x7f080ed3

.field public static final img_emptystatus_letters_60_60:I = 0x7f080ed4

.field public static final img_emptystatus_medical_documents_60_60:I = 0x7f080ed5

.field public static final img_emptystatus_meetingminutes_60_60:I = 0x7f080ed6

.field public static final img_emptystatus_painting_60_60:I = 0x7f080ed7

.field public static final img_emptystatus_technical_60_60:I = 0x7f080ed8

.field public static final img_emptystatus_test_papers_60_60:I = 0x7f080ed9

.field public static final img_enhance_line:I = 0x7f080eda

.field public static final img_enhance_loading:I = 0x7f080edb

.field public static final img_exact:I = 0x7f080edc

.field public static final img_excel:I = 0x7f080edd

.field public static final img_fidelity:I = 0x7f080ede

.field public static final img_folder_briefcase_cn_128_96:I = 0x7f080edf

.field public static final img_folder_briefcase_us_128_96:I = 0x7f080ee0

.field public static final img_folder_growth_cn_128_96:I = 0x7f080ee1

.field public static final img_folder_growth_us_128_96:I = 0x7f080ee2

.field public static final img_folder_home_cn_128_96:I = 0x7f080ee3

.field public static final img_folder_idcards_cn_128_96:I = 0x7f080ee4

.field public static final img_folder_idcards_us_128_96:I = 0x7f080ee5

.field public static final img_folder_ideas_cn_128_96:I = 0x7f080ee6

.field public static final img_folder_lives_us_128_96:I = 0x7f080ee7

.field public static final img_folder_person_44_44:I = 0x7f080ee8

.field public static final img_folder_share_44_44:I = 0x7f080ee9

.field public static final img_folder_template_default_128_96:I = 0x7f080eea

.field public static final img_folder_work_file_128_96:I = 0x7f080eeb

.field public static final img_gradient:I = 0x7f080eec

.field public static final img_guide_1:I = 0x7f080eed

.field public static final img_guide_1_560px:I = 0x7f080eee

.field public static final img_guide_1_560px_cn:I = 0x7f080eef

.field public static final img_guide_2:I = 0x7f080ef0

.field public static final img_guide_2_560px:I = 0x7f080ef1

.field public static final img_guide_2_560px_cn:I = 0x7f080ef2

.field public static final img_guide_3:I = 0x7f080ef3

.field public static final img_guide_3_560px:I = 0x7f080ef4

.field public static final img_guide_3_560px_cn:I = 0x7f080ef5

.field public static final img_guide_4_560px:I = 0x7f080ef6

.field public static final img_guide_4_560px_cn:I = 0x7f080ef7

.field public static final img_handy:I = 0x7f080ef8

.field public static final img_home_banner1_bg:I = 0x7f080ef9

.field public static final img_home_iqiyi_pop_newgift:I = 0x7f080efa

.field public static final img_home_nodoc:I = 0x7f080efb

.field public static final img_imagetotext_activation:I = 0x7f080efc

.field public static final img_imagetotext_noactivation:I = 0x7f080efd

.field public static final img_imagetotext_receive:I = 0x7f080efe

.field public static final img_kong_large:I = 0x7f080eff

.field public static final img_kong_small:I = 0x7f080f00

.field public static final img_label_vip:I = 0x7f080f01

.field public static final img_lead_idcards_240_126:I = 0x7f080f02

.field public static final img_learn_idscan:I = 0x7f080f03

.field public static final img_learn_notescan:I = 0x7f080f04

.field public static final img_life_billscan:I = 0x7f080f05

.field public static final img_loading_failed_100px:I = 0x7f080f06

.field public static final img_me_page_header_bg:I = 0x7f080f07

.field public static final img_me_page_vip_texture_1:I = 0x7f080f08

.field public static final img_me_page_vip_texture_2:I = 0x7f080f09

.field public static final img_minute:I = 0x7f080f0a

.field public static final img_moire:I = 0x7f080f0b

.field public static final img_newbie_scan_reward_bg:I = 0x7f080f0c

.field public static final img_no_information_100px:I = 0x7f080f0d

.field public static final img_o_vip_upgrade_arrow:I = 0x7f080f0e

.field public static final img_ocr_upgrade_vip:I = 0x7f080f0f

.field public static final img_onetry_pop_step_new:I = 0x7f080f10

.field public static final img_pay_bg:I = 0x7f080f11

.field public static final img_paycheck_label:I = 0x7f080f12

.field public static final img_pdftoword_activation:I = 0x7f080f13

.field public static final img_pdftoword_noactivation:I = 0x7f080f14

.field public static final img_pdftoword_receive:I = 0x7f080f15

.field public static final img_pop_gold_free:I = 0x7f080f16

.field public static final img_pop_pdftips_290_120:I = 0x7f080f17

.field public static final img_pop_pdftips_gp_290_120:I = 0x7f080f18

.field public static final img_pop_premium_free:I = 0x7f080f19

.field public static final img_pop_premium_free_red:I = 0x7f080f1a

.field public static final img_pop_vip_cn:I = 0x7f080f1b

.field public static final img_popup_discountfigure:I = 0x7f080f1c

.field public static final img_popup_firstimg_decoration:I = 0x7f080f1d

.field public static final img_popup_firstimg_titleimg:I = 0x7f080f1e

.field public static final img_popup_firsttext:I = 0x7f080f1f

.field public static final img_popup_rate_guide:I = 0x7f080f20

.field public static final img_popup_title:I = 0x7f080f21

.field public static final img_popup_topimg:I = 0x7f080f22

.field public static final img_popupbg_decoration:I = 0x7f080f23

.field public static final img_ppt:I = 0x7f080f24

.field public static final img_printer_quyin:I = 0x7f080f25

.field public static final img_recall_dialog_bg:I = 0x7f080f26

.field public static final img_receive_success:I = 0x7f080f27

.field public static final img_rejoin_benefit_btn_bg:I = 0x7f080f28

.field public static final img_rejoin_benefit_content_bg:I = 0x7f080f29

.field public static final img_rejoin_benefit_dialog_bg:I = 0x7f080f2a

.field public static final img_rejoin_benefit_title_left:I = 0x7f080f2b

.field public static final img_rejoin_benefit_title_right:I = 0x7f080f2c

.field public static final img_remove_free_banner_first_cn:I = 0x7f080f2d

.field public static final img_remove_free_banner_first_gp:I = 0x7f080f2e

.field public static final img_remove_free_mark:I = 0x7f080f2f

.field public static final img_rewardbg1_activation:I = 0x7f080f30

.field public static final img_rewardbg1_noactivation:I = 0x7f080f31

.field public static final img_rewardbg_activation:I = 0x7f080f32

.field public static final img_rewardbg_noactivation:I = 0x7f080f33

.field public static final img_round_arrow:I = 0x7f080f34

.field public static final img_safari:I = 0x7f080f35

.field public static final img_scan:I = 0x7f080f36

.field public static final img_scan_done_task_banner_bg:I = 0x7f080f37

.field public static final img_scan_done_vip_month_card_1_bg:I = 0x7f080f38

.field public static final img_scan_done_vip_month_red_envelope:I = 0x7f080f39

.field public static final img_scan_done_vip_month_title:I = 0x7f080f3a

.field public static final img_scan_done_vip_task_toast:I = 0x7f080f3b

.field public static final img_scan_pc:I = 0x7f080f3c

.field public static final img_share_adr_tip:I = 0x7f080f3d

.field public static final img_share_dir_guide:I = 0x7f080f3e

.field public static final img_share_done_vip_month_cs:I = 0x7f080f3f

.field public static final img_share_done_vip_month_purchase_bg:I = 0x7f080f40

.field public static final img_share_done_vip_month_shadow:I = 0x7f080f41

.field public static final img_share_done_vip_month_texture_1:I = 0x7f080f42

.field public static final img_share_done_vip_month_texture_2:I = 0x7f080f43

.field public static final img_share_done_vip_month_title_bg:I = 0x7f080f44

.field public static final img_share_done_vip_month_vip_card:I = 0x7f080f45

.field public static final img_share_done_vip_month_vip_light:I = 0x7f080f46

.field public static final img_sharewithme:I = 0x7f080f47

.field public static final img_sharp:I = 0x7f080f48

.field public static final img_single_doc:I = 0x7f080f49

.field public static final img_thumb_error_104:I = 0x7f080f4a

.field public static final img_tidy:I = 0x7f080f4b

.field public static final img_titleimg_1:I = 0x7f080f4c

.field public static final img_titleimg_2:I = 0x7f080f4d

.field public static final img_titleimg_3:I = 0x7f080f4e

.field public static final img_titleimg_4:I = 0x7f080f4f

.field public static final img_vip10days_noactivation:I = 0x7f080f50

.field public static final img_vip1year_noactivation:I = 0x7f080f51

.field public static final img_vip2days_activation:I = 0x7f080f52

.field public static final img_vip2days_noactivation:I = 0x7f080f53

.field public static final img_vip2days_receive:I = 0x7f080f54

.field public static final img_vip30days_noactivation:I = 0x7f080f55

.field public static final img_vip3csicon_receive:I = 0x7f080f56

.field public static final img_vip3daycsicon_activation:I = 0x7f080f57

.field public static final img_vip3daycsicon_noactivation:I = 0x7f080f58

.field public static final img_vip3days_activation:I = 0x7f080f59

.field public static final img_vip3days_noactivation:I = 0x7f080f5a

.field public static final img_vip3days_receive:I = 0x7f080f5b

.field public static final img_vip5days_activation:I = 0x7f080f5c

.field public static final img_vip5days_noactivation:I = 0x7f080f5d

.field public static final img_vip5days_receive:I = 0x7f080f5e

.field public static final img_vip7days_activation:I = 0x7f080f5f

.field public static final img_vip7days_noactivation:I = 0x7f080f60

.field public static final img_vip7days_receive:I = 0x7f080f61

.field public static final img_vip_activation:I = 0x7f080f62

.field public static final img_vip_month_promotion_animate_bg:I = 0x7f080f63

.field public static final img_vip_month_promotion_animate_title:I = 0x7f080f64

.field public static final img_vip_month_promotion_bg_1:I = 0x7f080f65

.field public static final img_vip_month_promotion_book:I = 0x7f080f66

.field public static final img_vip_month_promotion_calculate_bg:I = 0x7f080f67

.field public static final img_vip_month_promotion_discount_arrow:I = 0x7f080f68

.field public static final img_vip_month_promotion_discount_line:I = 0x7f080f69

.field public static final img_vip_month_promotion_red_envelope:I = 0x7f080f6a

.field public static final img_vip_month_promotion_top:I = 0x7f080f6b

.field public static final img_vip_month_promotion_top_banner_3:I = 0x7f080f6c

.field public static final img_vip_month_promotion_top_bg:I = 0x7f080f6d

.field public static final img_vip_month_promotion_top_bg_3:I = 0x7f080f6e

.field public static final img_vip_month_promotion_vip_card_mask:I = 0x7f080f6f

.field public static final img_vip_noactivation:I = 0x7f080f70

.field public static final img_vip_open_158_72:I = 0x7f080f71

.field public static final img_vip_receive:I = 0x7f080f72

.field public static final img_vip_upgrade_left_light:I = 0x7f080f73

.field public static final img_vip_upgrade_right_light:I = 0x7f080f74

.field public static final img_vip_upgrade_star_bg:I = 0x7f080f75

.field public static final img_vipcsicon_activation:I = 0x7f080f76

.field public static final img_vipcsicon_noactivation:I = 0x7f080f77

.field public static final img_vipcsicon_receive:I = 0x7f080f78

.field public static final img_watermark:I = 0x7f080f79

.field public static final img_watermark_1:I = 0x7f080f7a

.field public static final img_wifi_100px:I = 0x7f080f7b

.field public static final img_work_quickscan:I = 0x7f080f7c

.field public static final indexable_bg_center_overlay:I = 0x7f080f7d

.field public static final indexable_bg_md_overlay:I = 0x7f080f7e

.field public static final indicator_cs_list:I = 0x7f080f7f

.field public static final indicator_selector_golden_e8bc72:I = 0x7f080f80

.field public static final indicator_selector_gray_dark:I = 0x7f080f81

.field public static final indicator_selector_red_ff6748:I = 0x7f080f82

.field public static final indicator_selector_white_drop_cnl:I = 0x7f080f83

.field public static final indicator_share_app_selector:I = 0x7f080f84

.field public static final ink_bg_touch_background:I = 0x7f080f85

.field public static final ink_ic_eraser_select:I = 0x7f080f86

.field public static final ink_ic_eraser_selector:I = 0x7f080f87

.field public static final ink_ic_eraser_unselect:I = 0x7f080f88

.field public static final ink_ic_hight_light_pen_select:I = 0x7f080f89

.field public static final ink_ic_hight_light_pen_selector:I = 0x7f080f8a

.field public static final ink_ic_hight_light_pen_unselect:I = 0x7f080f8b

.field public static final ink_ic_mark_pen_select:I = 0x7f080f8c

.field public static final ink_ic_mark_pen_selector:I = 0x7f080f8d

.field public static final ink_ic_mark_pen_unselect:I = 0x7f080f8e

.field public static final ink_note_bg_scale_indicator:I = 0x7f080f8f

.field public static final ink_note_horizontal_scrollbar:I = 0x7f080f90

.field public static final ink_note_overscroll_edge:I = 0x7f080f91

.field public static final ink_note_overscroll_glow:I = 0x7f080f92

.field public static final ink_note_progress_loading:I = 0x7f080f93

.field public static final ink_note_scrollbar_handle_horizontal:I = 0x7f080f94

.field public static final ink_note_scrollbar_handle_horizontal_pressed:I = 0x7f080f95

.field public static final ink_note_scrollbar_handle_vertical:I = 0x7f080f96

.field public static final ink_note_scrollbar_handle_vertical_pressed:I = 0x7f080f97

.field public static final ink_note_seekbar_thumb:I = 0x7f080f98

.field public static final ink_note_spinner_custom_progress:I = 0x7f080f99

.field public static final ink_note_vertical_scrollbar:I = 0x7f080f9a

.field public static final ink_pen_size_indicator:I = 0x7f080f9b

.field public static final item_background_dark:I = 0x7f080f9c

.field public static final item_background_light:I = 0x7f080f9d

.field public static final item_divider_more_dialog:I = 0x7f080f9e

.field public static final item_grid_autoupload_bg:I = 0x7f080f9f

.field public static final item_grid_selector_autoupload:I = 0x7f080fa0

.field public static final item_pur_selector:I = 0x7f080fa1

.field public static final iv_esign_guide_mainpic:I = 0x7f080fa2

.field public static final iv_esign_wxmin_home:I = 0x7f080fa3

.field public static final iv_esign_wxmin_invite:I = 0x7f080fa4

.field public static final iv_esign_wxmin_link:I = 0x7f080fa5

.field public static final iv_preview_image_scan_light:I = 0x7f080fa6

.field public static final iv_sync_state_finished:I = 0x7f080fa7

.field public static final jigsaw_template_bound:I = 0x7f080fa8

.field public static final list_item_divider_member:I = 0x7f080fa9

.field public static final list_preference_selector_bg:I = 0x7f080faa

.field public static final list_selector_bg:I = 0x7f080fab

.field public static final list_selector_bg_both_design:I = 0x7f080fac

.field public static final list_selector_bg_corner_4:I = 0x7f080fad

.field public static final list_selector_bg_search_matched:I = 0x7f080fae

.field public static final list_selector_default:I = 0x7f080faf

.field public static final list_selector_default_bg_color_1:I = 0x7f080fb0

.field public static final list_selector_default_borderless:I = 0x7f080fb1

.field public static final list_selector_default_new:I = 0x7f080fb2

.field public static final list_selector_default_new_light:I = 0x7f080fb3

.field public static final list_selector_gray:I = 0x7f080fb4

.field public static final lock_mask:I = 0x7f080fb5

.field public static final logo_ad_applaunch:I = 0x7f080fb6

.field public static final logo_ad_launch:I = 0x7f080fb7

.field public static final logo_buy:I = 0x7f080fb8

.field public static final logo_chs:I = 0x7f080fb9

.field public static final logo_en:I = 0x7f080fba

.field public static final logo_start_page:I = 0x7f080fbb

.field public static final m3_appbar_background:I = 0x7f080fbc

.field public static final m3_avd_hide_password:I = 0x7f080fbd

.field public static final m3_avd_show_password:I = 0x7f080fbe

.field public static final m3_password_eye:I = 0x7f080fbf

.field public static final m3_popupmenu_background_overlay:I = 0x7f080fc0

.field public static final m3_radiobutton_ripple:I = 0x7f080fc1

.field public static final m3_selection_control_ripple:I = 0x7f080fc2

.field public static final m3_tabs_background:I = 0x7f080fc3

.field public static final m3_tabs_line_indicator:I = 0x7f080fc4

.field public static final m3_tabs_rounded_line_indicator:I = 0x7f080fc5

.field public static final m3_tabs_transparent_background:I = 0x7f080fc6

.field public static final magnifier:I = 0x7f080fc7

.field public static final magnifier_new:I = 0x7f080fc8

.field public static final maindoc_list_search_tips_image_title:I = 0x7f080fc9

.field public static final mask_card_detail:I = 0x7f080fca

.field public static final material_cursor_drawable:I = 0x7f080fcb

.field public static final material_ic_calendar_black_24dp:I = 0x7f080fcc

.field public static final material_ic_clear_black_24dp:I = 0x7f080fcd

.field public static final material_ic_edit_black_24dp:I = 0x7f080fce

.field public static final material_ic_keyboard_arrow_left_black_24dp:I = 0x7f080fcf

.field public static final material_ic_keyboard_arrow_next_black_24dp:I = 0x7f080fd0

.field public static final material_ic_keyboard_arrow_previous_black_24dp:I = 0x7f080fd1

.field public static final material_ic_keyboard_arrow_right_black_24dp:I = 0x7f080fd2

.field public static final material_ic_menu_arrow_down_black_24dp:I = 0x7f080fd3

.field public static final material_ic_menu_arrow_up_black_24dp:I = 0x7f080fd4

.field public static final me_cloud_ad_free_12:I = 0x7f080fd5

.field public static final modify_style:I = 0x7f080fd6

.field public static final mtrl_bottomsheet_drag_handle:I = 0x7f080fd7

.field public static final mtrl_checkbox_button:I = 0x7f080fd8

.field public static final mtrl_checkbox_button_checked_unchecked:I = 0x7f080fd9

.field public static final mtrl_checkbox_button_icon:I = 0x7f080fda

.field public static final mtrl_checkbox_button_icon_checked_indeterminate:I = 0x7f080fdb

.field public static final mtrl_checkbox_button_icon_checked_unchecked:I = 0x7f080fdc

.field public static final mtrl_checkbox_button_icon_indeterminate_checked:I = 0x7f080fdd

.field public static final mtrl_checkbox_button_icon_indeterminate_unchecked:I = 0x7f080fde

.field public static final mtrl_checkbox_button_icon_unchecked_checked:I = 0x7f080fdf

.field public static final mtrl_checkbox_button_icon_unchecked_indeterminate:I = 0x7f080fe0

.field public static final mtrl_checkbox_button_unchecked_checked:I = 0x7f080fe1

.field public static final mtrl_dialog_background:I = 0x7f080fe2

.field public static final mtrl_dropdown_arrow:I = 0x7f080fe3

.field public static final mtrl_ic_arrow_drop_down:I = 0x7f080fe4

.field public static final mtrl_ic_arrow_drop_up:I = 0x7f080fe5

.field public static final mtrl_ic_cancel:I = 0x7f080fe6

.field public static final mtrl_ic_check_mark:I = 0x7f080fe7

.field public static final mtrl_ic_checkbox_checked:I = 0x7f080fe8

.field public static final mtrl_ic_checkbox_unchecked:I = 0x7f080fe9

.field public static final mtrl_ic_error:I = 0x7f080fea

.field public static final mtrl_ic_indeterminate:I = 0x7f080feb

.field public static final mtrl_navigation_bar_item_background:I = 0x7f080fec

.field public static final mtrl_popupmenu_background:I = 0x7f080fed

.field public static final mtrl_popupmenu_background_overlay:I = 0x7f080fee

.field public static final mtrl_switch_thumb:I = 0x7f080fef

.field public static final mtrl_switch_thumb_checked:I = 0x7f080ff0

.field public static final mtrl_switch_thumb_checked_pressed:I = 0x7f080ff1

.field public static final mtrl_switch_thumb_checked_unchecked:I = 0x7f080ff2

.field public static final mtrl_switch_thumb_pressed:I = 0x7f080ff3

.field public static final mtrl_switch_thumb_pressed_checked:I = 0x7f080ff4

.field public static final mtrl_switch_thumb_pressed_unchecked:I = 0x7f080ff5

.field public static final mtrl_switch_thumb_unchecked:I = 0x7f080ff6

.field public static final mtrl_switch_thumb_unchecked_checked:I = 0x7f080ff7

.field public static final mtrl_switch_thumb_unchecked_pressed:I = 0x7f080ff8

.field public static final mtrl_switch_track:I = 0x7f080ff9

.field public static final mtrl_switch_track_decoration:I = 0x7f080ffa

.field public static final mtrl_tabs_default_indicator:I = 0x7f080ffb

.field public static final navigation_empty_icon:I = 0x7f080ffc

.field public static final new_year_discount_guide:I = 0x7f080ffd

.field public static final newpage_detail_indicator:I = 0x7f080ffe

.field public static final newsign_home_head:I = 0x7f080fff

.field public static final newsign_home_head_icon:I = 0x7f081000

.field public static final no_ad:I = 0x7f081001

.field public static final notification_action_background:I = 0x7f081002

.field public static final notification_bg:I = 0x7f081003

.field public static final notification_bg_low:I = 0x7f081004

.field public static final notification_bg_low_normal:I = 0x7f081005

.field public static final notification_bg_low_pressed:I = 0x7f081006

.field public static final notification_bg_normal:I = 0x7f081007

.field public static final notification_bg_normal_pressed:I = 0x7f081008

.field public static final notification_icon_background:I = 0x7f081009

.field public static final notification_icon_black:I = 0x7f08100a

.field public static final notification_template_icon_bg:I = 0x7f08100b

.field public static final notification_template_icon_low_bg:I = 0x7f08100c

.field public static final notification_tile_bg:I = 0x7f08100d

.field public static final notify_panel_notification_icon_bg:I = 0x7f08100e

.field public static final ocr_anim:I = 0x7f08100f

.field public static final ocr_illustration:I = 0x7f081010

.field public static final ocr_result_export:I = 0x7f081011

.field public static final ocr_result_title_bar_background:I = 0x7f081012

.field public static final office_preview_more:I = 0x7f081013

.field public static final office_preview_ppt_play:I = 0x7f081014

.field public static final office_share_excel:I = 0x7f081015

.field public static final office_share_pdf:I = 0x7f081016

.field public static final office_share_ppt:I = 0x7f081017

.field public static final office_share_word:I = 0x7f081018

.field public static final old_bw_en_1:I = 0x7f081019

.field public static final old_en_1:I = 0x7f08101a

.field public static final old_en_2:I = 0x7f08101b

.field public static final old_en_3:I = 0x7f08101c

.field public static final on_screen_hint_frame:I = 0x7f08101d

.field public static final one_login_btn_pressed:I = 0x7f08101e

.field public static final one_login_btn_unabled:I = 0x7f08101f

.field public static final one_login_btn_unpressed:I = 0x7f081020

.field public static final one_login_checked:I = 0x7f081021

.field public static final one_login_unchecked:I = 0x7f081022

.field public static final operation_arrow_right:I = 0x7f081023

.field public static final page_detail_indicator:I = 0x7f081024

.field public static final page_num_frame:I = 0x7f081025

.field public static final page_numbers:I = 0x7f081026

.field public static final pagelist_new_img_placeholder:I = 0x7f081027

.field public static final pb_me_page_blue:I = 0x7f081028

.field public static final pb_me_page_gold:I = 0x7f081029

.field public static final pb_me_page_normal:I = 0x7f08102a

.field public static final pb_me_page_normal_over_70:I = 0x7f08102b

.field public static final pb_me_page_red:I = 0x7f08102c

.field public static final pb_me_page_red_full:I = 0x7f08102d

.field public static final pb_me_page_vip_level_value_6:I = 0x7f08102e

.field public static final pb_me_page_vip_level_value_7:I = 0x7f08102f

.field public static final pb_me_page_vip_level_value_normal:I = 0x7f081030

.field public static final pb_new_vip_incentive:I = 0x7f081031

.field public static final pb_new_vip_incentive_detail:I = 0x7f081032

.field public static final pb_share_link_loading:I = 0x7f081033

.field public static final placeholder_icon:I = 0x7f081034

.field public static final pnl_layout_item_shadow:I = 0x7f081035

.field public static final pop_shadow:I = 0x7f081036

.field public static final popmenu_selector_bg:I = 0x7f081037

.field public static final popup_backgroud_light3:I = 0x7f081038

.field public static final popup_img1_gp_290_158:I = 0x7f081039

.field public static final popup_img2_gp_290_158:I = 0x7f08103a

.field public static final poster_qrcode_80px:I = 0x7f08103b

.field public static final preference_view_shadow:I = 0x7f08103c

.field public static final print_search_guide:I = 0x7f08103d

.field public static final processing:I = 0x7f08103e

.field public static final progress_horizonntal:I = 0x7f08103f

.field public static final progress_horizonntal_new:I = 0x7f081040

.field public static final progress_horizonntal_to_return:I = 0x7f081041

.field public static final progress_horizonntal_unsubscribe_recall:I = 0x7f081042

.field public static final progress_horizontal_bg_1:I = 0x7f081043

.field public static final progress_loading:I = 0x7f081044

.field public static final progress_loading_green:I = 0x7f081045

.field public static final progress_loading_grey_36px:I = 0x7f081046

.field public static final progress_loading_white_12px:I = 0x7f081047

.field public static final progress_loading_white_32px:I = 0x7f081048

.field public static final progress_loading_white_36px:I = 0x7f081049

.field public static final progress_newnew:I = 0x7f08104a

.field public static final progress_small:I = 0x7f08104b

.field public static final progress_small_border:I = 0x7f08104c

.field public static final progress_sync:I = 0x7f08104d

.field public static final purchase_buy_bg2:I = 0x7f08104e

.field public static final purchase_item_half_line:I = 0x7f08104f

.field public static final purchase_item_line:I = 0x7f081050

.field public static final purchase_pdf:I = 0x7f081051

.field public static final radio_bg_nps_circle:I = 0x7f081052

.field public static final radio_bg_nps_circle_f1f1f1:I = 0x7f081053

.field public static final rectange_black_text_back:I = 0x7f081054

.field public static final reward_close:I = 0x7f081055

.field public static final reward_gift_icon:I = 0x7f081056

.field public static final reward_top_bg:I = 0x7f081057

.field public static final ripple_corner_4_stroke_1_f1f1f1:I = 0x7f081058

.field public static final ripple_cs_brand_button:I = 0x7f081059

.field public static final rl_radius_corners:I = 0x7f08105a

.field public static final scanline:I = 0x7f08105b

.field public static final scanline_land:I = 0x7f08105c

.field public static final search_edittext_bg:I = 0x7f08105d

.field public static final secen_card_holder:I = 0x7f08105e

.field public static final security_icon:I = 0x7f08105f

.field public static final seekbar_circle_border_thumb:I = 0x7f081060

.field public static final seekbar_control_disabled_holo:I = 0x7f081061

.field public static final seekbar_control_focused_holo:I = 0x7f081062

.field public static final seekbar_control_normal_holo:I = 0x7f081063

.field public static final seekbar_control_pressed_holo:I = 0x7f081064

.field public static final seekbar_control_selector_holo:I = 0x7f081065

.field public static final seekbar_primary_holo:I = 0x7f081066

.field public static final seekbar_progress_horizontal_holo_light:I = 0x7f081067

.field public static final seekbar_secondary_holo:I = 0x7f081068

.field public static final seekbar_security_mark_size_material:I = 0x7f081069

.field public static final seekbar_signature:I = 0x7f08106a

.field public static final seekbar_signature_thumb:I = 0x7f08106b

.field public static final seekbar_track_holo_light:I = 0x7f08106c

.field public static final selected_check_box:I = 0x7f08106d

.field public static final selector_auto_warp_checkbox:I = 0x7f08106e

.field public static final selector_auto_warp_checkbox_dark:I = 0x7f08106f

.field public static final selector_capture_orienation_refactor_item_background:I = 0x7f081070

.field public static final selector_capture_setting_background:I = 0x7f081071

.field public static final selector_capture_setting_text_color:I = 0x7f081072

.field public static final selector_capture_setting_text_color_99:I = 0x7f081073

.field public static final selector_checkbox:I = 0x7f081074

.field public static final selector_checkbox_12:I = 0x7f081075

.field public static final selector_checkbox_can_exam:I = 0x7f081076

.field public static final selector_checkbox_drop_cnl_complaint:I = 0x7f081077

.field public static final selector_checkbox_o_vip_upgrade_complaint:I = 0x7f081078

.field public static final selector_checkbox_purchase_complaint:I = 0x7f081079

.field public static final selector_checkbox_round_retangle_login_main:I = 0x7f08107a

.field public static final selector_comm_19bcaa_corner_4:I = 0x7f08107b

.field public static final selector_doc_checkbox:I = 0x7f08107c

.field public static final selector_doc_checkbox_large:I = 0x7f08107d

.field public static final selector_doc_upgrade_checkbox:I = 0x7f08107e

.field public static final selector_esign_invite_type_item_bg:I = 0x7f08107f

.field public static final selector_esign_me_page_signature:I = 0x7f081080

.field public static final selector_jigsaw_template:I = 0x7f081081

.field public static final selector_no_prompt_retangle:I = 0x7f081082

.field public static final selector_pagelist_checkbox:I = 0x7f081083

.field public static final selector_radio_button:I = 0x7f081084

.field public static final selector_radio_button_off:I = 0x7f081085

.field public static final selector_radio_button_on:I = 0x7f081086

.field public static final selector_radio_button_red:I = 0x7f081087

.field public static final selector_radio_button_red_on:I = 0x7f081088

.field public static final selector_recommend_dir_checkbox:I = 0x7f081089

.field public static final selector_signin_with_google:I = 0x7f08108a

.field public static final selector_tag_item_bg:I = 0x7f08108b

.field public static final selector_text_color_signin_with_google:I = 0x7f08108c

.field public static final selector_upload_format:I = 0x7f08108d

.field public static final selector_vip_function_guide_tab:I = 0x7f08108e

.field public static final shading_light:I = 0x7f08108f

.field public static final shape_1919bcaa_corner11:I = 0x7f081090

.field public static final shape_19bcaa_corner4:I = 0x7f081091

.field public static final shape_33ffffff_corner12:I = 0x7f081092

.field public static final shape_33ffffff_corner4:I = 0x7f081093

.field public static final shape_4d000000_corner4:I = 0x7f081094

.field public static final shape_50_black_round_corner:I = 0x7f081095

.field public static final shape_668a8a8a_corner12:I = 0x7f081096

.field public static final shape_80_black_round_corner:I = 0x7f081097

.field public static final shape_accept_gift_button_fe2c2c_ff7223_corner_300dp:I = 0x7f081098

.field public static final shape_bg0_stroke_1_corner_16:I = 0x7f081099

.field public static final shape_bg1_corner_12_top:I = 0x7f08109a

.field public static final shape_bg_00000000_border_2dp_dash:I = 0x7f08109b

.field public static final shape_bg_000000_gradient_60:I = 0x7f08109c

.field public static final shape_bg_0077ff_corner_4dp:I = 0x7f08109d

.field public static final shape_bg_0077ff_full_corner_4dp:I = 0x7f08109e

.field public static final shape_bg_00b15f_corner_4dp:I = 0x7f08109f

.field public static final shape_bg_00b15f_full_corner_4dp:I = 0x7f0810a0

.field public static final shape_bg_096eee_corner_8dp:I = 0x7f0810a1

.field public static final shape_bg_0_blue_round_corner_solid_white_normal_1dp:I = 0x7f0810a2

.field public static final shape_bg_0_bottom_corner_8dp:I = 0x7f0810a3

.field public static final shape_bg_0_capture_tips:I = 0x7f0810a4

.field public static final shape_bg_0_corner_2dp:I = 0x7f0810a5

.field public static final shape_bg_0_corner_4dp:I = 0x7f0810a6

.field public static final shape_bg_0_dialog_corner_5dp:I = 0x7f0810a7

.field public static final shape_bg_0d000000_round_4dp:I = 0x7f0810a8

.field public static final shape_bg_0d7dfc_corner_4:I = 0x7f0810a9

.field public static final shape_bg_0f03c17f_corner_4_stroke_1_f1f1f1:I = 0x7f0810aa

.field public static final shape_bg_0f03c17f_corner_8_stroke_1_1404c07f:I = 0x7f0810ab

.field public static final shape_bg_0f03c17f_top_corner_8:I = 0x7f0810ac

.field public static final shape_bg_0f22abd4_corner_4_stroke_1_f1f1f1:I = 0x7f0810ad

.field public static final shape_bg_0f22abd4_corner_8_stroke_1_1422abd4:I = 0x7f0810ae

.field public static final shape_bg_0f22abd4_top_corner_8:I = 0x7f0810af

.field public static final shape_bg_0f2c79ff_corner_4_stroke_1_f1f1f1:I = 0x7f0810b0

.field public static final shape_bg_0f2c79ff_corner_8_stroke_1_14347eff:I = 0x7f0810b1

.field public static final shape_bg_0f2c79ff_top_corner_8:I = 0x7f0810b2

.field public static final shape_bg_0fde4939_corner_4_stroke_1_f1f1f1:I = 0x7f0810b3

.field public static final shape_bg_0fde4939_corner_8_stroke_1:I = 0x7f0810b4

.field public static final shape_bg_0fde4939_corner_8_stroke_1_14de4939:I = 0x7f0810b5

.field public static final shape_bg_0fde4939_top_corner_8:I = 0x7f0810b6

.field public static final shape_bg_0fff523b_corner_4_stroke_1_f1f1f1:I = 0x7f0810b7

.field public static final shape_bg_0fff523b_corner_8_stroke_1_14f02a1e:I = 0x7f0810b8

.field public static final shape_bg_0fff523b_top_corner_8:I = 0x7f0810b9

.field public static final shape_bg_141414_corner8:I = 0x7f0810ba

.field public static final shape_bg_141414_corner_top_12:I = 0x7f0810bb

.field public static final shape_bg_190dacd5_corner_4dp:I = 0x7f0810bc

.field public static final shape_bg_1919bcaa_corner_4dp:I = 0x7f0810bd

.field public static final shape_bg_1919bcaa_corner_4dp_border_19bcaa_1:I = 0x7f0810be

.field public static final shape_bg_19548bf5_corner_4dp:I = 0x7f0810bf

.field public static final shape_bg_19bc9c_corner_18dp:I = 0x7f0810c0

.field public static final shape_bg_19bc9c_corner_2dp:I = 0x7f0810c1

.field public static final shape_bg_19bc9c_corner_4dp:I = 0x7f0810c2

.field public static final shape_bg_19bcaa_border_2419bcaa_conner_4:I = 0x7f0810c3

.field public static final shape_bg_19bcaa_bottom_conner_4:I = 0x7f0810c4

.field public static final shape_bg_19bcaa_corner_1:I = 0x7f0810c5

.field public static final shape_bg_19bcaa_corner_100dp:I = 0x7f0810c6

.field public static final shape_bg_19bcaa_corner_2_stroke_2:I = 0x7f0810c7

.field public static final shape_bg_19bcaa_corner_2dp:I = 0x7f0810c8

.field public static final shape_bg_19bcaa_corner_40dp:I = 0x7f0810c9

.field public static final shape_bg_19bcaa_corner_4dp:I = 0x7f0810ca

.field public static final shape_bg_19bcaa_corner_4dp_stroke_1px:I = 0x7f0810cb

.field public static final shape_bg_19bcaa_corner_5dp_stroke_2dp:I = 0x7f0810cc

.field public static final shape_bg_19bcaa_corner_6dp_stroke_1px:I = 0x7f0810cd

.field public static final shape_bg_19bcaa_corner_common_2dp:I = 0x7f0810ce

.field public static final shape_bg_19bcaa_corner_stroke_1dp:I = 0x7f0810cf

.field public static final shape_bg_1_top_corner_8dp:I = 0x7f0810d0

.field public static final shape_bg_1a000000:I = 0x7f0810d1

.field public static final shape_bg_1c1c1e_conner_16:I = 0x7f0810d2

.field public static final shape_bg_1c1c1e_conner_8:I = 0x7f0810d3

.field public static final shape_bg_1c1c1e_top_corner_12dp:I = 0x7f0810d4

.field public static final shape_bg_1e2939_corner_4dp:I = 0x7f0810d5

.field public static final shape_bg_1e8c5efb_corner_4dp:I = 0x7f0810d6

.field public static final shape_bg_20_white_border_9c9c9c_corner_4:I = 0x7f0810d7

.field public static final shape_bg_33000000_corner_2:I = 0x7f0810d8

.field public static final shape_bg_3319bcaa_stroke_19bcaa_corner_4dp:I = 0x7f0810d9

.field public static final shape_bg_3_corner_8dp:I = 0x7f0810da

.field public static final shape_bg_48afe6_corner_4dp_stroke_2dp:I = 0x7f0810db

.field public static final shape_bg_4c000000_corner_18:I = 0x7f0810dc

.field public static final shape_bg_4c000000_corner_19:I = 0x7f0810dd

.field public static final shape_bg_4c000000_round_4dp:I = 0x7f0810de

.field public static final shape_bg_4d000000_corner_2:I = 0x7f0810df

.field public static final shape_bg_646464_corner_16:I = 0x7f0810e0

.field public static final shape_bg_646464_corner_2:I = 0x7f0810e1

.field public static final shape_bg_66000000_bottom_conner_4:I = 0x7f0810e2

.field public static final shape_bg_66000000_corner_18:I = 0x7f0810e3

.field public static final shape_bg_69d090_corner_4dp:I = 0x7f0810e4

.field public static final shape_bg_80000000:I = 0x7f0810e5

.field public static final shape_bg_80ff6748_top_corner_8dp:I = 0x7f0810e6

.field public static final shape_bg_99ffffff_corner_18dp:I = 0x7f0810e7

.field public static final shape_bg_bg2_top_corner12:I = 0x7f0810e8

.field public static final shape_bg_black_round_4dp:I = 0x7f0810e9

.field public static final shape_bg_c2daff_12:I = 0x7f0810ea

.field public static final shape_bg_color_bg_0_bottom_corner_8dp:I = 0x7f0810eb

.field public static final shape_bg_color_bg_0_top_corner_16dp:I = 0x7f0810ec

.field public static final shape_bg_corner_16dp_fdf6ea:I = 0x7f0810ed

.field public static final shape_bg_corner_20dp_fff4e8:I = 0x7f0810ee

.field public static final shape_bg_d59b45_top_corner_8dp:I = 0x7f0810ef

.field public static final shape_bg_dcdcdc_corner_4dp:I = 0x7f0810f0

.field public static final shape_bg_dialog_corner_5dp:I = 0x7f0810f1

.field public static final shape_bg_e2bd81:I = 0x7f0810f2

.field public static final shape_bg_e4e4e4_corner_4_new:I = 0x7f0810f3

.field public static final shape_bg_e4e4e4_corner_4dp:I = 0x7f0810f4

.field public static final shape_bg_e5efff_16:I = 0x7f0810f5

.field public static final shape_bg_e6e6e6_corner_2dp:I = 0x7f0810f6

.field public static final shape_bg_e8bc72_corner_4dp:I = 0x7f0810f7

.field public static final shape_bg_e8bc72_top_corner_8dp:I = 0x7f0810f8

.field public static final shape_bg_ebebeb_corner_4dp:I = 0x7f0810f9

.field public static final shape_bg_ecf0f1_top_corner_6dp:I = 0x7f0810fa

.field public static final shape_bg_f1f1f1_corner_10dp_top:I = 0x7f0810fb

.field public static final shape_bg_f1f1f1_corner_3_stroke_1_19bcaa:I = 0x7f0810fc

.field public static final shape_bg_f1f1f1_corner_3_stroke_1_dcdcdc:I = 0x7f0810fd

.field public static final shape_bg_f1f1f1_corner_4:I = 0x7f0810fe

.field public static final shape_bg_f1f1f1_corner_4dp:I = 0x7f0810ff

.field public static final shape_bg_f1f1f1_corner_8_stroke_1_19bcaa:I = 0x7f081100

.field public static final shape_bg_f1f1f1_corner_8_stroke_1_dcdcdc:I = 0x7f081101

.field public static final shape_bg_f1f1f1_top_corner_12:I = 0x7f081102

.field public static final shape_bg_f1f1f1_top_corner_20:I = 0x7f081103

.field public static final shape_bg_f1f1f1_top_corner_4:I = 0x7f081104

.field public static final shape_bg_f1f1f1_top_corner_8:I = 0x7f081105

.field public static final shape_bg_f2f2f2_corner_14dp:I = 0x7f081106

.field public static final shape_bg_f7f7f7_corner_100dp:I = 0x7f081107

.field public static final shape_bg_f7f7f7_corner_10dp_top:I = 0x7f081108

.field public static final shape_bg_f7f7f7_corner_20dp:I = 0x7f081109

.field public static final shape_bg_f7f7f7_corner_4_stroke_1_f1f1f1:I = 0x7f08110a

.field public static final shape_bg_f7f7f7_corner_4dp:I = 0x7f08110b

.field public static final shape_bg_f7f7f7_corner_5dp:I = 0x7f08110c

.field public static final shape_bg_f7f7f7_corner_6dp:I = 0x7f08110d

.field public static final shape_bg_f7f7f7_corner_8:I = 0x7f08110e

.field public static final shape_bg_f7f7f7_corner_8_bg1:I = 0x7f08110f

.field public static final shape_bg_f7f7f7_corner_8dp:I = 0x7f081110

.field public static final shape_bg_f7f7f7_top_corner_3:I = 0x7f081111

.field public static final shape_bg_f7f7f7_top_corner_4:I = 0x7f081112

.field public static final shape_bg_f8ecd9_f0dab7_corner_0:I = 0x7f081113

.field public static final shape_bg_f8ecd9_f0dab7_corner_8:I = 0x7f081114

.field public static final shape_bg_fafafa_corner_4dp:I = 0x7f081115

.field public static final shape_bg_fbfbfb_corner_6dp:I = 0x7f081116

.field public static final shape_bg_ff6748_corner_4dp:I = 0x7f081117

.field public static final shape_bg_ff6748_top_corner_8dp:I = 0x7f081118

.field public static final shape_bg_ff6a2a_corner_4dp:I = 0x7f081119

.field public static final shape_bg_ff6a48_corner_2dp:I = 0x7f08111a

.field public static final shape_bg_ff7255_corner_10:I = 0x7f08111b

.field public static final shape_bg_ff7255_corner_25:I = 0x7f08111c

.field public static final shape_bg_ff7255_corner_4dp:I = 0x7f08111d

.field public static final shape_bg_ff7255_corner_8dp:I = 0x7f08111e

.field public static final shape_bg_ff8e83_corner_4dp:I = 0x7f08111f

.field public static final shape_bg_ff8eb_corner_25:I = 0x7f081120

.field public static final shape_bg_ffd4ba:I = 0x7f081121

.field public static final shape_bg_ffeee6_corner_10:I = 0x7f081122

.field public static final shape_bg_ffffff_bottom_corner_8dp:I = 0x7f081123

.field public static final shape_bg_ffffff_corner_12dp:I = 0x7f081124

.field public static final shape_bg_ffffff_corner_14dp:I = 0x7f081125

.field public static final shape_bg_ffffff_corner_200_border_f1f1f1_1dp:I = 0x7f081126

.field public static final shape_bg_ffffff_corner_22dp:I = 0x7f081127

.field public static final shape_bg_ffffff_corner_2dp:I = 0x7f081128

.field public static final shape_bg_ffffff_corner_2dp_stroke_cbcbcb:I = 0x7f081129

.field public static final shape_bg_ffffff_corner_2dp_stroke_e8bc72:I = 0x7f08112a

.field public static final shape_bg_ffffff_corner_3_stroke_1_dcdcdc:I = 0x7f08112b

.field public static final shape_bg_ffffff_corner_3dp:I = 0x7f08112c

.field public static final shape_bg_ffffff_corner_4_border_cccccc_1dp:I = 0x7f08112d

.field public static final shape_bg_ffffff_corner_4_border_dcdcdc_1dp:I = 0x7f08112e

.field public static final shape_bg_ffffff_corner_4_border_f1f1f1_1dp:I = 0x7f08112f

.field public static final shape_bg_ffffff_corner_4_stroke_1_f1f1f1:I = 0x7f081130

.field public static final shape_bg_ffffff_corner_4_stroke_half_f1f1f1:I = 0x7f081131

.field public static final shape_bg_ffffff_corner_4dp:I = 0x7f081132

.field public static final shape_bg_ffffff_corner_4dp_light:I = 0x7f081133

.field public static final shape_bg_ffffff_corner_4dp_stroke_dcdcdc_1dp:I = 0x7f081134

.field public static final shape_bg_ffffff_corner_6dp:I = 0x7f081135

.field public static final shape_bg_ffffff_corner_6dp_light:I = 0x7f081136

.field public static final shape_bg_ffffff_corner_8_stroke_1_dcdcdc:I = 0x7f081137

.field public static final shape_bg_ffffff_corner_8_stroke_1_f1f1f1:I = 0x7f081138

.field public static final shape_bg_ffffff_corner_8_stroke_half_f1f1f1:I = 0x7f081139

.field public static final shape_bg_ffffff_corner_8dp:I = 0x7f08113a

.field public static final shape_bg_ffffff_stroke_1_corner_16:I = 0x7f08113b

.field public static final shape_bg_ffffffff_top_corner_12dp:I = 0x7f08113c

.field public static final shape_bg_ffffffff_top_corner_8dp:I = 0x7f08113d

.field public static final shape_bg_gradient_000000_60:I = 0x7f08113e

.field public static final shape_bg_gradient_01a78a_38dbb0_8dp:I = 0x7f08113f

.field public static final shape_bg_gradient_1c1c1e_270:I = 0x7f081140

.field public static final shape_bg_gradient_2a84ee_46e0ec_8dp:I = 0x7f081141

.field public static final shape_bg_gradient_a1acc7_aeb8d2_8dp:I = 0x7f081142

.field public static final shape_bg_gradient_corner_top_25dp:I = 0x7f081143

.field public static final shape_bg_gradient_d59b45_edcc8b:I = 0x7f081144

.field public static final shape_bg_gradient_f16564_ef9483_8dp:I = 0x7f081145

.field public static final shape_bg_gradient_f7f7f7_ffffff:I = 0x7f081146

.field public static final shape_bg_green_light_round_4dp:I = 0x7f081147

.field public static final shape_bg_horizontal_gradient_eb5f4c_d83234_corner25:I = 0x7f081148

.field public static final shape_bg_horizontal_gradient_ff5860_fd765a_corner25:I = 0x7f081149

.field public static final shape_bg_horizontal_gradient_ff6e1c_corner25:I = 0x7f08114a

.field public static final shape_bg_horizontal_gradient_gold_corner25:I = 0x7f08114b

.field public static final shape_bg_horizontal_gradient_gold_d59b45_edcc8b_corner4:I = 0x7f08114c

.field public static final shape_bg_horizontal_gradient_orangle_corner25:I = 0x7f08114d

.field public static final shape_bg_horizontal_stroke_ff6e46_corner25:I = 0x7f08114e

.field public static final shape_bg_round_d59b45:I = 0x7f08114f

.field public static final shape_bg_round_ffff9312:I = 0x7f081150

.field public static final shape_bg_stroke_096eee_corner_8dp:I = 0x7f081151

.field public static final shape_bg_stroke_19bc9c_corner_40dp:I = 0x7f081152

.field public static final shape_bg_stroke_19bc9c_corner_4dp:I = 0x7f081153

.field public static final shape_bg_stroke_19bc9c_corner_6dp:I = 0x7f081154

.field public static final shape_bg_stroke_3dp_corner_6dp_19bc9c:I = 0x7f081155

.field public static final shape_bg_stroke_c6c6c6_corner_8dp:I = 0x7f081156

.field public static final shape_bg_stroke_e8bc72_corner_8dp:I = 0x7f081157

.field public static final shape_bg_stroke_ff6748_corner_8dp:I = 0x7f081158

.field public static final shape_bg_vertical_gradient_white:I = 0x7f081159

.field public static final shape_bg_white2_round_200dp:I = 0x7f08115a

.field public static final shape_bg_white_border_ff7255_corner_25:I = 0x7f08115b

.field public static final shape_bg_white_bottom_corner_4_new:I = 0x7f08115c

.field public static final shape_bg_white_bottom_corner_4dp:I = 0x7f08115d

.field public static final shape_bg_white_bottom_corner_4dp_light:I = 0x7f08115e

.field public static final shape_bg_white_bottom_corner_8dp:I = 0x7f08115f

.field public static final shape_bg_white_corner_12:I = 0x7f081160

.field public static final shape_bg_white_corner_4:I = 0x7f081161

.field public static final shape_bg_white_corner_4_bg1:I = 0x7f081162

.field public static final shape_bg_white_corner_4dp_stroke_19bcaa_1px:I = 0x7f081163

.field public static final shape_bg_white_corner_4dp_stroke_dcdcdc_1px:I = 0x7f081164

.field public static final shape_bg_white_corner_8_stroke_1_f1f1f1:I = 0x7f081165

.field public static final shape_bg_white_round_200dp:I = 0x7f081166

.field public static final shape_bg_white_top_corner_12:I = 0x7f081167

.field public static final shape_bg_white_top_corner_12_light:I = 0x7f081168

.field public static final shape_border_1_3a3a3c_conner_4:I = 0x7f081169

.field public static final shape_bottom_corner_16_white:I = 0x7f08116a

.field public static final shape_checkout_dialog_pay_type_bg:I = 0x7f08116b

.field public static final shape_checkout_dialog_product_bg:I = 0x7f08116c

.field public static final shape_checkout_dialog_purchase_bg:I = 0x7f08116d

.field public static final shape_checkout_dialog_title_bg:I = 0x7f08116e

.field public static final shape_checkout_pay_way_invoice_bg:I = 0x7f08116f

.field public static final shape_circle_000000:I = 0x7f081170

.field public static final shape_circle_19bcaa:I = 0x7f081171

.field public static final shape_circle_26000000:I = 0x7f081172

.field public static final shape_circle_bg_dcdcdc:I = 0x7f081173

.field public static final shape_circle_eeeeec:I = 0x7f081174

.field public static final shape_circle_f9f9f9:I = 0x7f081175

.field public static final shape_circle_text_4:I = 0x7f081176

.field public static final shape_color_bg_1_conner_2:I = 0x7f081177

.field public static final shape_color_bg_2_corner_4:I = 0x7f081178

.field public static final shape_colorbg2_corner2:I = 0x7f081179

.field public static final shape_corner10:I = 0x7f08117a

.field public static final shape_corner_16_cc000000:I = 0x7f08117b

.field public static final shape_corner_2_stroke_text_1:I = 0x7f08117c

.field public static final shape_corner_4_cc000000:I = 0x7f08117d

.field public static final shape_corner_8_bg_0:I = 0x7f08117e

.field public static final shape_corner_8_bg_0_bottom:I = 0x7f08117f

.field public static final shape_corner_8_bg_0_top:I = 0x7f081180

.field public static final shape_corner_8_cc000000:I = 0x7f081181

.field public static final shape_corner_8_text_1:I = 0x7f081182

.field public static final shape_corner_8_text_5:I = 0x7f081183

.field public static final shape_corner_fafafa_4:I = 0x7f081184

.field public static final shape_cs_bg_0_border_1_corner_8:I = 0x7f081185

.field public static final shape_cs_bg_1_corner_4:I = 0x7f081186

.field public static final shape_cs_brand_active_corner_4:I = 0x7f081187

.field public static final shape_cs_brand_corner_4:I = 0x7f081188

.field public static final shape_cs_brand_disabled_corner_4:I = 0x7f081189

.field public static final shape_cs_color_bg_color_2_border_1_color_2_conner_8:I = 0x7f08118a

.field public static final shape_cs_color_text_3_brder_4:I = 0x7f08118b

.field public static final shape_cs_pdf_vip_buy_success_btn_bg:I = 0x7f08118c

.field public static final shape_cs_pdf_vip_buy_success_right_bg:I = 0x7f08118d

.field public static final shape_cs_pdf_vip_buy_success_to_login_content_bg:I = 0x7f08118e

.field public static final shape_cs_pdf_vip_buy_success_top_bg:I = 0x7f08118f

.field public static final shape_doc_import_btn_bg:I = 0x7f081190

.field public static final shape_doc_import_directory_btn_bg:I = 0x7f081191

.field public static final shape_dot_black:I = 0x7f081192

.field public static final shape_dot_green:I = 0x7f081193

.field public static final shape_drop_cnl_close_new_bg:I = 0x7f081194

.field public static final shape_f1f1f1_conner_8_border:I = 0x7f081195

.field public static final shape_f1f1f1_corner_4dp:I = 0x7f081196

.field public static final shape_f7f7f7_corner11:I = 0x7f081197

.field public static final shape_f7f7f7_corner4:I = 0x7f081198

.field public static final shape_f7f7f7_corner_top4:I = 0x7f081199

.field public static final shape_f7f7f7_corner_top8:I = 0x7f08119a

.field public static final shape_fbe9bc_f0c34f_bg:I = 0x7f08119b

.field public static final shape_ffffff_bg_corner_4:I = 0x7f08119c

.field public static final shape_ffffff_corner4:I = 0x7f08119d

.field public static final shape_ffffff_corner_10:I = 0x7f08119e

.field public static final shape_image_quality_tips_bg:I = 0x7f08119f

.field public static final shape_image_quality_tips_fg:I = 0x7f0811a0

.field public static final shape_image_quality_tips_fg_black:I = 0x7f0811a1

.field public static final shape_maindoc_list_search_tips:I = 0x7f0811a2

.field public static final shape_maindoc_list_search_tips_tag:I = 0x7f0811a3

.field public static final shape_me_page_area_free_card_bg:I = 0x7f0811a4

.field public static final shape_me_page_area_free_vip_btn_bg:I = 0x7f0811a5

.field public static final shape_me_page_header_bg_new:I = 0x7f0811a6

.field public static final shape_me_page_header_icon_bg:I = 0x7f0811a7

.field public static final shape_me_page_o_vip_right_icon_bg:I = 0x7f0811a8

.field public static final shape_me_page_vip_card_right:I = 0x7f0811a9

.field public static final shape_me_page_vip_card_upgrade_bg:I = 0x7f0811aa

.field public static final shape_me_page_vip_card_upgrade_gold_bg:I = 0x7f0811ab

.field public static final shape_me_page_vip_right_mask:I = 0x7f0811ac

.field public static final shape_my_account_cs_pdf_vip_bg:I = 0x7f0811ad

.field public static final shape_negative_pop_label_dark:I = 0x7f0811ae

.field public static final shape_no_dark_ffffff_corner_8:I = 0x7f0811af

.field public static final shape_o_vip_diff_price_bg:I = 0x7f0811b0

.field public static final shape_o_vip_purchase_bg:I = 0x7f0811b1

.field public static final shape_o_vip_purchase_item_bg:I = 0x7f0811b2

.field public static final shape_o_vip_upgrade_purchase_content_bg:I = 0x7f0811b3

.field public static final shape_o_vip_upgrade_tips_bg:I = 0x7f0811b4

.field public static final shape_rec_white_half:I = 0x7f0811b5

.field public static final shape_rect_bg_ebebeb_corner_right_corner4:I = 0x7f0811b6

.field public static final shape_rect_custom_toast:I = 0x7f0811b7

.field public static final shape_rectangle_f5f5f5_top_corner_8dp:I = 0x7f0811b8

.field public static final shape_rectangle_f7f7f7_conner_4:I = 0x7f0811b9

.field public static final shape_rejoin_benefit_title_left:I = 0x7f0811ba

.field public static final shape_rejoin_benefit_title_right:I = 0x7f0811bb

.field public static final shape_renewal_agreement_btn_bg:I = 0x7f0811bc

.field public static final shape_scan_done_task_content_bg:I = 0x7f0811bd

.field public static final shape_scan_done_vip_month_btn_bg:I = 0x7f0811be

.field public static final shape_send_gift_card_share_title_bg:I = 0x7f0811bf

.field public static final shape_share_done_vip_month_content_bg:I = 0x7f0811c0

.field public static final shape_solid_ff9b3e_corner_bg:I = 0x7f0811c1

.field public static final shape_solid_ffe9e4_corner_8:I = 0x7f0811c2

.field public static final shape_solid_fffaf4_corner_10:I = 0x7f0811c3

.field public static final shape_solid_ffffff_round:I = 0x7f0811c4

.field public static final shape_storke_white_corner_6dp:I = 0x7f0811c5

.field public static final shape_stroke_19bcaa_corner_4dp:I = 0x7f0811c6

.field public static final shape_stroke_212121_border_1_round:I = 0x7f0811c7

.field public static final shape_stroke_2c2c2e_corner_new:I = 0x7f0811c8

.field public static final shape_stroke_979797_corner:I = 0x7f0811c9

.field public static final shape_stroke_ddaa5a_radius_4:I = 0x7f0811ca

.field public static final shape_stroke_f76b39_width_2:I = 0x7f0811cb

.field public static final shape_stroke_ff6748_border_1_round:I = 0x7f0811cc

.field public static final shape_translate_lang:I = 0x7f0811cd

.field public static final shape_translate_langs:I = 0x7f0811ce

.field public static final shape_triangle_left_f1f1f1:I = 0x7f0811cf

.field public static final shape_vendor_privilege_item_point_normal:I = 0x7f0811d0

.field public static final shape_vip_month_promotion_discount_bg:I = 0x7f0811d1

.field public static final shape_vip_month_promotion_func_title_bg:I = 0x7f0811d2

.field public static final shape_vip_month_promotion_purchase_3_bg:I = 0x7f0811d3

.field public static final shape_vip_month_promotion_purchase_bg:I = 0x7f0811d4

.field public static final shape_vip_month_promotion_vip_card_bg:I = 0x7f0811d5

.field public static final shape_vip_upgrade_gift_item_bg:I = 0x7f0811d6

.field public static final shape_vip_upgrade_gift_receive_text_bg:I = 0x7f0811d7

.field public static final shape_vip_upgrade_left_line_bg:I = 0x7f0811d8

.field public static final shape_vip_upgrade_right_line_bg:I = 0x7f0811d9

.field public static final share_dir_member_error:I = 0x7f0811da

.field public static final share_dir_member_loading:I = 0x7f0811db

.field public static final share_item_workflow_icon:I = 0x7f0811dc

.field public static final share_jpg_no_water_bg:I = 0x7f0811dd

.field public static final share_panel_tab_select_left:I = 0x7f0811de

.field public static final share_unlogin_icon:I = 0x7f0811df

.field public static final share_vip_month_promotion_bg_3:I = 0x7f0811e0

.field public static final sharpness_high:I = 0x7f0811e1

.field public static final sign_in_tip:I = 0x7f0811e2

.field public static final slidebar_bg:I = 0x7f0811e3

.field public static final smart_erase_bottom_brush:I = 0x7f0811e4

.field public static final smart_erase_bottom_close:I = 0x7f0811e5

.field public static final smart_erase_bottom_confirm:I = 0x7f0811e6

.field public static final smart_erase_bottom_erase:I = 0x7f0811e7

.field public static final smart_erase_bottom_text:I = 0x7f0811e8

.field public static final smart_erase_bottom_watermark:I = 0x7f0811e9

.field public static final smart_erase_compare:I = 0x7f0811ea

.field public static final smart_erase_ic_vip:I = 0x7f0811eb

.field public static final smart_erase_next:I = 0x7f0811ec

.field public static final smart_erase_paint_size:I = 0x7f0811ed

.field public static final smart_erase_previous:I = 0x7f0811ee

.field public static final smart_erase_redo:I = 0x7f0811ef

.field public static final smart_erase_unredo:I = 0x7f0811f0

.field public static final spinner:I = 0x7f0811f1

.field public static final spinner_bg:I = 0x7f0811f2

.field public static final spinner_pressed:I = 0x7f0811f3

.field public static final storage_bg:I = 0x7f0811f4

.field public static final storage_blue:I = 0x7f0811f5

.field public static final storage_red:I = 0x7f0811f6

.field public static final storage_yellow:I = 0x7f0811f7

.field public static final superfilter_anim_scan_line:I = 0x7f0811f8

.field public static final superfilter_result_anim_scan_line:I = 0x7f0811f9

.field public static final svg_grid:I = 0x7f0811fa

.field public static final svg_sign_delete:I = 0x7f0811fb

.field public static final svg_sign_edit:I = 0x7f0811fc

.field public static final svg_sign_move:I = 0x7f0811fd

.field public static final svg_sound_off:I = 0x7f0811fe

.field public static final svg_sound_on:I = 0x7f0811ff

.field public static final svg_spirit_level:I = 0x7f081200

.field public static final svip_guide_subscript_zero_3x:I = 0x7f081201

.field public static final sync_doc_downloading:I = 0x7f081202

.field public static final sync_doc_fail:I = 0x7f081203

.field public static final sync_doc_success:I = 0x7f081204

.field public static final sync_doc_syncing:I = 0x7f081205

.field public static final sync_doc_uploading:I = 0x7f081206

.field public static final sync_progress_drawable:I = 0x7f081207

.field public static final sys_toolsbar_button_bg_normal:I = 0x7f081208

.field public static final sys_toolsbar_button_bg_push:I = 0x7f081209

.field public static final sys_toolsbar_separated_horizontal:I = 0x7f08120a

.field public static final tab_indicator:I = 0x7f08120b

.field public static final tag_add:I = 0x7f08120c

.field public static final tc_ocr_auto_wrap_select_20:I = 0x7f08120d

.field public static final tc_ocr_auto_wrap_unselect_20:I = 0x7f08120e

.field public static final tc_ocr_auto_wrap_unselect_dark_20:I = 0x7f08120f

.field public static final ten_g:I = 0x7f081210

.field public static final test_level_drawable:I = 0x7f081211

.field public static final text_color_red_gradually:I = 0x7f081212

.field public static final tool_page_v2_indicator:I = 0x7f081213

.field public static final tooltip_frame_dark:I = 0x7f081214

.field public static final tooltip_frame_light:I = 0x7f081215

.field public static final top_first_time_enter_en:I = 0x7f081216

.field public static final tt_ad_arrow_backward:I = 0x7f081217

.field public static final tt_ad_arrow_backward_wrapper:I = 0x7f081218

.field public static final tt_ad_arrow_forward:I = 0x7f081219

.field public static final tt_ad_arrow_forward_wrapper:I = 0x7f08121a

.field public static final tt_ad_closed_background_300_250:I = 0x7f08121b

.field public static final tt_ad_closed_background_320_50:I = 0x7f08121c

.field public static final tt_ad_closed_logo_red:I = 0x7f08121d

.field public static final tt_ad_cover_btn_begin_bg:I = 0x7f08121e

.field public static final tt_ad_download_progress_bar_horizontal:I = 0x7f08121f

.field public static final tt_ad_feedback:I = 0x7f081220

.field public static final tt_ad_landing_loading_three_left:I = 0x7f081221

.field public static final tt_ad_landing_loading_three_mid:I = 0x7f081222

.field public static final tt_ad_landing_loading_three_right:I = 0x7f081223

.field public static final tt_ad_link:I = 0x7f081224

.field public static final tt_ad_loading_rect:I = 0x7f081225

.field public static final tt_ad_loading_three_left:I = 0x7f081226

.field public static final tt_ad_loading_three_mid:I = 0x7f081227

.field public static final tt_ad_loading_three_right:I = 0x7f081228

.field public static final tt_ad_logo:I = 0x7f081229

.field public static final tt_ad_logo_new:I = 0x7f08122a

.field public static final tt_ad_logo_reward_full:I = 0x7f08122b

.field public static final tt_ad_refresh:I = 0x7f08122c

.field public static final tt_ad_report_info_bg:I = 0x7f08122d

.field public static final tt_ad_report_info_button_bg:I = 0x7f08122e

.field public static final tt_ad_skip_btn_bg:I = 0x7f08122f

.field public static final tt_ad_skip_btn_bg2:I = 0x7f081230

.field public static final tt_ad_threedots:I = 0x7f081231

.field public static final tt_ad_xmark:I = 0x7f081232

.field public static final tt_app_open_top_bg:I = 0x7f081233

.field public static final tt_back_video:I = 0x7f081234

.field public static final tt_backup_btn_1:I = 0x7f081235

.field public static final tt_backup_btn_2:I = 0x7f081236

.field public static final tt_browser_download_selector:I = 0x7f081237

.field public static final tt_browser_progress_style:I = 0x7f081238

.field public static final tt_button_back:I = 0x7f081239

.field public static final tt_circle_solid_mian:I = 0x7f08123a

.field public static final tt_close_btn:I = 0x7f08123b

.field public static final tt_close_move_detail:I = 0x7f08123c

.field public static final tt_close_move_details_normal:I = 0x7f08123d

.field public static final tt_close_move_details_pressed:I = 0x7f08123e

.field public static final tt_custom_dialog_bg:I = 0x7f08123f

.field public static final tt_detail_video_btn_bg:I = 0x7f081240

.field public static final tt_dislike_bottom_seletor:I = 0x7f081241

.field public static final tt_dislike_cancle_bg_selector:I = 0x7f081242

.field public static final tt_dislike_dialog_bg:I = 0x7f081243

.field public static final tt_dislike_icon:I = 0x7f081244

.field public static final tt_dislike_icon2:I = 0x7f081245

.field public static final tt_dislike_middle_seletor:I = 0x7f081246

.field public static final tt_dislike_top_bg:I = 0x7f081247

.field public static final tt_dislike_top_seletor:I = 0x7f081248

.field public static final tt_download_corner_bg:I = 0x7f081249

.field public static final tt_enlarge_video:I = 0x7f08124a

.field public static final tt_forward_video:I = 0x7f08124b

.field public static final tt_full_reward_loading_progress_style:I = 0x7f08124c

.field public static final tt_interact_circle:I = 0x7f08124d

.field public static final tt_interact_four_transparent_round_rect:I = 0x7f08124e

.field public static final tt_interact_oval:I = 0x7f08124f

.field public static final tt_interact_round_rect:I = 0x7f081250

.field public static final tt_interact_round_rect_stroke:I = 0x7f081251

.field public static final tt_item_background_material:I = 0x7f081252

.field public static final tt_landingpage_loading_1_progress_style:I = 0x7f081253

.field public static final tt_landingpage_loading_text_rect:I = 0x7f081254

.field public static final tt_leftbackbutton_titlebar_photo_preview:I = 0x7f081255

.field public static final tt_leftbackicon_selector:I = 0x7f081256

.field public static final tt_leftbackicon_selector_for_dark:I = 0x7f081257

.field public static final tt_lefterbackicon_titlebar:I = 0x7f081258

.field public static final tt_lefterbackicon_titlebar_for_dark:I = 0x7f081259

.field public static final tt_lefterbackicon_titlebar_press:I = 0x7f08125a

.field public static final tt_lefterbackicon_titlebar_press_for_dark:I = 0x7f08125b

.field public static final tt_lefterbackicon_titlebar_press_wrapper:I = 0x7f08125c

.field public static final tt_mute:I = 0x7f08125d

.field public static final tt_mute_btn_bg:I = 0x7f08125e

.field public static final tt_mute_wrapper:I = 0x7f08125f

.field public static final tt_new_pause_video:I = 0x7f081260

.field public static final tt_new_pause_video_press:I = 0x7f081261

.field public static final tt_new_play_video:I = 0x7f081262

.field public static final tt_normalscreen_loading:I = 0x7f081263

.field public static final tt_pangle_ad_close_btn_bg:I = 0x7f081264

.field public static final tt_pangle_ad_close_drawable:I = 0x7f081265

.field public static final tt_pangle_ad_mute_btn_bg:I = 0x7f081266

.field public static final tt_pangle_banner_btn_bg:I = 0x7f081267

.field public static final tt_pangle_btn_bg:I = 0x7f081268

.field public static final tt_pangle_close_icon:I = 0x7f081269

.field public static final tt_pangle_logo_white:I = 0x7f08126a

.field public static final tt_pangle_star_empty_bg:I = 0x7f08126b

.field public static final tt_pangle_star_full_bg:I = 0x7f08126c

.field public static final tt_play_movebar_textpage:I = 0x7f08126d

.field public static final tt_playable_btn_bk:I = 0x7f08126e

.field public static final tt_playable_progress_style:I = 0x7f08126f

.field public static final tt_privacy_back_icon:I = 0x7f081270

.field public static final tt_privacy_bg:I = 0x7f081271

.field public static final tt_privacy_btn_bg:I = 0x7f081272

.field public static final tt_privacy_progress_style:I = 0x7f081273

.field public static final tt_privacy_webview_bg:I = 0x7f081274

.field public static final tt_refreshing_video_textpage:I = 0x7f081275

.field public static final tt_refreshing_video_textpage_normal:I = 0x7f081276

.field public static final tt_refreshing_video_textpage_pressed:I = 0x7f081277

.field public static final tt_reward_countdown_bg:I = 0x7f081278

.field public static final tt_reward_dislike_icon:I = 0x7f081279

.field public static final tt_reward_full_feedback:I = 0x7f08127a

.field public static final tt_reward_full_mute:I = 0x7f08127b

.field public static final tt_reward_full_new_bar_bg:I = 0x7f08127c

.field public static final tt_reward_full_new_bar_btn_bg:I = 0x7f08127d

.field public static final tt_reward_full_unmute:I = 0x7f08127e

.field public static final tt_reward_full_video_backup_btn_bg:I = 0x7f08127f

.field public static final tt_reward_video_download_btn_bg:I = 0x7f081280

.field public static final tt_seek_progress:I = 0x7f081281

.field public static final tt_seek_thumb:I = 0x7f081282

.field public static final tt_seek_thumb_fullscreen:I = 0x7f081283

.field public static final tt_seek_thumb_fullscreen_press:I = 0x7f081284

.field public static final tt_seek_thumb_fullscreen_selector:I = 0x7f081285

.field public static final tt_seek_thumb_normal:I = 0x7f081286

.field public static final tt_seek_thumb_press:I = 0x7f081287

.field public static final tt_shadow_btn_back:I = 0x7f081288

.field public static final tt_shadow_btn_back_withoutnight:I = 0x7f081289

.field public static final tt_shadow_fullscreen_top:I = 0x7f08128a

.field public static final tt_shadow_lefterback_titlebar:I = 0x7f08128b

.field public static final tt_shadow_lefterback_titlebar_press:I = 0x7f08128c

.field public static final tt_shadow_lefterback_titlebar_press_withoutnight:I = 0x7f08128d

.field public static final tt_shadow_lefterback_titlebar_withoutnight:I = 0x7f08128e

.field public static final tt_shrink_fullscreen:I = 0x7f08128f

.field public static final tt_shrink_video:I = 0x7f081290

.field public static final tt_skip_btn:I = 0x7f081291

.field public static final tt_skip_btn_wrapper:I = 0x7f081292

.field public static final tt_slide_up_1:I = 0x7f081293

.field public static final tt_slide_up_10:I = 0x7f081294

.field public static final tt_slide_up_11:I = 0x7f081295

.field public static final tt_slide_up_12:I = 0x7f081296

.field public static final tt_slide_up_13:I = 0x7f081297

.field public static final tt_slide_up_14:I = 0x7f081298

.field public static final tt_slide_up_15:I = 0x7f081299

.field public static final tt_slide_up_2:I = 0x7f08129a

.field public static final tt_slide_up_3:I = 0x7f08129b

.field public static final tt_slide_up_4:I = 0x7f08129c

.field public static final tt_slide_up_5:I = 0x7f08129d

.field public static final tt_slide_up_6:I = 0x7f08129e

.field public static final tt_slide_up_7:I = 0x7f08129f

.field public static final tt_slide_up_8:I = 0x7f0812a0

.field public static final tt_slide_up_9:I = 0x7f0812a1

.field public static final tt_splash_brush_bg:I = 0x7f0812a2

.field public static final tt_splash_hand:I = 0x7f0812a3

.field public static final tt_splash_hand2:I = 0x7f0812a4

.field public static final tt_splash_hand3:I = 0x7f0812a5

.field public static final tt_splash_mute:I = 0x7f0812a6

.field public static final tt_splash_rock:I = 0x7f0812a7

.field public static final tt_splash_shake_hand:I = 0x7f0812a8

.field public static final tt_splash_slide_right_bg:I = 0x7f0812a9

.field public static final tt_splash_slide_right_circle:I = 0x7f0812aa

.field public static final tt_splash_slide_up_10:I = 0x7f0812ab

.field public static final tt_splash_slide_up_arrow:I = 0x7f0812ac

.field public static final tt_splash_slide_up_bg:I = 0x7f0812ad

.field public static final tt_splash_slide_up_circle:I = 0x7f0812ae

.field public static final tt_splash_slide_up_finger:I = 0x7f0812af

.field public static final tt_splash_twist:I = 0x7f0812b0

.field public static final tt_splash_unlock_btn_bg:I = 0x7f0812b1

.field public static final tt_splash_unlock_icon_empty:I = 0x7f0812b2

.field public static final tt_splash_unlock_image_arrow:I = 0x7f0812b3

.field public static final tt_splash_unlock_image_arrow_wrapper:I = 0x7f0812b4

.field public static final tt_splash_unlock_image_go:I = 0x7f0812b5

.field public static final tt_splash_unmute:I = 0x7f0812b6

.field public static final tt_star:I = 0x7f0812b7

.field public static final tt_star_thick:I = 0x7f0812b8

.field public static final tt_stop_movebar_textpage:I = 0x7f0812b9

.field public static final tt_suggestion_logo:I = 0x7f0812ba

.field public static final tt_titlebar_close:I = 0x7f0812bb

.field public static final tt_titlebar_close_drawable:I = 0x7f0812bc

.field public static final tt_titlebar_close_for_dark:I = 0x7f0812bd

.field public static final tt_titlebar_close_press:I = 0x7f0812be

.field public static final tt_titlebar_close_press_for_dark:I = 0x7f0812bf

.field public static final tt_titlebar_close_seletor:I = 0x7f0812c0

.field public static final tt_titlebar_close_seletor_for_dark:I = 0x7f0812c1

.field public static final tt_unmute:I = 0x7f0812c2

.field public static final tt_unmute_wrapper:I = 0x7f0812c3

.field public static final tt_up_slide:I = 0x7f0812c4

.field public static final tt_user:I = 0x7f0812c5

.field public static final tt_user_info:I = 0x7f0812c6

.field public static final tt_video_black_desc_gradient:I = 0x7f0812c7

.field public static final tt_video_close:I = 0x7f0812c8

.field public static final tt_video_close_drawable:I = 0x7f0812c9

.field public static final tt_video_loading_progress_bar:I = 0x7f0812ca

.field public static final tt_video_progress:I = 0x7f0812cb

.field public static final tt_video_progress_drawable:I = 0x7f0812cc

.field public static final tt_white_lefterbackicon_titlebar:I = 0x7f0812cd

.field public static final tt_white_lefterbackicon_titlebar_press:I = 0x7f0812ce

.field public static final tt_white_righterbackicon_titlebar:I = 0x7f0812cf

.field public static final tt_white_slide_up:I = 0x7f0812d0

.field public static final tt_wriggle_union:I = 0x7f0812d1

.field public static final tt_wriggle_union_white:I = 0x7f0812d2

.field public static final unlock_mask:I = 0x7f0812d3

.field public static final unselected_check_box:I = 0x7f0812d4

.field public static final upgrade_dot_select:I = 0x7f0812d5

.field public static final upgrade_dot_unselect:I = 0x7f0812d6

.field public static final util_share_dlg_selector_bg:I = 0x7f0812d7

.field public static final util_share_dlg_selector_bg_transition:I = 0x7f0812d8

.field public static final v40_im_a4pizzle:I = 0x7f0812d9

.field public static final v41_im_id_1:I = 0x7f0812da

.field public static final v41_im_id_2:I = 0x7f0812db

.field public static final v41_im_id_3:I = 0x7f0812dc

.field public static final v44_im_a4pizzle_en:I = 0x7f0812dd

.field public static final v44_im_id:I = 0x7f0812de

.field public static final v44_im_passport:I = 0x7f0812df

.field public static final v5223_ic_info:I = 0x7f0812e0

.field public static final v52_im_ocr:I = 0x7f0812e1

.field public static final v52_im_ties:I = 0x7f0812e2

.field public static final v5_3_sign:I = 0x7f0812e3

.field public static final vip_14:I = 0x7f0812e4

.field public static final vip_14px:I = 0x7f0812e5

.field public static final vip_16px:I = 0x7f0812e6

.field public static final vip_21_cloud:I = 0x7f0812e7

.field public static final vip_21_collage:I = 0x7f0812e8

.field public static final vip_21_comment:I = 0x7f0812e9

.field public static final vip_21_folder_local:I = 0x7f0812ea

.field public static final vip_21_idcard:I = 0x7f0812eb

.field public static final vip_21_link:I = 0x7f0812ec

.field public static final vip_21_noad:I = 0x7f0812ed

.field public static final vip_21_nowatermark:I = 0x7f0812ee

.field public static final vip_21_ocr:I = 0x7f0812ef

.field public static final vip_21_pdf:I = 0x7f0812f0

.field public static final vip_21_textrecognition:I = 0x7f0812f1

.field public static final vip_21_translation:I = 0x7f0812f2

.field public static final vip_21_uploading:I = 0x7f0812f3

.field public static final vip_68px:I = 0x7f0812f4

.field public static final vip_icon_crown:I = 0x7f0812f5

.field public static final vip_icon_discount:I = 0x7f0812f6

.field public static final vip_icon_red_packet:I = 0x7f0812f7

.field public static final vip_im_ad:I = 0x7f0812f8

.field public static final vip_im_id:I = 0x7f0812f9

.field public static final vip_im_ocr:I = 0x7f0812fa

.field public static final vip_im_pizzle:I = 0x7f0812fb

.field public static final water_delete_button:I = 0x7f0812fc

.field public static final water_resize_knob:I = 0x7f0812fd

.field public static final watermark_item_content:I = 0x7f0812fe

.field public static final watermark_share:I = 0x7f0812ff

.field public static final web_bg_btn:I = 0x7f081300

.field public static final web_ic_menu_more:I = 0x7f081301

.field public static final web_ic_menu_more_disable:I = 0x7f081302

.field public static final web_ic_menu_more_selector:I = 0x7f081303

.field public static final web_ic_refresh:I = 0x7f081304

.field public static final web_id_card_80px:I = 0x7f081305

.field public static final web_progress_horizonntal:I = 0x7f081306

.field public static final wechat_space:I = 0x7f081307

.field public static final whatsnew_524:I = 0x7f081308

.field public static final widget_list_view_divider:I = 0x7f081309

.field public static final word_export_album:I = 0x7f08130a

.field public static final word_export_pdf:I = 0x7f08130b

.field public static final workflow_arrow_right:I = 0x7f08130c

.field public static final workflow_edit:I = 0x7f08130d

.field public static final workflow_email_pending:I = 0x7f08130e

.field public static final workflow_feedback_banner:I = 0x7f08130f

.field public static final workflow_feedback_button_bg:I = 0x7f081310

.field public static final workflow_ic_checked:I = 0x7f081311

.field public static final workflow_ic_delete:I = 0x7f081312

.field public static final workflow_ic_uncheck:I = 0x7f081313

.field public static final workflow_icon_email:I = 0x7f081314

.field public static final workflow_vip:I = 0x7f081315

.field public static final yes_yellow:I = 0x7f081316

.field public static final yubikit_dialog_bg:I = 0x7f081317

.field public static final yubikit_ykfamily:I = 0x7f081318

.field public static final yubikit_ykfamily_on_grey:I = 0x7f081319

.field public static final zoom_in:I = 0x7f08131a

.field public static final zoom_out:I = 0x7f08131b

.field public static final zoom_slider:I = 0x7f08131c


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

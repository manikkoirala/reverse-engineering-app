.class public final Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity$Companion;
.super Ljava/lang/Object;
.source "TrimEnhanceAnimConfigEntity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080(I)Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_3

    .line 7
    .line 8
    const/4 v1, 0x2

    .line 9
    const/4 v2, 0x1

    .line 10
    if-eq p1, v2, :cond_2

    .line 11
    .line 12
    if-eq p1, v1, :cond_1

    .line 13
    .line 14
    const/4 v3, 0x3

    .line 15
    if-eq p1, v3, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->getTrimFlag()I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    or-int/2addr p1, v2

    .line 23
    or-int/2addr p1, v1

    .line 24
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->setTrimFlag(I)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->getEnhanceFlag()I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    or-int/2addr p1, v2

    .line 32
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->setEnhanceFlag(I)V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->getTrimFlag()I

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    or-int/2addr p1, v2

    .line 41
    or-int/2addr p1, v1

    .line 42
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->setTrimFlag(I)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->getTrimFlag()I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    or-int/2addr p1, v1

    .line 51
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->setTrimFlag(I)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_3
    const/4 p1, 0x0

    .line 56
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->setEnhanceFlag(I)V

    .line 57
    .line 58
    .line 59
    :goto_0
    return-object v0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

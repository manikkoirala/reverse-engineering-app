.class public final Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;
.super Ljava/lang/Object;
.source "TrimEnhanceAnimConfigEntity.kt"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Companion:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private enhanceFlag:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "enhanceFlag"
    .end annotation
.end field

.field private enhanceFrame:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "enhanceFrame"
    .end annotation
.end field

.field private enhanceInterval:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "enhanceInterval"
    .end annotation
.end field

.field private enhanceLineWaitTime:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "enhanceLineWaitTime"
    .end annotation
.end field

.field private preEnhanceFrame:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "preEnhanceFrame"
    .end annotation
.end field

.field private preEnhanceInterval:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "preEnhanceInterval"
    .end annotation
.end field

.field private trimFlag:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "trimFlag"
    .end annotation
.end field

.field private trimFrame:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "trimFrame"
    .end annotation
.end field

.field private trimInterval:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "trimInterval"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->Companion:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x14

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->trimFrame:I

    .line 7
    .line 8
    const-wide/16 v0, 0x2a

    .line 9
    .line 10
    iput-wide v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->trimInterval:J

    .line 11
    .line 12
    const/16 v0, 0xf

    .line 13
    .line 14
    iput v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceFrame:I

    .line 15
    .line 16
    const-wide/16 v1, 0x19

    .line 17
    .line 18
    iput-wide v1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceInterval:J

    .line 19
    .line 20
    const/4 v1, 0x2

    .line 21
    iput v1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceFlag:I

    .line 22
    .line 23
    const-wide/16 v1, 0xfa

    .line 24
    .line 25
    iput-wide v1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceLineWaitTime:J

    .line 26
    .line 27
    iput v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->preEnhanceFrame:I

    .line 28
    .line 29
    const-wide/16 v0, 0x14

    .line 30
    .line 31
    iput-wide v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->preEnhanceInterval:J

    .line 32
    .line 33
    return-void
.end method


# virtual methods
.method public final getEnhanceFlag()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceFlag:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final getEnhanceFrame()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceFrame:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final getEnhanceInterval()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceInterval:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final getEnhanceLineWaitTime()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceLineWaitTime:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final getPreEnhanceFrame()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->preEnhanceFrame:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final getPreEnhanceInterval()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->preEnhanceInterval:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final getTrimFlag()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->trimFlag:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final getTrimFrame()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->trimFrame:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final getTrimInterval()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->trimInterval:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final isCoveringBgWithBlack()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->trimFlag:I

    .line 2
    .line 3
    and-int/lit8 v0, v0, 0x2

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final isShowingPreAnimBeforeEnhance()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceFlag:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    and-int/2addr v0, v1

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v1, 0x0

    .line 9
    :goto_0
    return v1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final isUsingBitmapForEnhanceLine()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceFlag:I

    .line 2
    .line 3
    and-int/lit8 v0, v0, 0x2

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final isUsingNewTrimLib()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->trimFlag:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    and-int/2addr v0, v1

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v1, 0x0

    .line 9
    :goto_0
    return v1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final setEnhanceFlag(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceFlag:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setEnhanceFrame(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceFrame:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setEnhanceInterval(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceInterval:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setEnhanceLineWaitTime(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->enhanceLineWaitTime:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setPreEnhanceFrame(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->preEnhanceFrame:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setPreEnhanceInterval(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->preEnhanceInterval:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setTrimFlag(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->trimFlag:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setTrimFrame(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->trimFrame:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setTrimInterval(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->trimInterval:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {p0}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "toJsonString(this)"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.class public final Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;
.super Ljava/lang/Object;
.source "TrimEnhanceAnimationManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$UpdateStoredBitmapListener;,
        Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;,
        Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static 〇0〇O0088o:J

.field private static final 〇O00:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇O〇:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇8O0〇8:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Lcom/intsig/camscanner/view/ImageEditView;

.field private OO0o〇〇:Z

.field private OO0o〇〇〇〇0:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;

.field private Oo08:Landroid/graphics/Bitmap;

.field private Oooo8o0〇:Z

.field private final oO80:Landroid/os/Handler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇0:[I

.field private final 〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$UpdateStoredBitmapListener;

.field private 〇80〇808〇O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8o8o〇:Ljava/util/concurrent/locks/ReentrantLock;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O8o08O:I

.field private final 〇o00〇〇Oo:Landroid/widget/ProgressBar;

.field private final 〇o〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

.field private 〇〇808〇:J

.field private 〇〇888:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇O〇:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$Companion;

    .line 8
    .line 9
    new-instance v0, Landroid/graphics/Path;

    .line 10
    .line 11
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 12
    .line 13
    .line 14
    sput-object v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇O00:Landroid/graphics/Path;

    .line 15
    .line 16
    new-instance v0, Landroid/graphics/Paint;

    .line 17
    .line 18
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇〇8O0〇8:Landroid/graphics/Paint;

    .line 22
    .line 23
    const/4 v1, 0x1

    .line 24
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 25
    .line 26
    .line 27
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 30
    .line 31
    .line 32
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 33
    .line 34
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    if-eqz v2, :cond_0

    .line 39
    .line 40
    const v3, 0x7f06009f

    .line 41
    .line 42
    .line 43
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    .line 49
    .line 50
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    const/4 v2, 0x4

    .line 55
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    int-to-float v1, v1

    .line 60
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 61
    .line 62
    .line 63
    const-wide/16 v0, -0x1

    .line 64
    .line 65
    sput-wide v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇0〇O0088o:J

    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public constructor <init>(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$UpdateStoredBitmapListener;Landroid/widget/ProgressBar;Lcom/intsig/camscanner/loadimage/RotateBitmap;Lcom/intsig/camscanner/view/ImageEditView;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$UpdateStoredBitmapListener;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇o00〇〇Oo:Landroid/widget/ProgressBar;

    .line 7
    .line 8
    iput-object p3, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇o〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 9
    .line 10
    iput-object p4, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 11
    .line 12
    new-instance p1, Landroid/os/Handler;

    .line 13
    .line 14
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    invoke-direct {p1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 19
    .line 20
    .line 21
    iput-object p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->oO80:Landroid/os/Handler;

    .line 22
    .line 23
    new-instance p1, Ljava/util/ArrayList;

    .line 24
    .line 25
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 26
    .line 27
    .line 28
    iput-object p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇80〇808〇O:Ljava/util/List;

    .line 29
    .line 30
    new-instance p1, Ljava/util/concurrent/locks/ReentrantLock;

    .line 31
    .line 32
    invoke-direct {p1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 33
    .line 34
    .line 35
    iput-object p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇8o8o〇:Ljava/util/concurrent/locks/ReentrantLock;

    .line 36
    .line 37
    const/4 p1, 0x1

    .line 38
    iput-boolean p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇:Z

    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method public static synthetic O8(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8ooOoo〇(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final O8ooOoo〇(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;I)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/ImageEditView;->setPreEnhanceProcess(I)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic OO0o〇〇(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)[I
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->o〇0:[I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇〇888:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final OOO〇O0(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Z)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    const/4 v2, 0x0

    .line 12
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object p0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 16
    .line 17
    if-eqz p0, :cond_1

    .line 18
    .line 19
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/ImageEditView;->setNeedTrimCover(Z)V

    .line 20
    .line 21
    .line 22
    :cond_1
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇oo〇(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/graphics/Bitmap;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final Oo8Oo00oo(Landroid/graphics/Bitmap;[II)V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    if-eqz p2, :cond_4

    .line 2
    .line 3
    if-eqz p1, :cond_4

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-direct {v0, v1, v2, p2}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;-><init>(II[I)V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;

    .line 23
    .line 24
    goto :goto_2

    .line 25
    :cond_0
    if-nez v0, :cond_1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;->O8([I)V

    .line 29
    .line 30
    .line 31
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;

    .line 32
    .line 33
    if-nez v0, :cond_2

    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;->〇o〇(I)V

    .line 41
    .line 42
    .line 43
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;

    .line 44
    .line 45
    if-nez v0, :cond_3

    .line 46
    .line 47
    goto :goto_2

    .line 48
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;->〇o00〇〇Oo(I)V

    .line 53
    .line 54
    .line 55
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$UpdateStoredBitmapListener;

    .line 56
    .line 57
    if-eqz v0, :cond_5

    .line 58
    .line 59
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$UpdateStoredBitmapListener;->〇080(Landroid/graphics/Bitmap;)V

    .line 60
    .line 61
    .line 62
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇o00〇〇Oo:Landroid/widget/ProgressBar;

    .line 63
    .line 64
    if-nez v0, :cond_6

    .line 65
    .line 66
    goto :goto_3

    .line 67
    :cond_6
    invoke-virtual {v0, p3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 68
    .line 69
    .line 70
    :goto_3
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 71
    .line 72
    if-eqz v0, :cond_7

    .line 73
    .line 74
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/view/ImageEditView;->setTrimFrameBorder([I)V

    .line 75
    .line 76
    .line 77
    :cond_7
    iget-object p2, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 78
    .line 79
    if-eqz p2, :cond_8

    .line 80
    .line 81
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/view/ImageEditView;->setTrimProcess(I)V

    .line 82
    .line 83
    .line 84
    :cond_8
    iget-object p2, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 85
    .line 86
    if-eqz p2, :cond_a

    .line 87
    .line 88
    iget-object p3, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇o〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 89
    .line 90
    if-eqz p3, :cond_9

    .line 91
    .line 92
    invoke-virtual {p3}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->O8()I

    .line 93
    .line 94
    .line 95
    move-result p3

    .line 96
    goto :goto_4

    .line 97
    :cond_9
    const/4 p3, 0x0

    .line 98
    :goto_4
    invoke-virtual {p2, p1, p3}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇0〇O0088o(Landroid/graphics/Bitmap;I)V

    .line 99
    .line 100
    .line 101
    :cond_a
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private static final OoO8(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 7
    .line 8
    if-nez p0, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/ImageEditView;->setCoveringFullBlackBg(Z)V

    .line 13
    .line 14
    .line 15
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->Oo08:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final O〇8O8〇008(Landroid/app/Activity;)V
    .locals 6

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    new-instance v0, LOoo8〇〇/oO80;

    .line 4
    .line 5
    invoke-direct {v0, p0}, LOoo8〇〇/oO80;-><init>(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    :cond_0
    const/4 v0, 0x1

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇:Z

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    iput-boolean v1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->Oooo8o0〇:Z

    .line 16
    .line 17
    sget-object v2, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;

    .line 18
    .line 19
    invoke-virtual {v2}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-virtual {v2}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->getPreEnhanceFrame()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    const/4 v3, 0x0

    .line 28
    :cond_1
    iget-boolean v4, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇:Z

    .line 29
    .line 30
    if-eqz v4, :cond_a

    .line 31
    .line 32
    iget-object v4, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇8o8o〇:Ljava/util/concurrent/locks/ReentrantLock;

    .line 33
    .line 34
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->getHoldCount()I

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    if-lez v4, :cond_2

    .line 39
    .line 40
    iget-object v4, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇8o8o〇:Ljava/util/concurrent/locks/ReentrantLock;

    .line 41
    .line 42
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 43
    .line 44
    .line 45
    :cond_2
    const-wide/16 v4, 0xc8

    .line 46
    .line 47
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 48
    .line 49
    .line 50
    if-lt v3, v2, :cond_3

    .line 51
    .line 52
    iput-boolean v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->Oooo8o0〇:Z

    .line 53
    .line 54
    add-int/lit8 v3, v3, -0x1

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_3
    if-gtz v3, :cond_4

    .line 58
    .line 59
    iput-boolean v1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->Oooo8o0〇:Z

    .line 60
    .line 61
    add-int/lit8 v3, v3, 0x1

    .line 62
    .line 63
    :cond_4
    :goto_0
    iget-boolean v4, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇:Z

    .line 64
    .line 65
    if-eqz v4, :cond_a

    .line 66
    .line 67
    iget-object v4, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇8o8o〇:Ljava/util/concurrent/locks/ReentrantLock;

    .line 68
    .line 69
    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 70
    .line 71
    .line 72
    :goto_1
    if-ltz v3, :cond_5

    .line 73
    .line 74
    if-gt v3, v2, :cond_5

    .line 75
    .line 76
    const/4 v4, 0x1

    .line 77
    goto :goto_2

    .line 78
    :cond_5
    const/4 v4, 0x0

    .line 79
    :goto_2
    if-eqz v4, :cond_1

    .line 80
    .line 81
    iget-object v4, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 82
    .line 83
    if-eqz v4, :cond_6

    .line 84
    .line 85
    iget-boolean v5, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->Oooo8o0〇:Z

    .line 86
    .line 87
    xor-int/2addr v5, v0

    .line 88
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/view/ImageEditView;->setEnhanceLineFromBottom(Z)V

    .line 89
    .line 90
    .line 91
    :cond_6
    iget-object v4, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 92
    .line 93
    if-eqz v4, :cond_7

    .line 94
    .line 95
    iget-boolean v5, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->Oooo8o0〇:Z

    .line 96
    .line 97
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/view/ImageEditView;->〇〇〇0〇〇0(Z)V

    .line 98
    .line 99
    .line 100
    :cond_7
    iget-boolean v4, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->Oooo8o0〇:Z

    .line 101
    .line 102
    if-eqz v4, :cond_8

    .line 103
    .line 104
    const/4 v4, -0x1

    .line 105
    goto :goto_3

    .line 106
    :cond_8
    const/4 v4, 0x1

    .line 107
    :goto_3
    add-int/2addr v3, v4

    .line 108
    add-int/lit8 v4, v3, 0x1

    .line 109
    .line 110
    mul-int/lit8 v4, v4, 0x64

    .line 111
    .line 112
    div-int/2addr v4, v2

    .line 113
    add-int/2addr v4, v0

    .line 114
    if-eqz p1, :cond_9

    .line 115
    .line 116
    new-instance v5, LOoo8〇〇/〇80〇808〇O;

    .line 117
    .line 118
    invoke-direct {v5, p0, v4}, LOoo8〇〇/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;I)V

    .line 119
    .line 120
    .line 121
    invoke-virtual {p1, v5}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 122
    .line 123
    .line 124
    :cond_9
    sget-object v4, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;

    .line 125
    .line 126
    invoke-virtual {v4}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 127
    .line 128
    .line 129
    move-result-object v4

    .line 130
    invoke-virtual {v4}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->getPreEnhanceInterval()J

    .line 131
    .line 132
    .line 133
    move-result-wide v4

    .line 134
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 135
    .line 136
    .line 137
    goto :goto_1

    .line 138
    :cond_a
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method private final o0ooO()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇8o8o〇:Ljava/util/concurrent/locks/ReentrantLock;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o8(I)V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    const-string v0, "TrimAnimationM"

    .line 2
    .line 3
    const-string v1, "updateEnhanceSingleFrame"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇o00〇〇Oo:Landroid/widget/ProgressBar;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 14
    .line 15
    .line 16
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 17
    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/ImageEditView;->setEnhanceProcess(I)V

    .line 21
    .line 22
    .line 23
    :cond_1
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final o800o8O(IJZI)V
    .locals 18

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move/from16 v10, p1

    .line 4
    .line 5
    iget-object v1, v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->o〇0:[I

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    const/16 v2, 0x8

    .line 10
    .line 11
    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([II)[I

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const-string v2, "copyOf(this, newSize)"

    .line 16
    .line 17
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v1, 0x0

    .line 22
    :goto_0
    move-object v11, v1

    .line 23
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇o()V

    .line 24
    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/nativelib/BookSplitter;->InitDewarpRotateProcess()I

    .line 27
    .line 28
    .line 29
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 30
    .line 31
    .line 32
    move-result-wide v12

    .line 33
    const/4 v1, 0x0

    .line 34
    const/4 v14, 0x0

    .line 35
    :goto_1
    const-string v15, "TrimAnimationM"

    .line 36
    .line 37
    if-ge v14, v10, :cond_4

    .line 38
    .line 39
    iget-object v1, v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->Oo08:Landroid/graphics/Bitmap;

    .line 40
    .line 41
    invoke-static {v1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->OO0o〇〇〇〇0(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 42
    .line 43
    .line 44
    move-result-object v9

    .line 45
    const/4 v1, 0x1

    .line 46
    move/from16 v8, p4

    .line 47
    .line 48
    if-ne v8, v1, :cond_1

    .line 49
    .line 50
    iget-object v2, v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->o〇0:[I

    .line 51
    .line 52
    const/16 v16, 0x1

    .line 53
    .line 54
    const/16 v17, 0x0

    .line 55
    .line 56
    move-object v1, v9

    .line 57
    move-object v3, v11

    .line 58
    move v4, v14

    .line 59
    move/from16 v5, p1

    .line 60
    .line 61
    move/from16 v6, p1

    .line 62
    .line 63
    move/from16 v7, p5

    .line 64
    .line 65
    move/from16 v8, v16

    .line 66
    .line 67
    move-wide/from16 p2, v12

    .line 68
    .line 69
    move-object v12, v9

    .line 70
    move/from16 v9, v17

    .line 71
    .line 72
    invoke-static/range {v1 .. v9}, Lcom/intsig/nativelib/BookSplitter;->DrawDewarpRotateProcessImagePos(Landroid/graphics/Bitmap;[I[IIIIIII)I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    goto :goto_2

    .line 77
    :cond_1
    move-wide/from16 p2, v12

    .line 78
    .line 79
    move-object v12, v9

    .line 80
    iget-object v1, v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->o〇0:[I

    .line 81
    .line 82
    invoke-static {v12, v1, v11, v14, v10}, Lcom/intsig/nativelib/BookSplitter;->DrawDewarpProcessImagePos(Landroid/graphics/Bitmap;[I[III)I

    .line 83
    .line 84
    .line 85
    move-result v1

    .line 86
    :goto_2
    iput v1, v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇O8o08O:I

    .line 87
    .line 88
    new-instance v2, Ljava/lang/StringBuilder;

    .line 89
    .line 90
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .line 92
    .line 93
    const-string v3, "rotateWithAnim = "

    .line 94
    .line 95
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    const-string v1, " , nw is "

    .line 102
    .line 103
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    const-string v1, " frame"

    .line 110
    .line 111
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v1

    .line 118
    invoke-static {v15, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    if-eqz v12, :cond_3

    .line 122
    .line 123
    add-int/lit8 v1, v10, -0x1

    .line 124
    .line 125
    if-ge v14, v1, :cond_2

    .line 126
    .line 127
    mul-int/lit8 v1, v14, 0x64

    .line 128
    .line 129
    div-int/2addr v1, v10

    .line 130
    goto :goto_3

    .line 131
    :cond_2
    const/16 v1, 0x64

    .line 132
    .line 133
    :goto_3
    iget-object v2, v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇80〇808〇O:Ljava/util/List;

    .line 134
    .line 135
    invoke-interface {v2, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    .line 137
    .line 138
    invoke-static {v11}, Lcom/intsig/office/java/util/Arrays;->toString([I)Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v2

    .line 142
    new-instance v3, Ljava/lang/StringBuilder;

    .line 143
    .line 144
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    .line 146
    .line 147
    const-string v4, "executeBookLibAnim: trimFrameBounds: "

    .line 148
    .line 149
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v2

    .line 159
    invoke-static {v15, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    iget-object v2, v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->oO80:Landroid/os/Handler;

    .line 163
    .line 164
    new-instance v3, LOoo8〇〇/〇〇888;

    .line 165
    .line 166
    invoke-direct {v3, v0, v12, v11, v1}, LOoo8〇〇/〇〇888;-><init>(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/graphics/Bitmap;[II)V

    .line 167
    .line 168
    .line 169
    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 170
    .line 171
    .line 172
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 173
    .line 174
    .line 175
    move-result-wide v1

    .line 176
    iput-wide v1, v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇〇888:J

    .line 177
    .line 178
    add-int/lit8 v14, v14, 0x1

    .line 179
    .line 180
    move-wide/from16 v12, p2

    .line 181
    .line 182
    goto/16 :goto_1

    .line 183
    .line 184
    :cond_4
    move-wide/from16 p2, v12

    .line 185
    .line 186
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 187
    .line 188
    .line 189
    move-result-wide v1

    .line 190
    move-wide/from16 v3, p2

    .line 191
    .line 192
    sub-long/2addr v1, v3

    .line 193
    int-to-long v3, v10

    .line 194
    div-long/2addr v1, v3

    .line 195
    new-instance v3, Ljava/lang/StringBuilder;

    .line 196
    .line 197
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 198
    .line 199
    .line 200
    const-string v4, "each frame cost = "

    .line 201
    .line 202
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    .line 204
    .line 205
    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 206
    .line 207
    .line 208
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 209
    .line 210
    .line 211
    move-result-object v1

    .line 212
    invoke-static {v15, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    .line 214
    .line 215
    const-wide/16 v1, 0x50

    .line 216
    .line 217
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 218
    .line 219
    .line 220
    return-void
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->o〇O8〇〇o(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final oo〇(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/app/Activity;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O〇8O8〇008(Landroid/app/Activity;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇oOO8O8(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final o〇8(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->isShowingPreAnimBeforeEnhance()Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/ImageEditView;->setEnhanceLineFromBottom(Z)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final o〇O8〇〇o(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;I)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    add-int/lit8 p1, p1, 0x1

    .line 7
    .line 8
    mul-int/lit8 p1, p1, 0x64

    .line 9
    .line 10
    sget-object v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->getEnhanceFrame()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    div-int/2addr p1, v0

    .line 21
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->o8(I)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇00(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇0000OOO(Landroid/app/Activity;IJLcom/intsig/camscanner/imagescanner/ImageScannerViewModel;)V
    .locals 8

    .line 1
    new-instance v7, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$executeScannerLibAnim$scannerProcessListener$1;

    .line 2
    .line 3
    move-object v0, v7

    .line 4
    move-object v1, p1

    .line 5
    move-object v2, p5

    .line 6
    move-object v3, p0

    .line 7
    move v4, p2

    .line 8
    move-wide v5, p3

    .line 9
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$executeScannerLibAnim$scannerProcessListener$1;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/imagescanner/ImageScannerViewModel;Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;IJ)V

    .line 10
    .line 11
    .line 12
    const-string p1, "TrimAnimationM"

    .line 13
    .line 14
    const-string p2, "dewarpImagePlane beign"

    .line 15
    .line 16
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇o()V

    .line 20
    .line 21
    .line 22
    if-eqz p5, :cond_0

    .line 23
    .line 24
    invoke-virtual {p5}, Lcom/intsig/camscanner/imagescanner/ImageScannerViewModel;->O8888()I

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    invoke-static {p1, v7}, Lcom/intsig/scanner/ScannerEngine;->setProcessListener(ILcom/intsig/scanner/ScannerEngine$ScannerProcessListener;)I

    .line 29
    .line 30
    .line 31
    :cond_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private final 〇00〇8()V
    .locals 6

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget-wide v2, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇〇808〇:J

    .line 6
    .line 7
    sub-long/2addr v0, v2

    .line 8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 9
    .line 10
    .line 11
    move-result-wide v2

    .line 12
    new-instance v4, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v5, "dewarpImagePlane ok consume "

    .line 18
    .line 19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string v0, " , finish at "

    .line 26
    .line 27
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    const-string v1, "TrimAnimationM"

    .line 38
    .line 39
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->oo〇(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇00(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic 〇8o8o〇()Landroid/graphics/Paint;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇〇8O0〇8:Landroid/graphics/Paint;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇O00(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/graphics/Bitmap;[II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->Oo8Oo00oo(Landroid/graphics/Bitmap;[II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private static final 〇O888o0o(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/graphics/Bitmap;[II)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->Oo8Oo00oo(Landroid/graphics/Bitmap;[II)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method public static final synthetic 〇O8o08O()Landroid/graphics/Path;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇O00:Landroid/graphics/Path;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇O〇(J)V
    .locals 0

    .line 1
    sput-wide p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇0〇O0088o:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇o()V
    .locals 2

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iput-wide v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇〇808〇:J

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OOO〇O0(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇oOO8O8(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageEditView;->o〇0OOo〇0()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final 〇oo〇(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/graphics/Bitmap;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v1, 0x0

    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/ImageEditView;->setCoveringFullBlackBg(Z)V

    .line 13
    .line 14
    .line 15
    :goto_0
    iget-object p0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->O8:Lcom/intsig/camscanner/view/ImageEditView;

    .line 16
    .line 17
    if-eqz p0, :cond_1

    .line 18
    .line 19
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/ImageEditView;->setBitmapEnhanced(Landroid/graphics/Bitmap;)V

    .line 20
    .line 21
    .line 22
    :cond_1
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OoO8(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic 〇〇808〇()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇0〇O0088o:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/graphics/Bitmap;[II)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇O888o0o(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/graphics/Bitmap;[II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method


# virtual methods
.method public final O8〇o()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final oo88o8O(Landroid/app/Activity;Landroid/graphics/Bitmap;)V
    .locals 4

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇8o8o〇:Ljava/util/concurrent/locks/ReentrantLock;

    .line 2
    .line 3
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 4
    .line 5
    const-wide/16 v2, 0xdac

    .line 6
    .line 7
    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :catch_0
    move-exception v0

    .line 12
    const-string v1, "TrimAnimationM"

    .line 13
    .line 14
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 15
    .line 16
    .line 17
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 22
    .line 23
    .line 24
    :goto_0
    const/4 v0, 0x0

    .line 25
    iput-boolean v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇:Z

    .line 26
    .line 27
    if-eqz p1, :cond_0

    .line 28
    .line 29
    new-instance v1, LOoo8〇〇/〇o00〇〇Oo;

    .line 30
    .line 31
    invoke-direct {v1, p0, p2}, LOoo8〇〇/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/graphics/Bitmap;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p1, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 35
    .line 36
    .line 37
    :cond_0
    sget-object p2, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;

    .line 38
    .line 39
    invoke-virtual {p2}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 40
    .line 41
    .line 42
    move-result-object p2

    .line 43
    invoke-virtual {p2}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->getEnhanceFrame()I

    .line 44
    .line 45
    .line 46
    move-result p2

    .line 47
    :goto_1
    if-ge v0, p2, :cond_2

    .line 48
    .line 49
    if-eqz p1, :cond_1

    .line 50
    .line 51
    new-instance v1, LOoo8〇〇/〇o〇;

    .line 52
    .line 53
    invoke-direct {v1, p0, v0}, LOoo8〇〇/〇o〇;-><init>(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;I)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 57
    .line 58
    .line 59
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;

    .line 60
    .line 61
    invoke-virtual {v1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-virtual {v1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->getEnhanceInterval()J

    .line 66
    .line 67
    .line 68
    move-result-wide v1

    .line 69
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    .line 70
    .line 71
    .line 72
    add-int/lit8 v0, v0, 0x1

    .line 73
    .line 74
    goto :goto_1

    .line 75
    :cond_2
    if-eqz p1, :cond_3

    .line 76
    .line 77
    new-instance p2, LOoo8〇〇/O8;

    .line 78
    .line 79
    invoke-direct {p2, p0}, LOoo8〇〇/O8;-><init>(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V

    .line 80
    .line 81
    .line 82
    invoke-virtual {p1, p2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 83
    .line 84
    .line 85
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇8o8o〇:Ljava/util/concurrent/locks/ReentrantLock;

    .line 86
    .line 87
    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->getHoldCount()I

    .line 88
    .line 89
    .line 90
    move-result p1

    .line 91
    if-lez p1, :cond_4

    .line 92
    .line 93
    iget-object p1, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇8o8o〇:Ljava/util/concurrent/locks/ReentrantLock;

    .line 94
    .line 95
    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 96
    .line 97
    .line 98
    :cond_4
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public final o〇〇0〇(Landroid/graphics/Bitmap;[IDDLcom/intsig/camscanner/imagescanner/ImageScannerViewModel;Landroid/app/Activity;ZZI)I
    .locals 18
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    move-object/from16 v7, p2

    .line 4
    .line 5
    move-object/from16 v8, p7

    .line 6
    .line 7
    move-object/from16 v9, p8

    .line 8
    .line 9
    move/from16 v10, p9

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇o00〇〇Oo()V

    .line 14
    .line 15
    .line 16
    if-eqz v9, :cond_0

    .line 17
    .line 18
    new-instance v1, LOoo8〇〇/Oo08;

    .line 19
    .line 20
    invoke-direct {v1, v6, v10}, LOoo8〇〇/Oo08;-><init>(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Z)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v9, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-direct {v6, v1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->o〇8(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;)V

    .line 31
    .line 32
    .line 33
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->o0ooO()V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {v1}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->getTrimInterval()J

    .line 41
    .line 42
    .line 43
    move-result-wide v3

    .line 44
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->getTrimFrame()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    move-object/from16 v0, p1

    .line 53
    .line 54
    move-wide/from16 v11, p3

    .line 55
    .line 56
    move-wide/from16 v13, p5

    .line 57
    .line 58
    invoke-static {v0, v11, v12, v13, v14}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->OO0o〇〇(Landroid/graphics/Bitmap;DD)Landroid/util/Pair;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    .line 63
    .line 64
    check-cast v1, Landroid/graphics/Bitmap;

    .line 65
    .line 66
    iput-object v1, v6, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->Oo08:Landroid/graphics/Bitmap;

    .line 67
    .line 68
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 69
    .line 70
    check-cast v0, Ljava/lang/Double;

    .line 71
    .line 72
    const/4 v12, 0x0

    .line 73
    if-eqz v7, :cond_2

    .line 74
    .line 75
    array-length v1, v7

    .line 76
    new-array v1, v1, [I

    .line 77
    .line 78
    iput-object v1, v6, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->o〇0:[I

    .line 79
    .line 80
    array-length v5, v7

    .line 81
    const/4 v13, 0x0

    .line 82
    :goto_0
    if-ge v13, v5, :cond_1

    .line 83
    .line 84
    aget v14, v7, v13

    .line 85
    .line 86
    int-to-double v14, v14

    .line 87
    const-string v11, "trimScale"

    .line 88
    .line 89
    invoke-static {v0, v11}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    .line 93
    .line 94
    .line 95
    move-result-wide v16

    .line 96
    mul-double v14, v14, v16

    .line 97
    .line 98
    double-to-int v11, v14

    .line 99
    aput v11, v1, v13

    .line 100
    .line 101
    add-int/lit8 v13, v13, 0x1

    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_1
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 105
    .line 106
    goto :goto_1

    .line 107
    :cond_2
    const/4 v0, 0x0

    .line 108
    :goto_1
    const-string v11, "TrimAnimationM"

    .line 109
    .line 110
    if-nez v0, :cond_3

    .line 111
    .line 112
    const-string v0, "ERROR mCurrentThumbBounds is null, therefore skip trim animation"

    .line 113
    .line 114
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    return v12

    .line 118
    :cond_3
    sget-object v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;

    .line 119
    .line 120
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->isUsingNewTrimLib()Z

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    const/4 v13, 0x1

    .line 129
    if-ne v0, v13, :cond_4

    .line 130
    .line 131
    move-object/from16 v0, p0

    .line 132
    .line 133
    move v1, v2

    .line 134
    move-wide v2, v3

    .line 135
    move/from16 v4, p10

    .line 136
    .line 137
    move/from16 v5, p11

    .line 138
    .line 139
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->o800o8O(IJZI)V

    .line 140
    .line 141
    .line 142
    goto :goto_2

    .line 143
    :cond_4
    if-nez v0, :cond_5

    .line 144
    .line 145
    move-object/from16 v0, p0

    .line 146
    .line 147
    move-object/from16 v1, p8

    .line 148
    .line 149
    move-object/from16 v5, p7

    .line 150
    .line 151
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇0000OOO(Landroid/app/Activity;IJLcom/intsig/camscanner/imagescanner/ImageScannerViewModel;)V

    .line 152
    .line 153
    .line 154
    :cond_5
    :goto_2
    if-eqz v8, :cond_6

    .line 155
    .line 156
    invoke-virtual/range {p7 .. p7}, Lcom/intsig/camscanner/imagescanner/ImageScannerViewModel;->O8888()I

    .line 157
    .line 158
    .line 159
    move-result v0

    .line 160
    invoke-virtual {v8, v7}, Lcom/intsig/camscanner/imagescanner/ImageScannerViewModel;->oo08OO〇0([I)V

    .line 161
    .line 162
    .line 163
    const/4 v1, 0x0

    .line 164
    invoke-static {v0, v1}, Lcom/intsig/scanner/ScannerEngine;->setProcessListener(ILcom/intsig/scanner/ScannerEngine$ScannerProcessListener;)I

    .line 165
    .line 166
    .line 167
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇00〇8()V

    .line 168
    .line 169
    .line 170
    :cond_6
    iget-object v0, v6, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇80〇808〇O:Ljava/util/List;

    .line 171
    .line 172
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 173
    .line 174
    .line 175
    move-result v0

    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    .line 177
    .line 178
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    .line 180
    .line 181
    const-string v2, "start to recycle Bitmap array, size = "

    .line 182
    .line 183
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    .line 185
    .line 186
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 187
    .line 188
    .line 189
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object v0

    .line 193
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    .line 195
    .line 196
    iget-object v0, v6, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇80〇808〇O:Ljava/util/List;

    .line 197
    .line 198
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 199
    .line 200
    .line 201
    move-result v0

    .line 202
    if-le v0, v13, :cond_7

    .line 203
    .line 204
    iget-object v0, v6, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇80〇808〇O:Ljava/util/List;

    .line 205
    .line 206
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 207
    .line 208
    .line 209
    move-result v0

    .line 210
    sub-int/2addr v0, v13

    .line 211
    const/4 v1, 0x0

    .line 212
    :goto_3
    if-ge v1, v0, :cond_7

    .line 213
    .line 214
    iget-object v2, v6, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇80〇808〇O:Ljava/util/List;

    .line 215
    .line 216
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 217
    .line 218
    .line 219
    move-result-object v2

    .line 220
    check-cast v2, Landroid/graphics/Bitmap;

    .line 221
    .line 222
    invoke-static {v2}, Lcom/intsig/camscanner/util/Util;->OOo0O(Landroid/graphics/Bitmap;)V

    .line 223
    .line 224
    .line 225
    add-int/lit8 v1, v1, 0x1

    .line 226
    .line 227
    goto :goto_3

    .line 228
    :cond_7
    iget-object v0, v6, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇80〇808〇O:Ljava/util/List;

    .line 229
    .line 230
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 231
    .line 232
    .line 233
    if-eqz v10, :cond_8

    .line 234
    .line 235
    sget-object v0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;

    .line 236
    .line 237
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigManager;->〇080()Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;

    .line 238
    .line 239
    .line 240
    move-result-object v0

    .line 241
    invoke-virtual {v0}, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimConfigEntity;->isShowingPreAnimBeforeEnhance()Z

    .line 242
    .line 243
    .line 244
    move-result v0

    .line 245
    if-eqz v0, :cond_8

    .line 246
    .line 247
    new-instance v0, LOoo8〇〇/o〇0;

    .line 248
    .line 249
    invoke-direct {v0, v6, v9}, LOoo8〇〇/o〇0;-><init>(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;Landroid/app/Activity;)V

    .line 250
    .line 251
    .line 252
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 253
    .line 254
    .line 255
    :cond_8
    iget v0, v6, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇O8o08O:I

    .line 256
    .line 257
    iput v12, v6, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->〇O8o08O:I

    .line 258
    .line 259
    return v0
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
.end method

.method public final 〇0〇O0088o(Landroid/app/Activity;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇:Z

    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    new-instance v0, LOoo8〇〇/〇080;

    .line 7
    .line 8
    invoke-direct {v0, p0}, LOoo8〇〇/〇080;-><init>(Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇〇8O0〇8()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/anim/trimenhance/TrimEnhanceAnimationManager$TrimInfo;

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.class public final Lcom/intsig/camscanner/R$layout;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f0d0000

.field public static final abc_action_bar_up_container:I = 0x7f0d0001

.field public static final abc_action_menu_item_layout:I = 0x7f0d0002

.field public static final abc_action_menu_layout:I = 0x7f0d0003

.field public static final abc_action_mode_bar:I = 0x7f0d0004

.field public static final abc_action_mode_close_item_material:I = 0x7f0d0005

.field public static final abc_activity_chooser_view:I = 0x7f0d0006

.field public static final abc_activity_chooser_view_list_item:I = 0x7f0d0007

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f0d0008

.field public static final abc_alert_dialog_material:I = 0x7f0d0009

.field public static final abc_alert_dialog_title_material:I = 0x7f0d000a

.field public static final abc_cascading_menu_item_layout:I = 0x7f0d000b

.field public static final abc_dialog_title_material:I = 0x7f0d000c

.field public static final abc_expanded_menu_layout:I = 0x7f0d000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f0d000e

.field public static final abc_list_menu_item_icon:I = 0x7f0d000f

.field public static final abc_list_menu_item_layout:I = 0x7f0d0010

.field public static final abc_list_menu_item_radio:I = 0x7f0d0011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f0d0012

.field public static final abc_popup_menu_item_layout:I = 0x7f0d0013

.field public static final abc_screen_content_include:I = 0x7f0d0014

.field public static final abc_screen_simple:I = 0x7f0d0015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f0d0016

.field public static final abc_screen_toolbar:I = 0x7f0d0017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f0d0018

.field public static final abc_search_view:I = 0x7f0d0019

.field public static final abc_select_dialog_material:I = 0x7f0d001a

.field public static final abc_tooltip:I = 0x7f0d001b

.field public static final about:I = 0x7f0d001c

.field public static final ac_actionbar_container:I = 0x7f0d001d

.field public static final ac_actionbar_fax:I = 0x7f0d001e

.field public static final ac_auto_composite_preview_bottom_activate:I = 0x7f0d001f

.field public static final ac_auto_composite_root_bottom_activate:I = 0x7f0d0020

.field public static final ac_auto_upload:I = 0x7f0d0021

.field public static final ac_autocomposite_root:I = 0x7f0d0022

.field public static final ac_autocomposite_template:I = 0x7f0d0023

.field public static final ac_batch_ocr_data_result:I = 0x7f0d0024

.field public static final ac_batch_ocr_prepare:I = 0x7f0d0025

.field public static final ac_book_edit:I = 0x7f0d0026

.field public static final ac_book_result:I = 0x7f0d0027

.field public static final ac_cache_clean:I = 0x7f0d0028

.field public static final ac_capture:I = 0x7f0d0029

.field public static final ac_capture_new:I = 0x7f0d002a

.field public static final ac_certificate_folder_home:I = 0x7f0d002b

.field public static final ac_cloud_hint:I = 0x7f0d002c

.field public static final ac_deep_cache_clean:I = 0x7f0d002d

.field public static final ac_developer:I = 0x7f0d002e

.field public static final ac_dirjson_test_layout:I = 0x7f0d002f

.field public static final ac_doc_name_seting:I = 0x7f0d0030

.field public static final ac_feed_back_setting:I = 0x7f0d0031

.field public static final ac_feedback_main:I = 0x7f0d0032

.field public static final ac_fragment_container:I = 0x7f0d0033

.field public static final ac_gallery_preview:I = 0x7f0d0034

.field public static final ac_galleryfolder:I = 0x7f0d0035

.field public static final ac_image_restore_introduction:I = 0x7f0d0036

.field public static final ac_limit_tips:I = 0x7f0d0037

.field public static final ac_ocr_region:I = 0x7f0d0038

.field public static final ac_ocr_result_phone:I = 0x7f0d0039

.field public static final ac_ocr_result_tablet:I = 0x7f0d003a

.field public static final ac_redeem_in_vite_code:I = 0x7f0d003b

.field public static final ac_sync_mark:I = 0x7f0d003c

.field public static final ac_tagedit:I = 0x7f0d003d

.field public static final ac_transparent_for_dialog:I = 0x7f0d003e

.field public static final ac_weixin_care:I = 0x7f0d003f

.field public static final accounts_list:I = 0x7f0d0040

.field public static final action_layout_more:I = 0x7f0d0041

.field public static final action_layout_skip:I = 0x7f0d0042

.field public static final actionbar_add_only:I = 0x7f0d0043

.field public static final actionbar_batch_ocr_result:I = 0x7f0d0044

.field public static final actionbar_batch_ocr_select:I = 0x7f0d0045

.field public static final actionbar_btn:I = 0x7f0d0046

.field public static final actionbar_btn_green:I = 0x7f0d0047

.field public static final actionbar_document_layout:I = 0x7f0d0048

.field public static final actionbar_document_layout_10:I = 0x7f0d0049

.field public static final actionbar_document_only_view_layout:I = 0x7f0d004a

.field public static final actionbar_done_only:I = 0x7f0d004b

.field public static final actionbar_edit_layout:I = 0x7f0d004c

.field public static final actionbar_invoice_detail:I = 0x7f0d004d

.field public static final actionbar_mark_msg_read:I = 0x7f0d004e

.field public static final actionbar_menu_page_list:I = 0x7f0d004f

.field public static final actionbar_menu_page_list_bank_journal:I = 0x7f0d0050

.field public static final actionbar_move_doc:I = 0x7f0d0051

.field public static final actionbar_resort_merged_docs_only:I = 0x7f0d0052

.field public static final actionbar_righttop_btn:I = 0x7f0d0053

.field public static final actionbar_scandone_container:I = 0x7f0d0054

.field public static final actionbar_scandone_layout:I = 0x7f0d0055

.field public static final actionbar_share_only:I = 0x7f0d0056

.field public static final actionbar_sharedoc_menu:I = 0x7f0d0057

.field public static final actionbar_transfer_doc_more:I = 0x7f0d0058

.field public static final activate_dialog:I = 0x7f0d0059

.field public static final activity_account_single_fragment:I = 0x7f0d005a

.field public static final activity_add_signer:I = 0x7f0d005b

.field public static final activity_add_tag:I = 0x7f0d005c

.field public static final activity_app_config_visual:I = 0x7f0d005d

.field public static final activity_authentication:I = 0x7f0d005e

.field public static final activity_bind_phone:I = 0x7f0d005f

.field public static final activity_certificate_detail:I = 0x7f0d0060

.field public static final activity_certificate_edit:I = 0x7f0d0061

.field public static final activity_certificate_modle_more:I = 0x7f0d0062

.field public static final activity_certificate_photo_preview:I = 0x7f0d0063

.field public static final activity_certificate_preview:I = 0x7f0d0064

.field public static final activity_cloud_disk:I = 0x7f0d0065

.field public static final activity_common_frame_layout:I = 0x7f0d0066

.field public static final activity_count_region_edit:I = 0x7f0d0067

.field public static final activity_custom_gallery:I = 0x7f0d0068

.field public static final activity_discount_purchase_a:I = 0x7f0d0069

.field public static final activity_discount_purchase_b:I = 0x7f0d006a

.field public static final activity_discount_purchase_v2:I = 0x7f0d006b

.field public static final activity_doc_to_office:I = 0x7f0d006c

.field public static final activity_draft:I = 0x7f0d006d

.field public static final activity_draft_color_item:I = 0x7f0d006e

.field public static final activity_e_evidence_preview:I = 0x7f0d006f

.field public static final activity_edit_card_detailinfo:I = 0x7f0d0070

.field public static final activity_edit_oversea_card_detailinfo:I = 0x7f0d0071

.field public static final activity_esign_contact:I = 0x7f0d0072

.field public static final activity_esign_detail:I = 0x7f0d0073

.field public static final activity_esign_new:I = 0x7f0d0074

.field public static final activity_esign_page_list:I = 0x7f0d0075

.field public static final activity_esign_transition:I = 0x7f0d0076

.field public static final activity_exception_check:I = 0x7f0d0077

.field public static final activity_filter_setting:I = 0x7f0d0078

.field public static final activity_fragment_container:I = 0x7f0d0079

.field public static final activity_full_screen_ad_layout:I = 0x7f0d007a

.field public static final activity_gp_redeem:I = 0x7f0d007b

.field public static final activity_gp_renewal_redeem:I = 0x7f0d007c

.field public static final activity_guide_gp:I = 0x7f0d007d

.field public static final activity_guide_purchase:I = 0x7f0d007e

.field public static final activity_hand_write_sign:I = 0x7f0d007f

.field public static final activity_id_verify:I = 0x7f0d0080

.field public static final activity_image_to_word:I = 0x7f0d0081

.field public static final activity_login_main:I = 0x7f0d0082

.field public static final activity_long_img_preview:I = 0x7f0d0083

.field public static final activity_main_only_read:I = 0x7f0d0084

.field public static final activity_me_price_list_layout:I = 0x7f0d0085

.field public static final activity_migrate:I = 0x7f0d0086

.field public static final activity_model_capture:I = 0x7f0d0087

.field public static final activity_model_image_scanner:I = 0x7f0d0088

.field public static final activity_model_scan_done:I = 0x7f0d0089

.field public static final activity_move_doc:I = 0x7f0d008a

.field public static final activity_movecopy:I = 0x7f0d008b

.field public static final activity_my_benefits:I = 0x7f0d008c

.field public static final activity_negative_premium:I = 0x7f0d008d

.field public static final activity_new_sign:I = 0x7f0d008e

.field public static final activity_ocr:I = 0x7f0d008f

.field public static final activity_ocr_set:I = 0x7f0d0090

.field public static final activity_office_convert:I = 0x7f0d0091

.field public static final activity_other_share_in_img:I = 0x7f0d0092

.field public static final activity_ovip_upgrade_purchase:I = 0x7f0d0093

.field public static final activity_paper_inch_select:I = 0x7f0d0094

.field public static final activity_paper_property_select:I = 0x7f0d0095

.field public static final activity_paper_property_select_one:I = 0x7f0d0096

.field public static final activity_pdf_edit:I = 0x7f0d0097

.field public static final activity_pdf_editing:I = 0x7f0d0098

.field public static final activity_pdf_gallery:I = 0x7f0d0099

.field public static final activity_pdf_gallery_search:I = 0x7f0d009a

.field public static final activity_pdf_gallery_search_content:I = 0x7f0d009b

.field public static final activity_pdf_kit_compress:I = 0x7f0d009c

.field public static final activity_pdf_kit_main:I = 0x7f0d009d

.field public static final activity_pdf_preview:I = 0x7f0d009e

.field public static final activity_pdf_signature:I = 0x7f0d009f

.field public static final activity_pdf_signature_new:I = 0x7f0d00a0

.field public static final activity_pdf_view:I = 0x7f0d00a1

.field public static final activity_ppt_preview:I = 0x7f0d00a2

.field public static final activity_print_upload_fax_state:I = 0x7f0d00a3

.field public static final activity_privacy_policy:I = 0x7f0d00a4

.field public static final activity_privcacy_pilicy:I = 0x7f0d00a5

.field public static final activity_purchase_debug:I = 0x7f0d00a6

.field public static final activity_purchase_debug_page:I = 0x7f0d00a7

.field public static final activity_purchase_forever:I = 0x7f0d00a8

.field public static final activity_purchase_forever_full_screen:I = 0x7f0d00a9

.field public static final activity_purchase_window:I = 0x7f0d00aa

.field public static final activity_purchase_window_full_screen:I = 0x7f0d00ab

.field public static final activity_replace_gp_web_purchase:I = 0x7f0d00ac

.field public static final activity_resort_merged_doc:I = 0x7f0d00ad

.field public static final activity_reward_video_layout:I = 0x7f0d00ae

.field public static final activity_scan_done:I = 0x7f0d00af

.field public static final activity_scan_done_new:I = 0x7f0d00b0

.field public static final activity_scenariodir_card_detail:I = 0x7f0d00b1

.field public static final activity_search:I = 0x7f0d00b2

.field public static final activity_select_signer:I = 0x7f0d00b3

.field public static final activity_settings:I = 0x7f0d00b4

.field public static final activity_share_guide:I = 0x7f0d00b5

.field public static final activity_share_guide_style_one:I = 0x7f0d00b6

.field public static final activity_share_guide_style_two:I = 0x7f0d00b7

.field public static final activity_share_sign_to_other:I = 0x7f0d00b8

.field public static final activity_sign_done:I = 0x7f0d00b9

.field public static final activity_sign_manage:I = 0x7f0d00ba

.field public static final activity_signature:I = 0x7f0d00bb

.field public static final activity_signature_edit:I = 0x7f0d00bc

.field public static final activity_signature_new:I = 0x7f0d00bd

.field public static final activity_single_fragment:I = 0x7f0d00be

.field public static final activity_super_verify_code:I = 0x7f0d00bf

.field public static final activity_tag_manage:I = 0x7f0d00c0

.field public static final activity_target_dir:I = 0x7f0d00c1

.field public static final activity_team_member:I = 0x7f0d00c2

.field public static final activity_third_service:I = 0x7f0d00c3

.field public static final activity_third_service_detail:I = 0x7f0d00c4

.field public static final activity_topic_preview:I = 0x7f0d00c5

.field public static final activity_topic_scan:I = 0x7f0d00c6

.field public static final activity_trans_slide_web:I = 0x7f0d00c7

.field public static final activity_transfer_doc:I = 0x7f0d00c8

.field public static final activity_translate_result:I = 0x7f0d00c9

.field public static final activity_various_certificate_photo:I = 0x7f0d00ca

.field public static final activity_view_larger_image_layout:I = 0x7f0d00cb

.field public static final ad_admob_app_install:I = 0x7f0d00cc

.field public static final ad_admob_scan_done:I = 0x7f0d00cd

.field public static final ad_admob_scan_done_unified:I = 0x7f0d00ce

.field public static final ad_app_exit_admob:I = 0x7f0d00cf

.field public static final ad_app_exit_admob_content:I = 0x7f0d00d0

.field public static final ad_app_exit_admob_install:I = 0x7f0d00d1

.field public static final ad_app_exit_facebook:I = 0x7f0d00d2

.field public static final ad_app_exit_mintegral_video:I = 0x7f0d00d3

.field public static final ad_app_launch_admob:I = 0x7f0d00d4

.field public static final ad_app_launch_admob_content:I = 0x7f0d00d5

.field public static final ad_app_launch_admob_install:I = 0x7f0d00d6

.field public static final ad_app_launch_facebook_native:I = 0x7f0d00d7

.field public static final ad_app_launch_zcoup:I = 0x7f0d00d8

.field public static final ad_click_tip_layout_15:I = 0x7f0d00d9

.field public static final ad_doc_grid_wrap:I = 0x7f0d00da

.field public static final ad_doc_grid_zcoup:I = 0x7f0d00db

.field public static final ad_doc_list_wrap:I = 0x7f0d00dc

.field public static final ad_doc_list_zcoup:I = 0x7f0d00dd

.field public static final ad_exit_and_share_done_for_zcoup:I = 0x7f0d00de

.field public static final ad_facebook_grid_item:I = 0x7f0d00df

.field public static final ad_facebook_list_item:I = 0x7f0d00e0

.field public static final ad_facebook_media_layout:I = 0x7f0d00e1

.field public static final ad_facebook_native:I = 0x7f0d00e2

.field public static final ad_grid_item:I = 0x7f0d00e3

.field public static final ad_launch_layout:I = 0x7f0d00e4

.field public static final ad_list_admob_app_install:I = 0x7f0d00e5

.field public static final ad_list_item:I = 0x7f0d00e6

.field public static final ad_monitor:I = 0x7f0d00e7

.field public static final ad_scan_done_applovin:I = 0x7f0d00e8

.field public static final adapter_certificate_item:I = 0x7f0d00e9

.field public static final adapter_certificate_item_new:I = 0x7f0d00ea

.field public static final adapter_greet_card_templete_item:I = 0x7f0d00eb

.field public static final add_pdf_size_dialog:I = 0x7f0d00ec

.field public static final add_security_mark_guide:I = 0x7f0d00ed

.field public static final admob_empty_layout:I = 0x7f0d00ee

.field public static final alert_bottom_dialog_layout:I = 0x7f0d00ef

.field public static final anot_floating_bar:I = 0x7f0d00f0

.field public static final app_launch_ad_layout:I = 0x7f0d00f1

.field public static final app_launch_native:I = 0x7f0d00f2

.field public static final app_launch_native_only_image:I = 0x7f0d00f3

.field public static final applovin_consent_flow_gdpr_are_you_sure_screen:I = 0x7f0d00f4

.field public static final applovin_consent_flow_gdpr_phase_learn_more_screen:I = 0x7f0d00f5

.field public static final applovin_consent_flow_gdpr_phase_main_screen:I = 0x7f0d00f6

.field public static final applovin_consent_flow_gdpr_phase_partners_screen:I = 0x7f0d00f7

.field public static final applovin_debugger_list_item_detail:I = 0x7f0d00f8

.field public static final applovin_exo_list_divider:I = 0x7f0d00f9

.field public static final applovin_exo_player_control_view:I = 0x7f0d00fa

.field public static final applovin_exo_player_view:I = 0x7f0d00fb

.field public static final applovin_exo_styled_player_control_ffwd_button:I = 0x7f0d00fc

.field public static final applovin_exo_styled_player_control_rewind_button:I = 0x7f0d00fd

.field public static final applovin_exo_styled_player_control_view:I = 0x7f0d00fe

.field public static final applovin_exo_styled_player_view:I = 0x7f0d00ff

.field public static final applovin_exo_styled_settings_list:I = 0x7f0d0100

.field public static final applovin_exo_styled_settings_list_item:I = 0x7f0d0101

.field public static final applovin_exo_styled_sub_settings_list_item:I = 0x7f0d0102

.field public static final applovin_exo_track_selection_dialog:I = 0x7f0d0103

.field public static final applovin_native_ad_media_view:I = 0x7f0d0104

.field public static final assist_view_invoice:I = 0x7f0d0105

.field public static final backup_list_item:I = 0x7f0d0106

.field public static final base_actionbar_btn:I = 0x7f0d0107

.field public static final base_actionbar_btn_night:I = 0x7f0d0108

.field public static final book_image_scan_edit_bar:I = 0x7f0d0109

.field public static final bottom_bar_office2cloud:I = 0x7f0d010a

.field public static final bottom_bar_word_list:I = 0x7f0d010b

.field public static final bottom_bar_word_list_item:I = 0x7f0d010c

.field public static final bottom_batch_reedit_image:I = 0x7f0d010d

.field public static final bottom_dialog_login:I = 0x7f0d010e

.field public static final bottom_view_count_number:I = 0x7f0d010f

.field public static final box_auth:I = 0x7f0d0110

.field public static final browser_actions_context_menu_page:I = 0x7f0d0111

.field public static final browser_actions_context_menu_row:I = 0x7f0d0112

.field public static final brvah_quick_view_load_more:I = 0x7f0d0113

.field public static final c_only_edittext:I = 0x7f0d0114

.field public static final c_tag_edittext:I = 0x7f0d0115

.field public static final c_tag_edittext_new:I = 0x7f0d0116

.field public static final c_tag_textview:I = 0x7f0d0117

.field public static final camerax_view:I = 0x7f0d0118

.field public static final capture_chose_certification_size:I = 0x7f0d0119

.field public static final capture_common_guide:I = 0x7f0d011a

.field public static final capture_common_guide_new:I = 0x7f0d011b

.field public static final capture_doc_to_excel_guide:I = 0x7f0d011c

.field public static final capture_image_restore_guide:I = 0x7f0d011d

.field public static final capture_popup_item:I = 0x7f0d011e

.field public static final capture_popup_list:I = 0x7f0d011f

.field public static final capture_refactor:I = 0x7f0d0120

.field public static final capture_refactor_child_menu:I = 0x7f0d0121

.field public static final capture_refactor_menu_label_port:I = 0x7f0d0122

.field public static final capture_refactor_settings_drop:I = 0x7f0d0123

.field public static final capture_refactor_shutter_panel:I = 0x7f0d0124

.field public static final capture_setting_multi_enhance:I = 0x7f0d0125

.field public static final capture_setting_refactor:I = 0x7f0d0126

.field public static final capture_setting_refactor_container:I = 0x7f0d0127

.field public static final capture_shutter_panel:I = 0x7f0d0128

.field public static final capture_shutter_panel_new:I = 0x7f0d0129

.field public static final capture_shutter_panel_port:I = 0x7f0d012a

.field public static final capture_topic_paper_guide_how_to:I = 0x7f0d012b

.field public static final capture_widget_main_search:I = 0x7f0d012c

.field public static final capture_widget_multi_1_1:I = 0x7f0d012d

.field public static final capture_widget_multi_2_2:I = 0x7f0d012e

.field public static final capture_widget_provider:I = 0x7f0d012f

.field public static final capture_widget_qrcode_1_1:I = 0x7f0d0130

.field public static final capture_widget_qrcode_2_2:I = 0x7f0d0131

.field public static final capture_widget_single_1_1:I = 0x7f0d0132

.field public static final capture_widget_single_1_1_preview:I = 0x7f0d0133

.field public static final capture_widget_single_2_2:I = 0x7f0d0134

.field public static final certificate_detail_item_head:I = 0x7f0d0135

.field public static final certificate_detail_item_list:I = 0x7f0d0136

.field public static final certificate_more_header:I = 0x7f0d0137

.field public static final certificate_more_item:I = 0x7f0d0138

.field public static final certificate_row_layout:I = 0x7f0d0139

.field public static final change_batch_mode:I = 0x7f0d013a

.field public static final change_pwd_logined:I = 0x7f0d013b

.field public static final change_pwd_unlogined:I = 0x7f0d013c

.field public static final choose_country_code:I = 0x7f0d013d

.field public static final choose_country_code_item:I = 0x7f0d013e

.field public static final cl_click_ocr_mask:I = 0x7f0d013f

.field public static final collaborate_message_layout:I = 0x7f0d0140

.field public static final com_facebook_activity_layout:I = 0x7f0d0141

.field public static final com_facebook_device_auth_dialog_fragment:I = 0x7f0d0142

.field public static final com_facebook_login_fragment:I = 0x7f0d0143

.field public static final com_facebook_smart_device_dialog_fragment:I = 0x7f0d0144

.field public static final com_facebook_tooltip_bubble:I = 0x7f0d0145

.field public static final comm_dialog_cs_loading:I = 0x7f0d0146

.field public static final common_activity_authentication:I = 0x7f0d0147

.field public static final complete_button:I = 0x7f0d0148

.field public static final complete_pdf_to_office:I = 0x7f0d0149

.field public static final complete_pdf_to_word:I = 0x7f0d014a

.field public static final creative_debugger_displayed_ad_detail_activity:I = 0x7f0d014b

.field public static final cs_alert_dialog:I = 0x7f0d014c

.field public static final cs_alert_dialog_progress:I = 0x7f0d014d

.field public static final cs_common_alert_dialog:I = 0x7f0d014e

.field public static final cs_progress_dialog:I = 0x7f0d014f

.field public static final cs_simple_list_item:I = 0x7f0d0150

.field public static final cs_simple_list_item_1:I = 0x7f0d0151

.field public static final cs_simple_list_item_2:I = 0x7f0d0152

.field public static final cs_simple_list_item_multiple_choice:I = 0x7f0d0153

.field public static final cs_simple_list_item_single_choice:I = 0x7f0d0154

.field public static final cs_view_tips_normal:I = 0x7f0d0155

.field public static final cs_view_tips_triangle:I = 0x7f0d0156

.field public static final csl_bottom_purchase_new_style_1:I = 0x7f0d0157

.field public static final custom_dialog:I = 0x7f0d0158

.field public static final custom_esign_tablayout_tab:I = 0x7f0d0159

.field public static final custom_tablayout_tab:I = 0x7f0d015a

.field public static final custom_tablayout_tab2:I = 0x7f0d015b

.field public static final custom_tablayout_tab_3:I = 0x7f0d015c

.field public static final custom_tablayout_tab_external_import:I = 0x7f0d015d

.field public static final db_list_layout:I = 0x7f0d015e

.field public static final default_listview:I = 0x7f0d015f

.field public static final design_bottom_navigation_item:I = 0x7f0d0160

.field public static final design_bottom_sheet_dialog:I = 0x7f0d0161

.field public static final design_layout_snackbar:I = 0x7f0d0162

.field public static final design_layout_snackbar_include:I = 0x7f0d0163

.field public static final design_layout_tab_icon:I = 0x7f0d0164

.field public static final design_layout_tab_text:I = 0x7f0d0165

.field public static final design_menu_item_action_area:I = 0x7f0d0166

.field public static final design_navigation_item:I = 0x7f0d0167

.field public static final design_navigation_item_header:I = 0x7f0d0168

.field public static final design_navigation_item_separator:I = 0x7f0d0169

.field public static final design_navigation_item_subheader:I = 0x7f0d016a

.field public static final design_navigation_menu:I = 0x7f0d016b

.field public static final design_navigation_menu_item:I = 0x7f0d016c

.field public static final design_text_input_end_icon:I = 0x7f0d016d

.field public static final design_text_input_start_icon:I = 0x7f0d016e

.field public static final dialog_ad_after_share:I = 0x7f0d016f

.field public static final dialog_ad_config_add:I = 0x7f0d0170

.field public static final dialog_add_certificate:I = 0x7f0d0171

.field public static final dialog_add_oversea_certificate:I = 0x7f0d0172

.field public static final dialog_add_text_annotation:I = 0x7f0d0173

.field public static final dialog_all_doc_permission_deny:I = 0x7f0d0174

.field public static final dialog_all_doc_permission_request:I = 0x7f0d0175

.field public static final dialog_app_cfg_modify:I = 0x7f0d0176

.field public static final dialog_area_free_share:I = 0x7f0d0177

.field public static final dialog_ask_sync_unwifi_before_share_dir:I = 0x7f0d0178

.field public static final dialog_attract_for_reward:I = 0x7f0d0179

.field public static final dialog_authentication:I = 0x7f0d017a

.field public static final dialog_backup_intro:I = 0x7f0d017b

.field public static final dialog_backup_quit_confirm:I = 0x7f0d017c

.field public static final dialog_backup_type_choose:I = 0x7f0d017d

.field public static final dialog_bad_case_upload_confirm_bottom:I = 0x7f0d017e

.field public static final dialog_batch_process:I = 0x7f0d017f

.field public static final dialog_bind_account_result:I = 0x7f0d0180

.field public static final dialog_bottom_localpurchase_china:I = 0x7f0d0181

.field public static final dialog_bottom_print_paper_set:I = 0x7f0d0182

.field public static final dialog_bottom_share:I = 0x7f0d0183

.field public static final dialog_bottom_sheet:I = 0x7f0d0184

.field public static final dialog_bottom_sheet_free_ad:I = 0x7f0d0185

.field public static final dialog_btm_common_esign:I = 0x7f0d0186

.field public static final dialog_cam_exam_guide_bottom:I = 0x7f0d0187

.field public static final dialog_cap_new_user_guide:I = 0x7f0d0188

.field public static final dialog_capture_preview_enhance:I = 0x7f0d0189

.field public static final dialog_capture_preview_scale:I = 0x7f0d018a

.field public static final dialog_capture_preview_trim:I = 0x7f0d018b

.field public static final dialog_card_detail_show_personal_info:I = 0x7f0d018c

.field public static final dialog_card_photo_point:I = 0x7f0d018d

.field public static final dialog_card_recommend_tip:I = 0x7f0d018e

.field public static final dialog_certificate_photo_guide:I = 0x7f0d018f

.field public static final dialog_change_existed_account:I = 0x7f0d0190

.field public static final dialog_chose_img_dialog:I = 0x7f0d0191

.field public static final dialog_claim_gifts_layout:I = 0x7f0d0192

.field public static final dialog_claim_gifts_new:I = 0x7f0d0193

.field public static final dialog_cn_annual_premium:I = 0x7f0d0194

.field public static final dialog_cn_annual_premium_page_list:I = 0x7f0d0195

.field public static final dialog_cn_renew_recall:I = 0x7f0d0196

.field public static final dialog_cn_renew_recall_page_list:I = 0x7f0d0197

.field public static final dialog_cn_unsubscribe_recall_layout:I = 0x7f0d0198

.field public static final dialog_cn_unsubscribe_scaffold:I = 0x7f0d0199

.field public static final dialog_common:I = 0x7f0d019a

.field public static final dialog_common_quit_confirm:I = 0x7f0d019b

.field public static final dialog_common_watch_ad:I = 0x7f0d019c

.field public static final dialog_common_with_image:I = 0x7f0d019d

.field public static final dialog_content:I = 0x7f0d019e

.field public static final dialog_create_folder:I = 0x7f0d019f

.field public static final dialog_cs_bluetooth_connect:I = 0x7f0d01a0

.field public static final dialog_cs_pdf_vip_buy_success:I = 0x7f0d01a1

.field public static final dialog_cs_protocol_full_screen:I = 0x7f0d01a2

.field public static final dialog_cs_protocols:I = 0x7f0d01a3

.field public static final dialog_custom_progress:I = 0x7f0d01a4

.field public static final dialog_discount_purchase_v2:I = 0x7f0d01a5

.field public static final dialog_doc_archive:I = 0x7f0d01a6

.field public static final dialog_doc_capture_guide:I = 0x7f0d01a7

.field public static final dialog_doc_import:I = 0x7f0d01a8

.field public static final dialog_doc_newbie_scan_reward:I = 0x7f0d01a9

.field public static final dialog_doc_optical_recognize_retain:I = 0x7f0d01aa

.field public static final dialog_doc_set_pdf_password:I = 0x7f0d01ab

.field public static final dialog_document_fragment_more_new:I = 0x7f0d01ac

.field public static final dialog_document_protection:I = 0x7f0d01ad

.field public static final dialog_document_protection_on:I = 0x7f0d01ae

.field public static final dialog_download_cs_pdf:I = 0x7f0d01af

.field public static final dialog_edit_image_lottie:I = 0x7f0d01b0

.field public static final dialog_edu_group:I = 0x7f0d01b1

.field public static final dialog_edu_group_large:I = 0x7f0d01b2

.field public static final dialog_esign_datepicker:I = 0x7f0d01b3

.field public static final dialog_esign_guide:I = 0x7f0d01b4

.field public static final dialog_esign_save_file:I = 0x7f0d01b5

.field public static final dialog_esign_share_fcuntion:I = 0x7f0d01b6

.field public static final dialog_esign_share_file_or_link:I = 0x7f0d01b7

.field public static final dialog_eu_auth:I = 0x7f0d01b8

.field public static final dialog_excel_export_select:I = 0x7f0d01b9

.field public static final dialog_excel_no_recognize:I = 0x7f0d01ba

.field public static final dialog_excel_prompt:I = 0x7f0d01bb

.field public static final dialog_exemption:I = 0x7f0d01bc

.field public static final dialog_export_word:I = 0x7f0d01bd

.field public static final dialog_external_member_7days:I = 0x7f0d01be

.field public static final dialog_external_member_retained:I = 0x7f0d01bf

.field public static final dialog_file_type_and_tag_filter:I = 0x7f0d01c0

.field public static final dialog_folder_scenario_create:I = 0x7f0d01c1

.field public static final dialog_folder_scenario_create_content:I = 0x7f0d01c2

.field public static final dialog_function_reward:I = 0x7f0d01c3

.field public static final dialog_get_union_member_layout:I = 0x7f0d01c4

.field public static final dialog_gp_annual_premium:I = 0x7f0d01c5

.field public static final dialog_gp_annual_premium_page_list:I = 0x7f0d01c6

.field public static final dialog_gp_drop_cnl:I = 0x7f0d01c7

.field public static final dialog_gp_first_premium_gift:I = 0x7f0d01c8

.field public static final dialog_gp_first_premium_gift_success:I = 0x7f0d01c9

.field public static final dialog_gp_guide_mark:I = 0x7f0d01ca

.field public static final dialog_gp_guide_mark_new:I = 0x7f0d01cb

.field public static final dialog_gp_guide_old_mark:I = 0x7f0d01cc

.field public static final dialog_green_mode_tips:I = 0x7f0d01cd

.field public static final dialog_grid_view_layout:I = 0x7f0d01ce

.field public static final dialog_guide_purchase:I = 0x7f0d01cf

.field public static final dialog_guide_purchase_new:I = 0x7f0d01d0

.field public static final dialog_guide_rejoin_new_eight_day:I = 0x7f0d01d1

.field public static final dialog_guide_to_cam_exam:I = 0x7f0d01d2

.field public static final dialog_guide_to_cam_exam_pop_up:I = 0x7f0d01d3

.field public static final dialog_guide_to_ocr_result:I = 0x7f0d01d4

.field public static final dialog_html_convert_result:I = 0x7f0d01d5

.field public static final dialog_hua_wei_comment_layout:I = 0x7f0d01d6

.field public static final dialog_human_translate_duty_explain:I = 0x7f0d01d7

.field public static final dialog_image_adjust:I = 0x7f0d01d8

.field public static final dialog_image_edit_guide:I = 0x7f0d01d9

.field public static final dialog_image_editing_feed_back:I = 0x7f0d01da

.field public static final dialog_image_editing_quit_confirm:I = 0x7f0d01db

.field public static final dialog_image_editing_save_confirm:I = 0x7f0d01dc

.field public static final dialog_import_esign:I = 0x7f0d01dd

.field public static final dialog_import_source_select:I = 0x7f0d01de

.field public static final dialog_invite_share_dir:I = 0x7f0d01df

.field public static final dialog_invoice_request_fail:I = 0x7f0d01e0

.field public static final dialog_ipo_image_quality:I = 0x7f0d01e1

.field public static final dialog_layout_bottom_purchase:I = 0x7f0d01e2

.field public static final dialog_log_out_tips:I = 0x7f0d01e3

.field public static final dialog_login_for_compliance:I = 0x7f0d01e4

.field public static final dialog_login_protocol:I = 0x7f0d01e5

.field public static final dialog_member_explain:I = 0x7f0d01e6

.field public static final dialog_more:I = 0x7f0d01e7

.field public static final dialog_multi_doc_import:I = 0x7f0d01e8

.field public static final dialog_multi_docs_share:I = 0x7f0d01e9

.field public static final dialog_new_member_reward_know:I = 0x7f0d01ea

.field public static final dialog_newbie_certificate:I = 0x7f0d01eb

.field public static final dialog_newbie_ocr:I = 0x7f0d01ec

.field public static final dialog_nps_common_multi_choice:I = 0x7f0d01ed

.field public static final dialog_nps_satisfied_page:I = 0x7f0d01ee

.field public static final dialog_nps_score:I = 0x7f0d01ef

.field public static final dialog_nps_unsatisfied_question:I = 0x7f0d01f0

.field public static final dialog_ocr_amount_not_enough:I = 0x7f0d01f1

.field public static final dialog_ocr_no_result:I = 0x7f0d01f2

.field public static final dialog_ocr_prompt_upgrade_vip:I = 0x7f0d01f3

.field public static final dialog_office2_pdf_preview_share:I = 0x7f0d01f4

.field public static final dialog_office_doc_info:I = 0x7f0d01f5

.field public static final dialog_office_doc_share:I = 0x7f0d01f6

.field public static final dialog_one_trial_renew:I = 0x7f0d01f7

.field public static final dialog_one_trial_renew_purchase:I = 0x7f0d01f8

.field public static final dialog_one_trial_renew_success:I = 0x7f0d01f9

.field public static final dialog_password_identify_result:I = 0x7f0d01fa

.field public static final dialog_pdf_close_edit_watch_ad:I = 0x7f0d01fb

.field public static final dialog_pdf_edit_watch_ad:I = 0x7f0d01fc

.field public static final dialog_pdf_editing_compress:I = 0x7f0d01fd

.field public static final dialog_pdf_editing_encrypt:I = 0x7f0d01fe

.field public static final dialog_pdf_editing_strong_guide:I = 0x7f0d01ff

.field public static final dialog_pdf_editing_water_mark_guide:I = 0x7f0d0200

.field public static final dialog_pdf_editing_watermark:I = 0x7f0d0201

.field public static final dialog_pdf_equity_measurement:I = 0x7f0d0202

.field public static final dialog_pdf_import_type_set:I = 0x7f0d0203

.field public static final dialog_pdf_move:I = 0x7f0d0204

.field public static final dialog_permission_tips:I = 0x7f0d0205

.field public static final dialog_positive_normal_premium:I = 0x7f0d0206

.field public static final dialog_positive_normal_premium_new:I = 0x7f0d0207

.field public static final dialog_print_type:I = 0x7f0d0208

.field public static final dialog_purchase_for_cloud_overrun:I = 0x7f0d0209

.field public static final dialog_purchase_for_gp_normal_non_activity:I = 0x7f0d020a

.field public static final dialog_purchase_for_gp_normal_price_new_layout:I = 0x7f0d020b

.field public static final dialog_purchase_for_gp_normal_price_old_layout:I = 0x7f0d020c

.field public static final dialog_purchase_for_gp_normal_price_old_plus_layout:I = 0x7f0d020d

.field public static final dialog_purchase_for_gp_normal_top_layout:I = 0x7f0d020e

.field public static final dialog_purchase_for_gp_normal_top_list_layout:I = 0x7f0d020f

.field public static final dialog_purchase_for_gp_normal_top_viewpager_layout:I = 0x7f0d0210

.field public static final dialog_purchase_point:I = 0x7f0d0211

.field public static final dialog_pwd_login_over_five:I = 0x7f0d0212

.field public static final dialog_pwd_login_over_three:I = 0x7f0d0213

.field public static final dialog_qr_code_detail:I = 0x7f0d0214

.field public static final dialog_qr_code_share:I = 0x7f0d0215

.field public static final dialog_redeem_for_gp_new_normal:I = 0x7f0d0216

.field public static final dialog_rejoin_benefit:I = 0x7f0d0217

.field public static final dialog_renewal_agreement_bottom:I = 0x7f0d0218

.field public static final dialog_renewal_agreement_explain:I = 0x7f0d0219

.field public static final dialog_revert_china_privacy:I = 0x7f0d021a

.field public static final dialog_reward_ad_red:I = 0x7f0d021b

.field public static final dialog_scan_first_doc_purchase_success:I = 0x7f0d021c

.field public static final dialog_school_season_gift:I = 0x7f0d021d

.field public static final dialog_security:I = 0x7f0d021e

.field public static final dialog_select_ocr_pages:I = 0x7f0d021f

.field public static final dialog_send_gift_card_share:I = 0x7f0d0220

.field public static final dialog_send_to_pc:I = 0x7f0d0221

.field public static final dialog_senir_create_folder_guide:I = 0x7f0d0222

.field public static final dialog_senir_create_folder_guide_v2:I = 0x7f0d0223

.field public static final dialog_share_app_list:I = 0x7f0d0224

.field public static final dialog_share_bank_card_journal:I = 0x7f0d0225

.field public static final dialog_share_channel_select:I = 0x7f0d0226

.field public static final dialog_share_dir:I = 0x7f0d0227

.field public static final dialog_share_dir_create:I = 0x7f0d0228

.field public static final dialog_share_dir_guide:I = 0x7f0d0229

.field public static final dialog_share_done_vip_month:I = 0x7f0d022a

.field public static final dialog_share_image_watermark:I = 0x7f0d022b

.field public static final dialog_share_link_cloud_doc_sync:I = 0x7f0d022c

.field public static final dialog_share_link_loading:I = 0x7f0d022d

.field public static final dialog_share_link_setting:I = 0x7f0d022e

.field public static final dialog_share_list_header:I = 0x7f0d022f

.field public static final dialog_share_option:I = 0x7f0d0230

.field public static final dialog_share_pad:I = 0x7f0d0231

.field public static final dialog_share_pdf_check:I = 0x7f0d0232

.field public static final dialog_share_pdf_watermark_select:I = 0x7f0d0233

.field public static final dialog_share_uploading:I = 0x7f0d0234

.field public static final dialog_signature_guide_with_video:I = 0x7f0d0235

.field public static final dialog_skip_encrypt_pdf:I = 0x7f0d0236

.field public static final dialog_smart_erase_tutorial:I = 0x7f0d0237

.field public static final dialog_stripe_cancel_subscription:I = 0x7f0d0238

.field public static final dialog_super_filter_guide_bottom:I = 0x7f0d0239

.field public static final dialog_super_vcode_over_five:I = 0x7f0d023a

.field public static final dialog_super_vcode_over_three:I = 0x7f0d023b

.field public static final dialog_sys_print_ope:I = 0x7f0d023c

.field public static final dialog_tag_setting:I = 0x7f0d023d

.field public static final dialog_time_line_more:I = 0x7f0d023e

.field public static final dialog_to_retain_gp:I = 0x7f0d023f

.field public static final dialog_to_retain_gp_common:I = 0x7f0d0240

.field public static final dialog_to_retain_gp_style_coupon:I = 0x7f0d0241

.field public static final dialog_to_retain_gp_style_line_chart:I = 0x7f0d0242

.field public static final dialog_tool_page_more_functions:I = 0x7f0d0243

.field public static final dialog_translate_lan_select:I = 0x7f0d0244

.field public static final dialog_translate_lang_select:I = 0x7f0d0245

.field public static final dialog_trial_rule_config:I = 0x7f0d0246

.field public static final dialog_unlogin_share_prompt:I = 0x7f0d0247

.field public static final dialog_unlogin_share_rcn_prompt:I = 0x7f0d0248

.field public static final dialog_use_points:I = 0x7f0d0249

.field public static final dialog_vip_gift_receive:I = 0x7f0d024a

.field public static final dialog_vip_level_upgrade:I = 0x7f0d024b

.field public static final dialog_vip_month_privacy:I = 0x7f0d024c

.field public static final dialog_vip_month_promotion_style_1:I = 0x7f0d024d

.field public static final dialog_vip_month_promotion_style_2:I = 0x7f0d024e

.field public static final dialog_vip_month_promotion_style_3:I = 0x7f0d024f

.field public static final dialog_vip_month_purchase_success:I = 0x7f0d0250

.field public static final dialog_water_mark_set:I = 0x7f0d0251

.field public static final dialog_water_tips:I = 0x7f0d0252

.field public static final dialog_word_demo_preview:I = 0x7f0d0253

.field public static final dialog_word_export_select:I = 0x7f0d0254

.field public static final dlg_bind_wechat_fail:I = 0x7f0d0255

.field public static final dlg_bind_wechat_success:I = 0x7f0d0256

.field public static final dlg_deep_clean:I = 0x7f0d0257

.field public static final dlg_msg_checkbox:I = 0x7f0d0258

.field public static final dlg_msg_html_checkbox:I = 0x7f0d0259

.field public static final dlg_rotate:I = 0x7f0d025a

.field public static final dlg_save_transfer_result:I = 0x7f0d025b

.field public static final dlg_tips_by_video:I = 0x7f0d025c

.field public static final dlg_tips_topic_edit:I = 0x7f0d025d

.field public static final dlg_tips_topic_jigsaw:I = 0x7f0d025e

.field public static final doc_ad_item_grid:I = 0x7f0d025f

.field public static final doc_bottombar_private:I = 0x7f0d0260

.field public static final doc_bottombar_private_extract:I = 0x7f0d0261

.field public static final doc_bottombar_private_move:I = 0x7f0d0262

.field public static final doc_bottombar_team:I = 0x7f0d0263

.field public static final doc_fragment_signature_guid:I = 0x7f0d0264

.field public static final doc_grid_item:I = 0x7f0d0265

.field public static final doc_json_de_moire:I = 0x7f0d0266

.field public static final doc_list_item:I = 0x7f0d0267

.field public static final doc_list_item_10:I = 0x7f0d0268

.field public static final doc_list_item_short:I = 0x7f0d0269

.field public static final doc_main:I = 0x7f0d026a

.field public static final doc_pop_close_menu:I = 0x7f0d026b

.field public static final doc_pop_multi_layout:I = 0x7f0d026c

.field public static final doc_property_activity:I = 0x7f0d026d

.field public static final doc_single_image_container:I = 0x7f0d026e

.field public static final doclist_ad_item_list:I = 0x7f0d026f

.field public static final doclist_ad_item_list_new:I = 0x7f0d0270

.field public static final document_menu_item:I = 0x7f0d0271

.field public static final document_more_king_kong_item_view:I = 0x7f0d0272

.field public static final doodle_action_layout_save:I = 0x7f0d0273

.field public static final doodle_activity_doodle:I = 0x7f0d0274

.field public static final doodle_activity_text:I = 0x7f0d0275

.field public static final doodle_dialog_eyedropper_guilde:I = 0x7f0d0276

.field public static final dot_layout:I = 0x7f0d0277

.field public static final dual_screen_layout:I = 0x7f0d0278

.field public static final edit_user_name_dialog:I = 0x7f0d0279

.field public static final ency_decy_dialog:I = 0x7f0d027a

.field public static final expandable_layout_child:I = 0x7f0d027b

.field public static final expandable_layout_frame:I = 0x7f0d027c

.field public static final fax_charge:I = 0x7f0d027d

.field public static final fax_charge_iab:I = 0x7f0d027e

.field public static final fax_popup_hint:I = 0x7f0d027f

.field public static final feed_back_grid_item:I = 0x7f0d0280

.field public static final feed_back_list_item:I = 0x7f0d0281

.field public static final fg_doc_json_cancel_account:I = 0x7f0d0282

.field public static final fg_doc_json_local_gated:I = 0x7f0d0283

.field public static final fg_doc_json_trim_anim:I = 0x7f0d0284

.field public static final fg_doc_json_wechat:I = 0x7f0d0285

.field public static final fg_feed_back_list:I = 0x7f0d0286

.field public static final fg_feed_back_submit:I = 0x7f0d0287

.field public static final fg_picture_slide:I = 0x7f0d0288

.field public static final fg_reward_play_layout:I = 0x7f0d0289

.field public static final fg_reward_result_layout:I = 0x7f0d028a

.field public static final file_item_view:I = 0x7f0d028b

.field public static final first_enter_offline_dialog:I = 0x7f0d028c

.field public static final fl_certificate_edit:I = 0x7f0d028d

.field public static final floating_annot_bar:I = 0x7f0d028e

.field public static final floating_bar:I = 0x7f0d028f

.field public static final forder_grid_item:I = 0x7f0d0290

.field public static final forder_list_item:I = 0x7f0d0291

.field public static final forder_list_item_10:I = 0x7f0d0292

.field public static final fragment_a_key_login:I = 0x7f0d0293

.field public static final fragment_ad_config:I = 0x7f0d0294

.field public static final fragment_all_message:I = 0x7f0d0295

.field public static final fragment_area_code_confirm:I = 0x7f0d0296

.field public static final fragment_authority_managerment:I = 0x7f0d0297

.field public static final fragment_backup_main:I = 0x7f0d0298

.field public static final fragment_backup_setting:I = 0x7f0d0299

.field public static final fragment_bad_case_submit:I = 0x7f0d029a

.field public static final fragment_bank_card_journal_list:I = 0x7f0d029b

.field public static final fragment_bank_card_web_finder:I = 0x7f0d029c

.field public static final fragment_bankcard_journal_result:I = 0x7f0d029d

.field public static final fragment_batch_ocr_result:I = 0x7f0d029e

.field public static final fragment_batch_reedit_image:I = 0x7f0d029f

.field public static final fragment_bind_phone_email:I = 0x7f0d02a0

.field public static final fragment_books_result_preivew:I = 0x7f0d02a1

.field public static final fragment_booksplitter_edit:I = 0x7f0d02a2

.field public static final fragment_booksplitter_edit_new:I = 0x7f0d02a3

.field public static final fragment_cancel_account_home:I = 0x7f0d02a4

.field public static final fragment_capture_menu:I = 0x7f0d02a5

.field public static final fragment_card_detail_fill_prompt:I = 0x7f0d02a6

.field public static final fragment_card_detail_how_use:I = 0x7f0d02a7

.field public static final fragment_card_photo_page_list:I = 0x7f0d02a8

.field public static final fragment_certificate_detail:I = 0x7f0d02a9

.field public static final fragment_certificate_folder_home:I = 0x7f0d02aa

.field public static final fragment_certificate_photo_test:I = 0x7f0d02ab

.field public static final fragment_certificate_preview:I = 0x7f0d02ac

.field public static final fragment_certificate_preview_detail:I = 0x7f0d02ad

.field public static final fragment_change_account:I = 0x7f0d02ae

.field public static final fragment_change_device_id:I = 0x7f0d02af

.field public static final fragment_checkout_dialog:I = 0x7f0d02b0

.field public static final fragment_chose_operation_dialog:I = 0x7f0d02b1

.field public static final fragment_cloud_disk:I = 0x7f0d02b2

.field public static final fragment_cloud_service_auth:I = 0x7f0d02b3

.field public static final fragment_cloud_service_setting:I = 0x7f0d02b4

.field public static final fragment_cloud_service_setting_top:I = 0x7f0d02b5

.field public static final fragment_container:I = 0x7f0d02b6

.field public static final fragment_count_number:I = 0x7f0d02b7

.field public static final fragment_cs_main:I = 0x7f0d02b8

.field public static final fragment_de_moire_bottom_description:I = 0x7f0d02b9

.field public static final fragment_deal_progress_for_ocr:I = 0x7f0d02ba

.field public static final fragment_deal_progress_for_restore:I = 0x7f0d02bb

.field public static final fragment_deeplink_test:I = 0x7f0d02bc

.field public static final fragment_default_email_login:I = 0x7f0d02bd

.field public static final fragment_default_email_register:I = 0x7f0d02be

.field public static final fragment_default_phone_pwd_login:I = 0x7f0d02bf

.field public static final fragment_default_verify_code_login:I = 0x7f0d02c0

.field public static final fragment_delete_contact:I = 0x7f0d02c1

.field public static final fragment_doc_import:I = 0x7f0d02c2

.field public static final fragment_doc_json_ad:I = 0x7f0d02c3

.field public static final fragment_doc_json_androidr:I = 0x7f0d02c4

.field public static final fragment_doc_json_dir:I = 0x7f0d02c5

.field public static final fragment_doc_json_drag_compare_image:I = 0x7f0d02c6

.field public static final fragment_doc_json_guide:I = 0x7f0d02c7

.field public static final fragment_doc_json_pay_account:I = 0x7f0d02c8

.field public static final fragment_doc_json_unsorted:I = 0x7f0d02c9

.field public static final fragment_doc_json_web_offline:I = 0x7f0d02ca

.field public static final fragment_doc_json_web_url_test:I = 0x7f0d02cb

.field public static final fragment_doc_list_multiple_choice:I = 0x7f0d02cc

.field public static final fragment_doc_list_multiple_choice_10:I = 0x7f0d02cd

.field public static final fragment_doc_search:I = 0x7f0d02ce

.field public static final fragment_doc_text_fix_image:I = 0x7f0d02cf

.field public static final fragment_download_progress:I = 0x7f0d02d0

.field public static final fragment_download_progress_for_de_moire:I = 0x7f0d02d1

.field public static final fragment_download_progress_for_paper:I = 0x7f0d02d2

.field public static final fragment_drop_cnl_purchase:I = 0x7f0d02d3

.field public static final fragment_edit__actionbar_phone:I = 0x7f0d02d4

.field public static final fragment_edu_user_confirm:I = 0x7f0d02d5

.field public static final fragment_email_login:I = 0x7f0d02d6

.field public static final fragment_fail_cancel_account:I = 0x7f0d02d7

.field public static final fragment_fax_task_item:I = 0x7f0d02d8

.field public static final fragment_forget_pwd:I = 0x7f0d02d9

.field public static final fragment_gallery_test:I = 0x7f0d02da

.field public static final fragment_gp_guide_post_pay:I = 0x7f0d02db

.field public static final fragment_gp_guide_scroll_image:I = 0x7f0d02dc

.field public static final fragment_greet_card_guide:I = 0x7f0d02dd

.field public static final fragment_guide_auto_scroll_image:I = 0x7f0d02de

.field public static final fragment_guide_cn_normal:I = 0x7f0d02df

.field public static final fragment_guide_cn_purchase:I = 0x7f0d02e0

.field public static final fragment_guide_gp_last_new:I = 0x7f0d02e1

.field public static final fragment_guide_gp_normal_page_new:I = 0x7f0d02e2

.field public static final fragment_guide_gp_purchase_layout:I = 0x7f0d02e3

.field public static final fragment_guide_gp_video_layout:I = 0x7f0d02e4

.field public static final fragment_guide_test:I = 0x7f0d02e5

.field public static final fragment_hear_cnl:I = 0x7f0d02e6

.field public static final fragment_html_text:I = 0x7f0d02e7

.field public static final fragment_image_editing:I = 0x7f0d02e8

.field public static final fragment_image_restore_result:I = 0x7f0d02e9

.field public static final fragment_image_restore_trim:I = 0x7f0d02ea

.field public static final fragment_image_to_word:I = 0x7f0d02eb

.field public static final fragment_inno_lab_ps_detect:I = 0x7f0d02ec

.field public static final fragment_inno_lab_smart_erase_list:I = 0x7f0d02ed

.field public static final fragment_invite_share_debug:I = 0x7f0d02ee

.field public static final fragment_invoice_browse:I = 0x7f0d02ef

.field public static final fragment_invoice_check_list:I = 0x7f0d02f0

.field public static final fragment_invoice_detail:I = 0x7f0d02f1

.field public static final fragment_invoice_list:I = 0x7f0d02f2

.field public static final fragment_invoice_result:I = 0x7f0d02f3

.field public static final fragment_ip_address_test:I = 0x7f0d02f4

.field public static final fragment_local_directory:I = 0x7f0d02f5

.field public static final fragment_log_test:I = 0x7f0d02f6

.field public static final fragment_login_main:I = 0x7f0d02f7

.field public static final fragment_login_ways:I = 0x7f0d02f8

.field public static final fragment_long_image_stitch:I = 0x7f0d02f9

.field public static final fragment_long_img_preview:I = 0x7f0d02fa

.field public static final fragment_main_actionbar_container:I = 0x7f0d02fb

.field public static final fragment_main_colla_edit_actionbar_tablet:I = 0x7f0d02fc

.field public static final fragment_main_doc_host:I = 0x7f0d02fd

.field public static final fragment_main_doc_page:I = 0x7f0d02fe

.field public static final fragment_main_home:I = 0x7f0d02ff

.field public static final fragment_main_mydoc_edit_actionbar_tablet:I = 0x7f0d0300

.field public static final fragment_main_only_read_first:I = 0x7f0d0301

.field public static final fragment_main_only_read_fourth:I = 0x7f0d0302

.field public static final fragment_main_only_read_second:I = 0x7f0d0303

.field public static final fragment_main_only_read_third:I = 0x7f0d0304

.field public static final fragment_main_right_menu:I = 0x7f0d0305

.field public static final fragment_main_team_actionbar_phone:I = 0x7f0d0306

.field public static final fragment_main_team_actionbar_tablet:I = 0x7f0d0307

.field public static final fragment_main_team_bottombar_phone:I = 0x7f0d0308

.field public static final fragment_marketing_pop_dialog:I = 0x7f0d0309

.field public static final fragment_me_page:I = 0x7f0d030a

.field public static final fragment_message:I = 0x7f0d030b

.field public static final fragment_more_dialog:I = 0x7f0d030c

.field public static final fragment_multi_capture_result:I = 0x7f0d030d

.field public static final fragment_multi_image_edit:I = 0x7f0d030e

.field public static final fragment_multi_image_edit_example:I = 0x7f0d030f

.field public static final fragment_multiprocess_mem_test:I = 0x7f0d0310

.field public static final fragment_negative_purchase:I = 0x7f0d0311

.field public static final fragment_new_sign_home:I = 0x7f0d0312

.field public static final fragment_new_sign_me:I = 0x7f0d0313

.field public static final fragment_office_convert_pdf:I = 0x7f0d0314

.field public static final fragment_office_convert_pdf_process:I = 0x7f0d0315

.field public static final fragment_office_convert_process:I = 0x7f0d0316

.field public static final fragment_office_doc_preview:I = 0x7f0d0317

.field public static final fragment_page_detail:I = 0x7f0d0318

.field public static final fragment_page_imageview:I = 0x7f0d0319

.field public static final fragment_page_list:I = 0x7f0d031a

.field public static final fragment_page_list_new:I = 0x7f0d031b

.field public static final fragment_pagelist_container:I = 0x7f0d031c

.field public static final fragment_pdf_edit:I = 0x7f0d031d

.field public static final fragment_pdf_import_type_set:I = 0x7f0d031e

.field public static final fragment_pdf_preview:I = 0x7f0d031f

.field public static final fragment_pdf_to_word:I = 0x7f0d0320

.field public static final fragment_pdf_view_host:I = 0x7f0d0321

.field public static final fragment_peform_monitor_test:I = 0x7f0d0322

.field public static final fragment_phone_account_verify:I = 0x7f0d0323

.field public static final fragment_phone_pwd_login:I = 0x7f0d0324

.field public static final fragment_phone_verify_code_login:I = 0x7f0d0325

.field public static final fragment_ppt_presentation:I = 0x7f0d0326

.field public static final fragment_preview_topic:I = 0x7f0d0327

.field public static final fragment_print_filter:I = 0x7f0d0328

.field public static final fragment_printer_darkness_setting:I = 0x7f0d0329

.field public static final fragment_printer_device:I = 0x7f0d032a

.field public static final fragment_printer_paper_bank:I = 0x7f0d032b

.field public static final fragment_printer_preview:I = 0x7f0d032c

.field public static final fragment_printer_property:I = 0x7f0d032d

.field public static final fragment_printer_property_new:I = 0x7f0d032e

.field public static final fragment_printer_search:I = 0x7f0d032f

.field public static final fragment_printer_setting:I = 0x7f0d0330

.field public static final fragment_printer_wifi:I = 0x7f0d0331

.field public static final fragment_privacy_policy:I = 0x7f0d0332

.field public static final fragment_purchase_point:I = 0x7f0d0333

.field public static final fragment_qr_code_confirm_login:I = 0x7f0d0334

.field public static final fragment_qr_code_history_list:I = 0x7f0d0335

.field public static final fragment_qr_code_histroy_result:I = 0x7f0d0336

.field public static final fragment_qr_code_result_search:I = 0x7f0d0337

.field public static final fragment_scan_done_cloud_doc_sync:I = 0x7f0d0338

.field public static final fragment_scan_done_new:I = 0x7f0d0339

.field public static final fragment_scan_first_doc_premium:I = 0x7f0d033a

.field public static final fragment_scence_banner_test:I = 0x7f0d033b

.field public static final fragment_security_mark:I = 0x7f0d033c

.field public static final fragment_select_contactor:I = 0x7f0d033d

.field public static final fragment_send_to_pc:I = 0x7f0d033e

.field public static final fragment_setting_pwd:I = 0x7f0d033f

.field public static final fragment_settings:I = 0x7f0d0340

.field public static final fragment_share_text_dialog:I = 0x7f0d0341

.field public static final fragment_share_word_dialog:I = 0x7f0d0342

.field public static final fragment_sign_adjust:I = 0x7f0d0343

.field public static final fragment_signed_list_share:I = 0x7f0d0344

.field public static final fragment_signed_share_list:I = 0x7f0d0345

.field public static final fragment_smart_erase:I = 0x7f0d0346

.field public static final fragment_smart_erase_operate:I = 0x7f0d0347

.field public static final fragment_state_main:I = 0x7f0d0348

.field public static final fragment_state_main_10:I = 0x7f0d0349

.field public static final fragment_stripe_subscription_management:I = 0x7f0d034a

.field public static final fragment_super_dir_guide:I = 0x7f0d034b

.field public static final fragment_super_vcode_validate:I = 0x7f0d034c

.field public static final fragment_sync_test:I = 0x7f0d034d

.field public static final fragment_team_document:I = 0x7f0d034e

.field public static final fragment_test_image_preview:I = 0x7f0d034f

.field public static final fragment_tool_page:I = 0x7f0d0350

.field public static final fragment_tool_page_v2:I = 0x7f0d0351

.field public static final fragment_toword_test:I = 0x7f0d0352

.field public static final fragment_translate_result:I = 0x7f0d0353

.field public static final fragment_translate_scan:I = 0x7f0d0354

.field public static final fragment_upload_list_item:I = 0x7f0d0355

.field public static final fragment_verify_code:I = 0x7f0d0356

.field public static final fragment_verify_code_none_receive:I = 0x7f0d0357

.field public static final fragment_verify_new_email:I = 0x7f0d0358

.field public static final fragment_verify_new_phone:I = 0x7f0d0359

.field public static final fragment_verify_old_account:I = 0x7f0d035a

.field public static final fragment_vip_activity_purchase_dialog:I = 0x7f0d035b

.field public static final fragment_word_list:I = 0x7f0d035c

.field public static final fragment_workflow_config:I = 0x7f0d035d

.field public static final framgent_innovationlab_test:I = 0x7f0d035e

.field public static final gallery_badge_icon_textview:I = 0x7f0d035f

.field public static final gallery_doctype_image_guide_dialog:I = 0x7f0d0360

.field public static final gallery_select_guide_number:I = 0x7f0d0361

.field public static final gallery_sroller_animation_guide:I = 0x7f0d0362

.field public static final guide_cn_new_year_discount_bottom_layout:I = 0x7f0d0363

.field public static final guide_cn_purchase7_page_bottom:I = 0x7f0d0364

.field public static final guide_cn_purchase8_page_bottom:I = 0x7f0d0365

.field public static final guide_cn_purchase_page:I = 0x7f0d0366

.field public static final guide_cn_purchase_page_bottom:I = 0x7f0d0367

.field public static final guide_esign_typeview:I = 0x7f0d0368

.field public static final guide_gp_purchase_page:I = 0x7f0d0369

.field public static final guide_main:I = 0x7f0d036a

.field public static final guide_pages_item:I = 0x7f0d036b

.field public static final have_toolbar_and_righttop_layout:I = 0x7f0d036c

.field public static final have_toolbar_dark_layout:I = 0x7f0d036d

.field public static final have_toolbar_layout:I = 0x7f0d036e

.field public static final http_auth_dialog:I = 0x7f0d036f

.field public static final image_enhance_menu_7:I = 0x7f0d0370

.field public static final image_enhance_modes_item:I = 0x7f0d0371

.field public static final image_enhance_modes_item_new:I = 0x7f0d0372

.field public static final image_scan:I = 0x7f0d0373

.field public static final image_scan_10:I = 0x7f0d0374

.field public static final image_scan_7:I = 0x7f0d0375

.field public static final image_scan_confirm_bar:I = 0x7f0d0376

.field public static final image_scan_edit_bar:I = 0x7f0d0377

.field public static final image_scan_excel_bottom_tips:I = 0x7f0d0378

.field public static final image_scan_normal_tips:I = 0x7f0d0379

.field public static final image_share:I = 0x7f0d037a

.field public static final incluce_layout_id_tips:I = 0x7f0d037b

.field public static final include_bankcard_viewfindder:I = 0x7f0d037c

.field public static final include_booklet_jigsaw_refactor:I = 0x7f0d037d

.field public static final include_camera_api:I = 0x7f0d037e

.field public static final include_capture_bottom_guide:I = 0x7f0d037f

.field public static final include_certificate_frame_container:I = 0x7f0d0380

.field public static final include_certificate_viewfindder_refactor:I = 0x7f0d0381

.field public static final include_cn_driver_viewfindder_refactor:I = 0x7f0d0382

.field public static final include_driver_viewfindder_refactor:I = 0x7f0d0383

.field public static final include_edit_keyboard_btn:I = 0x7f0d0384

.field public static final include_edu_bannner_control:I = 0x7f0d0385

.field public static final include_empty_msg:I = 0x7f0d0386

.field public static final include_esign_btm_fun_panel:I = 0x7f0d0387

.field public static final include_esign_edit_sign_panel:I = 0x7f0d0388

.field public static final include_esign_prepare_sign_panel:I = 0x7f0d0389

.field public static final include_fail_load_msg:I = 0x7f0d038a

.field public static final include_house_property_viewfinder_refactor:I = 0x7f0d038b

.field public static final include_id_viewfindder:I = 0x7f0d038c

.field public static final include_image_quality_tips:I = 0x7f0d038d

.field public static final include_image_quality_tips_black:I = 0x7f0d038e

.field public static final include_king_kong_custom_seletable_view:I = 0x7f0d038f

.field public static final include_king_kong_custom_seletable_view_simple:I = 0x7f0d0390

.field public static final include_king_kong_custom_tool_box:I = 0x7f0d0391

.field public static final include_main_doc_header_dir:I = 0x7f0d0392

.field public static final include_main_doc_header_movecopy:I = 0x7f0d0393

.field public static final include_main_doc_header_root:I = 0x7f0d0394

.field public static final include_main_doc_header_view:I = 0x7f0d0395

.field public static final include_main_menu_fab:I = 0x7f0d0396

.field public static final include_obscuration_detail_item:I = 0x7f0d0397

.field public static final include_pdf_watermark_guide_arrow_down:I = 0x7f0d0398

.field public static final include_pdf_watermark_guide_arrow_top:I = 0x7f0d0399

.field public static final include_recommend_create_dir:I = 0x7f0d039a

.field public static final include_slient_ocr_test:I = 0x7f0d039b

.field public static final include_tips_network_msg:I = 0x7f0d039c

.field public static final include_translate_data_show:I = 0x7f0d039d

.field public static final include_translate_lang_select:I = 0x7f0d039e

.field public static final include_word_edit_bar:I = 0x7f0d039f

.field public static final include_word_json_edit_bar:I = 0x7f0d03a0

.field public static final ink_item_color:I = 0x7f0d03a1

.field public static final ink_note_fragment_edit_note:I = 0x7f0d03a2

.field public static final ink_note_page_layout:I = 0x7f0d03a3

.field public static final ink_settting_pnl:I = 0x7f0d03a4

.field public static final input_add_tag_dialog:I = 0x7f0d03a5

.field public static final input_pdf_pwd_dialog:I = 0x7f0d03a6

.field public static final ipo_test_fragment:I = 0x7f0d03a7

.field public static final item_accounts_grid:I = 0x7f0d03a8

.field public static final item_adapter_jagsaw_template:I = 0x7f0d03a9

.field public static final item_adapter_security_mark:I = 0x7f0d03aa

.field public static final item_all_connect_record:I = 0x7f0d03ab

.field public static final item_app_config_content:I = 0x7f0d03ac

.field public static final item_app_config_filter:I = 0x7f0d03ad

.field public static final item_app_config_tips:I = 0x7f0d03ae

.field public static final item_authority_managerment:I = 0x7f0d03af

.field public static final item_autocomposite_container:I = 0x7f0d03b0

.field public static final item_bank_journal_list_normal:I = 0x7f0d03b1

.field public static final item_bank_journal_list_unrecognize:I = 0x7f0d03b2

.field public static final item_batch_ocr_pre_image:I = 0x7f0d03b3

.field public static final item_bottom_menu:I = 0x7f0d03b4

.field public static final item_bottom_menu_group_divider:I = 0x7f0d03b5

.field public static final item_bottom_menu_group_title:I = 0x7f0d03b6

.field public static final item_bottom_menu_right_switch:I = 0x7f0d03b7

.field public static final item_capture_filter_setting:I = 0x7f0d03b8

.field public static final item_capture_menu_des:I = 0x7f0d03b9

.field public static final item_capture_menu_function:I = 0x7f0d03ba

.field public static final item_capture_menu_space:I = 0x7f0d03bb

.field public static final item_capture_pixel_all:I = 0x7f0d03bc

.field public static final item_capture_pixel_filter:I = 0x7f0d03bd

.field public static final item_capture_pixel_filter_new:I = 0x7f0d03be

.field public static final item_capture_refactor_setting_pixel:I = 0x7f0d03bf

.field public static final item_card_detail_banner_image:I = 0x7f0d03c0

.field public static final item_card_detail_ocr_detail:I = 0x7f0d03c1

.field public static final item_card_info:I = 0x7f0d03c2

.field public static final item_card_photo_page_list_image_list:I = 0x7f0d03c3

.field public static final item_card_security_desc:I = 0x7f0d03c4

.field public static final item_card_select_empty:I = 0x7f0d03c5

.field public static final item_card_select_group:I = 0x7f0d03c6

.field public static final item_card_select_item:I = 0x7f0d03c7

.field public static final item_certificate_detail_card_detail:I = 0x7f0d03c8

.field public static final item_certificate_edit:I = 0x7f0d03c9

.field public static final item_certificate_image:I = 0x7f0d03ca

.field public static final item_certificate_photo:I = 0x7f0d03cb

.field public static final item_certificate_photo_bottom:I = 0x7f0d03cc

.field public static final item_certificate_photo_menu:I = 0x7f0d03cd

.field public static final item_certificate_photo_menu_new:I = 0x7f0d03ce

.field public static final item_certificate_preview:I = 0x7f0d03cf

.field public static final item_certificate_preview_detail:I = 0x7f0d03d0

.field public static final item_checkout_pay_way:I = 0x7f0d03d1

.field public static final item_common_filter:I = 0x7f0d03d2

.field public static final item_contcact_on_esign_sharepage:I = 0x7f0d03d3

.field public static final item_content_bank_card_journal:I = 0x7f0d03d4

.field public static final item_count_category:I = 0x7f0d03d5

.field public static final item_custom_pager:I = 0x7f0d03d6

.field public static final item_data_security_desc:I = 0x7f0d03d7

.field public static final item_deep_cache_clean:I = 0x7f0d03d8

.field public static final item_dialog_btm_common_esign:I = 0x7f0d03d9

.field public static final item_doc_left_tag:I = 0x7f0d03da

.field public static final item_doc_sort_dlg:I = 0x7f0d03db

.field public static final item_doc_tag:I = 0x7f0d03dc

.field public static final item_doc_tag_flexbox:I = 0x7f0d03dd

.field public static final item_document_capture_guide:I = 0x7f0d03de

.field public static final item_drop_cnl_free_trial_enable:I = 0x7f0d03df

.field public static final item_drop_cnl_privileges:I = 0x7f0d03e0

.field public static final item_drop_cnl_purchase:I = 0x7f0d03e1

.field public static final item_drop_cnl_purchase_image:I = 0x7f0d03e2

.field public static final item_drop_cnl_top_banner_image:I = 0x7f0d03e3

.field public static final item_drop_cnl_top_loop_banner:I = 0x7f0d03e4

.field public static final item_drop_cnl_top_region_banner:I = 0x7f0d03e5

.field public static final item_drop_cnl_top_region_banner_image:I = 0x7f0d03e6

.field public static final item_edit_more:I = 0x7f0d03e7

.field public static final item_edu_gift_content:I = 0x7f0d03e8

.field public static final item_edu_market:I = 0x7f0d03e9

.field public static final item_ell_bottom:I = 0x7f0d03ea

.field public static final item_enhance_adapter:I = 0x7f0d03eb

.field public static final item_enhance_menu_gallery:I = 0x7f0d03ec

.field public static final item_enhance_menu_gallery_new:I = 0x7f0d03ed

.field public static final item_esign_contact:I = 0x7f0d03ee

.field public static final item_esign_contact_sign_status:I = 0x7f0d03ef

.field public static final item_esign_me_addsign:I = 0x7f0d03f0

.field public static final item_esign_share_file_or_link_item:I = 0x7f0d03f1

.field public static final item_esign_share_sign_img_galley:I = 0x7f0d03f2

.field public static final item_esign_share_type_dialog:I = 0x7f0d03f3

.field public static final item_esign_share_type_signdone:I = 0x7f0d03f4

.field public static final item_esign_signs_empty_hint:I = 0x7f0d03f5

.field public static final item_external_member_7days:I = 0x7f0d03f6

.field public static final item_first_premium_purchase_image:I = 0x7f0d03f7

.field public static final item_flow_text:I = 0x7f0d03f8

.field public static final item_folder_create_dialog_dir:I = 0x7f0d03f9

.field public static final item_folder_create_dialog_title:I = 0x7f0d03fa

.field public static final item_folder_scenario_create:I = 0x7f0d03fb

.field public static final item_function_recommend:I = 0x7f0d03fc

.field public static final item_genernal_bank_card_journal:I = 0x7f0d03fd

.field public static final item_gp_first_premium_right:I = 0x7f0d03fe

.field public static final item_gp_guide_scroll_image_page:I = 0x7f0d03ff

.field public static final item_gp_purchase_guide_comment:I = 0x7f0d0400

.field public static final item_gp_purchase_guide_comment2:I = 0x7f0d0401

.field public static final item_gp_purchase_guide_video_functions:I = 0x7f0d0402

.field public static final item_gp_purchase_guide_vip_functions:I = 0x7f0d0403

.field public static final item_gp_purchase_guide_vip_functions_list2:I = 0x7f0d0404

.field public static final item_gp_purchase_guide_vip_functions_list3:I = 0x7f0d0405

.field public static final item_greeting_card:I = 0x7f0d0406

.field public static final item_guide_cn_autoscroll_image_page:I = 0x7f0d0407

.field public static final item_guide_cn_looper_view_page:I = 0x7f0d0408

.field public static final item_guide_gp_purchase_item_comment:I = 0x7f0d0409

.field public static final item_guide_gp_purchase_item_function:I = 0x7f0d040a

.field public static final item_guide_gp_purchase_item_head:I = 0x7f0d040b

.field public static final item_guide_gp_purchase_item_normal:I = 0x7f0d040c

.field public static final item_guide_rejoin_eight_day:I = 0x7f0d040d

.field public static final item_headerview_doc_2_office:I = 0x7f0d040e

.field public static final item_hear_cnl_average_three:I = 0x7f0d040f

.field public static final item_hear_cnl_average_two:I = 0x7f0d0410

.field public static final item_hear_cnl_left_large_right_small:I = 0x7f0d0411

.field public static final item_hear_cnl_left_small_right_large:I = 0x7f0d0412

.field public static final item_home_banner_content:I = 0x7f0d0413

.field public static final item_home_banner_one_function:I = 0x7f0d0414

.field public static final item_home_banner_three_function:I = 0x7f0d0415

.field public static final item_home_banner_two_function:I = 0x7f0d0416

.field public static final item_image2word_imageview:I = 0x7f0d0417

.field public static final item_image_folder:I = 0x7f0d0418

.field public static final item_image_restore_introduction:I = 0x7f0d0419

.field public static final item_image_text_message:I = 0x7f0d041a

.field public static final item_image_thumb_48_48:I = 0x7f0d041b

.field public static final item_inno_lab_linear:I = 0x7f0d041c

.field public static final item_inno_lab_no_data_placeholder:I = 0x7f0d041d

.field public static final item_inno_lab_title:I = 0x7f0d041e

.field public static final item_inno_lab_two_tab:I = 0x7f0d041f

.field public static final item_invoice_browse:I = 0x7f0d0420

.field public static final item_invoice_check:I = 0x7f0d0421

.field public static final item_invoice_check_img:I = 0x7f0d0422

.field public static final item_invoice_check_type_left:I = 0x7f0d0423

.field public static final item_invoice_result:I = 0x7f0d0424

.field public static final item_lan_common_use:I = 0x7f0d0425

.field public static final item_largepic_mode_thumb:I = 0x7f0d0426

.field public static final item_long_image:I = 0x7f0d0427

.field public static final item_long_image_stitch:I = 0x7f0d0428

.field public static final item_long_stitch_bottom_mark:I = 0x7f0d0429

.field public static final item_lr_word:I = 0x7f0d042a

.field public static final item_lr_word_bottom_capture:I = 0x7f0d042b

.field public static final item_main_doc_cloud_disk_rec_tips:I = 0x7f0d042c

.field public static final item_main_doc_empty_card_bag:I = 0x7f0d042d

.field public static final item_main_doc_last_placeholder:I = 0x7f0d042e

.field public static final item_main_doc_optical_recognize:I = 0x7f0d042f

.field public static final item_main_home_show_more:I = 0x7f0d0430

.field public static final item_main_search_history:I = 0x7f0d0431

.field public static final item_main_search_referral:I = 0x7f0d0432

.field public static final item_main_search_referral_recommend:I = 0x7f0d0433

.field public static final item_maindoc_backup_list_mode_doc_type:I = 0x7f0d0434

.field public static final item_maindoc_card_bag_mode_doc_type:I = 0x7f0d0435

.field public static final item_maindoc_grid_mode_doc_type:I = 0x7f0d0436

.field public static final item_maindoc_grid_mode_doc_type_upgrade:I = 0x7f0d0437

.field public static final item_maindoc_grid_mode_folder_type:I = 0x7f0d0438

.field public static final item_maindoc_grid_mode_folder_type_upgrade:I = 0x7f0d0439

.field public static final item_maindoc_largepic_mode_doc_type:I = 0x7f0d043a

.field public static final item_maindoc_largepic_mode_folder_type:I = 0x7f0d043b

.field public static final item_maindoc_list_mode_ad_type:I = 0x7f0d043c

.field public static final item_maindoc_list_mode_doc_type:I = 0x7f0d043d

.field public static final item_maindoc_list_mode_doc_type_upgrade:I = 0x7f0d043e

.field public static final item_maindoc_list_mode_folder_type:I = 0x7f0d043f

.field public static final item_maindoc_list_mode_folder_type_upgrade:I = 0x7f0d0440

.field public static final item_maindoc_list_mode_search_footer_type:I = 0x7f0d0441

.field public static final item_maindoc_time_line_mode_doc_type:I = 0x7f0d0442

.field public static final item_me_page_area_free_card:I = 0x7f0d0443

.field public static final item_me_page_bar:I = 0x7f0d0444

.field public static final item_me_page_card:I = 0x7f0d0445

.field public static final item_me_page_enterprise_mall:I = 0x7f0d0446

.field public static final item_me_page_header:I = 0x7f0d0447

.field public static final item_me_page_header_new:I = 0x7f0d0448

.field public static final item_me_page_interval_reward:I = 0x7f0d0449

.field public static final item_me_page_king_kong:I = 0x7f0d044a

.field public static final item_me_page_login_opt:I = 0x7f0d044b

.field public static final item_me_page_o_vip_card:I = 0x7f0d044c

.field public static final item_me_page_o_vip_right:I = 0x7f0d044d

.field public static final item_me_page_settings_bottom:I = 0x7f0d044e

.field public static final item_me_page_settings_debug:I = 0x7f0d044f

.field public static final item_me_page_settings_edu_invite:I = 0x7f0d0450

.field public static final item_me_page_settings_top:I = 0x7f0d0451

.field public static final item_me_page_vip_card_new:I = 0x7f0d0452

.field public static final item_me_price_customer_tag:I = 0x7f0d0453

.field public static final item_me_price_detail_item_head:I = 0x7f0d0454

.field public static final item_me_price_detail_item_normal:I = 0x7f0d0455

.field public static final item_member:I = 0x7f0d0456

.field public static final item_mini_program_thumb_type_mul:I = 0x7f0d0457

.field public static final item_mini_program_thumb_type_two:I = 0x7f0d0458

.field public static final item_multi_image_edit:I = 0x7f0d0459

.field public static final item_new_sign_list_item:I = 0x7f0d045a

.field public static final item_newsign_me_page:I = 0x7f0d045b

.field public static final item_nine_phone_view:I = 0x7f0d045c

.field public static final item_ocr_language:I = 0x7f0d045d

.field public static final item_ocr_result_ocr_frame_view:I = 0x7f0d045e

.field public static final item_one_trial_renew:I = 0x7f0d045f

.field public static final item_other_share_in_img:I = 0x7f0d0460

.field public static final item_pad_share_image_preview:I = 0x7f0d0461

.field public static final item_page_detail_more:I = 0x7f0d0462

.field public static final item_page_list_a4:I = 0x7f0d0463

.field public static final item_page_list_ad:I = 0x7f0d0464

.field public static final item_page_list_ad_new:I = 0x7f0d0465

.field public static final item_page_list_image_edit_guide:I = 0x7f0d0466

.field public static final item_page_list_image_grid:I = 0x7f0d0467

.field public static final item_page_list_image_list:I = 0x7f0d0468

.field public static final item_page_list_ope_grid:I = 0x7f0d0469

.field public static final item_page_list_operate_item_new:I = 0x7f0d046a

.field public static final item_page_list_operate_item_topic:I = 0x7f0d046b

.field public static final item_page_lr_word:I = 0x7f0d046c

.field public static final item_pagelist_btm_ope:I = 0x7f0d046d

.field public static final item_pages_a4:I = 0x7f0d046e

.field public static final item_pdf_banner:I = 0x7f0d046f

.field public static final item_pdf_doc_grid_thumb:I = 0x7f0d0470

.field public static final item_pdf_edit_watermark_bottom:I = 0x7f0d0471

.field public static final item_pdf_edit_watermark_old_1:I = 0x7f0d0472

.field public static final item_pdf_edit_watermark_old_2:I = 0x7f0d0473

.field public static final item_pdf_edit_watermark_style_qrcode:I = 0x7f0d0474

.field public static final item_pdf_edit_watermark_style_qrcode_text:I = 0x7f0d0475

.field public static final item_pdf_edit_watermark_style_qrcode_text_style_two:I = 0x7f0d0476

.field public static final item_pdf_edit_watermark_top:I = 0x7f0d0477

.field public static final item_pdf_editing:I = 0x7f0d0478

.field public static final item_pdf_editing1:I = 0x7f0d0479

.field public static final item_pdf_gallery_dir:I = 0x7f0d047a

.field public static final item_pdf_gallery_file:I = 0x7f0d047b

.field public static final item_pdf_gallery_search_file:I = 0x7f0d047c

.field public static final item_pdf_kit_category:I = 0x7f0d047d

.field public static final item_pdf_kit_main:I = 0x7f0d047e

.field public static final item_pdf_signature_page:I = 0x7f0d047f

.field public static final item_permission_desc_cover:I = 0x7f0d0480

.field public static final item_ppt_thumb:I = 0x7f0d0481

.field public static final item_print_device:I = 0x7f0d0482

.field public static final item_print_device_title:I = 0x7f0d0483

.field public static final item_print_self_range:I = 0x7f0d0484

.field public static final item_print_type:I = 0x7f0d0485

.field public static final item_printer_darness:I = 0x7f0d0486

.field public static final item_printer_device:I = 0x7f0d0487

.field public static final item_printer_filter:I = 0x7f0d0488

.field public static final item_printer_filter_paper:I = 0x7f0d0489

.field public static final item_printer_paper:I = 0x7f0d048a

.field public static final item_printer_preview_image:I = 0x7f0d048b

.field public static final item_printer_search:I = 0x7f0d048c

.field public static final item_purchase:I = 0x7f0d048d

.field public static final item_purchase_debug_device_id:I = 0x7f0d048e

.field public static final item_purchase_debug_locale:I = 0x7f0d048f

.field public static final item_purchase_debug_page:I = 0x7f0d0490

.field public static final item_purchase_debug_request:I = 0x7f0d0491

.field public static final item_purchase_local:I = 0x7f0d0492

.field public static final item_purchase_local_gride:I = 0x7f0d0493

.field public static final item_purchase_local_horizontal:I = 0x7f0d0494

.field public static final item_purchase_new_style2:I = 0x7f0d0495

.field public static final item_qr_code_history_linear:I = 0x7f0d0496

.field public static final item_qr_code_history_title:I = 0x7f0d0497

.field public static final item_resort_merged_docs:I = 0x7f0d0498

.field public static final item_rich_message:I = 0x7f0d0499

.field public static final item_scan_done_add_tag:I = 0x7f0d049a

.field public static final item_scan_done_complete:I = 0x7f0d049b

.field public static final item_scan_done_expand_tag:I = 0x7f0d049c

.field public static final item_scan_done_tag:I = 0x7f0d049d

.field public static final item_scan_done_vip_month:I = 0x7f0d049e

.field public static final item_scan_done_vip_task:I = 0x7f0d049f

.field public static final item_scan_first_doc_congratulation:I = 0x7f0d04a0

.field public static final item_scan_first_doc_line:I = 0x7f0d04a1

.field public static final item_scan_first_doc_title:I = 0x7f0d04a2

.field public static final item_scan_first_doc_upgrade:I = 0x7f0d04a3

.field public static final item_scandone_ad_root:I = 0x7f0d04a4

.field public static final item_scandone_header:I = 0x7f0d04a5

.field public static final item_scandone_header_new:I = 0x7f0d04a6

.field public static final item_scandone_new_head:I = 0x7f0d04a7

.field public static final item_scene_card_show_reward:I = 0x7f0d04a8

.field public static final item_search_opertion_ad:I = 0x7f0d04a9

.field public static final item_select_ocr:I = 0x7f0d04aa

.field public static final item_setting_page_cs_pdf_vip:I = 0x7f0d04ab

.field public static final item_setting_page_line:I = 0x7f0d04ac

.field public static final item_setting_page_my_account_login_or_out:I = 0x7f0d04ad

.field public static final item_setting_page_right_txt_line:I = 0x7f0d04ae

.field public static final item_setting_page_title:I = 0x7f0d04af

.field public static final item_share_bank_card_journal:I = 0x7f0d04b0

.field public static final item_share_channel_select:I = 0x7f0d04b1

.field public static final item_share_dir_guide_dialog:I = 0x7f0d04b2

.field public static final item_share_dir_member_avatar:I = 0x7f0d04b3

.field public static final item_share_excel_raw_layout:I = 0x7f0d04b4

.field public static final item_share_image_preview_new:I = 0x7f0d04b5

.field public static final item_share_option:I = 0x7f0d04b6

.field public static final item_share_panel_pdf_water_mark_btm:I = 0x7f0d04b7

.field public static final item_share_panel_pdf_water_mark_old:I = 0x7f0d04b8

.field public static final item_share_panel_pdf_water_mark_top:I = 0x7f0d04b9

.field public static final item_share_preview_watermark_bottom:I = 0x7f0d04ba

.field public static final item_share_raw_layout:I = 0x7f0d04bb

.field public static final item_sign_share_list:I = 0x7f0d04bc

.field public static final item_signature_thumb:I = 0x7f0d04bd

.field public static final item_signature_thumb_new:I = 0x7f0d04be

.field public static final item_single_choice:I = 0x7f0d04bf

.field public static final item_super_dir_linear:I = 0x7f0d04c0

.field public static final item_system_msg_team_permission:I = 0x7f0d04c1

.field public static final item_tag_manage:I = 0x7f0d04c2

.field public static final item_tag_manage_new:I = 0x7f0d04c3

.field public static final item_tag_manage_new_add_btn:I = 0x7f0d04c4

.field public static final item_team_invite:I = 0x7f0d04c5

.field public static final item_team_invite_result:I = 0x7f0d04c6

.field public static final item_ten_year_back_list_item:I = 0x7f0d04c7

.field public static final item_ten_year_back_list_title:I = 0x7f0d04c8

.field public static final item_text_message:I = 0x7f0d04c9

.field public static final item_third_service:I = 0x7f0d04ca

.field public static final item_time_line_add_today:I = 0x7f0d04cb

.field public static final item_time_line_default_middle_view:I = 0x7f0d04cc

.field public static final item_time_line_more_list:I = 0x7f0d04cd

.field public static final item_time_line_office_middle_view:I = 0x7f0d04ce

.field public static final item_time_line_year:I = 0x7f0d04cf

.field public static final item_tool_page_ad_banner:I = 0x7f0d04d0

.field public static final item_tool_page_average_two:I = 0x7f0d04d1

.field public static final item_tool_page_circle_cell:I = 0x7f0d04d2

.field public static final item_tool_page_five_drop_first:I = 0x7f0d04d3

.field public static final item_tool_page_more_functions:I = 0x7f0d04d4

.field public static final item_tool_page_squre_cell:I = 0x7f0d04d5

.field public static final item_tool_page_title:I = 0x7f0d04d6

.field public static final item_tool_page_top_three_bottom_two:I = 0x7f0d04d7

.field public static final item_tool_page_v2_left_one_right_two:I = 0x7f0d04d8

.field public static final item_tool_page_v2_left_two_right_one:I = 0x7f0d04d9

.field public static final item_tool_page_v2_recent_use:I = 0x7f0d04da

.field public static final item_tool_page_v2_square:I = 0x7f0d04db

.field public static final item_tool_page_v2_title_more:I = 0x7f0d04dc

.field public static final item_tool_page_v2_top_three_bottom_one:I = 0x7f0d04dd

.field public static final item_topic_page:I = 0x7f0d04de

.field public static final item_topic_page_black:I = 0x7f0d04df

.field public static final item_topic_scan_pager:I = 0x7f0d04e0

.field public static final item_transfer_doc:I = 0x7f0d04e1

.field public static final item_transfer_doc_tips_snack_bar:I = 0x7f0d04e2

.field public static final item_translate_index:I = 0x7f0d04e3

.field public static final item_translate_lan:I = 0x7f0d04e4

.field public static final item_translate_lang:I = 0x7f0d04e5

.field public static final item_translate_result_new_content:I = 0x7f0d04e6

.field public static final item_translate_tag:I = 0x7f0d04e7

.field public static final item_tv_gp_guide_style3:I = 0x7f0d04e8

.field public static final item_upgrade:I = 0x7f0d04e9

.field public static final item_view_bind_account:I = 0x7f0d04ea

.field public static final item_vip:I = 0x7f0d04eb

.field public static final item_work_flow_auto_send_email_content:I = 0x7f0d04ec

.field public static final item_work_flow_auto_send_email_content_footer:I = 0x7f0d04ed

.field public static final item_work_flow_auto_send_email_content_item:I = 0x7f0d04ee

.field public static final item_work_flow_auto_send_email_head:I = 0x7f0d04ef

.field public static final item_work_flow_cloud_switch_content:I = 0x7f0d04f0

.field public static final item_work_flow_cloud_switch_head:I = 0x7f0d04f1

.field public static final item_work_flow_third_net_disk_content:I = 0x7f0d04f2

.field public static final item_work_flow_third_net_disk_item:I = 0x7f0d04f3

.field public static final item_work_flow_upload_third_net_disk_head:I = 0x7f0d04f4

.field public static final item_workflow_cloud_switch:I = 0x7f0d04f5

.field public static final layout_a_key_other_email_or_phone:I = 0x7f0d04f6

.field public static final layout_a_key_wechat:I = 0x7f0d04f7

.field public static final layout_actionbar_certificate_detail:I = 0x7f0d04f8

.field public static final layout_actionbar_default_verifycode_login:I = 0x7f0d04f9

.field public static final layout_actionbar_imagepage:I = 0x7f0d04fa

.field public static final layout_actionbar_imagepage_collaborate:I = 0x7f0d04fb

.field public static final layout_actionbar_imagepage_pic2word:I = 0x7f0d04fc

.field public static final layout_actionbar_imagepage_unmodify:I = 0x7f0d04fd

.field public static final layout_actionbar_menu_page_list_large_img_style:I = 0x7f0d04fe

.field public static final layout_actionbar_new_orc_result_actionbar:I = 0x7f0d04ff

.field public static final layout_actionbar_pagedetail_image:I = 0x7f0d0500

.field public static final layout_actionbar_pagedetail_text:I = 0x7f0d0501

.field public static final layout_ad_scandone_reward:I = 0x7f0d0502

.field public static final layout_after_scan_premium_animation:I = 0x7f0d0503

.field public static final layout_api_interstitial_dialog:I = 0x7f0d0504

.field public static final layout_camera_test_hint:I = 0x7f0d0505

.field public static final layout_cap_wave:I = 0x7f0d0506

.field public static final layout_certifiate_print_tips:I = 0x7f0d0507

.field public static final layout_certificate_photo_cover:I = 0x7f0d0508

.field public static final layout_check_icon:I = 0x7f0d0509

.field public static final layout_cl_not_ocr_recognize:I = 0x7f0d050a

.field public static final layout_cn_email_login_register_edit:I = 0x7f0d050b

.field public static final layout_common_other_login:I = 0x7f0d050c

.field public static final layout_cs_pdf_vip_buy_success_right_item:I = 0x7f0d050d

.field public static final layout_custom_image_toast:I = 0x7f0d050e

.field public static final layout_custom_image_toast_new:I = 0x7f0d050f

.field public static final layout_doc_import_header_entrance_item:I = 0x7f0d0510

.field public static final layout_doc_import_item:I = 0x7f0d0511

.field public static final layout_doc_import_permission_setting:I = 0x7f0d0512

.field public static final layout_doc_import_select_toolbar:I = 0x7f0d0513

.field public static final layout_doc_name_time_item:I = 0x7f0d0514

.field public static final layout_doc_page_empty_view:I = 0x7f0d0515

.field public static final layout_doc_page_empty_view_search:I = 0x7f0d0516

.field public static final layout_doc_shortcut:I = 0x7f0d0517

.field public static final layout_drop_cnl_top_scan:I = 0x7f0d0518

.field public static final layout_edit_containter_time_item:I = 0x7f0d0519

.field public static final layout_empty_paper_list:I = 0x7f0d051a

.field public static final layout_empty_search:I = 0x7f0d051b

.field public static final layout_enhance_menu_item:I = 0x7f0d051c

.field public static final layout_error_msg_and_privacy_agreement:I = 0x7f0d051d

.field public static final layout_esign_main_btm_sapce:I = 0x7f0d051e

.field public static final layout_external_member_retained_item_available:I = 0x7f0d051f

.field public static final layout_five_stars:I = 0x7f0d0520

.field public static final layout_float_view:I = 0x7f0d0521

.field public static final layout_focus_marker:I = 0x7f0d0522

.field public static final layout_function_recommend_contain_ai:I = 0x7f0d0523

.field public static final layout_function_recommend_no_ai:I = 0x7f0d0524

.field public static final layout_gp_guide_gallery1:I = 0x7f0d0525

.field public static final layout_gp_guide_gallery2:I = 0x7f0d0526

.field public static final layout_gp_guide_gallery3:I = 0x7f0d0527

.field public static final layout_gp_guide_gallery4:I = 0x7f0d0528

.field public static final layout_gp_guide_gallery5:I = 0x7f0d0529

.field public static final layout_gp_guide_gallery6:I = 0x7f0d052a

.field public static final layout_guide_gp_purchase_2:I = 0x7f0d052b

.field public static final layout_guide_gp_purchase_ios_faker3:I = 0x7f0d052c

.field public static final layout_handingwrite_hint:I = 0x7f0d052d

.field public static final layout_header_empty_view:I = 0x7f0d052e

.field public static final layout_image_adjust:I = 0x7f0d052f

.field public static final layout_image_page_tips:I = 0x7f0d0530

.field public static final layout_imagepage_bottom:I = 0x7f0d0531

.field public static final layout_imagepage_bottom_unmodify:I = 0x7f0d0532

.field public static final layout_imagepage_share:I = 0x7f0d0533

.field public static final layout_import_office_file_guide:I = 0x7f0d0534

.field public static final layout_item_export_other:I = 0x7f0d0535

.field public static final layout_item_file_type_filter:I = 0x7f0d0536

.field public static final layout_item_raw_other:I = 0x7f0d0537

.field public static final layout_king_kong_custom_tool:I = 0x7f0d0538

.field public static final layout_large_img_change_mode_tips:I = 0x7f0d0539

.field public static final layout_last_share_tip:I = 0x7f0d053a

.field public static final layout_last_share_tip_color:I = 0x7f0d053b

.field public static final layout_local_doc_folder_item:I = 0x7f0d053c

.field public static final layout_login_force_guide_rights:I = 0x7f0d053d

.field public static final layout_login_main_google_login:I = 0x7f0d053e

.field public static final layout_login_main_other_login:I = 0x7f0d053f

.field public static final layout_main_doc_stay_left_tag_list:I = 0x7f0d0540

.field public static final layout_main_doc_stay_top_tag_list:I = 0x7f0d0541

.field public static final layout_main_home_bottom_fab:I = 0x7f0d0542

.field public static final layout_main_home_bottom_new_fab:I = 0x7f0d0543

.field public static final layout_main_home_bottom_tab:I = 0x7f0d0544

.field public static final layout_main_home_empty_view:I = 0x7f0d0545

.field public static final layout_main_home_foot_view:I = 0x7f0d0546

.field public static final layout_main_home_header_view:I = 0x7f0d0547

.field public static final layout_main_home_kingkong:I = 0x7f0d0548

.field public static final layout_main_home_new_user_gift:I = 0x7f0d0549

.field public static final layout_main_special_dir_empty_item:I = 0x7f0d054a

.field public static final layout_me_pag_vip_right_view:I = 0x7f0d054b

.field public static final layout_member_reward_item_grid_available:I = 0x7f0d054c

.field public static final layout_move_copy_doc_page_empty_view:I = 0x7f0d054d

.field public static final layout_new_life_time_purchase:I = 0x7f0d054e

.field public static final layout_no_watermark_share_video:I = 0x7f0d054f

.field public static final layout_notify_office_convert_success:I = 0x7f0d0550

.field public static final layout_notify_office_convert_success_main:I = 0x7f0d0551

.field public static final layout_novice_task:I = 0x7f0d0552

.field public static final layout_nps_commit:I = 0x7f0d0553

.field public static final layout_nps_maxheight_edit:I = 0x7f0d0554

.field public static final layout_nps_question_option:I = 0x7f0d0555

.field public static final layout_ocr_language_scan:I = 0x7f0d0556

.field public static final layout_one_login_auth_custom:I = 0x7f0d0557

.field public static final layout_one_login_auth_half_screen_for_compliance:I = 0x7f0d0558

.field public static final layout_one_login_auth_half_screen_for_login:I = 0x7f0d0559

.field public static final layout_one_login_privacy_title:I = 0x7f0d055a

.field public static final layout_page_detail_bottom_more:I = 0x7f0d055b

.field public static final layout_page_detail_image_bottom:I = 0x7f0d055c

.field public static final layout_page_detail_word_bottom:I = 0x7f0d055d

.field public static final layout_page_list_add_image:I = 0x7f0d055e

.field public static final layout_page_list_grid_continue_add:I = 0x7f0d055f

.field public static final layout_page_list_large_continue_add:I = 0x7f0d0560

.field public static final layout_page_list_new_foot_view:I = 0x7f0d0561

.field public static final layout_parent:I = 0x7f0d0562

.field public static final layout_payment_choose_view:I = 0x7f0d0563

.field public static final layout_pdf_doc_share_link_grid_item:I = 0x7f0d0564

.field public static final layout_pdf_share_link_card_item:I = 0x7f0d0565

.field public static final layout_pdf_share_link_grid_item:I = 0x7f0d0566

.field public static final layout_permission_desc_cover:I = 0x7f0d0567

.field public static final layout_print_setting_image_item:I = 0x7f0d0568

.field public static final layout_print_setting_other:I = 0x7f0d0569

.field public static final layout_print_setting_page_range:I = 0x7f0d056a

.field public static final layout_print_setting_preview:I = 0x7f0d056b

.field public static final layout_print_setting_sp:I = 0x7f0d056c

.field public static final layout_print_setting_title:I = 0x7f0d056d

.field public static final layout_print_tips:I = 0x7f0d056e

.field public static final layout_printer_property_buy_item:I = 0x7f0d056f

.field public static final layout_printer_property_click_item:I = 0x7f0d0570

.field public static final layout_printer_property_common_text:I = 0x7f0d0571

.field public static final layout_printer_property_connect:I = 0x7f0d0572

.field public static final layout_printer_property_cover:I = 0x7f0d0573

.field public static final layout_printer_property_feed_back:I = 0x7f0d0574

.field public static final layout_ps_detected_loading:I = 0x7f0d0575

.field public static final layout_purchase_bottom_container_shadow:I = 0x7f0d0576

.field public static final layout_purchase_new_style2_content:I = 0x7f0d0577

.field public static final layout_purchase_new_viewpager_container:I = 0x7f0d0578

.field public static final layout_purchase_viewpager:I = 0x7f0d0579

.field public static final layout_purchase_viewpager_container:I = 0x7f0d057a

.field public static final layout_qr_code_bottom_content:I = 0x7f0d057b

.field public static final layout_qr_code_share:I = 0x7f0d057c

.field public static final layout_recommend_dir:I = 0x7f0d057d

.field public static final layout_recommend_dir_snack_bar:I = 0x7f0d057e

.field public static final layout_recommend_enhance_mode_set:I = 0x7f0d057f

.field public static final layout_rejoin_benefit_right_item:I = 0x7f0d0580

.field public static final layout_rotate_image_text:I = 0x7f0d0581

.field public static final layout_scan_done_vip_month_card_1:I = 0x7f0d0582

.field public static final layout_scan_done_vip_month_card_2:I = 0x7f0d0583

.field public static final layout_scan_done_vip_month_red_envelope:I = 0x7f0d0584

.field public static final layout_scan_done_vip_task_toast:I = 0x7f0d0585

.field public static final layout_scankit_title:I = 0x7f0d0586

.field public static final layout_scenario_folder_empty_view:I = 0x7f0d0587

.field public static final layout_screenshot_import:I = 0x7f0d0588

.field public static final layout_search_bar:I = 0x7f0d0589

.field public static final layout_search_bar_ad:I = 0x7f0d058a

.field public static final layout_second:I = 0x7f0d058b

.field public static final layout_senior_folder_certificate_card:I = 0x7f0d058c

.field public static final layout_set_default_pdf_app_tip:I = 0x7f0d058d

.field public static final layout_set_default_pdf_preview_backup_tip:I = 0x7f0d058e

.field public static final layout_sheet_button_item:I = 0x7f0d058f

.field public static final layout_sheet_buttons:I = 0x7f0d0590

.field public static final layout_signature_edit_heheqian:I = 0x7f0d0591

.field public static final layout_signature_edit_new:I = 0x7f0d0592

.field public static final layout_signature_edit_new_foresign:I = 0x7f0d0593

.field public static final layout_signature_paging_seal_preview:I = 0x7f0d0594

.field public static final layout_signature_tab_guide:I = 0x7f0d0595

.field public static final layout_signature_tips_hint:I = 0x7f0d0596

.field public static final layout_size_popwin_header:I = 0x7f0d0597

.field public static final layout_stripe_subscription_item:I = 0x7f0d0598

.field public static final layout_third_login:I = 0x7f0d0599

.field public static final layout_timer_alert_dialog:I = 0x7f0d059a

.field public static final layout_toast_image:I = 0x7f0d059b

.field public static final layout_verify_phone_code:I = 0x7f0d059c

.field public static final layout_vip_month_promotion_function_item:I = 0x7f0d059d

.field public static final layout_vip_month_tips_in_more_dialog:I = 0x7f0d059e

.field public static final layout_vip_upgrade_gift_item:I = 0x7f0d059f

.field public static final layout_vip_upgrade_right_item:I = 0x7f0d05a0

.field public static final layout_web_toolbar_menu_text:I = 0x7f0d05a1

.field public static final like:I = 0x7f0d05a2

.field public static final link_textview:I = 0x7f0d05a3

.field public static final ll_recall_price_ys_month_select:I = 0x7f0d05a4

.field public static final m3_alert_dialog:I = 0x7f0d05a5

.field public static final m3_alert_dialog_actions:I = 0x7f0d05a6

.field public static final m3_alert_dialog_title:I = 0x7f0d05a7

.field public static final m3_auto_complete_simple_item:I = 0x7f0d05a8

.field public static final m3_side_sheet_dialog:I = 0x7f0d05a9

.field public static final main_dialog_main_bottom_more:I = 0x7f0d05aa

.field public static final main_dialog_main_bottom_more_new:I = 0x7f0d05ab

.field public static final main_doc_grid:I = 0x7f0d05ac

.field public static final main_doc_list:I = 0x7f0d05ad

.field public static final main_doc_list_10:I = 0x7f0d05ae

.field public static final main_doc_rec_external_import_interceptor:I = 0x7f0d05af

.field public static final main_doc_rec_new_doc_cn_interceptor:I = 0x7f0d05b0

.field public static final main_doc_rec_new_doc_gp_interceptor:I = 0x7f0d05b1

.field public static final main_doc_rec_permission_interceptor:I = 0x7f0d05b2

.field public static final main_home_middle_operation:I = 0x7f0d05b3

.field public static final main_home_top_message_layout:I = 0x7f0d05b4

.field public static final main_long_press_guide:I = 0x7f0d05b5

.field public static final main_menu_team:I = 0x7f0d05b6

.field public static final main_menu_team_empty:I = 0x7f0d05b7

.field public static final main_menu_team_pull_parent:I = 0x7f0d05b8

.field public static final main_middle_native_layout:I = 0x7f0d05b9

.field public static final main_page_top_message_layout:I = 0x7f0d05ba

.field public static final main_team_empty_root_folder:I = 0x7f0d05bb

.field public static final material_chip_input_combo:I = 0x7f0d05bc

.field public static final material_clock_display:I = 0x7f0d05bd

.field public static final material_clock_display_divider:I = 0x7f0d05be

.field public static final material_clock_period_toggle:I = 0x7f0d05bf

.field public static final material_clock_period_toggle_land:I = 0x7f0d05c0

.field public static final material_clockface_textview:I = 0x7f0d05c1

.field public static final material_clockface_view:I = 0x7f0d05c2

.field public static final material_radial_view_group:I = 0x7f0d05c3

.field public static final material_textinput_timepicker:I = 0x7f0d05c4

.field public static final material_time_chip:I = 0x7f0d05c5

.field public static final material_time_input:I = 0x7f0d05c6

.field public static final material_timepicker:I = 0x7f0d05c7

.field public static final material_timepicker_dialog:I = 0x7f0d05c8

.field public static final material_timepicker_textinput_display:I = 0x7f0d05c9

.field public static final max_hybrid_native_ad_view:I = 0x7f0d05ca

.field public static final max_native_ad_banner_icon_and_text_layout:I = 0x7f0d05cb

.field public static final max_native_ad_banner_view:I = 0x7f0d05cc

.field public static final max_native_ad_leader_view:I = 0x7f0d05cd

.field public static final max_native_ad_media_banner_view:I = 0x7f0d05ce

.field public static final max_native_ad_medium_template_1:I = 0x7f0d05cf

.field public static final max_native_ad_mrec_view:I = 0x7f0d05d0

.field public static final max_native_ad_recycler_view_item:I = 0x7f0d05d1

.field public static final max_native_ad_small_template_1:I = 0x7f0d05d2

.field public static final max_native_ad_vertical_banner_view:I = 0x7f0d05d3

.field public static final max_native_ad_vertical_leader_view:I = 0x7f0d05d4

.field public static final max_native_ad_vertical_media_banner_view:I = 0x7f0d05d5

.field public static final me_page_waterfall:I = 0x7f0d05d6

.field public static final mediation_debugger_ad_unit_detail_activity:I = 0x7f0d05d7

.field public static final mediation_debugger_list_item_right_detail:I = 0x7f0d05d8

.field public static final mediation_debugger_list_section:I = 0x7f0d05d9

.field public static final mediation_debugger_list_section_centered:I = 0x7f0d05da

.field public static final mediation_debugger_list_view:I = 0x7f0d05db

.field public static final mediation_debugger_multi_ad_activity:I = 0x7f0d05dc

.field public static final mediation_debugger_text_view_activity:I = 0x7f0d05dd

.field public static final menu_printer:I = 0x7f0d05de

.field public static final merge_main_camera_btn:I = 0x7f0d05df

.field public static final modify_annotation_style:I = 0x7f0d05e0

.field public static final modify_security_mark_add:I = 0x7f0d05e1

.field public static final modify_smudge_annotation_bar:I = 0x7f0d05e2

.field public static final modify_water_mark_bar:I = 0x7f0d05e3

.field public static final motion_pagelist_coordination_header:I = 0x7f0d05e4

.field public static final movepage:I = 0x7f0d05e5

.field public static final mtrl_alert_dialog:I = 0x7f0d05e6

.field public static final mtrl_alert_dialog_actions:I = 0x7f0d05e7

.field public static final mtrl_alert_dialog_title:I = 0x7f0d05e8

.field public static final mtrl_alert_select_dialog_item:I = 0x7f0d05e9

.field public static final mtrl_alert_select_dialog_multichoice:I = 0x7f0d05ea

.field public static final mtrl_alert_select_dialog_singlechoice:I = 0x7f0d05eb

.field public static final mtrl_auto_complete_simple_item:I = 0x7f0d05ec

.field public static final mtrl_calendar_day:I = 0x7f0d05ed

.field public static final mtrl_calendar_day_of_week:I = 0x7f0d05ee

.field public static final mtrl_calendar_days_of_week:I = 0x7f0d05ef

.field public static final mtrl_calendar_horizontal:I = 0x7f0d05f0

.field public static final mtrl_calendar_month:I = 0x7f0d05f1

.field public static final mtrl_calendar_month_labeled:I = 0x7f0d05f2

.field public static final mtrl_calendar_month_navigation:I = 0x7f0d05f3

.field public static final mtrl_calendar_months:I = 0x7f0d05f4

.field public static final mtrl_calendar_vertical:I = 0x7f0d05f5

.field public static final mtrl_calendar_year:I = 0x7f0d05f6

.field public static final mtrl_layout_snackbar:I = 0x7f0d05f7

.field public static final mtrl_layout_snackbar_include:I = 0x7f0d05f8

.field public static final mtrl_navigation_rail_item:I = 0x7f0d05f9

.field public static final mtrl_picker_actions:I = 0x7f0d05fa

.field public static final mtrl_picker_dialog:I = 0x7f0d05fb

.field public static final mtrl_picker_fullscreen:I = 0x7f0d05fc

.field public static final mtrl_picker_header_dialog:I = 0x7f0d05fd

.field public static final mtrl_picker_header_fullscreen:I = 0x7f0d05fe

.field public static final mtrl_picker_header_selection_text:I = 0x7f0d05ff

.field public static final mtrl_picker_header_title_text:I = 0x7f0d0600

.field public static final mtrl_picker_header_toggle:I = 0x7f0d0601

.field public static final mtrl_picker_text_input_date:I = 0x7f0d0602

.field public static final mtrl_picker_text_input_date_range:I = 0x7f0d0603

.field public static final mtrl_search_bar:I = 0x7f0d0604

.field public static final mtrl_search_view:I = 0x7f0d0605

.field public static final naire_question_layout:I = 0x7f0d0606

.field public static final naire_result_layout:I = 0x7f0d0607

.field public static final negative_premium_svip_popup:I = 0x7f0d0608

.field public static final new_guide_mian:I = 0x7f0d0609

.field public static final new_guide_video_empty:I = 0x7f0d060a

.field public static final nfc_loading_layout:I = 0x7f0d060b

.field public static final notification:I = 0x7f0d060c

.field public static final notification_10:I = 0x7f0d060d

.field public static final notification_action:I = 0x7f0d060e

.field public static final notification_action_tombstone:I = 0x7f0d060f

.field public static final notification_media_action:I = 0x7f0d0610

.field public static final notification_media_cancel_action:I = 0x7f0d0611

.field public static final notification_template_big_media:I = 0x7f0d0612

.field public static final notification_template_big_media_custom:I = 0x7f0d0613

.field public static final notification_template_big_media_narrow:I = 0x7f0d0614

.field public static final notification_template_big_media_narrow_custom:I = 0x7f0d0615

.field public static final notification_template_custom_big:I = 0x7f0d0616

.field public static final notification_template_icon_group:I = 0x7f0d0617

.field public static final notification_template_lines_media:I = 0x7f0d0618

.field public static final notification_template_media:I = 0x7f0d0619

.field public static final notification_template_media_custom:I = 0x7f0d061a

.field public static final notification_template_part_chronometer:I = 0x7f0d061b

.field public static final notification_template_part_time:I = 0x7f0d061c

.field public static final ocr_open_api:I = 0x7f0d061d

.field public static final ocr_region_toast_layout:I = 0x7f0d061e

.field public static final office_ppt_guide:I = 0x7f0d061f

.field public static final office_preview_bottom:I = 0x7f0d0620

.field public static final office_preview_toolbar_edit:I = 0x7f0d0621

.field public static final office_preview_toolbar_normal:I = 0x7f0d0622

.field public static final office_preview_word_edit_bottom:I = 0x7f0d0623

.field public static final offline_folder_password_layout:I = 0x7f0d0624

.field public static final on_screen_hint:I = 0x7f0d0625

.field public static final ope_view_count_number:I = 0x7f0d0626

.field public static final ope_view_image_editing:I = 0x7f0d0627

.field public static final operation_bottom_grid_item:I = 0x7f0d0628

.field public static final operation_bottom_list_item:I = 0x7f0d0629

.field public static final operation_grid_item:I = 0x7f0d062a

.field public static final operation_grid_item_pdf:I = 0x7f0d062b

.field public static final operation_list_item:I = 0x7f0d062c

.field public static final operation_list_item_pdf:I = 0x7f0d062d

.field public static final pack_image_note:I = 0x7f0d062e

.field public static final pack_team_image_note:I = 0x7f0d062f

.field public static final page_detail_tab_indicator:I = 0x7f0d0630

.field public static final page_list_image_container:I = 0x7f0d0631

.field public static final page_list_operate_cam_exam_diversion:I = 0x7f0d0632

.field public static final page_list_operate_item:I = 0x7f0d0633

.field public static final page_list_operate_item_new:I = 0x7f0d0634

.field public static final page_list_operate_item_topic:I = 0x7f0d0635

.field public static final pagelist_image_text_container:I = 0x7f0d0636

.field public static final pagelist_large_img_bottom_bar:I = 0x7f0d0637

.field public static final pagelist_new_user_guide_dialog_2:I = 0x7f0d0638

.field public static final pagelist_normal_mode_bottom_bar:I = 0x7f0d0639

.field public static final pager_navigator_layout:I = 0x7f0d063a

.field public static final pager_navigator_layout_no_scroll:I = 0x7f0d063b

.field public static final passive_premium_account_popup:I = 0x7f0d063c

.field public static final pdf_actionbar_right:I = 0x7f0d063d

.field public static final pdf_edit_bottom_bar:I = 0x7f0d063e

.field public static final pdf_explanation_layout:I = 0x7f0d063f

.field public static final pdf_size_item:I = 0x7f0d0640

.field public static final pin_textview_layout:I = 0x7f0d0641

.field public static final pnl_add_certificate_tips:I = 0x7f0d0642

.field public static final pnl_all_capture_function_tips:I = 0x7f0d0643

.field public static final pnl_auto_capture_tips:I = 0x7f0d0644

.field public static final pnl_bank_card_journal_capture_shutter_layout:I = 0x7f0d0645

.field public static final pnl_book_capture_shutter_refactor:I = 0x7f0d0646

.field public static final pnl_book_guide_layout:I = 0x7f0d0647

.field public static final pnl_book_preview_layout:I = 0x7f0d0648

.field public static final pnl_bottom_book_result:I = 0x7f0d0649

.field public static final pnl_bottom_multi_capture_result:I = 0x7f0d064a

.field public static final pnl_calibrateview:I = 0x7f0d064b

.field public static final pnl_cancel_account_des_item:I = 0x7f0d064c

.field public static final pnl_cancel_warning_item:I = 0x7f0d064d

.field public static final pnl_capture_bank_card_journal_middle_layout:I = 0x7f0d064e

.field public static final pnl_capture_certificate_thumb:I = 0x7f0d064f

.field public static final pnl_capture_click_thumb_tips:I = 0x7f0d0650

.field public static final pnl_capture_count_guide_layout:I = 0x7f0d0651

.field public static final pnl_capture_excel_thumb:I = 0x7f0d0652

.field public static final pnl_capture_guide_common:I = 0x7f0d0653

.field public static final pnl_capture_invoice_guide_layout:I = 0x7f0d0654

.field public static final pnl_capture_merge_test:I = 0x7f0d0655

.field public static final pnl_capture_merge_tips:I = 0x7f0d0656

.field public static final pnl_capture_multi_thumb:I = 0x7f0d0657

.field public static final pnl_capture_ocr:I = 0x7f0d0658

.field public static final pnl_capture_ocr_new:I = 0x7f0d0659

.field public static final pnl_capture_ocr_thumb:I = 0x7f0d065a

.field public static final pnl_capture_ocr_thumb_refactor:I = 0x7f0d065b

.field public static final pnl_capture_signature_capture_new_middle_layout:I = 0x7f0d065c

.field public static final pnl_capture_signature_capture_shutter_layout:I = 0x7f0d065d

.field public static final pnl_capture_topic_paper_thumb:I = 0x7f0d065e

.field public static final pnl_capture_translate_thumb_refactor:I = 0x7f0d065f

.field public static final pnl_capture_word_thumb:I = 0x7f0d0660

.field public static final pnl_certificate_capture_shutter_refactor:I = 0x7f0d0661

.field public static final pnl_certificate_menu_mode:I = 0x7f0d0662

.field public static final pnl_certificate_menu_mode_cn_zh:I = 0x7f0d0663

.field public static final pnl_certificate_menu_mode_list:I = 0x7f0d0664

.field public static final pnl_certificate_menu_mode_list_new:I = 0x7f0d0665

.field public static final pnl_certificate_menu_mode_other:I = 0x7f0d0666

.field public static final pnl_certificate_menu_mode_us_en:I = 0x7f0d0667

.field public static final pnl_certificate_middle_layout:I = 0x7f0d0668

.field public static final pnl_certificate_middle_layout_new:I = 0x7f0d0669

.field public static final pnl_certificate_photo_menu_layout:I = 0x7f0d066a

.field public static final pnl_checkbox_mask:I = 0x7f0d066b

.field public static final pnl_combine_import:I = 0x7f0d066c

.field public static final pnl_combine_import_doc:I = 0x7f0d066d

.field public static final pnl_combine_photo:I = 0x7f0d066e

.field public static final pnl_container_nps:I = 0x7f0d066f

.field public static final pnl_container_paper_property:I = 0x7f0d0670

.field public static final pnl_container_question:I = 0x7f0d0671

.field public static final pnl_count_number_capture_scene:I = 0x7f0d0672

.field public static final pnl_count_number_guide_layout:I = 0x7f0d0673

.field public static final pnl_deep_clean_list_header:I = 0x7f0d0674

.field public static final pnl_dlg_btn:I = 0x7f0d0675

.field public static final pnl_edu_auth_success_dialog:I = 0x7f0d0676

.field public static final pnl_enhance_mode_layout:I = 0x7f0d0677

.field public static final pnl_enhance_mode_layout_new:I = 0x7f0d0678

.field public static final pnl_evidence_capture_preview_layout:I = 0x7f0d0679

.field public static final pnl_evidence_capture_shutter_layout_refactor:I = 0x7f0d067a

.field public static final pnl_evidence_capture_tips_layout:I = 0x7f0d067b

.field public static final pnl_excel_capture_new_middle_layout:I = 0x7f0d067c

.field public static final pnl_excel_capture_new_shutter_refactor:I = 0x7f0d067d

.field public static final pnl_excel_capture_preview_layout:I = 0x7f0d067e

.field public static final pnl_excel_capture_shutter_refactor:I = 0x7f0d067f

.field public static final pnl_excel_rec:I = 0x7f0d0680

.field public static final pnl_fail_cancel_reason:I = 0x7f0d0681

.field public static final pnl_filter_file_guide_pop_tips:I = 0x7f0d0682

.field public static final pnl_fragment_document_pulltorefreshview:I = 0x7f0d0683

.field public static final pnl_gallery_preview_actionbar:I = 0x7f0d0684

.field public static final pnl_greet_card_capture_shutter_layout_refactor:I = 0x7f0d0685

.field public static final pnl_greet_card_menu_layout:I = 0x7f0d0686

.field public static final pnl_greet_card_preview_layout:I = 0x7f0d0687

.field public static final pnl_guide_pop:I = 0x7f0d0688

.field public static final pnl_guide_pop_dialog:I = 0x7f0d0689

.field public static final pnl_image_preview_test:I = 0x7f0d068a

.field public static final pnl_image_restore_capture_shutter:I = 0x7f0d068b

.field public static final pnl_image_restore_capture_shutter_refactor:I = 0x7f0d068c

.field public static final pnl_image_restore_guide_layout:I = 0x7f0d068d

.field public static final pnl_item_certificate:I = 0x7f0d068e

.field public static final pnl_item_image_thumb:I = 0x7f0d068f

.field public static final pnl_ko_registration_agreement:I = 0x7f0d0690

.field public static final pnl_layout_item:I = 0x7f0d0691

.field public static final pnl_layout_loading_item:I = 0x7f0d0692

.field public static final pnl_login_tips:I = 0x7f0d0693

.field public static final pnl_main_empty_doc:I = 0x7f0d0694

.field public static final pnl_main_empty_idcard_doc:I = 0x7f0d0695

.field public static final pnl_main_no_doc_match:I = 0x7f0d0696

.field public static final pnl_mobile_sync_checkbox:I = 0x7f0d0697

.field public static final pnl_more_top_item:I = 0x7f0d0698

.field public static final pnl_multi_capture_middle_layout:I = 0x7f0d0699

.field public static final pnl_multi_capture_shutter_layout:I = 0x7f0d069a

.field public static final pnl_multi_capture_shutter_layout_refactor:I = 0x7f0d069b

.field public static final pnl_multi_edit_filter:I = 0x7f0d069c

.field public static final pnl_multi_image_edit_bottom:I = 0x7f0d069d

.field public static final pnl_multi_image_edit_bottom_book_mode:I = 0x7f0d069e

.field public static final pnl_multi_image_edit_bottom_fun_recommend:I = 0x7f0d069f

.field public static final pnl_multi_trim_pop_tips:I = 0x7f0d06a0

.field public static final pnl_namecard_doc_banner:I = 0x7f0d06a1

.field public static final pnl_new_certificate_photo_menu_layout:I = 0x7f0d06a2

.field public static final pnl_new_certificate_photo_menu_layout_new:I = 0x7f0d06a3

.field public static final pnl_normal_capture_preview_layout:I = 0x7f0d06a4

.field public static final pnl_ocr_capture_new_middle_layout:I = 0x7f0d06a5

.field public static final pnl_ocr_capture_new_preview_layout:I = 0x7f0d06a6

.field public static final pnl_ocr_capture_shutter_refactor:I = 0x7f0d06a7

.field public static final pnl_ocr_edge_scan:I = 0x7f0d06a8

.field public static final pnl_ocr_multi_image_edit_bottom:I = 0x7f0d06a9

.field public static final pnl_orientation_guide:I = 0x7f0d06aa

.field public static final pnl_page_detail_image:I = 0x7f0d06ab

.field public static final pnl_page_detail_image_bottom:I = 0x7f0d06ac

.field public static final pnl_page_detail_text_bottom:I = 0x7f0d06ad

.field public static final pnl_page_detail_word_bottom:I = 0x7f0d06ae

.field public static final pnl_page_item:I = 0x7f0d06af

.field public static final pnl_ppt_auto_scale_tips:I = 0x7f0d06b0

.field public static final pnl_ppt_capture_shutter_refactor:I = 0x7f0d06b1

.field public static final pnl_ppt_middle_layout:I = 0x7f0d06b2

.field public static final pnl_ppt_preview_layout:I = 0x7f0d06b3

.field public static final pnl_premium_image_layout:I = 0x7f0d06b4

.field public static final pnl_preview_book_mode_optimizing:I = 0x7f0d06b5

.field public static final pnl_preview_booksplitter_portrait:I = 0x7f0d06b6

.field public static final pnl_print_preview_bottom:I = 0x7f0d06b7

.field public static final pnl_printer_search_actionbar:I = 0x7f0d06b8

.field public static final pnl_printview_actionbar:I = 0x7f0d06b9

.field public static final pnl_proxy_capture_shutter_layout_refactor:I = 0x7f0d06ba

.field public static final pnl_qr_bar_code_capture_shutter:I = 0x7f0d06bb

.field public static final pnl_qrcode_capture_shutter:I = 0x7f0d06bc

.field public static final pnl_qrcode_middle_layout:I = 0x7f0d06bd

.field public static final pnl_question_commit:I = 0x7f0d06be

.field public static final pnl_question_menu:I = 0x7f0d06bf

.field public static final pnl_question_option:I = 0x7f0d06c0

.field public static final pnl_qustion_tag:I = 0x7f0d06c1

.field public static final pnl_radio_capture_multi_process_image_test:I = 0x7f0d06c2

.field public static final pnl_radio_ip_test:I = 0x7f0d06c3

.field public static final pnl_radio_multi_process_image_test:I = 0x7f0d06c4

.field public static final pnl_reappear:I = 0x7f0d06c5

.field public static final pnl_saveprogressbar:I = 0x7f0d06c6

.field public static final pnl_share_hor_list_new:I = 0x7f0d06c7

.field public static final pnl_share_image_preview:I = 0x7f0d06c8

.field public static final pnl_share_image_preview_title:I = 0x7f0d06c9

.field public static final pnl_share_link_setting_preview:I = 0x7f0d06ca

.field public static final pnl_share_tab_layout:I = 0x7f0d06cb

.field public static final pnl_share_title_preview:I = 0x7f0d06cc

.field public static final pnl_shortcut:I = 0x7f0d06cd

.field public static final pnl_shutter_pop_tips:I = 0x7f0d06ce

.field public static final pnl_signature_tips:I = 0x7f0d06cf

.field public static final pnl_singature_capture_preview_layout:I = 0x7f0d06d0

.field public static final pnl_singature_capture_shutter_layout_refactor:I = 0x7f0d06d1

.field public static final pnl_single_capture_middle_layout:I = 0x7f0d06d2

.field public static final pnl_single_capture_shutter_layout_refactor:I = 0x7f0d06d3

.field public static final pnl_smart_erase_capture_shutter_refactor:I = 0x7f0d06d4

.field public static final pnl_smart_erase_guide_layout:I = 0x7f0d06d5

.field public static final pnl_sync_progress:I = 0x7f0d06d6

.field public static final pnl_topic_capture_preview_refactor_layout:I = 0x7f0d06d7

.field public static final pnl_topic_capture_shutter_layout_refactor:I = 0x7f0d06d8

.field public static final pnl_topic_paper_edit_bottom:I = 0x7f0d06d9

.field public static final pnl_translate_capture_preview_layout:I = 0x7f0d06da

.field public static final pnl_translate_capture_shutter_refactor:I = 0x7f0d06db

.field public static final pnl_translate_new_capture_shutter_layout_refactor:I = 0x7f0d06dc

.field public static final pnl_user_guide_new:I = 0x7f0d06dd

.field public static final pnl_user_guide_start_demo:I = 0x7f0d06de

.field public static final pnl_word_capture_new_middle_layout:I = 0x7f0d06df

.field public static final pnl_word_capture_new_shutter_refactor:I = 0x7f0d06e0

.field public static final pop_arrow_share_doc_tips:I = 0x7f0d06e1

.field public static final pop_change_icon:I = 0x7f0d06e2

.field public static final pop_esign_prepare_pop:I = 0x7f0d06e3

.field public static final pop_feedback_layout:I = 0x7f0d06e4

.field public static final pop_gp_guide_tip_on_second:I = 0x7f0d06e5

.field public static final pop_share_success:I = 0x7f0d06e6

.field public static final pop_sign_tool_config:I = 0x7f0d06e7

.field public static final popup_arrow_tips:I = 0x7f0d06e8

.field public static final popup_card_item_long_touch_guide:I = 0x7f0d06e9

.field public static final popup_docpage_top_tags:I = 0x7f0d06ea

.field public static final popup_list_menu_item:I = 0x7f0d06eb

.field public static final popup_list_menu_listview:I = 0x7f0d06ec

.field public static final popup_pagelist_tags_tip:I = 0x7f0d06ed

.field public static final popup_signtype_tips:I = 0x7f0d06ee

.field public static final popup_window_login_guide:I = 0x7f0d06ef

.field public static final pre_pdf_to_office_tips:I = 0x7f0d06f0

.field public static final pre_pdf_to_word_tips:I = 0x7f0d06f1

.field public static final preference_based_content:I = 0x7f0d06f2

.field public static final preference_category_blank_layout:I = 0x7f0d06f3

.field public static final preference_category_layout:I = 0x7f0d06f4

.field public static final preference_checkbox_item:I = 0x7f0d06f5

.field public static final preference_expend_text_layout:I = 0x7f0d06f6

.field public static final preference_normal_layout:I = 0x7f0d06f7

.field public static final preference_normal_vip_layout:I = 0x7f0d06f8

.field public static final preference_switch_layout:I = 0x7f0d06f9

.field public static final preference_switch_vip_layout:I = 0x7f0d06fa

.field public static final preference_sync_now_layout:I = 0x7f0d06fb

.field public static final preference_text_view_layout:I = 0x7f0d06fc

.field public static final printer_connect_state:I = 0x7f0d06fd

.field public static final printer_empty_view:I = 0x7f0d06fe

.field public static final progress_dialog_pdf_kit_word_for_share:I = 0x7f0d06ff

.field public static final protocol_korea:I = 0x7f0d0700

.field public static final provider_cloud_disk_file:I = 0x7f0d0701

.field public static final provider_cloud_disk_folder:I = 0x7f0d0702

.field public static final provider_doc_importing_grid_mode:I = 0x7f0d0703

.field public static final provider_doc_importing_grid_mode_upgrade:I = 0x7f0d0704

.field public static final provider_doc_importing_large_pic_mode:I = 0x7f0d0705

.field public static final provider_doc_importing_list_mode:I = 0x7f0d0706

.field public static final provider_doc_importing_list_mode_upgrade:I = 0x7f0d0707

.field public static final provider_document_more_func:I = 0x7f0d0708

.field public static final provider_document_more_thumb:I = 0x7f0d0709

.field public static final provider_invoice_list:I = 0x7f0d070a

.field public static final provider_main_doc_rec:I = 0x7f0d070b

.field public static final ps_detected_fragment_share:I = 0x7f0d070c

.field public static final pull_refresh_collaborate_head:I = 0x7f0d070d

.field public static final purchase_forever_bottom_layout:I = 0x7f0d070e

.field public static final purchase_full_screen_bottom_layout:I = 0x7f0d070f

.field public static final purchase_full_screen_top_layout:I = 0x7f0d0710

.field public static final purchase_loading_layout:I = 0x7f0d0711

.field public static final qr_code_result_new:I = 0x7f0d0712

.field public static final qr_code_result_old:I = 0x7f0d0713

.field public static final recyclerview_item_paper_inch:I = 0x7f0d0714

.field public static final refresh_header:I = 0x7f0d0715

.field public static final rename_dialog:I = 0x7f0d0716

.field public static final rename_doc_title_dialog:I = 0x7f0d0717

.field public static final router_welcome_main:I = 0x7f0d0718

.field public static final scandone_mult_elements:I = 0x7f0d0719

.field public static final scene_email_pwd_login:I = 0x7f0d071a

.field public static final scene_forget_pwd:I = 0x7f0d071b

.field public static final scene_gp_login:I = 0x7f0d071c

.field public static final scene_mobile_number_input:I = 0x7f0d071d

.field public static final scene_mobile_pwd_login:I = 0x7f0d071e

.field public static final scene_register:I = 0x7f0d071f

.field public static final scene_reset_pwd:I = 0x7f0d0720

.field public static final scene_verify_code_input:I = 0x7f0d0721

.field public static final security_password_layout:I = 0x7f0d0722

.field public static final select_dialog_item_material:I = 0x7f0d0723

.field public static final select_dialog_multichoice_material:I = 0x7f0d0724

.field public static final select_dialog_singlechoice:I = 0x7f0d0725

.field public static final select_dialog_singlechoice_material:I = 0x7f0d0726

.field public static final select_doc_page:I = 0x7f0d0727

.field public static final send_fax_detail:I = 0x7f0d0728

.field public static final send_pages_list:I = 0x7f0d0729

.field public static final send_pages_list_item:I = 0x7f0d072a

.field public static final send_upload_detail:I = 0x7f0d072b

.field public static final set_mail_to_me:I = 0x7f0d072c

.field public static final setting_dialog_item:I = 0x7f0d072d

.field public static final share_exit_layout:I = 0x7f0d072e

.field public static final share_exit_only_image_layout:I = 0x7f0d072f

.field public static final share_panel_tab_view:I = 0x7f0d0730

.field public static final share_preference_fragment:I = 0x7f0d0731

.field public static final share_result_list_item:I = 0x7f0d0732

.field public static final share_type_item:I = 0x7f0d0733

.field public static final share_type_select_dialog:I = 0x7f0d0734

.field public static final sharelink_setting:I = 0x7f0d0735

.field public static final signature_actionbar_right:I = 0x7f0d0736

.field public static final simple_dropdown_item_1line:I = 0x7f0d0737

.field public static final simple_list_vip_item:I = 0x7f0d0738

.field public static final single_rv_container:I = 0x7f0d0739

.field public static final smart_erase_dialog_tips:I = 0x7f0d073a

.field public static final smart_erase_fragment_share:I = 0x7f0d073b

.field public static final smart_erase_view_operate:I = 0x7f0d073c

.field public static final stub_certificate_recapture_last_page:I = 0x7f0d073d

.field public static final stub_e_evidence_gps_top_container_vivo_market:I = 0x7f0d073e

.field public static final subfolder_operation_grid_item:I = 0x7f0d073f

.field public static final subfolder_operation_list_item:I = 0x7f0d0740

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0d0741

.field public static final sync_lottie_refresh_header_view:I = 0x7f0d0742

.field public static final sync_progress:I = 0x7f0d0743

.field public static final sync_refresh_header:I = 0x7f0d0744

.field public static final sync_refresh_header2:I = 0x7f0d0745

.field public static final sync_to_privatefolder_guide:I = 0x7f0d0746

.field public static final tag_add:I = 0x7f0d0747

.field public static final tag_list_item:I = 0x7f0d0748

.field public static final team_doc_grid_item:I = 0x7f0d0749

.field public static final team_doc_list_item:I = 0x7f0d074a

.field public static final team_folder_grid_item:I = 0x7f0d074b

.field public static final team_folder_list_item:I = 0x7f0d074c

.field public static final team_hint:I = 0x7f0d074d

.field public static final team_image_pageview:I = 0x7f0d074e

.field public static final team_introduce_guide:I = 0x7f0d074f

.field public static final team_layout:I = 0x7f0d0750

.field public static final team_member_title:I = 0x7f0d0751

.field public static final test_url_title_dialog:I = 0x7f0d0752

.field public static final text_floating_bar:I = 0x7f0d0753

.field public static final timing_pop_up_window:I = 0x7f0d0754

.field public static final tips_doc_pagelist_manualsort:I = 0x7f0d0755

.field public static final tool_page_v2_custom_tab_item:I = 0x7f0d0756

.field public static final toolbar:I = 0x7f0d0757

.field public static final toolbar_activity_common:I = 0x7f0d0758

.field public static final toolbar_overlay_layout:I = 0x7f0d0759

.field public static final toolbar_translate_lan:I = 0x7f0d075a

.field public static final topic_scan_confirm_bar:I = 0x7f0d075b

.field public static final transferring_pdf_to_office:I = 0x7f0d075c

.field public static final ud_upgrade:I = 0x7f0d075d

.field public static final uploadprixtfax_main:I = 0x7f0d075e

.field public static final uploadprixtfax_main_10:I = 0x7f0d075f

.field public static final user_tips_ocr_result:I = 0x7f0d0760

.field public static final util_dlg_share:I = 0x7f0d0761

.field public static final util_item_squared_share:I = 0x7f0d0762

.field public static final util_view_circle_image_text_button:I = 0x7f0d0763

.field public static final util_view_image_text_button:I = 0x7f0d0764

.field public static final view_add_tag:I = 0x7f0d0765

.field public static final view_backup_status:I = 0x7f0d0766

.field public static final view_dialog_select_item:I = 0x7f0d0767

.field public static final view_folder_search_tip:I = 0x7f0d0768

.field public static final view_item_print_guid:I = 0x7f0d0769

.field public static final view_item_share_workflow:I = 0x7f0d076a

.field public static final view_item_type_select:I = 0x7f0d076b

.field public static final view_loading:I = 0x7f0d076c

.field public static final view_nine_photo:I = 0x7f0d076d

.field public static final view_pager_item:I = 0x7f0d076e

.field public static final view_pager_item_guide_cn:I = 0x7f0d076f

.field public static final view_pager_item_negative_premium:I = 0x7f0d0770

.field public static final view_pager_item_negative_premium_new:I = 0x7f0d0771

.field public static final view_pager_item_normal:I = 0x7f0d0772

.field public static final view_pager_item_one_title:I = 0x7f0d0773

.field public static final view_pager_item_one_title_new:I = 0x7f0d0774

.field public static final view_pager_item_one_title_new_beidong:I = 0x7f0d0775

.field public static final view_pager_item_program:I = 0x7f0d0776

.field public static final view_pdf_enhance_fail:I = 0x7f0d0777

.field public static final view_pdf_on_enhance:I = 0x7f0d0778

.field public static final view_purchase_item:I = 0x7f0d0779

.field public static final view_purchase_item_new:I = 0x7f0d077a

.field public static final view_setting_link:I = 0x7f0d077b

.field public static final view_setting_pdf:I = 0x7f0d077c

.field public static final view_share_link_doc_state:I = 0x7f0d077d

.field public static final view_share_setting_item:I = 0x7f0d077e

.field public static final view_share_type_select:I = 0x7f0d077f

.field public static final view_signature_edit:I = 0x7f0d0780

.field public static final view_smart_erase_bottom_menu:I = 0x7f0d0781

.field public static final view_smart_erase_remove_watermark:I = 0x7f0d0782

.field public static final view_stub_setting_more_view:I = 0x7f0d0783

.field public static final view_to_return_operation_item:I = 0x7f0d0784

.field public static final view_toolbar_dark_layout:I = 0x7f0d0785

.field public static final view_toolbar_layout:I = 0x7f0d0786

.field public static final view_unsubscribe_recall_operation_item:I = 0x7f0d0787

.field public static final view_unsubscribe_recall_operation_item_new:I = 0x7f0d0788

.field public static final view_upgrade_dot:I = 0x7f0d0789

.field public static final view_verify_code:I = 0x7f0d078a

.field public static final view_verify_code_rect_style:I = 0x7f0d078b

.field public static final view_word_content_op:I = 0x7f0d078c

.field public static final view_word_header:I = 0x7f0d078d

.field public static final view_workflow_email_edit:I = 0x7f0d078e

.field public static final vk_webview_auth_dialog:I = 0x7f0d078f

.field public static final vlayout_item_share_link_grid_item:I = 0x7f0d0790

.field public static final vlayout_item_share_link_grid_item_new:I = 0x7f0d0791

.field public static final vlayout_item_share_link_hor_list_item:I = 0x7f0d0792

.field public static final vlayout_item_share_link_hor_list_item_new:I = 0x7f0d0793

.field public static final vlayout_item_share_link_list_item:I = 0x7f0d0794

.field public static final vlayout_item_share_link_list_new_item:I = 0x7f0d0795

.field public static final vlayout_item_share_link_pdf_card_item:I = 0x7f0d0796

.field public static final vlayout_item_share_link_pdf_channel_item:I = 0x7f0d0797

.field public static final vlayout_item_share_link_title:I = 0x7f0d0798

.field public static final vlayout_item_share_link_title_new:I = 0x7f0d0799

.field public static final vlayout_item_share_link_watermark_new:I = 0x7f0d079a

.field public static final vlayout_item_share_link_watermark_style_three:I = 0x7f0d079b

.field public static final vlayout_item_share_link_watermark_style_two:I = 0x7f0d079c

.field public static final vs_indicator_golden:I = 0x7f0d079d

.field public static final vs_indicator_red:I = 0x7f0d079e

.field public static final vs_layout_gpt_entrance:I = 0x7f0d079f

.field public static final vs_me_page_skins:I = 0x7f0d07a0

.field public static final vs_me_page_waterfall:I = 0x7f0d07a1

.field public static final water_color_item:I = 0x7f0d07a2

.field public static final water_mark_layout:I = 0x7f0d07a3

.field public static final web_ac_web_view:I = 0x7f0d07a4

.field public static final web_frag_web_view:I = 0x7f0d07a5

.field public static final web_menu_url_source:I = 0x7f0d07a6

.field public static final web_pnl_menu_btn:I = 0x7f0d07a7

.field public static final web_pnl_video_loading:I = 0x7f0d07a8

.field public static final web_progress_bar:I = 0x7f0d07a9

.field public static final widget_recent_doc:I = 0x7f0d07aa

.field public static final widget_recent_doc_empty:I = 0x7f0d07ab

.field public static final widget_recent_doc_list_item:I = 0x7f0d07ac

.field public static final widget_recent_doc_list_item_more:I = 0x7f0d07ad

.field public static final word_json_view_content:I = 0x7f0d07ae

.field public static final word_list_guide_pop:I = 0x7f0d07af

.field public static final x_edittext:I = 0x7f0d07b0

.field public static final yubikit_yubikey_prompt_content:I = 0x7f0d07b1


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

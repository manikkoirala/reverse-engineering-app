.class Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$InnerClickListener;
.super Ljava/lang/Object;
.source "CertificateAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InnerClickListener"
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$InnerClickListener;->o0:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;LO〇0〇o808〇/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$InnerClickListener;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    instance-of v0, p1, Ljava/lang/Integer;

    .line 6
    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$InnerClickListener;->o0:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;

    .line 10
    .line 11
    iget-object v0, v0, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;->O8o08O8O:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$OnItemClickListener;

    .line 12
    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    check-cast p1, Ljava/lang/Integer;

    .line 16
    .line 17
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$InnerClickListener;->o0:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;

    .line 22
    .line 23
    invoke-static {v0}, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;->〇O〇(Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;)Ljava/util/List;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    check-cast v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 32
    .line 33
    iget v0, v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 34
    .line 35
    const/16 v1, 0x3f3

    .line 36
    .line 37
    if-eq v0, v1, :cond_0

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$InnerClickListener;->o0:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;

    .line 40
    .line 41
    invoke-static {v0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;->〇O00(Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;I)V

    .line 42
    .line 43
    .line 44
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$InnerClickListener;->o0:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;

    .line 45
    .line 46
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$InnerClickListener;->o0:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;

    .line 50
    .line 51
    iget-object v1, v0, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;->O8o08O8O:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$OnItemClickListener;

    .line 52
    .line 53
    invoke-static {v0}, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;->〇O〇(Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;)Ljava/util/List;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    check-cast p1, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 62
    .line 63
    invoke-interface {v1, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$OnItemClickListener;->〇080(Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;)V

    .line 64
    .line 65
    .line 66
    :cond_1
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.class public final Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;
.super Lcom/intsig/camscanner/capture/core/BaseCaptureScene;
.source "CertificateCaptureScene.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇O8〇8000:Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O0〇0:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O8o〇O0:Landroid/view/View;

.field private O8〇o〇88:Landroid/widget/ImageView;

.field private final OO〇OOo:Lcom/intsig/view/RotateLayout;

.field private Oo0O0o8:Landroid/view/View;

.field private Ooo8o:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;

.field private O〇O:Landroid/widget/TextView;

.field private o00〇88〇08:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

.field private o8O:Landroid/view/View;

.field private oOO0880O:Landroid/widget/FrameLayout;

.field private oOO8:Landroid/widget/ImageView;

.field private final oOoO8OO〇:Z

.field private oOoo80oO:Landroid/view/View;

.field private oOo〇08〇:Landroid/view/View;

.field private oO〇oo:Landroid/widget/TextView;

.field private oooO888:Lcom/airbnb/lottie/LottieAnimationView;

.field private o〇0〇o:I

.field private final o〇O8OO:Lcom/intsig/camscanner/capture/common/CapturePreviewScaleAnimationClient;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇088O:Ljava/util/concurrent/ExecutorService;

.field private 〇0oO〇oo00:Lcom/intsig/app/BaseProgressDialog;

.field private 〇0ooOOo:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

.field private 〇0〇0:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

.field private 〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

.field private 〇80O8o8O〇:Landroid/view/View;

.field private 〇8〇80o:Z

.field private 〇8〇OOoooo:I

.field private 〇8〇o88:Landroid/widget/TextView;

.field private 〇O8oOo0:Landroid/view/View;

.field private 〇o08:Z

.field private 〇oo〇O〇80:Lcom/intsig/view/RotateTextView;

.field private 〇o〇88〇8:Z

.field private 〇〇O80〇0o:Lcom/bumptech/glide/request/RequestOptions;

.field private 〇〇o0〇8:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇〇0:Landroid/app/Dialog;

.field private 〇〇〇00:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O8〇8000:Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V
    .locals 7
    .param p1    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/capture/control/ICaptureControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "captureControl"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "iCaptureViewGroup"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "cameraClient"

    .line 17
    .line 18
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    sget-object v3, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 22
    .line 23
    move-object v1, p0

    .line 24
    move-object v2, p1

    .line 25
    move-object v4, p2

    .line 26
    move-object v5, p3

    .line 27
    move-object v6, p4

    .line 28
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 29
    .line 30
    .line 31
    new-instance p3, Ljava/util/ArrayList;

    .line 32
    .line 33
    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 34
    .line 35
    .line 36
    iput-object p3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 37
    .line 38
    new-instance p3, Ljava/util/ArrayList;

    .line 39
    .line 40
    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .line 42
    .line 43
    iput-object p3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 44
    .line 45
    new-instance p3, Landroid/util/ArrayMap;

    .line 46
    .line 47
    invoke-direct {p3}, Landroid/util/ArrayMap;-><init>()V

    .line 48
    .line 49
    .line 50
    iput-object p3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O0〇0:Landroid/util/ArrayMap;

    .line 51
    .line 52
    new-instance p3, Lcom/intsig/camscanner/capture/common/CapturePreviewScaleAnimationClient;

    .line 53
    .line 54
    invoke-direct {p3, p1}, Lcom/intsig/camscanner/capture/common/CapturePreviewScaleAnimationClient;-><init>(Landroid/app/Activity;)V

    .line 55
    .line 56
    .line 57
    iput-object p3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇O8OO:Lcom/intsig/camscanner/capture/common/CapturePreviewScaleAnimationClient;

    .line 58
    .line 59
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇08〇0oo0()Z

    .line 60
    .line 61
    .line 62
    move-result p4

    .line 63
    iput-boolean p4, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoO8OO〇:Z

    .line 64
    .line 65
    const-string p4, "CertificateCaptureScene"

    .line 66
    .line 67
    invoke-virtual {p0, p4}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇〇0〇88(Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    const/4 p4, 0x0

    .line 71
    invoke-virtual {p0, p4}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oO8o(Z)V

    .line 72
    .line 73
    .line 74
    new-instance p4, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$1;

    .line 75
    .line 76
    invoke-direct {p4, p0, p2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$1;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Lcom/intsig/camscanner/capture/control/ICaptureControl;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p3, p4}, Lcom/intsig/camscanner/capture/common/CapturePreviewScaleAnimationClient;->〇〇8O0〇8(Lcom/intsig/camscanner/capture/common/CapturePreviewScaleAnimationClient$PreviewScaleCallback;)V

    .line 80
    .line 81
    .line 82
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O〇08oOOO0(Landroidx/fragment/app/FragmentActivity;)V

    .line 83
    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method public static synthetic O08O0〇O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oo8ooo8O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final O0O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Z[Ljava/lang/String;Lcom/intsig/camscanner/capture/certificates/ListProgressListener;[IZ)V
    .locals 36

    .line 1
    move-object/from16 v8, p0

    .line 2
    .line 3
    move-object/from16 v0, p3

    .line 4
    .line 5
    move/from16 v7, p6

    .line 6
    .line 7
    iget-object v1, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 8
    .line 9
    const-string v6, "CertificateCaptureScene"

    .line 10
    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    const-string v0, "mCertificateCapture == null"

    .line 14
    .line 15
    invoke-static {v6, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    iget-boolean v1, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇o〇88〇8:Z

    .line 20
    .line 21
    new-instance v2, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    const-string v3, "handleCertificateCapture hasClickIdCardGuide: "

    .line 27
    .line 28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v1, ", needDeblur="

    .line 35
    .line 36
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-static {v6, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    const/4 v5, 0x0

    .line 50
    const/4 v4, 0x1

    .line 51
    if-eqz p4, :cond_1

    .line 52
    .line 53
    const/16 v27, 0x1

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    const/16 v27, 0x0

    .line 57
    .line 58
    :goto_0
    new-instance v1, LO〇0〇o808〇/〇O00;

    .line 59
    .line 60
    move/from16 v2, p2

    .line 61
    .line 62
    invoke-direct {v1, v8, v2}, LO〇0〇o808〇/〇O00;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Z)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v8, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8o8O〇O(Ljava/lang/Runnable;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 73
    .line 74
    .line 75
    move-result-wide v1

    .line 76
    const-wide/16 v14, -0x1

    .line 77
    .line 78
    const-wide/16 v28, 0x0

    .line 79
    .line 80
    cmp-long v3, v1, v28

    .line 81
    .line 82
    if-lez v3, :cond_3

    .line 83
    .line 84
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 93
    .line 94
    .line 95
    move-result-wide v2

    .line 96
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    if-eqz v1, :cond_2

    .line 101
    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 103
    .line 104
    .line 105
    move-result-object v1

    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 107
    .line 108
    .line 109
    move-result-object v2

    .line 110
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 111
    .line 112
    .line 113
    move-result-wide v2

    .line 114
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 115
    .line 116
    .line 117
    move-result v1

    .line 118
    add-int/2addr v1, v4

    .line 119
    goto :goto_1

    .line 120
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    invoke-interface {v1, v14, v15}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇o〇(J)V

    .line 125
    .line 126
    .line 127
    :cond_3
    const/4 v1, -0x1

    .line 128
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 129
    .line 130
    .line 131
    move-result-object v2

    .line 132
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 133
    .line 134
    .line 135
    move-result-wide v2

    .line 136
    cmp-long v9, v2, v28

    .line 137
    .line 138
    if-gez v9, :cond_7

    .line 139
    .line 140
    iget-object v2, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 141
    .line 142
    const-string v3, ""

    .line 143
    .line 144
    if-eqz v2, :cond_4

    .line 145
    .line 146
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 147
    .line 148
    .line 149
    move-result-object v9

    .line 150
    invoke-virtual {v2, v9}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->O8(Landroid/content/Context;)Ljava/lang/String;

    .line 151
    .line 152
    .line 153
    move-result-object v2

    .line 154
    if-nez v2, :cond_5

    .line 155
    .line 156
    :cond_4
    move-object v2, v3

    .line 157
    :cond_5
    invoke-static {v2, v3}, Lcom/intsig/camscanner/util/Util;->O8〇o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 158
    .line 159
    .line 160
    move-result-object v2

    .line 161
    const-string v3, "getDefaultModelName(\n   \u2026  ?: \"\", \"\"\n            )"

    .line 162
    .line 163
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 167
    .line 168
    .line 169
    move-result-object v3

    .line 170
    invoke-static {v3, v2, v4}, Lcom/intsig/camscanner/util/Util;->OOO(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object v2

    .line 174
    const-string v3, "getPreferName(applicatio\u2026Util.BRACKET_SUFFIXSTYLE)"

    .line 175
    .line 176
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    .line 178
    .line 179
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 180
    .line 181
    .line 182
    move-result-object v3

    .line 183
    invoke-interface {v3, v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇80(Ljava/lang/String;)V

    .line 184
    .line 185
    .line 186
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 187
    .line 188
    .line 189
    move-result-object v3

    .line 190
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇080()Landroid/net/Uri;

    .line 191
    .line 192
    .line 193
    move-result-object v3

    .line 194
    if-nez v3, :cond_6

    .line 195
    .line 196
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 197
    .line 198
    .line 199
    move-result-object v3

    .line 200
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 201
    .line 202
    .line 203
    move-result-wide v9

    .line 204
    new-instance v3, Ljava/lang/StringBuilder;

    .line 205
    .line 206
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 207
    .line 208
    .line 209
    const-string v11, "handleCertificateCapture title: "

    .line 210
    .line 211
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    .line 216
    .line 217
    const-string v2, " | mDocId: "

    .line 218
    .line 219
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    invoke-virtual {v3, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 223
    .line 224
    .line 225
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 226
    .line 227
    .line 228
    move-result-object v2

    .line 229
    invoke-static {v6, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    .line 231
    .line 232
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 233
    .line 234
    .line 235
    move-result-object v2

    .line 236
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OoO8()V

    .line 237
    .line 238
    .line 239
    goto :goto_2

    .line 240
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 241
    .line 242
    .line 243
    move-result-object v1

    .line 244
    invoke-static {v3}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 245
    .line 246
    .line 247
    move-result-wide v2

    .line 248
    invoke-interface {v1, v2, v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇o〇(J)V

    .line 249
    .line 250
    .line 251
    const/16 v30, 0x1

    .line 252
    .line 253
    goto :goto_3

    .line 254
    :cond_7
    :goto_2
    move/from16 v30, v1

    .line 255
    .line 256
    :goto_3
    array-length v3, v0

    .line 257
    new-array v2, v3, [Ljava/lang/String;

    .line 258
    .line 259
    new-array v1, v3, [J

    .line 260
    .line 261
    const/4 v12, 0x0

    .line 262
    :goto_4
    if-ge v12, v3, :cond_c

    .line 263
    .line 264
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 265
    .line 266
    .line 267
    move-result-object v13

    .line 268
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    .line 269
    .line 270
    .line 271
    move-result-object v9

    .line 272
    new-instance v10, Ljava/lang/StringBuilder;

    .line 273
    .line 274
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 275
    .line 276
    .line 277
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    .line 279
    .line 280
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    .line 282
    .line 283
    const-string v9, ".jpg"

    .line 284
    .line 285
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    .line 287
    .line 288
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 289
    .line 290
    .line 291
    move-result-object v10

    .line 292
    aget-object v9, v0, v12

    .line 293
    .line 294
    invoke-static {v9, v10}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 295
    .line 296
    .line 297
    move-result v9

    .line 298
    if-nez v9, :cond_8

    .line 299
    .line 300
    move-object v13, v2

    .line 301
    move-object v10, v6

    .line 302
    move/from16 v35, v12

    .line 303
    .line 304
    move-wide/from16 v33, v14

    .line 305
    .line 306
    const/16 p2, 0x1

    .line 307
    .line 308
    const/4 v9, 0x0

    .line 309
    move-object v14, v1

    .line 310
    move v15, v3

    .line 311
    goto/16 :goto_6

    .line 312
    .line 313
    :cond_8
    sget-object v9, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇080:Lcom/intsig/camscanner/app/DBInsertPageUtil;

    .line 314
    .line 315
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 316
    .line 317
    .line 318
    move-result-object v11

    .line 319
    invoke-interface {v11}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 320
    .line 321
    .line 322
    move-result-wide v16

    .line 323
    move-object/from16 v31, v10

    .line 324
    .line 325
    move-wide/from16 v10, v16

    .line 326
    .line 327
    add-int v16, v30, v12

    .line 328
    .line 329
    move-object/from16 v32, v13

    .line 330
    .line 331
    move/from16 v13, v16

    .line 332
    .line 333
    const/16 v16, 0x1

    .line 334
    .line 335
    move-wide/from16 v33, v14

    .line 336
    .line 337
    move/from16 v14, v16

    .line 338
    .line 339
    const/4 v15, 0x0

    .line 340
    const/16 v16, 0x2

    .line 341
    .line 342
    const/16 v17, 0x0

    .line 343
    .line 344
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 345
    .line 346
    .line 347
    move-result-object v18

    .line 348
    invoke-interface/range {v18 .. v18}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇0000OOO()Z

    .line 349
    .line 350
    .line 351
    move-result v18

    .line 352
    const/16 v19, 0x0

    .line 353
    .line 354
    const/16 v21, 0x1

    .line 355
    .line 356
    const/16 v24, 0x0

    .line 357
    .line 358
    const/16 v25, 0x2000

    .line 359
    .line 360
    const/16 v26, 0x0

    .line 361
    .line 362
    move/from16 v35, v12

    .line 363
    .line 364
    move-object/from16 v12, v32

    .line 365
    .line 366
    move/from16 v20, p6

    .line 367
    .line 368
    move/from16 v22, v3

    .line 369
    .line 370
    move/from16 v23, v35

    .line 371
    .line 372
    invoke-static/range {v9 .. v26}, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇〇8O0〇8(Lcom/intsig/camscanner/app/DBInsertPageUtil;JLjava/lang/String;IZ[IIIZZZZIIIILjava/lang/Object;)Landroid/net/Uri;

    .line 373
    .line 374
    .line 375
    move-result-object v9

    .line 376
    if-eqz v9, :cond_9

    .line 377
    .line 378
    invoke-static {v9}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 379
    .line 380
    .line 381
    move-result-wide v14

    .line 382
    goto :goto_5

    .line 383
    :cond_9
    move-wide/from16 v14, v33

    .line 384
    .line 385
    :goto_5
    cmp-long v9, v14, v28

    .line 386
    .line 387
    if-gtz v9, :cond_a

    .line 388
    .line 389
    move-object v14, v1

    .line 390
    move-object v13, v2

    .line 391
    move-object v10, v6

    .line 392
    const/16 p2, 0x1

    .line 393
    .line 394
    const/4 v9, 0x0

    .line 395
    goto/16 :goto_7

    .line 396
    .line 397
    :cond_a
    iget-object v9, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 398
    .line 399
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 400
    .line 401
    .line 402
    move-result-object v10

    .line 403
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 404
    .line 405
    .line 406
    aput-object v32, v2, v35

    .line 407
    .line 408
    aput-wide v14, v1, v35

    .line 409
    .line 410
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8O〇88oO0()Z

    .line 411
    .line 412
    .line 413
    move-result v9

    .line 414
    if-eqz v9, :cond_b

    .line 415
    .line 416
    const/4 v9, 0x0

    .line 417
    invoke-static/range {v31 .. v31}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 418
    .line 419
    .line 420
    move-result v10

    .line 421
    const/4 v11, 0x0

    .line 422
    const/4 v12, 0x1

    .line 423
    move-object v14, v1

    .line 424
    move-object/from16 v1, p0

    .line 425
    .line 426
    move-object v13, v2

    .line 427
    move-object/from16 v2, v32

    .line 428
    .line 429
    move v15, v3

    .line 430
    move-object/from16 v3, v31

    .line 431
    .line 432
    const/16 p2, 0x1

    .line 433
    .line 434
    move-object v4, v9

    .line 435
    const/4 v9, 0x0

    .line 436
    move v5, v10

    .line 437
    move-object v10, v6

    .line 438
    move v6, v11

    .line 439
    move v7, v12

    .line 440
    invoke-direct/range {v1 .. v7}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O80〇oOo(Ljava/lang/String;Ljava/lang/String;[IIZZ)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 441
    .line 442
    .line 443
    move-result-object v1

    .line 444
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 445
    .line 446
    .line 447
    move-result-object v2

    .line 448
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->getRotation()I

    .line 449
    .line 450
    .line 451
    move-result v2

    .line 452
    iput v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇00O0:I

    .line 453
    .line 454
    invoke-direct {v8, v1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇oo〇O〇80(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 455
    .line 456
    .line 457
    goto :goto_6

    .line 458
    :cond_b
    move-object v14, v1

    .line 459
    move-object v13, v2

    .line 460
    move v15, v3

    .line 461
    move-object v10, v6

    .line 462
    const/16 p2, 0x1

    .line 463
    .line 464
    const/4 v9, 0x0

    .line 465
    :goto_6
    add-int/lit8 v12, v35, 0x1

    .line 466
    .line 467
    move/from16 v7, p6

    .line 468
    .line 469
    move-object v6, v10

    .line 470
    move-object v2, v13

    .line 471
    move-object v1, v14

    .line 472
    move v3, v15

    .line 473
    move-wide/from16 v14, v33

    .line 474
    .line 475
    const/4 v4, 0x1

    .line 476
    const/4 v5, 0x0

    .line 477
    goto/16 :goto_4

    .line 478
    .line 479
    :cond_c
    move-object v14, v1

    .line 480
    move-object v13, v2

    .line 481
    move-object v10, v6

    .line 482
    const/16 p2, 0x1

    .line 483
    .line 484
    const/4 v9, 0x0

    .line 485
    const/4 v5, 0x1

    .line 486
    :goto_7
    if-nez v5, :cond_d

    .line 487
    .line 488
    const-string v0, "parseId fail"

    .line 489
    .line 490
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    .line 492
    .line 493
    return-void

    .line 494
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 495
    .line 496
    .line 497
    move-result-object v1

    .line 498
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 499
    .line 500
    .line 501
    move-result-object v2

    .line 502
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 503
    .line 504
    .line 505
    move-result-wide v2

    .line 506
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->Oo〇O(Landroid/content/Context;J)V

    .line 507
    .line 508
    .line 509
    iget-object v1, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇088O:Ljava/util/concurrent/ExecutorService;

    .line 510
    .line 511
    if-nez v1, :cond_e

    .line 512
    .line 513
    invoke-static/range {p2 .. p2}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    .line 514
    .line 515
    .line 516
    move-result-object v1

    .line 517
    iput-object v1, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇088O:Ljava/util/concurrent/ExecutorService;

    .line 518
    .line 519
    :cond_e
    iget-object v1, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇088O:Ljava/util/concurrent/ExecutorService;

    .line 520
    .line 521
    const/4 v2, 0x0

    .line 522
    if-eqz v1, :cond_11

    .line 523
    .line 524
    new-instance v3, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureCallable;

    .line 525
    .line 526
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 527
    .line 528
    .line 529
    move-result-object v4

    .line 530
    iget-object v11, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 531
    .line 532
    iget-object v5, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O0〇0:Landroid/util/ArrayMap;

    .line 533
    .line 534
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 535
    .line 536
    .line 537
    move-result-object v6

    .line 538
    invoke-interface {v6}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->getRotation()I

    .line 539
    .line 540
    .line 541
    move-result v18

    .line 542
    if-eqz p6, :cond_f

    .line 543
    .line 544
    sget-object v6, Lcom/intsig/camscanner/scanner/deblur/DeBlurUtils;->INSTANCE:Lcom/intsig/camscanner/scanner/deblur/DeBlurUtils;

    .line 545
    .line 546
    const/4 v7, 0x1

    .line 547
    invoke-static {v6, v9, v7, v2}, Lcom/intsig/camscanner/scanner/deblur/DeBlurUtils;->isDeBlurOn$default(Lcom/intsig/camscanner/scanner/deblur/DeBlurUtils;ZILjava/lang/Object;)Z

    .line 548
    .line 549
    .line 550
    move-result v6

    .line 551
    if-eqz v6, :cond_10

    .line 552
    .line 553
    const/16 v19, 0x1

    .line 554
    .line 555
    goto :goto_8

    .line 556
    :cond_f
    const/4 v7, 0x1

    .line 557
    :cond_10
    const/16 v19, 0x0

    .line 558
    .line 559
    :goto_8
    sget-object v6, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->INSTANCE:Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;

    .line 560
    .line 561
    invoke-virtual {v6}, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->isCropDewrapOn()Z

    .line 562
    .line 563
    .line 564
    move-result v20

    .line 565
    const/4 v6, 0x0

    .line 566
    move-object v9, v3

    .line 567
    move-object v15, v10

    .line 568
    move-object v10, v4

    .line 569
    move-object/from16 v12, p3

    .line 570
    .line 571
    move-object v4, v15

    .line 572
    move-object/from16 v15, p4

    .line 573
    .line 574
    move-object/from16 v16, v5

    .line 575
    .line 576
    move-object/from16 v17, p5

    .line 577
    .line 578
    invoke-direct/range {v9 .. v20}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureCallable;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;[Ljava/lang/String;[Ljava/lang/String;[JLcom/intsig/camscanner/capture/certificates/ListProgressListener;Landroid/util/ArrayMap;[IIZZ)V

    .line 579
    .line 580
    .line 581
    invoke-interface {v1, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    .line 582
    .line 583
    .line 584
    move-result-object v0

    .line 585
    goto :goto_9

    .line 586
    :cond_11
    move-object v4, v10

    .line 587
    const/4 v6, 0x0

    .line 588
    const/4 v7, 0x1

    .line 589
    move-object v0, v2

    .line 590
    :goto_9
    if-eqz v0, :cond_12

    .line 591
    .line 592
    :try_start_0
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    .line 593
    .line 594
    .line 595
    move-result-object v0

    .line 596
    move-object v2, v0

    .line 597
    check-cast v2, [J

    .line 598
    .line 599
    goto :goto_a

    .line 600
    :catch_0
    move-exception v0

    .line 601
    goto :goto_d

    .line 602
    :catch_1
    move-exception v0

    .line 603
    goto :goto_e

    .line 604
    :cond_12
    :goto_a
    if-eqz v2, :cond_14

    .line 605
    .line 606
    array-length v0, v2

    .line 607
    if-nez v0, :cond_13

    .line 608
    .line 609
    const/4 v5, 0x1

    .line 610
    goto :goto_b

    .line 611
    :cond_13
    const/4 v5, 0x0

    .line 612
    :goto_b
    xor-int/lit8 v0, v5, 0x1

    .line 613
    .line 614
    if-eqz v0, :cond_14

    .line 615
    .line 616
    array-length v0, v2

    .line 617
    const/4 v5, 0x0

    .line 618
    :goto_c
    if-ge v5, v0, :cond_14

    .line 619
    .line 620
    aget-wide v9, v2, v5

    .line 621
    .line 622
    iget-object v1, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 623
    .line 624
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 625
    .line 626
    .line 627
    move-result-object v3

    .line 628
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 629
    .line 630
    .line 631
    add-int/lit8 v5, v5, 0x1

    .line 632
    .line 633
    goto :goto_c

    .line 634
    :goto_d
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 635
    .line 636
    .line 637
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 638
    .line 639
    .line 640
    move-result-object v0

    .line 641
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 642
    .line 643
    .line 644
    return-void

    .line 645
    :goto_e
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 646
    .line 647
    .line 648
    return-void

    .line 649
    :cond_14
    invoke-virtual {v8, v6}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o88O8(Z)V

    .line 650
    .line 651
    .line 652
    iget-object v0, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 653
    .line 654
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 655
    .line 656
    .line 657
    move-result v0

    .line 658
    iget-object v1, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 659
    .line 660
    if-eqz v1, :cond_15

    .line 661
    .line 662
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇o〇()I

    .line 663
    .line 664
    .line 665
    move-result v5

    .line 666
    goto :goto_f

    .line 667
    :cond_15
    const/4 v5, 0x0

    .line 668
    :goto_f
    if-ge v0, v5, :cond_17

    .line 669
    .line 670
    if-eqz v27, :cond_16

    .line 671
    .line 672
    goto :goto_10

    .line 673
    :cond_16
    iget-object v0, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 674
    .line 675
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 676
    .line 677
    .line 678
    move-result v0

    .line 679
    add-int/2addr v0, v7

    .line 680
    invoke-direct {v8, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo8(I)V

    .line 681
    .line 682
    .line 683
    goto :goto_11

    .line 684
    :cond_17
    :goto_10
    iget-boolean v0, v8, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇o〇88〇8:Z

    .line 685
    .line 686
    move-object/from16 v1, p1

    .line 687
    .line 688
    invoke-direct {v8, v0, v1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇〇0o〇〇0(ZLcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 689
    .line 690
    .line 691
    :goto_11
    return-void
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
.end method

.method public static final synthetic O0O〇OOo(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O8oOo0:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic O0o8〇O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇088O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O0oO0(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇80O8o8O〇(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oO〇oo()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic O0o〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇00O0O〇o(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O0〇0(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->OO〇OOo:Lcom/intsig/view/RotateLayout;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 7
    .line 8
    .line 9
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoo80oO:Landroid/view/View;

    .line 10
    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_1
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    :goto_1
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final O0〇oO〇o()V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇0〇o:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ge v0, v1, :cond_0

    .line 5
    .line 6
    return-void

    .line 7
    :cond_0
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 8
    .line 9
    invoke-virtual {v2}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    const/4 v3, 0x0

    .line 14
    if-ne v0, v2, :cond_1

    .line 15
    .line 16
    :goto_0
    const/4 v2, 0x1

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-ne v0, v2, :cond_2

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_2
    const/4 v2, 0x0

    .line 28
    :goto_1
    if-eqz v2, :cond_5

    .line 29
    .line 30
    sget-object v0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O8〇8000:Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$Companion;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$Companion;->〇080()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_4

    .line 37
    .line 38
    if-eq v0, v1, :cond_3

    .line 39
    .line 40
    const/16 v0, 0x3f2

    .line 41
    .line 42
    goto/16 :goto_5

    .line 43
    .line 44
    :cond_3
    const/16 v0, 0x3f0

    .line 45
    .line 46
    goto/16 :goto_5

    .line 47
    .line 48
    :cond_4
    const/16 v0, 0x3e9

    .line 49
    .line 50
    goto/16 :goto_5

    .line 51
    .line 52
    :cond_5
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FAMILY_REGISTER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 53
    .line 54
    invoke-virtual {v2}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    if-ne v0, v2, :cond_6

    .line 59
    .line 60
    const/16 v0, 0x3eb

    .line 61
    .line 62
    goto/16 :goto_5

    .line 63
    .line 64
    :cond_6
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 65
    .line 66
    invoke-virtual {v2}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 67
    .line 68
    .line 69
    move-result v2

    .line 70
    if-ne v0, v2, :cond_7

    .line 71
    .line 72
    :goto_2
    const/4 v2, 0x1

    .line 73
    goto :goto_3

    .line 74
    :cond_7
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 75
    .line 76
    invoke-virtual {v2}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    if-ne v0, v2, :cond_8

    .line 81
    .line 82
    goto :goto_2

    .line 83
    :cond_8
    const/4 v2, 0x0

    .line 84
    :goto_3
    if-eqz v2, :cond_9

    .line 85
    .line 86
    const/16 v0, 0x3ea

    .line 87
    .line 88
    goto :goto_5

    .line 89
    :cond_9
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DRIVE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 90
    .line 91
    invoke-virtual {v2}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 92
    .line 93
    .line 94
    move-result v2

    .line 95
    if-ne v0, v2, :cond_a

    .line 96
    .line 97
    const/16 v0, 0x3ec

    .line 98
    .line 99
    goto :goto_5

    .line 100
    :cond_a
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->VEHICLE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 101
    .line 102
    invoke-virtual {v2}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 103
    .line 104
    .line 105
    move-result v2

    .line 106
    if-ne v0, v2, :cond_b

    .line 107
    .line 108
    const/16 v0, 0x3ed

    .line 109
    .line 110
    goto :goto_5

    .line 111
    :cond_b
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 112
    .line 113
    invoke-virtual {v2}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 114
    .line 115
    .line 116
    move-result v2

    .line 117
    if-ne v0, v2, :cond_c

    .line 118
    .line 119
    goto :goto_4

    .line 120
    :cond_c
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 121
    .line 122
    invoke-virtual {v2}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 123
    .line 124
    .line 125
    move-result v2

    .line 126
    if-ne v0, v2, :cond_d

    .line 127
    .line 128
    goto :goto_4

    .line 129
    :cond_d
    const/4 v1, 0x0

    .line 130
    :goto_4
    if-eqz v1, :cond_e

    .line 131
    .line 132
    const/16 v0, 0x3f1

    .line 133
    .line 134
    goto :goto_5

    .line 135
    :cond_e
    sget-object v1, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BUSINESS_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 136
    .line 137
    invoke-virtual {v1}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 138
    .line 139
    .line 140
    move-result v1

    .line 141
    if-ne v0, v1, :cond_f

    .line 142
    .line 143
    const/16 v0, 0x3ef

    .line 144
    .line 145
    goto :goto_5

    .line 146
    :cond_f
    sget-object v1, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->HOUSE_PROPERTY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 147
    .line 148
    invoke-virtual {v1}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 149
    .line 150
    .line 151
    move-result v1

    .line 152
    if-ne v0, v1, :cond_10

    .line 153
    .line 154
    const/16 v0, 0x3ee

    .line 155
    .line 156
    goto :goto_5

    .line 157
    :cond_10
    const/16 v0, 0x3f3

    .line 158
    .line 159
    :goto_5
    iput v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇OOoooo:I

    .line 160
    .line 161
    return-void
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final O88O(Landroid/content/Intent;)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    move-object v1, v0

    .line 10
    :goto_0
    const-string v2, "CertificateCaptureScene"

    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    const/4 v4, 0x1

    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    new-array p1, v4, [Landroid/net/Uri;

    .line 17
    .line 18
    aput-object v1, p1, v3

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_1
    invoke-static {p1}, Lcom/intsig/camscanner/app/IntentUtil;->〇O8o08O(Landroid/content/Intent;)Ljava/util/ArrayList;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    if-eqz p1, :cond_2

    .line 26
    .line 27
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-lez v1, :cond_2

    .line 32
    .line 33
    new-array v1, v3, [Landroid/net/Uri;

    .line 34
    .line 35
    invoke-interface {p1, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    check-cast p1, [Landroid/net/Uri;

    .line 40
    .line 41
    goto :goto_1

    .line 42
    :cond_2
    const-string p1, "uris are null"

    .line 43
    .line 44
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    move-object p1, v0

    .line 48
    :goto_1
    if-eqz p1, :cond_3

    .line 49
    .line 50
    new-instance v0, LO〇0〇o808〇/〇8o8o〇;

    .line 51
    .line 52
    invoke-direct {v0, p0, p1}, LO〇0〇o808〇/〇8o8o〇;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;[Landroid/net/Uri;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8o8O〇O(Ljava/lang/Runnable;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    const/4 v1, 0x4

    .line 63
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇o0O0O8(I)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {p0, v4}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o88O8(Z)V

    .line 67
    .line 68
    .line 69
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    new-instance v1, LO〇0〇o808〇/〇O8o08O;

    .line 74
    .line 75
    invoke-direct {v1, p1, p0}, LO〇0〇o808〇/〇O8o08O;-><init>([Landroid/net/Uri;Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 79
    .line 80
    .line 81
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 82
    .line 83
    :cond_3
    if-nez v0, :cond_4

    .line 84
    .line 85
    const-string p1, "uriList is empty"

    .line 86
    .line 87
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    :cond_4
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static synthetic O8OO08o([Landroid/net/Uri;Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o8o([Landroid/net/Uri;Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O8O〇8oo08(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final O8o08O8O(Ljava/lang/String;)[I
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {p1, v0}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    if-nez v1, :cond_0

    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    return-object p1

    .line 10
    :cond_0
    invoke-static {p1}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    rem-int/lit16 p1, p1, 0xb4

    .line 15
    .line 16
    const-string v2, "{\n            ImageUtil.\u2026)\n            )\n        }"

    .line 17
    .line 18
    const/16 v3, 0x8f

    .line 19
    .line 20
    const/16 v4, 0x69

    .line 21
    .line 22
    const/4 v5, 0x0

    .line 23
    if-nez p1, :cond_1

    .line 24
    .line 25
    aget p1, v1, v5

    .line 26
    .line 27
    aget v0, v1, v0

    .line 28
    .line 29
    invoke-static {p1, v0, v3, v4}, Lcom/intsig/utils/ImageUtil;->〇8o8o〇(IIII)Landroid/graphics/Rect;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    aget p1, v1, v5

    .line 38
    .line 39
    aget v0, v1, v0

    .line 40
    .line 41
    invoke-static {p1, v0, v4, v3}, Lcom/intsig/utils/ImageUtil;->〇8o8o〇(IIII)Landroid/graphics/Rect;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    :goto_0
    invoke-static {p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->rectToBorder(Landroid/graphics/Rect;)[I

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    return-object p1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final O8o〇O0(Landroid/view/View;Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 3

    .line 1
    const-string v0, "CertificateCaptureScene"

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    if-nez p2, :cond_0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const-string v1, "playAnimation"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    new-instance v0, Landroid/view/animation/AnimationSet;

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 17
    .line 18
    .line 19
    const-wide/16 v1, 0xc8

    .line 20
    .line 21
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOo0(Landroid/view/View;Landroid/view/View;)Landroid/view/animation/Animation;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->OO〇00〇8oO(Landroid/view/View;Landroid/view/View;)Landroid/view/animation/Animation;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    invoke-virtual {v0, p2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 36
    .line 37
    .line 38
    new-instance p2, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$playAnimation$1;

    .line 39
    .line 40
    invoke-direct {p2, p3}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$playAnimation$1;-><init>(Ljava/lang/Runnable;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 44
    .line 45
    .line 46
    new-instance p2, Landroid/view/animation/LinearInterpolator;

    .line 47
    .line 48
    invoke-direct {p2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, p2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 58
    .line 59
    .line 60
    return-void

    .line 61
    :cond_1
    :goto_0
    const-string p1, "playAnimation textA4 == null || desTips == null"

    .line 62
    .line 63
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private final O8〇o〇88()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x1

    .line 8
    xor-int/2addr v1, v2

    .line 9
    const/4 v3, 0x0

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v0, v3

    .line 14
    :goto_0
    const-string v1, "CertificateCaptureScene"

    .line 15
    .line 16
    if-eqz v0, :cond_5

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 23
    .line 24
    .line 25
    move-result-wide v3

    .line 26
    const-wide/16 v5, 0x0

    .line 27
    .line 28
    const-string v0, "recaptureLastPage, docId="

    .line 29
    .line 30
    cmp-long v7, v3, v5

    .line 31
    .line 32
    if-gez v7, :cond_1

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 39
    .line 40
    .line 41
    move-result-wide v2

    .line 42
    new-instance v4, Ljava/lang/StringBuilder;

    .line 43
    .line 44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    return-void

    .line 61
    :cond_1
    iget-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 62
    .line 63
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 64
    .line 65
    .line 66
    move-result v3

    .line 67
    if-lt v3, v2, :cond_4

    .line 68
    .line 69
    iget-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 70
    .line 71
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    if-ge v3, v2, :cond_2

    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 79
    .line 80
    .line 81
    move-result-object v3

    .line 82
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 83
    .line 84
    .line 85
    move-result-object v4

    .line 86
    invoke-interface {v4}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 87
    .line 88
    .line 89
    move-result-wide v4

    .line 90
    invoke-static {v3, v4, v5}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    if-gez v3, :cond_3

    .line 95
    .line 96
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 97
    .line 98
    .line 99
    move-result-object v2

    .line 100
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 101
    .line 102
    .line 103
    move-result-wide v2

    .line 104
    new-instance v4, Ljava/lang/StringBuilder;

    .line 105
    .line 106
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .line 108
    .line 109
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    return-void

    .line 123
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 124
    .line 125
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 126
    .line 127
    .line 128
    move-result v4

    .line 129
    sub-int/2addr v4, v2

    .line 130
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 131
    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 134
    .line 135
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 136
    .line 137
    .line 138
    move-result v4

    .line 139
    sub-int/2addr v4, v2

    .line 140
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 141
    .line 142
    .line 143
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 144
    .line 145
    .line 146
    move-result-object v0

    .line 147
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 148
    .line 149
    .line 150
    move-result-object v2

    .line 151
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 152
    .line 153
    .line 154
    move-result-wide v4

    .line 155
    const/4 v2, 0x0

    .line 156
    invoke-static {v0, v4, v5, v3, v2}, Lcom/intsig/camscanner/app/DBUtil;->〇0000OOO(Landroid/content/Context;JIZ)V

    .line 157
    .line 158
    .line 159
    new-instance v0, LO〇0〇o808〇/O8;

    .line 160
    .line 161
    invoke-direct {v0, p0}, LO〇0〇o808〇/O8;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 162
    .line 163
    .line 164
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8o8O〇O(Ljava/lang/Runnable;)V

    .line 165
    .line 166
    .line 167
    sget-object v3, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 168
    .line 169
    goto :goto_2

    .line 170
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 171
    .line 172
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 173
    .line 174
    .line 175
    move-result v0

    .line 176
    iget-object v2, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 177
    .line 178
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 179
    .line 180
    .line 181
    move-result v2

    .line 182
    new-instance v3, Ljava/lang/StringBuilder;

    .line 183
    .line 184
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 185
    .line 186
    .line 187
    const-string v4, "recaptureLastPage, mFutureList="

    .line 188
    .line 189
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    const-string v0, ", mCertificatePageId="

    .line 196
    .line 197
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    .line 199
    .line 200
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 201
    .line 202
    .line 203
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 204
    .line 205
    .line 206
    move-result-object v0

    .line 207
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    .line 209
    .line 210
    return-void

    .line 211
    :cond_5
    :goto_2
    if-nez v3, :cond_6

    .line 212
    .line 213
    const-string v0, "try to recaptureLastPage, but mFutureList is empty"

    .line 214
    .line 215
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    .line 217
    .line 218
    :cond_6
    return-void
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
.end method

.method private static final OO(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ILcom/intsig/camscanner/capture/common/CapturePreviewScaleData;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$capturePreviewScaleData"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o8〇OO()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->Oooo8o0〇(I)V

    .line 19
    .line 20
    .line 21
    const/4 p1, 0x1

    .line 22
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇O〇(Z)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇O8OO:Lcom/intsig/camscanner/capture/common/CapturePreviewScaleAnimationClient;

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    invoke-interface {p0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O〇8O8〇008()Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object p0

    .line 35
    invoke-virtual {p1, p0, p2}, Lcom/intsig/camscanner/capture/common/CapturePreviewScaleAnimationClient;->OoO8(Landroid/view/View;Lcom/intsig/camscanner/capture/common/CapturePreviewScaleData;)V

    .line 36
    .line 37
    .line 38
    :cond_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public static final synthetic OO88o(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)Landroid/widget/ImageView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO8:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic OO88〇OOO(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)Landroid/content/Context;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic OO8〇(Ljava/lang/Runnable;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇80o(Ljava/lang/Runnable;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private final OOo()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, LO〇0〇o808〇/OO0o〇〇;

    .line 6
    .line 7
    invoke-direct {v1, p0}, LO〇0〇o808〇/OO0o〇〇;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OO〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0O〇O00O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final OO〇00〇8oO(Landroid/view/View;Landroid/view/View;)Landroid/view/animation/Animation;
    .locals 12

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v1, v0, [I

    .line 3
    .line 4
    new-array v0, v0, [I

    .line 5
    .line 6
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 10
    .line 11
    .line 12
    new-instance v11, Landroid/view/animation/TranslateAnimation;

    .line 13
    .line 14
    const/4 v3, 0x0

    .line 15
    const/4 v4, 0x0

    .line 16
    const/4 v5, 0x0

    .line 17
    const/4 v2, 0x0

    .line 18
    aget v6, v0, v2

    .line 19
    .line 20
    int-to-float v6, v6

    .line 21
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    .line 22
    .line 23
    .line 24
    move-result v7

    .line 25
    int-to-float v7, v7

    .line 26
    const/high16 v8, 0x40000000    # 2.0f

    .line 27
    .line 28
    div-float/2addr v7, v8

    .line 29
    add-float/2addr v6, v7

    .line 30
    aget v2, v1, v2

    .line 31
    .line 32
    int-to-float v2, v2

    .line 33
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 34
    .line 35
    .line 36
    move-result v7

    .line 37
    int-to-float v7, v7

    .line 38
    div-float/2addr v7, v8

    .line 39
    add-float/2addr v2, v7

    .line 40
    sub-float/2addr v6, v2

    .line 41
    const/4 v7, 0x0

    .line 42
    const/4 v9, 0x0

    .line 43
    const/4 v10, 0x0

    .line 44
    const/4 v2, 0x1

    .line 45
    aget v0, v0, v2

    .line 46
    .line 47
    int-to-float v0, v0

    .line 48
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    .line 49
    .line 50
    .line 51
    move-result p2

    .line 52
    int-to-float p2, p2

    .line 53
    div-float/2addr p2, v8

    .line 54
    add-float/2addr v0, p2

    .line 55
    aget p2, v1, v2

    .line 56
    .line 57
    int-to-float p2, p2

    .line 58
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    int-to-float p1, p1

    .line 63
    div-float/2addr p1, v8

    .line 64
    add-float/2addr p2, p1

    .line 65
    sub-float p1, v0, p2

    .line 66
    .line 67
    move-object v2, v11

    .line 68
    move v8, v9

    .line 69
    move v9, v10

    .line 70
    move v10, p1

    .line 71
    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 72
    .line 73
    .line 74
    return-object v11
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private static final OO〇OOo(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇O〇(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic Oo0O080(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final Oo0O0o8([BLcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Lcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;)V
    .locals 9

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, ".jpg"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/SDStorageManager;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-static {p0, v0}, Lcom/intsig/camscanner/util/Util;->O0OO8〇0([BLjava/lang/String;)Z

    .line 17
    .line 18
    .line 19
    invoke-direct {p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Ooo08()Z

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    const-string v1, "lastPhotoPath"

    .line 24
    .line 25
    if-eqz p0, :cond_0

    .line 26
    .line 27
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p1, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8o08O8O(Ljava/lang/String;)[I

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    goto :goto_0

    .line 35
    :cond_0
    const/4 p0, 0x0

    .line 36
    :goto_0
    move-object v7, p0

    .line 37
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    invoke-interface {p0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->o0O0()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    const/4 v4, 0x0

    .line 46
    const/4 p0, 0x1

    .line 47
    new-array v5, p0, [Ljava/lang/String;

    .line 48
    .line 49
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    const/4 p0, 0x0

    .line 53
    aput-object v0, v5, p0

    .line 54
    .line 55
    const/4 v6, 0x0

    .line 56
    const/4 v8, 0x1

    .line 57
    move-object v2, p1

    .line 58
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O0O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Z[Ljava/lang/String;Lcom/intsig/camscanner/capture/certificates/ListProgressListener;[IZ)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o88O8(Z)V

    .line 62
    .line 63
    .line 64
    if-eqz p2, :cond_1

    .line 65
    .line 66
    invoke-interface {p2, v0}, Lcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;->〇080(Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    :cond_1
    new-instance p0, LO〇0〇o808〇/Oooo8o0〇;

    .line 70
    .line 71
    invoke-direct {p0, p1}, LO〇0〇o808〇/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8o8O〇O(Ljava/lang/Runnable;)V

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private final Oo0〇Ooo()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO0880O:Landroid/widget/FrameLayout;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    const/4 v1, 0x1

    .line 14
    :cond_0
    return v1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Oo8(I)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_5

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇o〇()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    :goto_0
    const/4 v2, 0x2

    .line 21
    if-le v0, v2, :cond_5

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 24
    .line 25
    const/4 v2, 0x1

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->Oo08()I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    const/16 v3, 0x9

    .line 33
    .line 34
    if-ne v0, v3, :cond_1

    .line 35
    .line 36
    const/4 v0, 0x1

    .line 37
    goto :goto_1

    .line 38
    :cond_1
    const/4 v0, 0x0

    .line 39
    :goto_1
    if-nez v0, :cond_5

    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    iget-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 46
    .line 47
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 48
    .line 49
    .line 50
    move-result v4

    .line 51
    sub-int/2addr v4, v2

    .line 52
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    const-string v3, "mCertificatePageId[mCertificatePageId.size - 1]"

    .line 57
    .line 58
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    check-cast v2, Ljava/lang/Number;

    .line 62
    .line 63
    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    .line 64
    .line 65
    .line 66
    move-result-wide v2

    .line 67
    const-string v4, "raw_data"

    .line 68
    .line 69
    filled-new-array {v4}, [Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v4

    .line 73
    invoke-static {v0, v2, v3, v4}, Lcom/intsig/camscanner/db/dao/ImageDao;->oO00OOO(Landroid/content/Context;J[Ljava/lang/String;)[Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    if-eqz v0, :cond_2

    .line 78
    .line 79
    aget-object v0, v0, v1

    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_2
    const/4 v0, 0x0

    .line 83
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O〇8O8〇008()Landroid/view/View;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    if-eqz v2, :cond_3

    .line 92
    .line 93
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    goto :goto_3

    .line 98
    :cond_3
    const/4 v2, 0x0

    .line 99
    :goto_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 100
    .line 101
    .line 102
    move-result-object v3

    .line 103
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O〇8O8〇008()Landroid/view/View;

    .line 104
    .line 105
    .line 106
    move-result-object v3

    .line 107
    if-eqz v3, :cond_4

    .line 108
    .line 109
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    .line 110
    .line 111
    .line 112
    move-result v3

    .line 113
    goto :goto_4

    .line 114
    :cond_4
    const/4 v3, 0x0

    .line 115
    :goto_4
    sget-object v4, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 116
    .line 117
    invoke-virtual {v4}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇o〇()Landroid/graphics/Bitmap$Config;

    .line 118
    .line 119
    .line 120
    move-result-object v4

    .line 121
    invoke-static {v0, v2, v3, v4, v1}, Lcom/intsig/utils/ImageUtil;->o〇O8〇〇o(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    .line 122
    .line 123
    .line 124
    move-result-object v1

    .line 125
    invoke-static {v0}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 126
    .line 127
    .line 128
    move-result v0

    .line 129
    new-instance v2, Lcom/intsig/camscanner/capture/common/CapturePreviewScaleData;

    .line 130
    .line 131
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 132
    .line 133
    .line 134
    move-result-object v3

    .line 135
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->getRotation()I

    .line 136
    .line 137
    .line 138
    move-result v3

    .line 139
    invoke-direct {v2, v1, v0, v3}, Lcom/intsig/camscanner/capture/common/CapturePreviewScaleData;-><init>(Landroid/graphics/Bitmap;II)V

    .line 140
    .line 141
    .line 142
    new-instance v0, LO〇0〇o808〇/o〇0;

    .line 143
    .line 144
    invoke-direct {v0, p0, p1, v2}, LO〇0〇o808〇/o〇0;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ILcom/intsig/camscanner/capture/common/CapturePreviewScaleData;)V

    .line 145
    .line 146
    .line 147
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8o8O〇O(Ljava/lang/Runnable;)V

    .line 148
    .line 149
    .line 150
    goto :goto_5

    .line 151
    :cond_5
    new-instance v0, LO〇0〇o808〇/〇〇888;

    .line 152
    .line 153
    invoke-direct {v0, p0, p1}, LO〇0〇o808〇/〇〇888;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;I)V

    .line 154
    .line 155
    .line 156
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8o8O〇O(Ljava/lang/Runnable;)V

    .line 157
    .line 158
    .line 159
    :goto_5
    return-void
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method private final Oo80()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇o08:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo()Landroid/os/Handler;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    new-instance v1, LO〇0〇o808〇/Oo08;

    .line 11
    .line 12
    invoke-direct {v1, p0}, LO〇0〇o808〇/Oo08;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 13
    .line 14
    .line 15
    const-wide/16 v2, 0x1388

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Ooo08()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    const/16 v2, 0x3f4

    .line 7
    .line 8
    iget v0, v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 9
    .line 10
    if-ne v2, v0, :cond_0

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    :cond_0
    return v1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Ooo8o()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "CertificateCaptureScene"

    .line 6
    .line 7
    const-string v1, "updateThumb mCertificateCapture == null"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    const/4 v1, 0x0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇o〇()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    goto :goto_0

    .line 21
    :cond_1
    const/4 v0, 0x0

    .line 22
    :goto_0
    const/4 v2, 0x1

    .line 23
    if-gt v0, v2, :cond_2

    .line 24
    .line 25
    return-void

    .line 26
    :cond_2
    iget-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo0O0o8:Landroid/view/View;

    .line 27
    .line 28
    const/16 v4, 0x8

    .line 29
    .line 30
    const/4 v5, 0x0

    .line 31
    if-eqz v3, :cond_4

    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oO00〇o()Z

    .line 34
    .line 35
    .line 36
    move-result v6

    .line 37
    if-eqz v6, :cond_3

    .line 38
    .line 39
    const/4 v4, 0x0

    .line 40
    :cond_3
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 41
    .line 42
    .line 43
    goto :goto_5

    .line 44
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    if-eqz v3, :cond_5

    .line 49
    .line 50
    const v6, 0x7f0a113f

    .line 51
    .line 52
    .line 53
    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    check-cast v3, Landroid/view/ViewStub;

    .line 58
    .line 59
    goto :goto_1

    .line 60
    :cond_5
    move-object v3, v5

    .line 61
    :goto_1
    if-nez v3, :cond_6

    .line 62
    .line 63
    goto :goto_2

    .line 64
    :cond_6
    invoke-virtual {v3, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 65
    .line 66
    .line 67
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    if-eqz v3, :cond_7

    .line 72
    .line 73
    const v6, 0x7f0a0d36

    .line 74
    .line 75
    .line 76
    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 77
    .line 78
    .line 79
    move-result-object v3

    .line 80
    goto :goto_3

    .line 81
    :cond_7
    move-object v3, v5

    .line 82
    :goto_3
    iput-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo0O0o8:Landroid/view/View;

    .line 83
    .line 84
    if-nez v3, :cond_8

    .line 85
    .line 86
    goto :goto_4

    .line 87
    :cond_8
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oO00〇o()Z

    .line 88
    .line 89
    .line 90
    move-result v6

    .line 91
    if-eqz v6, :cond_9

    .line 92
    .line 93
    const/4 v4, 0x0

    .line 94
    :cond_9
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 95
    .line 96
    .line 97
    :goto_4
    iget-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo0O0o8:Landroid/view/View;

    .line 98
    .line 99
    if-eqz v3, :cond_a

    .line 100
    .line 101
    new-instance v4, LO〇0〇o808〇/〇o00〇〇Oo;

    .line 102
    .line 103
    invoke-direct {v4, p0}, LO〇0〇o808〇/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    .line 108
    .line 109
    :cond_a
    :goto_5
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o8oO〇()Landroid/view/View;

    .line 110
    .line 111
    .line 112
    move-result-object v3

    .line 113
    const/4 v4, 0x4

    .line 114
    if-nez v3, :cond_b

    .line 115
    .line 116
    goto :goto_7

    .line 117
    :cond_b
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇o〇Oo88()Z

    .line 118
    .line 119
    .line 120
    move-result v6

    .line 121
    if-eqz v6, :cond_c

    .line 122
    .line 123
    const/4 v6, 0x4

    .line 124
    goto :goto_6

    .line 125
    :cond_c
    const/4 v6, 0x0

    .line 126
    :goto_6
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 127
    .line 128
    .line 129
    :goto_7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 130
    .line 131
    .line 132
    move-result-object v3

    .line 133
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o8oO〇()Landroid/view/View;

    .line 134
    .line 135
    .line 136
    move-result-object v6

    .line 137
    if-eqz v6, :cond_d

    .line 138
    .line 139
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    .line 140
    .line 141
    .line 142
    move-result v6

    .line 143
    if-nez v6, :cond_d

    .line 144
    .line 145
    const/4 v6, 0x1

    .line 146
    goto :goto_8

    .line 147
    :cond_d
    const/4 v6, 0x0

    .line 148
    :goto_8
    xor-int/2addr v6, v2

    .line 149
    invoke-interface {v3, v6}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇808〇(Z)V

    .line 150
    .line 151
    .line 152
    const/4 v3, 0x2

    .line 153
    if-le v0, v3, :cond_17

    .line 154
    .line 155
    sget-object v0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;

    .line 156
    .line 157
    iget-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 158
    .line 159
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->〇80〇808〇O(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)Z

    .line 160
    .line 161
    .line 162
    move-result v0

    .line 163
    if-eqz v0, :cond_e

    .line 164
    .line 165
    goto/16 :goto_c

    .line 166
    .line 167
    :cond_e
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o8〇OO()V

    .line 168
    .line 169
    .line 170
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 171
    .line 172
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 173
    .line 174
    .line 175
    move-result v3

    .line 176
    if-gtz v3, :cond_11

    .line 177
    .line 178
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8o〇O0:Landroid/view/View;

    .line 179
    .line 180
    if-nez v0, :cond_f

    .line 181
    .line 182
    goto :goto_9

    .line 183
    :cond_f
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 184
    .line 185
    .line 186
    :goto_9
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO8:Landroid/widget/ImageView;

    .line 187
    .line 188
    if-eqz v0, :cond_10

    .line 189
    .line 190
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 191
    .line 192
    .line 193
    :cond_10
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇oo〇O〇80:Lcom/intsig/view/RotateTextView;

    .line 194
    .line 195
    if-eqz v0, :cond_17

    .line 196
    .line 197
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 198
    .line 199
    .line 200
    goto/16 :goto_c

    .line 201
    .line 202
    :cond_11
    iget-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8o〇O0:Landroid/view/View;

    .line 203
    .line 204
    if-nez v3, :cond_12

    .line 205
    .line 206
    goto :goto_a

    .line 207
    :cond_12
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 208
    .line 209
    .line 210
    :goto_a
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 211
    .line 212
    .line 213
    move-result-object v3

    .line 214
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 215
    .line 216
    .line 217
    move-result v4

    .line 218
    sub-int/2addr v4, v2

    .line 219
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 220
    .line 221
    .line 222
    move-result-object v4

    .line 223
    const-string v6, "it[it.size - 1]"

    .line 224
    .line 225
    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    check-cast v4, Ljava/lang/Number;

    .line 229
    .line 230
    invoke-virtual {v4}, Ljava/lang/Number;->longValue()J

    .line 231
    .line 232
    .line 233
    move-result-wide v6

    .line 234
    const-string v4, "_data"

    .line 235
    .line 236
    filled-new-array {v4}, [Ljava/lang/String;

    .line 237
    .line 238
    .line 239
    move-result-object v4

    .line 240
    invoke-static {v3, v6, v7, v4}, Lcom/intsig/camscanner/db/dao/ImageDao;->oO00OOO(Landroid/content/Context;J[Ljava/lang/String;)[Ljava/lang/String;

    .line 241
    .line 242
    .line 243
    move-result-object v3

    .line 244
    if-eqz v3, :cond_13

    .line 245
    .line 246
    aget-object v5, v3, v1

    .line 247
    .line 248
    :cond_13
    iget-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO8:Landroid/widget/ImageView;

    .line 249
    .line 250
    if-nez v3, :cond_14

    .line 251
    .line 252
    goto :goto_b

    .line 253
    :cond_14
    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    .line 254
    .line 255
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 256
    .line 257
    .line 258
    :goto_b
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 259
    .line 260
    .line 261
    move-result-object v3

    .line 262
    invoke-static {v3}, Lcom/bumptech/glide/Glide;->oo88o8O(Landroidx/fragment/app/FragmentActivity;)Lcom/bumptech/glide/RequestManager;

    .line 263
    .line 264
    .line 265
    move-result-object v3

    .line 266
    invoke-virtual {v3, v5}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 267
    .line 268
    .line 269
    move-result-object v3

    .line 270
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0O()Lcom/bumptech/glide/request/RequestOptions;

    .line 271
    .line 272
    .line 273
    move-result-object v4

    .line 274
    invoke-virtual {v3, v4}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 275
    .line 276
    .line 277
    move-result-object v3

    .line 278
    const-string v4, "with(activity)\n         \u2026pply(getLoadImgOptions())"

    .line 279
    .line 280
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    .line 282
    .line 283
    iget-object v4, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO8:Landroid/widget/ImageView;

    .line 284
    .line 285
    if-eqz v4, :cond_15

    .line 286
    .line 287
    invoke-virtual {v3, v4}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 288
    .line 289
    .line 290
    :cond_15
    iget-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇oo〇O〇80:Lcom/intsig/view/RotateTextView;

    .line 291
    .line 292
    if-nez v3, :cond_16

    .line 293
    .line 294
    goto :goto_c

    .line 295
    :cond_16
    new-array v2, v2, [Ljava/lang/Object;

    .line 296
    .line 297
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 298
    .line 299
    .line 300
    move-result v0

    .line 301
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 302
    .line 303
    .line 304
    move-result-object v0

    .line 305
    aput-object v0, v2, v1

    .line 306
    .line 307
    const-string v0, "%d"

    .line 308
    .line 309
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/StringUtil;->o〇0(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 310
    .line 311
    .line 312
    move-result-object v0

    .line 313
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    .line 315
    .line 316
    :cond_17
    :goto_c
    return-void
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
.end method

.method private final O〇08oOOO0(Landroidx/fragment/app/FragmentActivity;)V
    .locals 3

    .line 1
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "getInstance()"

    .line 8
    .line 9
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {v0, p1, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 13
    .line 14
    .line 15
    const-class v1, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0〇0:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 24
    .line 25
    if-eqz v0, :cond_0

    .line 26
    .line 27
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇oo(Landroid/app/Activity;)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final O〇O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 6

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v1, 0x4

    .line 13
    const/4 v2, 0x2

    .line 14
    const/4 v3, 0x0

    .line 15
    const/4 v4, 0x1

    .line 16
    const/4 v5, 0x0

    .line 17
    if-nez v0, :cond_2

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8o〇O0:Landroid/view/View;

    .line 20
    .line 21
    if-nez v0, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 25
    .line 26
    .line 27
    :goto_0
    invoke-static {p0, v4, v5, v2, v3}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0〇0(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ZZILjava/lang/Object;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇o88()V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-interface {v0, v5, v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O(ZLcom/intsig/camscanner/capture/CaptureMode;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo0O0o8:Landroid/view/View;

    .line 41
    .line 42
    if-nez v0, :cond_1

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_1
    const/16 v2, 0x8

    .line 46
    .line 47
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 48
    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_2
    invoke-static {p0, v5, v5, v2, v3}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0〇0(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ZZILjava/lang/Object;)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 55
    .line 56
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    add-int/2addr v0, v4

    .line 61
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo8(I)V

    .line 62
    .line 63
    .line 64
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o8oO〇()Landroid/view/View;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    if-nez v0, :cond_3

    .line 69
    .line 70
    goto :goto_3

    .line 71
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇o〇Oo88()Z

    .line 72
    .line 73
    .line 74
    move-result v2

    .line 75
    if-eqz v2, :cond_4

    .line 76
    .line 77
    goto :goto_2

    .line 78
    :cond_4
    const/4 v1, 0x0

    .line 79
    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 80
    .line 81
    .line 82
    :goto_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o8oO〇()Landroid/view/View;

    .line 87
    .line 88
    .line 89
    move-result-object p0

    .line 90
    if-eqz p0, :cond_5

    .line 91
    .line 92
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    .line 93
    .line 94
    .line 95
    move-result p0

    .line 96
    if-nez p0, :cond_5

    .line 97
    .line 98
    const/4 v5, 0x1

    .line 99
    :cond_5
    xor-int/lit8 p0, v5, 0x1

    .line 100
    .line 101
    invoke-interface {v0, p0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇808〇(Z)V

    .line 102
    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private static final O〇o88o08〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇o08:Z

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇o88:Landroid/widget/TextView;

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v1, 0x4

    .line 15
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 16
    .line 17
    .line 18
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇80O8o8O〇:Landroid/view/View;

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOo〇08〇:Landroid/view/View;

    .line 21
    .line 22
    new-instance v2, LO〇0〇o808〇/OO0o〇〇〇〇0;

    .line 23
    .line 24
    invoke-direct {v2, p0}, LO〇0〇o808〇/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 25
    .line 26
    .line 27
    invoke-direct {p0, v0, v1, v2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8o〇O0(Landroid/view/View;Landroid/view/View;Ljava/lang/Runnable;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final o00〇88〇08(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "certificateItemInfo"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO0880O(Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o08o〇0()V
    .locals 2

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0oO〇oo00:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    const-string v1, "CertificateCaptureScene"

    .line 11
    .line 12
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0oO〇oo00:Lcom/intsig/app/BaseProgressDialog;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o0OoOOo0(Ljava/lang/String;)V
    .locals 3

    .line 1
    const-string v0, "select_id_mode"

    .line 2
    .line 3
    const-string v1, "type"

    .line 4
    .line 5
    const-string v2, "CSScan"

    .line 6
    .line 7
    invoke-static {v2, v0, v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic o0oO(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O〇o88o08〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final o880(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O〇〇O8()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o88o0O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o8oOOo(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o8O()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oO0()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇o88()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0ooOOo()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oooO888(Landroid/content/Context;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/4 v1, 0x0

    .line 22
    const/4 v2, 0x0

    .line 23
    invoke-interface {v0, v2, v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O(ZLcom/intsig/camscanner/capture/CaptureMode;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0ooOOo:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 27
    .line 28
    if-eqz v0, :cond_0

    .line 29
    .line 30
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O(Z)V

    .line 31
    .line 32
    .line 33
    :cond_0
    return-void
.end method

.method private static final o8o([Landroid/net/Uri;Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 9

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-static {p0}, Lkotlin/jvm/internal/ArrayIteratorKt;->〇080([Ljava/lang/Object;)Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    check-cast v1, Landroid/net/Uri;

    .line 26
    .line 27
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    new-instance v4, Ljava/lang/StringBuilder;

    .line 36
    .line 37
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v2, ".jpg"

    .line 47
    .line 48
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    invoke-static {}, Lcom/intsig/utils/DocumentUtil;->Oo08()Lcom/intsig/utils/DocumentUtil;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 60
    .line 61
    .line 62
    move-result-object v4

    .line 63
    invoke-virtual {v3, v4, v1, v2}, Lcom/intsig/utils/DocumentUtil;->〇8o8o〇(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Z

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    if-eqz v1, :cond_0

    .line 68
    .line 69
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    invoke-static {v1, v2}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇〇888(Landroid/content/Context;Ljava/lang/String;)Z

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    if-eqz v1, :cond_0

    .line 78
    .line 79
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 84
    .line 85
    .line 86
    move-result p0

    .line 87
    const/4 v1, 0x0

    .line 88
    if-lez p0, :cond_2

    .line 89
    .line 90
    new-array p0, v1, [Ljava/lang/String;

    .line 91
    .line 92
    invoke-interface {v0, p0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 93
    .line 94
    .line 95
    move-result-object p0

    .line 96
    move-object v5, p0

    .line 97
    check-cast v5, [Ljava/lang/String;

    .line 98
    .line 99
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 100
    .line 101
    .line 102
    move-result-object p0

    .line 103
    invoke-interface {p0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->o0O0()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 104
    .line 105
    .line 106
    move-result-object v3

    .line 107
    const/4 v4, 0x1

    .line 108
    new-instance v6, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$handleImportPicture$1$2$1;

    .line 109
    .line 110
    invoke-direct {v6, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$handleImportPicture$1$2$1;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 111
    .line 112
    .line 113
    const/4 v7, 0x0

    .line 114
    const/4 v8, 0x0

    .line 115
    move-object v2, p1

    .line 116
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O0O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Z[Ljava/lang/String;Lcom/intsig/camscanner/capture/certificates/ListProgressListener;[IZ)V

    .line 117
    .line 118
    .line 119
    goto :goto_1

    .line 120
    :cond_2
    const-string p0, "CertificateCaptureScene"

    .line 121
    .line 122
    const-string v0, "stringList.size = 0"

    .line 123
    .line 124
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    :goto_1
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o88O8(Z)V

    .line 128
    .line 129
    .line 130
    new-instance p0, LO〇0〇o808〇/〇〇808〇;

    .line 131
    .line 132
    invoke-direct {p0, p1}, LO〇0〇o808〇/〇〇808〇;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 133
    .line 134
    .line 135
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8o8O〇O(Ljava/lang/Runnable;)V

    .line 136
    .line 137
    .line 138
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private static final o8oOOo(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Z)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 7
    .line 8
    instance-of v0, v0, Lcom/intsig/camscanner/capture/certificates/model/SingleIdCardCapture;

    .line 9
    .line 10
    xor-int/lit8 v0, v0, 0x1

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇O8OO(ZZ)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇808〇(Z)V

    .line 21
    .line 22
    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOo88OOo(Z)V

    .line 26
    .line 27
    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o8〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->OO〇OOo(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final o8〇OO()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8o〇O0:Landroid/view/View;

    .line 2
    .line 3
    if-nez v0, :cond_5

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O()Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_5

    .line 10
    .line 11
    const v1, 0x7f0a1a5b

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Landroid/view/ViewStub;

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    if-nez v0, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 25
    .line 26
    .line 27
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O()Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    const/4 v2, 0x0

    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    const v3, 0x7f0a063a

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    goto :goto_1

    .line 42
    :cond_1
    move-object v0, v2

    .line 43
    :goto_1
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8o〇O0:Landroid/view/View;

    .line 44
    .line 45
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O()Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    if-eqz v0, :cond_2

    .line 50
    .line 51
    const v3, 0x7f0a0e26

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    check-cast v0, Landroid/widget/ImageView;

    .line 59
    .line 60
    goto :goto_2

    .line 61
    :cond_2
    move-object v0, v2

    .line 62
    :goto_2
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO8:Landroid/widget/ImageView;

    .line 63
    .line 64
    const/4 v3, 0x1

    .line 65
    new-array v3, v3, [Landroid/view/View;

    .line 66
    .line 67
    aput-object v0, v3, v1

    .line 68
    .line 69
    invoke-virtual {p0, v3}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8O0O808〇([Landroid/view/View;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O()Landroid/view/View;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    if-eqz v0, :cond_3

    .line 77
    .line 78
    const v1, 0x7f0a0e27

    .line 79
    .line 80
    .line 81
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    move-object v2, v0

    .line 86
    check-cast v2, Lcom/intsig/view/RotateTextView;

    .line 87
    .line 88
    :cond_3
    iput-object v2, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇oo〇O〇80:Lcom/intsig/view/RotateTextView;

    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO8:Landroid/widget/ImageView;

    .line 91
    .line 92
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇〇〇(Landroid/view/View;)V

    .line 93
    .line 94
    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8o〇O0:Landroid/view/View;

    .line 96
    .line 97
    if-nez v0, :cond_4

    .line 98
    .line 99
    goto :goto_3

    .line 100
    :cond_4
    const/4 v1, 0x4

    .line 101
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 102
    .line 103
    .line 104
    :cond_5
    :goto_3
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final o8〇OO0〇0o()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 10
    .line 11
    .line 12
    move-result-wide v1

    .line 13
    const/4 v3, 0x0

    .line 14
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 15
    .line 16
    .line 17
    move-result v4

    .line 18
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/certificate_package/activity/CertificateDetailActivity;->O0〇(Landroid/content/Context;JZZ)Landroid/content/Intent;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oO0()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_1

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    const-wide/16 v2, 0x0

    .line 18
    .line 19
    cmp-long v4, v0, v2

    .line 20
    .line 21
    if-lez v4, :cond_1

    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    const-string v1, "com.intsig.camscanner.NEW_PAGE"

    .line 36
    .line 37
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    const/4 v1, 0x1

    .line 42
    const/4 v2, 0x2

    .line 43
    if-eqz v0, :cond_0

    .line 44
    .line 45
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 54
    .line 55
    .line 56
    move-result-wide v3

    .line 57
    const/4 v5, 0x3

    .line 58
    invoke-static {v0, v3, v4, v5, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 66
    .line 67
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO〇00〇8oO(Landroid/content/Context;Ljava/util/List;I)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 79
    .line 80
    .line 81
    move-result-wide v1

    .line 82
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->Oo〇O(Landroid/content/Context;J)V

    .line 83
    .line 84
    .line 85
    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 91
    .line 92
    .line 93
    move-result-object v3

    .line 94
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 95
    .line 96
    .line 97
    move-result-wide v3

    .line 98
    invoke-static {v0, v3, v4, v2, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    const-wide/16 v1, -0x1

    .line 106
    .line 107
    invoke-interface {v0, v1, v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇o〇(J)V

    .line 108
    .line 109
    .line 110
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O0〇0:Landroid/util/ArrayMap;

    .line 111
    .line 112
    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V

    .line 113
    .line 114
    .line 115
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 116
    .line 117
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 118
    .line 119
    .line 120
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 121
    .line 122
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 123
    .line 124
    .line 125
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->OOo()V

    .line 126
    .line 127
    .line 128
    return-void
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final oO00〇o()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-gtz v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oO0〇〇O8o(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0oO〇oo00:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic oO0〇〇o8〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Landroid/widget/ImageView;IILandroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O(Landroid/widget/ImageView;IILandroid/graphics/drawable/Drawable;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
.end method

.method public static final synthetic oO80OOO〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)Landroid/os/Handler;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo()Landroid/os/Handler;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final oOO0880O(Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    iget v1, p1, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v1, 0x0

    .line 10
    :goto_0
    invoke-static {v1}, Lcom/intsig/camscanner/capture/certificates/model/CertificateCaptureFactory;->〇080(I)Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇80O8o8O〇(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)V

    .line 15
    .line 16
    .line 17
    const/16 v1, 0x3f3

    .line 18
    .line 19
    iget p1, p1, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 20
    .line 21
    if-ne v1, p1, :cond_1

    .line 22
    .line 23
    const-string p1, "CSScan"

    .line 24
    .line 25
    const-string v0, "scan_more_certificate"

    .line 26
    .line 27
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇oO〇〇8o()V

    .line 31
    .line 32
    .line 33
    goto :goto_2

    .line 34
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O8oOo0:Landroid/view/View;

    .line 35
    .line 36
    if-nez p1, :cond_2

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 40
    .line 41
    .line 42
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇OO8ooO8〇()Z

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    if-eqz p1, :cond_3

    .line 47
    .line 48
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇08O()V

    .line 49
    .line 50
    .line 51
    return-void

    .line 52
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇08〇o0O()V

    .line 53
    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8〇o〇88:Landroid/widget/ImageView;

    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 58
    .line 59
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇oO(Landroid/widget/ImageView;Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;)V

    .line 60
    .line 61
    .line 62
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o〇()V

    .line 63
    .line 64
    .line 65
    :goto_2
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final oOO8(I)V
    .locals 2

    .line 1
    const-string v0, "id_card"

    .line 2
    .line 3
    const-string v1, "CertificateCaptureScene"

    .line 4
    .line 5
    packed-switch p1, :pswitch_data_0

    .line 6
    .line 7
    .line 8
    :pswitch_0
    const-string p1, "exception "

    .line 9
    .line 10
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    goto/16 :goto_0

    .line 14
    .line 15
    :pswitch_1
    const-string p1, "CertificateCapture mode onClick SinglePageCapture"

    .line 16
    .line 17
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const-string p1, "one_page_id"

    .line 21
    .line 22
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    goto/16 :goto_0

    .line 26
    .line 27
    :pswitch_2
    const-string p1, "CertificateCapture mode onClick household_register_collage"

    .line 28
    .line 29
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    const-string p1, "household_register_collage"

    .line 33
    .line 34
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :pswitch_3
    const-string p1, "CertificateCapture mode onClick CertificateCapture"

    .line 39
    .line 40
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :pswitch_4
    const-string p1, "CertificateCapture mode onClick BankCardCapture"

    .line 48
    .line 49
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    const-string p1, "bank_card"

    .line 53
    .line 54
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :pswitch_5
    const-string p1, "CertificateCapture mode onClick USDriverCapture"

    .line 59
    .line 60
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    const-string p1, "oversea_driver"

    .line 64
    .line 65
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    goto :goto_0

    .line 69
    :pswitch_6
    const-string p1, "CertificateCapture mode onClick BusinessLicenseCapture"

    .line 70
    .line 71
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    const-string p1, "business_license"

    .line 75
    .line 76
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    goto :goto_0

    .line 80
    :pswitch_7
    const-string p1, "CertificateCapture mode onClick HousePropertyCapture"

    .line 81
    .line 82
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    const-string p1, "house"

    .line 86
    .line 87
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    goto :goto_0

    .line 91
    :pswitch_8
    const-string p1, "CertificateCapture mode onClick CN drive car liscence"

    .line 92
    .line 93
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    const-string p1, "china_car"

    .line 97
    .line 98
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    goto :goto_0

    .line 102
    :pswitch_9
    const-string p1, "CertificateCapture mode onClick CN drive person liscence"

    .line 103
    .line 104
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    const-string p1, "china_driver"

    .line 108
    .line 109
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    goto :goto_0

    .line 113
    :pswitch_a
    const-string p1, "CertificateCapture mode onClick ResidenceBookletCapture"

    .line 114
    .line 115
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    const-string p1, "household_register"

    .line 119
    .line 120
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    goto :goto_0

    .line 124
    :pswitch_b
    const-string p1, "CertificateCapture mode onClick PassPortCapture"

    .line 125
    .line 126
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    const-string p1, "passport"

    .line 130
    .line 131
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    goto :goto_0

    .line 135
    :pswitch_c
    const-string p1, "CertificateCapture mode onClick IDCardCapture"

    .line 136
    .line 137
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0OoOOo0(Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    :goto_0
    return-void

    .line 144
    nop

    .line 145
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method private static final oOO〇〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;[Landroid/net/Uri;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    array-length p1, p1

    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoO8OO〇(I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oOo0(Landroid/view/View;Landroid/view/View;)Landroid/view/animation/Animation;
    .locals 12

    .line 1
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    const/high16 v1, 0x3f800000    # 1.0f

    .line 7
    .line 8
    mul-float v0, v0, v1

    .line 9
    .line 10
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    int-to-float v2, v2

    .line 15
    div-float v5, v0, v2

    .line 16
    .line 17
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    .line 18
    .line 19
    .line 20
    move-result p2

    .line 21
    int-to-float p2, p2

    .line 22
    mul-float p2, p2, v1

    .line 23
    .line 24
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    int-to-float p1, p1

    .line 29
    div-float v7, p2, p1

    .line 30
    .line 31
    new-instance p1, Landroid/view/animation/ScaleAnimation;

    .line 32
    .line 33
    const/high16 v4, 0x3f800000    # 1.0f

    .line 34
    .line 35
    const/high16 v6, 0x3f800000    # 1.0f

    .line 36
    .line 37
    const/4 v8, 0x1

    .line 38
    const/high16 v9, 0x3f000000    # 0.5f

    .line 39
    .line 40
    const/4 v10, 0x1

    .line 41
    const/high16 v11, 0x3f000000    # 0.5f

    .line 42
    .line 43
    move-object v3, p1

    .line 44
    invoke-direct/range {v3 .. v11}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 45
    .line 46
    .line 47
    return-object p1
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oOoO8OO〇(I)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    invoke-static {v0, v1}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0oO〇oo00:Lcom/intsig/app/BaseProgressDialog;

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const v2, 0x7f131d02

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, p1}, Lcom/intsig/app/BaseProgressDialog;->o〇0OOo〇0(I)V

    .line 33
    .line 34
    .line 35
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :catch_0
    move-exception p1

    .line 40
    const-string v0, "CertificateCaptureScene"

    .line 41
    .line 42
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    .line 44
    .line 45
    :cond_0
    :goto_0
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private static final oOoo80oO(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇O〇(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final oOo〇08〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇80O8o8O〇:Landroid/view/View;

    .line 2
    .line 3
    const/4 v1, 0x4

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 8
    .line 9
    .line 10
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇o88:Landroid/widget/TextView;

    .line 11
    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_1
    const/4 v2, 0x0

    .line 16
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 17
    .line 18
    .line 19
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOo〇08〇:Landroid/view/View;

    .line 20
    .line 21
    if-nez v0, :cond_2

    .line 22
    .line 23
    goto :goto_2

    .line 24
    :cond_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 25
    .line 26
    .line 27
    :goto_2
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final oO〇8O8oOo(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇0O8ooO()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final oO〇oo()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 2
    .line 3
    const-string v1, "CertificateCaptureScene"

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string v0, "showCertificateFrameContainer mCertificateCapture == null"

    .line 8
    .line 9
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    const-string v0, "showCertificateFrameContainer"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO0880O:Landroid/widget/FrameLayout;

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    const/4 v2, 0x0

    .line 22
    if-nez v0, :cond_4

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    const v3, 0x7f0a1149

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    check-cast v0, Landroid/view/ViewStub;

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    move-object v0, v2

    .line 41
    :goto_0
    if-nez v0, :cond_2

    .line 42
    .line 43
    goto :goto_1

    .line 44
    :cond_2
    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 45
    .line 46
    .line 47
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    if-eqz v0, :cond_3

    .line 52
    .line 53
    const v3, 0x7f0a0621

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    check-cast v0, Landroid/widget/FrameLayout;

    .line 61
    .line 62
    goto :goto_2

    .line 63
    :cond_3
    move-object v0, v2

    .line 64
    :goto_2
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO0880O:Landroid/widget/FrameLayout;

    .line 65
    .line 66
    goto :goto_3

    .line 67
    :cond_4
    if-eqz v0, :cond_5

    .line 68
    .line 69
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 70
    .line 71
    .line 72
    :cond_5
    :goto_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    invoke-interface {v0, v1, v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O(ZLcom/intsig/camscanner/capture/CaptureMode;)V

    .line 77
    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 80
    .line 81
    if-eqz v0, :cond_6

    .line 82
    .line 83
    iget-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO0880O:Landroid/widget/FrameLayout;

    .line 84
    .line 85
    if-eqz v3, :cond_6

    .line 86
    .line 87
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 88
    .line 89
    .line 90
    move-result-object v4

    .line 91
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇o00〇〇Oo(Landroid/content/Context;)Landroid/view/View;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 96
    .line 97
    .line 98
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO0880O:Landroid/widget/FrameLayout;

    .line 99
    .line 100
    if-eqz v0, :cond_7

    .line 101
    .line 102
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 103
    .line 104
    .line 105
    move-result-object v2

    .line 106
    :cond_7
    instance-of v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 107
    .line 108
    if-eqz v0, :cond_a

    .line 109
    .line 110
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo〇O()Z

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    if-eqz v0, :cond_8

    .line 119
    .line 120
    move-object v0, v2

    .line 121
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 122
    .line 123
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 124
    .line 125
    .line 126
    move-result-object v1

    .line 127
    const/16 v3, 0x28

    .line 128
    .line 129
    invoke-static {v1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 130
    .line 131
    .line 132
    move-result v1

    .line 133
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 134
    .line 135
    goto :goto_4

    .line 136
    :cond_8
    move-object v0, v2

    .line 137
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 138
    .line 139
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 140
    .line 141
    :goto_4
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO0880O:Landroid/widget/FrameLayout;

    .line 142
    .line 143
    if-nez v0, :cond_9

    .line 144
    .line 145
    goto :goto_5

    .line 146
    :cond_9
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    .line 148
    .line 149
    :cond_a
    :goto_5
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0ooOOo()V

    .line 150
    .line 151
    .line 152
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o8〇OO()V

    .line 153
    .line 154
    .line 155
    return-void
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static final synthetic oo08OO〇0(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Ooo8o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final oo8ooo8O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o08o〇0()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final ooO()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->o0O0()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_TEMPLATE_DIR_PRESET:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 10
    .line 11
    if-ne v0, v1, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final ooo0〇〇O(ILandroid/content/Intent;)V
    .locals 10

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "com.intsig.camscanner.NEW_PAGE"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    const-string v1, "com.intsig.camscanner.NEW_DOC_CERTIFICATE"

    .line 20
    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    const-string v2, "com.intsig.camscanner.NEW_PAGE_CERTIFICATE"

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    move-object v2, v1

    .line 27
    :goto_0
    new-instance v3, Landroid/content/Intent;

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    const-class v5, Lcom/intsig/camscanner/DocumentActivity;

    .line 34
    .line 35
    const/4 v6, 0x0

    .line 36
    invoke-direct {v3, v2, v6, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    .line 38
    .line 39
    if-nez v0, :cond_2

    .line 40
    .line 41
    const-string v0, "constant_add_spec_action"

    .line 42
    .line 43
    const-string v4, "spec_action_show_scan_done"

    .line 44
    .line 45
    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    .line 47
    .line 48
    const-string v0, "extra_doc_type"

    .line 49
    .line 50
    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 51
    .line 52
    .line 53
    const/4 v0, 0x2

    .line 54
    const-string v4, "Constant_doc_finish_page_type"

    .line 55
    .line 56
    if-ne p1, v0, :cond_1

    .line 57
    .line 58
    const-string v0, "Doc_finish_type_id_card_only"

    .line 59
    .line 60
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_1
    const-string v0, "Doc_finish_type_id_card"

    .line 65
    .line 66
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    .line 68
    .line 69
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 74
    .line 75
    .line 76
    move-result-wide v4

    .line 77
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 78
    .line 79
    .line 80
    move-result-object v6

    .line 81
    const/4 v7, 0x0

    .line 82
    const/4 v8, 0x4

    .line 83
    const/4 v9, 0x0

    .line 84
    invoke-static/range {v4 .. v9}, Lcom/intsig/camscanner/scenariodir/util/CertificateDbUtil;->〇〇888(JLjava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Z

    .line 85
    .line 86
    .line 87
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo〇o()Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    const-string v0, "EXTRA_LOTTERY_VALUE"

    .line 96
    .line 97
    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    .line 99
    .line 100
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    const-string v0, "extra_folder_id"

    .line 109
    .line 110
    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    .line 112
    .line 113
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->o0O0()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    const-string v0, "extra_entrance"

    .line 122
    .line 123
    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 124
    .line 125
    .line 126
    const-string p1, "extra_id_card_flow"

    .line 127
    .line 128
    const/4 v0, 0x0

    .line 129
    if-eqz p2, :cond_3

    .line 130
    .line 131
    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 132
    .line 133
    .line 134
    move-result p2

    .line 135
    goto :goto_2

    .line 136
    :cond_3
    const/4 p2, 0x0

    .line 137
    :goto_2
    invoke-virtual {v3, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 138
    .line 139
    .line 140
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 141
    .line 142
    .line 143
    move-result p1

    .line 144
    const/4 p2, -0x1

    .line 145
    const-string v1, "CertificateCaptureScene"

    .line 146
    .line 147
    if-eqz p1, :cond_5

    .line 148
    .line 149
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 150
    .line 151
    .line 152
    move-result-object p1

    .line 153
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 154
    .line 155
    .line 156
    move-result-object p1

    .line 157
    const-wide/16 v4, -0x1

    .line 158
    .line 159
    if-eqz p1, :cond_4

    .line 160
    .line 161
    const-string v2, "tag_id"

    .line 162
    .line 163
    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 164
    .line 165
    .line 166
    move-result-wide v4

    .line 167
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 168
    .line 169
    .line 170
    move-result-object v2

    .line 171
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 172
    .line 173
    .line 174
    move-result-wide v6

    .line 175
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 176
    .line 177
    .line 178
    move-result-object v2

    .line 179
    invoke-static {v6, v7, v4, v5, v2}, Lcom/intsig/camscanner/util/Util;->o88〇OO08〇(JJLandroid/content/Context;)Landroid/net/Uri;

    .line 180
    .line 181
    .line 182
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 183
    .line 184
    .line 185
    move-result-object v2

    .line 186
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O0OO8〇0()Z

    .line 187
    .line 188
    .line 189
    move-result v2

    .line 190
    const-string v4, "extra_from_widget"

    .line 191
    .line 192
    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 193
    .line 194
    .line 195
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 196
    .line 197
    .line 198
    move-result-object v2

    .line 199
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇0O〇Oo()Z

    .line 200
    .line 201
    .line 202
    move-result v2

    .line 203
    const-string v4, "extra_start_do_camera"

    .line 204
    .line 205
    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 206
    .line 207
    .line 208
    const-string v2, "finishCertificateCapture, create a new document."

    .line 209
    .line 210
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 214
    .line 215
    .line 216
    move-result-object v1

    .line 217
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 218
    .line 219
    .line 220
    move-result-wide v1

    .line 221
    const-string v4, "doc_id"

    .line 222
    .line 223
    invoke-virtual {v3, v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 224
    .line 225
    .line 226
    sget-object v1, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;->〇080:Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;

    .line 227
    .line 228
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 229
    .line 230
    .line 231
    move-result-object v2

    .line 232
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object v2

    .line 236
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 237
    .line 238
    .line 239
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 240
    .line 241
    .line 242
    move-result-object v1

    .line 243
    invoke-interface {v1, v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇o(Landroid/content/Intent;)V

    .line 244
    .line 245
    .line 246
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 247
    .line 248
    .line 249
    move-result-object v1

    .line 250
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 251
    .line 252
    .line 253
    move-result-object v2

    .line 254
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 255
    .line 256
    .line 257
    move-result-wide v2

    .line 258
    const/4 v4, 0x1

    .line 259
    invoke-static {v1, v2, v3, v4, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 260
    .line 261
    .line 262
    const-string v1, "id_mode"

    .line 263
    .line 264
    invoke-static {v1}, Lcom/intsig/appsflyer/AppsFlyerHelper;->Oo08(Ljava/lang/String;)V

    .line 265
    .line 266
    .line 267
    if-eqz p1, :cond_6

    .line 268
    .line 269
    const-string v1, "constant_is_add_new_doc"

    .line 270
    .line 271
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 272
    .line 273
    .line 274
    move-result p1

    .line 275
    if-eqz p1, :cond_6

    .line 276
    .line 277
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 278
    .line 279
    .line 280
    move-result-object p1

    .line 281
    invoke-virtual {p1, p2}, Landroid/app/Activity;->setResult(I)V

    .line 282
    .line 283
    .line 284
    goto :goto_3

    .line 285
    :cond_5
    const-string p1, "finishCertificateCapture,it is an old document."

    .line 286
    .line 287
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    .line 289
    .line 290
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 291
    .line 292
    .line 293
    move-result-object p1

    .line 294
    invoke-virtual {p1, p2, v3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 295
    .line 296
    .line 297
    :cond_6
    :goto_3
    return-void
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private final oooO888(Landroid/content/Context;)V
    .locals 11

    .line 1
    const-string v0, "CertificateCaptureScene"

    .line 2
    .line 3
    const-string v1, "showCertificateMenu"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSScan"

    .line 9
    .line 10
    const-string v1, "scan_idmode"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const/16 v0, 0x8

    .line 16
    .line 17
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O8oOo0(I)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o8oO〇()Landroid/view/View;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    const/4 v2, 0x0

    .line 25
    if-nez v1, :cond_0

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇o〇Oo88()Z

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    if-eqz v3, :cond_1

    .line 33
    .line 34
    const/4 v3, 0x4

    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const/4 v3, 0x0

    .line 37
    :goto_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 38
    .line 39
    .line 40
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o8oO〇()Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    const/4 v4, 0x1

    .line 49
    if-eqz v3, :cond_2

    .line 50
    .line 51
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    if-nez v3, :cond_2

    .line 56
    .line 57
    const/4 v3, 0x1

    .line 58
    goto :goto_2

    .line 59
    :cond_2
    const/4 v3, 0x0

    .line 60
    :goto_2
    xor-int/2addr v3, v4

    .line 61
    invoke-interface {v1, v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇808〇(Z)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    const/4 v3, 0x0

    .line 69
    invoke-interface {v1, v4, v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O(ZLcom/intsig/camscanner/capture/CaptureMode;)V

    .line 70
    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoo80oO:Landroid/view/View;

    .line 73
    .line 74
    if-nez v1, :cond_1d

    .line 75
    .line 76
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    if-eqz v1, :cond_3

    .line 81
    .line 82
    const v5, 0x7f0a1142

    .line 83
    .line 84
    .line 85
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    check-cast v1, Landroid/view/ViewStub;

    .line 90
    .line 91
    goto :goto_3

    .line 92
    :cond_3
    move-object v1, v3

    .line 93
    :goto_3
    if-nez v1, :cond_4

    .line 94
    .line 95
    goto :goto_4

    .line 96
    :cond_4
    invoke-virtual {v1, v2}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 97
    .line 98
    .line 99
    :goto_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 100
    .line 101
    .line 102
    move-result-object v1

    .line 103
    if-eqz v1, :cond_5

    .line 104
    .line 105
    const v5, 0x7f0a0355

    .line 106
    .line 107
    .line 108
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    goto :goto_5

    .line 113
    :cond_5
    move-object v1, v3

    .line 114
    :goto_5
    iput-object v1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoo80oO:Landroid/view/View;

    .line 115
    .line 116
    if-eqz v1, :cond_6

    .line 117
    .line 118
    const/high16 v5, -0x67000000

    .line 119
    .line 120
    invoke-virtual {v1, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 121
    .line 122
    .line 123
    :cond_6
    sget-object v1, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;

    .line 124
    .line 125
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->Oo08()Ljava/util/List;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇080OO8〇0(Ljava/util/List;)I

    .line 130
    .line 131
    .line 132
    move-result v5

    .line 133
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 134
    .line 135
    .line 136
    move-result-object v6

    .line 137
    check-cast v6, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 138
    .line 139
    iput-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 140
    .line 141
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 142
    .line 143
    .line 144
    move-result-object v6

    .line 145
    if-eqz v6, :cond_7

    .line 146
    .line 147
    const v7, 0x7f0a0358

    .line 148
    .line 149
    .line 150
    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 151
    .line 152
    .line 153
    move-result-object v6

    .line 154
    check-cast v6, Landroid/widget/ImageView;

    .line 155
    .line 156
    goto :goto_6

    .line 157
    :cond_7
    move-object v6, v3

    .line 158
    :goto_6
    iput-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8〇o〇88:Landroid/widget/ImageView;

    .line 159
    .line 160
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 161
    .line 162
    .line 163
    move-result-object v6

    .line 164
    if-eqz v6, :cond_8

    .line 165
    .line 166
    const v7, 0x7f0a11b2

    .line 167
    .line 168
    .line 169
    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 170
    .line 171
    .line 172
    move-result-object v6

    .line 173
    check-cast v6, Landroid/widget/TextView;

    .line 174
    .line 175
    goto :goto_7

    .line 176
    :cond_8
    move-object v6, v3

    .line 177
    :goto_7
    iput-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O〇O:Landroid/widget/TextView;

    .line 178
    .line 179
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 180
    .line 181
    .line 182
    move-result-object v6

    .line 183
    if-eqz v6, :cond_9

    .line 184
    .line 185
    const v7, 0x7f0a0a65

    .line 186
    .line 187
    .line 188
    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 189
    .line 190
    .line 191
    move-result-object v6

    .line 192
    goto :goto_8

    .line 193
    :cond_9
    move-object v6, v3

    .line 194
    :goto_8
    iput-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOo〇08〇:Landroid/view/View;

    .line 195
    .line 196
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 197
    .line 198
    .line 199
    move-result-object v6

    .line 200
    if-eqz v6, :cond_a

    .line 201
    .line 202
    const v7, 0x7f0a1a38

    .line 203
    .line 204
    .line 205
    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 206
    .line 207
    .line 208
    move-result-object v6

    .line 209
    goto :goto_9

    .line 210
    :cond_a
    move-object v6, v3

    .line 211
    :goto_9
    iput-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇80O8o8O〇:Landroid/view/View;

    .line 212
    .line 213
    new-array v6, v4, [Landroid/view/View;

    .line 214
    .line 215
    iget-object v7, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOo〇08〇:Landroid/view/View;

    .line 216
    .line 217
    aput-object v7, v6, v2

    .line 218
    .line 219
    invoke-virtual {p0, v6}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8O0O808〇([Landroid/view/View;)V

    .line 220
    .line 221
    .line 222
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 223
    .line 224
    .line 225
    move-result-object v6

    .line 226
    if-eqz v6, :cond_b

    .line 227
    .line 228
    const v7, 0x7f0a0ba6

    .line 229
    .line 230
    .line 231
    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 232
    .line 233
    .line 234
    move-result-object v6

    .line 235
    goto :goto_a

    .line 236
    :cond_b
    move-object v6, v3

    .line 237
    :goto_a
    iput-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O8oOo0:Landroid/view/View;

    .line 238
    .line 239
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 240
    .line 241
    .line 242
    move-result-object v6

    .line 243
    if-eqz v6, :cond_c

    .line 244
    .line 245
    const v7, 0x7f0a0356

    .line 246
    .line 247
    .line 248
    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 249
    .line 250
    .line 251
    move-result-object v6

    .line 252
    check-cast v6, Landroid/widget/TextView;

    .line 253
    .line 254
    goto :goto_b

    .line 255
    :cond_c
    move-object v6, v3

    .line 256
    :goto_b
    iput-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇o88:Landroid/widget/TextView;

    .line 257
    .line 258
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 259
    .line 260
    .line 261
    move-result-object v6

    .line 262
    if-eqz v6, :cond_d

    .line 263
    .line 264
    const v7, 0x7f0a038a

    .line 265
    .line 266
    .line 267
    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 268
    .line 269
    .line 270
    move-result-object v6

    .line 271
    goto :goto_c

    .line 272
    :cond_d
    move-object v6, v3

    .line 273
    :goto_c
    iput-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o8O:Landroid/view/View;

    .line 274
    .line 275
    iget-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 276
    .line 277
    if-eqz v6, :cond_16

    .line 278
    .line 279
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->Oooo8o0〇()Z

    .line 280
    .line 281
    .line 282
    move-result v7

    .line 283
    if-eqz v7, :cond_f

    .line 284
    .line 285
    iget-object v7, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o8O:Landroid/view/View;

    .line 286
    .line 287
    if-nez v7, :cond_e

    .line 288
    .line 289
    goto :goto_d

    .line 290
    :cond_e
    invoke-virtual {v7, v2}, Landroid/view/View;->setVisibility(I)V

    .line 291
    .line 292
    .line 293
    goto :goto_d

    .line 294
    :cond_f
    iget-object v7, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o8O:Landroid/view/View;

    .line 295
    .line 296
    if-nez v7, :cond_10

    .line 297
    .line 298
    goto :goto_d

    .line 299
    :cond_10
    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 300
    .line 301
    .line 302
    :goto_d
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 303
    .line 304
    .line 305
    move-result-object v7

    .line 306
    const v8, 0x7f1310bf

    .line 307
    .line 308
    .line 309
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 310
    .line 311
    .line 312
    move-result-object v7

    .line 313
    const-string v8, "activity.getString(R.str\u2026cs_619_button_learn_more)"

    .line 314
    .line 315
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 316
    .line 317
    .line 318
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 319
    .line 320
    .line 321
    move-result-object v8

    .line 322
    iget v9, v6, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateDesciptionRid:I

    .line 323
    .line 324
    new-array v10, v4, [Ljava/lang/Object;

    .line 325
    .line 326
    aput-object v7, v10, v2

    .line 327
    .line 328
    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 329
    .line 330
    .line 331
    move-result-object v8

    .line 332
    const-string v9, "{\n                    ac\u2026rnMore)\n                }"

    .line 333
    .line 334
    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    .line 336
    .line 337
    goto :goto_e

    .line 338
    :catchall_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 339
    .line 340
    .line 341
    move-result-object v8

    .line 342
    iget v6, v6, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateDesciptionRid:I

    .line 343
    .line 344
    invoke-virtual {v8, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 345
    .line 346
    .line 347
    move-result-object v6

    .line 348
    new-instance v8, Ljava/lang/StringBuilder;

    .line 349
    .line 350
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 351
    .line 352
    .line 353
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    .line 355
    .line 356
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 357
    .line 358
    .line 359
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 360
    .line 361
    .line 362
    move-result-object v8

    .line 363
    :goto_e
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 364
    .line 365
    .line 366
    move-result-object v6

    .line 367
    const v9, 0x7f0601ee

    .line 368
    .line 369
    .line 370
    invoke-static {v6, v9}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 371
    .line 372
    .line 373
    move-result v6

    .line 374
    new-instance v9, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$showCertificateMenuNew$1$1;

    .line 375
    .line 376
    invoke-direct {v9, p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$showCertificateMenuNew$1$1;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 377
    .line 378
    .line 379
    invoke-static {v8, v7, v6, v2, v9}, Lcom/intsig/comm/util/StringUtilDelegate;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;IZLandroid/text/style/ClickableSpan;)Landroid/text/SpannableString;

    .line 380
    .line 381
    .line 382
    move-result-object v6

    .line 383
    if-eqz v6, :cond_14

    .line 384
    .line 385
    iget-object v7, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇o88:Landroid/widget/TextView;

    .line 386
    .line 387
    if-nez v7, :cond_11

    .line 388
    .line 389
    goto :goto_f

    .line 390
    :cond_11
    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setHighlightColor(I)V

    .line 391
    .line 392
    .line 393
    :goto_f
    iget-object v7, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇o88:Landroid/widget/TextView;

    .line 394
    .line 395
    if-nez v7, :cond_12

    .line 396
    .line 397
    goto :goto_10

    .line 398
    :cond_12
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    .line 399
    .line 400
    .line 401
    move-result-object v9

    .line 402
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 403
    .line 404
    .line 405
    :goto_10
    iget-object v7, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇o88:Landroid/widget/TextView;

    .line 406
    .line 407
    if-nez v7, :cond_13

    .line 408
    .line 409
    goto :goto_11

    .line 410
    :cond_13
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 411
    .line 412
    .line 413
    :goto_11
    sget-object v6, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 414
    .line 415
    goto :goto_12

    .line 416
    :cond_14
    move-object v6, v3

    .line 417
    :goto_12
    if-nez v6, :cond_16

    .line 418
    .line 419
    iget-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇o88:Landroid/widget/TextView;

    .line 420
    .line 421
    if-nez v6, :cond_15

    .line 422
    .line 423
    goto :goto_13

    .line 424
    :cond_15
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 425
    .line 426
    .line 427
    :cond_16
    :goto_13
    iget-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 428
    .line 429
    if-eqz v6, :cond_17

    .line 430
    .line 431
    iget v6, v6, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 432
    .line 433
    goto :goto_14

    .line 434
    :cond_17
    const/4 v6, 0x0

    .line 435
    :goto_14
    invoke-static {v6}, Lcom/intsig/camscanner/capture/certificates/model/CertificateCaptureFactory;->〇080(I)Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 436
    .line 437
    .line 438
    move-result-object v6

    .line 439
    invoke-virtual {p0, v6}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇80O8o8O〇(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)V

    .line 440
    .line 441
    .line 442
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 443
    .line 444
    .line 445
    move-result-object v6

    .line 446
    if-eqz v6, :cond_18

    .line 447
    .line 448
    const v7, 0x7f0a0357

    .line 449
    .line 450
    .line 451
    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 452
    .line 453
    .line 454
    move-result-object v6

    .line 455
    check-cast v6, Landroid/widget/TextView;

    .line 456
    .line 457
    goto :goto_15

    .line 458
    :cond_18
    move-object v6, v3

    .line 459
    :goto_15
    iput-object v6, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oO〇oo:Landroid/widget/TextView;

    .line 460
    .line 461
    if-eqz v6, :cond_19

    .line 462
    .line 463
    new-instance v7, LO〇0〇o808〇/〇0〇O0088o;

    .line 464
    .line 465
    invoke-direct {v7, p0}, LO〇0〇o808〇/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 466
    .line 467
    .line 468
    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 469
    .line 470
    .line 471
    :cond_19
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 472
    .line 473
    .line 474
    move-result-object v6

    .line 475
    if-eqz v6, :cond_1a

    .line 476
    .line 477
    const v7, 0x7f0a0f11

    .line 478
    .line 479
    .line 480
    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 481
    .line 482
    .line 483
    move-result-object v6

    .line 484
    check-cast v6, Landroidx/recyclerview/widget/RecyclerView;

    .line 485
    .line 486
    goto :goto_16

    .line 487
    :cond_1a
    move-object v6, v3

    .line 488
    :goto_16
    if-eqz v6, :cond_1c

    .line 489
    .line 490
    iget-boolean v7, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoO8OO〇:Z

    .line 491
    .line 492
    if-nez v7, :cond_1b

    .line 493
    .line 494
    const v7, 0x7f0601e5

    .line 495
    .line 496
    .line 497
    const/high16 v8, 0x3f000000    # 0.5f

    .line 498
    .line 499
    invoke-static {v7, v8}, Lcom/intsig/utils/ColorUtil;->〇o〇(IF)I

    .line 500
    .line 501
    .line 502
    move-result v7

    .line 503
    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundColor(I)V

    .line 504
    .line 505
    .line 506
    :cond_1b
    new-instance v7, Lcom/intsig/camscanner/topic/view/SlowLayoutManager;

    .line 507
    .line 508
    invoke-direct {v7, p1, v2, v2}, Lcom/intsig/camscanner/topic/view/SlowLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 509
    .line 510
    .line 511
    invoke-virtual {v6, v7}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 512
    .line 513
    .line 514
    new-instance v7, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;

    .line 515
    .line 516
    invoke-direct {v7, p1, v1, v5}, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    .line 517
    .line 518
    .line 519
    iput-object v7, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Ooo8o:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;

    .line 520
    .line 521
    invoke-virtual {v6, v7}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 522
    .line 523
    .line 524
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Ooo8o:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;

    .line 525
    .line 526
    if-eqz p1, :cond_1c

    .line 527
    .line 528
    new-instance v1, LO〇0〇o808〇/OoO8;

    .line 529
    .line 530
    invoke-direct {v1, p0}, LO〇0〇o808〇/OoO8;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 531
    .line 532
    .line 533
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;->o800o8O(Lcom/intsig/camscanner/capture/certificates/CertificateAdapter$OnItemClickListener;)V

    .line 534
    .line 535
    .line 536
    :cond_1c
    iget-boolean p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoO8OO〇:Z

    .line 537
    .line 538
    if-nez p1, :cond_1d

    .line 539
    .line 540
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo80()V

    .line 541
    .line 542
    .line 543
    :cond_1d
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 544
    .line 545
    .line 546
    move-result-object p1

    .line 547
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->ooOO()Lcom/intsig/camscanner/capture/CaptureModeMenuManager;

    .line 548
    .line 549
    .line 550
    move-result-object p1

    .line 551
    if-eqz p1, :cond_1e

    .line 552
    .line 553
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 554
    .line 555
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/capture/CaptureModeMenuManager;->〇o(Lcom/intsig/camscanner/capture/CaptureMode;)V

    .line 556
    .line 557
    .line 558
    :cond_1e
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoo80oO:Landroid/view/View;

    .line 559
    .line 560
    if-eqz p1, :cond_1f

    .line 561
    .line 562
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 563
    .line 564
    .line 565
    move-result-object p1

    .line 566
    goto :goto_17

    .line 567
    :cond_1f
    move-object p1, v3

    .line 568
    :goto_17
    instance-of v1, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 569
    .line 570
    if-eqz v1, :cond_22

    .line 571
    .line 572
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 573
    .line 574
    .line 575
    move-result-object v1

    .line 576
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo〇O()Z

    .line 577
    .line 578
    .line 579
    move-result v1

    .line 580
    if-eqz v1, :cond_20

    .line 581
    .line 582
    move-object v1, p1

    .line 583
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 584
    .line 585
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 586
    .line 587
    .line 588
    move-result-object v5

    .line 589
    const/16 v6, 0x28

    .line 590
    .line 591
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 592
    .line 593
    .line 594
    move-result v5

    .line 595
    iput v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 596
    .line 597
    goto :goto_18

    .line 598
    :cond_20
    move-object v1, p1

    .line 599
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 600
    .line 601
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 602
    .line 603
    :goto_18
    iget-object v1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoo80oO:Landroid/view/View;

    .line 604
    .line 605
    if-nez v1, :cond_21

    .line 606
    .line 607
    goto :goto_19

    .line 608
    :cond_21
    invoke-virtual {v1, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 609
    .line 610
    .line 611
    :cond_22
    :goto_19
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 612
    .line 613
    .line 614
    move-result-object p1

    .line 615
    if-eqz p1, :cond_23

    .line 616
    .line 617
    const v1, 0x7f0a034d

    .line 618
    .line 619
    .line 620
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 621
    .line 622
    .line 623
    move-result-object p1

    .line 624
    move-object v3, p1

    .line 625
    check-cast v3, Lcom/airbnb/lottie/LottieAnimationView;

    .line 626
    .line 627
    :cond_23
    iput-object v3, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oooO888:Lcom/airbnb/lottie/LottieAnimationView;

    .line 628
    .line 629
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇OO8ooO8〇()Z

    .line 630
    .line 631
    .line 632
    move-result p1

    .line 633
    if-eqz p1, :cond_24

    .line 634
    .line 635
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇08O()V

    .line 636
    .line 637
    .line 638
    goto :goto_1a

    .line 639
    :cond_24
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇08〇o0O()V

    .line 640
    .line 641
    .line 642
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO8oO0o〇()I

    .line 643
    .line 644
    .line 645
    move-result p1

    .line 646
    const/4 v1, 0x7

    .line 647
    if-ne p1, v1, :cond_25

    .line 648
    .line 649
    iget-object v5, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oO〇oo:Landroid/widget/TextView;

    .line 650
    .line 651
    const v6, 0x3f666666    # 0.9f

    .line 652
    .line 653
    .line 654
    const-wide/16 v7, 0x7d0

    .line 655
    .line 656
    const/4 v9, -0x1

    .line 657
    const/4 v10, 0x0

    .line 658
    invoke-static/range {v5 .. v10}, Lcom/intsig/utils/AnimateUtils;->〇〇888(Landroid/view/View;FJILandroid/animation/Animator$AnimatorListener;)V

    .line 659
    .line 660
    .line 661
    :cond_25
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O8oOo0:Landroid/view/View;

    .line 662
    .line 663
    if-eqz p1, :cond_26

    .line 664
    .line 665
    new-instance v1, LO〇0〇o808〇/o800o8O;

    .line 666
    .line 667
    invoke-direct {v1, p0}, LO〇0〇o808〇/o800o8O;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 668
    .line 669
    .line 670
    invoke-virtual {p1, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 671
    .line 672
    .line 673
    :cond_26
    :goto_1a
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOo88OOo(Z)V

    .line 674
    .line 675
    .line 676
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇OO〇00〇0O()Z

    .line 677
    .line 678
    .line 679
    move-result p1

    .line 680
    if-eqz p1, :cond_28

    .line 681
    .line 682
    iget p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇0〇o:I

    .line 683
    .line 684
    if-lez p1, :cond_28

    .line 685
    .line 686
    invoke-static {p1}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->oO80(I)Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 687
    .line 688
    .line 689
    move-result-object p1

    .line 690
    if-eqz p1, :cond_27

    .line 691
    .line 692
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getNameId()I

    .line 693
    .line 694
    .line 695
    move-result p1

    .line 696
    goto :goto_1b

    .line 697
    :cond_27
    const p1, 0x7f130b8e

    .line 698
    .line 699
    .line 700
    :goto_1b
    new-instance v1, Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;

    .line 701
    .line 702
    new-instance v3, Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreItemModel;

    .line 703
    .line 704
    iget v5, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇0〇o:I

    .line 705
    .line 706
    const-string v6, ""

    .line 707
    .line 708
    invoke-direct {v3, p1, v6, v5, v6}, Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreItemModel;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    .line 709
    .line 710
    .line 711
    invoke-direct {v1, v3}, Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;-><init>(Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreItemModel;)V

    .line 712
    .line 713
    .line 714
    goto :goto_1c

    .line 715
    :cond_28
    iget p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇OOoooo:I

    .line 716
    .line 717
    invoke-static {p1}, Lcom/intsig/camscanner/capture/certificates/model/CertificateCaptureFactory;->〇080(I)Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 718
    .line 719
    .line 720
    move-result-object v1

    .line 721
    :goto_1c
    if-eqz v1, :cond_29

    .line 722
    .line 723
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O0oO0(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)V

    .line 724
    .line 725
    .line 726
    goto :goto_1f

    .line 727
    :cond_29
    iget-boolean p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇80o:Z

    .line 728
    .line 729
    if-nez p1, :cond_2d

    .line 730
    .line 731
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 732
    .line 733
    .line 734
    move-result-object p1

    .line 735
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇o8OO0()Z

    .line 736
    .line 737
    .line 738
    move-result p1

    .line 739
    if-eqz p1, :cond_2a

    .line 740
    .line 741
    goto :goto_1e

    .line 742
    :cond_2a
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 743
    .line 744
    if-eqz p1, :cond_2b

    .line 745
    .line 746
    const/16 v1, 0x3f6

    .line 747
    .line 748
    iget p1, p1, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 749
    .line 750
    if-ne v1, p1, :cond_2b

    .line 751
    .line 752
    goto :goto_1d

    .line 753
    :cond_2b
    const/4 v4, 0x0

    .line 754
    :goto_1d
    if-eqz v4, :cond_2c

    .line 755
    .line 756
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O0〇0(I)V

    .line 757
    .line 758
    .line 759
    goto :goto_1f

    .line 760
    :cond_2c
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O0〇0(I)V

    .line 761
    .line 762
    .line 763
    goto :goto_1f

    .line 764
    :cond_2d
    :goto_1e
    new-instance p1, Lcom/intsig/camscanner/capture/certificates/model/IDCardCapture;

    .line 765
    .line 766
    invoke-direct {p1}, Lcom/intsig/camscanner/capture/certificates/model/IDCardCapture;-><init>()V

    .line 767
    .line 768
    .line 769
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O0oO0(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)V

    .line 770
    .line 771
    .line 772
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 773
    .line 774
    .line 775
    move-result-object p1

    .line 776
    invoke-interface {p1, v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->oOo(Z)V

    .line 777
    .line 778
    .line 779
    iput-boolean v4, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇o〇88〇8:Z

    .line 780
    .line 781
    iput-boolean v2, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇80o:Z

    .line 782
    .line 783
    :goto_1f
    return-void
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
.end method

.method public static synthetic o〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoo80oO(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final o〇00O(ILandroid/content/Intent;)V
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "finishCertificateCapture type: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "CertificateCaptureScene"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 32
    .line 33
    .line 34
    move-result-wide v1

    .line 35
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->OO0o〇〇〇〇0(Landroid/content/Context;J)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 47
    .line 48
    .line 49
    move-result-wide v1

    .line 50
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->OO0o〇〇〇〇0(Landroid/content/Context;J)Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    if-nez v0, :cond_0

    .line 55
    .line 56
    const-string v0, ""

    .line 57
    .line 58
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oO〇()Z

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    const/4 v2, -0x1

    .line 63
    if-eqz v1, :cond_2

    .line 64
    .line 65
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 66
    .line 67
    .line 68
    move-result-object p2

    .line 69
    invoke-interface {p2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->ooo〇〇O〇()Z

    .line 70
    .line 71
    .line 72
    move-result p2

    .line 73
    if-eqz p2, :cond_1

    .line 74
    .line 75
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    invoke-virtual {p1, v2}, Landroid/app/Activity;->setResult(I)V

    .line 84
    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->ooo0〇O88O(I)V

    .line 88
    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_2
    invoke-static {v0}, Lcom/intsig/camscanner/certificate_package/util/CertificateDBUtil;->〇80〇808〇O(Ljava/lang/String;)Z

    .line 92
    .line 93
    .line 94
    move-result v0

    .line 95
    if-eqz v0, :cond_3

    .line 96
    .line 97
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o8〇OO0〇0o()V

    .line 98
    .line 99
    .line 100
    goto :goto_0

    .line 101
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o80ooO()Z

    .line 102
    .line 103
    .line 104
    move-result v0

    .line 105
    if-eqz v0, :cond_4

    .line 106
    .line 107
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    invoke-virtual {p1, v2}, Landroid/app/Activity;->setResult(I)V

    .line 116
    .line 117
    .line 118
    goto :goto_0

    .line 119
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->ooo0〇〇O(ILandroid/content/Intent;)V

    .line 120
    .line 121
    .line 122
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 123
    .line 124
    .line 125
    move-result-object p1

    .line 126
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OoO8()V

    .line 127
    .line 128
    .line 129
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    sget-object p2, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_CERTIFICATE:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 134
    .line 135
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V

    .line 136
    .line 137
    .line 138
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private final o〇00O0O〇o(Landroid/content/Context;)V
    .locals 4

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const-string v0, "null cannot be cast to non-null type android.app.ActivityManager"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast p1, Landroid/app/ActivityManager;

    .line 13
    .line 14
    new-instance v0, Landroid/app/ActivityManager$MemoryInfo;

    .line 15
    .line 16
    invoke-direct {v0}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, v0}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 20
    .line 21
    .line 22
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iget-wide v0, v0, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    .line 27
    .line 28
    invoke-virtual {p1}, Ljava/lang/Runtime;->maxMemory()J

    .line 29
    .line 30
    .line 31
    move-result-wide v2

    .line 32
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OoO8(JJ)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final o〇0〇()V
    .locals 1

    .line 1
    new-instance v0, LO〇0〇o808〇/〇〇8O0〇8;

    .line 2
    .line 3
    invoke-direct {v0, p0}, LO〇0〇o808〇/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic o〇0〇o(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ZZILjava/lang/Object;)V
    .locals 1

    .line 1
    and-int/lit8 p4, p3, 0x1

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    if-eqz p4, :cond_0

    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    :cond_0
    and-int/lit8 p3, p3, 0x2

    .line 8
    .line 9
    if-eqz p3, :cond_1

    .line 10
    .line 11
    const/4 p2, 0x0

    .line 12
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇OOoooo(ZZ)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
.end method

.method private final o〇O8OO(ZZ)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo〇O()Lcom/intsig/view/RotateImageTextButton;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    goto :goto_2

    .line 9
    :cond_0
    if-nez p1, :cond_2

    .line 10
    .line 11
    if-eqz p2, :cond_1

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    const/16 v2, 0x8

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_2
    :goto_0
    const/4 v2, 0x0

    .line 18
    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 19
    .line 20
    .line 21
    :goto_2
    if-eqz p2, :cond_3

    .line 22
    .line 23
    if-eqz p1, :cond_4

    .line 24
    .line 25
    :cond_3
    const/4 v1, 0x1

    .line 26
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo〇O()Lcom/intsig/view/RotateImageTextButton;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    if-nez p1, :cond_5

    .line 31
    .line 32
    goto :goto_3

    .line 33
    :cond_5
    invoke-virtual {p1, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 34
    .line 35
    .line 36
    :goto_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo〇O()Lcom/intsig/view/RotateImageTextButton;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    if-nez p1, :cond_6

    .line 41
    .line 42
    goto :goto_5

    .line 43
    :cond_6
    if-eqz v1, :cond_7

    .line 44
    .line 45
    const/high16 p2, 0x3f800000    # 1.0f

    .line 46
    .line 47
    goto :goto_4

    .line 48
    :cond_7
    const p2, 0x3e99999a    # 0.3f

    .line 49
    .line 50
    .line 51
    :goto_4
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 52
    .line 53
    .line 54
    :goto_5
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o〇OOo000([BLcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Lcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo0O0o8([BLcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Lcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private final o〇oO(Landroid/widget/ImageView;Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;)V
    .locals 10

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_7

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    goto/16 :goto_5

    .line 22
    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oooO888:Lcom/airbnb/lottie/LottieAnimationView;

    .line 24
    .line 25
    const/16 v1, 0x8

    .line 26
    .line 27
    if-nez v0, :cond_1

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31
    .line 32
    .line 33
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8〇o〇88:Landroid/widget/ImageView;

    .line 34
    .line 35
    const/4 v2, 0x0

    .line 36
    if-nez v0, :cond_2

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_2
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 40
    .line 41
    .line 42
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O8oOo0:Landroid/view/View;

    .line 43
    .line 44
    if-eqz v0, :cond_3

    .line 45
    .line 46
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    goto :goto_2

    .line 51
    :cond_3
    const/16 v0, 0x64

    .line 52
    .line 53
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    const/16 v4, 0x46

    .line 58
    .line 59
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    sub-int v7, v0, v3

    .line 64
    .line 65
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    const/16 v3, 0x10e

    .line 70
    .line 71
    invoke-static {v0, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 72
    .line 73
    .line 74
    move-result v8

    .line 75
    new-instance v0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$handleLoadDisplayImg$glideTarget$1;

    .line 76
    .line 77
    move-object v4, v0

    .line 78
    move-object v5, p1

    .line 79
    move-object v6, p0

    .line 80
    move-object v9, p2

    .line 81
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$handleLoadDisplayImg$glideTarget$1;-><init>(Landroid/widget/ImageView;Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;IILcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;)V

    .line 82
    .line 83
    .line 84
    if-eqz p2, :cond_4

    .line 85
    .line 86
    iget p1, p2, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 87
    .line 88
    const/16 v3, 0x3f2

    .line 89
    .line 90
    if-ne p1, v3, :cond_4

    .line 91
    .line 92
    const/4 v2, 0x1

    .line 93
    :cond_4
    const-string p1, ""

    .line 94
    .line 95
    const-wide/16 v3, 0x0

    .line 96
    .line 97
    if-eqz v2, :cond_5

    .line 98
    .line 99
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O〇80o08O()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 104
    .line 105
    .line 106
    move-result v2

    .line 107
    if-nez v2, :cond_5

    .line 108
    .line 109
    :try_start_0
    new-instance v2, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;

    .line 110
    .line 111
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O〇80o08O()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v5

    .line 115
    invoke-direct {v2, v5}, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;-><init>(Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->〇8o8o〇()Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v5

    .line 122
    const-string v6, "getLocaleRegion()"

    .line 123
    .line 124
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v5

    .line 131
    const-string v6, "this as java.lang.String).toLowerCase()"

    .line 132
    .line 133
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    invoke-virtual {v2, v5}, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->getUrlByCoun(Ljava/lang/String;)Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v5

    .line 140
    const-string v6, "cardPic.getUrlByCoun(Lan\u2026leRegion().toLowerCase())"

    .line 141
    .line 142
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 143
    .line 144
    .line 145
    :try_start_1
    iget-wide v3, v2, Lcom/intsig/tsapp/sync/AppConfigJson$CardPic;->upload_time:J
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 146
    .line 147
    goto :goto_4

    .line 148
    :catch_0
    move-exception p1

    .line 149
    goto :goto_3

    .line 150
    :catch_1
    move-exception v2

    .line 151
    move-object v5, p1

    .line 152
    move-object p1, v2

    .line 153
    :goto_3
    const-string v2, "CertificateCaptureScene"

    .line 154
    .line 155
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 156
    .line 157
    .line 158
    :goto_4
    move-object p1, v5

    .line 159
    :cond_5
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 160
    .line 161
    .line 162
    move-result v2

    .line 163
    const v5, 0x7f060669

    .line 164
    .line 165
    .line 166
    if-eqz v2, :cond_6

    .line 167
    .line 168
    if-eqz p2, :cond_7

    .line 169
    .line 170
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 171
    .line 172
    .line 173
    move-result-object p1

    .line 174
    invoke-static {p1}, Lcom/bumptech/glide/Glide;->oo88o8O(Landroidx/fragment/app/FragmentActivity;)Lcom/bumptech/glide/RequestManager;

    .line 175
    .line 176
    .line 177
    move-result-object p1

    .line 178
    iget p2, p2, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateRidBig:I

    .line 179
    .line 180
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 181
    .line 182
    .line 183
    move-result-object p2

    .line 184
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/RequestManager;->Oooo8o0〇(Ljava/lang/Integer;)Lcom/bumptech/glide/RequestBuilder;

    .line 185
    .line 186
    .line 187
    move-result-object p1

    .line 188
    sget-object p2, Lcom/bumptech/glide/load/engine/DiskCacheStrategy;->〇o00〇〇Oo:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;

    .line 189
    .line 190
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/request/BaseRequestOptions;->oO80(Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 191
    .line 192
    .line 193
    move-result-object p1

    .line 194
    check-cast p1, Lcom/bumptech/glide/RequestBuilder;

    .line 195
    .line 196
    new-instance p2, Lcom/bumptech/glide/request/RequestOptions;

    .line 197
    .line 198
    invoke-direct {p2}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 199
    .line 200
    .line 201
    new-instance v2, Lcom/bumptech/glide/load/resource/bitmap/RoundedCorners;

    .line 202
    .line 203
    invoke-direct {v2, v1}, Lcom/bumptech/glide/load/resource/bitmap/RoundedCorners;-><init>(I)V

    .line 204
    .line 205
    .line 206
    invoke-virtual {p2, v2}, Lcom/bumptech/glide/request/BaseRequestOptions;->O0O8OO088(Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 207
    .line 208
    .line 209
    move-result-object p2

    .line 210
    check-cast p2, Lcom/bumptech/glide/request/RequestOptions;

    .line 211
    .line 212
    invoke-virtual {p2}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇80〇808〇O()Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 213
    .line 214
    .line 215
    move-result-object p2

    .line 216
    check-cast p2, Lcom/bumptech/glide/request/RequestOptions;

    .line 217
    .line 218
    invoke-virtual {p2, v5}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇80(I)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 219
    .line 220
    .line 221
    move-result-object p2

    .line 222
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 223
    .line 224
    .line 225
    move-result-object p1

    .line 226
    invoke-virtual {p1, v0}, Lcom/bumptech/glide/RequestBuilder;->O〇0(Lcom/bumptech/glide/request/target/Target;)Lcom/bumptech/glide/request/target/Target;

    .line 227
    .line 228
    .line 229
    move-result-object p1

    .line 230
    check-cast p1, Lcom/bumptech/glide/request/target/CustomTarget;

    .line 231
    .line 232
    goto :goto_5

    .line 233
    :cond_6
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 234
    .line 235
    .line 236
    move-result-object p2

    .line 237
    invoke-static {p2}, Lcom/bumptech/glide/Glide;->oo88o8O(Landroidx/fragment/app/FragmentActivity;)Lcom/bumptech/glide/RequestManager;

    .line 238
    .line 239
    .line 240
    move-result-object p2

    .line 241
    invoke-virtual {p2, p1}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 242
    .line 243
    .line 244
    move-result-object p1

    .line 245
    new-instance p2, Lcom/bumptech/glide/request/RequestOptions;

    .line 246
    .line 247
    invoke-direct {p2}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 248
    .line 249
    .line 250
    new-instance v2, Lcom/bumptech/glide/load/resource/bitmap/RoundedCorners;

    .line 251
    .line 252
    invoke-direct {v2, v1}, Lcom/bumptech/glide/load/resource/bitmap/RoundedCorners;-><init>(I)V

    .line 253
    .line 254
    .line 255
    invoke-virtual {p2, v2}, Lcom/bumptech/glide/request/BaseRequestOptions;->O0O8OO088(Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 256
    .line 257
    .line 258
    move-result-object p2

    .line 259
    check-cast p2, Lcom/bumptech/glide/request/RequestOptions;

    .line 260
    .line 261
    invoke-virtual {p2}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇80〇808〇O()Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 262
    .line 263
    .line 264
    move-result-object p2

    .line 265
    check-cast p2, Lcom/bumptech/glide/request/RequestOptions;

    .line 266
    .line 267
    invoke-virtual {p2, v5}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇80(I)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 268
    .line 269
    .line 270
    move-result-object p2

    .line 271
    check-cast p2, Lcom/bumptech/glide/request/RequestOptions;

    .line 272
    .line 273
    new-instance v1, Lcom/bumptech/glide/signature/ObjectKey;

    .line 274
    .line 275
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 276
    .line 277
    .line 278
    move-result-object v2

    .line 279
    invoke-direct {v1, v2}, Lcom/bumptech/glide/signature/ObjectKey;-><init>(Ljava/lang/Object;)V

    .line 280
    .line 281
    .line 282
    invoke-virtual {p2, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇0(Lcom/bumptech/glide/load/Key;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 283
    .line 284
    .line 285
    move-result-object p2

    .line 286
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 287
    .line 288
    .line 289
    move-result-object p1

    .line 290
    invoke-virtual {p1, v0}, Lcom/bumptech/glide/RequestBuilder;->O〇0(Lcom/bumptech/glide/request/target/Target;)Lcom/bumptech/glide/request/target/Target;

    .line 291
    .line 292
    .line 293
    :cond_7
    :goto_5
    return-void
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private final o〇o〇Oo88()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8O〇88oO0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-nez v0, :cond_2

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->ooO()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-gtz v0, :cond_1

    .line 22
    .line 23
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇〇00:Z

    .line 24
    .line 25
    if-nez v0, :cond_1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    const/4 v1, 0x0

    .line 29
    :cond_2
    :goto_0
    return v1
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇00O0(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOo〇08〇:Landroid/view/View;

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v1, 0x0

    .line 12
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 13
    .line 14
    .line 15
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇o88:Landroid/widget/TextView;

    .line 16
    .line 17
    const/4 v1, 0x4

    .line 18
    if-nez v0, :cond_1

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 22
    .line 23
    .line 24
    :goto_1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇80O8o8O〇:Landroid/view/View;

    .line 25
    .line 26
    if-nez p0, :cond_2

    .line 27
    .line 28
    goto :goto_2

    .line 29
    :cond_2
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 30
    .line 31
    .line 32
    :goto_2
    return-void
    .line 33
    .line 34
.end method

.method private final 〇080OO8〇0(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;",
            ">;)I"
        }
    .end annotation

    .line 1
    const/4 v0, -0x1

    .line 2
    if-eqz p1, :cond_3

    .line 3
    .line 4
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    goto :goto_1

    .line 11
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 26
    .line 27
    const/4 v1, 0x0

    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const/4 v1, 0x0

    .line 30
    move-object v0, v1

    .line 31
    const/4 v1, -0x1

    .line 32
    :goto_0
    if-eqz v0, :cond_2

    .line 33
    .line 34
    iget v0, v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 35
    .line 36
    const/16 v2, 0x3f5

    .line 37
    .line 38
    if-ne v0, v2, :cond_2

    .line 39
    .line 40
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-eqz v0, :cond_2

    .line 45
    .line 46
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    check-cast v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 51
    .line 52
    add-int/lit8 v1, v1, 0x1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_2
    return v1

    .line 56
    :cond_3
    :goto_1
    return v0
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private static final 〇088O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Z)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8o〇O0:Landroid/view/View;

    .line 7
    .line 8
    const/4 v1, 0x4

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 13
    .line 14
    .line 15
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o8oO〇()Landroid/view/View;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    goto :goto_1

    .line 22
    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 23
    .line 24
    .line 25
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const/4 v1, 0x1

    .line 30
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇808〇(Z)V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o8O()V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo0O0o8:Landroid/view/View;

    .line 37
    .line 38
    if-nez v0, :cond_2

    .line 39
    .line 40
    goto :goto_2

    .line 41
    :cond_2
    const/16 v1, 0x8

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 44
    .line 45
    .line 46
    :goto_2
    if-eqz p1, :cond_3

    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 49
    .line 50
    .line 51
    move-result-object p0

    .line 52
    invoke-interface {p0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OoO8()V

    .line 53
    .line 54
    .line 55
    :cond_3
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇08O8o8(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇oOoo〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final 〇08O〇00〇o(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;I)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->Oooo8o0〇(I)V

    .line 11
    .line 12
    .line 13
    const/4 p1, 0x1

    .line 14
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇O〇(Z)V

    .line 15
    .line 16
    .line 17
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Ooo8o()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 21
    .line 22
    .line 23
    move-result-object p0

    .line 24
    invoke-interface {p0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O880oOO08()V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇08〇0〇o〇8(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oO〇8O8oOo(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇08〇o0O()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/16 v1, 0x104

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oooO888:Lcom/airbnb/lottie/LottieAnimationView;

    .line 12
    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v1, 0x0

    .line 21
    :goto_0
    instance-of v2, v1, Landroid/view/ViewGroup;

    .line 22
    .line 23
    if-eqz v2, :cond_1

    .line 24
    .line 25
    check-cast v1, Landroid/view/ViewGroup;

    .line 26
    .line 27
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 32
    .line 33
    :cond_1
    return-void
.end method

.method private final 〇0O()Lcom/bumptech/glide/request/RequestOptions;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇O80〇0o:Lcom/bumptech/glide/request/RequestOptions;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 8
    .line 9
    .line 10
    sget-object v1, Lcom/bumptech/glide/load/engine/DiskCacheStrategy;->〇o00〇〇Oo:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->oO80(Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->O8O〇(Z)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇o〇()Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 30
    .line 31
    new-instance v7, Lcom/intsig/camscanner/util/GlideRoundTransform;

    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    const/4 v2, 0x2

    .line 38
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    const/4 v3, 0x1

    .line 43
    const/4 v4, 0x1

    .line 44
    const/4 v5, 0x1

    .line 45
    const/4 v6, 0x1

    .line 46
    move-object v1, v7

    .line 47
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/util/GlideRoundTransform;-><init>(IZZZZ)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0, v7}, Lcom/bumptech/glide/request/BaseRequestOptions;->O0O8OO088(Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 55
    .line 56
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇O80〇0o:Lcom/bumptech/glide/request/RequestOptions;

    .line 57
    .line 58
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇O80〇0o:Lcom/bumptech/glide/request/RequestOptions;

    .line 59
    .line 60
    const-string v1, "null cannot be cast to non-null type com.bumptech.glide.request.RequestOptions"

    .line 61
    .line 62
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    return-object v0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static synthetic 〇0O00oO(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇0O〇O00O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 4

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x0

    .line 11
    const/4 v2, -0x1

    .line 12
    const-string v3, "\u6b63\u5728\u62fc\u56fe"

    .line 13
    .line 14
    invoke-static {v0, v3, v1, v2}, Lcom/intsig/utils/DialogUtils;->O8(Landroid/content/Context;Ljava/lang/String;ZI)Lcom/intsig/app/BaseProgressDialog;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇〇0:Landroid/app/Dialog;

    .line 19
    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 23
    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇0ooOOo()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 2
    .line 3
    if-eqz v0, :cond_6

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO0880O:Landroid/widget/FrameLayout;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_3

    .line 10
    :cond_0
    const/16 v0, 0x8

    .line 11
    .line 12
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O0〇0(I)V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O8oOo0(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->ooOO()Lcom/intsig/camscanner/capture/CaptureModeMenuManager;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const/4 v2, 0x0

    .line 28
    if-eqz v1, :cond_2

    .line 29
    .line 30
    sget-object v3, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 31
    .line 32
    iget-object v4, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 33
    .line 34
    if-eqz v4, :cond_1

    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 37
    .line 38
    .line 39
    move-result-object v5

    .line 40
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->O8(Landroid/content/Context;)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v4

    .line 44
    goto :goto_0

    .line 45
    :cond_1
    move-object v4, v2

    .line 46
    :goto_0
    invoke-virtual {v1, v3, v4}, Lcom/intsig/camscanner/capture/CaptureModeMenuManager;->〇80(Lcom/intsig/camscanner/capture/CaptureMode;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    :cond_2
    const/4 v1, 0x2

    .line 50
    const/4 v3, 0x1

    .line 51
    invoke-static {p0, v3, v0, v1, v2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0〇0(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ZZILjava/lang/Object;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o8oO〇()Landroid/view/View;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    if-nez v1, :cond_3

    .line 59
    .line 60
    goto :goto_2

    .line 61
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇o〇Oo88()Z

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    if-eqz v2, :cond_4

    .line 66
    .line 67
    const/4 v2, 0x4

    .line 68
    goto :goto_1

    .line 69
    :cond_4
    const/4 v2, 0x0

    .line 70
    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 71
    .line 72
    .line 73
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o8oO〇()Landroid/view/View;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    if-eqz v2, :cond_5

    .line 82
    .line 83
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    if-nez v2, :cond_5

    .line 88
    .line 89
    const/4 v0, 0x1

    .line 90
    :cond_5
    xor-int/2addr v0, v3

    .line 91
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇808〇(Z)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {p0, v3}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOo88OOo(Z)V

    .line 95
    .line 96
    .line 97
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 98
    .line 99
    if-eqz v0, :cond_6

    .line 100
    .line 101
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->getRotation()I

    .line 106
    .line 107
    .line 108
    move-result v1

    .line 109
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇〇808〇(I)V

    .line 110
    .line 111
    .line 112
    :cond_6
    :goto_3
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method static synthetic 〇0〇0(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ZZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇O8OO(ZZ)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
.end method

.method private final 〇800OO〇0O(Landroid/widget/ImageView;IILandroid/graphics/drawable/Drawable;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    int-to-float p2, p2

    .line 6
    const/high16 v1, 0x3f800000    # 1.0f

    .line 7
    .line 8
    mul-float p2, p2, v1

    .line 9
    .line 10
    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    int-to-float v2, v2

    .line 15
    div-float/2addr p2, v2

    .line 16
    int-to-float p3, p3

    .line 17
    mul-float p3, p3, v1

    .line 18
    .line 19
    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    int-to-float v1, v1

    .line 24
    div-float/2addr p3, v1

    .line 25
    invoke-static {p2, p3}, Ljava/lang/Math;->min(FF)F

    .line 26
    .line 27
    .line 28
    move-result p2

    .line 29
    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    .line 30
    .line 31
    .line 32
    move-result p3

    .line 33
    int-to-float p3, p3

    .line 34
    mul-float p3, p3, p2

    .line 35
    .line 36
    float-to-int p3, p3

    .line 37
    iput p3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 38
    .line 39
    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    .line 40
    .line 41
    .line 42
    move-result p3

    .line 43
    int-to-float p3, p3

    .line 44
    mul-float p2, p2, p3

    .line 45
    .line 46
    float-to-int p2, p2

    .line 47
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 48
    .line 49
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    instance-of p2, p1, Landroid/view/ViewGroup;

    .line 57
    .line 58
    if-eqz p2, :cond_0

    .line 59
    .line 60
    check-cast p1, Landroid/view/ViewGroup;

    .line 61
    .line 62
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    iget p2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 67
    .line 68
    iput p2, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 69
    .line 70
    iget p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 71
    .line 72
    iput p2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 73
    .line 74
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O8oOo0:Landroid/view/View;

    .line 75
    .line 76
    if-nez p1, :cond_1

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_1
    const/4 p2, 0x0

    .line 80
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 81
    .line 82
    .line 83
    :goto_0
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private static final 〇8〇80o(Ljava/lang/Runnable;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "$runnable"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private final 〇8〇OOoooo(ZZ)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "showExitCertificateDialog empty="

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const-string v1, "CertificateCaptureScene"

    .line 25
    .line 26
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    new-instance v0, LO〇0〇o808〇/〇O888o0o;

    .line 30
    .line 31
    invoke-direct {v0, p0, p1}, LO〇0〇o808〇/〇O888o0o;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Z)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 35
    .line 36
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    if-nez p1, :cond_2

    .line 41
    .line 42
    if-eqz p2, :cond_0

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_0
    iget-boolean p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoO8OO〇:Z

    .line 46
    .line 47
    if-eqz p1, :cond_1

    .line 48
    .line 49
    const/4 v2, 0x0

    .line 50
    sget-object v3, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$showExitCertificateDialog$1;->o0:Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$showExitCertificateDialog$1;

    .line 51
    .line 52
    new-instance v4, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$showExitCertificateDialog$2;

    .line 53
    .line 54
    invoke-direct {v4, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$showExitCertificateDialog$2;-><init>(Ljava/lang/Runnable;)V

    .line 55
    .line 56
    .line 57
    const/4 v5, 0x0

    .line 58
    const/16 v6, 0x9

    .line 59
    .line 60
    const/4 v7, 0x0

    .line 61
    move-object v1, p0

    .line 62
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O〇oO〇oo8o(Lcom/intsig/camscanner/capture/core/BaseCaptureScene;Lcom/intsig/app/AlertDialog;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Landroid/content/DialogInterface$OnDismissListener;ILjava/lang/Object;)V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_1
    new-instance p1, Lcom/intsig/app/AlertDialog$Builder;

    .line 67
    .line 68
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 69
    .line 70
    .line 71
    move-result-object p2

    .line 72
    invoke-direct {p1, p2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 73
    .line 74
    .line 75
    const p2, 0x7f131d10

    .line 76
    .line 77
    .line 78
    invoke-virtual {p1, p2}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    const p2, 0x7f130792

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1, p2}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    new-instance p2, LO〇0〇o808〇/oo88o8O;

    .line 90
    .line 91
    invoke-direct {p2}, LO〇0〇o808〇/oo88o8O;-><init>()V

    .line 92
    .line 93
    .line 94
    const v1, 0x7f13057e

    .line 95
    .line 96
    .line 97
    invoke-virtual {p1, v1, p2}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    new-instance p2, LO〇0〇o808〇/〇oo〇;

    .line 102
    .line 103
    invoke-direct {p2, v0}, LO〇0〇o808〇/〇oo〇;-><init>(Ljava/lang/Runnable;)V

    .line 104
    .line 105
    .line 106
    const v0, 0x7f130128

    .line 107
    .line 108
    .line 109
    invoke-virtual {p1, v0, p2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 118
    .line 119
    .line 120
    :goto_0
    return-void

    .line 121
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 122
    .line 123
    .line 124
    return-void
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private final 〇8〇o88()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const-string v1, "CertificateCaptureScene"

    .line 16
    .line 17
    const-string v2, "resetCertificateCaptureParamer updateTipShowState"

    .line 18
    .line 19
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/4 v1, 0x1

    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->Oooo8o0〇(I)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇O〇(Z)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->getRotation()I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇〇808〇(I)V

    .line 38
    .line 39
    .line 40
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo0O0o8:Landroid/view/View;

    .line 41
    .line 42
    if-nez v0, :cond_1

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    const/16 v1, 0x8

    .line 46
    .line 47
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 48
    .line 49
    .line 50
    :goto_0
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final 〇8〇oO〇〇8o()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Landroid/content/Intent;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    const-class v3, Lcom/intsig/camscanner/capture/certificates/CertificateModelMoreActivity;

    .line 12
    .line 13
    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 14
    .line 15
    .line 16
    const/16 v2, 0x320

    .line 17
    .line 18
    invoke-virtual {v0, v1, v2}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇8〇o〇8(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇00O0(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇O80〇oOo(Ljava/lang/String;Ljava/lang/String;[IIZZ)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
    .locals 11

    .line 1
    const/4 v6, 0x0

    .line 2
    const/4 v7, 0x1

    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O000()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const-wide/16 v0, -0x1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 21
    .line 22
    .line 23
    move-result-wide v0

    .line 24
    :goto_0
    move-wide v8, v0

    .line 25
    const/4 v10, 0x0

    .line 26
    move-object v0, p1

    .line 27
    move-object v1, p2

    .line 28
    move-object v2, p3

    .line 29
    move v3, p4

    .line 30
    move/from16 v4, p5

    .line 31
    .line 32
    move/from16 v5, p6

    .line 33
    .line 34
    invoke-static/range {v0 .. v10}, Lcom/intsig/camscanner/multiimageedit/util/MultiImageEditPageManagerUtil;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;[IIZZZZJZ)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    const-string v1, "createMultiImageEditMode\u2026          false\n        )"

    .line 39
    .line 40
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
.end method

.method private final 〇O8oOo0(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO0880O:Landroid/widget/FrameLayout;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 7
    .line 8
    .line 9
    :goto_0
    if-nez p1, :cond_1

    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    goto :goto_1

    .line 13
    :cond_1
    const/4 p1, 0x0

    .line 14
    :goto_1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇〇00:Z

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇O8〇OO〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O〇O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇OO0()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇OO8ooO8〇()Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO8oO0o〇()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, 0x8

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-ne v0, v1, :cond_1

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget v0, v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 16
    .line 17
    const/16 v3, 0x3ea

    .line 18
    .line 19
    if-ne v0, v3, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v0, 0x0

    .line 24
    :goto_0
    if-eqz v0, :cond_1

    .line 25
    .line 26
    const/4 v2, 0x1

    .line 27
    :cond_1
    return v2
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇OO〇00〇0O()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8O〇88oO0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->ooO()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇OOoooo:I

    .line 14
    .line 15
    const/16 v1, 0x3f3

    .line 16
    .line 17
    if-ne v0, v1, :cond_1

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_1
    const/4 v0, 0x0

    .line 22
    :goto_0
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇Oo(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0〇0:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O8〇o(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇O〇〇O8()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, 0x0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget v0, v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 8
    .line 9
    const/16 v3, 0x3f3

    .line 10
    .line 11
    if-ne v0, v3, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    if-eqz v0, :cond_1

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Ooo8o:Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/CertificateAdapter;->〇〇8O0〇8()Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 29
    .line 30
    iget v0, v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 31
    .line 32
    invoke-static {v0}, Lcom/intsig/camscanner/capture/certificates/model/CertificateCaptureFactory;->〇080(I)Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇80O8o8O〇(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)V

    .line 37
    .line 38
    .line 39
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 40
    .line 41
    if-eqz v0, :cond_2

    .line 42
    .line 43
    iget v3, v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 44
    .line 45
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO8(I)V

    .line 46
    .line 47
    .line 48
    new-array v1, v1, [Landroid/util/Pair;

    .line 49
    .line 50
    new-instance v3, Landroid/util/Pair;

    .line 51
    .line 52
    const-string v4, "type"

    .line 53
    .line 54
    const-string v5, "id_mode"

    .line 55
    .line 56
    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 57
    .line 58
    .line 59
    aput-object v3, v1, v2

    .line 60
    .line 61
    const-string v2, "CSScan"

    .line 62
    .line 63
    const-string v3, "scan_guide_start"

    .line 64
    .line 65
    invoke-static {v2, v3, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 66
    .line 67
    .line 68
    iget v0, v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 69
    .line 70
    invoke-static {v0}, Lcom/intsig/camscanner/capture/certificates/model/CertificateCaptureFactory;->〇080(I)Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇80O8o8O〇(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)V

    .line 75
    .line 76
    .line 77
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oO〇oo()V

    .line 78
    .line 79
    .line 80
    :cond_2
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private static final 〇o08(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "$this_run"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8〇o〇88()V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x2

    .line 10
    new-array p1, p1, [Landroid/util/Pair;

    .line 11
    .line 12
    new-instance v0, Landroid/util/Pair;

    .line 13
    .line 14
    const-string v1, "type"

    .line 15
    .line 16
    const-string v2, "id_mode"

    .line 17
    .line 18
    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 19
    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    aput-object v0, p1, v1

    .line 23
    .line 24
    new-instance v0, Landroid/util/Pair;

    .line 25
    .line 26
    iget-object p0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 27
    .line 28
    if-eqz p0, :cond_0

    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->oO80()Lcom/intsig/camscanner/purchase/entity/Function;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    if-eqz p0, :cond_0

    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/entity/Function;->toTrackerValue()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    goto :goto_0

    .line 41
    :cond_0
    const/4 p0, 0x0

    .line 42
    :goto_0
    const-string v1, "scheme"

    .line 43
    .line 44
    invoke-direct {v0, v1, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 45
    .line 46
    .line 47
    const/4 p0, 0x1

    .line 48
    aput-object v0, p1, p0

    .line 49
    .line 50
    const-string p0, "CSScan"

    .line 51
    .line 52
    const-string v0, "retake_photo"

    .line 53
    .line 54
    invoke-static {p0, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 55
    .line 56
    .line 57
    const-string p0, "CertificateCaptureScene"

    .line 58
    .line 59
    const-string p1, "llc_recapture_last_page click back to last Page"

    .line 60
    .line 61
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇o0O()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    const-string v6, "id_mode"

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->Oo08()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const v2, 0x7f13062c

    .line 12
    .line 13
    .line 14
    const/4 v3, -0x1

    .line 15
    const/4 v4, 0x0

    .line 16
    packed-switch v1, :pswitch_data_0

    .line 17
    .line 18
    .line 19
    :cond_0
    :pswitch_0
    move-object v7, v4

    .line 20
    :goto_0
    const/4 v5, -0x1

    .line 21
    goto/16 :goto_3

    .line 22
    .line 23
    :pswitch_1
    const-string v1, "one_page_id"

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :pswitch_2
    const v1, 0x7f1307cc

    .line 27
    .line 28
    .line 29
    const-string v2, "household_register_collage"

    .line 30
    .line 31
    move-object v7, v2

    .line 32
    const v5, 0x7f1307cc

    .line 33
    .line 34
    .line 35
    goto :goto_3

    .line 36
    :pswitch_3
    instance-of v1, v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;

    .line 37
    .line 38
    if-eqz v1, :cond_0

    .line 39
    .line 40
    move-object v1, v0

    .line 41
    check-cast v1, Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;

    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;->〇〇8O0〇8()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    goto :goto_1

    .line 48
    :pswitch_4
    const-string v1, "china_car"

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :pswitch_5
    const v1, 0x7f130628

    .line 52
    .line 53
    .line 54
    const-string v2, "bank_card"

    .line 55
    .line 56
    move-object v7, v2

    .line 57
    const v5, 0x7f130628

    .line 58
    .line 59
    .line 60
    goto :goto_3

    .line 61
    :pswitch_6
    const v1, 0x7f13062a

    .line 62
    .line 63
    .line 64
    const-string v2, "house"

    .line 65
    .line 66
    move-object v7, v2

    .line 67
    const v5, 0x7f13062a

    .line 68
    .line 69
    .line 70
    goto :goto_3

    .line 71
    :pswitch_7
    const-string v1, "china_driver"

    .line 72
    .line 73
    :goto_1
    move-object v7, v1

    .line 74
    goto :goto_0

    .line 75
    :pswitch_8
    const v1, 0x7f130629

    .line 76
    .line 77
    .line 78
    const-string v2, "business_license"

    .line 79
    .line 80
    move-object v7, v2

    .line 81
    const v5, 0x7f130629

    .line 82
    .line 83
    .line 84
    goto :goto_3

    .line 85
    :pswitch_9
    const-string v1, "oversea_idcard"

    .line 86
    .line 87
    goto :goto_2

    .line 88
    :pswitch_a
    const v1, 0x7f13062d

    .line 89
    .line 90
    .line 91
    const-string v2, "passport"

    .line 92
    .line 93
    move-object v7, v2

    .line 94
    const v5, 0x7f13062d

    .line 95
    .line 96
    .line 97
    goto :goto_3

    .line 98
    :pswitch_b
    const v1, 0x7f13062b

    .line 99
    .line 100
    .line 101
    const-string v2, "household_register"

    .line 102
    .line 103
    move-object v7, v2

    .line 104
    const v5, 0x7f13062b

    .line 105
    .line 106
    .line 107
    goto :goto_3

    .line 108
    :pswitch_c
    const-string v1, "id_card"

    .line 109
    .line 110
    :goto_2
    move-object v7, v1

    .line 111
    const v5, 0x7f13062c

    .line 112
    .line 113
    .line 114
    :goto_3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇080()Z

    .line 115
    .line 116
    .line 117
    move-result v1

    .line 118
    if-eqz v1, :cond_1

    .line 119
    .line 120
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->Oo08()I

    .line 121
    .line 122
    .line 123
    move-result v1

    .line 124
    const/16 v2, 0x9

    .line 125
    .line 126
    if-eq v1, v2, :cond_1

    .line 127
    .line 128
    const/4 v1, 0x1

    .line 129
    const/4 v3, 0x1

    .line 130
    goto :goto_4

    .line 131
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇o〇()I

    .line 132
    .line 133
    .line 134
    move-result v1

    .line 135
    move v3, v1

    .line 136
    :goto_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 137
    .line 138
    .line 139
    move-result-object v1

    .line 140
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇o〇()I

    .line 141
    .line 142
    .line 143
    move-result v2

    .line 144
    const/16 v4, 0x88

    .line 145
    .line 146
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/app/IntentUtil;->o〇〇0〇(Landroid/app/Activity;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    :cond_2
    return-void

    .line 150
    nop

    .line 151
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static synthetic 〇o8oO(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇o08(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇oO8O0〇〇O(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;[Landroid/net/Uri;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOO〇〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;[Landroid/net/Uri;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇oOoo〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object p0

    .line 12
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Ljava/lang/Number;

    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 25
    .line 26
    .line 27
    move-result-wide v0

    .line 28
    const/4 v2, 0x1

    .line 29
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil;->〇〇888(JZ)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    return-void
    .line 34
.end method

.method public static synthetic 〇oOo〇(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oo〇O〇80(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 7
    .line 8
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v1, 0x0

    .line 15
    :goto_0
    iput-boolean v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO〇〇:Z

    .line 16
    .line 17
    :try_start_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->clone()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    const-string v1, "null cannot be cast to non-null type com.intsig.camscanner.multiimageedit.model.MultiImageEditModel"

    .line 22
    .line 23
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    check-cast p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 27
    .line 28
    iput-object p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    .line 30
    goto :goto_1

    .line 31
    :catch_0
    move-exception p1

    .line 32
    const-string v1, "CertificateCaptureScene"

    .line 33
    .line 34
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 35
    .line 36
    .line 37
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0〇0:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 38
    .line 39
    if-eqz p1, :cond_1

    .line 40
    .line 41
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 42
    .line 43
    const-wide/16 v2, 0x0

    .line 44
    .line 45
    invoke-virtual {p1, v1, v2, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇008〇oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 46
    .line 47
    .line 48
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0〇0:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 49
    .line 50
    if-eqz p1, :cond_2

    .line 51
    .line 52
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇O00(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 53
    .line 54
    .line 55
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0〇0:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 56
    .line 57
    if-eqz p1, :cond_3

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O〇O〇oO()Landroidx/lifecycle/MutableLiveData;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    if-eqz p1, :cond_3

    .line 64
    .line 65
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 66
    .line 67
    invoke-virtual {p1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 68
    .line 69
    .line 70
    :cond_3
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private static final 〇o〇88〇8(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p0, "CertificateCaptureScene"

    .line 2
    .line 3
    const-string p1, "muti canceled"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string p0, "CSQuitScanWarning"

    .line 9
    .line 10
    const-string p1, "cancel"

    .line 11
    .line 12
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇o〇o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇o〇88〇8(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇〇08O()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oooO888:Lcom/airbnb/lottie/LottieAnimationView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 8
    .line 9
    .line 10
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oooO888:Lcom/airbnb/lottie/LottieAnimationView;

    .line 11
    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->〇O〇()V

    .line 15
    .line 16
    .line 17
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8〇o〇88:Landroid/widget/ImageView;

    .line 18
    .line 19
    if-nez v0, :cond_2

    .line 20
    .line 21
    goto :goto_1

    .line 22
    :cond_2
    const/4 v2, 0x4

    .line 23
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 24
    .line 25
    .line 26
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O〇O:Landroid/widget/TextView;

    .line 27
    .line 28
    if-nez v0, :cond_3

    .line 29
    .line 30
    goto :goto_2

    .line 31
    :cond_3
    const/16 v2, 0x8

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 34
    .line 35
    .line 36
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    const/16 v2, 0x118

    .line 41
    .line 42
    invoke-static {v0, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    iget-object v2, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8〇o〇88:Landroid/widget/ImageView;

    .line 47
    .line 48
    const/4 v3, 0x0

    .line 49
    if-eqz v2, :cond_4

    .line 50
    .line 51
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    goto :goto_3

    .line 56
    :cond_4
    move-object v2, v3

    .line 57
    :goto_3
    if-nez v2, :cond_5

    .line 58
    .line 59
    goto :goto_4

    .line 60
    :cond_5
    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 61
    .line 62
    :goto_4
    iget-object v2, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oooO888:Lcom/airbnb/lottie/LottieAnimationView;

    .line 63
    .line 64
    if-eqz v2, :cond_6

    .line 65
    .line 66
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 67
    .line 68
    .line 69
    move-result-object v3

    .line 70
    :cond_6
    instance-of v2, v3, Landroid/view/ViewGroup;

    .line 71
    .line 72
    if-eqz v2, :cond_7

    .line 73
    .line 74
    check-cast v3, Landroid/view/ViewGroup;

    .line 75
    .line 76
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 81
    .line 82
    :cond_7
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇O8oOo0:Landroid/view/View;

    .line 83
    .line 84
    if-nez v0, :cond_8

    .line 85
    .line 86
    goto :goto_5

    .line 87
    :cond_8
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 88
    .line 89
    .line 90
    :goto_5
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final 〇〇0O8ooO()V
    .locals 15

    .line 1
    const-string v0, "CertificateCaptureScene"

    .line 2
    .line 3
    const-string v1, "go2MultiCaptureFilterPreview"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->o〇0()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    const-string v3, "doc_id"

    .line 28
    .line 29
    const-wide/16 v4, -0x1

    .line 30
    .line 31
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 32
    .line 33
    .line 34
    move-result-wide v2

    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    invoke-interface {v4, v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo0oO〇O〇O(I)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 40
    .line 41
    .line 42
    move-result-object v6

    .line 43
    const-string v0, "captureControl.createParcelDocInfo(docType)"

    .line 44
    .line 45
    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇o8()Ljava/util/List;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->Oo〇o(Ljava/util/List;)[J

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iput-object v0, v6, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 61
    .line 62
    const-wide/16 v4, 0x0

    .line 63
    .line 64
    cmp-long v0, v2, v4

    .line 65
    .line 66
    if-gez v0, :cond_1

    .line 67
    .line 68
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 73
    .line 74
    .line 75
    move-result-wide v2

    .line 76
    cmp-long v0, v2, v4

    .line 77
    .line 78
    if-lez v0, :cond_1

    .line 79
    .line 80
    const/4 v1, 0x1

    .line 81
    :cond_1
    iput-boolean v1, v6, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO〇00〇8oO:Z

    .line 82
    .line 83
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->o〇8〇()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    iput-object v0, v6, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 92
    .line 93
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 94
    .line 95
    if-eqz v0, :cond_2

    .line 96
    .line 97
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇o〇()I

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    move v8, v0

    .line 102
    goto :goto_1

    .line 103
    :cond_2
    const/4 v0, -0x1

    .line 104
    const/4 v8, -0x1

    .line 105
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 106
    .line 107
    .line 108
    move-result-object v5

    .line 109
    const/4 v7, 0x0

    .line 110
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O0OO8〇0()Z

    .line 115
    .line 116
    .line 117
    move-result v9

    .line 118
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇0O〇Oo()Z

    .line 123
    .line 124
    .line 125
    move-result v10

    .line 126
    const/4 v11, 0x0

    .line 127
    const/4 v12, 0x0

    .line 128
    const/4 v13, 0x1

    .line 129
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo〇o()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v14

    .line 137
    invoke-static/range {v5 .. v14}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewActivity;->o0Oo(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;ZIZZLjava/util/ArrayList;Ljava/lang/String;ZLjava/lang/String;)Landroid/content/Intent;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o80ooO()Z

    .line 142
    .line 143
    .line 144
    move-result v1

    .line 145
    if-eqz v1, :cond_3

    .line 146
    .line 147
    const-string v1, "extra_certificatepageids"

    .line 148
    .line 149
    iget-object v2, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o00〇88〇08:Ljava/util/ArrayList;

    .line 150
    .line 151
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 152
    .line 153
    .line 154
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 155
    .line 156
    .line 157
    move-result-object v1

    .line 158
    const/16 v2, 0x321

    .line 159
    .line 160
    invoke-static {v1, v0, v2}, Lcom/intsig/utils/TransitionUtil;->〇o00〇〇Oo(Landroid/app/Activity;Landroid/content/Intent;I)V

    .line 161
    .line 162
    .line 163
    return-void
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static final synthetic 〇〇0o〇o8(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoO8OO〇:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic 〇〇8(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8o8O〇O(Ljava/lang/Runnable;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇〇O00〇8(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇08O〇00〇o(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇〇o0o(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇Oo(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final 〇〇o0〇8(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const/16 v1, 0x3f3

    .line 11
    .line 12
    iget v2, v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 13
    .line 14
    if-eq v1, v2, :cond_0

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O8〇o〇88:Landroid/widget/ImageView;

    .line 17
    .line 18
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇oO(Landroid/widget/ImageView;Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o〇()V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇〇o〇()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O〇O:Landroid/widget/TextView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o880:Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    if-eqz v1, :cond_1

    .line 10
    .line 11
    iget v1, v1, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 12
    .line 13
    const/16 v3, 0x3e9

    .line 14
    .line 15
    if-ne v1, v3, :cond_1

    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_1
    const/4 v1, 0x0

    .line 20
    :goto_0
    const/16 v3, 0x8

    .line 21
    .line 22
    if-eqz v1, :cond_4

    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO8oO0o〇()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O〇O:Landroid/widget/TextView;

    .line 29
    .line 30
    if-nez v1, :cond_2

    .line 31
    .line 32
    goto :goto_2

    .line 33
    :cond_2
    const/4 v4, 0x4

    .line 34
    if-ge v0, v4, :cond_3

    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_3
    const/16 v2, 0x8

    .line 38
    .line 39
    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 40
    .line 41
    .line 42
    goto :goto_2

    .line 43
    :cond_4
    if-nez v0, :cond_5

    .line 44
    .line 45
    goto :goto_2

    .line 46
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇OO8ooO8〇()Z

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    if-eqz v1, :cond_6

    .line 51
    .line 52
    const/16 v2, 0x8

    .line 53
    .line 54
    :cond_6
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 55
    .line 56
    .line 57
    :goto_2
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static synthetic 〇〇〇0880(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ILcom/intsig/camscanner/capture/common/CapturePreviewScaleData;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->OO(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ILcom/intsig/camscanner/capture/common/CapturePreviewScaleData;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private final 〇〇〇0o〇〇0(ZLcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 12

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o0O〇8o0O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "CertificateCaptureScene"

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string p1, "jumpToComponentPage isSavingPicture true"

    .line 10
    .line 11
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const p2, 0x7f130e22

    .line 19
    .line 20
    .line 21
    invoke-static {p1, p2}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 22
    .line 23
    .line 24
    return-void

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 26
    .line 27
    if-nez v0, :cond_1

    .line 28
    .line 29
    const-string p1, "mCertificateCapture == null"

    .line 30
    .line 31
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8O〇88oO0()Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-eqz v0, :cond_2

    .line 40
    .line 41
    new-instance p1, LO〇0〇o808〇/o〇O8〇〇o;

    .line 42
    .line 43
    invoke-direct {p1, p0}, LO〇0〇o808〇/o〇O8〇〇o;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8o8O〇O(Ljava/lang/Runnable;)V

    .line 47
    .line 48
    .line 49
    return-void

    .line 50
    :cond_2
    new-instance v4, Lkotlin/jvm/internal/Ref$ObjectRef;

    .line 51
    .line 52
    invoke-direct {v4}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    .line 53
    .line 54
    .line 55
    new-instance v0, Landroid/content/Intent;

    .line 56
    .line 57
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    const-class v3, Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity;

    .line 62
    .line 63
    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    .line 65
    .line 66
    iput-object v0, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 67
    .line 68
    sget-object v0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;

    .line 69
    .line 70
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->Oooo8o0〇()Z

    .line 71
    .line 72
    .line 73
    move-result v2

    .line 74
    if-eqz v2, :cond_3

    .line 75
    .line 76
    new-instance v2, Landroid/content/Intent;

    .line 77
    .line 78
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 79
    .line 80
    .line 81
    move-result-object v3

    .line 82
    const-class v5, Lcom/intsig/camscanner/autocomposite/CertificatePreviewActivity;

    .line 83
    .line 84
    invoke-direct {v2, v3, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    .line 86
    .line 87
    iput-object v2, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 88
    .line 89
    :cond_3
    iget-object v2, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 90
    .line 91
    const/4 v3, 0x0

    .line 92
    if-eqz v2, :cond_4

    .line 93
    .line 94
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇80〇808〇O()Lcom/intsig/camscanner/autocomposite/TemplateItem;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    move-object v5, v2

    .line 99
    goto :goto_0

    .line 100
    :cond_4
    move-object v5, v3

    .line 101
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 102
    .line 103
    const/4 v6, 0x0

    .line 104
    if-eqz v2, :cond_5

    .line 105
    .line 106
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->o〇0()I

    .line 107
    .line 108
    .line 109
    move-result v2

    .line 110
    goto :goto_1

    .line 111
    :cond_5
    const/4 v2, 0x0

    .line 112
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 113
    .line 114
    .line 115
    move-result-object v7

    .line 116
    invoke-interface {v7, v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo0oO〇O〇O(I)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    const-string v7, "captureControl.createParcelDocInfo(docType)"

    .line 121
    .line 122
    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    iget-object v7, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 126
    .line 127
    check-cast v7, Landroid/content/Intent;

    .line 128
    .line 129
    const-string v8, "extra_doc_info"

    .line 130
    .line 131
    invoke-virtual {v7, v8, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 132
    .line 133
    .line 134
    iget-object v7, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇o0〇8:Ljava/util/ArrayList;

    .line 135
    .line 136
    invoke-static {v7}, Lcom/intsig/camscanner/util/Util;->O00(Ljava/util/List;)[J

    .line 137
    .line 138
    .line 139
    move-result-object v7

    .line 140
    iput-object v7, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 141
    .line 142
    iget-object v7, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 143
    .line 144
    check-cast v7, Landroid/content/Intent;

    .line 145
    .line 146
    const-string v8, "extra_is_appendpage"

    .line 147
    .line 148
    const/4 v9, 0x1

    .line 149
    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 150
    .line 151
    .line 152
    iget-object v7, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 153
    .line 154
    check-cast v7, Landroid/content/Intent;

    .line 155
    .line 156
    const-string v8, "extra_need_change_page_order"

    .line 157
    .line 158
    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 159
    .line 160
    .line 161
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 162
    .line 163
    .line 164
    move-result-object v7

    .line 165
    invoke-virtual {v7}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 166
    .line 167
    .line 168
    move-result-object v7

    .line 169
    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v7

    .line 173
    const-string v8, "com.intsig.camscanner.NEW_PAGE"

    .line 174
    .line 175
    invoke-static {v8, v7}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 176
    .line 177
    .line 178
    move-result v7

    .line 179
    if-nez v7, :cond_6

    .line 180
    .line 181
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 182
    .line 183
    .line 184
    move-result-object v7

    .line 185
    invoke-interface {v7}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OO0o〇〇()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v7

    .line 189
    iput-object v7, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 190
    .line 191
    :cond_6
    if-eqz v5, :cond_7

    .line 192
    .line 193
    iget-object v7, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 194
    .line 195
    check-cast v7, Landroid/content/Intent;

    .line 196
    .line 197
    const-string v8, "key_templateinfo"

    .line 198
    .line 199
    iget-object v10, v5, Lcom/intsig/camscanner/autocomposite/TemplateItem;->〇080:Ljava/util/ArrayList;

    .line 200
    .line 201
    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 202
    .line 203
    .line 204
    iget-object v7, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 205
    .line 206
    check-cast v7, Landroid/content/Intent;

    .line 207
    .line 208
    const-string v8, "key_Fitcentre"

    .line 209
    .line 210
    iget-boolean v10, v5, Lcom/intsig/camscanner/autocomposite/TemplateItem;->〇o00〇〇Oo:Z

    .line 211
    .line 212
    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 213
    .line 214
    .line 215
    iget-object v7, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 216
    .line 217
    check-cast v7, Landroid/content/Intent;

    .line 218
    .line 219
    const-string v8, "key_RoundedCorner"

    .line 220
    .line 221
    iget-boolean v10, v5, Lcom/intsig/camscanner/autocomposite/TemplateItem;->〇o〇:Z

    .line 222
    .line 223
    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 224
    .line 225
    .line 226
    iget-object v7, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 227
    .line 228
    check-cast v7, Landroid/content/Intent;

    .line 229
    .line 230
    const-string v8, "KEY_X_RADIUS_SCALE"

    .line 231
    .line 232
    iget v10, v5, Lcom/intsig/camscanner/autocomposite/TemplateItem;->O8:F

    .line 233
    .line 234
    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 235
    .line 236
    .line 237
    iget-object v7, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 238
    .line 239
    check-cast v7, Landroid/content/Intent;

    .line 240
    .line 241
    const-string v8, "KEY_Y_RADIUS_SCALE"

    .line 242
    .line 243
    iget v10, v5, Lcom/intsig/camscanner/autocomposite/TemplateItem;->Oo08:F

    .line 244
    .line 245
    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 246
    .line 247
    .line 248
    :cond_7
    iget-object v7, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 249
    .line 250
    check-cast v7, Landroid/content/Intent;

    .line 251
    .line 252
    const-string v8, "extra_composite_can_edit"

    .line 253
    .line 254
    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 255
    .line 256
    .line 257
    iget-object v7, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 258
    .line 259
    check-cast v7, Landroid/content/Intent;

    .line 260
    .line 261
    const-string v8, "extra_from_certificate_capture"

    .line 262
    .line 263
    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 264
    .line 265
    .line 266
    iget-object v7, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 267
    .line 268
    if-eqz v7, :cond_9

    .line 269
    .line 270
    iget-object v8, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 271
    .line 272
    check-cast v8, Landroid/content/Intent;

    .line 273
    .line 274
    if-eqz v7, :cond_8

    .line 275
    .line 276
    invoke-virtual {v7}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->OO0o〇〇()Z

    .line 277
    .line 278
    .line 279
    move-result v7

    .line 280
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 281
    .line 282
    .line 283
    move-result-object v7

    .line 284
    goto :goto_2

    .line 285
    :cond_8
    move-object v7, v3

    .line 286
    :goto_2
    const-string v10, "extra_certificate_is_normal_fun"

    .line 287
    .line 288
    invoke-virtual {v8, v10, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 289
    .line 290
    .line 291
    :cond_9
    if-eqz p2, :cond_a

    .line 292
    .line 293
    iget-object v7, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 294
    .line 295
    check-cast v7, Landroid/content/Intent;

    .line 296
    .line 297
    const-string v8, "extra_entrance"

    .line 298
    .line 299
    invoke-virtual {v7, v8, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 300
    .line 301
    .line 302
    :cond_a
    const-string v7, "extra_from_certificate_type"

    .line 303
    .line 304
    if-eqz p1, :cond_c

    .line 305
    .line 306
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 307
    .line 308
    if-eqz p1, :cond_c

    .line 309
    .line 310
    if-eqz p1, :cond_b

    .line 311
    .line 312
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->Oo08()I

    .line 313
    .line 314
    .line 315
    move-result p1

    .line 316
    if-nez p1, :cond_b

    .line 317
    .line 318
    const/4 v6, 0x1

    .line 319
    :cond_b
    if-eqz v6, :cond_c

    .line 320
    .line 321
    const-string p1, "CSScan"

    .line 322
    .line 323
    const-string v3, "scan_select_idcard_success"

    .line 324
    .line 325
    invoke-static {p1, v3}, Lcom/intsig/camscanner/log/LogAgentData;->o800o8O(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    .line 327
    .line 328
    const-string p1, "from idCardguide"

    .line 329
    .line 330
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    .line 332
    .line 333
    iget-object p1, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 334
    .line 335
    check-cast p1, Landroid/content/Intent;

    .line 336
    .line 337
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_PREVIEW_DETECT_IDCARD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 338
    .line 339
    invoke-virtual {p1, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 340
    .line 341
    .line 342
    goto :goto_3

    .line 343
    :cond_c
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 344
    .line 345
    if-eqz p1, :cond_e

    .line 346
    .line 347
    iget-object v1, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 348
    .line 349
    check-cast v1, Landroid/content/Intent;

    .line 350
    .line 351
    if-eqz p1, :cond_d

    .line 352
    .line 353
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->oO80()Lcom/intsig/camscanner/purchase/entity/Function;

    .line 354
    .line 355
    .line 356
    move-result-object v3

    .line 357
    :cond_d
    invoke-virtual {v1, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 358
    .line 359
    .line 360
    :cond_e
    :goto_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 361
    .line 362
    .line 363
    move-result-object p1

    .line 364
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OOoo()Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 365
    .line 366
    .line 367
    move-result-object p1

    .line 368
    sget-object v1, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_CERTIFICATION:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 369
    .line 370
    if-ne p1, v1, :cond_f

    .line 371
    .line 372
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->IDCARD_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 373
    .line 374
    if-eq p2, p1, :cond_f

    .line 375
    .line 376
    iget-object p1, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 377
    .line 378
    check-cast p1, Landroid/content/Intent;

    .line 379
    .line 380
    const-string p2, "extra_id_card_flow"

    .line 381
    .line 382
    invoke-virtual {p1, p2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 383
    .line 384
    .line 385
    iget-object p1, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 386
    .line 387
    check-cast p1, Landroid/content/Intent;

    .line 388
    .line 389
    sget-object p2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SCAN_DONE_IDCARD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 390
    .line 391
    invoke-virtual {p1, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 392
    .line 393
    .line 394
    :cond_f
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->Oooo8o0〇()Z

    .line 395
    .line 396
    .line 397
    move-result p1

    .line 398
    if-eqz p1, :cond_10

    .line 399
    .line 400
    new-instance p1, LO〇0〇o808〇/〇o〇;

    .line 401
    .line 402
    invoke-direct {p1, p0}, LO〇0〇o808〇/〇o〇;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 403
    .line 404
    .line 405
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8o8O〇O(Ljava/lang/Runnable;)V

    .line 406
    .line 407
    .line 408
    iget-object p1, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 409
    .line 410
    check-cast p1, Landroid/content/Intent;

    .line 411
    .line 412
    sget-object p2, Lcom/intsig/camscanner/util/CONSTANT;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 413
    .line 414
    invoke-virtual {p1, p2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 415
    .line 416
    .line 417
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0〇0:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 418
    .line 419
    if-eqz p1, :cond_11

    .line 420
    .line 421
    invoke-static {p1}, Landroidx/lifecycle/ViewModelKt;->getViewModelScope(Landroidx/lifecycle/ViewModel;)Lkotlinx/coroutines/CoroutineScope;

    .line 422
    .line 423
    .line 424
    move-result-object v6

    .line 425
    if-eqz v6, :cond_11

    .line 426
    .line 427
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 428
    .line 429
    .line 430
    move-result-object v7

    .line 431
    const/4 v8, 0x0

    .line 432
    new-instance v9, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$jumpToComponentPage$5;

    .line 433
    .line 434
    const/4 p1, 0x0

    .line 435
    move-object v0, v9

    .line 436
    move-object v1, p0

    .line 437
    move-object v3, v5

    .line 438
    move-object v5, p1

    .line 439
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene$jumpToComponentPage$5;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/autocomposite/TemplateItem;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/coroutines/Continuation;)V

    .line 440
    .line 441
    .line 442
    const/4 v10, 0x2

    .line 443
    const/4 v11, 0x0

    .line 444
    invoke-static/range {v6 .. v11}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 445
    .line 446
    .line 447
    goto :goto_4

    .line 448
    :cond_10
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 449
    .line 450
    .line 451
    move-result-object p1

    .line 452
    iget-object p2, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 453
    .line 454
    check-cast p2, Landroid/content/Intent;

    .line 455
    .line 456
    const/16 v0, 0xcf

    .line 457
    .line 458
    invoke-virtual {p1, p2, v0}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 459
    .line 460
    .line 461
    :cond_11
    :goto_4
    return-void
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
.end method


# virtual methods
.method protected O0o〇〇Oo()Landroid/view/View;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O00()Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    new-instance v2, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController$SettingEntity;

    .line 16
    .line 17
    invoke-direct {v2}, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController$SettingEntity;-><init>()V

    .line 18
    .line 19
    .line 20
    const/4 v3, 0x1

    .line 21
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController$SettingEntity;->o〇0(Z)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController$SettingEntity;->〇80〇808〇O(Z)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController$SettingEntity;->oO80(Z)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController$SettingEntity;->〇080()V

    .line 31
    .line 32
    .line 33
    sget-object v3, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 34
    .line 35
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;->O8ooOoo〇(Landroid/content/Context;Lcom/intsig/camscanner/capture/settings/CaptureSettingsController$SettingEntity;)Landroid/view/View;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    goto :goto_0

    .line 40
    :cond_0
    const/4 v0, 0x0

    .line 41
    :goto_0
    return-object v0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method protected O80〇O〇080(IZ)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O80〇O〇080(IZ)V

    .line 2
    .line 3
    .line 4
    iget-object p2, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->〇〇808〇(I)V

    .line 9
    .line 10
    .line 11
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->OO〇OOo:Lcom/intsig/view/RotateLayout;

    .line 12
    .line 13
    if-eqz p2, :cond_1

    .line 14
    .line 15
    invoke-virtual {p2, p1}, Lcom/intsig/view/RotateLayout;->setOrientation(I)V

    .line 16
    .line 17
    .line 18
    :cond_1
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O880oOO08(Z)Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o0O〇8o0O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    const/4 v2, 0x0

    .line 11
    if-eqz p1, :cond_1

    .line 12
    .line 13
    const/4 p1, 0x2

    .line 14
    invoke-static {p0, v1, v2, p1, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇0〇o(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ZZILjava/lang/Object;)V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇OO0()Z

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    if-eqz p1, :cond_2

    .line 23
    .line 24
    const/4 p1, 0x3

    .line 25
    invoke-static {p0, v2, v2, p1, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇0〇o(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ZZILjava/lang/Object;)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo0〇Ooo()Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    if-eqz p1, :cond_3

    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8O〇88oO0()Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    if-nez p1, :cond_3

    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->ooO()Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    if-nez p1, :cond_3

    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o8O()V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_3
    const/4 v1, 0x0

    .line 52
    :goto_0
    return v1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public O8〇o()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o0O〇8o0O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    const-string v0, "CertificateCaptureScene"

    .line 9
    .line 10
    const-string v2, "isSavingPicture"

    .line 11
    .line 12
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return v1

    .line 16
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Oo0〇Ooo()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    return v1

    .line 23
    :cond_1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8〇o()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected OOO〇O0(Landroid/view/View;)V
    .locals 6

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOO〇O0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object v1, v0

    .line 17
    :goto_0
    const/4 v2, 0x0

    .line 18
    const-string v3, "CertificateCaptureScene"

    .line 19
    .line 20
    if-nez v1, :cond_1

    .line 21
    .line 22
    goto :goto_1

    .line 23
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    const v5, 0x7f0a0351

    .line 28
    .line 29
    .line 30
    if-ne v4, v5, :cond_3

    .line 31
    .line 32
    const-string v0, "shutter"

    .line 33
    .line 34
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 38
    .line 39
    const/16 v1, 0x1b

    .line 40
    .line 41
    if-lt v0, v1, :cond_2

    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo〇o()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_2

    .line 48
    .line 49
    const/16 v0, 0x8

    .line 50
    .line 51
    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 52
    .line 53
    .line 54
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-interface {p1, v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇08O8o〇0(Z)V

    .line 59
    .line 60
    .line 61
    goto/16 :goto_8

    .line 62
    .line 63
    :cond_3
    :goto_1
    if-nez v1, :cond_4

    .line 64
    .line 65
    goto :goto_2

    .line 66
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    const v4, 0x7f0a034c

    .line 71
    .line 72
    .line 73
    if-ne p1, v4, :cond_5

    .line 74
    .line 75
    const-string p1, "import"

    .line 76
    .line 77
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    const-string p1, "cs_scan"

    .line 81
    .line 82
    const-string v0, "id_mode"

    .line 83
    .line 84
    invoke-virtual {p0, v0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0OO8〇0(Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇o0O()V

    .line 91
    .line 92
    .line 93
    goto :goto_8

    .line 94
    :cond_5
    :goto_2
    const/4 p1, 0x1

    .line 95
    if-nez v1, :cond_6

    .line 96
    .line 97
    goto :goto_4

    .line 98
    :cond_6
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 99
    .line 100
    .line 101
    move-result v4

    .line 102
    const v5, 0x7f0a0352

    .line 103
    .line 104
    .line 105
    if-ne v4, v5, :cond_7

    .line 106
    .line 107
    :goto_3
    const/4 v4, 0x1

    .line 108
    goto :goto_6

    .line 109
    :cond_7
    :goto_4
    if-nez v1, :cond_8

    .line 110
    .line 111
    goto :goto_5

    .line 112
    :cond_8
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    const v5, 0x7f0a0e26

    .line 117
    .line 118
    .line 119
    if-ne v4, v5, :cond_9

    .line 120
    .line 121
    goto :goto_3

    .line 122
    :cond_9
    :goto_5
    const/4 v4, 0x0

    .line 123
    :goto_6
    if-eqz v4, :cond_a

    .line 124
    .line 125
    const-string v0, "jumpToNextPage"

    .line 126
    .line 127
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->o0O0()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 135
    .line 136
    .line 137
    move-result-object v0

    .line 138
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇〇0o〇〇0(ZLcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 139
    .line 140
    .line 141
    goto :goto_8

    .line 142
    :cond_a
    if-nez v1, :cond_b

    .line 143
    .line 144
    goto :goto_7

    .line 145
    :cond_b
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 146
    .line 147
    .line 148
    move-result p1

    .line 149
    const v4, 0x7f0a034b

    .line 150
    .line 151
    .line 152
    if-ne p1, v4, :cond_c

    .line 153
    .line 154
    const-string p1, "exit ceritificate"

    .line 155
    .line 156
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    const/4 p1, 0x3

    .line 160
    invoke-static {p0, v2, v2, p1, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇0〇o(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ZZILjava/lang/Object;)V

    .line 161
    .line 162
    .line 163
    goto :goto_8

    .line 164
    :cond_c
    :goto_7
    if-nez v1, :cond_d

    .line 165
    .line 166
    goto :goto_8

    .line 167
    :cond_d
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 168
    .line 169
    .line 170
    move-result p1

    .line 171
    const v0, 0x7f0a0a65

    .line 172
    .line 173
    .line 174
    if-ne p1, v0, :cond_e

    .line 175
    .line 176
    const-string p1, "showCertificateDes"

    .line 177
    .line 178
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    .line 180
    .line 181
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOo〇08〇()V

    .line 182
    .line 183
    .line 184
    :cond_e
    :goto_8
    return-void
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method protected OOoo(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "intent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "auto_change_to_id_card"

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇80o:Z

    .line 14
    .line 15
    const-string v0, "extra_certificate_capture_type"

    .line 16
    .line 17
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    iput p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇OOoooo:I

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public Ooo8(Landroid/widget/ImageView;Landroid/widget/TextView;)Z
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const v0, 0x7f0805b3

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 7
    .line 8
    .line 9
    :cond_0
    if-eqz p2, :cond_1

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 12
    .line 13
    iget v0, v0, Lcom/intsig/camscanner/capture/CaptureMode;->mStringRes:I

    .line 14
    .line 15
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 16
    .line 17
    .line 18
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo8(Landroid/widget/ImageView;Landroid/widget/TextView;)Z

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    return p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method protected Oo〇O8o〇8()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O()Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo〇O()Lcom/intsig/view/RotateImageTextButton;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const v1, 0x7f0a034c

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/view/RotateImageTextButton;

    .line 23
    .line 24
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇008〇oo(Lcom/intsig/view/RotateImageTextButton;)V

    .line 25
    .line 26
    .line 27
    new-array v1, v3, [Landroid/view/View;

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo〇O()Lcom/intsig/view/RotateImageTextButton;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    aput-object v4, v1, v2

    .line 34
    .line 35
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8O0O808〇([Landroid/view/View;)V

    .line 36
    .line 37
    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oo()Lcom/intsig/camscanner/view/RotateImageView;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    if-nez v1, :cond_1

    .line 43
    .line 44
    const v1, 0x7f0a0351

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    check-cast v0, Lcom/intsig/camscanner/view/RotateImageView;

    .line 52
    .line 53
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8888(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 54
    .line 55
    .line 56
    new-array v0, v3, [Landroid/view/View;

    .line 57
    .line 58
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oo()Lcom/intsig/camscanner/view/RotateImageView;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    aput-object v1, v0, v2

    .line 63
    .line 64
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8O0O808〇([Landroid/view/View;)V

    .line 65
    .line 66
    .line 67
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o8oO〇()Landroid/view/View;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    if-nez v0, :cond_3

    .line 72
    .line 73
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O()Landroid/view/View;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    if-eqz v0, :cond_2

    .line 78
    .line 79
    const v1, 0x7f0a034b

    .line 80
    .line 81
    .line 82
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    goto :goto_0

    .line 87
    :cond_2
    const/4 v0, 0x0

    .line 88
    :goto_0
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo0oOo〇0(Landroid/view/View;)V

    .line 89
    .line 90
    .line 91
    new-array v0, v3, [Landroid/view/View;

    .line 92
    .line 93
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o8oO〇()Landroid/view/View;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    aput-object v1, v0, v2

    .line 98
    .line 99
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8O0O808〇([Landroid/view/View;)V

    .line 100
    .line 101
    .line 102
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 103
    .line 104
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇o()Landroid/view/View;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    if-eqz v0, :cond_4

    .line 109
    .line 110
    const v1, 0x7f0a00f6

    .line 111
    .line 112
    .line 113
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    check-cast v1, Lcom/intsig/camscanner/view/RotateImageView;

    .line 118
    .line 119
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0〇oo(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 120
    .line 121
    .line 122
    const v1, 0x7f0a00f5

    .line 123
    .line 124
    .line 125
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    check-cast v1, Lcom/intsig/camscanner/view/RotateImageView;

    .line 130
    .line 131
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oO8008O(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 132
    .line 133
    .line 134
    const v1, 0x7f0a00f9

    .line 135
    .line 136
    .line 137
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 138
    .line 139
    .line 140
    move-result-object v1

    .line 141
    check-cast v1, Lcom/intsig/camscanner/view/RotateImageView;

    .line 142
    .line 143
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o88O〇8(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 144
    .line 145
    .line 146
    const v1, 0x7f0a00f8

    .line 147
    .line 148
    .line 149
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 150
    .line 151
    .line 152
    move-result-object v0

    .line 153
    check-cast v0, Lcom/intsig/camscanner/view/RotateImageView;

    .line 154
    .line 155
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o08oOO(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 156
    .line 157
    .line 158
    :cond_4
    return-void
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method protected O〇O〇oO()Landroid/view/View;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected o0()V
    .locals 3

    .line 1
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 8
    .line 9
    .line 10
    const-class v1, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    check-cast v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0ooOOo:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const-string v1, "extra_certificate_capture_type"

    .line 29
    .line 30
    const/4 v2, 0x0

    .line 31
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    iput v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇OOoooo:I

    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    const-string v1, "EXTRA_DOC_TYPE"

    .line 46
    .line 47
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    iput v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇0〇o:I

    .line 52
    .line 53
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O0〇oO〇o()V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public o0O0()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->o〇0()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o0O0()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oO()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oO()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇o〇88〇8:Z

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected oO00OOO()Landroid/view/View;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const v1, 0x7f0d0661

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final oOo〇8o008()Landroid/app/Dialog;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇〇0:Landroid/app/Dialog;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected onDestroy()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->onDestroy()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇088O:Ljava/util/concurrent/ExecutorService;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 9
    .line 10
    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇088O:Ljava/util/concurrent/ExecutorService;

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇0〇()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public ooo〇8oO()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->Oo08()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    sget-object v2, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;

    .line 11
    .line 12
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->〇080(I)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-gez v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    move v1, v0

    .line 20
    :cond_1
    :goto_0
    return v1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public ooo〇〇O〇([BLcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;)V
    .locals 3

    .line 1
    new-instance v0, LO〇0〇o808〇/oO80;

    .line 2
    .line 3
    invoke-direct {v0, p0}, LO〇0〇o808〇/oO80;-><init>(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8o8O〇O(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    if-eqz p2, :cond_0

    .line 10
    .line 11
    invoke-interface {p2}, Lcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;->〇o00〇〇Oo()V

    .line 12
    .line 13
    .line 14
    :cond_0
    const/4 v0, 0x1

    .line 15
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o88O8(Z)V

    .line 16
    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    new-instance v2, LO〇0〇o808〇/〇80〇808〇O;

    .line 23
    .line 24
    invoke-direct {v2, p1, p0, p2}, LO〇0〇o808〇/〇80〇808〇O;-><init>([BLcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;Lcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, v2}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 28
    .line 29
    .line 30
    sget-object p1, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->〇080:Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;

    .line 31
    .line 32
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->Oooo8o0〇(I)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇0O〇Oo(I)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    sget-object v1, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->Oo08()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {v1, v2, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureConfigManager;->oO80(II)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->Oo08()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    new-instance v2, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v3, "certificateType: "

    .line 25
    .line 26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v0, " orientation: "

    .line 33
    .line 34
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    const-string v0, "CertificateCaptureScene"

    .line 45
    .line 46
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    move p1, v1

    .line 50
    :cond_0
    return p1
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇80O8o8O〇(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const/4 v0, 0x1

    .line 8
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O00O(Z)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected 〇O()Landroid/view/View;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oOoO8OO〇:Z

    .line 10
    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    const v1, 0x7f0d0669

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const v1, 0x7f0d0668

    .line 18
    .line 19
    .line 20
    :goto_0
    const/4 v2, 0x0

    .line 21
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected 〇o8OO0()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oooO888(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o〇Oo0()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇o〇Oo0()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->Ooo8o()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇00OO(IILandroid/content/Intent;)Z
    .locals 6

    .line 1
    const/16 v0, 0x88

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    const-string v2, "CertificateCaptureScene"

    .line 5
    .line 6
    const/4 v3, 0x1

    .line 7
    if-eq p1, v0, :cond_a

    .line 8
    .line 9
    const/16 v0, 0xcf

    .line 10
    .line 11
    const/4 v4, 0x0

    .line 12
    if-eq p1, v0, :cond_7

    .line 13
    .line 14
    const/16 v0, 0x320

    .line 15
    .line 16
    const/4 v5, 0x0

    .line 17
    if-eq p1, v0, :cond_5

    .line 18
    .line 19
    const/16 v0, 0x321

    .line 20
    .line 21
    if-eq p1, v0, :cond_0

    .line 22
    .line 23
    const/4 v3, 0x0

    .line 24
    goto/16 :goto_0

    .line 25
    .line 26
    :cond_0
    const-string p1, "onActivityResult KEY_REQUEST_CERTIFICATE_PREVIEW"

    .line 27
    .line 28
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    if-eq p2, v1, :cond_3

    .line 32
    .line 33
    if-eqz p2, :cond_1

    .line 34
    .line 35
    goto/16 :goto_0

    .line 36
    .line 37
    :cond_1
    const-string p1, "DocToWordMultiCaptureScene"

    .line 38
    .line 39
    const-string p2, "RESULT_CANCELED"

    .line 40
    .line 41
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0〇0:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 45
    .line 46
    if-eqz p1, :cond_2

    .line 47
    .line 48
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O8〇o(Z)V

    .line 49
    .line 50
    .line 51
    :cond_2
    invoke-static {p0, v4, v3, v3, v5}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇0〇o(Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;ZZILjava/lang/Object;)V

    .line 52
    .line 53
    .line 54
    goto/16 :goto_0

    .line 55
    .line 56
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 61
    .line 62
    .line 63
    move-result-wide p1

    .line 64
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o0O0()I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    .line 69
    .line 70
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    .line 72
    .line 73
    const-string v5, "RESULT_OK docId:"

    .line 74
    .line 75
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    const-string p1, " ,docType:"

    .line 82
    .line 83
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 97
    .line 98
    if-eqz p1, :cond_4

    .line 99
    .line 100
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->o〇0()I

    .line 101
    .line 102
    .line 103
    move-result v4

    .line 104
    :cond_4
    invoke-direct {p0, v4, p3}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇00O(ILandroid/content/Intent;)V

    .line 105
    .line 106
    .line 107
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0〇0:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 108
    .line 109
    if-eqz p1, :cond_b

    .line 110
    .line 111
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O8〇o(Z)V

    .line 112
    .line 113
    .line 114
    goto/16 :goto_0

    .line 115
    .line 116
    :cond_5
    new-instance p1, Ljava/lang/StringBuilder;

    .line 117
    .line 118
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    .line 120
    .line 121
    const-string v0, "onActivityResult KEY_REQUEST_CERTIFICATE_MORE resultCode="

    .line 122
    .line 123
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    if-ne p2, v1, :cond_b

    .line 137
    .line 138
    if-eqz p3, :cond_6

    .line 139
    .line 140
    sget-object p1, Lcom/intsig/camscanner/capture/certificates/CertificateModelMoreActivity;->〇o0O:Ljava/lang/String;

    .line 141
    .line 142
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 143
    .line 144
    .line 145
    move-result-object v5

    .line 146
    :cond_6
    instance-of p1, v5, Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreItemModel;

    .line 147
    .line 148
    if-eqz p1, :cond_b

    .line 149
    .line 150
    new-instance p1, Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;

    .line 151
    .line 152
    check-cast v5, Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreItemModel;

    .line 153
    .line 154
    invoke-direct {p1, v5}, Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;-><init>(Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreItemModel;)V

    .line 155
    .line 156
    .line 157
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇80O8o8O〇(Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;)V

    .line 158
    .line 159
    .line 160
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oO〇oo()V

    .line 161
    .line 162
    .line 163
    goto :goto_0

    .line 164
    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    .line 165
    .line 166
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    .line 168
    .line 169
    const-string v0, "onActivityResult REQ_CERTIFICATE_COMPOSITE resultCode="

    .line 170
    .line 171
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 175
    .line 176
    .line 177
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object p1

    .line 181
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    if-ne p2, v1, :cond_9

    .line 185
    .line 186
    iget-object p1, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇800OO〇0O:Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;

    .line 187
    .line 188
    if-eqz p1, :cond_8

    .line 189
    .line 190
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;->o〇0()I

    .line 191
    .line 192
    .line 193
    move-result v4

    .line 194
    :cond_8
    invoke-direct {p0, v4, p3}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->o〇00O(ILandroid/content/Intent;)V

    .line 195
    .line 196
    .line 197
    goto :goto_0

    .line 198
    :cond_9
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->oO0()V

    .line 199
    .line 200
    .line 201
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇8〇o88()V

    .line 202
    .line 203
    .line 204
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇0ooOOo()V

    .line 205
    .line 206
    .line 207
    goto :goto_0

    .line 208
    :cond_a
    new-instance p1, Ljava/lang/StringBuilder;

    .line 209
    .line 210
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 211
    .line 212
    .line 213
    const-string v0, "onActivityResult PICK_IMAGE_CERTIFICATE resultCode="

    .line 214
    .line 215
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    .line 217
    .line 218
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 219
    .line 220
    .line 221
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 222
    .line 223
    .line 224
    move-result-object p1

    .line 225
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    if-ne p2, v1, :cond_b

    .line 229
    .line 230
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->O88O(Landroid/content/Intent;)V

    .line 231
    .line 232
    .line 233
    :cond_b
    :goto_0
    return v3
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
.end method

.method public 〇〇〇0〇〇0()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇OO0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;->〇〇〇00:Z

    .line 10
    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    return v1

    .line 14
    :cond_1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇〇〇0〇〇0()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

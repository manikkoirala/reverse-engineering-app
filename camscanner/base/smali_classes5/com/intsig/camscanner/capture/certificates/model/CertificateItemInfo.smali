.class public Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;
.super Ljava/lang/Object;
.source "CertificateItemInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final CERTIFICATE_TYPE_BANK_CARD:I = 0x3f1

.field public static final CERTIFICATE_TYPE_BOOKLET:I = 0x3eb

.field public static final CERTIFICATE_TYPE_BOOKLET_JIGSAW:I = 0x3f4

.field public static final CERTIFICATE_TYPE_BUSINESS_LICENSE:I = 0x3ef

.field public static final CERTIFICATE_TYPE_CARD_BAG_MORE:I = 0x3f6

.field public static final CERTIFICATE_TYPE_CN_DRIVE_CAR_LISCENCE:I = 0x3ed

.field public static final CERTIFICATE_TYPE_CN_DRIVE_PERSON_LISCENCE:I = 0x3ec

.field public static final CERTIFICATE_TYPE_FOREIGN_CARD:I = 0x3f2

.field public static final CERTIFICATE_TYPE_FOREIGN_DRIVE:I = 0x3f0

.field public static final CERTIFICATE_TYPE_HOUSE_CERTIFICATE:I = 0x3ee

.field public static final CERTIFICATE_TYPE_ID_CARD:I = 0x3e9

.field public static final CERTIFICATE_TYPE_MORE:I = 0x3f3

.field public static final CERTIFICATE_TYPE_PASSPORT:I = 0x3ea

.field public static final CERTIFICATE_TYPE_SINGLE_PAGE:I = 0x3f5


# instance fields
.field public certificateDesciptionRid:I

.field public certificateNameRid:I

.field public certificateRid:I

.field public certificateRidBig:I

.field public certificateType:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 6

    const v5, 0x7f13188f

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    .line 1
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;-><init>(IIIII)V

    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateType:I

    .line 4
    iput p2, p0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateRid:I

    .line 5
    iput p3, p0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateRidBig:I

    .line 6
    iput p4, p0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateNameRid:I

    .line 7
    iput p5, p0, Lcom/intsig/camscanner/capture/certificates/model/CertificateItemInfo;->certificateDesciptionRid:I

    return-void
.end method

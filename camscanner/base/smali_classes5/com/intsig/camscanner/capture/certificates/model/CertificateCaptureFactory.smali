.class public Lcom/intsig/camscanner/capture/certificates/model/CertificateCaptureFactory;
.super Ljava/lang/Object;
.source "CertificateCaptureFactory.java"


# direct methods
.method public static 〇080(I)Lcom/intsig/camscanner/capture/certificates/model/BaseCertificateCapture;
    .locals 0

    .line 1
    packed-switch p0, :pswitch_data_0

    .line 2
    .line 3
    .line 4
    :pswitch_0
    const/4 p0, 0x0

    .line 5
    goto :goto_0

    .line 6
    :pswitch_1
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/SingleIdCardCapture;

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/SingleIdCardCapture;-><init>()V

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :pswitch_2
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/BookletJigsawCapture;

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/BookletJigsawCapture;-><init>()V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :pswitch_3
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/CertificateCapture;

    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/CertificateCapture;-><init>()V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :pswitch_4
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/BankCardCapture;

    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/BankCardCapture;-><init>()V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :pswitch_5
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/USDriverCapture;

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/USDriverCapture;-><init>()V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :pswitch_6
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/BusinessLicenseCapture;

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/BusinessLicenseCapture;-><init>()V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :pswitch_7
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/HousePropertyCapture;

    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/HousePropertyCapture;-><init>()V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :pswitch_8
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/CNTravelCapture;

    .line 49
    .line 50
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/CNTravelCapture;-><init>()V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :pswitch_9
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/CNDriverCapture;

    .line 55
    .line 56
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/CNDriverCapture;-><init>()V

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :pswitch_a
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/ResidenceBookletCapture;

    .line 61
    .line 62
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/ResidenceBookletCapture;-><init>()V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :pswitch_b
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/PassPortCapture;

    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/PassPortCapture;-><init>()V

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :pswitch_c
    new-instance p0, Lcom/intsig/camscanner/capture/certificates/model/IDCardCapture;

    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/certificates/model/IDCardCapture;-><init>()V

    .line 75
    .line 76
    .line 77
    :goto_0
    return-object p0

    .line 78
    nop

    .line 79
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

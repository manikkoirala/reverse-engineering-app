.class final Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "CaptureActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/CaptureActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ScaleGestureListener"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/capture/CaptureActivity;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/capture/CaptureActivity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4
    .param p1    # Landroid/view/ScaleGestureDetector;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "detector"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->O00OoO〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x0

    .line 13
    const/4 v2, 0x1

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8〇o()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-ne v0, v2, :cond_0

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    :goto_0
    if-nez v0, :cond_2

    .line 26
    .line 27
    const-string v0, "CameraCaptureActivity"

    .line 28
    .line 29
    const-string v3, "onScale Ignore startCapture, disable enableCapture"

    .line 30
    .line 31
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 35
    .line 36
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->O00OoO〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    if-eqz v0, :cond_1

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o0ooO()Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-ne v0, v2, :cond_1

    .line 47
    .line 48
    const/4 v0, 0x1

    .line 49
    goto :goto_1

    .line 50
    :cond_1
    const/4 v0, 0x0

    .line 51
    :goto_1
    if-nez v0, :cond_2

    .line 52
    .line 53
    return v1

    .line 54
    :cond_2
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    if-eqz v0, :cond_3

    .line 63
    .line 64
    return v2

    .line 65
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 66
    .line 67
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->oO8(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O8ooOoo〇()Z

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    if-eqz v0, :cond_8

    .line 80
    .line 81
    const v0, 0x3f80a3d7    # 1.005f

    .line 82
    .line 83
    .line 84
    const/4 v3, 0x2

    .line 85
    cmpl-float v0, p1, v0

    .line 86
    .line 87
    if-lez v0, :cond_5

    .line 88
    .line 89
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 90
    .line 91
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->oO8(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08〇0〇o〇8()Z

    .line 96
    .line 97
    .line 98
    move-result p1

    .line 99
    if-eqz p1, :cond_4

    .line 100
    .line 101
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 102
    .line 103
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->oO8(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0o〇〇()Lcom/intsig/camscanner/view/ZoomControl;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/view/ZoomControl;->oO80(I)Z

    .line 112
    .line 113
    .line 114
    goto :goto_2

    .line 115
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 116
    .line 117
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->oO8(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0o〇〇()Lcom/intsig/camscanner/view/ZoomControl;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ZoomControl;->〇〇888()Z

    .line 126
    .line 127
    .line 128
    :goto_2
    const/4 v1, 0x1

    .line 129
    goto :goto_3

    .line 130
    :cond_5
    const v0, 0x3f7eb852    # 0.995f

    .line 131
    .line 132
    .line 133
    cmpg-float p1, p1, v0

    .line 134
    .line 135
    if-gez p1, :cond_7

    .line 136
    .line 137
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 138
    .line 139
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->oO8(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 140
    .line 141
    .line 142
    move-result-object p1

    .line 143
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08〇0〇o〇8()Z

    .line 144
    .line 145
    .line 146
    move-result p1

    .line 147
    if-eqz p1, :cond_6

    .line 148
    .line 149
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 150
    .line 151
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->oO8(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 152
    .line 153
    .line 154
    move-result-object p1

    .line 155
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0o〇〇()Lcom/intsig/camscanner/view/ZoomControl;

    .line 156
    .line 157
    .line 158
    move-result-object p1

    .line 159
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/view/ZoomControl;->OO0o〇〇〇〇0(I)Z

    .line 160
    .line 161
    .line 162
    goto :goto_2

    .line 163
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 164
    .line 165
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->oO8(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 166
    .line 167
    .line 168
    move-result-object p1

    .line 169
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0o〇〇()Lcom/intsig/camscanner/view/ZoomControl;

    .line 170
    .line 171
    .line 172
    move-result-object p1

    .line 173
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ZoomControl;->〇80〇808〇O()Z

    .line 174
    .line 175
    .line 176
    goto :goto_2

    .line 177
    :cond_7
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 178
    .line 179
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->oO8(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 180
    .line 181
    .line 182
    move-result-object p1

    .line 183
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0o〇〇()Lcom/intsig/camscanner/view/ZoomControl;

    .line 184
    .line 185
    .line 186
    move-result-object p1

    .line 187
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ZoomControl;->o〇0()V

    .line 188
    .line 189
    .line 190
    :goto_3
    return v1

    .line 191
    :cond_8
    const v0, 0x3f8147ae    # 1.01f

    .line 192
    .line 193
    .line 194
    cmpl-float v0, p1, v0

    .line 195
    .line 196
    if-lez v0, :cond_9

    .line 197
    .line 198
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 199
    .line 200
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->〇〇〇OOO〇〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Landroid/os/Handler;

    .line 201
    .line 202
    .line 203
    move-result-object p1

    .line 204
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 205
    .line 206
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->OOo00(Lcom/intsig/camscanner/capture/CaptureActivity;)Ljava/lang/Runnable;

    .line 207
    .line 208
    .line 209
    move-result-object v0

    .line 210
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 211
    .line 212
    .line 213
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 214
    .line 215
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->〇〇〇OOO〇〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Landroid/os/Handler;

    .line 216
    .line 217
    .line 218
    move-result-object p1

    .line 219
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 220
    .line 221
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->O〇0o8〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Ljava/lang/Runnable;

    .line 222
    .line 223
    .line 224
    move-result-object v0

    .line 225
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 226
    .line 227
    .line 228
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 229
    .line 230
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->〇〇〇OOO〇〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Landroid/os/Handler;

    .line 231
    .line 232
    .line 233
    move-result-object p1

    .line 234
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 235
    .line 236
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->O〇0o8〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Ljava/lang/Runnable;

    .line 237
    .line 238
    .line 239
    move-result-object v0

    .line 240
    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 241
    .line 242
    .line 243
    return v2

    .line 244
    :cond_9
    const v0, 0x3f7d70a4    # 0.99f

    .line 245
    .line 246
    .line 247
    cmpg-float p1, p1, v0

    .line 248
    .line 249
    if-gez p1, :cond_a

    .line 250
    .line 251
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 252
    .line 253
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->〇〇〇OOO〇〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Landroid/os/Handler;

    .line 254
    .line 255
    .line 256
    move-result-object p1

    .line 257
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 258
    .line 259
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->OOo00(Lcom/intsig/camscanner/capture/CaptureActivity;)Ljava/lang/Runnable;

    .line 260
    .line 261
    .line 262
    move-result-object v0

    .line 263
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 264
    .line 265
    .line 266
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 267
    .line 268
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->〇〇〇OOO〇〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Landroid/os/Handler;

    .line 269
    .line 270
    .line 271
    move-result-object p1

    .line 272
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 273
    .line 274
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->O〇0o8〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Ljava/lang/Runnable;

    .line 275
    .line 276
    .line 277
    move-result-object v0

    .line 278
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 279
    .line 280
    .line 281
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 282
    .line 283
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->〇〇〇OOO〇〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Landroid/os/Handler;

    .line 284
    .line 285
    .line 286
    move-result-object p1

    .line 287
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 288
    .line 289
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->OOo00(Lcom/intsig/camscanner/capture/CaptureActivity;)Ljava/lang/Runnable;

    .line 290
    .line 291
    .line 292
    move-result-object v0

    .line 293
    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 294
    .line 295
    .line 296
    return v2

    .line 297
    :cond_a
    return v1
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 5
    .param p1    # Landroid/view/ScaleGestureDetector;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "detector"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->O00OoO〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x1

    .line 13
    const/4 v2, 0x0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8〇o()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-ne v0, v1, :cond_0

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    :goto_0
    const-string v3, "CameraCaptureActivity"

    .line 26
    .line 27
    if-nez v0, :cond_2

    .line 28
    .line 29
    const-string v0, "onScaleBegin Ignore startCapture, disable enableCapture"

    .line 30
    .line 31
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 35
    .line 36
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->O00OoO〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    if-eqz v0, :cond_1

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o0ooO()Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-ne v0, v1, :cond_1

    .line 47
    .line 48
    const/4 v0, 0x1

    .line 49
    goto :goto_1

    .line 50
    :cond_1
    const/4 v0, 0x0

    .line 51
    :goto_1
    if-nez v0, :cond_2

    .line 52
    .line 53
    return v2

    .line 54
    :cond_2
    const-string v0, "onScaleBegin"

    .line 55
    .line 56
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 60
    .line 61
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->oo0O(Lcom/intsig/camscanner/capture/CaptureActivity;)V

    .line 62
    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 65
    .line 66
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->〇〇〇OOO〇〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Landroid/os/Handler;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    iget-object v3, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 71
    .line 72
    invoke-static {v3}, Lcom/intsig/camscanner/capture/CaptureActivity;->o8O〇008(Lcom/intsig/camscanner/capture/CaptureActivity;)Ljava/lang/Runnable;

    .line 73
    .line 74
    .line 75
    move-result-object v3

    .line 76
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 77
    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 80
    .line 81
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureActivity;->〇8o0o0(Lcom/intsig/camscanner/capture/CaptureActivity;)Landroid/view/View;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    if-nez v0, :cond_3

    .line 86
    .line 87
    goto :goto_2

    .line 88
    :cond_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 89
    .line 90
    .line 91
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 92
    .line 93
    const/4 v3, 0x2

    .line 94
    const/4 v4, 0x0

    .line 95
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/capture/CaptureActivity;->O〇O88(Lcom/intsig/camscanner/capture/CaptureActivity;ZZILjava/lang/Object;)V

    .line 96
    .line 97
    .line 98
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 99
    .line 100
    invoke-static {v0, v1}, Lcom/intsig/camscanner/capture/CaptureActivity;->〇8ooo(Lcom/intsig/camscanner/capture/CaptureActivity;Z)V

    .line 101
    .line 102
    .line 103
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    .line 104
    .line 105
    .line 106
    move-result p1

    .line 107
    return p1
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 3
    .param p1    # Landroid/view/ScaleGestureDetector;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "detector"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 7
    .line 8
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->O00OoO〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const/4 v0, 0x1

    .line 13
    const/4 v1, 0x0

    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8〇o()Z

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    if-ne p1, v0, :cond_0

    .line 21
    .line 22
    const/4 p1, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 p1, 0x0

    .line 25
    :goto_0
    const-string v2, "CameraCaptureActivity"

    .line 26
    .line 27
    if-nez p1, :cond_2

    .line 28
    .line 29
    const-string p1, "onScaleEnd Ignore startCapture, onScaleEnd disable enableCapture"

    .line 30
    .line 31
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 35
    .line 36
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->O00OoO〇(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    if-eqz p1, :cond_1

    .line 41
    .line 42
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o0ooO()Z

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    if-ne p1, v0, :cond_1

    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_1
    const/4 v0, 0x0

    .line 50
    :goto_1
    if-nez v0, :cond_2

    .line 51
    .line 52
    return-void

    .line 53
    :cond_2
    const-string p1, "onScaleEnd"

    .line 54
    .line 55
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 59
    .line 60
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureActivity;->oO8(Lcom/intsig/camscanner/capture/CaptureActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0o〇〇()Lcom/intsig/camscanner/view/ZoomControl;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ZoomControl;->o〇0()V

    .line 69
    .line 70
    .line 71
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 72
    .line 73
    const/4 v0, 0x2

    .line 74
    const/4 v2, 0x0

    .line 75
    invoke-static {p1, v1, v1, v0, v2}, Lcom/intsig/camscanner/capture/CaptureActivity;->O〇O88(Lcom/intsig/camscanner/capture/CaptureActivity;ZZILjava/lang/Object;)V

    .line 76
    .line 77
    .line 78
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

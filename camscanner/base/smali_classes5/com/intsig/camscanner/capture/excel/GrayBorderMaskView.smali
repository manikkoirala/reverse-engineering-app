.class public Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;
.super Landroid/view/View;
.source "GrayBorderMaskView.java"


# instance fields
.field public O8o08O8O:Z

.field private final OO:Landroid/graphics/Paint;

.field private o0:Landroid/graphics/Paint;

.field private final o〇00O:I

.field private final 〇08O〇00〇o:Landroid/graphics/Path;

.field private 〇OOo8〇0:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, -0x1

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 3
    sget-object p2, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    const/16 p3, 0x1e

    invoke-static {p2, p3}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    move-result p2

    iput p2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 4
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->OO:Landroid/graphics/Paint;

    .line 5
    new-instance p2, Landroid/graphics/Path;

    invoke-direct {p2}, Landroid/graphics/Path;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    const/16 p3, 0x19

    invoke-static {p2, p3}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    move-result p2

    iput p2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o〇00O:I

    const/4 p2, 0x0

    .line 7
    iput-boolean p2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->O8o08O8O:Z

    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇080(Landroid/content/Context;)V

    return-void
.end method

.method private 〇080(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇o00〇〇Oo(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇o〇(Landroid/content/Context;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private 〇o00〇〇Oo(Landroid/content/Context;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o0:Landroid/graphics/Paint;

    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const v1, 0x7f060060

    .line 13
    .line 14
    .line 15
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o0:Landroid/graphics/Paint;

    .line 23
    .line 24
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 25
    .line 26
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o0:Landroid/graphics/Paint;

    .line 30
    .line 31
    iget v0, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 32
    .line 33
    mul-int/lit8 v0, v0, 0x2

    .line 34
    .line 35
    int-to-float v0, v0

    .line 36
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private 〇o〇(Landroid/content/Context;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->OO:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const v1, 0x7f0602fa

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 15
    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->OO:Landroid/graphics/Paint;

    .line 18
    .line 19
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 20
    .line 21
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->OO:Landroid/graphics/Paint;

    .line 25
    .line 26
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    const/4 v1, 0x3

    .line 31
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    int-to-float v0, v0

    .line 36
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    const/4 v3, 0x0

    .line 13
    const/4 v4, 0x0

    .line 14
    int-to-float v5, v0

    .line 15
    int-to-float v6, v1

    .line 16
    iget-object v7, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o0:Landroid/graphics/Paint;

    .line 17
    .line 18
    move-object v2, p1

    .line 19
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 20
    .line 21
    .line 22
    iget-boolean v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->O8o08O8O:Z

    .line 23
    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 27
    .line 28
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 29
    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 32
    .line 33
    iget v3, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 34
    .line 35
    int-to-float v4, v3

    .line 36
    iget v5, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o〇00O:I

    .line 37
    .line 38
    add-int/2addr v3, v5

    .line 39
    int-to-float v3, v3

    .line 40
    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 41
    .line 42
    .line 43
    iget-object v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 44
    .line 45
    iget v3, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 46
    .line 47
    int-to-float v4, v3

    .line 48
    int-to-float v3, v3

    .line 49
    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 50
    .line 51
    .line 52
    iget-object v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 53
    .line 54
    iget v3, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 55
    .line 56
    iget v4, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o〇00O:I

    .line 57
    .line 58
    add-int/2addr v4, v3

    .line 59
    int-to-float v4, v4

    .line 60
    int-to-float v3, v3

    .line 61
    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 62
    .line 63
    .line 64
    iget-object v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 65
    .line 66
    iget v3, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 67
    .line 68
    int-to-float v4, v3

    .line 69
    iget v5, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o〇00O:I

    .line 70
    .line 71
    sub-int v5, v1, v5

    .line 72
    .line 73
    sub-int/2addr v5, v3

    .line 74
    int-to-float v3, v5

    .line 75
    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 76
    .line 77
    .line 78
    iget-object v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 79
    .line 80
    iget v3, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 81
    .line 82
    int-to-float v4, v3

    .line 83
    sub-int v3, v1, v3

    .line 84
    .line 85
    int-to-float v3, v3

    .line 86
    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 87
    .line 88
    .line 89
    iget-object v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 90
    .line 91
    iget v3, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o〇00O:I

    .line 92
    .line 93
    iget v4, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 94
    .line 95
    add-int/2addr v3, v4

    .line 96
    int-to-float v3, v3

    .line 97
    sub-int v4, v1, v4

    .line 98
    .line 99
    int-to-float v4, v4

    .line 100
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 101
    .line 102
    .line 103
    iget-object v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 104
    .line 105
    iget v3, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 106
    .line 107
    sub-int v4, v0, v3

    .line 108
    .line 109
    int-to-float v4, v4

    .line 110
    iget v5, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o〇00O:I

    .line 111
    .line 112
    sub-int v5, v1, v5

    .line 113
    .line 114
    sub-int/2addr v5, v3

    .line 115
    int-to-float v3, v5

    .line 116
    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 117
    .line 118
    .line 119
    iget-object v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 120
    .line 121
    iget v3, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 122
    .line 123
    sub-int v4, v0, v3

    .line 124
    .line 125
    int-to-float v4, v4

    .line 126
    sub-int v3, v1, v3

    .line 127
    .line 128
    int-to-float v3, v3

    .line 129
    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 130
    .line 131
    .line 132
    iget-object v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 133
    .line 134
    iget v3, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o〇00O:I

    .line 135
    .line 136
    sub-int v3, v0, v3

    .line 137
    .line 138
    iget v4, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 139
    .line 140
    sub-int/2addr v3, v4

    .line 141
    int-to-float v3, v3

    .line 142
    sub-int/2addr v1, v4

    .line 143
    int-to-float v1, v1

    .line 144
    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 145
    .line 146
    .line 147
    iget-object v1, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 148
    .line 149
    iget v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 150
    .line 151
    sub-int v3, v0, v2

    .line 152
    .line 153
    int-to-float v3, v3

    .line 154
    iget v4, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o〇00O:I

    .line 155
    .line 156
    add-int/2addr v4, v2

    .line 157
    int-to-float v2, v4

    .line 158
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 159
    .line 160
    .line 161
    iget-object v1, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 162
    .line 163
    iget v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 164
    .line 165
    sub-int v3, v0, v2

    .line 166
    .line 167
    int-to-float v3, v3

    .line 168
    int-to-float v2, v2

    .line 169
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 170
    .line 171
    .line 172
    iget-object v1, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 173
    .line 174
    iget v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o〇00O:I

    .line 175
    .line 176
    sub-int/2addr v0, v2

    .line 177
    iget v2, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 178
    .line 179
    sub-int/2addr v0, v2

    .line 180
    int-to-float v0, v0

    .line 181
    int-to-float v2, v2

    .line 182
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 183
    .line 184
    .line 185
    iget-object v0, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 186
    .line 187
    iget-object v1, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->OO:Landroid/graphics/Paint;

    .line 188
    .line 189
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 190
    .line 191
    .line 192
    :cond_0
    return-void
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public setBorderWidth(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->〇OOo8〇0:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/capture/excel/GrayBorderMaskView;->o0:Landroid/graphics/Paint;

    .line 4
    .line 5
    mul-int/lit8 p1, p1, 0x2

    .line 6
    .line 7
    int-to-float p1, p1

    .line 8
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

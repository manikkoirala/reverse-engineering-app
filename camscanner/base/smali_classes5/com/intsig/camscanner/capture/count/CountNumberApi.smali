.class public final Lcom/intsig/camscanner/capture/count/CountNumberApi;
.super Ljava/lang/Object;
.source "CountNumberApi.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;,
        Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;,
        Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationData;,
        Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchData;,
        Lcom/intsig/camscanner/capture/count/CountNumberApi$FullCountResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/capture/count/CountNumberApi;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountNumberApi;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/capture/count/CountNumberApi;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/capture/count/CountNumberApi;->〇080:Lcom/intsig/camscanner/capture/count/CountNumberApi;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final 〇080(Ljava/lang/String;)Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;
    .locals 7
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "filePath"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/io/File;

    .line 7
    .line 8
    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    .line 12
    .line 13
    .line 14
    move-result p0

    .line 15
    const/4 v1, 0x0

    .line 16
    const-string v2, "CountNumberApi"

    .line 17
    .line 18
    if-nez p0, :cond_0

    .line 19
    .line 20
    const-string p0, "classifyApi file is not exist!"

    .line 21
    .line 22
    invoke-static {v2, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-object v1

    .line 26
    :cond_0
    new-instance p0, Lcom/intsig/tianshu/ParamsBuilder;

    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v3, "token"

    .line 32
    .line 33
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇8〇()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    invoke-virtual {p0, v3, v4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    const-string v3, "cs_ept_d"

    .line 42
    .line 43
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->o〇0()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v4

    .line 47
    invoke-virtual {p0, v3, v4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 48
    .line 49
    .line 50
    move-result-object p0

    .line 51
    const-string v3, "md5"

    .line 52
    .line 53
    invoke-static {v0}, Lcom/intsig/utils/MD5Utils;->〇080(Ljava/io/File;)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v4

    .line 57
    invoke-virtual {p0, v3, v4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 58
    .line 59
    .line 60
    move-result-object p0

    .line 61
    const-string v3, "size"

    .line 62
    .line 63
    invoke-virtual {v0}, Ljava/io/File;->length()J

    .line 64
    .line 65
    .line 66
    move-result-wide v4

    .line 67
    invoke-virtual {p0, v3, v4, v5}, Lcom/intsig/tianshu/ParamsBuilder;->OO0o〇〇〇〇0(Ljava/lang/String;J)Lcom/intsig/tianshu/ParamsBuilder;

    .line 68
    .line 69
    .line 70
    move-result-object p0

    .line 71
    const-string v3, "platform"

    .line 72
    .line 73
    const-string v4, "android"

    .line 74
    .line 75
    invoke-virtual {p0, v3, v4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 76
    .line 77
    .line 78
    move-result-object p0

    .line 79
    const-string v3, "auto_match"

    .line 80
    .line 81
    const/4 v4, 0x1

    .line 82
    invoke-virtual {p0, v3, v4}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 83
    .line 84
    .line 85
    move-result-object p0

    .line 86
    :try_start_0
    invoke-static {}, Lcom/intsig/CsHosts;->〇〇888()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v3

    .line 90
    new-instance v5, Ljava/lang/StringBuilder;

    .line 91
    .line 92
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    .line 94
    .line 95
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    const-string v3, "/count/v2/classify"

    .line 99
    .line 100
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v3

    .line 107
    invoke-virtual {p0, v3}, Lcom/intsig/tianshu/ParamsBuilder;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object p0

    .line 111
    invoke-static {p0}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 112
    .line 113
    .line 114
    move-result-object p0

    .line 115
    invoke-static {v0}, Lkotlin/io/FilesKt;->〇o〇(Ljava/io/File;)[B

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    invoke-virtual {p0, v0}, Lcom/lzy/okgo/request/base/BodyRequest;->upBytes([B)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 120
    .line 121
    .line 122
    move-result-object p0

    .line 123
    check-cast p0, Lcom/lzy/okgo/request/PostRequest;

    .line 124
    .line 125
    invoke-virtual {p0}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 126
    .line 127
    .line 128
    move-result-object p0

    .line 129
    invoke-virtual {p0}, Lokhttp3/Response;->〇oo〇()Z

    .line 130
    .line 131
    .line 132
    move-result v0

    .line 133
    if-eqz v0, :cond_1

    .line 134
    .line 135
    invoke-virtual {p0}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    if-eqz v0, :cond_1

    .line 140
    .line 141
    invoke-virtual {p0}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 142
    .line 143
    .line 144
    move-result-object p0

    .line 145
    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 146
    .line 147
    .line 148
    invoke-virtual {p0}, Lokhttp3/ResponseBody;->charStream()Ljava/io/Reader;

    .line 149
    .line 150
    .line 151
    move-result-object p0

    .line 152
    const-class v0, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;

    .line 153
    .line 154
    invoke-static {p0, v0}, Lcom/intsig/okgo/utils/GsonUtils;->〇o〇(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 155
    .line 156
    .line 157
    move-result-object p0

    .line 158
    move-object v1, p0

    .line 159
    check-cast v1, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;

    .line 160
    .line 161
    sget-object p0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 162
    .line 163
    invoke-virtual {v1, p0}, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;->setUseUpCount(Ljava/lang/Boolean;)V

    .line 164
    .line 165
    .line 166
    goto :goto_1

    .line 167
    :cond_1
    invoke-virtual {p0}, Lokhttp3/Response;->〇O8o08O()I

    .line 168
    .line 169
    .line 170
    move-result v0

    .line 171
    const/16 v3, 0x196

    .line 172
    .line 173
    if-ne v0, v3, :cond_3

    .line 174
    .line 175
    invoke-virtual {p0}, Lokhttp3/Response;->o800o8O()Lokhttp3/Headers;

    .line 176
    .line 177
    .line 178
    move-result-object v0

    .line 179
    const/4 v5, 0x0

    .line 180
    if-eqz v0, :cond_2

    .line 181
    .line 182
    const-string v6, "X-IS-Error-Code"

    .line 183
    .line 184
    invoke-virtual {v0, v6}, Lokhttp3/Headers;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    if-eqz v0, :cond_2

    .line 189
    .line 190
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 191
    .line 192
    .line 193
    move-result v0

    .line 194
    const/16 v6, 0x67

    .line 195
    .line 196
    if-ne v0, v6, :cond_2

    .line 197
    .line 198
    goto :goto_0

    .line 199
    :cond_2
    const/4 v4, 0x0

    .line 200
    :goto_0
    if-eqz v4, :cond_3

    .line 201
    .line 202
    new-instance v1, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;

    .line 203
    .line 204
    invoke-direct {v1}, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;-><init>()V

    .line 205
    .line 206
    .line 207
    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 208
    .line 209
    invoke-virtual {v1, p0}, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;->setUseUpCount(Ljava/lang/Boolean;)V

    .line 210
    .line 211
    .line 212
    goto :goto_1

    .line 213
    :cond_3
    invoke-virtual {p0}, Lokhttp3/Response;->〇O8o08O()I

    .line 214
    .line 215
    .line 216
    move-result p0

    .line 217
    if-ne p0, v3, :cond_4

    .line 218
    .line 219
    new-instance v1, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;

    .line 220
    .line 221
    invoke-direct {v1}, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;-><init>()V

    .line 222
    .line 223
    .line 224
    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 225
    .line 226
    invoke-virtual {v1, p0}, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;->setRequestError(Ljava/lang/Boolean;)V

    .line 227
    .line 228
    .line 229
    :cond_4
    :goto_1
    new-instance p0, Ljava/lang/StringBuilder;

    .line 230
    .line 231
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 232
    .line 233
    .line 234
    const-string v0, "classifyApi success response is "

    .line 235
    .line 236
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    .line 238
    .line 239
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 240
    .line 241
    .line 242
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 243
    .line 244
    .line 245
    move-result-object p0

    .line 246
    invoke-static {v2, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    .line 248
    .line 249
    goto :goto_2

    .line 250
    :catch_0
    move-exception p0

    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    .line 252
    .line 253
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 254
    .line 255
    .line 256
    const-string v1, "classifyApi error "

    .line 257
    .line 258
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    .line 260
    .line 261
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 262
    .line 263
    .line 264
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 265
    .line 266
    .line 267
    move-result-object p0

    .line 268
    invoke-static {v2, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    new-instance v1, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;

    .line 272
    .line 273
    invoke-direct {v1}, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;-><init>()V

    .line 274
    .line 275
    .line 276
    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 277
    .line 278
    invoke-virtual {v1, p0}, Lcom/intsig/camscanner/capture/count/CountNumberApi$ClassificationDataResponse;->setRequestError(Ljava/lang/Boolean;)V

    .line 279
    .line 280
    .line 281
    :goto_2
    return-object v1
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public static final 〇o〇(Ljava/lang/String;ILandroid/graphics/Rect;Landroid/graphics/Rect;)Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Rect;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "CountNumberApi"

    .line 2
    .line 3
    const-string v1, "imageId"

    .line 4
    .line 5
    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v1, "region"

    .line 9
    .line 10
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    new-instance v1, Lcom/intsig/tianshu/ParamsBuilder;

    .line 14
    .line 15
    invoke-direct {v1}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v2, "token"

    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇8〇()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    invoke-virtual {v1, v2, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    const-string v2, "cs_ept_d"

    .line 29
    .line 30
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->o〇0()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v3

    .line 34
    invoke-virtual {v1, v2, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    const-string v2, "category"

    .line 39
    .line 40
    invoke-virtual {v1, v2, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-static {}, Lcom/intsig/CsHosts;->〇〇888()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    new-instance v3, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v2, "//count/v2/match"

    .line 57
    .line 58
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    invoke-virtual {v1, v2}, Lcom/intsig/tianshu/ParamsBuilder;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    new-instance v2, Lorg/json/JSONObject;

    .line 70
    .line 71
    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 72
    .line 73
    .line 74
    const-string v3, "image_id"

    .line 75
    .line 76
    invoke-virtual {v2, v3, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 77
    .line 78
    .line 79
    const-string p0, "area"

    .line 80
    .line 81
    invoke-static {p2}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->O8(Landroid/graphics/Rect;)Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object p2

    .line 85
    invoke-virtual {v2, p0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 86
    .line 87
    .line 88
    const/4 p0, 0x0

    .line 89
    const/4 p2, 0x0

    .line 90
    const/4 v3, 0x1

    .line 91
    if-eqz p3, :cond_2

    .line 92
    .line 93
    if-ne p1, v3, :cond_0

    .line 94
    .line 95
    const/4 p1, 0x1

    .line 96
    goto :goto_0

    .line 97
    :cond_0
    const/4 p1, 0x0

    .line 98
    :goto_0
    if-eqz p1, :cond_1

    .line 99
    .line 100
    goto :goto_1

    .line 101
    :cond_1
    move-object p3, p0

    .line 102
    :goto_1
    if-eqz p3, :cond_2

    .line 103
    .line 104
    const-string p1, "examplar"

    .line 105
    .line 106
    invoke-static {p3}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->O8(Landroid/graphics/Rect;)Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object p3

    .line 110
    invoke-virtual {v2, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 111
    .line 112
    .line 113
    :cond_2
    :try_start_0
    invoke-static {v1}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    invoke-virtual {p1, v2}, Lcom/lzy/okgo/request/base/BodyRequest;->upJson(Lorg/json/JSONObject;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    check-cast p1, Lcom/lzy/okgo/request/PostRequest;

    .line 122
    .line 123
    const-string p3, "CUSTOM_CONNECT_TIMEOUT"

    .line 124
    .line 125
    const/16 v1, 0x2710

    .line 126
    .line 127
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v1

    .line 131
    invoke-virtual {p1, p3, v1}, Lcom/lzy/okgo/request/base/Request;->headers(Ljava/lang/String;Ljava/lang/String;)Lcom/lzy/okgo/request/base/Request;

    .line 132
    .line 133
    .line 134
    move-result-object p1

    .line 135
    check-cast p1, Lcom/lzy/okgo/request/PostRequest;

    .line 136
    .line 137
    invoke-virtual {p1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 138
    .line 139
    .line 140
    move-result-object p1

    .line 141
    invoke-virtual {p1}, Lokhttp3/Response;->〇oo〇()Z

    .line 142
    .line 143
    .line 144
    move-result p3

    .line 145
    if-eqz p3, :cond_3

    .line 146
    .line 147
    invoke-virtual {p1}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 148
    .line 149
    .line 150
    move-result-object p3

    .line 151
    if-eqz p3, :cond_3

    .line 152
    .line 153
    invoke-virtual {p1}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 154
    .line 155
    .line 156
    move-result-object p0

    .line 157
    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 158
    .line 159
    .line 160
    invoke-virtual {p0}, Lokhttp3/ResponseBody;->charStream()Ljava/io/Reader;

    .line 161
    .line 162
    .line 163
    move-result-object p0

    .line 164
    const-class p1, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;

    .line 165
    .line 166
    invoke-static {p0, p1}, Lcom/intsig/okgo/utils/GsonUtils;->〇o〇(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 167
    .line 168
    .line 169
    move-result-object p0

    .line 170
    check-cast p0, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;

    .line 171
    .line 172
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 173
    .line 174
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;->setUseUpCount(Ljava/lang/Boolean;)V

    .line 175
    .line 176
    .line 177
    goto :goto_2

    .line 178
    :cond_3
    invoke-virtual {p1}, Lokhttp3/Response;->〇O8o08O()I

    .line 179
    .line 180
    .line 181
    move-result p3

    .line 182
    const/16 v1, 0x196

    .line 183
    .line 184
    if-ne p3, v1, :cond_5

    .line 185
    .line 186
    invoke-virtual {p1}, Lokhttp3/Response;->o800o8O()Lokhttp3/Headers;

    .line 187
    .line 188
    .line 189
    move-result-object p3

    .line 190
    if-eqz p3, :cond_4

    .line 191
    .line 192
    const-string v2, "X-IS-Error-Code"

    .line 193
    .line 194
    invoke-virtual {p3, v2}, Lokhttp3/Headers;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object p3

    .line 198
    if-eqz p3, :cond_4

    .line 199
    .line 200
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 201
    .line 202
    .line 203
    move-result p3

    .line 204
    const/16 v2, 0x67

    .line 205
    .line 206
    if-ne p3, v2, :cond_4

    .line 207
    .line 208
    const/4 p2, 0x1

    .line 209
    :cond_4
    if-eqz p2, :cond_5

    .line 210
    .line 211
    new-instance p0, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;

    .line 212
    .line 213
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;-><init>()V

    .line 214
    .line 215
    .line 216
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 217
    .line 218
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;->setUseUpCount(Ljava/lang/Boolean;)V

    .line 219
    .line 220
    .line 221
    goto :goto_2

    .line 222
    :cond_5
    invoke-virtual {p1}, Lokhttp3/Response;->〇O8o08O()I

    .line 223
    .line 224
    .line 225
    move-result p1

    .line 226
    if-ne p1, v1, :cond_6

    .line 227
    .line 228
    new-instance p0, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;

    .line 229
    .line 230
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;-><init>()V

    .line 231
    .line 232
    .line 233
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 234
    .line 235
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;->setRequestError(Ljava/lang/Boolean;)V

    .line 236
    .line 237
    .line 238
    :cond_6
    :goto_2
    new-instance p1, Ljava/lang/StringBuilder;

    .line 239
    .line 240
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 241
    .line 242
    .line 243
    const-string p2, "matchApi success response is "

    .line 244
    .line 245
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    .line 247
    .line 248
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 249
    .line 250
    .line 251
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 252
    .line 253
    .line 254
    move-result-object p1

    .line 255
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    .line 257
    .line 258
    goto :goto_3

    .line 259
    :catch_0
    move-exception p0

    .line 260
    new-instance p1, Ljava/lang/StringBuilder;

    .line 261
    .line 262
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 263
    .line 264
    .line 265
    const-string p2, "matchApi error "

    .line 266
    .line 267
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    .line 269
    .line 270
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 271
    .line 272
    .line 273
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 274
    .line 275
    .line 276
    move-result-object p0

    .line 277
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    .line 279
    .line 280
    new-instance p0, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;

    .line 281
    .line 282
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;-><init>()V

    .line 283
    .line 284
    .line 285
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 286
    .line 287
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberApi$MatchDataResponse;->setRequestError(Ljava/lang/Boolean;)V

    .line 288
    .line 289
    .line 290
    :goto_3
    return-object p0
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method


# virtual methods
.method public final 〇o00〇〇Oo(Ljava/util/List;)Landroid/graphics/Rect;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroid/graphics/Rect;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Landroid/graphics/Rect;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object v2

    .line 10
    check-cast v2, Ljava/lang/Integer;

    .line 11
    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v2, 0x0

    .line 20
    :goto_0
    if-eqz p1, :cond_1

    .line 21
    .line 22
    const/4 v3, 0x1

    .line 23
    invoke-static {p1, v3}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    check-cast v3, Ljava/lang/Integer;

    .line 28
    .line 29
    if-eqz v3, :cond_1

    .line 30
    .line 31
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    goto :goto_1

    .line 36
    :cond_1
    const/4 v3, 0x0

    .line 37
    :goto_1
    if-eqz p1, :cond_2

    .line 38
    .line 39
    const/4 v4, 0x2

    .line 40
    invoke-static {p1, v4}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v4

    .line 44
    check-cast v4, Ljava/lang/Integer;

    .line 45
    .line 46
    if-eqz v4, :cond_2

    .line 47
    .line 48
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    .line 49
    .line 50
    .line 51
    move-result v4

    .line 52
    goto :goto_2

    .line 53
    :cond_2
    const/4 v4, 0x0

    .line 54
    :goto_2
    if-eqz p1, :cond_3

    .line 55
    .line 56
    const/4 v5, 0x3

    .line 57
    invoke-static {p1, v5}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    check-cast p1, Ljava/lang/Integer;

    .line 62
    .line 63
    if-eqz p1, :cond_3

    .line 64
    .line 65
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    :cond_3
    invoke-direct {v0, v2, v3, v4, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 70
    .line 71
    .line 72
    return-object v0
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

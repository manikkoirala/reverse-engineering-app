.class public final Lcom/intsig/camscanner/capture/count/widget/CountNumberView;
.super Lcom/intsig/camscanner/view/ImageViewTouchBase;
.source "CountNumberView.kt"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/count/widget/CountNumberView$OnClickImageListener;,
        Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;,
        Lcom/intsig/camscanner/capture/count/widget/CountNumberView$WhenMappings;,
        Lcom/intsig/camscanner/capture/count/widget/CountNumberView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8〇o88:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O88O:Z

.field private O8o〇O0:F

.field private final O8〇o〇88:Landroid/view/GestureDetector;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO〇OOo:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo0O0o8:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field private Oo0〇Ooo:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo80:Ljava/lang/String;

.field private final Ooo08:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O〇08oOOO0:Landroid/graphics/RectF;

.field private O〇O:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$OnClickImageListener;

.field private O〇o88o08〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private final o0OoOOo0:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8o:Landroid/view/ScaleGestureDetector;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oO00〇o:Z

.field private oOO0880O:Z

.field private final oOO8:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$gestureListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOO〇〇:Landroid/graphics/Bitmap;

.field private oOoo80oO:Z

.field private final oO〇8O8oOo:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oo8ooo8O:Lcom/intsig/camscanner/capture/count/interfaces/ICountView;

.field private ooO:F

.field private o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

.field private o〇o〇Oo88:F

.field private 〇00O0:[F

.field private 〇08〇o0O:[I
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O〇O00O:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇800OO〇0O:F

.field private final 〇OO8ooO8〇:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OO〇00〇0O:F

.field private final 〇oo〇O〇80:Landroid/widget/OverScroller;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇o〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private 〇〇〇0o〇〇0:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇8〇o88:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 7
    .line 8
    .line 9
    new-instance p2, Landroid/view/ScaleGestureDetector;

    .line 10
    .line 11
    invoke-direct {p2, p1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    .line 12
    .line 13
    .line 14
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o8o:Landroid/view/ScaleGestureDetector;

    .line 15
    .line 16
    const/4 p2, 0x2

    .line 17
    new-array p2, p2, [I

    .line 18
    .line 19
    fill-array-data p2, :array_0

    .line 20
    .line 21
    .line 22
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 23
    .line 24
    sget-object p2, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 25
    .line 26
    new-instance v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$mCountLocationSelector$2;

    .line 27
    .line 28
    invoke-direct {v0, p1, p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$mCountLocationSelector$2;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/capture/count/widget/CountNumberView;)V

    .line 29
    .line 30
    .line 31
    invoke-static {p2, v0}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o8〇OO:Lkotlin/Lazy;

    .line 36
    .line 37
    new-instance v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$mCountLoadingDrawer$2;

    .line 38
    .line 39
    invoke-direct {v0, p1, p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$mCountLoadingDrawer$2;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/capture/count/widget/CountNumberView;)V

    .line 40
    .line 41
    .line 42
    invoke-static {p2, v0}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Ooo08:Lkotlin/Lazy;

    .line 47
    .line 48
    new-instance v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$mCountResultPainter$2;

    .line 49
    .line 50
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$mCountResultPainter$2;-><init>(Lcom/intsig/camscanner/capture/count/widget/CountNumberView;)V

    .line 51
    .line 52
    .line 53
    invoke-static {p2, v0}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 54
    .line 55
    .line 56
    move-result-object p2

    .line 57
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇OO8ooO8〇:Lkotlin/Lazy;

    .line 58
    .line 59
    new-instance p2, Landroid/graphics/Matrix;

    .line 60
    .line 61
    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    .line 62
    .line 63
    .line 64
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo0〇Ooo:Landroid/graphics/Matrix;

    .line 65
    .line 66
    new-instance p2, Landroid/graphics/RectF;

    .line 67
    .line 68
    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    .line 69
    .line 70
    .line 71
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇〇0o〇〇0:Landroid/graphics/RectF;

    .line 72
    .line 73
    new-instance p2, Landroid/graphics/RectF;

    .line 74
    .line 75
    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    .line 76
    .line 77
    .line 78
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 79
    .line 80
    new-instance p2, Landroid/graphics/Path;

    .line 81
    .line 82
    invoke-direct {p2}, Landroid/graphics/Path;-><init>()V

    .line 83
    .line 84
    .line 85
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇0O〇O00O:Landroid/graphics/Path;

    .line 86
    .line 87
    new-instance p2, Landroid/graphics/Path;

    .line 88
    .line 89
    invoke-direct {p2}, Landroid/graphics/Path;-><init>()V

    .line 90
    .line 91
    .line 92
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o0OoOOo0:Landroid/graphics/Path;

    .line 93
    .line 94
    const/4 p2, 0x1

    .line 95
    iput-boolean p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oOO0880O:Z

    .line 96
    .line 97
    const-string p2, "#CC000000"

    .line 98
    .line 99
    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 100
    .line 101
    .line 102
    move-result p2

    .line 103
    iput p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo0O0o8:I

    .line 104
    .line 105
    new-instance p2, Landroid/graphics/Paint;

    .line 106
    .line 107
    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    .line 108
    .line 109
    .line 110
    iget v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo0O0o8:I

    .line 111
    .line 112
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 113
    .line 114
    .line 115
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 116
    .line 117
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 118
    .line 119
    .line 120
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->OO〇OOo:Landroid/graphics/Paint;

    .line 121
    .line 122
    const/high16 p2, 0x41c00000    # 24.0f

    .line 123
    .line 124
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 125
    .line 126
    .line 127
    move-result p2

    .line 128
    int-to-float p2, p2

    .line 129
    iput p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇800OO〇0O:F

    .line 130
    .line 131
    const/high16 p2, 0x42000000    # 32.0f

    .line 132
    .line 133
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 134
    .line 135
    .line 136
    move-result p2

    .line 137
    int-to-float p2, p2

    .line 138
    iput p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O8o〇O0:F

    .line 139
    .line 140
    new-instance p2, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$gestureListener$1;

    .line 141
    .line 142
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$gestureListener$1;-><init>(Lcom/intsig/camscanner/capture/count/widget/CountNumberView;)V

    .line 143
    .line 144
    .line 145
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oOO8:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$gestureListener$1;

    .line 146
    .line 147
    new-instance v0, Landroid/widget/OverScroller;

    .line 148
    .line 149
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    .line 150
    .line 151
    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 152
    .line 153
    .line 154
    invoke-direct {v0, p1, v1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 155
    .line 156
    .line 157
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇oo〇O〇80:Landroid/widget/OverScroller;

    .line 158
    .line 159
    new-instance v0, Landroid/view/GestureDetector;

    .line 160
    .line 161
    invoke-direct {v0, p1, p2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 162
    .line 163
    .line 164
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O8〇o〇88:Landroid/view/GestureDetector;

    .line 165
    .line 166
    return-void

    .line 167
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private final O000()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 4
    .line 5
    if-eqz v1, :cond_1

    .line 6
    .line 7
    move-object v1, v0

    .line 8
    check-cast v1, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O8o08O()Landroid/graphics/RectF;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    new-instance v3, Landroid/graphics/RectF;

    .line 15
    .line 16
    invoke-direct {v3, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 17
    .line 18
    .line 19
    new-instance v2, Landroid/graphics/Matrix;

    .line 20
    .line 21
    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 22
    .line 23
    .line 24
    iget-object v4, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo0〇Ooo:Landroid/graphics/Matrix;

    .line 25
    .line 26
    invoke-virtual {v4, v2}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 27
    .line 28
    .line 29
    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 37
    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getImageBound()Landroid/graphics/RectF;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    if-eqz v2, :cond_0

    .line 44
    .line 45
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oo08(Landroid/graphics/RectF;)V

    .line 46
    .line 47
    .line 48
    :cond_0
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OoO8(Landroid/graphics/RectF;)[F

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O888o0o([F)V

    .line 53
    .line 54
    .line 55
    :cond_1
    instance-of v1, v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;

    .line 56
    .line 57
    if-eqz v1, :cond_2

    .line 58
    .line 59
    move-object v1, v0

    .line 60
    check-cast v1, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;

    .line 61
    .line 62
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo0〇Ooo:Landroid/graphics/Matrix;

    .line 63
    .line 64
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 65
    .line 66
    .line 67
    move-result-object v3

    .line 68
    const-string v4, "imageViewMatrix"

    .line 69
    .line 70
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;->O8ooOoo〇(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    .line 74
    .line 75
    .line 76
    :cond_2
    instance-of v1, v0, Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;

    .line 77
    .line 78
    if-eqz v1, :cond_3

    .line 79
    .line 80
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getImageBound()Landroid/graphics/RectF;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    if-eqz v1, :cond_3

    .line 85
    .line 86
    check-cast v0, Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;

    .line 87
    .line 88
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;->Oo08(Landroid/graphics/RectF;)V

    .line 89
    .line 90
    .line 91
    :cond_3
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static final synthetic O8ooOoo〇(Lcom/intsig/camscanner/capture/count/widget/CountNumberView;)Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final O8〇o(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oOO〇〇:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    aget v3, v1, v2

    .line 10
    .line 11
    if-lez v3, :cond_2

    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    aget v1, v1, v3

    .line 15
    .line 16
    if-lez v1, :cond_2

    .line 17
    .line 18
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-lez v1, :cond_2

    .line 23
    .line 24
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-gtz v1, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    int-to-float v1, v1

    .line 36
    const/high16 v4, 0x3f800000    # 1.0f

    .line 37
    .line 38
    mul-float v1, v1, v4

    .line 39
    .line 40
    iget-object v5, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 41
    .line 42
    aget v2, v5, v2

    .line 43
    .line 44
    int-to-float v2, v2

    .line 45
    div-float/2addr v1, v2

    .line 46
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    int-to-float v0, v0

    .line 51
    mul-float v0, v0, v4

    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 54
    .line 55
    aget v2, v2, v3

    .line 56
    .line 57
    int-to-float v2, v2

    .line 58
    div-float/2addr v0, v2

    .line 59
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    .line 60
    .line 61
    .line 62
    new-instance v2, Landroid/graphics/Matrix;

    .line 63
    .line 64
    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 68
    .line 69
    .line 70
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 71
    .line 72
    .line 73
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 74
    .line 75
    .line 76
    move-result-object p2

    .line 77
    invoke-virtual {p2, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 78
    .line 79
    .line 80
    :cond_2
    :goto_0
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private final Oo8Oo00oo(FF)[F
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getOriMatrixInCurrentView()Landroid/graphics/Matrix;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    return-object p1

    .line 9
    :cond_0
    const/4 v1, 0x2

    .line 10
    new-array v2, v1, [F

    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    aput p1, v2, v3

    .line 14
    .line 15
    const/4 p1, 0x1

    .line 16
    aput p2, v2, p1

    .line 17
    .line 18
    new-array p1, v1, [F

    .line 19
    .line 20
    fill-array-data p1, :array_0

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, p1, v2}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 24
    .line 25
    .line 26
    return-object p1

    .line 27
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic O〇8O8〇008(Lcom/intsig/camscanner/capture/count/widget/CountNumberView;ZZ)Landroid/graphics/PointF;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇0(ZZ)Landroid/graphics/PointF;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private final getImageBound()Landroid/graphics/RectF;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oOO〇〇:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    new-instance v1, Landroid/graphics/RectF;

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    int-to-float v2, v2

    .line 14
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    int-to-float v0, v0

    .line 19
    const/4 v3, 0x0

    .line 20
    invoke-direct {v1, v3, v3, v2, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    int-to-float v0, v0

    .line 35
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    int-to-float v2, v2

    .line 40
    cmpl-float v4, v0, v3

    .line 41
    .line 42
    if-lez v4, :cond_1

    .line 43
    .line 44
    cmpl-float v4, v2, v3

    .line 45
    .line 46
    if-lez v4, :cond_1

    .line 47
    .line 48
    iget v4, v1, Landroid/graphics/RectF;->left:F

    .line 49
    .line 50
    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    .line 51
    .line 52
    .line 53
    move-result v4

    .line 54
    iget v5, v1, Landroid/graphics/RectF;->top:F

    .line 55
    .line 56
    invoke-static {v3, v5}, Ljava/lang/Math;->max(FF)F

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    iget v5, v1, Landroid/graphics/RectF;->right:F

    .line 61
    .line 62
    invoke-static {v0, v5}, Ljava/lang/Math;->min(FF)F

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    iget v5, v1, Landroid/graphics/RectF;->bottom:F

    .line 67
    .line 68
    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    invoke-virtual {v1, v4, v3, v0, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 73
    .line 74
    .line 75
    :cond_1
    return-object v1
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final getMCountLoadingDrawer()Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Ooo08:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final getMCountLocationSelector()Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o8〇OO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final getMCountResultPainter()Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇OO8ooO8〇:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final getOriMatrixInCurrentView()Landroid/graphics/Matrix;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oOO〇〇:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return-object v1

    .line 7
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    aget v4, v2, v3

    .line 11
    .line 12
    if-lez v4, :cond_2

    .line 13
    .line 14
    const/4 v4, 0x1

    .line 15
    aget v2, v2, v4

    .line 16
    .line 17
    if-lez v2, :cond_2

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-lez v2, :cond_2

    .line 24
    .line 25
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    if-gtz v2, :cond_1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 33
    .line 34
    aget v1, v1, v3

    .line 35
    .line 36
    int-to-float v1, v1

    .line 37
    const/high16 v2, 0x3f800000    # 1.0f

    .line 38
    .line 39
    mul-float v1, v1, v2

    .line 40
    .line 41
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    int-to-float v3, v3

    .line 46
    div-float/2addr v1, v3

    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 48
    .line 49
    aget v3, v3, v4

    .line 50
    .line 51
    int-to-float v3, v3

    .line 52
    mul-float v3, v3, v2

    .line 53
    .line 54
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    int-to-float v0, v0

    .line 59
    div-float/2addr v3, v0

    .line 60
    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    new-instance v1, Landroid/graphics/Matrix;

    .line 65
    .line 66
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 67
    .line 68
    .line 69
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 74
    .line 75
    .line 76
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 77
    .line 78
    .line 79
    :cond_2
    :goto_0
    return-object v1
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final o8([I)F
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    aget v1, p1, v0

    .line 3
    .line 4
    const/high16 v2, 0x3f800000    # 1.0f

    .line 5
    .line 6
    if-lez v1, :cond_1

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    aget v3, p1, v1

    .line 10
    .line 11
    if-gtz v3, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/high16 v3, 0x43b40000    # 360.0f

    .line 15
    .line 16
    invoke-static {v3}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    const/high16 v4, 0x43f00000    # 480.0f

    .line 21
    .line 22
    invoke-static {v4}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 23
    .line 24
    .line 25
    move-result v4

    .line 26
    aget v0, p1, v0

    .line 27
    .line 28
    int-to-float v0, v0

    .line 29
    mul-float v0, v0, v2

    .line 30
    .line 31
    int-to-float v3, v3

    .line 32
    div-float/2addr v0, v3

    .line 33
    aget p1, p1, v1

    .line 34
    .line 35
    int-to-float p1, p1

    .line 36
    mul-float p1, p1, v2

    .line 37
    .line 38
    int-to-float v1, v4

    .line 39
    div-float/2addr p1, v1

    .line 40
    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    return p1

    .line 45
    :cond_1
    :goto_0
    return v2
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final oO(FF)V
    .locals 6

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08O8o〇0(FF)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 9
    .line 10
    instance-of v1, v0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 11
    .line 12
    if-eqz v1, :cond_2

    .line 13
    .line 14
    iget-boolean v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O88O:Z

    .line 15
    .line 16
    if-nez v2, :cond_2

    .line 17
    .line 18
    move-object v2, v0

    .line 19
    check-cast v2, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OO0o〇〇()Z

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    if-nez v3, :cond_2

    .line 26
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇8(FF)Landroid/graphics/RectF;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    if-eqz v3, :cond_2

    .line 32
    .line 33
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    .line 38
    .line 39
    .line 40
    move-result v5

    .line 41
    invoke-direct {p0, v4, v5}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo8Oo00oo(FF)[F

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    iput-object v4, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇00O0:[F

    .line 46
    .line 47
    iget-object v4, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oo8ooo8O:Lcom/intsig/camscanner/capture/count/interfaces/ICountView;

    .line 48
    .line 49
    if-eqz v4, :cond_1

    .line 50
    .line 51
    invoke-interface {v4}, Lcom/intsig/camscanner/capture/count/interfaces/ICountView;->ooo0〇〇O()V

    .line 52
    .line 53
    .line 54
    :cond_1
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OoO8(Landroid/graphics/RectF;)[F

    .line 55
    .line 56
    .line 57
    move-result-object v3

    .line 58
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O888o0o([F)V

    .line 59
    .line 60
    .line 61
    :cond_2
    if-eqz v1, :cond_3

    .line 62
    .line 63
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O88O:Z

    .line 64
    .line 65
    if-nez v1, :cond_3

    .line 66
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇0o(FF)Z

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    if-eqz v1, :cond_3

    .line 72
    .line 73
    check-cast v0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 74
    .line 75
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇808〇()Z

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    if-nez v1, :cond_3

    .line 80
    .line 81
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇80〇808〇O(FF)Z

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    if-nez v1, :cond_3

    .line 86
    .line 87
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇8(FF)Landroid/graphics/RectF;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    if-eqz p1, :cond_3

    .line 92
    .line 93
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    .line 94
    .line 95
    .line 96
    move-result p2

    .line 97
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    .line 98
    .line 99
    .line 100
    move-result v1

    .line 101
    invoke-direct {p0, p2, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo8Oo00oo(FF)[F

    .line 102
    .line 103
    .line 104
    move-result-object p2

    .line 105
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇00O0:[F

    .line 106
    .line 107
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OoO8(Landroid/graphics/RectF;)[F

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O888o0o([F)V

    .line 112
    .line 113
    .line 114
    :cond_3
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private static final oO00OOO(ILcom/intsig/camscanner/capture/count/widget/CountNumberView;Landroid/animation/ValueAnimator;ILandroid/animation/ValueAnimator;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "animation"

    .line 7
    .line 8
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p4}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p4

    .line 15
    const-string v0, "null cannot be cast to non-null type kotlin.Int"

    .line 16
    .line 17
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    check-cast p4, Ljava/lang/Integer;

    .line 21
    .line 22
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    .line 23
    .line 24
    .line 25
    move-result p4

    .line 26
    int-to-float v0, p4

    .line 27
    int-to-float v1, p0

    .line 28
    div-float/2addr v0, v1

    .line 29
    const/high16 v1, 0x3f800000    # 1.0f

    .line 30
    .line 31
    sub-float/2addr v1, v0

    .line 32
    if-lt p4, p0, :cond_0

    .line 33
    .line 34
    const/4 p0, 0x0

    .line 35
    iput-boolean p0, p1, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 36
    .line 37
    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->cancel()V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 41
    .line 42
    .line 43
    :cond_0
    iget-object p0, p1, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->OO〇OOo:Landroid/graphics/Paint;

    .line 44
    .line 45
    int-to-float p2, p3

    .line 46
    mul-float p2, p2, v1

    .line 47
    .line 48
    invoke-static {p2}, Lkotlin/math/MathKt;->〇o00〇〇Oo(F)I

    .line 49
    .line 50
    .line 51
    move-result p2

    .line 52
    invoke-virtual {p0, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
.end method

.method private final oo〇(Ljava/util/List;Z)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;Z)",
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oOO〇〇:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return-object v1

    .line 7
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    aget v4, v2, v3

    .line 11
    .line 12
    if-lez v4, :cond_4

    .line 13
    .line 14
    const/4 v4, 0x1

    .line 15
    aget v2, v2, v4

    .line 16
    .line 17
    if-lez v2, :cond_4

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-lez v2, :cond_4

    .line 24
    .line 25
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    if-gtz v2, :cond_1

    .line 30
    .line 31
    goto/16 :goto_2

    .line 32
    .line 33
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    int-to-float v1, v1

    .line 38
    const/high16 v2, 0x3f800000    # 1.0f

    .line 39
    .line 40
    mul-float v1, v1, v2

    .line 41
    .line 42
    iget-object v5, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 43
    .line 44
    aget v5, v5, v3

    .line 45
    .line 46
    int-to-float v5, v5

    .line 47
    div-float/2addr v1, v5

    .line 48
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    int-to-float v0, v0

    .line 53
    mul-float v0, v0, v2

    .line 54
    .line 55
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 56
    .line 57
    aget v2, v2, v4

    .line 58
    .line 59
    int-to-float v2, v2

    .line 60
    div-float/2addr v0, v2

    .line 61
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    new-instance v1, Landroid/graphics/Matrix;

    .line 66
    .line 67
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 68
    .line 69
    .line 70
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 71
    .line 72
    .line 73
    new-instance v0, Ljava/util/ArrayList;

    .line 74
    .line 75
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .line 77
    .line 78
    check-cast p1, Ljava/lang/Iterable;

    .line 79
    .line 80
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    if-eqz v2, :cond_3

    .line 89
    .line 90
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    check-cast v2, Landroid/graphics/Rect;

    .line 95
    .line 96
    new-instance v5, Landroid/graphics/RectF;

    .line 97
    .line 98
    invoke-direct {v5, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 99
    .line 100
    .line 101
    if-eqz p2, :cond_2

    .line 102
    .line 103
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountResultPainter()Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;

    .line 104
    .line 105
    .line 106
    move-result-object v2

    .line 107
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;->〇0〇O0088o()F

    .line 108
    .line 109
    .line 110
    move-result v2

    .line 111
    const/4 v6, 0x2

    .line 112
    new-array v6, v6, [F

    .line 113
    .line 114
    invoke-virtual {v5}, Landroid/graphics/RectF;->centerX()F

    .line 115
    .line 116
    .line 117
    move-result v7

    .line 118
    aput v7, v6, v3

    .line 119
    .line 120
    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    .line 121
    .line 122
    .line 123
    move-result v7

    .line 124
    aput v7, v6, v4

    .line 125
    .line 126
    invoke-virtual {v1, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 127
    .line 128
    .line 129
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 130
    .line 131
    .line 132
    move-result-object v7

    .line 133
    invoke-virtual {v7, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 134
    .line 135
    .line 136
    aget v7, v6, v3

    .line 137
    .line 138
    sub-float v8, v7, v2

    .line 139
    .line 140
    iput v8, v5, Landroid/graphics/RectF;->left:F

    .line 141
    .line 142
    aget v6, v6, v4

    .line 143
    .line 144
    sub-float v8, v6, v2

    .line 145
    .line 146
    iput v8, v5, Landroid/graphics/RectF;->top:F

    .line 147
    .line 148
    add-float/2addr v7, v2

    .line 149
    iput v7, v5, Landroid/graphics/RectF;->right:F

    .line 150
    .line 151
    add-float/2addr v6, v2

    .line 152
    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 153
    .line 154
    goto :goto_1

    .line 155
    :cond_2
    invoke-virtual {v1, v5}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 156
    .line 157
    .line 158
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 159
    .line 160
    .line 161
    move-result-object v2

    .line 162
    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 163
    .line 164
    .line 165
    :goto_1
    new-instance v2, Landroid/graphics/Rect;

    .line 166
    .line 167
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 168
    .line 169
    .line 170
    invoke-virtual {v5, v2}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 171
    .line 172
    .line 173
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    .line 175
    .line 176
    goto :goto_0

    .line 177
    :cond_3
    return-object v0

    .line 178
    :cond_4
    :goto_2
    return-object v1
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private final o〇8(FF)Landroid/graphics/RectF;
    .locals 9

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getImageBound()Landroid/graphics/RectF;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return-object v1

    .line 9
    :cond_0
    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    const-string v3, "CountNumberView"

    .line 14
    .line 15
    if-nez v2, :cond_1

    .line 16
    .line 17
    const-string p1, "findViewRectF: over imageBound"

    .line 18
    .line 19
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-object v1

    .line 23
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇o88o08〇:Ljava/util/List;

    .line 24
    .line 25
    iget-object v4, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇o〇:Ljava/util/List;

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    const/high16 v6, 0x3f800000    # 1.0f

    .line 32
    .line 33
    const/4 v7, 0x1

    .line 34
    const/4 v8, 0x0

    .line 35
    cmpg-float v5, v5, v6

    .line 36
    .line 37
    if-nez v5, :cond_2

    .line 38
    .line 39
    const/4 v5, 0x1

    .line 40
    goto :goto_0

    .line 41
    :cond_2
    const/4 v5, 0x0

    .line 42
    :goto_0
    if-nez v5, :cond_7

    .line 43
    .line 44
    move-object v5, v2

    .line 45
    check-cast v5, Ljava/util/Collection;

    .line 46
    .line 47
    if-eqz v5, :cond_4

    .line 48
    .line 49
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    .line 50
    .line 51
    .line 52
    move-result v5

    .line 53
    if-eqz v5, :cond_3

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_3
    const/4 v5, 0x0

    .line 57
    goto :goto_2

    .line 58
    :cond_4
    :goto_1
    const/4 v5, 0x1

    .line 59
    :goto_2
    if-nez v5, :cond_7

    .line 60
    .line 61
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 62
    .line 63
    .line 64
    move-result v5

    .line 65
    const/16 v6, 0x64

    .line 66
    .line 67
    if-gt v5, v6, :cond_7

    .line 68
    .line 69
    invoke-direct {p0, v2, v8}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oo〇(Ljava/util/List;Z)Ljava/util/List;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    move-object v5, v2

    .line 74
    check-cast v5, Ljava/util/Collection;

    .line 75
    .line 76
    if-eqz v5, :cond_6

    .line 77
    .line 78
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    .line 79
    .line 80
    .line 81
    move-result v5

    .line 82
    if-eqz v5, :cond_5

    .line 83
    .line 84
    goto :goto_3

    .line 85
    :cond_5
    const/4 v5, 0x0

    .line 86
    goto :goto_4

    .line 87
    :cond_6
    :goto_3
    const/4 v5, 0x1

    .line 88
    :goto_4
    if-nez v5, :cond_7

    .line 89
    .line 90
    move-object v4, v2

    .line 91
    :cond_7
    iget v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇800OO〇0O:F

    .line 92
    .line 93
    move-object v5, v4

    .line 94
    check-cast v5, Ljava/util/Collection;

    .line 95
    .line 96
    if-eqz v5, :cond_9

    .line 97
    .line 98
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    .line 99
    .line 100
    .line 101
    move-result v5

    .line 102
    if-eqz v5, :cond_8

    .line 103
    .line 104
    goto :goto_5

    .line 105
    :cond_8
    const/4 v7, 0x0

    .line 106
    :cond_9
    :goto_5
    if-nez v7, :cond_d

    .line 107
    .line 108
    iget-boolean v5, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oOoo80oO:Z

    .line 109
    .line 110
    if-eqz v5, :cond_d

    .line 111
    .line 112
    check-cast v4, Ljava/lang/Iterable;

    .line 113
    .line 114
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 115
    .line 116
    .line 117
    move-result-object v4

    .line 118
    :cond_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 119
    .line 120
    .line 121
    move-result v5

    .line 122
    if-eqz v5, :cond_b

    .line 123
    .line 124
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 125
    .line 126
    .line 127
    move-result-object v5

    .line 128
    move-object v6, v5

    .line 129
    check-cast v6, Landroid/graphics/Rect;

    .line 130
    .line 131
    float-to-int v7, p1

    .line 132
    float-to-int v8, p2

    .line 133
    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    .line 134
    .line 135
    .line 136
    move-result v6

    .line 137
    if-eqz v6, :cond_a

    .line 138
    .line 139
    move-object v1, v5

    .line 140
    :cond_b
    check-cast v1, Landroid/graphics/Rect;

    .line 141
    .line 142
    if-eqz v1, :cond_c

    .line 143
    .line 144
    invoke-virtual {v1}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object p1

    .line 148
    new-instance p2, Ljava/lang/StringBuilder;

    .line 149
    .line 150
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    .line 152
    .line 153
    const-string v2, "findViewRectF: algorithmRect: "

    .line 154
    .line 155
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    .line 160
    .line 161
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object p1

    .line 165
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    iget p1, v1, Landroid/graphics/Rect;->left:I

    .line 169
    .line 170
    int-to-float p1, p1

    .line 171
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    .line 172
    .line 173
    .line 174
    move-result p2

    .line 175
    int-to-float p2, p2

    .line 176
    const/high16 v2, 0x40000000    # 2.0f

    .line 177
    .line 178
    div-float/2addr p2, v2

    .line 179
    add-float/2addr p1, p2

    .line 180
    iget p2, v1, Landroid/graphics/Rect;->top:I

    .line 181
    .line 182
    int-to-float p2, p2

    .line 183
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    .line 184
    .line 185
    .line 186
    move-result v4

    .line 187
    int-to-float v4, v4

    .line 188
    div-float/2addr v4, v2

    .line 189
    add-float/2addr p2, v4

    .line 190
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    .line 191
    .line 192
    .line 193
    move-result v4

    .line 194
    int-to-float v4, v4

    .line 195
    div-float/2addr v4, v2

    .line 196
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    .line 197
    .line 198
    .line 199
    move-result v1

    .line 200
    int-to-float v1, v1

    .line 201
    div-float v2, v1, v2

    .line 202
    .line 203
    move v1, v2

    .line 204
    move v2, v4

    .line 205
    goto :goto_6

    .line 206
    :cond_c
    const-string v1, "findViewRectF not found algorithmRect:"

    .line 207
    .line 208
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    .line 210
    .line 211
    :cond_d
    move v1, v2

    .line 212
    :goto_6
    iget v4, v0, Landroid/graphics/RectF;->left:F

    .line 213
    .line 214
    sub-float v5, p1, v2

    .line 215
    .line 216
    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    .line 217
    .line 218
    .line 219
    move-result v4

    .line 220
    iget v5, v0, Landroid/graphics/RectF;->top:F

    .line 221
    .line 222
    sub-float v6, p2, v1

    .line 223
    .line 224
    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    .line 225
    .line 226
    .line 227
    move-result v5

    .line 228
    iget v6, v0, Landroid/graphics/RectF;->right:F

    .line 229
    .line 230
    add-float/2addr p1, v2

    .line 231
    invoke-static {v6, p1}, Ljava/lang/Math;->min(FF)F

    .line 232
    .line 233
    .line 234
    move-result p1

    .line 235
    iget v6, v0, Landroid/graphics/RectF;->bottom:F

    .line 236
    .line 237
    add-float/2addr p2, v1

    .line 238
    invoke-static {v6, p2}, Ljava/lang/Math;->min(FF)F

    .line 239
    .line 240
    .line 241
    move-result p2

    .line 242
    new-instance v6, Landroid/graphics/RectF;

    .line 243
    .line 244
    invoke-direct {v6, v4, v5, p1, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 245
    .line 246
    .line 247
    invoke-virtual {v6}, Landroid/graphics/RectF;->toShortString()Ljava/lang/String;

    .line 248
    .line 249
    .line 250
    move-result-object p1

    .line 251
    new-instance p2, Ljava/lang/StringBuilder;

    .line 252
    .line 253
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 254
    .line 255
    .line 256
    const-string v4, "findViewRectF: recheck before: "

    .line 257
    .line 258
    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    .line 260
    .line 261
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    .line 263
    .line 264
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 265
    .line 266
    .line 267
    move-result-object p1

    .line 268
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    .line 272
    .line 273
    .line 274
    move-result p1

    .line 275
    const/4 p2, 0x2

    .line 276
    int-to-float p2, p2

    .line 277
    mul-float v2, v2, p2

    .line 278
    .line 279
    cmpg-float p1, p1, v2

    .line 280
    .line 281
    if-gez p1, :cond_f

    .line 282
    .line 283
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    .line 284
    .line 285
    .line 286
    move-result p1

    .line 287
    sub-float/2addr v2, p1

    .line 288
    iget p1, v6, Landroid/graphics/RectF;->left:F

    .line 289
    .line 290
    iget v4, v0, Landroid/graphics/RectF;->left:F

    .line 291
    .line 292
    cmpl-float v5, p1, v4

    .line 293
    .line 294
    if-lez v5, :cond_e

    .line 295
    .line 296
    sub-float/2addr p1, v2

    .line 297
    invoke-static {v4, p1}, Ljava/lang/Math;->max(FF)F

    .line 298
    .line 299
    .line 300
    move-result p1

    .line 301
    iput p1, v6, Landroid/graphics/RectF;->left:F

    .line 302
    .line 303
    goto :goto_7

    .line 304
    :cond_e
    iget p1, v0, Landroid/graphics/RectF;->right:F

    .line 305
    .line 306
    iget v4, v6, Landroid/graphics/RectF;->right:F

    .line 307
    .line 308
    add-float/2addr v4, v2

    .line 309
    invoke-static {p1, v4}, Ljava/lang/Math;->min(FF)F

    .line 310
    .line 311
    .line 312
    move-result p1

    .line 313
    iput p1, v6, Landroid/graphics/RectF;->right:F

    .line 314
    .line 315
    :cond_f
    :goto_7
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    .line 316
    .line 317
    .line 318
    move-result p1

    .line 319
    mul-float v1, v1, p2

    .line 320
    .line 321
    cmpg-float p1, p1, v1

    .line 322
    .line 323
    if-gez p1, :cond_11

    .line 324
    .line 325
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    .line 326
    .line 327
    .line 328
    move-result p1

    .line 329
    sub-float/2addr v1, p1

    .line 330
    iget p1, v6, Landroid/graphics/RectF;->top:F

    .line 331
    .line 332
    iget p2, v0, Landroid/graphics/RectF;->top:F

    .line 333
    .line 334
    cmpl-float v2, p1, p2

    .line 335
    .line 336
    if-lez v2, :cond_10

    .line 337
    .line 338
    sub-float/2addr p1, v1

    .line 339
    invoke-static {p2, p1}, Ljava/lang/Math;->max(FF)F

    .line 340
    .line 341
    .line 342
    move-result p1

    .line 343
    iput p1, v6, Landroid/graphics/RectF;->top:F

    .line 344
    .line 345
    goto :goto_8

    .line 346
    :cond_10
    iget p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 347
    .line 348
    iget p2, v6, Landroid/graphics/RectF;->bottom:F

    .line 349
    .line 350
    add-float/2addr p2, v1

    .line 351
    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    .line 352
    .line 353
    .line 354
    move-result p1

    .line 355
    iput p1, v6, Landroid/graphics/RectF;->bottom:F

    .line 356
    .line 357
    :cond_11
    :goto_8
    invoke-virtual {v6}, Landroid/graphics/RectF;->toShortString()Ljava/lang/String;

    .line 358
    .line 359
    .line 360
    move-result-object p1

    .line 361
    new-instance p2, Ljava/lang/StringBuilder;

    .line 362
    .line 363
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 364
    .line 365
    .line 366
    const-string v0, "findViewRectF: recheck after: "

    .line 367
    .line 368
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    .line 370
    .line 371
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    .line 373
    .line 374
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 375
    .line 376
    .line 377
    move-result-object p1

    .line 378
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    .line 380
    .line 381
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 382
    .line 383
    invoke-virtual {p1, v6}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    .line 384
    .line 385
    .line 386
    move-result p1

    .line 387
    if-nez p1, :cond_12

    .line 388
    .line 389
    iget-boolean p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 390
    .line 391
    if-eqz p1, :cond_12

    .line 392
    .line 393
    const-string p1, "findViewRectF:  targetRectF is outside"

    .line 394
    .line 395
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    .line 397
    .line 398
    sget-object p1, Lcom/intsig/camscanner/util/GraphicUtils;->〇080:Lcom/intsig/camscanner/util/GraphicUtils;

    .line 399
    .line 400
    iget-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 401
    .line 402
    invoke-virtual {p1, v6, p2}, Lcom/intsig/camscanner/util/GraphicUtils;->〇080(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 403
    .line 404
    .line 405
    :cond_12
    return-object v6
.end method

.method private final o〇O()V
    .locals 4

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [I

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->OO〇OOo:Landroid/graphics/Paint;

    .line 12
    .line 13
    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    const-wide/16 v2, 0x96

    .line 20
    .line 21
    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 22
    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 26
    .line 27
    .line 28
    const/4 v2, 0x1

    .line 29
    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 30
    .line 31
    .line 32
    new-instance v2, LOo08OO8oO/〇〇888;

    .line 33
    .line 34
    const/16 v3, 0x64

    .line 35
    .line 36
    invoke-direct {v2, v3, p0, v0, v1}, LOo08OO8oO/〇〇888;-><init>(ILcom/intsig/camscanner/capture/count/widget/CountNumberView;Landroid/animation/ValueAnimator;I)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 40
    .line 41
    .line 42
    :cond_0
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 43
    .line 44
    .line 45
    return-void

    .line 46
    nop

    .line 47
    :array_0
    .array-data 4
        0x0
        0x64
    .end array-data
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static final synthetic o〇〇0〇(Lcom/intsig/camscanner/capture/count/widget/CountNumberView;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O000()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇00(ILcom/intsig/camscanner/capture/count/widget/CountNumberView;Landroid/animation/ValueAnimator;ILandroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00OOO(ILcom/intsig/camscanner/capture/count/widget/CountNumberView;Landroid/animation/ValueAnimator;ILandroid/animation/ValueAnimator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
.end method

.method public static final synthetic 〇0000OOO(Lcom/intsig/camscanner/capture/count/widget/CountNumberView;FF)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇O00(FF)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private final 〇00〇8(Landroid/content/Context;[I)Landroid/graphics/Bitmap;
    .locals 18

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    move-object/from16 v1, p0

    .line 4
    .line 5
    move-object/from16 v2, p2

    .line 6
    .line 7
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o8([I)F

    .line 8
    .line 9
    .line 10
    move-result v3

    .line 11
    new-instance v4, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v5, "createCountNumberBitmap: scale: "

    .line 17
    .line 18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v4

    .line 28
    const-string v5, "CountNumberView"

    .line 29
    .line 30
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountResultPainter()Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;->oo88o8O()Ljava/util/List;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 42
    .line 43
    .line 44
    move-result v4

    .line 45
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v4

    .line 49
    new-instance v5, Landroid/graphics/Paint;

    .line 50
    .line 51
    const/4 v6, 0x1

    .line 52
    invoke-direct {v5, v6}, Landroid/graphics/Paint;-><init>(I)V

    .line 53
    .line 54
    .line 55
    const/16 v7, 0x14

    .line 56
    .line 57
    invoke-static {v0, v7}, Lcom/intsig/utils/DisplayUtil;->〇〇8O0〇8(Landroid/content/Context;I)I

    .line 58
    .line 59
    .line 60
    move-result v8

    .line 61
    int-to-float v8, v8

    .line 62
    mul-float v8, v8, v3

    .line 63
    .line 64
    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 68
    .line 69
    .line 70
    move-result v8

    .line 71
    invoke-virtual {v5}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    .line 72
    .line 73
    .line 74
    move-result-object v9

    .line 75
    iget v10, v9, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 76
    .line 77
    iget v9, v9, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 78
    .line 79
    sub-int/2addr v10, v9

    .line 80
    const/high16 v9, 0x41200000    # 10.0f

    .line 81
    .line 82
    invoke-static {v9}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 83
    .line 84
    .line 85
    move-result v9

    .line 86
    int-to-float v9, v9

    .line 87
    mul-float v9, v9, v3

    .line 88
    .line 89
    const/high16 v11, 0x40800000    # 4.0f

    .line 90
    .line 91
    invoke-static {v11}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 92
    .line 93
    .line 94
    move-result v12

    .line 95
    int-to-float v12, v12

    .line 96
    mul-float v12, v12, v3

    .line 97
    .line 98
    const/high16 v13, 0x41800000    # 16.0f

    .line 99
    .line 100
    invoke-static {v13}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 101
    .line 102
    .line 103
    move-result v13

    .line 104
    int-to-float v13, v13

    .line 105
    mul-float v13, v13, v3

    .line 106
    .line 107
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 108
    .line 109
    .line 110
    move-result-object v14

    .line 111
    new-array v15, v6, [Ljava/lang/Object;

    .line 112
    .line 113
    const-string v16, ""

    .line 114
    .line 115
    const/16 v17, 0x0

    .line 116
    .line 117
    aput-object v16, v15, v17

    .line 118
    .line 119
    const v7, 0x7f131834

    .line 120
    .line 121
    .line 122
    invoke-virtual {v14, v7, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v7

    .line 126
    const-string v14, "context.resources.getStr\u2026ring.cs_645_count_12, \"\")"

    .line 127
    .line 128
    invoke-static {v7, v14}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    const/high16 v14, 0x41600000    # 14.0f

    .line 132
    .line 133
    invoke-static {v0, v14}, Lcom/intsig/utils/DisplayUtil;->〇O00(Landroid/content/Context;F)F

    .line 134
    .line 135
    .line 136
    move-result v14

    .line 137
    mul-float v14, v14, v3

    .line 138
    .line 139
    invoke-virtual {v5, v14}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 140
    .line 141
    .line 142
    const/high16 v14, -0x1000000

    .line 143
    .line 144
    invoke-virtual {v5, v14}, Landroid/graphics/Paint;->setColor(I)V

    .line 145
    .line 146
    .line 147
    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 148
    .line 149
    .line 150
    move-result v14

    .line 151
    const/4 v15, 0x2

    .line 152
    int-to-float v6, v15

    .line 153
    mul-float v9, v9, v6

    .line 154
    .line 155
    add-float/2addr v8, v9

    .line 156
    int-to-float v9, v10

    .line 157
    mul-float v12, v12, v6

    .line 158
    .line 159
    add-float/2addr v9, v12

    .line 160
    const/high16 v10, 0x40a00000    # 5.0f

    .line 161
    .line 162
    invoke-static {v10}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 163
    .line 164
    .line 165
    move-result v10

    .line 166
    int-to-float v10, v10

    .line 167
    mul-float v10, v10, v3

    .line 168
    .line 169
    add-float v12, v14, v10

    .line 170
    .line 171
    add-float/2addr v12, v8

    .line 172
    aget v11, v2, v17

    .line 173
    .line 174
    int-to-float v11, v11

    .line 175
    sub-float/2addr v11, v12

    .line 176
    div-float/2addr v11, v6

    .line 177
    new-instance v12, Landroid/graphics/RectF;

    .line 178
    .line 179
    invoke-direct {v12}, Landroid/graphics/RectF;-><init>()V

    .line 180
    .line 181
    .line 182
    add-float/2addr v14, v11

    .line 183
    add-float/2addr v14, v10

    .line 184
    iput v14, v12, Landroid/graphics/RectF;->left:F

    .line 185
    .line 186
    iput v13, v12, Landroid/graphics/RectF;->top:F

    .line 187
    .line 188
    add-float/2addr v14, v8

    .line 189
    iput v14, v12, Landroid/graphics/RectF;->right:F

    .line 190
    .line 191
    add-float v8, v13, v9

    .line 192
    .line 193
    iput v8, v12, Landroid/graphics/RectF;->bottom:F

    .line 194
    .line 195
    aget v2, v2, v17

    .line 196
    .line 197
    mul-float v13, v13, v6

    .line 198
    .line 199
    add-float/2addr v9, v13

    .line 200
    float-to-int v8, v9

    .line 201
    sget-object v9, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 202
    .line 203
    invoke-static {v2, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 204
    .line 205
    .line 206
    move-result-object v2

    .line 207
    new-instance v8, Landroid/graphics/Canvas;

    .line 208
    .line 209
    invoke-direct {v8, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 210
    .line 211
    .line 212
    const/4 v9, -0x1

    .line 213
    invoke-virtual {v8, v9}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 214
    .line 215
    .line 216
    invoke-virtual {v5}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    .line 217
    .line 218
    .line 219
    move-result-object v10

    .line 220
    invoke-virtual {v12}, Landroid/graphics/RectF;->centerY()F

    .line 221
    .line 222
    .line 223
    move-result v13

    .line 224
    iget v14, v10, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 225
    .line 226
    iget v10, v10, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 227
    .line 228
    sub-int v10, v14, v10

    .line 229
    .line 230
    div-int/2addr v10, v15

    .line 231
    sub-int/2addr v10, v14

    .line 232
    int-to-float v10, v10

    .line 233
    add-float/2addr v13, v10

    .line 234
    invoke-virtual {v8, v7, v11, v13, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 235
    .line 236
    .line 237
    invoke-virtual {v8}, Landroid/graphics/Canvas;->save()I

    .line 238
    .line 239
    .line 240
    new-instance v7, Landroid/graphics/Path;

    .line 241
    .line 242
    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 243
    .line 244
    .line 245
    const/high16 v10, 0x40800000    # 4.0f

    .line 246
    .line 247
    invoke-static {v10}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 248
    .line 249
    .line 250
    move-result v10

    .line 251
    int-to-float v10, v10

    .line 252
    mul-float v10, v10, v3

    .line 253
    .line 254
    const/16 v11, 0x8

    .line 255
    .line 256
    new-array v11, v11, [F

    .line 257
    .line 258
    aput v10, v11, v17

    .line 259
    .line 260
    const/4 v13, 0x1

    .line 261
    aput v10, v11, v13

    .line 262
    .line 263
    aput v10, v11, v15

    .line 264
    .line 265
    const/4 v13, 0x3

    .line 266
    aput v10, v11, v13

    .line 267
    .line 268
    const/4 v13, 0x4

    .line 269
    aput v10, v11, v13

    .line 270
    .line 271
    const/4 v13, 0x5

    .line 272
    aput v10, v11, v13

    .line 273
    .line 274
    const/4 v13, 0x6

    .line 275
    aput v10, v11, v13

    .line 276
    .line 277
    const/4 v13, 0x7

    .line 278
    aput v10, v11, v13

    .line 279
    .line 280
    sget-object v10, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 281
    .line 282
    invoke-virtual {v7, v12, v11, v10}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 283
    .line 284
    .line 285
    invoke-virtual {v8, v7}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 286
    .line 287
    .line 288
    const v7, 0x7f0601ee

    .line 289
    .line 290
    .line 291
    invoke-static {v0, v7}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 292
    .line 293
    .line 294
    move-result v7

    .line 295
    invoke-virtual {v8, v7}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 296
    .line 297
    .line 298
    invoke-virtual {v8}, Landroid/graphics/Canvas;->restore()V

    .line 299
    .line 300
    .line 301
    const/16 v7, 0x14

    .line 302
    .line 303
    invoke-static {v0, v7}, Lcom/intsig/utils/DisplayUtil;->〇〇8O0〇8(Landroid/content/Context;I)I

    .line 304
    .line 305
    .line 306
    move-result v0

    .line 307
    int-to-float v0, v0

    .line 308
    mul-float v0, v0, v3

    .line 309
    .line 310
    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 311
    .line 312
    .line 313
    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 314
    .line 315
    .line 316
    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 317
    .line 318
    .line 319
    move-result v0

    .line 320
    iget v3, v12, Landroid/graphics/RectF;->left:F

    .line 321
    .line 322
    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    .line 323
    .line 324
    .line 325
    move-result v7

    .line 326
    sub-float/2addr v7, v0

    .line 327
    div-float/2addr v7, v6

    .line 328
    add-float/2addr v3, v7

    .line 329
    invoke-virtual {v5}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    .line 330
    .line 331
    .line 332
    move-result-object v0

    .line 333
    invoke-virtual {v12}, Landroid/graphics/RectF;->centerY()F

    .line 334
    .line 335
    .line 336
    move-result v6

    .line 337
    iget v7, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 338
    .line 339
    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 340
    .line 341
    sub-int v0, v7, v0

    .line 342
    .line 343
    div-int/2addr v0, v15

    .line 344
    sub-int/2addr v0, v7

    .line 345
    int-to-float v0, v0

    .line 346
    add-float/2addr v6, v0

    .line 347
    invoke-virtual {v8, v4, v3, v6, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 348
    .line 349
    .line 350
    const-string v0, "resultBitmap"

    .line 351
    .line 352
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 353
    .line 354
    .line 355
    return-object v2
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private final 〇08O8o〇0(FF)Z
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 8
    .line 9
    iget v2, v0, Landroid/graphics/RectF;->left:F

    .line 10
    .line 11
    cmpg-float v2, p1, v2

    .line 12
    .line 13
    if-ltz v2, :cond_2

    .line 14
    .line 15
    iget v2, v0, Landroid/graphics/RectF;->right:F

    .line 16
    .line 17
    cmpl-float p1, p1, v2

    .line 18
    .line 19
    if-gtz p1, :cond_2

    .line 20
    .line 21
    iget p1, v0, Landroid/graphics/RectF;->top:F

    .line 22
    .line 23
    cmpg-float p1, p2, p1

    .line 24
    .line 25
    if-ltz p1, :cond_2

    .line 26
    .line 27
    iget p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 28
    .line 29
    cmpl-float p1, p2, p1

    .line 30
    .line 31
    if-lez p1, :cond_1

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_1
    return v1

    .line 35
    :cond_2
    :goto_0
    const-string p1, "CountNumberView"

    .line 36
    .line 37
    const-string p2, "isOutsideClip : true"

    .line 38
    .line 39
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    const/4 p1, 0x1

    .line 43
    return p1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇80()V
    .locals 4

    .line 1
    const-string v0, "CountNumberView"

    .line 2
    .line 3
    const-string v1, "updateLocationWhenScale"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 9
    .line 10
    instance-of v1, v0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 11
    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    iget-boolean v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 15
    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    check-cast v0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O8o08O()Landroid/graphics/RectF;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 25
    .line 26
    iget-object v3, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇〇0o〇〇0:Landroid/graphics/RectF;

    .line 27
    .line 28
    invoke-direct {p0, v2, v3}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O8〇o(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 29
    .line 30
    .line 31
    sget-object v2, Lcom/intsig/camscanner/util/GraphicUtils;->〇080:Lcom/intsig/camscanner/util/GraphicUtils;

    .line 32
    .line 33
    iget-object v3, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 34
    .line 35
    invoke-virtual {v2, v1, v3}, Lcom/intsig/camscanner/util/GraphicUtils;->〇080(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    invoke-direct {p0, v2, v3}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo8Oo00oo(FF)[F

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    iput-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇00O0:[F

    .line 51
    .line 52
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OoO8(Landroid/graphics/RectF;)[F

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O888o0o([F)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_0
    if-eqz v1, :cond_1

    .line 64
    .line 65
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oOO0880O:Z

    .line 66
    .line 67
    if-eqz v1, :cond_1

    .line 68
    .line 69
    check-cast v0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 70
    .line 71
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O8o08O()Landroid/graphics/RectF;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getImageBound()Landroid/graphics/RectF;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    if-eqz v2, :cond_1

    .line 80
    .line 81
    sget-object v3, Lcom/intsig/camscanner/util/GraphicUtils;->〇080:Lcom/intsig/camscanner/util/GraphicUtils;

    .line 82
    .line 83
    invoke-virtual {v3, v1, v2}, Lcom/intsig/camscanner/util/GraphicUtils;->〇080(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    .line 87
    .line 88
    .line 89
    move-result v2

    .line 90
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    invoke-direct {p0, v2, v3}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo8Oo00oo(FF)[F

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    iput-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇00O0:[F

    .line 99
    .line 100
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OoO8(Landroid/graphics/RectF;)[F

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O888o0o([F)V

    .line 105
    .line 106
    .line 107
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 108
    .line 109
    .line 110
    :cond_1
    :goto_0
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final 〇8〇0〇o〇O(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 4

    .line 1
    const-string v0, "CountNumberView"

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-lt p2, v1, :cond_0

    .line 7
    .line 8
    return-object v2

    .line 9
    :cond_0
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v3, "safeCreateBitmap: imSampleSize: "

    .line 15
    .line 16
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    .line 30
    .line 31
    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 32
    .line 33
    .line 34
    iput p2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 35
    .line 36
    invoke-static {p1, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 37
    .line 38
    .line 39
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    return-object p1

    .line 41
    :catchall_0
    move-exception p1

    .line 42
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :catch_0
    mul-int/lit8 p2, p2, 0x2

    .line 47
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇8〇0〇o〇O(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    .line 49
    .line 50
    .line 51
    :goto_0
    return-object v2
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇o(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇〇0o〇〇0:Landroid/graphics/RectF;

    .line 9
    .line 10
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O8〇o(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇0O〇O00O:Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 16
    .line 17
    .line 18
    iget v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇o〇Oo88:F

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    cmpl-float v1, v0, v1

    .line 22
    .line 23
    if-lez v1, :cond_1

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇0O〇O00O:Landroid/graphics/Path;

    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 28
    .line 29
    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 30
    .line 31
    invoke-virtual {v1, v2, v0, v0, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇0O〇O00O:Landroid/graphics/Path;

    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 38
    .line 39
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 40
    .line 41
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 42
    .line 43
    .line 44
    :goto_0
    if-eqz p1, :cond_2

    .line 45
    .line 46
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 47
    .line 48
    .line 49
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 50
    .line 51
    const/16 v1, 0x1a

    .line 52
    .line 53
    if-lt v0, v1, :cond_3

    .line 54
    .line 55
    if-eqz p1, :cond_4

    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇0O〇O00O:Landroid/graphics/Path;

    .line 58
    .line 59
    invoke-static {p1, v0}, LOoo8〇〇/OO0o〇〇〇〇0;->〇080(Landroid/graphics/Canvas;Landroid/graphics/Path;)Z

    .line 60
    .line 61
    .line 62
    goto :goto_1

    .line 63
    :cond_3
    if-eqz p1, :cond_4

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇0O〇O00O:Landroid/graphics/Path;

    .line 66
    .line 67
    sget-object v1, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    .line 68
    .line 69
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 70
    .line 71
    .line 72
    :cond_4
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getImageBound()Landroid/graphics/RectF;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    if-eqz v0, :cond_6

    .line 77
    .line 78
    if-eqz p1, :cond_5

    .line 79
    .line 80
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->OO〇OOo:Landroid/graphics/Paint;

    .line 81
    .line 82
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 83
    .line 84
    .line 85
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 86
    .line 87
    goto :goto_2

    .line 88
    :cond_5
    const/4 v0, 0x0

    .line 89
    :goto_2
    if-nez v0, :cond_7

    .line 90
    .line 91
    :cond_6
    new-instance v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$drawMask$2;

    .line 92
    .line 93
    invoke-direct {v0, p1, p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$drawMask$2;-><init>(Landroid/graphics/Canvas;Lcom/intsig/camscanner/capture/count/widget/CountNumberView;)V

    .line 94
    .line 95
    .line 96
    :cond_7
    if-eqz p1, :cond_8

    .line 97
    .line 98
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 99
    .line 100
    .line 101
    :cond_8
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static final synthetic 〇oOO8O8(Lcom/intsig/camscanner/capture/count/widget/CountNumberView;)Landroid/graphics/Paint;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->OO〇OOo:Landroid/graphics/Paint;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇〇0o(FF)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->ooO:F

    .line 2
    .line 3
    sub-float/2addr p1, v0

    .line 4
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    iget v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇OO〇00〇0O:F

    .line 9
    .line 10
    sub-float/2addr p2, v0

    .line 11
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    const/high16 v0, 0x41200000    # 10.0f

    .line 16
    .line 17
    cmpg-float p1, p1, v0

    .line 18
    .line 19
    if-gtz p1, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    int-to-float p1, p1

    .line 34
    cmpg-float p1, p2, p1

    .line 35
    .line 36
    if-gtz p1, :cond_0

    .line 37
    .line 38
    const/4 p1, 0x1

    .line 39
    goto :goto_0

    .line 40
    :cond_0
    const/4 p1, 0x0

    .line 41
    :goto_0
    return p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final O08000()V
    .locals 1

    .line 1
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oo88o8O(F)[F

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final OOO〇O0()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇08oOOO0:Landroid/graphics/RectF;

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLocationSelector()Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->o800o8O()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O〇O〇oO(Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "state"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;->o〇0(Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;Lkotlin/jvm/functions/Function1;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final getCacheSelectPositionInOriImg()Landroid/graphics/RectF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇08oOOO0:Landroid/graphics/RectF;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final getClipAreaInOriImg()Landroid/graphics/Rect;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇〇0o〇〇0:Landroid/graphics/RectF;

    .line 2
    .line 3
    new-instance v1, Landroid/graphics/Rect;

    .line 4
    .line 5
    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 9
    .line 10
    .line 11
    return-object v1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final getCountResultInOriImg()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oOO〇〇:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 8
    .line 9
    new-instance v2, Landroid/graphics/Matrix;

    .line 10
    .line 11
    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 12
    .line 13
    .line 14
    iget-object v3, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇00O:Landroid/graphics/Matrix;

    .line 15
    .line 16
    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 17
    .line 18
    .line 19
    const/4 v3, 0x0

    .line 20
    aget v3, v1, v3

    .line 21
    .line 22
    int-to-float v3, v3

    .line 23
    const/high16 v4, 0x3f800000    # 1.0f

    .line 24
    .line 25
    mul-float v3, v3, v4

    .line 26
    .line 27
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    int-to-float v5, v5

    .line 32
    div-float/2addr v3, v5

    .line 33
    const/4 v5, 0x1

    .line 34
    aget v1, v1, v5

    .line 35
    .line 36
    int-to-float v1, v1

    .line 37
    mul-float v1, v1, v4

    .line 38
    .line 39
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    int-to-float v0, v0

    .line 44
    div-float/2addr v1, v0

    .line 45
    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    invoke-virtual {v2, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 50
    .line 51
    .line 52
    new-instance v0, Ljava/util/ArrayList;

    .line 53
    .line 54
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountResultPainter()Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;->oo88o8O()Ljava/util/List;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    check-cast v1, Ljava/lang/Iterable;

    .line 66
    .line 67
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    if-eqz v3, :cond_1

    .line 76
    .line 77
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object v3

    .line 81
    check-cast v3, Lcom/intsig/camscanner/capture/count/data/CountNumberCircleData;

    .line 82
    .line 83
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/capture/count/data/CountNumberCircleData;->〇o〇(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    .line 84
    .line 85
    .line 86
    move-result-object v3

    .line 87
    new-instance v4, Landroid/graphics/Rect;

    .line 88
    .line 89
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v3, v4}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 93
    .line 94
    .line 95
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_1
    return-object v0
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final getSelectLocationInOriImg()Landroid/graphics/Rect;
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getOriMatrixInCurrentView()Landroid/graphics/Matrix;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return-object v0

    .line 9
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLocationSelector()Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O8o08O()Landroid/graphics/RectF;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    new-instance v2, Landroid/graphics/RectF;

    .line 18
    .line 19
    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 23
    .line 24
    .line 25
    iput-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇08oOOO0:Landroid/graphics/RectF;

    .line 26
    .line 27
    new-instance v0, Landroid/graphics/Rect;

    .line 28
    .line 29
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 33
    .line 34
    .line 35
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final getSelectTouchPositionInOriImg()[F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇00O0:[F

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o0ooO(Ljava/lang/String;)Z
    .locals 14
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "CountNumberView"

    .line 2
    .line 3
    const-string v1, "saveImagePath"

    .line 4
    .line 5
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const/4 v2, 0x0

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    return v2

    .line 16
    :cond_0
    iget-object v3, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo80:Ljava/lang/String;

    .line 17
    .line 18
    if-nez v3, :cond_1

    .line 19
    .line 20
    return v2

    .line 21
    :cond_1
    iget-object v7, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oOO〇〇:Landroid/graphics/Bitmap;

    .line 22
    .line 23
    if-nez v7, :cond_2

    .line 24
    .line 25
    return v2

    .line 26
    :cond_2
    invoke-static {v3}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 27
    .line 28
    .line 29
    move-result v4

    .line 30
    if-nez v4, :cond_3

    .line 31
    .line 32
    return v2

    .line 33
    :cond_3
    const/4 v4, 0x1

    .line 34
    :try_start_0
    invoke-direct {p0, v3, v4}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇8〇0〇o〇O(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    .line 35
    .line 36
    .line 37
    move-result-object v3

    .line 38
    if-nez v3, :cond_4

    .line 39
    .line 40
    const-string p1, "drawResultInOriImg: createBitmap failure"

    .line 41
    .line 42
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    return v2

    .line 46
    :cond_4
    const/4 v5, 0x2

    .line 47
    new-array v6, v5, [I

    .line 48
    .line 49
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 50
    .line 51
    .line 52
    move-result v5

    .line 53
    aput v5, v6, v2

    .line 54
    .line 55
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    .line 56
    .line 57
    .line 58
    move-result v5

    .line 59
    aput v5, v6, v4

    .line 60
    .line 61
    invoke-direct {p0, v1, v6}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇00〇8(Landroid/content/Context;[I)Landroid/graphics/Bitmap;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    aget v5, v6, v2

    .line 66
    .line 67
    aget v4, v6, v4

    .line 68
    .line 69
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 70
    .line 71
    .line 72
    move-result v8

    .line 73
    add-int/2addr v4, v8

    .line 74
    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 75
    .line 76
    invoke-static {v5, v4, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 77
    .line 78
    .line 79
    move-result-object v10

    .line 80
    new-instance v11, Landroid/graphics/Canvas;

    .line 81
    .line 82
    invoke-direct {v11, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 83
    .line 84
    .line 85
    const/4 v4, -0x1

    .line 86
    invoke-virtual {v11, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 90
    .line 91
    .line 92
    move-result v4

    .line 93
    int-to-float v4, v4

    .line 94
    const/4 v12, 0x0

    .line 95
    const/4 v13, 0x0

    .line 96
    invoke-virtual {v11, v3, v13, v4, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 97
    .line 98
    .line 99
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountResultPainter()Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;

    .line 100
    .line 101
    .line 102
    move-result-object v4

    .line 103
    iget-object v8, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇00O:Landroid/graphics/Matrix;

    .line 104
    .line 105
    const-string v5, "mBaseMatrix"

    .line 106
    .line 107
    invoke-static {v8, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 111
    .line 112
    .line 113
    move-result v5

    .line 114
    int-to-float v9, v5

    .line 115
    move-object v5, v11

    .line 116
    invoke-virtual/range {v4 .. v9}, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;->〇〇8O0〇8(Landroid/graphics/Canvas;[ILandroid/graphics/Bitmap;Landroid/graphics/Matrix;F)V

    .line 117
    .line 118
    .line 119
    invoke-virtual {v11, v1, v13, v13, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 120
    .line 121
    .line 122
    const/16 v4, 0x64

    .line 123
    .line 124
    invoke-static {v10, v4, p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->Oo8Oo00oo(Landroid/graphics/Bitmap;ILjava/lang/String;)Z

    .line 125
    .line 126
    .line 127
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    .line 135
    .line 136
    goto :goto_0

    .line 137
    :catchall_0
    move-exception p1

    .line 138
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 139
    .line 140
    .line 141
    :goto_0
    return v2
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public final o8oO〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1    # Landroid/content/res/Configuration;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "newConfig"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "CountNumberView"

    .line 7
    .line 8
    const-string v0, "onConfigurationChanged: "

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo0〇Ooo:Landroid/graphics/Matrix;

    .line 14
    .line 15
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 16
    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo0〇Ooo:Landroid/graphics/Matrix;

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 25
    .line 26
    .line 27
    new-instance p1, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$onConfigurationChanged$$inlined$doOnNextLayout$1;

    .line 28
    .line 29
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$onConfigurationChanged$$inlined$doOnNextLayout$1;-><init>(Lcom/intsig/camscanner/capture/count/widget/CountNumberView;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, p1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/view/SafeImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    if-nez p1, :cond_0

    .line 5
    .line 6
    return-void

    .line 7
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇o(Landroid/graphics/Canvas;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 11
    .line 12
    instance-of v1, v0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 13
    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 17
    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;->Oo08(Landroid/graphics/RectF;)V

    .line 25
    .line 26
    .line 27
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 28
    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    const-string v2, "imageViewMatrix"

    .line 36
    .line 37
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    iget-object v2, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O8o08O8O:Landroid/graphics/Matrix;

    .line 41
    .line 42
    const-string v3, "mSuppMatrix"

    .line 43
    .line 44
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇00O:Landroid/graphics/Matrix;

    .line 48
    .line 49
    const-string v4, "mBaseMatrix"

    .line 50
    .line 51
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;->〇o00〇〇Oo(Landroid/graphics/Canvas;Landroid/graphics/Matrix;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    .line 55
    .line 56
    .line 57
    :cond_2
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .param p1    # Landroid/view/ScaleGestureDetector;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "detector"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    mul-float p1, p1, v0

    .line 15
    .line 16
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    const/4 p1, 0x0

    .line 23
    return p1

    .line 24
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 25
    .line 26
    cmpg-float v1, p1, v0

    .line 27
    .line 28
    if-gez v1, :cond_1

    .line 29
    .line 30
    const/high16 p1, 0x3f800000    # 1.0f

    .line 31
    .line 32
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oo88o8O(F)[F

    .line 33
    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇80()V

    .line 36
    .line 37
    .line 38
    const/4 p1, 0x1

    .line 39
    return p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1
    .param p1    # Landroid/view/ScaleGestureDetector;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "detector"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    if-nez p1, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;->oO80(Z)V

    .line 13
    .line 14
    .line 15
    :goto_0
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1    # Landroid/view/ScaleGestureDetector;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "detector"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p1, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$onScaleEnd$$inlined$postDelayed$1;

    .line 7
    .line 8
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$onScaleEnd$$inlined$postDelayed$1;-><init>(Lcom/intsig/camscanner/capture/count/widget/CountNumberView;)V

    .line 9
    .line 10
    .line 11
    const-wide/16 v0, 0x1f4

    .line 12
    .line 13
    invoke-virtual {p0, p1, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getImageBound()Landroid/graphics/RectF;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    if-eqz p1, :cond_0

    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLocationSelector()Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oo08(Landroid/graphics/RectF;)V

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1

    .line 8
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    and-int/lit16 v2, v2, 0xff

    .line 21
    .line 22
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    const/4 v4, 0x2

    .line 27
    const/4 v5, 0x1

    .line 28
    if-lt v3, v4, :cond_1

    .line 29
    .line 30
    iput-boolean v5, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O88O:Z

    .line 31
    .line 32
    :cond_1
    if-nez v2, :cond_2

    .line 33
    .line 34
    iput v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->ooO:F

    .line 35
    .line 36
    iput v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇OO〇00〇0O:F

    .line 37
    .line 38
    iget-object v3, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o8o:Landroid/view/ScaleGestureDetector;

    .line 39
    .line 40
    invoke-virtual {v3, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 41
    .line 42
    .line 43
    iget-object v3, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O8〇o〇88:Landroid/view/GestureDetector;

    .line 44
    .line 45
    invoke-virtual {v3, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 46
    .line 47
    .line 48
    :cond_2
    const/4 v3, 0x5

    .line 49
    if-ne v2, v3, :cond_3

    .line 50
    .line 51
    iput-boolean v5, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O88O:Z

    .line 52
    .line 53
    :cond_3
    if-nez v2, :cond_4

    .line 54
    .line 55
    iget-object v3, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oo8ooo8O:Lcom/intsig/camscanner/capture/count/interfaces/ICountView;

    .line 56
    .line 57
    if-eqz v3, :cond_4

    .line 58
    .line 59
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/count/interfaces/ICountView;->o〇〇0〇88()V

    .line 60
    .line 61
    .line 62
    :cond_4
    if-ne v2, v5, :cond_6

    .line 63
    .line 64
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO(FF)V

    .line 65
    .line 66
    .line 67
    iget-boolean v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O88O:Z

    .line 68
    .line 69
    if-nez v2, :cond_5

    .line 70
    .line 71
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇O:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$OnClickImageListener;

    .line 72
    .line 73
    if-eqz v2, :cond_5

    .line 74
    .line 75
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$OnClickImageListener;->〇080()V

    .line 76
    .line 77
    .line 78
    :cond_5
    const/4 v2, 0x0

    .line 79
    iput-boolean v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O88O:Z

    .line 80
    .line 81
    :cond_6
    iget-boolean v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O88O:Z

    .line 82
    .line 83
    if-eqz v2, :cond_7

    .line 84
    .line 85
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLoadingDrawer()Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;->OO0o〇〇〇〇0()Z

    .line 90
    .line 91
    .line 92
    move-result v2

    .line 93
    if-nez v2, :cond_7

    .line 94
    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o8o:Landroid/view/ScaleGestureDetector;

    .line 96
    .line 97
    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 98
    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O8〇o〇88:Landroid/view/GestureDetector;

    .line 101
    .line 102
    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 103
    .line 104
    .line 105
    goto :goto_0

    .line 106
    :cond_7
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 107
    .line 108
    instance-of v2, v2, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 109
    .line 110
    if-eqz v2, :cond_8

    .line 111
    .line 112
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08O8o〇0(FF)Z

    .line 113
    .line 114
    .line 115
    move-result v2

    .line 116
    if-eqz v2, :cond_8

    .line 117
    .line 118
    return v5

    .line 119
    :cond_8
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 120
    .line 121
    instance-of v2, v2, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;

    .line 122
    .line 123
    if-eqz v2, :cond_9

    .line 124
    .line 125
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08O8o〇0(FF)Z

    .line 126
    .line 127
    .line 128
    move-result v0

    .line 129
    if-eqz v0, :cond_9

    .line 130
    .line 131
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 132
    .line 133
    const-string v1, "null cannot be cast to non-null type com.intsig.camscanner.capture.count.widget.CountNumberPainter"

    .line 134
    .line 135
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    check-cast v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;

    .line 139
    .line 140
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;->o800o8O()Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    sget-object v1, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;->Add:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 145
    .line 146
    if-ne v0, v1, :cond_9

    .line 147
    .line 148
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 149
    .line 150
    if-eqz v0, :cond_9

    .line 151
    .line 152
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇O()V

    .line 153
    .line 154
    .line 155
    :cond_9
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 156
    .line 157
    if-eqz v0, :cond_a

    .line 158
    .line 159
    iget-object v1, p0, Lcom/intsig/camscanner/view/ImageViewTouchBase;->O8o08O8O:Landroid/graphics/Matrix;

    .line 160
    .line 161
    const-string v2, "mSuppMatrix"

    .line 162
    .line 163
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getDisplayBoundRect()Landroid/graphics/RectF;

    .line 167
    .line 168
    .line 169
    move-result-object v2

    .line 170
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;->〇o〇(Landroid/view/MotionEvent;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Z

    .line 171
    .line 172
    .line 173
    :cond_a
    :goto_0
    return v5
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public final o〇0OOo〇0()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLoadingDrawer()Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;->OO0o〇〇()V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountResultPainter()Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o〇8oOO88()Lcom/intsig/camscanner/capture/count/widget/CountNumberView;
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getImageBound()Landroid/graphics/RectF;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "CountNumberView"

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const-string v0, "showLoading: imageBounds is null"

    .line 10
    .line 11
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    return-object v0

    .line 16
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    new-instance v4, Ljava/lang/StringBuilder;

    .line 25
    .line 26
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .line 28
    .line 29
    const-string v5, "showLoading: imageBounds="

    .line 30
    .line 31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string v5, " width="

    .line 38
    .line 39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string v2, ", height="

    .line 46
    .line 47
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLoadingDrawer()Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    iput-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLoadingDrawer()Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;->Oo08(Landroid/graphics/RectF;)V

    .line 71
    .line 72
    .line 73
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLoadingDrawer()Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountLoadingDrawer;->〇8o8o〇()V

    .line 78
    .line 79
    .line 80
    return-object p0
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final setClipArea(Landroid/graphics/Rect;)V
    .locals 6

    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v3, 0x1

    aget v0, v0, v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "originView\uff1awidth: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "    height: "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "CountNumberView"

    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setClipArea\uff1a"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    .line 10
    iput-boolean v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇〇0o〇〇0:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇〇0o〇〇0:Landroid/graphics/RectF;

    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O8〇o(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getImageBound()Landroid/graphics/RectF;

    move-result-object p1

    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "viewArea: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "  finalClipRect\uff1a"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    iput-boolean v3, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->OO〇OOo:Landroid/graphics/Paint;

    iget v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo0O0o8:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 17
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final setClipArea(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇〇0o〇〇0:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    const-string v2, "pos[0]"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    const-string v4, "pos[1]"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    const-string v5, "pos[2]"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v4

    const/4 v5, 0x3

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    const-string v5, "pos[3]"

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    move-result p1

    invoke-virtual {v1, v0, v3, v4, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO〇8O8oOo:Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇〇0o〇〇0:Landroid/graphics/RectF;

    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O8〇o(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 4
    iput-boolean v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void

    .line 6
    :cond_1
    :goto_0
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oO00〇o:Z

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final setDrawMode(Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mode"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;->O8(Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getImageBound()Landroid/graphics/RectF;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    if-eqz p1, :cond_1

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 20
    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;->Oo08(Landroid/graphics/RectF;)V

    .line 24
    .line 25
    .line 26
    :cond_1
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setDrawerType(Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;)V
    .locals 4
    .param p1    # Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "drawerType"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$WhenMappings;->〇080:[I

    .line 7
    .line 8
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    aget v0, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x1

    .line 15
    const/4 v2, 0x0

    .line 16
    if-eq v0, v1, :cond_1

    .line 17
    .line 18
    const/4 v1, 0x2

    .line 19
    if-eq v0, v1, :cond_0

    .line 20
    .line 21
    move-object v0, v2

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLocationSelector()Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    goto :goto_0

    .line 28
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountResultPainter()Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    :goto_0
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 33
    .line 34
    new-instance v1, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v3, "setDrawerMode   drawMode: "

    .line 40
    .line 41
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string p1, " mBaseCountDrawer\uff1a "

    .line 48
    .line 49
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    const-string v0, "CountNumberView"

    .line 60
    .line 61
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getImageBound()Landroid/graphics/RectF;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    if-eqz p1, :cond_2

    .line 69
    .line 70
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 71
    .line 72
    if-eqz v1, :cond_2

    .line 73
    .line 74
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;->Oo08(Landroid/graphics/RectF;)V

    .line 75
    .line 76
    .line 77
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 78
    .line 79
    move-object v2, p1

    .line 80
    :cond_2
    if-nez v2, :cond_3

    .line 81
    .line 82
    const-string p1, "getImageBound is null"

    .line 83
    .line 84
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    :cond_3
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final setHalfDefaultSelectSize(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇800OO〇0O:F

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setICountView(Lcom/intsig/camscanner/capture/count/interfaces/ICountView;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/capture/count/interfaces/ICountView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "countView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oo8ooo8O:Lcom/intsig/camscanner/capture/count/interfaces/ICountView;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setImagePath(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "path"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    invoke-static {p1, v0}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const/4 v2, 0x0

    .line 12
    const-string v3, "CountNumberView"

    .line 13
    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    iput-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇08〇o0O:[I

    .line 17
    .line 18
    aget v4, v1, v2

    .line 19
    .line 20
    aget v0, v1, v0

    .line 21
    .line 22
    new-instance v1, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v5, "CountNumberView  setImagePath:   path: "

    .line 28
    .line 29
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    const-string v5, "   width: "

    .line 36
    .line 37
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v4, "  height: "

    .line 44
    .line 45
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_0
    const/4 v0, 0x0

    .line 62
    :goto_0
    if-nez v0, :cond_1

    .line 63
    .line 64
    const-string p1, "setImagePath imageBound invalid"

    .line 65
    .line 66
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    return-void

    .line 70
    :cond_1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo80:Ljava/lang/String;

    .line 71
    .line 72
    const v0, 0x1fa400

    .line 73
    .line 74
    .line 75
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 76
    .line 77
    const/16 v3, 0x438

    .line 78
    .line 79
    invoke-static {p1, v3, v0, v1}, Lcom/intsig/camscanner/util/Util;->OOO8o〇〇(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oOO〇〇:Landroid/graphics/Bitmap;

    .line 84
    .line 85
    new-instance v0, Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 86
    .line 87
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p0, v0, v2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o800o8O(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 91
    .line 92
    .line 93
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final setMaskColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->Oo0O0o8:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->OO〇OOo:Landroid/graphics/Paint;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setMinSelectSize(F)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O8o〇O0:F

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLocationSelector()Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->oo88o8O(F)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setOnClickImageListener(Lcom/intsig/camscanner/capture/count/widget/CountNumberView$OnClickImageListener;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/capture/count/widget/CountNumberView$OnClickImageListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "listener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇O:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$OnClickImageListener;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final setResult(Ljava/util/List;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇o88o08〇:Ljava/util/List;

    .line 5
    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v1, "setResult: ori rects: "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const-string v1, "CountNumberView"

    .line 24
    .line 25
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->oo〇(Ljava/util/List;Z)Ljava/util/List;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    if-nez p1, :cond_1

    .line 33
    .line 34
    return-void

    .line 35
    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    .line 36
    .line 37
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    const-string v0, "setResult: view rects: "

    .line 41
    .line 42
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p2

    .line 52
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇o〇:Ljava/util/List;

    .line 56
    .line 57
    iget-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇oO:Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;

    .line 58
    .line 59
    if-eqz p2, :cond_2

    .line 60
    .line 61
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;->〇〇888(Ljava/util/List;)V

    .line 62
    .line 63
    .line 64
    :cond_2
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇8()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O08000()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->OOO〇O0()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLocationSelector()Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->o800o8O()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getImageBound()Landroid/graphics/RectF;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getMCountLocationSelector()Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oo08(Landroid/graphics/RectF;)V

    .line 25
    .line 26
    .line 27
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇〇0〇〇0()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇08oOOO0:Landroid/graphics/RectF;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    move-object v0, v1

    .line 18
    :goto_0
    const/4 v2, 0x0

    .line 19
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o00〇〇Oo(Ljava/lang/Float;F)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-nez v0, :cond_3

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇08oOOO0:Landroid/graphics/RectF;

    .line 26
    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    :cond_1
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o00〇〇Oo(Ljava/lang/Float;F)Z

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-eqz v0, :cond_2

    .line 42
    .line 43
    goto :goto_1

    .line 44
    :cond_2
    const/4 v0, 0x1

    .line 45
    return v0

    .line 46
    :cond_3
    :goto_1
    const/4 v0, 0x0

    .line 47
    return v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.class public final Lcom/intsig/camscanner/capture/count/CountNumberFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "CountNumberFragment.kt"

# interfaces
.implements Lcom/intsig/camscanner/capture/count/interfaces/ICountOpe;
.implements Lcom/intsig/camscanner/capture/count/interfaces/ICountBottom;
.implements Lcom/intsig/camscanner/capture/count/interfaces/ICountView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/count/CountNumberFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic OO〇00〇8oO:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final oOo0:Lcom/intsig/camscanner/capture/count/CountNumberFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Z

.field private final OO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:Lcom/intsig/camscanner/capture/count/data/CountNumberBundle;

.field private final oOo〇8o008:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Landroid/widget/TextView;

.field private 〇080OO8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;

.field private final 〇OOo8〇0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/capture/count/CountNumberFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->OO〇00〇8oO:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->oOo0:Lcom/intsig/camscanner/capture/count/CountNumberFragment$Companion;

    .line 31
    .line 32
    return-void
    .line 33
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇OOo8〇0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment$special$$inlined$viewModels$default$1;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$special$$inlined$viewModels$default$1;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 21
    .line 22
    .line 23
    sget-object v1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 24
    .line 25
    new-instance v2, Lcom/intsig/camscanner/capture/count/CountNumberFragment$special$$inlined$viewModels$default$2;

    .line 26
    .line 27
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$special$$inlined$viewModels$default$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 28
    .line 29
    .line 30
    invoke-static {v1, v2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-class v1, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 35
    .line 36
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    new-instance v2, Lcom/intsig/camscanner/capture/count/CountNumberFragment$special$$inlined$viewModels$default$3;

    .line 41
    .line 42
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$special$$inlined$viewModels$default$3;-><init>(Lkotlin/Lazy;)V

    .line 43
    .line 44
    .line 45
    new-instance v3, Lcom/intsig/camscanner/capture/count/CountNumberFragment$special$$inlined$viewModels$default$4;

    .line 46
    .line 47
    const/4 v4, 0x0

    .line 48
    invoke-direct {v3, v4, v0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$special$$inlined$viewModels$default$4;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/Lazy;)V

    .line 49
    .line 50
    .line 51
    new-instance v4, Lcom/intsig/camscanner/capture/count/CountNumberFragment$special$$inlined$viewModels$default$5;

    .line 52
    .line 53
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$special$$inlined$viewModels$default$5;-><init>(Landroidx/fragment/app/Fragment;Lkotlin/Lazy;)V

    .line 54
    .line 55
    .line 56
    invoke-static {p0, v1, v2, v3, v4}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->OO:Lkotlin/Lazy;

    .line 61
    .line 62
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment$mLoadingDialog$2;

    .line 63
    .line 64
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$mLoadingDialog$2;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 65
    .line 66
    .line 67
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇08O〇00〇o:Lkotlin/Lazy;

    .line 72
    .line 73
    sget-object v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;->None:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 74
    .line 75
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇080OO8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 76
    .line 77
    new-instance v0, Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;

    .line 78
    .line 79
    invoke-direct {v0}, Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;-><init>()V

    .line 80
    .line 81
    .line 82
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇0O:Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;

    .line 83
    .line 84
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment$countCallback$1;

    .line 85
    .line 86
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$countCallback$1;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 87
    .line 88
    .line 89
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->oOo〇8o008:Lkotlin/jvm/functions/Function1;

    .line 90
    .line 91
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private static final O08〇(Landroid/widget/PopupWindow;)V
    .locals 1

    .line 1
    const-string v0, "$popupWindow"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final O0O0〇()V
    .locals 2

    .line 1
    const-string v0, "CountNumberFragment"

    .line 2
    .line 3
    const-string v1, "hideLoading"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O8o08O8O:Z

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 18
    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o〇0OOo〇0()V

    .line 22
    .line 23
    .line 24
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇08O()Lcom/intsig/app/BaseProgressDialog;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method

.method private final O0〇()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->OO:Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 14
    .line 15
    const-string v3, "mActivity"

    .line 16
    .line 17
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;-><init>(Landroid/content/Context;)V

    .line 21
    .line 22
    .line 23
    const v2, 0x7f131835

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    const-string v3, "getString(R.string.cs_645_count_13)"

    .line 31
    .line 32
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;->Oo08(Ljava/lang/String;)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    sget-object v2, Lcom/intsig/camscanner/view/CsTips$Type;->NORMAL:Lcom/intsig/camscanner/view/CsTips$Type;

    .line 40
    .line 41
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;->O8(Lcom/intsig/camscanner/view/CsTips$Type;)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/CsTips$Builder;->〇080()Lcom/intsig/camscanner/view/CsTips;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/view/CsTips;->〇80〇808〇O(Landroid/view/View;)Landroid/widget/PopupWindow;

    .line 50
    .line 51
    .line 52
    :cond_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇0O:Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final O880O〇(Z)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_1

    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o〇00O:Landroid/widget/TextView;

    .line 5
    .line 6
    if-eqz p1, :cond_1

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    instance-of v2, v1, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 11
    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    move-object v0, v1

    .line 15
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 16
    .line 17
    :cond_0
    if-eqz v0, :cond_3

    .line 18
    .line 19
    invoke-virtual {v0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarWrapMenu(Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 24
    .line 25
    instance-of v1, p1, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 26
    .line 27
    if-eqz v1, :cond_2

    .line 28
    .line 29
    move-object v0, p1

    .line 30
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 31
    .line 32
    :cond_2
    if-eqz v0, :cond_3

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇088O()V

    .line 35
    .line 36
    .line 37
    :cond_3
    :goto_0
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final O8〇8〇O80(Lcom/intsig/camscanner/capture/count/CountNumberUIState;)V
    .locals 6

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "handleState: receive state="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "CountNumberFragment"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    instance-of v0, p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$MatchCountResultState;

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    const/4 v2, 0x1

    .line 27
    const-string v3, "mActivity"

    .line 28
    .line 29
    const/4 v4, 0x0

    .line 30
    if-eqz v0, :cond_b

    .line 31
    .line 32
    check-cast p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$MatchCountResultState;

    .line 33
    .line 34
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$MatchCountResultState;->〇080()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇0oO〇oo00(Lcom/intsig/camscanner/capture/count/ImageStatus;)V

    .line 39
    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O0O0〇()V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$MatchCountResultState;->〇080()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇o〇()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-ne v0, v2, :cond_0

    .line 55
    .line 56
    const/4 v0, 0x1

    .line 57
    goto :goto_0

    .line 58
    :cond_0
    const/4 v0, 0x0

    .line 59
    :goto_0
    xor-int/2addr v0, v2

    .line 60
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    if-eqz v2, :cond_1

    .line 65
    .line 66
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 67
    .line 68
    if-eqz v2, :cond_1

    .line 69
    .line 70
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O08000()V

    .line 71
    .line 72
    .line 73
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    if-eqz v2, :cond_2

    .line 78
    .line 79
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 80
    .line 81
    if-eqz v2, :cond_2

    .line 82
    .line 83
    sget-object v5, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;->COUNT_RESULT:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;

    .line 84
    .line 85
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setDrawerType(Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;)V

    .line 86
    .line 87
    .line 88
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    if-eqz v2, :cond_4

    .line 93
    .line 94
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 95
    .line 96
    if-eqz v2, :cond_4

    .line 97
    .line 98
    if-eqz v0, :cond_3

    .line 99
    .line 100
    sget-object v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;->Circle:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;

    .line 101
    .line 102
    goto :goto_1

    .line 103
    :cond_3
    sget-object v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;->Digit:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;

    .line 104
    .line 105
    :goto_1
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setDrawMode(Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;)V

    .line 106
    .line 107
    .line 108
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$MatchCountResultState;->〇080()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    if-eqz v0, :cond_5

    .line 113
    .line 114
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->oO80()Ljava/lang/Boolean;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 119
    .line 120
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 121
    .line 122
    .line 123
    move-result v0

    .line 124
    goto :goto_2

    .line 125
    :cond_5
    const/4 v0, 0x0

    .line 126
    :goto_2
    if-eqz v0, :cond_6

    .line 127
    .line 128
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 129
    .line 130
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$1;

    .line 134
    .line 135
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$1;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 136
    .line 137
    .line 138
    invoke-static {p1, v0}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->〇〇808〇(Landroidx/appcompat/app/AppCompatActivity;Lkotlin/jvm/functions/Function0;)V

    .line 139
    .line 140
    .line 141
    return-void

    .line 142
    :cond_6
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$MatchCountResultState;->〇080()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    if-eqz v0, :cond_a

    .line 147
    .line 148
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$MatchCountResultState;->〇080()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->o〇0()Ljava/lang/Boolean;

    .line 153
    .line 154
    .line 155
    move-result-object v0

    .line 156
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 157
    .line 158
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 159
    .line 160
    .line 161
    move-result v0

    .line 162
    if-eqz v0, :cond_7

    .line 163
    .line 164
    goto :goto_5

    .line 165
    :cond_7
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$MatchCountResultState;->〇080()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 166
    .line 167
    .line 168
    move-result-object p1

    .line 169
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇〇888()Ljava/util/List;

    .line 170
    .line 171
    .line 172
    move-result-object p1

    .line 173
    if-eqz p1, :cond_8

    .line 174
    .line 175
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 176
    .line 177
    .line 178
    move-result p1

    .line 179
    goto :goto_3

    .line 180
    :cond_8
    const/4 p1, 0x0

    .line 181
    :goto_3
    if-gtz p1, :cond_9

    .line 182
    .line 183
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 184
    .line 185
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    .line 187
    .line 188
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$4;

    .line 189
    .line 190
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$4;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 191
    .line 192
    .line 193
    new-instance v2, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$5;

    .line 194
    .line 195
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$5;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 196
    .line 197
    .line 198
    invoke-static {p1, v1, v1, v0, v2}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->〇oo〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 199
    .line 200
    .line 201
    goto :goto_4

    .line 202
    :cond_9
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇〇O80〇0o()V

    .line 203
    .line 204
    .line 205
    :goto_4
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O〇0O〇Oo〇o(Z)V

    .line 206
    .line 207
    .line 208
    goto/16 :goto_8

    .line 209
    .line 210
    :cond_a
    :goto_5
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 211
    .line 212
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    .line 214
    .line 215
    const v0, 0x7f131a5c

    .line 216
    .line 217
    .line 218
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 219
    .line 220
    .line 221
    move-result-object v0

    .line 222
    const v1, 0x7f131db2

    .line 223
    .line 224
    .line 225
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 226
    .line 227
    .line 228
    move-result-object v1

    .line 229
    new-instance v2, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$2;

    .line 230
    .line 231
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$2;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 232
    .line 233
    .line 234
    new-instance v3, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$3;

    .line 235
    .line 236
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$3;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 237
    .line 238
    .line 239
    invoke-static {p1, v0, v1, v2, v3}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->〇oo〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 240
    .line 241
    .line 242
    return-void

    .line 243
    :cond_b
    instance-of v0, p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$ClassificationChoiceState;

    .line 244
    .line 245
    if-eqz v0, :cond_12

    .line 246
    .line 247
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O0O0〇()V

    .line 248
    .line 249
    .line 250
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 251
    .line 252
    .line 253
    move-result-object v0

    .line 254
    if-eqz v0, :cond_c

    .line 255
    .line 256
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 257
    .line 258
    if-eqz v0, :cond_c

    .line 259
    .line 260
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O08000()V

    .line 261
    .line 262
    .line 263
    :cond_c
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 264
    .line 265
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    .line 267
    .line 268
    invoke-static {v0}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->〇oOO8O8(Landroidx/fragment/app/FragmentActivity;)V

    .line 269
    .line 270
    .line 271
    check-cast p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$ClassificationChoiceState;

    .line 272
    .line 273
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$ClassificationChoiceState;->〇080()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 274
    .line 275
    .line 276
    move-result-object v0

    .line 277
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇0oO〇oo00(Lcom/intsig/camscanner/capture/count/ImageStatus;)V

    .line 278
    .line 279
    .line 280
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$ClassificationChoiceState;->〇080()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 281
    .line 282
    .line 283
    move-result-object v0

    .line 284
    if-eqz v0, :cond_d

    .line 285
    .line 286
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->oO80()Ljava/lang/Boolean;

    .line 287
    .line 288
    .line 289
    move-result-object v0

    .line 290
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 291
    .line 292
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 293
    .line 294
    .line 295
    move-result v0

    .line 296
    goto :goto_6

    .line 297
    :cond_d
    const/4 v0, 0x0

    .line 298
    :goto_6
    if-eqz v0, :cond_e

    .line 299
    .line 300
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 301
    .line 302
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 303
    .line 304
    .line 305
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$6;

    .line 306
    .line 307
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$6;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 308
    .line 309
    .line 310
    invoke-static {p1, v0}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->〇〇808〇(Landroidx/appcompat/app/AppCompatActivity;Lkotlin/jvm/functions/Function0;)V

    .line 311
    .line 312
    .line 313
    return-void

    .line 314
    :cond_e
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$ClassificationChoiceState;->〇080()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 315
    .line 316
    .line 317
    move-result-object v0

    .line 318
    if-eqz v0, :cond_f

    .line 319
    .line 320
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->o〇0()Ljava/lang/Boolean;

    .line 321
    .line 322
    .line 323
    move-result-object v0

    .line 324
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 325
    .line 326
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 327
    .line 328
    .line 329
    move-result v4

    .line 330
    :cond_f
    if-nez v4, :cond_11

    .line 331
    .line 332
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$ClassificationChoiceState;->〇080()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 333
    .line 334
    .line 335
    move-result-object p1

    .line 336
    if-nez p1, :cond_10

    .line 337
    .line 338
    goto :goto_7

    .line 339
    :cond_10
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇〇〇0()V

    .line 340
    .line 341
    .line 342
    goto/16 :goto_8

    .line 343
    .line 344
    :cond_11
    :goto_7
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 345
    .line 346
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 347
    .line 348
    .line 349
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$7;

    .line 350
    .line 351
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$handleState$7;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 352
    .line 353
    .line 354
    invoke-static {p1, v0}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->oo88o8O(Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V

    .line 355
    .line 356
    .line 357
    return-void

    .line 358
    :cond_12
    instance-of v0, p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$AddState;

    .line 359
    .line 360
    if-eqz v0, :cond_13

    .line 361
    .line 362
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 363
    .line 364
    .line 365
    move-result-object v0

    .line 366
    if-eqz v0, :cond_19

    .line 367
    .line 368
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 369
    .line 370
    if-eqz v0, :cond_19

    .line 371
    .line 372
    sget-object v1, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;->Add:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 373
    .line 374
    check-cast p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$AddState;

    .line 375
    .line 376
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$UpdateState;->〇080()Lkotlin/jvm/functions/Function1;

    .line 377
    .line 378
    .line 379
    move-result-object p1

    .line 380
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇O〇oO(Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;Lkotlin/jvm/functions/Function1;)V

    .line 381
    .line 382
    .line 383
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 384
    .line 385
    .line 386
    iput-object v1, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇080OO8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 387
    .line 388
    goto :goto_8

    .line 389
    :cond_13
    instance-of v0, p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$DeleteState;

    .line 390
    .line 391
    if-eqz v0, :cond_14

    .line 392
    .line 393
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 394
    .line 395
    .line 396
    move-result-object v0

    .line 397
    if-eqz v0, :cond_19

    .line 398
    .line 399
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 400
    .line 401
    if-eqz v0, :cond_19

    .line 402
    .line 403
    sget-object v1, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;->Delete:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 404
    .line 405
    check-cast p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$DeleteState;

    .line 406
    .line 407
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$UpdateState;->〇080()Lkotlin/jvm/functions/Function1;

    .line 408
    .line 409
    .line 410
    move-result-object p1

    .line 411
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇O〇oO(Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;Lkotlin/jvm/functions/Function1;)V

    .line 412
    .line 413
    .line 414
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 415
    .line 416
    .line 417
    iput-object v1, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇080OO8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 418
    .line 419
    goto :goto_8

    .line 420
    :cond_14
    instance-of v0, p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$ResetState;

    .line 421
    .line 422
    if-eqz v0, :cond_15

    .line 423
    .line 424
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 425
    .line 426
    .line 427
    move-result-object p1

    .line 428
    if-eqz p1, :cond_19

    .line 429
    .line 430
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 431
    .line 432
    if-eqz p1, :cond_19

    .line 433
    .line 434
    sget-object v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;->Init:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 435
    .line 436
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇O〇oO(Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;Lkotlin/jvm/functions/Function1;)V

    .line 437
    .line 438
    .line 439
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇080OO8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 440
    .line 441
    goto :goto_8

    .line 442
    :cond_15
    instance-of v0, p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$UpdateLeftCount;

    .line 443
    .line 444
    if-eqz v0, :cond_18

    .line 445
    .line 446
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 447
    .line 448
    .line 449
    move-result-object v0

    .line 450
    if-eqz v0, :cond_16

    .line 451
    .line 452
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 453
    .line 454
    :cond_16
    if-nez v1, :cond_17

    .line 455
    .line 456
    goto :goto_8

    .line 457
    :cond_17
    new-array v0, v2, [Ljava/lang/Object;

    .line 458
    .line 459
    check-cast p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$UpdateLeftCount;

    .line 460
    .line 461
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/CountNumberUIState$UpdateLeftCount;->〇080()I

    .line 462
    .line 463
    .line 464
    move-result p1

    .line 465
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 466
    .line 467
    .line 468
    move-result-object p1

    .line 469
    aput-object p1, v0, v4

    .line 470
    .line 471
    const p1, 0x7f131840

    .line 472
    .line 473
    .line 474
    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 475
    .line 476
    .line 477
    move-result-object p1

    .line 478
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 479
    .line 480
    .line 481
    goto :goto_8

    .line 482
    :cond_18
    instance-of p1, p1, Lcom/intsig/camscanner/capture/count/CountNumberUIState$GuideState;

    .line 483
    .line 484
    if-eqz p1, :cond_19

    .line 485
    .line 486
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o0〇〇00()V

    .line 487
    .line 488
    .line 489
    :cond_19
    :goto_8
    return-void
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
.end method

.method private final OO0O(I)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->OO:Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 13
    .line 14
    .line 15
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇OOo8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 26
    .line 27
    .line 28
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    if-eqz v0, :cond_2

    .line 33
    .line 34
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇OOo8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;

    .line 35
    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;->〇O8o08O()V

    .line 39
    .line 40
    .line 41
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    if-eqz v0, :cond_3

    .line 46
    .line 47
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇OOo8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;

    .line 48
    .line 49
    if-eqz v0, :cond_3

    .line 50
    .line 51
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;->OO0o〇〇〇〇0(I)V

    .line 52
    .line 53
    .line 54
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    if-eqz p1, :cond_4

    .line 59
    .line 60
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 61
    .line 62
    if-eqz p1, :cond_4

    .line 63
    .line 64
    sget-object v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;->Init:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 65
    .line 66
    const/4 v2, 0x0

    .line 67
    invoke-virtual {p1, v0, v2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->O〇O〇oO(Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;Lkotlin/jvm/functions/Function1;)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 71
    .line 72
    .line 73
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    if-eqz p1, :cond_5

    .line 78
    .line 79
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->OO:Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;

    .line 80
    .line 81
    if-eqz p1, :cond_5

    .line 82
    .line 83
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇〇(Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;)V

    .line 84
    .line 85
    .line 86
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    if-eqz p1, :cond_6

    .line 91
    .line 92
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇0O:Landroid/widget/TextView;

    .line 93
    .line 94
    if-eqz p1, :cond_6

    .line 95
    .line 96
    const/4 v0, 0x0

    .line 97
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 98
    .line 99
    .line 100
    :cond_6
    const-string p1, "CSCountDone"

    .line 101
    .line 102
    invoke-static {p1}, Lcom/intsig/log/LogAgentHelper;->〇0000OOO(Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O880O〇(Z)V

    .line 106
    .line 107
    .line 108
    return-void
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static final synthetic Ooo8o(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o〇08oO80o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final OooO〇(Ljava/lang/String;Z)V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    new-instance v3, Lcom/intsig/camscanner/capture/count/CountNumberFragment$tips$1;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    invoke-direct {v3, p0, p1, p2, v4}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$tips$1;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Ljava/lang/String;ZLkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x3

    .line 14
    const/4 v5, 0x0

    .line 15
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O〇080〇o0()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->OO:Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 14
    .line 15
    const-string v3, "mActivity"

    .line 16
    .line 17
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;-><init>(Landroid/content/Context;)V

    .line 21
    .line 22
    .line 23
    const v2, 0x7f131836

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    const-string v3, "getString(R.string.cs_645_count_14)"

    .line 31
    .line 32
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;->Oo08(Ljava/lang/String;)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    sget-object v2, Lcom/intsig/camscanner/view/CsTips$Type;->NORMAL:Lcom/intsig/camscanner/view/CsTips$Type;

    .line 40
    .line 41
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;->O8(Lcom/intsig/camscanner/view/CsTips$Type;)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/CsTips$Builder;->〇080()Lcom/intsig/camscanner/view/CsTips;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/view/CsTips;->〇80〇808〇O(Landroid/view/View;)Landroid/widget/PopupWindow;

    .line 50
    .line 51
    .line 52
    :cond_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final O〇0O〇Oo〇o(Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->o〇00O:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->O8o08O8O:Landroidx/recyclerview/widget/RecyclerView;

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 25
    .line 26
    .line 27
    :cond_1
    if-eqz p1, :cond_2

    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇0O:Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;

    .line 30
    .line 31
    if-eqz p1, :cond_2

    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->〇〇〇0〇〇0()Ljava/util/List;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;->OoO8(Ljava/util/List;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 45
    .line 46
    .line 47
    :cond_2
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private static final O〇8〇008(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "CountNumberFragment"

    .line 7
    .line 8
    const-string v0, "mTvEditRegion click"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    if-eqz p1, :cond_0

    .line 18
    .line 19
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 20
    .line 21
    if-eqz p1, :cond_0

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇8()V

    .line 24
    .line 25
    .line 26
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    if-eqz p1, :cond_1

    .line 31
    .line 32
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->OO:Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;

    .line 33
    .line 34
    if-eqz p1, :cond_1

    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;->oO80()V

    .line 37
    .line 38
    .line 39
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o〇08oO80o()V

    .line 40
    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 43
    .line 44
    .line 45
    move-result-object p0

    .line 46
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO8o()V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O〇〇O80o8()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->o〇0OOo〇0()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const v1, 0x7f131a5a

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x1

    .line 13
    if-eq v0, v2, :cond_1

    .line 14
    .line 15
    const/4 v3, 0x2

    .line 16
    if-eq v0, v3, :cond_0

    .line 17
    .line 18
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const v0, 0x7f131a59

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    goto :goto_0

    .line 31
    :cond_1
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    :goto_0
    const-string v1, "when(mViewModel.curPage)\u2026s_653_count_05)\n        }"

    .line 36
    .line 37
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    new-instance v1, Lcom/intsig/app/AlertDialog$Builder;

    .line 41
    .line 42
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 43
    .line 44
    invoke-direct {v1, v3}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    const v1, 0x7f13183e

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    new-instance v1, LOOoo/o〇0;

    .line 63
    .line 64
    invoke-direct {v1, p0}, LOOoo/o〇0;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 65
    .line 66
    .line 67
    const v3, 0x7f131c05

    .line 68
    .line 69
    .line 70
    const v4, 0x7f0601f2

    .line 71
    .line 72
    .line 73
    invoke-virtual {v0, v3, v4, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇oOO8O8(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    const v1, 0x7f060207

    .line 78
    .line 79
    .line 80
    const/4 v3, 0x0

    .line 81
    const v4, 0x7f13057e

    .line 82
    .line 83
    .line 84
    invoke-virtual {v0, v4, v1, v3}, Lcom/intsig/app/AlertDialog$Builder;->〇0〇O0088o(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇〇888(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 97
    .line 98
    .line 99
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static final synthetic o00〇88〇08(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇〇〇00(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private static final o0Oo(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private final o0〇〇00()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->OO:Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 14
    .line 15
    const-string v3, "mActivity"

    .line 16
    .line 17
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;-><init>(Landroid/content/Context;)V

    .line 21
    .line 22
    .line 23
    const v2, 0x7f131a41

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    const-string v3, "getString(R.string.cs_652_count_05)"

    .line 31
    .line 32
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;->Oo08(Ljava/lang/String;)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    sget-object v2, Lcom/intsig/camscanner/view/CsTips$Type;->NORMAL:Lcom/intsig/camscanner/view/CsTips$Type;

    .line 40
    .line 41
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;->O8(Lcom/intsig/camscanner/view/CsTips$Type;)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/CsTips$Builder;->〇080()Lcom/intsig/camscanner/view/CsTips;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/view/CsTips;->〇80〇808〇O(Landroid/view/View;)Landroid/widget/PopupWindow;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    if-eqz v1, :cond_0

    .line 58
    .line 59
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->OO:Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;

    .line 60
    .line 61
    if-eqz v1, :cond_0

    .line 62
    .line 63
    new-instance v2, LOOoo/oO80;

    .line 64
    .line 65
    invoke-direct {v2, v0}, LOOoo/oO80;-><init>(Landroid/widget/PopupWindow;)V

    .line 66
    .line 67
    .line 68
    const-wide/16 v3, 0xbb8

    .line 69
    .line 70
    invoke-virtual {v1, v2, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 71
    .line 72
    .line 73
    :cond_0
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method static synthetic o808o8o08(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const p1, 0x7f13182d

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const-string p2, "getString(R.string.cs_645_count_05)"

    .line 13
    .line 14
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇oO88o(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private final o88()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->OO:Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;->setIOpe(Lcom/intsig/camscanner/capture/count/interfaces/ICountOpe;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇OOo8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;->setIBottom(Lcom/intsig/camscanner/capture/count/interfaces/ICountBottom;)V

    .line 25
    .line 26
    .line 27
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    if-eqz v0, :cond_2

    .line 32
    .line 33
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 34
    .line 35
    if-eqz v0, :cond_2

    .line 36
    .line 37
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setICountView(Lcom/intsig/camscanner/capture/count/interfaces/ICountView;)V

    .line 38
    .line 39
    .line 40
    const/high16 v1, 0x42100000    # 36.0f

    .line 41
    .line 42
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    int-to-float v1, v1

    .line 47
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setHalfDefaultSelectSize(F)V

    .line 48
    .line 49
    .line 50
    const/high16 v1, 0x42000000    # 32.0f

    .line 51
    .line 52
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    int-to-float v1, v1

    .line 57
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setMinSelectSize(F)V

    .line 58
    .line 59
    .line 60
    new-instance v1, Lcom/intsig/camscanner/capture/count/CountNumberFragment$initViews$1$1;

    .line 61
    .line 62
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$initViews$1$1;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setOnClickImageListener(Lcom/intsig/camscanner/capture/count/widget/CountNumberView$OnClickImageListener;)V

    .line 66
    .line 67
    .line 68
    :cond_2
    new-instance v0, Landroid/widget/TextView;

    .line 69
    .line 70
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 71
    .line 72
    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 73
    .line 74
    .line 75
    const v1, 0x7f131a56

    .line 76
    .line 77
    .line 78
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getText(I)Ljava/lang/CharSequence;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    const v2, 0x7f0601ee

    .line 90
    .line 91
    .line 92
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 97
    .line 98
    .line 99
    const/4 v1, 0x2

    .line 100
    const/high16 v2, 0x41600000    # 14.0f

    .line 101
    .line 102
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 103
    .line 104
    .line 105
    new-instance v1, LOOoo/〇〇888;

    .line 106
    .line 107
    invoke-direct {v1, p0}, LOOoo/〇〇888;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    .line 112
    .line 113
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o〇00O:Landroid/widget/TextView;

    .line 114
    .line 115
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 116
    .line 117
    instance-of v1, v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 118
    .line 119
    if-eqz v1, :cond_3

    .line 120
    .line 121
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 122
    .line 123
    goto :goto_0

    .line 124
    :cond_3
    const/4 v0, 0x0

    .line 125
    :goto_0
    if-eqz v0, :cond_4

    .line 126
    .line 127
    new-instance v1, Landroid/widget/TextView;

    .line 128
    .line 129
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 130
    .line 131
    .line 132
    move-result-object v2

    .line 133
    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 134
    .line 135
    .line 136
    invoke-virtual {v0, v1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarWrapMenu(Landroid/view/View;)V

    .line 137
    .line 138
    .line 139
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇Oo〇O()V

    .line 140
    .line 141
    .line 142
    return-void
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o0Oo(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public static final synthetic oOoO8OO〇(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O0O0〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic oO〇oo(Landroid/widget/PopupWindow;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇oO〇08o(Landroid/widget/PopupWindow;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O〇8〇008(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o〇08oO80o()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->o88O〇8(I)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/4 v2, 0x0

    .line 14
    if-eqz v0, :cond_7

    .line 15
    .line 16
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 17
    .line 18
    const-string v4, "tvLeftCounts"

    .line 19
    .line 20
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    xor-int/2addr v4, v1

    .line 28
    invoke-static {v3, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 29
    .line 30
    .line 31
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇OOo8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;

    .line 32
    .line 33
    const-string v4, "countNumberBottomView"

    .line 34
    .line 35
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-static {v3, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 39
    .line 40
    .line 41
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O〇0O〇Oo〇o(Z)V

    .line 42
    .line 43
    .line 44
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->OO:Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;

    .line 45
    .line 46
    const-string v4, "countNumberOpeView"

    .line 47
    .line 48
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-static {v3, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 52
    .line 53
    .line 54
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇OOo8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;

    .line 55
    .line 56
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;->〇8o8o〇()V

    .line 57
    .line 58
    .line 59
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O8〇8000()Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;

    .line 62
    .line 63
    .line 64
    move-result-object v4

    .line 65
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setDrawerType(Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;)V

    .line 66
    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 73
    .line 74
    .line 75
    move-result-object v3

    .line 76
    if-eqz v3, :cond_0

    .line 77
    .line 78
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇o00〇〇Oo()I

    .line 79
    .line 80
    .line 81
    move-result v3

    .line 82
    const/4 v4, -0x1

    .line 83
    if-ne v3, v4, :cond_0

    .line 84
    .line 85
    const/4 v3, 0x1

    .line 86
    goto :goto_0

    .line 87
    :cond_0
    const/4 v3, 0x0

    .line 88
    :goto_0
    if-eqz v3, :cond_1

    .line 89
    .line 90
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇OOo8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;

    .line 91
    .line 92
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;->O8()V

    .line 93
    .line 94
    .line 95
    goto/16 :goto_4

    .line 96
    .line 97
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 98
    .line 99
    .line 100
    move-result-object v3

    .line 101
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 102
    .line 103
    .line 104
    move-result-object v3

    .line 105
    if-eqz v3, :cond_2

    .line 106
    .line 107
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇o00〇〇Oo()I

    .line 108
    .line 109
    .line 110
    move-result v3

    .line 111
    if-nez v3, :cond_2

    .line 112
    .line 113
    const/4 v3, 0x1

    .line 114
    goto :goto_1

    .line 115
    :cond_2
    const/4 v3, 0x0

    .line 116
    :goto_1
    if-eqz v3, :cond_4

    .line 117
    .line 118
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 119
    .line 120
    .line 121
    move-result-object v3

    .line 122
    if-eqz v3, :cond_3

    .line 123
    .line 124
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 125
    .line 126
    if-eqz v3, :cond_3

    .line 127
    .line 128
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->〇〇〇0〇〇0()Z

    .line 129
    .line 130
    .line 131
    move-result v3

    .line 132
    if-nez v3, :cond_3

    .line 133
    .line 134
    const/4 v3, 0x1

    .line 135
    goto :goto_2

    .line 136
    :cond_3
    const/4 v3, 0x0

    .line 137
    :goto_2
    if-eqz v3, :cond_4

    .line 138
    .line 139
    const v1, 0x7f13182e

    .line 140
    .line 141
    .line 142
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    const-string v3, "getString(R.string.cs_645_count_06)"

    .line 147
    .line 148
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->OooO〇(Ljava/lang/String;Z)V

    .line 152
    .line 153
    .line 154
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇OOo8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;

    .line 155
    .line 156
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;->O8()V

    .line 157
    .line 158
    .line 159
    goto :goto_4

    .line 160
    :cond_4
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 161
    .line 162
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->OOO〇O0()V

    .line 163
    .line 164
    .line 165
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇0O:Landroid/widget/TextView;

    .line 166
    .line 167
    const-string v4, "tvTips"

    .line 168
    .line 169
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    .line 171
    .line 172
    invoke-static {v3, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 173
    .line 174
    .line 175
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 176
    .line 177
    .line 178
    move-result-object v3

    .line 179
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 180
    .line 181
    .line 182
    move-result-object v3

    .line 183
    if-eqz v3, :cond_5

    .line 184
    .line 185
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇o00〇〇Oo()I

    .line 186
    .line 187
    .line 188
    move-result v3

    .line 189
    if-nez v3, :cond_5

    .line 190
    .line 191
    goto :goto_3

    .line 192
    :cond_5
    const/4 v1, 0x0

    .line 193
    :goto_3
    if-nez v1, :cond_6

    .line 194
    .line 195
    const v1, 0x7f131a45

    .line 196
    .line 197
    .line 198
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v1

    .line 202
    const-string v3, "getString(R.string.cs_652_count_09)"

    .line 203
    .line 204
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->OooO〇(Ljava/lang/String;Z)V

    .line 208
    .line 209
    .line 210
    :cond_6
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇OOo8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;

    .line 211
    .line 212
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;->Oo08()V

    .line 213
    .line 214
    .line 215
    :cond_7
    :goto_4
    const-string v0, "CSCountPage"

    .line 216
    .line 217
    invoke-static {v0}, Lcom/intsig/log/LogAgentHelper;->〇0000OOO(Ljava/lang/String;)V

    .line 218
    .line 219
    .line 220
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O880O〇(Z)V

    .line 221
    .line 222
    .line 223
    return-void
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic o〇O8OO(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)Lcom/intsig/camscanner/capture/count/CountNumberViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final o〇o08〇()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 13
    .line 14
    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    const/4 v1, 0x1

    .line 17
    invoke-static {p0, v0, v1, v0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o808o8o08(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Ljava/lang/String;ILjava/lang/Object;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    if-eqz v2, :cond_1

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇o〇()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    if-eqz v3, :cond_2

    .line 47
    .line 48
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/ImageStatus;->O8()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    goto :goto_0

    .line 53
    :cond_2
    move-object v3, v0

    .line 54
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 55
    .line 56
    .line 57
    move-result-object v4

    .line 58
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 59
    .line 60
    .line 61
    move-result-object v4

    .line 62
    if-eqz v4, :cond_3

    .line 63
    .line 64
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/count/ImageStatus;->Oo08()Landroid/graphics/Rect;

    .line 65
    .line 66
    .line 67
    move-result-object v4

    .line 68
    goto :goto_1

    .line 69
    :cond_3
    move-object v4, v0

    .line 70
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    if-eqz v5, :cond_4

    .line 75
    .line 76
    iget-object v5, v5, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 77
    .line 78
    if-eqz v5, :cond_4

    .line 79
    .line 80
    invoke-virtual {v5}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->getSelectLocationInOriImg()Landroid/graphics/Rect;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    :cond_4
    invoke-virtual {v2, v3, v1, v4, v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->〇8o〇〇8080(Ljava/lang/String;ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 85
    .line 86
    .line 87
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final o〇oo(Ljava/lang/String;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment$initData$1;

    .line 5
    .line 6
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$initData$1;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    const-string p1, "CSCountScanErrorPop"

    .line 10
    .line 11
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇〇〇00(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic 〇088O(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇08O()Lcom/intsig/app/BaseProgressDialog;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇08O〇00〇o:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/app/BaseProgressDialog;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇0oO〇oo00(Lcom/intsig/camscanner/capture/count/ImageStatus;)V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇080()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    move-object v0, v1

    .line 22
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    if-eqz v2, :cond_1

    .line 31
    .line 32
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇80〇808〇O()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    goto :goto_1

    .line 37
    :cond_1
    const/4 v2, -0x1

    .line 38
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    if-eqz v3, :cond_2

    .line 47
    .line 48
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/ImageStatus;->O8()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    goto :goto_2

    .line 53
    :cond_2
    move-object v3, v1

    .line 54
    :goto_2
    if-eqz p1, :cond_7

    .line 55
    .line 56
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/ImageStatus;->O8()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v4

    .line 60
    const/4 v5, 0x1

    .line 61
    if-eqz v4, :cond_4

    .line 62
    .line 63
    invoke-static {v4}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 64
    .line 65
    .line 66
    move-result v6

    .line 67
    xor-int/2addr v6, v5

    .line 68
    if-eqz v6, :cond_3

    .line 69
    .line 70
    move-object v1, v4

    .line 71
    :cond_3
    if-nez v1, :cond_5

    .line 72
    .line 73
    :cond_4
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇8o8o〇(Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 77
    .line 78
    :cond_5
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/capture/count/ImageStatus;->OO0o〇〇(I)V

    .line 79
    .line 80
    .line 81
    if-eqz v0, :cond_6

    .line 82
    .line 83
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 84
    .line 85
    .line 86
    move-result v0

    .line 87
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇080()I

    .line 88
    .line 89
    .line 90
    move-result v1

    .line 91
    if-ne v1, v5, :cond_6

    .line 92
    .line 93
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->OO0o〇〇〇〇0(I)V

    .line 94
    .line 95
    .line 96
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->o88O8(Lcom/intsig/camscanner/capture/count/ImageStatus;)V

    .line 101
    .line 102
    .line 103
    :cond_7
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static final synthetic 〇0ooOOo(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇oO88o(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇0〇0(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Lcom/intsig/camscanner/capture/count/CountNumberUIState;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O8〇8〇O80(Lcom/intsig/camscanner/capture/count/CountNumberUIState;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇OOo8〇0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->OO〇00〇8oO:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇8〇80o(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O8o08O8O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇080OO8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$OpeState;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->OO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O8oOo0(Landroid/widget/PopupWindow;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O08〇(Landroid/widget/PopupWindow;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇O8〇8000()Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇o00〇〇Oo()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    :cond_0
    if-eqz v1, :cond_1

    .line 20
    .line 21
    sget-object v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;->SELECT:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;->NONE:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;

    .line 25
    .line 26
    :goto_0
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇Oo〇O()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->〇〇〇0〇〇0()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇0O:Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;

    .line 10
    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    new-instance v2, Lcom/intsig/camscanner/capture/count/CountNumberFragment$initCategoryList$1;

    .line 14
    .line 15
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$initCategoryList$1;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;->o800o8O(Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter$OnClickItemListener;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    if-eqz v1, :cond_6

    .line 26
    .line 27
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->O8o08O8O:Landroidx/recyclerview/widget/RecyclerView;

    .line 28
    .line 29
    if-eqz v1, :cond_6

    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇0O:Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;

    .line 32
    .line 33
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 34
    .line 35
    .line 36
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 37
    .line 38
    new-instance v3, Lcom/intsig/camscanner/capture/count/CountNumberFragment$initCategoryList$2$1;

    .line 39
    .line 40
    invoke-direct {v3, v2}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$initCategoryList$2$1;-><init>(Landroidx/appcompat/app/AppCompatActivity;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    instance-of v3, v2, Landroidx/recyclerview/widget/DefaultItemAnimator;

    .line 51
    .line 52
    const/4 v4, 0x0

    .line 53
    if-eqz v3, :cond_1

    .line 54
    .line 55
    check-cast v2, Landroidx/recyclerview/widget/DefaultItemAnimator;

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    move-object v2, v4

    .line 59
    :goto_0
    const/4 v3, 0x0

    .line 60
    if-nez v2, :cond_2

    .line 61
    .line 62
    goto :goto_1

    .line 63
    :cond_2
    invoke-virtual {v2, v3}, Landroidx/recyclerview/widget/SimpleItemAnimator;->setSupportsChangeAnimations(Z)V

    .line 64
    .line 65
    .line 66
    :goto_1
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    instance-of v5, v2, Landroidx/recyclerview/widget/SimpleItemAnimator;

    .line 71
    .line 72
    if-eqz v5, :cond_3

    .line 73
    .line 74
    move-object v4, v2

    .line 75
    check-cast v4, Landroidx/recyclerview/widget/SimpleItemAnimator;

    .line 76
    .line 77
    :cond_3
    if-nez v4, :cond_4

    .line 78
    .line 79
    goto :goto_2

    .line 80
    :cond_4
    invoke-virtual {v4, v3}, Landroidx/recyclerview/widget/SimpleItemAnimator;->setSupportsChangeAnimations(Z)V

    .line 81
    .line 82
    .line 83
    :goto_2
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    if-nez v2, :cond_5

    .line 88
    .line 89
    goto :goto_3

    .line 90
    :cond_5
    const-wide/16 v3, 0x0

    .line 91
    .line 92
    invoke-virtual {v2, v3, v4}, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;->setChangeDuration(J)V

    .line 93
    .line 94
    .line 95
    :goto_3
    new-instance v2, Lcom/intsig/camscanner/capture/count/adapter/SeparateDecoration;

    .line 96
    .line 97
    invoke-direct {v2}, Lcom/intsig/camscanner/capture/count/adapter/SeparateDecoration;-><init>()V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 101
    .line 102
    .line 103
    :cond_6
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇0O:Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;

    .line 104
    .line 105
    if-eqz v1, :cond_7

    .line 106
    .line 107
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/capture/count/adapter/CountCategoryAdapter;->OoO8(Ljava/util/List;)V

    .line 108
    .line 109
    .line 110
    :cond_7
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static final synthetic 〇o08(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o〇o08〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇oO88o(Ljava/lang/String;)V
    .locals 2

    .line 1
    const-string v0, "CountNumberFragment"

    .line 2
    .line 3
    const-string v1, "showLoading"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇08O()Lcom/intsig/app/BaseProgressDialog;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0, p1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇08O()Lcom/intsig/app/BaseProgressDialog;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final 〇oO〇08o(Landroid/widget/PopupWindow;)V
    .locals 1

    .line 1
    const-string v0, "$popupWindow"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic 〇o〇88〇8(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)Lcom/intsig/camscanner/capture/count/data/CountNumberBundle;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o0:Lcom/intsig/camscanner/capture/count/data/CountNumberBundle;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇〇(Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;)V
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O888Oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    const-string v2, "mActivity"

    .line 13
    .line 14
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/view/CsTips$Builder;-><init>(Landroid/content/Context;)V

    .line 18
    .line 19
    .line 20
    const v1, 0x7f131a41

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const-string v2, "getString(R.string.cs_652_count_05)"

    .line 28
    .line 29
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/CsTips$Builder;->Oo08(Ljava/lang/String;)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const/4 v1, 0x4

    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/CsTips$Builder;->〇o00〇〇Oo(I)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    const/high16 v1, 0x40a00000    # 5.0f

    .line 42
    .line 43
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/CsTips$Builder;->o〇0(I)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/CsTips$Builder;->〇080()Lcom/intsig/camscanner/view/CsTips;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/CsTips;->〇80〇808〇O(Landroid/view/View;)Landroid/widget/PopupWindow;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO0Oo0o0O()V

    .line 60
    .line 61
    .line 62
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    if-eqz v0, :cond_1

    .line 67
    .line 68
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->OO:Lcom/intsig/camscanner/capture/count/widget/CountNumberOpeView;

    .line 69
    .line 70
    if-eqz v0, :cond_1

    .line 71
    .line 72
    new-instance v1, LOOoo/〇80〇808〇O;

    .line 73
    .line 74
    invoke-direct {v1, p1}, LOOoo/〇80〇808〇O;-><init>(Landroid/widget/PopupWindow;)V

    .line 75
    .line 76
    .line 77
    const-wide/16 v2, 0xbb8

    .line 78
    .line 79
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 80
    .line 81
    .line 82
    :cond_1
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final 〇〇O80〇0o()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x2

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->o88O〇8(I)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const/4 v1, 0x1

    .line 18
    const/4 v2, 0x0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->Oooo8o0〇()Z

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    if-ne v3, v1, :cond_0

    .line 26
    .line 27
    const/4 v3, 0x1

    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/4 v3, 0x0

    .line 30
    :goto_0
    if-eqz v0, :cond_1

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇〇888()Ljava/util/List;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    if-eqz v4, :cond_1

    .line 37
    .line 38
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    :cond_1
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->OO0O(I)V

    .line 43
    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    if-eqz v2, :cond_4

    .line 50
    .line 51
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 52
    .line 53
    if-eqz v2, :cond_4

    .line 54
    .line 55
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o8oO〇(Z)V

    .line 56
    .line 57
    .line 58
    sget-object v1, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;->COUNT_RESULT:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;

    .line 59
    .line 60
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setDrawerType(Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;)V

    .line 61
    .line 62
    .line 63
    const/4 v1, 0x0

    .line 64
    if-eqz v0, :cond_2

    .line 65
    .line 66
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->Oo08()Landroid/graphics/Rect;

    .line 67
    .line 68
    .line 69
    move-result-object v4

    .line 70
    goto :goto_1

    .line 71
    :cond_2
    move-object v4, v1

    .line 72
    :goto_1
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setClipArea(Landroid/graphics/Rect;)V

    .line 73
    .line 74
    .line 75
    if-eqz v0, :cond_3

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇〇888()Ljava/util/List;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    :cond_3
    invoke-virtual {v2, v1, v3}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setResult(Ljava/util/List;Z)V

    .line 82
    .line 83
    .line 84
    :cond_4
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇〇O80〇0o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇〇〇0()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->o88O〇8(I)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->〇〇0〇0o8()V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const/4 v2, 0x0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->Oooo8o0〇()Z

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    if-ne v3, v1, :cond_0

    .line 32
    .line 33
    const/4 v2, 0x1

    .line 34
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 35
    .line 36
    .line 37
    move-result-object v3

    .line 38
    if-eqz v3, :cond_2

    .line 39
    .line 40
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 41
    .line 42
    if-eqz v3, :cond_2

    .line 43
    .line 44
    if-eqz v2, :cond_1

    .line 45
    .line 46
    sget-object v4, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;->Circle:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    sget-object v4, Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;->Digit:Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;

    .line 50
    .line 51
    :goto_0
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setDrawMode(Lcom/intsig/camscanner/capture/count/widget/CountNumberPainter$Companion$DrawMode;)V

    .line 52
    .line 53
    .line 54
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 55
    .line 56
    .line 57
    move-result-object v3

    .line 58
    if-eqz v3, :cond_5

    .line 59
    .line 60
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 61
    .line 62
    if-eqz v3, :cond_5

    .line 63
    .line 64
    invoke-virtual {v3, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o8oO〇(Z)V

    .line 65
    .line 66
    .line 67
    sget-object v1, Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;->SELECT:Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;

    .line 68
    .line 69
    invoke-virtual {v3, v1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setDrawerType(Lcom/intsig/camscanner/capture/count/widget/CountNumberView$DrawerType;)V

    .line 70
    .line 71
    .line 72
    const/4 v1, 0x0

    .line 73
    if-eqz v0, :cond_3

    .line 74
    .line 75
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->Oo08()Landroid/graphics/Rect;

    .line 76
    .line 77
    .line 78
    move-result-object v4

    .line 79
    goto :goto_1

    .line 80
    :cond_3
    move-object v4, v1

    .line 81
    :goto_1
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setClipArea(Landroid/graphics/Rect;)V

    .line 82
    .line 83
    .line 84
    if-eqz v0, :cond_4

    .line 85
    .line 86
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇〇888()Ljava/util/List;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    :cond_4
    invoke-virtual {v3, v1, v2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setResult(Ljava/util/List;Z)V

    .line 91
    .line 92
    .line 93
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o〇08oO80o()V

    .line 94
    .line 95
    .line 96
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final 〇〇〇00(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->o〇〇0〇88(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 17
    .line 18
    const-string v1, "mActivity"

    .line 19
    .line 20
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    new-instance v1, Lcom/intsig/camscanner/capture/count/CountNumberFragment$checkNetWorkAvailable$1;

    .line 24
    .line 25
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$checkNetWorkAvailable$1;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 26
    .line 27
    .line 28
    new-instance v2, Lcom/intsig/camscanner/capture/count/CountNumberFragment$checkNetWorkAvailable$2;

    .line 29
    .line 30
    invoke-direct {v2, p0, p2, p1}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$checkNetWorkAvailable$2;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Lkotlin/jvm/functions/Function0;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->o〇O8〇〇o(Landroid/content/Context;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    :goto_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇〇〇O〇()V
    .locals 10

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->O88O:Lcom/intsig/camscanner/capture/count/CountRegionEditActivity$Companion;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    new-instance v2, Ljava/io/File;

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->o〇8oOO88()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    if-nez v3, :cond_0

    .line 18
    .line 19
    const-string v3, ""

    .line 20
    .line 21
    :cond_0
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->o〇8oOO88()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    const/4 v4, 0x1

    .line 37
    invoke-static {v3, v4}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    const/4 v5, 0x0

    .line 42
    if-eqz v3, :cond_4

    .line 43
    .line 44
    array-length v6, v3

    .line 45
    const/4 v7, 0x2

    .line 46
    const/4 v8, 0x0

    .line 47
    if-lt v6, v7, :cond_1

    .line 48
    .line 49
    const/4 v6, 0x1

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    const/4 v6, 0x0

    .line 52
    :goto_0
    if-eqz v6, :cond_2

    .line 53
    .line 54
    goto :goto_1

    .line 55
    :cond_2
    move-object v3, v5

    .line 56
    :goto_1
    if-eqz v3, :cond_4

    .line 57
    .line 58
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 59
    .line 60
    .line 61
    move-result-object v6

    .line 62
    invoke-virtual {v6}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->o〇8oOO88()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v6

    .line 66
    aget v7, v3, v8

    .line 67
    .line 68
    aget v4, v3, v4

    .line 69
    .line 70
    new-instance v8, Ljava/lang/StringBuilder;

    .line 71
    .line 72
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    .line 74
    .line 75
    const-string v9, "clip origin size\uff1a"

    .line 76
    .line 77
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    const-string v6, " width: "

    .line 84
    .line 85
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    const-string v6, "  height: "

    .line 92
    .line 93
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v4

    .line 103
    const-string v6, "CountNumberFragment"

    .line 104
    .line 105
    invoke-static {v6, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 109
    .line 110
    .line 111
    move-result-object v4

    .line 112
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 113
    .line 114
    .line 115
    move-result-object v4

    .line 116
    if-eqz v4, :cond_3

    .line 117
    .line 118
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/count/ImageStatus;->Oo08()Landroid/graphics/Rect;

    .line 119
    .line 120
    .line 121
    move-result-object v5

    .line 122
    :cond_3
    invoke-static {v5, v3}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->〇O00(Landroid/graphics/Rect;[I)Landroid/graphics/Rect;

    .line 123
    .line 124
    .line 125
    move-result-object v5

    .line 126
    :cond_4
    const/16 v3, 0x19fa

    .line 127
    .line 128
    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity$Companion;->startActivityForResult(Landroid/app/Activity;Landroid/net/Uri;ILandroid/graphics/Rect;)V

    .line 129
    .line 130
    .line 131
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->〇008〇oo()V

    .line 136
    .line 137
    .line 138
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method


# virtual methods
.method public OOO()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O〇080〇o0()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->oOo〇8o008:Lkotlin/jvm/functions/Function1;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->o0ooO(Lkotlin/jvm/functions/Function1;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public addEvents()V
    .locals 6

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->addEvents()V

    .line 2
    .line 3
    .line 4
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const/4 v1, 0x0

    .line 9
    const/4 v2, 0x0

    .line 10
    new-instance v3, Lcom/intsig/camscanner/capture/count/CountNumberFragment$addEvents$1;

    .line 11
    .line 12
    const/4 v4, 0x0

    .line 13
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$addEvents$1;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Lkotlin/coroutines/Continuation;)V

    .line 14
    .line 15
    .line 16
    const/4 v4, 0x3

    .line 17
    const/4 v5, 0x0

    .line 18
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public count()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountNumberFragment$count$1;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$count$1;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;)V

    .line 4
    .line 5
    .line 6
    const-string v1, "CSCountErrorPop"

    .line 7
    .line 8
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇〇〇00(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->Oo0oOo〇0()V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getIntentData(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->getIntentData(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/capture/count/CountNumberActivity;->〇〇08O:Lcom/intsig/camscanner/capture/count/CountNumberActivity$Companion;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberActivity$Companion;->〇080()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    check-cast p1, Lcom/intsig/camscanner/capture/count/data/CountNumberBundle;

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o0:Lcom/intsig/camscanner/capture/count/data/CountNumberBundle;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o0:Lcom/intsig/camscanner/capture/count/data/CountNumberBundle;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/count/data/CountNumberBundle;->〇o〇()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    :goto_0
    const/4 v0, 0x1

    .line 12
    if-eqz p1, :cond_2

    .line 13
    .line 14
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-nez v1, :cond_1

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_1
    const/4 v1, 0x0

    .line 22
    goto :goto_2

    .line 23
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 24
    :goto_2
    if-nez v1, :cond_4

    .line 25
    .line 26
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-nez v1, :cond_3

    .line 31
    .line 32
    goto :goto_3

    .line 33
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->O8888(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o88()V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->addEvents()V

    .line 44
    .line 45
    .line 46
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o〇oo(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-static {v0}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->〇0〇O0088o(Z)V

    .line 50
    .line 51
    .line 52
    return-void

    .line 53
    :cond_4
    :goto_3
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 54
    .line 55
    if-eqz p1, :cond_5

    .line 56
    .line 57
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 58
    .line 59
    .line 60
    :cond_5
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public interceptBackPressed()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O〇〇O80o8()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public ooo0〇〇O()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇OOo8〇0:Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/widget/CountNumberBottomView;->Oo08()V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇8()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const/4 v2, 0x0

    .line 13
    const/4 v3, 0x0

    .line 14
    new-instance v4, Lcom/intsig/camscanner/capture/count/CountNumberFragment$save$1;

    .line 15
    .line 16
    const/4 v5, 0x0

    .line 17
    invoke-direct {v4, p0, v0, v5}, Lcom/intsig/camscanner/capture/count/CountNumberFragment$save$1;-><init>(Lcom/intsig/camscanner/capture/count/CountNumberFragment;Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;Lkotlin/coroutines/Continuation;)V

    .line 18
    .line 19
    .line 20
    const/4 v5, 0x3

    .line 21
    const/4 v6, 0x0

    .line 22
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇〇0〇88()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇0O:Landroid/widget/TextView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d02b7

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public reset()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oo0O〇0〇〇〇()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇8o〇〇8080()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇〇〇O〇()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->O8oOo80()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇O8〇8O0oO(IILandroid/content/Intent;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "onMyActivityResult: requestCode="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, ", resultCode="

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "CountNumberFragment"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const/16 v0, 0x19fa

    .line 32
    .line 33
    if-ne p1, v0, :cond_2

    .line 34
    .line 35
    const/4 p1, -0x1

    .line 36
    if-ne p2, p1, :cond_2

    .line 37
    .line 38
    if-eqz p3, :cond_0

    .line 39
    .line 40
    const-string p1, "finish_extra_rect_region"

    .line 41
    .line 42
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    check-cast p1, Landroid/graphics/Rect;

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    const/4 p1, 0x0

    .line 50
    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    .line 51
    .line 52
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .line 54
    .line 55
    const-string p3, "onMyActivityResult: REQ_CODE_ONLY_NEED_REGION="

    .line 56
    .line 57
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    if-eqz p1, :cond_2

    .line 71
    .line 72
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇8O0880()Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;

    .line 73
    .line 74
    .line 75
    move-result-object p2

    .line 76
    if-eqz p2, :cond_2

    .line 77
    .line 78
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/FragmentCountNumberBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/count/widget/CountNumberView;

    .line 79
    .line 80
    if-eqz p2, :cond_2

    .line 81
    .line 82
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->OOO〇O0()V

    .line 83
    .line 84
    .line 85
    const/4 p3, 0x1

    .line 86
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->o8oO〇(Z)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/capture/count/widget/CountNumberView;->setClipArea(Landroid/graphics/Rect;)V

    .line 90
    .line 91
    .line 92
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 93
    .line 94
    .line 95
    move-result-object p2

    .line 96
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->oO()Lcom/intsig/camscanner/capture/count/ImageStatus;

    .line 97
    .line 98
    .line 99
    move-result-object p2

    .line 100
    if-nez p2, :cond_1

    .line 101
    .line 102
    goto :goto_1

    .line 103
    :cond_1
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/capture/count/ImageStatus;->〇O8o08O(Landroid/graphics/Rect;)V

    .line 104
    .line 105
    .line 106
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->o〇08oO80o()V

    .line 107
    .line 108
    .line 109
    :cond_2
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
.end method

.method public 〇OOo8〇0()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->O0〇()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->〇O0o〇〇o()Lcom/intsig/camscanner/capture/count/CountNumberViewModel;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/CountNumberFragment;->oOo〇8o008:Lkotlin/jvm/functions/Function1;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/count/CountNumberViewModel;->〇oo〇(Lkotlin/jvm/functions/Function1;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

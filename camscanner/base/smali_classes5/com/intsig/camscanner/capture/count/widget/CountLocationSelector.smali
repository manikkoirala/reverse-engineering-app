.class public final Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;
.super Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;
.source "CountLocationSelector.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇O〇:Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO0o〇〇:Z

.field private OO0o〇〇〇〇0:F

.field private final Oo08:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oooo8o0〇:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oO80:Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇0:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇80〇808〇O:Lcom/intsig/camscanner/capture/count/crop/CropWindowMoveHandler;

.field private final 〇8o8o〇:F

.field private final 〇O8o08O:Landroid/graphics/Bitmap;

.field private final 〇o00〇〇Oo:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇:[F
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇808〇:F

.field private final 〇〇888:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O〇:Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mHostView"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/BaseCountDrawer;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o00〇〇Oo:Landroid/view/View;

    .line 15
    .line 16
    const/16 p2, 0x8

    .line 17
    .line 18
    new-array p2, p2, [F

    .line 19
    .line 20
    fill-array-data p2, :array_0

    .line 21
    .line 22
    .line 23
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o〇:[F

    .line 24
    .line 25
    new-instance p2, Landroid/graphics/RectF;

    .line 26
    .line 27
    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 31
    .line 32
    new-instance p2, Landroid/graphics/Path;

    .line 33
    .line 34
    invoke-direct {p2}, Landroid/graphics/Path;-><init>()V

    .line 35
    .line 36
    .line 37
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oo08:Landroid/graphics/Path;

    .line 38
    .line 39
    new-instance p2, Landroid/graphics/Paint;

    .line 40
    .line 41
    const/4 v0, 0x1

    .line 42
    invoke-direct {p2, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 43
    .line 44
    .line 45
    iput-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->o〇0:Landroid/graphics/Paint;

    .line 46
    .line 47
    new-instance v0, Landroid/graphics/RectF;

    .line 48
    .line 49
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 50
    .line 51
    .line 52
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇888:Landroid/graphics/RectF;

    .line 53
    .line 54
    new-instance v0, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;

    .line 55
    .line 56
    invoke-direct {v0}, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;-><init>()V

    .line 57
    .line 58
    .line 59
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->oO80:Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;

    .line 60
    .line 61
    const/high16 v1, 0x41c00000    # 24.0f

    .line 62
    .line 63
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    int-to-float v1, v1

    .line 68
    iput v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OO0o〇〇〇〇0:F

    .line 69
    .line 70
    const/high16 v1, 0x40400000    # 3.0f

    .line 71
    .line 72
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    int-to-float v1, v1

    .line 77
    iput v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇8o8o〇:F

    .line 78
    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    const v2, 0x7f0806c2

    .line 84
    .line 85
    .line 86
    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 91
    .line 92
    .line 93
    move-result v2

    .line 94
    int-to-float v2, v2

    .line 95
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;->〇O〇(F)V

    .line 96
    .line 97
    .line 98
    iput-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O8o08O:Landroid/graphics/Bitmap;

    .line 99
    .line 100
    new-instance v1, Landroid/graphics/RectF;

    .line 101
    .line 102
    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 103
    .line 104
    .line 105
    iput-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oooo8o0〇:Landroid/graphics/RectF;

    .line 106
    .line 107
    const/high16 v1, 0x42000000    # 32.0f

    .line 108
    .line 109
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    int-to-float v1, v1

    .line 114
    iput v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇808〇:F

    .line 115
    .line 116
    const/high16 v1, 0x40000000    # 2.0f

    .line 117
    .line 118
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 119
    .line 120
    .line 121
    move-result v1

    .line 122
    int-to-float v1, v1

    .line 123
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 124
    .line 125
    .line 126
    const v1, 0x7f0601ee

    .line 127
    .line 128
    .line 129
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 130
    .line 131
    .line 132
    move-result p1

    .line 133
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 134
    .line 135
    .line 136
    sget-object p1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 137
    .line 138
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 139
    .line 140
    .line 141
    iget p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇808〇:F

    .line 142
    .line 143
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;->〇〇8O0〇8(F)V

    .line 144
    .line 145
    .line 146
    iget p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇808〇:F

    .line 147
    .line 148
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;->〇O00(F)V

    .line 149
    .line 150
    .line 151
    return-void

    .line 152
    nop

    .line 153
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private final OO0o〇〇〇〇0(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oo08:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oo08:Landroid/graphics/Path;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o〇:[F

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    aget v2, v1, v2

    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    aget v1, v1, v3

    .line 15
    .line 16
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oo08:Landroid/graphics/Path;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o〇:[F

    .line 22
    .line 23
    const/4 v2, 0x2

    .line 24
    aget v2, v1, v2

    .line 25
    .line 26
    const/4 v3, 0x3

    .line 27
    aget v1, v1, v3

    .line 28
    .line 29
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oo08:Landroid/graphics/Path;

    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o〇:[F

    .line 35
    .line 36
    const/4 v2, 0x4

    .line 37
    aget v2, v1, v2

    .line 38
    .line 39
    const/4 v3, 0x5

    .line 40
    aget v1, v1, v3

    .line 41
    .line 42
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oo08:Landroid/graphics/Path;

    .line 46
    .line 47
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o〇:[F

    .line 48
    .line 49
    const/4 v2, 0x6

    .line 50
    aget v2, v1, v2

    .line 51
    .line 52
    const/4 v3, 0x7

    .line 53
    aget v1, v1, v3

    .line 54
    .line 55
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 56
    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oo08:Landroid/graphics/Path;

    .line 59
    .line 60
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oo08:Landroid/graphics/Path;

    .line 64
    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->o〇0:Landroid/graphics/Paint;

    .line 66
    .line 67
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final Oooo8o0〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    cmpg-float v0, v0, v1

    .line 9
    .line 10
    if-gtz v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 13
    .line 14
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    cmpg-float v0, v0, v1

    .line 19
    .line 20
    if-gtz v0, :cond_0

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    :goto_0
    return v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇0〇O0088o()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇80〇808〇O:Lcom/intsig/camscanner/capture/count/crop/CropWindowMoveHandler;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇80〇808〇O:Lcom/intsig/camscanner/capture/count/crop/CropWindowMoveHandler;

    .line 7
    .line 8
    :cond_0
    const/4 v0, 0x1

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8o8o〇(Landroid/graphics/Canvas;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O8o08O:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-float v0, v0

    .line 8
    const/high16 v1, 0x40000000    # 2.0f

    .line 9
    .line 10
    div-float/2addr v0, v1

    .line 11
    const/4 v1, 0x0

    .line 12
    :goto_0
    const/16 v2, 0x8

    .line 13
    .line 14
    if-ge v1, v2, :cond_0

    .line 15
    .line 16
    iget-object v3, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o〇:[F

    .line 17
    .line 18
    rem-int/lit8 v4, v1, 0x8

    .line 19
    .line 20
    aget v4, v3, v4

    .line 21
    .line 22
    add-int/lit8 v5, v1, 0x1

    .line 23
    .line 24
    rem-int/2addr v5, v2

    .line 25
    aget v5, v3, v5

    .line 26
    .line 27
    add-int/lit8 v6, v1, 0x2

    .line 28
    .line 29
    rem-int/lit8 v7, v6, 0x8

    .line 30
    .line 31
    aget v7, v3, v7

    .line 32
    .line 33
    add-int/lit8 v1, v1, 0x3

    .line 34
    .line 35
    rem-int/2addr v1, v2

    .line 36
    aget v1, v3, v1

    .line 37
    .line 38
    add-float/2addr v4, v7

    .line 39
    const/4 v2, 0x2

    .line 40
    int-to-float v2, v2

    .line 41
    div-float/2addr v4, v2

    .line 42
    add-float/2addr v5, v1

    .line 43
    div-float/2addr v5, v2

    .line 44
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 48
    .line 49
    .line 50
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O8o08O:Landroid/graphics/Bitmap;

    .line 51
    .line 52
    neg-float v2, v0

    .line 53
    iget-object v3, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->o〇0:Landroid/graphics/Paint;

    .line 54
    .line 55
    invoke-virtual {p1, v1, v2, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 59
    .line 60
    .line 61
    move v1, v6

    .line 62
    goto :goto_0

    .line 63
    :cond_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final 〇O00(FF)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OO0o〇〇:Z

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oooo8o0〇:Landroid/graphics/RectF;

    .line 5
    .line 6
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 7
    .line 8
    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->oO80:Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;->〇0〇O0088o(Landroid/graphics/RectF;)V

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->oO80:Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;

    .line 19
    .line 20
    iget v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OO0o〇〇〇〇0:F

    .line 21
    .line 22
    sget-object v3, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler$CropShape;->RECTANGLE:Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler$CropShape;

    .line 23
    .line 24
    invoke-virtual {v1, p1, p2, v2, v3}, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;->o〇0(FFFLcom/intsig/camscanner/capture/count/crop/CropWindowHandler$CropShape;)Lcom/intsig/camscanner/capture/count/crop/CropWindowMoveHandler;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇80〇808〇O:Lcom/intsig/camscanner/capture/count/crop/CropWindowMoveHandler;

    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->oO80:Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;

    .line 31
    .line 32
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;->〇〇808〇(Z)V

    .line 33
    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇80〇808〇O:Lcom/intsig/camscanner/capture/count/crop/CropWindowMoveHandler;

    .line 36
    .line 37
    if-eqz p1, :cond_0

    .line 38
    .line 39
    const/4 v0, 0x1

    .line 40
    :cond_0
    return v0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇O〇(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/RectF;->centerY()F

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    .line 14
    .line 15
    .line 16
    move-result p2

    .line 17
    sub-float/2addr v0, v1

    .line 18
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    const/4 v1, 0x0

    .line 23
    cmpl-float v0, v0, v1

    .line 24
    .line 25
    if-gtz v0, :cond_1

    .line 26
    .line 27
    sub-float/2addr p1, p2

    .line 28
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    cmpl-float p1, p1, v1

    .line 33
    .line 34
    if-lez p1, :cond_0

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 p1, 0x0

    .line 38
    goto :goto_1

    .line 39
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 40
    :goto_1
    return p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇〇8O0〇8(FF)Z
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇80〇808〇O:Lcom/intsig/camscanner/capture/count/crop/CropWindowMoveHandler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    return p1

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o00〇〇Oo:Landroid/view/View;

    .line 8
    .line 9
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v10, 0x1

    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    invoke-interface {v1, v10}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 17
    .line 18
    .line 19
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 20
    .line 21
    iget-object v4, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇888:Landroid/graphics/RectF;

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o00〇〇Oo:Landroid/view/View;

    .line 24
    .line 25
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    .line 26
    .line 27
    .line 28
    move-result v5

    .line 29
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o00〇〇Oo:Landroid/view/View;

    .line 30
    .line 31
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    .line 32
    .line 33
    .line 34
    move-result v6

    .line 35
    iget v7, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇8o8o〇:F

    .line 36
    .line 37
    const/4 v8, 0x0

    .line 38
    const/high16 v9, 0x3f800000    # 1.0f

    .line 39
    .line 40
    move v2, p1

    .line 41
    move v3, p2

    .line 42
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/camscanner/capture/count/crop/CropWindowMoveHandler;->OO0o〇〇(Landroid/graphics/RectF;FFLandroid/graphics/RectF;IIFZF)V

    .line 43
    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 46
    .line 47
    iget-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oooo8o0〇:Landroid/graphics/RectF;

    .line 48
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O〇(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OO0o〇〇:Z

    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->oO80:Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;

    .line 56
    .line 57
    iget-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 58
    .line 59
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;->〇0〇O0088o(Landroid/graphics/RectF;)V

    .line 60
    .line 61
    .line 62
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 63
    .line 64
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OoO8(Landroid/graphics/RectF;)[F

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o〇:[F

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o00〇〇Oo:Landroid/view/View;

    .line 71
    .line 72
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 73
    .line 74
    .line 75
    return v10
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method


# virtual methods
.method public final OO0o〇〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    cmpl-float v0, v0, v1

    .line 9
    .line 10
    if-lez v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 13
    .line 14
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    cmpl-float v0, v0, v1

    .line 19
    .line 20
    if-lez v0, :cond_0

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    :goto_0
    return v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo08(Landroid/graphics/RectF;)V
    .locals 3
    .param p1    # Landroid/graphics/RectF;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "rectF"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇888:Landroid/graphics/RectF;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 9
    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->oO80:Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇888:Landroid/graphics/RectF;

    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇888:Landroid/graphics/RectF;

    .line 20
    .line 21
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    const/high16 v2, 0x3f800000    # 1.0f

    .line 26
    .line 27
    invoke-virtual {p1, v0, v1, v2, v2}, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;->Oooo8o0〇(FFFF)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final OoO8(Landroid/graphics/RectF;)[F
    .locals 5
    .param p1    # Landroid/graphics/RectF;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "rect"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/16 v0, 0x8

    .line 7
    .line 8
    new-array v0, v0, [F

    .line 9
    .line 10
    iget v1, p1, Landroid/graphics/RectF;->left:F

    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    aput v1, v0, v2

    .line 14
    .line 15
    iget v2, p1, Landroid/graphics/RectF;->top:F

    .line 16
    .line 17
    const/4 v3, 0x1

    .line 18
    aput v2, v0, v3

    .line 19
    .line 20
    iget v3, p1, Landroid/graphics/RectF;->right:F

    .line 21
    .line 22
    const/4 v4, 0x2

    .line 23
    aput v3, v0, v4

    .line 24
    .line 25
    const/4 v4, 0x3

    .line 26
    aput v2, v0, v4

    .line 27
    .line 28
    const/4 v2, 0x4

    .line 29
    aput v3, v0, v2

    .line 30
    .line 31
    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    .line 32
    .line 33
    const/4 v2, 0x5

    .line 34
    aput p1, v0, v2

    .line 35
    .line 36
    const/4 v2, 0x6

    .line 37
    aput v1, v0, v2

    .line 38
    .line 39
    const/4 v1, 0x7

    .line 40
    aput p1, v0, v1

    .line 41
    .line 42
    return-object v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final o800o8O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    .line 4
    .line 5
    .line 6
    const/16 v0, 0x8

    .line 7
    .line 8
    new-array v0, v0, [F

    .line 9
    .line 10
    fill-array-data v0, :array_0

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o〇:[F

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o00〇〇Oo:Landroid/view/View;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final oo88o8O(F)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇808〇:F

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->oO80:Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;->〇〇8O0〇8(F)V

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->oO80:Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;

    .line 9
    .line 10
    iget v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇808〇:F

    .line 11
    .line 12
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/count/crop/CropWindowHandler;->〇O00(F)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇80〇808〇O(FF)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇O888o0o([F)V
    .locals 2
    .param p1    # [F
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "location"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    array-length v0, p1

    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    if-eq v0, v1, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o〇:[F

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 15
    .line 16
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇oOO8O8([F)F

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 23
    .line 24
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0000OOO([F)F

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 31
    .line 32
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o〇〇0〇([F)F

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 39
    .line 40
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->O8ooOoo〇([F)F

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    iput p1, v0, Landroid/graphics/RectF;->bottom:F

    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇o00〇〇Oo:Landroid/view/View;

    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final 〇O8o08O()Landroid/graphics/RectF;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o00〇〇Oo(Landroid/graphics/Canvas;Landroid/graphics/Matrix;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "canvas"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "displayMatrix"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p2, "scaleMatrix"

    .line 12
    .line 13
    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string p2, "baseMatrix"

    .line 17
    .line 18
    invoke-static {p4, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 22
    .line 23
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    .line 24
    .line 25
    .line 26
    move-result p2

    .line 27
    const/4 p3, 0x0

    .line 28
    cmpg-float p2, p2, p3

    .line 29
    .line 30
    if-gtz p2, :cond_0

    .line 31
    .line 32
    iget-object p2, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->O8:Landroid/graphics/RectF;

    .line 33
    .line 34
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    .line 35
    .line 36
    .line 37
    move-result p2

    .line 38
    cmpg-float p2, p2, p3

    .line 39
    .line 40
    if-gtz p2, :cond_0

    .line 41
    .line 42
    return-void

    .line 43
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OO0o〇〇〇〇0(Landroid/graphics/Canvas;)V

    .line 44
    .line 45
    .line 46
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇8o8o〇(Landroid/graphics/Canvas;)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method public 〇o〇(Landroid/view/MotionEvent;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Matrix;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "event"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "scaleMatrix"

    .line 7
    .line 8
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->Oooo8o0〇()Z

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    const/4 p3, 0x0

    .line 16
    if-eqz p2, :cond_0

    .line 17
    .line 18
    return p3

    .line 19
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 20
    .line 21
    .line 22
    move-result p2

    .line 23
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    if-eqz p1, :cond_3

    .line 32
    .line 33
    const/4 v1, 0x1

    .line 34
    if-eq p1, v1, :cond_2

    .line 35
    .line 36
    const/4 v1, 0x2

    .line 37
    if-eq p1, v1, :cond_1

    .line 38
    .line 39
    return p3

    .line 40
    :cond_1
    invoke-direct {p0, p2, v0}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇〇8O0〇8(FF)Z

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    return p1

    .line 45
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇0〇O0088o()Z

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    return p1

    .line 50
    :cond_3
    invoke-direct {p0, p2, v0}, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->〇O00(FF)Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    return p1
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public final 〇〇808〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/count/widget/CountLocationSelector;->OO0o〇〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

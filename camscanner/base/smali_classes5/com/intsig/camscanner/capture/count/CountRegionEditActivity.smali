.class public final Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "CountRegionEditActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/count/CountRegionEditActivity$LoadBitmapTask;,
        Lcom/intsig/camscanner/capture/count/CountRegionEditActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O88O:Lcom/intsig/camscanner/capture/count/CountRegionEditActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:Landroid/net/Uri;

.field private o8oOOo:F

.field private ooo0〇〇O:Lcom/intsig/camscanner/view/ImageEditView;

.field private 〇O〇〇O8:F

.field private 〇o0O:[I

.field private 〇〇08O:Lcom/intsig/camscanner/loadimage/RotateBitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->O88O:Lcom/intsig/camscanner/capture/count/CountRegionEditActivity$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O0〇(Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;)Lcom/intsig/camscanner/view/ImageEditView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->ooo0〇〇O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final OO0O([ILandroid/graphics/Rect;)[F
    .locals 18

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p2

    .line 6
    .line 7
    iget-object v3, v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->〇〇08O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    if-eqz v3, :cond_0

    .line 11
    .line 12
    invoke-virtual {v3}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->Oo08()I

    .line 13
    .line 14
    .line 15
    move-result v3

    .line 16
    int-to-float v3, v3

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v3, 0x0

    .line 19
    :goto_0
    iget-object v5, v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->〇〇08O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 20
    .line 21
    if-eqz v5, :cond_1

    .line 22
    .line 23
    invoke-virtual {v5}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o00〇〇Oo()I

    .line 24
    .line 25
    .line 26
    move-result v5

    .line 27
    int-to-float v5, v5

    .line 28
    goto :goto_1

    .line 29
    :cond_1
    const/4 v5, 0x0

    .line 30
    :goto_1
    float-to-int v6, v5

    .line 31
    const/4 v7, 0x2

    .line 32
    div-int/2addr v6, v7

    .line 33
    float-to-int v8, v3

    .line 34
    div-int/2addr v8, v7

    .line 35
    const/4 v9, 0x7

    .line 36
    const/4 v10, 0x6

    .line 37
    const/4 v11, 0x5

    .line 38
    const/4 v12, 0x4

    .line 39
    const/4 v13, 0x3

    .line 40
    const/4 v14, 0x1

    .line 41
    const/16 v15, 0x8

    .line 42
    .line 43
    const/16 v16, 0x0

    .line 44
    .line 45
    if-eqz v2, :cond_2

    .line 46
    .line 47
    if-eqz v1, :cond_2

    .line 48
    .line 49
    array-length v1, v1

    .line 50
    if-lt v1, v7, :cond_2

    .line 51
    .line 52
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->o808o8o08()Landroid/graphics/Matrix;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    if-eqz v1, :cond_2

    .line 57
    .line 58
    invoke-virtual {v1, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 59
    .line 60
    .line 61
    new-instance v3, Landroid/graphics/RectF;

    .line 62
    .line 63
    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 64
    .line 65
    .line 66
    new-instance v4, Landroid/graphics/RectF;

    .line 67
    .line 68
    iget v5, v2, Landroid/graphics/Rect;->left:I

    .line 69
    .line 70
    int-to-float v5, v5

    .line 71
    iget v6, v2, Landroid/graphics/Rect;->top:I

    .line 72
    .line 73
    int-to-float v6, v6

    .line 74
    iget v8, v2, Landroid/graphics/Rect;->right:I

    .line 75
    .line 76
    int-to-float v8, v8

    .line 77
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    .line 78
    .line 79
    int-to-float v2, v2

    .line 80
    invoke-direct {v4, v5, v6, v8, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {v1, v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 84
    .line 85
    .line 86
    new-instance v1, Landroid/graphics/Rect;

    .line 87
    .line 88
    iget v2, v3, Landroid/graphics/RectF;->left:F

    .line 89
    .line 90
    float-to-int v2, v2

    .line 91
    iget v4, v3, Landroid/graphics/RectF;->top:F

    .line 92
    .line 93
    float-to-int v4, v4

    .line 94
    iget v5, v3, Landroid/graphics/RectF;->right:F

    .line 95
    .line 96
    float-to-int v5, v5

    .line 97
    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    .line 98
    .line 99
    float-to-int v3, v3

    .line 100
    invoke-direct {v1, v2, v4, v5, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 101
    .line 102
    .line 103
    new-array v2, v15, [F

    .line 104
    .line 105
    iget v3, v1, Landroid/graphics/Rect;->left:I

    .line 106
    .line 107
    int-to-float v4, v3

    .line 108
    aput v4, v2, v16

    .line 109
    .line 110
    iget v4, v1, Landroid/graphics/Rect;->top:I

    .line 111
    .line 112
    int-to-float v5, v4

    .line 113
    aput v5, v2, v14

    .line 114
    .line 115
    iget v5, v1, Landroid/graphics/Rect;->right:I

    .line 116
    .line 117
    int-to-float v6, v5

    .line 118
    aput v6, v2, v7

    .line 119
    .line 120
    int-to-float v4, v4

    .line 121
    aput v4, v2, v13

    .line 122
    .line 123
    int-to-float v4, v5

    .line 124
    aput v4, v2, v12

    .line 125
    .line 126
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    .line 127
    .line 128
    int-to-float v4, v1

    .line 129
    aput v4, v2, v11

    .line 130
    .line 131
    int-to-float v3, v3

    .line 132
    aput v3, v2, v10

    .line 133
    .line 134
    int-to-float v1, v1

    .line 135
    aput v1, v2, v9

    .line 136
    .line 137
    return-object v2

    .line 138
    :cond_2
    iget v1, v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->o8oOOo:F

    .line 139
    .line 140
    cmpg-float v1, v1, v4

    .line 141
    .line 142
    if-gtz v1, :cond_7

    .line 143
    .line 144
    iget v1, v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->〇O〇〇O8:F

    .line 145
    .line 146
    cmpg-float v1, v1, v4

    .line 147
    .line 148
    if-gtz v1, :cond_7

    .line 149
    .line 150
    iget-object v1, v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->ooo0〇〇O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 151
    .line 152
    if-eqz v1, :cond_3

    .line 153
    .line 154
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 155
    .line 156
    .line 157
    move-result v1

    .line 158
    goto :goto_2

    .line 159
    :cond_3
    const/4 v1, 0x0

    .line 160
    :goto_2
    int-to-float v1, v1

    .line 161
    const/high16 v2, 0x3f800000    # 1.0f

    .line 162
    .line 163
    mul-float v1, v1, v2

    .line 164
    .line 165
    div-float/2addr v1, v3

    .line 166
    iget-object v4, v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->ooo0〇〇O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 167
    .line 168
    if-eqz v4, :cond_4

    .line 169
    .line 170
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    .line 171
    .line 172
    .line 173
    move-result v4

    .line 174
    goto :goto_3

    .line 175
    :cond_4
    const/4 v4, 0x0

    .line 176
    :goto_3
    int-to-float v4, v4

    .line 177
    mul-float v4, v4, v2

    .line 178
    .line 179
    div-float/2addr v4, v5

    .line 180
    invoke-static {v1, v4}, Ljava/lang/Math;->min(FF)F

    .line 181
    .line 182
    .line 183
    move-result v1

    .line 184
    mul-float v2, v1, v3

    .line 185
    .line 186
    mul-float v4, v1, v5

    .line 187
    .line 188
    const/16 v9, 0xf0

    .line 189
    .line 190
    invoke-static {v0, v9}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 191
    .line 192
    .line 193
    move-result v10

    .line 194
    int-to-float v10, v10

    .line 195
    const v17, 0x3f666666    # 0.9f

    .line 196
    .line 197
    .line 198
    cmpl-float v2, v2, v10

    .line 199
    .line 200
    if-lez v2, :cond_5

    .line 201
    .line 202
    invoke-static {v0, v9}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 203
    .line 204
    .line 205
    move-result v2

    .line 206
    int-to-float v2, v2

    .line 207
    div-float/2addr v2, v1

    .line 208
    div-float/2addr v2, v3

    .line 209
    goto :goto_4

    .line 210
    :cond_5
    const v2, 0x3f666666    # 0.9f

    .line 211
    .line 212
    .line 213
    :goto_4
    iput v2, v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->o8oOOo:F

    .line 214
    .line 215
    const/16 v2, 0xb4

    .line 216
    .line 217
    invoke-static {v0, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 218
    .line 219
    .line 220
    move-result v9

    .line 221
    int-to-float v9, v9

    .line 222
    cmpl-float v4, v4, v9

    .line 223
    .line 224
    if-lez v4, :cond_6

    .line 225
    .line 226
    invoke-static {v0, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 227
    .line 228
    .line 229
    move-result v2

    .line 230
    int-to-float v2, v2

    .line 231
    div-float/2addr v2, v1

    .line 232
    div-float v17, v2, v5

    .line 233
    .line 234
    move/from16 v1, v17

    .line 235
    .line 236
    goto :goto_5

    .line 237
    :cond_6
    const v1, 0x3f666666    # 0.9f

    .line 238
    .line 239
    .line 240
    :goto_5
    iput v1, v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->〇O〇〇O8:F

    .line 241
    .line 242
    :cond_7
    iget v1, v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->o8oOOo:F

    .line 243
    .line 244
    mul-float v3, v3, v1

    .line 245
    .line 246
    iget v1, v0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->〇O〇〇O8:F

    .line 247
    .line 248
    mul-float v5, v5, v1

    .line 249
    .line 250
    new-array v1, v15, [F

    .line 251
    .line 252
    int-to-float v2, v8

    .line 253
    int-to-float v4, v7

    .line 254
    div-float/2addr v3, v4

    .line 255
    sub-float v8, v2, v3

    .line 256
    .line 257
    aput v8, v1, v16

    .line 258
    .line 259
    int-to-float v6, v6

    .line 260
    div-float/2addr v5, v4

    .line 261
    sub-float v4, v6, v5

    .line 262
    .line 263
    aput v4, v1, v14

    .line 264
    .line 265
    add-float/2addr v2, v3

    .line 266
    aput v2, v1, v7

    .line 267
    .line 268
    aput v4, v1, v13

    .line 269
    .line 270
    aput v2, v1, v12

    .line 271
    .line 272
    add-float/2addr v6, v5

    .line 273
    aput v6, v1, v11

    .line 274
    .line 275
    const/4 v2, 0x6

    .line 276
    aput v8, v1, v2

    .line 277
    .line 278
    const/4 v2, 0x7

    .line 279
    aput v6, v1, v2

    .line 280
    .line 281
    return-object v1
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public static final synthetic O〇080〇o0(Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;)Lcom/intsig/camscanner/loadimage/RotateBitmap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->〇〇08O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;)Landroid/net/Uri;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->O0O:Landroid/net/Uri;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic O〇〇O80o8(Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;[ILandroid/graphics/Rect;)[F
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->OO0O([ILandroid/graphics/Rect;)[F

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public static final synthetic o0Oo(Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;[I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->〇o0O:[I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o808o8o08()Landroid/graphics/Matrix;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->〇〇08O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_3

    .line 5
    .line 6
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->ooo0〇〇O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 7
    .line 8
    if-eqz v2, :cond_3

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->〇o0O:[I

    .line 11
    .line 12
    if-eqz v2, :cond_3

    .line 13
    .line 14
    array-length v3, v2

    .line 15
    const/4 v4, 0x2

    .line 16
    const/4 v5, 0x1

    .line 17
    const/4 v6, 0x0

    .line 18
    if-lt v3, v4, :cond_0

    .line 19
    .line 20
    aget v3, v2, v6

    .line 21
    .line 22
    if-lez v3, :cond_0

    .line 23
    .line 24
    aget v3, v2, v5

    .line 25
    .line 26
    if-lez v3, :cond_0

    .line 27
    .line 28
    const/4 v3, 0x1

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const/4 v3, 0x0

    .line 31
    :goto_0
    if-eqz v3, :cond_1

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_1
    move-object v2, v1

    .line 35
    :goto_1
    if-eqz v2, :cond_3

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->Oo08()I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    int-to-float v3, v3

    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o00〇〇Oo()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    int-to-float v0, v0

    .line 47
    const/4 v4, 0x0

    .line 48
    cmpg-float v7, v3, v4

    .line 49
    .line 50
    if-lez v7, :cond_3

    .line 51
    .line 52
    cmpg-float v4, v0, v4

    .line 53
    .line 54
    if-gtz v4, :cond_2

    .line 55
    .line 56
    goto :goto_2

    .line 57
    :cond_2
    aget v1, v2, v6

    .line 58
    .line 59
    int-to-float v1, v1

    .line 60
    const/high16 v4, 0x3f800000    # 1.0f

    .line 61
    .line 62
    mul-float v1, v1, v4

    .line 63
    .line 64
    div-float/2addr v1, v3

    .line 65
    aget v2, v2, v5

    .line 66
    .line 67
    int-to-float v2, v2

    .line 68
    mul-float v2, v2, v4

    .line 69
    .line 70
    div-float/2addr v2, v0

    .line 71
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    new-instance v1, Landroid/graphics/Matrix;

    .line 76
    .line 77
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 78
    .line 79
    .line 80
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 81
    .line 82
    .line 83
    :cond_3
    :goto_2
    return-object v1
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final o〇08oO80o(Landroid/graphics/Rect;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    const-string v1, "finish_extra_rect_region"

    .line 9
    .line 10
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 11
    .line 12
    .line 13
    :cond_0
    const/4 p1, -0x1

    .line 14
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final o〇o08〇([I)Z
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->o808o8o08()Landroid/graphics/Matrix;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    array-length v2, p1

    .line 11
    const/4 v3, 0x6

    .line 12
    if-lt v2, v3, :cond_0

    .line 13
    .line 14
    new-instance v2, Landroid/graphics/RectF;

    .line 15
    .line 16
    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 17
    .line 18
    .line 19
    new-instance v3, Landroid/graphics/RectF;

    .line 20
    .line 21
    const/4 v4, 0x0

    .line 22
    aget v4, p1, v4

    .line 23
    .line 24
    int-to-float v4, v4

    .line 25
    aget v5, p1, v1

    .line 26
    .line 27
    int-to-float v5, v5

    .line 28
    const/4 v6, 0x2

    .line 29
    aget v6, p1, v6

    .line 30
    .line 31
    int-to-float v6, v6

    .line 32
    const/4 v7, 0x5

    .line 33
    aget p1, p1, v7

    .line 34
    .line 35
    int-to-float p1, p1

    .line 36
    invoke-direct {v3, v4, v5, v6, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 40
    .line 41
    .line 42
    new-instance p1, Landroid/graphics/Rect;

    .line 43
    .line 44
    iget v0, v2, Landroid/graphics/RectF;->left:F

    .line 45
    .line 46
    float-to-int v0, v0

    .line 47
    iget v3, v2, Landroid/graphics/RectF;->top:F

    .line 48
    .line 49
    float-to-int v3, v3

    .line 50
    iget v4, v2, Landroid/graphics/RectF;->right:F

    .line 51
    .line 52
    float-to-int v4, v4

    .line 53
    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    .line 54
    .line 55
    float-to-int v2, v2

    .line 56
    invoke-direct {p1, v0, v3, v4, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 57
    .line 58
    .line 59
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->o〇08oO80o(Landroid/graphics/Rect;)V

    .line 60
    .line 61
    .line 62
    :cond_0
    return v1
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static final synthetic 〇oO88o(Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;Lcom/intsig/camscanner/loadimage/RotateBitmap;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->〇〇08O:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/app/AppUtil;->〇〇o8(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    const/4 v0, 0x0

    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    const-string v1, "extra_path"

    .line 12
    .line 13
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    check-cast p1, Landroid/net/Uri;

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    move-object p1, v0

    .line 21
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->O0O:Landroid/net/Uri;

    .line 22
    .line 23
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    if-eqz p1, :cond_1

    .line 28
    .line 29
    const-string v0, "extra_rect_init_region"

    .line 30
    .line 31
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    move-object v0, p1

    .line 36
    check-cast v0, Landroid/graphics/Rect;

    .line 37
    .line 38
    :cond_1
    const p1, 0x7f0a073b

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    check-cast p1, Lcom/intsig/camscanner/view/ImageEditView;

    .line 46
    .line 47
    iput-object p1, p0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->ooo0〇〇O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 48
    .line 49
    const p1, 0x7f0a14eb

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    if-eqz p1, :cond_2

    .line 57
    .line 58
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    .line 60
    .line 61
    :cond_2
    new-instance p1, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity$LoadBitmapTask;

    .line 62
    .line 63
    invoke-direct {p1, p0, v0}, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity$LoadBitmapTask;-><init>(Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;Landroid/graphics/Rect;)V

    .line 64
    .line 65
    .line 66
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇8o8o〇()Ljava/util/concurrent/ExecutorService;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    const/4 v1, 0x0

    .line 71
    new-array v1, v1, [Ljava/lang/Void;

    .line 72
    .line 73
    invoke-virtual {p1, v0, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "v"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    const v0, 0x7f0a14eb

    .line 11
    .line 12
    .line 13
    if-ne p1, v0, :cond_1

    .line 14
    .line 15
    const-string p1, "CSCountCrop"

    .line 16
    .line 17
    const-string v0, "confirm"

    .line 18
    .line 19
    invoke-static {p1, v0}, Lcom/intsig/log/LogAgentHelper;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->ooo0〇〇O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 23
    .line 24
    if-eqz p1, :cond_0

    .line 25
    .line 26
    const/4 v0, 0x0

    .line 27
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/ImageEditView;->〇oOO8O8(Z)[I

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 p1, 0x0

    .line 33
    :goto_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/count/CountRegionEditActivity;->o〇o08〇([I)Z

    .line 34
    .line 35
    .line 36
    :cond_1
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    const-string p1, "CountRegionEditActivity"

    .line 5
    .line 6
    const-string p2, "onKeyDown: intercept physical back"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    return p1

    .line 13
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d0067

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

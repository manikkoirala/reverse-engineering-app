.class final Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;
.super Ljava/lang/Object;
.source "PreviewInvoiceRecognizeObserver.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DetectRunnable"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic O8o08O8O:Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver;

.field private final OO:[Z
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectItem;

.field private o〇00O:Z

.field private 〇08O〇00〇o:I

.field private volatile 〇OOo8〇0:Z


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->O8o08O8O:Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇OOo8〇0:Z

    .line 8
    .line 9
    new-array v0, p1, [Z

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->OO:[Z

    .line 12
    .line 13
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->o〇00O:Z

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final setResult(Z)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇08O〇00〇o:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->OO:[Z

    .line 4
    .line 5
    array-length v2, v1

    .line 6
    if-lt v0, v2, :cond_0

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇08O〇00〇o:I

    .line 10
    .line 11
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇08O〇00〇o:I

    .line 12
    .line 13
    aput-boolean p1, v1, v0

    .line 14
    .line 15
    add-int/lit8 v0, v0, 0x1

    .line 16
    .line 17
    iput v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇08O〇00〇o:I

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇080(Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectItem;)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    const-string v1, "PreviewInvoiceRecognizeObserver"

    .line 3
    .line 4
    if-eqz p1, :cond_1

    .line 5
    .line 6
    :try_start_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectItem;->〇080()[B

    .line 7
    .line 8
    .line 9
    move-result-object v2

    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectItem;->〇o〇()I

    .line 11
    .line 12
    .line 13
    move-result v3

    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectItem;->〇o00〇〇Oo()I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    invoke-static {v2, v3, p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->detectInvoice([BII)F

    .line 19
    .line 20
    .line 21
    move-result p1
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    goto :goto_0

    .line 23
    :catch_0
    move-exception p1

    .line 24
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 25
    .line 26
    .line 27
    const/4 p1, 0x0

    .line 28
    :goto_0
    const v1, 0x3f4ccccd    # 0.8f

    .line 29
    .line 30
    .line 31
    cmpl-float p1, p1, v1

    .line 32
    .line 33
    if-lez p1, :cond_0

    .line 34
    .line 35
    const/4 v0, 0x1

    .line 36
    :cond_0
    return v0

    .line 37
    :cond_1
    const-string p1, "checkCardType - detectItem is null"

    .line 38
    .line 39
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    return v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method


# virtual methods
.method public final O8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇OOo8〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final Oo08()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->OO:[Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 5
    .line 6
    .line 7
    iput-boolean v1, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->o〇00O:Z

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇OOo8〇0:Z

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final oO80(Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectItem;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->o0:Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectItem;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final o〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->o〇00O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public run()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->o0:Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectItem;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    iput-boolean v1, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇OOo8〇0:Z

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->O8o08O8O:Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver;

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver;->OO0o〇〇(Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->O8o08O8O:Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver;

    .line 16
    .line 17
    monitor-enter v0

    .line 18
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 19
    .line 20
    .line 21
    move-result-wide v3

    .line 22
    invoke-static {v2}, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver;->〇O8o08O(Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver;)J

    .line 23
    .line 24
    .line 25
    move-result-wide v5

    .line 26
    sub-long/2addr v3, v5

    .line 27
    sget-object v2, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver;->〇80〇808〇O:Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$Companion;

    .line 28
    .line 29
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$Companion;->〇080()J

    .line 30
    .line 31
    .line 32
    move-result-wide v5

    .line 33
    cmp-long v7, v3, v5

    .line 34
    .line 35
    if-gtz v7, :cond_1

    .line 36
    .line 37
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$Companion;->〇080()J

    .line 38
    .line 39
    .line 40
    move-result-wide v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 41
    sub-long/2addr v5, v3

    .line 42
    :try_start_1
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V

    .line 43
    .line 44
    .line 45
    const-string v2, "PreviewInvoiceRecognizeObserver"

    .line 46
    .line 47
    new-instance v5, Ljava/lang/StringBuilder;

    .line 48
    .line 49
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .line 51
    .line 52
    const-string v6, "waiting for frame - "

    .line 53
    .line 54
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v3

    .line 64
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :catchall_0
    move-exception v2

    .line 69
    :try_start_2
    const-string v3, "PreviewInvoiceRecognizeObserver"

    .line 70
    .line 71
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :catch_0
    move-exception v2

    .line 76
    const-string v3, "PreviewInvoiceRecognizeObserver"

    .line 77
    .line 78
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 79
    .line 80
    .line 81
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 82
    .line 83
    .line 84
    move-result-object v2

    .line 85
    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 86
    .line 87
    .line 88
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇o〇()Z

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    if-nez v2, :cond_2

    .line 93
    .line 94
    iget-object v2, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->o0:Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectItem;

    .line 95
    .line 96
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇080(Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectItem;)Z

    .line 97
    .line 98
    .line 99
    move-result v2

    .line 100
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->setResult(Z)V

    .line 101
    .line 102
    .line 103
    :cond_2
    sget-object v2, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 104
    .line 105
    monitor-exit v0

    .line 106
    iput-boolean v1, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇OOo8〇0:Z

    .line 107
    .line 108
    return-void

    .line 109
    :catchall_1
    move-exception v1

    .line 110
    monitor-exit v0

    .line 111
    throw v1
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final 〇o00〇〇Oo()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->o〇00O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇o〇()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->OO:[Z

    .line 2
    .line 3
    array-length v1, v0

    .line 4
    const/4 v2, 0x0

    .line 5
    const/4 v3, 0x0

    .line 6
    :goto_0
    if-ge v3, v1, :cond_1

    .line 7
    .line 8
    aget-boolean v4, v0, v3

    .line 9
    .line 10
    if-nez v4, :cond_0

    .line 11
    .line 12
    return v2

    .line 13
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_1
    const/4 v0, 0x1

    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇888(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/PreviewInvoiceRecognizeObserver$DetectRunnable;->〇OOo8〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.class public interface abstract Lcom/intsig/camscanner/capture/control/ICaptureControl;
.super Ljava/lang/Object;
.source "ICaptureControl.java"


# virtual methods
.method public abstract O0()Z
.end method

.method public abstract O00()Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract O000()Z
.end method

.method public abstract O00O(Z)V
.end method

.method public abstract O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract O0O8OO088()Z
.end method

.method public abstract O0OO8〇0()Z
.end method

.method public abstract O0o(Ljava/lang/String;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract O0oO008()V
.end method

.method public abstract O0oo0o0〇()V
.end method

.method public abstract O0o〇O0〇()V
.end method

.method public abstract O0o〇〇Oo()Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener;
.end method

.method public abstract O0〇OO8(ZZILcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/callback/Callback0;)V
    .param p4    # Lcom/intsig/camscanner/capture/CaptureMode;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcom/intsig/callback/Callback0;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract O880oOO08()V
.end method

.method public abstract O8O〇(Lcom/intsig/camscanner/capture/CaptureMode;)V
.end method

.method public abstract O8O〇88oO0()V
.end method

.method public abstract OO0o〇〇()Ljava/lang/String;
.end method

.method public abstract OO0o〇〇〇〇0()Landroid/os/Handler;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract OO0〇〇8(ZLcom/intsig/callback/Callback0;)V
    .param p2    # Lcom/intsig/callback/Callback0;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract OO8oO0o〇()Lcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;
.end method

.method public abstract OOO(Landroid/net/Uri;)V
.end method

.method public abstract OOO8o〇〇()V
.end method

.method public abstract OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract OOo0O(ILandroid/content/Intent;)V
.end method

.method public abstract OOo8o〇O(Lcom/intsig/camscanner/capture/CaptureMode;)V
.end method

.method public abstract OOoo()Lcom/intsig/camscanner/capture/SupportCaptureModeOption;
.end method

.method public abstract OO〇0008O8(Lcom/intsig/camscanner/capture/core/MoreSettingLayoutStatusListener;)V
.end method

.method public abstract Oo(Lcom/intsig/camscanner/capture/core/MoreSettingLayoutStatusListener;)V
.end method

.method public abstract Oo08()Z
.end method

.method public abstract Oo08OO8oO(Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;)V
.end method

.method public abstract Oo0oO〇O〇O(I)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
.end method

.method public abstract Oo8Oo00oo(Ljava/lang/String;)V
.end method

.method public abstract OoO8()V
.end method

.method public abstract OoO〇()Lcom/intsig/camscanner/capture/inputdata/CaptureSceneInputData;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract Ooo()I
.end method

.method public abstract Ooo8〇〇()Ljava/lang/String;
.end method

.method public abstract Oo〇O()Z
.end method

.method public abstract Oo〇O8o〇8([BII)V
    .param p1    # [B
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract Oo〇o()Ljava/lang/String;
.end method

.method public abstract O〇0(Ljava/lang/String;)V
.end method

.method public abstract O〇08()V
.end method

.method public abstract O〇0〇o808〇(Z)V
.end method

.method public abstract O〇8O8〇008()Landroid/view/View;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract O〇OO()Lcom/intsig/camscanner/capture/scene/CaptureSceneData;
.end method

.method public abstract O〇Oo()Z
.end method

.method public abstract O〇Oooo〇〇(Z)V
.end method

.method public abstract getActivity()Landroidx/fragment/app/FragmentActivity;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract getRotation()I
.end method

.method public abstract o0()I
.end method

.method public abstract o0O0()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
.end method

.method public abstract o0O〇8o0O(Z)V
.end method

.method public abstract o8()Lcom/intsig/app/AlertDialog;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract o800o8O()I
.end method

.method public abstract o80ooO()F
.end method

.method public abstract o88〇OO08〇(Lcom/intsig/camscanner/capture/ppt/PPTScaleCallback;)V
.end method

.method public abstract o8O0()Ljava/lang/String;
.end method

.method public abstract o8O〇(Z)V
.end method

.method public abstract o8oO〇(Z)V
.end method

.method public abstract oO00OOO(Lcom/intsig/camscanner/capture/CaptureMode;)V
    .param p1    # Lcom/intsig/camscanner/capture/CaptureMode;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract oOo(Z)V
.end method

.method public abstract oO〇()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/camscanner/CameraHardwareException;
        }
    .end annotation
.end method

.method public abstract oo()V
.end method

.method public abstract ooOO()Lcom/intsig/camscanner/capture/CaptureModeMenuManager;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract ooO〇00O()V
.end method

.method public abstract ooo0〇O88O()Z
.end method

.method public abstract ooo〇8oO()V
.end method

.method public abstract ooo〇〇O〇()Z
.end method

.method public abstract o〇8()Ljava/lang/String;
.end method

.method public abstract o〇8〇()Ljava/lang/String;
.end method

.method public abstract o〇O8〇〇o(Z)V
.end method

.method public abstract o〇o()J
.end method

.method public abstract o〇〇0〇()V
.end method

.method public abstract 〇0(I)V
.end method

.method public abstract 〇0000OOO()Z
.end method

.method public abstract 〇000O0()Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract 〇000〇〇08(ILandroid/content/Intent;)V
.end method

.method public abstract 〇00O0O0(Z)Z
.end method

.method public abstract 〇00〇8()V
.end method

.method public abstract 〇080()Landroid/net/Uri;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract 〇08O8o〇0(Z)V
.end method

.method public abstract 〇0OO8()Z
.end method

.method public abstract 〇0O〇Oo()Z
.end method

.method public abstract 〇0〇O0088o(Z)V
.end method

.method public abstract 〇8(Lcom/intsig/camscanner/capture/CaptureMode;)V
.end method

.method public abstract 〇80(Ljava/lang/String;)V
.end method

.method public abstract 〇8o()V
.end method

.method public abstract 〇8〇0〇o〇O(Z)V
.end method

.method public abstract 〇O(ZLcom/intsig/camscanner/capture/CaptureMode;)V
    .param p2    # Lcom/intsig/camscanner/capture/CaptureMode;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract 〇O888o0o()V
.end method

.method public abstract 〇O8o08O()J
.end method

.method public abstract 〇Oo〇o8()V
.end method

.method public abstract 〇O〇()Landroid/view/View;
.end method

.method public abstract 〇O〇80o08O(Ljava/lang/String;)V
.end method

.method public abstract 〇o(Landroid/content/Intent;)V
    .param p1    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract 〇o00〇〇Oo()Landroid/view/View;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract 〇o0O0O8(I)V
.end method

.method public abstract 〇o8OO0()Z
.end method

.method public abstract 〇oOO8O8(Z)V
.end method

.method public abstract 〇o〇(J)V
.end method

.method public abstract 〇o〇8()V
.end method

.method public abstract 〇o〇Oo0()Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract 〇〇00OO()I
.end method

.method public abstract 〇〇0o()Lcom/intsig/view/RotateImageTextButton;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract 〇〇0o8O〇〇()Ljava/lang/String;
.end method

.method public abstract 〇〇808〇(Z)V
.end method

.method public abstract 〇〇8O0〇8()Lcom/intsig/view/RotateLayout;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract 〇〇o8()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end method

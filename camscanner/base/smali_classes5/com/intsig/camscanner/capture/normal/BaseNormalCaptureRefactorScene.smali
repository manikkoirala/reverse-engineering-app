.class public abstract Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;
.super Lcom/intsig/camscanner/capture/core/BaseCaptureScene;
.source "BaseNormalCaptureRefactorScene.kt"

# interfaces
.implements Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;
.implements Lcom/intsig/camscanner/capture/core/MoreSettingLayoutStatusListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Ooo8o:Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0〇0:Z

.field private O8o〇O0:Lcom/intsig/camscanner/view/CircleProgressBar;

.field private O8〇o〇88:Z

.field private OO〇OOo:Landroid/widget/TextView;

.field private Oo0O0o8:Lcom/intsig/camscanner/view/BorderView;

.field private O〇O:Z

.field private final o00〇88〇08:Lcom/intsig/camscanner/capture/contract/PreviewContract$BorderView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o880:Lcom/intsig/camscanner/capture/contract/PreviewContract$View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/camscanner/control/ProgressAnimHandler<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOO0880O:Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOO8:Z

.field private oOoO8OO〇:Landroid/view/View;

.field private oOoo80oO:Z

.field private oOo〇08〇:Lcom/intsig/camscanner/multiimageedit/viewModel/CaptureMergeGuideViewModel;

.field private final oO〇oo:Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oooO888:Z

.field private o〇0〇o:Z

.field private o〇O8OO:Landroid/widget/ImageView;

.field private final 〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0ooOOo:Z

.field private 〇0〇0:Landroid/view/View;

.field private 〇800OO〇0O:Lcom/intsig/view/RotateLayout;

.field private 〇80O8o8O〇:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

.field private 〇8〇80o:Landroid/widget/TextView;

.field private volatile 〇8〇OOoooo:Z

.field private 〇8〇o88:Z

.field private final 〇O8oOo0:Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇oo〇O〇80:Z

.field private 〇o〇88〇8:Landroid/view/View;

.field private final 〇〇o0〇8:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->Ooo8o:Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;)V
    .locals 1
    .param p1    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/capture/CaptureMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/capture/control/ICaptureControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "captureMode"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "captureControl"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "iCaptureViewGroup"

    .line 17
    .line 18
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const-string v0, "cameraClient"

    .line 22
    .line 23
    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    const-string v0, "captureSceneFactory"

    .line 27
    .line 28
    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-direct/range {p0 .. p5}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 32
    .line 33
    .line 34
    iput-object p6, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO0880O:Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;

    .line 35
    .line 36
    const/4 p2, 0x1

    .line 37
    iput-boolean p2, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoo80oO:Z

    .line 38
    .line 39
    iput-boolean p2, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 40
    .line 41
    iput-boolean p2, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O〇O:Z

    .line 42
    .line 43
    new-instance p4, Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 44
    .line 45
    invoke-direct {p4, p1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;-><init>(Ljava/lang/Object;)V

    .line 46
    .line 47
    .line 48
    iput-object p4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 49
    .line 50
    sget-object p4, Lcom/intsig/camscanner/capture/qrcode/util/QRBarUtil;->〇080:Lcom/intsig/camscanner/capture/qrcode/util/QRBarUtil;

    .line 51
    .line 52
    new-instance p5, Lo8o〇〇0O/〇080;

    .line 53
    .line 54
    invoke-direct {p5, p0}, Lo8o〇〇0O/〇080;-><init>(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p4, p5}, Lcom/intsig/camscanner/capture/qrcode/util/QRBarUtil;->o〇0(Lcom/intsig/callback/Callback;)Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;

    .line 58
    .line 59
    .line 60
    move-result-object p4

    .line 61
    iput-object p4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇O8oOo0:Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;

    .line 62
    .line 63
    new-instance p4, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler;

    .line 64
    .line 65
    new-instance p5, Lo8o〇〇0O/〇o00〇〇Oo;

    .line 66
    .line 67
    invoke-direct {p5, p0}, Lo8o〇〇0O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 68
    .line 69
    .line 70
    invoke-direct {p4, p1, p3, p5}, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/callback/Callback0;)V

    .line 71
    .line 72
    .line 73
    iput-object p4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oO〇oo:Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler;

    .line 74
    .line 75
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 76
    .line 77
    .line 78
    move-result-object p4

    .line 79
    invoke-virtual {p4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object p4

    .line 83
    const-string p5, "com.intsig.camscanner.PARE_RETAKE"

    .line 84
    .line 85
    invoke-static {p5, p4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 86
    .line 87
    .line 88
    move-result p4

    .line 89
    iput-boolean p4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O8〇o〇88:Z

    .line 90
    .line 91
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇88()Z

    .line 92
    .line 93
    .line 94
    move-result p4

    .line 95
    iput-boolean p4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇8〇o88:Z

    .line 96
    .line 97
    iget-object p4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 98
    .line 99
    invoke-virtual {p4, p2}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇0000OOO(I)V

    .line 100
    .line 101
    .line 102
    iget-object p4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 103
    .line 104
    const-wide/16 p5, 0x8

    .line 105
    .line 106
    invoke-virtual {p4, p5, p6}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->O8ooOoo〇(J)V

    .line 107
    .line 108
    .line 109
    iget-object p4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 110
    .line 111
    new-instance p5, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$1;

    .line 112
    .line 113
    invoke-direct {p5, p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$1;-><init>(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p4, p5}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇oOO8O8(Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;)V

    .line 117
    .line 118
    .line 119
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 120
    .line 121
    .line 122
    move-result-object p4

    .line 123
    const/4 p5, 0x0

    .line 124
    if-eqz p4, :cond_0

    .line 125
    .line 126
    const-string p6, "EXTRA_IGNORE_START_DEMO"

    .line 127
    .line 128
    invoke-virtual {p4, p6, p5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 129
    .line 130
    .line 131
    move-result p5

    .line 132
    :cond_0
    iput-boolean p5, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oooO888:Z

    .line 133
    .line 134
    new-instance p4, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$previewContractView$1;

    .line 135
    .line 136
    invoke-direct {p4, p0, p1, p3}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$previewContractView$1;-><init>(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;)V

    .line 137
    .line 138
    .line 139
    iput-object p4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o880:Lcom/intsig/camscanner/capture/contract/PreviewContract$View;

    .line 140
    .line 141
    new-instance p1, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$previewContractBorderView$1;

    .line 142
    .line 143
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$previewContractBorderView$1;-><init>(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 144
    .line 145
    .line 146
    iput-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o00〇88〇08:Lcom/intsig/camscanner/capture/contract/PreviewContract$BorderView;

    .line 147
    .line 148
    iput-boolean p2, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0〇0:Z

    .line 149
    .line 150
    new-instance p3, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 151
    .line 152
    invoke-direct {p3, p4, p1}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;-><init>(Lcom/intsig/camscanner/capture/contract/PreviewContract$View;Lcom/intsig/camscanner/capture/contract/PreviewContract$BorderView;)V

    .line 153
    .line 154
    .line 155
    iput-object p3, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 156
    .line 157
    iput-boolean p2, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇0ooOOo:Z

    .line 158
    .line 159
    return-void
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
.end method

.method private final O0oO0(Landroid/content/Intent;)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoo80oO:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v0, "extra_show_import_file"

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoo80oO:Z

    .line 13
    .line 14
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇oo〇O〇80:Z

    .line 15
    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    const-string v0, "EXTRA_IS_CAPTURE_ONE_PICTURE"

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇oo〇O〇80:Z

    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final O0〇oO〇o(Landroid/content/Intent;)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    move-object v1, v0

    .line 10
    :goto_0
    if-eqz v1, :cond_1

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-interface {p1, v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OOO(Landroid/net/Uri;)V

    .line 17
    .line 18
    .line 19
    goto :goto_3

    .line 20
    :cond_1
    invoke-static {p1}, Lcom/intsig/camscanner/app/IntentUtil;->〇O8o08O(Landroid/content/Intent;)Ljava/util/ArrayList;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const-string v1, "BaseNormalCaptureRefactorScene"

    .line 25
    .line 26
    if-eqz p1, :cond_3

    .line 27
    .line 28
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-lez v2, :cond_3

    .line 33
    .line 34
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oo08OO〇0(Ljava/util/ArrayList;)V

    .line 35
    .line 36
    .line 37
    :try_start_0
    invoke-static {}, Lcom/intsig/logagent/LogAgent;->json()Lcom/intsig/logagent/JsonBuilder;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-virtual {v2}, Lcom/intsig/logagent/JsonBuilder;->get()Lorg/json/JSONObject;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    const-string v3, "from"

    .line 46
    .line 47
    iget-boolean v4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 48
    .line 49
    if-eqz v4, :cond_2

    .line 50
    .line 51
    const-string v4, "single"

    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_2
    const-string v4, "batch"

    .line 55
    .line 56
    :goto_1
    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    const-string v3, "else"

    .line 61
    .line 62
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    invoke-virtual {v2, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 71
    .line 72
    .line 73
    move-result-object v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    goto :goto_2

    .line 75
    :catch_0
    move-exception p1

    .line 76
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 77
    .line 78
    .line 79
    :goto_2
    const-string p1, "CSScan"

    .line 80
    .line 81
    const-string v1, "import_gallery"

    .line 82
    .line 83
    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 84
    .line 85
    .line 86
    goto :goto_3

    .line 87
    :cond_3
    const-string p1, "uris are null"

    .line 88
    .line 89
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    :goto_3
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static synthetic O8OO08o(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇Oo(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic O8O〇8oo08(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇00O0O0()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final O8o08O8O(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;)V
    .locals 7

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oO〇oo:Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler;

    .line 9
    .line 10
    iget-object v3, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 11
    .line 12
    iget-object v4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o〇O8OO:Landroid/widget/ImageView;

    .line 13
    .line 14
    iget-object v5, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇8〇80o:Landroid/widget/TextView;

    .line 15
    .line 16
    iget-object v6, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoO8OO〇:Landroid/view/View;

    .line 17
    .line 18
    move-object v2, p1

    .line 19
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler;->〇0〇O0088o(Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;Landroid/view/View;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    sget-object p0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 p0, 0x0

    .line 26
    :goto_0
    if-nez p0, :cond_1

    .line 27
    .line 28
    const-string p0, "BaseNormalCaptureRefactorScene"

    .line 29
    .line 30
    const-string p1, "QRBarZxingScanHandler result get NULL"

    .line 31
    .line 32
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final OO(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 17
    .line 18
    .line 19
    move-result-wide v1

    .line 20
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->O〇8O8〇008(Landroid/content/Context;J)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->o〇8〇()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    if-nez v1, :cond_0

    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 39
    .line 40
    .line 41
    move-result-object p0

    .line 42
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O〇0(Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    :cond_0
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static synthetic OO8〇(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->OO(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final OOo()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    const v1, 0x7f0a0d03

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    iput-object v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 15
    .line 16
    const v1, 0x7f0a186b

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    check-cast v1, Landroid/widget/TextView;

    .line 24
    .line 25
    iput-object v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇8〇80o:Landroid/widget/TextView;

    .line 26
    .line 27
    const v1, 0x7f0a0a64

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    check-cast v1, Landroid/widget/ImageView;

    .line 35
    .line 36
    iput-object v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o〇O8OO:Landroid/widget/ImageView;

    .line 37
    .line 38
    const v1, 0x7f0a0a63

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    iput-object v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇0〇0:Landroid/view/View;

    .line 46
    .line 47
    const v1, 0x7f0a09f5

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    iput-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoO8OO〇:Landroid/view/View;

    .line 55
    .line 56
    invoke-static {}, Lcom/intsig/camscanner/experiment/QRCodeOptExp;->〇o〇()Z

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    if-eqz v0, :cond_1

    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇0〇0:Landroid/view/View;

    .line 63
    .line 64
    const/4 v1, 0x0

    .line 65
    if-eqz v0, :cond_0

    .line 66
    .line 67
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 68
    .line 69
    .line 70
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇8〇80o:Landroid/widget/TextView;

    .line 71
    .line 72
    if-eqz v0, :cond_1

    .line 73
    .line 74
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 75
    .line 76
    invoke-virtual {v2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    const/16 v3, 0x10

    .line 81
    .line 82
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 83
    .line 84
    .line 85
    move-result v2

    .line 86
    invoke-virtual {v0, v1, v1, v2, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 87
    .line 88
    .line 89
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇0〇0:Landroid/view/View;

    .line 90
    .line 91
    if-eqz v0, :cond_2

    .line 92
    .line 93
    new-instance v1, Lo8o〇〇0O/〇o〇;

    .line 94
    .line 95
    invoke-direct {v1, p0}, Lo8o〇〇0O/〇o〇;-><init>(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    .line 100
    .line 101
    :cond_2
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static final synthetic OO〇(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoO8OO〇:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic o8〇(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final oO80OOO〇()I
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇8〇o88:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O888o0o()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    return v0

    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOO()Lcom/intsig/camscanner/capture/CaptureMode;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_WORKBENCH:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 18
    .line 19
    if-ne v0, v1, :cond_1

    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isOpenCaptureWorkbenchForThree()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_1

    .line 30
    .line 31
    const/4 v0, 0x2

    .line 32
    return v0

    .line 33
    :cond_1
    const/4 v0, 0x3

    .line 34
    return v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final oo08OO〇0(Ljava/util/ArrayList;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "+",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const-string v1, "tag_id"

    .line 10
    .line 11
    const-wide/16 v2, -0x1

    .line 12
    .line 13
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 14
    .line 15
    .line 16
    move-result-wide v8

    .line 17
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->o0O0()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_TEMPLATE_DIR_PRESET:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 26
    .line 27
    if-ne v0, v1, :cond_0

    .line 28
    .line 29
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OO0o〇〇()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    const/4 v3, 0x1

    .line 44
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v4

    .line 52
    const/4 v5, 0x0

    .line 53
    const/4 v6, 0x1

    .line 54
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/util/Util;->ooo〇8oO(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    goto :goto_0

    .line 59
    :cond_0
    const/4 v0, 0x0

    .line 60
    :goto_0
    move-object v14, v0

    .line 61
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 62
    .line 63
    .line 64
    move-result-object v4

    .line 65
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 70
    .line 71
    .line 72
    move-result-wide v6

    .line 73
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v10

    .line 81
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Ooo8〇〇()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v11

    .line 89
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇0000OOO()Z

    .line 94
    .line 95
    .line 96
    move-result v12

    .line 97
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o08o〇0()Z

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    const/4 v1, 0x0

    .line 102
    const/4 v2, 0x1

    .line 103
    if-eqz v0, :cond_1

    .line 104
    .line 105
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 110
    .line 111
    .line 112
    move-result-wide v15

    .line 113
    const-wide/16 v17, 0x0

    .line 114
    .line 115
    cmp-long v0, v15, v17

    .line 116
    .line 117
    if-gtz v0, :cond_1

    .line 118
    .line 119
    const/4 v13, 0x1

    .line 120
    goto :goto_1

    .line 121
    :cond_1
    const/4 v13, 0x0

    .line 122
    :goto_1
    const/4 v15, -0x1

    .line 123
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O〇OO()Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 128
    .line 129
    .line 130
    move-result-object v16

    .line 131
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo〇o()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v17

    .line 139
    move-object/from16 v5, p1

    .line 140
    .line 141
    invoke-static/range {v4 .. v17}, Lcom/intsig/camscanner/BatchModeActivity;->〇o〇88(Landroid/content/Context;Ljava/util/ArrayList;JJLjava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ILcom/intsig/camscanner/capture/scene/CaptureSceneData;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    const/4 v3, 0x3

    .line 146
    new-array v3, v3, [Lcom/intsig/camscanner/capture/CaptureMode;

    .line 147
    .line 148
    sget-object v4, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_SINGLE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 149
    .line 150
    aput-object v4, v3, v1

    .line 151
    .line 152
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_MULTI:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 153
    .line 154
    aput-object v1, v3, v2

    .line 155
    .line 156
    const/4 v1, 0x2

    .line 157
    sget-object v4, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_WORKBENCH:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 158
    .line 159
    aput-object v4, v3, v1

    .line 160
    .line 161
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOO()Lcom/intsig/camscanner/capture/CaptureMode;

    .line 162
    .line 163
    .line 164
    move-result-object v1

    .line 165
    invoke-static {v3, v1}, Lkotlin/collections/ArraysKt;->〇0〇O0088o([Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 166
    .line 167
    .line 168
    move-result v1

    .line 169
    if-eqz v1, :cond_2

    .line 170
    .line 171
    const-string v1, "BatchModeActivity.show.operation.page"

    .line 172
    .line 173
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 174
    .line 175
    .line 176
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 177
    .line 178
    .line 179
    move-result-object v1

    .line 180
    const/16 v2, 0x86

    .line 181
    .line 182
    invoke-virtual {v1, v0, v2}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 183
    .line 184
    .line 185
    return-void
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method private final ooo0〇〇O(I)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Recycle",
            "ObjectAnimatorBinding"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "startCloseAnimation width is "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "BaseNormalCaptureRefactorScene"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 v0, 0x2

    .line 24
    new-array v1, v0, [I

    .line 25
    .line 26
    const/4 v2, 0x0

    .line 27
    aput p1, v1, v2

    .line 28
    .line 29
    sget-object p1, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler;->〇8o8o〇:Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler$Companion;

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler$Companion;->〇o00〇〇Oo()I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    mul-int/lit8 v3, v3, 0x2

    .line 36
    .line 37
    const/4 v4, 0x1

    .line 38
    aput v3, v1, v4

    .line 39
    .line 40
    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    new-instance v3, Lo8o〇〇0O/o〇0;

    .line 45
    .line 46
    invoke-direct {v3, p0}, Lo8o〇〇0O/o〇0;-><init>(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v1, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 50
    .line 51
    .line 52
    const-wide/16 v5, 0x190

    .line 53
    .line 54
    invoke-virtual {v1, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 55
    .line 56
    .line 57
    iget-object v3, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoO8OO〇:Landroid/view/View;

    .line 58
    .line 59
    if-eqz v3, :cond_0

    .line 60
    .line 61
    invoke-virtual {v3}, Landroid/view/View;->getX()F

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 66
    .line 67
    .line 68
    move-result-object v7

    .line 69
    invoke-static {v7}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 70
    .line 71
    .line 72
    move-result v7

    .line 73
    div-int/2addr v7, v0

    .line 74
    int-to-float v7, v7

    .line 75
    sub-float/2addr v3, v7

    .line 76
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler$Companion;->〇o00〇〇Oo()I

    .line 77
    .line 78
    .line 79
    move-result p1

    .line 80
    int-to-float p1, p1

    .line 81
    add-float/2addr v3, p1

    .line 82
    goto :goto_0

    .line 83
    :cond_0
    const/4 v3, 0x0

    .line 84
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 85
    .line 86
    new-array v7, v4, [F

    .line 87
    .line 88
    aput v3, v7, v2

    .line 89
    .line 90
    const-string v3, "translationX"

    .line 91
    .line 92
    invoke-static {p1, v3, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    invoke-virtual {p1, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 97
    .line 98
    .line 99
    new-instance v3, Landroid/animation/AnimatorSet;

    .line 100
    .line 101
    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 102
    .line 103
    .line 104
    new-array v0, v0, [Landroid/animation/Animator;

    .line 105
    .line 106
    aput-object v1, v0, v2

    .line 107
    .line 108
    aput-object p1, v0, v4

    .line 109
    .line 110
    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 111
    .line 112
    .line 113
    new-instance p1, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$startCloseAnimation$$inlined$doOnEnd$1;

    .line 114
    .line 115
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene$startCloseAnimation$$inlined$doOnEnd$1;-><init>(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {v3, p1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 119
    .line 120
    .line 121
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 122
    .line 123
    .line 124
    return-void
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public static final synthetic o〇(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;Lcom/intsig/camscanner/view/BorderView;[IIIZZ)V
    .locals 0

    .line 1
    invoke-virtual/range {p0 .. p6}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇008〇o0〇〇(Lcom/intsig/camscanner/view/BorderView;[IIIZZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method private static final o〇00O(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-wide/16 v0, 0x0

    .line 7
    .line 8
    invoke-direct {p0, v0, v1, v0, v1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇080OO8〇0(JJ)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇080OO8〇0(JJ)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇0ooOOo:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇O8oOo0:Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;

    .line 6
    .line 7
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/intsig/camscanner/capture/contract/PreviewContract$Presenter;->〇080(JJ)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇08〇0〇o〇8(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)Lcom/intsig/camscanner/control/ProgressAnimHandler;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final 〇8〇oO〇〇8o(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8O〇()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_2

    .line 11
    .line 12
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇8〇OOoooo:Z

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x1

    .line 18
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇8〇OOoooo:Z

    .line 19
    .line 20
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o〇0〇o:Z

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O〇Oooo〇〇()V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O00O()V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    invoke-interface {p0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 35
    .line 36
    .line 37
    move-result-object p0

    .line 38
    if-eqz p0, :cond_2

    .line 39
    .line 40
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->OO0o〇〇〇〇0()V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    if-eqz v0, :cond_2

    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->Oo8()Z

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oO80OOO〇()I

    .line 59
    .line 60
    .line 61
    move-result p0

    .line 62
    invoke-virtual {v0, v1, p0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇〇808〇(ZI)V

    .line 63
    .line 64
    .line 65
    :cond_2
    :goto_0
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static final synthetic 〇8〇o〇8(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)Landroid/content/Context;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇O8〇OO〇(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o〇00O(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final 〇Oo(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "BaseNormalCaptureRefactorScene"

    .line 7
    .line 8
    const-string v0, "click barcodeCloseView"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-string p1, "type"

    .line 14
    .line 15
    const-string v0, "single"

    .line 16
    .line 17
    const-string v1, "CSScan"

    .line 18
    .line 19
    const-string v2, "scan_code_bubble_close"

    .line 20
    .line 21
    invoke-static {v1, v2, p1, v0}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/camscanner/experiment/QRCodeOptExp;->〇080()Z

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    const/4 v0, 0x0

    .line 29
    if-eqz p1, :cond_0

    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oO〇oo:Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarZxingResultHandler;->〇O〇()I

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->ooo0〇〇O(I)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 42
    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 46
    .line 47
    .line 48
    :cond_1
    :goto_0
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇0ooOOo:Z

    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇o8oO(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇8〇oO〇〇8o(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇oO8O0〇〇O(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇〇08O(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;Landroid/animation/ValueAnimator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oOoo〇()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OoO〇()Lcom/intsig/camscanner/capture/inputdata/CaptureSceneInputData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/inputdata/CaptureSceneInputData;->〇080()Lcom/intsig/camscanner/capture/inputdata/NormalCaptureInputData;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/inputdata/NormalCaptureInputData;->〇080()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v1, 0x1

    .line 23
    :goto_0
    iput-boolean v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoo80oO:Z

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/inputdata/CaptureSceneInputData;->〇080()Lcom/intsig/camscanner/capture/inputdata/NormalCaptureInputData;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    if-eqz v0, :cond_1

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/inputdata/NormalCaptureInputData;->O8()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    goto :goto_1

    .line 36
    :cond_1
    const/4 v0, 0x0

    .line 37
    :goto_1
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇oo〇O〇80:Z

    .line 38
    .line 39
    :cond_2
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private static final 〇〇08O(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;Landroid/animation/ValueAnimator;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "animation"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/util/ViewExtKt;->〇00(Landroid/view/View;)V

    .line 16
    .line 17
    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    goto :goto_0

    .line 28
    :cond_1
    move-object v0, v1

    .line 29
    :goto_0
    instance-of v2, v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 30
    .line 31
    if-eqz v2, :cond_2

    .line 32
    .line 33
    move-object v1, v0

    .line 34
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 35
    .line 36
    :cond_2
    if-nez v1, :cond_3

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_3
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    const-string v0, "null cannot be cast to non-null type kotlin.Int"

    .line 44
    .line 45
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    check-cast p1, Ljava/lang/Integer;

    .line 49
    .line 50
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    iput p1, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 55
    .line 56
    :goto_1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 57
    .line 58
    if-eqz p0, :cond_4

    .line 59
    .line 60
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 61
    .line 62
    .line 63
    :cond_4
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇〇0O8ooO()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/pdf/DocOpenUtil;->〇080(Landroid/content/Context;)Landroid/content/Intent;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :try_start_0
    const-string v1, "CSScan"

    .line 10
    .line 11
    const-string v2, "import_document"

    .line 12
    .line 13
    invoke-static {v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    const/16 v2, 0xdb

    .line 21
    .line 22
    invoke-virtual {v1, v0, v2}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :catch_0
    move-exception v0

    .line 27
    const-string v1, "BaseNormalCaptureRefactorScene"

    .line 28
    .line 29
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 30
    .line 31
    .line 32
    :goto_0
    return-void
    .line 33
.end method

.method private final 〇〇0o〇o8(Ljava/lang/String;)V
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇888(Landroid/app/Activity;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_2

    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇oo〇O〇80:Z

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    const/4 v2, 0x1

    .line 20
    const/4 v3, 0x1

    .line 21
    const/16 v4, 0x85

    .line 22
    .line 23
    const/4 v5, -0x1

    .line 24
    const-string v6, "retake"

    .line 25
    .line 26
    const/4 v7, 0x0

    .line 27
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/app/IntentUtil;->o〇〇0〇(Landroid/app/Activity;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-nez v0, :cond_1

    .line 36
    .line 37
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O8〇o〇88:Z

    .line 38
    .line 39
    if-eqz v0, :cond_1

    .line 40
    .line 41
    const-string p1, "retake"

    .line 42
    .line 43
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O8〇o〇88:Z

    .line 48
    .line 49
    xor-int/lit8 v1, v1, 0x1

    .line 50
    .line 51
    const/16 v2, 0x85

    .line 52
    .line 53
    invoke-static {v0, v2, v1, p1}, Lcom/intsig/camscanner/app/IntentUtil;->o〇O8〇〇o(Landroid/app/Activity;IZLjava/lang/String;)V

    .line 54
    .line 55
    .line 56
    :cond_2
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static synthetic 〇〇8(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;IIZZZILjava/lang/Object;)V
    .locals 6

    .line 1
    if-nez p7, :cond_4

    .line 2
    .line 3
    and-int/lit8 p7, p6, 0x2

    .line 4
    .line 5
    if-eqz p7, :cond_0

    .line 6
    .line 7
    const/4 p2, -0x1

    .line 8
    const/4 v2, -0x1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    move v2, p2

    .line 11
    :goto_0
    and-int/lit8 p2, p6, 0x4

    .line 12
    .line 13
    const/4 p7, 0x0

    .line 14
    if-eqz p2, :cond_1

    .line 15
    .line 16
    const/4 v3, 0x0

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    move v3, p3

    .line 19
    :goto_1
    and-int/lit8 p2, p6, 0x8

    .line 20
    .line 21
    if-eqz p2, :cond_2

    .line 22
    .line 23
    const/4 v4, 0x0

    .line 24
    goto :goto_2

    .line 25
    :cond_2
    move v4, p4

    .line 26
    :goto_2
    and-int/lit8 p2, p6, 0x10

    .line 27
    .line 28
    if-eqz p2, :cond_3

    .line 29
    .line 30
    const/4 p5, 0x1

    .line 31
    const/4 v5, 0x1

    .line 32
    goto :goto_3

    .line 33
    :cond_3
    move v5, p5

    .line 34
    :goto_3
    move-object v0, p0

    .line 35
    move v1, p1

    .line 36
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oO0〇〇o8〇(IIZZZ)V

    .line 37
    .line 38
    .line 39
    return-void

    .line 40
    :cond_4
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    .line 41
    .line 42
    const-string p1, "Super calls with default arguments not supported in this target, function: go2MultiCaptureFilterPreview"

    .line 43
    .line 44
    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    throw p0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
.end method

.method public static final synthetic 〇〇O00〇8(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8O〇()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇〇o0o(Landroid/content/Intent;)Z
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    const-string v2, "extra_take_next_page"

    .line 6
    .line 7
    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    if-ne v2, v0, :cond_0

    .line 12
    .line 13
    const/4 v2, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v2, 0x0

    .line 16
    :goto_0
    if-eqz v2, :cond_2

    .line 17
    .line 18
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 19
    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oO()V

    .line 23
    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO0880O:Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;

    .line 26
    .line 27
    sget-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_MULTI:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 28
    .line 29
    invoke-virtual {v1, v2, p1}, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/capture/CaptureMode;Landroid/content/Intent;)V

    .line 30
    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0O(Landroid/content/Intent;)V

    .line 34
    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_2
    const/4 v0, 0x0

    .line 38
    :goto_1
    return v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static synthetic 〇〇〇0880(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O8o08O8O(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method protected final O08O0〇O()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->OO〇OOo:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected O0O(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public O0O〇OOo()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇〇o0〇8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O0o8〇O()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇oo〇()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-nez v2, :cond_0

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇O〇()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    :cond_0
    return v1

    .line 25
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O888o0o()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇8〇o88:Z

    .line 32
    .line 33
    if-eqz v0, :cond_2

    .line 34
    .line 35
    const/4 v1, 0x1

    .line 36
    :cond_2
    return v1
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method protected final O0o〇()Lcom/intsig/camscanner/multiimageedit/viewModel/CaptureMergeGuideViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOo〇08〇:Lcom/intsig/camscanner/multiimageedit/viewModel/CaptureMergeGuideViewModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected O80〇O〇080(IZ)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O80〇O〇080(IZ)V

    .line 2
    .line 3
    .line 4
    iget-object p2, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇800OO〇0O:Lcom/intsig/view/RotateLayout;

    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-virtual {p2, p1}, Lcom/intsig/view/RotateLayout;->setOrientation(I)V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O88o〇(Z)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O88o〇(Z)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 7
    .line 8
    if-eqz p1, :cond_1

    .line 9
    .line 10
    invoke-static {p1}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O〇(Landroid/view/View;)V

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const-wide/16 v0, 0x0

    .line 15
    .line 16
    invoke-direct {p0, v0, v1, v0, v1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇080OO8〇0(JJ)V

    .line 17
    .line 18
    .line 19
    :cond_1
    :goto_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected final OO88o()Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇80O8o8O〇:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected final OO88〇OOO()Lcom/intsig/camscanner/view/CircleProgressBar;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O8o〇O0:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected OOO〇O0(Landroid/view/View;)V
    .locals 6

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOO〇O0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    const-string v1, "BaseNormalCaptureRefactorScene"

    .line 17
    .line 18
    if-nez v0, :cond_1

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    const v3, 0x7f0a0e0d

    .line 26
    .line 27
    .line 28
    if-ne v2, v3, :cond_3

    .line 29
    .line 30
    const-string v0, "shutter"

    .line 31
    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    const/4 v1, 0x0

    .line 44
    if-eqz v0, :cond_2

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇O〇()Z

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    const/4 v2, 0x1

    .line 51
    if-ne v0, v2, :cond_2

    .line 52
    .line 53
    const/4 v1, 0x1

    .line 54
    :cond_2
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇o(Z)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o0oO()V

    .line 58
    .line 59
    .line 60
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 61
    .line 62
    const/16 v1, 0x1b

    .line 63
    .line 64
    if-lt v0, v1, :cond_a

    .line 65
    .line 66
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo〇o()Z

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    if-eqz v0, :cond_a

    .line 71
    .line 72
    const/16 v0, 0x8

    .line 73
    .line 74
    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 75
    .line 76
    .line 77
    goto :goto_6

    .line 78
    :cond_3
    :goto_1
    const-string p1, "cs_scan"

    .line 79
    .line 80
    const-string v2, "single"

    .line 81
    .line 82
    const-string v3, "batch"

    .line 83
    .line 84
    if-nez v0, :cond_4

    .line 85
    .line 86
    goto :goto_3

    .line 87
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 88
    .line 89
    .line 90
    move-result v4

    .line 91
    const v5, 0x7f0a0e09

    .line 92
    .line 93
    .line 94
    if-ne v4, v5, :cond_6

    .line 95
    .line 96
    const-string v0, "import_doc_file"

    .line 97
    .line 98
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 102
    .line 103
    if-eqz v0, :cond_5

    .line 104
    .line 105
    goto :goto_2

    .line 106
    :cond_5
    move-object v2, v3

    .line 107
    :goto_2
    invoke-virtual {p0, v2, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->ooO〇00O(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇〇0O8ooO()V

    .line 111
    .line 112
    .line 113
    goto :goto_6

    .line 114
    :cond_6
    :goto_3
    if-nez v0, :cond_7

    .line 115
    .line 116
    goto :goto_6

    .line 117
    :cond_7
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 118
    .line 119
    .line 120
    move-result v0

    .line 121
    const v4, 0x7f0a0e08

    .line 122
    .line 123
    .line 124
    if-ne v0, v4, :cond_a

    .line 125
    .line 126
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 127
    .line 128
    new-instance v4, Ljava/lang/StringBuilder;

    .line 129
    .line 130
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .line 132
    .line 133
    const-string v5, "import_picture, mIsSingleMode="

    .line 134
    .line 135
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    .line 147
    .line 148
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 149
    .line 150
    if-eqz v0, :cond_8

    .line 151
    .line 152
    move-object v0, v2

    .line 153
    goto :goto_4

    .line 154
    :cond_8
    move-object v0, v3

    .line 155
    :goto_4
    invoke-virtual {p0, v0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    .line 157
    .line 158
    iget-boolean p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 159
    .line 160
    if-eqz p1, :cond_9

    .line 161
    .line 162
    goto :goto_5

    .line 163
    :cond_9
    move-object v2, v3

    .line 164
    :goto_5
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇〇0o〇o8(Ljava/lang/String;)V

    .line 165
    .line 166
    .line 167
    :cond_a
    :goto_6
    return-void
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method protected OOoo(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "intent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOoo(Landroid/content/Intent;)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0oO0(Landroid/content/Intent;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected OO〇00〇8oO(Z)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected final Oo0O080()Lcom/intsig/camscanner/capture/contract/PreviewContract$BorderView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o00〇88〇08:Lcom/intsig/camscanner/capture/contract/PreviewContract$BorderView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo8()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oooo8o0〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;->oO80()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected Oo〇O8o〇8()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->Oo0O0o8:Lcom/intsig/camscanner/view/BorderView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_1

    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8〇0〇o〇O()Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    const v2, 0x7f0a01e1

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    check-cast v0, Lcom/intsig/camscanner/view/BorderView;

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    move-object v0, v1

    .line 23
    :goto_0
    iput-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->Oo0O0o8:Lcom/intsig/camscanner/view/BorderView;

    .line 24
    .line 25
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 26
    .line 27
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->OO〇OOo:Landroid/widget/TextView;

    .line 28
    .line 29
    if-nez v0, :cond_3

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8〇0〇o〇O()Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_2

    .line 36
    .line 37
    const v2, 0x7f0a127a

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    check-cast v0, Landroid/widget/TextView;

    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_2
    move-object v0, v1

    .line 48
    :goto_1
    iput-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->OO〇OOo:Landroid/widget/TextView;

    .line 49
    .line 50
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 51
    .line 52
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇800OO〇0O:Lcom/intsig/view/RotateLayout;

    .line 53
    .line 54
    if-nez v0, :cond_6

    .line 55
    .line 56
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8〇0〇o〇O()Landroid/view/View;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    if-eqz v0, :cond_4

    .line 61
    .line 62
    const v2, 0x7f0a01b7

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    check-cast v0, Lcom/intsig/view/RotateLayout;

    .line 70
    .line 71
    goto :goto_2

    .line 72
    :cond_4
    move-object v0, v1

    .line 73
    :goto_2
    iput-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇800OO〇0O:Lcom/intsig/view/RotateLayout;

    .line 74
    .line 75
    if-nez v0, :cond_5

    .line 76
    .line 77
    goto :goto_3

    .line 78
    :cond_5
    const/4 v2, 0x4

    .line 79
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 80
    .line 81
    .line 82
    :goto_3
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 83
    .line 84
    :cond_6
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oo()Lcom/intsig/camscanner/view/RotateImageView;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    const/4 v2, 0x0

    .line 89
    const/4 v3, 0x1

    .line 90
    if-nez v0, :cond_8

    .line 91
    .line 92
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O()Landroid/view/View;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    if-eqz v0, :cond_7

    .line 97
    .line 98
    const v4, 0x7f0a0e0d

    .line 99
    .line 100
    .line 101
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    check-cast v0, Lcom/intsig/camscanner/view/RotateImageView;

    .line 106
    .line 107
    goto :goto_4

    .line 108
    :cond_7
    move-object v0, v1

    .line 109
    :goto_4
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8888(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 110
    .line 111
    .line 112
    new-array v0, v3, [Landroid/view/View;

    .line 113
    .line 114
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oo()Lcom/intsig/camscanner/view/RotateImageView;

    .line 115
    .line 116
    .line 117
    move-result-object v4

    .line 118
    aput-object v4, v0, v2

    .line 119
    .line 120
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8O0O808〇([Landroid/view/View;)V

    .line 121
    .line 122
    .line 123
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 124
    .line 125
    :cond_8
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo〇O()Lcom/intsig/view/RotateImageTextButton;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    if-nez v0, :cond_a

    .line 130
    .line 131
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O()Landroid/view/View;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    if-eqz v0, :cond_9

    .line 136
    .line 137
    const v4, 0x7f0a0e08

    .line 138
    .line 139
    .line 140
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    check-cast v0, Lcom/intsig/view/RotateImageTextButton;

    .line 145
    .line 146
    goto :goto_5

    .line 147
    :cond_9
    move-object v0, v1

    .line 148
    :goto_5
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇008〇oo(Lcom/intsig/view/RotateImageTextButton;)V

    .line 149
    .line 150
    .line 151
    new-array v0, v3, [Landroid/view/View;

    .line 152
    .line 153
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo〇O()Lcom/intsig/view/RotateImageTextButton;

    .line 154
    .line 155
    .line 156
    move-result-object v4

    .line 157
    aput-object v4, v0, v2

    .line 158
    .line 159
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8O0O808〇([Landroid/view/View;)V

    .line 160
    .line 161
    .line 162
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 163
    .line 164
    :cond_a
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇〇o8()Lcom/intsig/view/RotateImageTextButton;

    .line 165
    .line 166
    .line 167
    move-result-object v0

    .line 168
    if-nez v0, :cond_d

    .line 169
    .line 170
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O()Landroid/view/View;

    .line 171
    .line 172
    .line 173
    move-result-object v0

    .line 174
    if-eqz v0, :cond_b

    .line 175
    .line 176
    const v4, 0x7f0a0e09

    .line 177
    .line 178
    .line 179
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 180
    .line 181
    .line 182
    move-result-object v0

    .line 183
    check-cast v0, Lcom/intsig/view/RotateImageTextButton;

    .line 184
    .line 185
    goto :goto_6

    .line 186
    :cond_b
    move-object v0, v1

    .line 187
    :goto_6
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇〇0〇0o8(Lcom/intsig/view/RotateImageTextButton;)V

    .line 188
    .line 189
    .line 190
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇〇o8()Lcom/intsig/view/RotateImageTextButton;

    .line 191
    .line 192
    .line 193
    move-result-object v0

    .line 194
    if-eqz v0, :cond_c

    .line 195
    .line 196
    iget-boolean v4, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoo80oO:Z

    .line 197
    .line 198
    invoke-static {v0, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 199
    .line 200
    .line 201
    :cond_c
    new-array v0, v3, [Landroid/view/View;

    .line 202
    .line 203
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇〇o8()Lcom/intsig/view/RotateImageTextButton;

    .line 204
    .line 205
    .line 206
    move-result-object v3

    .line 207
    aput-object v3, v0, v2

    .line 208
    .line 209
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8O0O808〇([Landroid/view/View;)V

    .line 210
    .line 211
    .line 212
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 213
    .line 214
    :cond_d
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O8o〇O0:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 215
    .line 216
    if-nez v0, :cond_f

    .line 217
    .line 218
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O()Landroid/view/View;

    .line 219
    .line 220
    .line 221
    move-result-object v0

    .line 222
    if-eqz v0, :cond_e

    .line 223
    .line 224
    const v1, 0x7f0a01b6

    .line 225
    .line 226
    .line 227
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 228
    .line 229
    .line 230
    move-result-object v0

    .line 231
    move-object v1, v0

    .line 232
    check-cast v1, Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 233
    .line 234
    :cond_e
    iput-object v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O8o〇O0:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 235
    .line 236
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 237
    .line 238
    :cond_f
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇o()Landroid/view/View;

    .line 239
    .line 240
    .line 241
    move-result-object v0

    .line 242
    if-eqz v0, :cond_10

    .line 243
    .line 244
    const v1, 0x7f0a00f6

    .line 245
    .line 246
    .line 247
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 248
    .line 249
    .line 250
    move-result-object v1

    .line 251
    check-cast v1, Lcom/intsig/camscanner/view/RotateImageView;

    .line 252
    .line 253
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0〇oo(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 254
    .line 255
    .line 256
    const v1, 0x7f0a00f5

    .line 257
    .line 258
    .line 259
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 260
    .line 261
    .line 262
    move-result-object v1

    .line 263
    check-cast v1, Lcom/intsig/camscanner/view/RotateImageView;

    .line 264
    .line 265
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oO8008O(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 266
    .line 267
    .line 268
    const v1, 0x7f0a00f9

    .line 269
    .line 270
    .line 271
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 272
    .line 273
    .line 274
    move-result-object v1

    .line 275
    check-cast v1, Lcom/intsig/camscanner/view/RotateImageView;

    .line 276
    .line 277
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o88O〇8(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 278
    .line 279
    .line 280
    const v1, 0x7f0a00f8

    .line 281
    .line 282
    .line 283
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 284
    .line 285
    .line 286
    move-result-object v0

    .line 287
    check-cast v0, Lcom/intsig/camscanner/view/RotateImageView;

    .line 288
    .line 289
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o08oOO(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 290
    .line 291
    .line 292
    :cond_10
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->OOo()V

    .line 293
    .line 294
    .line 295
    return-void
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
.end method

.method public O〇0〇o808〇([BII)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8O〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇00O0O0()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-interface {v0, p1, p2, p3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo〇O8o〇8([BII)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0o8〇O()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/preview/AbstractPreviewHandle;->〇o〇()Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-nez v1, :cond_1

    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 36
    .line 37
    invoke-virtual {v1, p1, p2, p3}, Lcom/intsig/camscanner/capture/preview/AbstractPreviewHandle;->O8([BII)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇oOo〇()Z

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    if-nez v1, :cond_2

    .line 46
    .line 47
    iget-object v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->Oo0O0o8:Lcom/intsig/camscanner/view/BorderView;

    .line 48
    .line 49
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O〇OO(Lcom/intsig/camscanner/view/BorderView;)V

    .line 50
    .line 51
    .line 52
    :cond_2
    :goto_0
    if-nez v0, :cond_3

    .line 53
    .line 54
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇0ooOOo:Z

    .line 55
    .line 56
    if-eqz v0, :cond_3

    .line 57
    .line 58
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 59
    .line 60
    if-eqz v0, :cond_3

    .line 61
    .line 62
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O〇Oo()Z

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    if-nez v0, :cond_3

    .line 71
    .line 72
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇080:Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;

    .line 73
    .line 74
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->o〇0()Z

    .line 75
    .line 76
    .line 77
    move-result v0

    .line 78
    if-eqz v0, :cond_3

    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇O8oOo0:Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;

    .line 81
    .line 82
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/camscanner/capture/preview/AbstractPreviewHandle;->O8([BII)V

    .line 83
    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 87
    .line 88
    if-eqz p1, :cond_4

    .line 89
    .line 90
    invoke-static {p1}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O〇(Landroid/view/View;)V

    .line 91
    .line 92
    .line 93
    :cond_4
    :goto_1
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public O〇Oooo〇〇()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O〇Oooo〇〇()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇800OO〇0O:Lcom/intsig/view/RotateLayout;

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v1, 0x4

    .line 10
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 11
    .line 12
    .line 13
    :goto_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected O〇O〇oO()Landroid/view/View;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const v1, 0x7f0d06a4

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected o0()V
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o0()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇oOoo〇()V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OoO〇()Lcom/intsig/camscanner/capture/inputdata/CaptureSceneInputData;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/inputdata/CaptureSceneInputData;->〇080()Lcom/intsig/camscanner/capture/inputdata/NormalCaptureInputData;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/inputdata/NormalCaptureInputData;->〇o00〇〇Oo()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    const/4 v2, 0x1

    .line 29
    if-ne v0, v2, :cond_0

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    :cond_0
    iput-boolean v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O〇O:Z

    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method protected o08o〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected o0oO()V
    .locals 0
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected final o88o0O()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇O〇()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v2, 0x1

    .line 17
    if-ne v0, v2, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v2, 0x0

    .line 21
    :goto_0
    if-eqz v2, :cond_5

    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->o800o8O(Z)V

    .line 34
    .line 35
    .line 36
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    if-nez v0, :cond_2

    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_2
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇O888o0o(Z)V

    .line 48
    .line 49
    .line 50
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    if-eqz v0, :cond_3

    .line 59
    .line 60
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->OO0o〇〇()V

    .line 61
    .line 62
    .line 63
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->ooOO()Lcom/intsig/camscanner/capture/CaptureModeMenuManager;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    if-eqz v0, :cond_4

    .line 72
    .line 73
    const/4 v1, 0x0

    .line 74
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/CaptureModeMenuManager;->〇O〇80o08O(Lcom/intsig/camscanner/capture/CaptureMode;)V

    .line 75
    .line 76
    .line 77
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O888o0o()V

    .line 82
    .line 83
    .line 84
    :cond_5
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public o8o〇〇0O(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;->〇oOO8O8(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected final o8〇OO0〇0o()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oooO888:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo88o8O(Z)V

    .line 7
    .line 8
    .line 9
    const-string v0, "BaseNormalCaptureRefactorScene"

    .line 10
    .line 11
    const-string v1, "if true, not show new user guide"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0〇0:Z

    .line 18
    .line 19
    if-eqz v0, :cond_3

    .line 20
    .line 21
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇8〇OOoooo:Z

    .line 22
    .line 23
    if-nez v0, :cond_3

    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-eqz v0, :cond_1

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇0〇O0088o()Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    const/4 v2, 0x1

    .line 40
    if-ne v0, v2, :cond_1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    const/4 v2, 0x0

    .line 44
    :goto_0
    if-eqz v2, :cond_2

    .line 45
    .line 46
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oO8o(Z)V

    .line 47
    .line 48
    .line 49
    :cond_2
    new-instance v0, Lo8o〇〇0O/O8;

    .line 50
    .line 51
    invoke-direct {v0, p0}, Lo8o〇〇0O/O8;-><init>(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 52
    .line 53
    .line 54
    const-wide/16 v1, 0x1f4

    .line 55
    .line 56
    invoke-virtual {p0, v0, v1, v2}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OO0〇〇8(Ljava/lang/Runnable;J)V

    .line 57
    .line 58
    .line 59
    :cond_3
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public oO()V
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oO()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0o8〇O()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;->oO80()V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-static {v0}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O〇(Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoO8OO〇:Landroid/view/View;

    .line 23
    .line 24
    if-eqz v0, :cond_2

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 28
    .line 29
    .line 30
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 31
    .line 32
    const/4 v1, 0x0

    .line 33
    if-eqz v0, :cond_3

    .line 34
    .line 35
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    goto :goto_0

    .line 40
    :cond_3
    move-object v0, v1

    .line 41
    :goto_0
    instance-of v2, v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 42
    .line 43
    if-eqz v2, :cond_4

    .line 44
    .line 45
    move-object v1, v0

    .line 46
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 47
    .line 48
    :cond_4
    if-nez v1, :cond_5

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_5
    const/4 v0, -0x2

    .line 52
    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 53
    .line 54
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 55
    .line 56
    if-eqz v0, :cond_6

    .line 57
    .line 58
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 59
    .line 60
    .line 61
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇O8oOo0:Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;

    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/preview/AbstractPreviewHandle;->oO80()V

    .line 64
    .line 65
    .line 66
    const/4 v0, 0x1

    .line 67
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇0ooOOo:Z

    .line 68
    .line 69
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    invoke-interface {v0, p0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OO〇0008O8(Lcom/intsig/camscanner/capture/core/MoreSettingLayoutStatusListener;)V

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method protected final oO0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇oo〇O〇80:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected final oO0〇〇O8o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoo80oO:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected final oO0〇〇o8〇(IIZZZ)V
    .locals 16

    .line 1
    move/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "go2MultiCaptureFilterPreview, selectCertificateType:"

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    const-string v2, "BaseNormalCaptureRefactorScene"

    .line 21
    .line 22
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    move-object/from16 v2, p0

    .line 27
    .line 28
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->OO〇00〇8oO(Z)V

    .line 29
    .line 30
    .line 31
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    const-string v4, "doc_id"

    .line 40
    .line 41
    const-wide/16 v5, -0x1

    .line 42
    .line 43
    invoke-virtual {v3, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 44
    .line 45
    .line 46
    move-result-wide v3

    .line 47
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 48
    .line 49
    .line 50
    move-result-object v5

    .line 51
    invoke-interface {v5, v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo0oO〇O〇O(I)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 52
    .line 53
    .line 54
    move-result-object v7

    .line 55
    const-string v0, "captureControl.createPar\u2026fo(selectCertificateType)"

    .line 56
    .line 57
    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇o8()Ljava/util/List;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->Oo〇o(Ljava/util/List;)[J

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    iput-object v0, v7, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 73
    .line 74
    const/4 v0, 0x1

    .line 75
    const-wide/16 v5, 0x0

    .line 76
    .line 77
    cmp-long v8, v3, v5

    .line 78
    .line 79
    if-gez v8, :cond_0

    .line 80
    .line 81
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 86
    .line 87
    .line 88
    move-result-wide v3

    .line 89
    cmp-long v8, v3, v5

    .line 90
    .line 91
    if-lez v8, :cond_0

    .line 92
    .line 93
    const/4 v3, 0x1

    .line 94
    goto :goto_0

    .line 95
    :cond_0
    const/4 v3, 0x0

    .line 96
    :goto_0
    iput-boolean v3, v7, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO〇00〇8oO:Z

    .line 97
    .line 98
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 99
    .line 100
    .line 101
    move-result-object v3

    .line 102
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->o〇8〇()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v3

    .line 106
    iput-object v3, v7, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 107
    .line 108
    if-lez p2, :cond_1

    .line 109
    .line 110
    move/from16 v0, p2

    .line 111
    .line 112
    :goto_1
    const/4 v8, 0x0

    .line 113
    goto :goto_2

    .line 114
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8O〇88oO0()Z

    .line 115
    .line 116
    .line 117
    move-result v3

    .line 118
    if-eqz v3, :cond_2

    .line 119
    .line 120
    const/16 v1, 0x321

    .line 121
    .line 122
    const/16 v0, 0x321

    .line 123
    .line 124
    const/4 v8, 0x1

    .line 125
    goto :goto_2

    .line 126
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O00()Z

    .line 127
    .line 128
    .line 129
    move-result v0

    .line 130
    if-eqz v0, :cond_3

    .line 131
    .line 132
    const/16 v0, 0x385

    .line 133
    .line 134
    goto :goto_1

    .line 135
    :cond_3
    const/16 v0, 0xde

    .line 136
    .line 137
    goto :goto_1

    .line 138
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 139
    .line 140
    .line 141
    move-result-object v6

    .line 142
    const/4 v9, -0x1

    .line 143
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 144
    .line 145
    .line 146
    move-result-object v1

    .line 147
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O0OO8〇0()Z

    .line 148
    .line 149
    .line 150
    move-result v10

    .line 151
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 152
    .line 153
    .line 154
    move-result-object v1

    .line 155
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇0O〇Oo()Z

    .line 156
    .line 157
    .line 158
    move-result v11

    .line 159
    const/4 v12, 0x0

    .line 160
    const/4 v13, 0x0

    .line 161
    const-string v14, "normal_multi"

    .line 162
    .line 163
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 164
    .line 165
    .line 166
    move-result-object v1

    .line 167
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo〇o()Ljava/lang/String;

    .line 168
    .line 169
    .line 170
    move-result-object v15

    .line 171
    invoke-static/range {v6 .. v15}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewActivity;->O〇080〇o0(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;ZIZZLjava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    .line 173
    .line 174
    move-result-object v1

    .line 175
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o80ooO()Z

    .line 176
    .line 177
    .line 178
    move-result v3

    .line 179
    if-eqz v3, :cond_5

    .line 180
    .line 181
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 182
    .line 183
    .line 184
    move-result-object v3

    .line 185
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇o8()Ljava/util/List;

    .line 186
    .line 187
    .line 188
    move-result-object v3

    .line 189
    instance-of v4, v3, Ljava/util/ArrayList;

    .line 190
    .line 191
    if-eqz v4, :cond_4

    .line 192
    .line 193
    check-cast v3, Ljava/util/ArrayList;

    .line 194
    .line 195
    goto :goto_3

    .line 196
    :cond_4
    const/4 v3, 0x0

    .line 197
    :goto_3
    if-eqz v3, :cond_5

    .line 198
    .line 199
    const-string v4, "extra_certificatepageids"

    .line 200
    .line 201
    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 202
    .line 203
    .line 204
    :cond_5
    const-string v3, "extra_multi_scan_process"

    .line 205
    .line 206
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇oo〇O〇80()I

    .line 207
    .line 208
    .line 209
    move-result v4

    .line 210
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 211
    .line 212
    .line 213
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 214
    .line 215
    .line 216
    move-result-object v3

    .line 217
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇0OO8()Z

    .line 218
    .line 219
    .line 220
    move-result v3

    .line 221
    const-string v4, "extra_show_all_capture_mode"

    .line 222
    .line 223
    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 224
    .line 225
    .line 226
    const-string v3, "extra_from_single_capture"

    .line 227
    .line 228
    move/from16 v4, p3

    .line 229
    .line 230
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 231
    .line 232
    .line 233
    const-string v3, "extra_from_show_wave_animation"

    .line 234
    .line 235
    move/from16 v4, p4

    .line 236
    .line 237
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 238
    .line 239
    .line 240
    const-string v3, "extra_boolean_show_adapter_enhance_anim"

    .line 241
    .line 242
    move/from16 v4, p5

    .line 243
    .line 244
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 245
    .line 246
    .line 247
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 248
    .line 249
    .line 250
    move-result-object v3

    .line 251
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->o0O0()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 252
    .line 253
    .line 254
    move-result-object v3

    .line 255
    const-string v4, "extra_capture_function_entrance"

    .line 256
    .line 257
    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 258
    .line 259
    .line 260
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 261
    .line 262
    .line 263
    move-result-object v3

    .line 264
    invoke-static {v3, v1, v0}, Lcom/intsig/utils/TransitionUtil;->〇o00〇〇Oo(Landroid/app/Activity;Landroid/content/Intent;I)V

    .line 265
    .line 266
    .line 267
    return-void
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
.end method

.method protected final oOo0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected final oOo〇8o008(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o〇0〇o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected onDestroy()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->onDestroy()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;->oO80()V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O〇08()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected final o〇00O0O〇o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOo〇08〇:Lcom/intsig/camscanner/multiimageedit/viewModel/CaptureMergeGuideViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-direct {v0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 12
    .line 13
    .line 14
    const-class v1, Lcom/intsig/camscanner/multiimageedit/viewModel/CaptureMergeGuideViewModel;

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/viewModel/CaptureMergeGuideViewModel;

    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOo〇08〇:Lcom/intsig/camscanner/multiimageedit/viewModel/CaptureMergeGuideViewModel;

    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected final o〇0〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇O8〇〇o(Landroid/content/Intent;)V
    .locals 9

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0o8〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;->〇oo〇()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;->oO80()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->o8O0()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    if-eqz v2, :cond_0

    .line 29
    .line 30
    invoke-static {v2}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    if-eqz v4, :cond_0

    .line 35
    .line 36
    const/4 v0, 0x1

    .line 37
    new-array v7, v0, [Z

    .line 38
    .line 39
    const/4 v8, 0x0

    .line 40
    aput-boolean v0, v7, v8

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇00OO()I

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    new-array v6, v0, [I

    .line 53
    .line 54
    move-object v5, v7

    .line 55
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/camscanner/capture/preview/DetectPreviewBorderHandle;->〇〇808〇(Ljava/lang/String;I[I[Z[I)[I

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    aget-boolean v1, v7, v8

    .line 60
    .line 61
    if-eqz v1, :cond_0

    .line 62
    .line 63
    if-eqz v0, :cond_0

    .line 64
    .line 65
    if-eqz p1, :cond_0

    .line 66
    .line 67
    const-string v1, "extra_border"

    .line 68
    .line 69
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 70
    .line 71
    .line 72
    :cond_0
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method protected final o〇OOo000()Lcom/intsig/camscanner/view/BorderView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->Oo0O0o8:Lcom/intsig/camscanner/view/BorderView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected final 〇08O8o8()Lcom/intsig/view/RotateLayout;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇800OO〇0O:Lcom/intsig/view/RotateLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇08O〇00〇o(ILandroid/content/Intent;)V
    .locals 0

    .line 1
    const/4 p2, -0x1

    .line 2
    if-ne p1, p2, :cond_0

    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-virtual {p1, p2}, Landroid/app/Activity;->setResult(I)V

    .line 9
    .line 10
    .line 11
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method protected final 〇0O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected final 〇0O00oO()Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected final 〇O80〇oOo()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇80O8o8O〇:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const-string v3, "getInstance()"

    .line 16
    .line 17
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {v0, v1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 21
    .line 22
    .line 23
    const-class v1, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇80O8o8O〇:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 32
    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇oo(Landroid/app/Activity;)V

    .line 40
    .line 41
    .line 42
    :cond_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method protected final 〇OO0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 18
    .param p1    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    const-string v1, "multiImageEditModel"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    iget-boolean v2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇800OO〇0O:Z

    .line 13
    .line 14
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇00O0O0(Z)Z

    .line 15
    .line 16
    .line 17
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 26
    .line 27
    .line 28
    move-result-wide v2

    .line 29
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    sget-object v2, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇080:Lcom/intsig/camscanner/app/DBInsertPageUtil;

    .line 34
    .line 35
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 40
    .line 41
    .line 42
    move-result-wide v3

    .line 43
    iget-object v5, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 44
    .line 45
    add-int/lit8 v6, v1, 0x1

    .line 46
    .line 47
    const/4 v7, 0x1

    .line 48
    iget-object v8, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 49
    .line 50
    const/4 v9, 0x1

    .line 51
    iget v10, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 52
    .line 53
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇0000OOO()Z

    .line 58
    .line 59
    .line 60
    move-result v11

    .line 61
    const/4 v12, 0x1

    .line 62
    const/4 v13, 0x1

    .line 63
    const/4 v14, 0x1

    .line 64
    const/4 v15, 0x0

    .line 65
    const/16 v16, 0x800

    .line 66
    .line 67
    const/16 v17, 0x0

    .line 68
    .line 69
    invoke-static/range {v2 .. v17}, Lcom/intsig/camscanner/app/DBInsertPageUtil;->o800o8O(Lcom/intsig/camscanner/app/DBInsertPageUtil;JLjava/lang/String;IZ[IIIZZZZLjava/lang/Integer;ILjava/lang/Object;)Landroid/net/Uri;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    if-eqz v1, :cond_0

    .line 74
    .line 75
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 80
    .line 81
    .line 82
    move-result-object v3

    .line 83
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇O8o08O()J

    .line 84
    .line 85
    .line 86
    move-result-wide v3

    .line 87
    invoke-static {v2, v3, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->Oo〇O(Landroid/content/Context;J)V

    .line 88
    .line 89
    .line 90
    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 91
    .line 92
    .line 93
    move-result-wide v1

    .line 94
    iput-wide v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 95
    .line 96
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇〇o8()Ljava/util/List;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 105
    .line 106
    .line 107
    move-result-object v1

    .line 108
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_0
    const-string v0, "BaseNormalCaptureRefactorScene"

    .line 113
    .line 114
    const-string v1, "insertPageToDB insertImageUri= null"

    .line 115
    .line 116
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    :goto_0
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public 〇O〇(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O8o〇O0:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O8o〇O0:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/CircleProgressBar;->O8(F)V

    .line 14
    .line 15
    .line 16
    :cond_1
    const-wide/16 v0, 0x0

    .line 17
    .line 18
    const-wide/16 v2, 0x3e8

    .line 19
    .line 20
    const v4, 0x7f130879

    .line 21
    .line 22
    .line 23
    if-eqz p1, :cond_3

    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇o〇88〇8:Landroid/view/View;

    .line 26
    .line 27
    if-eqz p1, :cond_2

    .line 28
    .line 29
    invoke-static {p1}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O〇(Landroid/view/View;)V

    .line 30
    .line 31
    .line 32
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 33
    .line 34
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;->〇080(JJ)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    const v2, 0x7f130818

    .line 54
    .line 55
    .line 56
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    new-instance v2, Ljava/lang/StringBuilder;

    .line 61
    .line 62
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo8Oo00oo(Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_3
    iget-boolean p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 80
    .line 81
    if-eqz p1, :cond_4

    .line 82
    .line 83
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇080OO8〇0(JJ)V

    .line 84
    .line 85
    .line 86
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    const v2, 0x7f130817

    .line 103
    .line 104
    .line 105
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    new-instance v2, Ljava/lang/StringBuilder;

    .line 110
    .line 111
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    .line 113
    .line 114
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo8Oo00oo(Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 128
    .line 129
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;->oO80()V

    .line 130
    .line 131
    .line 132
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->Oo0O0o8:Lcom/intsig/camscanner/view/BorderView;

    .line 133
    .line 134
    if-eqz p1, :cond_5

    .line 135
    .line 136
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/BorderView;->〇o00〇〇Oo()V

    .line 137
    .line 138
    .line 139
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇800OO〇0O:Lcom/intsig/view/RotateLayout;

    .line 140
    .line 141
    if-nez p1, :cond_6

    .line 142
    .line 143
    goto :goto_0

    .line 144
    :cond_6
    const/4 v0, 0x4

    .line 145
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 146
    .line 147
    .line 148
    :goto_0
    return-void
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method protected 〇o8OO0()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇O8oOo0:Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;->〇O〇(Z)V

    .line 9
    .line 10
    .line 11
    const-wide/16 v0, 0x0

    .line 12
    .line 13
    invoke-direct {p0, v0, v1, v0, v1}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇080OO8〇0(JJ)V

    .line 14
    .line 15
    .line 16
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 21
    .line 22
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O〇Oooo〇〇(Z)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-interface {v0, p0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo08OO8oO(Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-interface {v0, p0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo(Lcom/intsig/camscanner/capture/core/MoreSettingLayoutStatusListener;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇o0O0O8()Lcom/intsig/view/RotateLayout;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    if-nez v0, :cond_1

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_1
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOoo80oO:Z

    .line 47
    .line 48
    if-eqz v1, :cond_2

    .line 49
    .line 50
    const/4 v1, 0x0

    .line 51
    goto :goto_0

    .line 52
    :cond_2
    const/16 v1, 0x8

    .line 53
    .line 54
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 55
    .line 56
    .line 57
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇〇8O0〇8()V

    .line 58
    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O8o〇O0:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 61
    .line 62
    if-eqz v0, :cond_3

    .line 63
    .line 64
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0o8〇O()Z

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 69
    .line 70
    .line 71
    :cond_3
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇oOO8O8()V
    .locals 5

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇oOO8O8()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0o8〇O()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 11
    .line 12
    const-wide/16 v1, 0x1f4

    .line 13
    .line 14
    const-wide/16 v3, 0x0

    .line 15
    .line 16
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;->〇080(JJ)V

    .line 17
    .line 18
    .line 19
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->oo()V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇oOo〇()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOO()Lcom/intsig/camscanner/capture/CaptureMode;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_WORKBENCH:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 6
    .line 7
    if-eq v0, v1, :cond_1

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0o8〇O()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 19
    :goto_1
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o〇8()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇o〇8()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;->oO80()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O8o〇O0:Lcom/intsig/camscanner/view/CircleProgressBar;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/CircleProgressBar;->O8(F)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o〇o(Z)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇08O8o〇0(Z)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->OO〇00〇8oO(Z)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    if-eqz p1, :cond_0

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇0000OOO()V

    .line 23
    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇〇00OO(IILandroid/content/Intent;)Z
    .locals 5

    .line 1
    const/16 v0, 0x64

    .line 2
    .line 3
    const-string v1, "BaseNormalCaptureRefactorScene"

    .line 4
    .line 5
    if-eq p1, v0, :cond_f

    .line 6
    .line 7
    const/16 v0, 0xca

    .line 8
    .line 9
    const-string v2, "single"

    .line 10
    .line 11
    const/4 v3, -0x1

    .line 12
    if-eq p1, v0, :cond_d

    .line 13
    .line 14
    const/16 v0, 0xcd

    .line 15
    .line 16
    if-eq p1, v0, :cond_b

    .line 17
    .line 18
    const/16 v0, 0xdb

    .line 19
    .line 20
    if-eq p1, v0, :cond_8

    .line 21
    .line 22
    const/16 v0, 0xdf

    .line 23
    .line 24
    if-eq p1, v0, :cond_7

    .line 25
    .line 26
    const/16 v0, 0x322

    .line 27
    .line 28
    const/4 v4, 0x0

    .line 29
    if-eq p1, v0, :cond_2

    .line 30
    .line 31
    const/16 v0, 0x85

    .line 32
    .line 33
    if-eq p1, v0, :cond_1

    .line 34
    .line 35
    const/16 v0, 0x86

    .line 36
    .line 37
    if-eq p1, v0, :cond_0

    .line 38
    .line 39
    goto/16 :goto_1

    .line 40
    .line 41
    :cond_0
    const-string p1, "onActivityResult uris are null GO_TO_BATCH"

    .line 42
    .line 43
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0, p2, p3}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇08O〇00〇o(ILandroid/content/Intent;)V

    .line 47
    .line 48
    .line 49
    goto/16 :goto_0

    .line 50
    .line 51
    :cond_1
    const-string p1, "onActivityResult PICK_IMAGE="

    .line 52
    .line 53
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    if-ne p2, v3, :cond_10

    .line 57
    .line 58
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0〇oO〇o(Landroid/content/Intent;)V

    .line 59
    .line 60
    .line 61
    goto/16 :goto_0

    .line 62
    .line 63
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    .line 64
    .line 65
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    .line 67
    .line 68
    const-string p3, "onActivityResult KEY_REQUEST_SINGLE_TO_MULTI resultCode = "

    .line 69
    .line 70
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    if-ne p2, v3, :cond_5

    .line 84
    .line 85
    const-string p1, "onActivityResult finishMultiPage Activity.RESULT_OK "

    .line 86
    .line 87
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    const/4 p2, 0x0

    .line 95
    invoke-interface {p1, v4, p2}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OO0〇〇8(ZLcom/intsig/callback/Callback0;)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 99
    .line 100
    .line 101
    move-result-object p1

    .line 102
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object p1

    .line 110
    const-string p2, "com.intsig.camscanner.NEW_DOC_MULTIPLE"

    .line 111
    .line 112
    if-eqz p1, :cond_3

    .line 113
    .line 114
    const-string p3, "com.intsig.camscanner.NEW_DOC"

    .line 115
    .line 116
    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 117
    .line 118
    .line 119
    move-result p3

    .line 120
    if-nez p3, :cond_3

    .line 121
    .line 122
    const-string p3, "android.intent.action.VIEW"

    .line 123
    .line 124
    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 125
    .line 126
    .line 127
    move-result p3

    .line 128
    if-eqz p3, :cond_4

    .line 129
    .line 130
    :cond_3
    move-object p1, p2

    .line 131
    :cond_4
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 132
    .line 133
    .line 134
    move-result p1

    .line 135
    if-eqz p1, :cond_10

    .line 136
    .line 137
    invoke-static {v2}, Lcom/intsig/appsflyer/AppsFlyerHelper;->Oo08(Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    const-string p1, "SCANNER_ACTION_NEW_DOC_MULTIPLE batch from single"

    .line 141
    .line 142
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    goto/16 :goto_0

    .line 146
    .line 147
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇80O8o8O〇:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 148
    .line 149
    if-eqz p1, :cond_6

    .line 150
    .line 151
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 152
    .line 153
    .line 154
    move-result-object p1

    .line 155
    if-eqz p1, :cond_6

    .line 156
    .line 157
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 158
    .line 159
    .line 160
    move-result v4

    .line 161
    :cond_6
    if-lez v4, :cond_10

    .line 162
    .line 163
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->o88o0O()V

    .line 164
    .line 165
    .line 166
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 167
    .line 168
    .line 169
    move-result-object p1

    .line 170
    new-instance p2, Lo8o〇〇0O/Oo08;

    .line 171
    .line 172
    invoke-direct {p2, p0}, Lo8o〇〇0O/Oo08;-><init>(Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;)V

    .line 173
    .line 174
    .line 175
    invoke-virtual {p1, p2}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 176
    .line 177
    .line 178
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oO()V

    .line 179
    .line 180
    .line 181
    iget-object p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO0880O:Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;

    .line 182
    .line 183
    sget-object p2, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_MULTI:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 184
    .line 185
    new-instance p3, Landroid/content/Intent;

    .line 186
    .line 187
    invoke-direct {p3}, Landroid/content/Intent;-><init>()V

    .line 188
    .line 189
    .line 190
    invoke-virtual {p1, p2, p3}, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/capture/CaptureMode;Landroid/content/Intent;)V

    .line 191
    .line 192
    .line 193
    goto/16 :goto_0

    .line 194
    .line 195
    :cond_7
    const-string p1, "onActivityResult uris are null IMPORT_TO_MUITLI_FILTER"

    .line 196
    .line 197
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .line 199
    .line 200
    if-ne p2, v3, :cond_10

    .line 201
    .line 202
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 203
    .line 204
    .line 205
    move-result-object p1

    .line 206
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 207
    .line 208
    .line 209
    goto/16 :goto_0

    .line 210
    .line 211
    :cond_8
    if-ne p2, v3, :cond_10

    .line 212
    .line 213
    if-eqz p3, :cond_a

    .line 214
    .line 215
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 216
    .line 217
    .line 218
    move-result-object p1

    .line 219
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo〇o()Ljava/lang/String;

    .line 220
    .line 221
    .line 222
    move-result-object p1

    .line 223
    const-string p2, "1"

    .line 224
    .line 225
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 226
    .line 227
    .line 228
    move-result p1

    .line 229
    if-eqz p1, :cond_9

    .line 230
    .line 231
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 232
    .line 233
    .line 234
    move-result-object p1

    .line 235
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->Oo〇o()Ljava/lang/String;

    .line 236
    .line 237
    .line 238
    move-result-object p1

    .line 239
    const-string p2, "EXTRA_LOTTERY_VALUE"

    .line 240
    .line 241
    invoke-virtual {p3, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    .line 243
    .line 244
    new-instance p1, Lcom/intsig/camscanner/eventbus/ImportFileEvent;

    .line 245
    .line 246
    invoke-direct {p1, p3}, Lcom/intsig/camscanner/eventbus/ImportFileEvent;-><init>(Landroid/content/Intent;)V

    .line 247
    .line 248
    .line 249
    invoke-static {p1}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 250
    .line 251
    .line 252
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 253
    .line 254
    .line 255
    move-result-object p1

    .line 256
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 257
    .line 258
    .line 259
    goto/16 :goto_0

    .line 260
    .line 261
    :cond_9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 262
    .line 263
    .line 264
    move-result-object p1

    .line 265
    const/16 p2, 0x1f4

    .line 266
    .line 267
    invoke-virtual {p1, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 268
    .line 269
    .line 270
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 271
    .line 272
    .line 273
    move-result-object p1

    .line 274
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 275
    .line 276
    .line 277
    goto/16 :goto_0

    .line 278
    .line 279
    :cond_a
    const-string p1, "pick pdf result: data = null "

    .line 280
    .line 281
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    .line 283
    .line 284
    goto/16 :goto_0

    .line 285
    .line 286
    :cond_b
    new-instance p1, Ljava/lang/StringBuilder;

    .line 287
    .line 288
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 289
    .line 290
    .line 291
    const-string v0, "onActivityResult REQ_PAGE_RETAKE resultCode = "

    .line 292
    .line 293
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    .line 295
    .line 296
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 297
    .line 298
    .line 299
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 300
    .line 301
    .line 302
    move-result-object p1

    .line 303
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    .line 305
    .line 306
    if-ne p2, v3, :cond_c

    .line 307
    .line 308
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 309
    .line 310
    .line 311
    move-result-object p1

    .line 312
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇Oo〇o8()V

    .line 313
    .line 314
    .line 315
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 316
    .line 317
    .line 318
    move-result-object p1

    .line 319
    invoke-virtual {p1, v3, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 320
    .line 321
    .line 322
    :cond_c
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 323
    .line 324
    .line 325
    move-result-object p1

    .line 326
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 327
    .line 328
    .line 329
    goto :goto_0

    .line 330
    :cond_d
    new-instance p1, Ljava/lang/StringBuilder;

    .line 331
    .line 332
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 333
    .line 334
    .line 335
    const-string v0, "onActivityResult ACTION_NEW_DOC resultCode="

    .line 336
    .line 337
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    .line 339
    .line 340
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 341
    .line 342
    .line 343
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 344
    .line 345
    .line 346
    move-result-object p1

    .line 347
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    .line 349
    .line 350
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇〇o0o(Landroid/content/Intent;)Z

    .line 351
    .line 352
    .line 353
    move-result p1

    .line 354
    if-nez p1, :cond_e

    .line 355
    .line 356
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 357
    .line 358
    .line 359
    move-result-object p1

    .line 360
    invoke-interface {p1, p2, p3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇000〇〇08(ILandroid/content/Intent;)V

    .line 361
    .line 362
    .line 363
    :cond_e
    if-ne p2, v3, :cond_10

    .line 364
    .line 365
    iget-boolean p1, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->oOO8:Z

    .line 366
    .line 367
    if-eqz p1, :cond_10

    .line 368
    .line 369
    invoke-static {v2}, Lcom/intsig/appsflyer/AppsFlyerHelper;->Oo08(Ljava/lang/String;)V

    .line 370
    .line 371
    .line 372
    const-string p1, "ACTION_NEW_DOC single"

    .line 373
    .line 374
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    .line 376
    .line 377
    goto :goto_0

    .line 378
    :cond_f
    new-instance p1, Ljava/lang/StringBuilder;

    .line 379
    .line 380
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 381
    .line 382
    .line 383
    const-string v0, "onActivityResult TRIM_ENHANCE_IMG resultCode="

    .line 384
    .line 385
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    .line 387
    .line 388
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 389
    .line 390
    .line 391
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 392
    .line 393
    .line 394
    move-result-object p1

    .line 395
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    .line 397
    .line 398
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇〇o0o(Landroid/content/Intent;)Z

    .line 399
    .line 400
    .line 401
    move-result p1

    .line 402
    if-nez p1, :cond_10

    .line 403
    .line 404
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 405
    .line 406
    .line 407
    move-result-object p1

    .line 408
    invoke-interface {p1, p2, p3}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->OOo0O(ILandroid/content/Intent;)V

    .line 409
    .line 410
    .line 411
    :cond_10
    :goto_0
    const/4 v4, 0x1

    .line 412
    :goto_1
    return v4
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
.end method

.method public 〇〇8O0〇8()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->O0o8〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/capture/normal/BaseNormalCaptureRefactorScene;->〇088O:Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;

    .line 8
    .line 9
    const-wide/16 v1, 0x64

    .line 10
    .line 11
    const-wide/16 v3, 0x0

    .line 12
    .line 13
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/capture/preview/AutoCaptureHandle;->〇080(JJ)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.class Lcom/intsig/camscanner/capture/DraftView$DrawHandler;
.super Landroid/os/Handler;
.source "DraftView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/DraftView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DrawHandler"
.end annotation


# instance fields
.field 〇080:Landroid/graphics/Paint;

.field final synthetic 〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/capture/DraftView;Landroid/os/Looper;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 2
    .line 3
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 4
    .line 5
    .line 6
    new-instance p2, Landroid/graphics/Paint;

    .line 7
    .line 8
    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object p2, p0, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 12
    .line 13
    invoke-static {p1}, Lcom/intsig/camscanner/capture/DraftView;->〇O00(Lcom/intsig/camscanner/capture/DraftView;)F

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 16

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->o〇0(Lcom/intsig/camscanner/capture/DraftView;)Landroid/view/SurfaceHolder;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-interface {v0}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    if-nez v2, :cond_0

    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    .line 20
    .line 21
    .line 22
    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    .line 23
    .line 24
    .line 25
    const/high16 v0, -0x1000000

    .line 26
    .line 27
    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 28
    .line 29
    .line 30
    iget-object v3, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 31
    .line 32
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 33
    .line 34
    .line 35
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 36
    .line 37
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 38
    .line 39
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 47
    .line 48
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇8o8o〇(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Matrix;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 53
    .line 54
    .line 55
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 56
    .line 57
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇080(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    const-string v4, "DraftView"

    .line 62
    .line 63
    if-eqz v0, :cond_7

    .line 64
    .line 65
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 66
    .line 67
    iget v8, v0, Lcom/intsig/camscanner/capture/DraftView;->〇O〇〇O8:I

    .line 68
    .line 69
    if-nez v8, :cond_5

    .line 70
    .line 71
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇o〇(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 76
    .line 77
    .line 78
    move-result v8

    .line 79
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 80
    .line 81
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇o〇(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 86
    .line 87
    .line 88
    move-result v9

    .line 89
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 90
    .line 91
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇080(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 96
    .line 97
    .line 98
    move-result v10

    .line 99
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 100
    .line 101
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇080(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 106
    .line 107
    .line 108
    move-result v11

    .line 109
    const/4 v12, 0x0

    .line 110
    const/4 v13, 0x0

    .line 111
    :goto_0
    if-ge v13, v9, :cond_7

    .line 112
    .line 113
    const/4 v0, 0x0

    .line 114
    :goto_1
    if-ge v0, v8, :cond_4

    .line 115
    .line 116
    add-int v14, v0, v10

    .line 117
    .line 118
    if-le v14, v8, :cond_1

    .line 119
    .line 120
    add-int v15, v13, v11

    .line 121
    .line 122
    if-le v15, v9, :cond_1

    .line 123
    .line 124
    iget-object v15, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 125
    .line 126
    invoke-static {v15}, Lcom/intsig/camscanner/capture/DraftView;->〇080(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 127
    .line 128
    .line 129
    move-result-object v15

    .line 130
    new-instance v6, Landroid/graphics/Rect;

    .line 131
    .line 132
    sub-int v5, v8, v0

    .line 133
    .line 134
    sub-int v7, v9, v13

    .line 135
    .line 136
    invoke-direct {v6, v12, v12, v5, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 137
    .line 138
    .line 139
    new-instance v5, Landroid/graphics/Rect;

    .line 140
    .line 141
    invoke-direct {v5, v0, v13, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 142
    .line 143
    .line 144
    const/4 v7, 0x0

    .line 145
    invoke-virtual {v2, v15, v6, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 146
    .line 147
    .line 148
    goto :goto_2

    .line 149
    :cond_1
    add-int v5, v13, v11

    .line 150
    .line 151
    if-le v5, v9, :cond_2

    .line 152
    .line 153
    :try_start_0
    iget-object v5, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 154
    .line 155
    invoke-static {v5}, Lcom/intsig/camscanner/capture/DraftView;->〇080(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 156
    .line 157
    .line 158
    move-result-object v5

    .line 159
    new-instance v6, Landroid/graphics/Rect;

    .line 160
    .line 161
    sub-int v7, v9, v13

    .line 162
    .line 163
    invoke-direct {v6, v12, v12, v10, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 164
    .line 165
    .line 166
    new-instance v7, Landroid/graphics/Rect;

    .line 167
    .line 168
    invoke-direct {v7, v0, v13, v14, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 169
    .line 170
    .line 171
    const/4 v15, 0x0

    .line 172
    invoke-virtual {v2, v5, v6, v7, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    .line 174
    .line 175
    goto :goto_2

    .line 176
    :catch_0
    move-exception v0

    .line 177
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 178
    .line 179
    .line 180
    goto :goto_2

    .line 181
    :cond_2
    if-le v14, v8, :cond_3

    .line 182
    .line 183
    iget-object v6, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 184
    .line 185
    invoke-static {v6}, Lcom/intsig/camscanner/capture/DraftView;->〇080(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 186
    .line 187
    .line 188
    move-result-object v6

    .line 189
    new-instance v7, Landroid/graphics/Rect;

    .line 190
    .line 191
    sub-int v15, v8, v0

    .line 192
    .line 193
    invoke-direct {v7, v12, v12, v15, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 194
    .line 195
    .line 196
    new-instance v15, Landroid/graphics/Rect;

    .line 197
    .line 198
    invoke-direct {v15, v0, v13, v8, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 199
    .line 200
    .line 201
    const/4 v5, 0x0

    .line 202
    invoke-virtual {v2, v6, v7, v15, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 203
    .line 204
    .line 205
    goto :goto_2

    .line 206
    :cond_3
    const/4 v5, 0x0

    .line 207
    iget-object v6, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 208
    .line 209
    invoke-static {v6}, Lcom/intsig/camscanner/capture/DraftView;->〇080(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 210
    .line 211
    .line 212
    move-result-object v6

    .line 213
    int-to-float v0, v0

    .line 214
    int-to-float v7, v13

    .line 215
    invoke-virtual {v2, v6, v0, v7, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 216
    .line 217
    .line 218
    :goto_2
    move v0, v14

    .line 219
    goto :goto_1

    .line 220
    :cond_4
    add-int/2addr v13, v11

    .line 221
    goto :goto_0

    .line 222
    :cond_5
    const/4 v5, 0x1

    .line 223
    if-eq v8, v5, :cond_6

    .line 224
    .line 225
    const/4 v5, 0x2

    .line 226
    if-ne v8, v5, :cond_7

    .line 227
    .line 228
    :cond_6
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇080(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 229
    .line 230
    .line 231
    move-result-object v0

    .line 232
    const/4 v5, 0x0

    .line 233
    const/4 v6, 0x0

    .line 234
    invoke-virtual {v2, v0, v5, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 235
    .line 236
    .line 237
    goto :goto_3

    .line 238
    :cond_7
    const/4 v5, 0x0

    .line 239
    :goto_3
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 240
    .line 241
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇o〇(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 242
    .line 243
    .line 244
    move-result-object v0

    .line 245
    if-eqz v0, :cond_8

    .line 246
    .line 247
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 248
    .line 249
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇o〇(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Bitmap;

    .line 250
    .line 251
    .line 252
    move-result-object v0

    .line 253
    iget-object v6, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 254
    .line 255
    invoke-virtual {v2, v0, v5, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 256
    .line 257
    .line 258
    :cond_8
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 259
    .line 260
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/DraftView;)Lcom/intsig/camscanner/capture/DraftView$ClippedImage;

    .line 261
    .line 262
    .line 263
    move-result-object v0

    .line 264
    iget-boolean v0, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇080:Z

    .line 265
    .line 266
    const/high16 v5, 0x40000000    # 2.0f

    .line 267
    .line 268
    if-eqz v0, :cond_9

    .line 269
    .line 270
    :try_start_1
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 271
    .line 272
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/DraftView;)Lcom/intsig/camscanner/capture/DraftView$ClippedImage;

    .line 273
    .line 274
    .line 275
    move-result-object v0

    .line 276
    iget v0, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o〇:F

    .line 277
    .line 278
    iget-object v6, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 279
    .line 280
    invoke-static {v6}, Lcom/intsig/camscanner/capture/DraftView;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/DraftView;)Lcom/intsig/camscanner/capture/DraftView$ClippedImage;

    .line 281
    .line 282
    .line 283
    move-result-object v6

    .line 284
    iget v6, v6, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->O8:F

    .line 285
    .line 286
    iget-object v7, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 287
    .line 288
    invoke-static {v7}, Lcom/intsig/camscanner/capture/DraftView;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/DraftView;)Lcom/intsig/camscanner/capture/DraftView$ClippedImage;

    .line 289
    .line 290
    .line 291
    move-result-object v7

    .line 292
    invoke-virtual {v7}, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->o〇0()F

    .line 293
    .line 294
    .line 295
    move-result v7

    .line 296
    add-float/2addr v7, v0

    .line 297
    iget-object v8, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 298
    .line 299
    invoke-static {v8}, Lcom/intsig/camscanner/capture/DraftView;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/DraftView;)Lcom/intsig/camscanner/capture/DraftView$ClippedImage;

    .line 300
    .line 301
    .line 302
    move-result-object v8

    .line 303
    invoke-virtual {v8}, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->Oo08()F

    .line 304
    .line 305
    .line 306
    move-result v8

    .line 307
    add-float/2addr v8, v6

    .line 308
    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    .line 309
    .line 310
    .line 311
    move-result v9

    .line 312
    iget-object v10, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 313
    .line 314
    invoke-static {v10}, Lcom/intsig/camscanner/capture/DraftView;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/DraftView;)Lcom/intsig/camscanner/capture/DraftView$ClippedImage;

    .line 315
    .line 316
    .line 317
    move-result-object v10

    .line 318
    iget-object v10, v10, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 319
    .line 320
    invoke-virtual {v2, v10}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 321
    .line 322
    .line 323
    iget-object v10, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 324
    .line 325
    const v11, -0xe64364

    .line 326
    .line 327
    .line 328
    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 329
    .line 330
    .line 331
    iget-object v10, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 332
    .line 333
    const/4 v11, 0x1

    .line 334
    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 335
    .line 336
    .line 337
    iget-object v10, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 338
    .line 339
    const/high16 v11, 0x41000000    # 8.0f

    .line 340
    .line 341
    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 342
    .line 343
    .line 344
    iget-object v10, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 345
    .line 346
    sget-object v11, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 347
    .line 348
    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 349
    .line 350
    .line 351
    new-instance v10, Landroid/graphics/RectF;

    .line 352
    .line 353
    invoke-direct {v10, v0, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 354
    .line 355
    .line 356
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 357
    .line 358
    const/4 v6, 0x0

    .line 359
    invoke-virtual {v2, v10, v6, v6, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 360
    .line 361
    .line 362
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 363
    .line 364
    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 365
    .line 366
    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 367
    .line 368
    .line 369
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 370
    .line 371
    const v6, -0xffff01

    .line 372
    .line 373
    .line 374
    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 375
    .line 376
    .line 377
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 378
    .line 379
    const/high16 v6, 0x40800000    # 4.0f

    .line 380
    .line 381
    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 382
    .line 383
    .line 384
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 385
    .line 386
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/DraftView;)Lcom/intsig/camscanner/capture/DraftView$ClippedImage;

    .line 387
    .line 388
    .line 389
    move-result-object v0

    .line 390
    iget-object v0, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 391
    .line 392
    iget-object v6, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 393
    .line 394
    invoke-static {v6}, Lcom/intsig/camscanner/capture/DraftView;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/DraftView;)Lcom/intsig/camscanner/capture/DraftView$ClippedImage;

    .line 395
    .line 396
    .line 397
    move-result-object v6

    .line 398
    iget v6, v6, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o〇:F

    .line 399
    .line 400
    iget-object v10, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 401
    .line 402
    invoke-static {v10}, Lcom/intsig/camscanner/capture/DraftView;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/DraftView;)Lcom/intsig/camscanner/capture/DraftView$ClippedImage;

    .line 403
    .line 404
    .line 405
    move-result-object v10

    .line 406
    iget v10, v10, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->O8:F

    .line 407
    .line 408
    const/4 v11, 0x0

    .line 409
    invoke-virtual {v2, v0, v6, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 410
    .line 411
    .line 412
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 413
    .line 414
    iget-object v0, v0, Lcom/intsig/camscanner/capture/DraftView;->O〇08oOOO0:Landroid/graphics/RectF;

    .line 415
    .line 416
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 417
    .line 418
    .line 419
    move-result v0

    .line 420
    div-float/2addr v0, v5

    .line 421
    sub-float/2addr v7, v0

    .line 422
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 423
    .line 424
    iget-object v0, v0, Lcom/intsig/camscanner/capture/DraftView;->O〇08oOOO0:Landroid/graphics/RectF;

    .line 425
    .line 426
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 427
    .line 428
    .line 429
    move-result v0

    .line 430
    div-float/2addr v0, v5

    .line 431
    sub-float/2addr v8, v0

    .line 432
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 433
    .line 434
    iget-object v0, v0, Lcom/intsig/camscanner/capture/DraftView;->O〇08oOOO0:Landroid/graphics/RectF;

    .line 435
    .line 436
    invoke-virtual {v0, v7, v8}, Landroid/graphics/RectF;->offsetTo(FF)V

    .line 437
    .line 438
    .line 439
    invoke-virtual {v2, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 440
    .line 441
    .line 442
    goto :goto_4

    .line 443
    :catch_1
    move-exception v0

    .line 444
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 445
    .line 446
    .line 447
    :cond_9
    :goto_4
    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 448
    .line 449
    .line 450
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 451
    .line 452
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->Oooo8o0〇(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Path;

    .line 453
    .line 454
    .line 455
    move-result-object v0

    .line 456
    invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z

    .line 457
    .line 458
    .line 459
    move-result v0

    .line 460
    if-nez v0, :cond_a

    .line 461
    .line 462
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 463
    .line 464
    const v3, 0x6619bc9c

    .line 465
    .line 466
    .line 467
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 468
    .line 469
    .line 470
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 471
    .line 472
    const/4 v3, 0x1

    .line 473
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 474
    .line 475
    .line 476
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 477
    .line 478
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 479
    .line 480
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 481
    .line 482
    .line 483
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 484
    .line 485
    iget-object v3, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 486
    .line 487
    invoke-static {v3}, Lcom/intsig/camscanner/capture/DraftView;->〇O〇(Lcom/intsig/camscanner/capture/DraftView;)F

    .line 488
    .line 489
    .line 490
    move-result v3

    .line 491
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 492
    .line 493
    .line 494
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 495
    .line 496
    sget-object v3, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    .line 497
    .line 498
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 499
    .line 500
    .line 501
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 502
    .line 503
    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    .line 504
    .line 505
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 506
    .line 507
    .line 508
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 509
    .line 510
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->Oooo8o0〇(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Path;

    .line 511
    .line 512
    .line 513
    move-result-object v0

    .line 514
    iget-object v3, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇080:Landroid/graphics/Paint;

    .line 515
    .line 516
    invoke-virtual {v2, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 517
    .line 518
    .line 519
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 520
    .line 521
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇〇888(Lcom/intsig/camscanner/capture/DraftView;)Z

    .line 522
    .line 523
    .line 524
    move-result v0

    .line 525
    if-eqz v0, :cond_a

    .line 526
    .line 527
    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    .line 528
    .line 529
    .line 530
    move-result v0

    .line 531
    iget-object v3, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 532
    .line 533
    invoke-static {v3}, Lcom/intsig/camscanner/capture/DraftView;->Oooo8o0〇(Lcom/intsig/camscanner/capture/DraftView;)Landroid/graphics/Path;

    .line 534
    .line 535
    .line 536
    move-result-object v3

    .line 537
    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 538
    .line 539
    .line 540
    const v3, -0x55666601

    .line 541
    .line 542
    .line 543
    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 544
    .line 545
    .line 546
    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 547
    .line 548
    .line 549
    :cond_a
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 550
    .line 551
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/DraftView;)Lcom/intsig/camscanner/capture/DraftView$ClippedImage;

    .line 552
    .line 553
    .line 554
    move-result-object v0

    .line 555
    iget-boolean v0, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇080:Z

    .line 556
    .line 557
    if-eqz v0, :cond_b

    .line 558
    .line 559
    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    .line 560
    .line 561
    .line 562
    move-result v0

    .line 563
    iget-object v3, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 564
    .line 565
    invoke-static {v3}, Lcom/intsig/camscanner/capture/DraftView;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/DraftView;)Lcom/intsig/camscanner/capture/DraftView$ClippedImage;

    .line 566
    .line 567
    .line 568
    move-result-object v3

    .line 569
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o〇()[F

    .line 570
    .line 571
    .line 572
    move-result-object v3

    .line 573
    const/4 v4, 0x4

    .line 574
    aget v4, v3, v4

    .line 575
    .line 576
    iget-object v6, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 577
    .line 578
    iget-object v6, v6, Lcom/intsig/camscanner/capture/DraftView;->O〇08oOOO0:Landroid/graphics/RectF;

    .line 579
    .line 580
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    .line 581
    .line 582
    .line 583
    move-result v6

    .line 584
    div-float/2addr v6, v5

    .line 585
    sub-float/2addr v4, v6

    .line 586
    const/4 v6, 0x5

    .line 587
    aget v3, v3, v6

    .line 588
    .line 589
    iget-object v6, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 590
    .line 591
    iget-object v6, v6, Lcom/intsig/camscanner/capture/DraftView;->O〇08oOOO0:Landroid/graphics/RectF;

    .line 592
    .line 593
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    .line 594
    .line 595
    .line 596
    move-result v6

    .line 597
    div-float/2addr v6, v5

    .line 598
    sub-float/2addr v3, v6

    .line 599
    iget-object v5, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 600
    .line 601
    iget-object v5, v5, Lcom/intsig/camscanner/capture/DraftView;->〇00O0:Landroid/graphics/Bitmap;

    .line 602
    .line 603
    const/4 v6, 0x0

    .line 604
    invoke-virtual {v2, v5, v4, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 605
    .line 606
    .line 607
    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 608
    .line 609
    .line 610
    :cond_b
    iget-object v0, v1, Lcom/intsig/camscanner/capture/DraftView$DrawHandler;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/DraftView;

    .line 611
    .line 612
    invoke-static {v0}, Lcom/intsig/camscanner/capture/DraftView;->o〇0(Lcom/intsig/camscanner/capture/DraftView;)Landroid/view/SurfaceHolder;

    .line 613
    .line 614
    .line 615
    move-result-object v0

    .line 616
    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 617
    .line 618
    .line 619
    return-void
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
.end method

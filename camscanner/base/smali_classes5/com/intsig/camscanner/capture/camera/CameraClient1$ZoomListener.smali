.class final Lcom/intsig/camscanner/capture/camera/CameraClient1$ZoomListener;
.super Ljava/lang/Object;
.source "CameraClient1.kt"

# interfaces
.implements Landroid/hardware/Camera$OnZoomChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/camera/CameraClient1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ZoomListener"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/camscanner/capture/camera/CameraClient1;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/capture/camera/CameraClient1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/camera/CameraClient1$ZoomListener;->〇080:Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public onZoomChange(IZLandroid/hardware/Camera;)V
    .locals 1
    .param p3    # Landroid/hardware/Camera;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "camera"

    .line 2
    .line 3
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p3, p0, Lcom/intsig/camscanner/capture/camera/CameraClient1$ZoomListener;->〇080:Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 7
    .line 8
    invoke-static {p3}, Lcom/intsig/camscanner/capture/camera/CameraClient1;->OOO(Lcom/intsig/camscanner/capture/camera/CameraClient1;)Landroid/hardware/Camera$Parameters;

    .line 9
    .line 10
    .line 11
    move-result-object p3

    .line 12
    if-eqz p3, :cond_2

    .line 13
    .line 14
    if-ltz p1, :cond_2

    .line 15
    .line 16
    iget-object p3, p0, Lcom/intsig/camscanner/capture/camera/CameraClient1$ZoomListener;->〇080:Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 17
    .line 18
    invoke-static {p3}, Lcom/intsig/camscanner/capture/camera/CameraClient1;->OOO(Lcom/intsig/camscanner/capture/camera/CameraClient1;)Landroid/hardware/Camera$Parameters;

    .line 19
    .line 20
    .line 21
    move-result-object p3

    .line 22
    if-eqz p3, :cond_0

    .line 23
    .line 24
    invoke-virtual {p3}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    .line 25
    .line 26
    .line 27
    move-result p3

    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/4 p3, 0x0

    .line 30
    :goto_0
    if-gt p1, p3, :cond_2

    .line 31
    .line 32
    iget-object p3, p0, Lcom/intsig/camscanner/capture/camera/CameraClient1$ZoomListener;->〇080:Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 33
    .line 34
    invoke-static {p3}, Lcom/intsig/camscanner/capture/camera/CameraClient1;->OOO(Lcom/intsig/camscanner/capture/camera/CameraClient1;)Landroid/hardware/Camera$Parameters;

    .line 35
    .line 36
    .line 37
    move-result-object p3

    .line 38
    if-nez p3, :cond_1

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_1
    invoke-virtual {p3, p1}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    .line 42
    .line 43
    .line 44
    :goto_1
    iget-object p3, p0, Lcom/intsig/camscanner/capture/camera/CameraClient1$ZoomListener;->〇080:Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 45
    .line 46
    invoke-static {p3}, Lcom/intsig/camscanner/capture/camera/CameraClient1;->O000(Lcom/intsig/camscanner/capture/camera/CameraClient1;)Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 47
    .line 48
    .line 49
    move-result-object p3

    .line 50
    int-to-float v0, p1

    .line 51
    invoke-virtual {p3, v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇O〇(F)V

    .line 52
    .line 53
    .line 54
    :cond_2
    iget-object p3, p0, Lcom/intsig/camscanner/capture/camera/CameraClient1$ZoomListener;->〇080:Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 55
    .line 56
    invoke-static {p3}, Lcom/intsig/camscanner/capture/camera/CameraClient1;->〇80(Lcom/intsig/camscanner/capture/camera/CameraClient1;)Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 57
    .line 58
    .line 59
    move-result-object p3

    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraClient1$ZoomListener;->〇080:Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 61
    .line 62
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/camera/CameraClient1;->getMaxZoom()I

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    div-int/2addr p1, v0

    .line 67
    mul-int/lit8 p1, p1, 0x63

    .line 68
    .line 69
    invoke-interface {p3, p1, p2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;->o8〇OO(IZ)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

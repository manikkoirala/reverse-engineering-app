.class public final Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;
.super Ljava/lang/Object;
.source "CameraAdapterClient.kt"

# interfaces
.implements Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/camera/CameraAdapterClient$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇〇888:Lcom/intsig/camscanner/capture/camera/CameraAdapterClient$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Landroid/media/MediaActionSound;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇0:Z

.field private final 〇080:Lkotlinx/coroutines/CoroutineScope;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camera/CameraView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇〇888:Lcom/intsig/camscanner/capture/camera/CameraAdapterClient$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Lkotlinx/coroutines/CoroutineScope;Lcom/intsig/camera/CameraView;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;)V
    .locals 6
    .param p1    # Lkotlinx/coroutines/CoroutineScope;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camera/CameraView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "lifeScope"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "cameraView"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "contractView"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇080:Lkotlinx/coroutines/CoroutineScope;

    .line 20
    .line 21
    iput-object p2, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 22
    .line 23
    iput-object p3, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o〇:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 24
    .line 25
    new-instance p2, Landroid/media/MediaActionSound;

    .line 26
    .line 27
    invoke-direct {p2}, Landroid/media/MediaActionSound;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object p2, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->O8:Landroid/media/MediaActionSound;

    .line 31
    .line 32
    new-instance p2, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 33
    .line 34
    invoke-direct {p2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;-><init>()V

    .line 35
    .line 36
    .line 37
    iput-object p2, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 38
    .line 39
    const/high16 p3, 0x3f800000    # 1.0f

    .line 40
    .line 41
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇O〇(F)V

    .line 42
    .line 43
    .line 44
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇080()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    const/4 v2, 0x0

    .line 49
    new-instance v3, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient$1;

    .line 50
    .line 51
    const/4 p2, 0x0

    .line 52
    invoke-direct {v3, p0, p2}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient$1;-><init>(Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;Lkotlin/coroutines/Continuation;)V

    .line 53
    .line 54
    .line 55
    const/4 v4, 0x2

    .line 56
    const/4 v5, 0x0

    .line 57
    move-object v0, p1

    .line 58
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 59
    .line 60
    .line 61
    const/4 p1, 0x1

    .line 62
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->o〇0:Z

    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private final O000()Ljava/lang/String;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "torch"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const-string v1, "off"

    .line 17
    .line 18
    :goto_0
    return-object v1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O8(Ljava/lang/String;)I
    .locals 2

    .line 1
    if-eqz p1, :cond_8

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0xddf

    .line 8
    .line 9
    if-eq v0, v1, :cond_6

    .line 10
    .line 11
    const v1, 0x1ad6f

    .line 12
    .line 13
    .line 14
    if-eq v0, v1, :cond_4

    .line 15
    .line 16
    const v1, 0x2dddaf

    .line 17
    .line 18
    .line 19
    if-eq v0, v1, :cond_2

    .line 20
    .line 21
    const v1, 0x696d3fc

    .line 22
    .line 23
    .line 24
    if-eq v0, v1, :cond_0

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const-string v0, "torch"

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    if-nez p1, :cond_1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    sget-object p1, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/google/android/camera/data/Flash$Companion;->Oo08()I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    goto :goto_1

    .line 43
    :cond_2
    const-string v0, "auto"

    .line 44
    .line 45
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    if-nez p1, :cond_3

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_3
    sget-object p1, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/google/android/camera/data/Flash$Companion;->〇080()I

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    goto :goto_1

    .line 59
    :cond_4
    const-string v0, "off"

    .line 60
    .line 61
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    move-result p1

    .line 65
    if-nez p1, :cond_5

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_5
    sget-object p1, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 69
    .line 70
    invoke-virtual {p1}, Lcom/google/android/camera/data/Flash$Companion;->〇o〇()I

    .line 71
    .line 72
    .line 73
    move-result p1

    .line 74
    goto :goto_1

    .line 75
    :cond_6
    const-string v0, "on"

    .line 76
    .line 77
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    move-result p1

    .line 81
    if-nez p1, :cond_7

    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_7
    sget-object p1, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 85
    .line 86
    invoke-virtual {p1}, Lcom/google/android/camera/data/Flash$Companion;->O8()I

    .line 87
    .line 88
    .line 89
    move-result p1

    .line 90
    goto :goto_1

    .line 91
    :cond_8
    :goto_0
    sget-object p1, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/google/android/camera/data/Flash$Companion;->〇080()I

    .line 94
    .line 95
    .line 96
    move-result p1

    .line 97
    :goto_1
    return p1
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final oO00OOO(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;)Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->O000()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇8o8o〇(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oooo8o0〇(Lcom/intsig/camscanner/util/PremiumParcelSize;)V

    .line 18
    .line 19
    .line 20
    const/high16 v1, 0x3f800000    # 1.0f

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇O〇(F)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->OO0o〇〇(Lcom/intsig/camscanner/util/PremiumParcelSize;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888()Z

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->OO0o〇〇〇〇0(Z)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80()Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇808〇(Z)V

    .line 44
    .line 45
    .line 46
    return-object v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final 〇80(Landroid/content/Context;)V
    .locals 5

    .line 1
    const-string v0, "CameraAdapterClient"

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    const-string v1, "initPictureSizeSetting>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->o〇〇0〇()Ljava/util/ArrayList;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-static {p1, v1}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->OO0o〇〇〇〇0(Landroid/content/Context;Ljava/util/ArrayList;)I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    invoke-static {p1, v1, v2}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->〇8o8o〇(Landroid/content/Context;Ljava/util/List;I)I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o〇:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 23
    .line 24
    invoke-interface {v2, p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;->OO〇00〇8oO(I)V

    .line 25
    .line 26
    .line 27
    if-ltz p1, :cond_0

    .line 28
    .line 29
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    if-ge p1, v2, :cond_0

    .line 34
    .line 35
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    const-string v1, "list[selectPos]"

    .line 40
    .line 41
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    check-cast p1, Lcom/intsig/camscanner/util/ParcelSize;

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 47
    .line 48
    new-instance v2, Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 49
    .line 50
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 51
    .line 52
    .line 53
    move-result v3

    .line 54
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    invoke-direct {v2, v3, v4}, Lcom/intsig/camscanner/util/PremiumParcelSize;-><init>(II)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->OO0o〇〇(Lcom/intsig/camscanner/util/PremiumParcelSize;)V

    .line 62
    .line 63
    .line 64
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o〇:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 65
    .line 66
    iget-object v2, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 67
    .line 68
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;->o08〇〇0O(Lcom/intsig/camscanner/util/PremiumParcelSize;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 80
    .line 81
    .line 82
    move-result v2

    .line 83
    invoke-virtual {p0, v1, v2}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oooo8o0〇(II)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 87
    .line 88
    .line 89
    move-result v1

    .line 90
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 91
    .line 92
    .line 93
    move-result p1

    .line 94
    new-instance v2, Ljava/lang/StringBuilder;

    .line 95
    .line 96
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    .line 98
    .line 99
    const-string v3, "optimalPictureSize current"

    .line 100
    .line 101
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    const-string v1, ","

    .line 108
    .line 109
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    goto :goto_0

    .line 123
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 124
    .line 125
    .line 126
    move-result v1

    .line 127
    new-instance v2, Ljava/lang/StringBuilder;

    .line 128
    .line 129
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 130
    .line 131
    .line 132
    const-string v3, "optimalPictureSize selectPos="

    .line 133
    .line 134
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    const-string p1, ", list size="

    .line 141
    .line 142
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 146
    .line 147
    .line 148
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object p1

    .line 152
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    :goto_0
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 156
    .line 157
    goto :goto_1

    .line 158
    :cond_1
    const/4 p1, 0x0

    .line 159
    :goto_1
    if-nez p1, :cond_2

    .line 160
    .line 161
    const-string p1, "initPictureSizeSetting --- but context is null"

    .line 162
    .line 163
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    :cond_2
    return-void
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method private final 〇O〇80o08O(Lcom/intsig/camscanner/util/PremiumParcelSize;)Lcom/google/android/camera/size/CameraSize;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-lez v0, :cond_1

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-gtz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    new-instance v0, Lcom/google/android/camera/size/CameraSize;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    invoke-direct {v0, v1, p1}, Lcom/google/android/camera/size/CameraSize;-><init>(II)V

    .line 25
    .line 26
    .line 27
    return-object v0

    .line 28
    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 29
    return-object p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;)Landroid/media/MediaActionSound;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->O8:Landroid/media/MediaActionSound;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public O08000()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o〇:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;->getActivityContext()Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v1, "CameraAdapterClient"

    .line 10
    .line 11
    const-string v2, "initPreviewParam"

    .line 12
    .line 13
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->o〇0:Z

    .line 17
    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    iput-boolean v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->o〇0:Z

    .line 22
    .line 23
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇80(Landroid/content/Context;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    const/4 v0, 0x1

    .line 27
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O8ooOoo〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->o〇〇0〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O8〇o()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/util/PremiumParcelSize;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/util/CameraUtil;->〇080:Lcom/intsig/camscanner/capture/util/CameraUtil;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->o〇〇0〇()Ljava/util/ArrayList;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->o〇O()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/capture/util/CameraUtil;->O8(Ljava/util/ArrayList;Lcom/intsig/camscanner/util/PremiumParcelSize;)Ljava/util/ArrayList;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OO0o〇〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->〇00〇8()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OO0o〇〇〇〇0()Landroid/view/View;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OOO〇O0(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->OO0o〇〇〇〇0(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public Oo08()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->oo〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo8Oo00oo()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->Oo8Oo00oo()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OoO8(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camera/CameraView;->OoO8(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final Ooo()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->getCameraApi()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    sget-object v1, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/google/android/camera/data/CameraApi$Companion;->〇080()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eq v0, v1, :cond_0

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication;->oo〇()Lkotlinx/coroutines/CoroutineScope;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇080()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    const/4 v3, 0x0

    .line 38
    new-instance v4, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient$playShutterSound$1;

    .line 39
    .line 40
    const/4 v0, 0x0

    .line 41
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient$playShutterSound$1;-><init>(Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;Lkotlin/coroutines/Continuation;)V

    .line 42
    .line 43
    .line 44
    const/4 v5, 0x2

    .line 45
    const/4 v6, 0x0

    .line 46
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 47
    .line 48
    .line 49
    :cond_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public Oooo8o0〇(II)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 4
    .line 5
    invoke-direct {v1, p1, p2}, Lcom/intsig/camscanner/util/PremiumParcelSize;-><init>(II)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->OO0o〇〇(Lcom/intsig/camscanner/util/PremiumParcelSize;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o〇:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 12
    .line 13
    int-to-double v1, p1

    .line 14
    int-to-double v3, p2

    .line 15
    div-double/2addr v1, v3

    .line 16
    invoke-interface {v0, v1, v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;->o〇o〇Oo88(D)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 20
    .line 21
    new-instance v1, Lcom/google/android/camera/size/CameraSize;

    .line 22
    .line 23
    invoke-direct {v1, p1, p2}, Lcom/google/android/camera/size/CameraSize;-><init>(II)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/camera/CameraView;->setPictureSize(Lcom/google/android/camera/size/CameraSize;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O〇8O8〇008(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-static {p1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 13
    :goto_1
    if-eqz v0, :cond_2

    .line 14
    .line 15
    return-object p1

    .line 16
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_3

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 29
    .line 30
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇8o8o〇(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    const/16 v1, 0xddf

    .line 38
    .line 39
    if-eq v0, v1, :cond_a

    .line 40
    .line 41
    const v1, 0x1ad6f

    .line 42
    .line 43
    .line 44
    if-eq v0, v1, :cond_8

    .line 45
    .line 46
    const v1, 0x2dddaf

    .line 47
    .line 48
    .line 49
    if-eq v0, v1, :cond_6

    .line 50
    .line 51
    const v1, 0x696d3fc

    .line 52
    .line 53
    .line 54
    if-eq v0, v1, :cond_4

    .line 55
    .line 56
    goto :goto_2

    .line 57
    :cond_4
    const-string v0, "torch"

    .line 58
    .line 59
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    if-nez v0, :cond_5

    .line 64
    .line 65
    goto :goto_2

    .line 66
    :cond_5
    sget-object v0, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 67
    .line 68
    invoke-virtual {v0}, Lcom/google/android/camera/data/Flash$Companion;->Oo08()I

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    goto :goto_3

    .line 77
    :cond_6
    const-string v0, "auto"

    .line 78
    .line 79
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    if-nez v0, :cond_7

    .line 84
    .line 85
    goto :goto_2

    .line 86
    :cond_7
    sget-object v0, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 87
    .line 88
    invoke-virtual {v0}, Lcom/google/android/camera/data/Flash$Companion;->〇080()I

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    goto :goto_3

    .line 97
    :cond_8
    const-string v0, "off"

    .line 98
    .line 99
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 100
    .line 101
    .line 102
    move-result v0

    .line 103
    if-nez v0, :cond_9

    .line 104
    .line 105
    goto :goto_2

    .line 106
    :cond_9
    sget-object v0, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 107
    .line 108
    invoke-virtual {v0}, Lcom/google/android/camera/data/Flash$Companion;->〇o〇()I

    .line 109
    .line 110
    .line 111
    move-result v0

    .line 112
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    goto :goto_3

    .line 117
    :cond_a
    const-string v0, "on"

    .line 118
    .line 119
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 120
    .line 121
    .line 122
    move-result v0

    .line 123
    if-nez v0, :cond_b

    .line 124
    .line 125
    :goto_2
    const/4 v0, 0x0

    .line 126
    goto :goto_3

    .line 127
    :cond_b
    sget-object v0, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 128
    .line 129
    invoke-virtual {v0}, Lcom/google/android/camera/data/Flash$Companion;->O8()I

    .line 130
    .line 131
    .line 132
    move-result v0

    .line 133
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 134
    .line 135
    .line 136
    move-result-object v0

    .line 137
    :goto_3
    if-eqz v0, :cond_c

    .line 138
    .line 139
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 140
    .line 141
    .line 142
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 143
    .line 144
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 145
    .line 146
    .line 147
    move-result v0

    .line 148
    invoke-virtual {v1, v0}, Lcom/intsig/camera/CameraView;->setFlash(I)V

    .line 149
    .line 150
    .line 151
    :cond_c
    return-object p1
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public O〇O〇oO()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->O8ooOoo〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getMaxZoom()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->getMaxZoom()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    float-to-int v0, v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getMinZoom()F
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/capture/capturemode/CaptureModePreferenceHelper;->O8()I

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->getMinZoom()F

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "getMinZoom1\uff1a "

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v1, "CameraAdapterClient"

    .line 28
    .line 29
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->getMinZoom()F

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    return v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public o0ooO()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o8()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o800o8O()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->getFlash()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    sget-object v1, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/google/android/camera/data/Flash$Companion;->Oo08()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-ne v0, v1, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o8oO〇()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->o〇8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->o0ooO()V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 13
    .line 14
    const-string v1, "continuous-picture"

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇O8o08O(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oO()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camera/CameraView;->setAutoFocus(Z)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oO80()V
    .locals 2

    .line 1
    const-string v0, "CameraAdapterClient"

    .line 2
    .line 3
    const-string v1, "closeCamera"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oo88o8O(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camera/CameraView;->〇o00〇〇Oo(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public oo〇(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camera/CameraView;->setDisplayOrientation(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o〇0()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->O8〇o()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇0OOo〇0()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->oO00OOO(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;)Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->OOO〇O0()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇8oOO88(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;)V
    .locals 8
    .param p1    # Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "model"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    const-string v1, "setAndActivateModel -- "

    .line 9
    .line 10
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    iget-object v2, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 18
    .line 19
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    const-string v2, "; "

    .line 28
    .line 29
    const-string v3, " -> "

    .line 30
    .line 31
    if-nez v1, :cond_0

    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    new-instance v5, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v6, "1.flashMode "

    .line 49
    .line 50
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v4

    .line 78
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇8o8o〇(Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇080()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    iget-object v4, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 86
    .line 87
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇080()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 88
    .line 89
    .line 90
    move-result-object v4

    .line 91
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    if-nez v1, :cond_1

    .line 96
    .line 97
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 98
    .line 99
    new-instance v4, Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 100
    .line 101
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇080()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 102
    .line 103
    .line 104
    move-result-object v5

    .line 105
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 106
    .line 107
    .line 108
    move-result v5

    .line 109
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇080()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 110
    .line 111
    .line 112
    move-result-object v6

    .line 113
    invoke-virtual {v6}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 114
    .line 115
    .line 116
    move-result v6

    .line 117
    invoke-direct {v4, v5, v6}, Lcom/intsig/camscanner/util/PremiumParcelSize;-><init>(II)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇80〇808〇O(Lcom/intsig/camscanner/util/PremiumParcelSize;)V

    .line 121
    .line 122
    .line 123
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 124
    .line 125
    .line 126
    move-result-object v1

    .line 127
    iget-object v4, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 128
    .line 129
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 130
    .line 131
    .line 132
    move-result-object v4

    .line 133
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 134
    .line 135
    .line 136
    move-result v1

    .line 137
    if-nez v1, :cond_2

    .line 138
    .line 139
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 140
    .line 141
    .line 142
    move-result-object v1

    .line 143
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->〇080()Z

    .line 144
    .line 145
    .line 146
    move-result v1

    .line 147
    if-nez v1, :cond_3

    .line 148
    .line 149
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 150
    .line 151
    .line 152
    move-result-object v1

    .line 153
    iget-object v4, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 154
    .line 155
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 156
    .line 157
    .line 158
    move-result-object v4

    .line 159
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 160
    .line 161
    .line 162
    move-result v1

    .line 163
    if-nez v1, :cond_5

    .line 164
    .line 165
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 166
    .line 167
    .line 168
    move-result-object v1

    .line 169
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->〇080()Z

    .line 170
    .line 171
    .line 172
    move-result v1

    .line 173
    if-eqz v1, :cond_5

    .line 174
    .line 175
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 176
    .line 177
    .line 178
    move-result-object v1

    .line 179
    iget-object v4, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 180
    .line 181
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 182
    .line 183
    .line 184
    move-result-object v4

    .line 185
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 186
    .line 187
    .line 188
    move-result v1

    .line 189
    if-nez v1, :cond_4

    .line 190
    .line 191
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 192
    .line 193
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 194
    .line 195
    .line 196
    move-result-object v1

    .line 197
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 198
    .line 199
    .line 200
    move-result-object v4

    .line 201
    new-instance v5, Ljava/lang/StringBuilder;

    .line 202
    .line 203
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 204
    .line 205
    .line 206
    const-string v6, "2.previewSz "

    .line 207
    .line 208
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    .line 210
    .line 211
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    .line 216
    .line 217
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 218
    .line 219
    .line 220
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    .line 222
    .line 223
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 224
    .line 225
    .line 226
    move-result-object v1

    .line 227
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    .line 229
    .line 230
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 231
    .line 232
    new-instance v4, Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 233
    .line 234
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 235
    .line 236
    .line 237
    move-result-object v5

    .line 238
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 239
    .line 240
    .line 241
    move-result v5

    .line 242
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 243
    .line 244
    .line 245
    move-result-object v6

    .line 246
    invoke-virtual {v6}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 247
    .line 248
    .line 249
    move-result v6

    .line 250
    invoke-direct {v4, v5, v6}, Lcom/intsig/camscanner/util/PremiumParcelSize;-><init>(II)V

    .line 251
    .line 252
    .line 253
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oooo8o0〇(Lcom/intsig/camscanner/util/PremiumParcelSize;)V

    .line 254
    .line 255
    .line 256
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 257
    .line 258
    .line 259
    move-result-object v1

    .line 260
    iget-object v4, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 261
    .line 262
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 263
    .line 264
    .line 265
    move-result-object v4

    .line 266
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 267
    .line 268
    .line 269
    move-result v1

    .line 270
    if-nez v1, :cond_5

    .line 271
    .line 272
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 273
    .line 274
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 275
    .line 276
    .line 277
    move-result-object v1

    .line 278
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 279
    .line 280
    .line 281
    move-result-object v4

    .line 282
    new-instance v5, Ljava/lang/StringBuilder;

    .line 283
    .line 284
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 285
    .line 286
    .line 287
    const-string v6, "3.pictureSz "

    .line 288
    .line 289
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    .line 291
    .line 292
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 293
    .line 294
    .line 295
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    .line 297
    .line 298
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 299
    .line 300
    .line 301
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    .line 303
    .line 304
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 305
    .line 306
    .line 307
    move-result-object v1

    .line 308
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    .line 310
    .line 311
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 312
    .line 313
    new-instance v4, Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 314
    .line 315
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 316
    .line 317
    .line 318
    move-result-object v5

    .line 319
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 320
    .line 321
    .line 322
    move-result v5

    .line 323
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 324
    .line 325
    .line 326
    move-result-object v6

    .line 327
    invoke-virtual {v6}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 328
    .line 329
    .line 330
    move-result v6

    .line 331
    invoke-direct {v4, v5, v6}, Lcom/intsig/camscanner/util/PremiumParcelSize;-><init>(II)V

    .line 332
    .line 333
    .line 334
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->OO0o〇〇(Lcom/intsig/camscanner/util/PremiumParcelSize;)V

    .line 335
    .line 336
    .line 337
    :cond_5
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0()F

    .line 338
    .line 339
    .line 340
    move-result v1

    .line 341
    iget-object v4, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 342
    .line 343
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0()F

    .line 344
    .line 345
    .line 346
    move-result v4

    .line 347
    const/4 v5, 0x1

    .line 348
    cmpg-float v1, v1, v4

    .line 349
    .line 350
    if-nez v1, :cond_6

    .line 351
    .line 352
    const/4 v1, 0x1

    .line 353
    goto :goto_0

    .line 354
    :cond_6
    const/4 v1, 0x0

    .line 355
    :goto_0
    if-nez v1, :cond_7

    .line 356
    .line 357
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 358
    .line 359
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0()F

    .line 360
    .line 361
    .line 362
    move-result v1

    .line 363
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0()F

    .line 364
    .line 365
    .line 366
    move-result v4

    .line 367
    new-instance v6, Ljava/lang/StringBuilder;

    .line 368
    .line 369
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 370
    .line 371
    .line 372
    const-string v7, "4.zoomValue "

    .line 373
    .line 374
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    .line 376
    .line 377
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 378
    .line 379
    .line 380
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    .line 382
    .line 383
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 384
    .line 385
    .line 386
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 387
    .line 388
    .line 389
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 390
    .line 391
    .line 392
    move-result-object v1

    .line 393
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    .line 395
    .line 396
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 397
    .line 398
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0()F

    .line 399
    .line 400
    .line 401
    move-result v4

    .line 402
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇O〇(F)V

    .line 403
    .line 404
    .line 405
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0()F

    .line 406
    .line 407
    .line 408
    move-result v1

    .line 409
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->getMaxZoom()I

    .line 410
    .line 411
    .line 412
    move-result v4

    .line 413
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->getMinZoom()F

    .line 414
    .line 415
    .line 416
    move-result v6

    .line 417
    invoke-static {v1, v4, v6}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->〇oo〇(FIF)I

    .line 418
    .line 419
    .line 420
    move-result v1

    .line 421
    iget-object v4, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o〇:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 422
    .line 423
    invoke-interface {v4, v1, v5}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;->o8〇OO(IZ)V

    .line 424
    .line 425
    .line 426
    :cond_7
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888()Z

    .line 427
    .line 428
    .line 429
    move-result v1

    .line 430
    iget-object v4, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 431
    .line 432
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888()Z

    .line 433
    .line 434
    .line 435
    move-result v4

    .line 436
    if-eq v1, v4, :cond_8

    .line 437
    .line 438
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 439
    .line 440
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888()Z

    .line 441
    .line 442
    .line 443
    move-result v1

    .line 444
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888()Z

    .line 445
    .line 446
    .line 447
    move-result v4

    .line 448
    new-instance v5, Ljava/lang/StringBuilder;

    .line 449
    .line 450
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 451
    .line 452
    .line 453
    const-string v6, "5.previewing "

    .line 454
    .line 455
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 456
    .line 457
    .line 458
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 459
    .line 460
    .line 461
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462
    .line 463
    .line 464
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 465
    .line 466
    .line 467
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    .line 469
    .line 470
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 471
    .line 472
    .line 473
    move-result-object v1

    .line 474
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475
    .line 476
    .line 477
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 478
    .line 479
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888()Z

    .line 480
    .line 481
    .line 482
    move-result v4

    .line 483
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->OO0o〇〇〇〇0(Z)V

    .line 484
    .line 485
    .line 486
    :cond_8
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80()Z

    .line 487
    .line 488
    .line 489
    move-result v1

    .line 490
    iget-object v4, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 491
    .line 492
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80()Z

    .line 493
    .line 494
    .line 495
    move-result v4

    .line 496
    if-eq v1, v4, :cond_9

    .line 497
    .line 498
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 499
    .line 500
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80()Z

    .line 501
    .line 502
    .line 503
    move-result v1

    .line 504
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80()Z

    .line 505
    .line 506
    .line 507
    move-result v4

    .line 508
    new-instance v5, Ljava/lang/StringBuilder;

    .line 509
    .line 510
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 511
    .line 512
    .line 513
    const-string v6, "6.sound "

    .line 514
    .line 515
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    .line 517
    .line 518
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 519
    .line 520
    .line 521
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 522
    .line 523
    .line 524
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 525
    .line 526
    .line 527
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 528
    .line 529
    .line 530
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 531
    .line 532
    .line 533
    move-result-object v1

    .line 534
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 535
    .line 536
    .line 537
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 538
    .line 539
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80()Z

    .line 540
    .line 541
    .line 542
    move-result p1

    .line 543
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇808〇(Z)V

    .line 544
    .line 545
    .line 546
    :cond_9
    const-string p1, "CameraAdapterClient"

    .line 547
    .line 548
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 549
    .line 550
    .line 551
    move-result-object v0

    .line 552
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    .line 554
    .line 555
    iget-object p1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 556
    .line 557
    new-instance v7, Lcom/google/android/camera/data/CameraModel;

    .line 558
    .line 559
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 560
    .line 561
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo()Ljava/lang/String;

    .line 562
    .line 563
    .line 564
    move-result-object v0

    .line 565
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->O8(Ljava/lang/String;)I

    .line 566
    .line 567
    .line 568
    move-result v1

    .line 569
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 570
    .line 571
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0()F

    .line 572
    .line 573
    .line 574
    move-result v2

    .line 575
    const/4 v3, 0x0

    .line 576
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 577
    .line 578
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8()Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 579
    .line 580
    .line 581
    move-result-object v0

    .line 582
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇O〇80o08O(Lcom/intsig/camscanner/util/PremiumParcelSize;)Lcom/google/android/camera/size/CameraSize;

    .line 583
    .line 584
    .line 585
    move-result-object v4

    .line 586
    const/4 v5, 0x4

    .line 587
    const/4 v6, 0x0

    .line 588
    move-object v0, v7

    .line 589
    invoke-direct/range {v0 .. v6}, Lcom/google/android/camera/data/CameraModel;-><init>(IFLjava/lang/Boolean;Lcom/google/android/camera/size/CameraSize;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 590
    .line 591
    .line 592
    invoke-virtual {p1, v7}, Lcom/intsig/camera/CameraView;->setCameraModel(Lcom/google/android/camera/data/CameraModel;)V

    .line 593
    .line 594
    .line 595
    return-void
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
.end method

.method public o〇O()Lcom/intsig/camscanner/util/PremiumParcelSize;
    .locals 6

    .line 1
    const-string v0, "getDefaultSize>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

    .line 2
    .line 3
    const-string v1, "CameraAdapterClient"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->o〇〇0〇()Ljava/util/ArrayList;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    invoke-virtual {v2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-static {v2, v0}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->OO0o〇〇〇〇0(Landroid/content/Context;Ljava/util/ArrayList;)I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    invoke-static {v3, v0, v2}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->〇8o8o〇(Landroid/content/Context;Ljava/util/List;I)I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-ltz v2, :cond_0

    .line 31
    .line 32
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-ge v2, v3, :cond_0

    .line 37
    .line 38
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    const-string v2, "list[selectPos]"

    .line 43
    .line 44
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    check-cast v0, Lcom/intsig/camscanner/util/ParcelSize;

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    new-instance v4, Ljava/lang/StringBuilder;

    .line 58
    .line 59
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .line 61
    .line 62
    const-string v5, "getDefaultSize current"

    .line 63
    .line 64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    const-string v2, ","

    .line 71
    .line 72
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v2

    .line 82
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    new-instance v1, Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 86
    .line 87
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 92
    .line 93
    .line 94
    move-result v0

    .line 95
    invoke-direct {v1, v2, v0}, Lcom/intsig/camscanner/util/PremiumParcelSize;-><init>(II)V

    .line 96
    .line 97
    .line 98
    return-object v1

    .line 99
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 100
    .line 101
    .line 102
    move-result v0

    .line 103
    new-instance v3, Ljava/lang/StringBuilder;

    .line 104
    .line 105
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .line 107
    .line 108
    const-string v4, "getDefaultSize selectPos="

    .line 109
    .line 110
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    const-string v2, ", list size="

    .line 117
    .line 118
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    const/4 v0, 0x0

    .line 132
    return-object v0
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public o〇O8〇〇o(II)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇〇0〇()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/util/PremiumParcelSize;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/camera/CameraView;->getSupportedAllPictureSize()Lcom/google/android/camera/size/CameraSizeMap;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    invoke-virtual {v1}, Lcom/google/android/camera/size/CameraSizeMap;->〇〇888()Ljava/util/List;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    check-cast v1, Ljava/lang/Iterable;

    .line 21
    .line 22
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-eqz v2, :cond_0

    .line 31
    .line 32
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    check-cast v2, Lcom/google/android/camera/size/CameraSize;

    .line 37
    .line 38
    new-instance v3, Landroid/util/Size;

    .line 39
    .line 40
    invoke-virtual {v2}, Lcom/google/android/camera/size/CameraSize;->getWidth()I

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    invoke-virtual {v2}, Lcom/google/android/camera/size/CameraSize;->getHeight()I

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    invoke-direct {v3, v4, v2}, Landroid/util/Size;-><init>(II)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    new-instance v2, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    const-string v3, "getPictureSizeList size:"

    .line 65
    .line 66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    const-string v2, "CameraAdapterClient"

    .line 77
    .line 78
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    invoke-static {v0}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->Oo08(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    invoke-static {v0}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->Oooo8o0〇(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    return-object v0
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public resetZoom()V
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/capture/capturemode/CaptureModePreferenceHelper;->O8()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/high16 v2, 0x3f800000    # 1.0f

    .line 7
    .line 8
    const/4 v3, 0x1

    .line 9
    if-ne v0, v3, :cond_2

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->getMinZoom()F

    .line 14
    .line 15
    .line 16
    move-result v4

    .line 17
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇O〇(F)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->〇o()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->getMaxZoom()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    int-to-float v0, v0

    .line 30
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->getMinZoom()F

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    sub-float/2addr v0, v4

    .line 35
    const/4 v4, 0x0

    .line 36
    cmpg-float v0, v0, v4

    .line 37
    .line 38
    if-nez v0, :cond_0

    .line 39
    .line 40
    const/4 v1, 0x1

    .line 41
    :cond_0
    if-nez v1, :cond_1

    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->getMaxZoom()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    int-to-float v0, v0

    .line 48
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->getMinZoom()F

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    sub-float/2addr v0, v1

    .line 53
    div-float v4, v2, v0

    .line 54
    .line 55
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o〇:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 56
    .line 57
    const/16 v1, 0x63

    .line 58
    .line 59
    int-to-float v1, v1

    .line 60
    mul-float v4, v4, v1

    .line 61
    .line 62
    float-to-int v1, v4

    .line 63
    invoke-interface {v0, v1, v3}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;->o8〇OO(IZ)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 68
    .line 69
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇O〇(F)V

    .line 70
    .line 71
    .line 72
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 73
    .line 74
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->〇o()V

    .line 75
    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o〇:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 78
    .line 79
    invoke-interface {v0, v1, v3}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;->o8〇OO(IZ)V

    .line 80
    .line 81
    .line 82
    :goto_0
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇00()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->〇O00()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇0000OOO(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camera/CameraView;->o8(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇00〇8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->〇oOO8O8()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇080()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->〇0000OOO()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇08O8o〇0()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇0〇O0088o()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇8(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->getMaxZoom()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->getMinZoom()F

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-static {p1, v1, v2}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->oo88o8O(IIF)F

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇O〇(F)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0()F

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    new-instance v1, Ljava/lang/StringBuilder;

    .line 25
    .line 26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .line 28
    .line 29
    const-string v2, "setZoomValue - zoomValue="

    .line 30
    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string v2, " - zoomRate="

    .line 38
    .line 39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    const-string v1, "CameraAdapterClient"

    .line 50
    .line 51
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 55
    .line 56
    iget-object v1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 57
    .line 58
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0()F

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    invoke-virtual {v0, v1}, Lcom/intsig/camera/CameraView;->setZoomRatio(F)V

    .line 63
    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o〇:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 66
    .line 67
    const/4 v1, 0x1

    .line 68
    invoke-interface {v0, p1, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;->o8〇OO(IZ)V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇80〇808〇O()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇8o8o〇(Z)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->〇O888o0o()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x0

    .line 8
    if-nez v1, :cond_1

    .line 9
    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 p1, 0x0

    .line 14
    goto :goto_1

    .line 15
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 16
    :goto_1
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇808〇(Z)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    invoke-virtual {p1, v0}, Lcom/intsig/camera/CameraView;->o800o8O(Z)Z

    .line 28
    .line 29
    .line 30
    return v2
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇8〇0〇o〇O()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O00()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->〇o〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    xor-int/lit8 v0, v0, 0x1

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O888o0o()Ljava/lang/String;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->getCameraApi()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    sget-object v1, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/google/android/camera/data/CameraApi$Companion;->〇080()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    const-string v3, "adapter_camera1"

    .line 14
    .line 15
    if-ne v0, v2, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/camera/data/CameraApi$Companion;->〇o00〇〇Oo()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-ne v0, v2, :cond_1

    .line 23
    .line 24
    const-string v3, "adapter_camera2"

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/camera/data/CameraApi$Companion;->o〇0()I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-ne v0, v2, :cond_2

    .line 32
    .line 33
    const-string v3, "adapter_camerax"

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/camera/data/CameraApi$Companion;->〇o〇()I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    if-ne v0, v1, :cond_3

    .line 41
    .line 42
    const-string v3, "adapter_camerah"

    .line 43
    .line 44
    :cond_3
    :goto_0
    return-object v3
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇O8o08O()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    invoke-static {v0, v1, v2, v1}, Lcom/google/android/camera/ICamera$DefaultImpls;->o〇0(Lcom/google/android/camera/ICamera;Ljava/lang/Integer;ILjava/lang/Object;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O〇()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o00〇〇Oo()V
    .locals 2

    .line 1
    const-string v0, "CameraAdapterClient"

    .line 2
    .line 3
    const-string v1, "openCamera"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->o〇8()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇oOO8O8()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->〇O888o0o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    xor-int/lit8 v0, v0, 0x1

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇oo〇()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇0o()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->O〇8O8〇008()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇808〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->Oo08:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camera/CameraView;->〇o〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    xor-int/lit8 v0, v0, 0x1

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇8O0〇8(Landroid/view/SurfaceHolder;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇〇〇0〇〇0(IIIIII)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;->〇o00〇〇Oo:Lcom/intsig/camera/CameraView;

    .line 2
    .line 3
    int-to-float v1, p1

    .line 4
    int-to-float v2, p2

    .line 5
    move v3, p3

    .line 6
    move v4, p4

    .line 7
    move v5, p5

    .line 8
    move v6, p6

    .line 9
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/camera/CameraView;->〇〇〇0〇〇0(FFIIII)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
.end method

.class public abstract Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;
.super Ljava/lang/Object;
.source "CustomAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask$InnerLooperCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final Oo08:Ljava/util/concurrent/ExecutorService;

.field private static final o〇0:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private O8:Ljava/lang/String;

.field private 〇080:Ljava/util/concurrent/FutureTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/FutureTask<",
            "TResult;>;"
        }
    .end annotation
.end field

.field private 〇o00〇〇Oo:Landroid/os/Handler;

.field protected 〇o〇:Ljava/lang/Object;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TParams;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sput-object v0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->Oo08:Ljava/util/concurrent/ExecutorService;

    .line 6
    .line 7
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    sput-object v0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->o〇0:Ljava/util/concurrent/ExecutorService;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TParams;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->O8:Ljava/lang/String;

    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇o〇:Ljava/lang/Object;

    .line 5
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v1, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask$InnerLooperCallback;

    invoke-direct {v1, p0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask$InnerLooperCallback;-><init>(Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;)V

    invoke-direct {p1, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object p1, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇o00〇〇Oo:Landroid/os/Handler;

    return-void
.end method

.method private Oo08(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private oO80(Ljava/lang/Object;)Ljava/util/concurrent/FutureTask;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TParams;)",
            "Ljava/util/concurrent/FutureTask<",
            "TResult;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask$1;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask$1;-><init>(Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    new-instance p1, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask$2;

    .line 7
    .line 8
    invoke-direct {p1, p0, v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask$2;-><init>(Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;Ljava/util/concurrent/Callable;)V

    .line 9
    .line 10
    .line 11
    return-object p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->O8:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->Oo08(Ljava/lang/Runnable;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static 〇〇808〇(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->Oo08:Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    invoke-interface {v0, p0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public abstract O8(Ljava/lang/Object;)Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TParams;)TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public OO0o〇〇()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OO0o〇〇〇〇0()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oooo8o0〇(Ljava/lang/String;)Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->O8:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇o〇:Ljava/lang/Object;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->oO80(Ljava/lang/Object;)Ljava/util/concurrent/FutureTask;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇080:Ljava/util/concurrent/FutureTask;

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->OO0o〇〇()V

    .line 10
    .line 11
    .line 12
    sget-object v0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->Oo08:Ljava/util/concurrent/ExecutorService;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇080:Ljava/util/concurrent/FutureTask;

    .line 15
    .line 16
    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public abstract 〇80〇808〇O(Ljava/lang/Exception;)V
.end method

.method public 〇8o8o〇(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TProgress;)V"
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public abstract 〇O8o08O(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation
.end method

.method public 〇o〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇080:Ljava/util/concurrent/FutureTask;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇o〇:Ljava/lang/Object;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->oO80(Ljava/lang/Object;)Ljava/util/concurrent/FutureTask;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇080:Ljava/util/concurrent/FutureTask;

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->OO0o〇〇()V

    .line 10
    .line 11
    .line 12
    sget-object v0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->o〇0:Ljava/util/concurrent/ExecutorService;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇080:Ljava/util/concurrent/FutureTask;

    .line 15
    .line 16
    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

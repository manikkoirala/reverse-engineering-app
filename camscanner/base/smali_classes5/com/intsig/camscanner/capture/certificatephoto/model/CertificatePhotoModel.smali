.class public Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;
.super Ljava/lang/Object;
.source "CertificatePhotoModel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public OO:I

.field public o0:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public 〇08O〇00〇o:I

.field public 〇OOo8〇0:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->o0:I

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->o0:I

    .line 8
    iput p2, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->〇OOo8〇0:I

    .line 9
    iput p3, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->〇08O〇00〇o:I

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->o0:I

    .line 3
    iput p2, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->〇OOo8〇0:I

    .line 4
    iput p3, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->OO:I

    .line 5
    iput p4, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->〇08O〇00〇o:I

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->o0:I

    .line 14
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->〇OOo8〇0:I

    .line 15
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->OO:I

    .line 16
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->〇08O〇00〇o:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1
    iget p2, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->o0:I

    .line 2
    .line 3
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4
    .line 5
    .line 6
    iget p2, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->〇OOo8〇0:I

    .line 7
    .line 8
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 9
    .line 10
    .line 11
    iget p2, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->OO:I

    .line 12
    .line 13
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 14
    .line 15
    .line 16
    iget p2, p0, Lcom/intsig/camscanner/capture/certificatephoto/model/CertificatePhotoModel;->〇08O〇00〇o:I

    .line 17
    .line 18
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.class public final Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;
.super Landroidx/lifecycle/AndroidViewModel;
.source "CaptureRefactorViewModel.kt"

# interfaces
.implements Lcom/intsig/camscanner/capture/control/ICaptureControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;,
        Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;,
        Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$PersonalMold;,
        Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$TeamMold;,
        Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureModeMenuShownEntity;,
        Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CustomZoomControlListener;,
        Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$WhenMappings;,
        Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇0ooOOo:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

.field private O0〇0:Z

.field private final O88O:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureModeMenuShownEntity;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O8o08O8O:Lkotlinx/coroutines/flow/MutableSharedFlow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/flow/MutableSharedFlow<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O8o〇O0:Z

.field private O8〇o〇88:Lcom/intsig/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/callback/Callback<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private OO:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;

.field private OO〇OOo:Z

.field private Oo0O0o8:Z

.field private final Oo0〇Ooo:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public Oo80:Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;

.field private Ooo08:Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;

.field private O〇08oOOO0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O〇O:Z

.field private O〇o88o08〇:I

.field private o0:Landroid/content/Intent;

.field private o00〇88〇08:Lcom/intsig/camscanner/capture/ppt/PPTScaleCallback;

.field private o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

.field private o880:Z

.field private o8O:Z

.field private final o8o:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8oOOo:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO:Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO0〇0o:Ljava/lang/String;

.field private final oO00〇o:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOO0880O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOO8:Z

.field private final oOO〇〇:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOoO8OO〇:Lcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOoo80oO:Z

.field private oOo〇08〇:Z

.field private oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

.field private oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oO〇oo:Z

.field public oo8ooo8O:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

.field private ooO:Lcom/intsig/camscanner/util/PremiumParcelSize;

.field private ooo0〇〇O:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

.field private oooO888:Landroid/content/SharedPreferences;

.field private o〇00O:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇0〇o:Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇O8OO:Z

.field private o〇oO:I

.field private final o〇o〇Oo88:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇00O0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:Lkotlinx/coroutines/flow/SharedFlow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/flow/SharedFlow<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇088O:Z

.field private 〇08O〇00〇o:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08〇o0O:Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;

.field private 〇0O:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0〇0:Ljava/lang/String;

.field private 〇800OO〇0O:J

.field private 〇80O8o8O〇:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

.field private 〇8〇80o:Z

.field private 〇8〇OOoooo:Lcom/intsig/camscanner/capture/inputdata/CaptureSceneInputData;

.field private final 〇8〇o88:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/intsig/camscanner/capture/core/MoreSettingLayoutStatusListener;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8〇oO〇〇8o:Ljava/lang/String;

.field private 〇O8oOo0:Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OO8ooO8〇:I

.field private 〇OOo8〇0:Ljava/lang/String;

.field private final 〇OO〇00〇0O:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇O〇〇O8:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o0O:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureModeMenuShownEntity;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇oo〇O〇80:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇88〇8:J

.field public 〇〇08O:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

.field private 〇〇o0〇8:Z

.field private 〇〇o〇:I

.field private final 〇〇〇0o〇〇0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0ooOOo:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 14
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "app"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 7
    .line 8
    .line 9
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 10
    .line 11
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 12
    .line 13
    invoke-direct {p1, v0}, Landroidx/lifecycle/MutableLiveData;-><init>(Ljava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 17
    .line 18
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 19
    .line 20
    invoke-direct {p1, v0}, Landroidx/lifecycle/MutableLiveData;-><init>(Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08O〇00〇o:Landroidx/lifecycle/MutableLiveData;

    .line 24
    .line 25
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 26
    .line 27
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇00O:Landroidx/lifecycle/MutableLiveData;

    .line 31
    .line 32
    sget-object p1, Lkotlinx/coroutines/channels/BufferOverflow;->DROP_OLDEST:Lkotlinx/coroutines/channels/BufferOverflow;

    .line 33
    .line 34
    const/4 v0, 0x0

    .line 35
    const/4 v1, 0x1

    .line 36
    invoke-static {v0, v1, p1}, Lkotlinx/coroutines/flow/SharedFlowKt;->〇080(IILkotlinx/coroutines/channels/BufferOverflow;)Lkotlinx/coroutines/flow/MutableSharedFlow;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8o08O8O:Lkotlinx/coroutines/flow/MutableSharedFlow;

    .line 41
    .line 42
    invoke-static {p1}, Lkotlinx/coroutines/flow/FlowKt;->〇080(Lkotlinx/coroutines/flow/MutableSharedFlow;)Lkotlinx/coroutines/flow/SharedFlow;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇080OO8〇0:Lkotlinx/coroutines/flow/SharedFlow;

    .line 47
    .line 48
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 49
    .line 50
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 51
    .line 52
    .line 53
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O:Landroidx/lifecycle/MutableLiveData;

    .line 54
    .line 55
    new-instance p1, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 56
    .line 57
    const-wide/16 v3, 0x0

    .line 58
    .line 59
    const/4 v5, 0x0

    .line 60
    const/4 v6, 0x0

    .line 61
    const/4 v7, 0x0

    .line 62
    const/4 v8, 0x0

    .line 63
    const-wide/16 v9, 0x0

    .line 64
    .line 65
    const/4 v11, 0x0

    .line 66
    const/16 v12, 0x7f

    .line 67
    .line 68
    const/4 v13, 0x0

    .line 69
    move-object v2, p1

    .line 70
    invoke-direct/range {v2 .. v13}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;-><init>(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;JZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 71
    .line 72
    .line 73
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 74
    .line 75
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 76
    .line 77
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 78
    .line 79
    .line 80
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8oOOo:Landroidx/lifecycle/MutableLiveData;

    .line 81
    .line 82
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 83
    .line 84
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 85
    .line 86
    .line 87
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O〇〇O8:Landroidx/lifecycle/MutableLiveData;

    .line 88
    .line 89
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 90
    .line 91
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 92
    .line 93
    .line 94
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o0O:Landroidx/lifecycle/MutableLiveData;

    .line 95
    .line 96
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 97
    .line 98
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 99
    .line 100
    .line 101
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O88O:Landroidx/lifecycle/MutableLiveData;

    .line 102
    .line 103
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 104
    .line 105
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 106
    .line 107
    .line 108
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO〇〇:Landroidx/lifecycle/MutableLiveData;

    .line 109
    .line 110
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 111
    .line 112
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 113
    .line 114
    .line 115
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8o:Landroidx/lifecycle/MutableLiveData;

    .line 116
    .line 117
    iput v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇oO:I

    .line 118
    .line 119
    const/16 p1, 0x5a

    .line 120
    .line 121
    iput p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇o88o08〇:I

    .line 122
    .line 123
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 124
    .line 125
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 126
    .line 127
    .line 128
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇00O0:Landroidx/lifecycle/MutableLiveData;

    .line 129
    .line 130
    const-string p1, "off"

    .line 131
    .line 132
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇08oOOO0:Ljava/lang/String;

    .line 133
    .line 134
    new-instance p1, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;

    .line 135
    .line 136
    invoke-direct {p1}, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;-><init>()V

    .line 137
    .line 138
    .line 139
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8〇OO:Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;

    .line 140
    .line 141
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇O〇oO()Z

    .line 142
    .line 143
    .line 144
    move-result p1

    .line 145
    if-eqz p1, :cond_0

    .line 146
    .line 147
    const/4 v0, 0x2

    .line 148
    :cond_0
    iput v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OO8ooO8〇:I

    .line 149
    .line 150
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 151
    .line 152
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 153
    .line 154
    .line 155
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OO〇00〇0O:Landroidx/lifecycle/MutableLiveData;

    .line 156
    .line 157
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 158
    .line 159
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 160
    .line 161
    .line 162
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo0〇Ooo:Landroidx/lifecycle/MutableLiveData;

    .line 163
    .line 164
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 165
    .line 166
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 167
    .line 168
    .line 169
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇〇0o〇〇0:Landroidx/lifecycle/MutableLiveData;

    .line 170
    .line 171
    new-instance p1, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 172
    .line 173
    invoke-direct {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;-><init>()V

    .line 174
    .line 175
    .line 176
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 177
    .line 178
    new-instance p1, Lcom/intsig/camscanner/view/ZoomControl;

    .line 179
    .line 180
    invoke-direct {p1}, Lcom/intsig/camscanner/view/ZoomControl;-><init>()V

    .line 181
    .line 182
    .line 183
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 184
    .line 185
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 186
    .line 187
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 188
    .line 189
    .line 190
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇o〇Oo88:Landroidx/lifecycle/MutableLiveData;

    .line 191
    .line 192
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 193
    .line 194
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 195
    .line 196
    .line 197
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO00〇o:Landroidx/lifecycle/MutableLiveData;

    .line 198
    .line 199
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 200
    .line 201
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO0880O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 202
    .line 203
    new-instance p1, Ljava/util/ArrayList;

    .line 204
    .line 205
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 206
    .line 207
    .line 208
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇oo〇O〇80:Ljava/util/List;

    .line 209
    .line 210
    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 211
    .line 212
    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    .line 213
    .line 214
    .line 215
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8〇o88:Ljava/util/Set;

    .line 216
    .line 217
    new-instance p1, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$dispatchTouchEventListener$1;

    .line 218
    .line 219
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$dispatchTouchEventListener$1;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)V

    .line 220
    .line 221
    .line 222
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8oOo0:Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener;

    .line 223
    .line 224
    iput-boolean v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇o0〇8:Z

    .line 225
    .line 226
    new-instance p1, Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;

    .line 227
    .line 228
    invoke-direct {p1}, Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;-><init>()V

    .line 229
    .line 230
    .line 231
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0〇o:Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;

    .line 232
    .line 233
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 234
    .line 235
    .line 236
    move-result p1

    .line 237
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇088O:Z

    .line 238
    .line 239
    new-instance p1, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$saveCaptureImageCallback$1;

    .line 240
    .line 241
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$saveCaptureImageCallback$1;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)V

    .line 242
    .line 243
    .line 244
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOoO8OO〇:Lcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;

    .line 245
    .line 246
    return-void
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method private final O0o8〇O()Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO0o〇〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 13
    .line 14
    const/4 v2, 0x1

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇80〇808〇O()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-ne v0, v2, :cond_0

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    if-eqz v0, :cond_1

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO0880O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 29
    .line 30
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_TEMPLATE_DIR_PRESET:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 31
    .line 32
    if-ne v0, v3, :cond_1

    .line 33
    .line 34
    const/4 v1, 0x1

    .line 35
    :cond_1
    return v1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final O0o〇()Ljava/lang/String;
    .locals 12

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    const-string v1, "com.intsig.camscanner.NEW_DOC"

    .line 16
    .line 17
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-nez v1, :cond_1

    .line 22
    .line 23
    const-string v1, "android.intent.action.VIEW"

    .line 24
    .line 25
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-eqz v1, :cond_0

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const-string v1, "com.intsig.camscanner.NEW_PAGE"

    .line 33
    .line 34
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-eqz v1, :cond_2

    .line 39
    .line 40
    const-string v0, "com.intsig.camscanner.NEW_PAGE_MULTIPLE"

    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-static {v0, v1}, Lcom/intsig/camscanner/capture/scene/CaptureSceneDataExtKt;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 52
    .line 53
    .line 54
    new-instance v0, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;

    .line 55
    .line 56
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;-><init>(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    invoke-static {v0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 64
    .line 65
    .line 66
    const-string v0, "com.intsig.camscanner.NEW_DOC_MULTIPLE"

    .line 67
    .line 68
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 69
    .line 70
    .line 71
    move-result-wide v1

    .line 72
    const-wide/16 v3, 0x0

    .line 73
    .line 74
    const-string v5, "CaptureRefactorViewModel"

    .line 75
    .line 76
    cmp-long v6, v1, v3

    .line 77
    .line 78
    if-lez v6, :cond_8

    .line 79
    .line 80
    new-instance v1, Landroid/content/ContentValues;

    .line 81
    .line 82
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 83
    .line 84
    .line 85
    const-string v2, "state"

    .line 86
    .line 87
    const/4 v6, 0x1

    .line 88
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 89
    .line 90
    .line 91
    move-result-object v7

    .line 92
    invoke-virtual {v1, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 93
    .line 94
    .line 95
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 96
    .line 97
    const/4 v7, 0x0

    .line 98
    if-eqz v2, :cond_3

    .line 99
    .line 100
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->oO80()Z

    .line 101
    .line 102
    .line 103
    move-result v2

    .line 104
    if-ne v2, v6, :cond_3

    .line 105
    .line 106
    const/4 v2, 0x1

    .line 107
    goto :goto_2

    .line 108
    :cond_3
    const/4 v2, 0x0

    .line 109
    :goto_2
    const/4 v8, 0x2

    .line 110
    if-eqz v2, :cond_4

    .line 111
    .line 112
    const-string v2, "page_orientation"

    .line 113
    .line 114
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 115
    .line 116
    .line 117
    move-result-object v9

    .line 118
    invoke-virtual {v1, v2, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 119
    .line 120
    .line 121
    :cond_4
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 122
    .line 123
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 124
    .line 125
    .line 126
    move-result-wide v9

    .line 127
    invoke-static {v2, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 128
    .line 129
    .line 130
    move-result-object v2

    .line 131
    const-string v9, "withAppendedId(Documents\u2026ument.CONTENT_URI, docId)"

    .line 132
    .line 133
    invoke-static {v2, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 137
    .line 138
    .line 139
    move-result-object v9

    .line 140
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 141
    .line 142
    .line 143
    move-result-object v9

    .line 144
    const/4 v10, 0x0

    .line 145
    invoke-virtual {v9, v2, v1, v10, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 146
    .line 147
    .line 148
    move-result v1

    .line 149
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 150
    .line 151
    .line 152
    move-result-object v2

    .line 153
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 154
    .line 155
    .line 156
    move-result-wide v9

    .line 157
    const/4 v11, 0x3

    .line 158
    invoke-static {v2, v9, v10, v11, v6}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 159
    .line 160
    .line 161
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 162
    .line 163
    .line 164
    move-result-object v2

    .line 165
    invoke-static {v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->O〇0(Landroid/content/Context;)Z

    .line 166
    .line 167
    .line 168
    move-result v2

    .line 169
    if-eqz v2, :cond_5

    .line 170
    .line 171
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 172
    .line 173
    .line 174
    move-result-object v2

    .line 175
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇OOo000(Landroid/content/Context;)V

    .line 176
    .line 177
    .line 178
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 179
    .line 180
    .line 181
    move-result-object v2

    .line 182
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 183
    .line 184
    .line 185
    move-result-wide v9

    .line 186
    invoke-static {v2, v9, v10}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 187
    .line 188
    .line 189
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 190
    .line 191
    .line 192
    move-result-object v2

    .line 193
    invoke-static {v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇80〇808〇O(Landroid/content/Context;)V

    .line 194
    .line 195
    .line 196
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 197
    .line 198
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇o00〇〇Oo()J

    .line 199
    .line 200
    .line 201
    move-result-wide v9

    .line 202
    cmp-long v2, v9, v3

    .line 203
    .line 204
    if-lez v2, :cond_7

    .line 205
    .line 206
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 207
    .line 208
    .line 209
    move-result-wide v2

    .line 210
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 211
    .line 212
    .line 213
    move-result-object v4

    .line 214
    invoke-static {v2, v3, v9, v10, v4}, Lcom/intsig/camscanner/util/Util;->o88〇OO08〇(JJLandroid/content/Context;)Landroid/net/Uri;

    .line 215
    .line 216
    .line 217
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 218
    .line 219
    invoke-virtual {v2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 220
    .line 221
    .line 222
    move-result-object v2

    .line 223
    invoke-static {v2, v9, v10}, Lcom/intsig/camscanner/db/dao/TagDao;->O8(Landroid/content/Context;J)Ljava/lang/String;

    .line 224
    .line 225
    .line 226
    move-result-object v2

    .line 227
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 228
    .line 229
    .line 230
    move-result v3

    .line 231
    if-nez v3, :cond_6

    .line 232
    .line 233
    new-array v3, v8, [Landroid/util/Pair;

    .line 234
    .line 235
    new-instance v4, Landroid/util/Pair;

    .line 236
    .line 237
    const-string v8, "name"

    .line 238
    .line 239
    invoke-direct {v4, v8, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 240
    .line 241
    .line 242
    aput-object v4, v3, v7

    .line 243
    .line 244
    new-instance v4, Landroid/util/Pair;

    .line 245
    .line 246
    const-string v7, "type"

    .line 247
    .line 248
    const-string v8, "tab_filter"

    .line 249
    .line 250
    invoke-direct {v4, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 251
    .line 252
    .line 253
    aput-object v4, v3, v6

    .line 254
    .line 255
    const-string v4, "CSNewDoc"

    .line 256
    .line 257
    const-string v6, "select_identified_label"

    .line 258
    .line 259
    invoke-static {v4, v6, v3}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 260
    .line 261
    .line 262
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    .line 263
    .line 264
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 265
    .line 266
    .line 267
    const-string v4, "saveResult, tagId="

    .line 268
    .line 269
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    .line 271
    .line 272
    invoke-virtual {v3, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 273
    .line 274
    .line 275
    const-string v4, "; tagName="

    .line 276
    .line 277
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    .line 279
    .line 280
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    .line 282
    .line 283
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 284
    .line 285
    .line 286
    move-result-object v2

    .line 287
    invoke-static {v5, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    .line 289
    .line 290
    :cond_7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 291
    .line 292
    .line 293
    move-result-wide v2

    .line 294
    new-instance v4, Ljava/lang/StringBuilder;

    .line 295
    .line 296
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 297
    .line 298
    .line 299
    const-string v6, "saveMutipage()   update Doc id="

    .line 300
    .line 301
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    .line 303
    .line 304
    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 305
    .line 306
    .line 307
    const-string v2, ", num="

    .line 308
    .line 309
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    .line 311
    .line 312
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 313
    .line 314
    .line 315
    const-string v1, ", action="

    .line 316
    .line 317
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    .line 319
    .line 320
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    .line 322
    .line 323
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 324
    .line 325
    .line 326
    move-result-object v1

    .line 327
    invoke-static {v5, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    .line 329
    .line 330
    goto :goto_3

    .line 331
    :cond_8
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 332
    .line 333
    .line 334
    move-result-wide v1

    .line 335
    new-instance v3, Ljava/lang/StringBuilder;

    .line 336
    .line 337
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 338
    .line 339
    .line 340
    const-string v4, "saveMutipage()   docId="

    .line 341
    .line 342
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    .line 344
    .line 345
    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 346
    .line 347
    .line 348
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 349
    .line 350
    .line 351
    move-result-object v1

    .line 352
    invoke-static {v5, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    .line 354
    .line 355
    :goto_3
    return-object v0
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
.end method

.method private final OO88〇OOO(Landroid/content/Intent;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇Oo〇o8()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    const-string v2, "scanner_image_src"

    .line 9
    .line 10
    const/4 v3, -0x1

    .line 11
    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    if-ne v1, v2, :cond_0

    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    :cond_0
    if-eqz v0, :cond_2

    .line 19
    .line 20
    const-string v0, "isCaptureguide"

    .line 21
    .line 22
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    if-nez p1, :cond_2

    .line 27
    .line 28
    :try_start_0
    invoke-static {}, Lcom/intsig/logagent/LogAgent;->json()Lcom/intsig/logagent/JsonBuilder;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/intsig/logagent/JsonBuilder;->get()Lorg/json/JSONObject;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    const-string v0, "from"

    .line 37
    .line 38
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇o0〇8:Z

    .line 39
    .line 40
    if-eqz v1, :cond_1

    .line 41
    .line 42
    const-string v1, "single"

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    const-string v1, "batch"

    .line 46
    .line 47
    :goto_0
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    const-string v0, "else"

    .line 52
    .line 53
    const-string v1, "1"

    .line 54
    .line 55
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 56
    .line 57
    .line 58
    move-result-object p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    goto :goto_1

    .line 60
    :catch_0
    move-exception p1

    .line 61
    const-string v0, "CaptureRefactorViewModel"

    .line 62
    .line 63
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 64
    .line 65
    .line 66
    const/4 p1, 0x0

    .line 67
    :goto_1
    const-string v0, "CSScan"

    .line 68
    .line 69
    const-string v1, "import_gallery"

    .line 70
    .line 71
    invoke-static {v0, v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 72
    .line 73
    .line 74
    :cond_2
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final OO8〇()V
    .locals 2

    .line 1
    const-string v0, "CaptureRefactorViewModel"

    .line 2
    .line 3
    const-string v1, "initializeSecondTime()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇OOo000()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇o〇Oo88:Landroidx/lifecycle/MutableLiveData;

    .line 12
    .line 13
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OO〇(Ljava/lang/String;)V
    .locals 25

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇00O0O0(Z)Z

    .line 5
    .line 6
    .line 7
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 12
    .line 13
    .line 14
    move-result-wide v3

    .line 15
    invoke-static {v2, v3, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    iget-object v4, v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 24
    .line 25
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->o〇0()Z

    .line 26
    .line 27
    .line 28
    move-result v4

    .line 29
    if-eqz v4, :cond_0

    .line 30
    .line 31
    invoke-static {v3}, Lcom/intsig/camscanner/tsapp/collaborate/CollaborateUtil;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    new-instance v5, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    const-string v4, ".jpg"

    .line 51
    .line 52
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v15

    .line 59
    move-object/from16 v4, p1

    .line 60
    .line 61
    invoke-static {v4, v15}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v15}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O〇80o08O(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    invoke-static {v15}, Lcom/intsig/camscanner/util/SDStorageManager;->〇80(Ljava/lang/String;)Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v14

    .line 71
    invoke-static {v15}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 72
    .line 73
    .line 74
    move-result-object v9

    .line 75
    const/4 v13, 0x1

    .line 76
    new-array v12, v13, [Z

    .line 77
    .line 78
    aput-boolean v13, v12, v1

    .line 79
    .line 80
    const-string v5, "CaptureRefactorViewModel"

    .line 81
    .line 82
    const/16 v17, 0x0

    .line 83
    .line 84
    if-eqz v9, :cond_8

    .line 85
    .line 86
    new-array v4, v13, [I

    .line 87
    .line 88
    aput v1, v4, v1

    .line 89
    .line 90
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 91
    .line 92
    .line 93
    move-result-object v6

    .line 94
    invoke-interface {v6}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 95
    .line 96
    .line 97
    move-result-object v6

    .line 98
    instance-of v6, v6, Lcom/intsig/camscanner/capture/ppt/PPTCaptureScene;

    .line 99
    .line 100
    if-eqz v6, :cond_5

    .line 101
    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 103
    .line 104
    .line 105
    move-result-object v6

    .line 106
    invoke-interface {v6}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 107
    .line 108
    .line 109
    move-result-object v6

    .line 110
    instance-of v7, v6, Lcom/intsig/camscanner/capture/ppt/PPTCaptureScene;

    .line 111
    .line 112
    if-eqz v7, :cond_1

    .line 113
    .line 114
    check-cast v6, Lcom/intsig/camscanner/capture/ppt/PPTCaptureScene;

    .line 115
    .line 116
    move-object/from16 v16, v6

    .line 117
    .line 118
    goto :goto_0

    .line 119
    :cond_1
    move-object/from16 v16, v17

    .line 120
    .line 121
    :goto_0
    if-eqz v16, :cond_2

    .line 122
    .line 123
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/camscanner/capture/ppt/PPTCaptureScene;->O8OO08o()Lcom/intsig/camscanner/capture/preview/PPTPreviewDataHandle;

    .line 124
    .line 125
    .line 126
    move-result-object v6

    .line 127
    if-eqz v6, :cond_2

    .line 128
    .line 129
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇00OO()I

    .line 130
    .line 131
    .line 132
    move-result v8

    .line 133
    move-object v7, v15

    .line 134
    move-object v10, v12

    .line 135
    move-object v11, v4

    .line 136
    invoke-virtual/range {v6 .. v11}, Lcom/intsig/camscanner/capture/preview/DetectPreviewBorderHandle;->〇〇808〇(Ljava/lang/String;I[I[Z[I)[I

    .line 137
    .line 138
    .line 139
    move-result-object v6

    .line 140
    goto :goto_1

    .line 141
    :cond_2
    move-object/from16 v6, v17

    .line 142
    .line 143
    :goto_1
    if-eqz v16, :cond_3

    .line 144
    .line 145
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/camscanner/capture/ppt/PPTCaptureScene;->o〇()V

    .line 146
    .line 147
    .line 148
    :cond_3
    aget v4, v4, v1

    .line 149
    .line 150
    aget-boolean v7, v12, v1

    .line 151
    .line 152
    move/from16 v16, v4

    .line 153
    .line 154
    if-nez v7, :cond_4

    .line 155
    .line 156
    move-object/from16 v10, v17

    .line 157
    .line 158
    goto :goto_2

    .line 159
    :cond_4
    move-object v10, v6

    .line 160
    goto :goto_2

    .line 161
    :cond_5
    move-object/from16 v10, v17

    .line 162
    .line 163
    const/16 v16, 0x0

    .line 164
    .line 165
    :goto_2
    sget-object v4, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇080:Lcom/intsig/camscanner/app/DBInsertPageUtil;

    .line 166
    .line 167
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 168
    .line 169
    .line 170
    move-result-wide v6

    .line 171
    add-int/lit8 v2, v2, 0x1

    .line 172
    .line 173
    const/4 v9, 0x1

    .line 174
    const/4 v11, 0x1

    .line 175
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0000OOO()Z

    .line 176
    .line 177
    .line 178
    move-result v18

    .line 179
    const/16 v19, 0x0

    .line 180
    .line 181
    const/16 v20, 0x1

    .line 182
    .line 183
    const/16 v21, 0x0

    .line 184
    .line 185
    move-object v8, v5

    .line 186
    move-wide v5, v6

    .line 187
    move-object v7, v3

    .line 188
    move-object v1, v8

    .line 189
    move v8, v2

    .line 190
    move-object/from16 v22, v12

    .line 191
    .line 192
    move/from16 v12, v16

    .line 193
    .line 194
    move/from16 v13, v18

    .line 195
    .line 196
    move-object/from16 v23, v14

    .line 197
    .line 198
    move/from16 v14, v19

    .line 199
    .line 200
    move-object/from16 v24, v15

    .line 201
    .line 202
    move/from16 v15, v20

    .line 203
    .line 204
    move/from16 v16, v21

    .line 205
    .line 206
    invoke-virtual/range {v4 .. v16}, Lcom/intsig/camscanner/app/DBInsertPageUtil;->Oooo8o0〇(JLjava/lang/String;IZ[IIIZZZZ)Landroid/net/Uri;

    .line 207
    .line 208
    .line 209
    move-result-object v4

    .line 210
    iget v5, v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇o88o08〇:I

    .line 211
    .line 212
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0000OOO()Z

    .line 213
    .line 214
    .line 215
    move-result v6

    .line 216
    iget-object v7, v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 217
    .line 218
    if-eqz v7, :cond_6

    .line 219
    .line 220
    invoke-interface {v7}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇〇808〇()Lcom/intsig/camscanner/capture/CaptureMode;

    .line 221
    .line 222
    .line 223
    move-result-object v17

    .line 224
    :cond_6
    move-object/from16 v7, v17

    .line 225
    .line 226
    new-instance v8, Ljava/lang/StringBuilder;

    .line 227
    .line 228
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 229
    .line 230
    .line 231
    const-string v9, "insertOneImage "

    .line 232
    .line 233
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    .line 235
    .line 236
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 237
    .line 238
    .line 239
    const-string v9, " mDocNum = "

    .line 240
    .line 241
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    .line 243
    .line 244
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 245
    .line 246
    .line 247
    const-string v9, " mViewModel.mRotation = "

    .line 248
    .line 249
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    .line 251
    .line 252
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 253
    .line 254
    .line 255
    const-string v5, ", mIsOfflineFolder:"

    .line 256
    .line 257
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    .line 259
    .line 260
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 261
    .line 262
    .line 263
    const-string v5, " imageUUID="

    .line 264
    .line 265
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    .line 267
    .line 268
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    .line 270
    .line 271
    const-string v3, " currentMode="

    .line 272
    .line 273
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    .line 275
    .line 276
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 277
    .line 278
    .line 279
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 280
    .line 281
    .line 282
    move-result-object v3

    .line 283
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    .line 285
    .line 286
    if-eqz v4, :cond_7

    .line 287
    .line 288
    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 289
    .line 290
    .line 291
    move-result-wide v5

    .line 292
    iput-wide v5, v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇800OO〇0O:J

    .line 293
    .line 294
    iget-object v3, v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇oo〇O〇80:Ljava/util/List;

    .line 295
    .line 296
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 297
    .line 298
    .line 299
    move-result-object v5

    .line 300
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    .line 302
    .line 303
    move-object/from16 v5, v24

    .line 304
    .line 305
    goto :goto_3

    .line 306
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    .line 307
    .line 308
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 309
    .line 310
    .line 311
    const-string v5, "insertOneImage failed "

    .line 312
    .line 313
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    .line 315
    .line 316
    move-object/from16 v5, v24

    .line 317
    .line 318
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    .line 320
    .line 321
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 322
    .line 323
    .line 324
    move-result-object v3

    .line 325
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    .line 327
    .line 328
    :goto_3
    move/from16 v16, v2

    .line 329
    .line 330
    move-object/from16 v17, v4

    .line 331
    .line 332
    goto :goto_4

    .line 333
    :cond_8
    move-object v1, v5

    .line 334
    move-object/from16 v22, v12

    .line 335
    .line 336
    move-object/from16 v23, v14

    .line 337
    .line 338
    move-object v5, v15

    .line 339
    new-instance v3, Ljava/lang/StringBuilder;

    .line 340
    .line 341
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 342
    .line 343
    .line 344
    const-string v4, "invalid image "

    .line 345
    .line 346
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    .line 348
    .line 349
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    .line 351
    .line 352
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 353
    .line 354
    .line 355
    move-result-object v3

    .line 356
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    .line 358
    .line 359
    move/from16 v16, v2

    .line 360
    .line 361
    :goto_4
    if-eqz v17, :cond_11

    .line 362
    .line 363
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 364
    .line 365
    invoke-virtual {v2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 366
    .line 367
    .line 368
    move-result-object v2

    .line 369
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 370
    .line 371
    .line 372
    move-result-wide v3

    .line 373
    invoke-static {v2, v3, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->Oo〇O(Landroid/content/Context;J)V

    .line 374
    .line 375
    .line 376
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0oO()Z

    .line 377
    .line 378
    .line 379
    move-result v15

    .line 380
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 381
    .line 382
    .line 383
    move-result-object v2

    .line 384
    invoke-static {v2}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getCurrentEnhanceMode(Landroid/content/Context;)I

    .line 385
    .line 386
    .line 387
    move-result v2

    .line 388
    const/4 v3, 0x0

    .line 389
    aget-boolean v4, v22, v3

    .line 390
    .line 391
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇oO08〇o0()Z

    .line 392
    .line 393
    .line 394
    move-result v6

    .line 395
    iget-object v7, v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 396
    .line 397
    if-eqz v7, :cond_9

    .line 398
    .line 399
    invoke-interface {v7}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->oO80()Z

    .line 400
    .line 401
    .line 402
    move-result v7

    .line 403
    const/4 v8, 0x1

    .line 404
    if-ne v7, v8, :cond_a

    .line 405
    .line 406
    const/4 v13, 0x1

    .line 407
    goto :goto_5

    .line 408
    :cond_9
    const/4 v8, 0x1

    .line 409
    :cond_a
    const/4 v13, 0x0

    .line 410
    :goto_5
    if-eqz v13, :cond_b

    .line 411
    .line 412
    xor-int/lit8 v2, v4, 0x1

    .line 413
    .line 414
    const/16 v4, 0x11

    .line 415
    .line 416
    move v13, v2

    .line 417
    const/16 v14, 0x11

    .line 418
    .line 419
    goto :goto_9

    .line 420
    :cond_b
    iget-object v4, v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 421
    .line 422
    if-eqz v4, :cond_c

    .line 423
    .line 424
    invoke-interface {v4}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->O8()Z

    .line 425
    .line 426
    .line 427
    move-result v4

    .line 428
    if-ne v4, v8, :cond_c

    .line 429
    .line 430
    const/4 v13, 0x1

    .line 431
    goto :goto_6

    .line 432
    :cond_c
    const/4 v13, 0x0

    .line 433
    :goto_6
    if-nez v13, :cond_f

    .line 434
    .line 435
    iget-object v4, v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 436
    .line 437
    if-eqz v4, :cond_d

    .line 438
    .line 439
    invoke-interface {v4}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->Oooo8o0〇()Z

    .line 440
    .line 441
    .line 442
    move-result v4

    .line 443
    if-ne v4, v8, :cond_d

    .line 444
    .line 445
    const/4 v13, 0x1

    .line 446
    goto :goto_7

    .line 447
    :cond_d
    const/4 v13, 0x0

    .line 448
    :goto_7
    if-eqz v13, :cond_e

    .line 449
    .line 450
    goto :goto_8

    .line 451
    :cond_e
    move v14, v2

    .line 452
    move v13, v6

    .line 453
    goto :goto_9

    .line 454
    :cond_f
    :goto_8
    const/4 v2, -0x1

    .line 455
    const/4 v13, 0x0

    .line 456
    const/4 v14, -0x1

    .line 457
    :goto_9
    invoke-static/range {v17 .. v17}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 458
    .line 459
    .line 460
    move-result-wide v9

    .line 461
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 462
    .line 463
    .line 464
    move-result-object v6

    .line 465
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 466
    .line 467
    .line 468
    move-result-wide v11

    .line 469
    const-string v2, "path"

    .line 470
    .line 471
    move-object/from16 v4, v23

    .line 472
    .line 473
    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 474
    .line 475
    .line 476
    iget-object v2, v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 477
    .line 478
    if-eqz v2, :cond_10

    .line 479
    .line 480
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->oO80()Z

    .line 481
    .line 482
    .line 483
    move-result v2

    .line 484
    if-ne v2, v8, :cond_10

    .line 485
    .line 486
    const/16 v17, 0x1

    .line 487
    .line 488
    goto :goto_a

    .line 489
    :cond_10
    const/16 v17, 0x0

    .line 490
    .line 491
    :goto_a
    move-wide v7, v11

    .line 492
    move-object v11, v5

    .line 493
    move-object v12, v4

    .line 494
    invoke-interface/range {v6 .. v17}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OOO8o〇〇(JJLjava/lang/String;Ljava/lang/String;ZIZIZ)V

    .line 495
    .line 496
    .line 497
    goto :goto_b

    .line 498
    :cond_11
    const-string v2, "insertOneImage failed"

    .line 499
    .line 500
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    .line 502
    .line 503
    :goto_b
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇oO08〇o0()Z

    .line 504
    .line 505
    .line 506
    move-result v2

    .line 507
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 508
    .line 509
    .line 510
    move-result-object v3

    .line 511
    invoke-static {v3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getCurrentEnhanceMode(Landroid/content/Context;)I

    .line 512
    .line 513
    .line 514
    move-result v3

    .line 515
    new-instance v4, Ljava/lang/StringBuilder;

    .line 516
    .line 517
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 518
    .line 519
    .line 520
    const-string v5, "needTrim="

    .line 521
    .line 522
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 523
    .line 524
    .line 525
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 526
    .line 527
    .line 528
    const-string v2, " enhanceMode="

    .line 529
    .line 530
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 531
    .line 532
    .line 533
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 534
    .line 535
    .line 536
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 537
    .line 538
    .line 539
    move-result-object v2

    .line 540
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    .line 542
    .line 543
    return-void
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)Lcom/intsig/camscanner/capture/scene/CaptureSceneData;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->ooo0〇〇O:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final o08〇〇0O()I
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O000(I)I

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇0O〇Oo(I)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    :cond_0
    return v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o0ooO(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;Landroid/net/Uri;IZZILjava/lang/Object;)V
    .locals 1

    .line 1
    and-int/lit8 p6, p5, 0x4

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    if-eqz p6, :cond_0

    .line 5
    .line 6
    const/4 p3, 0x0

    .line 7
    :cond_0
    and-int/lit8 p5, p5, 0x8

    .line 8
    .line 9
    if-eqz p5, :cond_1

    .line 10
    .line 11
    const/4 p4, 0x0

    .line 12
    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8〇o(Landroid/net/Uri;IZZ)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method private static final o88o0O(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;Ljava/lang/Boolean;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-eqz p1, :cond_1

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const/4 v0, 0x1

    .line 19
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇0〇O0088o(Z)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    if-eqz p1, :cond_0

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O88o〇(Z)V

    .line 29
    .line 30
    .line 31
    :cond_0
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8〇80o:Z

    .line 32
    .line 33
    const-string p1, "CSScan"

    .line 34
    .line 35
    const-string v1, "scan_idcard_find"

    .line 36
    .line 37
    invoke-static {p1, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    const-string p1, "CaptureRefactorViewModel"

    .line 41
    .line 42
    const-string v1, "find idCard on preview"

    .line 43
    .line 44
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o880:Z

    .line 48
    .line 49
    :cond_1
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oO()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O0O:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;->〇O〇〇O8()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oOo〇8o008(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OOo0O()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O8ooOoo〇()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_2

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇o00〇〇Oo()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    const/4 v1, 0x1

    .line 29
    if-eq v0, p1, :cond_1

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇o〇()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 40
    .line 41
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->o〇0(I)V

    .line 42
    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇o〇()I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    if-ne p1, v1, :cond_3

    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 53
    .line 54
    const/4 v0, 0x2

    .line 55
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇〇888(I)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->Oo8Oo00oo()V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 67
    .line 68
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇o〇()I

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    if-nez v0, :cond_3

    .line 73
    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 75
    .line 76
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    if-eq v0, p1, :cond_3

    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 83
    .line 84
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->o〇0(I)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇0000OOO(I)V

    .line 92
    .line 93
    .line 94
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 95
    .line 96
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇〇888(I)V

    .line 97
    .line 98
    .line 99
    goto :goto_0

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 101
    .line 102
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->oO80(I)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 110
    .line 111
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 112
    .line 113
    .line 114
    move-result v0

    .line 115
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇8(I)V

    .line 116
    .line 117
    .line 118
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

    .line 119
    .line 120
    if-eqz p1, :cond_3

    .line 121
    .line 122
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 123
    .line 124
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/CustomSeekBar;->O8(I)V

    .line 129
    .line 130
    .line 131
    :cond_3
    :goto_0
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public static final synthetic oo88o8O(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final o〇(Lcom/intsig/camscanner/capture/CaptureMode;)Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->WRITING_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 2
    .line 3
    if-eq p1, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->WHITE_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 6
    .line 7
    if-ne p1, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 13
    :goto_1
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇08O〇00〇o()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->ooOO()Lcom/intsig/camscanner/capture/CaptureModeMenuManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/CaptureModeMenuManager;->oO00OOO(Lcom/intsig/camscanner/capture/CaptureMode;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇0O00oO(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;Ljava/lang/Boolean;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-eqz p1, :cond_1

    .line 13
    .line 14
    const-string p1, "CSInvoiceToast"

    .line 15
    .line 16
    invoke-static {p1}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    const-string p1, "CaptureRefactorViewModel"

    .line 20
    .line 21
    const-string v0, "find invoice on preview"

    .line 22
    .line 23
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    const/4 v0, 0x1

    .line 31
    if-eqz p1, :cond_0

    .line 32
    .line 33
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O88o〇(Z)V

    .line 34
    .line 35
    .line 36
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->oOO〇〇(Z)V

    .line 41
    .line 42
    .line 43
    :cond_1
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o88o0O(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;Ljava/lang/Boolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇8o8O〇O()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇80()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇o88o08〇:I

    .line 13
    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O00oO(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;Ljava/lang/Boolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇O00(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0〇o:Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇oO8O0〇〇O(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;Landroid/view/View;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;ILjava/lang/Object;)V
    .locals 9

    .line 1
    and-int/lit8 v0, p8, 0x40

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    move-object v8, v0

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    move-object/from16 v8, p7

    .line 9
    .line 10
    :goto_0
    move-object v1, p0

    .line 11
    move-object v2, p1

    .line 12
    move-object v3, p2

    .line 13
    move-object v4, p3

    .line 14
    move-object v5, p4

    .line 15
    move-object v6, p5

    .line 16
    move-object v7, p6

    .line 17
    invoke-virtual/range {v1 .. v8}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o8oO(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;Landroid/view/View;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
.end method

.method private final 〇oOo〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oooO888:Landroid/content/SharedPreferences;

    .line 2
    .line 3
    const-string v1, "off"

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v2, "pref_camera_flashmode_key"

    .line 8
    .line 9
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    if-nez v0, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    move-object v1, v0

    .line 19
    :goto_1
    iput-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇08oOOO0:Ljava/lang/String;

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇oo〇(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇〇〇0〇〇0()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇o8()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇088O:Z

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08O〇00〇o()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v0, 0x0

    .line 24
    :goto_0
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O0()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇oO:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O00()Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8〇OO:Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O000()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO〇OOo:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O00O(Z)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o08O()Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇o88o08〇:I

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8o8O〇O()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;->〇O888o0o(ZII)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO〇〇:Landroidx/lifecycle/MutableLiveData;

    .line 15
    .line 16
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8〇o〇88:Lcom/intsig/callback/Callback;

    .line 24
    .line 25
    if-eqz p1, :cond_0

    .line 26
    .line 27
    iget v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇o88o08〇:I

    .line 28
    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-interface {p1, v0}, Lcom/intsig/callback/Callback;->call(Ljava/lang/Object;)V

    .line 34
    .line 35
    .line 36
    :cond_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O08O0〇O(Z)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OO8ooO8〇:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v1, 0x2

    .line 7
    const/4 v2, 0x1

    .line 8
    if-eqz p1, :cond_1

    .line 9
    .line 10
    if-ne v0, v1, :cond_1

    .line 11
    .line 12
    iput v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OO8ooO8〇:I

    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Ooo08:Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;

    .line 15
    .line 16
    if-eqz p1, :cond_2

    .line 17
    .line 18
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;->Oooo8o0〇()V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_1
    if-ne v0, v2, :cond_2

    .line 23
    .line 24
    if-nez p1, :cond_2

    .line 25
    .line 26
    iput v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OO8ooO8〇:I

    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Ooo08:Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;

    .line 29
    .line 30
    if-eqz p1, :cond_2

    .line 31
    .line 32
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;->〇〇8O0〇8()V

    .line 33
    .line 34
    .line 35
    :cond_2
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public O0O8OO088()Z
    .locals 9

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->Oo08()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const-string v1, "CaptureRefactorViewModel"

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    const-string v0, "resetCameraZoomValue FAILED. because mCameraClientNew is NOT supported"

    .line 15
    .line 16
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    return v2

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o00〇88〇08:Lcom/intsig/camscanner/capture/ppt/PPTScaleCallback;

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-interface {v0, v2}, Lcom/intsig/camscanner/capture/ppt/PPTScaleCallback;->o〇0(Z)V

    .line 25
    .line 26
    .line 27
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    new-instance v3, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v4, "resetCameraZoomValue and Checking zoom value - "

    .line 39
    .line 40
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08〇0〇o〇8()Z

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    const/4 v4, 0x1

    .line 58
    if-eqz v3, :cond_4

    .line 59
    .line 60
    iget-object v3, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 61
    .line 62
    invoke-virtual {v3}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 63
    .line 64
    .line 65
    move-result v3

    .line 66
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0()I

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    int-to-float v5, v5

    .line 71
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 72
    .line 73
    .line 74
    move-result v6

    .line 75
    cmpg-float v5, v5, v6

    .line 76
    .line 77
    if-nez v5, :cond_2

    .line 78
    .line 79
    const/4 v5, 0x1

    .line 80
    goto :goto_0

    .line 81
    :cond_2
    const/4 v5, 0x0

    .line 82
    :goto_0
    if-nez v5, :cond_3

    .line 83
    .line 84
    const/high16 v3, 0x3f800000    # 1.0f

    .line 85
    .line 86
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 87
    .line 88
    .line 89
    move-result v5

    .line 90
    sub-float/2addr v3, v5

    .line 91
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0()I

    .line 92
    .line 93
    .line 94
    move-result v5

    .line 95
    int-to-float v5, v5

    .line 96
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 97
    .line 98
    .line 99
    move-result v6

    .line 100
    sub-float/2addr v5, v6

    .line 101
    div-float/2addr v3, v5

    .line 102
    const/16 v5, 0x63

    .line 103
    .line 104
    int-to-float v5, v5

    .line 105
    mul-float v3, v3, v5

    .line 106
    .line 107
    float-to-int v3, v3

    .line 108
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 109
    .line 110
    .line 111
    move-result v5

    .line 112
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0()I

    .line 113
    .line 114
    .line 115
    move-result v6

    .line 116
    new-instance v7, Ljava/lang/StringBuilder;

    .line 117
    .line 118
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    .line 120
    .line 121
    const-string v8, "resetCameraZoomValue getMinZoom: "

    .line 122
    .line 123
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    const-string v5, "   maxZoomValue:  "

    .line 130
    .line 131
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object v5

    .line 141
    invoke-static {v1, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    .line 143
    .line 144
    :cond_3
    if-eq v0, v3, :cond_5

    .line 145
    .line 146
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->resetZoom()V

    .line 151
    .line 152
    .line 153
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇OOo000()V

    .line 154
    .line 155
    .line 156
    return v4

    .line 157
    :cond_4
    if-eqz v0, :cond_5

    .line 158
    .line 159
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->resetZoom()V

    .line 164
    .line 165
    .line 166
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇OOo000()V

    .line 167
    .line 168
    .line 169
    return v4

    .line 170
    :cond_5
    return v2
    .line 171
    .line 172
    .line 173
.end method

.method public O0OO8〇0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8o〇O0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O0O〇OOo(Lcom/intsig/camscanner/capture/inputdata/CaptureSceneInputData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8〇OOoooo:Lcom/intsig/camscanner/capture/inputdata/CaptureSceneInputData;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public O0o(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "imagePath"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    iget-boolean v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇oo:Z

    .line 11
    .line 12
    if-nez v2, :cond_0

    .line 13
    .line 14
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO〇(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iget-boolean p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇oo:Z

    .line 18
    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o〇8()V

    .line 22
    .line 23
    .line 24
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 25
    .line 26
    .line 27
    move-result-wide v2

    .line 28
    sub-long/2addr v2, v0

    .line 29
    new-instance p1, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v0, "insertImage consume "

    .line 35
    .line 36
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    const-string v0, "CaptureRefactorViewModel"

    .line 47
    .line 48
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final O0oO0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8〇80o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public O0oO008()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇O8OO:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x1

    .line 10
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->oOO〇〇(Z)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O0oo0o0〇()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08O8o〇0(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O0o〇O0〇()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8〇80o:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x1

    .line 10
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇0〇O0088o(Z)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O0o〇〇Oo()Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8oOo0:Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O0〇OO8(ZZILcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/callback/Callback0;)V
    .locals 8

    .line 1
    const-string v0, "finishMultiPage"

    .line 2
    .line 3
    const-string v1, "CaptureRefactorViewModel"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/scanner/CandidateLinesManager;->getInstance()Lcom/intsig/camscanner/scanner/CandidateLinesManager;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/scanner/CandidateLinesManager;->destroyResource4Lines()V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇Oo〇o8()V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O0o〇()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    new-instance v2, Landroid/content/Intent;

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    const-class v4, Lcom/intsig/camscanner/DocumentActivity;

    .line 33
    .line 34
    const/4 v5, 0x0

    .line 35
    invoke-direct {v2, v0, v5, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    instance-of v3, v3, Lcom/intsig/camscanner/capture/toword/DocToWordMultiCaptureScene;

    .line 43
    .line 44
    if-eqz v3, :cond_0

    .line 45
    .line 46
    const-string v3, "EXTRA_KEY_TO_WORD_ENTRANCE"

    .line 47
    .line 48
    const-string v4, "image_word_doc"

    .line 49
    .line 50
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    .line 52
    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 58
    .line 59
    .line 60
    move-result-wide v6

    .line 61
    invoke-static {v3, v6, v7}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    const-string v4, "doc_pagenum"

    .line 66
    .line 67
    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 68
    .line 69
    .line 70
    const-string v3, "extra_entrance"

    .line 71
    .line 72
    iget-object v4, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO0880O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 73
    .line 74
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 75
    .line 76
    .line 77
    const-string v3, "EXTRA_LOTTERY_VALUE"

    .line 78
    .line 79
    iget-object v4, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OOo8〇0:Ljava/lang/String;

    .line 80
    .line 81
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    .line 83
    .line 84
    const-string v3, "EXTRA_BOOLEAN_PAGE_LIST_EDIT_LOTTIE"

    .line 85
    .line 86
    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 87
    .line 88
    .line 89
    const-string p2, "extra_int_page_list_add_one_page_workbench"

    .line 90
    .line 91
    invoke-virtual {v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 92
    .line 93
    .line 94
    const-string p2, "extra_folder_id"

    .line 95
    .line 96
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object p3

    .line 100
    invoke-virtual {v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    .line 102
    .line 103
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇8〇()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object p2

    .line 107
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 108
    .line 109
    .line 110
    move-result p2

    .line 111
    if-nez p2, :cond_1

    .line 112
    .line 113
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇8〇()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object p2

    .line 117
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO0o〇〇()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object p3

    .line 121
    invoke-static {p2, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 122
    .line 123
    .line 124
    move-result p2

    .line 125
    if-nez p2, :cond_1

    .line 126
    .line 127
    const-string p2, "doc_title"

    .line 128
    .line 129
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇8〇()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object p3

    .line 133
    invoke-virtual {v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    .line 135
    .line 136
    :cond_1
    const-string p2, "Constant_doc_finish_page_type"

    .line 137
    .line 138
    const-string p3, "spec_action_show_scan_done"

    .line 139
    .line 140
    const-string v3, "constant_add_spec_action"

    .line 141
    .line 142
    if-eqz p1, :cond_2

    .line 143
    .line 144
    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    .line 146
    .line 147
    const-string p3, "Doc_finish_type_ppt"

    .line 148
    .line 149
    invoke-virtual {v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    .line 151
    .line 152
    goto :goto_0

    .line 153
    :cond_2
    iget-boolean v4, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇o0〇8:Z

    .line 154
    .line 155
    if-eqz v4, :cond_4

    .line 156
    .line 157
    sget-object v4, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_WORKBENCH:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 158
    .line 159
    if-eq p4, v4, :cond_3

    .line 160
    .line 161
    sget-object v4, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_MULTI:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 162
    .line 163
    if-ne p4, v4, :cond_4

    .line 164
    .line 165
    :cond_3
    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    .line 167
    .line 168
    const-string p3, "Doc_finish_type_default"

    .line 169
    .line 170
    invoke-virtual {v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    .line 172
    .line 173
    goto :goto_0

    .line 174
    :cond_4
    sget-object p2, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_WORKBENCH:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 175
    .line 176
    if-eq p4, p2, :cond_5

    .line 177
    .line 178
    sget-object p2, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_MULTI:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 179
    .line 180
    if-ne p4, p2, :cond_6

    .line 181
    .line 182
    :cond_5
    const-string p2, "SPEC_ACTION_SHOW_OPERATION_PAGE"

    .line 183
    .line 184
    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 185
    .line 186
    .line 187
    :cond_6
    :goto_0
    const-string p2, "com.intsig.camscanner.NEW_DOC_MULTIPLE"

    .line 188
    .line 189
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 190
    .line 191
    .line 192
    move-result p2

    .line 193
    const/4 p3, -0x1

    .line 194
    const/4 p4, 0x1

    .line 195
    const/4 v0, 0x0

    .line 196
    if-eqz p2, :cond_15

    .line 197
    .line 198
    sget-object p2, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;->〇080:Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;

    .line 199
    .line 200
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object v4

    .line 204
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 205
    .line 206
    .line 207
    const-string p2, "extra_from_widget"

    .line 208
    .line 209
    iget-boolean v4, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8o〇O0:Z

    .line 210
    .line 211
    invoke-virtual {v2, p2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 212
    .line 213
    .line 214
    iget-object p2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 215
    .line 216
    if-eqz p2, :cond_7

    .line 217
    .line 218
    invoke-interface {p2}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->O8()Z

    .line 219
    .line 220
    .line 221
    move-result p2

    .line 222
    if-ne p4, p2, :cond_7

    .line 223
    .line 224
    const/4 p2, 0x1

    .line 225
    goto :goto_1

    .line 226
    :cond_7
    const/4 p2, 0x0

    .line 227
    :goto_1
    const-string v4, "extra_start_do_camera"

    .line 228
    .line 229
    if-nez p2, :cond_9

    .line 230
    .line 231
    iget-object p2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 232
    .line 233
    if-eqz p2, :cond_8

    .line 234
    .line 235
    invoke-interface {p2}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->Oooo8o0〇()Z

    .line 236
    .line 237
    .line 238
    move-result p2

    .line 239
    if-ne p4, p2, :cond_8

    .line 240
    .line 241
    const/4 p2, 0x1

    .line 242
    goto :goto_2

    .line 243
    :cond_8
    const/4 p2, 0x0

    .line 244
    :goto_2
    if-eqz p2, :cond_a

    .line 245
    .line 246
    :cond_9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 247
    .line 248
    .line 249
    move-result-object p2

    .line 250
    invoke-virtual {p2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 251
    .line 252
    .line 253
    move-result-object p2

    .line 254
    const-string v6, "is_from_e_evidence_preview"

    .line 255
    .line 256
    invoke-virtual {p2, v6, p4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 257
    .line 258
    .line 259
    move-result p2

    .line 260
    if-eqz p2, :cond_a

    .line 261
    .line 262
    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 263
    .line 264
    .line 265
    goto :goto_3

    .line 266
    :cond_a
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇Oo()Z

    .line 267
    .line 268
    .line 269
    move-result p2

    .line 270
    invoke-virtual {v2, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 271
    .line 272
    .line 273
    :goto_3
    iget-boolean p2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇o0〇8:Z

    .line 274
    .line 275
    new-instance v4, Ljava/lang/StringBuilder;

    .line 276
    .line 277
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 278
    .line 279
    .line 280
    const-string v6, "finishMultiPage, create a new document. singleMode:"

    .line 281
    .line 282
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    .line 284
    .line 285
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 286
    .line 287
    .line 288
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 289
    .line 290
    .line 291
    move-result-object p2

    .line 292
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    .line 294
    .line 295
    const-string p2, "doc_id"

    .line 296
    .line 297
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 298
    .line 299
    .line 300
    move-result-wide v6

    .line 301
    invoke-virtual {v2, p2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 302
    .line 303
    .line 304
    if-nez p1, :cond_d

    .line 305
    .line 306
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 307
    .line 308
    if-eqz p1, :cond_b

    .line 309
    .line 310
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇o〇()Z

    .line 311
    .line 312
    .line 313
    move-result p1

    .line 314
    if-ne p1, p4, :cond_b

    .line 315
    .line 316
    const/4 p1, 0x1

    .line 317
    goto :goto_4

    .line 318
    :cond_b
    const/4 p1, 0x0

    .line 319
    :goto_4
    if-eqz p1, :cond_d

    .line 320
    .line 321
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OoO8()Z

    .line 322
    .line 323
    .line 324
    move-result p1

    .line 325
    if-eqz p1, :cond_c

    .line 326
    .line 327
    const-string p1, "spec_action_loading_to_pdf_editing"

    .line 328
    .line 329
    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    .line 331
    .line 332
    :cond_c
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 333
    .line 334
    .line 335
    move-result-object p1

    .line 336
    invoke-virtual {p1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    .line 337
    .line 338
    .line 339
    move-result-object p1

    .line 340
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 341
    .line 342
    .line 343
    move-result-wide v3

    .line 344
    invoke-static {p1, v3, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->OO0o〇〇〇〇0(Landroid/content/Context;J)Ljava/lang/String;

    .line 345
    .line 346
    .line 347
    move-result-object p1

    .line 348
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 349
    .line 350
    .line 351
    move-result-object p2

    .line 352
    invoke-virtual {p2}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    .line 353
    .line 354
    .line 355
    move-result-object p2

    .line 356
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 357
    .line 358
    .line 359
    move-result-wide v3

    .line 360
    invoke-static {p2, v3, v4}, Lcom/intsig/camscanner/app/DBUtil;->O880oOO08(Landroid/content/Context;J)Ljava/lang/String;

    .line 361
    .line 362
    .line 363
    move-result-object p2

    .line 364
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 365
    .line 366
    .line 367
    move-result p2

    .line 368
    if-eqz p2, :cond_d

    .line 369
    .line 370
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO0(Ljava/lang/String;)V

    .line 371
    .line 372
    .line 373
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 374
    .line 375
    .line 376
    move-result-object p1

    .line 377
    sput-object p1, Lcom/intsig/camscanner/mainmenu/common/MainCommonUtil;->〇o00〇〇Oo:Ljava/lang/String;

    .line 378
    .line 379
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 380
    .line 381
    .line 382
    move-result-object p1

    .line 383
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 384
    .line 385
    .line 386
    move-result-object p2

    .line 387
    invoke-static {p1, p2}, Lcom/intsig/camscanner/app/DBUtil;->〇o〇8(Landroid/content/Context;Ljava/lang/String;)Z

    .line 388
    .line 389
    .line 390
    move-result p1

    .line 391
    sput-boolean p1, Lcom/intsig/camscanner/mainmenu/common/MainCommonUtil;->〇o〇:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    .line 393
    goto :goto_5

    .line 394
    :catch_0
    move-exception p1

    .line 395
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 396
    .line 397
    .line 398
    :cond_d
    :goto_5
    if-eqz p5, :cond_e

    .line 399
    .line 400
    invoke-interface {p5}, Lcom/intsig/callback/Callback0;->call()V

    .line 401
    .line 402
    .line 403
    goto :goto_6

    .line 404
    :cond_e
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO〇00〇8oO:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;

    .line 405
    .line 406
    if-eqz p1, :cond_f

    .line 407
    .line 408
    invoke-interface {p1, v2}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;->〇o(Landroid/content/Intent;)V

    .line 409
    .line 410
    .line 411
    :cond_f
    :goto_6
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 412
    .line 413
    if-eqz p1, :cond_10

    .line 414
    .line 415
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇o〇()Z

    .line 416
    .line 417
    .line 418
    move-result p1

    .line 419
    if-ne p1, p4, :cond_10

    .line 420
    .line 421
    const/4 p1, 0x1

    .line 422
    goto :goto_7

    .line 423
    :cond_10
    const/4 p1, 0x0

    .line 424
    :goto_7
    if-nez p1, :cond_12

    .line 425
    .line 426
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇8〇()Ljava/lang/String;

    .line 427
    .line 428
    .line 429
    move-result-object p1

    .line 430
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 431
    .line 432
    .line 433
    move-result p1

    .line 434
    if-nez p1, :cond_12

    .line 435
    .line 436
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇8〇()Ljava/lang/String;

    .line 437
    .line 438
    .line 439
    move-result-object p1

    .line 440
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO0o〇〇()Ljava/lang/String;

    .line 441
    .line 442
    .line 443
    move-result-object p2

    .line 444
    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 445
    .line 446
    .line 447
    move-result p1

    .line 448
    if-nez p1, :cond_12

    .line 449
    .line 450
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇8〇()Ljava/lang/String;

    .line 451
    .line 452
    .line 453
    move-result-object p1

    .line 454
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO0o〇〇()Ljava/lang/String;

    .line 455
    .line 456
    .line 457
    move-result-object p2

    .line 458
    new-instance p5, Ljava/lang/StringBuilder;

    .line 459
    .line 460
    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    .line 461
    .line 462
    .line 463
    const-string v2, "mRenameDocTitle="

    .line 464
    .line 465
    invoke-virtual {p5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 466
    .line 467
    .line 468
    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 469
    .line 470
    .line 471
    const-string p1, " mDocTitle="

    .line 472
    .line 473
    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 474
    .line 475
    .line 476
    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    .line 478
    .line 479
    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 480
    .line 481
    .line 482
    move-result-object p1

    .line 483
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    .line 485
    .line 486
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 487
    .line 488
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 489
    .line 490
    .line 491
    move-result-wide v2

    .line 492
    invoke-static {p1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 493
    .line 494
    .line 495
    move-result-object p1

    .line 496
    const-string p2, "withAppendedId(Documents\u2026ument.CONTENT_URI, docId)"

    .line 497
    .line 498
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 499
    .line 500
    .line 501
    new-instance p2, Landroid/content/ContentValues;

    .line 502
    .line 503
    invoke-direct {p2}, Landroid/content/ContentValues;-><init>()V

    .line 504
    .line 505
    .line 506
    const-string p5, "title"

    .line 507
    .line 508
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇8〇()Ljava/lang/String;

    .line 509
    .line 510
    .line 511
    move-result-object v2

    .line 512
    invoke-virtual {p2, p5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    .line 514
    .line 515
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇8〇()Ljava/lang/String;

    .line 516
    .line 517
    .line 518
    move-result-object p5

    .line 519
    invoke-static {p5}, Lcom/intsig/nativelib/PinyinUtil;->getPinyinOf(Ljava/lang/String;)Ljava/lang/String;

    .line 520
    .line 521
    .line 522
    move-result-object p5

    .line 523
    const-string v2, "title_sort_index"

    .line 524
    .line 525
    invoke-virtual {p2, v2, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    .line 527
    .line 528
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 529
    .line 530
    .line 531
    move-result-object p5

    .line 532
    invoke-virtual {p5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 533
    .line 534
    .line 535
    move-result-object p5

    .line 536
    invoke-virtual {p5, p1, p2, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 537
    .line 538
    .line 539
    move-result p1

    .line 540
    if-nez p1, :cond_11

    .line 541
    .line 542
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 543
    .line 544
    .line 545
    move-result-wide p1

    .line 546
    new-instance p5, Ljava/lang/StringBuilder;

    .line 547
    .line 548
    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    .line 549
    .line 550
    .line 551
    const-string v2, "updateDocTitle file may be deleted id = "

    .line 552
    .line 553
    invoke-virtual {p5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    .line 555
    .line 556
    invoke-virtual {p5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 557
    .line 558
    .line 559
    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 560
    .line 561
    .line 562
    move-result-object p1

    .line 563
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    .line 565
    .line 566
    goto :goto_8

    .line 567
    :cond_11
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 568
    .line 569
    .line 570
    move-result-object p1

    .line 571
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 572
    .line 573
    .line 574
    move-result-wide v2

    .line 575
    const/4 p2, 0x3

    .line 576
    invoke-static {p1, v2, v3, p2, p4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 577
    .line 578
    .line 579
    :cond_12
    :goto_8
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 580
    .line 581
    if-eqz p1, :cond_13

    .line 582
    .line 583
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇o〇()Z

    .line 584
    .line 585
    .line 586
    move-result p1

    .line 587
    if-ne p1, p4, :cond_13

    .line 588
    .line 589
    const/4 p1, 0x1

    .line 590
    goto :goto_9

    .line 591
    :cond_13
    const/4 p1, 0x0

    .line 592
    :goto_9
    if-eqz p1, :cond_14

    .line 593
    .line 594
    const-string p1, "batch"

    .line 595
    .line 596
    invoke-static {p1}, Lcom/intsig/appsflyer/AppsFlyerHelper;->Oo08(Ljava/lang/String;)V

    .line 597
    .line 598
    .line 599
    :cond_14
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 600
    .line 601
    .line 602
    move-result-object p1

    .line 603
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 604
    .line 605
    .line 606
    move-result-object p1

    .line 607
    const-string p2, "constant_is_add_new_doc"

    .line 608
    .line 609
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 610
    .line 611
    .line 612
    move-result p1

    .line 613
    if-eqz p1, :cond_17

    .line 614
    .line 615
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 616
    .line 617
    .line 618
    move-result-object p1

    .line 619
    invoke-virtual {p1, p3}, Landroid/app/Activity;->setResult(I)V

    .line 620
    .line 621
    .line 622
    goto :goto_a

    .line 623
    :cond_15
    const-string p1, "finishMultiPage,it is an old document."

    .line 624
    .line 625
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    .line 627
    .line 628
    if-eqz p5, :cond_16

    .line 629
    .line 630
    invoke-interface {p5}, Lcom/intsig/callback/Callback0;->call()V

    .line 631
    .line 632
    .line 633
    :cond_16
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 634
    .line 635
    .line 636
    move-result-object p1

    .line 637
    invoke-virtual {p1, p3, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 638
    .line 639
    .line 640
    :cond_17
    :goto_a
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 641
    .line 642
    if-eqz p1, :cond_18

    .line 643
    .line 644
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇80〇808〇O()Z

    .line 645
    .line 646
    .line 647
    move-result p1

    .line 648
    if-ne p1, p4, :cond_18

    .line 649
    .line 650
    goto :goto_b

    .line 651
    :cond_18
    const/4 p4, 0x0

    .line 652
    :goto_b
    if-eqz p4, :cond_19

    .line 653
    .line 654
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 655
    .line 656
    .line 657
    move-result-object p1

    .line 658
    iget-boolean p2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇o0〇8:Z

    .line 659
    .line 660
    invoke-static {p1, p2}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇00o(Landroid/content/Context;Z)V

    .line 661
    .line 662
    .line 663
    goto :goto_c

    .line 664
    :cond_19
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 665
    .line 666
    if-eqz p1, :cond_1a

    .line 667
    .line 668
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇〇808〇()Lcom/intsig/camscanner/capture/CaptureMode;

    .line 669
    .line 670
    .line 671
    move-result-object v5

    .line 672
    :cond_1a
    new-instance p1, Ljava/lang/StringBuilder;

    .line 673
    .line 674
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 675
    .line 676
    .line 677
    const-string p2, "finishMultiPage, mCaptureMode = "

    .line 678
    .line 679
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 680
    .line 681
    .line 682
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 683
    .line 684
    .line 685
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 686
    .line 687
    .line 688
    move-result-object p1

    .line 689
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    .line 691
    .line 692
    :goto_c
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 693
    .line 694
    .line 695
    move-result-object p1

    .line 696
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 697
    .line 698
    .line 699
    return-void
.end method

.method public final O0〇oO〇o(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOoo80oO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final O0〇oo()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureModeMenuShownEntity;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o0O:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O80〇O〇080()I
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o08〇〇0O()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-eq v0, v1, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/16 v0, 0x10e

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8o8O〇O()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iget v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇o88o08〇:I

    .line 20
    .line 21
    new-instance v2, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    const-string v3, "getCameraRotation4Snap  mRotation="

    .line 27
    .line 28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v1, " settingRotation="

    .line 35
    .line 36
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    const-string v2, "CaptureRefactorViewModel"

    .line 47
    .line 48
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    :goto_0
    add-int/lit16 v0, v0, 0x168

    .line 52
    .line 53
    rem-int/lit16 v0, v0, 0x168

    .line 54
    .line 55
    return v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public O880oOO08()V
    .locals 3

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v0, v0, Lcom/intsig/camscanner/capture/camera/CameraAdapterClient;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->oO〇()V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OO88〇OOO()V
    :try_end_0
    .catch Lcom/intsig/camscanner/CameraHardwareException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :catch_0
    move-exception v0

    .line 26
    const-string v1, "CaptureRefactorViewModel"

    .line 27
    .line 28
    const-string v2, "restartPreview "

    .line 29
    .line 30
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo8()V

    .line 34
    .line 35
    .line 36
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oo8ooo8O:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    const-string v0, "mCameraClientNew"

    .line 7
    .line 8
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O88o〇()Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8〇OO:Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O88〇〇o0O()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇00O0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O8OO08o(Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "intent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0:Landroid/content/Intent;

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {p1}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const-string v1, "null cannot be cast to non-null type android.content.Intent"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    check-cast v0, Landroid/content/Intent;

    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0:Landroid/content/Intent;

    .line 22
    .line 23
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 24
    .line 25
    :cond_0
    const-string v0, "doc_title"

    .line 26
    .line 27
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇80(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const-string v0, "EXTRA_LOTTERY_VALUE"

    .line 35
    .line 36
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    iput-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OOo8〇0:Ljava/lang/String;

    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO0o〇〇()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇0(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    const-string v0, "extra_scene_json"

    .line 50
    .line 51
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    const/4 v1, 0x0

    .line 56
    if-eqz v0, :cond_2

    .line 57
    .line 58
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 59
    .line 60
    .line 61
    move-result v2

    .line 62
    xor-int/lit8 v2, v2, 0x1

    .line 63
    .line 64
    if-eqz v2, :cond_1

    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_1
    move-object v0, v1

    .line 68
    :goto_0
    if-eqz v0, :cond_2

    .line 69
    .line 70
    :try_start_0
    invoke-static {v0}, Lcom/intsig/camscanner/capture/scene/CaptureSceneDataExtKt;->〇o〇(Ljava/lang/String;)Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO80OOO〇(Lcom/intsig/camscanner/capture/scene/CaptureSceneData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .line 76
    .line 77
    goto :goto_1

    .line 78
    :catch_0
    move-exception v0

    .line 79
    const-string v2, "CaptureRefactorViewModel"

    .line 80
    .line 81
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 82
    .line 83
    .line 84
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 85
    .line 86
    const-string v2, "doc_id_clllaborator"

    .line 87
    .line 88
    const/4 v3, 0x0

    .line 89
    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 90
    .line 91
    .line 92
    move-result v2

    .line 93
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->oO80(Z)V

    .line 94
    .line 95
    .line 96
    const-string v0, "extra_from_widget"

    .line 97
    .line 98
    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8o〇O0:Z

    .line 103
    .line 104
    const-string v0, "extra_start_do_camera"

    .line 105
    .line 106
    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 107
    .line 108
    .line 109
    move-result v0

    .line 110
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO8:Z

    .line 111
    .line 112
    const-string v0, "camera_ad_from_part"

    .line 113
    .line 114
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    iput-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0〇0:Ljava/lang/String;

    .line 119
    .line 120
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 121
    .line 122
    const-string v2, "tag_id"

    .line 123
    .line 124
    const-wide/16 v4, -0x1

    .line 125
    .line 126
    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 127
    .line 128
    .line 129
    move-result-wide v6

    .line 130
    invoke-virtual {v0, v6, v7}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->OO0o〇〇〇〇0(J)V

    .line 131
    .line 132
    .line 133
    const-string v0, "doc_id"

    .line 134
    .line 135
    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 136
    .line 137
    .line 138
    move-result-wide v4

    .line 139
    invoke-virtual {p0, v4, v5}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o〇(J)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇OO()Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    invoke-static {v0}, Lcom/intsig/camscanner/capture/scene/CaptureSceneDataExtKt;->〇080(Lcom/intsig/camscanner/capture/scene/CaptureSceneData;)Z

    .line 147
    .line 148
    .line 149
    move-result v0

    .line 150
    if-eqz v0, :cond_5

    .line 151
    .line 152
    invoke-virtual {p0, v3}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇Oo(Z)V

    .line 153
    .line 154
    .line 155
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇OO()Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 156
    .line 157
    .line 158
    move-result-object v0

    .line 159
    if-eqz v0, :cond_3

    .line 160
    .line 161
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->getArchiveDirData()Lcom/intsig/camscanner/capture/scene/AutoArchiveDirData;

    .line 162
    .line 163
    .line 164
    move-result-object v0

    .line 165
    goto :goto_2

    .line 166
    :cond_3
    move-object v0, v1

    .line 167
    :goto_2
    if-eqz v0, :cond_6

    .line 168
    .line 169
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇OO()Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 170
    .line 171
    .line 172
    move-result-object v0

    .line 173
    if-eqz v0, :cond_4

    .line 174
    .line 175
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->getArchiveDirData()Lcom/intsig/camscanner/capture/scene/AutoArchiveDirData;

    .line 176
    .line 177
    .line 178
    move-result-object v0

    .line 179
    if-eqz v0, :cond_4

    .line 180
    .line 181
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/scene/AutoArchiveDirData;->getDirSyncId()Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object v1

    .line 185
    :cond_4
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO0(Ljava/lang/String;)V

    .line 186
    .line 187
    .line 188
    goto :goto_3

    .line 189
    :cond_5
    const-string v0, "extra_offline_folder"

    .line 190
    .line 191
    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 192
    .line 193
    .line 194
    move-result v0

    .line 195
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇Oo(Z)V

    .line 196
    .line 197
    .line 198
    const-string v0, "extra_folder_id"

    .line 199
    .line 200
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object v0

    .line 204
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO0(Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    :cond_6
    :goto_3
    const-string v0, "team_token_id"

    .line 208
    .line 209
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object v0

    .line 213
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇080OO8〇0(Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    const-string v0, "extra_key_boolean_need_card_bag_list_direct"

    .line 217
    .line 218
    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 219
    .line 220
    .line 221
    move-result p1

    .line 222
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo0O0o8:Z

    .line 223
    .line 224
    return-void
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public O8O〇(Lcom/intsig/camscanner/capture/CaptureMode;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureModeMenuShownEntity;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {v0, p1, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureModeMenuShownEntity;-><init>(Lcom/intsig/camscanner/capture/CaptureMode;Z)V

    .line 5
    .line 6
    .line 7
    new-instance p1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v1, "showCaptureModelMenu "

    .line 13
    .line 14
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const-string v1, "CaptureRefactorViewModel"

    .line 25
    .line 26
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o0O:Landroidx/lifecycle/MutableLiveData;

    .line 30
    .line 31
    invoke-virtual {p1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 32
    .line 33
    .line 34
    return-void
.end method

.method public O8O〇88oO0()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇08O8o〇0(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O8O〇8oo08()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oooO888:Landroid/content/SharedPreferences;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    const-string v2, "pref_camera_grid_key"

    .line 7
    .line 8
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-ne v0, v2, :cond_0

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    :cond_0
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O8o08O8O(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "flashState"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v1, "updateFlashStatus --flashState:"

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const-string v1, "CaptureRefactorViewModel"

    .line 24
    .line 25
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-static {p1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_0

    .line 33
    .line 34
    return-void

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O〇8O8〇008(Ljava/lang/String;)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_3

    .line 48
    .line 49
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇08oOOO0:Ljava/lang/String;

    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo0〇Ooo:Landroidx/lifecycle/MutableLiveData;

    .line 52
    .line 53
    const/4 v1, 0x2

    .line 54
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oooO888:Landroid/content/SharedPreferences;

    .line 62
    .line 63
    if-eqz v0, :cond_1

    .line 64
    .line 65
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    goto :goto_0

    .line 70
    :cond_1
    const/4 v0, 0x0

    .line 71
    :goto_0
    if-eqz v0, :cond_2

    .line 72
    .line 73
    const-string v1, "pref_camera_flashmode_key"

    .line 74
    .line 75
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 76
    .line 77
    .line 78
    :cond_2
    if-eqz v0, :cond_3

    .line 79
    .line 80
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 81
    .line 82
    .line 83
    :cond_3
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final O8oOo80()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O8ooOoo〇(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OO〇00〇0O:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final O8〇o(Landroid/net/Uri;IZZ)V
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.intsig.camscanner.NEW_DOC"

    if-nez v0, :cond_0

    move-object v0, v1

    :cond_0
    const-string v2, "com.intsig.camscanner.NEW_IMG"

    .line 2
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "EXTRA_CAPTURE_SETTING_ROTATION"

    if-eqz v2, :cond_2

    .line 3
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    move-result-object p3

    invoke-interface {p3}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    move-result-object p3

    if-eqz p3, :cond_1

    invoke-virtual {p3, p2}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O8〇〇o(Landroid/content/Intent;)V

    .line 5
    :cond_1
    invoke-virtual {p2, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8o8O〇O()I

    move-result p1

    .line 7
    invoke-virtual {p2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const/4 p3, -0x1

    invoke-virtual {p1, p3, p2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void

    :cond_2
    const-string v2, "android.intent.action.VIEW"

    .line 10
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const-string v4, "CaptureRefactorViewModel"

    if-eqz v2, :cond_3

    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    .line 12
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doBackIntent callingpackegae="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 13
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doBackIntent action="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v5

    const-class v6, Lcom/intsig/camscanner/ImageScannerActivity;

    invoke-direct {v2, v0, p1, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8o8O〇O()I

    move-result p1

    .line 16
    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 17
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->ooOO()Lcom/intsig/camscanner/capture/CaptureModeMenuManager;

    move-result-object p1

    const/4 v3, 0x0

    if-eqz p1, :cond_4

    sget-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_MULTI:Lcom/intsig/camscanner/capture/CaptureMode;

    invoke-virtual {p1, v5}, Lcom/intsig/camscanner/capture/CaptureModeMenuManager;->oO00OOO(Lcom/intsig/camscanner/capture/CaptureMode;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto :goto_0

    :cond_4
    move-object p1, v3

    :goto_0
    const-string v5, "EXTRA_INCLUDE_MULTI_CAPTURE"

    .line 18
    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p1, "scanner_image_src"

    .line 19
    invoke-virtual {v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 20
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇OO()Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    move-result-object p1

    const/4 p2, 0x1

    if-eqz p1, :cond_6

    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇OO()Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-static {p1, p2}, Lcom/intsig/camscanner/capture/scene/CaptureSceneDataExtKt;->Oo08(Lcom/intsig/camscanner/capture/scene/CaptureSceneData;Z)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_5
    move-object p1, v3

    :goto_1
    const-string v5, "extra_scene_json"

    .line 22
    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_6
    const-string p1, "extra_is_waiting_big_image_for_signature"

    .line 23
    invoke-virtual {v2, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "isCaptureguide"

    .line 24
    invoke-virtual {v2, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "EXTRA_LOTTERY_VALUE"

    .line 25
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo〇o()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v2, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    const/4 p3, 0x0

    if-eqz p1, :cond_7

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->〇8o8o〇()Z

    move-result p1

    if-ne p1, p2, :cond_7

    const/4 p1, 0x1

    goto :goto_2

    :cond_7
    const/4 p1, 0x0

    :goto_2
    const-string p4, "extra_doc_info"

    const-string v5, "mode_type"

    if-eqz p1, :cond_8

    .line 27
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->OCR:Lcom/intsig/camscanner/capture/CaptureMode;

    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/16 p1, 0x7a

    .line 28
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo0oO〇O〇O(I)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    move-result-object p1

    .line 29
    invoke-virtual {v2, p4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p1, "CSScan"

    const-string p4, "scan_ocr_photo_ok"

    .line 30
    invoke-static {p1, p4}, Lcom/intsig/camscanner/log/LogAgentData;->o800o8O(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    move-result-wide v5

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "doBackIntent --> isOCRMode --> createParcelDocInfo --> docId:"

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 32
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 33
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    if-eqz p1, :cond_9

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->o〇0()Z

    move-result p1

    if-ne p1, p2, :cond_9

    const/4 p1, 0x1

    goto :goto_3

    :cond_9
    const/4 p1, 0x0

    :goto_3
    if-eqz p1, :cond_a

    .line 34
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->EXCEL:Lcom/intsig/camscanner/capture/CaptureMode;

    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto/16 :goto_e

    .line 35
    :cond_a
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    if-eqz p1, :cond_b

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->OO0o〇〇()Z

    move-result p1

    if-ne p1, p2, :cond_b

    const/4 p1, 0x1

    goto :goto_4

    :cond_b
    const/4 p1, 0x0

    :goto_4
    if-eqz p1, :cond_d

    .line 36
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    if-eqz p1, :cond_c

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇〇808〇()Lcom/intsig/camscanner/capture/CaptureMode;

    move-result-object p1

    goto :goto_5

    :cond_c
    move-object p1, v3

    :goto_5
    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 37
    invoke-virtual {p0, p3}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo0oO〇O〇O(I)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    move-result-object p1

    .line 38
    invoke-virtual {v2, p4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_e

    .line 39
    :cond_d
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    if-eqz p1, :cond_e

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇O〇()Z

    move-result p1

    if-ne p1, p2, :cond_e

    const/4 p1, 0x1

    goto :goto_6

    :cond_e
    const/4 p1, 0x0

    :goto_6
    if-eqz p1, :cond_f

    .line 40
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->IMAGE_RESTORE:Lcom/intsig/camscanner/capture/CaptureMode;

    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto/16 :goto_e

    .line 41
    :cond_f
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    if-eqz p1, :cond_10

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->〇080()Z

    move-result p1

    if-ne p1, p2, :cond_10

    const/4 p1, 0x1

    goto :goto_7

    :cond_10
    const/4 p1, 0x0

    :goto_7
    if-eqz p1, :cond_11

    .line 42
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->DOC_TO_WORD:Lcom/intsig/camscanner/capture/CaptureMode;

    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_e

    .line 43
    :cond_11
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    if-eqz p1, :cond_12

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->Oo08()Z

    move-result p1

    if-ne p1, p2, :cond_12

    const/4 p1, 0x1

    goto :goto_8

    :cond_12
    const/4 p1, 0x0

    :goto_8
    if-eqz p1, :cond_13

    .line 44
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->DOC_TO_EXCEL:Lcom/intsig/camscanner/capture/CaptureMode;

    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_e

    .line 45
    :cond_13
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    move-result-object p1

    if-eqz p1, :cond_14

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    move-result-object p1

    goto :goto_9

    :cond_14
    move-object p1, v3

    :goto_9
    if-eqz p1, :cond_15

    .line 46
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOO()Lcom/intsig/camscanner/capture/CaptureMode;

    move-result-object p4

    goto :goto_a

    :cond_15
    move-object p4, v3

    :goto_a
    sget-object v6, Lcom/intsig/camscanner/capture/CaptureMode;->WRITING_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

    if-eq p4, v6, :cond_19

    if-eqz p1, :cond_16

    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo8〇〇()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    move-result-object p1

    if-eqz p1, :cond_16

    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOO()Lcom/intsig/camscanner/capture/CaptureMode;

    move-result-object p1

    goto :goto_b

    :cond_16
    move-object p1, v3

    :goto_b
    if-ne p1, v6, :cond_17

    goto :goto_d

    .line 47
    :cond_17
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    if-eqz p1, :cond_18

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇〇808〇()Lcom/intsig/camscanner/capture/CaptureMode;

    move-result-object p1

    goto :goto_c

    :cond_18
    move-object p1, v3

    :goto_c
    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_e

    .line 48
    :cond_19
    :goto_d
    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string p1, "EXTRA_FROM_PART"

    .line 49
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇〇0〇88()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v2, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    :goto_e
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    move-result-object p1

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    move-result-object p1

    if-eqz p1, :cond_1a

    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O8〇〇o(Landroid/content/Intent;)V

    :cond_1a
    const-string p1, "extra_offline_folder"

    .line 51
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0000OOO()Z

    move-result p4

    invoke-virtual {v2, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "doc_title"

    .line 52
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO0o〇〇()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v2, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "doc_id"

    .line 53
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    move-result-wide v5

    invoke-virtual {v2, p1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string p1, "extra_entrance"

    .line 54
    iget-object p4, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO0880O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    invoke-virtual {v2, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇o〇()Ljava/lang/String;

    move-result-object p1

    const-string p4, "team_token"

    invoke-virtual {v2, p4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1b

    const-string p1, "extra_from_widget"

    .line 57
    iget-boolean p4, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8o〇O0:Z

    invoke-virtual {v2, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "extra_start_do_camera"

    .line 58
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇Oo()Z

    move-result p4

    invoke-virtual {v2, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "extra_folder_id"

    .line 59
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇0o8O〇〇()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v2, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇o00〇〇Oo()J

    move-result-wide v0

    const-string p1, "tag_id"

    invoke-virtual {v2, p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 61
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const/16 p4, 0xca

    invoke-virtual {p1, v2, p4}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_f

    .line 62
    :cond_1b
    iget-boolean p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOoo80oO:Z

    if-eqz p1, :cond_1c

    .line 63
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string p4, "image_sync_id"

    invoke-virtual {p1, p4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 64
    invoke-virtual {v2, p4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    const-string p4, "pageuri"

    invoke-virtual {v2, p4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 66
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const/16 p4, 0xcd

    invoke-virtual {p1, v2, p4}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_f

    .line 67
    :cond_1c
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const/16 p4, 0x64

    invoke-virtual {p1, v2, p4}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 68
    :goto_f
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    if-eqz p1, :cond_1d

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇80〇808〇O()Z

    move-result p1

    if-ne p1, p2, :cond_1d

    goto :goto_10

    :cond_1d
    const/4 p2, 0x0

    :goto_10
    if-eqz p2, :cond_1e

    .line 69
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iget-boolean p2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇o0〇8:Z

    invoke-static {p1, p2}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇00o(Landroid/content/Context;Z)V

    goto :goto_11

    .line 70
    :cond_1e
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    if-eqz p1, :cond_1f

    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇〇808〇()Lcom/intsig/camscanner/capture/CaptureMode;

    move-result-object v3

    :cond_1f
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "doBackIntent, mCaptureMode = "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :goto_11
    return-void
.end method

.method public final OO(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oooO888:Landroid/content/SharedPreferences;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const-string v1, "KEY_USE_GRADIENTER"

    .line 12
    .line 13
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 20
    .line 21
    .line 22
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o08O()Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iget-object v0, v0, Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;->〇80〇808〇O:Lcom/intsig/camscanner/capture/util/SensorUtil;

    .line 27
    .line 28
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/util/SensorUtil;->O8(Z)Lcom/intsig/camscanner/capture/util/SensorUtil;

    .line 29
    .line 30
    .line 31
    if-eqz p1, :cond_1

    .line 32
    .line 33
    const-string p1, "on"

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const-string p1, "off"

    .line 37
    .line 38
    :goto_0
    const-string v0, "CSScan"

    .line 39
    .line 40
    const-string v1, "spirit_level"

    .line 41
    .line 42
    const-string v2, "type"

    .line 43
    .line 44
    invoke-static {v0, v1, v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public OO0o〇〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->O8()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OO0o〇〇〇〇0()Landroid/os/Handler;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OO0o〇〇〇〇0()Landroid/os/Handler;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OO0〇〇8(ZLcom/intsig/callback/Callback0;)V
    .locals 6

    .line 1
    const/4 v2, 0x0

    .line 2
    const/4 v3, 0x0

    .line 3
    const/4 v4, 0x0

    .line 4
    move-object v0, p0

    .line 5
    move v1, p1

    .line 6
    move-object v5, p2

    .line 7
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O0〇OO8(ZZILcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/callback/Callback0;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final OO88o(Z)V
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇00〇080〇(Z)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇00(Z)V

    .line 5
    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    const-string p1, "on"

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string p1, "off"

    .line 13
    .line 14
    :goto_0
    const-string v0, "CSScan"

    .line 15
    .line 16
    const-string v1, "auto_crop"

    .line 17
    .line 18
    const-string v2, "type"

    .line 19
    .line 20
    invoke-static {v0, v1, v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public OO8oO0o〇()Lcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOoO8OO〇:Lcom/intsig/camscanner/capture/core/SaveCaptureImageCallback;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OOO(Landroid/net/Uri;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    new-instance v2, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$doSelectPictureDone$1;

    .line 10
    .line 11
    invoke-direct {v2, p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$doSelectPictureDone$1;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;Landroid/net/Uri;)V

    .line 12
    .line 13
    .line 14
    const/4 p1, 0x0

    .line 15
    invoke-direct {v0, v1, v2, p1}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const-string p1, "CaptureRefactorViewModel"

    .line 23
    .line 24
    const-string v0, "doSelectPictureDone, uri is null"

    .line 25
    .line 26
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    const/4 p1, 0x0

    .line 30
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0OOo〇0(Z)V

    .line 31
    .line 32
    .line 33
    :goto_0
    return-void
    .line 34
.end method

.method public OOO8o〇〇()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇〇808〇()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const-string v1, "CaptureRefactorViewModel"

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OO88〇OOO()V
    :try_end_0
    .catch Lcom/intsig/camscanner/CameraHardwareException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :catch_0
    move-exception v0

    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO()V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇800OO〇0O()Landroid/view/SurfaceHolder;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    if-eqz v0, :cond_3

    .line 38
    .line 39
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇O:Z

    .line 40
    .line 41
    if-nez v0, :cond_1

    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO0o〇〇〇〇0()Landroid/os/Handler;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    const/4 v1, 0x3

    .line 48
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 49
    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇O00()Z

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    if-eqz v0, :cond_2

    .line 61
    .line 62
    const-string v0, "onResume CameraHardwareException"

    .line 63
    .line 64
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO()V

    .line 68
    .line 69
    .line 70
    return-void

    .line 71
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO8〇()V

    .line 72
    .line 73
    .line 74
    :cond_3
    :goto_1
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final OOo(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO0880O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public OOo0O(ILandroid/content/Intent;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "finishOnePage resultCode="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "CaptureRefactorViewModel"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 v0, -0x1

    .line 24
    if-ne p1, v0, :cond_2

    .line 25
    .line 26
    if-eqz p2, :cond_1

    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    if-eqz v0, :cond_0

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o0O0()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    goto :goto_0

    .line 43
    :cond_0
    const/4 v0, 0x0

    .line 44
    :goto_0
    const-string v1, "extra_doc_type"

    .line 45
    .line 46
    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 47
    .line 48
    .line 49
    :cond_1
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO88〇OOO(Landroid/content/Intent;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 64
    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8O0()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 72
    .line 73
    .line 74
    :goto_1
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public final OOo88OOo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇o88o08〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OOo8o〇O(Lcom/intsig/camscanner/capture/CaptureMode;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureModeMenuShownEntity;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p1, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureModeMenuShownEntity;-><init>(Lcom/intsig/camscanner/capture/CaptureMode;Z)V

    .line 5
    .line 6
    .line 7
    new-instance p1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v1, "hideCaptureModelMenu "

    .line 13
    .line 14
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const-string v1, "CaptureRefactorViewModel"

    .line 25
    .line 26
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o0O:Landroidx/lifecycle/MutableLiveData;

    .line 30
    .line 31
    invoke-virtual {p1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 32
    .line 33
    .line 34
    return-void
.end method

.method public OOoo()Lcom/intsig/camscanner/capture/SupportCaptureModeOption;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇80O8o8O〇:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 6
    .line 7
    :cond_0
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OO〇0008O8(Lcom/intsig/camscanner/capture/core/MoreSettingLayoutStatusListener;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8〇o88:Ljava/util/Set;

    .line 4
    .line 5
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public Oo(Lcom/intsig/camscanner/capture/core/MoreSettingLayoutStatusListener;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8〇o88:Ljava/util/Set;

    .line 4
    .line 5
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public Oo08()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->Oo08()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo08OO8oO(Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Ooo08:Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final Oo0O080(Lcom/intsig/callback/Callback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/callback/Callback<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8〇o〇88:Lcom/intsig/callback/Callback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final Oo0oOo〇0()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08O〇00〇o:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo0oO〇O〇O(I)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 7
    .line 8
    .line 9
    move-result-wide v1

    .line 10
    iput-wide v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    iput-object v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0000OOO()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    iput-boolean v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇08O〇00〇o:Z

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 25
    .line 26
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇o〇()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    iput-object v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 31
    .line 32
    iput p1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇OO()Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    iput-object p1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇0O:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O0o8〇O()Z

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    if-eqz p1, :cond_0

    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO0o〇〇()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    iput-object p1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 51
    .line 52
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇o00〇〇Oo()J

    .line 55
    .line 56
    .line 57
    move-result-wide v1

    .line 58
    const-wide/16 v3, -0x1

    .line 59
    .line 60
    cmp-long p1, v1, v3

    .line 61
    .line 62
    if-lez p1, :cond_1

    .line 63
    .line 64
    new-instance p1, Ljava/util/ArrayList;

    .line 65
    .line 66
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .line 68
    .line 69
    iput-object p1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo〇8o008:Ljava/util/List;

    .line 70
    .line 71
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 72
    .line 73
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇o00〇〇Oo()J

    .line 74
    .line 75
    .line 76
    move-result-wide v1

    .line 77
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    .line 83
    .line 84
    :cond_1
    return-object v0
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public Oo8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O〇〇O8:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo8Oo00oo(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->Oo8Oo00oo(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public OoO8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8oOOo:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final OoOOo8()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇08oOOO0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OoO〇()Lcom/intsig/camscanner/capture/inputdata/CaptureSceneInputData;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8〇OOoooo:Lcom/intsig/camscanner/capture/inputdata/CaptureSceneInputData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Ooo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇o88o08〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final Ooo8()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇00O:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Ooo8〇〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇o〇()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final Oo〇()Lcom/intsig/camscanner/capture/SupportCaptureModeOption;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇80O8o8O〇:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo〇O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo〇O8o〇8([BII)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇oo〇O〇80:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    new-instance v0, L〇8o8O〇O/〇080;

    .line 11
    .line 12
    invoke-direct {v0, p0}, L〇8o8O〇O/〇080;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)V

    .line 13
    .line 14
    .line 15
    new-instance v1, L〇8o8O〇O/〇o00〇〇Oo;

    .line 16
    .line 17
    invoke-direct {v1, p0}, L〇8o8O〇O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    const/4 v3, 0x1

    .line 25
    const/4 v4, 0x0

    .line 26
    if-eqz v2, :cond_1

    .line 27
    .line 28
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇oo〇()Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-nez v2, :cond_1

    .line 33
    .line 34
    const/4 v2, 0x1

    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const/4 v2, 0x0

    .line 37
    :goto_0
    if-eqz v2, :cond_4

    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0〇o:Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;

    .line 40
    .line 41
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;->〇o00〇〇Oo()Z

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    if-eqz v2, :cond_2

    .line 46
    .line 47
    return-void

    .line 48
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 49
    .line 50
    .line 51
    move-result-wide v5

    .line 52
    iget-wide v7, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o〇88〇8:J

    .line 53
    .line 54
    sub-long/2addr v5, v7

    .line 55
    const-wide/16 v7, 0x190

    .line 56
    .line 57
    cmp-long v2, v5, v7

    .line 58
    .line 59
    if-gez v2, :cond_3

    .line 60
    .line 61
    return-void

    .line 62
    :cond_3
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0〇o:Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;

    .line 63
    .line 64
    new-instance v5, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$onIdCardDetectPreview$1;

    .line 65
    .line 66
    invoke-direct {v5, p0, p2, p3, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$onIdCardDetectPreview$1;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;II[B)V

    .line 67
    .line 68
    .line 69
    const/4 p1, 0x2

    .line 70
    new-array p1, p1, [Lkotlin/Triple;

    .line 71
    .line 72
    new-instance p2, Lkotlin/Triple;

    .line 73
    .line 74
    const/16 p3, 0x3e9

    .line 75
    .line 76
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 77
    .line 78
    .line 79
    move-result-object p3

    .line 80
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇〇0〇〇0()Z

    .line 81
    .line 82
    .line 83
    move-result v6

    .line 84
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 85
    .line 86
    .line 87
    move-result-object v6

    .line 88
    invoke-direct {p2, p3, v6, v0}, Lkotlin/Triple;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 89
    .line 90
    .line 91
    aput-object p2, p1, v4

    .line 92
    .line 93
    new-instance p2, Lkotlin/Triple;

    .line 94
    .line 95
    const/16 p3, 0x3e8

    .line 96
    .line 97
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 98
    .line 99
    .line 100
    move-result-object p3

    .line 101
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 102
    .line 103
    invoke-direct {p2, p3, v0, v1}, Lkotlin/Triple;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 104
    .line 105
    .line 106
    aput-object p2, p1, v3

    .line 107
    .line 108
    invoke-virtual {v2, v5, p1}, Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;->o〇0(Lkotlin/jvm/functions/Function0;[Lkotlin/Triple;)V

    .line 109
    .line 110
    .line 111
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 112
    .line 113
    .line 114
    move-result-wide p1

    .line 115
    iput-wide p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o〇88〇8:J

    .line 116
    .line 117
    goto :goto_1

    .line 118
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 119
    .line 120
    .line 121
    move-result-object p1

    .line 122
    invoke-interface {p1, v4}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇0〇O0088o(Z)V

    .line 123
    .line 124
    .line 125
    :goto_1
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
.end method

.method public Oo〇o()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O0〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇0(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇8〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8〇OO0〇0o:Ljava/lang/String;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public O〇08()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0〇o:Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;->〇080()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇0〇o808〇(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8oOOo:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public O〇8O8〇008()Landroid/view/View;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->O〇8O8〇008()Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O〇8oOo8O()Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08〇o0O:Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇OO()Lcom/intsig/camscanner/capture/scene/CaptureSceneData;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->ooo0〇〇O:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇Oo()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8〇80o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇Oooo〇〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇o0〇8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final O〇O〇oO()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O888o0o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OO8ooO8〇:I

    .line 8
    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    iput v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OO8ooO8〇:I

    .line 13
    .line 14
    :cond_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O〇oO〇oo8o()Lcom/intsig/camscanner/capture/ppt/PPTScaleCallback;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o00〇88〇08:Lcom/intsig/camscanner/capture/ppt/PPTScaleCallback;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O〇〇()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8oOOo:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getActivity()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getRotation()I
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8o8O〇O()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o0()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->getMaxZoom()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o08O()Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo80:Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    const-string v0, "settingControl"

    .line 7
    .line 8
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o08oOO()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇〇0o〇〇0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o08o〇0(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇80O8o8O〇:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o0O0()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO0880O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o0O〇8o0O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O0〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o0oO()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o8()Lcom/intsig/app/AlertDialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->o8()Lcom/intsig/app/AlertDialog;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o800o8O()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->o800o8O()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o80ooO()F
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08〇0〇o〇8()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->getMinZoom()F

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    return v0

    .line 23
    :cond_1
    :goto_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 24
    .line 25
    return v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o88O8()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureModeMenuShownEntity;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O88O:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o88O〇8()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO00〇o:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o88〇OO08〇(Lcom/intsig/camscanner/capture/ppt/PPTScaleCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o00〇88〇08:Lcom/intsig/camscanner/capture/ppt/PPTScaleCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o8O0()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o8O〇(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->oOO〇〇(Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o8oO〇(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->o8oO〇(Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final o8o〇〇0O()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o8〇()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇oO08〇o0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final oO0(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->Oooo8o0〇(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public oO00OOO(Lcom/intsig/camscanner/capture/CaptureMode;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->oO00OOO(Lcom/intsig/camscanner/capture/CaptureMode;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final oO0〇〇O8o(Z)V
    .locals 4

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string v0, "on"

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string v0, "off"

    .line 7
    .line 8
    :goto_0
    const-string v1, "CSScan"

    .line 9
    .line 10
    const-string v2, "auto_camera"

    .line 11
    .line 12
    const-string v3, "type"

    .line 13
    .line 14
    invoke-static {v1, v2, v3, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->ooo08Oo(Z)V

    .line 18
    .line 19
    .line 20
    if-eqz p1, :cond_1

    .line 21
    .line 22
    const/4 v0, 0x2

    .line 23
    goto :goto_1

    .line 24
    :cond_1
    const/4 v0, 0x0

    .line 25
    :goto_1
    iput v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OO8ooO8〇:I

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Ooo08:Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;

    .line 28
    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/contract/AutoCaptureCallback;->〇O〇(Z)V

    .line 32
    .line 33
    .line 34
    :cond_2
    if-eqz p1, :cond_3

    .line 35
    .line 36
    const/4 p1, 0x1

    .line 37
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O08O0〇O(Z)V

    .line 38
    .line 39
    .line 40
    :cond_3
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final oO0〇〇o8〇(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇oO:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final oO8008O()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO〇〇:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final oO80OOO〇(Lcom/intsig/camscanner/capture/scene/CaptureSceneData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->ooo0〇〇O:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final oO8o()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO0880O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oOo(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇08〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public oO〇()Z
    .locals 2

    .line 1
    const-string v0, "CaptureRefactorViewModel"

    .line 2
    .line 3
    const-string v1, "setCameraParameters()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O08000()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v1, 0x1

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0OOo〇0(Z)V

    .line 20
    .line 21
    .line 22
    const/4 v0, 0x0

    .line 23
    return v0

    .line 24
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o08O()Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;->〇80〇808〇O()V

    .line 29
    .line 30
    .line 31
    return v1
    .line 32
    .line 33
.end method

.method protected onCleared()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/lifecycle/ViewModel;->onCleared()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O0O:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;

    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08〇o0O:Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oo()V
    .locals 4

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    const/16 v2, 0x190

    .line 6
    .line 7
    int-to-long v2, v2

    .line 8
    add-long/2addr v0, v2

    .line 9
    iput-wide v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o〇88〇8:J

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0〇o:Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;->oO80()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final oo08OO〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final oo0O〇0〇〇〇()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇o〇Oo88:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public ooOO()Lcom/intsig/camscanner/capture/CaptureModeMenuManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/capture/CaptureModeMenuManager;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public ooO〇00O()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o〇8()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO00〇o:Landroidx/lifecycle/MutableLiveData;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 12
    .line 13
    .line 14
    const-wide/16 v0, 0x0

    .line 15
    .line 16
    iput-wide v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇800OO〇0O:J

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇oo〇O〇80:Ljava/util/List;

    .line 19
    .line 20
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0:Landroid/content/Intent;

    .line 24
    .line 25
    const-wide/16 v1, -0x1

    .line 26
    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    const-string v3, "doc_id"

    .line 30
    .line 31
    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 32
    .line 33
    .line 34
    move-result-wide v1

    .line 35
    :cond_0
    invoke-virtual {p0, v1, v2}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇o〇(J)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0:Landroid/content/Intent;

    .line 39
    .line 40
    if-eqz v0, :cond_1

    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-virtual {v1, v0}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    .line 47
    .line 48
    .line 49
    :cond_1
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public ooo0〇O88O()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->〇〇888()Lcom/intsig/camscanner/capture/CaptureMode;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    move-object v0, v1

    .line 12
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 13
    .line 14
    if-eqz v2, :cond_1

    .line 15
    .line 16
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;->〇o00〇〇Oo()Lcom/intsig/camscanner/capture/CaptureMode;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    :cond_1
    if-ne v0, v1, :cond_2

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    goto :goto_1

    .line 24
    :cond_2
    const/4 v0, 0x0

    .line 25
    :goto_1
    return v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final ooo8o〇o〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOoo80oO:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public ooo〇8oO()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0〇o:Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/PreviewRecognizeObserverChains;->oO80()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public ooo〇〇O〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo0O0o8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final oo〇()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->oO80()V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->OOO〇O0(Z)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o〇00O(Lcom/intsig/camscanner/util/PremiumParcelSize;)V
    .locals 9
    .param p1    # Lcom/intsig/camscanner/util/PremiumParcelSize;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "size"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "tryToSetPixelSize"

    .line 7
    .line 8
    const-string v1, "CaptureRefactorViewModel"

    .line 9
    .line 10
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8〇OO:Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;

    .line 14
    .line 15
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;->o8(Lcom/intsig/camscanner/util/PremiumParcelSize;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇O8〇〇o()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-nez v0, :cond_0

    .line 28
    .line 29
    sget-object v0, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇080:Lcom/intsig/camscanner/ads/reward/AdRewardedManager;

    .line 30
    .line 31
    sget-object v2, Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;->HD_MODEL:Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇oo〇(Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-nez v0, :cond_0

    .line 38
    .line 39
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 40
    .line 41
    invoke-direct {p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 42
    .line 43
    .line 44
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPop:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 45
    .line 46
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 51
    .line 52
    iput-object v0, p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 53
    .line 54
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_HD_PICTURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 55
    .line 56
    iput-object v0, p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 57
    .line 58
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    invoke-static {v0, p1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 67
    .line 68
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇O8〇〇o()Z

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    if-eqz v0, :cond_1

    .line 73
    .line 74
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    const/4 v2, 0x2

    .line 79
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/VipUtil;->〇o〇(Landroid/content/Context;I)V

    .line 80
    .line 81
    .line 82
    :cond_1
    const-string v0, "CSScan"

    .line 83
    .line 84
    const-string v2, "hd"

    .line 85
    .line 86
    const/4 v3, 0x1

    .line 87
    new-array v3, v3, [Landroid/util/Pair;

    .line 88
    .line 89
    new-instance v4, Landroid/util/Pair;

    .line 90
    .line 91
    const-string v5, "type"

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 94
    .line 95
    .line 96
    move-result v6

    .line 97
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 98
    .line 99
    .line 100
    move-result v7

    .line 101
    new-instance v8, Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    const-string v6, "*"

    .line 110
    .line 111
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v6

    .line 121
    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 122
    .line 123
    .line 124
    const/4 v5, 0x0

    .line 125
    aput-object v4, v3, v5

    .line 126
    .line 127
    invoke-static {v0, v2, v3}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 135
    .line 136
    .line 137
    move-result v2

    .line 138
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 139
    .line 140
    .line 141
    move-result v3

    .line 142
    invoke-interface {v0, v2, v3}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->Oooo8o0〇(II)V

    .line 143
    .line 144
    .line 145
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇oOoo〇(Lcom/intsig/camscanner/util/PremiumParcelSize;)V

    .line 146
    .line 147
    .line 148
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo0〇Ooo:Landroidx/lifecycle/MutableLiveData;

    .line 149
    .line 150
    const/4 v0, 0x4

    .line 151
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    invoke-virtual {p1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 156
    .line 157
    .line 158
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OO〇00〇0O:Landroidx/lifecycle/MutableLiveData;

    .line 159
    .line 160
    const/4 v0, -0x1

    .line 161
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 162
    .line 163
    .line 164
    move-result-object v0

    .line 165
    invoke-virtual {p1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    .line 167
    .line 168
    goto :goto_0

    .line 169
    :catch_0
    move-exception p1

    .line 170
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 171
    .line 172
    .line 173
    :goto_0
    return-void
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public final o〇00O0O〇o(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oo8ooo8O:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final o〇0OOo〇0(Z)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const v0, 0x7f13057d

    .line 8
    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 11
    .line 12
    .line 13
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    const/4 v1, 0x0

    .line 17
    if-eqz p1, :cond_1

    .line 18
    .line 19
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;->〇o〇()Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    if-ne p1, v0, :cond_1

    .line 24
    .line 25
    const/4 p1, 0x1

    .line 26
    goto :goto_0

    .line 27
    :cond_1
    const/4 p1, 0x0

    .line 28
    :goto_0
    if-eqz p1, :cond_2

    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇oo〇O〇80:Ljava/util/List;

    .line 31
    .line 32
    check-cast p1, Ljava/util/Collection;

    .line 33
    .line 34
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    xor-int/2addr p1, v0

    .line 39
    if-eqz p1, :cond_2

    .line 40
    .line 41
    const/4 p1, 0x0

    .line 42
    invoke-virtual {p0, v1, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO0〇〇8(ZLcom/intsig/callback/Callback0;)V

    .line 43
    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 51
    .line 52
    .line 53
    :goto_1
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final o〇0o〇〇()Lcom/intsig/camscanner/view/ZoomControl;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o〇0〇(Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo80:Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o〇8()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->o〇8()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o〇8oOO88()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇8〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o〇O()Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇O8〇〇o(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->o〇O8〇〇o(Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o〇OOo000()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->Oo08()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08〇0〇o〇8()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    const/4 v2, 0x0

    .line 23
    const/16 v3, 0x63

    .line 24
    .line 25
    const-string v4, "CaptureRefactorViewModel"

    .line 26
    .line 27
    if-eqz v1, :cond_2

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0()I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    int-to-float v1, v1

    .line 34
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 35
    .line 36
    .line 37
    move-result v5

    .line 38
    cmpg-float v1, v1, v5

    .line 39
    .line 40
    if-nez v1, :cond_1

    .line 41
    .line 42
    const/4 v1, 0x1

    .line 43
    goto :goto_0

    .line 44
    :cond_1
    const/4 v1, 0x0

    .line 45
    :goto_0
    if-nez v1, :cond_2

    .line 46
    .line 47
    const/high16 v0, 0x3f800000    # 1.0f

    .line 48
    .line 49
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    sub-float/2addr v0, v1

    .line 54
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0()I

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    int-to-float v1, v1

    .line 59
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 60
    .line 61
    .line 62
    move-result v5

    .line 63
    sub-float/2addr v1, v5

    .line 64
    div-float/2addr v0, v1

    .line 65
    int-to-float v1, v3

    .line 66
    mul-float v0, v0, v1

    .line 67
    .line 68
    float-to-int v0, v0

    .line 69
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 70
    .line 71
    .line 72
    move-result v1

    .line 73
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0()I

    .line 74
    .line 75
    .line 76
    move-result v5

    .line 77
    new-instance v6, Ljava/lang/StringBuilder;

    .line 78
    .line 79
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .line 81
    .line 82
    const-string v7, "resetCameraZoomValue getMinZoom: "

    .line 83
    .line 84
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    const-string v1, "   maxZoomValue:  "

    .line 91
    .line 92
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 106
    .line 107
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 108
    .line 109
    .line 110
    move-result v1

    .line 111
    new-instance v5, Ljava/lang/StringBuilder;

    .line 112
    .line 113
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .line 115
    .line 116
    const-string v6, "reInitializeZoom() zoomValue="

    .line 117
    .line 118
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object v1

    .line 128
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 132
    .line 133
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->Oo08(I)V

    .line 134
    .line 135
    .line 136
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 137
    .line 138
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->oO80(I)V

    .line 139
    .line 140
    .line 141
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 142
    .line 143
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 144
    .line 145
    .line 146
    move-result-object v2

    .line 147
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O8ooOoo〇()Z

    .line 148
    .line 149
    .line 150
    move-result v2

    .line 151
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/ZoomControl;->〇o〇(Z)V

    .line 152
    .line 153
    .line 154
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 155
    .line 156
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 157
    .line 158
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇080()I

    .line 159
    .line 160
    .line 161
    move-result v2

    .line 162
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/ZoomControl;->Oo08(I)V

    .line 163
    .line 164
    .line 165
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 166
    .line 167
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/view/ZoomControl;->O8(I)V

    .line 168
    .line 169
    .line 170
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

    .line 171
    .line 172
    if-eqz v1, :cond_3

    .line 173
    .line 174
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 175
    .line 176
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇080()I

    .line 177
    .line 178
    .line 179
    move-result v2

    .line 180
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o00〇〇Oo(I)V

    .line 181
    .line 182
    .line 183
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

    .line 184
    .line 185
    if-eqz v1, :cond_4

    .line 186
    .line 187
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/capture/CustomSeekBar;->O8(I)V

    .line 188
    .line 189
    .line 190
    :cond_4
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 191
    .line 192
    new-instance v2, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CustomZoomControlListener;

    .line 193
    .line 194
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CustomZoomControlListener;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)V

    .line 195
    .line 196
    .line 197
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/ZoomControl;->〇o00〇〇Oo(Lcom/intsig/camscanner/view/ZoomControl$OnZoomChangedListener;)V

    .line 198
    .line 199
    .line 200
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 201
    .line 202
    .line 203
    move-result-object v1

    .line 204
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇80〇808〇O()V

    .line 205
    .line 206
    .line 207
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 208
    .line 209
    .line 210
    move-result-object v1

    .line 211
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇8(I)V

    .line 212
    .line 213
    .line 214
    return-void
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
.end method

.method public o〇o()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇800OO〇0O:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇〇0〇()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->o〇〇0〇()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇〇0〇88()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO0880O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$WhenMappings;->〇080:[I

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    aget v0, v1, v0

    .line 10
    .line 11
    packed-switch v0, :pswitch_data_0

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const/4 v1, 0x0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOO()Lcom/intsig/camscanner/capture/CaptureMode;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    goto/16 :goto_0

    .line 26
    .line 27
    :pswitch_0
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_DOC42_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const-string v1, "WIDGET_DOC42_SCAN_SINGLE.toTrackerValue()"

    .line 34
    .line 35
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    goto/16 :goto_1

    .line 39
    .line 40
    :pswitch_1
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC11_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    const-string v1, "WIDGET_FUNC11_SCAN_SINGLE.toTrackerValue()"

    .line 47
    .line 48
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    goto/16 :goto_1

    .line 52
    .line 53
    :pswitch_2
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC22_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    const-string v1, "WIDGET_FUNC22_SCAN_SINGLE.toTrackerValue()"

    .line 60
    .line 61
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    goto/16 :goto_1

    .line 65
    .line 66
    :pswitch_3
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC11_SCAN_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 67
    .line 68
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    const-string v1, "WIDGET_FUNC11_SCAN_BATCH.toTrackerValue()"

    .line 73
    .line 74
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    goto/16 :goto_1

    .line 78
    .line 79
    :pswitch_4
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC22_SCAN_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 80
    .line 81
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    const-string v1, "WIDGET_FUNC22_SCAN_BATCH.toTrackerValue()"

    .line 86
    .line 87
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    goto/16 :goto_1

    .line 91
    .line 92
    :pswitch_5
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC11_QR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 93
    .line 94
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    const-string v1, "WIDGET_FUNC11_QR.toTrackerValue()"

    .line 99
    .line 100
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    goto/16 :goto_1

    .line 104
    .line 105
    :pswitch_6
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC22_QR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 106
    .line 107
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    const-string v1, "WIDGET_FUNC22_QR.toTrackerValue()"

    .line 112
    .line 113
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    goto/16 :goto_1

    .line 117
    .line 118
    :pswitch_7
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_SEARCH42_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 119
    .line 120
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    const-string v1, "WIDGET_SEARCH42_SCAN_SINGLE.toTrackerValue()"

    .line 125
    .line 126
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    goto/16 :goto_1

    .line 130
    .line 131
    :pswitch_8
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_SEARCH42_SCAN_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 132
    .line 133
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v0

    .line 137
    const-string v1, "WIDGET_SEARCH42_SCAN_BATCH.toTrackerValue()"

    .line 138
    .line 139
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    goto/16 :goto_1

    .line 143
    .line 144
    :pswitch_9
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_SEARCH42_ID_MODE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 145
    .line 146
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    const-string v1, "WIDGET_SEARCH42_ID_MODE.toTrackerValue()"

    .line 151
    .line 152
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    goto/16 :goto_1

    .line 156
    .line 157
    :pswitch_a
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_SEARCH42_OCR_MODE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 158
    .line 159
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    const-string v1, "WIDGET_SEARCH42_OCR_MODE.toTrackerValue()"

    .line 164
    .line 165
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    goto/16 :goto_1

    .line 169
    .line 170
    :pswitch_b
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->APP_SHORTCUT_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 171
    .line 172
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object v0

    .line 176
    const-string v1, "APP_SHORTCUT_SINGLE.toTrackerValue()"

    .line 177
    .line 178
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    .line 180
    .line 181
    goto/16 :goto_1

    .line 182
    .line 183
    :pswitch_c
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->APP_SHORTCUT_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 184
    .line 185
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v0

    .line 189
    const-string v1, "APP_SHORTCUT_BATCH.toTrackerValue()"

    .line 190
    .line 191
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    .line 193
    .line 194
    goto/16 :goto_1

    .line 195
    .line 196
    :pswitch_d
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->APP_SHORTCUT_TO_WORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 197
    .line 198
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v0

    .line 202
    const-string v1, "APP_SHORTCUT_TO_WORD.toTrackerValue()"

    .line 203
    .line 204
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    goto :goto_1

    .line 208
    :pswitch_e
    const-string v0, "h5"

    .line 209
    .line 210
    goto :goto_1

    .line 211
    :pswitch_f
    const-string v0, "cs_advanced_folder_family_file"

    .line 212
    .line 213
    goto :goto_1

    .line 214
    :pswitch_10
    const-string v0, "cs_advanced_folder_ideas"

    .line 215
    .line 216
    goto :goto_1

    .line 217
    :pswitch_11
    const-string v0, "cs_advanced_folder_work_file"

    .line 218
    .line 219
    goto :goto_1

    .line 220
    :pswitch_12
    const-string v0, "cs_advanced_folder_briefcase"

    .line 221
    .line 222
    goto :goto_1

    .line 223
    :pswitch_13
    const-string v0, "cs_advanced_folder_growth_record"

    .line 224
    .line 225
    goto :goto_1

    .line 226
    :pswitch_14
    const-string v0, "cs_advanced_folder_certificate"

    .line 227
    .line 228
    goto :goto_1

    .line 229
    :pswitch_15
    const-string v0, "cs_premium_marketing"

    .line 230
    .line 231
    goto :goto_1

    .line 232
    :pswitch_16
    const-string v0, "CSFunctionRecommend"

    .line 233
    .line 234
    goto :goto_1

    .line 235
    :pswitch_17
    const-string v0, "scan_toolbox"

    .line 236
    .line 237
    goto :goto_1

    .line 238
    :pswitch_18
    const-string v0, "cs_usertag_recommand"

    .line 239
    .line 240
    goto :goto_1

    .line 241
    :pswitch_19
    const-string v0, "cs_directory"

    .line 242
    .line 243
    goto :goto_1

    .line 244
    :pswitch_1a
    const-string v0, "cs_list"

    .line 245
    .line 246
    goto :goto_1

    .line 247
    :pswitch_1b
    const-string v0, "cs_main"

    .line 248
    .line 249
    goto :goto_1

    .line 250
    :cond_0
    move-object v0, v1

    .line 251
    :goto_0
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇(Lcom/intsig/camscanner/capture/CaptureMode;)Z

    .line 252
    .line 253
    .line 254
    move-result v0

    .line 255
    if-eqz v0, :cond_1

    .line 256
    .line 257
    const-string v0, "other"

    .line 258
    .line 259
    goto :goto_1

    .line 260
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 261
    .line 262
    .line 263
    move-result-object v0

    .line 264
    if-eqz v0, :cond_2

    .line 265
    .line 266
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo8〇〇()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 267
    .line 268
    .line 269
    move-result-object v0

    .line 270
    if-eqz v0, :cond_2

    .line 271
    .line 272
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOO()Lcom/intsig/camscanner/capture/CaptureMode;

    .line 273
    .line 274
    .line 275
    move-result-object v1

    .line 276
    :cond_2
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇(Lcom/intsig/camscanner/capture/CaptureMode;)Z

    .line 277
    .line 278
    .line 279
    move-result v0

    .line 280
    if-eqz v0, :cond_3

    .line 281
    .line 282
    const-string v0, "allfunc"

    .line 283
    .line 284
    goto :goto_1

    .line 285
    :cond_3
    const-string v0, ""

    .line 286
    .line 287
    :goto_1
    return-object v0

    .line 288
    nop

    .line 289
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
.end method

.method public 〇0(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

    .line 2
    .line 3
    const-string v1, "CaptureRefactorViewModel"

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string p1, "preViewZoomIn zoomControl == null || mZoomBar == null"

    .line 8
    .line 9
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O8ooOoo〇()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_2

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    add-int/2addr v2, p1

    .line 30
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->oO80(I)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇080()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-le p1, v0, :cond_1

    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇080()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->oO80(I)V

    .line 54
    .line 55
    .line 56
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 59
    .line 60
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/ZoomControl;->O8(I)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 72
    .line 73
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇8(I)V

    .line 78
    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

    .line 81
    .line 82
    if-eqz p1, :cond_3

    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 85
    .line 86
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/CustomSeekBar;->O8(I)V

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 95
    .line 96
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/ZoomControl;->oO80(I)Z

    .line 97
    .line 98
    .line 99
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 100
    .line 101
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇080()I

    .line 102
    .line 103
    .line 104
    move-result p1

    .line 105
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 106
    .line 107
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 108
    .line 109
    .line 110
    move-result v0

    .line 111
    new-instance v2, Ljava/lang/StringBuilder;

    .line 112
    .line 113
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .line 115
    .line 116
    const-string v3, "preViewZoomIn\uff1a mZoomModel.maxZoomVal: "

    .line 117
    .line 118
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    const-string p1, " mZoomModel.zoomValue: "

    .line 125
    .line 126
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 133
    .line 134
    .line 135
    move-result-object p1

    .line 136
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    return-void
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public 〇00(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8o:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇0000OOO()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇〇888()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇000O0()Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o08O()Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇000〇〇08(ILandroid/content/Intent;)V
    .locals 3

    .line 1
    const/4 v0, -0x1

    .line 2
    if-ne p1, v0, :cond_b

    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇Oo〇o8()V

    .line 5
    .line 6
    .line 7
    :try_start_0
    sget-object v0, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;->〇080:Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    if-eqz p2, :cond_0

    .line 17
    .line 18
    const-string v0, "key_chose_file_path_info"

    .line 19
    .line 20
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    check-cast v0, Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/4 v0, 0x0

    .line 28
    :goto_0
    if-eqz p2, :cond_3

    .line 29
    .line 30
    const-string v1, "extra_folder_id"

    .line 31
    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    iget-object v2, v0, Lcom/intsig/camscanner/datastruct/FolderDocInfo;->o0:Ljava/lang/String;

    .line 35
    .line 36
    if-nez v2, :cond_2

    .line 37
    .line 38
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    :cond_2
    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    .line 44
    .line 45
    :cond_3
    if-eqz p2, :cond_5

    .line 46
    .line 47
    const-string v1, "extra_offline_folder"

    .line 48
    .line 49
    if-eqz v0, :cond_4

    .line 50
    .line 51
    iget-boolean v0, v0, Lcom/intsig/camscanner/datastruct/FolderDocInfo;->〇OOo8〇0:Z

    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0000OOO()Z

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    :goto_1
    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 59
    .line 60
    .line 61
    :cond_5
    if-eqz p2, :cond_6

    .line 62
    .line 63
    const-string v0, "extra_entrance"

    .line 64
    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO0880O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 66
    .line 67
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 68
    .line 69
    .line 70
    :cond_6
    if-eqz p2, :cond_7

    .line 71
    .line 72
    const-string v0, "extra_from_detect_idcard_2_a4_paper"

    .line 73
    .line 74
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o880:Z

    .line 75
    .line 76
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 77
    .line 78
    .line 79
    :cond_7
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO〇00〇8oO:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;

    .line 80
    .line 81
    if-eqz v0, :cond_8

    .line 82
    .line 83
    invoke-interface {v0, p2}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;->〇o(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .line 85
    .line 86
    goto :goto_2

    .line 87
    :catch_0
    move-exception p2

    .line 88
    const-string v0, "CaptureRefactorViewModel"

    .line 89
    .line 90
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91
    .line 92
    .line 93
    :cond_8
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 94
    .line 95
    .line 96
    move-result-object p2

    .line 97
    invoke-virtual {p2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 98
    .line 99
    .line 100
    move-result-object p2

    .line 101
    const/4 v0, 0x0

    .line 102
    if-eqz p2, :cond_9

    .line 103
    .line 104
    const-string v1, "constant_is_add_new_doc"

    .line 105
    .line 106
    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 107
    .line 108
    .line 109
    move-result p2

    .line 110
    const/4 v1, 0x1

    .line 111
    if-ne p2, v1, :cond_9

    .line 112
    .line 113
    const/4 v0, 0x1

    .line 114
    :cond_9
    if-eqz v0, :cond_a

    .line 115
    .line 116
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 117
    .line 118
    .line 119
    move-result-object p2

    .line 120
    invoke-virtual {p2, p1}, Landroid/app/Activity;->setResult(I)V

    .line 121
    .line 122
    .line 123
    :cond_a
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 124
    .line 125
    .line 126
    move-result-object p1

    .line 127
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 128
    .line 129
    .line 130
    goto :goto_3

    .line 131
    :cond_b
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8O0()Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object p1

    .line 135
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 136
    .line 137
    .line 138
    :goto_3
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public final 〇008〇o0〇〇()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇008〇oo()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇OO〇00〇0O:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇00O0O0(Z)Z
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    const/4 v2, 0x0

    .line 6
    const-wide/16 v3, 0x0

    .line 7
    .line 8
    cmp-long v5, v0, v3

    .line 9
    .line 10
    if-ltz v5, :cond_0

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 13
    .line 14
    .line 15
    move-result-wide v0

    .line 16
    cmp-long v5, v0, v3

    .line 17
    .line 18
    if-lez v5, :cond_7

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 25
    .line 26
    .line 27
    move-result-wide v3

    .line 28
    invoke-static {v0, v3, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-nez v0, :cond_7

    .line 33
    .line 34
    :cond_0
    const-string v0, "checkDodId - insertImage mDocId "

    .line 35
    .line 36
    const-string v1, "CaptureRefactorViewModel"

    .line 37
    .line 38
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O0o8〇O()Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    const/4 v3, 0x0

    .line 46
    const/4 v4, 0x1

    .line 47
    if-eqz v0, :cond_1

    .line 48
    .line 49
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO0o〇〇()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v5

    .line 59
    invoke-static {v0, v5, v4}, Lcom/intsig/camscanner/util/Util;->OOO(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    goto :goto_1

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->ooo0〇〇O:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 65
    .line 66
    if-eqz v0, :cond_3

    .line 67
    .line 68
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    iget-object v5, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->ooo0〇〇O:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 73
    .line 74
    if-eqz v5, :cond_2

    .line 75
    .line 76
    invoke-virtual {v5}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->getSceneDocTitle()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v5

    .line 80
    goto :goto_0

    .line 81
    :cond_2
    move-object v5, v3

    .line 82
    :goto_0
    invoke-static {v0, v5, v4}, Lcom/intsig/camscanner/util/Util;->OOO(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    goto :goto_1

    .line 87
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    if-eqz v0, :cond_4

    .line 96
    .line 97
    iget-object v5, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 98
    .line 99
    invoke-virtual {v5}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->Oo08()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v5

    .line 103
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Ooo8〇〇()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v6

    .line 107
    invoke-virtual {v0, v5, v6, v4}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇〇0〇(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    goto :goto_1

    .line 112
    :cond_4
    move-object v0, v3

    .line 113
    :goto_1
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇80(Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 117
    .line 118
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->O8()Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    iput-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8〇OO0〇0o:Ljava/lang/String;

    .line 123
    .line 124
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO〇00〇8oO:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;

    .line 125
    .line 126
    if-eqz v0, :cond_5

    .line 127
    .line 128
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;->〇080(Z)Landroid/net/Uri;

    .line 129
    .line 130
    .line 131
    move-result-object v3

    .line 132
    :cond_5
    if-nez v3, :cond_6

    .line 133
    .line 134
    const-string p1, "url == null"

    .line 135
    .line 136
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    return v2

    .line 140
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 141
    .line 142
    invoke-static {v3}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 143
    .line 144
    .line 145
    move-result-wide v0

    .line 146
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇80〇808〇O(J)V

    .line 147
    .line 148
    .line 149
    iput-boolean v4, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO〇OOo:Z

    .line 150
    .line 151
    const/4 v2, 0x1

    .line 152
    :cond_7
    return v2
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public 〇00〇8()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇00〇8()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇080()Landroid/net/Uri;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO〇00〇8oO:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;->〇080(Z)Landroid/net/Uri;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$PersonalMold;

    .line 13
    .line 14
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$PersonalMold;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$PersonalMold;->〇080(Z)Landroid/net/Uri;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    :cond_1
    return-object v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇080O0()Lcom/intsig/camscanner/util/PremiumParcelSize;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->ooO:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇080OO8〇0(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇8o8o〇(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_1

    .line 7
    .line 8
    invoke-static {p1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    goto :goto_1

    .line 17
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 18
    :goto_1
    if-eqz p1, :cond_2

    .line 19
    .line 20
    new-instance p1, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$PersonalMold;

    .line 21
    .line 22
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$PersonalMold;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)V

    .line 23
    .line 24
    .line 25
    goto :goto_2

    .line 26
    :cond_2
    new-instance p1, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$TeamMold;

    .line 27
    .line 28
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$TeamMold;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)V

    .line 29
    .line 30
    .line 31
    :goto_2
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO〇00〇8oO:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;

    .line 32
    .line 33
    return-void
    .line 34
.end method

.method public 〇08O8o8(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string p1, "CaptureRefactorViewModel"

    .line 6
    .line 7
    const-string v0, "preViewZoomOut zoomControl == null || mZoomBar == null"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O8ooOoo〇()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_2

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    sub-int/2addr v1, p1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->oO80(I)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    if-gez p1, :cond_1

    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 42
    .line 43
    const/4 v0, 0x0

    .line 44
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->oO80(I)V

    .line 45
    .line 46
    .line 47
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/ZoomControl;->O8(I)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 63
    .line 64
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇8(I)V

    .line 69
    .line 70
    .line 71
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

    .line 72
    .line 73
    if-eqz p1, :cond_3

    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/CustomSeekBar;->O8(I)V

    .line 82
    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 86
    .line 87
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/ZoomControl;->OO0o〇〇〇〇0(I)Z

    .line 88
    .line 89
    .line 90
    :cond_3
    :goto_0
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇08O8o〇0(Z)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇08O8o〇0(Z)V

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    if-eqz p1, :cond_1

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇0OO8()V

    .line 19
    .line 20
    .line 21
    :cond_1
    :goto_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇08〇0〇o〇8()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/capture/capturemode/CaptureModePreferenceHelper;->O8()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v1, 0x0

    .line 10
    :goto_0
    return v1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇0O(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8o08O8O:Lkotlinx/coroutines/flow/MutableSharedFlow;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-interface {v0, p1}, Lkotlinx/coroutines/flow/MutableSharedFlow;->〇o00〇〇Oo(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇0OO8()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇80O8o8O〇:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇0O〇Oo()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOO8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇0〇O0088o(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇0〇O0088o(Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇8(Lcom/intsig/camscanner/capture/CaptureMode;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇8(Lcom/intsig/camscanner/capture/CaptureMode;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇80(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇O8o08O(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇08O:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    const-string v0, "mCaptureUiControl"

    .line 7
    .line 8
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇8o()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO0o〇〇〇〇0()Landroid/os/Handler;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x3

    .line 10
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO8〇()V

    .line 15
    .line 16
    .line 17
    :goto_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇8o〇〇8080()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇oO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇8〇0〇o〇O(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇8〇0〇o〇O(Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇8〇o〇8(Lcom/intsig/camscanner/capture/CustomSeekBar;Ljava/lang/Runnable;)V
    .locals 7
    .param p2    # Ljava/lang/Runnable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "CaptureRefactorViewModel"

    .line 2
    .line 3
    const-string v1, "dismissZoomBarRunnable"

    .line 4
    .line 5
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o8()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇〇888()Z

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    const-string p1, "initializeZoom mParameters is null"

    .line 27
    .line 28
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

    .line 33
    .line 34
    if-eqz v2, :cond_1

    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

    .line 38
    .line 39
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->Oo08()Z

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    invoke-interface {v3}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O8ooOoo〇()Z

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    new-instance v4, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    const-string v5, "initializeZoom-zoomSupported:"

    .line 61
    .line 62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    const-string v2, ", mSmoothZoomSupported:"

    .line 69
    .line 70
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->Oo08()Z

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    if-eqz v2, :cond_9

    .line 92
    .line 93
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 94
    .line 95
    const/16 v3, 0x63

    .line 96
    .line 97
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->Oo08(I)V

    .line 98
    .line 99
    .line 100
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 101
    .line 102
    const/4 v4, 0x0

    .line 103
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->oO80(I)V

    .line 104
    .line 105
    .line 106
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 107
    .line 108
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 109
    .line 110
    .line 111
    move-result-object v5

    .line 112
    invoke-interface {v5}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O8ooOoo〇()Z

    .line 113
    .line 114
    .line 115
    move-result v5

    .line 116
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/view/ZoomControl;->〇o〇(Z)V

    .line 117
    .line 118
    .line 119
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 120
    .line 121
    iget-object v5, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 122
    .line 123
    invoke-virtual {v5}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇080()I

    .line 124
    .line 125
    .line 126
    move-result v5

    .line 127
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/view/ZoomControl;->Oo08(I)V

    .line 128
    .line 129
    .line 130
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 131
    .line 132
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->O8()I

    .line 133
    .line 134
    .line 135
    move-result v2

    .line 136
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0()I

    .line 137
    .line 138
    .line 139
    move-result v5

    .line 140
    int-to-float v5, v5

    .line 141
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 142
    .line 143
    .line 144
    move-result v6

    .line 145
    cmpg-float v5, v5, v6

    .line 146
    .line 147
    if-nez v5, :cond_2

    .line 148
    .line 149
    goto :goto_0

    .line 150
    :cond_2
    const/4 v1, 0x0

    .line 151
    :goto_0
    if-nez v1, :cond_4

    .line 152
    .line 153
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08〇0〇o〇8()Z

    .line 154
    .line 155
    .line 156
    move-result v1

    .line 157
    if-eqz v1, :cond_4

    .line 158
    .line 159
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 160
    .line 161
    .line 162
    move-result v1

    .line 163
    const/high16 v2, 0x3f800000    # 1.0f

    .line 164
    .line 165
    cmpg-float v1, v1, v2

    .line 166
    .line 167
    if-gez v1, :cond_3

    .line 168
    .line 169
    const/4 v1, 0x4

    .line 170
    const/4 v2, 0x4

    .line 171
    goto :goto_1

    .line 172
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 173
    .line 174
    .line 175
    move-result v1

    .line 176
    sub-float/2addr v2, v1

    .line 177
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0()I

    .line 178
    .line 179
    .line 180
    move-result v1

    .line 181
    int-to-float v1, v1

    .line 182
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 183
    .line 184
    .line 185
    move-result v4

    .line 186
    sub-float/2addr v1, v4

    .line 187
    div-float/2addr v2, v1

    .line 188
    int-to-float v1, v3

    .line 189
    mul-float v2, v2, v1

    .line 190
    .line 191
    float-to-int v1, v2

    .line 192
    move v2, v1

    .line 193
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o80ooO()F

    .line 194
    .line 195
    .line 196
    move-result v1

    .line 197
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0()I

    .line 198
    .line 199
    .line 200
    move-result v3

    .line 201
    new-instance v4, Ljava/lang/StringBuilder;

    .line 202
    .line 203
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 204
    .line 205
    .line 206
    const-string v5, "getMinZoom: "

    .line 207
    .line 208
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    .line 210
    .line 211
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    const-string v1, "   maxZoomValue:  "

    .line 215
    .line 216
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    .line 218
    .line 219
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 223
    .line 224
    .line 225
    move-result-object v1

    .line 226
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    .line 228
    .line 229
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08〇0〇o〇8()Z

    .line 230
    .line 231
    .line 232
    move-result v1

    .line 233
    if-eqz v1, :cond_5

    .line 234
    .line 235
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008(I)V

    .line 236
    .line 237
    .line 238
    :cond_5
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 239
    .line 240
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/ZoomControl;->O8(I)V

    .line 241
    .line 242
    .line 243
    if-eqz p1, :cond_6

    .line 244
    .line 245
    iget-object v1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 246
    .line 247
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇080()I

    .line 248
    .line 249
    .line 250
    move-result v1

    .line 251
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o00〇〇Oo(I)V

    .line 252
    .line 253
    .line 254
    :cond_6
    if-eqz p1, :cond_7

    .line 255
    .line 256
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/capture/CustomSeekBar;->O8(I)V

    .line 257
    .line 258
    .line 259
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 260
    .line 261
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 262
    .line 263
    .line 264
    const-string v3, "initializeZoom  initProgress\uff1a "

    .line 265
    .line 266
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    .line 268
    .line 269
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 270
    .line 271
    .line 272
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 273
    .line 274
    .line 275
    move-result-object v1

    .line 276
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    .line 278
    .line 279
    if-eqz p1, :cond_8

    .line 280
    .line 281
    new-instance v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$initializeZoom$1;

    .line 282
    .line 283
    invoke-direct {v0, p0, p2}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$initializeZoom$1;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;Ljava/lang/Runnable;)V

    .line 284
    .line 285
    .line 286
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o〇(Lcom/intsig/camscanner/capture/CustomSeekBar$CustomOnSeekBarChangeListener;)V

    .line 287
    .line 288
    .line 289
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 290
    .line 291
    new-instance p2, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CustomZoomControlListener;

    .line 292
    .line 293
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CustomZoomControlListener;-><init>(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)V

    .line 294
    .line 295
    .line 296
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/view/ZoomControl;->〇o00〇〇Oo(Lcom/intsig/camscanner/view/ZoomControl$OnZoomChangedListener;)V

    .line 297
    .line 298
    .line 299
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 300
    .line 301
    .line 302
    move-result-object p1

    .line 303
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇80〇808〇O()V

    .line 304
    .line 305
    .line 306
    :cond_9
    return-void

    .line 307
    :catch_0
    move-exception p1

    .line 308
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 309
    .line 310
    .line 311
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0OOo〇0(Z)V

    .line 312
    .line 313
    .line 314
    return-void
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public 〇O(ZLcom/intsig/camscanner/capture/CaptureMode;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureModeMenuShownEntity;

    .line 2
    .line 3
    invoke-direct {v0, p2, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureModeMenuShownEntity;-><init>(Lcom/intsig/camscanner/capture/CaptureMode;Z)V

    .line 4
    .line 5
    .line 6
    new-instance p1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string p2, "updateModeStatus "

    .line 12
    .line 13
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    const-string p2, "CaptureRefactorViewModel"

    .line 24
    .line 25
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O88O:Landroidx/lifecycle/MutableLiveData;

    .line 29
    .line 30
    invoke-virtual {p1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇O80〇oOo(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇08O:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇O888o0o()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇O888o0o()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O8o08O()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇080()J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    return-wide v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇O8〇OO〇(IZ)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "Zoom changed: zoomValue="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, ". stopped="

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "CaptureRefactorViewModel"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    if-ltz p1, :cond_0

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇080()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-gt p1, v0, :cond_0

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 42
    .line 43
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->oO80(I)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇0O〇O00O:Lcom/intsig/camscanner/view/ZoomControl;

    .line 47
    .line 48
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/ZoomControl;->O8(I)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o0OoOOo0:Lcom/intsig/camscanner/capture/CustomSeekBar;

    .line 52
    .line 53
    if-eqz v0, :cond_0

    .line 54
    .line 55
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/CustomSeekBar;->O8(I)V

    .line 56
    .line 57
    .line 58
    :cond_0
    if-eqz p2, :cond_2

    .line 59
    .line 60
    iget-object p2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 61
    .line 62
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇o〇()I

    .line 63
    .line 64
    .line 65
    move-result p2

    .line 66
    if-eqz p2, :cond_2

    .line 67
    .line 68
    iget-object p2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 69
    .line 70
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇o00〇〇Oo()I

    .line 71
    .line 72
    .line 73
    move-result p2

    .line 74
    const/4 v0, -0x1

    .line 75
    if-eq p2, v0, :cond_1

    .line 76
    .line 77
    iget-object p2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 78
    .line 79
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇o00〇〇Oo()I

    .line 80
    .line 81
    .line 82
    move-result p2

    .line 83
    if-eq p1, p2, :cond_1

    .line 84
    .line 85
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇O00()Z

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    if-nez p1, :cond_2

    .line 94
    .line 95
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    iget-object p2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 100
    .line 101
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇o00〇〇Oo()I

    .line 102
    .line 103
    .line 104
    move-result p2

    .line 105
    invoke-interface {p1, p2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇0000OOO(I)V

    .line 106
    .line 107
    .line 108
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 109
    .line 110
    const/4 p2, 0x1

    .line 111
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇〇888(I)V

    .line 112
    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 116
    .line 117
    const/4 p2, 0x0

    .line 118
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;->〇〇888(I)V

    .line 119
    .line 120
    .line 121
    :cond_2
    :goto_0
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public final 〇OO0(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇o88o08〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇OO8Oo0〇()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O〇〇O8:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇OOo8〇0(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oooO888:Landroid/content/SharedPreferences;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const-string v1, "pref_camera_grid_key"

    .line 12
    .line 13
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 20
    .line 21
    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇〇0o〇〇0:Landroidx/lifecycle/MutableLiveData;

    .line 23
    .line 24
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 29
    .line 30
    .line 31
    if-eqz p1, :cond_1

    .line 32
    .line 33
    const-string p1, "on"

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const-string p1, "off"

    .line 37
    .line 38
    :goto_0
    const-string v0, "CSScan"

    .line 39
    .line 40
    const-string v1, "gridlines"

    .line 41
    .line 42
    const-string v2, "type"

    .line 43
    .line 44
    invoke-static {v0, v1, v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final 〇Oo(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->OO0o〇〇(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇Oo〇o8()V
    .locals 4

    .line 1
    const-string v0, "from_part"

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇〇0〇88()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "CSScan"

    .line 8
    .line 9
    const-string v3, "scan_ok"

    .line 10
    .line 11
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O〇()Landroid/view/View;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇O〇()Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O〇80o08O(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇o(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "intent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OO〇00〇8oO:Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$MoldInterface;->〇o(Landroid/content/Intent;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇o00〇〇Oo()Landroid/view/View;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇o00〇〇Oo()Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o0O0O8(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO00〇o:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇o8OO0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇08〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇o8oO(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;Landroid/view/View;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "captureUiControl"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "context"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "captureView"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "captureViewGroup"

    .line 17
    .line 18
    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O80〇oOo(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;)V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oooO888:Landroid/content/SharedPreferences;

    .line 25
    .line 26
    if-nez p1, :cond_0

    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oooO888:Landroid/content/SharedPreferences;

    .line 37
    .line 38
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇oOo〇()V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8〇OO:Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;

    .line 42
    .line 43
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;->o〇0OOo〇0(Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;)V

    .line 44
    .line 45
    .line 46
    const/4 p1, 0x0

    .line 47
    if-eqz p7, :cond_1

    .line 48
    .line 49
    invoke-virtual {p0, p7}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇00O0O〇o(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 53
    .line 54
    .line 55
    move-result-object p7

    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O〇08oOOO0:Ljava/lang/String;

    .line 57
    .line 58
    invoke-interface {p7, v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O〇8O8〇008(Ljava/lang/String;)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p7

    .line 62
    if-nez p7, :cond_4

    .line 63
    .line 64
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->〇O00()I

    .line 65
    .line 66
    .line 67
    move-result p7

    .line 68
    const/4 v0, 0x2

    .line 69
    const/4 v1, 0x1

    .line 70
    if-ne p7, v0, :cond_2

    .line 71
    .line 72
    const/4 p7, 0x1

    .line 73
    goto :goto_0

    .line 74
    :cond_2
    const/4 p7, 0x0

    .line 75
    :goto_0
    if-ne p7, v1, :cond_3

    .line 76
    .line 77
    new-instance p7, Lcom/intsig/camscanner/capture/camera/CameraClientX;

    .line 78
    .line 79
    invoke-direct {p7, p3}, Lcom/intsig/camscanner/capture/camera/CameraClientX;-><init>(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;)V

    .line 80
    .line 81
    .line 82
    goto :goto_1

    .line 83
    :cond_3
    new-instance p7, Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 84
    .line 85
    invoke-direct {p7, p3}, Lcom/intsig/camscanner/capture/camera/CameraClient1;-><init>(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$View;)V

    .line 86
    .line 87
    .line 88
    :goto_1
    invoke-virtual {p0, p7}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇00O0O〇o(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 89
    .line 90
    .line 91
    sget-object p3, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 92
    .line 93
    :cond_4
    new-instance p3, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;

    .line 94
    .line 95
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 96
    .line 97
    .line 98
    move-result-object p7

    .line 99
    invoke-direct {p3, p2, p0, p6, p7}, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 100
    .line 101
    .line 102
    iput-object p3, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇08〇o0O:Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;

    .line 103
    .line 104
    iput-object p4, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo〇8o008:Lcom/intsig/camscanner/capture/contract/ICaptureModeControl;

    .line 105
    .line 106
    const/4 p3, 0x0

    .line 107
    if-eqz p4, :cond_5

    .line 108
    .line 109
    new-instance p6, Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;

    .line 110
    .line 111
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 112
    .line 113
    .line 114
    move-result-object p7

    .line 115
    invoke-direct {p6, p2, p0, p7, p4}, Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;Lcom/intsig/camscanner/capture/contract/CaptureModeControlCallback;)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {p0, p6}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0〇(Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;)V

    .line 119
    .line 120
    .line 121
    sget-object p2, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 122
    .line 123
    goto :goto_2

    .line 124
    :cond_5
    move-object p2, p3

    .line 125
    :goto_2
    const-string p4, "CaptureRefactorViewModel"

    .line 126
    .line 127
    if-nez p2, :cond_6

    .line 128
    .line 129
    const-string p2, "initialize but captureModeControl is Null! exit!"

    .line 130
    .line 131
    invoke-static {p4, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0OOo〇0(Z)V

    .line 135
    .line 136
    .line 137
    return-void

    .line 138
    :cond_6
    if-eqz p5, :cond_7

    .line 139
    .line 140
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o08O()Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;

    .line 141
    .line 142
    .line 143
    move-result-object p2

    .line 144
    invoke-virtual {p2, p5}, Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;->〇O8o08O(Landroid/view/View;)V

    .line 145
    .line 146
    .line 147
    sget-object p3, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 148
    .line 149
    :cond_7
    if-nez p3, :cond_8

    .line 150
    .line 151
    const-string p2, "initialize but rootView is Null! exit!"

    .line 152
    .line 153
    invoke-static {p4, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    .line 155
    .line 156
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0OOo〇0(Z)V

    .line 157
    .line 158
    .line 159
    :cond_8
    return-void
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method public 〇oOO8O8(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇oOO8O8(Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇oOoo〇(Lcom/intsig/camscanner/util/PremiumParcelSize;)V
    .locals 4

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->ooO:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 2
    .line 3
    if-eqz p1, :cond_2

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    new-instance v2, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v3, "settingItemPixel width:"

    .line 19
    .line 20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v0, ", height:"

    .line 27
    .line 28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    const-string v1, "CaptureRefactorViewModel"

    .line 39
    .line 40
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    const/16 v1, 0x32

    .line 48
    .line 49
    if-lt v0, v1, :cond_0

    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-ge v0, v1, :cond_1

    .line 56
    .line 57
    :cond_0
    const-string v0, "settingItemPixel"

    .line 58
    .line 59
    invoke-static {v0}, Lcom/intsig/camscanner/tools/FrameDetectionTool;->O8(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oooO888:Landroid/content/SharedPreferences;

    .line 63
    .line 64
    if-eqz v0, :cond_2

    .line 65
    .line 66
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    if-eqz v0, :cond_2

    .line 71
    .line 72
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 77
    .line 78
    .line 79
    move-result p1

    .line 80
    new-instance v2, Ljava/lang/StringBuilder;

    .line 81
    .line 82
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 83
    .line 84
    .line 85
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    const-string v1, "x"

    .line 89
    .line 90
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    const-string v1, "keysetcapturesize"

    .line 101
    .line 102
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    if-eqz p1, :cond_2

    .line 107
    .line 108
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 109
    .line 110
    .line 111
    :cond_2
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final 〇oo()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O000(I)I

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o〇(J)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->〇80〇808〇O(J)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇o〇8()V
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    const-wide/16 v2, 0x0

    .line 6
    .line 7
    cmp-long v4, v0, v2

    .line 8
    .line 9
    if-lez v4, :cond_1

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const-string v1, "com.intsig.camscanner.NEW_PAGE"

    .line 24
    .line 25
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    const/4 v1, 0x1

    .line 30
    const/4 v2, 0x2

    .line 31
    if-eqz v0, :cond_0

    .line 32
    .line 33
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 40
    .line 41
    .line 42
    move-result-wide v4

    .line 43
    const/4 v6, 0x3

    .line 44
    invoke-static {v3, v4, v5, v6, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    iget-object v3, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇oo〇O〇80:Ljava/util/List;

    .line 52
    .line 53
    invoke-static {v1, v3, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO〇00〇8oO(Landroid/content/Context;Ljava/util/List;I)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 61
    .line 62
    .line 63
    move-result-wide v1

    .line 64
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->Oo〇O(Landroid/content/Context;J)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_0
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 69
    .line 70
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇O8o08O()J

    .line 75
    .line 76
    .line 77
    move-result-wide v3

    .line 78
    invoke-static {v0, v3, v4, v2, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 79
    .line 80
    .line 81
    :cond_1
    :goto_0
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇o〇Oo0()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇0o8O〇〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Ooo8〇〇()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/DBUtil;->ooOO(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇o〇o()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->OOoo()Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 6
    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇00OO()I
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇O8o08O()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8o8O〇O()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    add-int/2addr v0, v1

    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o08〇〇0O()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    const/4 v2, 0x1

    .line 19
    if-eq v1, v2, :cond_1

    .line 20
    .line 21
    const/4 v2, 0x2

    .line 22
    if-eq v1, v2, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    rem-int/lit16 v2, v0, 0xb4

    .line 26
    .line 27
    if-nez v2, :cond_2

    .line 28
    .line 29
    add-int/lit8 v0, v0, 0x5a

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    rem-int/lit16 v2, v0, 0xb4

    .line 33
    .line 34
    if-eqz v2, :cond_2

    .line 35
    .line 36
    add-int/lit16 v0, v0, 0x10e

    .line 37
    .line 38
    :cond_2
    :goto_0
    add-int/lit16 v0, v0, 0x168

    .line 39
    .line 40
    rem-int/lit16 v0, v0, 0x168

    .line 41
    .line 42
    new-instance v2, Ljava/lang/StringBuilder;

    .line 43
    .line 44
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .line 46
    .line 47
    const-string v3, "getCameraRotation4Snap   orientation="

    .line 48
    .line 49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    const-string v1, " rotation="

    .line 56
    .line 57
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    const-string v2, "CaptureRefactorViewModel"

    .line 68
    .line 69
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    return v0
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final 〇〇00O〇0o()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->Oo0〇Ooo:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇0O8ooO(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇oo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇〇0o()Lcom/intsig/view/RotateImageTextButton;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇〇0o()Lcom/intsig/view/RotateImageTextButton;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇0o8O〇〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oOo0:Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureDocModel;->Oo08()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇0o〇o8(Z)Z
    .locals 6

    .line 1
    const-string v0, "CaptureRefactorViewModel"

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 5
    .line 6
    .line 7
    move-result-object v2

    .line 8
    invoke-interface {v2, p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇8o8o〇(Z)Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    const/4 v3, 0x1

    .line 13
    if-eqz v2, :cond_2

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const-string v4, "audio"

    .line 20
    .line 21
    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    instance-of v4, v2, Landroid/media/AudioManager;

    .line 26
    .line 27
    if-eqz v4, :cond_0

    .line 28
    .line 29
    check-cast v2, Landroid/media/AudioManager;

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 v2, 0x0

    .line 33
    :goto_0
    if-eqz v2, :cond_2

    .line 34
    .line 35
    if-eqz p1, :cond_1

    .line 36
    .line 37
    iget v4, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇o〇:I

    .line 38
    .line 39
    invoke-virtual {v2, v3, v4, v1}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 40
    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_1
    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    iput v4, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇o〇:I

    .line 48
    .line 49
    invoke-virtual {v2, v3, v1, v1}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 50
    .line 51
    .line 52
    :goto_1
    iget v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇〇o〇:I

    .line 53
    .line 54
    new-instance v4, Ljava/lang/StringBuilder;

    .line 55
    .line 56
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .line 58
    .line 59
    const-string v5, "setCaptureSound soundOn="

    .line 60
    .line 61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    const-string v5, " mCurSound="

    .line 68
    .line 69
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    :cond_2
    iget-object v2, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oooO888:Landroid/content/SharedPreferences;

    .line 83
    .line 84
    if-eqz v2, :cond_4

    .line 85
    .line 86
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 87
    .line 88
    .line 89
    move-result-object v2

    .line 90
    if-eqz v2, :cond_4

    .line 91
    .line 92
    const-string v4, "key_sound_state_new"

    .line 93
    .line 94
    if-eqz p1, :cond_3

    .line 95
    .line 96
    const/4 v5, 0x1

    .line 97
    goto :goto_2

    .line 98
    :cond_3
    const/4 v5, 0x0

    .line 99
    :goto_2
    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    if-eqz v2, :cond_4

    .line 104
    .line 105
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    .line 107
    .line 108
    :cond_4
    const/4 v1, 0x1

    .line 109
    goto :goto_3

    .line 110
    :catch_0
    move-exception v2

    .line 111
    new-instance v3, Ljava/lang/StringBuilder;

    .line 112
    .line 113
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .line 115
    .line 116
    const-string v4, "setCaptureSound-"

    .line 117
    .line 118
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    const-string p1, " "

    .line 125
    .line 126
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    invoke-static {v0, p1, v2}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 134
    .line 135
    .line 136
    :goto_3
    return v1
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public final 〇〇0〇0o8()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o8o:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇8(I)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->OooO8〇08o(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇〇808〇(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇〇808〇(Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇〇8O0〇8()Lcom/intsig/view/RotateLayout;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇8O0O808〇()Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;->〇〇8O0〇8()Lcom/intsig/view/RotateLayout;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇O00〇8()Z
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "audio"

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    instance-of v1, v0, Landroid/media/AudioManager;

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    check-cast v0, Landroid/media/AudioManager;

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    move-object v0, v2

    .line 20
    :goto_0
    const/4 v1, 0x0

    .line 21
    if-eqz v0, :cond_4

    .line 22
    .line 23
    const/4 v3, 0x1

    .line 24
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    iget-object v4, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oooO888:Landroid/content/SharedPreferences;

    .line 29
    .line 30
    if-eqz v4, :cond_2

    .line 31
    .line 32
    if-lez v0, :cond_1

    .line 33
    .line 34
    const/4 v2, 0x1

    .line 35
    goto :goto_1

    .line 36
    :cond_1
    const/4 v2, 0x0

    .line 37
    :goto_1
    const-string v5, "key_sound_state_new"

    .line 38
    .line 39
    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    .line 48
    .line 49
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .line 51
    .line 52
    const-string v5, "getCurrentCameraSoundStatus curSound="

    .line 53
    .line 54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    const-string v0, "; isSound="

    .line 61
    .line 62
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    const-string v4, "CaptureRefactorViewModel"

    .line 73
    .line 74
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    if-nez v2, :cond_3

    .line 78
    .line 79
    goto :goto_2

    .line 80
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    if-ne v0, v3, :cond_4

    .line 85
    .line 86
    const/4 v1, 0x1

    .line 87
    :cond_4
    :goto_2
    return v1
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final 〇〇o0o()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oooO888:Landroid/content/SharedPreferences;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    const-string v2, "KEY_USE_GRADIENTER"

    .line 7
    .line 8
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-ne v0, v2, :cond_0

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    :cond_0
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇o8()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇oo〇O〇80:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇〇()Lkotlinx/coroutines/flow/SharedFlow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/SharedFlow<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->〇080OO8〇0:Lkotlinx/coroutines/flow/SharedFlow;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇〇0880()Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->oO〇8O8oOo:Lcom/intsig/camscanner/capture/mvvm/CaptureZoomModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

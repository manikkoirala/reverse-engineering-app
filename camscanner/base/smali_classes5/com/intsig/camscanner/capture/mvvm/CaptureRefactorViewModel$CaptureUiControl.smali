.class public interface abstract Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl;
.super Ljava/lang/Object;
.source "CaptureRefactorViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CaptureUiControl"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel$CaptureUiControl$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract O08000()Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;
.end method

.method public abstract OO0o〇〇〇〇0()Landroid/os/Handler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract OO88〇OOO()V
.end method

.method public abstract OOO(Landroid/view/MotionEvent;)V
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract OOO8o〇〇(JJLjava/lang/String;Ljava/lang/String;ZIZIZ)V
    .param p5    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract OOO〇O0()Lcom/intsig/camscanner/capture/core/BaseCaptureScene;
.end method

.method public abstract OOo0O()Z
.end method

.method public abstract Oo8Oo00oo(Ljava/lang/String;)V
.end method

.method public abstract O〇8O8〇008()Landroid/view/View;
.end method

.method public abstract getActivity()Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract o8()Lcom/intsig/app/AlertDialog;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract o800o8O()I
.end method

.method public abstract o8oO〇(Z)V
.end method

.method public abstract oO00OOO(Lcom/intsig/camscanner/capture/CaptureMode;)V
.end method

.method public abstract oOO〇〇(Z)V
.end method

.method public abstract oO〇()V
.end method

.method public abstract o〇8()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract o〇O8〇〇o(Z)V
.end method

.method public abstract o〇〇0〇()V
.end method

.method public abstract 〇00〇8()V
.end method

.method public abstract 〇08O8o〇0(Z)V
.end method

.method public abstract 〇0〇O0088o(Z)V
.end method

.method public abstract 〇8(Lcom/intsig/camscanner/capture/CaptureMode;)V
.end method

.method public abstract 〇800OO〇0O()Landroid/view/SurfaceHolder;
.end method

.method public abstract 〇8〇0〇o〇O(Z)V
.end method

.method public abstract 〇O(Z)Z
.end method

.method public abstract 〇O888o0o()V
.end method

.method public abstract 〇O〇()Landroid/view/View;
.end method

.method public abstract 〇o00〇〇Oo()Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract 〇oOO8O8(Z)V
.end method

.method public abstract 〇〇0o()Lcom/intsig/view/RotateImageTextButton;
.end method

.method public abstract 〇〇808〇(Z)V
.end method

.method public abstract 〇〇8O0〇8()Lcom/intsig/view/RotateLayout;
.end method

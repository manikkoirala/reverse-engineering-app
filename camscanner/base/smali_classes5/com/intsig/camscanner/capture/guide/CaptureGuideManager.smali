.class public final Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;
.super Ljava/lang/Object;
.source "CaptureGuideManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/guide/CaptureGuideManager$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Oooo8o0〇:Lcom/intsig/camscanner/capture/guide/CaptureGuideManager$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:Z

.field private OO0o〇〇:Ljava/lang/Runnable;

.field private OO0o〇〇〇〇0:Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;

.field private Oo08:Lcom/intsig/camscanner/capture/CapWaveControl;

.field private oO80:Z

.field private o〇0:Lcom/intsig/camscanner/view/CircleWaveView;

.field private final 〇080:Landroid/app/Activity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇80〇808〇O:Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;

.field private 〇8o8o〇:Z

.field private final 〇O8o08O:Z

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/capture/control/ICaptureControl;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇888:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->Oooo8o0〇:Lcom/intsig/camscanner/capture/guide/CaptureGuideManager$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/capture/control/ICaptureControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "captureControl"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "captureGuideManagerCallback"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 20
    .line 21
    iput-object p2, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 22
    .line 23
    iput-object p3, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇o〇:Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;

    .line 24
    .line 25
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇88()Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇O8o08O:Z

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;ZIZ)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇oOO8O8(ZIZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private final Oo08(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->OO0o〇〇:Ljava/lang/Runnable;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 6
    .line 7
    .line 8
    :cond_0
    const/4 v0, 0x1

    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-static {p0, v2, v0, v1}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇〇888(Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;ZILjava/lang/Object;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->o〇O8〇〇o(Z)V

    .line 15
    .line 16
    .line 17
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo88o8O(Z)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final oo88o8O(ZI)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    new-instance p1, Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;

    .line 8
    .line 9
    iget-object p2, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 10
    .line 11
    invoke-direct {p1, p2}, Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;-><init>(Landroid/app/Activity;)V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    new-instance p1, Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 22
    .line 23
    invoke-direct {p1, v0, v1, p2}, Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/capture/control/ICaptureControl;I)V

    .line 24
    .line 25
    .line 26
    iput-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;

    .line 27
    .line 28
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;

    .line 29
    .line 30
    if-eqz p1, :cond_2

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;->oO80()V

    .line 33
    .line 34
    .line 35
    :cond_2
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o〇0(Z)V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇O8o08O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O888o0o()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇Oo()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_5

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->o〇0:Lcom/intsig/camscanner/view/CircleWaveView;

    .line 19
    .line 20
    new-instance v1, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v2, "checkIsNeedShowWave: useNewWaveView="

    .line 26
    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v2, ", circleWaveView="

    .line 34
    .line 35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    const-string v1, "CaptureGuideManager"

    .line 46
    .line 47
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    const/4 v0, 0x0

    .line 51
    if-eqz p1, :cond_2

    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 54
    .line 55
    const v1, 0x7f0a0367

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    check-cast p1, Lcom/intsig/camscanner/view/CircleWaveView;

    .line 63
    .line 64
    iput-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->o〇0:Lcom/intsig/camscanner/view/CircleWaveView;

    .line 65
    .line 66
    if-eqz p1, :cond_1

    .line 67
    .line 68
    const/4 v1, 0x1

    .line 69
    iput-boolean v1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇〇888:Z

    .line 70
    .line 71
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/CircleWaveView;->〇〇888()V

    .line 75
    .line 76
    .line 77
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_1
    const/4 p1, 0x0

    .line 81
    :goto_0
    if-nez p1, :cond_5

    .line 82
    .line 83
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇〇888:Z

    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->Oo08:Lcom/intsig/camscanner/capture/CapWaveControl;

    .line 87
    .line 88
    if-nez p1, :cond_3

    .line 89
    .line 90
    new-instance p1, Lcom/intsig/camscanner/capture/CapWaveControl;

    .line 91
    .line 92
    iget-object v1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 93
    .line 94
    const v2, 0x3f0253c8

    .line 95
    .line 96
    .line 97
    const v3, 0x3e99999a    # 0.3f

    .line 98
    .line 99
    .line 100
    invoke-direct {p1, v1, v2, v3}, Lcom/intsig/camscanner/capture/CapWaveControl;-><init>(Landroid/app/Activity;FF)V

    .line 101
    .line 102
    .line 103
    iput-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->Oo08:Lcom/intsig/camscanner/capture/CapWaveControl;

    .line 104
    .line 105
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->Oo08:Lcom/intsig/camscanner/capture/CapWaveControl;

    .line 106
    .line 107
    if-eqz p1, :cond_4

    .line 108
    .line 109
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/CapWaveControl;->〇080()Z

    .line 110
    .line 111
    .line 112
    move-result v0

    .line 113
    :cond_4
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇〇888:Z

    .line 114
    .line 115
    :cond_5
    :goto_1
    return-void
    .line 116
    .line 117
.end method

.method private final o〇O8〇〇o(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇80〇808〇O:Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;-><init>(Landroid/app/Activity;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇80〇808〇O:Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;

    .line 13
    .line 14
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇O88〇()V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇80〇808〇O:Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/capture/guide/〇o〇;

    .line 22
    .line 23
    invoke-direct {v1, p1, p0}, Lcom/intsig/camscanner/capture/guide/〇o〇;-><init>(ZLcom/intsig/camscanner/capture/guide/CaptureGuideManager;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;->〇〇888(Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl$OnSkipListener;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;->〇80〇808〇O()V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;->oO80()V

    .line 33
    .line 34
    .line 35
    :cond_1
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private static final 〇00(ZLcom/intsig/camscanner/capture/guide/CaptureGuideManager;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isShowScanFirstDocForDemo()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    if-eqz p0, :cond_0

    .line 17
    .line 18
    iget-object p0, p1, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇o〇:Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;

    .line 19
    .line 20
    invoke-interface {p0}, Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;->o〇0()V

    .line 21
    .line 22
    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇080(ZLcom/intsig/camscanner/capture/guide/CaptureGuideManager;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇00(ZLcom/intsig/camscanner/capture/guide/CaptureGuideManager;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇80〇808〇O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->ooOO()Lcom/intsig/camscanner/capture/CaptureModeMenuManager;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    new-instance v1, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager$disableModeClickAndChangeItsColorForStartDemo$1$1;

    .line 10
    .line 11
    invoke-direct {v1}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager$disableModeClickAndChangeItsColorForStartDemo$1$1;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/CaptureModeMenuManager;->o8(Lcom/intsig/camscanner/capture/CaptureModeMenuManager$CaptureModeMenuCallBack;)V

    .line 15
    .line 16
    .line 17
    const-string v1, "#B2FFFFFF"

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/CaptureModeMenuManager;->〇8o8o〇(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->Oo08(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oOO8O8(ZIZ)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇o〇:Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;->O8()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇o〇:Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;

    .line 7
    .line 8
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;->〇080()V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->oO80:Z

    .line 13
    .line 14
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOOoo8()V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->o〇0(Z)V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->oo88o8O(ZI)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇o〇:Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;

    .line 24
    .line 25
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;->Oo08()V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇80〇808〇O()V

    .line 29
    .line 30
    .line 31
    const/4 p1, 0x0

    .line 32
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo88o8O(Z)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇8o8o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic 〇〇888(Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->o〇0(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method


# virtual methods
.method public final O8ooOoo〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 2
    .line 3
    const v1, 0x7f0a0367

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Lcom/intsig/camscanner/view/CircleWaveView;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->o〇0:Lcom/intsig/camscanner/view/CircleWaveView;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const/4 v1, 0x1

    .line 17
    iput-boolean v1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇〇888:Z

    .line 18
    .line 19
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/CircleWaveView;->〇〇888()V

    .line 23
    .line 24
    .line 25
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/4 v0, 0x0

    .line 29
    :goto_0
    if-nez v0, :cond_1

    .line 30
    .line 31
    const/4 v0, 0x0

    .line 32
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇〇888:Z

    .line 33
    .line 34
    :cond_1
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final OO0o〇〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;->o〇0()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final OO0o〇〇〇〇0()V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->oO80:Z

    .line 3
    .line 4
    iget-boolean v1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇O8o08O:Z

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O888o0o()Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x3

    .line 16
    :goto_0
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->oo88o8O(ZI)V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇80〇808〇O()V

    .line 21
    .line 22
    .line 23
    const-string v0, "CSScan"

    .line 24
    .line 25
    const-string v1, "demo_start"

    .line 26
    .line 27
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
.end method

.method public final OoO8(Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->OO0o〇〇:Ljava/lang/Runnable;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final Oooo8o0〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇80〇808〇O:Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;->〇o〇()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O〇8O8〇008(ZI)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/guide/CaptureGuideDialogFragment;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideDialogFragment;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager$showNewGuide$1;

    .line 7
    .line 8
    invoke-direct {v1, p0, p1, p2}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager$showNewGuide$1;-><init>(Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;ZI)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/guide/CaptureGuideDialogFragment;->oOo〇08〇(Lcom/intsig/camscanner/capture/guide/CaptureGuideDialogFragment$CaptureGuideCallback;)V

    .line 12
    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 15
    .line 16
    instance-of p2, p1, Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    if-eqz p2, :cond_0

    .line 19
    .line 20
    check-cast p1, Landroidx/fragment/app/FragmentActivity;

    .line 21
    .line 22
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    const-string p2, "mActivity.supportFragmen\u2026anager.beginTransaction()"

    .line 31
    .line 32
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    const-class p2, Lcom/intsig/camscanner/capture/guide/CaptureGuideDialogFragment;

    .line 36
    .line 37
    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    invoke-virtual {p1, v0, p2}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    const-string p1, "CaptureGuideManager"

    .line 49
    .line 50
    const-string p2, "mActivity is not FragmentActivity"

    .line 51
    .line 52
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    :goto_0
    const-string p1, "CSScan"

    .line 56
    .line 57
    const-string p2, "demo_show"

    .line 58
    .line 59
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final o800o8O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇8o8o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final oO80()V
    .locals 2

    .line 1
    const-string v0, "CaptureGuideManager"

    .line 2
    .line 3
    const-string v1, "clearWaveAnim: stop"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->Oo08:Lcom/intsig/camscanner/capture/CapWaveControl;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/CapWaveControl;->O8()V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->o〇0:Lcom/intsig/camscanner/view/CircleWaveView;

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/CircleWaveView;->oO80()V

    .line 20
    .line 21
    .line 22
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->o〇0:Lcom/intsig/camscanner/view/CircleWaveView;

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 28
    .line 29
    .line 30
    :cond_2
    iput-boolean v1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇〇888:Z

    .line 31
    .line 32
    return-void
    .line 33
.end method

.method public final 〇0000OOO()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇o〇:Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;->〇o00〇〇Oo()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇0〇O0088o()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8oO0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 8
    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇0(Landroid/content/Context;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇8o8o〇()Landroid/net/Uri;
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/control/CaptureImgDecodeHelper;->o〇0()Lcom/intsig/camscanner/control/CaptureImgDecodeHelper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;->Oo08()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-static {}, Lcom/intsig/camscanner/capture/guide/CapGuideUserStartDemoControl;->O8()[F

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    iget-object v3, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 14
    .line 15
    invoke-virtual {v0, v3, v1, v2}, Lcom/intsig/camscanner/control/CaptureImgDecodeHelper;->〇O8o08O(Landroid/content/Context;Ljava/lang/String;[F)Landroid/net/Uri;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    return-object v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇O00()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇〇888:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇O888o0o(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->oO80:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇O8o08O()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇8o8o〇()Landroid/net/Uri;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v1, 0x1

    .line 9
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0O〇o(Z)V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇o〇:Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;

    .line 13
    .line 14
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;->〇o〇(Landroid/net/Uri;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇O〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->oO80:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇oo〇()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇80〇808〇O:Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/guide/CapGuideUserSkipControl;->Oo08()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-ne v0, v2, :cond_0

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    :cond_0
    if-eqz v1, :cond_1

    .line 15
    .line 16
    return v2

    .line 17
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇8o8o〇:Z

    .line 18
    .line 19
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇808〇(ZI)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/capture/capturemode/CaptureModePreferenceHelper;->〇O888o0o()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->O8()Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    const/4 v2, 0x0

    .line 19
    const/4 v3, 0x1

    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8oO0()Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-eqz v1, :cond_1

    .line 27
    .line 28
    const/4 v1, 0x1

    .line 29
    goto :goto_0

    .line 30
    :cond_1
    const/4 v1, 0x0

    .line 31
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v5, "init\tisShow = "

    .line 37
    .line 38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v4

    .line 48
    const-string v5, "CaptureGuideManager"

    .line 49
    .line 50
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    if-nez v0, :cond_3

    .line 54
    .line 55
    if-eqz v1, :cond_2

    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_2
    invoke-static {v3}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇Oooo〇〇(Z)I

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    const/4 p2, 0x3

    .line 63
    if-ge p1, p2, :cond_6

    .line 64
    .line 65
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->o〇O8〇〇o(Z)V

    .line 66
    .line 67
    .line 68
    goto :goto_2

    .line 69
    :cond_3
    :goto_1
    if-nez v0, :cond_4

    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇080:Landroid/app/Activity;

    .line 72
    .line 73
    invoke-static {v0}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇0(Landroid/content/Context;)I

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    if-nez v0, :cond_5

    .line 78
    .line 79
    :cond_4
    const/4 v2, 0x1

    .line 80
    :cond_5
    iput-boolean v2, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->O8:Z

    .line 81
    .line 82
    if-eqz v2, :cond_6

    .line 83
    .line 84
    iput-boolean v3, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇8o8o〇:Z

    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->〇o〇:Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;

    .line 87
    .line 88
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/normal/CaptureGuideManagerCallback;->O8()V

    .line 89
    .line 90
    .line 91
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->O〇8O8〇008(ZI)V

    .line 92
    .line 93
    .line 94
    :cond_6
    :goto_2
    iget-boolean p1, p0, Lcom/intsig/camscanner/capture/guide/CaptureGuideManager;->O8:Z

    .line 95
    .line 96
    new-instance p2, Ljava/lang/StringBuilder;

    .line 97
    .line 98
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .line 100
    .line 101
    const-string v0, "mNeedGuide = "

    .line 102
    .line 103
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    invoke-static {v5, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    return-void
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public final 〇〇8O0〇8()Z
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇Oooo〇〇(Z)I

    .line 3
    .line 4
    .line 5
    move-result v1

    .line 6
    const/4 v2, 0x3

    .line 7
    if-ge v1, v2, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.class public final Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "QrCodeHistoryLinearProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/capture/qrcode/data/IQrCodeHistoryType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:I

.field private final o〇00O:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider;->o〇00O:I

    .line 6
    .line 7
    const v0, 0x7f0d0496

    .line 8
    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider;->O8o08O8O:I

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string p2, "parent"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p2, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider;->oO80()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-static {p1, v0}, Lcom/chad/library/adapter/base/util/AdapterUtilsKt;->〇080(Landroid/view/ViewGroup;I)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-direct {p2, p0, p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;-><init>(Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider;Landroid/view/View;)V

    .line 17
    .line 18
    .line 19
    return-object p2
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/capture/qrcode/data/IQrCodeHistoryType;)V
    .locals 5
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/capture/qrcode/data/IQrCodeHistoryType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    check-cast p2, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryLinearItem;

    .line 12
    .line 13
    check-cast p1, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;

    .line 14
    .line 15
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryLinearItem;->〇80〇808〇O()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 24
    .line 25
    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatTextView;

    .line 33
    .line 34
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryLinearItem;->〇o00〇〇Oo()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    const/4 v2, 0x0

    .line 39
    const/4 v3, 0x1

    .line 40
    if-eqz v1, :cond_1

    .line 41
    .line 42
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    if-nez v1, :cond_0

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    const/4 v1, 0x0

    .line 50
    goto :goto_1

    .line 51
    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 52
    :goto_1
    if-nez v1, :cond_2

    .line 53
    .line 54
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryLinearItem;->〇o00〇〇Oo()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    goto :goto_4

    .line 59
    :cond_2
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryLinearItem;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    if-eqz v1, :cond_4

    .line 64
    .line 65
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    if-nez v1, :cond_3

    .line 70
    .line 71
    goto :goto_2

    .line 72
    :cond_3
    const/4 v1, 0x0

    .line 73
    goto :goto_3

    .line 74
    :cond_4
    :goto_2
    const/4 v1, 0x1

    .line 75
    :goto_3
    if-nez v1, :cond_5

    .line 76
    .line 77
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryLinearItem;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    goto :goto_4

    .line 82
    :cond_5
    const-string v1, ""

    .line 83
    .line 84
    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryLinearItem;->O8()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    if-eqz v0, :cond_7

    .line 92
    .line 93
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    if-nez v0, :cond_6

    .line 98
    .line 99
    goto :goto_5

    .line 100
    :cond_6
    const/4 v0, 0x0

    .line 101
    goto :goto_6

    .line 102
    :cond_7
    :goto_5
    const/4 v0, 0x1

    .line 103
    :goto_6
    if-eqz v0, :cond_8

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 110
    .line 111
    const-string v1, "--"

    .line 112
    .line 113
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    .line 115
    .line 116
    goto :goto_7

    .line 117
    :cond_8
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 122
    .line 123
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryLinearItem;->O8()Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v1

    .line 127
    const-string v4, "yyyy-MM-dd HH:mm:ss"

    .line 128
    .line 129
    invoke-static {v1, v4}, Lcom/intsig/utils/DateTimeUtil;->o〇0(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v1

    .line 133
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    .line 135
    .line 136
    :goto_7
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryLinearItem;->〇O00()Z

    .line 137
    .line 138
    .line 139
    move-result v0

    .line 140
    const-string v1, "helper.mBinding.llQrCodeHistoryRightCheck"

    .line 141
    .line 142
    const-string v4, "helper.mBinding.ivArrowRight"

    .line 143
    .line 144
    if-ne v0, v3, :cond_9

    .line 145
    .line 146
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 151
    .line 152
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 156
    .line 157
    .line 158
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;->〇080OO8〇0:Landroid/widget/LinearLayout;

    .line 163
    .line 164
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    .line 166
    .line 167
    invoke-static {v0, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 168
    .line 169
    .line 170
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;

    .line 171
    .line 172
    .line 173
    move-result-object p1

    .line 174
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 175
    .line 176
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryLinearItem;->Oooo8o0〇()Z

    .line 177
    .line 178
    .line 179
    move-result p2

    .line 180
    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 181
    .line 182
    .line 183
    goto :goto_8

    .line 184
    :cond_9
    if-nez v0, :cond_a

    .line 185
    .line 186
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;

    .line 187
    .line 188
    .line 189
    move-result-object p2

    .line 190
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 191
    .line 192
    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    invoke-static {p2, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 196
    .line 197
    .line 198
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider$QrCodeLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;

    .line 199
    .line 200
    .line 201
    move-result-object p1

    .line 202
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryLinearBinding;->〇080OO8〇0:Landroid/widget/LinearLayout;

    .line 203
    .line 204
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    invoke-static {p1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 208
    .line 209
    .line 210
    :cond_a
    :goto_8
    return-void
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider;->O8o08O8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/capture/qrcode/data/IQrCodeHistoryType;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider;->o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/capture/qrcode/data/IQrCodeHistoryType;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryLinearProvider;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

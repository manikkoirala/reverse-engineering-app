.class public final Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "QrCodeHistoryTitleProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider$QrCodeTitleHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/capture/qrcode/data/IQrCodeHistoryType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:I

.field private final o〇00O:I


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    const p1, 0x7f0d0497

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider;->O8o08O8O:I

    return-void
.end method

.method public synthetic constructor <init>(ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 3
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider;-><init>(Z)V

    return-void
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string p2, "parent"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p2, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider$QrCodeTitleHolder;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider;->oO80()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-static {p1, v0}, Lcom/chad/library/adapter/base/util/AdapterUtilsKt;->〇080(Landroid/view/ViewGroup;I)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-direct {p2, p0, p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider$QrCodeTitleHolder;-><init>(Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider;Landroid/view/View;)V

    .line 17
    .line 18
    .line 19
    return-object p2
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/capture/qrcode/data/IQrCodeHistoryType;)V
    .locals 2
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/capture/qrcode/data/IQrCodeHistoryType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    check-cast p2, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryTitleItem;

    .line 12
    .line 13
    check-cast p1, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider$QrCodeTitleHolder;

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider$QrCodeTitleHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryTitleBinding;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryTitleBinding;->OO:Landroid/view/View;

    .line 20
    .line 21
    const-string v1, "helper.mBinding.vTopSpace"

    .line 22
    .line 23
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-eqz v1, :cond_0

    .line 31
    .line 32
    const/4 v1, 0x1

    .line 33
    goto :goto_0

    .line 34
    :cond_0
    const/4 v1, 0x0

    .line 35
    :goto_0
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider$QrCodeTitleHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryTitleBinding;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemQrCodeHistoryTitleBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 43
    .line 44
    invoke-virtual {p2}, Lcom/intsig/camscanner/capture/qrcode/data/QrCodeHistoryTitleItem;->〇o00〇〇Oo()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    if-eqz p2, :cond_1

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_1
    const-string p2, ""

    .line 52
    .line 53
    :goto_1
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider;->O8o08O8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/capture/qrcode/data/IQrCodeHistoryType;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider;->o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/capture/qrcode/data/IQrCodeHistoryType;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/qrcode/adapter/provider/QrCodeHistoryTitleProvider;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

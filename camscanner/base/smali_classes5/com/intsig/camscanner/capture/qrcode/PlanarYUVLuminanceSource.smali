.class public Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;
.super Lcom/intsig/camscanner/capture/qrcode/LuminanceSource;
.source "PlanarYUVLuminanceSource.java"


# instance fields
.field private O8:I

.field private Oo08:I

.field private o〇0:I

.field private 〇o〇:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/LuminanceSource;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public o〇0()Lcom/intsig/camscanner/capture/qrcode/YUVImage;
    .locals 14

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/qrcode/LuminanceSource;->〇o〇()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/qrcode/LuminanceSource;->〇080()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->〇o〇:[B

    .line 10
    .line 11
    const/4 v3, 0x0

    .line 12
    const-string v4, "PlanarYUVLuminanceSource"

    .line 13
    .line 14
    if-nez v2, :cond_0

    .line 15
    .line 16
    const-string v0, "mYuvData == null"

    .line 17
    .line 18
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-object v3

    .line 22
    :cond_0
    array-length v5, v2

    .line 23
    mul-int v6, v0, v1

    .line 24
    .line 25
    mul-int/lit8 v6, v6, 0x3

    .line 26
    .line 27
    div-int/lit8 v6, v6, 0x2

    .line 28
    .line 29
    const/high16 v7, 0x800000

    .line 30
    .line 31
    if-le v6, v7, :cond_1

    .line 32
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v1, "dataYUVSize="

    .line 39
    .line 40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    return-object v3

    .line 54
    :cond_1
    new-array v3, v6, [B

    .line 55
    .line 56
    iget v7, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->o〇0:I

    .line 57
    .line 58
    :goto_0
    iget v8, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->o〇0:I

    .line 59
    .line 60
    add-int v9, v8, v1

    .line 61
    .line 62
    if-gt v7, v9, :cond_4

    .line 63
    .line 64
    sub-int v8, v7, v8

    .line 65
    .line 66
    mul-int v8, v8, v0

    .line 67
    .line 68
    iget v9, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->Oo08:I

    .line 69
    .line 70
    :goto_1
    iget v10, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->Oo08:I

    .line 71
    .line 72
    add-int v11, v10, v0

    .line 73
    .line 74
    if-gt v9, v11, :cond_3

    .line 75
    .line 76
    sub-int v10, v9, v10

    .line 77
    .line 78
    add-int/2addr v10, v8

    .line 79
    add-int/lit8 v11, v7, -0x1

    .line 80
    .line 81
    iget v12, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->O8:I

    .line 82
    .line 83
    mul-int v11, v11, v12

    .line 84
    .line 85
    add-int/2addr v11, v9

    .line 86
    const/4 v12, -0x1

    .line 87
    if-le v10, v12, :cond_2

    .line 88
    .line 89
    if-ge v10, v6, :cond_2

    .line 90
    .line 91
    if-le v11, v12, :cond_2

    .line 92
    .line 93
    if-ge v11, v5, :cond_2

    .line 94
    .line 95
    aget-byte v11, v2, v11

    .line 96
    .line 97
    aput-byte v11, v3, v10

    .line 98
    .line 99
    goto :goto_2

    .line 100
    :cond_2
    new-instance v12, Ljava/lang/StringBuilder;

    .line 101
    .line 102
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    .line 104
    .line 105
    const-string v13, "tempIndexOfdataYUV="

    .line 106
    .line 107
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    const-string v10, " tempIndexOfYuv="

    .line 114
    .line 115
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object v10

    .line 125
    invoke-static {v4, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    :goto_2
    add-int/lit8 v9, v9, 0x1

    .line 129
    .line 130
    goto :goto_1

    .line 131
    :cond_3
    add-int/lit8 v7, v7, 0x1

    .line 132
    .line 133
    goto :goto_0

    .line 134
    :cond_4
    new-instance v2, Lcom/intsig/camscanner/capture/qrcode/YUVImage;

    .line 135
    .line 136
    invoke-direct {v2}, Lcom/intsig/camscanner/capture/qrcode/YUVImage;-><init>()V

    .line 137
    .line 138
    .line 139
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/capture/qrcode/YUVImage;->O8([B)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/capture/qrcode/YUVImage;->Oo08(I)V

    .line 143
    .line 144
    .line 145
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/capture/qrcode/YUVImage;->o〇0(I)V

    .line 146
    .line 147
    .line 148
    return-object v2
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇o00〇〇Oo(I[B)[B
    .locals 3

    .line 1
    if-ltz p1, :cond_2

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/qrcode/LuminanceSource;->〇080()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-ge p1, v0, :cond_2

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/qrcode/LuminanceSource;->〇o〇()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz p2, :cond_0

    .line 14
    .line 15
    array-length v1, p2

    .line 16
    if-ge v1, v0, :cond_1

    .line 17
    .line 18
    :cond_0
    new-array p2, v0, [B

    .line 19
    .line 20
    :cond_1
    iget v1, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->o〇0:I

    .line 21
    .line 22
    add-int/2addr p1, v1

    .line 23
    iget v1, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->O8:I

    .line 24
    .line 25
    mul-int p1, p1, v1

    .line 26
    .line 27
    iget v1, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->Oo08:I

    .line 28
    .line 29
    add-int/2addr p1, v1

    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->〇o〇:[B

    .line 31
    .line 32
    const/4 v2, 0x0

    .line 33
    invoke-static {v1, p1, p2, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 34
    .line 35
    .line 36
    return-object p2

    .line 37
    :cond_2
    new-instance p2, Ljava/lang/IllegalArgumentException;

    .line 38
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string v1, "Requested row is outside the image: "

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    throw p2
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇〇888([BIIIIII)V
    .locals 0

    .line 1
    invoke-virtual {p0, p7}, Lcom/intsig/camscanner/capture/qrcode/LuminanceSource;->O8(I)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p6}, Lcom/intsig/camscanner/capture/qrcode/LuminanceSource;->Oo08(I)V

    .line 5
    .line 6
    .line 7
    add-int/2addr p6, p4

    .line 8
    if-gt p6, p2, :cond_0

    .line 9
    .line 10
    add-int/2addr p7, p5

    .line 11
    if-gt p7, p3, :cond_0

    .line 12
    .line 13
    iput-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->〇o〇:[B

    .line 14
    .line 15
    iput p2, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->O8:I

    .line 16
    .line 17
    iput p4, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->Oo08:I

    .line 18
    .line 19
    iput p5, p0, Lcom/intsig/camscanner/capture/qrcode/PlanarYUVLuminanceSource;->o〇0:I

    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 23
    .line 24
    const-string p2, "Crop rectangle does not fit within image data."

    .line 25
    .line 26
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    throw p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.class public abstract Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;
.super Ljava/lang/Object;
.source "CsBarcodeFormat.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$Companion;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatAztec;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCodaBar;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode39;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode93;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode128;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatDataMatrix;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatEan8;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatEan13;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatItf;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatMaxiCode;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatPdf417;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatQrCode;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatRss14;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatRssExpanded;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcA;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcE;,
        Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcEanExtension;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final o0:I

.field private final 〇OOo8〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->OO:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private constructor <init>(ILjava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->o0:I

    .line 4
    iput-object p2, p0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇OOo8〇0:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(ILjava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;-><init>(ILjava/lang/String;)V

    return-void
.end method

.method public static final 〇080(Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->OO:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$Companion;->〇080(Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    return-object p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget v1, p0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->o0:I

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇OOo8〇0:Ljava/lang/String;

    .line 8
    .line 9
    new-instance v3, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v0, "{simpleType="

    .line 18
    .line 19
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string v0, ", dbName="

    .line 26
    .line 27
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v0, "}"

    .line 34
    .line 35
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    return-object v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final 〇o00〇〇Oo()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇o〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->o0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

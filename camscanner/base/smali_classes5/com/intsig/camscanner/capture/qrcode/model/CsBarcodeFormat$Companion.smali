.class public final Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$Companion;
.super Ljava/lang/Object;
.source "CsBarcodeFormat.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080(Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "format"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o〇()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    const/4 v0, 0x1

    .line 11
    if-eq p1, v0, :cond_0

    .line 12
    .line 13
    const-string p1, "qr_code"

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const-string p1, "bar_code"

    .line 17
    .line 18
    :goto_0
    return-object p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatAztec;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatAztec;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    goto/16 :goto_0

    .line 14
    .line 15
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCodaBar;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCodaBar;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_1

    .line 26
    .line 27
    goto/16 :goto_0

    .line 28
    .line 29
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode39;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode39;

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-eqz v1, :cond_2

    .line 40
    .line 41
    goto/16 :goto_0

    .line 42
    .line 43
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode93;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode93;

    .line 44
    .line 45
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-eqz v1, :cond_3

    .line 54
    .line 55
    goto/16 :goto_0

    .line 56
    .line 57
    :cond_3
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode128;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode128;

    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    if-eqz v1, :cond_4

    .line 68
    .line 69
    goto/16 :goto_0

    .line 70
    .line 71
    :cond_4
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatDataMatrix;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatDataMatrix;

    .line 72
    .line 73
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    if-eqz v1, :cond_5

    .line 82
    .line 83
    goto/16 :goto_0

    .line 84
    .line 85
    :cond_5
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatEan8;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatEan8;

    .line 86
    .line 87
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    if-eqz v1, :cond_6

    .line 96
    .line 97
    goto/16 :goto_0

    .line 98
    .line 99
    :cond_6
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatEan13;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatEan13;

    .line 100
    .line 101
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    move-result v1

    .line 109
    if-eqz v1, :cond_7

    .line 110
    .line 111
    goto/16 :goto_0

    .line 112
    .line 113
    :cond_7
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatItf;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatItf;

    .line 114
    .line 115
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v1

    .line 119
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 120
    .line 121
    .line 122
    move-result v1

    .line 123
    if-eqz v1, :cond_8

    .line 124
    .line 125
    goto/16 :goto_0

    .line 126
    .line 127
    :cond_8
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatMaxiCode;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatMaxiCode;

    .line 128
    .line 129
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v1

    .line 133
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 134
    .line 135
    .line 136
    move-result v1

    .line 137
    if-eqz v1, :cond_9

    .line 138
    .line 139
    goto :goto_0

    .line 140
    :cond_9
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatPdf417;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatPdf417;

    .line 141
    .line 142
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 147
    .line 148
    .line 149
    move-result v1

    .line 150
    if-eqz v1, :cond_a

    .line 151
    .line 152
    goto :goto_0

    .line 153
    :cond_a
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatQrCode;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatQrCode;

    .line 154
    .line 155
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v1

    .line 159
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 160
    .line 161
    .line 162
    move-result v1

    .line 163
    if-eqz v1, :cond_b

    .line 164
    .line 165
    goto :goto_0

    .line 166
    :cond_b
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatRss14;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatRss14;

    .line 167
    .line 168
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v1

    .line 172
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 173
    .line 174
    .line 175
    move-result v1

    .line 176
    if-eqz v1, :cond_c

    .line 177
    .line 178
    goto :goto_0

    .line 179
    :cond_c
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatRssExpanded;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatRssExpanded;

    .line 180
    .line 181
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object v1

    .line 185
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 186
    .line 187
    .line 188
    move-result v1

    .line 189
    if-eqz v1, :cond_d

    .line 190
    .line 191
    goto :goto_0

    .line 192
    :cond_d
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcA;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcA;

    .line 193
    .line 194
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object v1

    .line 198
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 199
    .line 200
    .line 201
    move-result v1

    .line 202
    if-eqz v1, :cond_e

    .line 203
    .line 204
    goto :goto_0

    .line 205
    :cond_e
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcE;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcE;

    .line 206
    .line 207
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 208
    .line 209
    .line 210
    move-result-object v1

    .line 211
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 212
    .line 213
    .line 214
    move-result v1

    .line 215
    if-eqz v1, :cond_f

    .line 216
    .line 217
    goto :goto_0

    .line 218
    :cond_f
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcEanExtension;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcEanExtension;

    .line 219
    .line 220
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;->〇o00〇〇Oo()Ljava/lang/String;

    .line 221
    .line 222
    .line 223
    move-result-object v1

    .line 224
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 225
    .line 226
    .line 227
    move-result p1

    .line 228
    if-eqz p1, :cond_10

    .line 229
    .line 230
    goto :goto_0

    .line 231
    :cond_10
    const/4 v0, 0x0

    .line 232
    :goto_0
    return-object v0
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

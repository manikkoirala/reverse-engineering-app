.class public final Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;
.super Lcom/intsig/camscanner/capture/core/BaseCaptureScene;
.source "QRCodeCaptureScene.kt"

# interfaces
.implements Lcom/intsig/camscanner/view/ViewfinderView$ViewfinderFrameListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene$Companion;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8〇o88:Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o〇O0:Landroid/widget/ImageView;

.field private O8〇o〇88:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

.field private OO〇OOo:Lcom/intsig/camscanner/view/FocusIndicatorView;

.field private Oo0O0o8:Lcom/intsig/camscanner/view/ViewfinderView;

.field private O〇O:Lcom/intsig/camscanner/capture/camera/CameraClient1$AutoFocusCallbackForQRcode;

.field private final oOO0880O:Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;

.field private oOO8:Landroid/widget/TextView;

.field private final oOoo80oO:Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇800OO〇0O:Landroid/widget/LinearLayout;

.field private final 〇oo〇O〇80:Lcom/intsig/lifecycle/LifecycleHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇8〇o88:Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V
    .locals 7
    .param p1    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/capture/control/ICaptureControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "captureControl"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "iCaptureViewGroup"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "cameraClient"

    .line 17
    .line 18
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    sget-object v3, Lcom/intsig/camscanner/capture/CaptureMode;->QRCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 22
    .line 23
    move-object v1, p0

    .line 24
    move-object v2, p1

    .line 25
    move-object v4, p2

    .line 26
    move-object v5, p3

    .line 27
    move-object v6, p4

    .line 28
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 29
    .line 30
    .line 31
    const-string p3, "QRCodeCaptureScene"

    .line 32
    .line 33
    invoke-virtual {p0, p3}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇〇0〇88(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    new-instance p3, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;

    .line 37
    .line 38
    new-instance p4, Lcom/intsig/camscanner/capture/qrcode/o〇0;

    .line 39
    .line 40
    invoke-direct {p4, p0}, Lcom/intsig/camscanner/capture/qrcode/o〇0;-><init>(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p3, p1, p2, p4}, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Ljava/lang/Runnable;)V

    .line 44
    .line 45
    .line 46
    iput-object p3, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOoo80oO:Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;

    .line 47
    .line 48
    new-instance p2, Lcom/intsig/lifecycle/LifecycleHandler;

    .line 49
    .line 50
    new-instance p3, Lcom/intsig/camscanner/capture/qrcode/〇〇888;

    .line 51
    .line 52
    invoke-direct {p3, p0}, Lcom/intsig/camscanner/capture/qrcode/〇〇888;-><init>(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;)V

    .line 53
    .line 54
    .line 55
    invoke-direct {p2, p3, p1}, Lcom/intsig/lifecycle/LifecycleHandler;-><init>(Landroid/os/Handler$Callback;Landroidx/lifecycle/LifecycleOwner;)V

    .line 56
    .line 57
    .line 58
    iput-object p2, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇oo〇O〇80:Lcom/intsig/lifecycle/LifecycleHandler;

    .line 59
    .line 60
    new-instance p1, Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;

    .line 61
    .line 62
    new-instance p2, Lcom/intsig/camscanner/capture/qrcode/oO80;

    .line 63
    .line 64
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/capture/qrcode/oO80;-><init>(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;)V

    .line 65
    .line 66
    .line 67
    invoke-direct {p1, p2}, Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;-><init>(Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle$QRCodeCallBack;)V

    .line 68
    .line 69
    .line 70
    iput-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO0880O:Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;

    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private static final O0o8〇O(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8〇0〇o〇O()Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    if-nez v0, :cond_2

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8〇0〇o〇O()Landroid/view/View;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    if-nez v0, :cond_1

    .line 29
    .line 30
    goto :goto_1

    .line 31
    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 32
    .line 33
    .line 34
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->Oo0O0o8:Lcom/intsig/camscanner/view/ViewfinderView;

    .line 35
    .line 36
    if-nez v0, :cond_4

    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    if-eqz v0, :cond_3

    .line 43
    .line 44
    const v1, 0x7f0a1a7c

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    check-cast v0, Lcom/intsig/camscanner/view/ViewfinderView;

    .line 52
    .line 53
    goto :goto_2

    .line 54
    :cond_3
    const/4 v0, 0x0

    .line 55
    :goto_2
    iput-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->Oo0O0o8:Lcom/intsig/camscanner/view/ViewfinderView;

    .line 56
    .line 57
    if-eqz v0, :cond_4

    .line 58
    .line 59
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/view/ViewfinderView;->setViewfinderFrameListener(Lcom/intsig/camscanner/view/ViewfinderView$ViewfinderFrameListener;)V

    .line 60
    .line 61
    .line 62
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOoo80oO:Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;

    .line 63
    .line 64
    iget-object v1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->Oo0O0o8:Lcom/intsig/camscanner/view/ViewfinderView;

    .line 65
    .line 66
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;->〇O〇80o08O(Landroid/view/View;)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO0880O:Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;

    .line 70
    .line 71
    if-eqz v0, :cond_5

    .line 72
    .line 73
    iget-object v1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->Oo0O0o8:Lcom/intsig/camscanner/view/ViewfinderView;

    .line 74
    .line 75
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;->OoO8(Lcom/intsig/camscanner/view/DisplayFramingRectInterface;)V

    .line 76
    .line 77
    .line 78
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇08〇0〇o〇8()V

    .line 79
    .line 80
    .line 81
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->o〇()V

    .line 82
    .line 83
    .line 84
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/util/QRBarUtil;->〇080:Lcom/intsig/camscanner/capture/qrcode/util/QRBarUtil;

    .line 85
    .line 86
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    iget-object v2, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOoo80oO:Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;

    .line 91
    .line 92
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;->O〇8O8〇008()I

    .line 93
    .line 94
    .line 95
    move-result v2

    .line 96
    invoke-virtual {v0, v1, p0, v2}, Lcom/intsig/camscanner/capture/qrcode/util/QRBarUtil;->〇o〇(Landroid/view/View;Lcom/intsig/camscanner/capture/core/BaseCaptureScene;I)V

    .line 97
    .line 98
    .line 99
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO0880O:Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;

    .line 100
    .line 101
    if-eqz v0, :cond_6

    .line 102
    .line 103
    const-wide/16 v1, 0x0

    .line 104
    .line 105
    invoke-virtual {v0, v1, v2, v1, v2}, Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;->〇080(JJ)V

    .line 106
    .line 107
    .line 108
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇〇o0o()V

    .line 109
    .line 110
    .line 111
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private static final O8OO08o(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;Landroid/os/Message;)Z
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "msg"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget p1, p1, Landroid/os/Message;->what:I

    .line 12
    .line 13
    const/16 v0, 0x3e9

    .line 14
    .line 15
    if-ne p1, v0, :cond_0

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇〇o0o()V

    .line 18
    .line 19
    .line 20
    const/4 p0, 0x1

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 p0, 0x0

    .line 23
    :goto_0
    return p0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O8O〇8oo08(Z)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o800o8O()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    new-instance v1, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v2, "enableTorch ="

    .line 15
    .line 16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string v2, " torchState="

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-string v1, "QRCodeCaptureScene"

    .line 35
    .line 36
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 40
    .line 41
    if-eqz v0, :cond_2

    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O〇O〇oO()Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-eqz v0, :cond_1

    .line 52
    .line 53
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o800o8O()Z

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    if-eq p1, v0, :cond_1

    .line 62
    .line 63
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    if-eqz p1, :cond_0

    .line 68
    .line 69
    const-string v2, "torch"

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_0
    const-string v2, "off"

    .line 73
    .line 74
    :goto_0
    invoke-interface {v0, v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O〇8O8〇008(Ljava/lang/String;)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->o88o0O(Z)V

    .line 78
    .line 79
    .line 80
    const-string p1, "enableTorch, succeed finish"

    .line 81
    .line 82
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    .line 84
    .line 85
    return-void

    .line 86
    :catch_0
    move-exception p1

    .line 87
    const-string v0, "enableTorch, Fail to control torch"

    .line 88
    .line 89
    invoke-static {v1, v0, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 90
    .line 91
    .line 92
    goto :goto_1

    .line 93
    :cond_1
    const-string p1, "enableTorch, parameters=null"

    .line 94
    .line 95
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 99
    .line 100
    .line 101
    move-result-object p1

    .line 102
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o800o8O()Z

    .line 103
    .line 104
    .line 105
    move-result p1

    .line 106
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->o88o0O(Z)V

    .line 107
    .line 108
    .line 109
    :cond_2
    const-string p1, "enableTorch, end"

    .line 110
    .line 111
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    return-void
    .line 115
    .line 116
    .line 117
.end method

.method private static final OO8〇(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇o〇o()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic OO〇(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇oOo〇(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o0oO()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO0880O:Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;->oO80()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->o88o0O(Z)V

    .line 15
    .line 16
    .line 17
    iput-object v1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 18
    .line 19
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇oo〇O〇80:Lcom/intsig/lifecycle/LifecycleHandler;

    .line 20
    .line 21
    if-eqz v0, :cond_2

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    :cond_2
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o88o0O(Z)V
    .locals 1

    .line 1
    if-eqz p1, :cond_5

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO8:Landroid/widget/TextView;

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    const v0, 0x7f131e25

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 11
    .line 12
    .line 13
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO8:Landroid/widget/TextView;

    .line 14
    .line 15
    if-eqz p1, :cond_1

    .line 16
    .line 17
    const/4 v0, -0x1

    .line 18
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 19
    .line 20
    .line 21
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O8o〇O0:Landroid/widget/ImageView;

    .line 22
    .line 23
    if-eqz p1, :cond_2

    .line 24
    .line 25
    const v0, 0x7f0807d1

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 29
    .line 30
    .line 31
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 32
    .line 33
    if-eqz p1, :cond_3

    .line 34
    .line 35
    const v0, 0x7f0810c0

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 39
    .line 40
    .line 41
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 42
    .line 43
    if-nez p1, :cond_4

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_4
    const/high16 v0, 0x3f800000    # 1.0f

    .line 47
    .line 48
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO8:Landroid/widget/TextView;

    .line 53
    .line 54
    if-eqz p1, :cond_6

    .line 55
    .line 56
    const v0, 0x7f131e26

    .line 57
    .line 58
    .line 59
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 60
    .line 61
    .line 62
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO8:Landroid/widget/TextView;

    .line 63
    .line 64
    if-eqz p1, :cond_7

    .line 65
    .line 66
    const v0, -0xa5a5a6

    .line 67
    .line 68
    .line 69
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 70
    .line 71
    .line 72
    :cond_7
    iget-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O8o〇O0:Landroid/widget/ImageView;

    .line 73
    .line 74
    if-eqz p1, :cond_8

    .line 75
    .line 76
    const v0, 0x7f0807d0

    .line 77
    .line 78
    .line 79
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 80
    .line 81
    .line 82
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 83
    .line 84
    if-eqz p1, :cond_9

    .line 85
    .line 86
    const v0, 0x7f0810e7

    .line 87
    .line 88
    .line 89
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 90
    .line 91
    .line 92
    :cond_9
    iget-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 93
    .line 94
    if-nez p1, :cond_a

    .line 95
    .line 96
    goto :goto_0

    .line 97
    :cond_a
    const v0, 0x3f19999a    # 0.6f

    .line 98
    .line 99
    .line 100
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 101
    .line 102
    .line 103
    :goto_0
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private final o8〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O8O〇8oo08(Z)V

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇08〇0〇o〇8()V
    .locals 4

    .line 1
    const-string v0, "initQRcodeCamera start"

    .line 2
    .line 3
    const-string v1, "QRCodeCaptureScene"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOoo80oO:Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;->O8〇o()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const-string v0, "initQRcodeCamera isDialogShowing"

    .line 17
    .line 18
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO0880O:Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;

    .line 22
    .line 23
    if-eqz v0, :cond_4

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;->oO80()V

    .line 26
    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO0880O:Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;

    .line 30
    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    const-wide/16 v2, 0x0

    .line 34
    .line 35
    invoke-virtual {v0, v2, v3, v2, v3}, Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;->〇080(JJ)V

    .line 36
    .line 37
    .line 38
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->Oo0O0o8:Lcom/intsig/camscanner/view/ViewfinderView;

    .line 39
    .line 40
    if-nez v0, :cond_2

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_2
    const/4 v2, 0x0

    .line 44
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 45
    .line 46
    .line 47
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->Oo0O0o8:Lcom/intsig/camscanner/view/ViewfinderView;

    .line 48
    .line 49
    if-eqz v0, :cond_3

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ViewfinderView;->〇080()V

    .line 52
    .line 53
    .line 54
    :cond_3
    const-string v0, "initQRcodeCamera isDialogShowing not"

    .line 55
    .line 56
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .line 58
    .line 59
    goto :goto_1

    .line 60
    :catch_0
    move-exception v0

    .line 61
    const-string v2, "Unexpected error initializing camera"

    .line 62
    .line 63
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 64
    .line 65
    .line 66
    :cond_4
    :goto_1
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private static final 〇8〇o〇8(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;Ljava/lang/String;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOoo80oO:Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;

    .line 7
    .line 8
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;->〇oOO8O8(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇O8〇OO〇(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O8OO08o(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇o8oO(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇8〇o〇8(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇oO8O0〇〇O(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O0o8〇O(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇oOo〇(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO0880O:Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;->〇0〇O0088o(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇o〇o()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/qrcode/〇80〇808〇O;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/qrcode/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->usingAdapterClient()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-nez v1, :cond_0

    .line 15
    .line 16
    invoke-static {v0}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->o〇O8〇〇o(Lcom/intsig/callback/Callback0;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O880oOO08()V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    invoke-interface {v0}, Lcom/intsig/callback/Callback0;->call()V

    .line 28
    .line 29
    .line 30
    :goto_0
    const-string v0, "QRCodeCaptureScene"

    .line 31
    .line 32
    const-string v1, "resumeQRCodeMode"

    .line 33
    .line 34
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private final 〇〇O00〇8()Landroid/view/View;
    .locals 3

    .line 1
    new-instance v0, Landroid/view/View;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    const/high16 v1, -0x1000000

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 13
    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 17
    .line 18
    .line 19
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    .line 20
    .line 21
    const/4 v2, -0x1

    .line 22
    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 26
    .line 27
    .line 28
    return-object v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇o0o()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O〇O:Lcom/intsig/camscanner/capture/camera/CameraClient1$AutoFocusCallbackForQRcode;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_2

    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    instance-of v2, v0, Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 11
    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    check-cast v0, Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    move-object v0, v1

    .line 18
    :goto_0
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/camera/CameraClient1;->〇o0O0O8()Lcom/intsig/camscanner/capture/camera/CameraClient1$AutoFocusCallbackForQRcode;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    goto :goto_1

    .line 25
    :cond_1
    move-object v0, v1

    .line 26
    :goto_1
    iput-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O〇O:Lcom/intsig/camscanner/capture/camera/CameraClient1$AutoFocusCallbackForQRcode;

    .line 27
    .line 28
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇oo〇O〇80:Lcom/intsig/lifecycle/LifecycleHandler;

    .line 29
    .line 30
    if-eqz v0, :cond_6

    .line 31
    .line 32
    iget-object v2, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O〇O:Lcom/intsig/camscanner/capture/camera/CameraClient1$AutoFocusCallbackForQRcode;

    .line 33
    .line 34
    if-nez v2, :cond_3

    .line 35
    .line 36
    goto :goto_2

    .line 37
    :cond_3
    if-eqz v2, :cond_4

    .line 38
    .line 39
    const/16 v3, 0x3e9

    .line 40
    .line 41
    invoke-virtual {v2, v0, v3}, Lcom/intsig/camscanner/capture/camera/CameraClient1$AutoFocusCallbackForQRcode;->〇080(Landroid/os/Handler;I)V

    .line 42
    .line 43
    .line 44
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    instance-of v2, v0, Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 49
    .line 50
    if-eqz v2, :cond_5

    .line 51
    .line 52
    move-object v1, v0

    .line 53
    check-cast v1, Lcom/intsig/camscanner/capture/camera/CameraClient1;

    .line 54
    .line 55
    :cond_5
    if-eqz v1, :cond_6

    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O〇O:Lcom/intsig/camscanner/capture/camera/CameraClient1$AutoFocusCallbackForQRcode;

    .line 58
    .line 59
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/capture/camera/CameraClient1;->ooO〇00O(Lcom/intsig/camscanner/capture/camera/CameraClient1$AutoFocusCallbackForQRcode;)V

    .line 60
    .line 61
    .line 62
    :cond_6
    :goto_2
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public static synthetic 〇〇〇0880(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->OO8〇(Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method protected O0o〇〇Oo()Landroid/view/View;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O00()Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    new-instance v2, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController$SettingEntity;

    .line 16
    .line 17
    invoke-direct {v2}, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController$SettingEntity;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/capture/settings/CaptureSettingsController;->O8ooOoo〇(Landroid/content/Context;Lcom/intsig/camscanner/capture/settings/CaptureSettingsController$SettingEntity;)Landroid/view/View;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O8〇o()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected OOO〇O0(Landroid/view/View;)V
    .locals 8

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOO〇O0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    const-string v0, "QRCodeCaptureScene"

    .line 17
    .line 18
    if-nez p1, :cond_1

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    const v2, 0x7f0a0f56

    .line 26
    .line 27
    .line 28
    if-ne v1, v2, :cond_2

    .line 29
    .line 30
    const-string p1, "click image picture"

    .line 31
    .line 32
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    const/4 v2, 0x1

    .line 40
    const/4 v3, 0x1

    .line 41
    const/16 v4, 0x131

    .line 42
    .line 43
    const/4 v5, -0x1

    .line 44
    const-string v6, "qr"

    .line 45
    .line 46
    const/4 v7, 0x0

    .line 47
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/app/IntentUtil;->o〇〇0〇(Landroid/app/Activity;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    goto :goto_2

    .line 51
    :cond_2
    :goto_1
    if-nez p1, :cond_3

    .line 52
    .line 53
    goto :goto_2

    .line 54
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    const v1, 0x7f0a0d0a

    .line 59
    .line 60
    .line 61
    if-ne p1, v1, :cond_4

    .line 62
    .line 63
    const-string p1, "enableTorch,onClick"

    .line 64
    .line 65
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o800o8O()Z

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    xor-int/lit8 p1, p1, 0x1

    .line 77
    .line 78
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O8O〇8oo08(Z)V

    .line 79
    .line 80
    .line 81
    :cond_4
    :goto_2
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method protected OOoo(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "intent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOoo(Landroid/content/Intent;)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOoo80oO:Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;->oo〇(Landroid/content/Intent;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public Ooo8(Landroid/widget/ImageView;Landroid/widget/TextView;)Z
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const v0, 0x7f0805da

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 7
    .line 8
    .line 9
    :cond_0
    if-eqz p2, :cond_1

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->QRCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 12
    .line 13
    iget v0, v0, Lcom/intsig/camscanner/capture/CaptureMode;->mStringRes:I

    .line 14
    .line 15
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 16
    .line 17
    .line 18
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo8(Landroid/widget/ImageView;Landroid/widget/TextView;)Z

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    return p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method protected Oo〇O8o〇8()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇O()Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const v1, 0x7f0a0f56

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    check-cast v0, Lcom/intsig/view/RotateImageTextButton;

    .line 15
    .line 16
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇008〇oo(Lcom/intsig/view/RotateImageTextButton;)V

    .line 17
    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    new-array v0, v0, [Landroid/view/View;

    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Oo〇O()Lcom/intsig/view/RotateImageTextButton;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    aput-object v2, v0, v1

    .line 28
    .line 29
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇8O0O808〇([Landroid/view/View;)V

    .line 30
    .line 31
    .line 32
    :cond_0
    return-void
    .line 33
.end method

.method public O〇0〇o808〇([BII)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO0880O:Lcom/intsig/camscanner/capture/qrcode/QRCodePreviewBorderHandle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/camscanner/capture/preview/AbstractPreviewHandle;->O8([BII)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method protected O〇O〇oO()Landroid/view/View;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected o0()V
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o0()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOoo80oO:Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;

    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;->oo〇(Landroid/content/Intent;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOoo80oO:Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle;->O〇8O8〇008()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const v1, 0x138d9

    .line 24
    .line 25
    .line 26
    if-ne v1, v0, :cond_2

    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const/4 v1, 0x0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    const v2, 0x7f0a016d

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    .line 43
    .line 44
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 45
    .line 46
    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    if-eqz v0, :cond_1

    .line 52
    .line 53
    const v2, 0x7f0a1aad

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    if-eqz v0, :cond_1

    .line 61
    .line 62
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 63
    .line 64
    .line 65
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    if-eqz v0, :cond_2

    .line 70
    .line 71
    const v2, 0x7f0a1aac

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    if-eqz v0, :cond_2

    .line 79
    .line 80
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 81
    .line 82
    .line 83
    :cond_2
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public oO()V
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oO()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/capture/camera/CameraXUtilKt;->o〇O8〇〇o(Lcom/intsig/callback/Callback0;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->o0oO()V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O8〇o〇88:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 12
    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    invoke-interface {v2, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o〇8oOO88(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;)V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O8〇o〇88:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 23
    .line 24
    :cond_0
    const-string v0, "QRCodeCaptureScene"

    .line 25
    .line 26
    const-string v1, "exitCurrentScene"

    .line 27
    .line 28
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇000O0()Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    if-eqz v0, :cond_1

    .line 40
    .line 41
    const/4 v1, 0x1

    .line 42
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;->O8(Z)V

    .line 43
    .line 44
    .line 45
    :cond_1
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method protected oO00OOO()Landroid/view/View;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o088〇〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const v1, 0x7f0d06bc

    .line 16
    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const-string v1, "from(activity)\n         \u2026de_capture_shutter, null)"

    .line 24
    .line 25
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇〇O00〇8()Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    :goto_0
    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->onDestroy()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O08000()Landroid/content/Context;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const/4 v1, 0x0

    .line 9
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->o00OOO8(Landroid/content/Context;Z)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o〇()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0()Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "QRCodeCaptureScene"

    .line 6
    .line 7
    if-eqz v0, :cond_4

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    invoke-interface {v2}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o800o8O()Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    new-instance v3, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v4, "initTorch torchState="

    .line 23
    .line 24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 38
    .line 39
    if-nez v2, :cond_0

    .line 40
    .line 41
    const v2, 0x7f0a0d0a

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    check-cast v2, Landroid/widget/LinearLayout;

    .line 49
    .line 50
    iput-object v2, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 51
    .line 52
    const v2, 0x7f0a0a7f

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    check-cast v2, Landroid/widget/ImageView;

    .line 60
    .line 61
    iput-object v2, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O8o〇O0:Landroid/widget/ImageView;

    .line 62
    .line 63
    const v2, 0x7f0a18b5

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    check-cast v0, Landroid/widget/TextView;

    .line 71
    .line 72
    iput-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->oOO8:Landroid/widget/TextView;

    .line 73
    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 75
    .line 76
    if-eqz v0, :cond_0

    .line 77
    .line 78
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    .line 80
    .line 81
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O〇O〇oO()Z

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    if-eqz v0, :cond_2

    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 92
    .line 93
    if-nez v0, :cond_1

    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_1
    const/4 v2, 0x0

    .line 97
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 98
    .line 99
    .line 100
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o800o8O()Z

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->o88o0O(Z)V

    .line 109
    .line 110
    .line 111
    goto :goto_1

    .line 112
    :cond_2
    const-string v0, "initTorch isFlashTorchSupported"

    .line 113
    .line 114
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 118
    .line 119
    if-nez v0, :cond_3

    .line 120
    .line 121
    goto :goto_1

    .line 122
    :cond_3
    const/16 v2, 0x8

    .line 123
    .line 124
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 125
    .line 126
    .line 127
    :goto_1
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 128
    .line 129
    goto :goto_2

    .line 130
    :cond_4
    const/4 v0, 0x0

    .line 131
    :goto_2
    if-nez v0, :cond_5

    .line 132
    .line 133
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 134
    .line 135
    .line 136
    move-result-object v0

    .line 137
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o800o8O()Z

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    new-instance v2, Ljava/lang/StringBuilder;

    .line 142
    .line 143
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .line 145
    .line 146
    const-string v3, "initTorch assistPreviewLayout == null torchState="

    .line 147
    .line 148
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 152
    .line 153
    .line 154
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    :cond_5
    return-void
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇080O0()V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    invoke-static {p0, v2, v0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇08O8o〇0(Lcom/intsig/camscanner/capture/core/BaseCaptureScene;ZILjava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected 〇O()Landroid/view/View;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const v1, 0x7f0d06bd

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected 〇o8OO0()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-interface {v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o0ooO()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;-><init>(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->O8〇o〇88:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 15
    .line 16
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o〇0OOo〇0()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;

    .line 21
    .line 22
    .line 23
    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    const-string v3, "torch"

    .line 37
    .line 38
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->o00OOO8(Landroid/content/Context;Z)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->o〇8oOO88(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;)V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇o〇o()V

    .line 53
    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->usingAdapterClient()Z

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    .line 65
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->Ooo()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->〇〇808〇()Z

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    if-nez v0, :cond_0

    .line 74
    .line 75
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->O880oOO08()V

    .line 80
    .line 81
    .line 82
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->OO〇OOo:Lcom/intsig/camscanner/view/FocusIndicatorView;

    .line 83
    .line 84
    if-nez v0, :cond_1

    .line 85
    .line 86
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇o00〇〇Oo()Landroid/view/View;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    const v1, 0x7f0a0668

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    check-cast v0, Lcom/intsig/camscanner/view/FocusIndicatorView;

    .line 102
    .line 103
    iput-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->OO〇OOo:Lcom/intsig/camscanner/view/FocusIndicatorView;

    .line 104
    .line 105
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 106
    .line 107
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->OO〇OOo:Lcom/intsig/camscanner/view/FocusIndicatorView;

    .line 108
    .line 109
    if-eqz v0, :cond_2

    .line 110
    .line 111
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/FocusIndicatorView;->Oo08()V

    .line 112
    .line 113
    .line 114
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇O〇80o08O()Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇000O0()Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    if-eqz v0, :cond_3

    .line 123
    .line 124
    const/4 v1, 0x0

    .line 125
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/setting/CaptureSettingControlNew;->O8(Z)V

    .line 126
    .line 127
    .line 128
    :cond_3
    const/4 v0, 0x1

    .line 129
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->OOo88OOo(Z)V

    .line 130
    .line 131
    .line 132
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o〇o()Landroid/view/View;

    .line 133
    .line 134
    .line 135
    move-result-object v0

    .line 136
    if-eqz v0, :cond_4

    .line 137
    .line 138
    const v1, 0x7f0a00f6

    .line 139
    .line 140
    .line 141
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 142
    .line 143
    .line 144
    move-result-object v1

    .line 145
    check-cast v1, Lcom/intsig/camscanner/view/RotateImageView;

    .line 146
    .line 147
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0〇oo(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 148
    .line 149
    .line 150
    const v1, 0x7f0a00f5

    .line 151
    .line 152
    .line 153
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 154
    .line 155
    .line 156
    move-result-object v1

    .line 157
    check-cast v1, Lcom/intsig/camscanner/view/RotateImageView;

    .line 158
    .line 159
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->oO8008O(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 160
    .line 161
    .line 162
    const v1, 0x7f0a00f9

    .line 163
    .line 164
    .line 165
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 166
    .line 167
    .line 168
    move-result-object v1

    .line 169
    check-cast v1, Lcom/intsig/camscanner/view/RotateImageView;

    .line 170
    .line 171
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o88O〇8(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 172
    .line 173
    .line 174
    const v1, 0x7f0a00f8

    .line 175
    .line 176
    .line 177
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 178
    .line 179
    .line 180
    move-result-object v0

    .line 181
    check-cast v0, Lcom/intsig/camscanner/view/RotateImageView;

    .line 182
    .line 183
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->o08oOO(Lcom/intsig/camscanner/view/RotateImageView;)V

    .line 184
    .line 185
    .line 186
    :cond_4
    return-void

    .line 187
    :catchall_0
    move-exception v0

    .line 188
    new-instance v1, Ljava/lang/StringBuilder;

    .line 189
    .line 190
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 191
    .line 192
    .line 193
    const-string v2, "refreshSceneView t="

    .line 194
    .line 195
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v0

    .line 205
    const-string v1, "QRCodeCaptureScene"

    .line 206
    .line 207
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    .line 209
    .line 210
    return-void
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
.end method

.method public 〇oOO8O8()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8O〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇oOO8O8()V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o〇8()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇o〇8()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->o0oO()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->o8〇()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o〇Oo0()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O0〇OO8()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;->〇o〇o()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇00OO(IILandroid/content/Intent;)Z
    .locals 8

    .line 1
    const/16 v0, 0x131

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eq p1, v0, :cond_1

    .line 5
    .line 6
    const/16 p3, 0x2766

    .line 7
    .line 8
    if-eq p1, p3, :cond_0

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_0
    const/4 p1, -0x1

    .line 13
    if-ne p2, p1, :cond_3

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    invoke-virtual {p2, p1}, Landroid/app/Activity;->setResult(I)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-static {p1}, Landroidx/core/app/ActivityCompat;->finishAfterTransition(Landroid/app/Activity;)V

    .line 27
    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_1
    const/4 p1, 0x0

    .line 31
    if-eqz p3, :cond_2

    .line 32
    .line 33
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    goto :goto_0

    .line 38
    :cond_2
    move-object p2, p1

    .line 39
    :goto_0
    new-instance p3, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string v0, "select image for qr scan uri: "

    .line 45
    .line 46
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p3

    .line 56
    const-string v0, "QRCodeCaptureScene"

    .line 57
    .line 58
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    if-eqz p2, :cond_3

    .line 62
    .line 63
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->getActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 64
    .line 65
    .line 66
    move-result-object p3

    .line 67
    invoke-static {p3}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 72
    .line 73
    .line 74
    move-result-object v3

    .line 75
    const/4 v4, 0x0

    .line 76
    new-instance v5, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene$onActivityResult$1;

    .line 77
    .line 78
    invoke-direct {v5, p2, p0, p1}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene$onActivityResult$1;-><init>(Landroid/net/Uri;Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;Lkotlin/coroutines/Continuation;)V

    .line 79
    .line 80
    .line 81
    const/4 v6, 0x2

    .line 82
    const/4 v7, 0x0

    .line 83
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 84
    .line 85
    .line 86
    :cond_3
    :goto_1
    return v1
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public 〇〇808〇(Landroid/graphics/Rect;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇〇〇0〇〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

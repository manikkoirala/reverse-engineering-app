.class public final Lcom/intsig/camscanner/capture/qrcode/util/GoogleQrUtils$Companion;
.super Ljava/lang/Object;
.source "GoogleQrUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/qrcode/util/GoogleQrUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/qrcode/util/GoogleQrUtils$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080(Lcom/google/mlkit/vision/barcode/Barcode;)Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/google/mlkit/vision/barcode/Barcode;->〇o00〇〇Oo()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object p1, v0

    .line 14
    :goto_0
    if-nez p1, :cond_1

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    const/16 v2, 0x1000

    .line 22
    .line 23
    if-ne v1, v2, :cond_2

    .line 24
    .line 25
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatAztec;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatAztec;

    .line 26
    .line 27
    goto/16 :goto_d

    .line 28
    .line 29
    :cond_2
    :goto_1
    if-nez p1, :cond_3

    .line 30
    .line 31
    goto :goto_2

    .line 32
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    const/16 v2, 0x8

    .line 37
    .line 38
    if-ne v1, v2, :cond_4

    .line 39
    .line 40
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCodaBar;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCodaBar;

    .line 41
    .line 42
    goto/16 :goto_d

    .line 43
    .line 44
    :cond_4
    :goto_2
    if-nez p1, :cond_5

    .line 45
    .line 46
    goto :goto_3

    .line 47
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    const/4 v2, 0x2

    .line 52
    if-ne v1, v2, :cond_6

    .line 53
    .line 54
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode39;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode39;

    .line 55
    .line 56
    goto/16 :goto_d

    .line 57
    .line 58
    :cond_6
    :goto_3
    if-nez p1, :cond_7

    .line 59
    .line 60
    goto :goto_4

    .line 61
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    const/4 v2, 0x4

    .line 66
    if-ne v1, v2, :cond_8

    .line 67
    .line 68
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode93;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode93;

    .line 69
    .line 70
    goto/16 :goto_d

    .line 71
    .line 72
    :cond_8
    :goto_4
    if-nez p1, :cond_9

    .line 73
    .line 74
    goto :goto_5

    .line 75
    :cond_9
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    const/4 v2, 0x1

    .line 80
    if-ne v1, v2, :cond_a

    .line 81
    .line 82
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode128;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatCode128;

    .line 83
    .line 84
    goto/16 :goto_d

    .line 85
    .line 86
    :cond_a
    :goto_5
    if-nez p1, :cond_b

    .line 87
    .line 88
    goto :goto_6

    .line 89
    :cond_b
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 90
    .line 91
    .line 92
    move-result v1

    .line 93
    const/16 v2, 0x10

    .line 94
    .line 95
    if-ne v1, v2, :cond_c

    .line 96
    .line 97
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatDataMatrix;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatDataMatrix;

    .line 98
    .line 99
    goto/16 :goto_d

    .line 100
    .line 101
    :cond_c
    :goto_6
    if-nez p1, :cond_d

    .line 102
    .line 103
    goto :goto_7

    .line 104
    :cond_d
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 105
    .line 106
    .line 107
    move-result v1

    .line 108
    const/16 v2, 0x40

    .line 109
    .line 110
    if-ne v1, v2, :cond_e

    .line 111
    .line 112
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatEan8;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatEan8;

    .line 113
    .line 114
    goto :goto_d

    .line 115
    :cond_e
    :goto_7
    if-nez p1, :cond_f

    .line 116
    .line 117
    goto :goto_8

    .line 118
    :cond_f
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 119
    .line 120
    .line 121
    move-result v1

    .line 122
    const/16 v2, 0x20

    .line 123
    .line 124
    if-ne v1, v2, :cond_10

    .line 125
    .line 126
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatEan13;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatEan13;

    .line 127
    .line 128
    goto :goto_d

    .line 129
    :cond_10
    :goto_8
    if-nez p1, :cond_11

    .line 130
    .line 131
    goto :goto_9

    .line 132
    :cond_11
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 133
    .line 134
    .line 135
    move-result v1

    .line 136
    const/16 v2, 0x80

    .line 137
    .line 138
    if-ne v1, v2, :cond_12

    .line 139
    .line 140
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatItf;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatItf;

    .line 141
    .line 142
    goto :goto_d

    .line 143
    :cond_12
    :goto_9
    if-nez p1, :cond_13

    .line 144
    .line 145
    goto :goto_a

    .line 146
    :cond_13
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 147
    .line 148
    .line 149
    move-result v1

    .line 150
    const/16 v2, 0x800

    .line 151
    .line 152
    if-ne v1, v2, :cond_14

    .line 153
    .line 154
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatPdf417;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatPdf417;

    .line 155
    .line 156
    goto :goto_d

    .line 157
    :cond_14
    :goto_a
    if-nez p1, :cond_15

    .line 158
    .line 159
    goto :goto_b

    .line 160
    :cond_15
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 161
    .line 162
    .line 163
    move-result v1

    .line 164
    const/16 v2, 0x100

    .line 165
    .line 166
    if-ne v1, v2, :cond_16

    .line 167
    .line 168
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatQrCode;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatQrCode;

    .line 169
    .line 170
    goto :goto_d

    .line 171
    :cond_16
    :goto_b
    if-nez p1, :cond_17

    .line 172
    .line 173
    goto :goto_c

    .line 174
    :cond_17
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 175
    .line 176
    .line 177
    move-result v1

    .line 178
    const/16 v2, 0x200

    .line 179
    .line 180
    if-ne v1, v2, :cond_18

    .line 181
    .line 182
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcA;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcA;

    .line 183
    .line 184
    goto :goto_d

    .line 185
    :cond_18
    :goto_c
    if-nez p1, :cond_19

    .line 186
    .line 187
    goto :goto_d

    .line 188
    :cond_19
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 189
    .line 190
    .line 191
    move-result p1

    .line 192
    const/16 v1, 0x400

    .line 193
    .line 194
    if-ne p1, v1, :cond_1a

    .line 195
    .line 196
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcE;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/qrcode/model/CsBarcodeFormat$FormatUpcE;

    .line 197
    .line 198
    :cond_1a
    :goto_d
    return-object v0
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

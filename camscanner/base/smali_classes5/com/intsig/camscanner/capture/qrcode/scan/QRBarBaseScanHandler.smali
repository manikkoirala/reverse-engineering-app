.class public abstract Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;
.super Lcom/intsig/camscanner/capture/preview/AbstractPreviewHandle;
.source "QRBarBaseScanHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private OO0o〇〇:J

.field private final 〇8o8o〇:Lcom/intsig/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/callback/Callback<",
            "Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O8o08O:I


# direct methods
.method public constructor <init>(Lcom/intsig/callback/Callback;)V
    .locals 1
    .param p1    # Lcom/intsig/callback/Callback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/callback/Callback<",
            "Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "callbackQrResult"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/preview/AbstractPreviewHandle;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;->〇8o8o〇:Lcom/intsig/callback/Callback;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public final OO0o〇〇(J)V
    .locals 5

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;->OO0o〇〇:J

    .line 2
    .line 3
    iget v2, p0, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;->〇O8o08O:I

    .line 4
    .line 5
    int-to-long v3, v2

    .line 6
    mul-long v0, v0, v3

    .line 7
    .line 8
    add-long/2addr p1, v0

    .line 9
    add-int/lit8 v2, v2, 0x1

    .line 10
    .line 11
    iput v2, p0, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;->〇O8o08O:I

    .line 12
    .line 13
    int-to-long v0, v2

    .line 14
    div-long/2addr p1, v0

    .line 15
    iput-wide p1, p0, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;->OO0o〇〇:J

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public abstract Oooo8o0〇(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .param p2    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation
.end method

.method public final 〇O00()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodeLogAgent;->〇080:Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodeLogAgent;

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;->OO0o〇〇:J

    .line 4
    .line 5
    iget v3, p0, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;->〇O8o08O:I

    .line 6
    .line 7
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodeLogAgent;->〇8o8o〇(JI)V

    .line 8
    .line 9
    .line 10
    const-wide/16 v0, 0x0

    .line 11
    .line 12
    iput-wide v0, p0, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;->OO0o〇〇:J

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    iput v0, p0, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;->〇O8o08O:I

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected final 〇O8o08O()Lcom/intsig/callback/Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/callback/Callback<",
            "Lcom/intsig/camscanner/capture/qrcode/model/CsBarResultModel;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/qrcode/scan/QRBarBaseScanHandler;->〇8o8o〇:Lcom/intsig/callback/Callback;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public abstract 〇O〇(Z)V
.end method

.method public abstract 〇〇808〇(Lcom/intsig/camscanner/view/DisplayFramingRectInterface;)V
.end method

.class public Lcom/intsig/camscanner/capture/CustomSeekBar;
.super Ljava/lang/Object;
.source "CustomSeekBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/CustomSeekBar$CustomOnSeekBarChangeListener;
    }
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/capture/CustomSeekBar$CustomOnSeekBarChangeListener;

.field private 〇080:Landroid/widget/SeekBar;

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/view/VerticalSeekBar;

.field private 〇o〇:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o〇:Z

    .line 6
    .line 7
    instance-of v0, p1, Landroid/widget/SeekBar;

    .line 8
    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o〇:Z

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    check-cast p1, Landroid/widget/SeekBar;

    .line 14
    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇080:Landroid/widget/SeekBar;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    check-cast p1, Lcom/intsig/camscanner/view/VerticalSeekBar;

    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/VerticalSeekBar;

    .line 21
    .line 22
    :goto_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/capture/CustomSeekBar;)Lcom/intsig/camscanner/capture/CustomSeekBar$CustomOnSeekBarChangeListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->O8:Lcom/intsig/camscanner/capture/CustomSeekBar$CustomOnSeekBarChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public O8(I)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o〇:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇080:Landroid/widget/SeekBar;

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/VerticalSeekBar;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/VerticalProgressBar;->setProgress(I)V

    .line 14
    .line 15
    .line 16
    :goto_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇o00〇〇Oo(I)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o〇:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇080:Landroid/widget/SeekBar;

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/VerticalSeekBar;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/AbsVerticalSeekBar;->setMax(I)V

    .line 14
    .line 15
    .line 16
    :goto_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇o〇(Lcom/intsig/camscanner/capture/CustomSeekBar$CustomOnSeekBarChangeListener;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->O8:Lcom/intsig/camscanner/capture/CustomSeekBar$CustomOnSeekBarChangeListener;

    .line 4
    .line 5
    iget-boolean p1, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o〇:Z

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇080:Landroid/widget/SeekBar;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/capture/CustomSeekBar$1;

    .line 12
    .line 13
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/CustomSeekBar$1;-><init>(Lcom/intsig/camscanner/capture/CustomSeekBar;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CustomSeekBar;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/VerticalSeekBar;

    .line 21
    .line 22
    new-instance v0, Lcom/intsig/camscanner/capture/CustomSeekBar$2;

    .line 23
    .line 24
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/capture/CustomSeekBar$2;-><init>(Lcom/intsig/camscanner/capture/CustomSeekBar;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/VerticalSeekBar;->setOnSeekBarChangeListener(Lcom/intsig/camscanner/view/VerticalSeekBar$OnSeekBarChangeListener;)V

    .line 28
    .line 29
    .line 30
    :cond_1
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
.end method

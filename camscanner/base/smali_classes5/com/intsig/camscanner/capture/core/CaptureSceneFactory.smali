.class public final Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;
.super Ljava/lang/Object;
.source "CaptureSceneFactory.kt"

# interfaces
.implements Lcom/intsig/camscanner/capture/core/CaptureSceneNavigationCallBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/capture/core/CaptureSceneFactory$WhenMappings;,
        Lcom/intsig/camscanner/capture/core/CaptureSceneFactory$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8o08O8O:Lcom/intsig/camscanner/capture/core/CaptureSceneFactory$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Landroidx/appcompat/app/AppCompatActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/intsig/camscanner/capture/CaptureMode;",
            "Lcom/intsig/camscanner/capture/core/BaseCaptureScene;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->O8o08O8O:Lcom/intsig/camscanner/capture/core/CaptureSceneFactory$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V
    .locals 1
    .param p1    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/capture/control/ICaptureControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "captureControl"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "iCaptureViewGroup"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "cameraClient"

    .line 17
    .line 18
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    .line 23
    .line 24
    iput-object p1, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 25
    .line 26
    iput-object p2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 27
    .line 28
    iput-object p3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 29
    .line 30
    iput-object p4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 31
    .line 32
    new-instance p1, Ljava/util/EnumMap;

    .line 33
    .line 34
    const-class p2, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 35
    .line 36
    invoke-direct {p1, p2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 37
    .line 38
    .line 39
    iput-object p1, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o〇00O:Ljava/util/Map;

    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private final 〇080(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC_PAPER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-ne p1, v0, :cond_0

    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    const-string v0, "CaptureSceneFactory"

    .line 15
    .line 16
    const-string v2, "F-createCaptureSceneImpl try to get TOPIC_PAPER but off"

    .line 17
    .line 18
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-object v1

    .line 22
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory$WhenMappings;->〇080:[I

    .line 23
    .line 24
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    aget v0, v0, v2

    .line 29
    .line 30
    packed-switch v0, :pswitch_data_0

    .line 31
    .line 32
    .line 33
    goto/16 :goto_1

    .line 34
    .line 35
    :pswitch_0
    new-instance v1, Lcom/intsig/camscanner/capture/invoice/InvoiceCaptureScene;

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 40
    .line 41
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 42
    .line 43
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 44
    .line 45
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/invoice/InvoiceCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 46
    .line 47
    .line 48
    goto/16 :goto_1

    .line 49
    .line 50
    :pswitch_1
    new-instance v1, Lcom/intsig/camscanner/capture/count/CountNumberCaptureScene;

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 53
    .line 54
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 55
    .line 56
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 57
    .line 58
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 59
    .line 60
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/count/CountNumberCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 61
    .line 62
    .line 63
    goto/16 :goto_1

    .line 64
    .line 65
    :pswitch_2
    new-instance v1, Lcom/intsig/camscanner/capture/bank/BankCardJournalCaptureScene;

    .line 66
    .line 67
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 68
    .line 69
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 70
    .line 71
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 72
    .line 73
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 74
    .line 75
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/bank/BankCardJournalCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 76
    .line 77
    .line 78
    goto/16 :goto_1

    .line 79
    .line 80
    :pswitch_3
    new-instance v1, Lcom/intsig/camscanner/capture/whitepad/WhitePadScene;

    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 83
    .line 84
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 85
    .line 86
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 87
    .line 88
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 89
    .line 90
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/whitepad/WhitePadScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 91
    .line 92
    .line 93
    goto/16 :goto_1

    .line 94
    .line 95
    :pswitch_4
    new-instance v1, Lcom/intsig/camscanner/capture/writeboard/WritingPadScene;

    .line 96
    .line 97
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 98
    .line 99
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 100
    .line 101
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 102
    .line 103
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 104
    .line 105
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/writeboard/WritingPadScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 106
    .line 107
    .line 108
    goto/16 :goto_1

    .line 109
    .line 110
    :pswitch_5
    new-instance v1, Lcom/intsig/camscanner/capture/signature/CaptureSignatureCaptureScene;

    .line 111
    .line 112
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 113
    .line 114
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 115
    .line 116
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 117
    .line 118
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 119
    .line 120
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/signature/CaptureSignatureCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 121
    .line 122
    .line 123
    goto/16 :goto_1

    .line 124
    .line 125
    :pswitch_6
    new-instance v1, Lcom/intsig/camscanner/smarterase/SmartEraseCaptureScene;

    .line 126
    .line 127
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 128
    .line 129
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 130
    .line 131
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 132
    .line 133
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 134
    .line 135
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/smarterase/SmartEraseCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 136
    .line 137
    .line 138
    goto/16 :goto_1

    .line 139
    .line 140
    :pswitch_7
    new-instance v6, Lcom/intsig/camscanner/capture/modelmore/ProxyCaptureScene;

    .line 141
    .line 142
    iget-object v1, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 143
    .line 144
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 145
    .line 146
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 147
    .line 148
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 149
    .line 150
    move-object v0, v6

    .line 151
    move-object v5, p0

    .line 152
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/capture/modelmore/ProxyCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;)V

    .line 153
    .line 154
    .line 155
    goto/16 :goto_0

    .line 156
    .line 157
    :pswitch_8
    new-instance v1, Lcom/intsig/camscanner/capture/signature/SignatureCaptureScene;

    .line 158
    .line 159
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 160
    .line 161
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 162
    .line 163
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 164
    .line 165
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 166
    .line 167
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/signature/SignatureCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 168
    .line 169
    .line 170
    goto/16 :goto_1

    .line 171
    .line 172
    :pswitch_9
    new-instance v1, Lcom/intsig/camscanner/capture/qrcode/QRBarCodeCaptureScene;

    .line 173
    .line 174
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 175
    .line 176
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 177
    .line 178
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 179
    .line 180
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 181
    .line 182
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/qrcode/QRBarCodeCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 183
    .line 184
    .line 185
    goto/16 :goto_1

    .line 186
    .line 187
    :pswitch_a
    new-instance v1, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;

    .line 188
    .line 189
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 190
    .line 191
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 192
    .line 193
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 194
    .line 195
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 196
    .line 197
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/qrcode/QRCodeCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 198
    .line 199
    .line 200
    goto/16 :goto_1

    .line 201
    .line 202
    :pswitch_b
    new-instance v1, Lcom/intsig/camscanner/capture/greetcard/GreetCardCaptureScene;

    .line 203
    .line 204
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 205
    .line 206
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 207
    .line 208
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 209
    .line 210
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 211
    .line 212
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/greetcard/GreetCardCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 213
    .line 214
    .line 215
    goto/16 :goto_1

    .line 216
    .line 217
    :pswitch_c
    new-instance v1, Lcom/intsig/camscanner/capture/evidence/EEvidenceCaptureScene;

    .line 218
    .line 219
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 220
    .line 221
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 222
    .line 223
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 224
    .line 225
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 226
    .line 227
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/evidence/EEvidenceCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 228
    .line 229
    .line 230
    goto/16 :goto_1

    .line 231
    .line 232
    :pswitch_d
    new-instance v1, Lcom/intsig/camscanner/capture/topic/TopicPaperCaptureScene;

    .line 233
    .line 234
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 235
    .line 236
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 237
    .line 238
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 239
    .line 240
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 241
    .line 242
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/topic/TopicPaperCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 243
    .line 244
    .line 245
    goto/16 :goto_1

    .line 246
    .line 247
    :pswitch_e
    new-instance v1, Lcom/intsig/camscanner/capture/topic/TopicLegacyCaptureScene;

    .line 248
    .line 249
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 250
    .line 251
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 252
    .line 253
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 254
    .line 255
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 256
    .line 257
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/topic/TopicLegacyCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 258
    .line 259
    .line 260
    goto/16 :goto_1

    .line 261
    .line 262
    :pswitch_f
    new-instance v7, Lcom/intsig/camscanner/capture/topic/AggregatedTopicCaptureScene;

    .line 263
    .line 264
    iget-object v1, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 265
    .line 266
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 267
    .line 268
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 269
    .line 270
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 271
    .line 272
    move-object v0, v7

    .line 273
    move-object v5, p0

    .line 274
    move-object v6, p0

    .line 275
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/capture/topic/AggregatedTopicCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;Lcom/intsig/camscanner/capture/core/CaptureSceneNavigationCallBack;Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;)V

    .line 276
    .line 277
    .line 278
    move-object v1, v7

    .line 279
    goto/16 :goto_1

    .line 280
    .line 281
    :pswitch_10
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 282
    .line 283
    .line 284
    move-result-object v0

    .line 285
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->forbidNcnnLibForBookScene()Z

    .line 286
    .line 287
    .line 288
    move-result v0

    .line 289
    const/4 v2, 0x1

    .line 290
    if-ne v0, v2, :cond_1

    .line 291
    .line 292
    goto/16 :goto_1

    .line 293
    .line 294
    :cond_1
    new-instance v0, Lcom/intsig/camscanner/capture/book/BookCaptureScene;

    .line 295
    .line 296
    iget-object v1, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 297
    .line 298
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 299
    .line 300
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 301
    .line 302
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 303
    .line 304
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/capture/book/BookCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 305
    .line 306
    .line 307
    move-object v1, v0

    .line 308
    goto/16 :goto_1

    .line 309
    .line 310
    :pswitch_11
    new-instance v1, Lcom/intsig/camscanner/image_restore/ImageRestoreCaptureScene;

    .line 311
    .line 312
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 313
    .line 314
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 315
    .line 316
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 317
    .line 318
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 319
    .line 320
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/image_restore/ImageRestoreCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 321
    .line 322
    .line 323
    goto/16 :goto_1

    .line 324
    .line 325
    :pswitch_12
    new-instance v1, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;

    .line 326
    .line 327
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 328
    .line 329
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 330
    .line 331
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 332
    .line 333
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 334
    .line 335
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/certificates/CertificateCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 336
    .line 337
    .line 338
    goto/16 :goto_1

    .line 339
    .line 340
    :pswitch_13
    new-instance v6, Lcom/intsig/camscanner/capture/normal/NormalWorkbenchCaptureRefactorScene;

    .line 341
    .line 342
    iget-object v1, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 343
    .line 344
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 345
    .line 346
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 347
    .line 348
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 349
    .line 350
    move-object v0, v6

    .line 351
    move-object v5, p0

    .line 352
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/capture/normal/NormalWorkbenchCaptureRefactorScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;)V

    .line 353
    .line 354
    .line 355
    goto :goto_0

    .line 356
    :pswitch_14
    new-instance v6, Lcom/intsig/camscanner/capture/normal/NormalMultiCaptureRefactorScene;

    .line 357
    .line 358
    iget-object v1, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 359
    .line 360
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 361
    .line 362
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 363
    .line 364
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 365
    .line 366
    move-object v0, v6

    .line 367
    move-object v5, p0

    .line 368
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/capture/normal/NormalMultiCaptureRefactorScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;)V

    .line 369
    .line 370
    .line 371
    goto :goto_0

    .line 372
    :pswitch_15
    new-instance v6, Lcom/intsig/camscanner/capture/normal/NormalSingleCaptureRefactorScene;

    .line 373
    .line 374
    iget-object v1, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 375
    .line 376
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 377
    .line 378
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 379
    .line 380
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 381
    .line 382
    move-object v0, v6

    .line 383
    move-object v5, p0

    .line 384
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/capture/normal/NormalSingleCaptureRefactorScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;)V

    .line 385
    .line 386
    .line 387
    :goto_0
    move-object v1, v6

    .line 388
    goto/16 :goto_1

    .line 389
    .line 390
    :pswitch_16
    new-instance v1, Lcom/intsig/camscanner/capture/excel/DocToExcelCaptureScene;

    .line 391
    .line 392
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 393
    .line 394
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 395
    .line 396
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 397
    .line 398
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 399
    .line 400
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/excel/DocToExcelCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 401
    .line 402
    .line 403
    goto :goto_1

    .line 404
    :pswitch_17
    new-instance v1, Lcom/intsig/camscanner/capture/toword/DocToWordMultiCaptureScene;

    .line 405
    .line 406
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 407
    .line 408
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 409
    .line 410
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 411
    .line 412
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 413
    .line 414
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/toword/DocToWordMultiCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 415
    .line 416
    .line 417
    goto :goto_1

    .line 418
    :pswitch_18
    new-instance v1, Lcom/intsig/camscanner/ocrapi/OcrCaptureSceneNew;

    .line 419
    .line 420
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 421
    .line 422
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 423
    .line 424
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 425
    .line 426
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 427
    .line 428
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/ocrapi/OcrCaptureSceneNew;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 429
    .line 430
    .line 431
    goto :goto_1

    .line 432
    :pswitch_19
    new-instance v1, Lcom/intsig/camscanner/capture/certificatephoto/NewCertificatePhotoCaptureScene;

    .line 433
    .line 434
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 435
    .line 436
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 437
    .line 438
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 439
    .line 440
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 441
    .line 442
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/certificatephoto/NewCertificatePhotoCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 443
    .line 444
    .line 445
    goto :goto_1

    .line 446
    :pswitch_1a
    sget-object v0, Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;->〇080:Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;

    .line 447
    .line 448
    invoke-virtual {v0}, Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;->oO80()Z

    .line 449
    .line 450
    .line 451
    move-result v0

    .line 452
    if-eqz v0, :cond_2

    .line 453
    .line 454
    new-instance v1, Lcom/intsig/camscanner/capture/translate/TranslateNewCaptureScene;

    .line 455
    .line 456
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 457
    .line 458
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 459
    .line 460
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 461
    .line 462
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 463
    .line 464
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/translate/TranslateNewCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 465
    .line 466
    .line 467
    goto :goto_1

    .line 468
    :cond_2
    new-instance v1, Lcom/intsig/camscanner/capture/translate/TranslateCaptureScene;

    .line 469
    .line 470
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 471
    .line 472
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 473
    .line 474
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 475
    .line 476
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 477
    .line 478
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/translate/TranslateCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 479
    .line 480
    .line 481
    goto :goto_1

    .line 482
    :pswitch_1b
    new-instance v1, Lcom/intsig/camscanner/capture/excel/ExcelCaptureScene;

    .line 483
    .line 484
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 485
    .line 486
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 487
    .line 488
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 489
    .line 490
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 491
    .line 492
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/excel/ExcelCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 493
    .line 494
    .line 495
    goto :goto_1

    .line 496
    :pswitch_1c
    new-instance v1, Lcom/intsig/camscanner/capture/ppt/PPTCaptureScene;

    .line 497
    .line 498
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 499
    .line 500
    iget-object v2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 501
    .line 502
    iget-object v3, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->OO:Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;

    .line 503
    .line 504
    iget-object v4, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇08O〇00〇o:Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 505
    .line 506
    invoke-direct {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/capture/ppt/PPTCaptureScene;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/capture/control/ICaptureControl;Lcom/intsig/camscanner/capture/core/ICaptureViewGroup;Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;)V

    .line 507
    .line 508
    .line 509
    :goto_1
    return-object v1

    .line 510
    nop

    .line 511
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
.end method


# virtual methods
.method public OO0o〇〇〇〇0(Lcom/intsig/camscanner/capture/CaptureMode;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Lcom/intsig/camscanner/capture/CaptureMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "captureMode"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    new-instance v1, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v2, "jumpToOtherCaptureScene "

    .line 18
    .line 19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    const-string v2, "CaptureSceneFactory"

    .line 30
    .line 31
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->〇Oo〇o8(Landroid/content/Intent;)V

    .line 35
    .line 36
    .line 37
    iget-object p2, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇OOo8〇0:Lcom/intsig/camscanner/capture/control/ICaptureControl;

    .line 38
    .line 39
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/capture/control/ICaptureControl;->〇8(Lcom/intsig/camscanner/capture/CaptureMode;)V

    .line 40
    .line 41
    .line 42
    :cond_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final getActivity()Landroidx/appcompat/app/AppCompatActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇o00〇〇Oo(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;
    .locals 2
    .param p1    # Lcom/intsig/camscanner/capture/CaptureMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "captureMode"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o〇00O:Ljava/util/Map;

    .line 7
    .line 8
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    move-object v1, v0

    .line 13
    check-cast v1, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 14
    .line 15
    if-nez v1, :cond_0

    .line 16
    .line 17
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->〇080(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/camscanner/capture/core/CaptureSceneFactory;->o〇00O:Ljava/util/Map;

    .line 24
    .line 25
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 29
    .line 30
    :cond_0
    check-cast v0, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 31
    .line 32
    return-object v0
    .line 33
    .line 34
.end method

.class public final enum Lcom/intsig/camscanner/capture/CaptureMode;
.super Ljava/lang/Enum;
.source "CaptureMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/capture/CaptureMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum BANK_CARD_JOURNAL:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum BARCODE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum BOOK_SPLITTER:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum CAPTURE_SIGNATURE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum CERTIFICATE_PHOTO:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum COUNT_NUMBER:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum DOC_TO_EXCEL:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum DOC_TO_WORD:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum EXCEL:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum E_EVIDENCE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum GREET_CARD:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum IMAGE_RESTORE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum INVOICE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum MODEL_MORE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum MODEL_MORE_DETAIL:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum MODEL_PROXY:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum NAMECARD:Lcom/intsig/camscanner/capture/CaptureMode;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum NONE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum NORMAL:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum NORMAL_CAPTURE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum NORMAL_MULTI:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum NORMAL_SINGLE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum NORMAL_WORKBENCH:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum OCR:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum PPT:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum QRCODE:Lcom/intsig/camscanner/capture/CaptureMode;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum SIGNATURE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum SMART_ERASE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum TOPIC:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum TOPIC_LEGACY:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum TOPIC_PAPER:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum TRANSLATE:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum WHITE_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

.field public static final enum WRITING_PAD:Lcom/intsig/camscanner/capture/CaptureMode;


# instance fields
.field public mChildDrawableRes:I

.field public mDrawableRes:I
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public mParentMode:Lcom/intsig/camscanner/capture/CaptureParentMode;

.field public mStringRes:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public stringText:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 56

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 2
    .line 3
    const-string v1, "NONE"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->NONE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 12
    .line 13
    const v3, 0x7f130102

    .line 14
    .line 15
    .line 16
    const-string v4, "NAMECARD"

    .line 17
    .line 18
    const/4 v5, 0x1

    .line 19
    invoke-direct {v1, v4, v5, v3}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 20
    .line 21
    .line 22
    sput-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->NAMECARD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 23
    .line 24
    new-instance v3, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 25
    .line 26
    const v4, 0x7f130101

    .line 27
    .line 28
    .line 29
    const v6, 0x7f0805a2

    .line 30
    .line 31
    .line 32
    const-string v7, "GREET_CARD"

    .line 33
    .line 34
    const/4 v8, 0x2

    .line 35
    invoke-direct {v3, v7, v8, v4, v6}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;III)V

    .line 36
    .line 37
    .line 38
    sput-object v3, Lcom/intsig/camscanner/capture/CaptureMode;->GREET_CARD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 39
    .line 40
    new-instance v4, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 41
    .line 42
    const v6, 0x7f130105

    .line 43
    .line 44
    .line 45
    const-string v7, "PPT"

    .line 46
    .line 47
    const/4 v9, 0x3

    .line 48
    invoke-direct {v4, v7, v9, v6}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 49
    .line 50
    .line 51
    sput-object v4, Lcom/intsig/camscanner/capture/CaptureMode;->PPT:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 52
    .line 53
    new-instance v6, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 54
    .line 55
    const v7, 0x7f130103

    .line 56
    .line 57
    .line 58
    const-string v10, "NORMAL"

    .line 59
    .line 60
    const/4 v11, 0x4

    .line 61
    invoke-direct {v6, v10, v11, v7}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 62
    .line 63
    .line 64
    sput-object v6, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 65
    .line 66
    new-instance v7, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 67
    .line 68
    const v10, 0x7f13155d

    .line 69
    .line 70
    .line 71
    const-string v12, "NORMAL_CAPTURE"

    .line 72
    .line 73
    const/4 v13, 0x5

    .line 74
    invoke-direct {v7, v12, v13, v10}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 75
    .line 76
    .line 77
    sput-object v7, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_CAPTURE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 78
    .line 79
    new-instance v10, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 80
    .line 81
    const-string v15, "EXCEL"

    .line 82
    .line 83
    const/16 v16, 0x6

    .line 84
    .line 85
    const v17, 0x7f130b3f

    .line 86
    .line 87
    .line 88
    const/16 v18, -0x1

    .line 89
    .line 90
    sget-object v12, Lcom/intsig/camscanner/capture/CaptureParentMode;->OCR:Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 91
    .line 92
    const v20, 0x7f0805b4

    .line 93
    .line 94
    .line 95
    move-object v14, v10

    .line 96
    move-object/from16 v19, v12

    .line 97
    .line 98
    invoke-direct/range {v14 .. v20}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;IIILcom/intsig/camscanner/capture/CaptureParentMode;I)V

    .line 99
    .line 100
    .line 101
    sput-object v10, Lcom/intsig/camscanner/capture/CaptureMode;->EXCEL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 102
    .line 103
    new-instance v14, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 104
    .line 105
    const-string v20, "DOC_TO_EXCEL"

    .line 106
    .line 107
    const/16 v21, 0x7

    .line 108
    .line 109
    const v22, 0x7f13120c

    .line 110
    .line 111
    .line 112
    const/16 v23, -0x1

    .line 113
    .line 114
    const v25, 0x7f0805b4

    .line 115
    .line 116
    .line 117
    move-object/from16 v19, v14

    .line 118
    .line 119
    move-object/from16 v24, v12

    .line 120
    .line 121
    invoke-direct/range {v19 .. v25}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;IIILcom/intsig/camscanner/capture/CaptureParentMode;I)V

    .line 122
    .line 123
    .line 124
    sput-object v14, Lcom/intsig/camscanner/capture/CaptureMode;->DOC_TO_EXCEL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 125
    .line 126
    new-instance v15, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 127
    .line 128
    const-string v20, "OCR"

    .line 129
    .line 130
    const/16 v21, 0x8

    .line 131
    .line 132
    const v22, 0x7f130b3d

    .line 133
    .line 134
    .line 135
    const v25, 0x7f0805b5

    .line 136
    .line 137
    .line 138
    move-object/from16 v19, v15

    .line 139
    .line 140
    invoke-direct/range {v19 .. v25}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;IIILcom/intsig/camscanner/capture/CaptureParentMode;I)V

    .line 141
    .line 142
    .line 143
    sput-object v15, Lcom/intsig/camscanner/capture/CaptureMode;->OCR:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 144
    .line 145
    new-instance v16, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 146
    .line 147
    const-string v20, "DOC_TO_WORD"

    .line 148
    .line 149
    const/16 v21, 0x9

    .line 150
    .line 151
    const v22, 0x7f130c56

    .line 152
    .line 153
    .line 154
    const v25, 0x7f0805b6

    .line 155
    .line 156
    .line 157
    move-object/from16 v19, v16

    .line 158
    .line 159
    invoke-direct/range {v19 .. v25}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;IIILcom/intsig/camscanner/capture/CaptureParentMode;I)V

    .line 160
    .line 161
    .line 162
    sput-object v16, Lcom/intsig/camscanner/capture/CaptureMode;->DOC_TO_WORD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 163
    .line 164
    new-instance v12, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 165
    .line 166
    const v13, 0x7f130b37

    .line 167
    .line 168
    .line 169
    const-string v11, "NORMAL_SINGLE"

    .line 170
    .line 171
    const/16 v9, 0xa

    .line 172
    .line 173
    invoke-direct {v12, v11, v9, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 174
    .line 175
    .line 176
    sput-object v12, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_SINGLE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 177
    .line 178
    new-instance v11, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 179
    .line 180
    const v13, 0x7f130b38

    .line 181
    .line 182
    .line 183
    const-string v9, "NORMAL_MULTI"

    .line 184
    .line 185
    const/16 v8, 0xb

    .line 186
    .line 187
    invoke-direct {v11, v9, v8, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 188
    .line 189
    .line 190
    sput-object v11, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_MULTI:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 191
    .line 192
    new-instance v9, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 193
    .line 194
    const v13, 0x7f1319c9

    .line 195
    .line 196
    .line 197
    const-string v8, "NORMAL_WORKBENCH"

    .line 198
    .line 199
    const/16 v5, 0xc

    .line 200
    .line 201
    invoke-direct {v9, v8, v5, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 202
    .line 203
    .line 204
    sput-object v9, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_WORKBENCH:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 205
    .line 206
    new-instance v8, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 207
    .line 208
    const v13, 0x7f130b3c

    .line 209
    .line 210
    .line 211
    const-string v5, "CERTIFICATE"

    .line 212
    .line 213
    const/16 v2, 0xd

    .line 214
    .line 215
    invoke-direct {v8, v5, v2, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 216
    .line 217
    .line 218
    sput-object v8, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 219
    .line 220
    new-instance v5, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 221
    .line 222
    const v13, 0x7f130b3a

    .line 223
    .line 224
    .line 225
    const-string v2, "TOPIC"

    .line 226
    .line 227
    move-object/from16 v26, v8

    .line 228
    .line 229
    const/16 v8, 0xe

    .line 230
    .line 231
    invoke-direct {v5, v2, v8, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 232
    .line 233
    .line 234
    sput-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 235
    .line 236
    new-instance v2, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 237
    .line 238
    const v13, 0x7f130c90

    .line 239
    .line 240
    .line 241
    const-string v8, "TOPIC_LEGACY"

    .line 242
    .line 243
    move-object/from16 v27, v5

    .line 244
    .line 245
    const/16 v5, 0xf

    .line 246
    .line 247
    invoke-direct {v2, v8, v5, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 248
    .line 249
    .line 250
    sput-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC_LEGACY:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 251
    .line 252
    new-instance v8, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 253
    .line 254
    const v13, 0x7f130c91

    .line 255
    .line 256
    .line 257
    const-string v5, "TOPIC_PAPER"

    .line 258
    .line 259
    move-object/from16 v28, v2

    .line 260
    .line 261
    const/16 v2, 0x10

    .line 262
    .line 263
    invoke-direct {v8, v5, v2, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 264
    .line 265
    .line 266
    sput-object v8, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC_PAPER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 267
    .line 268
    new-instance v5, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 269
    .line 270
    const v13, 0x7f130221

    .line 271
    .line 272
    .line 273
    const v2, 0x7f0805a1

    .line 274
    .line 275
    .line 276
    move-object/from16 v29, v8

    .line 277
    .line 278
    const-string v8, "E_EVIDENCE"

    .line 279
    .line 280
    move-object/from16 v30, v9

    .line 281
    .line 282
    const/16 v9, 0x11

    .line 283
    .line 284
    invoke-direct {v5, v8, v9, v13, v2}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;III)V

    .line 285
    .line 286
    .line 287
    sput-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->E_EVIDENCE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 288
    .line 289
    new-instance v2, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 290
    .line 291
    const-string v8, "QRCODE"

    .line 292
    .line 293
    const/16 v13, 0x12

    .line 294
    .line 295
    const v9, 0x7f130b3b

    .line 296
    .line 297
    .line 298
    move-object/from16 v31, v5

    .line 299
    .line 300
    const v5, 0x7f0805a0

    .line 301
    .line 302
    .line 303
    invoke-direct {v2, v8, v13, v9, v5}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;III)V

    .line 304
    .line 305
    .line 306
    sput-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->QRCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 307
    .line 308
    new-instance v8, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 309
    .line 310
    const v9, 0x7f13131b

    .line 311
    .line 312
    .line 313
    const-string v13, "BARCODE"

    .line 314
    .line 315
    move-object/from16 v32, v2

    .line 316
    .line 317
    const/16 v2, 0x13

    .line 318
    .line 319
    invoke-direct {v8, v13, v2, v9, v5}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;III)V

    .line 320
    .line 321
    .line 322
    sput-object v8, Lcom/intsig/camscanner/capture/CaptureMode;->BARCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 323
    .line 324
    new-instance v5, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 325
    .line 326
    const-string v9, "SIGNATURE"

    .line 327
    .line 328
    const/16 v13, 0x14

    .line 329
    .line 330
    invoke-direct {v5, v9, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;I)V

    .line 331
    .line 332
    .line 333
    sput-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->SIGNATURE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 334
    .line 335
    new-instance v9, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 336
    .line 337
    const v13, 0x7f130b39

    .line 338
    .line 339
    .line 340
    const-string v2, "BOOK_SPLITTER"

    .line 341
    .line 342
    move-object/from16 v33, v5

    .line 343
    .line 344
    const/16 v5, 0x15

    .line 345
    .line 346
    invoke-direct {v9, v2, v5, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 347
    .line 348
    .line 349
    sput-object v9, Lcom/intsig/camscanner/capture/CaptureMode;->BOOK_SPLITTER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 350
    .line 351
    new-instance v2, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 352
    .line 353
    const v13, 0x7f130b3e

    .line 354
    .line 355
    .line 356
    const-string v5, "CERTIFICATE_PHOTO"

    .line 357
    .line 358
    move-object/from16 v34, v9

    .line 359
    .line 360
    const/16 v9, 0x16

    .line 361
    .line 362
    invoke-direct {v2, v5, v9, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 363
    .line 364
    .line 365
    sput-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE_PHOTO:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 366
    .line 367
    new-instance v5, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 368
    .line 369
    const v13, 0x7f130c85

    .line 370
    .line 371
    .line 372
    const-string v9, "TRANSLATE"

    .line 373
    .line 374
    move-object/from16 v35, v2

    .line 375
    .line 376
    const/16 v2, 0x17

    .line 377
    .line 378
    invoke-direct {v5, v9, v2, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 379
    .line 380
    .line 381
    sput-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->TRANSLATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 382
    .line 383
    new-instance v9, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 384
    .line 385
    const/16 v13, 0x18

    .line 386
    .line 387
    const v2, 0x7f130fd5

    .line 388
    .line 389
    .line 390
    move-object/from16 v36, v5

    .line 391
    .line 392
    const-string v5, "IMAGE_RESTORE"

    .line 393
    .line 394
    invoke-direct {v9, v5, v13, v2}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 395
    .line 396
    .line 397
    sput-object v9, Lcom/intsig/camscanner/capture/CaptureMode;->IMAGE_RESTORE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 398
    .line 399
    new-instance v2, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 400
    .line 401
    const-string v5, "MODEL_MORE"

    .line 402
    .line 403
    const/16 v13, 0x19

    .line 404
    .line 405
    move-object/from16 v37, v9

    .line 406
    .line 407
    const v9, 0x7f13066b

    .line 408
    .line 409
    .line 410
    invoke-direct {v2, v5, v13, v9}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 411
    .line 412
    .line 413
    sput-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->MODEL_MORE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 414
    .line 415
    new-instance v5, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 416
    .line 417
    const-string v13, "MODEL_MORE_DETAIL"

    .line 418
    .line 419
    move-object/from16 v38, v2

    .line 420
    .line 421
    const/16 v2, 0x1a

    .line 422
    .line 423
    invoke-direct {v5, v13, v2, v9}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 424
    .line 425
    .line 426
    sput-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->MODEL_MORE_DETAIL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 427
    .line 428
    new-instance v2, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 429
    .line 430
    const-string v13, "MODEL_PROXY"

    .line 431
    .line 432
    move-object/from16 v39, v5

    .line 433
    .line 434
    const/16 v5, 0x1b

    .line 435
    .line 436
    invoke-direct {v2, v13, v5, v9}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 437
    .line 438
    .line 439
    sput-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->MODEL_PROXY:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 440
    .line 441
    new-instance v5, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 442
    .line 443
    const/16 v9, 0x1c

    .line 444
    .line 445
    const v13, 0x7f1312d7

    .line 446
    .line 447
    .line 448
    move-object/from16 v40, v2

    .line 449
    .line 450
    const-string v2, "SMART_ERASE"

    .line 451
    .line 452
    invoke-direct {v5, v2, v9, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 453
    .line 454
    .line 455
    sput-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->SMART_ERASE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 456
    .line 457
    new-instance v2, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 458
    .line 459
    const/16 v9, 0x1d

    .line 460
    .line 461
    const v13, 0x7f130805

    .line 462
    .line 463
    .line 464
    move-object/from16 v41, v5

    .line 465
    .line 466
    const-string v5, "CAPTURE_SIGNATURE"

    .line 467
    .line 468
    invoke-direct {v2, v5, v9, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 469
    .line 470
    .line 471
    sput-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->CAPTURE_SIGNATURE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 472
    .line 473
    new-instance v5, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 474
    .line 475
    const/16 v9, 0x1e

    .line 476
    .line 477
    const v13, 0x7f13156b

    .line 478
    .line 479
    .line 480
    move-object/from16 v42, v2

    .line 481
    .line 482
    const-string v2, "WRITING_PAD"

    .line 483
    .line 484
    invoke-direct {v5, v2, v9, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 485
    .line 486
    .line 487
    sput-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->WRITING_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 488
    .line 489
    new-instance v2, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 490
    .line 491
    const/16 v9, 0x1f

    .line 492
    .line 493
    const v13, 0x7f131805

    .line 494
    .line 495
    .line 496
    move-object/from16 v43, v5

    .line 497
    .line 498
    const-string v5, "WHITE_PAD"

    .line 499
    .line 500
    invoke-direct {v2, v5, v9, v13}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 501
    .line 502
    .line 503
    sput-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->WHITE_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 504
    .line 505
    new-instance v5, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 506
    .line 507
    const-string v45, "BANK_CARD_JOURNAL"

    .line 508
    .line 509
    const/16 v46, 0x20

    .line 510
    .line 511
    const v47, 0x7f131757

    .line 512
    .line 513
    .line 514
    const/16 v48, -0x1

    .line 515
    .line 516
    sget-object v54, Lcom/intsig/camscanner/capture/CaptureParentMode;->BILL:Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 517
    .line 518
    const v50, 0x7f08054c

    .line 519
    .line 520
    .line 521
    move-object/from16 v44, v5

    .line 522
    .line 523
    move-object/from16 v49, v54

    .line 524
    .line 525
    invoke-direct/range {v44 .. v50}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;IIILcom/intsig/camscanner/capture/CaptureParentMode;I)V

    .line 526
    .line 527
    .line 528
    sput-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->BANK_CARD_JOURNAL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 529
    .line 530
    new-instance v9, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 531
    .line 532
    const/16 v13, 0x21

    .line 533
    .line 534
    const v5, 0x7f131829

    .line 535
    .line 536
    .line 537
    move-object/from16 v45, v2

    .line 538
    .line 539
    const-string v2, "COUNT_NUMBER"

    .line 540
    .line 541
    invoke-direct {v9, v2, v13, v5}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 542
    .line 543
    .line 544
    sput-object v9, Lcom/intsig/camscanner/capture/CaptureMode;->COUNT_NUMBER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 545
    .line 546
    new-instance v2, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 547
    .line 548
    const-string v50, "INVOICE"

    .line 549
    .line 550
    const/16 v51, 0x22

    .line 551
    .line 552
    const v52, 0x7f131873

    .line 553
    .line 554
    .line 555
    const/16 v53, -0x1

    .line 556
    .line 557
    const v55, 0x7f080932

    .line 558
    .line 559
    .line 560
    move-object/from16 v49, v2

    .line 561
    .line 562
    invoke-direct/range {v49 .. v55}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;IIILcom/intsig/camscanner/capture/CaptureParentMode;I)V

    .line 563
    .line 564
    .line 565
    sput-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->INVOICE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 566
    .line 567
    const/16 v5, 0x23

    .line 568
    .line 569
    new-array v5, v5, [Lcom/intsig/camscanner/capture/CaptureMode;

    .line 570
    .line 571
    const/4 v13, 0x0

    .line 572
    aput-object v0, v5, v13

    .line 573
    .line 574
    const/4 v0, 0x1

    .line 575
    aput-object v1, v5, v0

    .line 576
    .line 577
    const/4 v0, 0x2

    .line 578
    aput-object v3, v5, v0

    .line 579
    .line 580
    const/4 v0, 0x3

    .line 581
    aput-object v4, v5, v0

    .line 582
    .line 583
    const/4 v0, 0x4

    .line 584
    aput-object v6, v5, v0

    .line 585
    .line 586
    const/4 v0, 0x5

    .line 587
    aput-object v7, v5, v0

    .line 588
    .line 589
    const/4 v0, 0x6

    .line 590
    aput-object v10, v5, v0

    .line 591
    .line 592
    const/4 v0, 0x7

    .line 593
    aput-object v14, v5, v0

    .line 594
    .line 595
    const/16 v0, 0x8

    .line 596
    .line 597
    aput-object v15, v5, v0

    .line 598
    .line 599
    const/16 v0, 0x9

    .line 600
    .line 601
    aput-object v16, v5, v0

    .line 602
    .line 603
    const/16 v0, 0xa

    .line 604
    .line 605
    aput-object v12, v5, v0

    .line 606
    .line 607
    const/16 v0, 0xb

    .line 608
    .line 609
    aput-object v11, v5, v0

    .line 610
    .line 611
    const/16 v0, 0xc

    .line 612
    .line 613
    aput-object v30, v5, v0

    .line 614
    .line 615
    const/16 v0, 0xd

    .line 616
    .line 617
    aput-object v26, v5, v0

    .line 618
    .line 619
    const/16 v0, 0xe

    .line 620
    .line 621
    aput-object v27, v5, v0

    .line 622
    .line 623
    const/16 v0, 0xf

    .line 624
    .line 625
    aput-object v28, v5, v0

    .line 626
    .line 627
    const/16 v0, 0x10

    .line 628
    .line 629
    aput-object v29, v5, v0

    .line 630
    .line 631
    const/16 v0, 0x11

    .line 632
    .line 633
    aput-object v31, v5, v0

    .line 634
    .line 635
    const/16 v0, 0x12

    .line 636
    .line 637
    aput-object v32, v5, v0

    .line 638
    .line 639
    const/16 v0, 0x13

    .line 640
    .line 641
    aput-object v8, v5, v0

    .line 642
    .line 643
    const/16 v0, 0x14

    .line 644
    .line 645
    aput-object v33, v5, v0

    .line 646
    .line 647
    const/16 v0, 0x15

    .line 648
    .line 649
    aput-object v34, v5, v0

    .line 650
    .line 651
    const/16 v0, 0x16

    .line 652
    .line 653
    aput-object v35, v5, v0

    .line 654
    .line 655
    const/16 v0, 0x17

    .line 656
    .line 657
    aput-object v36, v5, v0

    .line 658
    .line 659
    const/16 v0, 0x18

    .line 660
    .line 661
    aput-object v37, v5, v0

    .line 662
    .line 663
    const/16 v0, 0x19

    .line 664
    .line 665
    aput-object v38, v5, v0

    .line 666
    .line 667
    const/16 v0, 0x1a

    .line 668
    .line 669
    aput-object v39, v5, v0

    .line 670
    .line 671
    const/16 v0, 0x1b

    .line 672
    .line 673
    aput-object v40, v5, v0

    .line 674
    .line 675
    const/16 v0, 0x1c

    .line 676
    .line 677
    aput-object v41, v5, v0

    .line 678
    .line 679
    const/16 v0, 0x1d

    .line 680
    .line 681
    aput-object v42, v5, v0

    .line 682
    .line 683
    const/16 v0, 0x1e

    .line 684
    .line 685
    aput-object v43, v5, v0

    .line 686
    .line 687
    const/16 v0, 0x1f

    .line 688
    .line 689
    aput-object v45, v5, v0

    .line 690
    .line 691
    const/16 v0, 0x20

    .line 692
    .line 693
    aput-object v44, v5, v0

    .line 694
    .line 695
    const/16 v0, 0x21

    .line 696
    .line 697
    aput-object v9, v5, v0

    .line 698
    .line 699
    const/16 v0, 0x22

    .line 700
    .line 701
    aput-object v2, v5, v0

    .line 702
    .line 703
    sput-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->$VALUES:[Lcom/intsig/camscanner/capture/CaptureMode;

    .line 704
    .line 705
    return-void
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const/4 p1, -0x1

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/capture/CaptureMode;->mStringRes:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    const/4 v0, -0x1

    .line 3
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;III)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    .line 6
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;IIILcom/intsig/camscanner/capture/CaptureParentMode;I)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILcom/intsig/camscanner/capture/CaptureParentMode;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/intsig/camscanner/capture/CaptureParentMode;",
            "I)V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 8
    iput p3, p0, Lcom/intsig/camscanner/capture/CaptureMode;->mStringRes:I

    .line 9
    iput p4, p0, Lcom/intsig/camscanner/capture/CaptureMode;->mDrawableRes:I

    .line 10
    iput-object p5, p0, Lcom/intsig/camscanner/capture/CaptureMode;->mParentMode:Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 11
    iput p6, p0, Lcom/intsig/camscanner/capture/CaptureMode;->mChildDrawableRes:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p1, p2, p4}, Lcom/intsig/camscanner/capture/CaptureMode;-><init>(Ljava/lang/String;II)V

    .line 5
    iput-object p3, p0, Lcom/intsig/camscanner/capture/CaptureMode;->stringText:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/capture/CaptureMode;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/capture/CaptureMode;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static values()[Lcom/intsig/camscanner/capture/CaptureMode;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->$VALUES:[Lcom/intsig/camscanner/capture/CaptureMode;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/camscanner/capture/CaptureMode;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/capture/CaptureMode;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public getTypeValue()Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->SMART_ERASE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 2
    .line 3
    if-ne p0, v0, :cond_0

    .line 4
    .line 5
    const-string v0, "smart_remove"

    .line 6
    .line 7
    return-object v0

    .line 8
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->COUNT_NUMBER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 9
    .line 10
    if-ne p0, v0, :cond_1

    .line 11
    .line 12
    const-string v0, "count_mode"

    .line 13
    .line 14
    return-object v0

    .line 15
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public setNameRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/capture/CaptureMode;->mStringRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.class final Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "CaptureRefactorActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/CaptureRefactorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ScaleGestureListener"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 3
    .param p1    # Landroid/view/ScaleGestureDetector;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "detector"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O8O(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x0

    .line 13
    const/4 v2, 0x1

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8〇o()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-ne v0, v2, :cond_0

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    :goto_0
    if-nez v0, :cond_1

    .line 26
    .line 27
    const-string p1, "CaptureRefactorActivity"

    .line 28
    .line 29
    const-string v0, "onScale Ignore startCapture, disable enableCapture"

    .line 30
    .line 31
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return v1

    .line 35
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    return v2

    .line 46
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 47
    .line 48
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O〇0o8o8〇(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->O8888()Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-interface {v0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Presenter;->O8ooOoo〇()Z

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    if-eqz v0, :cond_5

    .line 61
    .line 62
    const v0, 0x3f80a3d7    # 1.005f

    .line 63
    .line 64
    .line 65
    cmpl-float v0, p1, v0

    .line 66
    .line 67
    if-lez v0, :cond_3

    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 70
    .line 71
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O〇0o8o8〇(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0o〇〇()Lcom/intsig/camscanner/view/ZoomControl;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ZoomControl;->〇〇888()Z

    .line 80
    .line 81
    .line 82
    :goto_1
    const/4 v1, 0x1

    .line 83
    goto :goto_2

    .line 84
    :cond_3
    const v0, 0x3f7eb852    # 0.995f

    .line 85
    .line 86
    .line 87
    cmpg-float p1, p1, v0

    .line 88
    .line 89
    if-gez p1, :cond_4

    .line 90
    .line 91
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 92
    .line 93
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O〇0o8o8〇(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0o〇〇()Lcom/intsig/camscanner/view/ZoomControl;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ZoomControl;->〇80〇808〇O()Z

    .line 102
    .line 103
    .line 104
    goto :goto_1

    .line 105
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 106
    .line 107
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O〇0o8o8〇(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0o〇〇()Lcom/intsig/camscanner/view/ZoomControl;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ZoomControl;->o〇0()V

    .line 116
    .line 117
    .line 118
    :goto_2
    return v1

    .line 119
    :cond_5
    const v0, 0x3f8147ae    # 1.01f

    .line 120
    .line 121
    .line 122
    cmpl-float v0, p1, v0

    .line 123
    .line 124
    if-lez v0, :cond_6

    .line 125
    .line 126
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 127
    .line 128
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇oOO80o(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Landroid/os/Handler;

    .line 129
    .line 130
    .line 131
    move-result-object p1

    .line 132
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 133
    .line 134
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O〇00O(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Ljava/lang/Runnable;

    .line 135
    .line 136
    .line 137
    move-result-object v0

    .line 138
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 139
    .line 140
    .line 141
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 142
    .line 143
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇oOO80o(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Landroid/os/Handler;

    .line 144
    .line 145
    .line 146
    move-result-object p1

    .line 147
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 148
    .line 149
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇o〇OO80oO(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Ljava/lang/Runnable;

    .line 150
    .line 151
    .line 152
    move-result-object v0

    .line 153
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 154
    .line 155
    .line 156
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 157
    .line 158
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇oOO80o(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Landroid/os/Handler;

    .line 159
    .line 160
    .line 161
    move-result-object p1

    .line 162
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 163
    .line 164
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇o〇OO80oO(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Ljava/lang/Runnable;

    .line 165
    .line 166
    .line 167
    move-result-object v0

    .line 168
    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 169
    .line 170
    .line 171
    return v2

    .line 172
    :cond_6
    const v0, 0x3f7d70a4    # 0.99f

    .line 173
    .line 174
    .line 175
    cmpg-float p1, p1, v0

    .line 176
    .line 177
    if-gez p1, :cond_7

    .line 178
    .line 179
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 180
    .line 181
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇oOO80o(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Landroid/os/Handler;

    .line 182
    .line 183
    .line 184
    move-result-object p1

    .line 185
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 186
    .line 187
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O〇00O(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Ljava/lang/Runnable;

    .line 188
    .line 189
    .line 190
    move-result-object v0

    .line 191
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 192
    .line 193
    .line 194
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 195
    .line 196
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇oOO80o(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Landroid/os/Handler;

    .line 197
    .line 198
    .line 199
    move-result-object p1

    .line 200
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 201
    .line 202
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇o〇OO80oO(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Ljava/lang/Runnable;

    .line 203
    .line 204
    .line 205
    move-result-object v0

    .line 206
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 207
    .line 208
    .line 209
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 210
    .line 211
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇oOO80o(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Landroid/os/Handler;

    .line 212
    .line 213
    .line 214
    move-result-object p1

    .line 215
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 216
    .line 217
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O〇00O(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Ljava/lang/Runnable;

    .line 218
    .line 219
    .line 220
    move-result-object v0

    .line 221
    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 222
    .line 223
    .line 224
    return v2

    .line 225
    :cond_7
    return v1
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 4
    .param p1    # Landroid/view/ScaleGestureDetector;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "detector"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O8O(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x1

    .line 13
    const/4 v2, 0x0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8〇o()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-ne v0, v1, :cond_0

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    :goto_0
    const-string v3, "CaptureRefactorActivity"

    .line 26
    .line 27
    if-nez v0, :cond_1

    .line 28
    .line 29
    const-string p1, "onScaleBegin Ignore startCapture, disable enableCapture"

    .line 30
    .line 31
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return v2

    .line 35
    :cond_1
    const-string v0, "onScaleBegin"

    .line 36
    .line 37
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 41
    .line 42
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O〇O800oo(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)V

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 46
    .line 47
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇oOO80o(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Landroid/os/Handler;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    iget-object v3, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 52
    .line 53
    invoke-static {v3}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O88(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Ljava/lang/Runnable;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 58
    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 61
    .line 62
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->o〇OoO0(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Landroid/view/View;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    if-nez v0, :cond_2

    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 70
    .line 71
    .line 72
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 73
    .line 74
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O0o0(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Landroid/view/View;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    if-nez v0, :cond_3

    .line 79
    .line 80
    goto :goto_2

    .line 81
    :cond_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 82
    .line 83
    .line 84
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 85
    .line 86
    invoke-static {v0, v1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇8oo8888(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;Z)V

    .line 87
    .line 88
    .line 89
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    return p1
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 3
    .param p1    # Landroid/view/ScaleGestureDetector;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "detector"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 7
    .line 8
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O8O(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Lcom/intsig/camscanner/capture/core/BaseCaptureScene;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const/4 v0, 0x0

    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/core/BaseCaptureScene;->O8〇o()Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    const/4 v1, 0x1

    .line 20
    if-ne p1, v1, :cond_0

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    :cond_0
    const-string p1, "CaptureRefactorActivity"

    .line 24
    .line 25
    if-nez v0, :cond_1

    .line 26
    .line 27
    const-string v0, "onScaleEnd Ignore startCapture, onScaleEnd disable enableCapture"

    .line 28
    .line 29
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :cond_1
    const-string v0, "onScaleEnd"

    .line 34
    .line 35
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 39
    .line 40
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O〇0o8o8〇(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/mvvm/CaptureRefactorViewModel;->o〇0o〇〇()Lcom/intsig/camscanner/view/ZoomControl;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ZoomControl;->o〇0()V

    .line 49
    .line 50
    .line 51
    iget-object p1, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 52
    .line 53
    invoke-static {p1}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->〇oOO80o(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Landroid/os/Handler;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/capture/CaptureRefactorActivity$ScaleGestureListener;->o0:Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 58
    .line 59
    invoke-static {v0}, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;->O88(Lcom/intsig/camscanner/capture/CaptureRefactorActivity;)Ljava/lang/Runnable;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    const-wide/16 v1, 0x1388

    .line 64
    .line 65
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 66
    .line 67
    .line 68
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

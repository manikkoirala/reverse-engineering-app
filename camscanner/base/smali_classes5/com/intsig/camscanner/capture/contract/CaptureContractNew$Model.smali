.class public final Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;
.super Ljava/lang/Object;
.source "CaptureContractNew.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/util/PremiumParcelSize;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo08:F

.field private oO80:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇0:Z

.field private 〇080:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/util/PremiumParcelSize;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇:Lcom/intsig/camscanner/util/PremiumParcelSize;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇888:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "undefine"

    .line 2
    iput-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇080:Ljava/lang/String;

    .line 3
    new-instance v0, Lcom/intsig/camscanner/util/PremiumParcelSize;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/intsig/camscanner/util/PremiumParcelSize;-><init>(II)V

    iput-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 4
    new-instance v0, Lcom/intsig/camscanner/util/PremiumParcelSize;

    invoke-direct {v0, v1, v1}, Lcom/intsig/camscanner/util/PremiumParcelSize;-><init>(II)V

    iput-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o〇:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 5
    new-instance v0, Lcom/intsig/camscanner/util/PremiumParcelSize;

    invoke-direct {v0, v1, v1}, Lcom/intsig/camscanner/util/PremiumParcelSize;-><init>(II)V

    iput-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8:Lcom/intsig/camscanner/util/PremiumParcelSize;

    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888:Z

    const-string v0, "undefine_focus"

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;)V
    .locals 3

    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;-><init>()V

    if-eqz p1, :cond_0

    .line 9
    iget-object v0, p1, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇080:Ljava/lang/String;

    iput-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇080:Ljava/lang/String;

    .line 10
    new-instance v0, Lcom/intsig/camscanner/util/PremiumParcelSize;

    iget-object v1, p1, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o〇:Lcom/intsig/camscanner/util/PremiumParcelSize;

    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    move-result v1

    iget-object v2, p1, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o〇:Lcom/intsig/camscanner/util/PremiumParcelSize;

    invoke-virtual {v2}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/util/PremiumParcelSize;-><init>(II)V

    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o〇:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 12
    new-instance v0, Lcom/intsig/camscanner/util/PremiumParcelSize;

    iget-object v1, p1, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8:Lcom/intsig/camscanner/util/PremiumParcelSize;

    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    move-result v1

    iget-object v2, p1, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8:Lcom/intsig/camscanner/util/PremiumParcelSize;

    invoke-virtual {v2}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/util/PremiumParcelSize;-><init>(II)V

    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 14
    iget v0, p1, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08:F

    iput v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08:F

    .line 15
    iget-boolean v0, p1, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0:Z

    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0:Z

    .line 16
    iget-boolean p1, p1, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888:Z

    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final O8()Lcom/intsig/camscanner/util/PremiumParcelSize;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final OO0o〇〇(Lcom/intsig/camscanner/util/PremiumParcelSize;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/util/PremiumParcelSize;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final OO0o〇〇〇〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final Oo08()Lcom/intsig/camscanner/util/PremiumParcelSize;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o〇:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final Oooo8o0〇(Lcom/intsig/camscanner/util/PremiumParcelSize;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/util/PremiumParcelSize;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o〇:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final oO80()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o〇0()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public toString()Ljava/lang/String;
    .locals 9
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o〇:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->O8:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 6
    .line 7
    iget v3, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08:F

    .line 8
    .line 9
    iget-boolean v4, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0:Z

    .line 10
    .line 11
    iget-boolean v5, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888:Z

    .line 12
    .line 13
    iget-object v6, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80:Ljava/lang/String;

    .line 14
    .line 15
    new-instance v7, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v8, "1.flashMode="

    .line 21
    .line 22
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v0, ", 2.previewSize="

    .line 29
    .line 30
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v0, ", 3.pictureSize="

    .line 37
    .line 38
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const-string v0, ", 4.zoomRate="

    .line 45
    .line 46
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    const-string v0, ", 5.isCameraPreviewing="

    .line 53
    .line 54
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    const-string v0, ". 6.isShutterSoundEnabled="

    .line 61
    .line 62
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    const-string v0, ", 7.focusMode="

    .line 69
    .line 70
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    return-object v0
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final 〇080()Lcom/intsig/camscanner/util/PremiumParcelSize;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇80〇808〇O(Lcom/intsig/camscanner/util/PremiumParcelSize;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/util/PremiumParcelSize;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇o00〇〇Oo:Lcom/intsig/camscanner/util/PremiumParcelSize;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇8o8o〇(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇080:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇O8o08O(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇O〇(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->Oo08:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇o00〇〇Oo()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇o〇()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->oO80:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇808〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->〇〇888:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final 〇〇888()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/contract/CaptureContractNew$Model;->o〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

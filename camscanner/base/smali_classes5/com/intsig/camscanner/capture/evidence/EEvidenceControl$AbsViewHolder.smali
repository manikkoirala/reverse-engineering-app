.class public abstract Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;
.super Ljava/lang/Object;
.source "EEvidenceControl.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "AbsViewHolder"
.end annotation


# instance fields
.field final synthetic O8o08O8O:Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;

.field OO:Landroid/widget/TextView;

.field protected o0:Landroid/app/Activity;

.field private o〇00O:Z

.field private 〇08O〇00〇o:Landroid/widget/TextView;

.field 〇OOo8〇0:Landroid/widget/RelativeLayout;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;Landroid/app/Activity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->O8o08O8O:Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o〇00O:Z

    .line 8
    .line 9
    iput-object p2, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o0:Landroid/app/Activity;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O8()Ljava/lang/String;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o0:Landroid/app/Activity;

    .line 2
    .line 3
    const v1, 0x7f1303f9

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x1

    .line 11
    new-array v1, v1, [Ljava/lang/Object;

    .line 12
    .line 13
    const/16 v2, 0x14

    .line 14
    .line 15
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const/4 v3, 0x0

    .line 20
    aput-object v2, v1, v3

    .line 21
    .line 22
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OO0o〇〇〇〇0(Z)V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;->〇8o8o〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "isShowBottomTip = "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 26
    .line 27
    if-eqz p1, :cond_0

    .line 28
    .line 29
    const/4 p1, 0x0

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/16 p1, 0x8

    .line 32
    .line 33
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private o〇0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->O8o08O8O:Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;->〇〇888(Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->〇8o8o〇(Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private 〇8o8o〇(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->〇OOo8〇0:Landroid/widget/RelativeLayout;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/16 p1, 0x8

    .line 8
    .line 9
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->OO0o〇〇〇〇0(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->〇8o8o〇(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public abstract Oo08()I
.end method

.method public oO80()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o0:Landroid/app/Activity;

    .line 2
    .line 3
    const v1, 0x7f0a0f8c

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->〇OOo8〇0:Landroid/widget/RelativeLayout;

    .line 13
    .line 14
    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o0:Landroid/app/Activity;

    .line 18
    .line 19
    const v1, 0x7f0a13d2

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    check-cast v0, Landroid/widget/TextView;

    .line 27
    .line 28
    iput-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->OO:Landroid/widget/TextView;

    .line 29
    .line 30
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o0:Landroid/app/Activity;

    .line 34
    .line 35
    const v1, 0x7f0a13ce

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    check-cast v0, Landroid/widget/TextView;

    .line 43
    .line 44
    iput-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 45
    .line 46
    invoke-static {}, Lcom/intsig/camscanner/capture/capturemode/CaptureModePreferenceHelper;->O8()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    const/4 v1, 0x1

    .line 51
    if-ne v0, v1, :cond_0

    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o0:Landroid/app/Activity;

    .line 54
    .line 55
    const v1, 0x7f0a13cf

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    check-cast v0, Landroid/widget/TextView;

    .line 63
    .line 64
    iput-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 65
    .line 66
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->O8()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const v0, 0x7f0a13d2

    .line 6
    .line 7
    .line 8
    if-ne p1, v0, :cond_3

    .line 9
    .line 10
    const-string p1, "CSDigitalevidence"

    .line 11
    .line 12
    const-string v0, "evidence_locationauth"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->O8o08O8O:Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;->OoO8()Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    if-nez p1, :cond_0

    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->O8o08O8O:Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;

    .line 26
    .line 27
    const v0, 0x7f130281

    .line 28
    .line 29
    .line 30
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;->O8ooOoo〇(I)V

    .line 31
    .line 32
    .line 33
    return-void

    .line 34
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->O8o08O8O:Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;

    .line 35
    .line 36
    invoke-static {p1}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;->oO80(Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;)Z

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    if-nez p1, :cond_1

    .line 41
    .line 42
    iget-object p1, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o0:Landroid/app/Activity;

    .line 43
    .line 44
    const v0, 0x7f130084

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    new-instance p1, Landroid/content/Intent;

    .line 55
    .line 56
    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 57
    .line 58
    .line 59
    const-string v0, "android.settings.LOCATION_SOURCE_SETTINGS"

    .line 60
    .line 61
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o0:Landroid/app/Activity;

    .line 65
    .line 66
    const/16 v1, 0xd5

    .line 67
    .line 68
    invoke-virtual {v0, p1, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 69
    .line 70
    .line 71
    return-void

    .line 72
    :cond_1
    const-string p1, "android.permission.ACCESS_FINE_LOCATION"

    .line 73
    .line 74
    const-string v0, "android.permission.ACCESS_COARSE_LOCATION"

    .line 75
    .line 76
    filled-new-array {p1, v0}, [Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o0:Landroid/app/Activity;

    .line 81
    .line 82
    invoke-static {v0, p1}, Lcom/intsig/util/PermissionUtil;->oo88o8O(Landroid/content/Context;[Ljava/lang/String;)Z

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    if-eqz v0, :cond_2

    .line 87
    .line 88
    iget-object p1, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->O8o08O8O:Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;

    .line 89
    .line 90
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;->〇O00()V

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o0:Landroid/app/Activity;

    .line 95
    .line 96
    new-instance v1, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder$1;

    .line 97
    .line 98
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder$1;-><init>(Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;)V

    .line 99
    .line 100
    .line 101
    invoke-static {v0, p1, v1}, Lcom/intsig/util/PermissionUtil;->Oo08(Landroid/content/Context;[Ljava/lang/String;Lcom/intsig/permission/PermissionCallback;)V

    .line 102
    .line 103
    .line 104
    :cond_3
    :goto_0
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method 〇80〇808〇O(I)V
    .locals 1

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o0:Landroid/app/Activity;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Landroid/view/ViewStub;

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :catch_0
    move-exception p1

    .line 16
    invoke-static {}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl;->〇8o8o〇()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    :goto_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method 〇〇888()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o〇00O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->o〇00O:Z

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->Oo08()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->〇80〇808〇O(I)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/evidence/EEvidenceControl$AbsViewHolder;->oO80()V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

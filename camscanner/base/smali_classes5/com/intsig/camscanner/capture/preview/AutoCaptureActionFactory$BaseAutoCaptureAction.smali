.class Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;
.super Ljava/lang/Object;
.source "AutoCaptureActionFactory.java"

# interfaces
.implements Lcom/intsig/camscanner/capture/preview/AutoCaptureAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BaseAutoCaptureAction"
.end annotation


# instance fields
.field private final 〇080:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

.field 〇o00〇〇Oo:J


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/capture/preview/AutoCaptureState;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;->〇080:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 5
    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    iput-wide v0, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;->〇o00〇〇Oo:J

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public start()V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "start initAutoCaptureState="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;->〇080:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "AutoCaptureActionFactory"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 26
    .line 27
    .line 28
    move-result-wide v0

    .line 29
    iput-wide v0, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;->〇o00〇〇Oo:J

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method

.method public 〇080([III)Lcom/intsig/camscanner/capture/preview/AutoCaptureState;
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory;->〇o〇()Lcom/intsig/camscanner/capture/preview/CachePreviewData;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/capture/preview/CachePreviewData;->〇080([I)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 9
    .line 10
    .line 11
    move-result-wide p1

    .line 12
    iget-wide v0, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;->〇o00〇〇Oo:J

    .line 13
    .line 14
    sub-long/2addr p1, v0

    .line 15
    const-wide/16 v0, 0x3e8

    .line 16
    .line 17
    cmp-long p3, p1, v0

    .line 18
    .line 19
    if-gez p3, :cond_0

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;->〇080:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 22
    .line 23
    return-object p1

    .line 24
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory;->〇o〇()Lcom/intsig/camscanner/capture/preview/CachePreviewData;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/preview/CachePreviewData;->oO80()Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    if-eqz p1, :cond_1

    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;->〇080:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 35
    .line 36
    return-object p1

    .line 37
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/capture/preview/AutoCaptureState;->NULL:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 38
    .line 39
    return-object p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

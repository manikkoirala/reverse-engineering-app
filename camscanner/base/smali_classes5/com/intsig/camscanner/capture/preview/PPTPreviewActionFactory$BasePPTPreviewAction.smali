.class Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;
.super Ljava/lang/Object;
.source "PPTPreviewActionFactory.java"

# interfaces
.implements Lcom/intsig/camscanner/capture/preview/PPTPreviewAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BasePPTPreviewAction"
.end annotation


# instance fields
.field O8:I

.field Oo08:F

.field oO80:J

.field o〇0:I

.field 〇080:I

.field final 〇o00〇〇Oo:Lcom/intsig/camscanner/capture/preview/PPTPreviewState;

.field 〇o〇:J

.field 〇〇888:Lcom/intsig/camscanner/capture/preview/CachePreviewData;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/capture/preview/PPTPreviewState;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/high16 v0, 0x3f800000    # 1.0f

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->Oo08:F

    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    iput v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->o〇0:I

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/capture/preview/CachePreviewData;->〇8o8o〇()Lcom/intsig/camscanner/capture/preview/CachePreviewData;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->〇〇888:Lcom/intsig/camscanner/capture/preview/CachePreviewData;

    .line 16
    .line 17
    const-wide/16 v0, 0x1f4

    .line 18
    .line 19
    iput-wide v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->oO80:J

    .line 20
    .line 21
    iput-object p1, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/preview/PPTPreviewState;

    .line 22
    .line 23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 24
    .line 25
    .line 26
    move-result-wide v0

    .line 27
    iput-wide v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->〇o〇:J

    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->〇〇888:Lcom/intsig/camscanner/capture/preview/CachePreviewData;

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/preview/CachePreviewData;->〇O8o08O()V

    .line 32
    .line 33
    .line 34
    return-void
.end method


# virtual methods
.method public O8(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->Oo08:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public reset()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public start()V
    .locals 2

    .line 1
    const/4 v0, -0x1

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->〇080:I

    .line 3
    .line 4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 5
    .line 6
    .line 7
    move-result-wide v0

    .line 8
    iput-wide v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->〇o〇:J

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->〇〇888:Lcom/intsig/camscanner/capture/preview/CachePreviewData;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/preview/CachePreviewData;->〇O8o08O()V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    iput v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->o〇0:I

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇080([III)Lcom/intsig/camscanner/capture/preview/PPTPreviewState;
    .locals 2

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->〇〇888:Lcom/intsig/camscanner/capture/preview/CachePreviewData;

    .line 2
    .line 3
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/capture/preview/CachePreviewData;->〇080([I)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 7
    .line 8
    .line 9
    move-result-wide p1

    .line 10
    iget-wide v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->〇o〇:J

    .line 11
    .line 12
    sub-long/2addr p1, v0

    .line 13
    iget-wide v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->oO80:J

    .line 14
    .line 15
    cmp-long p3, p1, v0

    .line 16
    .line 17
    if-gez p3, :cond_0

    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/preview/PPTPreviewState;

    .line 20
    .line 21
    return-object p1

    .line 22
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/capture/preview/PPTPreviewState;->NULL:Lcom/intsig/camscanner/capture/preview/PPTPreviewState;

    .line 23
    .line 24
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public 〇o00〇〇Oo(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->O8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇o〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/preview/PPTPreviewActionFactory$BasePPTPreviewAction;->o〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

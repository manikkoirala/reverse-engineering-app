.class Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$SearchAction;
.super Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;
.source "AutoCaptureActionFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchAction"
.end annotation


# instance fields
.field private 〇o〇:J


# direct methods
.method constructor <init>()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/preview/AutoCaptureState;->SEARCH:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;-><init>(Lcom/intsig/camscanner/capture/preview/AutoCaptureState;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    iput-wide v0, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$SearchAction;->〇o〇:J

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public start()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;->start()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 5
    .line 6
    .line 7
    move-result-wide v0

    .line 8
    iput-wide v0, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$SearchAction;->〇o〇:J

    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory;->〇o〇()Lcom/intsig/camscanner/capture/preview/CachePreviewData;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/preview/CachePreviewData;->〇O8o08O()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇080([III)Lcom/intsig/camscanner/capture/preview/AutoCaptureState;
    .locals 2

    .line 1
    invoke-super {p0, p1, p2, p3}, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;->〇080([III)Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    sget-object v0, Lcom/intsig/camscanner/capture/preview/AutoCaptureState;->NULL:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 6
    .line 7
    if-eq p1, v0, :cond_0

    .line 8
    .line 9
    return-object p1

    .line 10
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory;->〇o〇()Lcom/intsig/camscanner/capture/preview/CachePreviewData;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-virtual {p1, p2, p3}, Lcom/intsig/camscanner/capture/preview/CachePreviewData;->OO0o〇〇〇〇0(II)Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    if-eqz p1, :cond_1

    .line 19
    .line 20
    sget-object p1, Lcom/intsig/camscanner/capture/preview/AutoCaptureState;->CLOSER:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 21
    .line 22
    return-object p1

    .line 23
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 24
    .line 25
    .line 26
    move-result-wide p1

    .line 27
    iget-wide v0, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$BaseAutoCaptureAction;->〇o00〇〇Oo:J

    .line 28
    .line 29
    sub-long/2addr p1, v0

    .line 30
    const-wide/16 v0, 0x2710

    .line 31
    .line 32
    cmp-long p3, p1, v0

    .line 33
    .line 34
    if-lez p3, :cond_2

    .line 35
    .line 36
    sget-object p1, Lcom/intsig/camscanner/capture/preview/AutoCaptureState;->NOT_FIND:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 37
    .line 38
    return-object p1

    .line 39
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory;->〇o〇()Lcom/intsig/camscanner/capture/preview/CachePreviewData;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-static {}, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory;->〇o00〇〇Oo()I

    .line 44
    .line 45
    .line 46
    move-result p2

    .line 47
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/capture/preview/CachePreviewData;->〇80〇808〇O(I)Z

    .line 48
    .line 49
    .line 50
    move-result p1

    .line 51
    if-eqz p1, :cond_4

    .line 52
    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 54
    .line 55
    .line 56
    move-result-wide p1

    .line 57
    iget-wide v0, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$SearchAction;->〇o〇:J

    .line 58
    .line 59
    sub-long/2addr p1, v0

    .line 60
    const-wide/16 v0, 0x1f4

    .line 61
    .line 62
    cmp-long p3, p1, v0

    .line 63
    .line 64
    if-lez p3, :cond_3

    .line 65
    .line 66
    sget-object p1, Lcom/intsig/camscanner/capture/preview/AutoCaptureState;->DO_NOT_MOVE:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 67
    .line 68
    return-object p1

    .line 69
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/capture/preview/AutoCaptureState;->SEARCH:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 70
    .line 71
    return-object p1

    .line 72
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 73
    .line 74
    .line 75
    move-result-wide p1

    .line 76
    iput-wide p1, p0, Lcom/intsig/camscanner/capture/preview/AutoCaptureActionFactory$SearchAction;->〇o〇:J

    .line 77
    .line 78
    sget-object p1, Lcom/intsig/camscanner/capture/preview/AutoCaptureState;->SEARCH:Lcom/intsig/camscanner/capture/preview/AutoCaptureState;

    .line 79
    .line 80
    return-object p1
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

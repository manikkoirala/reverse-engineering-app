.class public final enum Lcom/intsig/camscanner/capture/CaptureParentMode;
.super Ljava/lang/Enum;
.source "CaptureParentMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/capture/CaptureParentMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/capture/CaptureParentMode;

.field public static final enum BILL:Lcom/intsig/camscanner/capture/CaptureParentMode;

.field public static final enum OCR:Lcom/intsig/camscanner/capture/CaptureParentMode;


# instance fields
.field public mNeedMerge:Z

.field public final mStringRes:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    new-instance v0, Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 2
    .line 3
    const v1, 0x7f130b3d

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/capture/CaptureModeMergeExp;->o〇0()Z

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    const-string v3, "OCR"

    .line 11
    .line 12
    const/4 v4, 0x0

    .line 13
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/intsig/camscanner/capture/CaptureParentMode;-><init>(Ljava/lang/String;IIZ)V

    .line 14
    .line 15
    .line 16
    sput-object v0, Lcom/intsig/camscanner/capture/CaptureParentMode;->OCR:Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 17
    .line 18
    new-instance v1, Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 19
    .line 20
    const v2, 0x7f131872

    .line 21
    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/capture/invoice/InvoiceExp;->〇080()Z

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    const-string v5, "BILL"

    .line 28
    .line 29
    const/4 v6, 0x1

    .line 30
    invoke-direct {v1, v5, v6, v2, v3}, Lcom/intsig/camscanner/capture/CaptureParentMode;-><init>(Ljava/lang/String;IIZ)V

    .line 31
    .line 32
    .line 33
    sput-object v1, Lcom/intsig/camscanner/capture/CaptureParentMode;->BILL:Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 34
    .line 35
    const/4 v2, 0x2

    .line 36
    new-array v2, v2, [Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 37
    .line 38
    aput-object v0, v2, v4

    .line 39
    .line 40
    aput-object v1, v2, v6

    .line 41
    .line 42
    sput-object v2, Lcom/intsig/camscanner/capture/CaptureParentMode;->$VALUES:[Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const/4 p1, -0x1

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/capture/CaptureParentMode;->mStringRes:I

    const/4 p1, 0x0

    .line 3
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/CaptureParentMode;->mNeedMerge:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5
    iput p3, p0, Lcom/intsig/camscanner/capture/CaptureParentMode;->mStringRes:I

    .line 6
    iput-boolean p4, p0, Lcom/intsig/camscanner/capture/CaptureParentMode;->mNeedMerge:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/capture/CaptureParentMode;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static values()[Lcom/intsig/camscanner/capture/CaptureParentMode;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureParentMode;->$VALUES:[Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/camscanner/capture/CaptureParentMode;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/capture/CaptureParentMode;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

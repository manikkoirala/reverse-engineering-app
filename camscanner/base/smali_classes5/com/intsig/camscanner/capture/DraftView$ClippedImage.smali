.class Lcom/intsig/camscanner/capture/DraftView$ClippedImage;
.super Ljava/lang/Object;
.source "DraftView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/capture/DraftView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ClippedImage"
.end annotation


# static fields
.field public static Oooo8o0〇:F = 3.0f

.field public static 〇〇808〇:F = 0.2f


# instance fields
.field O8:F

.field OO0o〇〇:F

.field OO0o〇〇〇〇0:Z

.field Oo08:Landroid/graphics/RectF;

.field oO80:Landroid/graphics/Matrix;

.field o〇0:F

.field 〇080:Z

.field 〇80〇808〇O:Landroid/graphics/Matrix;

.field 〇8o8o〇:Z

.field 〇O8o08O:F

.field 〇o00〇〇Oo:Landroid/graphics/Bitmap;

.field 〇o〇:F

.field 〇〇888:F


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oO80(Landroid/graphics/RectF;FF)Z
    .locals 4

    .line 1
    const-string v0, "DraftView"

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz p1, :cond_1

    .line 5
    .line 6
    iget-boolean v2, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇080:Z

    .line 7
    .line 8
    if-nez v2, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v2, 0x2

    .line 12
    new-array v2, v2, [F

    .line 13
    .line 14
    aput p2, v2, v1

    .line 15
    .line 16
    const/4 p2, 0x1

    .line 17
    aput p3, v2, p2

    .line 18
    .line 19
    new-instance p3, Landroid/graphics/Matrix;

    .line 20
    .line 21
    invoke-direct {p3}, Landroid/graphics/Matrix;-><init>()V

    .line 22
    .line 23
    .line 24
    iget-object v3, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 25
    .line 26
    invoke-virtual {v3, p3}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 27
    .line 28
    .line 29
    invoke-virtual {p3, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 30
    .line 31
    .line 32
    aget p3, v2, v1

    .line 33
    .line 34
    aget p2, v2, p2

    .line 35
    .line 36
    invoke-virtual {p1, p3, p2}, Landroid/graphics/RectF;->contains(FF)Z

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    goto :goto_1

    .line 41
    :cond_1
    :goto_0
    const-string p1, "isInAnchorRect markerRotateRect == null"

    .line 42
    .line 43
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 47
    .line 48
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .line 50
    .line 51
    const-string p2, "isInAnchorRect :"

    .line 52
    .line 53
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    return v1
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/capture/DraftView$ClippedImage;Landroid/graphics/RectF;FF)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->oO80(Landroid/graphics/RectF;FF)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method


# virtual methods
.method public O8()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇080:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇080:Z

    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇O8o08O:F

    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OO0o〇〇〇〇0(FFFF)V
    .locals 15

    move-object v0, p0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o〇()[F

    move-result-object v1

    const/16 v2, 0x8

    .line 2
    aget v3, v1, v2

    sub-float v3, p1, v3

    float-to-double v3, v3

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    const/16 v7, 0x9

    aget v8, v1, v7

    sub-float v8, p2, v8

    float-to-double v8, v8

    invoke-static {v8, v9, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    add-double/2addr v3, v8

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    double-to-float v3, v3

    const/4 v4, 0x4

    .line 3
    aget v4, v1, v4

    const/4 v8, 0x0

    aget v8, v1, v8

    sub-float/2addr v4, v8

    float-to-double v8, v4

    invoke-static {v8, v9, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    const/4 v4, 0x5

    aget v4, v1, v4

    const/4 v10, 0x1

    aget v10, v1, v10

    sub-float/2addr v4, v10

    float-to-double v10, v4

    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v4, v8

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v4, v8

    .line 4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onRotateAction: a="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v9, ";b="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "DraftView"

    invoke-static {v9, v8}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    iget-object v8, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-double v8, v8

    invoke-static {v8, v9, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    iget-object v10, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-double v10, v10

    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    float-to-double v10, v3

    div-double/2addr v8, v5

    .line 6
    sget v5, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇〇808〇:F

    float-to-double v5, v5

    mul-double v5, v5, v8

    cmpl-double v12, v10, v5

    if-ltz v12, :cond_0

    sget v5, Lcom/intsig/camscanner/capture/DraftView;->oOO0880O:F

    cmpl-float v5, v3, v5

    if-ltz v5, :cond_0

    sget v5, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->Oooo8o0〇:F

    float-to-double v5, v5

    mul-double v8, v8, v5

    cmpg-double v5, v10, v8

    if-gtz v5, :cond_0

    div-float/2addr v3, v4

    .line 7
    iget-object v4, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇80〇808〇O:Landroid/graphics/Matrix;

    aget v5, v1, v2

    aget v6, v1, v7

    invoke-virtual {v4, v3, v3, v5, v6}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 8
    iget-object v4, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->oO80:Landroid/graphics/Matrix;

    aget v5, v1, v2

    aget v6, v1, v7

    invoke-virtual {v4, v3, v3, v5, v6}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 9
    iget v4, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->OO0o〇〇:F

    mul-float v4, v4, v3

    iput v4, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->OO0o〇〇:F

    .line 10
    :cond_0
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    .line 11
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    .line 12
    aget v5, v1, v2

    sub-float v5, p3, v5

    aget v6, v1, v7

    sub-float v6, p4, v6

    invoke-virtual {v3, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 13
    aget v5, v1, v2

    sub-float v5, p1, v5

    aget v6, v1, v7

    sub-float v6, p2, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 14
    invoke-virtual {p0, v3}, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇〇888(Landroid/graphics/PointF;)D

    move-result-wide v5

    .line 15
    invoke-virtual {p0, v4}, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇〇888(Landroid/graphics/PointF;)D

    move-result-wide v8

    .line 16
    iget v10, v3, Landroid/graphics/PointF;->x:F

    iget v11, v4, Landroid/graphics/PointF;->x:F

    mul-float v10, v10, v11

    iget v11, v3, Landroid/graphics/PointF;->y:F

    iget v12, v4, Landroid/graphics/PointF;->y:F

    mul-float v11, v11, v12

    add-float/2addr v10, v11

    float-to-double v10, v10

    mul-double v12, v5, v8

    div-double/2addr v10, v12

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    cmpl-double v14, v10, v12

    if-lez v14, :cond_1

    move-wide v10, v12

    .line 17
    :cond_1
    invoke-static {v10, v11}, Ljava/lang/Math;->acos(D)D

    move-result-wide v10

    const-wide v12, 0x4066800000000000L    # 180.0

    mul-double v10, v10, v12

    const-wide v12, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v10, v12

    .line 18
    iget v12, v3, Landroid/graphics/PointF;->x:F

    float-to-double v12, v12

    div-double/2addr v12, v5

    double-to-float v12, v12

    iput v12, v3, Landroid/graphics/PointF;->x:F

    .line 19
    iget v12, v3, Landroid/graphics/PointF;->y:F

    float-to-double v12, v12

    div-double/2addr v12, v5

    double-to-float v5, v12

    iput v5, v3, Landroid/graphics/PointF;->y:F

    .line 20
    iget v5, v4, Landroid/graphics/PointF;->x:F

    float-to-double v5, v5

    div-double/2addr v5, v8

    double-to-float v5, v5

    iput v5, v4, Landroid/graphics/PointF;->x:F

    .line 21
    iget v5, v4, Landroid/graphics/PointF;->y:F

    float-to-double v5, v5

    div-double/2addr v5, v8

    double-to-float v5, v5

    iput v5, v4, Landroid/graphics/PointF;->y:F

    .line 22
    new-instance v5, Landroid/graphics/PointF;

    iget v6, v4, Landroid/graphics/PointF;->y:F

    iget v4, v4, Landroid/graphics/PointF;->x:F

    neg-float v4, v4

    invoke-direct {v5, v6, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 23
    iget v4, v3, Landroid/graphics/PointF;->x:F

    iget v6, v5, Landroid/graphics/PointF;->x:F

    mul-float v4, v4, v6

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget v5, v5, Landroid/graphics/PointF;->y:F

    mul-float v3, v3, v5

    add-float/2addr v4, v3

    const/4 v3, 0x0

    cmpl-float v3, v4, v3

    if-lez v3, :cond_2

    goto :goto_0

    :cond_2
    neg-double v10, v10

    .line 24
    :goto_0
    iget v3, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇O8o08O:F

    float-to-double v3, v3

    add-double/2addr v3, v10

    double-to-float v3, v3

    iput v3, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇O8o08O:F

    .line 25
    iget-object v3, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇80〇808〇O:Landroid/graphics/Matrix;

    double-to-float v4, v10

    aget v5, v1, v2

    aget v6, v1, v7

    invoke-virtual {v3, v4, v5, v6}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 26
    iget-object v3, v0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->oO80:Landroid/graphics/Matrix;

    aget v2, v1, v2

    aget v1, v1, v7

    invoke-virtual {v3, v4, v2, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    return-void
.end method

.method public Oo08()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇〇888:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇0()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->o〇0:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method 〇80〇808〇O(IIFF)Z
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇080:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    int-to-float v0, v0

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->o〇0:F

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    int-to-float v0, v0

    .line 22
    iput v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇〇888:F

    .line 23
    .line 24
    new-instance v0, Landroid/graphics/Matrix;

    .line 25
    .line 26
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 27
    .line 28
    .line 29
    iget-object v2, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 30
    .line 31
    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 32
    .line 33
    .line 34
    const/4 v2, 0x2

    .line 35
    new-array v2, v2, [F

    .line 36
    .line 37
    aput p3, v2, v1

    .line 38
    .line 39
    const/4 p3, 0x1

    .line 40
    aput p4, v2, p3

    .line 41
    .line 42
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 43
    .line 44
    .line 45
    new-instance p4, Landroid/graphics/RectF;

    .line 46
    .line 47
    iget v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o〇:F

    .line 48
    .line 49
    iget v3, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->O8:F

    .line 50
    .line 51
    iget v4, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->o〇0:F

    .line 52
    .line 53
    add-float/2addr v4, v0

    .line 54
    int-to-float p1, p1

    .line 55
    sub-float/2addr v4, p1

    .line 56
    iget p1, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇〇888:F

    .line 57
    .line 58
    add-float/2addr p1, v3

    .line 59
    int-to-float p2, p2

    .line 60
    sub-float/2addr p1, p2

    .line 61
    invoke-direct {p4, v0, v3, v4, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 62
    .line 63
    .line 64
    aget p1, v2, v1

    .line 65
    .line 66
    aget p2, v2, p3

    .line 67
    .line 68
    invoke-virtual {p4, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    .line 73
    .line 74
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .line 76
    .line 77
    const-string p2, "isMovingClippedImage :"

    .line 78
    .line 79
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    const-string p2, "DraftView"

    .line 90
    .line 91
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    return v1
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method public 〇o00〇〇Oo(Landroid/graphics/Bitmap;FFLandroid/graphics/Matrix;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o〇:F

    .line 4
    .line 5
    iput p3, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->O8:F

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    cmpg-float p2, p2, v0

    .line 9
    .line 10
    if-gez p2, :cond_0

    .line 11
    .line 12
    iput v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o〇:F

    .line 13
    .line 14
    :cond_0
    cmpg-float p2, p3, v0

    .line 15
    .line 16
    if-gez p2, :cond_1

    .line 17
    .line 18
    iput v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->O8:F

    .line 19
    .line 20
    :cond_1
    const/4 p2, 0x1

    .line 21
    iput-boolean p2, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇080:Z

    .line 22
    .line 23
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 24
    .line 25
    .line 26
    move-result p2

    .line 27
    int-to-float p2, p2

    .line 28
    iput p2, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->o〇0:F

    .line 29
    .line 30
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    int-to-float p1, p1

    .line 35
    iput p1, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇〇888:F

    .line 36
    .line 37
    new-instance p1, Landroid/graphics/Matrix;

    .line 38
    .line 39
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 40
    .line 41
    .line 42
    iput-object p1, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->oO80:Landroid/graphics/Matrix;

    .line 43
    .line 44
    new-instance p1, Landroid/graphics/Matrix;

    .line 45
    .line 46
    invoke-direct {p1, p4}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 47
    .line 48
    .line 49
    iput-object p1, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 50
    .line 51
    const/4 p1, 0x0

    .line 52
    iput-boolean p1, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->OO0o〇〇〇〇0:Z

    .line 53
    .line 54
    iput v0, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇O8o08O:F

    .line 55
    .line 56
    const/high16 p1, 0x3f800000    # 1.0f

    .line 57
    .line 58
    iput p1, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->OO0o〇〇:F

    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method public 〇o〇()[F
    .locals 7

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    new-array v1, v0, [F

    .line 4
    .line 5
    new-array v0, v0, [F

    .line 6
    .line 7
    new-instance v2, Landroid/graphics/RectF;

    .line 8
    .line 9
    iget v3, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇o〇:F

    .line 10
    .line 11
    iget v4, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->O8:F

    .line 12
    .line 13
    iget v5, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->o〇0:F

    .line 14
    .line 15
    add-float/2addr v5, v3

    .line 16
    iget v6, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇〇888:F

    .line 17
    .line 18
    add-float/2addr v6, v4

    .line 19
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 20
    .line 21
    .line 22
    iget v3, v2, Landroid/graphics/RectF;->left:F

    .line 23
    .line 24
    const/4 v4, 0x0

    .line 25
    aput v3, v1, v4

    .line 26
    .line 27
    iget v4, v2, Landroid/graphics/RectF;->top:F

    .line 28
    .line 29
    const/4 v5, 0x1

    .line 30
    aput v4, v1, v5

    .line 31
    .line 32
    iget v5, v2, Landroid/graphics/RectF;->right:F

    .line 33
    .line 34
    const/4 v6, 0x2

    .line 35
    aput v5, v1, v6

    .line 36
    .line 37
    const/4 v6, 0x3

    .line 38
    aput v4, v1, v6

    .line 39
    .line 40
    const/4 v4, 0x4

    .line 41
    aput v5, v1, v4

    .line 42
    .line 43
    iget v4, v2, Landroid/graphics/RectF;->bottom:F

    .line 44
    .line 45
    const/4 v5, 0x5

    .line 46
    aput v4, v1, v5

    .line 47
    .line 48
    const/4 v5, 0x6

    .line 49
    aput v3, v1, v5

    .line 50
    .line 51
    const/4 v3, 0x7

    .line 52
    aput v4, v1, v3

    .line 53
    .line 54
    const/16 v3, 0x8

    .line 55
    .line 56
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    .line 57
    .line 58
    .line 59
    move-result v4

    .line 60
    aput v4, v1, v3

    .line 61
    .line 62
    const/16 v3, 0x9

    .line 63
    .line 64
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    .line 65
    .line 66
    .line 67
    move-result v2

    .line 68
    aput v2, v1, v3

    .line 69
    .line 70
    iget-object v2, p0, Lcom/intsig/camscanner/capture/DraftView$ClippedImage;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 71
    .line 72
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 73
    .line 74
    .line 75
    return-object v0
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇〇888(Landroid/graphics/PointF;)D
    .locals 2

    .line 1
    iget v0, p1, Landroid/graphics/PointF;->x:F

    .line 2
    .line 3
    mul-float v0, v0, v0

    .line 4
    .line 5
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 6
    .line 7
    mul-float p1, p1, p1

    .line 8
    .line 9
    add-float/2addr v0, p1

    .line 10
    float-to-double v0, v0

    .line 11
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    return-wide v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

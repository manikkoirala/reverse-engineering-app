.class public final Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "InvoiceView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/camscanner/capture/invoice/InvoiceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/capture/invoice/InvoiceView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 2
    .line 3
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p4, "e1"

    .line 2
    .line 3
    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p4, "e2"

    .line 7
    .line 8
    invoke-static {p2, p4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p4, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 12
    .line 13
    invoke-virtual {p4}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 14
    .line 15
    .line 16
    move-result p4

    .line 17
    const/4 v0, 0x0

    .line 18
    const/high16 v1, 0x3f800000    # 1.0f

    .line 19
    .line 20
    cmpg-float p4, p4, v1

    .line 21
    .line 22
    if-nez p4, :cond_0

    .line 23
    .line 24
    const/4 p4, 0x1

    .line 25
    goto :goto_0

    .line 26
    :cond_0
    const/4 p4, 0x0

    .line 27
    :goto_0
    if-nez p4, :cond_1

    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 30
    .line 31
    invoke-static {p1}, Lcom/intsig/camscanner/capture/invoice/InvoiceView;->o〇〇0〇(Lcom/intsig/camscanner/capture/invoice/InvoiceView;)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    const-string p2, "onFling for scale moving"

    .line 36
    .line 37
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return v0

    .line 41
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    sub-float/2addr p1, p2

    .line 50
    iget-object p2, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 51
    .line 52
    invoke-static {p2}, Lcom/intsig/camscanner/capture/invoice/InvoiceView;->O8ooOoo〇(Lcom/intsig/camscanner/capture/invoice/InvoiceView;)I

    .line 53
    .line 54
    .line 55
    move-result p2

    .line 56
    int-to-float p2, p2

    .line 57
    cmpl-float p2, p1, p2

    .line 58
    .line 59
    if-lez p2, :cond_2

    .line 60
    .line 61
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    .line 62
    .line 63
    .line 64
    move-result p2

    .line 65
    iget-object p4, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 66
    .line 67
    invoke-static {p4}, Lcom/intsig/camscanner/capture/invoice/InvoiceView;->〇oOO8O8(Lcom/intsig/camscanner/capture/invoice/InvoiceView;)I

    .line 68
    .line 69
    .line 70
    move-result p4

    .line 71
    int-to-float p4, p4

    .line 72
    cmpl-float p2, p2, p4

    .line 73
    .line 74
    if-lez p2, :cond_2

    .line 75
    .line 76
    iget-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 77
    .line 78
    invoke-static {p1}, Lcom/intsig/camscanner/capture/invoice/InvoiceView;->o〇〇0〇(Lcom/intsig/camscanner/capture/invoice/InvoiceView;)Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    const-string p2, "fling next one"

    .line 83
    .line 84
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    iget-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 88
    .line 89
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oo88o8O(F)[F

    .line 90
    .line 91
    .line 92
    iget-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 93
    .line 94
    invoke-static {p1}, Lcom/intsig/camscanner/capture/invoice/InvoiceView;->〇0000OOO(Lcom/intsig/camscanner/capture/invoice/InvoiceView;)Lcom/intsig/camscanner/capture/invoice/interfaces/IInvoiceView;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    if-eqz p1, :cond_3

    .line 99
    .line 100
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/invoice/interfaces/IInvoiceView;->next()V

    .line 101
    .line 102
    .line 103
    goto :goto_1

    .line 104
    :cond_2
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    .line 105
    .line 106
    .line 107
    move-result p1

    .line 108
    iget-object p2, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 109
    .line 110
    invoke-static {p2}, Lcom/intsig/camscanner/capture/invoice/InvoiceView;->O8ooOoo〇(Lcom/intsig/camscanner/capture/invoice/InvoiceView;)I

    .line 111
    .line 112
    .line 113
    move-result p2

    .line 114
    int-to-float p2, p2

    .line 115
    cmpl-float p1, p1, p2

    .line 116
    .line 117
    if-lez p1, :cond_3

    .line 118
    .line 119
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    .line 120
    .line 121
    .line 122
    move-result p1

    .line 123
    iget-object p2, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 124
    .line 125
    invoke-static {p2}, Lcom/intsig/camscanner/capture/invoice/InvoiceView;->〇oOO8O8(Lcom/intsig/camscanner/capture/invoice/InvoiceView;)I

    .line 126
    .line 127
    .line 128
    move-result p2

    .line 129
    int-to-float p2, p2

    .line 130
    cmpl-float p1, p1, p2

    .line 131
    .line 132
    if-lez p1, :cond_3

    .line 133
    .line 134
    iget-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 135
    .line 136
    invoke-static {p1}, Lcom/intsig/camscanner/capture/invoice/InvoiceView;->o〇〇0〇(Lcom/intsig/camscanner/capture/invoice/InvoiceView;)Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object p1

    .line 140
    const-string p2, "fling previous one"

    .line 141
    .line 142
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    iget-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 146
    .line 147
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oo88o8O(F)[F

    .line 148
    .line 149
    .line 150
    iget-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 151
    .line 152
    invoke-static {p1}, Lcom/intsig/camscanner/capture/invoice/InvoiceView;->〇0000OOO(Lcom/intsig/camscanner/capture/invoice/InvoiceView;)Lcom/intsig/camscanner/capture/invoice/interfaces/IInvoiceView;

    .line 153
    .line 154
    .line 155
    move-result-object p1

    .line 156
    if-eqz p1, :cond_3

    .line 157
    .line 158
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/invoice/interfaces/IInvoiceView;->previous()V

    .line 159
    .line 160
    .line 161
    :cond_3
    :goto_1
    return v0
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "e1"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "e2"

    .line 7
    .line 8
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    const/high16 p2, 0x3f800000    # 1.0f

    .line 18
    .line 19
    cmpl-float p1, p1, p2

    .line 20
    .line 21
    if-lez p1, :cond_0

    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 24
    .line 25
    neg-float p2, p3

    .line 26
    neg-float p3, p4

    .line 27
    invoke-static {p1, p2, p3}, Lcom/intsig/camscanner/capture/invoice/InvoiceView;->OOO〇O0(Lcom/intsig/camscanner/capture/invoice/InvoiceView;FF)V

    .line 28
    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 31
    .line 32
    const/4 p2, 0x1

    .line 33
    invoke-static {p1, p2, p2}, Lcom/intsig/camscanner/capture/invoice/InvoiceView;->〇00(Lcom/intsig/camscanner/capture/invoice/InvoiceView;ZZ)Landroid/graphics/PointF;

    .line 34
    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/camscanner/capture/invoice/InvoiceView$flingGestureDetector$1;->o0:Lcom/intsig/camscanner/capture/invoice/InvoiceView;

    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 39
    .line 40
    .line 41
    return p2

    .line 42
    :cond_0
    const/4 p1, 0x0

    .line 43
    return p1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

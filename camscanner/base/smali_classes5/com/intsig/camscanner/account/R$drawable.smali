.class public final Lcom/intsig/camscanner/account/R$drawable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/account/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final a_key_login_cs_logo:I = 0x7f08004d

.field public static final ab_baground_cs:I = 0x7f08004f

.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f080050

.field public static final abc_action_bar_item_background_material:I = 0x7f080051

.field public static final abc_btn_borderless_material:I = 0x7f080052

.field public static final abc_btn_check_material:I = 0x7f080053

.field public static final abc_btn_check_material_anim:I = 0x7f080054

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f080055

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f080056

.field public static final abc_btn_colored_material:I = 0x7f080057

.field public static final abc_btn_default_mtrl_shape:I = 0x7f080058

.field public static final abc_btn_radio_material:I = 0x7f080059

.field public static final abc_btn_radio_material_anim:I = 0x7f08005a

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f08005b

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f08005c

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f08005d

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f08005e

.field public static final abc_cab_background_internal_bg:I = 0x7f08005f

.field public static final abc_cab_background_top_material:I = 0x7f080060

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f080061

.field public static final abc_control_background_material:I = 0x7f080062

.field public static final abc_dialog_material_background:I = 0x7f080063

.field public static final abc_edit_text_material:I = 0x7f080064

.field public static final abc_ic_ab_back_material:I = 0x7f080065

.field public static final abc_ic_arrow_drop_right_black_24dp:I = 0x7f080066

.field public static final abc_ic_clear_material:I = 0x7f080067

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f080068

.field public static final abc_ic_go_search_api_material:I = 0x7f080069

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f08006a

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f08006b

.field public static final abc_ic_menu_overflow_material:I = 0x7f08006c

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f08006d

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f08006e

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f08006f

.field public static final abc_ic_search_api_material:I = 0x7f080070

.field public static final abc_ic_voice_search_api_material:I = 0x7f080071

.field public static final abc_item_background_holo_dark:I = 0x7f080072

.field public static final abc_item_background_holo_light:I = 0x7f080073

.field public static final abc_list_divider_material:I = 0x7f080074

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f080075

.field public static final abc_list_focused_holo:I = 0x7f080076

.field public static final abc_list_longpressed_holo:I = 0x7f080077

.field public static final abc_list_pressed_holo_dark:I = 0x7f080078

.field public static final abc_list_pressed_holo_light:I = 0x7f080079

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f08007a

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f08007b

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f08007c

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f08007d

.field public static final abc_list_selector_holo_dark:I = 0x7f08007e

.field public static final abc_list_selector_holo_light:I = 0x7f08007f

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f080080

.field public static final abc_popup_background_mtrl_mult:I = 0x7f080081

.field public static final abc_ratingbar_indicator_material:I = 0x7f080082

.field public static final abc_ratingbar_material:I = 0x7f080083

.field public static final abc_ratingbar_small_material:I = 0x7f080084

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f080085

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f080086

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f080087

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f080088

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f080089

.field public static final abc_seekbar_thumb_material:I = 0x7f08008a

.field public static final abc_seekbar_tick_mark_material:I = 0x7f08008b

.field public static final abc_seekbar_track_material:I = 0x7f08008c

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f08008d

.field public static final abc_spinner_textfield_background_material:I = 0x7f08008e

.field public static final abc_star_black_48dp:I = 0x7f08008f

.field public static final abc_star_half_black_48dp:I = 0x7f080090

.field public static final abc_switch_thumb_material:I = 0x7f080091

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f080092

.field public static final abc_tab_indicator_material:I = 0x7f080093

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f080094

.field public static final abc_text_cursor_material:I = 0x7f080095

.field public static final abc_text_select_handle_left_mtrl:I = 0x7f080096

.field public static final abc_text_select_handle_middle_mtrl:I = 0x7f080097

.field public static final abc_text_select_handle_right_mtrl:I = 0x7f080098

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f080099

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f08009a

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f08009b

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f08009c

.field public static final abc_textfield_search_material:I = 0x7f08009d

.field public static final abc_vector_test:I = 0x7f08009e

.field public static final account_common_btn_bg:I = 0x7f08009f

.field public static final ad_close_icon:I = 0x7f0800a5

.field public static final app_splash:I = 0x7f0800df

.field public static final avd_hide_password:I = 0x7f080139

.field public static final avd_show_password:I = 0x7f08013a

.field public static final baidu_toutiao_back:I = 0x7f08013b

.field public static final base_bg_light_black:I = 0x7f080183

.field public static final bg_alert_dialog_common:I = 0x7f0801b0

.field public static final bg_back_to_main:I = 0x7f0801b1

.field public static final bg_bg0_border1:I = 0x7f0801b4

.field public static final bg_blue_round_corner_normal:I = 0x7f0801bb

.field public static final bg_blue_round_corner_solid_white_normal:I = 0x7f0801bf

.field public static final bg_brand_corner_24:I = 0x7f0801c5

.field public static final bg_brand_corner_4:I = 0x7f0801c6

.field public static final bg_btn_19bcaa_corner2:I = 0x7f0801cb

.field public static final bg_btn_green_text:I = 0x7f0801d3

.field public static final bg_call_to_action_normal:I = 0x7f0801d9

.field public static final bg_call_to_action_pressed:I = 0x7f0801da

.field public static final bg_call_to_action_selector:I = 0x7f0801db

.field public static final bg_capture_number:I = 0x7f0801e9

.field public static final bg_colorbg0_border0_corner8:I = 0x7f08020a

.field public static final bg_complete_button:I = 0x7f08020b

.field public static final bg_corner_2_190dacd5:I = 0x7f08020e

.field public static final bg_corner_2_1919bc51:I = 0x7f08020f

.field public static final bg_corner_2_194580f2:I = 0x7f080210

.field public static final bg_corner_2_198150f8:I = 0x7f080211

.field public static final bg_corner_2_66000000:I = 0x7f080212

.field public static final bg_corner_2_bg1:I = 0x7f080213

.field public static final bg_corner_2_f1f1f1:I = 0x7f080215

.field public static final bg_corner_8_bg1:I = 0x7f08021b

.field public static final bg_custom_snack_bar:I = 0x7f080232

.field public static final bg_doc_image_stroke:I = 0x7f080246

.field public static final bg_f7f7f7_corner_4:I = 0x7f08025e

.field public static final bg_ffffff_corner_4:I = 0x7f080270

.field public static final bg_ffffff_corner_4_stroke_brand:I = 0x7f080274

.field public static final bg_ffffff_top_corner_12:I = 0x7f080278

.field public static final bg_gray_round_corner_disable:I = 0x7f080296

.field public static final bg_grey_round_corner_disable:I = 0x7f0802a0

.field public static final bg_guide_btn:I = 0x7f0802a3

.field public static final bg_light_black:I = 0x7f0802bd

.field public static final bg_light_green_round_corner_disable:I = 0x7f0802be

.field public static final bg_login_for_compliance_cb_show_pwd:I = 0x7f0802c1

.field public static final bg_login_for_compliance_protocol:I = 0x7f0802c2

.field public static final bg_more:I = 0x7f0802c9

.field public static final bg_more_title_bar:I = 0x7f0802cc

.field public static final bg_one_login_btn:I = 0x7f0802d7

.field public static final bg_orange_bottom_right_round:I = 0x7f0802de

.field public static final bg_radio_tab_checked:I = 0x7f0802fa

.field public static final bg_radius_commen_dark_btn:I = 0x7f0802fb

.field public static final bg_red_number:I = 0x7f0802fe

.field public static final bg_red_number_ff7255:I = 0x7f0802ff

.field public static final bg_round_corner_right_bottom:I = 0x7f080312

.field public static final bg_snack_bar:I = 0x7f08032f

.field public static final bg_start_page:I = 0x7f080332

.field public static final bg_tabhost_green:I = 0x7f080337

.field public static final bg_title_image_text_button:I = 0x7f080342

.field public static final bg_top_round:I = 0x7f080344

.field public static final bg_top_round_12:I = 0x7f080345

.field public static final bg_top_round_dfedf9_ffffff:I = 0x7f080349

.field public static final bg_verify_code_blank:I = 0x7f080351

.field public static final bg_verify_code_next:I = 0x7f080352

.field public static final bg_white_border1_corner24:I = 0x7f08035e

.field public static final bg_white_border2_corner4:I = 0x7f08035f

.field public static final bg_white_corner21:I = 0x7f080364

.field public static final bg_white_round_corner_normal:I = 0x7f080369

.field public static final bind_send_ver_code_btn_bg:I = 0x7f080374

.field public static final btn_alert_dialog:I = 0x7f08037b

.field public static final btn_check_alert_dialog:I = 0x7f08037e

.field public static final btn_checkbox_checked_mtrl:I = 0x7f08037f

.field public static final btn_checkbox_checked_to_unchecked_mtrl_animation:I = 0x7f080380

.field public static final btn_checkbox_unchecked_mtrl:I = 0x7f080381

.field public static final btn_checkbox_unchecked_to_checked_mtrl_animation:I = 0x7f080382

.field public static final btn_gray:I = 0x7f080383

.field public static final btn_radio_alert_dialog:I = 0x7f080396

.field public static final btn_radio_off_mtrl:I = 0x7f080399

.field public static final btn_radio_off_to_on_mtrl_animation:I = 0x7f08039a

.field public static final btn_radio_on_mtrl:I = 0x7f08039b

.field public static final btn_radio_on_to_off_mtrl_animation:I = 0x7f08039c

.field public static final cd_btn_check_off:I = 0x7f0803b0

.field public static final cd_btn_check_on:I = 0x7f0803b1

.field public static final cd_btn_radio_off:I = 0x7f0803b2

.field public static final cd_btn_radio_on:I = 0x7f0803b3

.field public static final cd_select_selected:I = 0x7f0803b4

.field public static final cd_select_unselected:I = 0x7f0803b5

.field public static final comm_bg_dialog_loading:I = 0x7f0803d8

.field public static final common_full_open_on_phone:I = 0x7f0803d9

.field public static final common_google_signin_btn_icon_dark:I = 0x7f0803da

.field public static final common_google_signin_btn_icon_dark_focused:I = 0x7f0803db

.field public static final common_google_signin_btn_icon_dark_normal:I = 0x7f0803dc

.field public static final common_google_signin_btn_icon_dark_normal_background:I = 0x7f0803dd

.field public static final common_google_signin_btn_icon_disabled:I = 0x7f0803de

.field public static final common_google_signin_btn_icon_light:I = 0x7f0803df

.field public static final common_google_signin_btn_icon_light_focused:I = 0x7f0803e0

.field public static final common_google_signin_btn_icon_light_normal:I = 0x7f0803e1

.field public static final common_google_signin_btn_icon_light_normal_background:I = 0x7f0803e2

.field public static final common_google_signin_btn_text_dark:I = 0x7f0803e3

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0803e4

.field public static final common_google_signin_btn_text_dark_normal:I = 0x7f0803e5

.field public static final common_google_signin_btn_text_dark_normal_background:I = 0x7f0803e6

.field public static final common_google_signin_btn_text_disabled:I = 0x7f0803e7

.field public static final common_google_signin_btn_text_light:I = 0x7f0803e8

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0803e9

.field public static final common_google_signin_btn_text_light_normal:I = 0x7f0803ea

.field public static final common_google_signin_btn_text_light_normal_background:I = 0x7f0803eb

.field public static final coom_bg_dialog_loading:I = 0x7f0803ed

.field public static final cs_launcher_300_200:I = 0x7f0803fc

.field public static final cs_launcher_300_200_router:I = 0x7f0803fd

.field public static final cs_seek_track:I = 0x7f080400

.field public static final design_fab_background:I = 0x7f080410

.field public static final design_ic_visibility:I = 0x7f080411

.field public static final design_ic_visibility_off:I = 0x7f080412

.field public static final design_password_eye:I = 0x7f080413

.field public static final design_snackbar_background:I = 0x7f080414

.field public static final dock_btn:I = 0x7f080422

.field public static final dot_background:I = 0x7f08043a

.field public static final dot_purchase_round:I = 0x7f08043d

.field public static final dot_purchase_round2:I = 0x7f08043e

.field public static final dot_purchase_round_d59845:I = 0x7f08043f

.field public static final edittext_cursor_drawable:I = 0x7f080458

.field public static final googleg_disabled_color_18:I = 0x7f08046a

.field public static final googleg_standard_color_18:I = 0x7f08046b

.field public static final gray_common_btn_bg:I = 0x7f08046e

.field public static final holo_checkbox_bg:I = 0x7f080494

.field public static final holo_common_btn_bg:I = 0x7f080495

.field public static final home_nav_search:I = 0x7f08049c

.field public static final home_select_selected:I = 0x7f08049d

.field public static final home_select_unselected:I = 0x7f08049e

.field public static final ic_ad_close:I = 0x7f0804d5

.field public static final ic_alipay_logo:I = 0x7f0804ed

.field public static final ic_arrow_back_black_24:I = 0x7f08050d

.field public static final ic_arrow_down:I = 0x7f08050f

.field public static final ic_arrow_left:I = 0x7f080516

.field public static final ic_cancel:I = 0x7f0805a5

.field public static final ic_cancel_16_16:I = 0x7f0805a6

.field public static final ic_cancellation:I = 0x7f0805a8

.field public static final ic_capture_progress:I = 0x7f0805d8

.field public static final ic_china_mobile_pay:I = 0x7f080618

.field public static final ic_circle_progress:I = 0x7f08061e

.field public static final ic_clear_black_24:I = 0x7f080621

.field public static final ic_clock_black_24dp:I = 0x7f080627

.field public static final ic_close_5a5a5a:I = 0x7f08062f

.field public static final ic_cloud_service_auth_bg:I = 0x7f080650

.field public static final ic_cloud_service_auth_left:I = 0x7f080651

.field public static final ic_cloud_service_auth_right:I = 0x7f080652

.field public static final ic_cloud_sync_1gb_44_44:I = 0x7f080654

.field public static final ic_cloud_sync_44_44:I = 0x7f080655

.field public static final ic_cloud_sync_auto_backup_44_44:I = 0x7f080656

.field public static final ic_cloud_sync_close:I = 0x7f080657

.field public static final ic_cloud_sync_cloud_store_44_44:I = 0x7f080658

.field public static final ic_cloud_sync_diff_device_44_44:I = 0x7f080659

.field public static final ic_cloud_sync_exam_erase_44_44:I = 0x7f08065a

.field public static final ic_cloud_sync_photo_recovery_44_44:I = 0x7f08065b

.field public static final ic_cloud_sync_share_dir_44_44:I = 0x7f08065c

.field public static final ic_cloud_sync_smart_erase_44_44:I = 0x7f08065d

.field public static final ic_code_verify_bg_3:I = 0x7f08068b

.field public static final ic_comm_redo:I = 0x7f08069f

.field public static final ic_comm_save:I = 0x7f0806a0

.field public static final ic_comm_undo:I = 0x7f0806a1

.field public static final ic_common_close:I = 0x7f0806a7

.field public static final ic_common_close_24px:I = 0x7f0806a9

.field public static final ic_common_close_white:I = 0x7f0806ad

.field public static final ic_delete_circle_gray:I = 0x7f0806ec

.field public static final ic_docjson_shortcut:I = 0x7f080729

.field public static final ic_done_36:I = 0x7f08073e

.field public static final ic_exclamationpoint:I = 0x7f080785

.field public static final ic_file_square_unchecked_20px:I = 0x7f0807a2

.field public static final ic_gp_guide_pay_type_selected:I = 0x7f080854

.field public static final ic_gp_guide_pay_type_unselected:I = 0x7f080855

.field public static final ic_green_frame:I = 0x7f08085b

.field public static final ic_home_search_hint:I = 0x7f0808a5

.field public static final ic_input_clear_18px:I = 0x7f08092f

.field public static final ic_keyboard_arrow_right:I = 0x7f080939

.field public static final ic_keyboard_black_24dp:I = 0x7f08093a

.field public static final ic_login_bottom_dialog_back:I = 0x7f080961

.field public static final ic_login_email_half_screen:I = 0x7f080962

.field public static final ic_login_email_login_bottom:I = 0x7f080963

.field public static final ic_login_email_me_page:I = 0x7f080964

.field public static final ic_login_email_me_page_cn:I = 0x7f080965

.field public static final ic_login_google:I = 0x7f080966

.field public static final ic_login_google_bg:I = 0x7f080967

.field public static final ic_login_guide_bubble_intro:I = 0x7f080968

.field public static final ic_login_head_icon:I = 0x7f080969

.field public static final ic_login_head_last_login:I = 0x7f08096a

.field public static final ic_login_mail:I = 0x7f08096b

.field public static final ic_login_mailphone:I = 0x7f08096c

.field public static final ic_login_main_home_bubble_bg:I = 0x7f08096d

.field public static final ic_login_main_home_bubble_bg_last_login:I = 0x7f08096e

.field public static final ic_login_main_selected:I = 0x7f08096f

.field public static final ic_login_main_unselected:I = 0x7f080970

.field public static final ic_login_mobile:I = 0x7f080971

.field public static final ic_login_mobile_half_screen:I = 0x7f080972

.field public static final ic_login_mobile_me_page:I = 0x7f080973

.field public static final ic_login_mobile_me_page_cn:I = 0x7f080974

.field public static final ic_login_protocol_dialog_close:I = 0x7f080977

.field public static final ic_login_protocol_select:I = 0x7f080978

.field public static final ic_login_protocol_unselect:I = 0x7f080979

.field public static final ic_login_tips_for_licence:I = 0x7f08097a

.field public static final ic_login_ways_back:I = 0x7f08097b

.field public static final ic_login_ways_bg:I = 0x7f08097c

.field public static final ic_login_ways_email:I = 0x7f08097d

.field public static final ic_login_ways_google_logo:I = 0x7f08097e

.field public static final ic_login_ways_logo:I = 0x7f08097f

.field public static final ic_login_ways_phone:I = 0x7f080980

.field public static final ic_login_ways_text:I = 0x7f080981

.field public static final ic_login_wechat:I = 0x7f080982

.field public static final ic_login_wechat_me_page:I = 0x7f080983

.field public static final ic_logo_google_play:I = 0x7f080985

.field public static final ic_m3_chip_check:I = 0x7f080993

.field public static final ic_m3_chip_checked_circle:I = 0x7f080994

.field public static final ic_m3_chip_close:I = 0x7f080995

.field public static final ic_more_black:I = 0x7f080a04

.field public static final ic_mtrl_checked_circle:I = 0x7f080a13

.field public static final ic_mtrl_chip_checked_black:I = 0x7f080a14

.field public static final ic_mtrl_chip_checked_circle:I = 0x7f080a15

.field public static final ic_mtrl_chip_close_circle:I = 0x7f080a16

.field public static final ic_new_pdf:I = 0x7f080a1d

.field public static final ic_outside_logo_cspdf:I = 0x7f080a6d

.field public static final ic_password_close:I = 0x7f080a8a

.field public static final ic_password_open:I = 0x7f080a8c

.field public static final ic_pdf_gallery_back:I = 0x7f080ab0

.field public static final ic_pwd_eye_close:I = 0x7f080b47

.field public static final ic_pwd_eye_open:I = 0x7f080b48

.field public static final ic_quick_login_close:I = 0x7f080b5a

.field public static final ic_recycle_scroll_thumb:I = 0x7f080b67

.field public static final ic_red_warning:I = 0x7f080b6e

.field public static final ic_reopen:I = 0x7f080b8b

.field public static final ic_return_line_24px:I = 0x7f080b93

.field public static final ic_return_line_white:I = 0x7f080b94

.field public static final ic_right_arrow:I = 0x7f080ba0

.field public static final ic_scroll_bar:I = 0x7f080bda

.field public static final ic_search_black_24:I = 0x7f080be0

.field public static final ic_search_cancel:I = 0x7f080be1

.field public static final ic_search_clear:I = 0x7f080be2

.field public static final ic_search_edittext_foucs:I = 0x7f080be4

.field public static final ic_search_edittext_normal:I = 0x7f080be5

.field public static final ic_sign_in_wechat:I = 0x7f080c78

.field public static final ic_sign_phone_cloud:I = 0x7f080c7a

.field public static final ic_sign_phone_equipment:I = 0x7f080c7b

.field public static final ic_sign_phone_message:I = 0x7f080c7c

.field public static final ic_tick_green:I = 0x7f080cd5

.field public static final ic_triangle_green_18px:I = 0x7f080d58

.field public static final ic_unionpay_logo:I = 0x7f080d65

.field public static final ic_verify_done:I = 0x7f080d80

.field public static final ic_verify_screenshot:I = 0x7f080d81

.field public static final ic_vip_10:I = 0x7f080d86

.field public static final ic_vip_20:I = 0x7f080d8b

.field public static final ic_vip_icon:I = 0x7f080d9f

.field public static final ic_vip_svg:I = 0x7f080dc9

.field public static final ic_wechat_timeline:I = 0x7f080df2

.field public static final ic_weixinpay_logo:I = 0x7f080df4

.field public static final ic_widget_capture_main_search:I = 0x7f080e03

.field public static final ic_widget_capture_multi_1_1:I = 0x7f080e04

.field public static final ic_widget_capture_multi_2_2:I = 0x7f080e05

.field public static final ic_widget_capture_qrcode_1_1:I = 0x7f080e06

.field public static final ic_widget_capture_qrcode_2_2:I = 0x7f080e07

.field public static final ic_widget_capture_single_1_1:I = 0x7f080e08

.field public static final ic_widget_capture_single_2_2:I = 0x7f080e09

.field public static final icon:I = 0x7f080e34

.field public static final icon_noti:I = 0x7f080e60

.field public static final icon_sliding_block_horizontal:I = 0x7f080e71

.field public static final icon_view_file:I = 0x7f080e7f

.field public static final img_android_home_close_24px:I = 0x7f080eae

.field public static final list_preference_selector_bg:I = 0x7f080faa

.field public static final list_selector_bg:I = 0x7f080fab

.field public static final list_selector_bg_both_design:I = 0x7f080fac

.field public static final list_selector_default:I = 0x7f080faf

.field public static final list_selector_default_bg_color_1:I = 0x7f080fb0

.field public static final list_selector_default_borderless:I = 0x7f080fb1

.field public static final list_selector_default_new:I = 0x7f080fb2

.field public static final list_selector_default_new_light:I = 0x7f080fb3

.field public static final list_selector_gray:I = 0x7f080fb4

.field public static final logo_start_page:I = 0x7f080fbb

.field public static final m3_appbar_background:I = 0x7f080fbc

.field public static final m3_avd_hide_password:I = 0x7f080fbd

.field public static final m3_avd_show_password:I = 0x7f080fbe

.field public static final m3_password_eye:I = 0x7f080fbf

.field public static final m3_popupmenu_background_overlay:I = 0x7f080fc0

.field public static final m3_radiobutton_ripple:I = 0x7f080fc1

.field public static final m3_selection_control_ripple:I = 0x7f080fc2

.field public static final m3_tabs_background:I = 0x7f080fc3

.field public static final m3_tabs_line_indicator:I = 0x7f080fc4

.field public static final m3_tabs_rounded_line_indicator:I = 0x7f080fc5

.field public static final m3_tabs_transparent_background:I = 0x7f080fc6

.field public static final material_cursor_drawable:I = 0x7f080fcb

.field public static final material_ic_calendar_black_24dp:I = 0x7f080fcc

.field public static final material_ic_clear_black_24dp:I = 0x7f080fcd

.field public static final material_ic_edit_black_24dp:I = 0x7f080fce

.field public static final material_ic_keyboard_arrow_left_black_24dp:I = 0x7f080fcf

.field public static final material_ic_keyboard_arrow_next_black_24dp:I = 0x7f080fd0

.field public static final material_ic_keyboard_arrow_previous_black_24dp:I = 0x7f080fd1

.field public static final material_ic_keyboard_arrow_right_black_24dp:I = 0x7f080fd2

.field public static final material_ic_menu_arrow_down_black_24dp:I = 0x7f080fd3

.field public static final material_ic_menu_arrow_up_black_24dp:I = 0x7f080fd4

.field public static final mtrl_bottomsheet_drag_handle:I = 0x7f080fd7

.field public static final mtrl_checkbox_button:I = 0x7f080fd8

.field public static final mtrl_checkbox_button_checked_unchecked:I = 0x7f080fd9

.field public static final mtrl_checkbox_button_icon:I = 0x7f080fda

.field public static final mtrl_checkbox_button_icon_checked_indeterminate:I = 0x7f080fdb

.field public static final mtrl_checkbox_button_icon_checked_unchecked:I = 0x7f080fdc

.field public static final mtrl_checkbox_button_icon_indeterminate_checked:I = 0x7f080fdd

.field public static final mtrl_checkbox_button_icon_indeterminate_unchecked:I = 0x7f080fde

.field public static final mtrl_checkbox_button_icon_unchecked_checked:I = 0x7f080fdf

.field public static final mtrl_checkbox_button_icon_unchecked_indeterminate:I = 0x7f080fe0

.field public static final mtrl_checkbox_button_unchecked_checked:I = 0x7f080fe1

.field public static final mtrl_dialog_background:I = 0x7f080fe2

.field public static final mtrl_dropdown_arrow:I = 0x7f080fe3

.field public static final mtrl_ic_arrow_drop_down:I = 0x7f080fe4

.field public static final mtrl_ic_arrow_drop_up:I = 0x7f080fe5

.field public static final mtrl_ic_cancel:I = 0x7f080fe6

.field public static final mtrl_ic_check_mark:I = 0x7f080fe7

.field public static final mtrl_ic_checkbox_checked:I = 0x7f080fe8

.field public static final mtrl_ic_checkbox_unchecked:I = 0x7f080fe9

.field public static final mtrl_ic_error:I = 0x7f080fea

.field public static final mtrl_ic_indeterminate:I = 0x7f080feb

.field public static final mtrl_navigation_bar_item_background:I = 0x7f080fec

.field public static final mtrl_popupmenu_background:I = 0x7f080fed

.field public static final mtrl_popupmenu_background_overlay:I = 0x7f080fee

.field public static final mtrl_switch_thumb:I = 0x7f080fef

.field public static final mtrl_switch_thumb_checked:I = 0x7f080ff0

.field public static final mtrl_switch_thumb_checked_pressed:I = 0x7f080ff1

.field public static final mtrl_switch_thumb_checked_unchecked:I = 0x7f080ff2

.field public static final mtrl_switch_thumb_pressed:I = 0x7f080ff3

.field public static final mtrl_switch_thumb_pressed_checked:I = 0x7f080ff4

.field public static final mtrl_switch_thumb_pressed_unchecked:I = 0x7f080ff5

.field public static final mtrl_switch_thumb_unchecked:I = 0x7f080ff6

.field public static final mtrl_switch_thumb_unchecked_checked:I = 0x7f080ff7

.field public static final mtrl_switch_thumb_unchecked_pressed:I = 0x7f080ff8

.field public static final mtrl_switch_track:I = 0x7f080ff9

.field public static final mtrl_switch_track_decoration:I = 0x7f080ffa

.field public static final mtrl_tabs_default_indicator:I = 0x7f080ffb

.field public static final navigation_empty_icon:I = 0x7f080ffc

.field public static final notification_action_background:I = 0x7f081002

.field public static final notification_bg:I = 0x7f081003

.field public static final notification_bg_low:I = 0x7f081004

.field public static final notification_bg_low_normal:I = 0x7f081005

.field public static final notification_bg_low_pressed:I = 0x7f081006

.field public static final notification_bg_normal:I = 0x7f081007

.field public static final notification_bg_normal_pressed:I = 0x7f081008

.field public static final notification_icon_background:I = 0x7f081009

.field public static final notification_icon_black:I = 0x7f08100a

.field public static final notification_template_icon_bg:I = 0x7f08100b

.field public static final notification_template_icon_low_bg:I = 0x7f08100c

.field public static final notification_tile_bg:I = 0x7f08100d

.field public static final notify_panel_notification_icon_bg:I = 0x7f08100e

.field public static final one_login_btn_pressed:I = 0x7f08101e

.field public static final one_login_btn_unabled:I = 0x7f08101f

.field public static final one_login_btn_unpressed:I = 0x7f081020

.field public static final one_login_checked:I = 0x7f081021

.field public static final one_login_unchecked:I = 0x7f081022

.field public static final popmenu_selector_bg:I = 0x7f081037

.field public static final progress_horizonntal:I = 0x7f08103f

.field public static final progress_horizonntal_new:I = 0x7f081040

.field public static final progress_horizonntal_to_return:I = 0x7f081041

.field public static final progress_horizonntal_unsubscribe_recall:I = 0x7f081042

.field public static final progress_horizontal_bg_1:I = 0x7f081043

.field public static final progress_small:I = 0x7f08104b

.field public static final progress_small_border:I = 0x7f08104c

.field public static final search_edittext_bg:I = 0x7f08105d

.field public static final seekbar_control_disabled_holo:I = 0x7f081061

.field public static final seekbar_control_focused_holo:I = 0x7f081062

.field public static final seekbar_control_normal_holo:I = 0x7f081063

.field public static final seekbar_control_pressed_holo:I = 0x7f081064

.field public static final seekbar_control_selector_holo:I = 0x7f081065

.field public static final seekbar_primary_holo:I = 0x7f081066

.field public static final seekbar_progress_horizontal_holo_light:I = 0x7f081067

.field public static final seekbar_secondary_holo:I = 0x7f081068

.field public static final seekbar_track_holo_light:I = 0x7f08106c

.field public static final selected_check_box:I = 0x7f08106d

.field public static final selector_checkbox_round_retangle_login_main:I = 0x7f08107a

.field public static final selector_radio_button:I = 0x7f081084

.field public static final selector_radio_button_off:I = 0x7f081085

.field public static final selector_radio_button_on:I = 0x7f081086

.field public static final selector_radio_button_red:I = 0x7f081087

.field public static final selector_radio_button_red_on:I = 0x7f081088

.field public static final shape_bg_19bc9c_corner_4dp:I = 0x7f0810c2

.field public static final shape_bg_f1f1f1_corner_4dp:I = 0x7f0810ff

.field public static final shape_bg_ffffff_corner_4_border_f1f1f1_1dp:I = 0x7f08112f

.field public static final shape_bg_ffffff_corner_6dp:I = 0x7f081135

.field public static final shape_bg_horizontal_gradient_ff5860_fd765a_corner25:I = 0x7f081149

.field public static final shape_bg_white_top_corner_12:I = 0x7f081167

.field public static final shape_colorbg2_corner2:I = 0x7f081179

.field public static final shape_rect_custom_toast:I = 0x7f0811b7

.field public static final test_level_drawable:I = 0x7f081211

.field public static final tooltip_frame_dark:I = 0x7f081214

.field public static final tooltip_frame_light:I = 0x7f081215

.field public static final unselected_check_box:I = 0x7f0812d4

.field public static final util_share_dlg_selector_bg:I = 0x7f0812d7

.field public static final util_share_dlg_selector_bg_transition:I = 0x7f0812d8

.field public static final vip_16px:I = 0x7f0812e6


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

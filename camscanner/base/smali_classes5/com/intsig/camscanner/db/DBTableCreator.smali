.class public Lcom/intsig/camscanner/db/DBTableCreator;
.super Ljava/lang/Object;
.source "DBTableCreator.java"


# direct methods
.method public static O8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS collaborators (_id INTEGER PRIMARY KEY AUTOINCREMENT,co_account_name TEXT,account_uid TEXT,collaborate_time INTEGER,doc_co_token TEXT,permission_state INTEGER DEFAULT 0,phone_area_code TEXT,sync_account_id INTEGER DEFAULT -1,display_name TEXT,collaborator_state INTEGER DEFAULT 0,document_id INTEGER DEFAULT 0,sync_extra_data1 TEXT,sync_extra_data2 TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static O8ooOoo〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS tags (_id INTEGER PRIMARY KEY AUTOINCREMENT,title TEXT NOT NULL,title_pinyin TEXT,description TEXT,icon_id INTEGER DEFAULT 0,icon BLOB,tag_num INTEGER NOT NULL DEFAULT 0,sync_account_id INTEGER DEFAULT -1,visible INTEGER DEFAULT 0,created_time INTEGER,last_modified INTEGER,sync_state INTEGER DEFAULT 1,sync_extra_state INTEGER,sync_timestamp INTEGER,sync_tag_id TEXT,data_version INTEGER,should_sync INTEGER DEFAULT 0,sync_extra_data1 TEXT,sync_extra_data2 TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static OO0o〇〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS gallery_pictures (_id INTEGER PRIMARY KEY AUTOINCREMENT,local_path TEXT,modified_date LONG,image_type INTEGER DEFAULT 0);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static OO0o〇〇〇〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS signature_contact (_id INTEGER PRIMARY KEY AUTOINCREMENT,sync_account_id TEXT,nickname TEXT,contact_user_id TEXT DEFAULT 0,contact_id TEXT DEFAULT 0,phone TEXT,email TEXT,account_status TEXT,area_code TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "CREATE TABLE IF NOT EXISTS invited_esign_doc (_id INTEGER PRIMARY KEY AUTOINCREMENT,sync_account_id TEXT,encrypt_id TEXT,sync_doc_version INTEGER DEFAULT 0,sync_tag_version INTEGER DEFAULT 0);"

    .line 7
    .line 8
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static OOO〇O0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS uploadstate (_id INTEGER PRIMARY KEY AUTOINCREMENT,doc_id INTEGER NOT NULL,site_id INTEGER,remote_id INTEGER DEFAULT 0,token TEXT,parent_id TEXT,tag TEXT,sync_account_id INTEGER);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static Oo08(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS comments (_id INTEGER PRIMARY KEY AUTOINCREMENT,server_id TEXT,comment_account TEXT,account_uid TEXT,comment_display_name TEXT,comment_time INTEGER,doc_co_token TEXT,doc_id INTEGER DEFAULT -1,comment_content TEXT,comment_state INTEGER DEFAULT 0,sync_account_id INTEGER DEFAULT -1,sync_extra_data1 TEXT,sync_extra_data2 TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static OoO8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS pdfsize (_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,pdf_width INTEGER,pdf_height INTEGER);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static Oooo8o0〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS graphics (_id INTEGER PRIMARY KEY AUTOINCREMENT,image_id INTEGER,pos_x INTEGER,pos_y INTEGER,width INTEGER,height INTEGER,scale FLOAT DEFAULT 1.0,dpi FLOAT DEFAULT 264.0,sync_extra_data1 TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static O〇8O8〇008(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS SyncDeleteStatus(_id INTEGER PRIMARY KEY AUTOINCREMENT,file_sync_id TEXT,web_recycle_status INTEGER DEFAULT 0,sync_account_id INTEGER DEFAULT -1);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o800o8O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS printtask(_id INTEGER PRIMARY KEY AUTOINCREMENT,filename TEXT,filepath TEXT NOT NULL,pages INTEGER DEFAULT -1,state INTEGER DEFAULT 1,email TEXT,printer TEXT,printerId TEXT,authtoken TEXT,tag TEXT,created INTEGER DEFAULT 0,time TEXT,sync_account_id INTEGER);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static oO80(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS documentlike (_id INTEGER PRIMARY KEY AUTOINCREMENT,serverid INTEGER DEFAULT -1,sync_account_id INTEGER DEFAULT -1,account TEXT,userid TEXT,displayname TEXT,time INTEGER,co_token TEXT,SYNC_EXTRA_DATA1 TEXT,SYNC_EXTRA_DATA2 TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static oo88o8O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS sharedapps (_id INTEGER PRIMARY KEY AUTOINCREMENT,package_name TEXT,class_name TEXT,last_share_time LONG,share_type INTEGER);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS message_center (_id INTEGER PRIMARY KEY AUTOINCREMENT,msg_id INTEGER,msg_type INTEGER,sub_type INTEGER,msg TEXT,extra TEXT,create_time LONG,expire_time LONG,update_time LONG,sender_id TEXT,receiver_id TEXT,cmd_id INTEGER,task_id TEXT,sync_account_id INTEGER DEFAULT -1,read_sync_state INTEGER DEFAULT 0);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o〇O8〇〇o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS sync_restore (_id INTEGER PRIMARY KEY AUTOINCREMENT,doc_version INTEGER,page_version INTEGER,tag_version INTEGER,sync_time INTEGER,sync_account_id INTEGER);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o〇〇0〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS teammembers(_id INTEGER PRIMARY KEY AUTOINCREMENT,user_id TEXT,nickname TEXT,account TEXT,team_token TEXT,sync_account_id INTEGER DEFAULT -1);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇00(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS messagecenters(_id INTEGER PRIMARY KEY AUTOINCREMENT,sync_account_id INTEGER DEFAULT -1,server_msgid TEXT,seq_num TEXT,msg_num TEXT,msg_id TEXT,title TEXT,jump_url TEXT,msg_abstract TEXT,create_time LONG,user_read_time LONG DEFAULT 0,type INTEGER DEFAULT -1,sub_type INTEGER DEFAULT -1,NEED_NOTIFY_SERVICE INTEGER DEFAULT 0,team_token TEXT,team_area INTEGER,team_title TEXT,op_account TEXT,op_nickname TEXT,update_type TEXT,permission INTEGER,event TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇0000OOO(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS teaminfos(_id INTEGER PRIMARY KEY AUTOINCREMENT,m_user_id TEXT,team_token TEXT,title TEXT,create_time LONG DEFAULT 0,upload_time LONG DEFAULT 0,pdf_orientation  INTEGER DEFAULT 0,pdf_size_id LONG DEFAULT -1,expiration LONG DEFAULT 0,serverTime LONG DEFAULT 0,status  INTEGER DEFAULT 0,member_num  INTEGER DEFAULT 0,lock  INTEGER DEFAULT 0,top_doc  INTEGER DEFAULT 0,area  INTEGER DEFAULT 1,sync_account_id INTEGER DEFAULT -1,max_layer_num  INTEGER DEFAULT 0,max_vip_total_num  INTEGER DEFAULT 10000,cur_total_num  INTEGER DEFAULT 0,root_dir_sync_id TEXT,title_sort_index TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇080(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS accounts (_id INTEGER PRIMARY KEY AUTOINCREMENT,username TEXT NOT NULL,password TEXT,authentation INTEGER DEFAULT 0,max_upload_size INTEGER DEFAULT 0,type INTEGER,sync_account_id INTEGER);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇0〇O0088o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS page_mark (_id INTEGER PRIMARY KEY AUTOINCREMENT,page_id INTEGER,mark_text TEXT,mark_rect_height FLOAT,mark_rect_width FLOAT,mark_rotate FLOAT,mark_text_color INTEGER,mark_text_font_size INTEGER,mark_text_font TEXT,mark_index INTEGER,mark_x FLOAT,mark_y FLOAT,sync_extra_data1 TEXT,sync_extra_data2 TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇80〇808〇O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS documents(_id INTEGER PRIMARY KEY AUTOINCREMENT,title TEXT NOT NULL ,_data TEXT ,pages INTEGER NOT NULL DEFAULT 0,state INTEGER DEFAULT 0,created INTEGER,modified INTEGER,tag_id INTEGER REFERENCES tags(_id),minithumb_data BLOB,thumb_state INTEGER DEFAULT 0,password TEXT,password_pdf TEXT,page_size INTEGER DEFAULT 0,page_orientation INTEGER DEFAULT 1,page_margin INTEGER DEFAULT 0,sync_account_id INTEGER DEFAULT -1,sync_doc_id TEXT,sync_timestamp INTEGER DEFAULT 0,sync_state INTEGER DEFAULT 1,sync_extra_state INTEGER DEFAULT 0,sync_version INTEGER DEFAULT 0,page_index_modified INTEGER DEFAULT 0,sync_ui_state INTEGER DEFAULT 0,co_token TEXT,belong_state INTEGER DEFAULT 0,co_state INTEGER DEFAULT 0,co_time INTEGER DEFAULT 0,co_tmp_time INTEGER DEFAULT 0,co_num_limit INTEGER DEFAULT 0,pdf_state INTEGER DEFAULT 0,auto_upload_state INTEGER DEFAULT 0,need_auto_upload INTEGER DEFAULT 0,title_sort_index TEXT,sync_extra_data1 TEXT,sync_extra_data2 TEXT,creator_user_id TEXT,creator_account TEXT,creator_nickname TEXT,upload_time LONG,last_upload_time LONG,permission INTEGER DEFAULT 2,team_token TEXT,sync_dir_id TEXT,type INTEGER DEFAULT 0,folder_type INTEGER DEFAULT 0,certificate_info TEXT,PDF_PAGE_NUM_LOCATION INTEGER DEFAULT 0,property TEXT,share_owner INTEGER DEFAULT 0,scenario_doc_type INTEGER DEFAULT 0,scenario_certificate_info TEXT,certificate_state INTEGER DEFAULT 0,scenario_recommend_status INTEGER DEFAULT 0,scenario_rename_hint_shown INTEGER DEFAULT 0,current_share_time INTEGER DEFAULT 0,file_type TEXT,file_source INTERGER DEFAULT 0,office_file_path TEXT,office_file_sync_state INTERGER DEFAULT 0,office_file_sync_state_backup INTERGER DEFAULT 0,office_file_sync_version INTERGER DEFAULT 0,office_file_upload_version INTERGER DEFAULT 0,office_page_size INTERGER DEFAULT 0,office_first_page_id TEXT,office_thumb_sync_state INTERGER DEFAULT 0,func_tags INTEGER DEFAULT 0,esign_info TEXT,pay_extra TEXT,pay_lock_file INTERGER DEFAULT 0,title_source INTERGER DEFAULT 0,ocr_title_local TEXT,ocr_title_cloud TEXT,page_view_mode INTEGER DEFAULT 0,is_electronic INTEGER DEFAULT 0,certificate_lock_file INTEGER DEFAULT 0,origin INTEGER DEFAULT 0,image_json_param TEXT,office_file_upload_state INTERGER DEFAULT 0,relation_word_id TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇8o8o〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS faxinfo (_id INTEGER PRIMARY KEY AUTOINCREMENT,sync_account_id INTEGER DEFAULT -1,number TEXT,countrycode TEXT,send_time INTEGER);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇O00(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS mtags (_id INTEGER PRIMARY KEY AUTOINCREMENT,document_id INTEGER DEFAULT 0,tag_id INTEGER DEFAULT 0,tag_sync_id TEXT,tag_source INTEGER DEFAULT 0);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇O888o0o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS qr_code (_id INTEGER PRIMARY KEY AUTOINCREMENT,code_format TEXT,recognized_content TEXT,image_path TEXT,alias TEXT,modified_date  TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇O8o08O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS faxtask(_id INTEGER PRIMARY KEY AUTOINCREMENT,filename TEXT,filepath TEXT NOT NULL,faxnumber TEXT NOT NULL,pages INTEGER DEFAULT -1,state INTEGER DEFAULT 1,txn_id INTEGER DEFAULT -1,subject TEXT,created INTEGER DEFAULT 0,time TEXT,sync_account_id INTEGER,country_code TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇O〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS invitesharedir (_id INTEGER PRIMARY KEY AUTOINCREMENT,sync_account_id INTEGER DEFAULT -1,share_id TEXT,upload_time LONG,member_number INTEGER DEFAULT 0,member_permissions INTEGER DEFAULT 3,sync_doc_version INTEGER DEFAULT 0,sync_tag_version INTEGER DEFAULT 0,dir_num INTEGER DEFAULT 0,dir_layer INTEGER DEFAULT 0);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS bank_card_journal_page (_id INTEGER PRIMARY KEY AUTOINCREMENT,sync_account_id TEXT,sync_image_id TEXT,start_trade_data TEXT,end_trade_data TEXT,title TEXT,income FLOAT DEFAULT 0,expenses FLOAT DEFAULT 0,upload_time LONG);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇oOO8O8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS teamfileinfos(_id INTEGER PRIMARY KEY AUTOINCREMENT,user_id TEXT,team_token TEXT,file_sync_id TEXT,user_permission INTEGER DEFAULT -2,create_time LONG DEFAULT 0,sync_account_id INTEGER DEFAULT -1);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇oo〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS signature(_id INTEGER PRIMARY KEY AUTOINCREMENT,image_id TEXT,signature_path TEXT,engine_size INTEGER,position  TEXT,signature_color TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇o〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS collaboratemsgs (_id INTEGER PRIMARY KEY AUTOINCREMENT,co_token TEXT,sync_account_id INTEGER DEFAULT -1,state INTEGER DEFAULT 0,doc_title TEXT,date INTEGER,operator TEXT,update_type TEXT,content TEXT,event TEXT,SYNC_EXTRA_DATA1 TEXT,SYNC_EXTRA_DATA2 TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇〇808〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS images (_id INTEGER PRIMARY KEY AUTOINCREMENT,_data TEXT NOT NULL,document_id INTEGER REFERENCES documents(_id),thumb_data TEXT,page_num INTEGER NOT NULL,note TEXT,status INTEGER DEFAULT 0,raw_data TEXT,image_border TEXT,ocr_result TEXT,ocr_result_user TEXT,ocr_border TEXT,enhance_mode INTEGER,image_titile TEXT,sync_account_id INTEGER DEFAULT -1,sync_timestamp INTEGER,sync_state INTEGER,sync_extra_state INTEGER,sync_version INTEGER DEFAULT 0,sync_image_id TEXT,created_time INTEGER,last_modified INTEGER,detail_index INTEGER DEFAULT -1,contrast_index INTEGER DEFAULT -1,bright_index INTEGER DEFAULT -1,image_rotation INTEGER DEFAULT 0,ori_rotation INTEGER DEFAULT -1,min_version FLOAT DEFAULT 0,max_version FLOAT DEFAULT 0,image_backup TEXT,sync_ui_state INTEGER DEFAULT 0,sync_jpage_timestamp INTEGER,sync_jpage_state INTEGER,sync_jpage_extra_state INTEGER,sync_jpage_version INTEGER,belong_state INTEGER DEFAULT 0,sync_raw_jpg_state INTEGER DEFAULT 0,image_confirm_state INTEGER DEFAULT 0,camcard_state INTEGER DEFAULT 0,cache_state INTEGER DEFAULT 0,sync_extra_data1 TEXT,sync_extra_data2 TEXT,page_water_maker_text TEXT,folder_type INTEGER DEFAULT 0,sync_jpg_error_type INTEGER DEFAULT 0,ocr_paragraph TEXT,ocr_time INTEGER DEFAULT 0,sign_border TEXT,file_type INTEGER DEFAULT 0,ocr_string TEXT,ocr_is_over INTEGER DEFAULT 0,trimmed_image_data TEXT,sync_trimmed_paper_jpg_state INTEGER DEFAULT 0,auto_wrap INTEGER DEFAULT 0,need_deblur_flag INTEGER DEFAULT 0,need_crop_dewarp_flag INTEGER DEFAULT 0,status_legal INTEGER DEFAULT 0,image_type INTEGER DEFAULT 0,pay_for_export TEXT,p_md5 TEXT,turned_to_pdf_page INTEGER DEFAULT 0,image_quality_status INTEGER DEFAULT -1,trim_switch_state INTEGER DEFAULT 0,parent_spell_page TEXT,child_spell_pages TEXT,ocr_type INTEGER DEFAULT 0);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇〇888(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS dirs (_id INTEGER PRIMARY KEY AUTOINCREMENT,sync_dir_id TEXT,parent_sync_id TEXT,title TEXT,create_time LONG,upload_time LONG,last_upload_time LONG,belong_state INTEGER DEFAULT 0,sync_state INTEGER DEFAULT 1,sync_account_id INTEGER DEFAULT -1,team_token TEXT,permission INTEGER DEFAULT 2,team_layer INTEGER DEFAULT 1,title_sort_index TEXT,folder_type INTEGER DEFAULT 0,share_id TEXT,share_status INTEGER DEFAULT 0,is_share_entry INTEGER DEFAULT 0,share_owner INTEGER DEFAULT 0,scenario_dir_type INTEGER DEFAULT 0,dir_view_mode INTEGER DEFAULT 0,template_id TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇〇8O0〇8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TABLE IF NOT EXISTS notepath (_id INTEGER PRIMARY KEY AUTOINCREMENT,graphics_id INTEGER,pen_type INTEGER,pen_color INTEGER,pen_width FLOAT,pen_points TEXT,sync_extra_data1 TEXT);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method
